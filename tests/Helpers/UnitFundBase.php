<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Helpers;

use App\Cytonn\Models\Unitization\UnitFund;

trait UnitFundBase
{
    public function setupUnitFundRequirements()
    {
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function getUnitFund($attributes = [])
    {
        return factory(UnitFund::class)->create($attributes);
    }
}
