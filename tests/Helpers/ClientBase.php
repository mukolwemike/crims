<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Helpers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\ContactCategory;
use App\Cytonn\Models\ContactEntity;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\ContactType;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstateInstructionType;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\ReservationForm;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\UnitHolding;

trait ClientBase
{
    /*
     * Setup Records for the title table
     */
    public function setupContactEntities()
    {
        $titles = [
            ['id' => '1', 'name' => 'individual'],
            ['id' => '2', 'name' => 'corporate']
        ];

        foreach ($titles as $title) {
            factory(ContactEntity::class)->create($title);
        }
    }

    /*
     * Setup Records for the title table
     */
    public function setupTitles()
    {
        $titles = [
            ['id' => '1', 'name' => 'Mr'],
            ['id' => '2', 'name' => 'Mrs']
        ];

        foreach ($titles as $title) {
            factory(Title::class)->create($title);
        }
    }

    /*
     * Setup Records for the countries table
     */
    public function setupCountries()
    {
        $countries = [
            ['id' => '1', 'name' => 'Afghanistan', 'iso_abbr' => 'AF'],
            ['id' => '2', 'name' => 'Kenya', 'iso_abbr' => 'KE'],
            ['id' => '3', 'name' => 'Uganda', 'iso_abbr' => 'UG'],
            ['id' => '4', 'name' => 'Tanzania', 'iso_abbr' => 'TZ']
        ];

        foreach ($countries as $country) {
            factory(Country::class)->create($country);
        }
    }


    /*
     * Setup records for the genders table
     */
    public function setupGenders()
    {
        $genders = [
            ['id' => '1', 'name' => 'Male', 'abbr' => 'M'],
            ['id' => '2', 'name' => 'Female', 'abbr' => 'F']
        ];

        foreach ($genders as $gender) {
            factory(Gender::class)->create($gender);
        }
    }

    /*
     * Setup records for the contact types table
     */
    public function setupContactTypes()
    {
        $types = [
            ['id' => '1', 'name' => 'Institution'],
            ['id' => '2', 'name' => 'Diaspora']
        ];

        foreach ($types as $type) {
            factory(ContactType::class)->create($type);
        }
    }

    /*
     * Setup records for the contact types table
     */
    public function setupContactCategories()
    {
        $categories = [
            ['id' => '1', 'name' => 'A'],
            ['id' => '2', 'name' => 'B']
        ];

        foreach ($categories as $category) {
            factory(ContactCategory::class)->create($category);
        }
    }

    /*
     * CLIENT
     */
    /*
     * Setup records for the client types table
     */
    public function setupClientType()
    {
        $types = [
            ['id' => '1', 'name' => 'individual'],
            ['id' => '2', 'name' => 'corporate']
        ];

        foreach ($types as $type) {
            factory(ClientType::class)->create($type);
        }
    }

    public function setupClient()
    {
        return factory(Client::class)->create();
    }

    public function setupREPaymentSchedules()
    {
        return factory(RealEstatePaymentSchedule::class)->create();
    }

    public function setupRealEstateInstructionType()
    {
        return factory(RealEstateInstructionType::class)->create();
    }

    public function setupUnit()
    {
        return factory(RealestateUnit::class)->create();
    }

    public function setupUnitsizes()
    {
        return factory(RealestateUnitSize::class)->create();
    }

    public function setupREUnitHolding()
    {
        return factory(UnitHolding::class)->create();
    }

    public function setupREPayments()
    {
        return factory(RealEstatePayment::class)->create();
    }

    public function setReservation()
    {
        return factory(ReservationForm::class)->create();
    }

    public function setupREProjects()
    {
        return factory(Project::class)->create();
    }

    /*
     * Setup records for the client types table
     */
    public function setupContactMethod()
    {
        $types = [
            ['id' => '1', 'description' => 'email'],
            ['id' => '2', 'description' => 'post']
        ];

        foreach ($types as $type) {
            factory(ContactMethod::class)->create($type);
        }
    }

    /*
     * Setup records for the corporate investor type table
     */
    public function setupCorporateInvestorType()
    {
        $types = [
            ['id' => '1', 'name' => 'Company'],
            ['id' => '2', 'name' => 'Trust']
        ];

        foreach ($types as $type) {
            factory(CorporateInvestorType::class)->create($type);
        }
    }

    /*
     * Setup records for the fundmanagers table
     */
    public function setupFundManager()
    {
        $types = [
            ['id' => '1', 'name' => 'CHYS'],
            ['id' => '2', 'name' => 'CPN']
        ];

        foreach ($types as $type) {
            factory(FundManager::class)->create($type);
        }
    }

    /*
     * Setup records for the employment table
     */
    public function setupEmployment()
    {
        $records = [
            ['id' => '1', 'name' => 'Employed'],
            ['id' => '2', 'name' => 'Self Employed']
        ];

        foreach ($records as $record) {
            factory(Employment::class)->create($record);
        }
    }

    public function getClientFilledInvestmentApplication($data = [])
    {
        $in_db = ClientFilledInvestmentApplication::where($data)->first();

        if ($in_db) {
            return $in_db;
        }

        return factory(ClientFilledInvestmentApplication::class)->create($data);
    }
}
