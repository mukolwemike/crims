<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/6/19
 * Time: 9:59 AM
 */

namespace Tests\Helpers;

use App\Cytonn\Models\ClientUser;

trait ClientAuth
{
    public function login()
    {
        $user = $this->getClientUser();

        $this->actingAs($user);

        return $user;
    }

    public function getClientUser()
    {
        return factory(ClientUser::class)->create();
    }
}
