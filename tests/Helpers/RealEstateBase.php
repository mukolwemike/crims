<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Helpers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\ProjectType;
use App\Cytonn\Models\RealestateCommission;
use App\Cytonn\Models\RealEstateCommissionParameter;
use App\Cytonn\Models\RealEstateCommissionPaymentScheduleType;
use App\Cytonn\Models\RealEstateCommissionPercentage;
use App\Cytonn\Models\RealestateCommissionRate;
use App\Cytonn\Models\RealEstateCommissionStructure;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\RealEstateSalesAgreement;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\RealEstateUnitTranchePricing;
use App\Cytonn\Models\RealEstateUnitTrancheSizing;
use App\Cytonn\Models\RealestateUnitType;
use App\Cytonn\Models\ReservationForm;
use App\Cytonn\Models\UnitHolding;

trait RealEstateBase
{
    public function setupRealEstateRequirements()
    {
        $this->setupProjectTypes();
        $this->setupProjects();
        $this->setupRealestateUnitSize();
        $this->setupRealestateUnitTypes();
        $this->setupRealestateUnitTranches();
        $this->setupRealestateUnitTrancheSizing();
        $this->setupRealEstatePaymentPlans();
        $this->setupRealEstateTranchePricing();
        $this->setupRealEstateUnit();
        $this->setupRealEstatePaymentTypes();
        $this->setupAdvocates();
        $this->setupRealEstateCommissionStructure();
        $this->setupRealEstateCommissionRates();
        $this->setupRealEstateCommissionPercentages();
        $this->setupRealEstateCommissionPaymentScheduleType();
        $this->setupRealEstateCommissionParameters();
    }

    public function setupProjectTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'Residential Development', 'slug' => 'residential_development'],
            ['id' => '2', 'name' => 'Master Planned Development', 'slug' => 'masterplanned_development'],
            ['id' => '3', 'name' => 'Sharplands', 'slug' => 'sharplands'],
        ];

        foreach ($records as $record) {
            factory(ProjectType::class)->create($record);
        }
    }

    public function setupProjects()
    {
        $records = [
            [
                'id' => '1',
                'name' => 'The Amara Ridge',
                'code' => '1003',
                'type_id' => 1,
                'reservation_fee' => 100000,
                'fund_manager_id' => 1,
                'custodial_account_id' => 1,
                'currency_id' => 1,
                'project_manager_id' => 1
            ],
            [
                'id' => '2',
                'name' => 'The Alma',
                'code' => '1004',
                'type_id' => 1,
                'reservation_fee' => 25000,
                'fund_manager_id' => 1,
                'custodial_account_id' => 1,
                'currency_id' => 1,
                'project_manager_id' => 1
            ],
        ];

        foreach ($records as $record) {
            factory(Project::class)->create($record);
        }
    }

    public function setupRealestateUnitSize()
    {
        $records = [
            ['id' => '1', 'name' => '1 Bedroom'],
            ['id' => '2', 'name' => '2 Bedroom'],
            ['id' => '3', 'name' => '3 Bedroom'],
            ['id' => '4', 'name' => '4 Bedroom'],
            ['id' => '5', 'name' => '5 Bedroom'],
        ];

        foreach ($records as $record) {
            factory(RealestateUnitSize::class)->create($record);
        }
    }

    public function setupRealestateUnitTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'Apartment'],
            ['id' => '2', 'name' => 'Villa'],
            ['id' => '3', 'name' => 'Plot'],
            ['id' => '4', 'name' => 'Cottage'],
            ['id' => '5', 'name' => 'Townhouse'],
            ['id' => '6', 'name' => 'Semi-detached Maisonette'],
        ];

        foreach ($records as $record) {
            factory(RealestateUnitType::class)->create($record);
        }
    }

    public function setupRealestateUnitTranches()
    {
        $records = [
            ['id' => '1', 'project_id' => '2', 'name' => 'Tranche One'],
            ['id' => '2', 'project_id' => '2', 'name' => 'Tranche Two'],
            ['id' => '3', 'project_id' => '2', 'name' => 'Tranche Three'],
            ['id' => '4', 'project_id' => '1', 'name' => 'Tranche One'],
            ['id' => '5', 'project_id' => '1', 'name' => 'Tranche Two'],
            ['id' => '6', 'project_id' => '1', 'name' => 'Tranche Three'],
        ];

        foreach ($records as $record) {
            factory(RealEstateUnitTranche::class)->create($record);
        }
    }

    public function setupRealestateUnitTrancheSizing()
    {
        $records = [
            ['id' => '1', 'tranche_id' => '1', 'number' => 20, 'size_id' => 1],
            ['id' => '2', 'tranche_id' => '1', 'number' => 30, 'size_id' => 2],
            ['id' => '3', 'tranche_id' => '1', 'number' => 40, 'size_id' => 3],
            ['id' => '4', 'tranche_id' => '2', 'number' => 20, 'size_id' => 1],
            ['id' => '5', 'tranche_id' => '2', 'number' => 30, 'size_id' => 2],
            ['id' => '6', 'tranche_id' => '2', 'number' => 40, 'size_id' => 3],
            ['id' => '7', 'tranche_id' => '3', 'number' => 20, 'size_id' => 1],
            ['id' => '8', 'tranche_id' => '3', 'number' => 30, 'size_id' => 2],
            ['id' => '9', 'tranche_id' => '3', 'number' => 40, 'size_id' => 3],
        ];

        foreach ($records as $record) {
            factory(RealEstateUnitTrancheSizing::class)->create($record);
        }
    }

    public function setupRealEstatePaymentPlans()
    {
        $records = [
            ['id' => '1', 'name' => 'Cash', 'slug' => 'cash'],
            ['id' => '2', 'name' => 'Installment', 'slug' => 'installment'],
            ['id' => '3', 'name' => 'Mortgage', 'slug' => 'mortgage'],
            ['id' => '4', 'name' => 'Zero Deposit Mortgage', 'slug' => 'zero_deposit_mortgage'],
            ['id' => '5', 'name' => 'Zero Deposit Installment', 'slug' => 'zero_deposit_installment'],
        ];

        foreach ($records as $record) {
            factory(RealEstatePaymentPlan::class)->create($record);
        }
    }

    public function setupRealEstateTranchePricing()
    {
        $records = [
            ['id' => '1', 'payment_plan_id' => '1', 'sizing_id' => '1', 'price' => '10000000', 'value' => '10000000'],
            ['id' => '2', 'payment_plan_id' => '2', 'sizing_id' => '1', 'price' => '11000000', 'value' => '11000000'],
            ['id' => '3', 'payment_plan_id' => '3', 'sizing_id' => '1', 'price' => '12000000', 'value' => '12000000'],
            ['id' => '4', 'payment_plan_id' => '4', 'sizing_id' => '1', 'price' => '13000000', 'value' => '13000000'],
            ['id' => '5', 'payment_plan_id' => '5', 'sizing_id' => '1', 'price' => '14000000', 'value' => '14000000'],
            ['id' => '6', 'payment_plan_id' => '1', 'sizing_id' => '2', 'price' => '15000000', 'value' => '15000000'],
            ['id' => '7', 'payment_plan_id' => '2', 'sizing_id' => '2', 'price' => '16000000', 'value' => '16000000'],
            ['id' => '8', 'payment_plan_id' => '3', 'sizing_id' => '2', 'price' => '17000000', 'value' => '17000000'],
            ['id' => '9', 'payment_plan_id' => '4', 'sizing_id' => '2', 'price' => '18000000', 'value' => '18000000'],
            ['id' => '10', 'payment_plan_id' => '5', 'sizing_id' => '2', 'price' => '19000000', 'value' => '19000000'],
            ['id' => '11', 'payment_plan_id' => '1', 'sizing_id' => '3', 'price' => '20000000', 'value' => '20000000'],
            ['id' => '12', 'payment_plan_id' => '2', 'sizing_id' => '3', 'price' => '21000000', 'value' => '21000000'],
            ['id' => '13', 'payment_plan_id' => '3', 'sizing_id' => '3', 'price' => '22000000', 'value' => '22000000'],
            ['id' => '14', 'payment_plan_id' => '4', 'sizing_id' => '3', 'price' => '23000000', 'value' => '23000000'],
            ['id' => '15', 'payment_plan_id' => '5', 'sizing_id' => '3', 'price' => '24000000', 'value' => '24000000'],
        ];

        foreach ($records as $record) {
            factory(RealEstateUnitTranchePricing::class)->create($record);
        }
    }

    public function setupRealEstateUnit()
    {
        $records = [
            ['id' => '1', 'project_id' => 2, 'number' => 'E-103', 'size_id' => 1, 'type_id' => 1],
            ['id' => '2', 'project_id' => 2, 'number' => 'H-702', 'size_id' => 2, 'type_id' => 1],
            ['id' => '3', 'project_id' => 2, 'number' => 'F-702', 'size_id' => 3, 'type_id' => 1],
            ['id' => '4', 'project_id' => 2, 'number' => 'F-903', 'size_id' => 1, 'type_id' => 1],
            ['id' => '5', 'project_id' => 2, 'number' => 'D-908', 'size_id' => 2, 'type_id' => 1],
            ['id' => '6', 'project_id' => 2, 'number' => 'G-801', 'size_id' => 2, 'type_id' => 1],
        ];

        foreach ($records as $record) {
            factory(RealestateUnit::class)->create($record);
        }
    }

    public function setupRealEstatePaymentTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'Reservation fees', 'slug' => 'reservation_fee'],
            ['id' => '2', 'name' => 'Deposit', 'slug' => 'deposit'],
            ['id' => '3', 'name' => 'Installment', 'slug' => 'installment'],
        ];

        foreach ($records as $record) {
            factory(RealEstatePaymentType::class)->create($record);
        }
    }

    public function setupAdvocates()
    {
        $records = [
            ['id' => '1', 'name' => 'X & Y Advocates'],
            ['id' => '2', 'name' => 'A & B Advocates'],
        ];

        foreach ($records as $record) {
            factory(Advocate::class)->create($record);
        }
    }

    public function setupRealEstateCommissionStructure()
    {
        $records = [
            ['id' => '1', 'effective_date' => '2015-01-01'],
            ['id' => '2', 'effective_date' => '2018-01-01'],
        ];

        foreach ($records as $record) {
            factory(RealEstateCommissionStructure::class)->create($record);
        }
    }

    public function setupRealEstateCommissionRates()
    {
        $records = [
            ['id' => '1', 'project_id' => '2', 'recipient_type_id' => 1, 'type' => 'percentage', 'amount' => 1.0],
            ['id' => '2', 'project_id' => '2', 'recipient_type_id' => 2, 'type' => 'percentage', 'amount' => 1.0],
            ['id' => '3', 'project_id' => '2', 'recipient_type_id' => 3, 'type' => 'percentage', 'amount' => 2.0],
            ['id' => '4', 'project_id' => '2', 'recipient_type_id' => 4, 'type' => 'percentage', 'amount' => 1.5],
            ['id' => '5', 'project_id' => '2', 'recipient_type_id' => 5, 'type' => 'percentage', 'amount' => 1.0],
        ];

        foreach ($records as $record) {
            factory(RealestateCommissionRate::class)->create($record);
        }
    }

    public function setupRealEstateCommissionPercentages()
    {
        $records = [
            ['id' => '1', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 1, 'value' => 10, 'stage' => null],
            ['id' => '2', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 2, 'value' => 35, 'stage' => null],
            ['id' => '3', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 3, 'value' => 35, 'stage' => null],
            ['id' => '4', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 4, 'value' => 6, 'stage' => 20],
            ['id' => '5', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 4, 'value' => 6, 'stage' => 40],
            ['id' => '6', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 4, 'value' => 6, 'stage' => 50],
            ['id' => '7', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 4, 'value' => 6, 'stage' => 60],
            ['id' => '8', 'realestate_commission_structure_id' => '2', 'realestate_commission_parameter_id' => 4, 'value' => 6, 'stage' => 100],
        ];

        foreach ($records as $record) {
            factory(RealEstateCommissionPercentage::class)->create($record);
        }
    }

    public function setupRealEstateCommissionPaymentScheduleType()
    {
        $records = [
            ['id' => '1', 'name' => 'Deposit', 'slug' => 'deposit', 'description' => 'Commission on payment of 10% deposit'],
            ['id' => '2', 'name' => 'Sales Agreement', 'slug' => 'sales_agreement', 'description' => 'Commission on signing sales agreement'],
            ['id' => '3', 'name' => 'Installment', 'slug' => 'installment', 'description' => 'Commission staggered with installments'],
        ];

        foreach ($records as $record) {
            factory(RealEstateCommissionPaymentScheduleType::class)->create($record);
        }
    }

    public function setupRealEstateCommissionParameters()
    {
        $records = [
            ['id' => '1', 'name' => 'Minimum Payment Percentage', 'slug' => 'minimum_payment_percentage'],
            ['id' => '2', 'name' => 'Deposit Award', 'slug' => 'deposit_award'],
            ['id' => '3', 'name' => 'Sales Agreement Award', 'slug' => 'sales_agreement_award'],
            ['id' => '4', 'name' => 'Installment Stages', 'slug' => 'installment_stages'],
        ];

        foreach ($records as $record) {
            factory(RealEstateCommissionParameter::class)->create($record);
        }
    }

    public function setupUnitHolding($attributes = [])
    {
        if (!isset($attributes['client_id'])) {
            $attributes['client_id'] = $this->getClient()->id;
        }

        return factory(UnitHolding::class)->create($attributes);
    }

    public function setupReservationForm($attributes = [])
    {
        return factory(ReservationForm::class)->create($attributes);
    }

    public function setupRealEstatePaymentSchedule($attributes = [])
    {
        return factory(RealEstatePaymentSchedule::class)->create($attributes);
    }

    public function setupRealEstatePayment($attributes = [])
    {
        return factory(RealEstatePayment::class)->create($attributes);
    }

    public function setupReserveUnit($attributes = [])
    {
        $holding = $this->setupUnitHolding($attributes);

        $this->setupReservationForm(['holding_id' => $holding->id]);

        $this->setupRealEstatePaymentSchedule([
            'date' => $holding->reservation_date,
            'unit_holding_id' => $holding->id,
            'amount' => 25000,
        ]);

        return $holding;
    }

    public function setupRealEstateCommission($attributes = [])
    {
        if (!isset($attributes['holding_id'])) {
            $attributes['holding_id'] = $this->setupReserveUnit();
        }

        return factory(RealestateCommission::class)->create($attributes);
    }

    public function makeRealEstatePayment(
        $holdingAttributes = [],
        $paymentAttributes = []
    ) {
        if (!isset($holdingAttributes['holding_id'])) {
            $attributes['holding_id'] = $this->setupReserveUnit($holdingAttributes);
        }

        return $this->setupRealEstatePayment($paymentAttributes);
    }

    public function getUnitHolding(
        $holdingAttributes = [],
        $commisionAttributes = [],
        $looAttributes = [],
        $salesAgreementAttributes = []
    ) {
        $holding = $this->setupReserveUnit($holdingAttributes);

        $commisionAttributes['holding_id'] = $holding->id;

        $rate = isset($commisionAttributes['commission_rate']) ? $commisionAttributes['commission_rate'] : 1;

        $commisionAttributes['amount'] = $rate * $holding->price() / 100;

        if (!isset($commisionAttributes['recipient_id'])) {
            $commisionAttributes['recipient_id'] = $this->getCommissionRecipient()->id;
        }

        $this->setupRealEstateCommission($commisionAttributes);

        if (!is_null($looAttributes)) {
            $looAttributes['holding_id'] = $holding->id;

            $this->getRealEstateLoo($looAttributes);
        }

        if (!is_null($salesAgreementAttributes)) {
            $salesAgreementAttributes['holding_id'] = $holding->id;

            $this->getRealEstateSaleAgreement($salesAgreementAttributes);
        }

        return $holding;
    }

    public function getRealEstateLoo($attributes = [])
    {
        return factory(RealestateLetterOfOffer::class)->create($attributes);
    }

    public function getRealEstateSaleAgreement($attributes = [])
    {
        return factory(RealEstateSalesAgreement::class)->create($attributes);
    }

    public function getRealEstateCommissionAmount($amount, $rate)
    {
        return $amount * $rate / 100;
    }
}
