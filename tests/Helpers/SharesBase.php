<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Helpers;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesCategory;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;

trait SharesBase
{
    /*
     * Setup the share entities requirements
     */
    public function setupSharesRequirements()
    {
        $this->setupShareEntities();
        $this->setupShareCategories();
    }

    /*
     * Setup a share holder record
     */
    public function getShareHolder($client, $holder_id = null)
    {
        return factory(ShareHolder::class)->create([
            'client_id' => $client->id,
            'id' => $holder_id
        ]);
    }

    /*
     * Get a share purchase record
     */
    public function getSharePurchase($shareHolder, $date = null)
    {
        $data = [
            'entity_id' => $shareHolder->entity_id,
            'client_id' => $shareHolder->client_id,
            'share_holder_id' => $shareHolder->id
        ];

        if ($date) {
            $data['date'] = $date;
        }

        return factory(SharePurchases::class)->create($data);
    }

    /*
     * Get a share sale record
     */
    public function getShareSale($shareHolder, $buyer, $date = null, $type = 1)
    {
        $data = [
            'entity_id' => $shareHolder->entity_id,
            'share_holder_id' => $shareHolder->id,
            'type' => $type
        ];

        if ($date) {
            $data['date'] = $date;
        }

        if ($buyer) {
            $data['buyer_id'] = $buyer->id;
        }

        return factory(ShareSale::class)->create($data);
    }
    
    /*
     * Setup Records for the share entities table
     */
    public function setupShareEntities()
    {
        $entities = [
            ['id' => '1', 'fund_manager_id' => '2', 'default_for' => '2', 'max_holding' => 20, 'account_id' => 2, 'currency_id' => 1],
            ['id' => '2', 'fund_manager_id' => '1', 'default_for' => null, 'max_holding' => 20, 'account_id' => 1, 'currency_id' => 1]
        ];

        foreach ($entities as $entity) {
            factory(SharesEntity::class)->create($entity);
        }
    }

    /*
     * Setup Records for the share categories table
     */
    public function setupShareCategories()
    {
        $entities = [
            ['id' => '1', 'name' => 'General'],
        ];

        foreach ($entities as $entity) {
            factory(SharesCategory::class)->create($entity);
        }
    }

    /**
     * Set up share holder available shares
     */
    public function setupShareSale($client = null)
    {
        $share_holder_id = $client ? $this->getShareHolder($client): 1;

        return factory(ShareSale::class)->create([
            'share_holder_id' => $share_holder_id,
        ]);
    }

    /**
     * Set up share holder available shares
     */
    public function setupSharePurchase($client = null)
    {
        $share_holder_id = $client ? $this->getShareHolder($client): 1;

        return factory(SharePurchases::class)->create([
            'client_id' => $client ? $client->id : 1,
            'share_holder_id' => $share_holder_id
        ]);
    }

    public function setupShareSaleOrder()
    {
        $client = $this->getClient(null, 1);

        $seller = $this->getShareHolder($client);

        return factory(SharesSalesOrder::class)->create([
            'seller_id' => $seller->id
        ]);
    }

    public function setupSharePurchaseOrder($attributes = [])
    {
        $client = $this->getClient(null, 2);

        $buyer = $this->getShareHolder($client);

        $attributes['buyer_id'] = $buyer->id;

        return factory(SharesPurchaseOrder::class)->create($attributes);
    }
}
