<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Helpers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientRank;
use App\Cytonn\Models\CommissionRecipientType;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Generator;

trait CommissionsBase
{
    /*
     * Setup records for the commission recipient type table
     */
    public function setupCommissionRecipientTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'Staff', 'slug' => 'staff'],
            ['id' => '2', 'name' => 'Retained FA', 'slug' => 'fa'],
            ['id' => '3', 'name' => 'IFA', 'slug' => 'ifa'],
            ['id' => '4', 'name' => 'Non Retained FA', 'slug' => 'non_retained_fa'],
            ['id' => '5', 'name' => 'Suspended Terminated FA', 'slug' => 'suspended_fa'],
        ];

        foreach ($records as $record) {
            factory(CommissionRecipientType::class)->create($record);
        }
    }

    /*
     * Setup records for the commission recipient ranks table
     */
    public function setupCommissionRecipientRanks()
    {
        $records = [
            ['id' => '1', 'name' => 'Financial Advisor', 'level' => 1],
            ['id' => '2', 'name' => 'Senior Financial Advisor', 'level' => 2],
            ['id' => '3', 'name' => 'Unit Manager', 'level' => 3],
            ['id' => '4', 'name' => 'Distribution Manager', 'level' => 4],
            ['id' => '5', 'name' => 'Staff', 'level' => 0],
            ['id' => '6', 'name' => 'Independent Financial Advisor', 'level' => 0],
            ['id' => '7', 'name' => 'Senior Distribution Manager', 'level' => 5],
            ['id' => '10', 'name' => 'Chief Distribution Officer', 'level' => 6],
        ];

        foreach ($records as $record) {
            factory(CommissionRecipientRank::class)->create($record);
        }
    }

    public function getCommissionRecipient($attributes = [])
    {
        return factory(CommissionRecepient::class)->create($attributes);
    }

    public function getInvestmentCommission($attributes = [])
    {
        return factory(Commission::class)->create($attributes);
    }

    public function getInvestmentAndCommissions(
        $investmentAttributes = [],
        $commissionAttributes = [],
        $createSchedules = true
    ) {
        if (!isset($investmentAttributes['client_id'])) {
            $investmentAttributes['client_id'] = $this->getClient()->id;
        }

        $investment = factory(ClientInvestment::class)->create($investmentAttributes);

        $commissionAttributes['investment_id'] = $investment->id;

        if (!isset($commissionAttributes['recipient_id'])) {
            $commissionAttributes['recipient_id'] = $this->getCommissionRecipient()->id;
        }

        $this->getInvestmentCommission($commissionAttributes);

        if ($createSchedules) {
            (app(Generator::class))->createSchedules($investment);
        }

        return $investment;
    }

    public function getCommissionAmount($amount, $rate, $days)
    {
        return ($amount * $days * $rate) / (100 * 365);
    }

    public function getBulkCommission($attributes = [])
    {
        if (!isset($attributes['start'])) {
            $attributes['start'] = Carbon::now()->toDateString();
            $attributes['end'] = Carbon::now()->addMonth()->toDateString();
        }

        return factory(BulkCommissionPayment::class)->create($attributes);
    }
}
