<?php
/**
 * Date: 06/05/2018
 * Time: 16:05
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Tests\Helpers;


use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\Payment;

trait ClientPayments
{
    public function addPayment(
        Client $client,
        $amount,
        Carbon $date = null,
        Product $product = null,
        Project $project = null,
        SharesEntity $entity = null,
        UnitFund $unitFund = null
    )
    {
        return Payment::make(
            $client,
            null,
            null,
            $product,
            $project,
            $entity,
            $date,
            $amount,
            'FI',
            'Amount added for testing purposes',
            $unitFund
        )->debit();
    }

    public function getBalance(
        Client $client,
        Carbon $date,
        Product $product = null,
        Project $project = null,
        SharesEntity $entity = null,
        UnitFund $unitFund = null
    )
    {
        return ClientPayment::balance(
            $client,
            $product,
            $project,
            $entity,
            $date,
            $unitFund
        );
    }
}