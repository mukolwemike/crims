<?php

namespace Tests\Helpers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;

trait LoyaltyBase
{
    public function setupClientInvestments(Client $client, $start, $end)
    {
        $records = [
            [
                'id' => 1,
                'client_id' => $client->id,
                'amount' => '5000000.00',
                'approval_id' => 1,
                'investment_type_id' => 1,
                'product_id' => 1,
                'interest_rate' => '18.00',
                'invested_date' => Carbon::parse($start),
                'maturity_date' => Carbon::parse($end),
                'withdrawn' => 1,
                'withdraw_amount' => 0,
                'rolled' => 1,
                'withdrawal_date' => Carbon::parse($end),
            ],
//            [
//                'id' => 2,
//                'client_id' => $client->id,
//                'amount' => '5058684.93',
//                'investment_type_id' => 3,
//                'approval_id' => 1,
//                'product_id' => 1,
//                'invested_date' => Carbon::parse('2015-01-27'),
//                'maturity_date' => Carbon::parse('2017-04-28'),
//                'withdrawn' => 1,
//                'withdraw_amount' => 0,
//                'rolled' => 1,
//                'withdrawal_date' => Carbon::parse('2017-04-28'),
//            ],
//
//            [
//                'id' => 3,
//                'client_id' => $client->id,
//                'amount' => '5000000.00',
//                'investment_type_id' => 2,
//                'approval_id' => 1,
//                'product_id' => 1,
//                'invested_date' => Carbon::parse('2017-04-28'),
//                'maturity_date' => Carbon::parse('2019-02-22'),
//                'withdrawn' => 1,
//                'withdraw_amount' => 0,
//                'rolled' => 1,
//                'withdrawal_date' => Carbon::parse('2019-03-03'),
//            ]
        ];

        foreach ($records as $record) {
            factory(ClientInvestment::class)->create($record);
        }
    }

//    public function saveLoyaltyRate()
//    {
//        $investments = ClientInvestment::whereNull('loyalty_rate')->get();
//
//        foreach ($investments as $investment) {
//            $tenor = $investment->client->repo->getConsecutiveMonthsInvested(Carbon::parse($investment->invested_date));
//
//            if ($tenor < 36) {
//                $investment->loyalty_rate = 0.00025;
//            } elseif ($tenor >= 36 && $tenor < 72) {
//                $investment->loyalty_rate = 0.00035;
//            } elseif ($tenor >= 72 && $tenor < 132) {
//                $investment->loyalty_rate = 0.00045;
//            } elseif ($tenor >= 132) {
//                $investment->loyalty_rate = 0.0005;
//            }
//
//            $investment->save();
//        }
//    }
}
