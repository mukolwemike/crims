<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Helpers;

use App\Cytonn\Models\ClientInstructionType;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientPaymentType;
use App\Cytonn\Models\CommissionRecipientRank;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\ProductType;
use App\Cytonn\Models\Rate;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Investment\InterestRepository;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Models\ClientInvestmentWithdrawalType;

trait InvestmentBase
{
    /*
     * Setup records for the currency table
     */
    public function setupCurrency()
    {
        $records = [
            ['id' => '1', 'code' => 'KES', 'name' => 'Kenya Shillings', 'symbol' => 'KES', 'base' => 1, 'exchange' => 1],
            ['id' => '2', 'code' => 'USD', 'name' => 'US Dollars', 'symbol' => '$', 'base' => 0, 'exchange' => 100],
        ];

        foreach ($records as $record) {
            factory(Currency::class)->create($record);
        }
    }

    /*
     * Setup records for the currency table
     */
    public function setupCustodialAccount()
    {
        $records = [
            ['id' => '1', 'account_name' => 'Cytonn Cash Mgt KES', 'currency_id' => 1, 'fund_manager_id' => 1],
            ['id' => '2', 'account_name' => 'Cytonn Cash Mgt USD', 'currency_id' => 2, 'fund_manager_id' => 1],
            ['id' => '3', 'account_name' => 'Seriani MMF Portfolio Account', 'currency_id' => 2, 'fund_manager_id' => 6],
        ];

        foreach ($records as $record) {
            factory(CustodialAccount::class)->create($record);
        }
    }

    /*
     * Setup records for the product type table
     */
    public function setupProductTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'CHYS', 'slug' => 'chys'],
            ['id' => '2', 'name' => 'Coop', 'slug' => 'coop']
        ];

        foreach ($records as $record) {
            factory(ProductType::class)->create($record);
        }
    }

    /*
     * Setup records for the document type table
     */
    public function setupDocumentTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'Email Indemnity', 'slug' => 'email_indemnity', 'folder' => null],
            ['id' => '2', 'name' => 'Letter of Offer', 'slug' => 'loo', 'folder' => 'loos'],
            ['id' => '3', 'name' => 'Sales Agreement', 'slug' => 'sales_agreement', 'folder' => 'sales_agreements'],
            ['id' => '4', 'name' => 'Contracts', 'slug' => 'contracts', 'folder' => null],
            ['id' => '5', 'name' => 'KYC Documents', 'slug' => 'kyc', 'folder' => null],
        ];

        foreach ($records as $record) {
            factory(DocumentType::class)->create($record);
        }
    }

    /*
     * Setup records for the product table
     */
    public function setupProduct()
    {
        $records = [
            ['id' => '1', 'name' => 'CHYS - KES', 'currency_id' => 1, 'custodial_account_id' => 1, 'fund_manager_id' => 1, 'type_id' => 1, 'active' => 1],
            ['id' => '2', 'name' => 'CHYS - USD', 'currency_id' => 2, 'custodial_account_id' => 2, 'fund_manager_id' => 1, 'type_id' => 1, 'active' => 1],
        ];

        foreach ($records as $record) {
            factory(Product::class)->create($record);
        }
    }

    /*
     * Setup records for the interest rate updates table
     */
    public function setupInterestRateUpdates()
    {
        $records = [
            ['id' => '1', 'product_id' => 1, 'description' => 'Rate Update KES', 'date' => '2016-12-01'],
            ['id' => '2', 'product_id' => 2, 'description' => 'Rate Update USD', 'date' => '2016-12-01'],
        ];

        foreach ($records as $record) {
            factory(InterestRateUpdate::class)->create($record);
        }
    }

    /*
     * Setup records for the rates table
     */
    public function setupRates()
    {
        $records = [
            ['id' => '1', 'product_id' => 1, 'tenor' => 1, 'tenor_type' => 1, 'rate' => 12, 'interest_rate_update_id' => 1, 'client_type' => 1, 'min_amount' => 500000, 'hidden' => 0],
            ['id' => '2', 'product_id' => 1, 'tenor' => 3, 'tenor_type' => 1, 'rate' => 15, 'interest_rate_update_id' => 1, 'client_type' => 1, 'min_amount' => 500000, 'hidden' => 0],
            ['id' => '3', 'product_id' => 1, 'tenor' => 6, 'tenor_type' => 1, 'rate' => 16, 'interest_rate_update_id' => 1, 'client_type' => 1, 'min_amount' => 500000, 'hidden' => 0],
            ['id' => '4', 'product_id' => 1, 'tenor' => 9, 'tenor_type' => 1, 'rate' => 17, 'interest_rate_update_id' => 1, 'client_type' => 1, 'min_amount' => 500000, 'hidden' => 0],
            ['id' => '5', 'product_id' => 1, 'tenor' => 12, 'tenor_type' => 1, 'rate' => 18, 'interest_rate_update_id' => 1, 'client_type' => 1, 'min_amount' => 500000, 'hidden' => 0],
        ];

        foreach ($records as $record) {
            factory(Rate::class)->create($record);
        }
    }

    /*
     * Setup records for the interest actions table
     */
    public function setupInterestActions()
    {
        $records = [
            ['id' => '1', 'name' => 'Send to bank', 'slug' => 'send_to_bank'],
            ['id' => '2', 'name' => 'Reinvest', 'slug' => 'reinvest'],
        ];

        foreach ($records as $record) {
            factory(InterestAction::class)->create($record);
        }
    }

    /*
     * Get a product
     */
    public function getProduct()
    {
        return factory(Product::class)->create();
    }

    public function getFund()
    {
        return factory(UnitFund::class)->create();
    }

    /*
     * Get a rate based on the passed array
     */
    public function setRate($rateArray)
    {
        $update_arr = array_only($rateArray, ['product_id']);

        $update_arr['date'] = Carbon::today()->subYear()->toDateString();
        $update = factory(InterestRateUpdate::class)->create($update_arr);
        $rateArray['interest_rate_update_id'] = $update->id;

        return factory(Rate::class)->create($rateArray);
    }

    /*
     * Setup the custodial transaction types
     */
    public function setupCustodialTransactionTypes()
    {
        $records = [
            ['id' => 1, 'name' => 'FI', 'description' => 'Client Funds In'],
            ['id' => 2, 'name' => 'FO', 'description' => 'Client funds out'],
            ['id' => 3, 'name' => 'I', 'description' => 'Fund Investment'],
            ['id' => 4, 'name' => 'R', 'description' => 'Fund Redemption'],
            ['id' => 5, 'name' => 'ECF', 'description' => 'Custodial fees'],
            ['id' => 6, 'name' => 'EMF', 'description' => 'Residual Income'],
            ['id' => 7, 'name' => 'EFC', 'description' => 'FA Commission'],
            ['id' => 8, 'name' => 'EFS', 'description' => 'Structuring Fees'],
            ['id' => 9, 'name' => 'RR', 'description' => 'Reconciliation'],
            ['id' => 10, 'name' => 'FMI', 'description' => 'Deposit by Principal Partner'],
            ['id' => 11, 'name' => 'TI', 'description' => 'Transfer In'],
            ['id' => 12, 'name' => 'TO', 'description' => 'Transfer Out'],
            ['id' => 13, 'name' => 'EFP', 'description' => 'Professional Fees'],
            ['id' => 14, 'name' => 'EABA', 'description' => 'Advisory Board Allowance'],
            ['id' => 15, 'name' => 'EOE', 'description' => 'Operating Expenses'],
            ['id' => 16, 'name' => 'EPE', 'description' => 'Penalty'],
            ['id' => 17, 'name' => 'ELF', 'description' => 'Legal Fees'],
            ['id' => 18, 'name' => 'EWT', 'description' => 'Withholding Tax'],
            ['id' => 19, 'name' => 'ETE', 'description' => 'Training expenses'],
        ];

        foreach ($records as $record) {
            factory(CustodialTransactionType::class)->create($record);
        }
    }

    /*
     * Setup the client payments types
     */
    public function setupClientPaymentTypes()
    {
        $records = [
            ['id' => 1, 'name' => 'Funds In', 'slug' => 'FI'],
            ['id' => 2, 'name' => 'Funds Out', 'slug' => 'FO'],
            ['id' => 3, 'name' => 'Investment', 'slug' => 'I'],
            ['id' => 4, 'name' => 'Withdrawal', 'slug' => 'W'],
            ['id' => 5, 'name' => 'Transfer In', 'slug' => 'TI'],
            ['id' => 6, 'name' => 'Transfer Out', 'slug' => 'TO'],
            ['id' => 7, 'name' => 'Membership fees', 'slug' => 'M'],
            ['id' => 8, 'name' => 'Unit Fund Fees', 'slug' => 'FEE'],
            ['id' => 9, 'name' => 'Member Redemption', 'slug' => 'MR']
        ];

        foreach ($records as $record) {
            factory(ClientPaymentType::class)->create($record);
        }
    }

    /*
     * Setup records for the investment types table
     */
    public function setupInvestmentTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'investment'],
            ['id' => '2', 'name' => 'topup'],
            ['id' => '3', 'name' => 'rollover'],
            ['id' => '4', 'name' => 'partial_withdraw'],
            ['id' => '5', 'name' => 'reinvested_interest'],
        ];

        foreach ($records as $record) {
            factory(ClientInvestmentType::class)->create($record);
        }
    }

    /*
     * Setup records for the withdrawal types table
     */
    public function setupInvestmentWithdrawalTypes()
    {
        $records = [
            ['id' => '1', 'slug' => 'sent_to_client', 'name' => 'Sent to client'],
            ['id' => '2', 'slug' => 'reinvested', 'name' => 'Reinvested'],
            ['id' => '3', 'slug' => 'retained_in_fund', 'name' => 'Retained in fund'],
        ];

        foreach ($records as $record) {
            factory(ClientInvestmentWithdrawalType::class)->create($record);
        }
    }

    /*
     * Setup records for the withdrawal types table
     */
    public function setupInvestmentInstructionTypes()
    {
        $records = [
            ['id' => '1', 'name' => 'rollover'],
            ['id' => '2', 'name' => 'withdraw'],
        ];

        foreach ($records as $record) {
            factory(ClientInstructionType::class)->create($record);
        }
    }

    public function getClientInvestment($attributes = [])
    {
        if (!isset($attributes['client_id'])) {
            $attributes['client_id'] = $this->getClient()->id;
        }

        return factory(ClientInvestment::class)->create($attributes);
    }

    public function getClientWithdrawal($attributes)
    {
        return factory(ClientInvestmentWithdrawal::class)->create($attributes);
    }

    public function getInvestment($investedDate = null, $maturityDate = null, $amount = 10000000)
    {
        if (is_null($investedDate)) {
            $investedDate = Carbon::now()->subDays(365);
        }

        if (is_null($maturityDate)) {
            $maturityDate = Carbon::now();
        }

        return $this->getInvestmentAndCommissions([
            'invested_date' => $investedDate,
            'maturity_date' => $maturityDate,
            'amount' => $amount
        ]);
    }

    public function getWithdrawal($investment, $date, $amount)
    {
        return $this->getClientWithdrawal([
            'investment_id' => $investment->id,
            'amount' => $amount,
            'date' => $date
        ]);
    }

    private function getTenorInDays(ClientInvestment $investment)
    {
        return Carbon::parse($investment->maturity_date)->diffInDays(Carbon::parse($investment->invested_date));
    }

    public function getClientInvestmentInstruction($attributes)
    {
        return factory(ClientInvestmentInstruction::class)->create($attributes);
    }

    public function getInvestmentInterestSchedules($investment)
    {
        (new InterestRepository())->generateInterestScheduleForInvestment($investment);
    }

    public function getClientPayment($attributes = [])
    {
        return factory(ClientPayment::class)->create($attributes);
    }

    public function getCustodialTransaction($attributes = [])
    {
        return factory(CustodialTransaction::class)->create($attributes);
    }

    public function getClientPaymentAndTransaction($paymentAttributes = [], $transactionAttributes = [])
    {
        $paymentAttributes['custodial_transaction_id'] = $this->getCustodialTransaction($transactionAttributes);

        return $this->getClientPayment($paymentAttributes);
    }
}
