<?php

namespace Tests\Helpers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;

trait USSDBase
{
    public function setupUSSDRequirements()
    {
        $this->setupChannel();
        $this->setupClientType();
        $this->setupFund();
        $this->setupFundSources();
        $this->setupInvestmentTypes();
        $this->setupContactEntities();
        $this->setupCountries();
    }

    public function getSessionBodyData($phone, $text = '')
    {
        $phone = ($phone) ? $phone : $this->phone;

        return [
            'sessionId' => 1,
            'serviceCode' => '639-02',
            'phoneNumber' => $phone,
            'text' => $text,
        ];
    }

    public function dialUp($phone = '', $text = '')
    {
        return $this->call('POST', '/ussd/' . $this->key, $this->getSessionBodyData($phone, $text));
    }

    public function setupFundSources()
    {
        $fundSources = [
            ['name' => 'Profits/Business'],
            ['name' => 'Rental/Property Sale'],
            ['name' => 'Sale of Shares'],
            ['name' => 'Dividends/interest'],
            ['name' => 'Maturing Investments'],
            ['name' => 'Loan'],
            ['name' => 'Savings '],
            ['name' => 'Pension'],
        ];

        foreach ($fundSources as $fundSource) {
            factory(FundSource::class)->create($fundSource);
        }
    }

    public function cleanPhone($phone)
    {
        $wplus = ltrim(trim($phone), '+');

        return ltrim($wplus, '254');
    }

    public function setupFund()
    {
        $fund = [
            'name' => 'Cytonn Money Market  Fund',
            'initial_unit_price' => 1,
            'minimum_investment_amount' => 5000.00,
            'fund_objectives' => 'Make Money for the client',
            'currency_id' => 1,
            'fund_manager_id' => 6,
            'short_name' => 'CMMF',
            'benefits' => 'Make Money for the client',
            'minimum_investment_horizon' => 14,
        ];

        factory(UnitFund::class)->create($fund);
    }

    public function setupChannel()
    {
        $channel = [
            'name' => 'USSD',
            'slug' => 'ussd'
        ];

        factory(Channel::class)->create($channel);
    }

    public function setupClientUser()
    {
        $details = [
            'username' => $this->faker->unique()->word,
            'email' => $this->faker->email,
            'password' => 'password',
            'phone_country_code' => '+254',
            'phone' => $this->cleanPhone($this->phone),
            'pin' => '1234',
            'login_attempts' => 0,
            'active' => 1,
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
        ];

        factory(ClientUser::class)->create($details);
    }

    public function setupUssdClient()
    {
        $phone = '0' . $this->cleanPhone($this->phone);

        return factory(Client::class)->create(['telephone_home' => $phone, 'phone' => $phone, 'telephone_office' => $phone]);
    }

    public function setupClientInvestmentApplication()
    {
        $details = [
            'unit_fund_id' => UnitFund::all()->random()->id,
            "type_id" => ClientType::where('name', 'individual')->first()->id,
            "joint" => false,
            "application_type_id" => ClientInvestmentType::where('name', 'investment')->first()->id,
            "client_id" => Client::first()->id,
            "amount" => $this->faker->numberBetween(100000, 10000000),
            "tenor" => $this->faker->numberBetween(3, 12),
            "lastname" => $this->faker->firstName,
            "middlename" => $this->faker->lastName,
            "firstname" => $this->faker->firstName,
            "id_or_passport" => Client::first()->id_or_passport,
            "telephone_home" => $this->cleanPhone($this->phone),
            "funds_source_id" => 1,
            "terms_accepted" => true,
        ];

        factory(ClientInvestmentApplication::class)->create($details);
    }

    public function setupClientInvestment()
    {
        factory(ClientInvestment::class)->create();
    }

    public function setupUnitPurchase($client, $fund)
    {
        $details = [
            'client_id' => $client->id,
            'unit_fund_id' => $fund->id,
            'client_payment_id' => factory(\App\Cytonn\Models\ClientPayment::class)->create()->id,
            'number' => $this->faker->numberBetween(1000, 1000000),
            'date' => $this->faker->date(),
            'description' => 'Purchase',
            'price' => 1
        ];

        factory(UnitFundPurchase::class)->create($details);
    }

    public function setupClientBank()
    {
        $details = [
            'name' => 'Mpesa',
            'swift_code' => 'MPESA',
        ];

        return (factory(ClientBank::class)->create($details));
    }

    public function setupClientBankBranch($bank)
    {
        $details = [
            'name' => $this->faker->name,
            'bank_id' => $bank->id,
        ];

        return (factory(ClientBankBranch::class)->create($details));
    }

    public function setupClientBankAccount($client, $branch, $bank)
    {
        dd($bank->branch, $branch);
        $details = [
            'client_id' => $client->id,
            'account_name' => $this->faker->firstName . ' ' . $this->faker->lastname,
            'account_number' => $this->faker->numberBetween(1000, 1000000),
            'bank_name' => $bank->name,
            'branch_name' => $branch->name,
            'branch_id' => $branch->id,
        ];

        factory(ClientBankAccount::class)->create($details);
    }
}
