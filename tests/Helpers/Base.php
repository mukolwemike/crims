<?php

namespace Tests\Helpers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\User;
use Cytonn\Investment\FundManager\FundManagerScope;

/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

trait Base
{
    use ClientBase;
    use InvestmentBase;
    use SharesBase;
    use CommissionsBase;
    use RealEstateBase;
    use USSDBase;
    use UnitFundBase;
    use LoyaltyBase;
    /*
     * Login to the system to start testing
     */
    public function login()
    {
        $this->setupClientRequirements();
        $this->setupInvestmentRequirements();

        $user = $this->getUser();

        $this->actingAs($user);
        $this->completeLogin();

        return $user;
    }

    public function setupForRealEstateTests()
    {
        $this->setupRealEstateRequirements();
    }

    /*
     * Get a user for testing
     */
    public function getUser($attributes = [])
    {
        return factory(User::class)->create($attributes);
    }

    public function completeLogin()
    {
        $scope = app(FundManagerScope::class);
        $scope->setSelectedFundManager(1);
    }

    /*
     * Setup the requirements for a contact record
     */
    public function setupContactRequirements()
    {
        $this->setupContactEntities();
        $this->setupTitles();
        $this->setupCountries();
        $this->setupGenders();
        $this->setupContactTypes();
        $this->setupContactCategories();
    }

    /*
     * Get a client record
     */
    public function getContact($data = [])
    {
        return factory(Contact::class)->create($data);
    }

    /*
     * Get a client record
     */
    public function getCorporateContact()
    {
        return factory(Contact::class)->create([
            'entity_type_id' => 2,
        ]);
    }

    /*
     * Setup the requirements for a client record
     */
    public function setupClientRequirements()
    {
        $this->setupContactRequirements();

        $this->setupClientType();
        $this->setupContactMethod();
        $this->setupCorporateInvestorType();
        $this->setupFundManager();
        $this->setupEmployment();
    }

    /*
     * Get a client record
     */
    public function getClient($contact = null, $client = null)
    {
        if (is_null($contact)) {
            $contact = $this->getContact();
        }

        $client = factory(Client::class)->create([
            'id' => $client,
            'contact_id' => $contact->id
        ]);

        return $client;
    }

    /*
     * Get a custom client record
     */
    public function getCustomClient($attributes = [])
    {
        if (!isset($attributes['contact_id'])) {
            $attributes['contact_id'] = $this->getContact()->id;
        }

        return factory(Client::class)->create($attributes);
    }

    /*
     * Setup investments requirements
     */
    public function setupInvestmentRequirements()
    {
        $this->setupCurrency();
        $this->setupCustodialAccount();
        $this->setupProductTypes();
        $this->setupInvestmentTypes();
        $this->setupProduct();
        $this->setupInterestRateUpdates();
        $this->setupRates();
        $this->setupInterestActions();
        $this->setupCustodialTransactionTypes();
        $this->setupInvestmentWithdrawalTypes();
        $this->setupCommissionRecipientTypes();
        $this->setupCommissionRecipientRanks();
        $this->setupInvestmentInstructionTypes();
        $this->setupDocumentTypes();
    }

    /*
     * Get an approval record
     */
    public function getClientTransactionApproval($attributes = [])
    {
        return factory(ClientTransactionApproval::class)->create($attributes);
    }
}
