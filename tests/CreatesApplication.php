<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    protected $appType = 'admin';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {

        $app = require __DIR__."/../bootstrap/apps/{$this->appType}.php";

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
