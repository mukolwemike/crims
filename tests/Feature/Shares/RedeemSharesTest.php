<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Shares;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareSale;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class RedeemSharesTest extends TestCase
{
    /*
     * Get the database transactions trait
     */
    use DatabaseTransactions;

    /*
     * Get the base class;
     */
    use Base;

    /*
     * Get the required data
     */
    protected $user;
    protected $faker;
    protected $client;
    protected $shareHolder;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        $this->setupSharesRequirements();

        $this->client = $this->getClient();

        $this->shareHolder = $this->getShareHolder($this->client);
    }

    /**
     * A test for the contact titles api
     *
     * @return void
     */
    public function testRedeemSharesList()
    {
        $now = Carbon::now()->toDateString();

        $purchase = $this->getSharePurchase($this->shareHolder, $now);

        $this->get('/api/shares/shareholders/redemption/holder/' . $this->shareHolder->id . '/date/' . $now)
            ->assertStatus(200)
            ->assertJsonFragment([
                'total_shares' => $purchase->number,
            ]);
    }

    /*
     * A test for the shares redeem post for approval
     *
     * @return void
     */
    public function testPostRedeemShares()
    {
        $now = Carbon::now()->toDateString();

        $data = [
            'date' => $now,
            'share_holder_id' => $this->shareHolder->id
        ];

        $this->post('/dashboard/shares/redeem/shareholders/' . $this->shareHolder->id, $data)
            ->followRedirects()
            ->assertStatus(200);

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'client_id' => $this->shareHolder->client_id,
            'transaction_type' => 'redeem_shares',
            'sent_by' => $this->user->id,
            'approved' => NULL,
            'payload' => serialize([
                'date' => $now,
                'share_holder_id' => $this->shareHolder->id
            ])
        ]);
    }

    /*
     * Test approval of shares redemption
     */
    public function testSharesRedeemApproval()
    {
        $now = Carbon::now()->toDateString();

        $purchase = $this->getSharePurchase($this->shareHolder, $now);

        $approval = factory(ClientTransactionApproval::class)->create([
            'client_id' => $this->shareHolder->client_id,
            'transaction_type' => 'redeem_shares',
            'payload' => [
                'date' => $now,
                'share_holder_id' => $this->shareHolder->id
            ],
            'sent_by' => $this->user->id
        ]);

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseHas((new ShareSale())->getTable(), [
            'sale_price' => $purchase->purchase_price,
            'date' => Carbon::parse($now),
            'approval_id' => $approval->id,
            'entity_id' => $purchase->entity_id,
            'share_holder_id' => $purchase->share_holder_id,
            'type' => 2,
            'share_purchase_id' => $purchase->id
        ]);
    }

}