<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Shares\Commissions;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use App\Cytonn\Models\SharesPurchaseOrder;
use Cytonn\Core\DataStructures\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class SharesCommissionFeatureTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        $this->setupSharesRequirements();
    }

    public function testCommissionDetailsRecordOnPurchaseOrder()
    {
        $client = $this->getClient();

        $shareHolder = $this->getShareHolder($client);

        $recipient = $this->getCommissionRecipient();

        $share_purchase_order_data = [
            'buyer_id' => $shareHolder->id,
            'request_date' => "2018-10-31",
            'number' => 3100,
            'price' => 55,
            'commission_recipient_id' => $recipient->id,
            'commission_rate' => 3.5,
            'good_till_filled_cancelled' => 1,
            'expiry_date' => null
        ];

        $url = 'dashboard/shares/purchases/order/'.$shareHolder->id;

        $this->post($url, $share_purchase_order_data)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Shares purchase order has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'make_shares_purchase_order',
            'client_id' => $shareHolder->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'make_shares_purchase_order')
            ->where('client_id', $client->id)
            ->latest()
            ->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseHas((new SharesPurchaseOrder())->getTable(), $share_purchase_order_data);
    }

//    public function testCommissionAwardedOnMatchSaleToPurchase()
//    {
//        $share_sale_order= $this->setupShareSaleOrder();
//
//        $recipient = $this->getCommissionRecipient();
//
//        $share_purchase_order = $this->setupSharePurchaseOrder([
//            'commission_recipient_id' => $recipient->id,
//            'commission_rate' => 4,
//            'commission_start_date' => Carbon::now()
//        ]);
//
//        $match_sale_to_purchase_data = [
//            'purchase_order_id' => [ $share_purchase_order->id ],
//            'date' => '2018-10-31',
//            'bargain_price' => 56,
//            'sales_order_id' => 33
//        ];
//
//        $url = '/dashboard/shares/sales/match/'.$share_sale_order->id;
//
//        $this->post($url, $match_sale_to_purchase_data)
//            ->assertRedirect()
//            ->followRedirects()
//            ->assertSeeText('Match for shares sales order has been saved for approval');
//
//        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
//            'transaction_type' => 'match_sale_to_purchase',
//            'client_id' => $share_sale_order->seller->client_id,
//            'sent_by' => Auth::id()
//        ]);
//
//        $approval = ClientTransactionApproval::where('transaction_type', 'match_sale_to_purchase')
//            ->latest()->first();
//
//        $this->approve($approval)
//            ->assertRedirect()
//            ->followRedirects()
//            ->assertStatus(200)
//            ->assertSeeText("Shares sales order was successfully matched to a shares purchase order(s)!");
//
//        $purchase = $share_purchase_order->sharePurchases()->latest()->first();
//
//        $sale = $share_purchase_order->shareSales()->latest()->first();
//
//        $commission = $share_purchase_order->commission_rate * $purchase->repo->getPurchasePrice() / 100;
//
//        $this->assertDatabaseHas((new SharesCommissionPaymentSchedule())->getTable(), [
//            'share_sale_id' => $sale->id,
//            'share_purchase_id' => $purchase->id,
//            'commission_recipient_id' => $recipient->id,
//            'commission' => $commission
//        ]);
//    }
}
