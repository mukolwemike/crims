<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 05/11/2018
 * Time: 12:20
 */

namespace Tests\Feature\Shares;

use App\Cytonn\Models\SharesEntity;
use Tests\TestCase;
use App\Cytonn\Models\ClientTransactionApproval;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\Helpers\ClientPayments;

class ShareHolderApplicationTest extends TestCase
{
    use DatabaseTransactions, Base, ClientPayments;

    protected $user;

    protected $faker;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        factory(\App\Cytonn\Models\ClientInvestmentType::class)->create(['name' => 'investment']);
        factory(\App\Cytonn\Models\ClientInvestmentType::class)->create(['name' => 'topup']);
    }

    public function testShareHolderAccountRequiredDetails()
    {
        $data['entity_id'] = factory(SharesEntity::class)->create()->id;
        $data['shareholder_number'] = null;

        $result = $this->post('api/client/shareholder/application', $data)
            ->isSuccessful();

        $this->assertTrue($result == 200);
    }

    public function testRegisterShareholderApplication()
    {
        $account = [
            'entity_id' => 2,
            'shareholder_number' => 'COOP-0123',
        ];

        $config = [
            'individual' => 1, // client_type
            'new_client' => 1, // new client
            'complete' => true,
            'franchise' => false,
            'transaction_type' => 'register_shareholder'
        ];

        $subscriber = [
            'dob' => $this->faker->date,
            'email' => $this->faker->email,
            "title" => 1,
            "firstname" => "Hanna",
            "middlename" => "Baker",
            "lastname" => "Styles",
            'funds_source_id' => 1,
            'funds_source_other' => "",
            'gender_id' => 1,
            'id_or_passport' => $this->faker->unique()->randomNumber($nbDigits = 8),
            'investor_account_name' => $this->faker->name,
            'investor_account_number' => $this->faker->unique()->randomNumber($nbDigits = 8),
            'investor_bank' => 2,
            'investor_bank_branch' => 1091,
            'jointHolders' => [],
            'method_of_contact_id' => 1,
            'pin_no' => "AJD2988U7873EH3",
            'postal_address' => "1234",
            'postal_code' => "00200",
            'telephone_home' => $this->faker->phoneNumber,
            'telephone_office' => $this->faker->phoneNumber,
            "type" => "register_shareholder",
        ];

        $data = $config + $account + $subscriber;

        // post and check approvals table
        $this->post('api/shares/shareholder/registration', $data)
            ->isSuccessful();

        $table = (new ClientTransactionApproval())->getTable();

        $this->assertDatabaseHas($table, ['transaction_type' => $config['transaction_type']]);
    }
}
