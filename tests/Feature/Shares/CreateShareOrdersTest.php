<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/30/18
 * Time: 8:37 PM
 */

namespace Tests\Feature\Shares;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Core\DataStructures\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class ShareOrderTest extends TestCase
{
    use DatabaseTransactions, Base;

    protected $user;

    protected $faker;

    protected $client;

    protected $shareHolder;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        $this->setupSharesRequirements();
    }

    /**
     * Test creation of share sale order
     */
    public function testCreateShareSaleOrder()
    {
        // Set up share holder and client

        $client = $this->getClient();

        $shareHolder = $this->getShareHolder($client);

        $this->setupSharePurchase();

        // Share sale order data

        $share_sale_order_data = [
            'seller_id' => $shareHolder->id,
            'request_date' => '2018-10-31',
            'number' => 200,
            'price'=> 50,
            'good_till_filled_cancelled' => 1,
            'expiry_date' => null
        ];

        // Save share sale order for approval

        $url = '/dashboard/shares/sales/order/'.$shareHolder->id;

        $this->post($url, $share_sale_order_data)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Shares sales order has been saved for approval');

        // Approve share sale order

        $approval = ClientTransactionApproval::where('transaction_type', 'make_shares_sales_order')
            ->where('client_id', $client->id)
            ->latest()
            ->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        // Confirm that sale order is created

        $this->assertDatabaseHas((new SharesSalesOrder())->getTable(), $share_sale_order_data);
    }
    
    /**
     * Test creation of share purchase order
     */
    public function testCreateSharePurchaseOrder()
    {
        //Set up

        $client = $this->getClient(null, 3);

        $shareHolder = $this->getShareHolder($client, 2);

        // Share purchase order data

        $share_purchase_order_data = [
            'buyer_id' => $shareHolder->id,
            'request_date' => "2018-10-31",
            'number' => 3100,
            'price' => 55,
            'commission_recipient_id' => 3,
            'commission_rate' => 3.5,
            'good_till_filled_cancelled' => 1,
            'expiry_date' => null
        ];

        //Save share purchase order for approval

        $url = 'dashboard/shares/purchases/order/'.$shareHolder->id;

        $this->post($url, $share_purchase_order_data)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Shares purchase order has been saved for approval');

        $approval = ClientTransactionApproval::where('transaction_type', 'make_shares_purchase_order')
            ->where('client_id', $client->id)
            ->latest()
            ->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        // Confirm that sale order is created

        $this->assertDatabaseHas((new SharesPurchaseOrder())->getTable(), $share_purchase_order_data);
    }
}
