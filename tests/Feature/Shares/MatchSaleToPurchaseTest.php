<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/31/18
 * Time: 1:37 PM
 */

namespace Tests\Feature\Shares;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\ShareSale;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\Helpers\ClientPayments;
use Tests\TestCase;
use Faker\Factory as Faker;

class MatchSaleToPurchaseTest extends TestCase
{
    use DatabaseTransactions, Base, ClientPayments;

    protected $user;

    protected $faker;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        $this->setupSharesRequirements();
    }

    public function testMatchSaleToPurchase()
    {
        $share_sale_order= $this->setupShareSaleOrder();

        $share_purchase_order = $this->setupSharePurchaseOrder();

        $match_sale_to_purchase_data = [
            'purchase_order_id' => [ $share_purchase_order->id ],
            'date' => '2018-10-31',
            'bargain_price' => 56,
            'sales_order_id' => 33
        ];

        $url = '/dashboard/shares/sales/match/'.$share_sale_order->id;

        $this->post($url, $match_sale_to_purchase_data)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Match for shares sales order has been saved for approval');

        $approval = ClientTransactionApproval::where('transaction_type', 'match_sale_to_purchase')
            ->latest()
            ->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        // Check if the purchase is made

        $this->assertDatabaseHas((new SharePurchases())->getTable(), [
            'number'                    => $share_purchase_order->number,
            'purchase_price'            => $match_sale_to_purchase_data['bargain_price'],
            'shares_purchase_order_id'  => $share_purchase_order->id,
            'shares_sales_order_id'     => $share_sale_order->id,
            'share_holder_id'           => $share_purchase_order->buyer_id,
            'category_id'               => 1,
        ]);

        //Check if the sale is made
        $this->assertDatabaseHas((new ShareSale())->getTable(), [
            'number'                    => $share_purchase_order->number,
            'sale_price'                => $match_sale_to_purchase_data['bargain_price'],
            'date'                      => Carbon::parse('2018-10-31'),
            'shares_purchase_order_id'  => $share_purchase_order->id,
            'shares_sales_order_id'     => $share_sale_order->id,
            'category_id'               => 1,
            'buyer_id'                   => $share_purchase_order->buyer_id
        ]);
    }

    public function testSalePurchaseSettlement()
    {
        //Set up

        $share_sale_order = $this->setupShareSaleOrder();

        $client = $this->getClient(null, 2);

        $share_purchase = $this->setupSharePurchase($client);

        $sale= $this->setupShareSale();

        $share_settlement_data = [
            'settlement_date' => '2018-10-02',
            'share_purchase_ids' => [ $share_purchase->id ],
            'sales_order_id' => $share_sale_order->id
        ];

        // Post share purchase settlement for approval

        $url = '/api/shares/sales/purchase-settlement';

        $this->post($url, $share_settlement_data)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Settlement for shares purchases has been saved for approval');

        $approval = ClientTransactionApproval::where('transaction_type', 'settle_sale_to_purchase')
            ->latest()
            ->first();

        // Add buyers cash in flow

        $payment = $this->addPayment(
            $client,
            $share_purchase->purchase_price * $share_purchase->number,
            Carbon::parse('2018-01-04'),
            null,
            null,
            $share_purchase->entity
        );

        // Approve settlement

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");
    }
}
