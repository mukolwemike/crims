<?php
/**
 * Created by PhpStorm.
 * User: yerick
 * Date: 25/10/2018
 * Time: 12:06
 */

namespace Tests\Feature\UnitTrust;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\Helpers\ClientPayments;
use Tests\TestCase;

class UnitFundTest extends TestCase
{
    use DatabaseTransactions, Base, ClientPayments;

    /*
     * Get the required data
     */
    protected $user;
    /**
     * @var Faker
     */
    protected $faker;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Factory::create();

        $this->user = $this->login();
    }

    public function testUnitFundPurchase()
    {
        $fund = $this->getFund();

        /**
         * Create fund purchase instruction
         */
        $purchase_data = [
            'unit_fund_id' => $fund->id,

        ];

        /**
         * Process fund purchase instructions
         */

        /**
         * Approve fund purchase instruction
         */
    }
}
