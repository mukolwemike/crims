<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Pensions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Unitization\Pensions\ProcessPensionMember;
use Faker\Factory as Faker;
use Tests\Helpers\Base;
use Tests\TestCase;

class ProcessPensionMemberTest extends TestCase
{
    use Base;

    protected $faker;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->setupClientRequirements();

        $this->setupInvestmentRequirements();

        $this->getUser([
            'username' => 'system'
        ]);
    }

    /**
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function testCreateNewMember()
    {
        $unitFund = $this->getUnitFund();

        $memberDetails = $this->getMemberDetails();

        $processMember = $this->getProcessPensionMember($unitFund, $memberDetails);

        $processMember->process();

        $this->assertTrue($processMember->getNewClient());

        $this->assertDatabaseHas((new Contact())->getTable(), [
            'email' => $memberDetails->E_mail,
            'phone' => $memberDetails->Phone_No
        ]);

        $contact = Contact::where('email', $memberDetails->E_mail)
            ->where('phone', $memberDetails->Phone_No)
            ->first();

        $this->assertDatabaseHas((new Client())->getTable(), [
            'contact_id' => $contact->id,
            'client_code' => $memberDetails->No_,
            'id_or_passport' => $memberDetails->ID_No,
            'pin_no' => $memberDetails->Pin_No,
            'telephone_home' => $memberDetails->Phone_No
        ]);
    }

    /**
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function testEditExistingMember()
    {
        $unitFund = $this->getUnitFund();

        $memberDetails = $this->getMemberDetails();

        $client = $this->getCustomClient([
            'client_code' => $memberDetails->No_
        ]);

        $processMember = $this->getProcessPensionMember($unitFund, $memberDetails);

        $processMember->process();

        $this->assertFalse($processMember->getNewClient());

        $clientCount = Client::where('client_code', $memberDetails->No_)->count();

        $this->assertEquals(1, $clientCount);

        $this->assertDatabaseHas((new Contact())->getTable(), [
            'id' => $client->contact_id,
            'email' => $memberDetails->E_mail,
            'phone' => $memberDetails->Phone_No
        ]);

        $contact = $client->contact;

        $this->assertDatabaseHas((new Client())->getTable(), [
            'id' => $client->id,
            'contact_id' => $contact->id,
            'client_code' => $memberDetails->No_,
            'id_or_passport' => $memberDetails->ID_No,
            'pin_no' => $memberDetails->Pin_No,
            'telephone_home' => $memberDetails->Phone_No
        ]);
    }

    /**
     * @param UnitFund $unitFund
     * @param $memberDetails
     * @return ProcessPensionMember
     */
    private function getProcessPensionMember(UnitFund $unitFund, $memberDetails)
    {
        return new ProcessPensionMember($memberDetails, $unitFund);
    }

    /**
     * @return object
     */
    private function getMemberDetails()
    {
        return (object)[
            'No_' => $this->faker->numberBetween(1000, 9999),
            'Member_Name' => $this->faker->name,
            'E_mail' => $this->faker->email,
            'Phone_No' => $this->faker->phoneNumber,
            'ID_No' => $this->faker->numberBetween(1000000, 100000000),
            'Pin_No' => $this->faker->numberBetween(1000000, 100000000),
            'DateofBirth' => $this->faker->date('d/m/Y'),
            'Dateofjoining' => $this->faker->date('d/m/Y'),
            'DateofRetirement' => $this->faker->date('d/m/Y')
        ];
    }
}
