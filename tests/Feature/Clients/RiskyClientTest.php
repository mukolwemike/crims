<?php
///**
// * Created by PhpStorm.
// * User: molukaka
// * Date: 21/11/2018
// * Time: 07:35
// */
//
//namespace Tests\Feature\Clients;
//
//use App\Cytonn\Models\Client\RiskyClient;
//use Faker\Factory as Faker;
//use Illuminate\Foundation\Testing\DatabaseTransactions;
//use Tests\Helpers\Base;
//use Tests\TestCase;
//
//class RiskyClientTest extends TestCase
//{
//    use DatabaseTransactions, Base;
//
//    protected $user;
//
//    protected $faker;
//
//
//    public function setUp()
//    {
//        parent::setUp();
//
//        $this->faker = Faker::create();
//
//        $this->user = $this->login();
//    }
//
//    public function testAddNewRiskyClient()
//    {
//        $riskyClient = (factory(RiskyClient::class)->create())->toArray();
//
//        $this->post('/api/client/risky/create', $riskyClient)
//            ->isSuccessful();
//
//        $table = (new RiskyClient())->getTable();
//
//        $this->assertDatabaseHas($table, ['firstname' => $riskyClient['firstname'], 'email' => $riskyClient['email']]);
//    }
//
//    public function testChangeRiskyStatus()
//    {
//       /** @todo */
//    }
//}
