<?php
/**
 * Date: 06/05/2018
 * Time: 15:23
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Tests\Feature\Clients;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use Carbon\Carbon;
use Tests\Helpers\Base;
;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Faker\Factory as Faker;

class PaymentsTest extends TestCase
{
    use DatabaseTransactions, Base;

    /*
     * Get the required data
     */
    protected $user;
    /**
     * @var Faker
     */
    protected $faker;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    public function testAddCashToAccount()
    {
        $this->get("dashboard/investments/accounts-cash")
            ->followRedirects()
            ->assertSuccessful()
            ->assertSeeText("Add Inflow");

        $this->get("dashboard/investments/accounts-cash/create")
            ->assertSuccessful()
            ->assertSeeText("Add Inflow");

        $product = $this->getProduct();

        $client_id = $this->getClient()->id;

        $this->post("/dashboard/investments/accounts-cash/save", [
            'category' => 'product',
            'project_id' => null,
            'product_id' => $product->id,
            'entity_id' => null,
            'unit_fund_id' => null,
            'custodial_account_id' => $product->custodialAccount->id,
            'exchange_rate' => 1,
            'source' => 'deposit',
            'bank_reference_no' => str_random(10),
            'amount' => 100000,
            'description' => 'Some long stories',
            'date' => Carbon::today(),
            'mpesa_confirmation_code' => null,
            'new_client' => false,
            'client_id' => $client_id,
            'recipient_known' => false
        ])->followRedirects()
        ->assertSuccessful()
        ->assertSeeText('saved for approval');

        $approval = ClientTransactionApproval::latest()->first();

        $this->approve($approval)->followRedirects()
            ->assertSuccessful()
            ->assertSeeText('approved');

        $this->assertDatabaseHas((new ClientPayment())->getTable(), [
            'amount' => 100000,
            'client_id' => $client_id
        ]);

        $this->assertDatabaseHas((new CustodialTransaction())->getTable(), [
            'amount' => 100000,
            'custodial_account_id' => $product->custodialAccount->id
        ]);
    }

    public function testAdditionOfCheque()
    {
        //TODO
    }

    public function testValuationOfCheque()
    {
        //TODO
    }

    public function testBouncingOfCheque()
    {
        //TODO
    }

    public function testLinkOfCashToClient()
    {
        //TODO
    }
}
