<?php

namespace Tests\Feature\Clients\LoyaltyPoints;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;

class LoyaltyPointsTest extends TestCase
{
    use DatabaseTransactions, Base;

    protected $user;

    protected $client;

    protected $faker;

    const REDEEM_LIMIT = 2000;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->login();

        $this->client = $this->setupClient();
    }

    /** @test */
    public function cantRedeem()
    {
        $this->setupClientInvestments($this->client, '2014-12-16', '2015-01-27');

        $this->saveLoyaltyRate();

        $scenario1 = $this->client->calculateLoyalty()->getTotalInvestmentsPoints();

        $this->assertNotTrue($scenario1 > self::REDEEM_LIMIT);
    }

    /** @test */
    public function canRedeem()
    {
        $this->setupClientInvestments($this->client, '2014-12-16', '2018-01-27');

        $this->saveLoyaltyRate();

        $scenario2 = $this->client->calculateLoyalty()->getTotalInvestmentsPoints();

        $this->assertTrue($scenario2 > self::REDEEM_LIMIT);
    }

    /** @test */
    public function calculateClientUnitTrustInvestmentsPoints()
    {
    }

    /** @test */
    public function calculateClientREInvestmentsPoints()
    {
    }

    /** @test */
    public function redeemClientLoyaltyPoints()
    {
    }
}
