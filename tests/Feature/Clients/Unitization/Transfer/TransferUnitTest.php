<?php

namespace Tests\Feature\Clients\Unitization\Transfer;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;

class TransferUnitTest extends TestCase
{
    use DatabaseTransactions, Base;
}
