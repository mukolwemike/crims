<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/6/19
 * Time: 12:36 PM
 */

namespace Tests\Feature\Clients\RealEstate;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\ClientAuth;
use Tests\Helpers\ClientBase;
use Faker\Factory as Faker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    use DatabaseTransactions, ClientBase, ClientAuth;

    protected $appType = 'clients';

    protected $client;

//    protected $unit;

    protected $holding;

    protected $faker;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->login();

        $this->client = $this->setupClient();

        $this->setupUnit();

        $this->setupUnitsizes();

        $this->holding = $this->setupREUnitHolding();

        $this->setupREPayments();

        $this->setReservation();

        $this->setupREProjects();
    }

    public function testRealEstateApis()
    {
        // RE SUMMARY
        $summaries = $this->json('GET', '/api/'.$this->client->uuid.'/re-summaries');

        $summaries->assertJson([
            'status' => 201
        ]);

        // RE UNIT HOLDING DETAILS
        $holding = $this->json('GET', '/api/'.$this->holding->id.'/unit-details');

        $holding->assertJson([
            'status' => 201
        ]);

        // RE PAYMENT SCHEDULES
        $schedules = $this->json('GET', '/api/'.$this->holding->id.'/payment-schedules');

        $schedules->assertJson([
            'status' => 201
        ]);
    }
}
