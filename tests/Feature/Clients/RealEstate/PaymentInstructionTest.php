<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/5/19
 * Time: 1:02 PM
 */

namespace Tests\Feature\Clients\RealEstate;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\ClientAuth;
use Tests\Helpers\ClientBase;
use Faker\Factory as Faker;
use Tests\TestCase;

class PaymentInstructionTest extends TestCase
{
    use DatabaseTransactions, ClientBase, ClientAuth;

    protected $appType = 'clients';

    protected $client;

    protected $schedule;

    protected $holding;

    protected $type;

    protected $faker;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->login();

        $this->client = $this->setupClient();

        $this->schedule = $this->setupREPaymentSchedules();

        $this->type = $this->setupRealEstateInstructionType();

        $this->holding = $this->setupREUnitHolding();
    }

    public function testPostPaymentInstruction()
    {
        $data = [
            'client' => $this->client->uuid,
            'amount' => 20000,
            'description' => 'This is a test description for the payment',
            'schedule_id' => $this->schedule->id,
            'date' => '2019-03-05',
            'mode_of_payment' => 'mpesa',
            'type' => $this->type->slug,
            'holding_id' => $this->holding->id
        ];

        $response = $this->json('POST', '/api/client/make-payments/'.$this->client->uuid, $data);

        $response
            ->assertStatus(201)
            ->assertJson([
                'status' => 201,
                'message' => 'Payment uploaded successfully'
            ]);
    }
}
