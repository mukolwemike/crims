<?php

namespace Tests\Feature\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\PortfolioRepaymentType;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DepositHoldingCalculatorTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @param array $data
     * @return DepositHolding
     */
    private function createInvestment(array $data = [])
    {
        return factory(DepositHolding::class)->create($data);
    }

    private function createWithdrawal(DepositHolding $investment, array $data = [])
    {
        $data['investment_id'] = $investment->id;

        return factory(PortfolioInvestmentRepayment::class)->create($data);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeposit()
    {
        $inv = $this->createInvestment();

        $at_start = $inv->calculate($inv->invested_date);

        $this->assertEquals($inv->amount, $at_start->principal());
        $this->assertEquals(0, $at_start->grossInterest());
    }

    private function createBond()
    {
        return $this->createInvestment([
            'amount' => 5000000.00,
            'bond_purchase_cost' => 5668038.12,
            'bond_clean_price' => 107.55,
            'bond_dirty_price' => 113.321,
            'bond_purchase_date' => '2019-07-09',
            'invested_date' => '2017-07-31',
            'maturity_date' => '2027-07-19',
            'interest_rate' => '11.5',
            'coupon_rate' => '11.5'
        ]);
    }

    public function testBond()
    {
        $inv = $this->createBond();

        $at_start = $inv->calculate($inv->bond_purchase_date);

        $this->assertEquals($inv->bond_purchase_cost, $at_start->principal());
        $this->assertEquals(0, $at_start->grossInterest());

        $at_invested = $inv->calculate($inv->invested_date);
        $this->assertEquals($inv->bond_purchase_cost, $at_invested->principal());
        $this->assertEquals(0, $at_invested->grossInterest());

        $at_maturity = $inv->calculate($inv->maturity_date);
        $tenor = $inv->maturity_date->diffInDays($inv->bond_purchase_date);

        $interest = 0.85 * $inv->bond_purchase_cost * $tenor * $inv->interest_rate /(100*365);
        $this->assertEquals($interest, $at_maturity->netInterest());
        $this->assertEquals($inv->bond_purchase_cost + $interest, $at_maturity->total());
    }

    public function testWithWithdrawals()
    {
        $inv = $this->createBond();

        $wdate = $inv->bond_purchase_date->copy()->addMonths(3);

        $with = $this->createWithdrawal($inv, [
            'amount' => 1000000,
            'date' => $wdate,
            'type_id' => PortfolioRepaymentType::where('slug', 'full_repayment')->first()->id
        ]);

        $inv = $inv->fresh();


        $tenor = 1 + $wdate->diffInDays($inv->bond_purchase_date);

        $interest_at_with = 0.85 * $inv->bond_purchase_cost * $tenor * $inv->interest_rate /(100*365);
        $total_at_with = $inv->bond_purchase_cost + $interest_at_with;
        $principal_after_with = $total_at_with - $with->amount;
        $total_after_with = $total_at_with - $with->amount;

        $at_with = $inv->calculate($wdate, true);
        $this->assertEquals($interest_at_with, $at_with->netInterestBeforeDeductions());
        $this->assertEquals($total_after_with, $at_with->total());
        $this->assertEquals($principal_after_with, $at_with->principal());

        $tenor_after_with = $inv->maturity_date->diffInDays($wdate) - 1;
        $interest_at_end = 0.85* $principal_after_with * $tenor_after_with * $inv->interest_rate / (100 * 365);
        $total_at_end = $principal_after_with + $interest_at_end;

        $at_maturity = $inv->calculate($inv->maturity_date, true);

        $this->assertEquals((int) $interest_at_end, (int)$at_maturity->netInterest());
        $this->assertEquals((int)$total_at_end, (int)$at_maturity->total());
    }
}
