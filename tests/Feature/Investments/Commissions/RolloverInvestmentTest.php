<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientInvestmentRolloverInstruction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class RolloverInvestmentTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    public function testSingleRolloverProcess()
    {
        $maturityDate = Carbon::now();

        $investedDate = $maturityDate->copy()->subDays(365);

        $investment = $this->getInvestment(
            $investedDate->toDateString(),
            $maturityDate->toDateString(),
            10000000
        );

        $instructionData = [
            'product_id' => $investment->product_id,
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'topup_amount' => 0,
            'amount_select' => 'principal_interest',
            'tenor' => 12,
            'amount' => $investment->repo->getTotalValueOfInvestmentAtDate($maturityDate),
            'agreed_rate' => 18,
            'automatic_rollover' => false
        ];

        $url = '/api/investments/roll-over-inv/' . $investment->id;

        $this->post($url, $instructionData)
            ->assertSuccessful();

        $this->assertDatabaseHas(
            (new ClientInvestmentInstruction())->getTable(),
            array_except($instructionData, ['product_id', 'client_id'])
        );

        $instruction = ClientInvestmentInstruction::where('investment_id', $investment->id)->latest()->first();

        $rolloverData = [
            'interest_rate' => 18,
            'invested_date' => $maturityDate->toDateString(),
            'maturity_date' => $maturityDate->copy()->addDays(365)->toDateString(),
            'description' => 'Testing',
            'commission_recepient' => $investment->commission->recipient_id,
            'commission_start_date' => null,
            'commission_rate' => $investment->commission->rate,
            'commission_rate_name' => 'NRFA - KES (1%)',
            'interest_payment_interval' => 3,
            'interest_payment_date' => 31,
            'interest_action_id' => 1,
            'interest_reinvest_tenor' => null
        ];

        $rolloverUrl = '/dashboard/investments/orders/rollover/' . $instruction->id;

        $this->post($rolloverUrl, $rolloverData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The investment has been saved successfully for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'rollover_investment',
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'rollover_investment')
            ->where('investment_id', $investment->id)->latest()->first();

        $this->approve($approval)
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id
        ]);

        $newInvestment = ClientInvestment::where('client_id', $investment->client_id)
            ->where('investment_type_id', 3)
            ->latest()->first();

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $newInvestment->commission->id
        ]);
    }

    public function testCombineRolloverProcess()
    {
        $maturityDate = Carbon::now();

        $investedDate = $maturityDate->copy()->subDays(365);

        $investment = $this->getInvestment(
            $investedDate->toDateString(),
            $maturityDate->toDateString(),
            10000000
        );

        $investment2 = $this->getInvestment(
            $investedDate->copy()->addDay()->toDateString(),
            $maturityDate->copy()->addDay()->toDateString(),
            5000000
        );

        $investment3 = $this->getInvestment(
            $investedDate->copy()->addDay()->toDateString(),
            $maturityDate->copy()->addDay()->toDateString(),
            1000000
        );

        $combineDate = Carbon::parse($investment->invested_date)->addDays(270);

        $instructionData = [
            'product_id' => $investment->product_id,
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'topup_amount' => 0,
            'amount_select' => 'principal_interest',
            'tenor' => 12,
            'amount' => $investment->repo->getTotalValueOfInvestmentAtDate($maturityDate),
            'agreed_rate' => 18,
            'combinedInvestment' => [
                [
                    'id' => $investment->id
                ],
                [
                    'id' => $investment2->id
                ],
                [
                    'id' => $investment3->id
                ]
            ],
            'automatic_rollover' => false,
            'combine_date' => $combineDate->toDateString()
        ];

        $url = '/api/investments/roll-over-inv/' . $investment->id;

        $this->post($url, $instructionData)
            ->assertSuccessful();

        $this->assertDatabaseHas(
            (new ClientInvestmentInstruction())->getTable(),
            array_except($instructionData, ['product_id', 'client_id', 'combinedInvestment', 'combine_date'])
        );

        $instruction = ClientInvestmentInstruction::where('investment_id', $investment->id)->latest()->first();

        $this->assertDatabaseHas(
            (new ClientInvestmentRolloverInstruction())->getTable(),
            [
                'instruction_id' => $instruction->id,
                'investment_id' => $investment->id
            ]
        );
        $this->assertDatabaseHas(
            (new ClientInvestmentRolloverInstruction())->getTable(),
            [
                'instruction_id' => $instruction->id,
                'investment_id' => $investment2->id
            ]
        );
        $this->assertDatabaseHas(
            (new ClientInvestmentRolloverInstruction())->getTable(),
            [
                'instruction_id' => $instruction->id,
                'investment_id' => $investment3->id
            ]
        );

        $rolloverData = [
            'interest_rate' => 18,
            'invested_date' => $maturityDate->toDateString(),
            'maturity_date' => $maturityDate->copy()->addDays(365)->toDateString(),
            'description' => 'Testing',
            'commission_recepient' => $investment->commission->recipient_id,
            'commission_start_date' => null,
            'commission_rate' => $investment->commission->rate,
            'commission_rate_name' => 'NRFA - KES (1%)',
            'interest_payment_interval' => 3,
            'interest_payment_date' => 31,
            'interest_action_id' => 1,
            'interest_reinvest_tenor' => null
        ];

        $rolloverUrl = '/dashboard/investments/orders/rollover/' . $instruction->id;

        $this->post($rolloverUrl, $rolloverData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The investment has been saved successfully for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'rollover_investment',
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'rollover_investment')
            ->where('investment_id', $investment->id)->latest()->first();

        $this->approve($approval)
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $combineDate
        ]);

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment2->commission->id,
            'date' => $combineDate
        ]);

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment3->commission->id,
            'date' => $combineDate
        ]);

        $newInvestment = ClientInvestment::where('client_id', $investment->client_id)
            ->where('investment_type_id', 3)
            ->latest()->first();

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $newInvestment->commission->id
        ]);
    }
}
