<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Investments;

use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionClawback;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class WithdrawalClawbackTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    public function testWithdrawalProcess()
    {
        $maturityDate = Carbon::now();

        $investedDate = $maturityDate->copy()->subDays(365);

        $investment = $this->getInvestment(
            $investedDate->toDateString(),
            $maturityDate->toDateString(),
            10000000
        );

        $instructionData = [
            'withdraw_type' => 'withdrawal',
            'withdrawal_stage' => 'mature',
            'amount_select' => 'principal_interest',
            'reason' => 'Testing',
            'investment_id' => $investment->id
        ];

        $url = '/api/investments/withdraw/' . $investment->id;

        $this->post($url, $instructionData)
            ->assertSuccessful();

        $this->assertDatabaseHas((new ClientInvestmentInstruction())->getTable(), $instructionData);

        $instruction = ClientInvestmentInstruction::where('investment_id', $investment->id)->latest()->first();

        $withdrawalData = [
            'callback' => 'on',
            'deduct_penalty' => '1',
            'description' => 'Testing',
            'narration' => 'Testing'
        ];

        $withDrawUrl = '/dashboard/investments/orders/withdraw/' . $instruction->id;

        $this->post($withDrawUrl, $withdrawalData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Withdrawal has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'withdrawal',
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'withdrawal')
            ->where('investment_id', $investment->id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id
        ]);
    }

    private function setupWithdrawalApproval($instruction)
    {
        $investment = $instruction->investment;

        $approvalData = [
            'callback' => 'on',
            'deduct_penalty' => 1,
            'description' => 'Testing',
            'narration' => 'Testing'
        ];

        $approval = $this->getClientTransactionApproval([
            'payload' => $approvalData,
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'sent_by' => Auth::id(),
            'transaction_type' => 'withdrawal'
        ]);

        $instruction->update([
            'approval_id' => $approval->id
        ]);

        return $approval;
    }

    public function testNoClawbackOnMatureWithdrawal()
    {
        $maturityDate = Carbon::now();

        $investedDate = $maturityDate->copy()->subDays(365);

        $investment = $this->getInvestment(
            $investedDate->toDateString(),
            $maturityDate->toDateString(),
            10000000
        );

        $instruction = $this->getClientInvestmentInstruction([
            'investment_id' => $investment->id,
            'type_id' => 2,
            'amount_select' => 'amount',
            'amount' => 1000000,
            'withdrawal_date' => Carbon::parse($investment->maturity_date)->toDateString(),
            'withdrawal_stage' => 'mature',
            'due_date' => $investment->maturity_date,
            'withdraw_type' => 'withdrawal'
        ]);

        $approval = $this->setupWithdrawalApproval($instruction);

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $instruction->withdrawal_date,
            'type' => 'withdraw'
        ]);
    }

    public function testCanClawbackOnPrematureWithdrawal()
    {
        $maturityDate = Carbon::now();

        $investedDate = $maturityDate->copy()->subDays(365);

        $investment = $this->getInvestment(
            $investedDate->toDateString(),
            $maturityDate->toDateString(),
            10000000
        );

        $withdrawalDate = $investedDate->copy()->addDays(180);

        $instruction = $this->getClientInvestmentInstruction([
            'investment_id' => $investment->id,
            'type_id' => 2,
            'amount_select' => 'amount',
            'amount' => 1000000,
            'withdrawal_date' => $withdrawalDate,
            'withdrawal_stage' => 'premature',
            'due_date' => $withdrawalDate,
            'withdraw_type' => 'withdrawal'
        ]);

        $approval = $this->setupWithdrawalApproval($instruction);

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $instruction->withdrawal_date,
            'type' => 'withdraw'
        ]);
    }

    public function testNoClawbackOnInterestWithdrawal()
    {
        $maturityDate = Carbon::now();

        $investedDate = $maturityDate->copy()->subDays(365);

        $investment = $this->getInvestment(
            $investedDate->toDateString(),
            $maturityDate->toDateString(),
            10000000
        );

        $withdrawalDate = $investedDate->copy()->addDays(180);

        $instruction = $this->getClientInvestmentInstruction([
            'investment_id' => $investment->id,
            'type_id' => 2,
            'amount_select' => 'interest',
            'amount' => null,
            'withdrawal_date' => $withdrawalDate,
            'withdrawal_stage' => 'premature',
            'due_date' => $withdrawalDate,
            'withdraw_type' => 'withdrawal'
        ]);

        $approval = $this->setupWithdrawalApproval($instruction);

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $instruction->withdrawal_date,
            'type' => 'withdraw'
        ]);
    }

    public function testNoClawbackOnPrematureZeroCommission()
    {
        $maturityDate = Carbon::now();

        $investedDate = $maturityDate->copy()->subDays(365);

        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => $investedDate->toDateString(),
            'maturity_date' => $maturityDate->toDateString(),
            'amount' => 10000000
        ], [
            'rate' => 0
        ]);

        $withdrawalDate = $investedDate->copy()->addDays(180);

        $instruction = $this->getClientInvestmentInstruction([
            'investment_id' => $investment->id,
            'type_id' => 2,
            'amount_select' => 'interest',
            'amount' => null,
            'withdrawal_date' => $withdrawalDate,
            'withdrawal_stage' => 'premature',
            'due_date' => $withdrawalDate,
            'withdraw_type' => 'withdrawal'
        ]);

        $approval = $this->setupWithdrawalApproval($instruction);

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $instruction->withdrawal_date,
            'type' => 'withdraw'
        ]);
    }
}
