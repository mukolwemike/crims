<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Investments\Commissions;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class ReinvestedInterestInvestmentTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    public function testInterestReinvestmentProcess()
    {
        $client = $this->getCustomClient([
            'combine_interest_reinvestment' => 1,
            'combine_interest_reinvestment_tenor' => 12
        ]);

        $investedDate = Carbon::now()->startOfMonth()->subMonths(2);

        $maturityDate = $investedDate->copy()->addDays(365);

        $investment = $this->getInvestmentAndCommissions(
            [
                'invested_date' => $investedDate,
                'maturity_date' => $maturityDate,
                'client_id' => $client->id,
                'amount' => 12000000,
                'interest_action_id' => 2,
                'interest_payment_interval' => 1,
                'interest_payment_date' => 31
            ]
        );

        $this->getInvestmentInterestSchedules($investment);

        $firstSchedule = $investment->interestSchedules()->orderBy('date')->first();

        $firstReinvestedInvestment = $this->setupInterestBulkApprovalReinvestedInvestment($firstSchedule);

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $firstSchedule->date,
            'type' => 'withdraw'
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $firstReinvestedInvestment->commission->id,
        ]);

        $secondSchedule = $investment->interestSchedules()->where('date', '>', $firstSchedule->date)
            ->orderBy('date')->first();

        $secondReinvestedInvestment = $this->setupInterestBulkApprovalReinvestedInvestment($secondSchedule);

        $this->assertDatabaseHas((new ClientInvestment())->getTable(), [
            'id' => $firstReinvestedInvestment->id,
            'withdrawn' => 1,
            'rolled' => 1,
            'withdrawal_date' => $secondSchedule->date
        ]);

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $firstReinvestedInvestment->commission->id,
            'date' => $secondSchedule->date,
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $secondReinvestedInvestment->commission->id,
        ]);

        $thirdSchedule = $investment->interestSchedules()->where('date', '>', $secondSchedule->date)
            ->orderBy('date')->first();

        $thirdReinvestedInvestment = $this->setupInterestBulkApprovalReinvestedInvestment($thirdSchedule);

        $this->assertDatabaseHas((new ClientInvestment())->getTable(), [
            'id' => $secondReinvestedInvestment->id,
            'withdrawn' => 1,
            'rolled' => 1,
            'withdrawal_date' => $thirdSchedule->date
        ]);

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $secondReinvestedInvestment->commission->id,
            'date' => $thirdSchedule->date,
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $thirdReinvestedInvestment->commission->id,
        ]);
    }

    private function setupInterestBulkApprovalReinvestedInvestment($schedule)
    {
        $interestData = [
            'date' => $schedule->date
        ];

        $url = '/dashboard/investments/interest';

        $this->post($url, $interestData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The interest payments have been sent for bulk approval');

        $approvalData = [
            'interest_payment_schedules' => [$schedule->id],
            'removed' => [],
            'date' => $interestData['date'],
        ];

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'interest_payment_bulk_approval',
            'client_id' => null,
            'payload' => serialize($approvalData),
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'interest_payment_bulk_approval')
            ->where('payload', serialize($approvalData))
            ->where('sent_by', Auth::id())->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("Interest payments have been queued for payment/reinvestment.");

        $reinvestedInvestedDate = Carbon::parse($schedule->date)->addDay();

        $this->assertDatabaseHas((new ClientInvestment())->getTable(), [
            'client_id' => $schedule->investment->client_id,
            'investment_type_id' => 5,
            'invested_date' => $reinvestedInvestedDate
        ]);

        $reinvestedInterest = ClientInvestment::where('client_id', $schedule->investment->client_id)
            ->where('investment_type_id', 5)
            ->where('invested_date', $reinvestedInvestedDate)
            ->first();

        return $reinvestedInterest;
    }
}
