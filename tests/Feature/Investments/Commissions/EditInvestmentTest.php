<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Investments\Commissions;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Crypt;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class EditInvestmentTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    public function testEditCommissionRecipient()
    {
        $investment = $this->getInvestment();

        $oldSchedules = $investment->commission->schedules;

        $newRecipient = $this->getCommissionRecipient();

        $editUrl = '/dashboard/investments/edit';

        $editData = [
            'investment' => Crypt::encrypt($investment->id),
            'commission_recepient' => $newRecipient->id,
            'commission_start_date' => null,
            'commission_rate' => 1,
            'commission_rate_name' => 'RFA - KES (0.5%)'
        ];

        $this->post($editUrl, $editData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSee('Edited investment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'edit_investment',
            'client_id' => $investment->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = (new ClientTransactionApproval)->where('transaction_type', 'edit_investment')
            ->where('client_id', $investment->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The Investment has been changed');

        $investment = $investment->fresh();

        $this->assertEquals($newRecipient->id, $investment->commission->recipient_id);

        $oldSchedules->each(function ($schedule) {
            $this->assertNotNull($schedule->fresh()->deleted_at);
        });

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'deleted_at' => null
        ]);
    }

    public function testEditCommissionStartDate()
    {
        $investment = $this->getInvestment();

        $oldSchedules = $investment->commission->schedules;

        $editUrl = '/dashboard/investments/edit';

        $commStartDate = Carbon::parse($investment->invested_date)->addMonth();

        $editData = [
            'investment' => Crypt::encrypt($investment->id),
            'commission_recepient' => $investment->commission->recipient_id,
            'commission_start_date' => $commStartDate,
            'commission_rate' => 1,
            'commission_rate_name' => 'RFA - KES (0.5%)'
        ];

        $this->post($editUrl, $editData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSee('Edited investment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'edit_investment',
            'client_id' => $investment->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = (new ClientTransactionApproval)->where('transaction_type', 'edit_investment')
            ->where('client_id', $investment->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The Investment has been changed');

        $investment = $investment->fresh();

        $this->assertEquals($commStartDate, $investment->commission->start_date);

        $oldSchedules->each(function ($schedule) {
            $this->assertNotNull($schedule->fresh()->deleted_at);
        });

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $commStartDate,
            'deleted_at' => null
        ]);

        $investment->commission->schedules->each(function ($schedule) use ($commStartDate) {
            $this->assertGreaterThanOrEqual($commStartDate, $schedule->date);
        });
    }

    public function testEditCommissionRate()
    {
        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => '2018-01-01',
            'maturity_date' => '2019-01-01',
            'amount' => 10000000
        ], [
            'rate' => 1
        ]);

        $oldSchedules = $investment->commission->schedules;

        $editUrl = '/dashboard/investments/edit';

        $newRate = 2;

        $editData = [
            'investment' => Crypt::encrypt($investment->id),
            'commission_recepient' => $investment->commission->recipient_id,
            'commission_start_date' => null,
            'commission_rate' => $newRate,
            'commission_rate_name' => 'RFA - KES (2.0%)'
        ];

        $this->post($editUrl, $editData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSee('Edited investment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'edit_investment',
            'client_id' => $investment->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = (new ClientTransactionApproval)->where('transaction_type', 'edit_investment')
            ->where('client_id', $investment->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The Investment has been changed');

        $investment = $investment->fresh();

        $this->assertEquals($newRate, $investment->commission->rate);

        $oldSchedules->each(function ($schedule) {
            $this->assertNotNull($schedule->fresh()->deleted_at);
        });

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'deleted_at' => null
        ]);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $newRate, $days);

        $this->assertEquals($commissionAmount, $investment->commission->schedules()->sum('amount'));
    }

    public function testEditExtendInvestmentMaturityDate()
    {
        $investment = $this->getInvestment();

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->startOfMonth()->subDays(11)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->startOfMonth()->addDays(19)->toDateString(),
        ]);

        $editUrl = '/dashboard/investments/edit';

        $oldSchedules = $investment->commission->schedules;

        $newMaturity = Carbon::parse($investment->maturity_date)->addDays(90);

        $editData = [
            'investment' => Crypt::encrypt($investment->id),
            'maturity_date' => $newMaturity,
            'on_call' => 0
        ];

        $this->post($editUrl, $editData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSee('Edited investment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'edit_investment',
            'client_id' => $investment->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = (new ClientTransactionApproval)->where('transaction_type', 'edit_investment')
            ->where('client_id', $investment->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The Investment has been changed');

        $investment = $investment->fresh();

        $this->assertEquals($newMaturity, $investment->maturity_date);

        $oldSchedules->each(function ($schedule) {
            $this->assertNull($schedule->fresh()->deleted_at);
        });

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, 90);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'description' => 'Bonus for investment extension',
            'amount' => $commissionAmount
        ]);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertEquals(round($commissionAmount, 2), round($investment->commission->schedules()->sum('amount'), 2));
    }

    public function testEditReduceInvestmentMaturityDate()
    {
        $investedDate = Carbon::now()->subMonths(3);

        $maturityDate = $investedDate->copy()->addDays(270);

        $investment = $this->getInvestment($investedDate->toDateString(), $maturityDate->toDateString());

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->startOfMonth()->subDays(11)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->startOfMonth()->addDays(19)->toDateString(),
        ]);

        $editUrl = '/dashboard/investments/edit';

        $oldSchedules = $investment->commission->schedules;

        $newMaturity = Carbon::parse($investment->maturity_date)->subDays(90);

        $editData = [
            'investment' => Crypt::encrypt($investment->id),
            'maturity_date' => $newMaturity,
            'on_call' => 0
        ];

        $this->post($editUrl, $editData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSee('Edited investment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'edit_investment',
            'client_id' => $investment->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = (new ClientTransactionApproval)->where('transaction_type', 'edit_investment')
            ->where('client_id', $investment->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The Investment has been changed');

        $investment = $investment->fresh();

        $this->assertEquals($newMaturity, $investment->maturity_date);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, 90);

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount,
            'type' => 'edit'
        ]);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertEquals(
            round($commissionAmount, 2),
            round($investment->commission->schedules()->sum('amount') - $investment->commission->clawBacks()->sum('amount'), 2)
        );

        $oldSchedules->each(function ($schedule) use ($investment) {
            if (Carbon::parse($schedule->date) > Carbon::parse($investment->maturity_date)) {
                $this->assertEquals($investment->maturity_date, $schedule->fresh()->date);
            } else {
                $this->assertEquals($schedule->date, $schedule->fresh()->date);
            }
        });
    }
}
