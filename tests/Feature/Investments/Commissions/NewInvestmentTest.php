<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\Investments\Commissions;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPaymentType;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Cytonn\Core\DataStructures\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class NewInvestmentTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    public function testNewInvestmentProcess()
    {
        $client = $this->getClient();

        $payment = $this->getClientPaymentAndTransaction(
            [
                'client_id' => $client->id,
                'amount' => 100000000,
                'date' => Carbon::now()->subDay(),
                'product_id' => 1,
                'type_id' => 1
            ],
            [
                'type' => 1,
                'custodial_account_id' => 1,
                'client_id' => 1,
                'amount' => 100000000,
                'date' => Carbon::now()
            ]
        );

        $instructionData = [
            'client_id' => $client->id,
            'amount' => 10000000,
            'tenor' => 12,
            'product_id' => 1,
            'agreed_rate' => 18
        ];

        $url = 'api/instruction/top-up-investment';

        $this->post($url, $instructionData)
            ->assertSuccessful();

        $this->assertDatabaseHas(
            (new ClientTopupForm())->getTable(),
            $instructionData
        );

        $topupForm = ClientTopupForm::where('client_id', $client->id)->latest()->first();

        $investedDate = Carbon::now();

        $maturityDate = $investedDate->copy()->addDays(365);

        $recipient = $this->getCommissionRecipient();

        $newInvestmentData = [
            'interest_rate' => 18,
            'invested_date' => $investedDate->toDateString(),
            'maturity_date' => $maturityDate->toDateString(),
            'description' => 'Testing',
            'commission_recepient' => $recipient->id,
            'commission_start_date' => null,
            'commission_rate' => 1,
            'commission_rate_name' => 'NRFA - KES (1%)',
            'interest_payment_interval' => 3,
            'interest_payment_date' => 31,
            'interest_action_id' => 1,
            'interest_reinvest_tenor' => null
        ];

        $topupUrl = '/dashboard/investments/orders/topup/' . $topupForm->id;

        $this->post($topupUrl, $newInvestmentData)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The investment has been saved successfully');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'new_investment',
            'client_id' => $client->id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'new_investment')
            ->where('client_id', $client->id)->latest()->first();

        $this->approve($approval)
            ->assertStatus(200);

        $newInvestment = ClientInvestment::where('client_id', $client->id)->latest()->first();

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $newInvestment->commission->id,
        ]);
    }
}
