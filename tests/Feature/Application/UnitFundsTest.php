<?php

namespace Tests\Feature\Application;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Exceptions\Handler;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\InterestPayments\ScheduleGenerator;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

class UnitFundsTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
    /*
     * Get the required data
     */
    protected $user;

    protected $faker;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();
    }

    /** @test */
    public function unit_fund_test_purchase_units_using_a_repository()
    {
//        // create client->id
//        $client_id = factory('App\Cytonn\Models\Client')->create()->id;
//
//        // create fund->id
//        $fund_id = $client = factory('App\Cytonn\Models\Unitization\UnitFund')->create()->id;
//
//        dd($fund_id);
//
//        $instruction_id = $this->faker->numberBetween(1, 100);
//
//        $payload = ' {"date":"2018-08-01","amount":"9000000","client_id":380,"unit_fund_id":3, "instruction_id":12}';
//
//        // call to the controller@store->repository->purchase i.e. accessible and save data
//        $response = $this->post('/api/unitization/application-investment', [
//            'instruction_id' => 12,
//            'client_id' =>$client_id,
//            'transaction_type' => 'unit_fund_investment',
//            'payload' => $payload,
//        ]);
//
//        dd($response);
//
////        ->followRedirects()
////        ->assertSuccessful(202);
//
//        // assert db values true
    }
}
