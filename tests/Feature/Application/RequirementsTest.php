<?php

namespace Tests\Feature\Application;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\CompanyNatures;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Title;
use Tests\Helpers\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

class RequirementsTest extends TestCase
{
    /*
     * Get the database transactions trait
     */
    use DatabaseTransactions;

    /*
     * Get the base class;
     */
    use Base;

    /*
     * Get the required data
     */
    protected $user;
    protected $faker;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    /**
     * A test for the contact titles api
     *
     * @return void
     */
    public function testTitles()
    {
        $title = factory(Title::class)->create();

        $this->get('/api/contact/titles')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $title->name,
                'id' => $title->id
            ]);
    }

    /**
     * A test for the contact countries api
     *
     * @return void
     */
    public function testCountries()
    {
        $country = factory(Country::class)->create();

        $this->get('/api/contact/countries')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $country->name,
                'id' => $country->id
            ]);
    }

    /**
     * A test for the contact employment types api
     *
     * @return void
     */
    public function testEmploymentTypes()
    {
        $employment = factory(Employment::class)->create();

        $this->get('/api/contact/employment_types')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $employment->name,
                'id' => $employment->id
            ]);
    }

    /**
     * A test for the contact contact methods api
     *
     * @return void
     */
    public function testContactMethods()
    {
        $contactMethod = factory(ContactMethod::class)->create();

        $this->get('/api/contact/contact_methods')
            ->assertStatus(200)
            ->assertJsonFragment([
                'description' => $contactMethod->description,
                'id' => $contactMethod->id
            ]);
    }

    /**
     * A test for the contact source of funds api
     *
     * @return void
     */
    public function testSourceOfFunds()
    {
        $fundSource = factory(FundSource::class)->create();

        $this->get('/api/contact/source_of_funds')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $fundSource->name,
                'id' => $fundSource->id
            ]);
    }

    /**
     * A test for the contact business natures api
     *
     * @return void
     */
    public function testBusinessNature()
    {
        $companyNature = factory(CompanyNatures::class)->create();

        $this->get('/api/contact/business_natures')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $companyNature->name,
                'id' => $companyNature->id
            ]);
    }

    /**
     * A test for the contact gender api
     *
     * @return void
     */
    public function testGender()
    {
        $gender = factory(Gender::class)->create();

        $this->get('/api/gender/list')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $gender->name,
                'abbr' => $gender->abbr,
                'id' => $gender->id
            ]);
    }

    /**
     * A test for the contact client bank api
     *
     * @return void
     */
    public function testClientBank()
    {
        $bank = factory(ClientBank::class)->create();

        $this->get('/api/all/banks')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $bank->name,
                'swiftCode' => $bank->swift_code,
                'clearingCode' => $bank->clearing_code,
                'id' => $bank->id
            ]);
    }

    /**
     * A test for the contact products api
     *
     * @return void
     */
    public function testProducts()
    {
        $product = factory(Product::class)->create();

        $this->get('/api/investments/products')
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => $product->name,
                'description' => $product->description,
                'id' => $product->id
            ]);
    }
}
