<?php

namespace Tests\Feature\Application;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Title;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\Helpers\ClientPayments;
use Tests\TestCase;

class ApplyTest extends TestCase
{
    /*
     * Get the database transactions trait
     */
    use DatabaseTransactions, Base, ClientPayments;

    /*
     * Get the required data
     */
    protected $user;
    /**
     * @var Faker
     */
    protected $faker;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        factory(\App\Cytonn\Models\ClientInvestmentType::class)->create(['name' => 'investment']);
        factory(\App\Cytonn\Models\ClientInvestmentType::class)->create(['name' => 'topup']);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFillClientInvestmentApplication()
    {
        $product = $this->getProduct();

        $config = [
            'client_type' => "individual",
        ];

        $investment = [
            'amount' => 20000,
            'product_id' => $product->id,
            'tenor' => 12,
            'agreed_rate' => 10
        ];

        $subscriber = [
            'business_sector' => "mnnmmnn",
            'country_id' => Country::first()->id,
            'dob' => $this->faker->date,
            'email' => $this->faker->email,
            'employer_address' => "nmnm",
            'employer_name' => "nnnmnm",
            'employment_id' => 1,
            'firstname' => $this->faker->firstname,
            'funds_source_id' => 1,
            'funds_source_other' => "",
            'gender_id' => 1,
            'id_or_passport' => "nnmnmn",
            'investor_account_name' => "nmnmnm",
            'investor_account_number' => "nmnmnm",
            'investor_bank' => 2,
            'investor_bank_branch' => 1091,
            'jointHolders' => [],
            'kin_email' => "nmnmnm@mmso.cll",
            'kin_name' => "nmnn",
            'kin_phone' => "nmnnm",
            'kin_postal' => "mnmnnm",
            'lastname' => "bnbnbn",
            'method_of_contact_id' => 1,
            'middlename' => "bnbnbn",
            'pin_no' => "nmnmnm",
            'postal_address' => "mnmnnm",
            'postal_code' => "nmnmnm",
            'present_occupation' => "nmnnm",
            'street' => "nmnmnmnm",
            'telephone_home' => "nmnmnm",
            'telephone_office' => "nnnm",
            'title' => Title::first()->id,
            'town' => "nmmnnmnm"
        ];

        $data = $config + $investment + $subscriber;

        $this->post('api/applications/validate/subscriber', $data)
            ->isSuccessful();

        $table = (new ClientFilledInvestmentApplication)->getTable();

        $this->assertDatabaseHas($table, ['firstname' => $data['firstname'], 'email' => $data['email']]);
    }

    public function testProcessApplication()
    {
        $filled = $this->getClientFilledInvestmentApplication([]);

        //process
        $this->get("dashboard/investments/client-instructions/application/{$filled->id}/process")
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("The application has been added successfully and investment created for processing");

        //approve
        $appl = $filled->application;

        $approval = $appl->approval;

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertStatus(200)
            ->assertSeeText("approved");

        $app = ClientInvestmentApplication::find($appl->id)->first();

        $client = $app->client;

        //Invest
        $topupForm = ClientTopupForm::where('client_id', $client->id)->latest()->first();

        $this->get("/dashboard/investments/client-instructions/topup/{$topupForm->id}")
            ->isSuccessful();

        $this->get("http://crims.local/dashboard/investments/client-instructions/topup/{$topupForm->id}/process")
            ->isSuccessful();

        $this->post("/dashboard/investments/orders/topup/{$topupForm->id}", [
            'interest_rate' => 15,
            'invested_date' => '2018-05-04',
            'maturity_date' => '2018-08-20',
            'description' => 'Some long stories',
            'commission_recepient' => 223,
            'commission_rate' => 0.5,
            'commission_start_date' => null,
            'interest_payment_interval' => null,
            'interest_payment_date' => null,
            'interest_action_id' => null,
            'interest_reinvest_tenor' => null,
        ])
            ->followRedirects()
            ->assertSuccessful()
            ->assertSeeText("saved successfully");

        $approval = $topupForm->fresh()->approval;

        //ensure client has sufficient cash balance
        $this->addPayment(
            $approval->client,
            $filled->amount,
            Carbon::parse('2018-05-04'),
            $topupForm->product
        );

        $this->approve($approval)->followRedirects()
            ->assertSuccessful()
            ->assertSeeText("Investment Topup Successful");

        $this->assertDatabaseHas((new ClientInvestment())->getTable(), [
            'interest_rate' => 15,
            'invested_date' => Carbon::parse('2018-05-04')->toDateTimeString(),
            'maturity_date' => Carbon::parse('2018-08-20')->toDateTimeString(),
            'client_id' => $client->id,
            'amount' => $filled->amount,
            'product_id' => $topupForm->product->id
        ]);

        $balance = $this->getBalance($client, Carbon::parse('2018-05-04'), $topupForm->product);

        $this->assertEquals(0, $balance);
    }
}
