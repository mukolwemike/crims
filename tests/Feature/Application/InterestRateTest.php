<?php

namespace Tests\Feature\Application;

use App\Cytonn\Models\Rate;
use Tests\Helpers\Base;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

class InterestRateTest extends TestCase
{
    /*
     * Get the database transactions trait
     */
    use DatabaseTransactions, Base;

    /*
     * Get the required data
     */
    protected $user;
    protected $faker;

    /*
     * Setup the test class
     */
    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    /**
     * Test for rate for a client above 500k
     *
     * @return void
     */
    public function test_high_investment_client_type()
    {
        $this->clearRates();

        $product = $this->getProduct();

        $rate = $this->setRate([
            'product_id' => $product->id,
            'tenor' => 12,
            'tenor_type' => 1,
            'rate' => 22,
            'interest_rate_update_id' => 1,
            'client_type' => 1,
            'min_amount' => 500000
        ]);

        $data = [
            'amount' => 1000000,
            'product_id' => $product->id,
            'tenor' => $rate->tenor
        ];

        $this->post('api/product/interest-rates', $data)
            ->assertStatus(200)
            ->assertSee("$rate->rate");
    }

    /**
     * Test for rate for a client below 500k
     *
     * @return void
     */
    public function test_low_investment_client_type()
    {
        $this->clearRates();

        $product = $this->getProduct();

        $rate = $this->setRate([
            'product_id' => $product->id,
            'tenor' => 1,
            'tenor_type' => 1,
            'rate' => 13,
            'interest_rate_update_id' => 1,
            'min_amount' => 0
        ]);

        $data = [
            'amount' => 11,
            'product_id' => $product->id,
            'tenor' => $rate->tenor
        ];

        $this->post('api/product/interest-rates', $data)
            ->assertStatus(200)
            ->assertSee("$rate->rate");
    }

    private function clearRates()
    {
        Rate::all()->map->delete();
    }
}
