<?php

/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 25/04/2019
 * Time: 09:06
 */

namespace Tests\Feature\USSD;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;

class UssdTest extends TestCase
{
    use DatabaseTransactions, Base;

    protected $appType = 'clients';

    protected $faker;

    protected $user;

    protected $key;

    protected $phone;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->key = env('USSD_KEY');

        $this->phone = '+254700123321';

        $this->setupUSSDRequirements();
    }

    /** @test */
    public function ussdApiDialUp()
    {
        $response = $this->dialUp();

        $response->assertSuccessful()
            ->assertStatus(200);
    }

    /** @test */
    public function newUserDialUp()
    {
        $response = $this->dialUp();

        $this->assertRegExp('/Welcome to Cytonn Money Market Fund/', $response->getContent());
    }

    /** @test */
    public function userRequestCall()
    {
        $response = $this->dialUp();

        $this->assertRegExp('/Request Call/', $response->getContent());

        $response = $this->makePostRequest('2');
        $this->assertStringStartsWith('END', $response->getContent());
        $this->assertRegExp('/call/', $response->getContent());
    }

    /** @test */
    public function userRequestMMF()
    {
        $response = $this->dialUp();

        $this->assertRegExp('/More info on MMF/', $response->getContent());

        $response = $this->makePostRequest('3');
        $this->assertStringStartsWith('END', $response->getContent());
        $this->assertRegExp('/more information on Cytonn MMF/', $response->getContent());
    }

    /** @test */
    public function createNewAccount()
    {
        // test new user create account
        $response = $this->dialUp();
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Create Account/', $response->getContent());

        $response = $this->makePostRequest('1');
        $this->assertStringStartsWith('CON', $response->getContent());

        $fullname = $this->faker->firstname . ' ' . $this->faker->lastname . ' ' . $this->faker->lastname;
        $response = $this->makePostRequest($fullname);
        $this->assertStringStartsWith('CON', $response->getContent());

        $id_passport = $this->faker->numberBetween(1000000, 100000000);
        $response = $this->makePostRequest($id_passport);
        $this->assertStringStartsWith('CON', $response->getContent());

        $email = $this->faker->email;
        $response = $this->makePostRequest($email);
        $this->assertStringStartsWith('CON', $response->getContent());

        $response = $this->makePostRequest('1');
        $this->assertStringStartsWith('CON', $response->getContent());

        $response = $this->makePostRequest('00');
        $this->assertStringStartsWith('CON', $response->getContent());

        $this->assertRegExp('/confirm your details/', $response->getContent());
        $response = $this->makePostRequest('1');

        $this->assertStringStartsWith('END', $response->getContent());

        $this->assertDatabaseHas('client_users', [
            'phone' => $this->cleanPhone($this->phone),
        ]);
    }

    /** @test */
    public function loginAction()
    {
        $this->setupClientUser();
        $this->linkUserClient();

        $response = $this->dialUp();

        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Please Enter your PIN/', $response->getContent());

        $response = $this->makePostRequest('1111');
        $this->assertRegExp('/Incorrect Pin/', $response->getContent());

        $response = $this->makePostRequest('1234');
        $this->assertRegExp('/Select Option/', $response->getContent());
    }

    /** @test */
    function resetPinWrongId()
    {
        $this->setupClientUser();
        $this->linkUserClient();

        $response = $this->dialUp();

        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Please Enter your PIN/', $response->getContent());

        $response = $this->makePostRequest('00');
        $this->assertRegExp('/Please enter your ID No./', $response->getContent());

        $response = $this->makePostRequest('12343233');
        $this->assertStringStartsWith('END', $response->getContent());
        $this->assertRegExp('/No account available with given ID or Passport/', $response->getContent());
    }

    /** @test */
    function resetPinCorrectId()
    {
        $this->setupClientUser();
        $this->linkUserClient();

        $response = $this->dialUp();

        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Please Enter your PIN/', $response->getContent());

        $response = $this->makePostRequest('00');
        $this->assertRegExp('/Please enter your ID No./', $response->getContent());

        $response = $this->makePostRequest(Client::first()->id_or_passport);
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Enter new PIN/', $response->getContent());

        $newPin = '4321';

        $response = $this->makePostRequest($newPin);
        $this->assertStringStartsWith('END', $response->getContent());

        $text = '/New pin is sent by SMS/';
        $this->assertRegExp($text, $response->getContent());
        $this->assertTrue(\Hash::check($newPin, ClientUser::first()->pin));
    }

    /** @test */
    public function investAction()
    {
        $this->setupClientUser();
        $this->linkUserClient();

        $this->dialUp();
        $this->makePostRequest('1234');

        $response = $this->makePostRequest('1');
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Select Transaction Option/', $response->getContent());

        $response = $this->makePostRequest('1');
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/invest/', $response->getContent());

        $response = $this->makePostRequest('2000');
        $this->assertRegExp('/Invalid amount./', $response->getContent());

        $response = $this->makePostRequest('6000');
        $this->assertRegExp('/method of payment/', $response->getContent());
        $this->assertRegExp('/M-PESA/', $response->getContent());
        $this->assertRegExp('/BANK Transfer/', $response->getContent());

        $this->setupCustodialAccount();

        $response = $this->makePostRequest('2');

        // setup operating account
        dd($response->getContent());
        $this->assertStringStartsWith('END', $response->getContent());
    }

    /** @test */
    public function withdrawAction()
    {
        $this->setupClientUser();
        $this->linkUserClient();

        $bank = $this->setupClientBank();
        $branch = $this->setupClientBankBranch($bank);
        $this->setupClientBankAccount($this->setupUssdClient(), $branch, $bank);

        $this->dialUp();
        $this->makePostRequest('1234');

        $response = $this->makePostRequest('1');
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Select Transaction Option/', $response->getContent());

        $response = $this->makePostRequest('2');
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/withdraw/', $response->getContent());

        $response = $this->makePostRequest('500');
        $this->assertRegExp('/send the money/', $response->getContent());
        $this->assertRegExp('/M-PESA/', $response->getContent());
        $this->assertRegExp('/BANK Transfer/', $response->getContent());

        $response = $this->makePostRequest('1');

        $this->assertRegExp('/Select the account/', $response->getContent());

        $response = $this->makePostRequest('1');
        dd('here', $response->getContent());


        $this->assertStringStartsWith('END', $response->getContent());
    }

    /** @test */
    public function checkBalance()
    {
        $this->setupClientUser();
        $this->linkUserClient();

        $this->dialUp();
        $this->makePostRequest('1234');

        $response = $this->makePostRequest('3');
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Check Balance/', $response->getContent());

        $response = $this->makePostRequest('1');

        // new user i.e. no CMMF Investment
        $this->assertRegExp('/you have no Account with CMMF investment/', $response->getContent());
        $this->assertStringStartsWith('CON', $response->getContent());
    }

    /** @test */
    function referClient()
    {
        $this->setupClientUser();
        $this->linkUserClient();

        $this->dialUp();
        $this->makePostRequest('1234');

        $response = $this->makePostRequest('2');
        $this->assertStringStartsWith('CON', $response->getContent());
        $this->assertRegExp('/Enter the Referred Client\'s Full Name/', $response->getContent());

        $response = $this->makePostRequest('Judas Iscariot');
        $this->assertRegExp('/Please Enter the Referred Client\'s Phone Number/', $response->getContent());

        $response = $this->makePostRequest('0722123987');
        $this->assertRegExp('/Confirm/', $response->getContent());

        $response = $this->makePostRequest('1');
        $this->assertStringStartsWith('END', $response->getContent());
    }

    private function makePostRequest($text)
    {
        return $this->call('POST', '/ussd/' . $this->key, $this->getSessionBodyData('', $text));
    }

    protected function linkUserClient()
    {
        $client = $this->setupUssdClient();
        $user = ClientUser::first();

        $user->repo->provisionUser();
        $user->repo->assignAccess($client);

        $this->setupClientInvestmentApplication();
    }
}
