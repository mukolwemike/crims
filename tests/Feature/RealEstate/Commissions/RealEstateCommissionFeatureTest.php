<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\Feature\RealEstate;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealestateCommission;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Realestate\Commissions\Calculator\Award;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class RealEstateCommissionFeatureTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        $this->setupForRealEstateTests();
    }

    private function getAwardClass(UnitHolding $holding)
    {
        return new Award($holding);
    }

    /*
     * Test Award of Commissions
     */
    public function testAwardOfUnitHoldingCommission()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidPercentage = (int) rand($constants->minimum_payment_percentage, 100);

        $paidAmount = $paidPercentage  * $holding->price() / 100;

        $payment = $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $looDate = Carbon::latest(Carbon::parse($payment->date), $holding->loo->date_received);

        $saDate = Carbon::latest($looDate, $holding->salesAgreement->date_received);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $saDate);

        $this->post('/dashboard/realestate/commissions/award/' . $holding->id, [
            'holding_id' => $holding->id
        ]);

        $depositCommissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $constants->deposit_award);

        $salesAgreementCommissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $constants->sales_agreement_award);

        $installmentCommissionPercentage = $paidPercentage * array_sum($constants->installment_stages) / 100;

        $installmentCommissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $installmentCommissionPercentage);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $depositCommissionAmount,
            'type_id' => 1,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $salesAgreementCommissionAmount,
            'type_id' => 2,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $installmentCommissionAmount,
            'type_id' => 3,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);
    }

    /*
     * Test Award of Commission on Payment Made
     */
    public function testCreationOfCommissionOnFirstPayment()
    {
        $holding = $this->setupReserveUnit([
            'reservation_date' => null,
            'tranche_id' => null,
            'payment_plan_id' => null,
        ]);

        $clientPayment = $this->getClientPaymentAndTransaction(
            [
                'client_id' => $holding->client_id,
                'amount' => 100000000,
                'date' => Carbon::now()->subDay(),
                'project_id' => $holding->unit->project_id,
                'type_id' => 1
            ],
            [
                'type' => 1,
                'custodial_account_id' => 1,
                'client_id' => 1,
                'amount' => 100000000,
                'date' => Carbon::now()->subDay()
            ]
        );

        $url = '/dashboard/realestate/payments/create/' . $holding->id;

        $paymentSchedule = $holding->paymentSchedules()->first();

        $reservationDate = Carbon::now();

        $recipient = $this->getCommissionRecipient();

        $postArray = [
            "date" => $reservationDate,
            "amount" => "25000",
            "description" => "Reservation Amount",
            "schedule_id" => $paymentSchedule->id,
            "tranche_id" => 1,
            "negotiated_price" => null,
            "payment_plan_id" => 2,
            "recipient_id" => $recipient->id,
            "awarded" => "1",
            "discount" => "0",
            "rate" => 1,
            "commission_rate_name" => "Staff - 1.00%",
            "reason" => null,
            "client_code" => null,
        ];

        $this->post($url, $postArray)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The payment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'make_real_estate_payment',
            'client_id' => $holding->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'make_real_estate_payment')
            ->where('client_id', $holding->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Payment was successfully made');

        $this->assertDatabaseHas((new RealestateCommission())->getTable(), [
            'recipient_id' => $recipient->id,
            'holding_id' => $holding->id,
            'awarded' => $postArray['awarded'],
            'commission_rate' => $postArray['rate']
        ]);
    }

    public function testAwardOfCommissionOnMultiplePayments()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ]);

        $clientPayment = $this->getClientPaymentAndTransaction(
            [
                'client_id' => $holding->client_id,
                'amount' => 100000000,
                'date' => Carbon::now()->subDay(),
                'project_id' => $holding->unit->project_id,
                'type_id' => 1
            ],
            [
                'type' => 1,
                'custodial_account_id' => 1,
                'client_id' => 1,
                'amount' => 100000000,
                'date' => Carbon::now()->subDay()
            ]
        );

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => 25000
        ]);

        $price = $holding->price();

        $depositSchedule = $this->setupRealEstatePaymentSchedule([
            'unit_holding_id' => $holding->id,
            'amount' => $price * 0.1,
            'payment_type_id' => 2,
            'date' => $reservationDate->copy()->addMonth(),
        ]);

        $installmentSchedule = $this->setupRealEstatePaymentSchedule([
            'unit_holding_id' => $holding->id,
            'amount' => $price * 0.2,
            'payment_type_id' => 3,
            'date' => $reservationDate->copy()->addMonth(2),
        ]);

        $url = '/dashboard/realestate/payments/create/' . $holding->id;

        $reservationDate = Carbon::now();

        $postArray = [
            "date" => $reservationDate,
            "amount" => $depositSchedule->amount,
            "description" => "Deposit Amount",
            "schedule_id" => $depositSchedule->id,
        ];

        $this->post($url, $postArray)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The payment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'make_real_estate_payment',
            'client_id' => $holding->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'make_real_estate_payment')
            ->where('client_id', $holding->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Payment was successfully made');

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'type_id' => 1
        ]);

        $postArray = [
            "date" => $reservationDate,
            "amount" => $depositSchedule->amount,
            "description" => "Installment Amount",
            "schedule_id" => $installmentSchedule->id,
        ];

        $this->post($url, $postArray)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The payment has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'make_real_estate_payment',
            'client_id' => $holding->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'make_real_estate_payment')
            ->where('client_id', $holding->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('Payment was successfully made');

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'type_id' => 3
        ]);
    }

    public function testAwardOfCommissionOnLooSigned()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([], [], [
            'date_signed' => null
        ]);

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => 25000
        ]);

        $price = $holding->price();

        $depositSchedule = $this->setupRealEstatePaymentSchedule([
            'unit_holding_id' => $holding->id,
            'amount' => $price * 0.1,
            'payment_type_id' => 2,
            'date' => $reservationDate->copy()->addMonth(),
        ]);

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $depositSchedule->amount
        ]);

        $url = '/dashboard/realestate/loos/upload/' . $holding->loo->id;

        $reservationDate = Carbon::now();

        $postArray = [
            "sent_on" => $reservationDate->copy()->subDays(14),
            "date_signed" => $reservationDate,
            "advocate_id" => $holding->loo->advocate_id,
            "file" => UploadedFile::fake()->create('document.pdf', 100),
        ];

        $this->post($url, $postArray)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The LOO has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'loo_uploading',
            'client_id' => $holding->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'loo_uploading')
            ->where('client_id', $holding->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The LOO details have been saved');

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'type_id' => 1
        ]);
    }

    public function testAwardOfCommissionOnSalesAgreementSigned()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([], [], [], [
            'date_signed' => null
        ]);

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => 25000
        ]);

        $price = $holding->price();

        $depositSchedule = $this->setupRealEstatePaymentSchedule([
            'unit_holding_id' => $holding->id,
            'amount' => $price * 0.1,
            'payment_type_id' => 2,
            'date' => $reservationDate->copy()->addMonth(),
        ]);

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $depositSchedule->amount
        ]);

        $url = '/dashboard/realestate/reservations/upload-sales-agreement/' . $holding->id;

        $reservationDate = Carbon::now();

        $postArray = [
            "title" => 'Sales Agreement',
            "date_signed" => $reservationDate,
            "file" => UploadedFile::fake()->create('document.pdf', 100),
        ];

        $this->post($url, $postArray)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The sales agreement has been saved for approval');

        $this->assertDatabaseHas((new ClientTransactionApproval())->getTable(), [
            'transaction_type' => 'sales_agreement_uploading',
            'client_id' => $holding->client_id,
            'sent_by' => Auth::id()
        ]);

        $approval = ClientTransactionApproval::where('transaction_type', 'sales_agreement_uploading')
            ->where('client_id', $holding->client_id)->latest()->first();

        $this->approve($approval)
            ->assertRedirect()
            ->followRedirects()
            ->assertSeeText('The sales agreement details have been saved');

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'type_id' => 2
        ]);
    }
}
