<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 05/11/2018
 * Time: 12:22
 */

namespace Tests\Feature\RealEstate;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Country;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\Commander\CommanderTrait;
use Tests\Helpers\Base;
use Tests\Helpers\ClientPayments;
use Tests\TestCase;

class ReserveUnitTest extends TestCase
{
    use DatabaseTransactions, Base, ClientPayments, CommanderTrait;

    protected $user;

    protected $faker;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        factory(\App\Cytonn\Models\ClientInvestmentType::class)->create(['name' => 'investment']);
        factory(\App\Cytonn\Models\ClientInvestmentType::class)->create(['name' => 'topup']);
    }


    public function testUnitReservationApplication()
    {
        $unit = $this->setupRealEstateUnit();

        $subscriber = [
            "title" => 2,
            "pin_no" => "ASNQ8H3764GD34",
            "id_or_passport" => $this->faker->unique()->randomNumber($nbDigits = 8),
            "telephone_home" => $this->faker->phoneNumber,
            "method_of_contact_id" => 1,
            "employment_id" => 4,
            "type" => "reserve_unit",
            "unit_id" => $unit->id,
            "contact_person_lname" => 'Zuma',
            "contact_person_fname" => 'Jacob',
            'dob' => $this->faker->date,
            'email' => $this->faker->email,
            'funds_source_id' => 1,
            'funds_source_other' => "",
            'postal_address' => "1234",
            'postal_code' => "00200",
            'street' => "5TH NGONG AV",
            'country_id' => Country::first()->id,
            'telephone_office' => $this->faker->phoneNumber,
            "individual" => 1,
            "new_client" => 1,
            "complete" => true,
            "franchise" => true,
            "penalty_exempt" => true,
            "firstname" => "David",
            "middlename" => "Peter",
            "lastname" => "Loves",
            "gender_id" => 1,
            "town" => "Nakuru",
            "existing_client" => false,
            'branch_id' => 2,
            "transaction_type" => 'reserve_unit'
        ];

        // post and check approvals table
        $this->post('api/realestate/units/reserve', $subscriber)
            ->isSuccessful();

        $table = (new ClientTransactionApproval())->getTable();

        $this->assertDatabaseHas($table, ['transaction_type' => $subscriber['transaction_type']]);
    }

    public function testUnitReservationApproval()
    {
        $this->assertTrue(true);
//        $unit = $this->setupRealEstateUnit();
//
//        //approval
//        $approval = factory(ClientTransactionApproval::class)->create([
//            'client_id' => null, // new client
//            'transaction_type' => 'reserve_unit',
//            'payload' => [
//                "individual" => 1,
//                "new_client" => 1,
//                "complete" => true,
//                "franchise" => false,
//                "title" => 5,
//                "lastname" => "James",
//                "middlename" => "Peter",
//                "firstname" => "Tommy",
//                "gender_id" => 1,
//                "email" => $this->faker->email,
//                "method_of_contact_id" => 1,
//                "employment_id" => 4,
//                "type" => "reserve_unit",
//                "unit_id" => $unit->id,
//                "investor_account_name" => "Tommy Peters",
//                "investor_account_number" => "1723616728638123",
//                "investor_bank" => 5,
//                "investor_bank_branch" => 525,
//                "existing_client" => false,
//            ],
//            'sent_by' => $this->user->id
//        ]);
//
//        $data = $this->approve($approval)
//            ->assertStatus(200);
//
////        ->assertRedirect()
////
////        ->assertSeeText("approved");
//
//        $this->assertDatabaseHas((new ReservationForm())->getTable(), [
//            'client_type_id' => $data->individual,
//            'client_id' => $approval->client_id,
//            'firstname' => $data->firstname,
//            'middlename' => $data->middlename,
//            'lastname' => $data->lastname,
//            'email' => $data->email,
////            'holding' => $unityHolding->if
//        ]);
    }
}
