<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\units\Investments\RealEstate\Commissions;

use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Realestate\Commissions\Calculator\Award;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class RealEstateCommissionAwardUnitTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        $this->setupForRealEstateTests();
    }

    private function getAwardClass(UnitHolding $holding)
    {
        return new Award($holding);
    }

    /*
     * ON DEPOSIT COMPLETE
     */
    public function testNoCommissionPaidWithoutLoo()
    {
        $holding = $this->getUnitHolding([], [], null);

        $awardClass = $this->getAwardClass($holding);

        $awardClass->onDepositComplete();

        $this->assertDatabaseMissing((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
        ]);
    }

    public function testNoCommissionPaidForUnsignedLoo()
    {
        $holding = $this->getUnitHolding([], [], [
            'date_signed' => null
        ]);

        $awardClass = $this->getAwardClass($holding);

        $awardClass->onDepositComplete();

        $this->assertDatabaseMissing((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
        ]);
    }

    public function testNoCommissionPaidZeroValueUnit()
    {
        $holding = $this->getUnitHolding([
            'discount' => 100
        ]);

        $awardClass = $this->getAwardClass($holding);

        $awardClass->onDepositComplete();

        $this->assertDatabaseMissing((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
        ]);
    }

    public function testNoCommissionPaidBeforeMinimumPercentageRequirement()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidPercentage = $constants->minimum_payment_percentage >= 1 ? $constants->minimum_payment_percentage - 1 : 0;

        $paidAmount = $paidPercentage * $holding->price() / 100;

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $awardClass->onDepositComplete();

        $this->assertDatabaseMissing((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
        ]);
    }

    public function testNoCommissionPaidForZeroCommissionRate()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ], [
            'commission_rate' => 0
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidAmount = $constants->minimum_payment_percentage * $holding->price() / 100;

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $awardClass->onDepositComplete();

        $this->assertDatabaseMissing((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
        ]);
    }

    public function testCommissionPaidOnDepositComplete()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidAmount = $constants->minimum_payment_percentage * $holding->price() / 100;

        $payment = $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $holding->loo->date_received);

        $awardClass = $this->getAwardClass($holding);

        $depositPercentage = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date))->deposit_award;

        $awardClass->onDepositComplete();

        $commissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $depositPercentage);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $commissionAmount,
            'type_id' => 1,
            'date' => $latestDate
        ]);
    }

    /*
     * ON SALES AGREEMENT SIGNED
     */
    public function testNoSalesAgreementCommissionPaidOnUnsignedSalesAgreement()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ], [], [], [
            'date_signed' => null
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidAmount = $constants->minimum_payment_percentage * $holding->price() / 100;

        $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $awardClass->onSalesAgreementSigned($holding->salesAgreement);

        $this->assertDatabaseMissing((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'type_id' => 2,
        ]);
    }

    public function testSalesAgreementCommissionPaidOnSignedSalesAgreement()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidAmount = $constants->minimum_payment_percentage * $holding->price() / 100;

        $payment = $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $holding->loo->date_received);

        $latestDate = Carbon::latest($latestDate, $holding->salesAgreement->date_received);

        $awardClass->onSalesAgreementSigned($holding->salesAgreement);

        $commissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $constants->sales_agreement_award);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $commissionAmount,
            'type_id' => 2,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);
    }

    /*
     * ON INSTALLMENT PAID
     */
    public function testInstallmentCommissionPaidOnPayment()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidPercentage = (int) rand($constants->minimum_payment_percentage, 100);

        $paidAmount = $paidPercentage  * $holding->price() / 100;

        $payment = $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $holding->loo->date_received);

        $latestDate = Carbon::latest($latestDate, $holding->salesAgreement->date_received);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $latestDate);

        $awardClass->onInstallmentPaidNew($payment);

        $commissionPercentage = $paidPercentage * array_sum($constants->installment_stages) / 100;

        $commissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $commissionPercentage);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $commissionAmount,
            'type_id' => 3,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);

        $paidPercentage = (int) rand(0, 100 - $paidPercentage);

        $paidAmount = $paidPercentage  * $holding->price() / 100;

        $payment = $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $holding->loo->date_received);

        $latestDate = Carbon::latest($latestDate, $holding->salesAgreement->date_received);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $latestDate);

        $awardClass->onInstallmentPaidNew($payment);

        $commissionPercentage = $paidPercentage * array_sum($constants->installment_stages) / 100;

        $commissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $commissionPercentage);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $commissionAmount,
            'type_id' => 3,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);
    }

    /*
     * ALL TYPES COMBINED
     */
    public function testCommissionPaidForAllTypes()
    {
        $reservationDate = Carbon::now()->subMonths(2);

        $holding = $this->getUnitHolding([
            'reservation_date' => $reservationDate->toDateString()
        ]);

        $awardClass = $this->getAwardClass($holding);

        $constants = $awardClass->getCommissionConstants(Carbon::parse($holding->reservation_date));

        $paidPercentage = (int) rand($constants->minimum_payment_percentage, 100);

        $paidAmount = $paidPercentage  * $holding->price() / 100;

        $payment = $this->setupRealEstatePayment([
            'holding_id' => $holding->id,
            'date' => Carbon::now(),
            'amount' => $paidAmount
        ]);

        $looDate = Carbon::latest(Carbon::parse($payment->date), $holding->loo->date_received);

        $saDate = Carbon::latest($looDate, $holding->salesAgreement->date_received);

        $latestDate = Carbon::latest(Carbon::parse($payment->date), $saDate);

        $awardClass->onDepositComplete();

        $awardClass->onSalesAgreementSigned($holding->salesAgreement);

        $awardClass->onInstallmentPaidNew($payment);

        $depositCommissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $constants->deposit_award);

        $salesAgreementCommissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $constants->sales_agreement_award);

        $installmentCommissionPercentage = $paidPercentage * array_sum($constants->installment_stages) / 100;

        $installmentCommissionAmount = $this->getRealEstateCommissionAmount($holding->commission->amount, $installmentCommissionPercentage);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $depositCommissionAmount,
            'type_id' => 1,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $salesAgreementCommissionAmount,
            'type_id' => 2,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);

        $this->assertDatabaseHas((new RealEstateCommissionPaymentSchedule())->getTable(), [
            'commission_id' => $holding->commission->id,
            'amount' => $installmentCommissionAmount,
            'type_id' => 3,
            'date' => Carbon::parse($latestDate)->toDateTimeString()
        ]);
    }
}
