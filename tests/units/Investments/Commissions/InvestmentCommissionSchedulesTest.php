<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\units\Investments\Commissions;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class InvestmentCommissionSchedulesTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    protected $commissionGenerator;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();

        $this->commissionGenerator = new Generator();
    }

    public function testNoCommissionForZeroCommissionInvestment()
    {
        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => '2018-01-01',
            'maturity_date' => '2019-01-01',
            'amount' => 10000000
        ], [
            'rate' => 0
        ], false);

        $this->commissionGenerator->createSchedules($investment);

        $this->assertDatabaseMissing((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id
        ]);
    }

    public function testNoCommissionForZeroTenorInvestment()
    {
        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => '2018-01-01',
            'maturity_date' => '2018-01-01',
            'amount' => 10000000
        ], [], false);

        $this->commissionGenerator->createSchedules($investment);

        $this->assertDatabaseMissing((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id
        ]);
    }

    /*
     * ONE TIME
     */
    public function testPaysOneTimeCommissionForAboveOneYear()
    {
        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => '2018-01-01',
            'maturity_date' => '2019-01-01',
            'amount' => 10000000
        ], [], false);

        $this->commissionGenerator->createSchedules($investment);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount,
            'date' => $investment->invested_date,
            'first' => true,
            'full' => true
        ]);

        $this->assertEquals(1, $investment->commission->schedules()->count());
    }

    public function testPaysOneTimeCommissionFromCommissionStartDate()
    {
        $investedDate = Carbon::now();

        $maturityDate = $investedDate->copy()->addDays(365);

        $commissionStartDate = $investedDate->copy()->addMonths(3)->startOfMonth();

        $investment = $this->getInvestmentAndCommissions(
            [
                'invested_date' => $investedDate->toDateString(),
                'maturity_date' => $maturityDate->toDateString(),
                'amount' => 10000000
            ],
            [
                'start_date' => $commissionStartDate->toDateString()
            ],
            false
        );

        $this->commissionGenerator->createSchedules($investment);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount,
            'date' => $commissionStartDate,
            'first' => true,
            'full' => true
        ]);
    }

    public function testRegeneratesOneTimeSchedules()
    {
        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => '2018-01-01',
            'maturity_date' => '2019-01-01',
            'amount' => 10000000
        ], [], false);

        $this->commissionGenerator->createSchedules($investment);

        $oldSchedules = $investment->commission->schedules;

        $this->commissionGenerator->recreateSchedules($investment);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount,
            'date' => $investment->invested_date,
            'first' => true,
            'full' => true
        ]);

        $oldSchedules->each(function ($schedule) {
            $this->assertNotNull($schedule->fresh()->deleted_at);
        });

        $this->assertEquals(1, $investment->commission->schedules()->count());
    }

    public function testNoCreationOfExtensionSchedulesForUnpaidCommission()
    {
        $investment = $this->getInvestment();

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->subDays(41)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->subDays(11)->toDateString(),
        ]);

        $oldSchedules = $investment->commission->schedules;

        $oldMaturityDate = Carbon::parse($investment->maturity_date);

        $investment->update([
            'maturity_date' => $oldMaturityDate->copy()->addMonths(3)
        ]);

        $investment = $investment->fresh();

        $this->commissionGenerator->createSchedulesForExtension($investment, $oldMaturityDate);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount,
            'date' => $investment->invested_date,
            'first' => true,
            'full' => true
        ]);

        $oldSchedules->each(function ($schedule) {
            $this->assertNotNull($schedule->fresh()->deleted_at);
        });
    }

    public function testCreationOfOneTimeExtensionSchedule()
    {
        $investment = $this->getInvestment();

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->startOfMonth()->subDays(11)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->startOfMonth()->addDays(19)->toDateString(),
        ]);

        $oldMaturityDate = Carbon::parse($investment->maturity_date);

        $investment->update([
            'maturity_date' => $oldMaturityDate->copy()->addDays(365)
        ]);

        $investment = $investment->fresh();

        $this->commissionGenerator->createSchedulesForExtension($investment, $oldMaturityDate);

        $commissionAmount = $this->getCommissionAmount(
            $investment->amount,
            $investment->commission->rate,
            365
        );

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount,
            'date' => Carbon::today(),
            'first' => false,
            'full' => false
        ]);

        $this->assertEquals(2, $investment->commission->schedules()->count());
    }

    /*
     * STAGGERED
     */
    public function testPaysStaggeredCommissionForBelowOneYear()
    {
        $investedDate = Carbon::parse('2018-01-01');

        $maturityDate = $investedDate->copy()->addMonths(6)->startOfMonth();

        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => $investedDate->toDateString(),
            'maturity_date' => $maturityDate->toDateString(),
            'amount' => 10000000
        ], [], false);

        $this->commissionGenerator->createSchedules($investment);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount / 2,
            'date' => $investment->invested_date,
            'first' => true,
            'full' => false
        ]);

        $startDate = $investedDate->copy();

        while ($startDate < $maturityDate) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'commission_id' => $investment->commission->id,
                'amount' => $commissionAmount / (2 * 6),
                'date' => $startDate,
                'first' => false,
                'full' => false
            ]);

            $startDate->addMonthNoOverflow()->startOfMonth();
        }

        $this->assertEquals(7, $investment->commission->schedules()->count());
    }

    public function testPaysStaggeredCommissionFromCommissionStartDate()
    {
        $investedDate = Carbon::now();

        $maturityDate = $investedDate->copy()->addMonths(6)->startOfMonth();

        $commissionStartDate = $investedDate->copy()->addMonths(3)->startOfMonth();

        $investment = $this->getInvestmentAndCommissions(
            [
                'invested_date' => $investedDate->toDateString(),
                'maturity_date' => $maturityDate->toDateString(),
                'amount' => 10000000
            ],
            [
                'start_date' => $commissionStartDate->toDateString()
            ],
            false
        );

        $this->commissionGenerator->createSchedules($investment);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount / 2,
            'date' => $commissionStartDate,
            'first' => true,
            'full' => false
        ]);

        $startDate = $investedDate->copy();

        while ($startDate < $maturityDate) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'commission_id' => $investment->commission->id,
                'amount' => $commissionAmount / (2 * 6),
                'date' => $startDate < $commissionStartDate ? $commissionStartDate : $startDate,
                'first' => false,
                'full' => false
            ]);

            $startDate->addMonthNoOverflow()->startOfMonth();
        }
    }

    public function testRegeneratesStaggeredCommissionSchedules()
    {
        $investedDate = Carbon::parse('2018-01-01');

        $maturityDate = $investedDate->copy()->addMonths(6)->startOfMonth();

        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => $investedDate->toDateString(),
            'maturity_date' => $maturityDate->toDateString(),
            'amount' => 10000000
        ], [], false);

        $this->commissionGenerator->createSchedules($investment);

        $oldSchedules = $investment->commission->schedules;

        $this->commissionGenerator->recreateSchedules($investment);

        $days = $this->getTenorInDays($investment);

        $commissionAmount = $this->getCommissionAmount($investment->amount, $investment->commission->rate, $days);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount / 2,
            'date' => $investment->invested_date,
            'first' => true,
            'full' => false
        ]);

        $startDate = $investedDate->copy();

        while ($startDate < $maturityDate) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'commission_id' => $investment->commission->id,
                'amount' => $commissionAmount / (2 * 6),
                'date' => $startDate,
                'first' => false,
                'full' => false
            ]);

            $startDate->addMonthNoOverflow()->startOfMonth();
        }

        $this->assertEquals(7, $investment->commission->schedules()->count());

        $oldSchedules->each(function ($schedule) {
            $this->assertNotNull($schedule->fresh()->deleted_at);
        });
    }

    public function testCreationOfStaggeredExtensionSchedule()
    {
        $maturityDate = Carbon::now()->subDays(10);

        $investedDate = $maturityDate->copy()->subMonths(6);

        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => $investedDate->toDateString(),
            'maturity_date' => $maturityDate->toDateString(),
            'amount' => 10000000
        ]);

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->startOfMonth()->subDays(11)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->addMonth()->startOfMonth()->addDays(19)->toDateString(),
        ]);

        $oldMaturityDate = Carbon::parse($investment->maturity_date);

        $investment->update([
            'maturity_date' => $oldMaturityDate->copy()->addMonths(3)
        ]);

        $investment = $investment->fresh();

        $this->commissionGenerator->createSchedulesForExtension($investment, $oldMaturityDate);

        $commissionAmount = $this->getCommissionAmount(
            $investment->amount,
            $investment->commission->rate,
            Carbon::parse($investment->maturity_date)->diffInDays($oldMaturityDate)
        );

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount / 2,
            'date' => Carbon::today(),
            'description' => 'Bonus for investment extension',
            'first' => false,
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'amount' => $commissionAmount / (2 * 3),
            'first' => false,
        ]);

        $this->assertEquals(12, $investment->commission->schedules()->count());
    }
}
