<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Tests\units\Commissions\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Investment\Commission\ClawBack;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class InvestmentCommissionClawbackTest extends TestCase
{
    use DatabaseTransactions;

    use Base;

    protected $faker;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->user = $this->login();
    }

    private function getClawbackClass($investment, $withdrawal)
    {
        return new ClawBack($investment, $withdrawal);
    }

    /*
     * WITHDRAWALS
     */
    public function testNoClawbackForWithdrawalOnMaturityDate()
    {
        $investment = $this->getInvestment();

        $withdrawal = $this->getWithdrawal($investment, $investment->maturity_date, 1000000);

        $clawbackClass = new ClawBack($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
            'type' => 'withdraw'
        ]);
    }

    public function testNoClawbackForZeroRateCommission()
    {
        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => '2018-01-01',
            'maturity_date' => '2019-01-01',
            'amount' => 10000000
        ], [
            'rate' => 0
        ]);

        $this->assertDatabaseMissing((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(165);

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, 1000000);

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
            'type' => 'withdraw'
        ]);
    }

    public function testNoClawbackForInterestWithdrawal()
    {
        $investment = $this->getInvestment();

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(165);

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, 500000);

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
            'type' => 'withdraw'
        ]);
    }

    /*
     * PREMATURE WITHDRAWALS
     */
    /*
     * ONE TIME
     */
    public function testCanClawbackOnPrematureWithdrawalForOnetimeCommission()
    {
        $investment = $this->getInvestment();

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(165);

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, 1000000);

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 200);

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'amount' => $clawbackAmount,
            'withdrawal_id' => $withdrawal->id,
            'type' => 'withdraw'
        ]);
    }

    public function testNoClawbackOnPrematurePartialWithdrawalForUnpaidOneTime()
    {
        $investment = $this->getInvestment();

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->subDays(41)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->subDays(11)->toDateString(),
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(15);

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, 1000000);

        $scheduled = $investment->commission->schedules()->sum('amount');

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 350);

        $deserved = $scheduled - $clawbackAmount;

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
            'type' => 'withdraw'
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $investment->invested_date,
            'amount' => $deserved
        ]);
    }

    public function testNoClawbackOnPrematureFullWithdrawalForUnpaidOneTime()
    {
        $investment = $this->getInvestment();

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->subDays(41)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->subDays(11)->toDateString(),
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(15);

        $currentValue = $investment->calculate($withdrawalDate, true)->total();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $scheduled = $investment->commission->schedules()->sum('amount');

        $schedule = $investment->commission->schedules()->first();

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 350);

        $deserved = $scheduled - $clawbackAmount;

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'amount' => $deserved,
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'id' => $schedule->id,
            'date' => $schedule->date,
            'amount' => $schedule->amount,
            'claw_back' => true
        ]);
    }

    public function testNoClawbackOnPrematureFullWithdrawalOnInvestedDateForOneTime()
    {
        $investment = $this->getInvestment();

        $bcp = $this->getBulkCommission([
            'start' => Carbon::parse($investment->invested_date)->subDays(41)->toDateString(),
            'end' => Carbon::parse($investment->invested_date)->subDays(11)->toDateString(),
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date);

        $currentValue = $investment->calculate($withdrawalDate, false)->total();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $schedule = $investment->commission->schedules()->first();

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'id' => $schedule->id,
            'date' => $schedule->date,
            'amount' => $schedule->amount,
            'claw_back' => true
        ]);

        $this->assertDatabaseMissing((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'claw_back' => false
        ]);
    }

    /*
     * STAGGERED
     */
    public function testNoClawbackOnPrematureFullWithdrawalOnInvestedDateForStaggered()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $bcp = $this->getBulkCommission([
            'start' => '2017-11-19',
            'end' => '2017-12-19'
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date);

        $currentValue = $investment->calculate($withdrawalDate, false)->total();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);

        $investment->commission->schedules()->each(function ($schedule) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'id' => $schedule->id,
                'date' => $schedule->date,
                'amount' => $schedule->amount,
                'claw_back' => true
            ]);
        });

        $this->assertDatabaseMissing((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'claw_back' => false
        ]);
    }

    public function testNoClawbackOnPrematurePartialWithdrawalForUnpaidStaggered()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $bcp = $this->getBulkCommission([
            'start' => '2018-02-19',
            'end' => '2018-03-19'
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(60);

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, 3650000);

        $unpaid = $investment->commission->schedules()->where('date', '>=', $bcp->end)->sum('amount');

        $unpaidSchedules = $investment->commission->schedules()->where('date', '>=', $bcp->end)->get();

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 120);

        $deserved = $unpaid - $clawbackAmount;

        $ratio = $deserved / $unpaid;

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);

        $unpaidSchedules->each(function ($schedule) use ($ratio) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'id' => $schedule->id,
                'date' => $schedule->date,
                'amount' => $schedule->amount * $ratio,
            ]);
        });
    }

    public function testNoClawbackOnPrematureFullWithdrawalForUnpaidStaggered()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $bcp = $this->getBulkCommission([
            'start' => '2017-11-19',
            'end' => '2017-12-19'
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(15);

        $currentValue = $investment->calculate($withdrawalDate, true)->total();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $unpaidSchedules = $investment->commission->schedules()->where('date', '>=', $bcp->end)->get();

        $unpaidAmount = $unpaidSchedules->sum('amount');

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 165);

        $deserved = $unpaidAmount - $clawbackAmount;

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'amount' => $deserved,
        ]);

        $unpaidSchedules->each(function ($schedule) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'id' => $schedule->id,
                'date' => $schedule->date,
                'amount' => $schedule->amount,
                'claw_back' => true
            ]);
        });
    }

    public function testCanClawbackOnPrematureWithdrawalForStaggered()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $bcp = $this->getBulkCommission([
            'start' => '2018-04-19',
            'end' => '2018-05-19'
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(140);

        $currentValue = $investment->calculate($withdrawalDate, false)->total();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $unpaidSchedules = $investment->commission->schedules()->where('date', '>=', $bcp->end)->get();

        $unpaidAmount = $unpaidSchedules->sum('amount');

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 40);

        $deserved = $unpaidAmount - $clawbackAmount;

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onWithdrawal();

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
            'amount' => abs($deserved)
        ]);

        $unpaidSchedules->each(function ($schedule) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'id' => $schedule->id,
                'date' => $schedule->date,
                'amount' => $schedule->amount,
                'claw_back' => true
            ]);
        });
    }

    /*
     * ROLLOVERS
     */
    public function testNoClawbackOnRolloverOnMaturityDate()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $withdrawalDate = Carbon::parse($investment->maturity_date);

        $currentValue = $investment->calculate($withdrawalDate, true)->total();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onRollover($currentValue, $withdrawal->date, $investment->maturity_date);

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);
    }

    public function testNoClawbackOnRolloverForZeroCommission()
    {
        $investment = $this->getInvestmentAndCommissions([
            'invested_date' => '2018-01-01',
            'maturity_date' => '2018-06-30',
            'amount' => 36500000
        ], [
            'rate' => 0
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(90);

        $currentValue = $investment->calculate($withdrawalDate, true)->total();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onRollover($currentValue, $withdrawal->date, $investment->maturity_date);

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);
    }

    public function testNoClawbackOnPrematureRolloverForUnpaidStaggered()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $bcp = $this->getBulkCommission([
            'start' => '2018-04-19',
            'end' => '2018-05-19'
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(165);

        $currentValue = $investment->calculate($withdrawalDate, false)->principal();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $unpaidSchedules = $investment->commission->schedules()->where('date', '>=', $bcp->end)->get();

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 15);

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onRollover($currentValue, $withdrawal->date, $investment->maturity_date);

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);

        $unpaidSchedules->each(function ($schedule) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'id' => $schedule->id,
                'date' => $schedule->date,
                'amount' => $schedule->amount,
                'claw_back' => true
            ]);
        });
    }

    public function testClawbackOnPrematureRollover()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $bcp = $this->getBulkCommission([
            'start' => '2018-01-19',
            'end' => '2018-02-19'
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(60);

        $currentValue = $investment->calculate($withdrawalDate, false)->principal();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $unpaidSchedules = $investment->commission->schedules()->where('date', '>=', $bcp->end)->get();

        $unpaidAmount = $unpaidSchedules->sum('amount');

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 120);

        $deserved = $clawbackAmount - $unpaidAmount;

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onRollover($currentValue, $withdrawal->date, $investment->maturity_date);

        $this->assertDatabaseHas((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
            'amount' => $deserved
        ]);

        $unpaidSchedules->each(function ($schedule) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'id' => $schedule->id,
                'date' => $schedule->date,
                'amount' => $schedule->amount,
                'claw_back' => true
            ]);
        });
    }

    public function testNoClawbackOnPrematureRolloverWithUnpaidCommission()
    {
        $investment = $this->getInvestment('2018-01-01', '2018-06-30', 36500000);

        $bcp = $this->getBulkCommission([
            'start' => '2018-04-19',
            'end' => '2018-05-19'
        ]);

        $withdrawalDate = Carbon::parse($investment->invested_date)->addDays(175);

        $currentValue = $investment->calculate($withdrawalDate, false)->principal();

        $withdrawal = $this->getWithdrawal($investment, $withdrawalDate, $currentValue);

        $unpaidSchedules = $investment->commission->schedules()->where('date', '>=', $bcp->end)->get();

        $unpaidAmount = $unpaidSchedules->sum('amount');

        $clawbackAmount = $this->getCommissionAmount($withdrawal->amount, $investment->commission->rate, 5);

        $deserved = $unpaidAmount - $clawbackAmount;

        $clawbackClass = $this->getClawbackClass($investment, $withdrawal);

        $clawbackClass->onRollover($currentValue, $withdrawal->date, $investment->maturity_date);

        $this->assertDatabaseMissing((new CommissionClawback())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'withdrawal_id' => $withdrawal->id,
        ]);

        $unpaidSchedules->each(function ($schedule) {
            $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
                'id' => $schedule->id,
                'date' => $schedule->date,
                'amount' => $schedule->amount,
                'claw_back' => true
            ]);
        });

        $this->assertDatabaseHas((new CommissionPaymentSchedule())->getTable(), [
            'commission_id' => $investment->commission->id,
            'date' => $withdrawal->date,
            'amount' => $deserved,
        ]);
    }
}
