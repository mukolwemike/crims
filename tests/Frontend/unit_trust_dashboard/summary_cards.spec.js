// import Summary from '../../../resources/client/assets/js/crims/dashboard/activities/unittrust/summarycards/summary-cards.vue';
// import {mount, createLocalVue, shallow} from 'vue-test-utils';
// import Vuex from 'vuex';
// import Vue from 'vue';
// import VueRouter from 'vue-router';
// import VueStore from '../../../resources/client/assets/js/store/index';
// import expect from 'expect';
// import routedRoutes from '../../../resources/client/assets/js/router/index'
// import sinon from 'sinon';
// import ElementUI from 'element-ui';
// Vue.use(ElementUI);
// Vue.use(VueRouter);
// Vue.use(Vuex);
//
// describe('Unit Trust Dashboard Summary Cards', () => {
//     let getters;
//     let mutations;
//     let routes;
//     let mockRouter;
//     let fund = {};
//
//     getters = {
//         fundsummary: () => 'fundsummary',
//         fetchingSammary: () => 'fetchingSammary',
//
//         suspendCallout: () => 'suspendCallout',
//         topupCount: () => 'topupCount',
//         isClient: () => 'isClient',
//         isMultipleClient: () => 'isMultipleClient',
//         currentUser: () => 'currentUser',
//         fetchCount: () => 'fetchCount'
//     };
//
//     mutations = {
//         CLIENT_BANK_ACCOUNTS() {
//         },
//         FETCH_FUND_SUMMARY() {
//         },
//     }
//
//     routes = [
//         {
//             name: 'main',
//             path: '/'
//         },
//         {
//             name: 'unittrust.list',
//             path: '/unittrust'
//         }
//
//     ]
//
//     const localVue = createLocalVue();
//     VueRouter.install.installed = true
//     localVue.use(Vuex);
//     localVue.use(ElementUI);
//
//     const router = new VueRouter({routedRoutes, routes});
//
//     const store = new Vuex.Store({
//         getters,
//         mutations
//     });
//
//     const wrapper = shallow(Summary, {
//         store,
//         localVue,
//         router
//     });
//
//
//     mockRouter = {
//         push: sinon.spy()
//     };
//
//
// })