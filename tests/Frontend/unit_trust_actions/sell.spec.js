import Sell from '../../../resources/client/assets/js/crims/investments/unittrust/details/actions/sell/sell.vue';
import {mount, createLocalVue, shallow} from 'vue-test-utils';
import Vuex from 'vuex';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueStore from '../../../resources/client/assets/js/store/index';
import expect from 'expect';
import routedRoutes from '../../../resources/client/assets/js/router/index'
import sinon from 'sinon';
import ElementUI from 'element-ui';
Vue.use(ElementUI);
Vue.use(VueRouter);
Vue.use(Vuex);

describe('Sell Unit Trust', () => {
    let getters;
    let mutations;
    let routes;
    let mockRouter;
    let fund = {};

    getters = {
        clientBankAccounts: () => 'clientBankAccounts',
        selectedClient: () => 'selectedClient',
        fundsummary: ()=> 'fundsummary',
        cisUnitFunds: () => 'cisUnitFunds',


        suspendCallout: () => 'suspendCallout',
        topupCount: () => 'topupCount',
        isClient: () => 'isClient',
        isMultipleClient: () => 'isMultipleClient',
        currentUser: () => 'currentUser',
        fetchCount: () => 'fetchCount'
    };

    mutations = {
        CLIENT_BANK_ACCOUNTS() {
        },
        FETCH_FUND_SUMMARY() {
        },
        GET_CLIENT_TOTAL_BALANCE(){}
    }

    routes = [
        {
            name: 'main',
            path: '/'
        },
        {
            name: 'unittrust.list',
            path: '/unittrust'
        }

    ]

    const localVue = createLocalVue();
    VueRouter.install.installed = true
    localVue.use(Vuex);
    localVue.use(ElementUI);

    const router = new VueRouter({routedRoutes, routes});

    const store = new Vuex.Store({
        getters,
        mutations
    });

    const wrapper = shallow(Sell, {
        store,
        localVue,
        router,
        propsData: {
            fund: fund
        }
    });


    mockRouter = {
        push: sinon.spy()
    };


    it('The default step is form', function () {
        expect(wrapper.vm.step).toBe('form');
    });

    it('One can only click Sell unit funds Button (Always disabled unless) if form unit field is greater than zero and also less than or equals to owned units. Date field should also not empty', function () {
        let self = wrapper.vm;
        expect(self.canSellUnit).toBe(false);
        wrapper.setProps({
            fund: {
                ownedUnitsUnedited: 200000
            }
        });
        wrapper.setData({
            sellUnitsForm: {
                number: '2000',
                date: new Date()
            }
        });
        expect(self.canSellUnit).toBe(true);

    });



    it('Sell-units-method should submit form to confirm step (only) if the compulsory form fields are completely field out', function () {
        let self = wrapper.vm;
        wrapper.setData({
            sellUnitsForm: {
                number: '2000',
                date: new Date()
            }
        });

        self.sell_units();
        expect(self.step).toBe('confirm');

    });

    it('Reset Button clears the buy unit trust form, clear errors and set default step to "form"', () => {
        let self = wrapper.vm;
        let form = {}
        wrapper.setData({
            sellUnitsForm: {
                number: '2000',
                date: new Date()
            },
            errors: [{form: 'error 101'}],
            step: 'confirm'
        });
        self.resetForm();
        expect(self.sellUnitsForm).toEqual(form);
        expect(wrapper.vm.errors.length).toBe(0);
        expect(wrapper.vm.step).toBe('form');
    })

    it('Errors variable should default to empty array', () => {
        expect(wrapper.vm.errors.length).toBe(0);
    })
})