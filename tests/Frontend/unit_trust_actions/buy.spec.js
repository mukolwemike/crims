import Buy from '../../../resources/client/assets/js/crims/investments/unittrust/details/actions/buy/buy.vue';
import {mount, createLocalVue, shallow} from 'vue-test-utils';
import Vuex from 'vuex';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueStore from '../../../resources/client/assets/js/store/index';
import expect from 'expect';
import routedRoutes from '../../../resources/client/assets/js/router/index'
import sinon from 'sinon';
import ElementUI from 'element-ui';
Vue.use(ElementUI);
Vue.use(VueRouter);
Vue.use(Vuex);

describe('Buy Unit Trust', () => {
    let getters;
    let mutations;
    let routes;
    let mockRouter;
    let fund = {};

    getters = {
        unitsToPurchase: () => 'unitsToPurchase',
        cisUnitFunds: () => [],
        fetchingFundBalance: () => 'fetchingFundBalance',
        fetchUnitsToPurchase: () => 'fetchUnitsToPurchase',
        defaultHeaders: () => 'defaultHeaders',
        selectedClient: () => 'selectedClient',
        fundBalance: () => 'fundBalance',
        fundsummary: ()=> 'fundsummary',


        suspendCallout: () => 'suspendCallout',
        topupCount: () => 'topupCount',
        isClient: () => 'isClient',
        isMultipleClient: () => 'isMultipleClient',
        currentUser: () => 'currentUser',
        fetchCount: () => 'fetchCount'
    };

    mutations = {
        GET_CLIENT_TOTAL_BALANCE() {
        },
        CLIENT_BANK_ACCOUNTS() {
        },
        FETCH_FUND_SUMMARY() {
        },
        GET_CIS_UNIT_FUNDS() {
        },
        UNIT_PURCHASE_CALCULATOR() {
        },
        FETCH_FUND_BALANCE() {
        }
    }

    routes = [
        {
            name: 'main',
            path: '/'
        },
        {
            name: 'unittrust.list',
            path: '/unittrust'
        }

    ]

    const localVue = createLocalVue();
    VueRouter.install.installed = true
    localVue.use(Vuex);
    localVue.use(ElementUI);

    const router = new VueRouter({routedRoutes, routes});

    const store = new Vuex.Store({
        getters,
        mutations
    });

    const wrapper = shallow(Buy, {
        store,
        localVue,
        router,
        propsData: {
            fund: fund
        }
    });


    mockRouter = {
        push: sinon.spy()
    };


    it('The default step is form', function () {
        expect(wrapper.vm.step).toBe('form');
    });

    it('One can only click buy unit funds if Amount field, date, units and unit_fund_id are specified and are not empty', function () {
        let self = wrapper.vm;
        expect(self.canBuyUnits).toBe(false);
        wrapper.setData({
            buyUnitsForm: {
                amount: '2000000',
                date: '12-05-2018',
                units: '2000',
                unit_fund_id: '3'
            }
        });
        expect(self.canBuyUnits).toBe(true);

    });

    it('Full form computed property is returned only when buyUnitsform amount and unit_fund_id have been field out', function () {
        let self = wrapper.vm;
        wrapper.setData({
            buyUnitsForm: {
                amount: '2000000',
                unit_fund_id: '3'
            }
        });
        expect(self.fullform).not.toBe(undefined);

    });

    // it('Watch fullform computed property and when not undefined commit a Mutator', function () {
    //     let self = wrapper.vm;
    //     wrapper.setData({
    //         buyUnitsForm: {
    //             amount: '',
    //             unit_fund_id: ''
    //         }
    //     });
    //     console.log(self.fullform, VueStore.getters.unitsToPurchase);
    //
    //     expect(VueStore.getters.fetchUnitsToPurchase).toBe(false);
    //
    //     wrapper.setData({
    //         buyUnitsForm: {
    //             amount: '2000000',
    //             unit_fund_id: '3'
    //         }
    //     });
    //     console.log(self.fullform, VueStore.getters.unitsToPurchase)
    //
    //     // expect(VueStore.getters.unitsToPurchase).not.toBe('');
    //
    // });

    it('Buy-units-method should submit form to confirm step (only) if the compulsory form fields are completely field out', function () {
        let self = wrapper.vm;
        wrapper.setData({
            buyUnitsForm: {
                amount: '2000000',
                date: '12-05-2018',
                units: '2000',
                unit_fund_id: '3'
            }
        });

        self.buy_units();
        expect(self.step).toBe('confirm');

    });

    it('Reset Button clears the buy unit trust form', () => {
        let self = wrapper.vm;
        let form = {}
        wrapper.setData({
            buyUnitsForm: {
                amount: '2000000',
                date: '12-05-2018',
                units: '2000',
                unit_fund_id: '3'
            }
        });
        self.resetForm();
        expect(self.buyUnitsForm).toEqual(form);
    })

    it('Errors variable should default to empty array', () => {
        expect(wrapper.vm.errors.length).toBe(0);
    })
})