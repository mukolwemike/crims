import Details from '../../../resources/client/assets/js/crims/investments/unittrust/details/details.vue';
import {mount, createLocalVue, shallow} from 'vue-test-utils';
import Vuex from 'vuex';
import Vue from 'vue';
import VueRouter from 'vue-router';
import  VueStore from '../../../resources/client/assets/js/store/index';
import expect from 'expect';
import routedRoutes from '../../../resources/client/assets/js/router/index'
import sinon from 'sinon';

//components
// import Dashboard from '../../../resources/client/assets/js/crims/dashboard/dashboard.vue';

Vue.use(VueRouter);
Vue.use(Vuex);




describe ('Unit Trust Details', ()=> {
    let getters;
    let mutations;
    let routes;
    let mockRouter;

    getters = {
        fundsummary: () => [],
        selectedClient: () => 'selectedClient',
        fetchingFundSummary: () => 'fetchingFundSummary',
        fundBalance: () => 'fundBalance',
        cisUnitFunds: () => 'cisUnitFunds',

        fetchingFundPurchases: () => 'fetchingFundPurchases',
        unitPurchases: () => 'unitPurchases',

        fundSales: () => 'fundSales',
        fundTransfers: () => 'fundTransfers',

        fundGivenTransfers: () => 'fundGivenTransfers',
        fetchingGivenTransfers: () => 'fetchingGivenTransfers',

        fundRecievedTransfers: () => 'fundRecievedTransfers',
        fetchingRecievedTransfers: () => 'fetchingRecievedTransfers',

        fundInstructions: () => 'fundInstructions',
        fetchingFundInstructions: () => 'fetchingFundInstructions',
        suspendCallout: () => 'suspendCallout',
        topupCount: () => 'topupCount',
        isClient: () => 'isClient',
        isMultipleClient: () => 'isMultipleClient',
        currentUser: () => 'currentUser',
        fetchCount: () => 'fetchCount'
    };

    mutations = {
        GET_CLIENT_TOTAL_BALANCE(){},
        CLIENT_BANK_ACCOUNTS(){},
        FETCH_FUND_SUMMARY(){},
        GET_CIS_UNIT_FUNDS(){}
    }

    routes = [
        {
            name: 'main',
            path: '/'
        },
        {
            name: 'unittrust.list',
            path: '/unittrust'
        }

    ]

    const localVue = createLocalVue();
    VueRouter.install.installed = true
    localVue.use(Vuex)

    const router = new VueRouter({routedRoutes, routes});

    const store = new Vuex.Store({
        getters,
        mutations
    });

    const wrapper = mount(Details, {
        store,
        localVue,
        router
    });

    mockRouter = {
        push: sinon.spy()
    };




    it('Check whether Unit Trusts Details page exists', function () {
        expect(wrapper.exists()).toBe(true)
    });



    it('Unit Trust Details page drawer buy_units initial state should be false', function () {
        expect(wrapper.vm.buy_units).toBe(false);
    });

    it('Unit Trust Details page drawer sell_units initial should be false', function () {
        expect(wrapper.vm.sell_units).toBe(false);
    });

    it('Unit Trust Details page drawer transfer_u initialnits should be false', function () {
        expect(wrapper.vm.transfer_units).toBe(false);
    });

    it('On Unit Trust  Details page mount FETCH_FUND_SUMMARY mutator is commited from a function if there are no funds', function () {
        VueStore.commit('FETCH_FUND_SUMMARY', {client_id: '5cd31630-bfd2-11e8-a369-d30f6e10cebdmn'})
        expect(VueStore.getters.fetchingFundSummary).toBe(true)
    });

    //
    // it('On Details page mount If the page does not have unit trust the user is redirected to Dashboard page', function () {
    //     VueStore.getters.selectedClient.hasUT = true
    //
    //     // wrapper.vm.checkPageData();
    //
    //     if(VueStore.getters.selectedClient.hasUT) {
    //         console.log('Router pushed to', wrapper.vm.$route,   VueStore.getters.selectedClient.hasUT)
    //     }else
    //     {
    //         console.log('Router pushed to', wrapper.vm.$route,   VueStore.getters.selectedClient.hasUT)
    //     }
    //
    // });


})