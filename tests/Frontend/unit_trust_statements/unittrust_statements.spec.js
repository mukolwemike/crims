import Statement from '../../../resources/client/assets/js/crims/statements/unittrust/index.vue';
import {mount, createLocalVue, shallow} from 'vue-test-utils';
import Vuex from 'vuex';
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueStore from '../../../resources/client/assets/js/store/index';
import expect from 'expect';
import routedRoutes from '../../../resources/client/assets/js/router/index'
import sinon from 'sinon';
import ElementUI from 'element-ui';

Vue.use(ElementUI);
Vue.use(VueRouter);
Vue.use(Vuex);

describe('Unit Trust Statement', () => {
    let getters;
    let mutations;
    let routes;
    let mockRouter;
    let fund = {};

    getters = {
        loadingFundStatement: () => 'loadingFundStatement',
        fundStatement: () => 'fundStatement',
        clientFunds: () => 'clientFunds',
        fetchingClientFunds: () => 'fetchingClientFunds',
        selectedClient: () => 'selectedClient',

        suspendCallout: () => 'suspendCallout',
        topupCount: () => 'topupCount',
        isClient: () => 'isClient',
        isMultipleClient: () => 'isMultipleClient',
        currentUser: () => 'currentUser',
        fetchCount: () => 'fetchCount',
        topups_data: () => 'topups_data',
        totalBalance: () => 'totalBalance',
        calloutAction: () => 'calloutAction'
    };

    mutations = {
        GET_FUND_STATEMENT() {
        },
        CLIENT_BANK_ACCOUNTS() {
        },
        FETCH_CLIENT_FUNDS() {
        },
        DOWNLOAD_FUND_STATEMENT() {
        },
        GET_CLIENT_TOTAL_BALANCE(){}
    };

    routes = [
        {
            name: 'main',
            path: '/'
        },
        {
            name: 'unittrust.list',
            path: '/unittrust'
        }

    ];

    const localVue = createLocalVue();
    VueRouter.install.installed = true;
    localVue.use(Vuex);
    localVue.use(ElementUI);

    const router = new VueRouter({routedRoutes, routes});

    const store = new Vuex.Store({
        getters,
        mutations
    });

    const wrapper = shallow(Statement, {
        store,
        localVue,
        router,
        propsData: {
            fund: fund
        }
    });


    mockRouter = {
        push: sinon.spy()
    };



    it('Check whether the statement component have the right page class', function () {
        let page_class = wrapper.find('.crims-page');
        expect(page_class.exists()).toBe(true);
    });

    it('Check whether the statement page table have the right table wrapper class', function () {
        wrapper.setData({
            loadingStatements: false
        });
        let table_wrapper_class = wrapper.find('.crims-table-area');
        expect(table_wrapper_class.exists()).toBe(true);
    });

    it('Statements table is only visible if the statements api have loaded the table', function () {
        wrapper.setData({
            loadingStatements: false
        });
        let table_wrapper_class = wrapper.find('.crims-table-area');
        expect(table_wrapper_class.exists()).toBe(true);
    });

    it('Full form computed property is returned only when Filter form fields are filled out and funds have loaded!', function () {
        let self = wrapper.vm;
        wrapper.setData({
            funds: {
                fund: '3',
                date: new Date(),
                start: new Date()
            }
        });
        expect(self.fullform).not.toBe(undefined);

    });


});