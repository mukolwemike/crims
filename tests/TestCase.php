<?php

namespace Tests;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ContactEntity;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStage;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;

    protected $seeders = [
        \PermissionsSeeder::class,
        \Custody\ClientPaymentTypeSeeder::class,
        \SettingsSeeder::class
    ];

    public function setUp()
    {
        parent::setUp();

        $this->addMacros();

        $this->runSeeders();

        $this->addEssentialRecords();
    }

    protected function addMacros()
    {
        $test = $this;

        TestResponse::macro('followRedirects', function ($testCase = null) use ($test) {
            $response = $this;
            $testCase = $testCase ?: $test;

            while ($response->isRedirect()) {
                $response = $testCase->get($response->headers->get('Location'));
            }

            return $response;
        });
    }

    protected function runSeeders()
    {
        foreach ($this->seeders as $seederClass) {
            $seeder = app($seederClass);
            $seeder->call($seeder);
        }
    }

    public function addEssentialRecords()
    {
        if (!ClientTransactionApprovalStage::where('applies_to_all', true)->exists()) {
            factory(ClientTransactionApprovalStage::class)->create(['applies_to_all' => true]);
        }
    }

    protected function approve(ClientTransactionApproval $approval)
    {
        //can view approval page
        $this->get("/dashboard/investments/approve/{$approval->id}")
            ->followRedirects()
            ->assertSuccessful();

        $stages = $approval->type()->allStages();

        if (!$stages->count()) {
            echo "Make sure approval stages have been created";
            return null;
        }

        $response = null;

        foreach ($stages as $stage) {
            //click approve
            $response = $this->get("dashboard/investments/approve/{$approval->id}/stage/{$stage->id}");
        }

        return $response;
    }
}
