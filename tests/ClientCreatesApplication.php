<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait ClientCreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/apps/clients.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
