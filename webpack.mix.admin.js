const {mix} = require('laravel-mix'),
    glob = require('glob');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.common.js'
        }
    }
});

let angularFiles = glob.sync(__dirname+'/resources/admin/assets/angular/app/**/*.*')
    .map((str) => { return str.replace(__dirname+'/', '');  });

let dependencyFiles = glob.sync(__dirname+'/resources/admin/assets/angular/dependencies/**/*.*')
    .map((str) => { return str.replace(__dirname+'/', ''); });

let extract = [
    'vue', 'vuex', 'vue-router', 'element-ui', 'chartkick', 'chart.js', 'sweetalert', 'vue-resource', 'moment',
    'vuetable-2', 'jquery', 'lodash', 'collect.js', 'file-saver'
];


mix
    .setPublicPath('public/admin')
    .js('resources/admin/assets/js/app.js', 'public/admin/js')
    .extract(extract)
    .babel(angularFiles, 'public/admin/js/cytonn.js')
    .scripts(dependencyFiles, 'public/admin/js/dependencies.js')
    .sass('resources/admin/assets/sass/app.scss', 'public/admin/css')
    .sass('resources/admin/assets/sass/email.scss', 'public/admin/css')
    // .sourceMaps()
;

mix.version();