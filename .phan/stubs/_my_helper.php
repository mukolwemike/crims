<?php
/**
 * Date: 09/02/2018
 * Time: 12:58
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace {

    use Cytonn\Notifier\FlashNotifier;

    class Input
    {
        public function get($key, $default = null)
        {
            return Request::get($key, $default);
        }

        public function all()
        {
            return Request::all();
        }

        public function except($keys)
        {
            return Request::except($keys);
        }
    }

    class Flash
    {
        /**
         * @param $message
         */
        public static function message($message)
        {
            return FlashNotifier::message($message);
        }

        /**
         * @param $message
         */
        public static function warning($message)
        {
            return FlashNotifier::warning($message);
        }

        /**
         * @param $message
         */
        public static function error($message)
        {
            return FlashNotifier::error($message);
        }

        /**
         * @param $message
         */
        public static function success($message)
        {
            return FlashNotifier::success($message);
        }

        /**
         * @param $message
         * @param string $title
         */
        public static function overlay($message, $title = 'Notice')
        {
            return FlashNotifier::overlay($message, $title);
        }
    }
}

namespace Maatwebsite\Excel{

    class LaravelExcelWriter
    {
        /**
         * @param string $ext
         * @param bool $path
         * @param bool $returnInfo
         * @return mixed
         */
        public function store($ext = 'xls', $path = false, $returnInfo = false)
        {
            return \Maatwebsite\Excel\Writers\LaravelExcelWriter::store($ext, $path, $returnInfo);
        }

        public function export($ext = 'xls', Array $headers = [])
        {
            return \Maatwebsite\Excel\Writers\LaravelExcelWriter::export($ext, $headers);
        }

        public function download($ext = 'xls', Array $headers = [])
        {
            return $this->export($ext, $headers);
        }
    }

}

namespace ConsoleTVs\Charts{

    class Multi
    {

        public function title($title)
        {
            return \ConsoleTVs\Charts\Builder\Multi::title($title);
        }
    }
}

namespace LaravelFCM\Sender {

    class DownstreamResponse extends \LaravelFCM\Response\DownstreamResponse
    {

    }
}
