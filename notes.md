## Project notes ##
### PSR2 Compliance ###
Use the commands below to check for or fix any PSR2 violations in the project

* `vendor/bin/phpcbf app --standard=PSR2`

Run phpcs tests

* `vendor/bin/phpcs app --standard=PSR2`

### Resetting Test DB ###
When tests fail due to DB schema changes, you need to update the sqlite DB by following the procedure below:

* Delete all migrations
* Regenerate new migrations from your schema (make sure it's upto date) - `$ php artisan migrate:generate`
* Delete or rename databases/production.sqlite and create a new empty file with the name
* Run the migrations `$ php artisan migrate  --env=testing`. In case of failures, open the failing migration and comment out the failing line or snippet for indexes
* Run the migrations again until they are complete