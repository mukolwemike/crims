pipeline {
  agent any
  stages {
    stage('Prepare') {
      steps {
        sh 'composer install'
        sh 'rm -rf build/api'
        sh 'rm -rf build/coverage'
        sh 'rm -rf build/logs'
        sh 'rm -rf build/pdepend'
        sh 'rm -rf build/phpdox'
        sh 'mkdir -p build/api'
        sh 'mkdir build/coverage'
        sh 'mkdir build/logs'
        sh 'mkdir build/pdepend'
        sh 'mkdir build/phpdox'
        sh 'touch build/logs/phpstan.xml'
      }
    }
    stage('QA Tests') {
      parallel {
        stage('Run PHP Tests') {
          environment {
            CACHE_DRIVER = 'file'
            SESSION_DRIVER = 'file'
            QUEUE_DRIVER = 'sync'
            MAIL_DRIVER = 'log'
            APP_KEY = 'base64:3+xNz9Gn0DMWacQ60JVOFV464i5IcjoC+9rb98TxZAI='
            APP_ENV = 'testing'
            APP_DEBUG = 'true'
            DOMAIN = 'https://127.0.0.1:8000'
          }
          steps {
            sh 'vendor/bin/phpunit --coverage-clover=`pwd`/build/coverage/coverage.xml --log-junit=`pwd`/build/logs/phpunit.xml'
          }
        }
        stage('Phan') {
          steps {
            sh '/var/lib/jenkins/.composer/vendor/bin/phan --output-mode=checkstyle --output=`pwd`/build/logs/phan.xml'
          }
        }
        stage('Phpcs') {
          steps {
            sh 'vendor/bin/phpcs -v --report=checkstyle --report-file=`pwd`/build/logs/checkstyle.xml --standard=PSR2 --extensions=php --ignore=autoload.php --ignore=vendor/ app'
          }
        }
        stage('Phpstan') {
            steps {
                sh 'php7.1 /home/deploy/phars/phpstan.phar analyse app --level=0  >> `pwd`/build/logs/phpstan.xml || exit 0'
            }
        }
      }
    }
  }
  post {
    always {
      checkstyle(pattern: 'build/logs/*.xml', canRunOnFailed: true)
      junit 'build/logs/phpunit.xml'
      step([$class: 'CloverPublisher', cloverReportDir: 'build/coverage', cloverReportFileName: 'coverage.xml'])
      archiveArtifacts(artifacts: 'build/logs/**/*.xml', fingerprint: true)
      archiveArtifacts(artifacts: 'build/coverage/**/*.xml', fingerprint: true)
      cleanWs(cleanWhenAborted: true, cleanWhenFailure: true, cleanWhenNotBuilt: true, cleanWhenSuccess: true, cleanWhenUnstable: true, cleanupMatrixParent: true, deleteDirs: true)
    }

    success {
      slackSend(color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }

    failure {
      slackSend(color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
      mail(to: 'mchaka@cytonn.com', subject: "[Jenkins] [Failure] Failed Pipeline: ${currentBuild.fullDisplayName}", body: "Something is wrong with ${env.BUILD_URL}")
    }
  }
}