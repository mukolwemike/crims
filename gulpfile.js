const gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    elixir = require('laravel-elixir');


elixir(function(mix) {
    mix.version('public/assets/js/cytonn.min.js');
});


gulp.task('js', function()
{
    gulp.src('resources/assets/angular/app/**')
        .pipe(sourcemaps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(concat('cytonn.min.js').on('error', function(){}))
        .pipe(uglify().on('error', function(){}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public/assets/js/'));
});

gulp.task('dependencies', function()
{
    gulp.src('resources/assets/angular/dependencies/**')
        .pipe(sourcemaps.init())
        .pipe(concat('dependencies.min.js').on('error', function(){}))
        .pipe(uglify().on('error', function(){}))
        .pipe(gulp.dest('public/assets/js/'));
});

gulp.task('watch', function()
{
    gulp.watch('resources/assets/angular/app/**/*.js',['js'] );
    gulp.watch('resources/assets/angular/dependencies/**/*.js',['dependencies'] );
});



gulp.task('default', ['dependencies', 'js', 'watch']);
