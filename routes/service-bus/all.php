<?php
/**
 * Date: 08/06/2017
 * Time: 13:47
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

return [
    'commands' => [
        'servicebus_test' => \ServiceBus\Commands\Investments\Investments::class,
        'SyncDepartmentUnits' => \ServiceBus\Events\DepartmentUnits\SyncDepartmentUnits::class
    ],
    'queries' => [
        'servicebus_test' => \ServiceBus\Events\Investments::class,
        'FAEvaluationSummary' => \ServiceBus\Queries\FASummary\FAEvaluationSummary::class,
        'FAEvaluationSummaryExport' => \ServiceBus\Queries\FASummary\FAEvaluationSummaryExport::class,
        'IndividualFAEvaluationSummary' => \ServiceBus\Queries\FASummary\IndividualFAEvaluationSummary::class,
        'IndividualYtdInflowSummary' => \ServiceBus\Queries\FASummary\IndividualYtdInflowSummary::class,
        'IndividualYearClientsExport' => \ServiceBus\Queries\FASummary\IndividualYearClientsExport::class,
        'MonthlyProductionSummary' => \ServiceBus\Queries\FASummary\MonthlyProductionSummary::class,
        'IndividualMonthlyProduction' => \ServiceBus\Queries\FASummary\IndividualMonthlyProduction::class,
        'IndividualMonthProduction' => \ServiceBus\Queries\FASummary\IndividualMonthProduction::class,
        'FAMonthlyProduction' => \ServiceBus\Queries\FASummary\FAMonthlyProduction::class,
        'YearCmsSummary' => \ServiceBus\Queries\FASummary\YearCmsSummary::class,
        'FAMonthlyRetention' => \ServiceBus\Queries\FASummary\FAMonthlyRetention::class,
        'ClientComplianceReport' => \ServiceBus\Queries\ClientCompliance\ClientComplianceSummary::class,
        'GetCommissionRecipients' => \ServiceBus\Queries\Commissions\GetCommissionRecipient::class,
        'GetClientData' => \ServiceBus\Queries\Clients\GetClientData::class,
        'GetJointHolderData' => \ServiceBus\Queries\Clients\GetJointHolderData::class,
        'CompetitionSummary' => \ServiceBus\Queries\FASummary\CompetitionSummary::class,
        'DailyDistributionReport' => \ServiceBus\Queries\FASummary\DailyDistribution\DailyDistributionReport::class,
        'WeeklyDistributionReport' => \ServiceBus\Queries\FASummary\WeeklyDistribution\WeeklyDistributionReport::class,

        //Can Deleted once used
        'UpdateClientInteractions' => \ServiceBus\Queries\FASummary\UpdateClientInteractions::class,

        //BI
        'BI_ActiveClients' => \ServiceBus\Queries\BI\Investments\ActiveClients::class,
        'BI_AllClients' => \ServiceBus\Queries\BI\Investments\AllClients::class,
        'Products' => \ServiceBus\Queries\BI\Investments\Products::class,
        'Projects' => \ServiceBus\Queries\BI\RealEstate\Projects::class,
        'FundManagers' => \ServiceBus\Queries\BI\Investments\FundManagers::class,
        'InvestmentAnalytics' => \ServiceBus\Queries\BI\Investments\InvestmentsAnalytics::class,
        'ComplianceReport' => \ServiceBus\Queries\BI\Investments\ComplianceReport::class,
        'ProjectReport' => \ServiceBus\Queries\BI\RealEstate\ProjectReport::class,
        'BI_InvestmentFlowSummary' => \ServiceBus\Queries\BI\Investments\InvestmentFlowSummary::class,
        'BI_RealEstateFlowSummary' => \ServiceBus\Queries\BI\RealEstate\RealEstateFlowSummary::class,
        'BI_DepositSummary' => \App\ServiceBus\Queries\BI\Portfolio\DepositHoldingSummary::class,
        'BI_FaEvaluationSummary' => \App\ServiceBus\Queries\BI\Distribution\BIFaEvaluationSummary::class,

        //HR
        'FACommissionProjection' => \ServiceBus\Queries\HR\FAProjection::class,

        //Real Estate
        'GetProjects' => \ServiceBus\Queries\RealEstate\GetProjects::class,
        'GetOverduePayments' => \ServiceBus\Queries\RealEstate\GetOverduePayments::class,
        'GetUpcomingPayments' => \ServiceBus\Queries\RealEstate\GetUpcomingPayments::class,
        'GetScheduleDetails' => \ServiceBus\Queries\RealEstate\GetScheduleDetails::class,
        'GetProjectsSummary' => \App\ServiceBus\Queries\RealEstate\GetProjectsSummary::class,

        //Sales Admin
        'CumulativeSalesAdminActivitySummary' =>
            \ServiceBus\Events\SalesAdmin\CumulativeSalesAdminActivitySummary::class,

        //CAML
        'CAML_FundSummary' => \ServiceBus\Queries\CAML\CAMLSummary::class,
    ],
    'events' => [
        'servicebus_test' => \ServiceBus\Events\Investments::class,
        'SyncCommissionRecipient' => \ServiceBus\Events\CommissionRecipient\SyncCommissionRecipient::class,
        'SyncDepartmentUnits' => \ServiceBus\Events\DepartmentUnits\SyncDepartmentUnits::class,
        'FAEvaluationSummaryExportEvent' => \ServiceBus\Queries\FASummary\FAEvaluationSummaryExport::class,
        'IndividualFAEvaluationSummaryExport' => \ServiceBus\Queries\FASummary\IndividualFAEvaluationSummaryExport::class,
        'RTDContactsSummary' => \ServiceBus\Events\Distribution\RTD\RTDContactsSummary::class,
        'RTDRecruitmentSummary' => \ServiceBus\Events\Distribution\RTD\RTDRecruitmentSummary::class,
        'ConsolidatedBusinessMonthlyReport' => \ServiceBus\Events\Distribution\MonthlyReports\ConsolidatedBusinessMonthlyReport::class,
        'ConsolidatedRealEstateReport' => \ServiceBus\Events\Distribution\MonthlyReports\ConsolidatedRealEstateReport::class,
        'ClientInteractionsReport' => \ServiceBus\Events\Distribution\ClientInteractionsReport::class,

        //Sales Admin
        'WeeklySalesAdminPaymentsDue' => \ServiceBus\Events\SalesAdmin\WeeklySalesAdminPaymentsDue::class,
        'RealEstateSummary' => \ServiceBus\Events\SalesAdmin\RealEstateSummary::class,
        'DailySalesAdminActivitySummary' => \ServiceBus\Events\SalesAdmin\DailySalesAdminActivitySummary::class,
        'SalesAdminNextContactDateReport' => \ServiceBus\Events\SalesAdmin\NextContactDateSummary::class,

        //Payment System
        'payments_transaction_in' => \ServiceBus\Events\PaymentSystem\PaymentProcessor::class,
        'payments_balance_in' => \ServiceBus\Events\PaymentSystem\PaymentProcessor::class . '@handleBalance',
        'UtilityPaymentProcessor' => \ServiceBus\Events\PaymentSystem\UtilityPaymentProcessor::class
    ]
];
