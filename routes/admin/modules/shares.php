<?php
/**
 * Date: 24/05/2017
 * Time: 10:12
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

Route::group(['middleware' => 'allow:shares:applications'], function () {
    Route::get('/dashboard/shares/shareholders/register', 'ShareHolderController@getRegister');
    
    Route::post('/dashboard/shares/shareholders/register', [
        'as'=>'register_shareholder',
        'uses'=>'ShareHolderController@postRegister'
    ]);

    Route::post('/dashboard/shares/shareholders/add/joint-holder', [
        'as'=>'add_joint_holder',
        'uses'=>'ShareHolderController@addJointHolder'
    ]);
});

Route::group(['middleware' => 'allow:shares:view'], function () {
    Route::get('/dashboard/shares', 'SharesController@menu');

    Route::get('/dashboard/shares/entities/', 'SharesEntityController@getIndex');

    Route::get('dashboard/shares/purchases/', 'SharesPurchaseOrdersController@getIndex');
    Route::get('dashboard/shares/purchases/shareholders/{share_holder_id}', 'SharesPurchaseOrdersController@getShareholders');
    Route::get('dashboard/shares/purchases/show/{order_id}', 'SharesPurchaseOrdersController@getShow');

    Route::get('/dashboard/shares/payments/', 'SharesPaymentsController@getIndex');
    Route::get('/dashboard/shares/payments/shareholders/{share_holder_id}', 'SharesPaymentsController@getShareholders');

    Route::get('/dashboard/shares/entities/show/{entity_id}', 'SharesEntityController@getShow');

    Route::get('/dashboard/shares/shareholders/', 'ShareHolderController@getIndex');
    Route::get('/dashboard/shares/shareholders/show/{id}', 'ShareHolderController@getShow');
    Route::post('/dashboard/shares/shareholders/remove/{id}', 'ShareHolderController@removeShareHolder');

    Route::post('/dashboard/shares/shareholders/statement/{share_holder_id}', [
        'as' => 'view_shares_statement',
        'uses' => 'ShareHolderController@postStatement'
    ]);

    Route::get('dashboard/shares/sales/', 'SharesSalesOrdersController@getIndex');
    Route::get('dashboard/shares/sales/shareholders/{share_holder_id}', 'SharesSalesOrdersController@getShareholders');
    Route::get('dashboard/shares/sales/show/{order_id}', 'SharesSalesOrdersController@getShow');

    Route::get('dashboard/shares/redeem/shareholders/{share_holder_id}', 'ShareHolderController@getRedeem');

    Route::post('/dashboard/shares/redeem/shareholders/{share_holder_id}', [
        'as' => 'save_redeem_shares',
        'uses' => 'ShareHolderController@saveRedeemShares'
    ]);

    Route::get('/dashboard/shares/sales/notify-unmatched-orders/', 'SharesSalesOrdersController@notifyUnmatched');
});

Route::group(['middleware' => 'allow:shares:create'], function () {
    Route::post('/dashboard/shares/entities/create', [
            'as'=>'add_share_entity',
            'uses'=>'SharesEntityController@postShareEntity'
        ]);

    Route::post('/dashboard/shares/entities/edit/{entity_id}', [
            'as'=>'edit_share_entity',
            'uses'=>'SharesEntityController@editShareEntity'
        ]);

    Route::post('/dashboard/shares/entities/issue/{entity_id}', [
            'as'=>'make_shares_entity_issue',
            'uses'=>'SharesEntityController@postIssue'
        ]);

    Route::post('/dashboard/shares/entities/share-price-edit/{entity_id}', [
        'as'=>'edit_shares_price',
        'uses'=>'SharesEntityController@editSharePrice',
    ]);

    Route::post('/dashboard/shares/entities/buy-shares/{entity_id}', 'SharesEntityController@postBuyShares');

    Route::post('/dashboard/shares/shareholders/deposit/{share_holder_id}', 'ShareHolderController@postDeposit');

    Route::post('dashboard/shares/purchases/match/{order_id}', 'SharesPurchaseOrdersController@postMatch');

    Route::post(
        'dashboard/shares/purchases/order/{share_holder_id}',
        [
            'as'=>'make_shares_purchase_order',
            'uses'=>'SharesPurchaseOrdersController@postOrder'
        ]
    );
    Route::post(
        'dashboard/shares/purchases/buy-entity-shares/{share_holder_id}',
        [
            'as'=>'buy_entity_shares',
            'uses'=>'SharesPurchaseOrdersController@postBuyEntityShares'
        ]
    );
    Route::post(
        'dashboard/shares/purchases/cancel/{order_id}',
        [
            'as'=>'cancel_shares_purchase_order',
            'uses'=>'SharesPurchaseOrdersController@postCancel'
        ]
    );

    Route::post(
        'dashboard/shares/purchases/edit/{order_id}',
        [
            'as'=>'edit_shares_purchase_order',
            'uses'=>'SharesPurchaseOrdersController@postEdit'
        ]
    );
    Route::post(
        'dashboard/shares/purchases/reverse-from-entity/{order_id}',
        [
            'as'=>'reverse_shares_purchase_from_entity',
            'uses'=>'SharesPurchaseOrdersController@postReverseFromEntity'
        ]
    );

    Route::post(
        'dashboard/shares/sales/match/{order_id}',
        [
            'as'=>'match_sale_to_purchase',
            'uses'=>'SharesSalesOrdersController@postMatch'
        ]
    );

    Route::post('dashboard/shares/sales/order/{share_holder_id}', [
        'as'=>'make_shares_sales_order',
        'uses'=>'SharesSalesOrdersController@postOrder'
    ]);

    Route::post(
        'dashboard/shares/sales/cancel/{order_id}',
        [
            'as'=>'cancel_shares_sale_order',
            'uses' => 'SharesSalesOrdersController@postCancel'
        ]
    );

    Route::post('dashboard/shareholder/{id}/pay-duty', [
        'as' => 'pay_custom_duty',
        'uses' => 'ShareHolderController@payDuty'
    ]);
});


Route::group(['middleware' => 'allow:shares:send-client-documents'], function () {
    Route::post('/dashboard/shares/shareholders/mail-statement/{share_holder_id}', [
        'as' => 'mail_shares_statement',
        'uses' => 'ShareHolderController@postMailStatement'
    ]);

    Route::get('dashboard/shares/sales/confirmations/', 'SharesSaleOrderConfirmationsController@getIndex');
    Route::get('dashboard/shares/sales/confirmations/show/{sale_order_id}', 'SharesSaleOrderConfirmationsController@getShow');
    Route::post('dashboard/shares/sales/confirmations/send/{sale_order_id}', 'SharesSaleOrderConfirmationsController@postSend');
    Route::get('dashboard/shares/sales/confirmations/preview/{sale_order_id}', 'SharesSaleOrderConfirmationsController@getPreview');
    Route::get('dashboard/shares/sales/confirmations/previous-preview/{confirmation_id}', 'SharesSaleOrderConfirmationsController@getPreviousPreview');

    Route::get('/dashboard/shares/confirmations/', 'SharesBusinessConfirmationsController@getIndex');
    Route::get('/dashboard/shares/confirmations/show/{id}', 'SharesBusinessConfirmationsController@getShow');
    Route::post('/dashboard/shares/confirmations/send/{id}', 'SharesBusinessConfirmationsController@postSend');

    Route::get('dashboard/shares/purchases/confirmations/', 'SharesPurchaseOrderConfirmationsController@getIndex');
    Route::get('dashboard/shares/purchases/confirmations/show/{purchase_order_id}', 'SharesPurchaseOrderConfirmationsController@getShow');
    Route::post('dashboard/shares/purchases/confirmations/send/{purchase_order_id}', 'SharesPurchaseOrderConfirmationsController@postSend');
    Route::get('dashboard/shares/purchases/confirmations/preview/{purchase_order_id}', 'SharesPurchaseOrderConfirmationsController@getPreview');
    Route::get('dashboard/shares/purchases/confirmations/previous-preview/{confirmation_id}', 'SharesPurchaseOrderConfirmationsController@getPreviousPreview');

    Route::get('/dashboard/shares/purchase/{id}/acknowledgement/{type}', 'ShareHolderController@acknowledgement');
});

Route::group(['middleware' => 'allow:shares:statements'], function () {

    Route::post('/dashboard/shares/statements/campaigns/send/{campaignId}', [        'as' => 'send_shares_statement_campaign',        'uses' => 'SharesStatementsController@sendCampaign'    ]);

    Route::post('/dashboard/shares/statements/campaigns/send/{campaignId}', [
        'as' => 'send_shares_statement_campaign',
        'uses' => 'SharesStatementsController@sendCampaign'
    ]);

    Route::get('dashboard/shares/statements/', 'SharesStatementsController@getIndex');
    Route::post('dashboard/shares/statements/create-campaign', [
        'as'=> 'create_shares_statement_campaign',
        'uses'=>'SharesStatementsController@postCreateCampaign'
    ]);
    Route::get('dashboard/shares/statements/campaigns/{id}', 'SharesStatementsController@getCampaigns');
    Route::post('dashboard/shares/statements/add-share-holder-to-campaign', [
        'as' => 'add_shareholder_to_shares_statement_campaign',
        'uses' => 'SharesStatementsController@postAddShareHolderToCampaign'
    ]);

    Route::post('/dashboard/shares/statements/campaigns/close/{campaignId}', [
        'as' => 'close_shares_statement_campaign',
        'uses' => 'SharesStatementsController@closeCampaign'
    ]);

    Route::post('/dashboard/shares/statements/campaigns/close/{campaignId}', [
        'as' => 'close_shares_statement_campaign',
        'uses' => 'SharesStatementsController@closeCampaign'
    ]);

    Route::post('/dashboard/shares/statements/campaigns/campaign_message/{campaignId}', [
        'as' => 'store_shares_campaign_message',
        'uses' => 'SharesStatementsController@storeCampaignMessage'
    ]);
});

Route::group(['middleware' => 'allow:shares:reports'], function () {
    Route::get('/dashboard/shares/reports/', 'SharesReportsController@getIndex');

    Route::post(
        '/dashboard/shares/report/shareholdings-report',
        [
            'as'=>'export_shareholdings_report_path',
            'uses'=>'SharesReportsController@exportShareholdingsReport'
        ]
    );
});

Route::group(['middleware' => 'allow:shares:view-commissions'], function () {
    Route::get('/dashboard/shares/commissions', 'Shares\ShareCommissionController@index');

    Route::get('/dashboard/shares/commission/payment/{recipient_id}', 'Shares\ShareCommissionController@recipientDetails');

//    Route::get('/dashboard/realestate/commissions/for-unit/{unit_holding_id}', [
//        'as'=> 'commission_for_unit',
//        'uses'=>'RealEstateCommissionsController@getForUnit'
//    ]);
});

Route::group(['middleware' => 'allow:shares:create-commissions'], function () {
    Route::post('dashboard/shares/bulk_shares_commission_payments/{bulk_id}', [
        'as' => 'bulk_shares_commission_payments',
        'uses' => 'Shares\ShareCommissionController@sendBulkApproval'
    ]);

    Route::post('dashboard/shares/edit_shares_commission_payment_schedule', [
        'as' => 'edit_shares_commission_payment_schedule',
        'uses' => 'Shares\ShareCommissionController@editSchedule'
    ]);

    Route::post('dashboard/shares/add_shares_commission_payment_schedule', [
        'as' => 'add_shares_commission_payment_schedule',
        'uses' => 'Shares\ShareCommissionController@addSchedule'
    ]);
//    Route::get('/dashboard/realestate/commissions/', 'RealEstateCommissionsController@getIndex');
//
//    Route::get('/dashboard/realestate/commissions/for-unit/{unit_holding_id}', [
//        'as'=> 'commission_for_unit',
//        'uses'=>'RealEstateCommissionsController@getForUnit'
//    ]);
});
