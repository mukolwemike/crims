<?php
/**
 * Date: 24/05/2017
 * Time: 10:16
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

Route::group(['middleware' => 'allow:clients:menu'], function () {
    Route::get('/dashboard/clients/menu', 'ClientsController@menu');
});

Route::group(['middleware' => 'allow:clients:view'], function () {
    Route::get('dashboard/clients', 'ClientsController@index');

    Route::get('/dashboard/clients/bank-instructions/', 'ClientBankInstructionController@getIndex');
    Route::get('/dashboard/clients/bank-instructions/show/{id}', 'ClientBankInstructionController@getShow');
    Route::post('/dashboard/clients/bank-instructions/combine/{instr_id}', 'ClientBankInstructionController@postCombine');
    Route::get('/dashboard/clients/bank-instructions/dissolve/{instr_id}', 'ClientBankInstructionController@getDissolve');

    Route::get('/dashboard/clients/details/{id}', [
        'as' =>'view_client',
        'uses' => 'ClientsController@details'
    ]);

    Route::get('/dashboard/clients/joint/{id}', 'ClientsController@jointHolderDetails');

    Route::get('/dashboard/clients/summary', 'ClientsController@summary');

    Route::get('/dashboard/clients/iprs-file/{filename?}/view', 'ClientsController@iprsFilename');

    Route::get('/dashboard/clients/kyc/document/{id}', 'ClientInstructionController@filledApplicationDocuments');

    Route::get('/dashboard/clients/loyalty-points', 'ClientsController@loyaltyPointsIndex');

    Route::get('/dashboard/clients/loyalty-values', 'ClientsController@loyaltyPointsValuesIndex');
    
    Route::get('/dashboard/clients/loyalty-vouchers', 'ClientsController@loyaltyPointsVouchersIndex');
    
    Route::get('/dashboard/clients/loyalty-redeeming/{clientId}', 'ClientsController@loyaltyPointsRedeemIndex');
});

Route::group(['middleware' => 'allow:clients:risky_view'], function () {
    Route::get('dashboard/clients/risky', 'ClientsController@riskyClientsIndex');

    Route::get('dashboard/client/risky/show/{id}', 'ClientsController@riskyClientsShow');

    Route::get('/dashboard/clients/risky/risky-clients-report', 'ClientsController@exportRiskyClients');
});

Route::group(['middleware' => 'allow:clients:risky_create'], function () {
    Route::get('dashboard/clients/risky/create', 'ClientsController@riskyClientsCreate');

    Route::get('dashboard/client/risky/edit/{id}', 'ClientsController@riskyClientsEdit');

    Route::post('dashboard/client/risky/clearance', [
        'as' => 'clear_reason',
        'uses' => 'ClientsController@riskyClientsCleared'
    ]);

    Route::post('dashboard/client/risky/rejection', [
        'as' => 'reject_reason',
        'uses'=> 'ClientsController@riskyClientsRejected'
    ]);

    Route::post('dashboard/client/risky/delete', [
        'as' => 'delete_risk',
        'uses'=> 'ClientsController@riskyClientsDelete'
    ]);
});

Route::group(['middleware' => 'allow:clients:create'], function () {
    Route::get('dashboard/clients/create/{id?}', 'ClientsController@create');

    Route::post('dashboard/clients/create/{id}', [
        'as' => 'create_client',
        'uses' => 'ClientsController@store'
    ]);

    Route::delete('dashboard/clients/details/{id}', [
        'as'    =>  'delete_unused_client_account',
        'uses'  => 'ClientsController@delete'
    ]);

    Route::post('/dashboard/clients/details/{client_id}/indemnity/{id?}', [
        'as' => 'save_indemnity_email',
        'uses' => 'ClientsController@indemnity'
    ]);

    Route::post('/dashboard/clients/details/{client_id}/consent/{id?}', [
        'as' => 'save_consent_form',
        'uses' => 'ClientsController@consent'
    ]);

    Route::post('/dashboard/clients/details/consent/delete/{id}', [
        'as' => 'delete_consent_form',
        'uses' => 'ClientsController@deleteConsent'
    ]);

    Route::get('/dashboard/clients/details/{client_id}/indemnity/{id?}/{toggle}', 'ClientsController@toggleIndemnityStatus');

    Route::get('/dashboard/clients/joint/{id}/edit', 'ClientsController@editJointHolder');

    Route::post('/dashboard/clients/joint/delete/{id}', [
        'as' => 'delete_joint_holder',
        'uses' => 'ClientsController@deleteJointHolder'
    ]);

    Route::post('dashboard/clients/taxexemption/delete/{id}', [
        'as' => 'delete_tax_exemption',
        'uses' => 'ClientsController@deleteTaxExemption'
    ]);

    Route::post('/dashboard/clients/joint/{id}/edit', [
        'as' => 'edit_joint_holder_path',
        'uses' => 'ClientsController@saveJointHolder'
    ]);

    Route::post('/dashboard/clients/{id}/contact_person', [
        'as' => 'add_client_contact_person_path',

        'uses' => 'ClientsController@addContactPerson'
    ]);

    Route::get('/dashboard/clients/{id}/contact_person/{contact_id}/delete', [
        'as' => 'delete_client_contact_person_path',

        'uses' => 'ClientsController@deleteContactPerson'
    ]);
    
    Route::post('/dashboard/clients/{id}/emails', [
        'as' => 'add_client_emails_path',
        'uses' => 'ClientsController@addAlternativeEmails'
    ]);

    Route::post('/dashboard/clients/{id}/bank_details', [
        'as' => 'bank_details_add_path',
        'uses' => 'ClientsController@addBankDetails'
    ]);

    Route::post('/dashboard/clients/{id}/bank_details/default', [
        'as' => 'bank_set_default',
        'uses' => 'ClientsController@setDefaultAccount'
    ]);

    Route::get('/dashboard/clients/{id}/add-joint-holder', 'ClientsController@createJointHolders');

    Route::post('/dashboard/clients/{id}/joint-holders/add', [
        'as' => 'add_client_joint_holder',
        'uses' => 'ClientsController@addJointHolder'
    ]);

    Route::post('/dashboard/clients/{id}/bank_details/{acc_id}/delete', [
        'as' => 'bank_details_delete_path',
        'uses' => 'ClientsController@deleteBankDetails'
    ]);

    Route::post('/dashboard/clients/{id}/relationship_person', [
        'as' => 'add_relationship_person',
        'uses' => 'ClientsController@addRelationshipPerson'
    ]);

    Route::post('/dashboard/clients/{id}/incoming_fa_add', [
        'as' => 'add_incoming_fa',
        'uses' => 'ClientsController@addIncomingFa'
    ]);

    Route::post('/dashboard/clients/{id}/incoming_fa_edit', [
        'as' => 'edit_incoming_fa',
        'uses' => 'ClientsController@editIncomingFa'
    ]);

    Route::post('/dashboard/clients/update_signing_mandate/{id}', [
        'as' => 'clients.update_signing_mandate',
        'uses' => 'ClientsController@updateSigningMandate'
    ]);

    Route::post('/dashboard/clients/upload_client_signature/{id}', [
        'as' => 'clients.upload_client_signature',
        'uses' => 'ClientsController@uploadClientSignature'
    ]);

    Route::post('/dashboard/clients/{clientId}/guards/{id?}', [
        'as' => 'clients.store_guards',
        'uses' => 'ClientsController@storeClientGuards'
    ]);

    Route::post('/dashboard/clients/{clientId}/delete_guards/{id}', [
        'as' => 'clients.delete_guards',
        'uses' => 'ClientsController@deleteClientGuards'
    ]);

    Route::post('/dashboard/clients/combine_interest_reinvestment/activate/{id}', [
        'as' => 'activate_interest_reinvested_plan',
        'uses' => 'ClientsController@storeInterestReinvestmentPlan'
    ]);

    Route::post('/dashboard/clients/tenor/edit', [
        'as' => 'edit_client_tenor',
        'uses' => 'ClientsController@editTenor'
    ]);

    Route::post('/dashboard/client/signature/{id}/deactivate', [
        'as' => 'deactivate_client_signature',
        'uses' => 'ClientsController@deactivateSignature'
    ]);
});
