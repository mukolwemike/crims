<?php
/**
 * Date: 24/05/2017
 * Time: 10:18
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

Route::get('account/{username}/activate/{key}', 'RegistrationController@activate');

Route::post('account/{username}/activate/{key}', [
    'uses' => 'RegistrationController@createPassword'
]);

//login
Route::get('/auth/login', 'LoginController@create');
Route::get('/login', 'LoginController@create');
Route::get('/login_error', 'LoginController@loginError');

Route::get('/sso/authorize', [
    'as' => 'auth.sso',
    'uses' => 'LoginController@ssoAuthorize'
]);

Route::get('/confirm_login/{username}', [
    'as' => 'auth.confirm_login',
    'uses' => 'LoginController@confirmLogin'
]);

Route::post('/complete_login/{username}', [
    'as' => 'auth.complete_login',
    'uses' => 'LoginController@completeLogin'
]);

Route::get('/sso/complete_auth', [
    'as' => 'auth.sso',
    'uses' => 'LoginController@ssoCompleteAuth'
]);

Route::get('/sso/logout', [
    'as' => 'sso.logout',
    'uses' => 'LoginController@ssoLogout'
]);

Route::post('/auth/login', [
    'as' => 'login_path',
    'uses' => 'LoginController@store'
]);

Route::post('/login', 'LoginController@store');

Route::post('login/confirm/{username}', [

    'uses' => 'LoginController@confirm'
]);

Route::get('login/confirm/{username}', 'LoginController@confirm');

//request password reminder
Route::get('/password/remind', 'RemindersController@getRemind');

Route::post('/password/remind', [

    'uses' => 'RemindersController@postRemind'
]);

//password reset
Route::get('/password/reset/{username}/{token}', 'RemindersController@getReset');

Route::post('/password/reset/{username}/{token}', [

    'uses' => 'RemindersController@postReset'
]);

Route::get('random/{id?}', [
    'as' => 'exemptions.random',
    'uses'=>'ExemptionController@randomCommand'
]);
