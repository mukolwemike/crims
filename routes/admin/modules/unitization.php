<?php

Route::group(['before'=>'auth', 'namespace'=>'Unitization'], function () {
////    Route::resource('/dashboard/unitization/unit-funds', 'UnitFundController');
    Route::post('/dashboard/unitization/commission/payment/bulk/{id}', [
        'as'=>'bulk_unitization_commission_payments',
        'uses'=>'CommissionController@sendBulkPayments'
    ]);

    Route::post('/dashboard/unitization/commission/award/bulk/{id}', [
        'as'=>'award_unititization_commission',
        'uses'=>'CommissionController@awardCommission'
    ]);

    Route::get('/dashboard/unitization/commission/payment/{id}', [
        'as'=>'unititization_commission_payment',
        'uses'=>'CommissionController@recipientPayment'
    ]);

    Route::post('/dashboard/unitization/commission/award/bulk/{id}', [
        'as'=>'award_unititization_commission',
        'uses'=>'CommissionController@awardCommission'
    ]);

    Route::post('dashboard/unitization/edit_unitization_commission_payment_schedule', [
        'as' => 'edit_unitization_commission_payment_schedule',
        'uses' => 'CommissionController@editSchedule'
    ]);
});

Route::get('dashboard/unitization/bulk_interest_payment/store', [
    'as'=>'unit_fund_post_bulk_interest',
    'uses'=>'Api\Unitization\UnitFundInvestmentController@postBulkInterest'
]);

Route::get('/dashboard/unitization/{vue_capture?}', function () {
    if (! \Illuminate\Support\Facades\Auth::user()) {
        return redirect('/login');
    }
    return view('layouts.default');
})->where('vue_capture', '[\/\w\.-]*');
