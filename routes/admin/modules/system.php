<?php


Route::group(['middleware' => 'allow:system:administration'], function () {
    Route::get('/dashboard/system', 'SystemController@index');

    /*
     * Closed Periods
     */
    Route::get('/dashboard/system/closed_periods', [
        'as' => 'closed_periods',
        'uses'=>'ClosedPeriodController@index'
    ]);

    Route::get('/dashboard/system/closed_periods/create/{id?}', [
        'as' => 'closed_periods.create',
        'uses'=>'ClosedPeriodController@create'
    ]);

    Route::post('/dashboard/system/closed_periods/store/{id?}', [
        'as' => 'closed_periods.store',
        'uses'=>'ClosedPeriodController@store'
    ]);

    /*
     * Exemptions
     */
    Route::get('/dashboard/system/exemptions', [
        'as' => 'exemptions',
        'uses'=>'ExemptionController@index'
    ]);

    Route::get('/dashboard/system/random', [
        'as' => 'exemptions.random',
        'uses'=>'ExemptionController@randomCommand'
    ]);

    Route::get('/dashboard/system/exemptions/create/{id?}', [
        'as' => 'exemptions.create',
        'uses'=>'ExemptionController@create'
    ]);

    Route::post('/dashboard/system/exemptions/store/{id?}', [
        'as' => 'exemptions.store',
        'uses'=>'ExemptionController@store'
    ]);

    Route::get('/dashboard/system/approvals', 'System\ApprovalsController@index');

    Route::group(['middleware' => 'allow:user_management'], function () {
        Route::get('/dashboard/system/approvals/{stage_id}/approver/remove/{user_id}', [
           'as' => 'delink_transaction_stage_to_approver',
           'uses' => 'System\ApprovalsController@deleteApprover'
        ]);

        Route::get('/dashboard/system/portfolio_approvals/{stage_id}/approver/remove/{user_id}', [
           'as' => 'delink_portfolio_transaction_stage_to_approver',
           'uses' => 'System\ApprovalsController@deletePortfolioApprover'
        ]);

        Route::get('/dashboard/system/approvals/{stage_id}/transaction/remove/{transaction_type_id}', [
           'as' => 'remove_transaction_type_to_approval_stage',
           'uses' => 'System\ApprovalsController@deleteTransaction'
        ]);

        Route::get('/dashboard/system/portfolio_approvals/{stage_id}/transaction/remove/{transaction_type_id}', [
           'as' => 'remove_portfolio_transaction_type_to_approval_stage',
           'uses' => 'System\ApprovalsController@deletePortfolioTransaction'
        ]);

        Route::post('/dashboard/system/approvals/{stage_id}/approver', [
           'as' => 'link_transaction_stage_to_approver',
           'uses' => 'System\ApprovalsController@storeApprover'
        ]);

        Route::post('/dashboard/system/portfolio_approvals/{stage_id}/approver', [
           'as' => 'link_portfolio_transaction_stage_to_approver',
           'uses' => 'System\ApprovalsController@storePortfolioApprover'
        ]);

        Route::post('/dashboard/system/approvals/{stage_id}/transaction', [
           'as' => 'add_transaction_type_to_approval_stage',
           'uses' => 'System\ApprovalsController@storeTransaction'
        ]);

        Route::post('/dashboard/system/portfolio_approvals/{stage_id}/transaction', [
           'as' => 'add_porfolio_transaction_type_to_approval_stage',
           'uses' => 'System\ApprovalsController@storePortfolioTransaction'
        ]);
    });
});

Route::get('Dashboard/unitization/{vue_capture?}', function () {
    if (! \Illuminate\Support\Facades\Auth::user()) {
        return redirect('/login');
    }
    return view('layouts.default');
})->where('vue_capture', '[\/\w\.-]*');

Route::get('Dashboard/portfolio/securities/{vue_capture?}', function () {
    if (! \Illuminate\Support\Facades\Auth::user()) {
        return redirect('/login');
    }
    return view('layouts.default');
})->where('vue_capture', '[\/\w\.-]*');
