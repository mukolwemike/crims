<?php
/**
 * Date: 24/05/2017
 * Time: 10:11
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

Route::group(['middleware' => 'allow:investments:menu'], function () {
    Route::get('/dashboard/investments', 'InvestmentsController@index');
});

Route::group(['middleware' => 'allow:investments:view'], function () {
    Route::get('/dashboard/investments/interest', 'InvestmentsController@interestPayments');

    Route::get('/dashboard/investments/applications', 'InvestmentsController@applications');

    Route::get('/dashboard/investments/applications/{id}', 'InvestmentsController@applicationDetails');

    Route::post('/dashboard/investments/interest-payment-schedules/export', [
        'as' => 'export_bulk_approval_interest_payments',
        'uses' => 'ExportController@exportBulkInterestPaymentSchedulesUpForApproval'
    ]);

    Route::post('/dashboard/investments/commission/payment-dates/show/{id}/export', [
        'as' => 'export_combined_commissions',
        'uses' => 'ExportController@exportCombinedCommissions'
    ]);

    Route::get('/dashboard/investments/clientinvestments', 'InvestmentsController@investmentsGrid');

    Route::get('/dashboard/investments/unit-fund-investments', 'InvestmentsController@unitFundInvestments');

    Route::get('/dashboard/investments/clientinvestments/maturity', 'InvestmentsController@maturity');

    Route::post('/dashboard/investments/clientinvestments/maturity/bulk_rollover', [
        'as' => 'view_bulk_rollover',
        'uses' => 'InvestmentsController@viewBulkRolloverMaturities'
    ]);

    Route::get('/dashboard/investments/clientinvestments/deductions', 'InvestmentsController@deductionsgrid');

    Route::get('/dashboard/investments/clientinvestments/withdrawals', 'InvestmentsController@withdrawalsgrid');

    Route::get('/dashboard/investments/clientinvestments/scheduled', 'InvestmentsController@scheduledgrid');

    Route::any('/dashboard/investments/clientinvestments/maturity/analysis', 'InvestmentsController@maturityAnalysis');

    Route::post('/dashboard/investments/clientinvestments/maturity', [
        'uses' => 'InvestmentsController@maturity'
    ]);

    Route::get('/dashboard/investments/clientinvestments/maturity/{inv_id}', 'InvestmentsController@investmentMaturity');

    Route::get('/dashboard/investments/clientinvestments/interest/fullschedule', 'InvestmentsController@fullInterestSchedule');

    Route::get('/dashboard/investments/clientinvestments/{id}', 'InvestmentsController@investmentDetails');

    Route::get('/dashboard/investments/clientinvestments/schedule/{id}', 'InvestmentsController@investmentScheduleDetails');

    /**
     * Client Taxation
     */
    Route::get('/dashboard/investments/taxation', 'InvestmentsController@taxation');

    Route::get('/dashboard/investments/taxation/records', 'InvestmentsController@clientsTaxationRecords');

    Route::get('/dashboard/investments/taxation/{id}', 'InvestmentsController@taxationInvestmentsDetails');

    Route::get('/dashboard/investments/setup', 'InvestmentsController@setup');

    Route::get('/dashboard/investments/confirmation', 'InvestmentsController@confirmations');

    Route::get('/dashboard/investments/confirmation/{id}', 'InvestmentsController@confirmationDetails');

    Route::get('/dashboard/investments/confirmation/acknowledgement_notification/{id}', 'InvestmentsController@confirmationAcknowledgementNotification');

    Route::get('/dashboard/investments/confirm/preview/{id}', 'ReportsController@businessConfirmationFromInvestment');

    Route::get('/dashboard/investments/confirmation/previous/preview/{id}', 'ReportsController@businessConfirmationFromData');

    Route::get('/dashboard/shares/confirm/preview/{id}', 'ReportsController@sharesBusinessConfirmation');

    Route::get('/dashboard/shares/confirmations/previous/preview/{id}', 'ReportsController@sharesBusinessConfirmationFromData');

    //instructions
    Route::get('/dashboard/investments/instructions/{id}', 'InvestmentsController@instruction');

    Route::get('/dashboard/investments/instructions/{investment_id}/{instruction_id}', 'InvestmentsController@instructionSignatories');

    Route::post('/dashboard/investments/instructions/{investment_id}/{instruction_id}', [

        'uses' => 'InvestmentsController@generateInstruction'
    ]);

    Route::get('/dashboard/investments/interest-rates/', 'InterestRateUpdatesController@getIndex');
    Route::get('/dashboard/investments/interest-rates/show/{id}', 'InterestRateUpdatesController@getShow');
    Route::get('/dashboard/investments/interest-rates/rates/{id}', 'InterestRateUpdatesController@getRates');

    Route::get('/dashboard/coop/membership/confirmations/', 'CoopMembershipConfirmationController@getIndex');
    Route::get('/dashboard/coop/membership/confirmations/show/{id}', 'CoopMembershipConfirmationController@getShow');
    Route::post('/dashboard/coop/membership/confirmations/show/{id}', 'CoopMembershipConfirmationController@postShow');
    Route::get('/dashboard/coop/membership/confirmations/replay/{id}', 'CoopMembershipConfirmationController@getReplay');

    Route::get('/dashboard/investments/commission/{id}', 'InvestmentsController@commission');

    Route::get('/dashboard/investments/reports/{id}', 'CustomReportController@show');

    Route::get('/dashboard/investments/investment_payment_schedules/{id}', [
        'as' => 'investment_payment_schedules',
        'uses' => 'Investments\InvestmentPaymentScheduleController@index'
    ]);

    Route::get('/dashboard/investments/investment_payment_schedules/details/{id}', [
        'as' => 'investment_payment_schedules.details',
        'uses' => 'Investments\InvestmentPaymentScheduleController@details'
    ]);

    Route::get('/dashboard/investments/investment_payment_schedules/preview/{id}', [
        'as' => 'investment_payment_schedules.preview',
        'uses' => 'Investments\InvestmentPaymentScheduleController@previewPaymentSchedule'
    ]);

    Route::get('/dashboard/investments/clientinvestments/investment-payment-schedules/listing', [
        'as' => 'investment_payment_schedules.listing',
        'uses' => 'Investments\InvestmentPaymentScheduleController@listing'
    ]);
});

Route::group(['middleware' => 'allow:investments:create'], function () {
    Route::post('/dashboard/investments/interest', [
        'as' => 'send_bulk_interest_payments',
        'uses' => 'InvestmentsController@sendBulkForApproval'
    ]);

    Route::post('/dashboard/investments/bulk_rollover_maturities', [
        'as' => 'bulk_rollover_maturities',
        'uses' => 'InvestmentsController@postBulkRolloverMaturities'
    ]);

    Route::get('/dashboard/investments/applications/{id}/joint/{jid?}', 'InvestmentApplicationController@addJointApplicant');

    Route::post('/dashboard/investments/applications/{id}/joint/{jid?}', [
        'uses' => 'InvestmentApplicationController@saveJointApplicant'
    ]);

    Route::post('/dashboard/investments/applications/{id}/joint/{jid?}', [
        'uses' => 'InvestmentApplicationController@saveJointApplicant'
    ]);

    //edit joint holder
    Route::post('/dashboard/investments/applications/{id}/joint/{jid}/edit', [
        'as' => 'edit_joint_holder',
        'uses' => 'ClientInstructionController@editJointHolder'
    ]);

    Route::post('/dashboard/investments/applications/{id}/email', [
        'as' => 'add_additional_email_path',
        'uses' => 'InvestmentApplicationController@addAdditionalEmails'
    ]);
    Route::post('/dashboard/investments/applications/{id}/email', [
        'as' => 'add_additional_email_path',
        'uses' => 'InvestmentApplicationController@addAdditionalEmails'
    ]);

    Route::post('/dashboard/investments/applications/{id}/contact', [
        'as' => 'add_contact_person_path',
        'uses' => 'InvestmentApplicationController@addContactPerson'
    ]);

    Route::get('/dashboard/investments/applications/{id}/request', 'InvestmentApplicationController@sendApprovalRequest');

    Route::get('/dashboard/investments/applications/edit/{id}', 'InvestmentApplicationController@editApplicationDetails');

    Route::post('/dashboard/investments/applications/edit/{id}', [
        'as' => 'edit_investment_application_path',
        'uses' => 'InvestmentApplicationController@editClientApplication'
    ]);

    Route::post('/dashboard/investments/applications/{id}/delete', [
        'as' => 'delete_application_path',
        'uses' => 'InvestmentApplicationController@delete'
    ]);

    Route::get('/dashboard/investments/clientinvestments/maturity/send/{inv_id}', 'InvestmentsController@sendInvestmentMaturity');

    Route::post('/dashboard/investments/clientinvestments/maturity/send', [
        'as' => 'send_maturity_notifications_route',
        'uses' => 'InvestmentsController@sendMaturityNotifications'
    ]);

    Route::post('/dashboard/investments/clientinvestments/{id}/bank', [
        'as' => 'associate_bank_account_path',
        'uses' => 'InvestmentsController@associateAccount'
    ]);

    Route::get('/dashboard/investments/clientinvestments/schedule/cancel/{id}', 'InvestmentsController@cancelInvestmentSchedule');

    Route::get('/dashboard/investments/withdraw/{id}', [
        'as' => 'withdraw_path',
        'uses' => 'Investments\WithdrawalController@create'
    ]);

    Route::post('/dashboard/investments/withdraw/{id}', [
        'as' => 'withdraw_path',
        'uses' => 'Investments\WithdrawalController@store'
    ]);

    Route::get('/dashboard/investments/rollover/{id}', 'InvestmentActionController@rollover');

    Route::get('/dashboard/investments/roll-over/{id}', 'InvestmentActionController@rolloverInvestment');

    Route::post('/dashboard/investments/rollover', [
        'as' => 'rollover_path',
        'uses' => 'InvestmentActionController@saveRollover'
    ]);

    Route::get('/dashboard/investments/combinedrollover/{id}', 'InvestmentActionController@combinedRollover');

    Route::post('/dashboard/investments/combinedrollover/{id}', [
        'uses' => 'InvestmentActionController@combineSelectedRollover'
    ]);

    Route::post('/dashboard/investments/postcombinedrollover/{id}', [
        'as' => 'combined_rollover_path',
        'uses' => 'InvestmentActionController@saveCombineSelectedRollover'
    ]);


    Route::get('/dashboard/investments/topup/{id}', 'InvestmentActionController@topup');

    Route::post('/dashboard/investments/topup', [
        'as' => 'topup_path',
        'uses' => 'InvestmentActionController@saveTopup'
    ]);

    Route::post('/dashboard/investments/rollback/{id}', [
        'as' => 'rollback_path',
        'uses' => 'InvestmentActionController@rollback'
    ]);

    Route::post('/dashboard/investments/withdraw/{id}/reverse', [
        'as' => 'reverse_withdraw',
        'uses' => 'InvestmentActionController@reverseWithdraw'
    ]);

    Route::post('/dashboard/investments/orders/topup/{id}', [
        'as' => 'investment_topup_order',
        'uses' => 'Investments\Orders\ClientOrdersController@topup'
    ]);

    Route::post('/dashboard/investments/orders/withdraw/{id}', [
        'as' => 'investment_withdraw_order',
        'uses' => 'Investments\Orders\ClientOrdersController@withdraw'
    ]);

    Route::post('/dashboard/investments/orders/rollover/{id}', [
        'as' => 'investment_rollover_order',
        'uses' => 'Investments\Orders\ClientOrdersController@rollover'
    ]);

    /*
     * Interest payment
     */
    Route::get('/dashboard/investments/pay/{id}', 'InvestmentsController@pay');

    Route::post('/dashboard/investments/pay/{id}', [
        'uses' => 'InvestmentsController@postPay'
    ]);

    Route::post('/dashboard/investments/add-interest-payment-schedule/{id}', [
        'as' => 'add_interest_payment_schedule',
        'uses' => 'InvestmentsController@postAddSchedule'
    ]);

    Route::post('/dashboard/investments/update-interest-payment-schedule/{id}', [
        'as' => 'update_interest_payment_schedule',
        'uses' => 'InvestmentsController@putUpdateSchedule'
    ]);

    Route::get('/dashboard/investments/payment/reverse/{payment_id}', 'InvestmentsController@reversePayment');

    Route::get('/dashboard/investments/interest/schedule', 'InvestmentsController@monthlySchedule');

    Route::get('/dashboard/investments/interest/schedule/{id}', 'InvestmentsController@recipientSchedule');

    /*
     * Deductions
     */
    Route::get('/dashboard/investments/deductions/{id}', 'InvestmentDeductionsController@index');

    Route::post('/dashboard/investments/deductions/{inv_id}', [
        'as' => 'deduct_investment',
        'uses' => 'InvestmentDeductionsController@create'
    ]);

    /*
     * Edit investments
     */

    Route::get('/dashboard/investments/edit/{id}', 'InvestmentActionController@editInvestment');

    Route::post('/dashboard/investments/edit', [
        'as' => 'edit_investment_path',
        'uses' => 'InvestmentActionController@postEditInvestment'
    ]);

    Route::get('/dashboard/investments/transactions/schedule/{id}', 'InvestmentsController@scheduleTransaction');

    Route::post('/dashboard/investments/rollover/schedule', [
        'as' => 'rollover_schedule_path',
        'uses' => 'InvestmentsController@scheduleInvestmentRollover'
    ]);

    Route::post('/dashboard/investments/withdraw/schedule', [
        'as' => 'withdraw_schedule_path',
        'uses' => 'InvestmentsController@scheduleInvestmentWithdrawal'
    ]);


    Route::post('/dashboard/investments/confirmation/{id}', [

        'uses' => 'InvestmentsController@saveConfirmationForApproval'
    ]);


    Route::post(
        '/dashboard/investments/interest-rates/save-update/{id?}',
        [
            'as' => 'save_interest_rate_update',
            'uses' => 'InterestRateUpdatesController@postSaveUpdate'
        ]
    );

    Route::post('/dashboard/investments/interest-rates/save-rates/{id}', 'InterestRateUpdatesController@postSaveRates');

    //transfer
    Route::get('/dashboard/investments/transfer/{investment_id}', 'InvestmentActionController@transfer');

    Route::post('/dashboard/investments/transfer/{investment_id}', 'InvestmentActionController@postTransfer');

    Route::post('/dashboard/investments/investment_payment_schedules/process/{id}', [
        'as' => 'process_investment_payment_schedule',
        'uses' => 'Investments\InvestmentPaymentScheduleController@process'
    ]);

    Route::post('/dashboard/investments/investment_payment_schedules_details/update/{id}', [
        'as' => 'investment-payment-schedule-details.update',
        'uses' => 'Investments\InvestmentPaymentScheduleController@updateScheduleDetail'
    ]);

    Route::post('/dashboard/investments/investment_payment_schedules_details/reminders/send-reminders', [
        'as' => 'investment-payment-schedule-details.send-reminders',
        'uses' => 'Investments\InvestmentPaymentScheduleController@sendBulkReminders'
    ]);

    Route::post('/dashboard/investments/investment_payment_schedules_details/extend-plan/{id}', [
        'as' => 'investment-payment-schedule-details.extend-plan',
        'uses' => 'Investments\InvestmentPaymentScheduleController@extendPlan'
    ]);

    Route::post('/dashboard/investments/investment_payment_schedules_details/send-contract-termination/{id}', [
        'as' => 'investment-payment-schedule-details.send-contract-termination',
        'uses' => 'Investments\InvestmentPaymentScheduleController@sendContractTermination'
    ]);
});

Route::group(['middleware' => 'allow:investments:view-commission'], function () {
    Route::get('/dashboard/investments/commission', 'CommissionController@index');

    Route::get('/dashboard/investment/commission_recipients/list', 'CommissionController@viewCommissionRecipients');

    Route::get('/dashboard/investment/commission_recipients/details/{id}', 'CommissionController@viewCommissionRecipientPositions');

    Route::get('/dashboard/investments/commission/payment-dates/list', 'CommissionController@getPaymentDates');

    Route::get('/dashboard/investments/commission/payment-dates/show/{id}', 'CommissionController@showPaymentDate');

    Route::get('/dashboard/investments/commission/owner/{id}', 'CommissionController@recipient');

    Route::get('/dashboard/investments/commission/payment/{recipient_id}', 'CommissionController@payments');

    Route::post('dashboard/investments/commission/pension_commission/export/{id}', [
        'as' => 'pension_commission.export',
        'uses' => 'CommissionController@exportPensionCommission'
    ]);
});

Route::group(['middleware' => 'allow:investments:create-commission'], function () {
    Route::post('/dashboard/investments/commission/payment-dates/create', [
        'as' => 'save_commission_payment_date',
        'uses' => 'CommissionController@postSavePaymentDate'
    ]);

    Route::get('dashboard/investment/commission/add/{id?}', 'CommissionController@addRecepient');

    Route::post('/dashboard/investments/commission/add/{id?}', [
        'uses' => 'CommissionController@saveRecepient'
    ]);

    Route::post('/dashboard/investments/commission/payment/bulk/{id}', [
        'as' => 'bulk_commission_payment',
        'uses' => 'CommissionController@bulkPayments'
    ]);

    Route::post('/dashboard/investments/commission/payment/combined_bulk/{id}', [
        'as' => 'combined_bulk_commission_payment',
        'uses' => 'CommissionController@combinedBulkPayments'
    ]);

    Route::post('dashboard/investments/commission/recipient/{recipient_id}/add-schedule', [
        'as' => 'add_commission_payment_schedule',
        'uses' => 'CommissionController@addCommissionPaymentSchedule'
    ]);

    Route::post('dashboard/investments/commission/edit-schedule/{schedule_id}', [
        'as' => 'edit_commission_payment_schedule',
        'uses' => 'CommissionController@editCommissionPaymentSchedule'
    ]);

    Route::post('dashboard/investments/commission/stagger-clawbacks/{commission_id}', [
        'as' => 'stagger_commission_clawbacks',
        'uses' => 'CommissionController@staggerCommissionClawbacks'
    ]);

    Route::post('dashboard/investments/commission/recipient/{recipient_id}/add-clawback', [
        'as' => 'add_commission_clawback',
        'uses' => 'CommissionController@addCommissionClawback'
    ]);

    Route::put('dashboard/investments/commission/recipient/{recipient_id}/update-clawback', [
        'as' => 'update_commission_clawback',
        'uses' => 'CommissionController@updateCommissionClawback'
    ]);

    Route::delete('dashboard/investments/commission/recipient/{recipient_id}/delete-clawback', [
        'as' => 'delete_commission_clawback',
        'uses' => 'CommissionController@deleteCommissionClawback'
    ]);

    Route::post('dashboard/investments/commission/recipient/type/add', [
        'as' => 'fa_type_add',
        'uses' => 'CommissionController@addNewFaType'
    ]);

    Route::post('dashboard/investments/commission/additional_commission/store/{id?}', [
        'as' => 'store_additional_commission',
        'uses' => 'CommissionController@storeAdditionalCommission'
    ]);
});

Route::group(['middleware' => 'allow:investments:view-analytics'], function () {
    Route::get('/dashboard/investments/summary', 'InvestmentSummaryController@summary');

    Route::get('/dashboard/investments/summary/statements', 'InvestmentSummaryController@statements');

    Route::get('/dashboard/investments/summary/expenses', 'InvestmentSummaryController@expenses');

    Route::post('/dashboard/investments/summary/expenses', 'InvestmentSummaryController@expenses');

    Route::get('/dashboard/investments/summary/history', 'InvestmentSummaryController@history');

    Route::get('/dashboard/investments/summary/analytics', 'InvestmentSummaryController@analytics');

    Route::get('/dashboard/investments/summary/client/{id}', 'InvestmentSummaryController@clientSummary');
});

Route::group(['middleware' => 'allow:investments:view-statements'], function () {
    Route::get('/dashboard/investments/statements', 'InvestmentSummaryController@statements');

    Route::get('/report/statements/client/{product_id}/{id}', 'ReportsController@showClientStatement');

    Route::post('/report/statements/client/{product_id}/{id}', [
        'as' => 'show_statement',
        'uses' => 'ReportsController@showClientStatement'
    ]);

    Route::get('/dashboard/investments/reports/statements', 'ReportsController@batches');

    Route::get('/dashboard/investments/reports/statements/{id}', 'ReportsController@showBatchStatus');
});

Route::group(['middleware' => 'allow:investments:send-statements'], function () {
    Route::post('/dashboard/investments/statements/campaign/create/{id?}', [
        'as' => 'create_statement_campaign',
        'uses' => 'ClientStatementController@createCampaign'
    ]);

    Route::get('/dashboard/investments/statements/campaign/detail/{id}', 'ClientStatementController@showCampaign');

    Route::post('/dashboard/investments/statements/campaign/add_client', [
        'as' => 'add_client_to_statement_campaign',
        'uses' => 'ClientStatementController@addClientToCampaign'
    ]);

    Route::get('/dashboard/investments/statements/campaign/remove_client/{id}', [
        'as' => 'remove_client_from_statement_campaign',
        'uses' => 'ClientStatementController@removeClientFromCampaign'
    ]);

    Route::post('/dashboard/investments/statements/campaign/send/{campaignId}', [
        'as' => 'send_statement_campaign',
        'uses' => 'ClientStatementController@sendCampaign'
    ]);

    Route::post('/dashboard/investments/statements/campaign/campaign_message/{campaignId}', [
        'as' => 'store_campaign_message',
        'uses' => 'ClientStatementController@storeCampaignMessage'
    ]);

    Route::post('/dashboard/investments/statements/campaign/close/{campaignId}', [
        'as' => 'close_statement_campaign',
        'uses' => 'ClientStatementController@closeCampaign'
    ]);

    Route::post('/dashboard/investments/statement/addrequest', [
        'uses' => 'ReportsController@addStatementSendRequest'
    ]);

    Route::get('/dashboard/investments/statement/approverequest/{id}', 'ReportsController@approveStatementRequest')
        ->middleware('allow:investments:approve-statement-campaign');

    //reports
    Route::post('/report/statements/send/{client_id}', [
        'as' => 'send_individual_statement',

        'uses' => 'ReportsController@sendIndividualStatement'
    ]);
});

Route::group(['middleware' => 'allow:investments:view-compliance'], function () {
    /**
     * Applications compliance
     */
    Route::get('/dashboard/investments/compliance', 'InvestmentsComplianceController@index');
    Route::post('/dashboard/investments/compliance/export', [
        'as' => 'export_client_compliance',
        'uses' => 'InvestmentsComplianceController@export'
    ]);

    Route::get('/dashboard/investments/compliance/graph', 'InvestmentsComplianceController@graph');

    Route::get('/dashboard/investments/compliance/{id}', 'InvestmentsComplianceController@details');

    Route::post('/dashboard/investments/compliance/{id}', 'InvestmentsComplianceController@uploadKYCs');
});

Route::group(['middleware' => 'allow:investments:create-compliance'], function () {
    Route::post('/dashboard/investments/compliance-form', [
        'as' => 'upload_compliance_form',
        'uses' => 'InvestmentsComplianceController@uploadComplianceForm'
    ]);

    Route::get('/dashboard/investments/kyc/{id}', 'InvestmentsComplianceController@viewKYC');

    Route::post('/dashboard/investments/applications/compliance', [

        'as' => 'compliance_route',
        'uses' => 'InvestmentsComplianceController@approveCompliance'
    ]);
});


Route::group(['middleware' => 'allow:investments:view-applications'], function () {
});

Route::group(['middleware' => 'allow:investments:create-applications'], function () {
    Route::get('/dashboard/applications', function () {
        return view('investment.applications');
    });
    /**
     * Invest application
     */
    Route::get(
        '/dashboard/investments/applications/investment/{id}',
        [
            'as' => 'invest_application',
            'uses' => 'InvestmentActionController@investApplication'
        ]
    );

    Route::post('/dashboard/investments/applications/investment', [
        'as' => 'invest_application',
        'uses' => 'InvestmentActionController@saveInvestApplication'
    ]);

    //Applications
    Route::get('/dashboard/investments/apply/{type}/{id?}', 'InvestmentApplicationController@apply');

    Route::post('/dashboard/investments/apply/{type}/{id?}', [

        'uses' => 'InvestmentApplicationController@postApply'
    ]);

    /**
     * Cooperative
     */
//    Route::post('/dashboard/coop/clients/store', 'CoopClientsController@store');
    Route::get('/dashboard/coop/clients/', 'CoopClientsController@getIndex');
    Route::get('/dashboard/coop/clients/payments/{id}', 'CoopClientsController@getPayments');
    Route::get('/dashboard/coop/clients/apply', 'CoopClientsController@getApply');

    Route::post('/dashboard/coop/clients/store/{id?}', [
        'as' => 'store_coop_client',
        'uses' => 'CoopClientsController@postStore'
    ]);

    Route::get('/dashboard/coop/clients/edit/{id}', 'CoopClientsController@getEdit');


    Route::get('/dashboard/coop/memberships/', 'CoopMembershipFormsController@getIndex');
    Route::get('/dashboard/coop/memberships/edit/{id}', 'CoopMembershipFormsController@getEdit');
    Route::put('/dashboard/coop/memberships/update/{id}', [
        'as' => 'update_coop_membership',
        'uses' => 'CoopMembershipFormsController@putUpdate'
    ]);


    Route::get('/dashboard/coop/payments/', 'CoopPaymentsController@getIndex');
    Route::post('/dashboard/coop/payments/make-payment', 'CoopPaymentsController@postMakePayment');
    Route::post('/dashboard/coop/payments/investment-in-product', [
        'as' => 'invest_in_coop_product',
        'uses' => 'CoopPaymentsController@postInvestInProduct'
    ]);
    Route::post('/dashboard/coop/payments/topup-investment', [
        'as' => 'top_up_coop_investment',
        'uses' => 'CoopPaymentsController@postTopupInvestment'
    ]);
    Route::post('/dashboard/coop/payments/buy-shares', [
        'as' => 'buy_coop_shares',
        'uses' => 'CoopPaymentsController@postBuyShares'
    ]);


    Route::post('/dashboard/coop/productplans/add-product-plan', [
        'as' => 'add_coop_product_plan',
        'uses' => 'CoopProductPlansController@postAddProductPlan'
    ]);

    Route::put('/dashboard/coop/productplans/edit-product-plan/{id}', [
        'as' => 'edit_coop_product_plan',
        'uses' => 'CoopProductPlansController@putEditProductPlan'
    ]);

    Route::delete('/dashboard/coop/productplans/remove-product-plan/{id}', [
        'as' => 'delete_coop_product_plan',
        'uses' => 'CoopProductPlansController@deleteRemoveProductPlan'
    ]);

    Route::put('/dashboard/coop/productplans/terminate-product-plan/{id}', [
        'as' => 'terminate_coop_product_plan',
        'uses' => 'CoopProductPlansController@putTerminateProductPlan'
    ]);
});


Route::group(['middleware' => 'allow:client-approvals:approve'], function () {
    Route::post('/dashboard/investments/approve/{id}', [
        'uses' => 'ApprovalsController@save'
    ]);

    Route::post('/dashboard/investments/approve/{id}/reject/{stage_id}', [
        'as' => 'reject_client_transaction',
        'uses' => 'ApprovalsController@reject'
    ]);

    Route::get('/dashboard/investments/approve/{id}/stage/{stage_id}', 'ApprovalsController@approve');

    Route::get('/dashboard/investments/approve/{id}/resolve/{rejection_id}', 'ApprovalsController@resolve');

    Route::get('/dashboard/investments/approvalremove/{id}', 'ApprovalsController@delete');
});

Route::group(['middleware' => 'allow:client-approvals:view'], function () {
    //approvals
    Route::get('/dashboard/investments/approve', 'ApprovalsController@index');

    Route::get('/dashboard/investments/approve/{id}', [
        'as' => 'show_client_transaction_approval',
        'uses' => 'ApprovalsController@show'
    ]);

    Route::any('/dashboard/investments/activity', 'ActivityController@index');

    Route::get('/dashboard/investments/approve/{id}/action/{action}/{param?}', [
        'as' => 'approval_action',
        'uses' => 'ApprovalsController@action'
    ]);

    Route::post('/dashboard/investments/approvalpostaction/{id}', [
        'as' => 'approval_post_action',
        'uses' => 'ApprovalsController@postAction'
    ]);

    Route::get('/dashboard/investments/approvals/batches', 'ApprovalsController@batches');

    Route::get('/dashboard/investments/approvals/batch/{stage_id}/{batch_id}', 'ApprovalsController@batch');
});

Route::group(['middleware' => 'allow:investments:view_reports'], function () {
    Route::get('/dashboard/investments/reports/', 'InvestmentReportsController@getIndex');
});

Route::group(['middleware' => 'allow:investments:reports'], function () {
    //excel
    Route::get('/dashboard/investments/excel/{id}', 'ExportController@exportClientData');

    Route::post('/dashboard/investments/client-summary-export/{id}', [
        'as' => 'export_client_summry_report',
        'uses' => 'ExportController@exportClientDataAtDate'
    ]);

    Route::get('/dashboard/investments/statements/excel', 'ExportController@exportStatements');

    Route::post('/dashboard/investments/clientinvestments/deductions/excel', [
        'as' => 'export_deductions_to_excel',
        'uses' => 'ExportController@exportClientDeductions'
    ]);

    Route::post('/dashboard/investments/clientinvestments/withdrawals/excel', [
        'as' => 'export_withdrawals_to_excel',
        'uses' => 'ExportController@exportClientWithdrawals'
    ]);

    Route::post('/dashboard/investments/reports', [
        'as' => 'get_reports_path',
        'uses' => 'InvestmentReportsController@exportClientContacts'
    ]);

    Route::post('/dashboard/investments/reports/export_client_profiling_report', [
        'as' => 'export_client_profiling_report',
        'uses' => 'InvestmentReportsController@exportClientProfiling'
    ]);

    //Route::get('/dashboard/investments/report/year_statements',
    //    [
    //        'as'=>'export_interest_year_statements',
    //        'uses'=>'InvestmentReportsController@exportStatement'
    //    ]);

    Route::post(
        '/dashboard/investments/report/closing-balances-for-partners',
        [
            'as' => 'export_closing_balances_for_partners_path',
            'uses' => 'InvestmentReportsController@exportClosingBalancesForPartners'
        ]
    );


    Route::get('/dashboard/investments/reports/clients-summaries', 'InvestmentReportsController@getClientsSummaries');

    Route::post('/dashboard/investments/reports/inv/clients-summaries', [
        'as' => 'inv_client_summary_report',
        'uses' => 'InvestmentReportsController@postClientsSummaries'
    ]);

    Route::post('/dashboard/investments/reports/untaxed-clients', [
        'as' => 'export_untaxed_clients_report',
        'uses' => 'InvestmentReportsController@exportUntaxedClients'
    ]);

    Route::post('/dashboard/investments/reports/daily-business-confirmation', [
        'as' => 'daily_business_confirmations',
        'uses' => 'InvestmentReportsController@exportDailyBusinessConfirmations'
    ]);

    Route::post('/dashboard/investments/reports/inactive-cms-clients', [
        'as' => 'export_inactive_cms_clients_report',
        'uses' => 'InvestmentReportsController@exportInactiveCMSClients'
    ]);

    Route::post('/dashboard/investments/reports/active-clients-export', [
        'as' => 'export_active_clients_report',
        'uses' => 'InvestmentReportsController@exportActiveClientsReport'
    ]);

    Route::post(
        '/dashboard/investments/report',
        [
            'as' => 'export_investment_path',
            'uses' => 'InvestmentReportsController@exportInvestments'
        ]
    );

    Route::post(
        '/dashboard/investments/report/interest-expense',
        [
            'as' => 'export_interest_expense_path',
            'uses' => 'InvestmentReportsController@exportInterestExpense'
        ]
    );

    Route::post(
        '/dashboard/investments/report/closing-balances-for-partners',
        [
            'as' => 'export_closing_balances_for_partners_path',
            'uses' => 'InvestmentReportsController@exportClosingBalancesForPartners'
        ]
    );

    Route::post('/dashboard/investments/residual-income-report', [
        'as' => 'export_residual_income_path',
        'uses' => 'InvestmentReportsController@exportResidualIncome'
    ]);

    Route::post('/dashboard/investments/coop-clients-report', [
        'as' => 'export_coop_clients_path',
        'uses' => 'InvestmentReportsController@exportCoopClients'
    ]);

    Route::post('/dashboard/investments/withholding-tax-report', [
        'as' => 'export_investments_withholding_tax_path',
        'uses' => 'InvestmentReportsController@exportWithholdingTax'
    ]);

    Route::post('/dashboard/investments/client-withholding-tax-report', [
        'as' => 'export_withholding_report',
        'uses' => 'InvestmentReportsController@exportWithholdingTaxReport'
    ]);

    Route::post('/dashboard/investments/export_withholding_itax_report', [
        'as' => 'export_withholding_itax_report',
        'uses' => 'InvestmentReportsController@exportWithholdingTaxItaxReport'
    ]);

    Route::post('/dashboard/investments/client-investments-churn', [
        'as' => 'export_client_investments_churn_path',
        'uses' => 'InvestmentReportsController@exportInvestmentsChurn'
    ]);

    Route::post('/dashboard/investments/fa-clients', [
        'as' => 'export_fa_clients_path',
        'uses' => 'InvestmentReportsController@exportFAClients'
    ]);

    Route::post('/dashboard/investments/distribution-structure', [
        'as' => 'export_distribution_structure_path',
        'uses' => 'InvestmentReportsController@exportDistributionStructure'
    ]);

    Route::post('/dashboard/investments/bank-instructions', [
        'as' => 'export_bank_instructions_path',
        'uses' => 'InvestmentReportsController@exportBankInstructions'
    ]);

    Route::post('/dashboard/investments/penalty-deductions', [
        'as' => 'export_penalty_deductions_path',
        'uses' => 'InvestmentReportsController@exportPenaltyDeductions'
    ]);

    Route::post('/dashboard/investments/asset-liability-reconciliation', [
        'as' => 'export_asset_liability_reconciliation_path',
        'uses' => 'InvestmentReportsController@exportAssetLiabilityReconciliation'
    ]);

    Route::post('/dashboard/investments/production_report', [
        'as' => 'production_report',
        'uses' => 'InvestmentReportsController@generateProductionReport'
    ]);

    Route::post('/dashboard/investments/monthly_inflows_report', [
        'as' => 'monthly_inflows_report',
        'uses' => 'InvestmentReportsController@generateMonthlyInflowsReport'
    ]);

    Route::post('/dashboard/investments/export_unaccounted_funds', [
        'as' => 'export_unaccounted_funds',
        'uses' => 'InvestmentReportsController@exportUnaccountedFunds'
    ]);

    Route::post('/dashboard/investments/export_withdrawals_report', [
        'as' => 'export_withdrawals_report',
        'uses' => 'InvestmentReportsController@exportWithdrawalsReport'
    ]);

    Route::post('/dashboard/investments/export_inflows_report', [
        'as' => 'export_inflows_report',
        'uses' => 'InvestmentReportsController@exportInflowsReport'
    ]);

    Route::post('/dashboard/investments/export_client_statements_report', [
        'as' => 'export_client_statements_report',
        'uses' => 'InvestmentReportsController@exportClientStatementsReport'
    ]);

    Route::post('/dashboard/investments/export_fa_year_production_report', [
        'as' => 'export_fa_year_production_report',
        'uses' => 'InvestmentReportsController@exportFaYearProductionReport'
    ]);

    Route::post('/dashboard/investments/export_fundmanager_monthly_netflow', [
        'as' => 'export_fundmanager_monthly_netflow',
        'uses' => 'InvestmentReportsController@exportFundManagerMonthlyNetflow'
    ]);

    Route::post('/dashboard/investments/export_currency_report', [
        'as' => 'export_currency_report',
        'uses' => 'InvestmentReportsController@exportCurrencyReport'
    ]);

    Route::post('/dashboard/investments/export_client_movement_audit', [
        'as' => 'export_client_movement_audit',
        'uses' => 'InvestmentReportsController@exportClientMovementAudit'
    ]);

    Route::post('/dashboard/investments/export_client_tenor_report', [
        'as' => 'export_client_tenor_report',
        'uses' => 'InvestmentReportsController@exportClientTenorReport'
    ]);

    Route::post('/dashboard/investments/export_daily_withdrawal_report', [
        'as' => 'export_daily_withdrawal_report',
        'uses' => 'InvestmentReportsController@exportDailyWithdrawalReport'
    ]);

    Route::post('/dashboard/investments/export_clawbacks_report', [
        'as' => 'export_clawbacks_report',
        'uses' => 'InvestmentReportsController@exportClawbacksReport'
    ]);

    Route::post('/dashboard/investments/export_client_custodial_transfer', [
        'as' => 'export_client_custodial_transfer',
        'uses' => 'InvestmentReportsController@exportClientCustodialTransfer'
    ]);

    Route::post('/dashboard/investments/export_client_commission_payments', [
        'as' => 'export_client_commission_payments',
        'uses' => 'InvestmentReportsController@exportClientCommissionPayments'
    ]);

    Route::post('/dashboard/investments/export_backdated_transaction_report', [
        'as' => 'export_backdated_transaction_report',
        'uses' => 'InvestmentReportsController@exportBackdatedTransactions'
    ]);

    Route::post('/dashboard/investments/export_unit_fund_client_investments', [
        'as' => 'export_unit_fund_client_investments',
        'uses' => 'InvestmentReportsController@exportUnitFundsClientInvestments'
    ]);

    Route::post('/dashboard/investments/export_fund_maturity_report', [
        'as' => 'export_fund_maturity_report',
        'uses' => 'InvestmentReportsController@exportFundMaturityReport'
    ]);

    Route::post('/dashboard/investments/export_client_maturity_report', [
        'as' => 'export_client_maturity_report',
        'uses' => 'InvestmentReportsController@exportClientMaturityReport'
    ]);

    Route::post('/dashboard/investments/export_fund_inflows', [
        'as' => 'export_fund_inflows',
        'uses' => 'InvestmentReportsController@exportFundInflows'
    ]);

    Route::post('/dashboard/investments/export_fund_outflows', [
        'as' => 'export_fund_outflows',
        'uses' => 'InvestmentReportsController@exportFundOutflows'
    ]);

    Route::post('/dashboard/investments/export_cash_transfers', [
        'as' => 'export_cash_transfers',
        'uses' => 'InvestmentReportsController@exportCashTransfers'
    ]);

    Route::post('/dashboard/investments/export_interest_expense', [
        'as' => 'export_fund_interest_expense',
        'uses' => 'InvestmentReportsController@exportSapInterestExpense'
    ]);

    Route::get('/dashboard/investments/ifs-clients-report', 'InvestmentReportsController@exportIfaClients');
    Route::get('/dashboard/investments/client-payments-report', 'InvestmentReportsController@clientPayments');

    Route::post('/dashboard/investments/export_payment_transactions_summary', [
        'as' => 'export_payment_transactions_summary',
        'uses' => 'InvestmentReportsController@exportPaymentTransactionsSummary'
    ]);

    Route::post('/dashboard/investments/export_fa_commission_history', [
        'as' => 'export_fa_commission_history',
        'uses' => 'InvestmentReportsController@exportFaCommissionHistory'
    ]);

    Route::post('/dashboard/investments/export_digital_transaction_summary', [
        'as' => 'export_digital_transaction_summary',
        'uses' => 'InvestmentReportsController@exportDigitalTransactionSummary'
    ]);

    Route::post('/dashboard/investments/export_intra_payments_summary', [
        'as' => 'export_intra_payments_summary',
        'uses' => 'InvestmentReportsController@exportIntraPaymentsSummary'
    ]);

    Route::post('/dashboard/investments/export_automatic_withdrawal_summary', [
        'as' => 'export_automatic_withdrawal_summary',
        'uses' => 'InvestmentReportsController@exportAutomaticWithdrawalSummary'
    ]);
});

Route::group(['middleware' => 'allow:investments:commission_reports'], function () {

    Route::post('/dashboard/investments/fa-commissions-projections', [
        'as' => 'fa_commissions_projections',
        'uses' => 'InvestmentReportsController@exportFaCommissionProjections'
    ]);

    Route::post('/dashboard/investments/commission-summary-report', [
        'as' => 'export_commission_summary_path',
        'uses' => 'InvestmentReportsController@exportCommissionSummary'
    ]);

    Route::post('/dashboard/investments/commission-summary-for-fas-report', [
        'as' => 'export_commission_summary_for_fas_path',
        'uses' => 'InvestmentReportsController@exportCommissionSummaryForFAs'
    ]);
});

Route::group(['middleware' => 'allow:cash-transactions'], function () {
    Route::get('/dashboard/investments/accounts-cash/', 'AccountCashController@getIndex');
    Route::get('/dashboard/investments/accounts-cash/show/{id}', 'AccountCashController@getShow');

    Route::post(
        '/dashboard/investments/accounts-cash/link-to-client-and-f-a/{id}',
        [
            'as' => 'link_cash_to_client_and_fa',
            'uses' => 'AccountCashController@postLinkToClientAndFA'
        ]
    );

    Route::post('/dashboard/investments/accounts-cash/value-cheque/{id}', [
        'as' => 'value_client_cheque_inflow',
        'uses' => 'AccountCashController@postValueCheque'
    ]);

    Route::post('/dashboard/investments/accounts-cash/bounce-cheque/{id}', [
        'as' => 'bounce_client_cheque_inflow',
        'uses' => 'AccountCashController@postBounceCheque'
    ]);


    Route::get(
        '/dashboard/investments/accounts-cash/create/{id?}',
        [
            'as' => 'get_add_cash_to_account',
            'uses' => 'AccountCashController@getCreate'
        ]
    );

    Route::post(
        '/dashboard/investments/accounts-cash/save/{id?}',
        [
            'as' => 'add_cash_to_account',
            'uses' => 'AccountCashController@postSave'
        ]
    );

    Route::delete(
        '/dashboard/investments/accounts-cash/destroy/{id}',
        [
            'as' => 'reverse_cash_inflow',
            'uses' => 'AccountCashController@deleteDestroy'
        ]
    );
    Route::get('/dashboard/investments/accounts-cash/daily-inflows', 'AccountCashController@getDailyInflows');

    Route::get('/dashboard/investments/client-payments/', 'ClientPaymentsController@getIndex');
    Route::get('/dashboard/investments/client-payments/show/{id}', 'ClientPaymentsController@getShow');

    Route::delete(
        '/dashboard/investments/client-payments/destroy/{id}',
        [
            'as' => 'reverse_cash_transfer',
            'uses' => 'ClientPaymentsController@deleteDestroy'
        ]
    );

    Route::get('/dashboard/investments/client-payments/client/{id}', 'ClientPaymentsController@getClient');

    Route::post(
        '/dashboard/investments/client-payments/withdraw/{client_id}',
        [
            'as' => 'transfer_cash_to_client',
            'uses' => 'ClientPaymentsController@postWithdraw'
        ]
    );

    Route::post(
        '/dashboard/investments/client-payments/withdraw/{client_id}/exemption',
        [
            'as' => 'exempt_client_payment',
            'uses' => 'ClientPaymentsController@exemptClientPayment'
        ]
    );

    Route::post(
        '/dashboard/investments/client-payments/transfer-to-another-client/{client_id}',
        [
            'as' => 'transfer_cash_to_another_client',
            'uses' => 'ClientPaymentsController@postTransferToAnotherClient'
        ]
    );

    Route::post('/dashboard/investments/client-payments/scheduled-instruction/{schedule_id}', 'ClientPaymentsController@postScheduledInstruction');
    Route::get('/dashboard/investments/client-payments/pdf/{payment_id}', 'ClientPaymentsController@getPdf');
    Route::post(
        '/dashboard/investments/client-payments/pdf/{payment_id}',
        [
            'as' => 'generate_instruction',
            'uses' => 'ClientPaymentsController@getPdf'
        ]
    );

    Route::get(
        '/dashboard/investments/client-payments/transact/{id}',
        [
            'as' => 'client_payments_transact',
            'uses' => 'ClientPaymentsController@getTransact'
        ]
    );

    Route::get('/dashboard/investments/client-payments/export-balances', [
        'as' => 'export_clients_with_balances',
        'uses' => 'ClientPaymentsController@getExportBalances'
    ]);

    Route::post(
        '/dashboard/investments/client-payments/{id}',
        [
            'as' => 'generate_instruction_for_scheduled_transaction',
            'uses' => 'ClientPaymentsController@postScheduledInstruction'
        ]
    );


    Route::get('/dashboard/investments/payment-instructions/', 'PaymentInstructionsController@getIndex');
    Route::get('/dashboard/investments/payment-instructions/show/{id}', 'PaymentInstructionsController@getShow');
    Route::post('/dashboard/investments/payment-instructions/update/{id}', [
        'as' => 'update_payment_instruction',
        'uses' => 'PaymentInstructionsController@postUpdate'
    ]);
    Route::post('/dashboard/investments/payment-instructions/reverse/{id}', [
        'as' => 'reverse_payment_instruction',
        'uses' => 'PaymentInstructionsController@postReverse'
    ]);
});

Route::group(['middleware' => 'allow:client-instructions:view'], function () {
    Route::get('/dashboard/investments/client-instructions', [
        'as' => 'client_investment_instruction_path',
        'uses' => 'ClientInstructionController@index'
    ]);

    Route::get('/dashboard/investments/orders', 'ClientInstructionController@orders');
    Route::get('/dashboard/investments/client-instructions/rollover/{id}', [
        'as' => 'view_client_rollover_instruction',
        'uses' => 'ClientInstructionController@rollover'
    ]);

    Route::get('/dashboard/investments/client-instructions/rollover-withdraw/{type}/{id}/edit', 'ClientInstructionController@editRollover');

    Route::get('/dashboard/investments/client-instructions/rollover/{id}/process', 'ClientInstructionController@processRollover');
    Route::get('/dashboard/investments/client-instructions/rollover/{id}/pdf', 'ClientInstructionController@pdfRollover');

    Route::get('/dashboard/investments/client-instructions/topup/{id}', [
        'as' => 'view_client_topup_instruction',
        'uses' => 'ClientInstructionController@topup'
    ]);

    Route::get('/dashboard/investments/client-instructions/unit_fund_instruction/{id}', [
        'as' => 'view_unit_fund_investment_instruction',
        'uses' => 'ClientInstructionController@unitFundInvestmentInstruction'
    ]);

    Route::get('/dashboard/investments/client-instructions/unit_fund_instruction/{id}/edit', [
        'as' => 'edit_unit_fund_investment_instruction',
        'uses' => 'ClientInstructionController@editUnitFundInvestmentInstruction'
    ]);

    Route::get('/dashboard/investments/client-instructions/utility_billing_instruction/{id}', [
        'as' => 'view_utility_billing_instruction',
        'uses' => 'ClientInstructionController@utilityBillingInstruction'
    ]);

    Route::post('/dashboard/investments/reports/clients-summaries', [
        'as' => 'export_utility_instructions_summary_report',
        'uses' => 'ClientInstructionController@exportUtilityBillingInstruction'
    ]);

    Route::get('/dashboard/investment/client-instruction/apply', 'ClientInstructionController@investmentApply');
    Route::get('/dashboard/investments/client-instructions/application/{id}/edit', 'ClientInstructionController@editApplication');
    Route::get('/dashboard/investment/client-instruction/top-up', 'ClientInstructionController@investmentTopup');
    Route::get('/dashboard/investment/client-instruction/purchase-units', 'ClientInstructionController@createUnitPurchase');
    Route::get('/dashboard/investments/client-instructions/topup/{id}/edit', 'ClientInstructionController@editTopup');

    Route::get('/dashboard/investments/client-instructions', 'ClientInstructionController@index');

//    Route::get('/dashboard/investments/client-instructions/rollover/{id}/process', 'ClientInstructionController@processRollover');

    Route::post(
        '/dashboard/investments/client-instructions/rollover/{id}/process',
        [
            'as' => 'process_rollover_instruction',
            'uses' => 'ClientInstructionController@processRollover'
        ]
    );

    Route::get('/dashboard/investments/client-instructions/rollover/{id}/pdf', 'ClientInstructionController@pdfRollover');


    Route::post('/dashboard/investments/client-instructions/export', 'ClientInstructionController@postExport');

    Route::post(
        '/dashboard/investments/client-instructions/withdraw-rollover/{id}/cancel',
        [
            'as' => 'cancel_client_withdraw_rollover_instruction',
            'uses' => 'ClientInstructionController@cancelWithdrawRollover'
        ]
    );

    Route::post(
        '/dashboard/investments/client-instructions/topup/{id}/process',
        [
            'as' => 'process_topup_instruction',
            'uses' => 'ClientInstructionController@processTopup'
        ]
    );

//    Route::get('/dashboard/investments/client-instructions/topup/{id}/process', 'ClientInstructionController@processTopup');

    Route::get('/dashboard/investments/client-instructions/topup/{id}/pdf', 'ClientInstructionController@pdfTopup');

    Route::get('/dashboard/investments/client-instructions/application/{id}', [
        'uses' => 'ClientInstructionController@application',
        'as' => 'view_client_filled_investment_application'
    ]);

    Route::get('/dashboard/investments/client-instructions/application/{id}/process', 'ClientInstructionController@processApplication');
    Route::get('/dashboard/investments/client-instructions/application/{id}/pdf', 'ClientInstructionController@pdfApplication');
    Route::get('/dashboard/investments/client-instructions/document/{id}', 'ClientInstructionController@showDocument');
    Route::get('/dashboard/investments/client-instructions/filled-application-documents/{id}', 'ClientInstructionController@filledApplicationDocuments');

    Route::get('/dashboard/investments/client-instructions/topup/{id}/process', 'ClientInstructionController@processTopup');

    Route::get('/dashboard/investments/client-instructions/topup/{id}/pdf', 'ClientInstructionController@pdfTopup');

    Route::get('/dashboard/investment/client/apply', 'ClientInstructionController@apply');
    Route::get('/dashboard/investment/client/application/{appId}/edit', 'ClientInstructionController@edit');

    Route::get('/dashboard/investments/client-instructions/{approval}/approval-document/{editInv?}', 'ClientInstructionController@approvalInstructionDocument');

    Route::get('/document/view-document/{id}', 'ClientInstructionController@filledApplicationDocuments');
});

Route::group(['middleware' => 'allow:client-instructions:action'], function () {
    Route::post(
        '/dashboard/investments/client-instructions/topup/{id}/cancel',
        [
            'as' => 'cancel_client_topup_instruction',
            'uses' => 'ClientInstructionController@cancelTopup'
        ]
    );

    Route::post('/dashboard/investments/client-instructions/topup/{id}/cancel', [
        'as' => 'cancel_client_topup_instruction',
        'uses' => 'ClientInstructionController@cancelTopup'
    ]);

    Route::post('/dashboard/investments/client-instructions/withdraw-rollover/{id}/cancel', [
        'as' => 'cancel_client_withdraw_rollover_instruction',
        'uses' => 'ClientInstructionController@cancelWithdrawRollover'
    ]);

    Route::post('/dashboard/investments/client-instructions', [
        'as' => 'export_bank_instructions',
        'uses' => 'ClientInstructionController@postExport'
    ]);
});

Route::group(['middleware' => 'allow:documents:view'], function () {
    Route::get('/dashboard/documents/{id}', [
        'uses' => 'DocumentController@show',
        'as' => 'show_document'
    ]);

    Route::get('/dashboard/documents', 'DocumentController@main');
    Route::get('/dashboard/documents/{module}/{slug}/{sectionId?}', 'DocumentController@index');
});

Route::group(['middleware' => 'allow:documents:create'], function () {
    Route::post('/dashboard/documents', [
        'uses' => 'DocumentController@upload',
        'as' => 'upload_document'
    ]);

    Route::put('/dashboard/documents', [
        'uses' => 'DocumentController@update',
        'as' => 'update_document'
    ]);

    Route::delete('/dashboard/documents', [
        'uses' => 'DocumentController@delete',
        'as' => 'delete_document'
    ]);
});

Route::group(['middleware' => 'allow:investments:manage-banks'], function () {
    Route::get('/dashboard/banks', 'BanksController@index');
    Route::get('/dashboard/banks/create', 'BanksController@create');
    Route::get('/dashboard/banks/{bank}', 'BanksController@show');
    Route::get('/dashboard/banks/{bank}/edit', 'BanksController@edit');
    Route::post('/dashboard/banks', [
        'as' => 'create_bank',
        'uses' => 'BanksController@store'
    ]);
    Route::put('/dashboard/banks/{bank}', [
        'as' => 'update_bank',
        'uses' => 'BanksController@update'
    ]);
    Route::post('/dashboard/banks/{bank}/branches', [
        'as' => 'create_bank_branch',
        'uses' => 'BanksController@addBranch'
    ]);
    Route::put('/dashboard/banks/{bank}/branches', [
        'as' => 'update_bank_branch',
        'uses' => 'BanksController@updateBranch'
    ]);
});

Route::group(['middleware' => 'allow:products:view'], function () {
    Route::get('/dashboard/products', [
        'as' => 'products',
        'uses' => 'ProductController@index'
    ]);

    Route::get('/dashboard/products/details/{id}', [
        'as' => 'products.details',
        'uses' => 'ProductController@details'
    ]);
});

Route::group(['middleware' => 'allow:products:create'], function () {
    Route::get('/dashboard/products/create/{id?}', [
        'as' => 'products/create',
        'uses' => 'ProductController@create'
    ]);

    Route::post('/dashboard/products/store/{id?}', [
        'as' => 'products.store',
        'uses' => 'ProductController@store'
    ]);

    Route::post('/dashboard/products/{productId}/upload_documents/{id?}', [
        'as' => 'products.upload_documents',
        'uses' => 'ProductController@uploadDocument'
    ]);
});
