<?php
/**
 * Date: 24/05/2017
 * Time: 10:11
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

Route::get('/dashboard/portfolio', 'PortfolioController@index')->middleware('allow:portfolio:menu');

Route::group(['middleware' => 'allow:portfolio:view-investments'], function () {
    Route::get('/dashboard/portfolio/institutions', 'PortfolioController@institutions');

    Route::get('/dashboard/portfolio/security/{id}/details/', 'PortfolioController@institutionDetail');

    Route::post('/dashboard/portfolio/institutions/export', [
        'as' => 'export_portfolio_summary',
        'uses' => 'PortfolioController@export'
    ]);

    Route::any('/dashboard/portfolio/institutions/statement/{id}', [
        'as' => 'portfolio_statement',
        'uses' => 'PortfolioController@statement'
    ]);

    Route::post('/dashboard/portfolio/{id}/repay/destroy', [
        'as' => 'portfolio_repayment_delete',
        'uses' => 'PortfolioInvestmentsRepaymentController@destroy'
    ]);

    Route::resource('/dashboard/portfolio/{id}/repay', 'PortfolioInvestmentsRepaymentController', [
        'names' => [
            'store' => 'portfolio_repay'
        ]
    ]);

    Route::get('/dashboard/portfolio/deposit-holdings', 'PortfolioController@showInvestmentsGrid');

    Route::get('/dashboard/portfolio/investments', 'PortfolioController@showInvestmentsGrid');

    Route::get('/dashboard/portfolio/investments/details/{id}', 'PortfolioController@showInvestmentDetails');

    Route::get('/dashboard/portfolio/investments/{holding_id}/loan/amortization', 'Portfolio\Deposits\LoanAmortizationController@show');

    Route::get('/dashboard/portfolio/investments/{holding_id}/loan/amortization/export', 'Portfolio\Deposits\LoanAmortizationController@export');

    Route::get('/dashboard/portfolio/investments/{holding_id}/holdings/interest-schedules', 'DepositHoldingController@getInterestSchedules');
});

Route::group(['middleware' => 'allow:portfolio:create-investments'], function () {
    Route::get('/dashboard/portfolio/institutions/add/{id?}', 'PortfolioController@addInstitution');

    Route::post('/dashboard/portfolio/institutions/add/{id?}', [

        'uses' => 'PortfolioController@storeInstitution'
    ]);

    Route::get('/dashboard/portfolio/institutions/{id}/client/create', [
        'as' => 'portfolio.client.create',
        'uses' => 'PortfolioController@addClient'
    ]);

    Route::post('/dashboard/portfolio/institutions/{id}/client/store', [
        'as' => 'portfolio.client.store',
        'uses' => 'PortfolioController@storeClient'
    ]);

    Route::get('/dashboard/portfolio/investments/add', 'PortfolioController@addInvestment');

    Route::post('/dashboard/portfolio/investments/add', [
        'uses' => 'PortfolioController@storeInvestment'
    ]);

    Route::get('/dashboard/portfolio/investments/send-instructions/{investment_id}', 'PortfolioController@getSendInstructions');

    Route::post('/dashboard/portfolio/investments/send-instructions/{investment_id}', [
        'as' => 'send_deposit_placement_instructions',
        'uses' => 'PortfolioController@postSendInstructions'
    ]);

    Route::get('/dashboard/portfolio/investments/edit/{investment_id}', 'DepositHoldingController@getEdit');

    Route::post('/dashboard/portfolio/investments/edit/{investment_id}', [
        'as' => 'edit_portfolio_investment',

        'uses' => 'DepositHoldingController@postEdit'
    ]);

    Route::get('/dashboard/portfolio/investments/maturity', 'PortfolioController@maturity');

    Route::post('/dashboard/portfolio/investments/maturity', [

        'as' => 'portfolio_maturity_route',
        'uses' => 'PortfolioController@maturity'
    ]);

    Route::any('/dashboard/portfolio/investments/maturity/analysis', 'PortfolioController@maturityAnalysis');

    Route::get('/dashboard/portfolio/investments/redeem/{id}', 'PortfolioController@withdraw');

    Route::post('/dashboard/portfolio/investments/withdraw', [
        'as' => 'portfolio_withdraw',
        'uses' => 'PortfolioController@postWithdraw'
    ]);

    Route::get('/dashboard/portfolio/investments/rollover/{id}', 'PortfolioController@rollover');

    Route::post('/dashboard/portfolio/investments/rollover', [
        'as' => 'portfolio_rollover',
        'uses' => 'PortfolioController@postRollover'
    ]);

    Route::get('/dashboard/portfolio/combinedrollover/{inv_id}', 'PortfolioInvestmentActionController@combinedRollover');

    Route::post('/dashboard/portfolio/performcombinedrollover/{inv_id}', 'PortfolioInvestmentActionController@performCombinedRollover');

    Route::post('/dashboard/portfolio/combinedrollover{inv_id}', [

        'as' => 'save_combined_portfolio_rollover',
        'uses' => 'PortfolioInvestmentActionController@saveCombinedRollover'
    ]);


    Route::post('/dashboard/portfolio/investments/reverse/{id}', [
        'as' => 'portfolio_reverse',

        'uses' => 'PortfolioInvestmentActionController@reverse'
    ]);

    Route::post('/dashboard/portfolio/investments/rollback-withdrawal/{id}', [
        'as' => 'portfolio_rollback_withdraw',
        'uses' => 'PortfolioInvestmentActionController@rollbackWithdrawal'
    ]);

    Route::get('/dashboard/portfolio/instructions/', 'PortfolioInstructionsController@getIndex');
    Route::get('/dashboard/portfolio/instructions/show/{id}', 'PortfolioInstructionsController@getShow');
    Route::post('/dashboard/portfolio/instructions/generate-p-d-f/{id}', [
        'as' => 'generate_portfolio_instruction_pdf',
        'uses' => 'PortfolioInstructionsController@postGeneratePDF'
    ]);

    Route::get('/dashboard/portfolio/investments/{holding_id}/holdings/generate-interest-schedules', 'DepositHoldingController@generateInterestSchedules');
});

Route::group(['middleware' => 'allow:bank:view'], function () {
    Route::get('/dashboard/portfolio/custodials', 'CustodialController@custodials');

    Route::get('/dashboard/portfolio/custodials/account-balance-trail/{id}', 'CustodialController@viewAccountBalanceTrail');

    Route::get('/dashboard/portfolio/custodials/account-balance-trail/{id}/export', 'CustodialController@exportAccountBalanceTrails');

    Route::get('/dashboard/portfolio/custodials/add/{id?}', 'CustodialController@addCustodial');


    Route::get('/dashboard/portfolio/custodials/details/{id?}', 'CustodialController@custodialDetails');
    Route::post('/dashboard/portfolio/custodials/details/export/excel', [
        'as' => 'export_details_to_excel_path',
        'uses' => 'CustodialController@exportToExcel'
    ]);

    Route::post('/dashboard/portfolio/custodial/link_product_project', [
        'as' => 'link_product_projects_receiving_accounts',
        'uses' => 'CustodialController@linkProductProject'
    ]);

    Route::get('/dashboard/portfolio/custodials/edit-transaction/{id}', 'CustodialController@getEditTransaction');

    Route::get('/dashboard/portfolio/unit-funds', 'UnitFundCustodialController@index');

    Route::get('/dashboard/portfolio/unit-fund/details/{id}', 'UnitFundCustodialController@show');

    Route::post('/dashboard/portfolio/unit-fund/custodial/remove', [
        'as' => 'remove_unit_fund_custodial_account',
        'uses' => 'UnitFundCustodialController@delete'
    ]);

    Route::post('/dashboard/portfolio/unit-fund/custodial/link', [
        'as' => 'link_unit_fund_custodial_account',
        'uses' => 'UnitFundCustodialController@linkAccount'
    ]);
});

Route::group(['middleware' => 'allow:bank:create'], function () {
    Route::post('/dashboard/portfolio/custodials/{id}/transactions/update', [
        'as' => 'update_custodial_transaction',
        'uses' => 'CustodialController@updateTransaction'
    ]);

    Route::post('/dashboard/portfolio/custodials/deposit/{id}', [
        'as' => 'custodial_deposit',
        'uses' => 'CustodialController@deposit'
    ]);

    Route::get('/dashboard/portfolio/custodials/transact/{id}', 'CustodialController@transact');

    Route::post('/dashboard/portfolio/custodials/transact/{id}', [
        'as' => 'custodial_withdraw',
        'uses' => 'CustodialController@saveWithdraw'
    ]);

    Route::post('/dashboard/portfolio/custodials/transfer/{id}', [
        'as' => 'custodial_transfer',
        'uses' => 'CustodialController@transfer'
    ]);

    Route::post('/dashboard/portfolio/custodials/reconcile/{id}', [
        'as' => 'custodial_reconcile',
        'uses' => 'CustodialController@reconcile'
    ]);

    Route::post('/dashboard/portfolio/custodials/balance/{id}', [
        'as' => 'custodial_balance_trail',

        'uses' => 'CustodialController@postBalance'
    ]);

    Route::get('/dashboard/portfolio/custodials/exchange_rates', [
        'as' => 'custodials.exchange_rates',
        'uses' => 'CustodialController@exchangeRates'
    ]);

    Route::get('/dashboard/portfolio/custodials/account-balance-trail/delete/{id}', 'CustodialController@deleteBalanceTrail');

    Route::post('/dashboard/portfolio/custodial/add/{id?}', [
        'uses' => 'CustodialController@storeCustodial'
    ]);

    Route::post('/dashboard/portfolio/custodial/bank/add', [
        'as' => 'new_bank_path',
        'uses' => 'CustodialController@storeBank'
    ]);

    Route::get('/dashboard/portfolio/custodial/transfer-mismatch/{id}', 'CustodialController@getTransferMismatch');

    Route::get('/dashboard/portfolio/suspense_transactions/{id}', 'Portfolio\SuspenseTransactionController@index');

    Route::get('/dashboard/portfolio/suspense-transactions/details/{id}', [
        'as' => 'view_suspense_transaction',
        'uses' => 'Portfolio\SuspenseTransactionController@details'
    ]);

    Route::post('/dashboard/portfolio/suspense-transactions/record_fees/{id}', [
        'as' => 'suspense_transactions.record_fees',
        'uses' => 'Portfolio\SuspenseTransactionController@recordFees'
    ]);

    Route::post('/dashboard/portfolio/suspense-transactions/bounce_cheque/{id}', [
        'as' => 'suspense_transactions.bounce_cheque',
        'uses' => 'Portfolio\SuspenseTransactionController@bounceCheque'
    ]);

    Route::post('/dashboard/portfolio/suspense-transactions/record_deposits/{id}', [
        'as' => 'suspense_transactions.record_deposits',
        'uses' => 'Portfolio\SuspenseTransactionController@recordDeposits'
    ]);

    Route::post('/dashboard/portfolio/suspense-transactions/record_client_inflow/{id}', [
        'as' => 'suspense_transactions.record_client_inflow',
        'uses' => 'Portfolio\SuspenseTransactionController@recordClientInflow'
    ]);

    Route::post('/dashboard/portfolio/suspense-transactions/match_custodial_transaction/{id}', [
        'as' => 'suspense_transactions.match_custodial_transaction',
        'uses' => 'Portfolio\SuspenseTransactionController@matchCustodialTransaction'
    ]);

    Route::post('/dashboard/portfolio/suspense-transactions/match_suspense_transaction/{id}', [
        'as' => 'suspense_transactions.match_suspense_transaction',
        'uses' => 'Portfolio\SuspenseTransactionController@matchSuspenseTransaction'
    ]);
});

Route::group(['middleware' => 'allow:portfolio:create_wht'], function () {
    Route::get('/dashboard/portfolio/custodial/{custodial_id}/wht', 'CustodialController@createWht');
});

Route::group(['middleware' => 'allow:portfolio-approvals:view'], function () {
    //approvals
    Route::get('/dashboard/portfolio/approve', 'PortfolioApprovalController@index');

    Route::get('/dashboard/portfolio/approve/{id}', 'PortfolioApprovalController@showApproval');

    Route::get('/dashboard/portfolio/approvals/batches', 'PortfolioApprovalController@batches');

    Route::get('/dashboard/portfolio/approvals/batch/{stage_id}/{batch_id}', 'PortfolioApprovalController@batch');
});

Route::group(['middleware' => 'allow:portfolio-approvals:create'], function () {
    Route::post('/dashboard/portfolio/approve/{id}', [

        'uses' => 'PortfolioApprovalController@saveApproval'
    ]);
});

Route::group(['middleware' => 'allow:portfolio-approvals:approve'], function () {
    Route::get('/dashboard/portfolio/approve/{id}/stage/{stageId}', 'PortfolioApprovalController@approve');

    Route::get('/dashboard/portfolio/approvalremove/{id}', 'PortfolioApprovalController@remove');
});

Route::group(['middleware' => 'allow:active-strategy:view'], function () {
    Route::get('/dashboard/portfolio/activestrategy', 'ActiveStrategyController@index');


    Route::get('/dashboard/portfolio/activestrategy/{id}', 'ActiveStrategyController@security');

    Route::get('/dashboard/portfolio/activestrategy/{id}/marketprice', 'ActiveStrategyController@marketPrice');
});

Route::group(['middleware' => 'allow:active-strategy:create'], function () {

    Route::get('/dashboard/portfolio/activestrategy/security/create/{id?}', 'ActiveStrategyController@createSecurity');

    Route::post('/dashboard/portfolio/activestrategy/security/create/{id?}', [
        'as' => 'create_active_strategy_security',
        'uses' => 'ActiveStrategyController@saveSecurity'
    ]);

    Route::post('/dashboard/portfolio/activestrategy/{id}/buy', [

        'as' => 'buy_active_strategy_shares',
        'uses' => 'ActiveStrategyController@buy'
    ]);

    Route::get('/dashboard/portfolio/activestrategy/holdings/update/{id}', 'ActiveStrategyController@editBought');

    Route::put('/dashboard/portfolio/activestrategy/holdings/update/{id}', [

        'as' => 'update_bought_active_strategy_shares',
        'uses' => 'ActiveStrategyController@updateBought'
    ]);

    Route::get('/dashboard/portfolio/activestrategy/{id}/targetprice', 'ActiveStrategyController@targetPrice');

    Route::post('/dashboard/portfolio/activestrategy/{id}/targetprice', [

        'as' => 'set_target_price',
        'uses' => 'ActiveStrategyController@saveTargetPrice'
    ]);

    Route::post('/dashboard/portfolio/activestrategy/{id}/marketprice', [

        'as' => 'set_market_price',
        'uses' => 'ActiveStrategyController@saveMarketPrice'
    ]);

    Route::post('/dashboard/portfolio/activestrategy/{id}/dividend', [
        'as' => 'save_dividends',
        'uses' => 'ActiveStrategyController@saveDividends'
    ]);

    Route::get('/dashboard/portfolio/activestrategy/{id}/dividends', [
        'as' => 'dividends.index',
        'uses' => 'ActiveStrategyController@viewDividends'
    ]);

    Route::post('/dashboard/portfolio/activestrategy/{id}/sell', [

        'as' => 'sell_active_strategy_shares',
        'uses' => 'ActiveStrategyController@sell'
    ]);

    Route::put('/dashboard/portfolio/activestrategy/shares/sold/update/{id}', [
        'as' => 'update_sold_active_strategy_shares',
        'uses' => 'ActiveStrategyController@updateSold'
    ]);

    Route::get('/dashboard/portfolio/equities/reverse_buy/{id}', [
        'as' => 'equities.reverse_buy',
        'uses' => 'EquityController@showReverseBuyShares'
    ]);

    Route::post('/dashboard/portfolio/equities/reverse_buy/{id}', [
        'as' => 'equities.post_reverse_buy',
        'uses' => 'EquityController@submitReverseBuyShares'
    ]);

    Route::post('/dashboard/portfolio/equities/edit_dates_buy/{id}', [
        'as' => 'equities.edit_dates_buy',
        'uses' => 'EquityController@editDatesBuyShares'
    ]);

    Route::post('/dashboard/portfolio/equities/edit_dates_buy/{id}', [
        'as' => 'equities.edit_dates_buy',
        'uses' => 'EquityController@editDatesSaleShares'
    ]);

    Route::get('/dashboard/portfolio/equities/saledetails/{id}', [
        'as' => 'equities.saledetails',
        'uses' => 'EquityController@showSaleDetails'
    ]);

    Route::post('/dashboard/portfolio/equities/reverse_sell/{id}', [
        'as' => 'equities.post_reverse_sell',
        'uses' => 'EquityController@submitReverseSellShares'
    ]);

    Route::get('/dashboard/portfolio/activestrategy/shares/sold/update/{id}', 'ActiveStrategyController@editSold');

    Route::get('/dashboard/portfolio/activestrategy/shares/sold/update/{id}', 'ActiveStrategyController@editSold');
});

Route::group(['middleware' => 'allow:portfolio:view-analytics'], function () {
    Route::get('/dashboard/portfolio/deposit/summary/{fundType?}', 'PortfolioController@summary');

    Route::get('/dashboard/portfolio/asset/{fundType?}', 'PortfolioController@assetSummary');

    Route::post('/dashboard/portfolio/summary/{fundType?}', [
        'as' => 'add_portfolio_limit',
        'uses' => 'PortfolioController@savePortfolioLimits'
    ]);

    Route::get('/dashboard/portfolio/analysis', 'PortfolioController@analysis');

    //Portfolio fund summary controller
    Route::get('/dashboard/portfolio/fund-summary', 'PortfolioFundSummaryController@index');

    Route::get('/dashboard/portfolio/fund-pricing-summary/{id?}', 'PortfolioFundSummaryController@pricingSummary');
});

Route::group(['middleware' => 'allow:portfolio:reports'], function () {
    Route::get('/dashboard/portfolio/reports/', 'PortfolioReportsController@getIndex');

    Route::post('/dashboard/portfolio/interest-expense-report', [
        'as' => 'export_portfolio_interest_expense_path',
        'uses' => 'PortfolioReportsController@exportInterestExpense'
    ]);

    Route::post('/dashboard/portfolio/withholding-tax-report', [
        'as' => 'export_portfolio_withholding_tax_path',
        'uses' => 'PortfolioReportsController@exportWithholdingTax'
    ]);

    Route::post('/dashboard/portfolio/custodial-accounts', [
        'as' => 'export_custodial_accounts_path',
        'uses' => 'PortfolioReportsController@exportCustodialAccounts'
    ]);

    Route::post('/dashboard/portfolio/deposit-analysis', [
        'as' => 'export_deposit_analysis_path',
        'uses' => 'PortfolioReportsController@exportDepositAnalysis'
    ]);

    Route::post('/dashboard/portfolio/residual-income-analysis', [
        'as' => 'export_residual_income_analysis_path',
        'uses' => 'PortfolioReportsController@exportResidualIncomeAnalysis'
    ]);

    Route::post('/dashboard/portfolio/summary-export', [
        'as' => 'export_portfolio_summary_path',
        'uses' => 'PortfolioReportsController@exportPortfolioSummary'
    ]);

    Route::post('/dashboard/portfolio/investments-on-call', [
        'as' => 'export_portfolio_investments_on_call_path',
        'uses' => 'PortfolioReportsController@exportInvestmentsOnCall'
    ]);

    Route::post('/dashboard/portfolio/yearly_deposit_holdings', [
        'as' => 'export_yearly_deposit_holdings',
        'uses' => 'PortfolioReportsController@exportYearDepositHoldings'
    ]);

    Route::post('/dashboard/portfolio/export_investor_valuation', [
        'as' => 'export_investor_valuation',
        'uses' => 'PortfolioReportsController@exportInvestorValuation'
    ]);

    Route::post('/dashboard/portfolio/export_portfolio_movement_audit_report', [
        'as' => 'export_portfolio_movement_audit_report',
        'uses' => 'PortfolioReportsController@exportPortfolioMovementAudit'
    ]);

    Route::post('/dashboard/portfolio/export_loan_tenor_range_report', [
        'as' => 'export_loan_tenor_range_report',
        'uses' => 'PortfolioReportsController@exportLoanTenorRangeReport'
    ]);

    Route::post('/dashboard/portfolio/export_unit_fund_transaction_summary', [
        'as' => 'export_unit_fund_transaction_summary',
        'uses' => 'PortfolioReportsController@exportUnitFundTransactionSummary'
    ]);

    Route::post('/dashboard/portfolio/export_unit_fund_holdings_summary', [
        'as' => 'export_unit_fund_holdings_summary',
        'uses' => 'PortfolioReportsController@exportUnitFundHoldingsSummary'
    ]);

    Route::post('/dashboard/portfolio/export_performance_attribution_summary', [
        'as' => 'export_performance_attribution_summary',
        'uses' => 'PortfolioReportsController@exportPerformanceAttributionSummary'
    ]);

    Route::get('/dashboard/portfolio/export-fund-pricing/{id}', 'PortfolioReportsController@exportFundPricingReport');

    Route::post('/dashboard/portfolio/export_unit_fund_pricing_summary', [
        'as' => 'export_unit_fund_pricing_summary',
        'uses' => 'PortfolioReportsController@exportFundPricingReport'
    ]);

    Route::post('/dashboard/portfolio/export_unit_fund_management_summary', [
        'as' => 'export_unit_fund_management_summary',
        'uses' => 'PortfolioReportsController@exportFundManagementSummaryReport'
    ]);

    Route::post('/dashboard/portfolio/export_fund_valuation_report', [
        'as' => 'export_unit_fund_valuation_summary',
        'uses' => 'PortfolioReportsController@exportFundValuationReport'
    ]);

    Route::post('/dashboard/portfolio/export_unit_fund_deposit_holdings', [
        'as' => 'export_unit_fund_deposit_holdings',
        'uses' => 'PortfolioReportsController@exportFundDepositHoldings'
    ]);
});

//portfolio bonds
Route::resource('/dashboard/portfolio/bonds', 'BondsController');

/**
 * Asset class
 */
Route::resource('/dashboard/portfolio/asset-classes', 'AssetClassController');

/**
 * new portfolio securities
 */
//Route::get('/dashboard/portfolio/securities/', 'ActiveStrategyController@index');

Route::resource('/dashboard/portfolio/securities', 'PortfolioSecurityController');

Route::get('/dashboard/portfolio/securities/{id}/deposit-holdings/{holding_id}', 'PortfolioSecurityController@showHoldings');

Route::get('/dashboard/portfolio/securities/{id}', 'PortfolioSecurityController@show');

Route::get('/dashboard/portfolio/equity/summary', 'EquityController@index');

Route::post(
    '/dashboard/portfolio/equity/summary-report',
    [
    'as' => 'export_portfolio_equities_report_path',
    'uses' => 'EquityController@exportReport'
    ]
);

Route::get('/dashboard/portfolio/securities/{id}/target-price', 'EquityController@targetPrice');

Route::get('/dashboard/portfolio/securities/{id}/market-price', 'EquityController@marketPrice');

Route::get('/dashboard/portfolio/security/{id}/dividends', 'EquityController@viewDividends');

Route::get('/dashboard/portfolio/security/{id}/dividends/{dividend_id}', 'EquityController@showDividend');

Route::post('/dashboard/portfolio/security/{id}/dividends/{dividend_id}', [
    'as' => 'receive_dividend',
    'uses' => 'EquityController@receiveDividend'
]);

/**
 * Order management
 */
Route::group(['middleware' => 'allow:portfolio:order-management'], function () {
    Route::resource('/dashboard/portfolio/orders', 'PortfolioOrderController');

    Route::get('/dashboard/portfolio/orders/generate-order/{id}/', [
        'as' => 'portfolio_order.generate-order',
        'uses' => 'PortfolioOrderController@generateOrderDocument'
    ]);
});

/**
 * Compliance routes
 */
Route::group(['middleware' => 'allow:portfolio:fund-compliance'], function () {
    Route::resource('/dashboard/portfolio/compliance', 'PortfolioComplianceController');
});

Route::get('/dashboard/portfolio/approve/{id}/action/{action}/{param?}', [
    'as' => 'portfolio_approval_action',
    'uses' => 'PortfolioApprovalController@action'
]);
