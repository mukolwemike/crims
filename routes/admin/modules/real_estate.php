<?php

/*
   |--------------------------------------------------------------------------
   | Real Estate
   |--------------------------------------------------------------------------
   | Real Estate Sales
   */
Route::group(['middleware' => 'allow:realestate:view'], function () {
    Route::get('/dashboard/realestate', 'RealEstateController@menu');

    //    Route::get('/dashboard/realestate/projects/tranches/{project_id}', 'RealEstateProjectsController@getProjectTranches');

    Route::get('/dashboard/realestate/projects/', 'RealEstateProjectsController@getIndex');

    Route::get('/dashboard/realestate/clients/', 'RealEstateClientsController@getIndex');
    Route::get('/dashboard/realestate/clients/show/{id}', 'RealEstateClientsController@getShow');
    Route::post('/dashboard/realestate/clients/statement/{client_id}/{project_id}', 'RealEstateClientsController@postStatement');
    Route::get(
        '/dashboard/realestate/clients/statement/{client_id}/{project_i_d}',
        [
            'as'=>'view_realestate_statement',
            'uses'=>'RealEstateClientsController@getStatement'
        ]
    );

    Route::get('/dashboard/realestate/projects/holdings/{holding_id}/deactivate', 'RealEstateUnitsController@deactivate');
    Route::get('/dashboard/realestate/projects/holdings/{holding_id}/activate', 'RealEstateUnitsController@activate');

    Route::get('/dashboard/realestate/projects/{project_id}/units/show/{unit_id}', 'RealEstateUnitsController@getShow');
    Route::get('/dashboard/realestate/forfeiture_notices/show/{id}', 'RealEstateUnitsController@getShowForfeiture');
    Route::get('/dashboard/realestate/projects/{project_id}/units/cancelled/{unit_id}/{client_id}', 'RealEstateUnitsController@getCancelled');

    Route::get('/dashboard/realestate/projects/show/{id}', 'RealEstateProjectsController@getShow');
    Route::get('/dashboard/realestate/projects/cancelled/{id}', 'RealEstateProjectsController@getCancelled');

    Route::get('/dashboard/realestate/reservations/show/{id}', 'RealEstateReservationsController@getShow');

    Route::get('/dashboard/realestate/loos/', 'LOOsController@getIndex');
    Route::get('/dashboard/realestate/loos/show/{id}', 'LOOsController@getShow');

    Route::get('/dashboard/realestate/sales-agreements/', 'SalesAgreementsController@getIndex');
    Route::get('/dashboard/realestate/sales-agreements/show/{id}', 'SalesAgreementsController@getShow');

    Route::get(
        '/dashboard/realestate/loos/l-o-o-authorization-form/{id}',
        [
            'as'=>'preview_loo_authorization_form',
            'uses'=>'LOOsController@getLOOAuthorizationForm'
        ]
    );

    Route::get('/dashboard/realestate/payments/', 'RealEstatePaymentsController@getIndex');
    Route::get('/dashboard/realestate/payments/show/{id}', 'RealEstatePaymentsController@getShow');

    Route::get(
        '/dashboard/realestate/payments-schedules/overdue',
        [
            'as'=>'get_overdue_payment_schedules',
            'uses'=>'RealEstatePaymentScheduleController@getOverdue'
        ]
    );

    Route::post(
        '/dashboard/realestate/payments-schedules/export-overdue',
        [
            'as'=>'export_overdue_payments',
            'uses'=>'RealEstatePaymentScheduleController@postExportOverdue'
        ]
    );

    Route::get('/dashboard/realestate/payments/preview-bc/{id}', 'RealEstatePaymentsController@getPreviewBc');

    Route::get('/dashboard/realestate/payments-schedules/', 'RealEstatePaymentScheduleController@getIndex');
    Route::get('/dashboard/realestate/payments-schedules/show/{holding_id}', 'RealEstatePaymentScheduleController@getShow');

    Route::get(
        '/dashboard/realestate/clients/preview-payment-plan/{client_id}',
        [
            'as'=>'preview_payment_plan',
            'uses'=>'RealEstateClientsController@getPreviewPaymentPlan'
        ]
    );

    Route::get('/dashboard/realestate/client-instructions/', 'RealEstateInstructionsController@index');

    Route::get('/dashboard/realestate/client-instructions/payment/{instruction}', 'RealEstateInstructionsController@show');

    Route::get('/dashboard/realestate/client-instructions/payment/{instruction}/process', 'RealEstateInstructionsController@process');
});

Route::group(['middleware' => 'allow:realestate:create'], function () {
    Route::get('/dashboard/realestate/forfeiture_notices', 'RealEstateUnitsController@getForfeitureNotices');

    Route::get('/dashboard/realestate/projects/units/forfeiture_notices/approval/{id}', 'RealEstateUnitsController@sendForfeitureNoticeApproval');

    Route::get('/dashboard/realestate/projects/units/forfeiture_notices/approve/{id}', 'RealEstateUnitsController@approveForfeitureNotice');

    Route::post('/dashboard/realestate/projects/units/forfeiture_notices/cancel', 'RealEstateUnitsController@cancelForfeitureNotice')->name('cancel_notice');

    Route::post('/dashboard/realestate/projects/units/forfeiture_notices/{id}', 'RealEstateUnitsController@sendForfeitureNotice');

    Route::get('/dashboard/realestate/projects/{project_id}/units/create/{unit_id?}', 'RealEstateUnitsController@getCreate');
    Route::post(
        '/dashboard/realestate/projects/{project_id}/units/store/{unit_id?}',
        [
            'as'=>'store_realestate_unit',
            'uses'=>'RealEstateUnitsController@postStore'
        ]
    );

    Route::get('/dashboard/realestate/projects/{project_id}/units/transfer/{unit_id}', 'RealEstateUnitsController@getTransfer');
    Route::get('/dashboard/realestate/projects/{project_id}/units/reserve/{unit_id}', 'RealEstateUnitsController@getReserve');
    Route::post('/dashboard/realestate/projects/{project_id}/units/reserve/{unit_id}', 'RealEstateUnitsController@postReserve');


    Route::get('/dashboard/realestate/projects/create/{id?}', 'RealEstateProjectsController@getCreate');
    Route::post(
        '/dashboard/realestate/projects/store/{id?}',
        [
            'as'=>'create_realestate_project',
            'uses'=>'RealEstateProjectsController@postStore'
        ]
    );

    Route::get('/dashboard/realestate/projects/setup/{id}', [
        'as' => 'realestate_projects_setup',
        'uses' => 'RealEstateProjectsController@getSetup'
    ]);

//    Route::post('/dashboard/realestate/projects/setup/{id}/loo-template', [
//        'as' => 'setup_project_loo_template',
//        'uses' => 'LooTemplateController@store'
//    ]);

    Route::post('/dashboard/realestate/projects/setup-unit-numbers/{id}', [
        'as' => 'setup_unit_numbers',
        'uses' => 'RealEstateProjectsController@postSetupUnitNumbers'
    ]);

    /*
     * Add unit groups to a project
     */
    Route::post('/dashboard/realestate/projects/setup_unit_groups/{id?}', [
        'as' => 'realestate.setup_unit_groups',
        'uses' => 'RealEstateProjectsController@postSetupUnitGroups'
    ]);

    Route::post(
        '/dashboard/realestate/projects/tranche/{id}/{tranche_id?}',
        [
            'as' => 'add_realestate_tranche',
            'uses' => 'RealEstateProjectsController@postTranche'
        ]
    );

    Route::get('/dashboard/realestate/projects/tranche-prices/{tranche_id}', 'RealEstateProjectsController@getTranchePrices');
    Route::get('/dashboard/realestate/projects/tranche-pricing/{tranche_id}', 'RealEstateProjectsController@getTranchePricing');
    Route::put('/dashboard/realestate/projects/tranche/{id}', 'RealEstateProjectsController@putTranche');
    Route::get('/dashboard/realestate/projects/setup-loo-details/{id}', 'RealEstateProjectsController@getSetupLooDetails');
    Route::put(
        '/dashboard/realestate/projects/update-l-o-o-details/{id}',
        [
            'as'=>'update_loo_details',
            'uses'=>'RealEstateProjectsController@putUpdateLOODetails'
        ]
    );

    Route::get('/dashboard/realestate/reservations/edit/{id}', 'RealEstateReservationsController@getEdit');
    Route::put('/dashboard/realestate/reservations/update/{id}', 'RealEstateReservationsController@putUpdate');
    Route::get('/dashboard/realestate/reservations/joint/{id}/{joint_id?}', 'RealEstateReservationsController@getJoint');
    Route::post(
        '/dashboard/realestate/reservations/joint/{id}/{joint_id?}',
        [
            'as'=>'save_joint_purchaser',
            'uses'=>'RealEstateReservationsController@postJoint'
        ]
    );

    Route::post(
        '/dashboard/realestate/reservations/forfeit/{id}',
        [
            'as'=>'unit_forfeiture',
            'uses'=>'RealEstateReservationsController@postForfeit'
        ]
    );

    Route::post(
        '/dashboard/realestate/reservations/reverse-forfeiture/{id}',
        [
            'as'=>'real_estate_reverse_forfeiture',
            'uses'=>'RealEstateReservationsController@postReverseCancelled'
        ]
    );

    Route::post(
        '/dashboard/realestate/reservations/transfer/{id}',
        [
            'as'=>'unit_transfer',
            'uses'=>'RealEstateReservationsController@postTransfer'
        ]
    );

    Route::post(
        '/dashboard/realestate/reservations/delete/{id}',
        [
            'as'=>'delete_holding',
            'uses'=>'RealEstateReservationsController@postDelete'
        ]
    );
    Route::get(
        '/dashboard/realestate/reservations/generate-and-view-loo/{holding_id}',
        [
            'as'=>'generate_view_loo',
            'uses'=>'RealEstateReservationsController@getGenerateAndViewLoo'
        ]
    );

    Route::get('/dashboard/realestate/reservations/generate-and-view-loo-word/{holding_id}', 'RealEstateReservationsController@getGenerateAndViewLooWord');
    Route::get(
        '/dashboard/realestate/reservations/generate-and-upload-loo/{holding_id}',
        [
            'as'=>'generate_upload_loo',
            'uses'=>'RealEstateReservationsController@getGenerateAndUploadLoo'
        ]
    );

    /**
     * Contract variation
     */

    Route::post(
        '/dashboard/realestate/reservations/generate_contract_variation/{loo_id}',
        [
            'as'=>'generate_contract_variation',
            'uses'=>'ContractVariationController@generate'
        ]
    );

    Route::get(
        '/dashboard/realestate/reservations/preview_contract_variation/{loo_id}',
        [
            'as'=>'preview_contract_variation',
            'uses'=>'ContractVariationController@preview'
        ]
    );

    Route::post(
        '/dashboard/realestate/reservations/approve_contract_variation/{variation_id}',
        [
            'as'=>'approve_contract_variation',
            'uses'=>'ContractVariationController@approve'
        ]
    );

    Route::post(
        '/dashboard/realestate/reservations/reject_contract_variation/{variation_id}',
        [
            'as'=>'reject_contract_variation',
            'uses'=>'ContractVariationController@reject'
        ]
    );

    Route::post(
        '/dashboard/realestate/reservations/send_contract_variation_to_clients/{contract_variation_id}',
        [
            'as'=>'send_contract_variation',
            'uses'=>'ContractVariationController@send'
        ]
    );

    Route::get('/dashboard/realestate/reservations/dispatcher', 'RealEstateReservationsController@getDispatcher');

    Route::put(
        '/dashboard/realestate/payments/update/{id}',
        [
            'as'=>'update_realestate_payment',
            'uses'=>'RealEstatePaymentsController@putUpdate'
        ]
    );

    Route::get('/dashboard/realestate/payments/create/{holding_i_d}/{type_slug?}', 'RealEstatePaymentsController@getCreate');

    Route::post('/dashboard/realestate/payments/create/{holding_id}', [
        'as' => 'save_realestate_payment',
        'uses' => 'RealEstatePaymentsController@postCreate'
    ]);

    Route::put('/dashboard/realestate/payments/update-payment-information/{holding_id}', [
        'as' => 'update_realestate_pricing_information',
        'uses' => 'RealEstatePaymentsController@putUpdatePaymentInformation'
    ]);

    Route::post(
        '/dashboard/realestate/payments/refund/{holding_id}',
        [
            'as'=>'re_refund_payment',
            'uses'=>'RealEstatePaymentsController@refund'
        ]
    );


    Route::get('/dashboard/realestate/payments/delete/{id}', 'RealEstatePaymentsController@getDelete');
    Route::get('/dashboard/realestate/payments/send-bc/{id}', 'RealEstatePaymentsController@getSendBc');


    Route::post(
        '/dashboard/realestate/payments-schedules/store/{holding_id}',
        [
            'as'=>'save_realestate_payment_schedule',
            'uses'=>'RealEstatePaymentScheduleController@postStore'
        ]
    );

    Route::post(
        '/dashboard/realestate/payments-type/{holding_id}/add',
        ['as' => 'add_realestate_payment_type', 'uses' => 'RealEstatePaymentsController@addPaymentType']
    );

    Route::delete(
        '/dashboard/realestate/payments-type/{holding_id}/{type}/remove',
        ['as' => 'remove_realestate_payment_type', 'uses' => 'RealEstatePaymentsController@deletePaymentType']
    );

    Route::put(
        '/dashboard/realestate/payments-type/{holding_id}/{type}/update',
        ['as'=> 'update_realestate_payment_type', 'uses'=>'RealEstatePaymentsController@updatePaymentType']
    );

    Route::delete(
        '/dashboard/realestate/payments-schedules/remove/{id}',
        [
            'as'=>'remove_realestate_payment_schedule',
            'uses'=>'RealEstatePaymentScheduleController@deleteRemove'
        ]
    );

    Route::delete(
        '/dashboard/realestate/payments-schedules/bulk-remove/{holding_id}',
        [
            'as'=>'bulk_delete_payment_schedules',
            'uses'=>'RealEstatePaymentScheduleController@deleteBulkRemove'
        ]
    );

    Route::put(
        '/dashboard/realestate/payments-schedules/update/{id}',
        [
            'as'=>'update_realestate_payment_schedule',
            'uses'=>'RealEstatePaymentScheduleController@putUpdate'
        ]
    );
    Route::post(
        '/dashboard/realestate/payments-schedules/send-bulk-reminders',
        [
            'as'=>'send_bulk_upcoming_payment_reminders',
            'uses'=>'RealEstatePaymentScheduleController@postSendBulkReminders'
        ]
    );


    Route::post(
        '/dashboard/realestate/payments-schedules/send-overdue-bulk-reminders',
        [
            'as'=>'send_bulk_overdue_payment_reminders',
            'uses'=>'RealEstatePaymentScheduleController@postSendOverdueBulkReminders'
        ]
    );

    Route::put(
        '/dashboard/realestate/loos/regenerate-and-update/{id}',
        [
            'as'=>'regenerate_loo_and_update',
            'uses'=>'LOOsController@putRegenerateAndUpdate'
        ]
    );


    Route::post(
        '/dashboard/realestate/loos/upload/{loo_id}',
        [
            'as'=>'upload_loo',
            'uses'=>'LOOsController@postUpload'
        ]
    );

    Route::get('/dashboard/realestate/unitsizes', [
        'as'=>'unitsizes',
        'uses'=>'RealEstateUnitSizeController@index'
    ]);

    Route::get('/dashboard/realestate/unitsizes/create/{id?}', [
        'as'=>'unitsizes.create',
        'uses'=>'RealEstateUnitSizeController@create'
    ]);

    Route::post('/dashboard/realestate/unitsizes/store/{id?}', [
        'as'=>'unitsizes.store',
        'uses'=>'RealEstateUnitSizeController@store'
    ]);
});

Route::post(
    '/dashboard/realestate/reservations/upload-sales-agreement/{holding_id}',
    [
        'as'=>'upload_sales_agreement',
        'uses'=>'RealEstateReservationsController@postUploadSalesAgreement'
    ]
)->middleware('allow:realestate:create:sales-agreement');

Route::group(['middleware' => 'allow:realestate:reports'], function () {
    Route::get('/dashboard/realestate/reports/', 'RealEstateReportsController@getIndex');
    Route::post('/dashboard/realestate/reports/export-project-cash-flows', 'RealEstateReportsController@postExportProjectCashFlows');
    Route::get('/dashboard/realestate/reports/forfeited-units', 'RealEstateReportsController@getForfeitedUnits');
    Route::get('/dashboard/realestate/reports/clients-summaries', 'RealEstateReportsController@getClientsSummaries');
    Route::get('/dashboard/realestate/reports/clients-overdue', 'RealEstateReportsController@getClientsOverdues');

    Route::post(
        '/dashboard/realestate/reports',
        [
            'as'=>'realestate_report_path',
            'uses'=>'RealEstateReportsController@exportClientContacts'
        ]
    );

    Route::post('/dashboard/realestate/reports/payment-summary', [
        'as'=>'realestate_payments_summary_path',
        'uses'=>'RealEstateReportsController@exportRealEstatePaymentsSummaries',
    ]);

    Route::post('/dashboard/realestate/reports/project-payment-summary', [
        'as'=>'realestate_project_payments_summary_path',
        'uses'=>'RealEstateReportsController@exportProjectPaymentsSummaries',
    ]);

    Route::post('/dashboard/realestate/reports/clients-tranche-report', [
        'as'=>'clients_tranche_report_path',
        'uses'=>'RealEstateReportsController@exportClientsTrancheReport',
    ]);

    Route::post('/dashboard/realestate/reports/reservation-summary-report', [
        'as'=>'reservation_summary_report_path',
        'uses'=>'RealEstateReportsController@exportReservationSummaryReport',
    ]);

    Route::post('/dashboard/realestate/reports/real_estate_inflows_export', [
        'as' => 'real_estate_inflows_export',
        'uses' => 'RealEstateReportsController@exportRealEstateInflows'
    ]);

    Route::post('/dashboard/realestate/reports/real_estate_commisisons_export', [
        'as' => 'real_estate_commisisons_export',
        'uses' => 'RealEstateReportsController@exportRealEstateCommissions'
    ]);

    Route::post('/dashboard/realestate/reports/real_estate_holdings_summary_export', [
        'as' => 'real_estate_holdings_summary_export',
        'uses' => 'RealEstateReportsController@exportRealEstateHoldingSummary'
    ]);

    Route::post('/dashboard/realestate/reports/real_estate_monthly_holdings_summary_export', [
        'as' => 'real_estate_monthly_holdings_summary_export',
        'uses' => 'RealEstateReportsController@exportRealEstateMonthlyHoldingSummary'
    ]);

    Route::post('/dashboard/realestate/reports/deposit-payment-expiry-notifications-report', [
        'as'=>'deposit_payment_expiry_notifications_report_path',
        'uses'=>'RealEstateReportsController@exportDepositPaymentExpiryNotificationsReport',
    ]);

    Route::post('/dashboard/realestate/reports/client_with_overdue_payments', [
        'as' => 'real_estate_client_with_overdue_payments',
        'uses' => 'RealEstateReportsController@exportClientWithOverduePayments'
    ]);

    Route::post('/dashboard/realestate/project/{projectId}/cash-flows', [
        'as' => 'view_project_cash_flows',
        'uses' => 'ExportController@exportProjectCashFlows'
    ]);
});

Route::group(['middleware' => 'allow:realestate:statements'], function () {
    Route::post(
        '/dashboard/realestate/statements/campaigns/send/{campaignId}',
        [
            'as' => 'send_realestate_statement_campaign',
            'uses' => 'RealEstateStatementsController@sendCampaign'
        ]
    );

    Route::post('/dashboard/realestate/statements/campaigns/send/{campaignId}', [

        'as' => 'send_realestate_statement_campaign',
        'uses' => 'RealEstateStatementsController@sendCampaign'
    ]);

    Route::get('dashboard/realestate/statements/', 'RealEstateStatementsController@getIndex');
    Route::post(
        'dashboard/realestate/statements/create-campaign',
        [
            'as'=>'create_realestate_statement_campaign',
            'uses'=>'RealEstateStatementsController@postCreateCampaign'
        ]
    );

    Route::get('dashboard/realestate/statements/campaigns/{id}', 'RealEstateStatementsController@getCampaigns');
    Route::post(
        'dashboard/realestate/statements/add-client-to-campaign',
        [
            'as'=>'add_client_to_realestate_statement_campaign',
            'uses' => 'RealEstateStatementsController@postAddClientToCampaign'
        ]
    );

    Route::post('/dashboard/investments/statements/campaigns/close/{campaignId}', [        'as' => 'close_realestate_statement_campaign',        'uses' => 'RealEstateStatementsController@closeCampaign'    ]);

    Route::post('/dashboard/investments/statements/campaigns/close/{campaignId}', [

        'as' => 'close_realestate_statement_campaign',
        'uses' => 'RealEstateStatementsController@closeCampaign'
    ]);

    Route::post('/dashboard/investments/statements/campaigns/campaign_message/{campaignId}', [
        'as' => 'store_realestate_campaign_message',
        'uses' => 'RealEstateStatementsController@storeCampaignMessage'
    ]);
});

Route::group(['middleware' => 'allow:realestate:send-client-documents'], function () {
    Route::post(
        '/dashboard/realestate/clients/mail-statement/{client_id}',
        [
            'as'=>'mail_realestate_statement',
            'uses'=>'RealEstateClientsController@postMailStatement'
        ]
    );

    Route::get(
        '/dashboard/realestate/clients/mail-payment-plan/{client_id}',
        [
            'as'=>'mail_payment_plan',
            'uses'=>'RealEstateClientsController@getMailPaymentPlan'
        ]
    );

    Route::post('/dashboard/realestate/loos/send/{id}', [
        'as'=>'send_loo_to_client',
        'uses'=>'LOOsController@postSend'
    ]);

    Route::post(
        '/dashboard/realestate/loos/send-l-o-o-authorization-form/{id}',
        [
            'as'=>'send_loo_authorization_form',
            'uses'=>'LOOsController@postSendLOOAuthorizationForm'
        ]
    );
});

Route::group(['middleware' => 'allow:realestate:view-commissions'], function () {
    Route::get('/dashboard/realestate/commissions/', 'RealEstateCommissionsController@getIndex');

    Route::get('/dashboard/realestate/commissions/for-unit/{unit_holding_id}', [
        'as'=> 'commission_for_unit',
        'uses'=>'RealEstateCommissionsController@getForUnit'
    ]);

    Route::get('/dashboard/realestate/commissions/payment/{recipient_id}', 'RealEstateCommissionsController@getPayment');
});

Route::group(['middleware' => 'allow:realestate:create-commissions'], function () {
    Route::post(
        '/dashboard/realestate/commissions/rates/rate',
        [
            'as'=>'create_realestate_commission_rate',
            'uses'=>'RealEstateCommissionRatesController@postRate'
        ]
    );

    Route::post(
        '/dashboard/realestate/commissions/rates/rate/{id}',
        [
            'as'=>'update_realestate_commission_rate',
            'uses'=>'RealEstateCommissionRatesController@putRate'
        ]
    );

    Route::post('/dashboard/realestate/commissions/bulk-payments/{id}', [
        'as'=>'bulk_realestate_commission_payment',
        'uses'=>'RealEstateCommissionsController@postBulkPayments'
    ]);

    Route::post('/dashboard/realestate/commissions/schedule/{id}', [
        'as'=>'update_for_schedule',
        'uses'=>'RealEstateCommissionsController@postSchedule'
    ]);

    Route::post('/dashboard/realestate/commissions/award/{holding_id}', [
        'as'=>'award_real_estate_holding_commission',
        'uses'=>'RealEstateCommissionsController@awardCommission'
    ]);

    Route::post('/dashboard/realestate/commissions/award_additional_commission/{id?}', [
        'as'=>'real_estate_award_additional_realestate_commission',
        'uses'=>'RealEstateCommissionsController@awardAdditionCommission'
    ]);
});

Route::group(['middleware' => 'allow:realestate-approvals:loo'], function () {
    Route::post('/dashboard/realestate/loos/approve/{id}', [
        'as'=>'pm_approve_loo',
        'uses'=>'LOOsController@postApprove'
    ]);

    Route::post('/dashboard/realestate/loos/reject/{id}', [
        'as'=>'pm_reject_loo',
        'uses'=>'LOOsController@postReject'
    ]);
});

Route::group(['middleware' => 'allow:realestate:manage-advocates'], function () {
    Route::get('/dashboard/realestate/advocates', 'AdvocatesController@index');
    Route::get('/dashboard/realestate/advocates/create', 'AdvocatesController@create');
    Route::get('/dashboard/realestate/advocates/{advocate}', 'AdvocatesController@show');
    Route::get('/dashboard/realestate/advocates/{advocate}/edit', 'AdvocatesController@edit');
    Route::post('/dashboard/realestate/advocates', [
        'as'=>'create_advocate',
        'uses'=>'AdvocatesController@store'
    ]);
    Route::put('/dashboard/realestate/advocates/{advocate}', [
        'as'=>'update_advocate',
        'uses'=>'AdvocatesController@update'
    ]);
    Route::get('/dashboard/realestate/advocates/{advocate}/status', [
        'as'=>'change_advocate_active_status',
        'uses'=>'AdvocatesController@toggleActive'
    ]);
});
