<?php

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index');

/**
 * Dashboard
 */
Route::get('dashboard', 'HomeController@dashboard');

Route::get('/logout', 'LoginController@destroy');

/*
|--------------------------------------------------------------------------
| Setup
|--------------------------------------------------------------------------
| Application setup
*/
Route::get('/dashboard/setup', 'SetupController@index');

/*
|--------------------------------------------------------------------------
| Feedback
|--------------------------------------------------------------------------
| Application feedback
*/
Route::get('/dashboard/feedback/create', 'FeedbackController@create');

Route::post('/dashboard/feedback/store', [
    'as' => 'save_application_feedback',
    'uses' => 'FeedbackController@store'
]);

Route::get('/dashboard/profile', 'ProfileController@show');

Route::get('/dashboard/user/sudo/', [
    'as' => 'set_sudo_mode',
    'uses' => 'ProfileController@confirmSudoMode'
]);

Route::post('/dashboard/user/sudo/{intended?}', [

    'as' => 'set_sudo_mode',
    'uses' => 'ProfileController@setSudoMode'
]);

Route::post('/dashboard/profile', [
    'as' => 'edit_profile_path',

    'uses' => 'ProfileController@editUser'
]);


Route::group(['middleware' => 'allow:user_management'], function () {
      /*
      |--------------------------------------------------------------------------
      | User and Access management
      |--------------------------------------------------------------------------
      |
      | Routes to user/access management
      */
    Route::get('/dashboard/users', 'UsersController@index');


    Route::get('/dashboard/users/details/{id}', 'UsersController@details');

    //access control
    Route::post('/dashboard/users/roles/assign', [
        'as' => 'assign_user_role',
        'uses' => 'UserAccessController@assignUserRole'
    ]);

    Route::post('/dashboard/users/permissions/assign', [
        'as' => 'assign_user_permission',
        'uses' => 'UserAccessController@assignUserPermission'
    ]);

    Route::get('/dashboard/users/permissions', 'UsersController@permissions');

    Route::get('/dashboard/users/permissions/details/{id}', 'UsersController@permissionDetail');

    Route::get('/dashboard/users/roles', 'UsersController@roles');

    Route::get('/dashboard/users/roles/details/{id}', 'UsersController@roleDetail');

    Route::get('/dashboard/users/roles/add/{id?}', 'UserAccessController@getAddRole');

    Route::post('/dashboard/users/roles/add/{id?}', [
        'uses' => 'UserAccessController@addRole'
    ]);

    Route::post('/dashboard/users/roles/grant', [
        'uses' => 'UserAccessController@grantRolePermission'
    ]);

    Route::post('/dashboard/users/roles/revoke', [
        'uses' => 'UserAccessController@revokeRolePermission'
    ]);

    Route::post('/dashboard/users/roles/removemember', [
        'uses' => 'UserAccessController@removeMember'
    ]);

    Route::get('/dashboard/users/add/{id?}', 'UsersController@create');

    Route::post('/dashboard/users/add/{id?}', [
        'as' => 'add_user_path',
        'uses' => 'UsersController@store'
    ]);

    Route::get('/dashboard/users/reset/{id}', 'RemindersController@sendresetlink');

    Route::get('/dashboard/users/deactivate/{id}', 'UsersController@deactivate');

    Route::get('/dashboard/users/reactivate/{id}', 'UsersController@reactivate');

    Route::get('/dashboard/users/resendlink/{id}', 'UsersController@resendLink');
});


/*
* Client's user accounts
*/
Route::group(['middleware' => 'allow:client_user_management'], function () {

    Route::get('/dashboard/users/clients', 'ClientUserController@index');

    Route::get('/dashboard/users/client/add/{id?}', 'ClientUserController@create');

    Route::post('/dashboard/users/client/add/{id?}', [
        'uses' => 'ClientUserController@store'
    ]);

    Route::get('/dashboard/users/client/details/{id}', 'ClientUserController@show');

    Route::get('/dashboard/users/client/{id}/reactivate', 'ClientUserController@reactivate');

    Route::get('/dashboard/users/client/{id}/deactivate', 'ClientUserController@deactivate');

    Route::get('/dashboard/users/client/resendlink/{id}', 'ClientUserController@resendLink');

    Route::get('/dashboard/users/client/resend_password/{id}', 'ClientUserController@resetPassword');

    Route::post('/dashboard/users/client/{id}/assign', [
        'as' => 'client_user_assign_path',
        'uses' => 'ClientUserController@assignAccess'
    ]);

    Route::post('/dashboard/users/client/{id}/signature', [
        'as' => 'client_user_assign_signature',
        'uses' => 'ClientUserController@assignSignature'
    ]);

    Route::post('/dashboard/users/client/{id}/assign/fa', [
        'as' => 'fa_user_assign_path',
        'uses' => 'ClientUserController@assignFa'
    ]);

    Route::get('dashboard/users/{user_id}/signature/{signature_id}/remove', 'ClientUserController@removeSignature');


    Route::get('/dashboard/users/client/revoke/{access_id}', 'ClientUserController@revokeAccess');

    Route::get('/dashboard/users/client/revoke/{fa_id}/fa/{user_id}', 'ClientUserController@revokeFa');

    Route::post('/dashboard/users/clients', [
        'as' => 'get_export_user_account_path',
        'uses' => 'ClientUserController@exportUserAccounts'
    ]);
});
