<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Http\Controllers\Api\Portfolio\EquitiesController;
use App\Http\Controllers\PortfolioSecurityController;

Route::group(['before' => 'auth'], function () {

    Route::get('/user/permissions/{permission}', function ($permission) {
        return response(['status' => app('crims.authorizer')->authorize($permission, [], false)]);
    });

    //companies
    Route::get('/companies', 'ApiController@getCompanies');

    //investments
    Route::get('investments/applications', 'ApiController@getApplications');

    Route::get('users', 'ApiController@getusers');

    Route::get('users/clients', 'ApiController@getClientUsers');

    Route::get('permissions', 'ApiController@getPermissions');

    Route::get('roles', 'ApiController@getRoles');

    //portfolio
    Route::get('portfolio/institutions', 'ApiController@getPortfolioInstitutions');

    Route::get('portfolio/custodial/balance/{id}', 'ApiController@custodialBalance');

    //products
    Route::get('products/all', 'ApiController@getProducts');

    Route::get('login/check', 'ApiController@loginCheck');

    //notifications
    Route::get('notifications/unread_count', 'ApiController@getUnreadCount');

    Route::get('investments/commission/rates/{product_id}', 'ApiController@getCommissionRatesForProduct');

    Route::get('investments/commission/rates/{product_id}/recipient/{recipient_id}/{date?}', 'ApiController@getCommissionRatesForProductAndRecipient');

    Route::post('investments/commission/recipient_product_rate', 'Api\CommissionController@getRecipientProductCommissionRate');
});


/*
 * API V2
 * Moved to independent controllers, uses transformers and can use server side pagination
 * All controllers are in the controllers/Api folder, controller names are namespaced
 */
Route::group(['before' => 'auth', 'namespace' => 'Api'], function () {

    Route::get('accounts/{acc_id}/compatibility', 'AccountsController@compatibility');

    Route::post('accounts/{acc_id}/summary', 'AccountsController@summary');

    Route::get('documents', 'DocumentsController@index');

    Route::get('documents/{module}/types/{slug}', 'DocumentsController@documentsOfType');

    Route::get('applications', 'InvestmentApplicationController@applications');

    Route::get('clients', 'ClientsController@index');

    Route::post('client/signature', 'ClientsController@signature');

    Route::resource('clients/bank-instruction', 'ClientBankInstructionController');

    Route::get('users/clients', 'UserController@clients');

    Route::get('clients/all', 'ClientsController@index');

    Route::get('/clients/search', 'ClientsController@search');

    Route::get('/clients/payments-balance/{client_id}/products/{id}', 'ClientsController@productPaymentsBalance');

    Route::get('clients/summary/{product_id}', 'ClientsController@summary');

    Route::get('investments/approvals', 'ApprovalsController@listClientApprovals');

    Route::get('investments/client-instructions/rollover', 'ClientInstructionController@rollovers');

    Route::get('investments/client-instructions/topup', 'ClientInstructionController@topups');

    Route::get('investments/client-instructions/application', 'ClientInstructionController@applications');

    Route::get('investments/client-instructions/unit_fund_investment_instruction', 'ClientInstructionController@unitFundInstructions');

    Route::get('investments/client-instructions/utility_billing_instructions', 'ClientInstructionController@utilityBillingInstructions');

    Route::get('investments/client-instructions/commission-paid-rate/{type_id}/{type_name}', 'ClientInstructionController@getCommissionRate');

    //Investment Payment Schedules
    Route::get('investments/investment-payment-schedules/{investment_id}', 'Investments\InvestmentPaymentScheduleController@index');

    Route::get('investments/investment-payment-schedules/edit/{schedule_id}', 'Investments\InvestmentPaymentScheduleController@getSchedule');

    Route::post('investments/investment-payment-schedules/update/{schedule_id}', 'Investments\InvestmentPaymentScheduleController@updateSchedule');

    Route::get('investments/investment-payment-schedules/listing/all_schedules', 'Investments\InvestmentPaymentScheduleController@listing');


    // documents
    Route::post('/client-instructions/approval-document/upload', 'ClientInstructionController@approvalTransactionDocument');

    Route::get('/instruction/{approval}/source-documents', 'ClientInstructionController@fetchTransactionDocument');

    Route::get('/instruction/source-document/{id}/delete', 'ClientInstructionController@deleteTransactionDocument');

    /*
     * Client KYC Validation
     */

    Route::get('/client/kyc-validation/check-validated/{id}', 'ClientUploadedKycController@checkValidated');

    Route::post('/client/kyc-validation/validate/{id}', 'ClientUploadedKycController@validateClient');

    Route::post('/client/kyc-validation/request-proof/{id}', 'ClientUploadedKycController@requestProof');

    /*
     * Real Estate Instructions
     */

    Route::get('realestate/realestate-instructions/application', 'RealEstate\RealEstateInstructionsController@applications');

    /**
     * Risky clients
     */
    Route::get('clients/risky', 'Client\RiskyClientsController@allRiskyClients');

    Route::get('clients/risky/all', 'Client\RiskyClientsController@index');

    Route::get('clients/risky/search', 'Client\RiskyClientsController@search');

    Route::get('clients/risky/status', 'Client\RiskyClientsController@riskStatus');

    /**
     * Portfolio api routes
     */
    Route::post('portfolio/maturityprofiledata/{period}/{currency_id}', 'PortfolioController@maturityProfile');

    Route::resource('portfolio/asset-classes', 'Portfolio\AssetClassController');

    Route::resource('portfolio/sub-asset-classes', 'Portfolio\SubAssetClassController');

    Route::get('portfolio/{assetCLassId}/sub-asset-classes', 'Portfolio\SubAssetClassController@subAssetClasses');

    Route::post('portfolio/{assetCLassId}/sub-asset-classes/add', 'Portfolio\SubAssetClassController@saveSubAssetClasses');

    Route::post('portfolio/{subAssetClassId}/edit', 'Portfolio\SubAssetClassController@editSubAssetClass');


    /**
     * Portfolio Securities routes
     */
//    Route::resource('/portfolio/securities', 'Portfolio\SecurityController');

    Route::get('investments', 'InvestmentsController@getInvestments');

    Route::get('investments/businessconfirmations', 'InvestmentsController@getBusinessConfirmations');

    Route::get('investments/accounts-cash', 'AccountCashController@index');

    Route::get('portfolio/custodial/transfer-mismatch/{id}', 'Portfolio\CustodialTransactionController@index');

    Route::post('portfolio/custodial/transfer-mismatch/calculate', 'Portfolio\CustodialTransactionController@calculate');

    Route::post('portfolio/custodial/transfer-mismatch/transfer/{id}', 'Portfolio\CustodialTransactionController@transfer');

    Route::get('investments/all-custodial-accounts', 'CustodialAccountController@getCustodialAccounts');

    Route::get('investments/short-name/all-custodial-accounts', 'CustodialAccountController@getCustodialAccountsShort');

    Route::get('investments/client-payments', 'ClientPaymentController@index');

    Route::get('investments/payment-instructions', 'PaymentInstructionsController@index');

    Route::get('investments/client-payments/client/{client_id}', 'ClientPaymentController@clientPayments');

    Route::get('investments/interest', 'InvestmentsController@interestPayments');

    Route::get('investments/interest/{id}/calculate', 'InvestmentsController@calculate');

    Route::get('investments/interest/excel', 'ApiExportController@exportInterestPayments');

    Route::get('system/closed_periods', 'ClosedPeriodController@closedPeriods');

    Route::get('system/exemptions', 'ExemptionController@exemptions');

    Route::get('products', 'ProductController@index');

    Route::get('investments/products/{type?}', 'InvestmentApplicationController@getProducts');

    Route::get('investments/{id}', 'InvestmentsController@getInvestmentById');
    Route::get('investments/{id}/net-interest-at-date', 'InvestmentsController@getNetInterestAtDate');
    Route::get('investments/{id}/total-value-at-date', 'InvestmentsController@getTotalValueOfInvestmentAtDate');
    Route::get('investments/{id}/penalty', 'InvestmentsController@penalty');

    Route::get('deductions', 'DeductionsController@getDeductions');

    Route::get('withdrawals', 'InvestmentsController@getWithdrawals');
    Route::get('client/withdrawals', 'InvestmentsController@clientWithdrawals');

    Route::get('investments/taxation/{product_id}', 'ClientTaxController@taxPerClientForPeriod');

    Route::get('investments/taxation/due/{product}', 'ClientTaxController@taxDue');

    Route::get('investments/interest-rates/{investment_id}', 'InvestmentsController@fetchInterestRates');

    Route::get('deductions/excel', 'ApiExportController@exportDeductions');

    Route::get('investments/commission/per_month/{product_id}', 'CommissionController@perMonth');

    Route::get('investments/commission/recipients', 'CommissionController@recipients');

    Route::get('investments/commission_recipients/getRecipients', 'CommissionController@getCommissionRecipients');

    Route::get('investments/commission_recipients/list', 'CommissionController@commissionRecipients');

    Route::get('investments/commission_recipients/getRecipientPositions/{fa_id}', 'CommissionController@getCommissionRecipientPositions');

    Route::get('investments/commission/payment-dates', 'CommissionController@paymentDates');

    Route::get('recipients/{recipient_id}/zero-commission-rate', 'CommissionController@hasZeroCommissionRate');

    Route::get('investments/commission/{product_id}', 'CommissionController@index');

    Route::get('investments/commission/overrides/recipient/{recipient_id}/currency/{currency_id}/start/{start}/end/{end}', 'CommissionController@getRecipientOverrides');

    Route::get('investments/commission/additional_commissions/recipient/{recipient_id}/currency/{currency_id}/start/{start}/end/{end}', 'CommissionController@getRecipientAdditionalCommission');

    Route::get('investments/fund_manager/switch/{id}', 'FundManagerController@change');

    Route::get('fund-managers-list', 'FundManagerController@getList');

    Route::get('applications', 'InvestmentApplicationController@applications');

    Route::get('/investments/analytics/cost-value/{type}/{entity_id}', 'InvestmentAnalyticsController@getCostValue');
    Route::get('/investments/analytics/market-value/{type}/{entity_id}', 'InvestmentAnalyticsController@getMarketValue');
    Route::get('/investments/analytics/custody-fees/{type}/{entity_id}', 'InvestmentAnalyticsController@getCustodyFees');
    Route::get('/investments/analytics/withholding-tax/{type}/{entity_id}', 'InvestmentAnalyticsController@getWithholdingTax');
    Route::get('/investments/analytics/residual-income/{type}/{entity_id}', 'InvestmentAnalyticsController@getResidualIncome');
    Route::get('/investments/analytics/weighted-rate/{type}/{entity_id}', 'InvestmentAnalyticsController@getWeightedRate');
    Route::get('/investments/analytics/weighted-tenor/{type}/{entity_id}', 'InvestmentAnalyticsController@getWeightedTenor');
    Route::get('/investments/analytics/persistence/{type}/{entity_id}', 'InvestmentAnalyticsController@getPersistence');
    Route::get('/investments/analytics/export/{type}/{entity_id}', 'InvestmentAnalyticsController@export');

    Route::get('/investments/updates/interest-rates', 'InterestRateUpdatesController@index');

    Route::get('/investments/bulk/messages', 'ClientsBulkMessagesController@index');

    Route::get('/investments/updates/interest-rates/{update_id}', 'InterestRateUpdatesController@getRates');

    Route::get('/banks', 'BanksController@index');
    Route::get('/all/banks', 'BanksController@allBanks');

    Route::get('/banks/{bank}/branches', 'BanksController@branches');
    Route::get('/banks/{bank}/bank-branches', 'BanksController@bankBranches');

    Route::get('/banks/{bank}/branches-list', 'BanksController@branchesList');

    Route::get('notifications', 'NotificationsController@notifications');

    Route::get('notifications/unread', 'NotificationsController@getAllUnread');

    Route::get('dashboard/unread-notifications', 'NotificationsController@getAllUnread');

    Route::get('notifications/unread/count', 'NotificationsController@getUnreadCount');

    Route::get('activity_log', 'ActivityLogController@index');

    Route::get('activity_log/client_transactions', 'ActivityLogController@clientTransactions');

    Route::get('activity_log/portfolio_transactions', 'ActivityLogController@portfolioTransactions');

    Route::get('/investments/statements/campaign/{campaign_id}', 'StatementController@campaignItems');

    Route::get('/investments/statements/campaign/{campaign_id}/missing', 'StatementController@missingClients');

    Route::get('/portfolio/investments/{currency_id}', 'PortfolioInvestmentController@investments');

    Route::get('/portfolio/approvals', 'PortfolioInvestmentController@approvals');

    Route::get('/portfolio/custodial/accounts/remaining/{id}', 'CustodialAccountController@getRemaining');

    Route::get('/portfolio/custodial/accounts/transactions/{id}', 'Portfolio\CustodialTransactionController@transactions');

    Route::get('/portfolio/suspense-transactions/list/{id}', 'Portfolio\SuspenseTransactionController@index');
    Route::get('/portfolio/suspense-transactions/details/{id}', 'Portfolio\SuspenseTransactionController@details');
    Route::get('/portfolio/suspense-transactions/payment_to/categories', 'Portfolio\SuspenseTransactionController@getPaymentToDetails');
    Route::post('/portfolio/suspense-transactions/search_transaction', 'Portfolio\SuspenseTransactionController@searchSuspenseTransaction');
    Route::post('/portfolio/custodial-transactions/search_transaction', 'Portfolio\CustodialTransactionController@searchCustodialTransaction');

    Route::get('/portfolio/custodials/exchange_rates', 'CustodialAccountController@custodialExchangeRates');

    Route::get('/portfolio/custodial/account-balance-trail/{id}', 'CustodialAccountController@accountBalanceTrail');

    Route::get('/products/{product_id}/custodial-accounts', 'CustodialAccountController@getCustodialAccountsForProduct');
    Route::get('/projects/{project_id}/custodial-accounts', 'CustodialAccountController@getCustodialAccountsForProject');
    Route::get('/share_entities/{entity_id}/custodial-accounts', 'CustodialAccountController@getCustodialAccountsForEntity');
    Route::get('/unit-funds/{fund}/custodial-accounts', 'CustodialAccountController@getCustodialAccountsForUnitFund');
    Route::get('/fund-managers/{manager}/custodial-accounts-list', 'CustodialAccountController@getCustodialAccountsForFundManager');

    Route::get('/portfolio/summary/{id}', 'PortfolioController@summary');

    Route::get('/portfolio/summary_totals/{id}', 'PortfolioController@summaryTotals');

    Route::get('/portfolio/analysis/currency/{id}', 'PortfolioController@depositAnalysis');

    Route::get('/portfolio/analysis/cash/{id}', 'PortfolioController@cashDepositAnalysis');

    Route::get('/portfolio/activestrategy', 'ActiveStrategyController@securities');

    Route::get('/portfolio/activestrategy/allocation', 'ActiveStrategyController@assetAllocation');

    Route::get('/portfolio/asset/allocation', 'ActiveStrategyController@assetAllocation');

    Route::get('/portfolio/activestrategy/{id}', 'ActiveStrategyController@holdings');
    Route::get('/portfolio/activestrategy/{id}/dividends/{type}', 'ActiveStrategyController@dividends');

    Route::get('/portfolio/activestrategy/{id}/sales', 'ActiveStrategyController@shareSales');

    Route::get('/portfolio/instructions', 'PortfolioInstructionsController@index');

    Route::get('/realestate/projects', 'RealEstateController@projects');

    Route::get('/realestate/clients', 'RealEstateController@clients');

    Route::get('/realestate/clients/{id}/units', 'RealEstateController@clientUnits');

    Route::get('/realestate/projects/{id}/units', 'RealEstateController@projectUnits');

    Route::get('/realestate/projects/{cid}/{pid}/units', 'RealEstateController@clientProjectUnits');

    Route::get('/realestate/projects/{id}/cancelled-units', 'RealEstateController@projectCancelledUnits');

    Route::resource('/realestate/payments', 'RealEstatePaymentController', [
        'only' => 'index'
    ]);

    Route::get('/realestate/payment-plans', 'RealEstateTrancheController@getPaymentPlans');
    Route::get('/realestate/projects/{id}/tranche-pricings', 'RealEstateTrancheController@getAllTrancheUnitPricings');

    Route::get('/realestate/payment-schedules/overdue', 'RealEstatePaymentSchedulesController@overdue');

    Route::get('/realestate/payment-schedules/{unit_id}/spacing/{spacing}', 'RealEstatePaymentSchedulesController@generatePaymentSchedules');


    Route::resource('/realestate/payments-schedules', 'RealEstateScheduledPaymentController', [
        'only' => 'index'
    ]);

    Route::get('/realestate/statements/campaigns/{campaign_id}', 'RealEstateStatementController@campaignItems');

    Route::get('/realestate/statements/campaigns/{campaign_id}/missing', 'RealEstateStatementController@missingClients');

    // Real Estate
    Route::get('/realestate/commissions/per_month', 'RealEstateCommissionController@perMonth');

    Route::get('/realestate/commissions', 'RealEstateCommissionController@index');

    Route::get('/realestate/commissions/payment-dates', 'RealEstateCommissionController@paymentDates');

    Route::post('/realestate/get_recipient_commission_rate', 'RealEstateCommissionController@getRecipientCommissionRate');

    Route::get('/realestate/loos', 'LOOsController@index');
    Route::get('/realestate/loos/export/', 'LOOsController@export');

    Route::get('/realestate/forfeiture_notices', 'RealestateForfeitureNoticeController@index');

    Route::get('/realestate/sales-agreements', 'SalesAgreementsController@index');

    Route::get('/realestate/unitsizes', 'RealestateUnitSizeController@getRealestateUnitSizes');

    Route::get('/realestate/advocates', 'AdvocatesController@index');

    Route::post('/realestate/units/reserve', 'RealEstate\RealEstateUnitsController@postReserve');

    // Coop Clients
    Route::get('coop/clients', 'CoopClientsController@index');
    Route::get('coop/clients/search', 'CoopClientsController@search');

    // Coop Memberships
    Route::get('coop/memberships', 'CoopMembershipFormsController@index');
    Route::get('coop/memberships/search', 'CoopMembershipFormsController@search');

    // Coop Payments
    Route::get('coop/payments', 'CoopPaymentsController@index');
    Route::get('coop/clients/{client_id}/payments', 'CoopPaymentsController@getClientPayments');

    // Shares
    Route::get('shares/shareholders', 'ShareHolderController@index');
    Route::get('shares/shareholders/redemption/holder/{holder_id}/date/{date?}', 'ShareHolderController@getShareHolderRedemption');

    Route::post('/shares/shareholder/registration', 'ShareHolderController@postRegister');

    Route::get('/shares/statements/campaigns/{campaign_id}', 'SharesStatementController@campaignItems');
    Route::get('/shares/statements/campaigns/{campaign_id}/missing', 'SharesStatementController@missingClients');

    Route::get('shares/businessconfirmations', 'SharesController@getBusinessConfirmations');
    Route::get('shares/shareholders/{id}/shares', 'SharesController@getShareHoldingsForShareHolder');

    Route::get('shares/payments', 'SharesPaymentsController@index');
    Route::get('shares/payments/shareholders/{holder_id}', 'SharesPaymentsController@shareholderPayments');

    Route::get('shares/sales', 'SharesSalesOrderController@index');
    Route::get('shares/sales/shareholders/{holder_id}', 'SharesSalesOrderController@shareholderSales');
    Route::post('shares/sales/purchase-settlement', 'SharesSalesOrderController@store');
    Route::post('shares/sales/purchase-unmatch', 'SharesSalesOrderController@unmatch');

    Route::get('shares/purchases', 'SharesPurchaseOrderController@index');
    Route::get('shares/purchases/shareholders/{holder_id}', 'SharesPurchaseOrderController@shareholderPurchases');
    Route::get('shares/getShareCommissionRates/{recipient_id}', 'SharesPurchaseOrderController@getShareCommissionRates');

    Route::post('shares/purchase-orders/send-payment-reminders', 'SharesPurchaseOrderController@sendReminders');

    Route::get('shares/commissions', 'Shares\ShareCommissionController@index');

    Route::get('shares/commissions/totals/recipient/{recipient_id}/start/{start}/end/{end}', 'Shares\ShareCommissionController@getCommissionTotals');

    Route::get('shares/commissions/schedules/recipient/{recipient_id}/start/{start}/end/{end}', 'Shares\ShareCommissionController@getCommissionSchedules');

    Route::get('shares/commissions/overrides/recipient/{recipient_id}/start/{start}/end/{end}', 'Shares\ShareCommissionController@getCommissionOverrides');

    Route::post('shares/get_share_commission_recipient_rate', 'SharesPurchaseOrderController@getShareCommissionRecipientRate');

    Route::post('unit-fund-investment/{id}/edit', 'Unitization\UnitFundInvestmentController@editUnitFundInstruction');

    Route::get('clients/standing-order-types', 'ClientStandingOrdersController@orderTypes');

    Route::resource('client/standing-orders', 'ClientStandingOrdersController');

    Route::group(['before' => 'auth', 'namespace' => 'Ussd'], function () {
        Route::group(['middleware' => 'allow:unittrust:view-funds'], function () {
            Route::resource('ussd/ussd-applications', 'USSDController');
            Route::get('ussd/ussd-applications/send-reminder/{id}', 'USSDController@sendReminder');
        });
    });

    Route::group(['before' => 'auth', 'namespace' => 'Unitization'], function () {

        Route::get('unitization/unit-funds-list', 'UnitFundController@getUnitFunds');

        Route::group(['middleware' => 'allow:unittrust:view-funds'], function () {
            Route::resource('unitization/unit-funds', 'UnitFundController');

            Route::resource('unitization/unit-funds/{fund}/summaries', 'UnitFundSummaryController');

            Route::get('unitization/unit-funds/{fund}/unit-fund-clients', 'UnitFundClientController@indexForFund');

            Route::get('unitization/unit-funds/{fundId}/unit-fund-list', 'UnitFundTransferController@getFunds');

            Route::get('unitization/unit-funds-all', 'UnitFundController@getUnitFunds');

            Route::get('unitization/share-entity-all', 'UnitFundController@getShareEntities');

            Route::get('unit-fund/fund-categories', 'UnitFundController@fundCategories');

            Route::resource('unitization/unit-funds/{fund}/unit-fund-price-trails', 'UnitFundPriceTrailController');

            Route::get('unitization/unit-fund-recipient-types-list', 'UnitFundCommissionRateController@getCommissionRecipientTypesList');

            Route::get('unitization/unit-funds/{fund}/unit-fund-price-trails-chart', 'UnitFundPriceTrailController@indexForChart');

            Route::resource('unitization/unit-funds/{fund}/unit-fund-dividends', 'UnitFundDividendController');

            Route::resource('unitization/unit-funds/{fund}/unit-fund-fees', 'UnitFundFeeController');

            Route::post('/unit-fund/fee/{id}/percentage', 'UnitFundFeeController@addFeeParameter');

            Route::get('unitization/unit-funds/{fundId}/unit-fund-charged-fees', 'UnitFundFeesChargedController@index');

            Route::resource('unitization/unit-funds/{fund}/unit-fund-commissions', 'UnitFundCommissionController');

            Route::get('unitization/commissions/totals/recipient/{recipient_id}/start/{start}/end/{end}', 'UnitFundCommissionController@getCommissionTotals');

            Route::get('unitization/commissions/schedules/recipient/{recipient_id}/start/{start}/end/{end}', 'UnitFundCommissionController@getCommissionSchedules');

            Route::get('unitization/commissions/overrides/recipient/{recipient_id}/start/{start}/end/{end}', 'UnitFundCommissionController@getCommissionOverrides');

            Route::resource('unitization/unit-funds/{holder}/unit-fund-commission-rates', 'UnitFundCommissionRateController');

            Route::get('unitization/unit-funds/{fund}/unit-fund-compliance', 'UnitFundController@compliance');
        });

        Route::group(['middleware' => 'allow:unittrust:create-unit-trust'], function () {
            Route::resource('unitization/unit-fund-business-confirmations', 'UnitFundBusinessConfirmationController');

            Route::post('unitization/unit-fund-business-confirmations/{purchase}/send', 'UnitFundBusinessConfirmationController@sendBC');

            Route::get('unitization/unit-fund-business-confirmations/{purchase}/preview', 'UnitFundBusinessConfirmationController@preview');

            Route::resource('unitization/unit-fund-fee-types', 'UnitFundFeeTypeController');

            Route::get('unitization/unit-fund-fee-types-list', 'UnitFundFeeTypeController@getList');

            Route::get('unitization/unit-fund-fee-types-list/partials', 'UnitFundFeeTypeController@getFundFeePartials');

            Route::resource('unitization/unit-fund-fee-parameters', 'UnitFundFeeParameterController');

            Route::get('unitization/unit-fund-fee-parameters-list', 'UnitFundFeeParameterController@getList');

            Route::resource('unitization/unit-funds/{fund}/unit-fund-holder-fees', 'UnitFundHolderFeeController');

            Route::resource('unitization/unit-funds/{fund}/unit-fund-switch-rates', 'UnitFundSwitchRateController');

            Route::resource('unitization/redemption-confirmations', 'UnitFundRedemptionConfirmationController');

            Route::post('unitization/redemption-confirmations/{sale}/send', 'UnitFundRedemptionConfirmationController@sendRC');

            Route::get('unitization/redemption-confirmations/{sale}/preview', 'UnitFundRedemptionConfirmationController@preview');
        });

        Route::group(['middleware' => 'allow:unittrust:view-unit-trust-client'], function () {
            Route::get('unitization/unit-funds/{fund}/unit-fund-clients/{client_id}', 'UnitFundClientController@show');

            Route::get('unitization/unit-funds/{fund}/unit-fund-clients/{client_id}/balance', 'UnitFundClientController@balance');

            Route::get('unitization/unit-funds/{fund}/unit-fund-clients/{client_id}/export-calculation', 'UnitFundClientController@exportCalculation');

            Route::get('unitization/unit-funds/{fund}/unit-fund-clients/{client_id}/owned-units', 'UnitFundClientController@unitsOwned');

            Route::get('unit-fund/{fid}/{cid}/statement/campaigns', 'UnitFundStatementCampaignController@getOpenCampaigns');

            Route::get("unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-purchases/{purchaseId}", 'UnitFundPurchaseController@show');

            Route::get('unitization/unit-fund-purchases/{purchase}/commission-schedules', 'UnitFundPurchaseController@getCommissionPurchase');

            Route::get('unitization/unit-funds/{fund_id}/{client_id}/interest-payment-schedule/view', 'UnitFundInvestmentController@viewInterestSchedule');

            Route::get('/unitization/unit-funds/{fund_id}/{client_id}/interest-payment-schedule/{id}', 'UnitFundInvestmentController@show');
        });

        Route::group(['middleware' => 'allow:unittrust:create-fund-instructions'], function () {

            Route::resource('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-purchases', 'UnitFundPurchaseController', [
                'except' => 'show'
            ]);

            Route::resource('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-sales', 'UnitFundSaleController');

            Route::resource('unitization/unit-funds/{fundId}/unit-fund-clients/{holder}/unit-fund-transfers', 'UnitFundTransferController');

            Route::resource('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-switches', 'UnitFundSwitchController');

            Route::get('unitization/unit-fund-switches', 'UnitFundSwitchController@indexAll');

            Route::post('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-sales/reverse', 'UnitFundSaleController@reverse');

            Route::post('unitization/unit-funds/{fundId}/unit-fund-clients/{holder}/unit-fund-transfers/reverse', 'UnitFundTransferController@reverse');

            Route::post('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-purchases/reverse', 'UnitFundPurchaseController@reverse');

            Route::post('unit-fund-purchase/recipient/edit', 'UnitFundPurchaseController@updateFa');

            Route::get('unit-fund-purchase/commission-schedule/{schedule}/{date}/edit', 'UnitFundPurchaseController@editCommissionPurchase');

            Route::post('unitization/unit-funds/interest-payment-schedule/save', 'UnitFundInvestmentController@saveInterestSchedule');

            Route::post('unitization/unit-funds/interest-payment-schedule/bulk', 'UnitFundInvestmentController@postBulkInterest');
        });

        Route::group(['middleware' => 'allow:unittrust:view-client-statements'], function () {
            Route::resource('unitization/unit-fund-statement-campaigns', 'UnitFundStatementCampaignController');

            Route::get('/unit-fund/statement/campaigns/{id}', 'UnitFundStatementCampaignController@getCampaigns');

            Route::put('unitization/unit-fund-statement-campaigns/{id}/{cid}/close', 'UnitFundStatementCampaignController@closeCampaign');

            Route::post('unitization/unit-fund-statement-campaigns/{cid}/send', 'UnitFundStatementCampaignController@sendCampaign');

            Route::get('unitization/unit-fund-statement-campaigns/{id}/{cid}/items', 'UnitFundCampaignStatementItemController@indexOfCampaign');

            Route::get('unitization/unit-fund-statement-campaigns/{id}/{cid}/items/missing', 'UnitFundCampaignStatementItemController@missingClients');

            Route::post('unitization/unit-funds/{fund}/unit-fund-clients/{client_id}/send-statement', 'UnitFundClientController@sendStatement');

            Route::get('unitization/unit-funds/{fund}/unit-fund-clients/{client_id}/preview-statement/{date?}/{start_date?}', 'UnitFundClientController@previewStatement');

            Route::get('unitization/statement-campaign-types-list', 'UnitFundStatementCampaignController@getTypesList');

            Route::resource('unitization/campaign-statement-items', 'UnitFundCampaignStatementItemController');
        });

        Route::group(['middleware' => 'allow:unittrust:unit-trust-compliance'], function () {
            Route::post('unitization/unit-funds/{fund}/compliance/link', 'UnitFundController@linkCompliance');
        });


        Route::group(['middleware' => 'allow:unittrust:view-unit-trust-reports'], function () {
            Route::post('unitization/unit-funds/reports', 'UnitFundReportController@index');
        });

        Route::resource('unitization/unit-funds/{fund}/unit-fund-fee-payments', 'UnitFundFeePaymentController');

        Route::get('unitization/unit-fund-fee-payment/recipients', 'UnitFundFeePaymentController@getRecipients');

        Route::post('unitization/application-investment', 'UnitFundInvestmentController@investApplication');

        Route::post('unitization/application-sale', 'UnitFundInvestmentController@saleApplication');

        Route::post('unitization/application-transfer', 'UnitFundInvestmentController@transferApplication');

        Route::post('unitization/application-cancellation', 'UnitFundInvestmentController@cancelApplication');

        Route::post('unitization/unit-fund-calculate', 'UnitFundCalculatorController@calculate');

        Route::get('unitization/application/{instruction}/pdf', 'UnitFundInvestmentController@viewPdf');
    });

    // Client Loyalty

    Route::group(['before' => 'auth', 'namespace' => 'Loyalty'], function () {
        Route::get('/clients/loyalty/index', 'ClientsLoyaltyController@index');

        Route::get('/clients/loyalty/details/{id}', 'ClientsLoyaltyController@details');

        Route::get('/clients/loyalty/values', 'ClientsLoyaltyController@loyaltyValues');

        Route::get('/clients/loyalty/vouchers', 'ClientsLoyaltyController@loyaltyVouchers');

        Route::post('/clients/loyalty/vouchers/add', 'ClientsLoyaltyController@processVoucher');

        Route::post('/clients/loyalty/vouchers/{voucherId}/delete', 'ClientsLoyaltyController@deleteVoucher');

        Route::get('/clients/loyalty/voucher/{voucherId}/details', 'ClientsLoyaltyController@getVoucherDetails');

        Route::post('/clients/loyalty/redeem/voucher/', 'ClientsLoyaltyController@redeemVoucher');

        Route::get('/loyalty/{clientID}/points-instructions/', 'ClientsLoyaltyController@recentActivities');
    });

    // Investments Dashboard
    Route::get('/dashboard/investments/sp_investments', 'InvestmentsController@spInvestments');
    Route::get('/dashboard/investments/utf_investments', 'InvestmentsController@utfInvestments');
    Route::get('/dashboard/investments/investments_application', 'InvestmentsController@investmentsApplication');
    Route::get('/dashboard/investments/pending_instructions', 'InvestmentsController@pendingInstructions');
    Route::get('/dashboard/investments/pending_approvals', 'InvestmentsController@pendingApprovals');

    //Portfolio securities

    Route::group(['before' => 'auth', 'namespace' => 'Portfolio'], function () {

        Route::resource('portfolio/securities', 'PortfolioSecurityController');
        Route::post('portfolio/security/{id}/edit', 'PortfolioSecurityController@update');

        Route::get('portfolio/raw-securities/{id}', 'PortfolioSecurityController@rawSecurities');

        Route::get('portfolio/securities/{security_id}/holdings/{action?}', 'PortfolioSecurityController@getSecurityHoldings');

        Route::post('portfolio/securities/{id}/equities/buy', 'EquitiesController@buyShares');
        Route::post('portfolio/securities/{id}/equities/sell', 'EquitiesController@sellShares');
        Route::post('portfolio/securities/{id}/equities/dividends', 'EquitiesController@shareDividends');

        Route::get('portfolio/securities/{id}/equities/market-price', 'EquitiesController@marketPrice');
        Route::get('portfolio/securities/{id}/equities/target-price', 'EquitiesController@targetPrice');

        Route::post('portfolio/securities/{id}/equities/market-price/add', 'EquitiesController@saveMarketPriceTrail');
        Route::post('portfolio/securities/{id}/equities/target-price/add', 'EquitiesController@saveTargetPriceTrail');

        Route::get('portfolio/investment-types', 'PortfolioSecurityController@investmentTypes');

        Route::post('portfolio/securities/{id}/deposits/add', 'DepositsController@create');

        Route::get('portfolio/deposits/tax-rates', 'DepositsController@taxRates');

        Route::get('portfolio/deposits/deposit-types', 'DepositsController@depositTypes');

        Route::get('portfolio/portfolio-deposits/deposit-types', 'DepositsController@portfolioDepositTypes');

        Route::get('portfolio/securities/{id}/deposits/{holdingId}', 'DepositsController@holdingDetails');

        Route::get('/portfolio/deposit-holdings/{id}/interest-schedules', 'DepositsController@interestSchedules');

        Route::get('portfolio/securities/{id}/{holdingId}/repay', 'DepositsRepaymentController@index');

        Route::post('portfolio/securities/deposit-holding/reverse', 'DepositsActionsController@reverse');

        Route::post('portfolio/securities/deposit-holding/rollback', 'DepositsActionsController@rollback');

        Route::resource('portfolio/securities/{id}/bonds', 'BondsController');

        Route::get('/portfolio/security/{security}', 'PortfolioSecurityController@securityTypes');

        Route::get('/portfolio/deposit/summary/{fundType?}', 'PortfolioSecuritySummaryController@depositSummary');


        Route::get('/portfolio/equities/list', 'EquitySummaryController@index');
        Route::get('/portfolio/equities/totals', 'EquitySummaryController@totals');
        Route::get('/portfolio/equities/allocation', 'EquitySummaryController@assetAllocation');
        Route::get('/portfolio/equities/{id}/dividends/{type}', 'EquitySummaryController@dividends');

        //TODO modify the id param passed
        Route::get('/portfolio/security/{id}/unit-funds-for-fund-manager', 'PortfolioSecurityController@getFundForFundManager');

        Route::get('/portfolio/fund-manager/{id}/unit-funds', 'PortfolioSecurityController@managerFunds');

        Route::get('/all-securities', 'PortfolioOrderController@securities');

        /**
         * Orders management
         */

        Route::group(['middleware' => 'allow:portfolio:order-management'], function () {

            Route::resource('/portfolio/orders', 'PortfolioOrderController');

            Route::post('/portfolio/orders/{id}', 'PortfolioOrderController@update');

            Route::get('/order-types', 'PortfolioOrderController@orderTypes');

            Route::get('/portfolio/fund-orders/{id}', 'PortfolioOrderController@fundOrders');

            Route::post('/portfolio/order/{id}/remove', 'PortfolioOrderController@delete');

            Route::get('/portfolio/raw-orders/{id}', 'PortfolioOrderController@rawOrders');

            Route::get('/portfolio/order-lifespan-types', 'PortfolioOrderController@orderLifespanTypes');

            Route::resource('/portfolio/order/{id}/allocate', 'PortfolioOrderAllocationController');
        });

        Route::post('/portfolio/order/allocations/{id}', [
            'middleware' => 'allow:portfolio-orders:confirm-allocation',
            'uses' => 'PortfolioOrderAllocationController@updateAllocation'
        ]);

        Route::group(['middleware' => 'allow:portfolio-orders:settle'], function () {
            Route::post('/portfolio/equity-order/settlement', 'PortfolioOrderAllocationController@settleEquityOrder');

            Route::post('/portfolio/deposit-order/settlement', 'PortfolioOrderAllocationController@settleDepositOrder');
        });


        Route::get('/portfolio/settlement-fees', 'PortfolioOrderAllocationController@settlementFees');


        /**
         * Portfolio fund management
         */
        Route::group(['middleware' => 'allow:portfolio:fund-compliance'], function () {
            Route::resource('/portfolio/compliance/benchmarks', 'PortfolioComplianceController');

            Route::resource('/portfolio/compliance/liquidity', 'PortfolioLiquidityLimitsController');

            Route::resource('/portfolio/compliance/no-go-zone', 'FundNoGoZoneController');

            Route::post('/portfolio/limits', 'FundComplianceController@fundCompliance');

            Route::get('/portfolio/compliance-types', 'FundComplianceController@complianceTypes');

            Route::get('/fund/compliance', 'FundComplianceController@complianceList');
        });
    });

    Route::get('/portfolio/investors', 'PortfolioController@investors');

    Route::get('/portfolio/{fund_manager_id}/funds', 'FundManagerController@funds');

    Route::get('/currencies-list', 'CurrencyController@getList');

    Route::post('/clients/all', 'ClientsController@allClients');

    Route::post('/commission/recipients', 'CommissionController@allRecipients');

    /**
     *  Portfolio Asset Allocation
     */

    Route::get('/portfolio/asset/allocation', 'PortfolioController@assetAllocation');

    /**
     * Application form partials
     */
    Route::get('contact/titles', 'InvestmentApplicationController@titles');

    Route::get('gender/list', 'InvestmentApplicationController@gender');

    Route::get('contact/contact_methods', 'InvestmentApplicationController@contactMethods');

    Route::get('contact/business_natures', 'InvestmentApplicationController@businessNatures');

    Route::get('contact/employment_types', 'InvestmentApplicationController@employmentTypes');

    Route::get('contact/source_of_funds', 'InvestmentApplicationController@sourceOfFunds');

    Route::get('contact/source_of_funds/{src_id}', 'InvestmentApplicationController@sourceOfFundsLabel');

    Route::get('contact/countries', 'InvestmentApplicationController@countries');

    Route::get('contact/application-form/partials', 'InvestmentApplicationController@formPartials');


    Route::post('applications/validate/risk', 'InvestmentApplicationController@validateRisk');
    Route::post('applications/validate/investment', 'InvestmentApplicationController@validateInvestment');
//    Route::post('applications/validate/subscriber', 'InvestmentApplicationController@saveApplication');
    Route::post('applications/validate/subscriber', 'InvestmentApplicationController@saveEntryApplication');
    Route::post('applications/update-instruction', 'InvestmentApplicationController@updateApplication');
    Route::post('applications/kyc/complete/{uuid}', 'InvestmentApplicationController@kycComplete');
    Route::post('applications/payment/complete/{uuid}', 'InvestmentApplicationController@paymentComplete');
    Route::post('applications/{uuid}', 'InvestmentApplicationController@getKycDocuments');
    Route::post('investment/applications/upload-kyc', 'InvestmentApplicationController@uploadKyc');
    Route::post('applications/upload/payment-document', 'InvestmentApplicationController@uploadPayment');
    Route::get('investment/applications/complete/{uuid}', 'InvestmentApplicationController@applicationComplete');
    Route::post('applications/joint_holder/validate', 'InvestmentApplicationController@validateJointHolder');

    Route::post('instruction/top-up-investment', 'InvestmentsController@topupInvestment');
    Route::post('instruction/topup/{invId}/edit', 'InvestmentsController@editInvestmentTopup');

    Route::post('product/interest-rates', 'ProductController@getInterestRates');

    Route::get('investment/{clientId}/client-bank-accounts', 'ClientsController@clientBankAccounts');
    Route::get('client/account/{accountId}/details', 'ClientsController@bankDetails');
    Route::post('investment/amount/{investment_id}', 'InvestmentsController@amountAffected');

    Route::post('investments/withdraw/{investment_id}', 'InvestmentsController@withdrawInvestment');
    Route::post('instruction/withdraw/{withdrawId}/edit', 'InvestmentsController@editWithdrawal');
    Route::post('instruction/rollover/{rolloverId}/edit', 'InvestmentsController@editRollover');

    Route::get('combine-investments/{invId}', 'InvestmentsController@getCombineInvestments');

    Route::post('investments/combine/{invId}', 'InvestmentsController@combineSelectedRollover');

    Route::post('investments/roll-over-inv/{invId}', 'InvestmentsController@rolloverInvestment');

    Route::get('/investment/orders/{stage}', 'OrdersController@index');

    Route::post('/validate/joint-holder', 'JointHolderController@validateJointHolder');

    Route::post('/validate/contact-person', 'ContactPersonController@validateContactPerson');
    Route::get('/kyc-document/types/{type}', 'DocumentsController@kycDocumentTypes');

    /*
     * Dashboard
     */
    Route::group(['before' => 'auth', 'namespace' => 'Dashboard'], function () {
        Route::get('/dashboard/clients-stats', 'HomeController@clientsStats');
        Route::get('/dashboard/withdrawals-stats', 'HomeController@withdrawalsStats');
        Route::get('/dashboard/inflows-stats', 'HomeController@inflowsStats');
        Route::get('/dashboard/aum-stats', 'HomeController@aumStats');

        Route::get('/dashboard/mature-withdrawals', 'HomeController@matureWithdrawals');
        Route::get('/dashboard/pre-mature-withdrawals', 'HomeController@prematureWithdrawals');
        Route::get('/dashboard/active-investments', 'HomeController@activeInvestments');
        Route::get('/dashboard/pending-investment-transactions', 'HomeController@pendingInvestmentTransactions');
        Route::get('/dashboard/active-portfolio-investments', 'HomeController@activePortfolioInvestments');
        Route::get('/dashboard/pending-portfolio-transactions', 'HomeController@pendingPortfolioTransactions');
        Route::get('/investment/fund-valuation', 'HomeController@fundValuation');
        Route::get('/investment/re-valuation', 'HomeController@reValuation');
    });

    Route::group(['before' => 'auth', 'namespace' => 'Client'], function () {

        Route::post('/client/investment/application', 'ClientApplicationController@validateInvestment');

        Route::post('/client/shareholder/application', 'ClientApplicationController@validateShareholderAccountDetails');

        Route::get('/client/investment/application/{id}/edit', 'ClientApplicationController@editApplication');

        Route::post('/client/subscriber/application', 'ClientApplicationController@saveInvestment');

        Route::post('/client/contact-persons', 'ClientApplicationController@saveContactPersons');

        Route::get('/client/contact-persons/{id}/delete', 'ClientApplicationController@deleteContactPersons');

        Route::post('/client/joint_holder/save', 'ClientApplicationController@saveJointHolders');

        Route::post('/client/joint_holder/{id}/delete', 'ClientApplicationController@deleteJointHolder');

        Route::get('/client/joint_holder/{id}/details', 'ClientApplicationController@getJointHolder');

        Route::post('/client/document/upload', 'ClientApplicationController@uploadDocument');

        Route::post('/client/application/signing-mandate', 'ClientApplicationController@updateSigningMandate');

        Route::get('/client/document/{id}/delete', 'ClientApplicationController@deleteDocument');

        Route::post('/client/risky/create/', 'RiskyClientsController@riskyClientCreate');

        Route::get('/client/risky/{id}/edit', 'RiskyClientsController@riskyClient');

        Route::post('/client/document/upload_tax_exemption', 'ClientApplicationController@uploadTaxExemption');

//        Route::get('/client/taxExemption/{id}/edit', 'ClientApplicationController@getTaxExemption');
    });


    Route::group(['before' => 'auth', 'namespace' => 'Portfolio'], function () {
        Route::get('/portfolio/fund/{id}/summary/asset-class', 'PortfolioFundSummaryController@assetClassSummary');

        Route::get('/portfolio/fund/{id}/summary/sub-asset-class', 'PortfolioFundSummaryController@subAssetClassSummary');

        Route::get('/portfolio/fund/{id}/summary/security', 'PortfolioFundSummaryController@securitySummary');

        Route::get('/portfolio/fund/{id}/fund-summary', 'PortfolioFundSummaryController@fundPricingSummary');

        Route::get('/portfolio/fund/fund-summary/deposits', 'PortfolioFundSummaryController@depositAssets');

        Route::get('/portfolio/fund/fund-summary/equities', 'PortfolioFundSummaryController@equityAssets');

        Route::get('/portfolio/deposit/{id}/loan/amortization', 'LoanAmortizationController@schedule');
    });

    Route::group(['before' => 'auth', 'namespace' => 'Dashboard'], function () {
    });

    Route::resource('/portfolio/sectors', 'Portfolio\PortfolioSectorController');

    Route::get('investment/withholding-taxes/{id}/{start?}/{end?}', 'Portfolio\WithholdingTaxController@taxes');

    Route::get('investment/custodial/{id}/wht', 'Portfolio\WithholdingTaxController@taxes');

    Route::resource('investment/withholding-taxes', 'Portfolio\WithholdingTaxController');

    Route::post('investment/tax/calculate', 'Portfolio\WithholdingTaxController@calculate');

    Route::group(['namespace' => 'Investments\Reporting'], function () {

        Route::resource('investments/custom-report', 'CustomReportController');

        Route::post('investments/custom-report/{id}/send', 'CustomReportController@export');
        Route::get('investments/custom-report/{id}/delete', 'CustomReportController@delete');

        Route::get('investments/custom/report', 'CustomReportController@index');

        Route::post('investments/custom-report/model/columns', 'CustomReportController@modelColumns');

        Route::post('investments/custom-report/model/relations', 'CustomReportController@modelRelations');

        Route::post('investments/custom-report/{report_id}/columns', 'CustomReportController@storeColumns');

        Route::post('investments/custom-report/{report_id}/arguments', 'CustomReportController@storeArguments');

        Route::post('investments/custom-report/{report_id}/filters', 'CustomReportController@storeFilters');

        Route::get('investments/custom-reports/filters', 'CustomReportController@filterTypes');
    });
});
