<?php
/**
 * Date: 08/09/2017
 * Time: 15:07
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

Route::post('products/calculate/return', 'Investments\InvestmentsController@calculateReturn');

Route::post('products/calculate/goal', 'Investments\InvestmentsController@calculateGoal');

Route::post('/product/interest/rates', 'Investments\ProductController@getProductInterestRate');

Route::get('investments/products/{type?}', 'Investments\InvestmentsController@getProducts');

Route::get('unitization/unit-funds-all', 'Unitization\UnitizationController@getAllFunds');

Route::get('unitization/funds', 'Api\Unitization\ClientUnitFundController@funds');

Route::get('unitization/unit-funds-cis', 'Unitization\UnitizationController@getCISFunds');

Route::get('client-unitization/unit-funds/{id}', 'Unitization\UnitizationController@show');

Route::get('client-unitization/show-unit-funds/{id}', 'Unitization\UnitizationController@showUnitFund');

Route::get('unitization/unit-fund-accounts/{id}', 'Unitization\UnitizationController@getFundAccounts');

Route::post('/commission-recipients/all', 'Api\Client\CommissionController@allRecipients');

// unitization Calculator
Route::post('unitization/unit-fund-calculate', 'Api\Unitization\UnitFundCalculatorController@calculate');

/*
 * Applications
 */
Route::get('applications/{uuid}', 'Applications\ApplicationController@show');
Route::get('application/documents/{uuid}', 'Applications\ApplicationController@applicationDocuments');

Route::get('applications/joint/{uuid}', 'Applications\ApplicationController@getJointHolders');

Route::post('applications/validate/investment', 'Applications\ApplicationController@validateInvestment');

Route::post('applications/validate/risk', 'Applications\ApplicationController@validateRisk');

Route::post('applications/save/{type}/{uuid?}', 'Applications\ApplicationController@processClientApplication');

Route::post('applications/form-upload/save', 'Applications\ApplicationController@uploadApplicationForm');

Route::get('applications/{referralCode}/referred-fa/{mobile?}', 'Applications\ApplicationController@getReferredFA');

Route::post('applications/uploads/document', 'Applications\ApplicationController@uploadDocument');

Route::post('applications/uploads/mandate', 'Applications\ApplicationController@mandate');

Route::post('application/client/signing-mandate', 'Applications\ApplicationController@updateMandate');

Route::post('/applications/kyc/complete/{uuid}', 'Applications\ApplicationController@kycComplete');

Route::post('/applications/payment/complete/mpesa/{uuid}', 'Applications\ApplicationController@paymentMpesaComplete');

Route::post('/applications/payment/complete/{uuid}', 'Applications\ApplicationController@paymentComplete');

Route::post('applications/payment/sms-bank-details', 'Applications\ApplicationController@requestBankSms');

Route::post('/applications/mandate/complete/{uuid}', 'Applications\ApplicationController@mandateComplete');

Route::post('/applications/joint_holder/validate', 'Applications\ApplicationController@validateJointHolder');

Route::get('/applications/{uuid}/complete', 'Applications\ApplicationController@complete');

Route::get('/commission-recipients/all', 'Applications\ApplicationController@commissionRecipients');

Route::post('/request/application/token', 'Applications\ApplicationController@sendVerifyToken');

Route::post('/verify/application/token', 'Applications\ApplicationController@validateToken');
/*
 * Contact Details
 */
Route::get('contact/titles', 'Client\ContactController@titles');

Route::get('gender/list', 'Client\ContactController@gender');

Route::get('contact/contact_methods', 'Client\ContactController@contactMethods');

Route::get('contact/business_natures', 'Client\ContactController@businessNatures');

Route::get('contact/employment_types', 'Client\ContactController@employmentTypes');

Route::get('contact/source_of_funds', 'Client\ContactController@sourceOfFunds');

Route::get('contact/countries', 'Client\ContactController@countries');

/*
 * Products
 */
Route::post('products/rates', 'Investments\ProductController@getRates');
Route::post('products/interest/rates', 'Investments\ProductController@getInterestRates');

Route::post('investments/interest/rates', 'Investments\ProductController@getInvestmentsRates');
Route::post('interest/rates', 'Investments\ProductController@interestRate');
Route::post('investment/interest-rate', 'Investments\ProductController@investmentRate');
Route::post('products/interest-rates/global', 'Investments\ProductController@globalRates');

/**
 * Route to the resources
 */
Route::get('interest/rates', 'Investments\ResourceController@index');

Route::get('/all/banks', 'Client\ClientBankController@allBanks');

Route::get('/banks/{bank}/bank-branches', 'Client\ClientBankController@bankBranches');

Route::get('/currencies', 'Investments\ProductController@currencies');

/**
 * Calculator
 */
Route::post('products/interest/returns', 'Investments\InvestmentsController@investmentReturns');
Route::post('products/goals/returns', 'Investments\InvestmentsController@investmentGoal');


/**
 * Terms and conditions for crims client mobile
 */
Route::get('/terms-and-conditions', 'Terms\TermsController@getAllTerms');

Route::get('/mobile/terms/general', 'Terms\TermsController@mobileGetAllFundGeneralTerms');

Route::get('/terms-and-conditions/{product}', 'Terms\TermsController@getAllTermsProduct');

Route::get('/terms-and-conditions/{form}/product/{product?}', 'Terms\TermsController@getAllTermsForm');

/**
 * Terms and conditions for crims client web
 */
Route::get('/terms/funds/{fund}/general', 'Terms\TermsController@fundGeneral');
Route::get('/terms/products/{product}/general', 'Terms\TermsController@general');
Route::get('/terms/{product}/topup', 'Terms\TermsController@topup');
Route::get('/terms/{investment}/rollover', 'Terms\TermsController@rollover');

Route::get('/terms-and-conditions/{form}/product/{product?}', 'TermsAndConditions\TermsAndConditionsController@getAllTermsForm');

Route::get('/get/offices', 'About\AboutController@getOffices');

/*
 * Real Estate Projects
 */
Route::get('/real-estate/projects', 'RealEstate\RealEstateController@index');
Route::get('/real-estate/payment-plans', 'RealEstate\RealEstateController@paymentPlans');

/**
 * Contact us
 */
Route::post('contactus', 'Home\HomeController@contactUs');

/**
 * Rates
 */
Route::get('product/prevailing-rates/{id}', 'Investments\ProductController@prevailingRates');

//Route::get('/real-state/project-summary', 'RealEstate\RealEstateController@projectSummary');
