<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 8/28/18
 * Time: 11:41 AM
 */
Route::post('/auth/verify', 'Auth\AuthController@verifyCredentialsFromMobile');

Route::post('/auth/verify-username', 'Auth\AuthController@verifyUsername');

Route::post('/auth/authenticate-password-reset', 'Auth\AuthController@authenticatePasswordReset');
