<?php
/**
 * Date: 08/09/2017
 * Time: 15:07
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

Route::post('/ussd/{key}', 'USSD\USSDController@index');
