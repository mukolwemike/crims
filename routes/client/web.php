<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/account/{username}/activate/{token}', 'Auth\AuthController@activate');

Route::post('/account/{username}/activate/{token}', [
    'as'=>'activate_user_path',
    'uses'=>'Auth\AuthController@saveActivate'
]);

Route::get('/account/{username}/reset/{token}', 'Auth\AuthController@getReset');

Route::post('/account/{username}/reset/{token}', 'Auth\AuthController@postReset');

Route::get('/logout', 'Auth\AuthController@performLogout');

Route::post('/auth', 'Auth\AuthController@authenticate');

Route::post('/auth/authenticate-password-reset', 'Auth\AuthController@authenticatePasswordReset');

Route::get('/auth/logout', 'Auth\AuthController@logout');

Route::post('/auth/verify', 'Auth\AuthController@verifyCredentials');

Route::post('/auth/verify-username', 'Auth\AuthController@verifyUsername');

Route::post('/auth/{username}/forgot', 'Auth\AuthController@forgotPassword');

Route::post('/auth/sms', 'Auth\AuthController@requestSMS');

Route::post('/auth/password-reset-sms', 'Auth\AuthController@passwordResetRequestSMS');


Route::get('/', [
    'as'=>'home_path',
    'uses'=>'Home\HomeController@index'
]);

Route::get('/unittrust/{vue_capture?}', function () {
    if (! \Illuminate\Support\Facades\Auth::user()) {
        return redirect('/');
    }
    return view('home.index');
})->where('vue_capture', '[\/\w\.-]*');
