<?php
Route::get('/logout', 'Auth\AuthController@logout');

Route::post('/auth/verify', 'Auth\AuthController@verifyCredentialsFromMobile');

Route::post('/auth/token/generate', 'Auth\AuthController@requestSMS');

Route::post('/auth/token/verify', 'Auth\AuthController@verifyTokenFromMobile');

Route::post('/auth/token/resend', 'Auth\AuthController@sendToken');

Route::post('/device/fcm/token', 'Auth\AuthController@updateDeviceFcm');

Route::post('/auth/password-reset-sms', 'Auth\AuthController@passwordResetRequestSMS');

Route::post('/auth/{username}/forgot', 'Auth\AuthController@forgotPassword');

Route::get('/contact/titles', 'Client\ContactController@getTitles');

Route::get('/gender/list', 'Client\ContactController@getGender');

// Application
Route::post('/applications/validate/investment', 'Applications\ApplicationController@validateInvestment');

Route::post('/applications/save/{type}/{uuid?}', 'Applications\ApplicationController@processClientApplication');

Route::get('unitization/unit-funds-cis', 'Unitization\UnitizationController@getCISFunds');

Route::post('/applications/payment/complete/mpesa/{uuid}', 'Applications\ApplicationController@paymentMpesaComplete');

Route::post('applications/uploads/document', 'Applications\ApplicationController@uploadDocument');

Route::post('/applications/payment/complete/{uuid}', 'Applications\ApplicationController@paymentComplete');

Route::post('/applications/payment/amount-update/{uuid}', 'Applications\ApplicationController@paymentAmountUpdate');

Route::get('applications/{referralCode}/{mobile?}/referred-fa', 'Applications\ApplicationController@getReferredFA');

Route::post('/request/mobile/application/token', 'Applications\ApplicationController@sendVerifyToken');

Route::post('/verify/mobile/application/token', 'Applications\ApplicationController@validateToken');
