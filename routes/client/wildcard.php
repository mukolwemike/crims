<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 5/24/18
 * Time: 3:34 PM
 */

/*
 * Return 404 for any missing assets
 */
Route::get('/assets/{path?}/{path1?}/{path2?}/{p3?}/{p4?}/{p5?}/{p6?}', function()
{
    app()->abort(404);
});

Route::get('/build/{path?}/{path1?}/{path2?}/{p3?}/{p4?}/{p5?}/{p6?}', function()
{
    app()->abort(404);
});




//take any undefined paths to home page, for js routing
Route::get('login', 'Home\HomeController@index');
Route::get('{path}', 'Home\HomeController@index');
Route::get('{path}/{param?}', 'Home\HomeController@index');
Route::get('{path}/{param?}/{param2}', 'Home\HomeController@index');
Route::get('{path}/{param?}/{param2}/{param3}', 'Home\HomeController@index');
Route::get('{path}/{param?}/{param2}/{param3}/{param4}', 'Home\HomeController@index');
Route::get('{path}/{param?}/{param2}/{param3}/{param4}/{param5}', 'Home\HomeController@index');
Route::get('{path}/{param?}/{param2}/{param3}/{param4}/{param5}/{param6}', 'Home\HomeController@index');