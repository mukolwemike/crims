<?php
Route::post('investments/client/statements', 'Investments\InvestmentsController@sendStatementToClient');

Route::get('/investments/active/{client_id}', 'Investments\InvestmentsController@getActiveInvestments');

Route::get('/investments/activities/{client_id}/product/{product_id}', 'Investments\InvestmentsController@getInvestmentDetails');

Route::get('/investments/withdraw/status/{id}', 'Investments\InvestmentsController@checkWithdrawalStatus');


/**
 * Feedback
 */
Route::post('feedback', 'Home\HomeController@postFeedback');

/*
 * User
 */
Route::get('user', 'Auth\AuthController@getAuthenticatedUser');

Route::group(['before' => 'auth', 'namespace' => 'Api\Client'], function () {
    Route::post('client/profile/edit', 'ClientController@editClient');

    Route::post('client/kyc/documents', 'ClientController@uploadClientKyc');

    Route::post('client/profile/edit-kyc', 'ClientController@editClientKyc');

    Route::get('client/profile/{client}/pending-kyc', 'ClientController@getPendingKyc');

    Route::post('client/profile/{client}/account-details', 'ClientController@saveAccountDetails');

    Route::get('client/profile/account-details/{account}', 'ClientController@getAccountDetails');

    Route::get('client/{client}/profile/account-details/{account}/remove', 'ClientController@removeAccountDetails');

    Route::get('countries', 'ClientController@countries');

    Route::post('refer-client', 'ClientController@referClient');
});

/*
 * Documents
 */
Route::get('/document/{id}', 'Documents\DocumentController@showDocument');
Route::get('/client/{id}/{type}/documents', 'Documents\DocumentController@clientDocuments');
Route::get('client/{uuid}/documents', 'Documents\DocumentController@clientDocuments');
Route::get('/client/document/{client_id}/document-types', 'Documents\DocumentController@documentTypes');


/*
 * Investments
 */


//Route::get('investments/active/{client_id}', 'Investments\InvestmentsController@activeInvestments');

Route::get('investments/{client_id}/topups', 'Investments\InvestmentsController@topups');

Route::get('investments/{client_id}/pending-topups', 'Investments\InvestmentsController@pendingTopups');

Route::get('investments/{investment_id}', 'Investments\InvestmentsController@investment');

//Route::post('investments/{investment_id}/withdraw', 'Investments\InvestmentsController@withdraw');

Route::post('investments/withdraw/{investment_id}', 'Investments\InvestmentsController@investmentWithdrawal');

Route::post('investment/amount/{investment_id}', 'Investments\InvestmentsController@amountAffected');

Route::post('investments/{investment_id}/rollover', 'Investments\InvestmentsController@rollover');

Route::post('investment/amount/{investment_id}', 'Investments\InvestmentsController@amountAffected');

Route::post('investments/rollover/{investment_id}', 'Investments\InvestmentsController@rollover');

Route::post('investments/{client_id}/topup', 'Investments\InvestmentsController@topup');

Route::get('investments/topup/{topup_id}', 'Investments\InvestmentsController@getTopup');

Route::post('investments/topup/upload/{topupid}', 'Investments\InvestmentsController@uploadBankTransfer');

Route::get('/client/investments/{clientId}', 'Investments\InvestmentsController@clientInvestments');

Route::get('/{clientId}/{productId}/investments', 'Investments\InvestmentsController@activeInvestments');

Route::post('/verify/transaction/token', 'Investments\InvestmentsController@sendVerifyToken');

Route::post('/verify/transaction/validate', 'Investments\InvestmentsController@validateToken');


/**
 * get the uploaded bank transfer document
 */
Route::get('investment/topup/document/{id}', 'Documents\DocumentController@getFile');

Route::get('/investments/statements/{product}/{client}/embed', 'Investments\InvestmentsController@statement');

//Route::post('/investments/statements/{product}/{client}/preview', 'Investments\InvestmentsController@statementPdf');
Route::get('/investments/statements/{product}/{client}/preview', 'Investments\InvestmentsController@statementPdf');

Route::get('/investments/statements/{product}/{client}/excel-preview', 'Investments\InvestmentsController@statementExcel');

Route::get('investments/client/{client_id}/products', 'Investments\InvestmentsController@myProducts');

/*
 * Client
 */
Route::get('/client/{id}', 'Client\ClientController@show');

Route::get('client/{client_id}/overview/brief', 'Client\AccountOverviewController@brief');

Route::get('client/{client_id}/activity-log/brief', 'Client\ActivityLogController@brief');

Route::get('client/{client_id}/activity-log/re-brief', 'Client\ActivityLogController@reBrief');

Route::get('client/{clientId}/investment/maturities', 'Investments\InvestmentsController@maturities');

Route::get('client/{clientId}/investment/upcomingMaturities', 'Investments\InvestmentsController@upcomingMaturities');

Route::get('client/details/{id}', 'Client\ClientController@bankDetails');

Route::get('client/{id}/bank-accounts', 'Client\ClientController@clientBankAccounts');

/*
 * Financial Advisors
 */
Route::get('fa/clients', 'FA\FinancialAdvisorController@myClients');

Route::get('fa/clients/portfolio', 'FA\FinancialAdvisorController@myClientsPortfolio');

Route::get('fa/clients/portfolio/{id}', 'FA\FinancialAdvisorController@faClientPortfolio');

Route::get('fa/clients/{userId}', 'FA\FinancialAdvisorController@getClients');

Route::get('fa/fas/{id}/{date}', 'FA\FinancialAdvisorController@myFas');

Route::resource('fa/commissions', 'FA\CommissionController');

Route::post('fa/commissions/summary/{id}', 'FA\CommissionSummaryController@index');

Route::post('fa/commissions/summary/{id}/export', 'FA\CommissionSummaryController@export');

Route::get('fa/{fa_id}/clawbacks', 'FA\CommissionSummaryController@faClawbacks');

Route::get('fa/{fa_id}/overrides', 'FA\FinancialAdvisorController@getOverrides');

Route::get('fa/{fa_id}/product_commissions', 'FA\FinancialAdvisorController@faProductCommissions');

/**
 * test fa commission projections
 */
Route::post('month/commissions', 'FA\ProjectionsController@commissionsProjections');

//Route::post('commission/projections', 'FA\ProjectionsController@faCommissionsProjections');
Route::post('commission/projections', 'FA\ProjectionsController@getProjections');

Route::get('client/statement/{productId}/{client}/invPortfolio', 'Client\ClientController@invPortfolio');

Route::get('client/re/portfolio/{clientId}/portfolio', 'Client\ClientController@rePortfolio');

Route::get('/currencies', 'Investments\ProductController@currencies');

Route::get('client/{client_id}/unit-funds/activities', 'Client\UnitFundActivityController@activities');

Route::get('notifications/{uuid}', 'Notifications\NotificationsController@index');
Route::get('notifications/{notification_id}/update', 'Notifications\NotificationsController@update');

Route::get('approvals/{uuid}', 'Approvals\ClientApprovalController@index');
Route::post('approvals/approve', 'Approvals\ClientApprovalController@approve');

/**
 * Client unit fund module
 */
Route::group(['before' => 'auth', 'namespace' => 'Api\Unitization'], function () {

    Route::get('unitization/client-unit-funds/{client_id}/summaries', 'ClientUnitFundController@index');

    Route::get('unitization/client-funds/{client_id}', 'ClientUnitFundController@clientFunds');

    Route::get('unitization/client-unit-funds/details/{client_id}', 'ClientUnitFundController@details');

    // Unit fund purchases
    Route::resource('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-purchases', 'UnitFundPurchaseController');

    Route::post('unitization/unit-funds/unit-fund-clients/{instruction_id}/bank-transfer', 'UnitFundPurchaseController@uploadBankTransfer');

    // Unit fund sales
    Route::resource('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-sales', 'UnitFundSaleController');

    Route::post('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-sales/calculate-sale-charge', 'UnitFundSaleController@calculateSaleCharge');

    Route::get('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-transfers-given', 'UnitFundTransferController@given');

    Route::get('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-transfers-received', 'UnitFundTransferController@received');

    Route::get('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-instructions', 'ClientUnitFundController@pendingInstructions');

    // Client Unit Fund Holders
    Route::get('unitization/{fundId}/unit-fund-holders/{clientId}/statement-preview/{start}/{date}', 'ClientUnitFundController@previewStatement');

    Route::get('unitization/{fundId}/unit-fund-holders/{clientId}/statement-download/{start}/{date}', 'ClientUnitFundController@downloadStatement');

    Route::get('unitization/{fundId}/unit-fund-holders/{clientId}/excel-statement-download/{start}/{date}', 'ClientUnitFundController@downloadExcelStatement');

    Route::get('unitization/{fundId}/unit-fund/{clientId}/statement-download/{startDate}/{endDate}', 'ClientUnitFundController@downloadMobileStatement');

    Route::get('unitization/{fundId}/unit-fund-holders/{clientId}/statement-send/{start}/{date}', 'ClientUnitFundController@sendStatement');

    Route::get('unitization/{fundId}/unit-fund-holders/{clientId}/statement-load/{start}/{date}', 'ClientUnitFundController@loadStatement');

    Route::get('unitization/unit-funds', 'ClientUnitFundController@index');

    Route::get('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/balance', 'ClientUnitFundController@balance');

    Route::get('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/owned-units/{date?}', 'ClientUnitFundController@unitsOwned');

    // Recent Activities
    Route::get('unitization/unit-fund/{client_id}/recent-activities-summaries', 'ClientUnitFundController@recentActivities');

    // Commission Summary
    Route::get('unitization/unit-fund/{fa_id}/commissions/', 'FA\CommissionSummaryController@commissionSummary');

    // Transfer Unit Fund
    Route::get('unitization/unit-fund/{clientCode}', 'UnitFundTransferController@getClient');

    Route::resource('unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-transfers', 'UnitFundTransferController');

    Route::get('/unitization/{fundId}/bank-mpesa-accounts', 'ClientUnitFundController@getFundBankMpesaAccounts');
});


/*
 * Real Estate
 */
Route::group(['before' => 'auth', 'namespace' => 'Api\RealEstate'], function () {

    Route::get('/{client}/re-summaries', 'RealEstateClientsController@realEstateSummaries');

    Route::get('/{unit}/unit-details', 'RealEstateClientsController@unitDetails');

    Route::get('/{unit}/payment-schedules', 'RealEstateClientsController@unitPaymentSchedules');

    Route::get('/client/realestate/{client}/payments/{paymentStatus?}', 'RealEstateInstructionsController@getPaymentsInstructions');

    Route::get('/client/realestate/payments/{payment}', 'RealEstateInstructionsController@getPaymentInstruction');

    Route::post('realestate/payment/upload/{payment}', 'RealEstatePaymentController@uploadPaymentProof');

    Route::get('/client/{clientId}/upcoming-payments', 'RealEstatePaymentController@upcomingPayments');

    Route::get('/client/{clientId}/overdue-payments', 'RealEstatePaymentController@overduePayments');

    Route::get('realestate/payment/document/{id}', 'RealEstatePaymentController@showDocument');

    Route::post('/client/make-payments/{client}', 'RealEstatePaymentController@makePayment');

    Route::get('/client/realestate/{project}/{client}/statements', 'RealEstateClientsController@getREStatements');

    Route::get('/client/realestate/{project}/{client}/{date_from}/{date_to}/download-statement', 'RealEstateClientsController@downloadReStatement');

    Route::get('/client/realestate/{project}/{client}/{date_from}/{date_to}/download-statement-excel', 'RealEstateClientsController@downloadExcelReStatement');

    Route::get('client/realestate/projects/{client}', 'RealEstateClientsController@reProjects');
});


/**
 * Cypesa Billing
 */
Route::group(['before' => 'auth', 'namespace' => 'Api\Billing'], function () {

    Route::get('/billing/all-billing-services', 'BillingController@allBills');

    Route::get('/billing/{clientId}/client-billing', 'BillingController@clientBills');

    Route::get('/billing/all-airtime-types', 'BillingController@allAirtimeTypes');

    Route::get('/billing/source-funds', 'BillingController@sourceFunds');

    Route::post('/billing/save-bill-details', 'BillingController@saveBillDetails');

    Route::post('/billing/buy-airtime-request', 'BillingController@buyAirtime');

    Route::get('/billing/{bill}/{client}/delete-bill', 'BillingController@deleteClientBill');

    Route::post('/billing/{bill}/{client}/update-bill', 'BillingController@updateClientBill');
});

/**
 * Loyalty Points
 */
Route::group(['before' => 'auth', 'namespace' => 'Api\LoyaltyPoints'], function () {

    Route::get('/{client}/loyalty-points-summaries', 'LoyaltyPointsController@loyaltyPointsSummaries');

    Route::get('/vouchers-details', 'LoyaltyPointsController@vouchersDetails');

    Route::get('/vouchers', 'LoyaltyPointsController@getVouchers');

    Route::post('/clients/loyalty/redeem/voucher/', 'LoyaltyPointsController@redeemVoucher');

    Route::get('/client/{client}/activity-log/loyalty-brief', 'LoyaltyPointsController@loyaltyBrief');

    Route::get('/client/{client}/redeem-instructions', 'LoyaltyPointsController@getRedeemInstructions');
});
