<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 02/07/2018
 * Time: 16:33
 */

Route::post('/authy/temp/one-touch/status', [
    'middleware' => 'validate_authy',
    'uses'=>'Auth\AuthController@authyCallback'
]);