<?php
/**
 * Date: 21/09/2016
 * Time: 5:23 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Providers\CrimsClient;

use Barryvdh\DomPDF\ServiceProvider;
use Illuminate\Support\Str;

/**
 * Class MacrosProvider
 *
 * @package App\Providers
 */
class MacrosProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        Str::macro(
            'glueWithAnd',
            function (array $pieces) {
                return glueWithAnd($pieces);
            }
        );
    }
}
