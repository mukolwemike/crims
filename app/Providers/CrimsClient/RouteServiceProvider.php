<?php

namespace App\Providers\CrimsClient;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\CrimsClient\Controllers';

    protected $apiVersions = [];

    protected $mobileAuthVersions = [1];

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $files = $this->getMatchingVersions('api', 'api_v');

        $mobileAuthFiles = $this->getMatchingVersions('mobile_auth', 'mobile_auth_v');

        $this->mapVersionedApi([base_path('routes/client/guest_api.php')], 'api', ['web']);

        $this->mapVersionedApi([base_path('routes/client/guest_api.php')], 'mobile', []);

        $this->mapVersionedApi($files, 'api', ['web', 'auth']);

        $this->mapVersionedApi($files, 'mobile', ['auth:api']);

        $this->mapBeforeAuthRoutes($mobileAuthFiles);

        $this->mapWildCardRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $routes = ['web', 'guest_web'];

        foreach ($routes as $route) {
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path("routes/client/{$route}.php"));
        }
    }

    protected function mapVersionedApi(array $files, $prefix, $middleware)
    {
        foreach ($files as $file) {
            Route::prefix($prefix)
                ->middleware($middleware)
                ->namespace($this->namespace)
                ->group($file);
        }
    }

    protected function mapBeforeAuthRoutes($files)
    {
        foreach ($files as $file) {
            Route::prefix('beforeAuth')
                ->namespace($this->namespace)
                ->group($file);
        }
    }

    protected function mapWildCardRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/client/wildcard.php'));

        Route::middleware([])
            ->namespace($this->namespace)
            ->group(base_path('routes/client/incoming-webhooks.php'));
    }

    private function getMatchingVersions($baseFile, $versionedFileName)
    {
        $files[] = base_path('routes/client/' . $baseFile . '.php');

        $requested_version = request()->header('X-API-VERSION');

        $versions = array_filter(($baseFile == 'api')
            ? $this->apiVersions
            : $this->mobileAuthVersions, function ($version) use ($requested_version) {
                return $version <= $requested_version;
            });

        foreach ($versions as $version) {
            $files[] = base_path("routes/client/" . $versionedFileName . $version . ".php");
        }

        return $files;
    }
}
