<?php

namespace App\Providers\CrimsClient;

use App\Cytonn\Authentication\Client\AuthRepository;
use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use App\Cytonn\Models\ClientUser as User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::tokensExpireIn(Carbon::now()->addMinutes(15));

        Passport::refreshTokensExpireIn(Carbon::now()->addMinutes(30));
        
        Gate::define(
            'access-client',
            function (User $user, Client $client) {
                return (new AuthRepository())->checkClientAccess($user, $client);
            }
        );
    
        Gate::define(
            'be-fa-for-client',
            function (User $user, Client $client) {
                return (new AuthRepository())->checkIsFaOrOwn($user, $client);
            }
        );
    }
}
