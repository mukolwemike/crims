<?php

namespace App\Providers\CrimsClient;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        // UNITIZATION
        'App\Events\Unitization\UnitFundTransferPerformed' => [
            'App\Listeners\Unitization\WhenUnitFundTransferPerformed',
        ],
    ];
    
    /**
     * the subscriber classes
     *
     * @var array
     */
    protected $subscribe = [
        'App\Listeners\Clients\InvestmentApplicationListener',
        'App\Listeners\Clients\InvestmentActionsListener',
        'App\Listeners\Clients\WhenUnitFundInstructionHasBeenMade'
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
