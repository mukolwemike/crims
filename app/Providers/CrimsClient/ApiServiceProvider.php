<?php

namespace App\Providers\CrimsClient;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiServiceProvider extends ServiceProvider
{


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $manager = new Manager();

        ResponseFactory::macro(
            'item',
            function ($item, $transformer, array $meta = []) use ($manager) {
                $resource = new Item($item, $transformer);

                $resource->setMeta($meta);

                $data =  $manager->createData($resource)->toArray();

                return $this->json($data);
            }
        );

        ResponseFactory::macro(
            'collection',
            function ($collection, $transformer, array $meta = []) use ($manager) {
                $resource = new Collection($collection, $transformer);

                $resource->setMeta($meta);

                $data =  $manager->createData($resource)->toArray();

                return $this->json($data);
            }
        );
        
        ResponseFactory::macro(
            'accepted',
            function ($location = null, $content = null) {
                $headers = [];
                if ($location) {
                    $headers = ['Location' => $location];
                }

                return $this->make($content, Response::HTTP_ACCEPTED, $headers);
            }
        );
        
        ResponseFactory::macro(
            'created',
            function ($location = null, $content = null) {
                $headers = [];
                if ($location) {
                    $headers = ['Location' => $location];
                }

                return $this->make($content, Response::HTTP_CREATED, $headers);
            }
        );

        ResponseFactory::macro(
            'noContent',
            function ($status = 200, array $headers = []) {
                return $this->make('', $status, $headers);
            }
        );

        ResponseFactory::macro(
            'error',
            function ($message, $statusCode) {
                throw new HttpException($statusCode, $message);
            }
        );

        ResponseFactory::macro(
            'errorNotFound',
            function ($message = 'Not Found') {
                $this->error($message, Response::HTTP_NOT_FOUND);
            }
        );

        ResponseFactory::macro(
            'errorBadRequest',
            function ($message = 'Bad Request') {
                $this->error($message, Response::HTTP_BAD_REQUEST);
            }
        );

        ResponseFactory::macro(
            'errorForbidden',
            function ($message = 'Forbidden') {
                $this->error($message, Response::HTTP_FORBIDDEN);
            }
        );

        ResponseFactory::macro(
            'errorInternal',
            function ($message = 'Internal Error') {
                $this->error($message, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        );

        ResponseFactory::macro(
            'errorUnauthorized',
            function ($message = 'Unauthorized') {
                $this->error($message, Response::HTTP_UNAUTHORIZED);
            }
        );

        ResponseFactory::macro(
            'errorMethodNotAllowed',
            function ($message = 'Method Not Allowed') {
                $this->error($message, Response::HTTP_METHOD_NOT_ALLOWED);
            }
        );
    }
}
