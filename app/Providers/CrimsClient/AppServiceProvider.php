<?php

namespace App\Providers\CrimsClient;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Queue;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::looping(function () {
            while (DB::transactionLevel() > 0) {
                DB::rollBack();
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (extension_loaded('newrelic')) {
            newrelic_set_appname("crimsClient");
        }

        if ($this->app->environment() == 'local') {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }

        Collection::macro(
            'lists',
            function ($key, $val = null) {
                return $this->pluck($key, $val)->all();
            }
        );

        Collection::macro('sortByDate', function ($column = 'created_at', $order = SORT_DESC) {
            if ($order == 'ASC') {
                $order = SORT_ASC;
            }
            if ($order == 'DESC') {
                $order = SORT_ASC;
            }

            return $this->sortBy(
                function ($item) use ($column) {
                    return strtotime($item->$column);
                },
                SORT_REGULAR,
                $order == SORT_DESC
            );
        });

        Builder::macro(
            'lists',
            function ($key, $val = null) {
                return $this->pluck($key, $val);
            }
        );
    }
}
