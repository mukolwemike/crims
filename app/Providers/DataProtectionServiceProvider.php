<?php

namespace App\Providers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\User;
use Cytonn\System\DataProtection\MonitorRowChanges;
use Illuminate\Support\ServiceProvider;

class DataProtectionServiceProvider extends ServiceProvider
{
    protected $protect = [
    //        ClientInvestment::class,
    //        User::class,
    //        CustodialTransaction::class
    ];


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->protect as $protected) {
            app($protected)->observe(MonitorRowChanges::class);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
