<?php namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{

    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }


    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $files = [
            'routes/admin/web.php',
            'routes/admin/modules/clients.php',
            'routes/admin/modules/investments.php',
            'routes/admin/modules/portfolio.php',
            'routes/admin/modules/real_estate.php',
            'routes/admin/modules/shares.php',
            'routes/admin/modules/unitization.php',
            'routes/admin/modules/system.php'
        ];

        Route::middleware(['web', 'guest'])
            ->namespace($this->namespace)
            ->group(base_path('routes/admin/modules/guest.php'));

        foreach ($files as $file) {
            Route::middleware(['web', 'auth'])
                ->namespace($this->namespace)
                ->group(base_path($file));
        }
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/admin/api.php'));
    }
}
