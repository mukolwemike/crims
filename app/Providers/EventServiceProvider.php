<?php

namespace App\Providers;

use App\Events\Approvals\ClientTransactionApproved;
use App\Events\Investments\Actions\WithdrawalHasBeenMade;
use App\Events\Investments\Actions\WithdrawalHasBeenRemoved;
use App\Events\Unitization\UnitFundWithdrawalFailed;
use App\Listeners\Investments\Actions\InvestmentApplicationListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        //Users
        'App\Events\Users\UserRegisteredNotification' => [
            'App\Listeners\Users\WhenUserRegisteredNotification',
        ],
        'App\Events\Users\UserAssignedPermission' => [
            'App\Listeners\Users\WhenUserAssignedPermission',
        ],
        'App\Events\Users\UserDeactivated' => [
            'App\Listeners\Users\WhenUserDeactivated',
        ],
        'App\Events\Users\UserReactivated' => [
            'App\Listeners\Users\WhenUserReactivated',
        ],
        WithdrawalHasBeenMade::class => [
            \App\Listeners\Investments\Actions\WithdrawalHasBeenMade::class
        ],
        WithdrawalHasBeenRemoved::class => [
            \App\Listeners\Investments\Actions\WithdrawalHasBeenRemoved::class
        ],

        ClientTransactionApproved::class => [
            \App\Listeners\Approvals\ClientTransactionApproved::class
        ],

        //Roles
        'App\Events\Roles\RoleAdded' => [
            'App\Listeners\Roles\WhenRoleAdded',
        ],
        'App\Events\Roles\RolePermissionsUpdated' => [
            'App\Listeners\Roles\WhenRolePermissionsUpdated',
        ],

        //Reports
        'App\Events\Reports\Investments\MonthlyWithdrawalSummary' => [
            'App\Listeners\Reports\Investments\WhenMonthlyWithdrawalSummary',
        ],

        //Portfolio
        'App\Events\Portfolio\Clients\PortfolioInvestorClientAdded' => [
            'App\Listeners\Portfolio\Clients\WhenPortfolioInvestorClientAdded',
        ],
        'App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenMade' => [
            'App\Listeners\Portfolio\Actions\WhenPortfolioRepaymentHasBeenMade',
        ],
        'App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenRemoved' => [
            'App\Listeners\Portfolio\Actions\WhenPortfolioRepaymentHasBeenRemoved',
        ],

        //ElasticSearch
        'App\Events\ElasticSearch\DailyElasticSearchIndex' => [
            'App\Listeners\ElasticSearch\WhenDailyElasticSearchIndex',
        ],

        //ElasticSearch
        'App\Events\Investments\Actions\UnitFundPurchaseHasBeenMade' => [
            'App\Listeners\Investments\Actions\WhenUnitFundPurchaseHasBeenMade',
        ],

        'App\Events\Investments\Actions\UnitFundSaleHasBeenMade' => [
            'App\Listeners\Investments\Actions\WhenUnitFundSaleHasBeenMade',
        ],

        'App\Events\Shares\SharePurchaseOrderHasBeenMatched' => [
            'App\Listeners\Shares\WhenSharePurchaseOrderHasBeenMatched',
        ],

        'App\Events\Shares\SharePurchaseOrderHasBeenSettled' => [
            'App\Listeners\Shares\WhenSharePurchaseOrderHasBeenSettled',
        ],

        //Investments
        'App\Events\Investments\Actions\MaturityDateEdited' => [
            'App\Listeners\Investments\Actions\WhenMaturityDateEdited',
        ],

        'App\Events\RealEstate\UnitHasBeenEdited' => [
            'App\Listeners\RealEstate\WhenUnitHasBeenEdited',
        ],

        //UNITIZATION
        'App\Events\Unitization\UnitFundSaleHasBeenPaid' => [
            'App\Listeners\Unitization\WhenUnitFundSaleHasBeenPaid',
        ],
        'App\Events\Unitization\UnitFundSaleHasBeenRemoved' => [
            'App\Listeners\Unitization\WhenUnitFundSaleHasBeenRemoved',
        ],

        UnitFundWithdrawalFailed::class => [
            \App\Listeners\Unitization\UnitFundWithdrawalFailed::class
        ],

        // REALESTATE
        'App\Events\RealEstate\ClientHoldingPaymentHasBeenCompleted' => [
            'App\Listeners\RealEstate\WhenClientHoldingPaymentHasBeenCompleted'
        ],

        //Utilites
        'App\Events\Utilities\UtilityPaymentHasBeenMade' => [
            'App\Listeners\Utilities\WhenUtilityPaymentHasBeenMade'
        ],
        'App\Events\Utilities\UtilityPaymentHasFailed' => [
            'App\Listeners\Utilities\WhenUtilityPaymentHasFailed'
        ]
    ];

    protected $subscribe = [
        'App\Listeners\Investments\Actions\InvestmentActionsListener',
        InvestmentApplicationListener::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
