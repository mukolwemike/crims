<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 13/05/2019
 * Time: 14:44
 */

namespace App\Mail;

use Cytonn\Core\Storage\StorageInterface;
use Illuminate\Mail\Events\MessageSent;
use Swift_Mime_SimpleMessage;

class MailSaver
{

    public function save(Swift_Mime_SimpleMessage $message)
    {
        $file_contents = $message->toString();

        $url = 'mails/'.'.eml';

        \App::make(StorageInterface::class)->filesystem()->put($url, $file_contents);
    }
}
