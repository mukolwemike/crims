<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Mail;

use Illuminate\Support\Facades\Config;

trait MailerManager
{
    /**
     * Check the toName from the data array
     */
    public function checkCc($mailable, $data)
    {
        if (isset($data['cc'])) {
            return $mailable->cc($data['cc']);
        }

        return $mailable;
    }

    /**
     * Check the toName from the data array
     */
    public function checkBcc($mailable, $data)
    {
        if (isset($data['bcc'])) {
            $mailable->bcc(array_merge(Config::get('system.administrators')));
        } else {
            $mailable->bcc(Config::get('system.administrators'));
        }

        return $mailable;
    }

    /**
     * Check the toName from the data array
     */
    public function checkToName($mailable, $data)
    {
        if (isset($data['toName'])) {
            $mailable->to($data['to'], $data['toName']);
        } else {
            $mailable->to($data['to']);
        }

        return $mailable;
    }

    /**
     * Check the toName from the data array
     */
    public function checkFrom($mailable, $data)
    {
        if (isset($data['from'])) {
            if (isset($data['fromName'])) {
                $mailable->from($data['from'], $data['fromName']);
            } else {
                $mailable->from($data['from']);
            }
        }

        return $mailable;
    }

    /**
     * Check the toName from the data array
     */
    public function checkAttachments($mailable, $data)
    {
        if (isset($data['attachments'])) {
            $mailable->attach($data['attachments']);
        }

        return $mailable;
    }
}
