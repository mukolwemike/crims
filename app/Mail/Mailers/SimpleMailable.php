<?php

namespace App\Mail\Mailers;

use App\Mail\MailerManager;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SimpleMailable extends Mailable
{
    use Queueable, SerializesModels;

    /*
     * Get the mailer manager
     */
    use MailerManager;

    /*
     * Specify the data required for the mailable
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->data['view'])
            ->to($this->data['to'])
            ->subject($this->data['subject'])
            ->checkCc($this, $this->data)
            ->checkBcc($this, $this->data)
            ->with($this->data);
    }
}
