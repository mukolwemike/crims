<?php

namespace App\Jobs;

use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SuperClosure\Serializer;

class ProcessQueuedClosure implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $closure;

    /**
     * @var null
     */
    private $key;

    private $overwrite = false;

    public $tries = 10;

    private $ignored = [
//        'dashboard_fund_valuation_for_fm'
    ];

    /**
     * Create a new job instance.
     *
     * @param callable $closure
     * @param null $key
     * @param bool $overwrite
     */
    public function __construct($closure, $key = null, $overwrite = false)
    {
        $this->closure = $closure;
        $this->key = $key;
        $this->overwrite = $overwrite;
    }

    public function handle()
    {

        $this->reset();

        try {
            $this->handleJob();
        } catch (\Exception $e) {
            $this->release(180);

            throw $e;
        }
    }

    /**
     * Execute the job.
     * @throws \Exception
     * @return void
     */
    public function handleJob()
    {

        $intro = 'ClosureRunner:: ';

        if ($this->overwrite) {
            $intro = $intro."Forced -> ";
        }

        if (php_sapi_name() == 'cli' && app()->environment() != 'testing') {
            print($intro."Handling closure with key - ".$this->key);
            print(PHP_EOL);
        }
        
        $cached = $this->inCache();

        if ((!$this->overwrite) && $cached) {
            print($intro."Skipped (in cache) - ".$this->key);
            return;
        }

        $unserialized = (new Serializer())->unserialize($this->closure);

        $val = call_user_func($unserialized);

        if ($this->key) {
            \Cache::put($this->key, $val, Carbon::now()->addMinutes(60));
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function inCache()
    {
        if (!$this->key) {
            return false;
        }

        return cache()->has($this->key);
    }

    private function reset()
    {
        while (DB::transactionLevel() > 0) {
            DB::rollBack(0);
        }
    }
}
