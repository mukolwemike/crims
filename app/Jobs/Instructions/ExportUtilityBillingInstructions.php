<?php

namespace App\Jobs\Instructions;

use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExportUtilityBillingInstructions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $start;

    protected $end;

    /**
     * ExportUtilityBillingPayments constructor.
     *
     * @param User $user
     * @param Carbon $start
     * @param Carbon $end
     */
    public function __construct(User $user, Carbon $start, Carbon $end)
    {
        $this->user = $user;
        $this->start = $start;
        $this->end = $end;
    }

    public function handle()
    {
        $instructions = UtilityBillingInstructions::betweenDates($this->start, $this->end)
            ->get()
            ->map(function ($instruction) {
                $out = new EmptyModel();

                $client = $instruction->client;

                $payload = \GuzzleHttp\json_decode($instruction->payload);

                $out->{'Reference'} = $instruction->id;
                $out->{'Client Code'} = $client->client_code;
                $out->{'Client Name'} = ClientPresenter::presentFullNames($client->id);
                $out->{'Account Name'} = isset($payload->account_name) ? $payload->account_name : 'Airtime - ' .$payload->account_number;
                $out->{'Account Number'} = $instruction->account_number;
                $out->{'Amount'} = $instruction->amount;
                $out->{'Number'} = $instruction->number ? $instruction->number : 0;
                $out->{'Bill Name'} = $instruction->utility->name;
                $out->{'Bill Type'} = $instruction->utility->type->name;
                $out->{'Source Fund'} = $instruction->unitFund->name;
                $out->{'To'} = 'Cypesa Wallet';
                $out->{'Date'} = DatePresenter::formatDate($instruction->date);
                $out->{'Sent On'} = Carbon::parse($instruction->created_at)->toDateString();
                $out->{'Status'} = $instruction->status->name;

                return $out;
            });

        $fileName = 'Utility Billing Instructions from ' . $this->start->copy()->toFormattedDateString() . ' to '
            . $this->end->copy()->toFormattedDateString();

        ExcelWork::generateAndStoreSingleSheet($instructions, $fileName);

        $file_path = storage_path('exports/' . $fileName . '.xlsx');

        $this->mailResult($fileName);

        \File::delete($file_path);
    }

    private function mailResult($fileName)
    {
        Mailer::compose()
            ->to($this->user->email)
            ->bcc(config('system.administrators'))
            ->from('support@cytonn.com')
            ->subject($fileName)
            ->text('Please find attached an export of utility billing instructions from '
                . $this->start->copy()->toFormattedDateString() . ' to '
                . $this->end->copy()->toFormattedDateString())
            ->excel([$fileName])
            ->send();
    }
}
