<?php

namespace App\Jobs\Instructions;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\BankDetails;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExportBankInstructions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $start;

    protected $end;

    /**
     * BankInstructions constructor.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     */
    public function __construct(Carbon $start, Carbon $end, User $user)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $instructions = BankInstruction::with([
            'custodialTransaction',
            'clientBankAccount',
            'custodialTransaction.client',
            'custodialTransaction.client.contact'
        ])->betweenDates($this->start, $this->end)
            ->whereHas('custodialTransaction', function ($transaction) {
                $transaction->whereHas('clientPayment', function ($payment) {
                    $payment->whereNull('submit_status');
                });
            })
            ->get()
            ->each(function ($instruction) {
                if (!$instruction->clientBankAccount) {
                    $default_account = ClientBankAccount::where([
                        'client_id' => $instruction->custodialTransaction->client_id,
                        'default' => true
                    ])->first();

                    $instruction->clientBankAccount = $default_account;
                }
            })
            ->map(function ($instruction) {
                $out = new EmptyModel();


                $custodialTransaction = $instruction->custodialTransaction;

                $client = $custodialTransaction->client;

                $for =  $instruction->paymentFor() . " ({$custodialTransaction->custodialAccount->currency->code})";

                $bank = new BankDetails(null, $client, $instruction->clientBankAccount);
                $fa = $client->getLatestFa();

                $out->{'Customer Reference'} = $client->client_code. '-'.$custodialTransaction->clientPayment->id;
                $out->{'Client Code'} = $client->client_code;
                $out->{'Beneficiary Account Name'} = $bank->accountName();
                $out->{'Account Number'} = $bank->accountNumber();
                $out->{'Payment Details'} = 'Client Withdrawal';
                $out->{'Payment Amount'} = round($instruction->amount);
                $out->{'Swift Code'} = $bank->swiftCode();
                $out->{'Branch Code'} = $bank->branchCode();
                $out->{'Beneficiary Email'} = $client->contact->email;
                $out->{'Payment Type'} = $this->paymentType($bank);
                $out->{'Financial Advisor'} = $fa ? $fa->name : '';
                $out->{'Payment For'} = $for;
                $out->{'Date'} = Carbon::parse($custodialTransaction->date)->toDateString();

                return $out;
            })
            ->groupBy('Payment For');

        $file_name = 'Bank Instructions from ' . $this->start->copy()->toFormattedDateString() . ' to '
            . $this->end->copy()->toFormattedDateString();

        ExcelWork::generateAndStoreMultiSheet($instructions, $file_name);

        $file_path = storage_path('exports/' . $file_name . '.xlsx');

        $this->mailResult($file_name);

        \File::delete($file_path);
    }


    private function paymentType($bank)
    {
        if ($code = $bank->swiftCode()) {
            return $code === 'SCBLKENXXXX' ? 'BT' : 'RTGS';
        }

        return ($bank->bankName() == 'Standard Chartered Bank Kenya Limited') ? 'BT' : 'RTGS';
    }

    /**
     * @param $fileName
     */
    private function mailResult($fileName)
    {
        Mailer::compose()
            ->to($this->user->email)
            ->bcc(config('system.administrators'))
            ->from('support@cytonn.com')
            ->subject($fileName)
            ->text('Please find attached an export of bank instructions from '
                . $this->start->copy()->toFormattedDateString() . ' to '
                . $this->end->copy()->toFormattedDateString())
            ->excel([$fileName])
            ->send();
    }
}
