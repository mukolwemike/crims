<?php
/**
 * Created by PhpStorm.
 * User: Kuttoh
 * Date: 21/02/20
 * Time: 08:16 AM
 */

namespace App\Jobs\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UnitFundNonCompliantClients implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $fundManager;
    protected $user;
    protected $start;
    protected $end;

    public function __construct(FundManager $fundManager, User $user, $start = null, $end = null)
    {
        $this->fundManager = $fundManager;

        $this->user = $user;

        $this->start = is_null($start) ? Carbon::today()->subMonth() : Carbon::parse($start)->startOfDay();

        $this->end = is_null($end) ? Carbon::now() : Carbon::parse($end)->endOfDay();
    }

    public function handle()
    {
        $fundClients = Client::where('fund_manager_id', $this->fundManager->id)
            ->whereBetween('created_at', [$this->start, $this->end])
            ->get();

        $nonCompliantClients = [];

        foreach ($fundClients as $client) {
            $complianceCheck = $client->repo->compliant();

            if (!$complianceCheck) {
                $nonCompliantClients [] = [
                    'Client Code' => $client->client_code,
                    'Client Name' => ClientPresenter::presentFullNames($client->id),
                    'ID or Passport' => $client->id_or_passport ? $client->id_or_passport : ''
                ];
            }
        }

        $start = $this->start;

        $end = $this->end;

        $fileName = 'Non-Compliant ' . $this->fundManager->name . ' Clients as at ' . $end->toDateString();

        ExcelWork::generateAndStoreSingleSheet($nonCompliantClients, $fileName);

        $email = [];

        if ($this->user) {
            $email = [ $this->user->email ];
        }

        $this->mailResult($fileName, $start, $end, $email);
    }

    private function mailResult($fileName, $start, $end, $email)
    {
        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject('Non-Compliant ' . $this->fundManager->name . ' Clients as at ' . $end->toDateString())
            ->text('Here is the export of non-compliant ' . $this->fundManager->name . ' clients created between '
                . $start->toDateTimeString() . ' and ' . $end->toDateTimeString())
            ->excel([$fileName])
            ->send();

        \File::delete(storage_path('exports/' . $fileName . '.xlsx'));
    }
}
