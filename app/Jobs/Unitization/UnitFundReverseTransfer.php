<?php


namespace App\Jobs\Unitization;


use Cytonn\Unitization\Trading\Reverse;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UnitFundReverseTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $transfer;

    public $tries = 1;


    /**
     * UnitFundReverseTransfer constructor.
     */
    public function __construct($transfer)
    {
        $this->transfer = $transfer;
    }

    public function handle()
    {
        \DB::transaction(function () {
            $reverse = new Reverse();

            $reverse->transfer($this->transfer);
        });
    }
}