<?php

namespace App\Jobs\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\User;
use App\Cytonn\Unitization\UnitFundClientRepository;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Excel;
use Cytonn\Core\DataStructures\Files\Excel as CrimsExcel;

class UnitFundPurchasesReport implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $fund;

    public function __construct(UnitFund $fund = null, User $user)
    {
        $this->fund = $fund;
        $this->user = $user;
    }

    public function handle()
    {
        $fname = $this->fund->name;

        $email = $this->user ? [$this->user->email] : [];

        \Excel::create($fname, function ($excel) {

            $clients = Client::whereHas('unitFundPurchases', function ($purchase) {
                $purchase->where('unit_fund_id', $this->fund->id);
            })
                ->orderBy('client_code', 'ASC')
                ->get();

            foreach ($clients as $client) {
                $clientName = \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id);

                $clientName = CrimsExcel::cleanSheetName($clientName);

                $name = str_limit($clientName . ' - ' .$client->client_code, 28);

                $excel->sheet($name, function ($sheet) use ($client) {
                    $this->excelSheet($sheet, $client);
                });
            }
        })->store('xlsx');

        $this->sendExcel(
            ucwords($fname) . ' Report',
            "Please find attached",
            $fname,
            $email
        );
    }

    public function excelSheet($sheet, Client $client)
    {
        $date = Carbon::today();

        $start_date = $client->unitFundPurchases()->oldest('date')->first()->date;

        $processed = $client->calculateFund($this->fund, $date, $start_date);

        $actions = $processed->filteredActions();

        $opening = $processed->getPrepared()->opening;

        return $sheet->loadView('unitization.reports.client_investment_summary', [
            'date' => $date,
            'client' => $client,
            'fund' => $this->fund,
            'actions' => $actions,
            'opening_balance' => $processed->getPrepared()->opening_balance,
            'start_date' => $start_date,
            'calculation' => $this->fund->category->calculation->slug,
            'price' => $processed->getPrepared()->price,
            'closing_balance' => $processed->getPrepared()->closing_balance,
            'opening' => $opening,
        ]);
    }
}
