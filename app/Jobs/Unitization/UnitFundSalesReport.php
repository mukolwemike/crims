<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/19/18
 * Time: 10:54 AM
 */

namespace App\Jobs\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UnitFundSalesReport implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $fund;

    protected $start;

    protected $end;

    public function __construct(User $user, UnitFund $fund = null, $start = null, $end = null)
    {
        $last = UnitFundSale::orderBy('date', 'asc')->first()->date;

        $this->fund = $fund;

        $this->user = $user;

        $this->start = is_null($start) ? $last : Carbon::parse($start);

        $this->end = $end ? Carbon::parse($end): Carbon::today();
    }

    public function handle()
    {
        $fund = $this->fund;

        $sales = $this->getSales();

        $salesArray = array();

        foreach ($sales as $sale) {
            $purchase = $sale->unitFundPurchasesSales()->orderBy('date')->first();

            if (is_null($purchase)) {
                $purchase = UnitFundPurchase::where('client_id', $sale->client_id)
                    ->where('unit_fund_id', $sale->unit_fund_id)->orderBy('date')->first();
            }

            $recipient = $purchase ? $purchase->getRecipient() : null;

            $position = $recipient ? $recipient->repo->getRecipientUserPositionByDate(Carbon::parse($sale->date))
                : null;

            $salesArray[$sale->fund->name][] = [
                'Client Code' => $sale->client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($sale->client_id),
                'Client Email' => $sale->client->contact->email,
                'Units Sold' => $sale->number,
                'Value Sold' => (float)$sale->number * $sale->fund->unitPrice($sale->date),
                'Purchase Value' => (float) $sale->repository()->getPurchaseValue(),
                'Sold On' => $sale->date->toFormattedDateString(),
                'Description' => $sale->description,
                'Fund' => $sale->fund->name,
                'FA' => $recipient ? $recipient->name : null,
                'FA Type' => $recipient ? @$recipient->repo->getRecipientType(Carbon::parse($sale->date))->name : null,
                'FA Branch' => $position ? $position->present()->getDepartmentBranch : ($recipient ?
                    $recipient->present()->getBranch : null),
                'FA Department Unit' => $position ? $position->present()->getDepartmentUnit : ($recipient ?
                    $recipient->present()->getDepartmentUnit : null),
                'FA Status' => $recipient ? $recipient->present()->getActive : null,
            ];
        }

        $file_name = 'Unit_Fund_Sales_from_' . $this->start->toDateString() . '_to_' . $this->end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($salesArray, $file_name);

        $email = [];

        if ($this->user) {
            $email = [ $this->user->email ];
        }

        $this->mailResult($file_name, $this->start, $this->end, $email);
    }

    private function getSales()
    {
        $sales = UnitFundSale::nonFees(true)
            ->whereBetween('date', [ $this->start->startOfDay(), $this->end->endOfDay()]);

        if ($this->fund) {
            $sales = $sales->where('unit_fund_id', $this->fund->id);
        }

        return $sales->get();
    }

    private function mailResult($file_name, $start, $end, $email)
    {
        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject('Fund Sales Export')
            ->text('Here is the export of fund sales that you requested for the period between ' .
                $start->toDateString() . ' and  ' . $end->toDateString())
            ->excel([$file_name])
            ->send();

        \File::delete(storage_path('exports/' . $file_name . '.xlsx'));
    }
}
