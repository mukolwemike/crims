<?php


namespace App\Jobs\Unitization;


use App\Cytonn\Unitization\ProcessTransfer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UnitFundProcessTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $input;
    protected $client;
    protected $fund;

    public $tries = 1;

    /**
     * UnitFundProcessTransfer constructor.
     */
    public function __construct($input, $client, $fund)
    {
        $this->input = $input;
        $this->client  = $client;
        $this->fund = $fund;
    }


    public function handle()
    {
        \DB::transaction(function () {
            $transferProcessor = new ProcessTransfer($this->input, $this->client, $this->fund);

            $transferProcessor->process();
        });
    }
}