<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/22/18
 * Time: 11:14 AM
 */


namespace App\Jobs\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Calculator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UnitTrustClientSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ExcelMailer;

    protected $date;

    protected $user;

    protected $fund;

    public function __construct(User $user, UnitFund $fund = null, $date = null)
    {

        $this->date = $date ? Carbon::parse($date): Carbon::today();

        $this->user = $user;

        $this->fund = $fund;
    }

    public function handle()
    {
        $clientSummary = $this->clientSummary();

        $fileName = ucfirst($this->fund->name) . ' Client Summary';

        $email = $this->user ? $this->user->email : [];

        ExcelWork::generateAndStoreSingleSheet($clientSummary, $fileName);

        Mailer::compose()
            ->to($email)
            ->bcc([])
            ->subject($fileName)
            ->text(
                'Please find attached the unit trust client summary as at '
                .$this->date->toFormattedDateString()
            )
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    public function clientSummary()
    {
        return Client::whereHas('unitFundPurchases', function ($purchase) {
                $purchase->where('date', '<=', $this->date);
        })
            ->get()
            ->map(function (Client $client) {
                $calc = $client->calculateFund($this->fund, $this->date);

                return [
                    'Client Code' => $client->client_code,
                    'Name' => $client->name(),
                    'Inflow' => $in = $calc->totalPurchases(),
                    'Withdrawal' => $out = $calc->totalSales(),
                    'Balance' => $in - $out,
                    'Net Interest' => $calc->totalInterest(),
                    'Total' => $calc->totalAmount()
                ];
            })
            ->toArray();
    }
}
