<?php

namespace App\Jobs\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\BooleanPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;

class UnitTrustClientsContacts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ExcelMailer;

    protected $start;

    protected $end;

    protected $user;

    protected $fund;

    public function __construct(User $user, UnitFund $fund = null, $start = null, $end = null)
    {
        $last = UnitFundPurchase::orderBy('date', 'asc')->first()->date;

        $this->start = is_null($start) ? $last : Carbon::parse($start);

        $this->end = $end ? Carbon::parse(): Carbon::today();

        $this->user = $user;

        $this->fund = $fund;
    }

    public function handle()
    {
        $fileName = 'Fund Clients\' Contact Summary';
        
        $start = $this->start;

        $end = $this->end;

        if ($this->fund) {
            $fund = $this->fund;

            Excel::create(
                $fileName,
                function ($excel) use ($fund, $start, $end) {
                    $this->generateSheet($excel, $start, $end, $fund);
                }
            )->store('xlsx');
        } else {
            $funds = UnitFund::has('purchases')->get();

            Excel::create(
                $fileName,
                function ($excel) use ($funds, $start, $end) {
                    $funds->each(
                        function ($fund) use ($excel, $start, $end) {
                            $this->generateSheet($excel, $start, $end, $fund);
                        }
                    );
                }
            )->store('xlsx');
        }

        $this->sendExcel(
            $fileName,
            'Please find the attached the requested unit holders\' summary for unit holders active between '.
            $this->start->copy()->toFormattedDateString() . ' and ' . $this->end->copy()->toFormattedDateString(),
            $fileName,
            [   $this->user->email  ]
        );

        return true;
    }

    private function generateSheet($excel, $start, $end, $fund)
    {
        $excel->sheet($fund->short_name, function ($sheet) use ($fund, $start, $end) {

            $clients = Client::whereHas('unitFundPurchases', function ($purchase) use ($fund, $start, $end) {
                    return $purchase->forUnitFund($fund)->activeBetweenDates($start, $end);
            })->get();

            $h = $clients->map(function (Client $client) use ($fund, $end) {
//                $phoneArray = implode($client->getContactPhoneNumbersArray());

                $e = new EmptyModel();

                $fa = $client->getLatestFA('units');

                $firstPurchase = $client->unitFundPurchases()->orderBy('date')->first();

                $firstPurchaseDate = $firstPurchase ? Carbon::parse($firstPurchase->date) : null;

                $calculator = $client->calculateFund($fund, $end, $firstPurchaseDate, false);

                $e->fill([
                    'Client Code' => $client->client_code,
                    'First Name' => $client->clientType->name == 'corporate'
                        ? $client->contact_person_fname : $client->contact->firstname,
                    'Last Name' => $client->contact->lastname,
                    'Phone' => $client->phone(),
                    'Email' => $client->contact->email,
                    'Employee Number' => $client->employee_number,
                    'FA' => $fa ? $fa->name : '',
                    'FA Email' => $fa ? $fa->email : '',
                    'All Emails' => implode(', ', $client->getContactEmailsArray()),
                    'Franchise' => BooleanPresenter::presentYesNo($client->franchise),
                    'Age' => is_null($client->dob)
                        ? null : Carbon::parse($client->dob)->diffInYears(Carbon::today(), true),
                    'Country' => is_null($client->country) ? null : $client->country->name,
                    'Fund Invested' => $fund->name,
                    'Number of Owned Units' => $calculator->totalUnits(),
                    'Value of Owned Units' => AmountPresenter::currency($calculator->totalUnits() * $fund->unitPrice($end->copy())),
                    'Is Active' => $client->repo->isActiveInUnitFundAsAt($fund, $end) ? "Yes" : "No",
                    'Total Purchases' => $calculator->totalPurchases(),
                    'Total Sales' => $calculator->totalSales(),
                    'First Purchase Date' => $firstPurchase ? Carbon::parse($firstPurchase->date)->toDateString() : '',
                    'KRA Pin' => $client->pin_no,
                    'Address' => $client->present()->getAddress,
                    'Physical Address' => $client->residence,
                    'Employment Status' => ($client->clientType->name == 'individual' and $client->employment)
                        ? $client->employment->name : null,
                    'Employment Other' => $client->clientType->name == 'individual' ? $client->employment_other : null,
                    'Business Sector' => $client->clientType->name == 'individual' ? $client->business_sector : null,
                    'Present Occupation' => $client->clientType->name == 'individual' ?
                        $client->present_occupation :
                        null,
                    'Employer Name' => $client->clientType->name == 'individual' ? $client->employer_name : null,
                    'Employer Address' => $client->clientType->name == 'individual' ? $client->employer_address : null,
                    'Telephone Office' => $client->telephone_office,
                    'Total Active SP Investments' => $client->repo->getTodayTotalInvestmentsValueForAllProducts(),
                ]);

                return $e;
            });

            $sheet->fromModel($h);
        });
    }
}
