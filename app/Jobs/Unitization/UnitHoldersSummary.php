<?php

namespace App\Jobs\Unitization;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;

class UnitHoldersSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ExcelMailer;

    protected $start;

    protected $end;

    protected $user;

    protected $fund;

    public function __construct($start, $end, User $user, UnitFund $fund = null)
    {
        $this->start = is_null($start) ? UnitFundPurchase::latest()->last()->date : Carbon::parse($start);
        $this->end = Carbon::parse($end);
        $this->user = $user;
        $this->fund = $fund;
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = 'Unit Holders\' Summary Export';
        $start = $this->start;
        $end = $this->end;

        if ($this->fund) {
            $fund = $this->fund;
            Excel::create(
                $fileName,
                function ($excel) use ($fund, $start, $end) {
                    $this->generateSheet($excel, $start, $end, $fund);
                }
            )->store('xlsx');
        } else {
            $funds = UnitFund::all();
            Excel::create(
                $fileName,
                function ($excel) use ($funds, $start, $end) {
                    $funds->each(
                        function ($fund) use ($excel, $start, $end) {
                            $this->generateSheet($excel, $start, $end, $fund);
                        }
                    );
                }
            )->store('xlsx');
        }

        $this->sendExcel(
            $fileName,
            'Please find the attached the requested unit holders\' summary for unit holders active between '.
            $this->start->copy()->toFormattedDateString() . ' and ' . $this->end->copy()->toFormattedDateString(),
            $fileName,
            [$this->user->email]
        );

        return true;
    }

    private function generateSheet($excel, $start, $end, $fund)
    {
        $excel->sheet(
            $fund->name,
            function ($sheet) use ($fund, $start, $end) {
                $holders = UnitFundHolder::where('unit_fund_id', $fund->id)
                ->whereHas(
                    'purchases',
                    function ($purchase) use ($start, $end) {
                        return $purchase->betweenDates($start, $end);
                    }
                )->get();

                $h = $holders->map(
                    function (UnitFundHolder $holder) use ($fund, $end) {
                        $e = new EmptyModel();
                        $e->fill(
                            [
                            'Holder Number' => $holder->number,
                            'Holder Name' => ClientPresenter::presentJointFullNames($holder->client_id),
                            'First Name' => ClientPresenter::presentJointFirstNames($holder->client_id),
                            'Phone' => $holder->client->contact->phone,
                            'Email' => $holder->client->contact->email,
                            'All Emails' => implode(', ', $holder->client->getContactEmailsArray()),
                            'Country' => is_null($holder->client->country) ? null : $holder->client->country->name,
                            'Number of Owned Units' => $holder->repo->getOwnedUnitsAsAt($end),
                            'Value of Owned Units' => $holder->repo->getValueOfOwnedUnitsAsAt($end)
                            ]
                        );
                        return $e;
                    }
                );

                $sheet->fromModel($h);
            }
        );
    }
}
