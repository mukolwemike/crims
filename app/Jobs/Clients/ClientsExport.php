<?php

namespace App\Jobs\Clients;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\ClientsWithInvestmentsAndOrRealEstateExcelGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClientsExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * ClientsExport constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file_name = 'Clients With Investments Or Real Estate Report - ' .
            DatePresenter::formatDate(Carbon::today()->toDateString());

        (new ClientsWithInvestmentsAndOrRealEstateExcelGenerator())->excel($file_name)->store('xlsx');

        $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

        $mailer = new GeneralMailer();
        $mailer->to($this->user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Client With Investments and Real Estate');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Please find attached an Excel document of the all clients with 
        either investments and/or real estate.');

        \File::delete($file_path);
    }
}
