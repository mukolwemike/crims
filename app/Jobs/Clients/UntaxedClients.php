<?php

namespace App\Jobs\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UntaxedClients implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * UntaxedClients constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = Carbon::today();
        $untaxed_clients = Client::where('taxable', '<>', 1)
            ->get()
            ->map(
                function ($client) {
                    $c = new EmptyModel();
                    $c->Client_code = $client->client_code;
                    $c->Name = ClientPresenter::presentJointFullNames($client->id);
                    $c->Email = $client->contact->email;
                    $c->Phone = $client->contact->phone;

                    return $c;
                }
            );

        if ($untaxed_clients->count()) {
            $filename = 'Untaxed Clients';

            Excel::fromModel($filename, $untaxed_clients)->store('xlsx');

            $this->mailResult(storage_path('exports/' . $filename . '.xlsx'), $date, $this->user);
        }
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $date
     * @param User $user
     */
    private function mailResult($file_path, $date, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Untaxed Clients');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find attached an export of all untaxed clients as at ' .
            $date->toFormattedDateString());
    }
}
