<?php

namespace App\Jobs\Clients;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExportFAClients implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $fas;
    /**
     * @var null
     */
    private $start;
    /**
     * @var null
     */
    private $end;

    private $faEmails;

    /**
     * ExportFAClients constructor.
     *
     * @param User $user
     * @param $fas
     * @param null $start
     * @param null $end
     */
    public function __construct(User $user, $fas, $start = null, $end = null)
    {
        $this->user = $user;
        $this->fas = $fas;
        $this->start = isNotEmptyOrNull($start) ? Carbon::parse($start) : null;
        $this->end = isNotEmptyOrNull($end) ? Carbon::parse($end) : Carbon::now();
        $this->faEmails = $this->fas->lists('email');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dataArray = array();

        $dataArray = array_merge($dataArray, $this->getInvestments());

        $dataArray = array_merge($dataArray, $this->getRealEstate());

        $dataArray = array_merge($dataArray, $this->getUnitFunds());

        ksort($dataArray);

        $summary = $this->getSummary($dataArray);

        $dataArray = array_merge($summary, $dataArray);

        $dataCol = collect($dataArray);

        if ($dataCol->count()) {
            $file_name = 'FA Clients Export';
            Excel::fromModel($file_name, $dataCol)->store('xlsx');
            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';
            $this->mailResult($this->user, $file_path);

            \File::delete($file_path);
        }
    }

    private function getSummary($dataArray)
    {
        $summaryArray = array();

        foreach ($dataArray as $key => $data) {
            $firstRecord = $data[0];

            $fa = $firstRecord->FA;

            $data = collect($data);

            if (!array_key_exists($fa, $summaryArray)) {
                $summaryArray[$fa]['FA Name'] = $fa;
                $summaryArray[$fa]['Total Principal'] = 0;
                $summaryArray[$fa]['Total Kes Value As At End'] = 0;
                $summaryArray[$fa]['Total RE Sales'] = 0;
                $summaryArray[$fa]['Total RE Payments'] = 0;
                $summaryArray[$fa]['Total Unit Trust Purchases'] = 0;
            }

            if ($firstRecord->Type == 'Investments') {
                $summaryArray[$fa]['Total Principal'] = $data->sum('Principal');
                $summaryArray[$fa]['Total Kes Value As At End'] = $data->sum('Kes Value As At End');
            } elseif ($firstRecord->Type == 'Real Estate') {
                $summaryArray[$fa]['Total RE Sales'] = $data->sum('Price');
                $summaryArray[$fa]['Total RE Payments'] = $data->sum('Paid');
            } elseif ($firstRecord->Type == 'Unit Funds') {
                $summaryArray[$fa]['Total Unit Trust Purchases'] = $data->sum('Purchase Value');
            }
        }

        return ['Summary' => $summaryArray];
    }

    /**
     * @return array
     */
    private function getInvestments()
    {
        $investments = ClientInvestment::whereHas('commission', function ($commission) {
            $commission->whereHas('recipient', function ($recipient) {
                $recipient->whereIn('email', $this->faEmails);
            });
        });

        if ($this->start) {
            $investments = $investments->where('invested_date', '>=', $this->start);
        }
        if ($this->end) {
            $investments = $investments->where('invested_date', '<=', $this->end)
                ->activeOnDate($this->end);
        }

        $investments = $investments->get();

        $investmentArray = array();

        foreach ($investments as $investment) {
            $c = new EmptyModel();
            $c->{'Client Code'} = $investment->client->client_code;
            $c->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
            $c->{'Email'} = $investment->client->contact->email;
            $c->{'Phone'} = $investment->client->contact->phone;
            $fa = $investment->commission->recipient;
            $c->{'Principal'} = $investment->amount;
            $c->{'Invested Date'} = $investment->invested_date;
            $c->{'Maturity Date'} = $investment->maturity_date;
            $c->{'Inflow'} = $investment->repo->inflow();
            $value = $investment->repo->getTotalValueOfInvestmentAtDate(Carbon::parse($this->end));
            $c->{'Value As At End'} = $value;
            $c->{'Kes Value As At End'} = convert($value, $investment->product->currency, Carbon::parse($this->end));
            $c->{'Product'} = $investment->product->name;
            $c->{'Interest Rate'} = $investment->interest_rate;
            $c->{'Interest Accrued'} = $investment->repo->getNetInterestForInvestmentAtDate(Carbon::parse($this->end));
            $c->{'Tenor'} = $investment->maturity_date->diffInMonths($investment->invested_date);
            $c->{'Commission Rate'} = $investment->commission->rate;
            $c->{'FA'} = $fa->name;
            $c->{'Type'} = 'Investments';

            $investmentArray[trimText($fa->name, 23, false) . ' - INV'][] = $c;
        }

        return $investmentArray;
    }

    /**
     * @return array
     */
    private function getRealEstate()
    {
        $holdings = UnitHolding::whereHas('commission', function ($commission) {
            $commission->whereHas('recipient', function ($recipient) {
                $recipient->whereIn('email', $this->faEmails);
            });
        });

        if ($this->start) {
            $holdings = $holdings->reservedAfter($this->start);
        }
        if ($this->end) {
            $holdings = $holdings->reservedBefore($this->end)->activeAtDate($this->end);
        }

        $holdings = $holdings->get();

        $holdingArray = array();

        foreach ($holdings as $holding) {
            $c = new EmptyModel();
            $c->{'Client Code'} = $holding->client->client_code;
            $c->{'Name'} = ClientPresenter::presentJointFullNames($holding->client_id);
            $c->{'Email'} = $holding->client->contact->email;
            $c->{'Phone'} = $holding->client->contact->phone;
            $c->{'Project'} = $holding->project->name;
            $c->{'Unit'} = $holding->unit->number;
            $c->{'Size/Type'} = $holding->unit->size->name . ' ' . $holding->unit->type->name;
            $fa = $holding->commission->recipient;
            $c->{'Tranche'} = ($holding->tranche) ? $holding->tranche->name : '';
            $c->{'Price'} = $holding->price();
            $c->{'Reservation Date'} = DatePresenter::formatDate($holding->payments()->first()->date);
            $c->{'Signed Loo'} = ($holding->loo) ? DatePresenter::formatDate($holding->loo->date_signed) : '';
            $c->{'Signed SA'} = DatePresenter::formatDate($holding->salesAgreement->date_signed);
            $c->{'Paid'} = $holding->payments->sum('amount');
            $c->{'Commission Rate Type'} = ucfirst(@$holding->commission->rate->type);
            $c->{'Commission Rate Amount'} = $holding->commission->commission_rate;
            $c->{'FA'} = $fa->name;
            $c->{'Type'} = 'Real Estate';

            $holdingArray[trimText($fa->name, 23, false) . ' - RE'][] = $c;
        }

        return $holdingArray;
    }

    /**
     * @return array
     */
    private function getUnitFunds()
    {
        $purchases = UnitFundPurchase::whereHas('unitFundCommission', function ($q) {
            $q->whereHas('recipient', function ($q) {
                $q->whereIn('email', $this->faEmails);
            });
        });

        if ($this->start) {
            $purchases = $purchases->where('date', '>=', $this->start);
        }
        if ($this->end) {
            $purchases = $purchases->where('date', '<=', $this->end)->activeOnDate($this->end);
        }

        $purchases = $purchases->get();

        $purchasesArray = array();

        foreach ($purchases as $purchase) {
            $c = new EmptyModel();
            $c->{'Client Code'} = $purchase->client->client_code;
            $c->{'Name'} = ClientPresenter::presentJointFullNames($purchase->client_id);
            $c->{'Email'} = $purchase->client->contact->email;
            $c->{'Phone'} = $purchase->client->contact->phone;
            $c->{'Purchase Date'} = $purchase->date;
            $c->{'Units Purchased'} = $purchase->number;
            $c->{'Purchase Price'} = $purchase->price;
            $c->{'Purchase Value'} = (float)$purchase->number * $purchase->price;
            $c->{'Unit Fund'} = $purchase->price;
            $c->{'Value As At End'} = $purchase->calculate(Carbon::parse($this->end))->marketValue();
            $fa = $purchase->unitFundCommission->recipient;
            $c->{'Commission Rate'} = $purchase->unitFundCommission->rate;
            $c->{'FA'} = $fa->name;
            $c->{'Type'} = 'Unit Funds';

            $purchasesArray[trimText($fa->name, 23, false) . ' - UNIT'][] = $c;
        }

        return $purchasesArray;
    }

    /**
     * Send an email with the export
     *
     * @param User $user
     * @param $file_path
     */
    private function mailResult(User $user, $file_path)
    {
        $mailer = new GeneralMailer();

        $mailer->to($user->email);
        $mailer->bcc(config('system.administrators'));
        $mailer->from('support@cytonn.com');
        $mailer->subject('FAs Clients Export');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Here is the Investments and Real Estate Clients Export that you 
        requested for the following FAs: <br /><br /> ' .
            implode(', ', $this->fas->lists('name')) . PHP_EOL . ' <br /><br /> Start: ' .
            $this->start . PHP_EOL . ' <br /> End: ' . $this->end);
    }
}
