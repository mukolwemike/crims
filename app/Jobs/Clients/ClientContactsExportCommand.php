<?php

namespace App\Jobs\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\ContactPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClientContactsExportCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    use ContactDetailsTrait;

    protected $category;

    protected $user;

    protected $projectIds;
    protected $productIds;
    protected $active;

    /**
     * ClientContactsExportCommand constructor.
     *
     * @param $category
     * @param User $user
     */
    public function __construct($category, User $user, $projectIds = [], $productIds = [], $active = 'all')
    {
        $this->category = $category;
        $this->user = $user;
        $this->productIds = $productIds;
        $this->projectIds = $projectIds;
        $this->active = $active;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $category = $this->category;

        if ($category != 'investment' and $category != 'realestate' and $category != 'combined') {
            return;
        }

        $active = $this->active;

        $projects = $this->getProjects($this->projectIds);

        $excludeProjects = $this->getExcludedProjects($this->projectIds);

        $products = $this->getProducts($this->productIds);

        $excludeProducts = $this->getExcludedProducts($this->productIds);

        $clients = Client::with('contact')->where(
            function ($c) use ($category, $active, $products, $projects, $excludeProjects, $excludeProducts) {
                if ($category == 'investment') {
                    $this->handleInvestments($c, $products, $excludeProducts, $active);
                } elseif ($category == 'realestate') {
                    $this->handleRealEstate($c, $projects, $excludeProjects, $active);
                } elseif ($category == 'combined') {
                    $this->handleCombined($c, $products, $projects, $excludeProducts, $excludeProjects, $active);
                }
            }
        )->orderBy('client_code', 'ASC')->get();

        $outArray = array();

        $contactDetailsArray = array();

        foreach ($clients as $client) {
            if (!$this->validateClient($client, $category, $excludeProducts, $excludeProjects)) {
                $c = $this->getContactDetails($client);

                $contactDetailsArray = $this->getClientContactDetails($client, $contactDetailsArray);

                if ($category == 'investment' or $category = 'combined') {
                    $first_investment = $client->investments()->first();
                    $c->{'Invested Date'} = is_null($first_investment) ? null : $first_investment->invested_date;
                }

                $c->RealEstate = $client->unitHoldings->sum(
                    function ($holding) {
                        return $holding->price();
                    }
                );

                if ($category == 'realestate' or $category = 'combined') {
                    $c->{'Real Estate Payments'} = $this->getRealEstatePayments($client, $active);

                    foreach (Project::whereIn('id', $projects)->get() as $project) {
                        $c->{$project->name . ' Paid'} = $this->getRealEstatePayments($client, $active, $project);
                    }
                }

                $outArray[] = $c;
            }
        }

        $contactEmailsAndPhone = $this->extractEmailAndPhone($contactDetailsArray);

        $out = [
            'Client Details' => collect($outArray),
            'Contact Details' => $contactDetailsArray,
            'Contact Emails' => $contactEmailsAndPhone['Contact Emails'],
            'Contact Phones' => $contactEmailsAndPhone['Contact Phones']
        ];

        $fileName = ucfirst($category) . ' Clients contacts';

        ExcelWork::generateAndStoreMultiSheet($out, $fileName);

        Mailer::compose()
            ->to($this->user->email)
            ->bcc(config('system.administrators'))
            ->subject('Client contacts')
            ->text('Here is the export of clients that you requested.')
            ->excel([$fileName])
            ->send();

        $file_path = storage_path() . '/exports/' . $fileName . '.xlsx';

        \File::delete($file_path);
    }

    /*
     * Handle for investments
     */
    public function handleInvestments($c, $products, $excludeProducts, $active)
    {
        if (count($this->productIds) > 0) {
            foreach ($products as $product) {
                $this->filterInvestments($c, [$product], $active);
            }

//            $this->excludeInvestments($c, $excludeProducts);
        } else {
            $this->filterInvestments($c, $products, $active);
        }
    }

    /*
     * Handle for Real Estate
     */
    public function handleRealEstate($c, $projects, $excludeProjects, $active)
    {
        if (count($this->projectIds) > 0) {
            foreach ($projects as $project) {
                $this->filterHoldings($c, [$project], $active);
            }

//            $this->excludeHoldings($c, $excludeProjects);
        } else {
            $this->filterHoldings($c, $projects, $active);
        }
    }

    /*
     * Handle for combined
     */
    public function handleCombined($c, $products, $projects, $excludeProducts, $excludeProjects, $active)
    {
        if (count($this->productIds) > 0 || count($this->projectIds) > 0) {
            if (count($this->productIds) > 0) {
                foreach ($products as $product) {
                    $this->filterInvestments($c, [$product], $active);
                }

//                $this->excludeInvestments($c, $excludeProducts, $active);
            }

            if (count($this->projectIds) > 0) {
                foreach ($projects as $project) {
                    $this->filterHoldings($c, [$project], $active);
                }

//                $this->excludeHoldings($c, $excludeProjects);
            }
        } else {
            $this->filterInvestments($c, $products, $active)->orWhere(function ($q) use ($projects, $active) {
                $this->filterHoldings($q, $projects, $active);
            });
        }
    }

    /*
     * Validate the client
     */
    public function validateClient($client, $category, $excludeProducts, $excludeProjects)
    {
        if ($category == 'investment') {
            return $this->checkForExtraInvestments($client, $excludeProducts);
        } elseif ($category == 'realestate') {
            return $this->checkForExtraHoldings($client, $excludeProjects);
        } elseif ($category == 'combined') {
            return $this->checkForExtraInvestments($client, $excludeProducts) ||
                $this->checkForExtraHoldings($client, $excludeProjects);
        }
        return false;
    }

    /*
     * Filter the investments
     */
    public function filterInvestments($model, $product, $active)
    {
        return $model->whereHas(
            'investments',
            function ($inv) use ($product, $active) {
                $inv->whereIn('product_id', $product);

                $this->filterActive($inv, $active);
            }
        );
    }

    /*
     * Check if there are extra investment products
     */
    public function checkForExtraInvestments($client, $excludeProducts)
    {
        return ClientInvestment::where('client_id', $client->id)->active()
            ->whereIn('product_id', $excludeProducts)->exists();
    }

    /*
     * Exclude the investments
     */
    public function excludeInvestments($model, $products)
    {
        return $model->whereDoesntHave(
            'investments',
            function ($inv) use ($products) {
                return $inv->whereNotIn('product_id', $products);
            }
        );
    }

    /*
     * Filter the real estate holdings
     */
    public function filterHoldings($model, $project, $active)
    {
        return $model->whereHas(
            'unitHoldings',
            function ($holding) use ($project, $active) {
                $holding->inProjectIds($project);

                $this->filterActive($holding, $active);
            }
        );
    }

    /*
     * Check for extra unit holdings
     */
    public function checkForExtraHoldings($client, $excludeProjects)
    {
        return UnitHolding::where('client_id', $client->id)->active()
            ->inProjectIds($excludeProjects)->exists();
    }

    /*
     * Exclude the real estate holdings
     */
    public function excludeHoldings(&$model, $projects)
    {
        return $model->whereDoesntHave(
            'unitHoldings',
            function ($holding) use ($projects) {
                return $holding->inProjectIds($projects);
            }
        );
    }

    /*
     * Get the product ids
     */
    public function getProducts($productIds)
    {
        return count($productIds) == 0 ? Product::pluck('id')->toArray() :
            Product::whereIn('id', $productIds)->pluck('id')->toArray();
    }

    /*
     * Get the product ids
     */
    public function getExcludedProducts($productIds)
    {
        return count($productIds) == 0 ? [] : Product::whereNotIn('id', $productIds)->pluck('id')->toArray();
    }

    /*
     * Get the product ids
     */
    public function getProjects($projectIds)
    {
        return count($projectIds) == 0 ? Project::pluck('id')->toArray() :
            Project::whereIn('id', $projectIds)->pluck('id')->toArray();
    }

    /*
     * Get the excluded project ids
     */
    public function getExcludedProjects($projectIds)
    {
        return count($projectIds) == 0 ? [] : Project::whereNotIn('id', $projectIds)->pluck('id')->toArray();
    }

    private function getClientContactDetails(Client $client, $clientArray = [])
    {
        if ($client->clientType->name == 'corporate') {
            foreach ($client->contactPersons as $person) {
                $nameArray = explode(' ', $person->name);

                $firstname = count($nameArray) > 0 ? $nameArray[0] : '';

                $clientArray[] = [
                    'Client Code' => $client->client_code,
                    'Title' => '',
                    'Firstname' => $firstname,
                    'Fullname' => $person->name,
                    'Email' => $person->email,
                    'Phone' => trimPhoneNumber($person->phone)
                ];
            }

            return $clientArray;
        }

        if ($client->joint) {
            foreach ($client->jointHolders as $holder) {
                $jointDetail = $holder->details;

                $clientArray[] = [
                    'Client Code' => $client->client_code,
                    'Title' => $jointDetail->present()->getTitle,
                    'Firstname' => $jointDetail->firstname,
                    'Fullname' => $jointDetail->present()->fullname,
                    'Email' => $jointDetail->email,
                    'Phone' => $jointDetail->present()->getPhone
                ];
            }
        }

        $contact = $client->contact;

        $clientArray[] = [
            'Client Code' => $client->client_code,
            'Title' => ClientPresenter::presentTitle($contact->title_id),
            'Firstname' => $contact->firstname,
            'Fullname' => ContactPresenter::presentFullNameNoTitle($contact->id),
            'Email' => $contact->email,
            'Phone' => $contact->present()->getPhone
        ];

        return $clientArray;
    }

    private function extractEmailAndPhone($contactArray)
    {
        $emailArray = array();
        $phoneArray = array();

        foreach ($contactArray as $contact) {
            if (isNotEmptyOrNull($contact['Email'])) {
                $emailArray[] = [
                    'Firstname' => $contact['Firstname'],
                    'Email' => $contact['Email'],
                ];
            }

            if (isNotEmptyOrNull($contact['Phone'])) {
                $phoneArray[] = [
                    'Firstname' => $contact['Firstname'],
                    'Phone' => $contact['Phone'],
                ];
            }
        }

        return [
            'Contact Emails' => $emailArray,
            'Contact Phones' => $phoneArray
        ];
    }
}
