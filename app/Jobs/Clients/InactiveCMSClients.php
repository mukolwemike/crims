<?php

namespace App\Jobs\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class InactiveCMSClients
 * @package App\Jobs\Clients
 */
class InactiveCMSClients implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $user;

    protected $category;

    /**
     * Inactive Clients constructor.
     *
     * @param User $user
     */
    public function __construct(User $user, $category = null)
    {
        $this->user = $user;

        $this->category = ($category) ? $category : 'investments';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->category == 'combined') {
            $client = $this->filterCombinedClients();
        } elseif ($this->category == 'realestate') {
            $client = $this->filterRealEstateClients();
        } elseif ($this->category == 'unit_funds') {
            $client = $this->filterUnitFundClients();
        } else {
            $client = $this->filterInvestmentClients();
        }

        $date = Carbon::today();

        $clients = $client->get()
            ->filter(function ($client) {
                if ($this->category == 'combined') {
                    return $client->investments()->count() == $client->investments()->active(false)->count() &&
                        $client->unitHoldings()->count() == $client->unitHoldings()->active(false)->count();
                } elseif ($this->category == 'realestate') {
                    return $client->unitHoldings()->count() == $client->unitHoldings()->active(false)->count();
                } else {
                    return $client->investments()->count() == $client->investments()->active(false)->count();
                }
            })
            ->map(function ($client) {
                $c = new EmptyModel();

                $fa = $client->getLatestFA();

                $c->{'Client Code'} = $client->client_code;
                $c->{'Client Name'} = ClientPresenter::presentJointFullNames($client->id);
                $c->{'E-mail'} = $client->contact->email;
                $c->{'Telephone No.'} = $client->contact->phone;
                $c->{'FA'} = $fa ? $fa->name : '';

                if ($this->category == 'combined') {
                    $lastActiveInvestment = $client->investments()->orderBy('withdrawal_date', 'DESC')->first();
                    $lastInvestment = $client->investments()->latest()->first();

                    $lastActiveHolding = $client->unitHoldings()->orderBy('forfeit_date', 'DESC')->first();

                    $c->{'Cumulative Inflow'} = $client->repo->totalInvestmentInflows();
                    $c->{'Last Investment Amount'} = @$lastInvestment->amount;
                    $c->{'Last Investment Value Date.'} = @$lastInvestment->invested_date;
                    $c->{'Last Investment Withdrawal Date.'} = @$lastInvestment->withdrawal_date;
                    $c->{'Last Active Investment Amount'} = @$lastActiveInvestment->amount;
                    $c->{'Last Active Investment Value Date.'} = @$lastActiveInvestment->invested_date;
                    $c->{'Last Active Investment Withdrawal Date.'} = @$lastActiveInvestment->withdrawal_date;
                    $c->{'Last Active Investment Product.'} = @$lastActiveInvestment->product->name;
                    $c->{'Last Active UnitHolding Forfeit Date.'} = @$lastActiveHolding->forfeit_date;
                    $c->{'Last Active UnitHolding Project.'} = @$lastActiveHolding->unit->project->name;

                    if (Carbon::parse(@$lastActiveHolding->forfeit_date) >=
                        Carbon::parse(@$lastActiveInvestment->withdrawal_date)) {
                        $c->{'Year'} = Carbon::parse(@$lastActiveHolding->forfeit_date)->year;
                    } else {
                        $c->{'Year'} = Carbon::parse(@$lastActiveInvestment->withdrawal_date)->year;
                    }
                } elseif ($this->category == "realestate") {
                    $lastActiveHolding = $client->unitHoldings()->orderBy('forfeit_date', 'DESC')->first();

                    $c->{'Last Active UnitHolding Forfeit Date.'} = @$lastActiveHolding->forfeit_date;
                    $c->{'Last Active UnitHolding Project.'} = @$lastActiveHolding->unit->project->name;
                    $c->{'Year'} = Carbon::parse(@$lastActiveHolding->forfeit_date)->year;
                } else {
                    $lastActiveInvestment = $client->investments()->orderBy('withdrawal_date', 'DESC')->first();
                    $lastInvestment = $client->investments()->latest()->first();

                    $c->{'Cumulative Inflow'} = $client->repo->totalInvestmentInflows();
                    $c->{'Last Investment Amount'} = @$lastInvestment->amount;
                    $c->{'Last Investment Value Date.'} = @$lastInvestment->invested_date;
                    $c->{'Last Investment Withdrawal Date.'} = @$lastInvestment->withdrawal_date;
                    $c->{'Last Active Investment Amount'} = @$lastActiveInvestment->amount;
                    $c->{'Last Active Investment Value Date'} = @$lastActiveInvestment->invested_date;
                    $c->{'Last Active Investment Withdrawal Date.'} = @$lastActiveInvestment->withdrawal_date;
                    $c->{'Last Active Investment Product.'} = @$lastActiveInvestment->product->name;
                    $c->{'Year'} = Carbon::parse(@$lastActiveInvestment->withdrawal_date)->year;
                }

                return $c;
            })->groupBy('Year');

        if ($clients->count()) {
            $filename = 'Inactive Clients Summary';

            Excel::fromModel($filename, $clients)->store('xlsx');

            $this->mailResult($filename, $date, $this->user);
        }
    }

    /**
     * @param $model
     * @return mixed
     */
    private function filterInvestmentClients()
    {
        return Client::whereHas('investments', function ($q) {
            $q->active(false);
        });
    }

    /**
     * @param $model
     * @return mixed
     */
    private function filterRealEstateClients()
    {
        return Client::whereHas('unitHoldings', function ($q) {
            $q->active(false);
        });
    }

    /**
     * @param $model
     * @return mixed
     */
    private function filterUnitFundClients()
    {
        return Client::whereHas('unitFundPurchases', function ($q) {
            $q->active(false);
        });
    }

    /**
     * @param $model
     * @return mixed
     */
    private function filterCombinedClients()
    {
        return Client::whereHas('investments', function ($q) {
            $q->active(false);
        })->orWhereHas('unitHoldings', function ($q) {
            $q->active(false);
        })->orWhereHas('unitFundPurchases', function ($q) {
            $q->active(false);
        });
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $date
     * @param User $user
     */
    private function mailResult($filename, $date, User $user)
    {
        Mailer::compose()
            ->to($user->email)
            ->bcc(config('system.administrators'))
            ->subject('Inactive Clients Summary')
            ->text('Please find attached an export of all inactive clients as at ' . $date->toFormattedDateString())
            ->excel([$filename])
            ->send();

        $path = storage_path() . '/exports/' . $filename . '.xlsx';

        return \File::delete($path);
    }
}
