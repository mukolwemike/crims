<?php

namespace App\Jobs\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ClientProfilingExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    use ContactDetailsTrait;

    protected $user;
    protected $category;
    protected $active;
    protected $gender;
    protected $accountType;
    protected $minInvestment;
    protected $maxInvestment;
    protected $minAge;
    protected $maxAge;
    protected $reOption;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        $category,
        $active = null,
        $gender = null,
        $accountType = null,
        $minInvestment = null,
        $maxInvestment = null,
        $minAge = null,
        $maxAge = null,
        $reOption = 'payments'
    ) {
        $this->user = $user;
        $this->category = $category;
        $this->active = $active;
        $this->gender = $gender;
        $this->accountType = $accountType;
        $this->minInvestment = $minInvestment;
        $this->maxInvestment = $maxInvestment;
        $this->minAge = $minAge;
        $this->maxAge = $maxAge;
        $this->reOption = $reOption;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $clients = $this->getClients();

        $out = $clients->map(
            function (Client $client) {
                $c = $this->getContactDetails($client);

                if ($this->category == 'investment') {
                    $first_investment = $client->investments()->first();
                    $c->{'Invested Date'} = is_null($first_investment) ? null : $first_investment->invested_date;
                }

                $c->RealEstate = $client->unitHoldings->sum(
                    function ($holding) {
                        return $holding->price();
                    }
                );

                if ($this->category == 'realestate') {
                    $c->{'Real Estate Payments'} = $this->getRealEstatePayments($client, $this->active);

                    foreach (Project::all() as $project) {
                        $c->{$project->name. ' Paid'} = $this->getRealEstatePayments($client, $this->active, $project);
                    }
                }

                return $c;
            }
        );

        $fileName = ucfirst($this->category) . ' Clients Profiling';

        \Excel::create(
            $fileName,
            function ($excel) use ($out) {
                $excel->sheet(
                    'Contacts',
                    function ($sheet) use ($out) {
                        $sheet->fromModel($out);
                    }
                );
            }
        )->store('xlsx');

        Mailer::compose()
            ->to($this->user->email)
            ->bcc(config('system.administrators'))
            ->subject('Client Profiling Report')
            ->text('Here is the export of clients that you requested.')
            ->excel([$fileName])
            ->send();

        $file_path = storage_path().'/exports/'.$fileName.'.xlsx';

        \File::delete($file_path);
    }

    /*
     * Get the clients based on the variables
     */
    public function getClients()
    {
        $client = Client::with('contact');

        $this->filterGender($client);

        $this->filterAccountType($client);

        $this->filterAge($client);

        $this->filterCategory($client);

        $clients = $client->orderBy('client_code', 'ASC')->get();

        if ($this->category == 'investment') {
            return $this->filterInvestmentsAmount($clients);
        } elseif ($this->category == 'realestate') {
            return $this->filterRealEstateAmount($clients);
        } else {
            return [];
        }
    }

    /*
     * Filter by gender
     */
    public function filterGender($client)
    {
        if ($this->gender) {
            $client->whereHas('contact', function ($q) {
                $q->where('gender_id', $this->gender);
            });
        }
    }

    /*
     * Filter by account type
     */
    public function filterAccountType($client)
    {
        if ($this->accountType == 'joint') {
            $client->where('joint', 1);
        } elseif ($this->accountType == 'individual') {
            $client->where('client_type_id', 1);
        } elseif ($this->accountType == 'corporate') {
            $client->where('client_type_id', 2);
        }
    }

    /*
     * Filter by the age
     */
    public function filterAge($client)
    {
        if ($this->minAge) {
            $endYear = Carbon::now()->startOfYear()->subYears($this->minAge);

            $client->whereNotNull('dob')->where('dob', '<=', $endYear);
        }

        if ($this->maxAge) {
            $startYear = Carbon::now()->startOfYear()->subYears($this->maxAge);

            $client->whereNotNull('dob')->where('dob', '>=', $startYear);
        }
    }

    /*
     * Filter by the category
     */
    public function filterCategory($client)
    {
        if ($this->category == 'investments') {
            $client->whereHas('investments', function ($q) {
                $this->filterActive($q, $this->active);
            });
        } elseif ($this->category == 'realestate') {
            $client->whereHas('unitHoldings', function ($q) {
                $this->filterActive($q, $this->active);
            });
        }
    }

    /*
     * Filter by investments amount
     */
    public function filterInvestmentsAmount($clients)
    {
        if ($this->minInvestment || $this->maxInvestment) {
            return $clients->filter(function ($client) {
                $investments = $client->investments();

                $this->filterActive($investments, $this->active);

                $amount = $investments->sum('amount');

                return $this->validateAmount($amount);
            });
        }

        return $clients;
    }

    /*
    * Filter by real estate amount
    */
    public function filterRealEstateAmount($clients)
    {
        if ($this->minInvestment || $this->maxInvestment) {
            return $clients->filter(function ($client) {
                if ($this->reOption == 'payments') {
                    $amount = $this->getRealEstatePayments($client, $this->active);

                    return $this->validateAmount($amount);
                } elseif ($this->reOption == 'sales') {
                    $holdings = $client->unitHoldings();

                    $this->filterActive($holdings, $this->active);

                    $amount = $holdings->get()->sum(
                        function ($holding) {
                            return $holding->price();
                        }
                    );

                    return $this->validateAmount($amount);
                }
            });
        }

        return $clients;
    }

    /*
     * Validate the amount
     */
    public function validateAmount($amount)
    {
        $verdict = true;

        if ($this->minInvestment) {
            $verdict = $amount >= $this->minInvestment;
        }

        if ($this->maxInvestment) {
            $verdict = $amount <= $this->maxInvestment;
        }

        return $verdict;
    }
}
