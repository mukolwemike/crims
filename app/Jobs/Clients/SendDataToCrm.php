<?php

namespace App\Jobs\Clients;

use Cytonn\Clients\ClientManager;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendDataToCrm implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $type;
    public $recordId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $recordId)
    {
        $this->type = $type;
        $this->recordId = $recordId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $manager = new ClientManager();

        $manager->sendDataToCrm($this->type, $this->recordId);
    }
}
