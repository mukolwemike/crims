<?php

namespace App\Jobs\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\ComplianceReportMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\ComplianceReportExcelGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendComplianceReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * SendComplianceReport constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $clients_grouped_by_fund_manager = Client::all()->each(
            function ($client) {
                $client->compliant = $client->repo->compliant();
            }
        )->groupBy('fund_manager_id');

        $file_name = 'Compliance Report - ' . DatePresenter::formatDate(Carbon::today()->toDateString());
        (new ComplianceReportExcelGenerator())->excel($file_name, $clients_grouped_by_fund_manager)->store('xlsx');
        $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

        (new ComplianceReportMailer())->sendEmail($clients_grouped_by_fund_manager, $file_path);

        \File::delete($file_path);
    }
}
