<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Jobs\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Collection;

trait ContactDetailsTrait
{
    private $cisFunds;

    /*
     * Get the contact details
     */
    public function getContactDetails(Client $client)
    {
        $emails_array = implode(", ", $client->getContactEmailsArray());
        $phones_array = implode(", ", $client->getContactPhoneNumbersArray());

        $c = new EmptyModel();
        $c->Client_code = $client->client_code;
        $c->Name = ClientPresenter::presentJointFullNames($client->id);
        $c->First_name = $client->clientType->name == 'corporate'
            ? $c->First_name = $client->contact_person_fname : $client->contact->firstname;
        $c->Last_name = $client->contact->lastname;
        $c->Main_Phone = $client->individualPhoneNumber();
        $c->Phone = $phones_array;
        $c->Email = $emails_array;
        $c->Employee_Number = $client->employee_number;
        $c->Franchise = BooleanPresenter::presentYesNo($client->franchise);
        $c->Gender = is_null($client->contact->gender) ? null : $client->contact->gender->abbr;
        $c->Age = is_null($client->dob)
            ? null : Carbon::parse($client->dob)->diffInYears(Carbon::today(), true);
        $c->Country = is_null($client->country) ? null : $client->country->name;
        $c->Investments = $this->sumInvestments($client) + $this->sumUtf($client);
        $c->{'KRA PIN'} = $client->pin_no;
        $c = $this->getFa($client, $c);
        $c->Address = $client->present()->getAddress;
        $c->Physical_Address = $client->residence;

        // Employment Info
        $c->{'Employment Status'} = ($client->clientType->name == 'individual' and $client->employment)
            ? $client->employment->name : null;
        $c->{'Employment Other'} = $client->clientType->name == 'individual' ? $client->employment_other : null;
        $c->{'Business Sector'} = $client->clientType->name == 'individual' ? $client->business_sector : null;
        $c->{'Present Occupation'} = $client->clientType->name == 'individual' ? $client->present_occupation : null;
        $c->{'Employer Name'} = $client->clientType->name == 'individual' ? $client->employer_name : null;
        $c->{'Employer Address'} = $client->clientType->name == 'individual' ? $client->employer_address : null;
        $c->{'Telephone Office'} = $client->telephone_office;

        return $c;
    }

    private function sumInvestments(Client $client)
    {
        $investments = $client->investments()->active()->get();

        return $investments->sum(function (ClientInvestment $investment) {
            $amt = $investment->calculate()->total();

            return convert($amt, $investment->product->currency);
        });
    }

    /**
     * @return Collection
     */
    private function funds()
    {
        if ($this->cisFunds) {
            return $this->cisFunds;
        }

        return $this->cisFunds = UnitFund::active()->ofType('cis')->get();
    }

    private function sumUtf(Client $client)
    {
        return $this->funds()->sum(function (UnitFund $fund) use ($client) {
            $total = $client->calculateFund($fund, Carbon::today())->totalAmount();

            return convert($total, $fund->currency);
        });
    }

    /*
     * Get the clients fa
     */
    public function getFa($client, $c)
    {
        try {
            $fa = $client->getLatestFA();
        } catch (\Exception $e) {
            $fa = '';
        }

        $c->FA = is_null($fa) ? null : $fa->name;

        $c->{'FA Email'} = is_null($fa) ? null : $fa->email;

        return $c;
    }

    /*
     * Filter by the active
     */
    public function filterActive($model, $active)
    {
        if ($active == 'active') {
            return $model->active();
        } elseif ($active == 'inactive') {
            return $model->active(false);
        }
    }

    /*
     * Get the real estate payments
     */
    public function getRealEstatePayments($client, $active, $project = null)
    {
        return RealEstatePayment::whereHas('holding', function ($q) use ($client, $project, $active) {
            $q->active()->where('client_id', $client->id);

            if ($project) {
                $q->whereHas(
                    'unit',
                    function ($unit) use ($project) {
                        $unit->where('project_id', $project->id);
                    }
                );
            }
        })->sum('amount');
    }
}
