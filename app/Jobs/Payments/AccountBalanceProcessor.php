<?php

namespace App\Jobs\Payments;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\CustodialAccountBalanceTrail;
use App\Cytonn\PaymentSystem\PaymentTransactionTrait;
use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AccountBalanceProcessor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, LocksTransactions;
    use PaymentTransactionTrait;


    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

        $this->queue = config('queue.priority.payments');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $acc = $this->getCustodialAccount(
            $this->data->account_no,
            $this->data->bank_swift
        );

        if (!$acc) {
            return;
        }

        $balance = CustodialAccountBalanceTrail::where(['account_id' => $acc->id, 'date' => $this->data->date])
            ->first();

        if ($balance) {
            $balance->update([
                'amount' => $this->data->available_balance
            ]);

            return;
        }

        CustodialAccountBalanceTrail::create([
            'account_id' => $acc->id,
            'amount' => $this->data->available_balance,
            'date' => $this->data->date
        ]);
    }



    /**
     * Payment failed to process.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $data = json_encode($this->data);

        \Log::info(
            'CRIMS : Account Balance Processor Error'.PHP_EOL. 'Error: '.$exception->getMessage().PHP_EOL.
            'Data: '.$data
        );

        Mail::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : Account Balance Processing Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
                <p>Data: <br>
                    <pre>" . $data . " </pre>
                </p>")
            ->send();
    }
}
