<?php

namespace App\Jobs\Payments;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\PaymentSystem\PaymentTransactionProcessor;
use DB;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PaymentsProcessor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, LocksTransactions;

    protected $data;

    const RETRY_AFTER = 360;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

        $this->queue = config('queue.priority.payments');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        logRollbar("Processing payment : " . \GuzzleHttp\json_encode($this->data));

        $this->reset();

        if (in_array($this->data->transaction_id, ['NKI32EVR1D', 'NKH31C2ZCB'])) {
            return;
        }

        $key = 'lock_for_payments_id_' . $this->data->transaction_id;

        try {
            $this->executeWithinAtomicLock(function () use ($key) {
                $this->process();
            }, $key, 30 * 60);
        } catch (\Exception $e) {
            if ($this->transactionExists()) {
                $this->reportProcessed();
                return;
            }
            $this->release(static::RETRY_AFTER);

            throw $e;
        }

        $this->reportProcessed();

        logRollbar("Completed Processing payment : " . \GuzzleHttp\json_encode($this->data));
    }

    private function transactionExists()
    {
        $transExists = CustodialTransaction::where('bank_transaction_id', $this->data->transaction_id)
            ->exists();

        $suspExists = SuspenseTransaction::where('transaction_id', $this->data->transaction_id)
            ->exists();

        return $suspExists || $transExists;
    }

    /**
     * @throws \Throwable
     */
    protected function process()
    {
        \DB::transaction(function () {
            $app = new PaymentTransactionProcessor($this->data);

            $app->process();
        }, 2);
    }

    protected function reportProcessed()
    {
        $job = new NotifyPaymentSystem($this->data->transaction_id, Carbon::now()->toDateTimeString());

        try {
            $job->handle();
        } catch (\Exception $e) {
            dispatch($job);

            reportException($e);
        } catch (\Throwable $e) {
            dispatch($job);

            if ($e instanceof Exception) {
                reportException($e);
            }
        }
    }

    private function reset()
    {
        while (DB::transactionLevel() > 0) {
            DB::rollBack(0);
        }
    }

    /**
     * Payment failed to process.
     *
     * @param Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $data = json_encode($this->data);

        \Log::info(
            'CRIMS : Payment Processor Error' . PHP_EOL . 'Error: ' . $exception->getMessage() . PHP_EOL . 'Data: ' . $data
        );

        Mail::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : Payment Processing Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
                <p>Data: <br>
                    <pre>" . $data . " </pre>
                </p>")
            ->send();
    }
}
