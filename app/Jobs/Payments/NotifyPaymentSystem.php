<?php

namespace App\Jobs\Payments;

use CytonnSB\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyPaymentSystem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $timestamp;

    /**
     * Create a new job instance.
     *
     * @param $id
     * @param $timestamp
     */
    public function __construct($id, $timestamp)
    {
        $this->id = $id;
        $this->timestamp = $timestamp;
        $this->queue = config('queue.priority.payments');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->doHandle();
        } catch (\Throwable $e) {
            logRollbar("Job Notify retrying. ID" . $this->id ." Timestamp: ". $this->timestamp);

            dispatch(new static($this->id, $this->timestamp));
        }
    }

    public function doHandle()
    {
        $client = new Client();

        $client->event(
            'payment_processed_on_crims',
            [
                'transaction_id' => $this->id,
                'timestamp' => $this->timestamp
            ],
            ['cytonn_payments']
        );

        logRollbar("Job Notify successfully");
    }
}
