<?php

namespace App\Jobs\Payments;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Exceptions\CrimsException;
use App\Mail\Mail;
use Cytonn\Utilities\UnitTrustUtilityPaymentProcessor;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UtilityPaymentProcessor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, LocksTransactions;

    protected $data;

    protected $instruction;

    const RETRY_AFTER = 20;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

        $this->queue = config('queue.priority.payments');
    }

    /**
     * Execute the job.
     *
     * @throws CrimsException
     *
     * @return void
     */
    public function handle()
    {
        logRollbar("Processing Response for instruction : " . \GuzzleHttp\json_encode($this->data));

        $this->reset();

        $this->getInstruction();

        if (is_null($this->instruction)) {
            return;
        }

        if ($this->isProcessed()) {
            return;
        }

        $key = 'lock_for_utility_instructions_id_' . $this->instruction->id;

        try {
            $this->executeWithinAtomicLock(function () {
                $this->process();
            }, $key, 30 * 60);
        } catch (\Exception $e) {
            $this->release(static::RETRY_AFTER);

            throw $e;
        }

        logRollbar("Completed Processing Response for instruction " . $this->data->reference);
    }

    /**
     * @throws CrimsException
     */
    private function getInstruction()
    {
        $reference = $this->processReference($this->data->reference);

        if (is_null($reference->instruction_id)) {
            return;
        }

        $this->instruction = UtilityBillingInstructions::findOrFail($reference->instruction_id);

        if (is_null($this->instruction)) {
            throw new CrimsException("Could not locate utility instruction for the specified instruction id {$reference->instruction_id}");
        }
    }

    /**
     * @param $reference
     * @return object
     */
    private function processReference($reference)
    {
        $referenceArray = explode("-", $reference);

        return (object) [
            'payment_id' => $referenceArray[0],
            'instruction_id' => isset($referenceArray[1]) ? $referenceArray[1] : null
        ];
    }

    /**
     * @throws \Throwable
     */
    private function process()
    {
        \DB::transaction(function () {
            if ($this->instruction->unit_fund_id) {
                (new UnitTrustUtilityPaymentProcessor($this->instruction, $this->data))->process();
            } else {
                //Add processor for CMS
            }
        }, 2);
    }

    /**
     * @return bool
     */
    private function isProcessed()
    {
        return in_array($this->instruction->status->slug, ['completed', 'cancelled']);
    }

    private function reset()
    {
        while (DB::transactionLevel() > 0) {
            DB::rollBack(0);
        }
    }

    /**
     * Payment failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $data = json_encode($this->data);

        \Log::info(
            'CRIMS : Utility Payment Processor Error'.PHP_EOL. 'Error: '.$exception->getMessage().PHP_EOL.'Data: '.$data
        );

        Mail::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : Utility Payment Processing Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
                <p>Data: <br>
                    <pre>" . $data . " </pre>
                </p>")
            ->send();
    }
}
