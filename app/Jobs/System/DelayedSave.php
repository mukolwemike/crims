<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 18/12/2018
 * Time: 15:50
 */

namespace App\Jobs\System;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DelayedSave implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $model;

    protected $data;

    public function __construct($model, array $data = [])
    {
        $this->model = $model;

        $this->data = $data;

        $this->queue = config('queue.priority.low');
    }

    public function handle()
    {
        $model = $this->model;

        if (is_string($this->model)) {
            $model = app($model);
        }

        $model = $model->fill($this->data);

        $model->save();
    }
}
