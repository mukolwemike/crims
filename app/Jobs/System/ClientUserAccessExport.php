<?php

namespace App\Jobs\System;

use App\Cytonn\Models\ClientUser;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClientUserAccessExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $active;
    protected $account_type;
    protected $start;
    protected $end;

    public function __construct($start, $end, $active, $account_type)
    {
        $this->active = $active;
        $this->start = $start ? Carbon::parse($start)->toDateString() : Carbon::now()->subMonth(1)->toDateString();
        $this->end = Carbon::parse($end)->toDateString();
        $this->account_type = $account_type;
        $this->queue = config('queue.priority.high');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     */
    public function handle()
    {
        ini_set('max_execution_time', 1800);

        $users = $this->filterAccounts();

        $userArr = array();

        foreach ($users as $user) {
            $userArr[] = [
                'name' => $user->present()->fullName,
                'username' => $user->username,
                'email' => $user->email,
                'phone' => $user->phone_country_code . ' - ' . $user->phone,
                'account_type' => $user->ussd_account ? 'USSD DEFAULT' : '',
                'status' => $user->active ? 'Active' : 'Inactive',
                'joined' => DatePresenter::formatDate($user->created_at),
                'clients' => $this->getUserClients($user)
            ];
        }

        $fileName = "Client Users Access Report";

        $this->sendReports($userArr, $fileName);
    }

    private function sendReports($userArr, $fileName)
    {
        ExcelWork::generateSingleCustomExcelAndStore($userArr, $fileName, 'exports.system.client_user_access_export');

        Mailer::compose()
            ->to([\Auth::user()->email])
            ->from(['support@cytonn.com' => 'Cytonn CRIMS'])
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find the attached report generated')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);

    }

    private function getUserClients(ClientUser $user)
    {
        $clients = $user->clients;
        $clientsArr = [];

        foreach ($clients as $client) {
            $clientsArr[] = ClientPresenter::presentFullNames($client->id) . ' - ' . $client->client_code;
        }
        return $clientsArr;
    }

    private function filterAccounts()
    {
        $users = ClientUser::with('clients')
            ->whereBetween('created_at', array($this->start, $this->end));

        if ($this->active) {
            $users->where('active', $this->active);
        }

        if ($this->account_type) {
            $users->where('ussd_account', $this->account_type);
        }

        return $users->get();
    }

}
