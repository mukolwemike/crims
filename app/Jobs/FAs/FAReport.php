<?php

namespace App\Jobs\FAs;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FAReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $recipient;

    /**
     * FAReport constructor.
     *
     * @param CommissionRecepient $recipient
     * @param User                $user
     */
    public function __construct(CommissionRecepient $recipient, User $user)
    {
        $this->recipient = $recipient;
        $this->u = $user;
    }



    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fa = $this->recipient;

        $commissions = $fa->commissions;

        $realestate_commissions = $fa->realEstateCommissions;

        $fileName = 'FA Report - ' . $fa->name;

        \Excel::create(
            $fileName,
            function ($excel) use ($fa, $commissions, $realestate_commissions) {
                $this->generateCommissionsSheet($fa, $commissions, $excel);

                $this->generateRealEstateCommissionsSheet($fa, $realestate_commissions, $excel);
            }
        )->store('xlsx');

        $path = storage_path().'/exports/'.$fileName.'.xlsx';

        $this->mailResult($path, $this->user);

        \File::delete($path);
    }

    /**
     * @param $fa
     * @param $commissions
     * @param $excel
     */
    private function generateCommissionsSheet($fa, $commissions, $excel)
    {
        $excel->sheet(
            'Investment Clients',
            function ($sheet) use ($fa, $commissions) {
                $date = new Carbon();

                $c = $commissions->map(
                    function (Commission $commission) use ($date) {
                        $commission->client_code = $commission->investment->client->client_code;
                        $commission->name = ClientPresenter::presentJointFullNames($commission->investment->client_id);
                        $commission->email = $commission->investment->client->contact->email;
                        $commission->phone = $commission->investment->client->contact->phone;
                        $commission->fa = $commission->recipient->name;
                        $commission->investment_type = $commission->investment->type->name;
                        $commission->product = $commission->investment->product->name;
                        $commission->principal = AmountPresenter::currency($commission->investment->amount);
                        $commission->interest_rate = $commission->investment->interest_rate;
                        $commission->invested_date = DatePresenter::formatDate($commission->investment->invested_date);
                        $commission->maturity_date = DatePresenter::formatDate($commission->investment->maturity_date);

                        return $commission;
                    }
                )->sortBy('client_code');

                $sheet->loadView('exports.inv_fa_report', ['commissions'=>$c, 'date'=>$date]);
            }
        );
    }

    /**
     * @param $fa
     * @param $realestate_commissions
     * @param $excel
     */
    private function generateRealEstateCommissionsSheet($fa, $realestate_commissions, $excel)
    {
        $excel->sheet(
            'Real Estate Clients',
            function ($sheet) use ($fa, $realestate_commissions) {
                $date = new Carbon();

                $c = $realestate_commissions->map(
                    function (RealestateCommission $r_commission) use ($date) {
                        $r_commission->client_code = $r_commission->holding->client->client_code;
                        $r_commission->name = ClientPresenter::presentJointFullNames($r_commission->holding->client_id);
                        $r_commission->email = $r_commission->holding->client->contact->email;
                        $r_commission->phone = $r_commission->holding->client->contact->phone;
                        $r_commission->fa = $r_commission->recipient->name;
                        $r_commission->project = $r_commission->holding->project->name;
                        $r_commission->unit =
                            $r_commission->holding->unit->size->name . ' ' . $r_commission->holding->unit->type->name;
                        $r_commission->price = AmountPresenter::currency($r_commission->holding->price());

                        return $r_commission;
                    }
                )->sortBy('client_code');

                $sheet->loadView('exports.re_fa_report', ['realestate_commissions'=>$c, 'date'=>$date]);
            }
        );
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     */
    private function mailResult($file_path, User $user)
    {
        Mailer::compose()
            ->to($user->email)
            ->from('support@cytonn.com')
            ->bcc([ 'mchaka@cytonn.com' ])
            ->subject('Investments Inactive Clients export in excel')
            ->text('Please find the attached report of inactive investment clients')
            ->excel([$file_path])
            ->send();
    }
}
