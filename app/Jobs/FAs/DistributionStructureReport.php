<?php

namespace App\Jobs\FAs;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DistributionStructureReport implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * DistributionStructureReport constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $today = Carbon::today();
        $file_name = 'Distribution Structure as at ' . $today->copy()->toFormattedDateString();

        $ums = CommissionRecepient::whereHas(
            'rank',
            function ($rank) {
                $rank->where('level', 3);
            }
        )
            ->get()
            ->each(
                function ($um) {
                    $um->dm = is_null($um->reportsTo) ?: $um->reportsTo->name;
                }
            );

        \Excel::create(
            $file_name,
            function ($excel) use ($ums, $today) {
                foreach ($ums->groupBy('dm') as $dm => $ums_arr) {
                    $excel->sheet(
                        $this->clean('DM - ' . $dm),
                        function ($sheet) use ($ums_arr, $today) {
                            $sheet->loadView(
                                'exports.distribution_structure.dms_and_ums',
                                ['ums_arr' => $ums_arr, 'today' => $today->copy()]
                            );
                        }
                    );
                }

                foreach ($ums as $um) {
                    $excel->sheet(
                        $this->clean('UM - ' . $um->name),
                        function ($sheet) use ($um, $today) {
                            $sfas = CommissionRecepient::whereHas(
                                'rank',
                                function ($rank) {
                                    // $rank->where('level', 2);
                                }
                            )
                                ->where('reports_to', $um->id)
                                ->get()
                                ->each(
                                    function ($sfa) {
                                        $sfa->fas = CommissionRecepient::whereHas(
                                            'rank',
                                            function ($rank) {
                                                // $rank->where('level', 1);
                                            }
                                        )
                                            ->where('reports_to', $sfa->id)->get();
                                        $sfa->um = $sfa->reportsTo->name;
                                    }
                                );
                            $sheet->loadView(
                                'exports.distribution_structure.ums_sfas_and_fas',
                                ['um' => $um, 'sfas' => $sfas, 'today' => $today->copy()]
                            );
                        }
                    );
                }
            }
        )->store('xlsx');

        $this->sendExcel(
            'Distribution Structure - ' . $today->copy()->toFormattedDateString(),
            "Please find attached an excel workbook containing the distribution department structure as at " .
            $today->toFormattedDateString(),
            $file_name,
            [$this->user->email]
        );

        $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';
        \File::delete($file_path);
    }

    private function clean($name)
    {
        return Excel::cleanSheetName($name);
    }
}
