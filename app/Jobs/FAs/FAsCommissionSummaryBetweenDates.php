<?php

namespace App\Jobs\FAs;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Reporting\FAsCommissionSummaryBetweenDatesExcelGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FAsCommissionSummaryBetweenDates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $start;

    protected $end;

    public function __construct(Carbon $start, Carbon $end, User $user)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = $this->start;
        $end = $this->end;

        $file_name = 'FA Commission Summary - ' .
            $start->copy()->toFormattedDateString() . ' to ' . $end->copy()->toFormattedDateString();

        (new FAsCommissionSummaryBetweenDatesExcelGenerator())
            ->excel($start->copy(), $end->copy(), $file_name)->store('xlsx');

        $path = storage_path() . '/exports/' . $file_name . '.xlsx';

        $this->mailResult($path, $start, $end, $this->user);

        \File::delete($path);
    }

    /**
     * @param $file_path
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     */
    private function mailResult($file_path, Carbon $start, Carbon $end, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->cc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('FA Commission Summary - ' . $start->copy()->toFormattedDateString() .
            ' to ' . $end->copy()->toFormattedDateString());
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached report of FA commission summary from ' .
            $start->copy()->toFormattedDateString() . ' to ' . $end->copy()->toFormattedDateString());
    }
}
