<?php

namespace App\Jobs\FAs;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Projections\CommissionProjection;
use Cytonn\Mailers\GeneralMailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FaCommissionProjectionReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $fas;
    protected $start;
    protected $end;

    public function __construct(User $user, $fas, Carbon $start, Carbon $end)
    {
        $this->fas = $fas;
        $this->user = $user;
        $this->start = $start;
        $this->end = $end;
    }

    public function handle()
    {
        $fas = $this->fas;

        $start = Carbon::parse($this->start)->startOfMonth();

        $end = Carbon::parse($this->end)->endOfMonth();

        $file_name = 'Fa projections';

        \Excel::create(
            $file_name,
            function ($excel) use ($fas, $start, $end) {
                foreach ($fas as $fa) {
                    $excel->sheet(
                        $fa->name,
                        function ($sheet) use ($fa, $start, $end) {
                            $projection = new CommissionProjection();

                            $model = $projection->getProjections($fa, $start, $end);

                            $sheet->fromModel($model);
                        }
                    );
                }
            }
        )->store('xlsx');

        $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';
        $this->mailResult($this->user, $file_path);

        \File::delete($file_path);
    }

    private function mailResult(User $user, $file_path)
    {
        $mailer = new GeneralMailer();

        $mailer->to($user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('FAs Commission Projections Export');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Here is the commission projections for the FAs you requested: ' .
            implode(', ', $this->fas->lists('name')) . PHP_EOL . ' start: ' . $this->start . PHP_EOL . ' end: ' .
            $this->end);
    }
}
