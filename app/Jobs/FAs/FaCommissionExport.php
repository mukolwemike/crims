<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/5/18
 * Time: 1:10 PM
 */

namespace App\Jobs\FAs;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Http\CrimsClient\Controllers\FA\CommissionSummaryController;
use Carbon\Carbon;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FaCommissionExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recipient;

    protected $date;

    protected $currency;

    public function __construct(CommissionRecepient $recipient, Currency $currency, Carbon $date)
    {
        $this->recipient = $recipient;

        $this->currency = $currency;

        $this->date = $date;
    }

    public function handle()
    {
        $input['currency'] = $this->currency->code;

        $input['date'] = $this->date;

        $summary = (new CommissionSummaryController())->commissionSummary($this->recipient->id, $input);

        Mailer::compose()
            ->to($this->recipient->email)
            ->from('support@cytonn.com')
            ->subject('Commission Summary Export')
            ->view('emails.fa.commission', [
                'summary' => $summary,
                'fa' => $this->recipient
            ])
            ->send();
    }
}
