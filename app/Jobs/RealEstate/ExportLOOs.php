<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExportLOOs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    public $loos;

    /**
     * Create a new job instance.
     * ExportLOOs constructor.
     *
     * @param $user
     */
    public function __construct($loos, $user)
    {
        $this->user = $user;
        $this->loos = $loos;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $loos = $this->loos->map(
            function (RealestateLetterOfOffer $loo) {
                $e = new EmptyModel();

                $e->{'ID'}              =   $loo->id;
                $e->{'Client Code'}     =   $loo->holding->client->client_code;
                $e->{'Client Name'}     =   ClientPresenter::presentFullNames($loo->holding->client_id);
                $e->{'Project'}         =   $loo->holding->project->name;
                $e->{'Advocate'}        =   is_null($loo->advocate) ? 'Not Set': $loo->advocate->name;
                $e->{'Vendor'}          =   $loo->holding->project->vendor;
                $e->{'Unit Number'}     =   $loo->holding->unit->number;
                $e->{'Unit Size'}       =   $loo->holding->unit->size->name . ' ' . $loo->holding->unit->type->name;
                $e->{'Sent'}            =   BooleanPresenter::presentYesNo($loo->sent_on);
                $e->{'Rejected'}        =
                    BooleanPresenter::presentYesNo(! $loo->pm_approved_on and $loo->rejects->count() > 0);
                $e->{'PM Approved On'}  =   $loo->pm_approved_on;
                $e->{'PM Approver'}     =
                    is_null($loo->pm_approval_id) ? null :UserPresenter::presentFullNames($loo->pm_approval_id);

                return $e;
            }
        )->groupBy('Project');
        ;

        $filename = 'Real Estate Letters of Offer';

        Excel::fromModel($filename, $loos)->store('xlsx');

        $this->mailResult(storage_path('exports/'.$filename.'.xlsx'), $user);
    }

    /**
     * @param $file_path
     * @param User      $user
     */
    private function mailResult($file_path, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->bcc(config('system.emails.super_admins'));
        $mailer->from('support@cytonn.com');
        $mailer->subject('Real Estate Letters of Offer');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached the requested export of letters of offer');

        \File::delete($file_path);
    }
}
