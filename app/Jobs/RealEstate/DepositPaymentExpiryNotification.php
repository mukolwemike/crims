<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DepositPaymentExpiryNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * DepositPaymentExpiryNotification constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        $date_60_days_ago = Carbon::today()->subDays(60);

        $this->sendDepositPaymentExpiryNotification($date_60_days_ago->copy(), $this->user);
    }

    /**
     * @param Carbon $date_60_days_ago
     * @param User $user
     */
    private function sendDepositPaymentExpiryNotification(Carbon $date_60_days_ago, User $user)
    {
        $holdings = UnitHolding::active()->where('created_at', '<=', $date_60_days_ago->copy()->endOfDay())
            ->get()
            ->filter(
                function ($holding) {
                    $deposit = 0.1 * $holding->price();
                    return $holding->payments()->sum('amount') < $deposit;
                }
            );


        $this->exportToExcel($holdings, $date_60_days_ago->copy(), $user);
    }

    /**
     * @param $holdings
     * @param Carbon $date_60_days_ago
     * @param User $user
     */
    private function exportToExcel($holdings, Carbon $date_60_days_ago, User $user)
    {
        if ($holdings->count()) {
            $holdings = $holdings->map(
                function ($holding) {
                    $e = new EmptyModel();

                    $payment_type = RealEstatePaymentType::where('slug', 'deposit')->first();

                    $e->{'Client Name'} = ClientPresenter::presentFullNames($holding->client_id);
                    $e->{'Client Code'} = is_null($holding->client) ? null : $holding->client->client_code;
                    $e->{'Project'} = $holding->project->name;
                    $e->{'Unit'} = $holding->unit->number;
                    $e->{'Size/Type'} = $holding->unit->size->name . ' ' . $holding->unit->type->name;
                    $e->{'Tranche'} = $holding->tranche->name;
                    $e->{'Reservation Date'} = $holding->created_at;
                    $e->{'Expiry Date'} = (new Carbon($holding->created_at))->addDays(30);
                    $e->{'Payment Plan'} = $holding->paymentPlan->name;
                    $e->{'Unit Price'} = $holding->price();
                    $e->{'Scheduled Deposit'} = $holding->price() * 0.1;
                    $e->{'Paid Deposit'} = $holding->payments()
                        ->where('payment_type_id', $payment_type->id)
                        ->sum('amount');
                    $e->{'FA'} = $holding->commission->recipient->name;

                    return $e;
                }
            )->sortBy('Project');

            $file_name = 'Deposit Payment Expiry Notifications (60+ Days) - ' .
                $date_60_days_ago->copy()->toFormattedDateString();

            ExcelWork::generateAndStoreMultiSheet(['Expiry Notifications' => $holdings], $file_name);

            $this->mailResult(storage_path('exports/' . $file_name . '.xlsx'), Carbon::today(), $user);
        }
    }

    /**
     * @param $file_path
     * @param Carbon $today
     * @param User $user
     */
    private function mailResult($file_path, Carbon $today, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->bcc(config('system.administrators'));
        $mailer->from('support@cytonn.com');
        $mailer->subject('Deposit Payment Expiry Notifications (60+ days) - ' .
            $today->copy()->toFormattedDateString());
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached the Deposit payment expiry 
        notifications (60+ days) report - ' . $today->toFormattedDateString());

        \File::delete($file_path);
    }
}
