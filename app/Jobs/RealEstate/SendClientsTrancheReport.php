<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\ClientsTrancheReportMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\ClientsTrancheReportExcelGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendClientsTrancheReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $project;

    /**
     * SendClientsTrancheReport constructor.
     *
     * @param Project $project
     * @param User $user
     */
    public function __construct(Project $project, User $user)
    {
        $this->project = $project;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $holdings_grouped_by_tranche = UnitHolding::where('active', 1)
            ->has('tranche')
            ->get()
            ->filter(
                function ($holding) {
                    return $holding->project->id === $this->project->id;
                }
            )
            ->groupBy('tranche_id');

        if (count($holdings_grouped_by_tranche)) {
            $file_paths = [];
            foreach ($holdings_grouped_by_tranche as $tranche => $holdings_arr) {
                $tranche = RealEstateUnitTranche::findOrFail($tranche);
                $file_name = $tranche->name . ' - ' . DatePresenter::formatDate(Carbon::today()->toDateString());
                (new ClientsTrancheReportExcelGenerator())
                    ->excel($tranche, $holdings_arr, $file_name)
                    ->store('xlsx');
                $file_paths[] = storage_path() . '/exports/' . $file_name . '.xlsx';
            }

            (new ClientsTrancheReportMailer())->sendEmail($file_paths, $this->user);

            foreach ($file_paths as $path) {
                \File::delete($path);
            }
        }
    }
}
