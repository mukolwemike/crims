<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\ExcelMailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RealEstateUnitReservationSummary implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $start;

    protected $end;

    /**
     * RealEstateUnitReservationSummary constructor.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     */
    public function __construct(Carbon $start, Carbon $end, User $user)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = $this->start;
        $end = $this->end;

        $purchases = $this->purchases($start, $end);
        $forfeitures = $this->forfeiture($start, $end);

        $fname = 'RE Reservations Summary';

        ExcelWork::generateAndStoreMultiSheet(['Reservations' => $purchases, 'Forfeitures' => $forfeitures], $fname);

//        Excel::fromModel($fname, ['Reservations' => $purchases, 'Forfeitures' => $forfeitures])->store('xlsx');

        $users = [$this->user->email];
        //        $users = User::whereIn('id', [$this->option('--user')])->pluck('email')->all();

        $this->sendExcel(
            $fname,
            'Please find attached for ' . $start->toFormattedDateString(). ' to ' .$end->toFormattedDateString(),
            $fname,
            $users
        );
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    private function purchases($start, $end)
    {
        $holding = UnitHolding::whereHas(
            'payments',
            function ($p) use ($start, $end) {
                $p->where('date', '>=', $start)->where('date', '<=', $end);
            }
        )->get()
            ->each(
                function (UnitHolding $holding) {
                    $firstPayment = $holding->payments()->oldest('date')->first();

                    $holding->firstPayment = $firstPayment;
                }
            )
            ->filter(
                function (UnitHolding $holding) use ($start, $end) {
                    $p = $holding->firstPayment;

                    return $p->date->copy()->gte($start) && $p->date->copy()->lte($end);
                }
            );

        return $this->map($holding);
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    private function forfeiture($start, $end)
    {
        $forfeitures = UnitHolding::has('payments')
            ->where('forfeit_date', '>=', $start)
            ->where('forfeit_date', '<=', $end)
            ->get()
            ->each(
                function (UnitHolding $holding) {
                    $firstPayment = $holding->payments()->oldest('date')->first();

                    $holding->firstPayment = $firstPayment;
                }
            );

        return $this->map($forfeitures);
    }

    /**
     * @param $coll
     * @return mixed
     */
    private function map($coll)
    {
        return $coll->map(
            function (UnitHolding $holding) {
                $e = new EmptyModel();

                $recipient = $holding->commission->recipient;

                $comms = $this->getCommissionPayable($holding);

                $e->fill(
                    [
                        'Project' => $holding->project->name,
                        'Unit' => $holding->unit->number,
                        'Client Name' => ($holding->client) ? $holding->client->name() : '',
                        'Topology' => $holding->unit->size->name . ' ' . $holding->unit->type->name,
                        'Reservation Date' => $holding->firstPayment->date->toDateString(),
                        'LOO Signed Date' => !$holding->loo ? null : $holding->loo->present()->dateSigned,
                        'Sales Agreement Signed Date' => !$holding->salesAgreement
                            ? null : $holding->salesAgreement->present()->dateSigned,
                        'Forfeiture Date' => $holding->forfeit_date,
                        'Price' => (float)$holding->price(),
                        'Amount Paid' => $holding->payments()->sum('amount'),
                        'FA' => $recipient->name,
                        'Type' => $recipient->type->name,
                        'Total Comms' => $comms['total'],
                        'Payable to FA' => $comms['fa'],
                        'Payable to CIM' => $comms['cim']
                    ]
                );

                return $e;
            }
        )->sortBy('Project');
    }

    /**
     * @param UnitHolding $holding
     * @return array
     */
    private function getCommissionPayable(UnitHolding $holding)
    {
        $highestRate = $holding->project->commissionRates()->orderBy('amount', 'DESC')->first();

        $type = $highestRate->type;
        switch ($type) {
            case 'percentage':
                $val = $holding->price();
                $totalComms = $highestRate->amount * $val / 100;
                $fa = $holding->commission->rate->amount * $val / 100;

                return ['total' => $totalComms, 'fa' => $fa, 'cim' => $totalComms - $fa];
                break;
            case 'amount':
                $totalComms = $highestRate->amount;
                $fa = $holding->commission->rate->amount;

                return ['total' => $totalComms, 'fa' => $fa, 'cim' => $totalComms - $fa];
                break;
            default:
                throw new \InvalidArgumentException("Commission scheme $type is not supported");
        }
    }
}
