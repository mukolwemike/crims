<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RePaymentsSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ExcelMailer;

    /**
     * @var Carbon
     */
    private $start;

    /**
     * @var Carbon
     */
    private $end;
    /**
     * @var array
     */
    private $projects;
    /**
     * @var User
     */
    private $recipient;

    /**
     * Create a new job instance.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param $recipient
     * @param array $projects
     */
    public function __construct(Carbon $start, Carbon $end, array $recipient = [], array $projects = [])
    {
        $this->start = $start;
        $this->end = $end;
        $this->projects = $projects;
        $this->recipient = $recipient;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $projects = Project::whereIn('id', $this->projects)->get();

        if (!count($projects)) {
            $projects = Project::all();
        }

        $summary = $projects->map(
            function (Project $project) {
                $amount = RealEstatePayment::columnBetween('date', $this->start, $this->end)
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project) {
                            $holding->whereHas(
                                'unit',
                                function ($unit) use ($project) {
                                    $unit->where('project_id', $project->id);
                                }
                            );
                        }
                    )->sum('amount');

                return (new EmptyModel())->fill(
                    [
                        'Name' => $project->name,
                        'Amount' => $amount
                    ]
                );
            }
        );

        $paymentsGroupedByProject = RealEstatePayment::columnBetween('date', $this->start, $this->end)
            ->whereHas(
                'holding',
                function ($holding) use ($projects) {
                    $holding->whereHas(
                        'unit',
                        function ($unit) use ($projects) {
                            $unit->whereIn('project_id', $projects->pluck('id'));
                        }
                    );
                }
            )
            ->get()
            ->map(
                function (RealEstatePayment $payment) {
                    $holding = $payment->holding;
                    $unit = $holding->unit;
                    $client = $holding->client;

                    return (new EmptyModel())->fill(
                        [
                            'Client Name' => ClientPresenter::presentFullNames($client->id),
                            'Client Code' => $client->client_code,
                            'Project' => $unit->project->name,
                            'Unit Number' => $unit->number,
                            'Unit Size' => $unit->size->name,
                            'Unit Type' => $unit->type->name,
                            'Date' => $payment->date,
                            'Amount' => $payment->amount,
                            'Payment Type' => $payment->type->name,
                            'Description' => $payment->description,
                            'Approved By' => is_null($payment->approval)
                                ? null : UserPresenter::presentFullNames($payment->approval->approver->id),
                        ]
                    );
                }
            )->groupBy('Project');

        $output = [
            'Project Summary' => $summary,
        ];

        foreach ($paymentsGroupedByProject as $project => $paymentDetails) {
            $output[$project] = $paymentDetails;
        }

        $fname = 'Project payments';

        $rec = User::whereIn('id', $this->recipient)->pluck('email')->all();

        Excel::fromModel($fname, $output)->store('xlsx');

        $this->sendExcel(
            'Real Estate Payments Summary',
            'Please find attached the requested Real Estate Payments Summary report for the projects: ' .
            implode(", ", $projects->pluck('name')->all()) . ' between ' .
            $this->start->toFormattedDateString() . ' and ' . $this->end->toFormattedDateString(),
            $fname,
            $rec
        );
    }
}
