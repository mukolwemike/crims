<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\Console\Input\InputArgument;

class ForfeitedUnits implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * ForfeitedUnits constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $forfeited_units = UnitHolding::active(false)
            ->get()
            ->map(
                function ($holding) {
                    $h = new EmptyModel();

                    $occupancy = $holding->where('unit_id', $holding->unit_id)->where('active', 1);
                    $currently_occupied = $occupancy->count() > 0;

                    $h->Unit = $holding->unit->number;
                    $h->{'Size/Type'} = $holding->unit->size->name . ' ' . $holding->unit->type->name;
                    $h->Project = $holding->project->name;
                    $h->{'Previous Owner'} = ClientPresenter::presentFullNames($holding->client_id);
                    $h->{'Client Code'} = is_null($holding->client) ? null : $holding->client->client_code;
                    $h->{'Tranche'} = @$holding->tranche->name;
                    $h->{'Price'} = $holding->price();
                    $h->Refund = AmountPresenter::currency($holding->refundedAmount());
                    $h->{'Forfeiture Date'} = $holding->forfeit_date;
                    $h->{'Currently Occupied'} = BooleanPresenter::presentYesNo($currently_occupied);
                    if ($currently_occupied) {
                        $h->{'Current Owner'} = ClientPresenter::presentFullNames($occupancy->first()->client_id);
                        $h->{'Client Code'} = $occupancy->first()->client->client_code;
                    }
                    $h->Reason = @$holding->forfeit_reason;
                    return $h;
                }
            )->groupBy('Project');
        ;

        $filename = 'Real Estate Forfeited Units';

        Excel::fromModel($filename, $forfeited_units)->store('xlsx');

        $this->mailResult(storage_path('exports/' . $filename . '.xlsx'), $user);
    }

    /**
     * @param $file_path
     * @param User $user
     */
    private function mailResult($file_path, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->bcc(config('system.emails.super_admins'));
        $mailer->from('support@cytonn.com');
        $mailer->subject('Real Estate Forfeited Units');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached the requested export of forfeited units');

        \File::delete($file_path);
    }

    protected function getArguments()
    {
        return [
            ['user_id', InputArgument::OPTIONAL, 'get user id']
        ];
    }
}
