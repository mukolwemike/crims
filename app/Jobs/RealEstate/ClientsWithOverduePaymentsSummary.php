<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClientsWithOverduePaymentsSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $date;
    protected $project;

    /**
     * OverduePaymentsReport constructor.
     * @param User $user
     */
    public function __construct(Carbon $date, User $user, Project $project = null)
    {
        $this->user = $user;
        $this->date = $date;
        $this->project = $project;
    }

    /**
     * @return bool
     */
    public function handle()
    {
        $holdings = $this->getReport();

        $fileName = 'Clients With Overdue Payments - '. $this->date->toDateString();

        ExcelWork::generateAndStoreMultiSheet($holdings, $fileName);

        Mailer::compose()
            ->to($this->user->email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the clients with overdue payments summary as at '
                . $this->date->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @return array
     */
    private function getReport()
    {
        $holdings = UnitHolding::active()->whereHas('paymentSchedules', function ($q) {
            $q->where('date', '<', $this->date)->where('paid', '!=', 1);
        });

        if ($this->project) {
            $holdings->inProjectIds([$this->project->id]);
        }

        $holdings = $holdings->get();

        $holdingArray = array();

        $today = Carbon::now();

        foreach ($holdings as $holding) {
            $holdingData = $this->getHoldingCalculations($holding);

            if (!is_null($holdingData)) {
                $client = $holding->client;

                $nextInvestment = $this->nextMaturingInvestment($client, $today);

                $holdingArray[$holding->project->name][] = [
                    'Client Name' => ClientPresenter::presentFullNames($holding->client_id),
                    'Client Code' => $holding->client->client_code,
                    'FA' => is_null($fa = $holding->client->getLatestFA('realestate')) ? null : $fa->name,
                    'Project' => $holding->project->name,
                    'Unit Number' => $holding->unit->number,
                    'Size / Type' => $holding->unit->size->name . ' ' . $holding->unit->type->name,
                    'Payment Plan' => $holding->paymentPlan ? $holding->paymentPlan->name : '',
                    'Tranche' => $holding->tranche ? $holding->tranche->name : '',
                    'Price' => $holdingData->price,
                    'Paid' => $holdingData->paid,
                    'Pending' => $holdingData->pending,
                    '% Paid' => $holdingData->percentage_paid,
                    'Overdue' => $holdingData->overdue,
                    'Aging' => $holdingData->aging,
                    'Total Investments Value' => $client->repo->getTodayTotalInvestmentsValueForAllProducts($today),
                    'Next Maturing Investment' => $nextInvestment ?
                        convert(
                            $nextInvestment->repo->getTotalValueOfInvestmentAtDate(
                                $today
                            ),
                            $nextInvestment->product->currency,
                            $today
                        ) : '',
                    'Next Maturity Date' => $nextInvestment ?
                        Carbon::parse($nextInvestment->maturity_date)->toDateString() :
                        '',
                ];
            }
        }

        return $holdingArray;
    }

    private function nextMaturingInvestment(Client $client, Carbon $today)
    {
        return ClientInvestment::where('client_id', $client->id)->active()
            ->where('maturity_date', '>=', $today)->orderBy('maturity_date')->first();
    }

    /**
     * @param UnitHolding $holding
     * @return object|null
     */
    private function getHoldingCalculations(UnitHolding $holding)
    {
        $overdue = $holding->paymentSchedules()
            ->inPricing()
            ->where('date', '<', $this->date)
            ->get()
            ->sum(function ($schedule) {
                if (Carbon::parse($schedule->date) > $this->date) {
                    return 0;
                }
                return $schedule->repo->overdue($this->date);
            });

        if ($overdue == 0) {
            return null;
        }

        $price = $holding->price();

        $paid = $holding->payments()->sum('amount');

        $pending = $price - $paid;

        $overdueDate = $holding->repo->getDateOverdue($this->date);

        $aging = $overdueDate ? $this->date->diffInDays($overdueDate) : '';

        return (object)[
            'overdue' => $overdue,
            'price' => $price,
            'paid' => $paid,
            'pending' => $pending,
            'percentage_paid' => percentage($paid, $price, 2),
            'aging' => $aging
        ];
    }
}
