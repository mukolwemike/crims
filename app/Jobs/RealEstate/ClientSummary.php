<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ClientSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * ClientSummary constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $clients = Client::has('unitHoldings')->get()
            ->map(
                function (Client $client) {
                    $holdings = UnitHolding::where('client_id', $client->id)->active()->get();

                    $projects = $holdings->each(
                        function ($holding) {
                            $holding->project_name = $holding->project->name;
                        }
                    )->lists('project_name');

                    $projects = Collection::make($projects)->unique()->all();

                    $projects = implode(', ', $projects);

                    $m = new EmptyModel();
                    $m->{'Client Code'} = $client->client_code;
                    $m->Name = ClientPresenter::presentJointFullNames($client->id);
                    $m->Email = $client->contact->email;
                    $m->Phone = $client->contact->phone;
                    $m->Country = is_null($client->country) ? null : $client->country->name;
                    $m->{'Number of units'} = $holdings->count();
                    $m->{'Projects'} = $projects;

                    return $m;
                }
            );

        $filename = 'Real Estate Client Summary';

        Excel::fromModel($filename, $clients)->store('xlsx');

        $this->mailResult(storage_path('exports/' . $filename . '.xlsx'), $this->user);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param User $user
     */
    private function mailResult($file_path, User $user)
    {
        $mailer = new GeneralMailer();

        $mailer->to($user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Real Estate Client summary');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached client summary');

        \File::delete($file_path);
    }
}
