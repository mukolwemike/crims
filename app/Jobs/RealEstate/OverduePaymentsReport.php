<?php

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class OverduePaymentsReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * OverduePaymentsReport constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * Execute the job
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();

        $schedules = RealEstatePaymentSchedule::whereHas('holding', function ($holding) {
            $holding->active();
        })->where('date', '<', $today->copy())
            ->where('paid', '!=', 1)
            ->get();

        $schedulesArray = array();

        foreach ($schedules as $key => $schedule) {
            $overdue = $schedule->repo->overdue($today);

            if ($overdue > 0) {
                $schedulesArray[$schedule->holding->project->name][] = [
                    'Client Name' => ClientPresenter::presentFullNames($schedule->holding->client_id),
                    'Client Code' => $schedule->holding->client->client_code,
                    'Project' => $schedule->holding->project->name,
                    'Unit Number' => $schedule->holding->unit->number,
                    'Size / Type' => $schedule->holding->unit->size->name . ' ' . $schedule->holding->unit->type->name,
                    'Overdue' => $overdue,
                    'Interest Accrued' => $schedule->interestAccrued()->sum('amount'),
                    'Description' => $schedule->description,
                    'Payment Type' => $schedule->type->name,
                    'Date' => $schedule->date,
                    'FA' => is_null($fa = $schedule->holding->client->getLatestFA('realestate')) ? null : $fa->name
                ];
            }
        }

        $filename = 'Real Estate Overdue Payments_' . $key;

        Excel::create($filename, function ($excel) use ($schedulesArray) {
            foreach ($schedulesArray as $key => $value) {
                $excel->sheet(trimText("$key", 30, false), function ($sheet) use ($value) {
                    $sheet->fromArray($value, null, null, true);
                });
            }
        })->store('xlsx', storage_path('exports'));

        $this->mailResult(storage_path('exports/' . $filename . '.xlsx'), $this->user);
    }

    public function getSchedules($today)
    {
    }

    /**
     * @param $file_path
     * @param User $user
     */
    private function mailResult($file_path, User $user)
    {
        $mailer = new GeneralMailer();

        $mailer->to($user->email);
        $mailer->bcc(config('system.administrators'));
        $mailer->from('support@cytonn.com');
        $mailer->subject('Real Estate Overdue Payments Chunked');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail(
            'Please find attached the requested report for real estate overdue payments.'
        );

        \File::delete($file_path);
    }
}
