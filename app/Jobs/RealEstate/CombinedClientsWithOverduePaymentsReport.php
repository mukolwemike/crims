<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 13/12/2018
 * Time: 10:37
 */

namespace App\Jobs\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class CombinedClientsWithOverduePaymentsReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * OverduePaymentsReport constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();

        $schedules = Project::all()->mapWithKeys(function ($project) use ($today) {
            $clients = Client::whereHas('unitHoldings', function ($holding) use ($today) {
                $holding->active()->whereHas('paymentSchedules', function ($schedule) use ($today) {
                    $schedule->where('date', '<', $today->copy());
                });
            })->get()
                ->map(function (Client $client) use ($project, $today) {
                    $overdue = $this->calculatePayments($client, $project);

                    $fa = $client->getLatestFa();

                    $nextInvestment = $this->nextMaturingInvestment($client, $today);

                    return new EmptyModel([
                        'Client Name' => ClientPresenter::presentFullNames($client->id),
                        'Client Code' => $client->client_code,
                        'FA' => $fa ? $fa->name : '',
                        'Project' => $project->name,
                        'No of units' => $overdue['no'],
                        'Total Prices' => $overdue['total'],
                        'Past Schedules' => $overdue['past_schedules'],
                        'Interest' => $overdue['interest'],
                        'Paid' => $overdue['paid'],
                        'Overdue' => $overdue['overdue'],
                        'Upcoming' => $overdue['upcoming'],
                        'Total Investments Value' => $client->repo->getTodayTotalInvestmentsValueForAllProducts($today),
                        'Next Maturing Investment' => $nextInvestment ?
                            convert(
                                $nextInvestment->repo->getTotalValueOfInvestmentAtDate(
                                    $today
                                ),
                                $nextInvestment->product->currency,
                                $today
                            ) : '',
                        'Next Maturity Date' => $nextInvestment ?
                            Carbon::parse($nextInvestment->maturity_date)->toDateString() :
                            '',
                    ]);
                })
                ->filter(function ($overdue) {
                    return $overdue->Overdue > 0;
                });

            return [$project->name => $clients];
        })->filter(function ($clients) {
            return count($clients) > 0;
        });

        $filename = 'Real Estate Clients With Overdue Payments';

        Excel::create($filename, function ($excel) use ($schedules) {
            foreach ($schedules as $key => $value) {
                $excel->sheet(trimText("$key", 30, false), function ($sheet) use ($value) {
                    $sheet->fromModel($value);
                });
            }
        })->store('xlsx', storage_path('exports'));

        $this->mailResult($filename, $this->user);

        \File::delete(storage_path('exports/' . $filename . '.xlsx'));
    }

    private function nextMaturingInvestment(Client $client, Carbon $today)
    {
        return ClientInvestment::where('client_id', $client->id)->active()
            ->where('maturity_date', '>=', $today)->orderBy('maturity_date')->first();
    }

    private function calculatePayments(Client $client, Project $project)
    {
        $holdings = $client->unitHoldings()->with('payments')
            ->active()
            ->whereHas('unit', function ($unit) use ($project) {
                $unit->where('project_id', $project->id);
            })->get();

        $price = $holdings->sum(function ($h) {
            return $h->price();
        });

        $paid = $holdings->sum(function ($h) {
            return $h->payments->sum('amount');
        });

        $overdue = $holdings->sum(function ($h) {
            return $h->paymentSchedules()
                ->inPricing()
                ->get()
                ->sum(function ($sc) {
                    if (Carbon::parse($sc->date)->isFuture()) {
                        return 0;
                    }

                    return $sc->repo->overdue(Carbon::today());
                });
        });

        $interest = $holdings->sum(function ($h) {
            return $h->interest(Carbon::today());
        });

        $upcoming = $holdings->sum(function ($h) {
            return $h->paymentSchedules()
                ->inPricing()
                ->get()
                ->sum(function ($sc) {
                    if (Carbon::parse($sc->date)->isPast()) {
                        return 0;
                    }

                    return $sc->amount;
                });
        });

        $pastSchedules = $holdings->sum(function ($h) {
            return $h->paymentSchedules()
                ->inPricing()
                ->get()
                ->sum(function ($sc) {
                    if (Carbon::parse($sc->date)->isFuture()) {
                        return 0;
                    }

                    return $sc->amount;
                });
        });

        return [
            'no' => $holdings->count(),
            'total' => $price,
            'interest' => $interest,
            'paid' => $paid,
            'overdue' => $overdue,
            'upcoming' => $upcoming,
            'past_schedules' => $pastSchedules
        ];
    }

    /**
     * @param $file_path
     * @param User $user
     */
    private function mailResult($filename, User $user)
    {
        Mailer::compose()
            ->to($user->email)
            ->bcc(config('system.administrators'))
            ->subject('Real Estate Clients With Overdue Payments')
            ->text('Please find attached the requested report for real estate clients with overdue payments ~ '
                .Carbon::today()->toFormattedDateString())
            ->excel([$filename])
            ->send();
    }
}
