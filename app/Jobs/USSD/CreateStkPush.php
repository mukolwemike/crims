<?php

namespace App\Jobs\USSD;

use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Jobs\USSD\SendMessages;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateStkPush implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payBill;
    protected $phone;
    protected $amount;
    protected $reference;

    /**
     * Create a new job instance.
     *
     * @param Client $client
     * @param $payBill
     * @param $phone
     * @param $amount
     * @param $reference
     */
    public function __construct($payBill, $phone, $amount, $reference)
    {

        $this->payBill = $payBill;
        $this->phone = $this->processNumber($phone);
        $this->amount = $amount;
        $this->reference = $reference;

        $this->onQueue(config('queue.priority.payments'));
        $this->tries = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $op = (new CytonnPaymentSystem())->requestMpesaPayment(
                $this->payBill,
                $this->phone,
                $this->amount,
                $this->reference
            );
        } catch (Exception $e) {
            reportException($e);
            $op = false;
        }

        if (!$op) {
            $msg = new SendMessages(
                $this->phone,
                'mpesa-details',
                ['paybill_no' => $this->payBill, 'client_code' => $this->reference]
            );

            dispatch($msg->onConnection('sync'));
        }
    }

    private function processNumber($phone)
    {
        $wplus = ltrim($phone, '+');
        $phone = str_replace(" ", "", $wplus);

        if(starts_with($phone, '0')) {
            $phone = ltrim($phone, '0');

            $phone = '254'.$phone;
        }

        return $phone;
    }

    public function forClient(Client $client)
    {
        $this->reference = $client->client_code;

        return $this;
    }

    public function forApplication(ClientFilledInvestmentApplication $application)
    {
        $this->reference = 'app_'.$application->id;

        return $this;
    }
}
