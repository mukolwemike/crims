<?php

namespace App\Jobs\USSD;

use App\Cytonn\Data\Iprs;
use App\Cytonn\USSD\USSDRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RequestCall implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $phone;

    protected $repo;

    protected $iprs;

    /**
     * Create a new job instance.
     *
     * @param $phone
     */
    public function __construct($phone)
    {
        $this->phone = $phone;

        $this->repo = (new USSDRepository());
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        $this->repo->requestCallFromCRM($this->phone);
    }
}
