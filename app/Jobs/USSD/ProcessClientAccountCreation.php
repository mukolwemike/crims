<?php

namespace App\Jobs\USSD;

use App\Cytonn\Data\Iprs;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\USSD\USSDRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessClientAccountCreation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $application;

    protected $ussdRepo;

    public function __construct(ClientFilledInvestmentApplication $application)
    {
        $this->application = $application;

        $this->ussdRepo = new USSDRepository();
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
         \DB::transaction(function () {
            $app = $this->ussdRepo->processApplication($this->application);

            $app = $this->ussdRepo->approveApplication($app);

            $app->update(['terms_accepted' => false]);

            dispatch(new ProcessClientOnboarding($app));
         });
    }
}
