<?php

namespace App\Jobs\USSD;

use App\Cytonn\Mailers\Client\ContactUsMailer;
use App\Cytonn\USSD\USSDRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ReferClient implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    protected $referredName;

    protected $referredNumber;

    protected $referredEmail;

    protected $clientName;

    protected $source;

    protected $product;

    protected $repo;

    /**
     * Create a new job instance.
     *
     * @param $phone
     */
    public function __construct($data)
    {
        $this->data = $data;

        $this->referredName = $data['fullname'];

        $this->referredNumber = cleanPhoneNumber($data['phone']);

        $this->referredEmail = isset($data['email']) ? $data['email'] : null;

        $this->clientName = $data['client_name'];

        $this->source = $data['source'];

        $this->product = $data['product'];

        $this->repo = (new USSDRepository());
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        try {
//            dd('here', $this->referredNumber);

            $this->repo->requestCallFromCRM($this->referredNumber, $this->referredName, $this->source, $this->referredEmail);
        } catch (\Exception $exception) {
        }

        try {
            if ($this->referredNumber) {
                dispatch(new SendMessages($this->referredNumber, 'client-referral', $this->data));
            } else {
                if ($this->referredEmail) {
                    (new ContactUsMailer())->sendClientReferralNotification($this->data);
                }
            }

        } catch (\Exception $exception) {
        }
    }
}
