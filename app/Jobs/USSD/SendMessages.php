<?php

namespace App\Jobs\USSD;

use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMessages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $phone;

    protected $type;

    protected $data;

    const TERMS_LINK = "https://cytonn.com/cmmf-terms";

    const CYTONN_WEB = "https://clients.cytonn.com";

    const CYTONN_APPLY_WEB = "https://clients.cytonn.com/apply/investment";

    const CYTONN_PHONE = '0709101200';

    /**
     * Create a new job instance.
     *
     * @param $phone
     * @param $type
     * @param array $data
     */
    public function __construct($phone, $type, $data = [])
    {
        $this->phone = $phone;

        $this->type = $type;

        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        \DB::transaction(function () {

            switch ($this->type) {
                case 'terms':
                    $this->sendMMFTerms();
                    break;
                case 'onboarding':
                    $this->onboardingSMS();
                    break;
                case 'bank-details':
                    $this->sendBankDetails();
                    break;
                case 'mpesa-details':
                    $this->sendMpesaDetails();
                    break;
                case 'payment-confirmation':
                    $this->sendPaymentConfirmation();
                    break;
                case 'mmf-info':
                    $this->sendMMFInfo();
                    break;
                case 'request-withdrawal':
                    $this->requestWithdrawal();
                    break;
                case 'confirm-withdrawal':
                    $this->confirmWithdrawal();
                    break;
                case 'confirm-purchase':
                    $this->confirmUnitPurchase();
                    break;
                case 'remind-kyc-upload':
                    $this->sendUploadKycReminder();
                    break;
                case 'update-risk':
                    $this->updateRiskReminder();
                    break;
                case 'risk-updated':
                    $this->updateRiskConfirm();
                    break;
                case 'request-kyc-proof':
                    $this->sendRequestKycProof();
                    break;
                case 'download-app':
                    $this->sendDownloadAppSteps();
                    break;
                case 'send-token':
                    $this->sendToken();
                    break;
                case 'send-app-token':
                    $this->sendApplicationToken();
                    break;
                case 'incomplete-appl-reminder':
                    $this->sendIncompleteApplicationReminder();
                    break;
                case 'transfer-units':
                    $this->sendTransferUnitsNotification();
                    break;
                case 'bill-pay-request':
                    $this->requestBillPay();
                    break;
                case 'airtime-pay-request':
                    $this->requestAirtimePay();
                    break;
                case 'bill-pay-approved':
                    $this->approvedBillPayRequest();
                    break;
                case 'airtime-pay-approved':
                    $this->approvedAirtimeBuyRequest();
                    break;
                case 'utility-payment-failed':
                    $this->utilityPaymentFailed();
                    break;
                case 'utility-payment-successful':
                    $this->utilityPaymentSuccessful();
                    break;
                case 'client-referral':
                    $this->clientReferral();
                    break;
                case 'password-reset-link':
                    $this->passwordResetLink();
                    break;
                default:
                    throw new \InvalidArgumentException("Message type $this->type not found");
                    break;
            }
        });
    }

    protected function passwordResetLink()
    {
        $data = $this->data;

        $link = getenv('CLIENT_DOMAIN') . '/account/' . $data['user']->username . '/reset/' . $data['code'];

        $message = "Dear {$data['user']->firstname}," .
            "visit {link} to reset your Cytonn password. " .
            "For queries, contact us on {phone}";

        $message = str_replace('{link}', $link, $message);
        $message = str_replace('{phone}', static::CYTONN_PHONE, $message);

        $this->send([$this->phone], $message);
    }

    protected function clientReferral()
    {
        $data = $this->data;

        $message = "{$data['client_name']} has invited you to invest with Cytonn! Dial *809# or " .
            "visit {link} to begin your investment journey. " .
            "For queries, contact us on {phone}";

        $message = str_replace('{link}', static::CYTONN_APPLY_WEB, $message);
        $message = str_replace('{phone}', static::CYTONN_PHONE, $message);

        $this->send([$this->phone], $message);
    }

    protected function sendToken()
    {
        $data = $this->data;

        $message = "Dear " . $data['user']->present()->fullName .
            ", please use {token} to verify your transaction in your Cytonn Client Portal.";

        $message = str_replace('{token}', $data['token'], $message);

        $this->send([$this->phone], $message);
    }

    protected function sendApplicationToken()
    {
        $data = $this->data;

        if ($this->data['fa_phone']) {
            $faMessage = "Client-{$this->phone} has made an online application and chosen you as the FA. " .
                "The client has received a code, " .
                "if you are creating on their behalf use this code {$data['token']}.";

            $this->send([$this->data['fa_phone']], $faMessage);

            $clientMessage = "Hello, use code {$data['token']} to verify your phone no. {$this->phone} for your Cytonn account. " .
                "Your financial adviser has received a code in case they are creating your account on your behalf";

            $this->send([$this->phone], $clientMessage);
        } else {
            $message = "Hi a Cytonn account is being created with your number {$this->phone}. " .
                "Use this code {$data['token']} to verify your number.";

            $this->send([$this->phone], $message);
        }
    }

    protected function sendPaymentError()
    {
        $msg = "An error occurred while making payment. Kindly Dial *809# to retry.";

        $this->send([$this->phone], $msg);
    }

    protected function sendMMFTerms()
    {
        $message = "Thank you for activating your Cytonn Unit Funds account. " .
            "Please read the terms and conditions provided here {link}. " .
            "Feel free to contact us on {phone} for any clarifications.";

        $message = str_replace('{link}', static::TERMS_LINK, $message);
        $message = str_replace('{phone}', static::CYTONN_PHONE, $message);

        $this->send([$this->phone], $message);
    }

    protected function sendBankDetails()
    {
        $data = $this->data;

        $account = $data['account'];
        $mpesa = $data['mpesa'];

        $message = '';

        if ($account) {
            $message = 'For Bank deposits please use the following account details;' . PHP_EOL . PHP_EOL .
                'Account Name: ' . $account->account_name . PHP_EOL .
                'Bank Name: ' . $account->bank_name . PHP_EOL .
                'Account Number: ' . $account->account_no . PHP_EOL . PHP_EOL;
        }


        if ($mpesa) {
            $message = $message . 'You can also pay directly via MPESA to PayBill '
                . $mpesa->account_no . ', Account No ' . $data['client_code'] . '.';
        }

        if ($message) {
            $this->send([$this->phone], $message);
        }
    }

    protected function sendMpesaDetails()
    {
        $data = $this->data;

        $message = "Please note that you can make your deposits via M-pesa. Our Pay bill No " .
            "is {$data['paybill_no']} and your Assigned Account no is {$data['client_code']}";

        $this->send([$this->phone], $message);
    }

    protected function sendPaymentConfirmation()
    {
        $data = $this->data;

        $message = "Hello " . ClientPresenter::presentFirstName($data['client_id']) .
            ", This is to confirm receipt of your deposit of KES {$data['amount']}." .
            "You shall receive an SMS once the purchase of the units is successful.";

        $this->send([$this->phone], $message);
    }

    protected function confirmUnitPurchase()
    {
        $data = $this->data;

        $message = "This is to confirm receipt of your deposit of KES {$data['amount']} in Cytonn Money Market Fund." .
            " We shall send a business confirmation on the investment.";

        $this->send([$this->phone], $message);
    }

    protected function sendUploadKycReminder()
    {
        $data = $this->data;

        $message = "Dear " . ClientPresenter::presentFirstName($data['client_id']) . ", " . PHP_EOL .
            " thank you for investing in Cytonn Money Market Fund. " .
            "Please upload a copy of your ID at {link} here to ensure you can withdraw your investment in future.";

        $link = '<a href="#">ID Upload</a>';

        $message = str_replace('{link}', $link, $message);

        $this->send([$this->phone], $message);
    }

    protected function requestWithdrawal()
    {
        $data = $this->data;

        $message = "This is to acknowledge the request to withdraw KES {$data['number']} from Cytonn Money Market Fund."
            . "We're currently processing your instruction and we will update you shortly. " .
            "CMMF, a wallet that you stand to earn upto 11% p.a.";

        $this->send([$this->phone], $message);
    }

    protected function confirmWithdrawal()
    {
        $data = $this->data;

        $message = "Dear " . ClientPresenter::presentFirstName($data['client_id']) .
            ", KES {$data['amount']} has been withdrawn from CAML Money Market Fund. " .
            "Your new Balance is KES {$data['balance']} 
            . For subsequent investments, kindly dial  *809# or pay directly from your " .
            "MPESA menu using paybill {$data['paybill_no']}
             and account number {$data['client_code']}";

        $this->send([$this->phone], $message);
    }


    protected function updateRiskReminder()
    {
        $data = $this->data;

        $message = ClientPresenter::presentFirstName($data['client_id']) . "thank you for investing with us. 
        We would like to know more about you. " .
            "Kindly dial *809#  and choose “My Account” option then select “Risk Info option”.";

        $this->send([$this->phone], $message);
    }

    protected function updateRiskConfirm()
    {
        $message = "Your risk profile has been Updated. Kindly Dial *809# to continue Investing";

        $this->send([$this->phone], $message);
    }

    protected function sendMMFInfo()
    {
        $link = 'https://cytonn.com/asset-managers/money-market-fund';

        $message = 'Thank you for your interest in Cytonn Money Market Fund. For more information please visit ' . $link;

        $this->send([$this->phone], $message);
    }

    protected function sendRequestKycProof()
    {
        $data = $this->data;

        $client = Client::find($data['client_id']);

        $name = $client->joint
            ? ClientPresenter::presentJointFirstNames($client->id) : ClientPresenter::presentFirstName($client->id);

        $message = "Dear " . $name . ", The names you provided did not " .
            "match those in your provided document. Kindly update the details by dialing *809# and select My Account or " .
            "upload a copy of your {doc} by visiting our client portal by following this {link}. Call us on "
            . static::CYTONN_PHONE . '.';

        $doc = $data['type'] == 'pin_no' ? 'Tax Pin' : 'ID/Passport';

        $message = str_replace('{doc}', $doc, $message);
        $message = str_replace('{link}', "https://clients.cytonn.com/", $message);

        $this->send([$this->phone], $message);
    }


    protected function sendDownloadAppSteps()
    {
        $android = 'https://www.cytonn.com/android-app';
        $iphone = 'https://www.cytonn.com/iphone-app';

        $message = 'Click for Android (' . $android . ') or for Iphone (' . $iphone
            . ') to download the Cytonn Investment Management App to manage your investments.';

        $this->send([$this->phone], $message);
    }


    protected function onboardingSMS()
    {
        $message = "Thank you for activating your Cytonn account. " .
            "Dial *809# to access your Money Market account and use {pin} as your PIN. " .
            "Quote your client code {client_code} whenever you send funds to us. " .
            "Please read the terms and conditions provided here {link}. " .
            "For queries, call us on {phone}.";

        $message = str_replace("{link}", static::TERMS_LINK, $message);
        $message = str_replace("{phone}", static::CYTONN_PHONE, $message);
        $message = str_replace("{client_code}", $this->data['client_code'], $message);
        $message = str_replace("{pin}", $this->data['pin'], $message);

        $this->send([$this->phone], $message);
    }

    protected function sendTransferUnitsNotification()
    {
        $data = $this->data;
        $sender = $data['sender'];
        $receiver = $data['receiver'];
        $units = $data['units'];

        $message = '';

        if ($data['type'] === 'sender') {
            $message = "Hi " . ClientPresenter::presentFirstName($sender->id) . ", you just transferred " . $units . " units 
            from your CMMF account to " . ClientPresenter::presentFullNameNoTitle($receiver->id) . " - " .
                $receiver->client_code . ". Transaction ref 00. Thank you for doing business with us.";
        } elseif ($data['type'] === 'receiver') {
            $message = "Great news, " . ClientPresenter::presentFullNameNoTitle($sender->id) . " has transferred " . $units .
                " units to your CMMF account. Transaction ref 00. Thank you for doing business with us.";
        } elseif ($data['type'] === 'joint' || $data['type'] === 'corporate') {
            $message = "This is to acknowledge the request to transfer " . $units . " units from your CMMF to
                " . ClientPresenter::presentFullNameNoTitle($receiver->id) . " - " . $receiver->client_code .
                " We're currently processing your instruction and we will update you shortly. 
                Thank you for doing business with us.";
        }

        $this->send([$this->phone], $message);
    }

    protected function sendIncompleteApplicationReminder()
    {
        $data = $this->data;

        $msg = "Hello " . $data['client_name'] . ", Cytonn is reminding you to complete your application to " .
            "Cytonn Money Market Fund. Use the following account details to make your payments " . PHP_EOL . PHP_EOL .
            "Account Name: " . $data['account']->account_name . PHP_EOL .
            "Bank Name: " . $data['account']->bank_name . PHP_EOL .
            "Account Number: " . $data['account']->account_no . PHP_EOL . PHP_EOL .
            "Or" . PHP_EOL . PHP_EOL .
            "Mpesa Paybill: " . $data['mpesa']->account_no . PHP_EOL .
            "Account Number: " . $data['client_code'];

        $this->send([$this->phone], $msg);
    }

    protected function requestBillPay()
    {
        $data = $this->data;

        $message = "This is to acknowledge the request to pay {$data['bill_name']} bill. " .
            "Account Name :  {$data['account_name']}, Account Number: {$data['account_number']}, " .
            "Amount: KES {$data['amount']} from {$data['source_fund']}. " .
            "We're currently processing your instruction and we will update you shortly.";

        $this->send([$this->phone], $message);
    }

    protected function requestAirtimePay()
    {
        $data = $this->data;

        $message = "This is to acknowledge the request to buy airtime for Phone Number {$data['account_number']}. " .
            "Amount: KES {$data['amount']} from {$data['source_fund']}. " .
            "We're currently processing your instruction and we will update you shortly.";

        $this->send([$this->phone], $message);
    }

    protected function approvedBillPayRequest()
    {
        $data = $this->data;

        $message = "Your request to pay {$data['bill_name']} bill. " .
            "Account Name :  {$data['account_name']}, Account Number: {$data['account_number']}, " .
            "Amount: KES {$data['amount']} from {$data['source_fund']} has been successfully approved, Ref {$data['ref_id']}.  " .
            "Thank you for doing business with us.";

        $this->send([$this->phone], $message);
    }

    protected function approvedAirtimeBuyRequest()
    {
        $data = $this->data;

        $message = "Your request to buy airtime for Phone NUmber {$data['account_number']}. " .
            "Amount: KES {$data['amount']} from {$data['source_fund']} has been successfully approved, Ref {$data['ref_id']}. " .
            "Thank you for doing business with us.";

        $this->send([$this->phone], $message);
    }

    protected function utilityPaymentFailed()
    {
        $data = $this->data;

        $message = "Your request to pay {$data['bill_name']} bill. " .
            "Account Name :  {$data['account_name']}, Account Number: {$data['account_number']}, " .
            "Amount: KES {$data['amount']} from {$data['source_fund']} has failed, Ref {$data['ref_id']}.  " .
            "Call {phone} for assistance.";

        $message = str_replace('{phone}', static::CYTONN_PHONE, $message);

        $this->send([$this->phone], $message);
    }

    protected function utilityPaymentSuccessful()
    {
        $data = $this->data;

        if ($data['bill_type'] == 'gotv' || $data['bill_type'] == 'dstv') {
            $message = $this->dstvMessage($data);
        } elseif ($data['bill_type'] == 'kplc_prepaid') {
            $message = $this->kplcMessage($data);
        } else {
            $message = $this->billMessage($data);
        }

        $message = str_replace('{phone}', static::CYTONN_PHONE, $message);

        $this->send([$this->phone], $message);
    }

    private function kplcMessage($data)
    {
        return "KPLC Mtr No: {$data['destination_account_number']} " . PHP_EOL .
            "{$data['biller_response']}" . PHP_EOL .
            "Amount: {$data['amount']}" . PHP_EOL .
            "Date: " . Carbon::now()->toDateString() . PHP_EOL .
            "Ref: {$data['reference']}" . PHP_EOL .
            "Source Number: {$data['source_account_number']}" . PHP_EOL .
            "Call {phone} for assistance.";
    }

    private function dstvMessage($data)
    {
        return "Account No: {$data['destination_account_number']} " . PHP_EOL .
            "{$data['biller_response']}" . PHP_EOL .
            "Amount: {$data['amount']}" . PHP_EOL .
            "Date: " . Carbon::now()->toDateString() . PHP_EOL .
            "Ref: {$data['reference']}" . PHP_EOL .
            "Source Number: {$data['source_account_number']}" . PHP_EOL .
            "Call {phone} for assistance.";
    }

    private function billMessage($data)
    {
        $bill = isset($data['bill']) ? $data['bill'] : (isset($data['bill_type']) ? $data['bill_type'] : 'Airtime');

        $date = isset($data['datePaid']) ? $data['datePaid'] : Carbon::now()->toDateString();

        $message = "You have paid KES. {$data['amount']} to {$bill} on {$date}, ref {$data['reference']}. Call {phone}";

        $message = str_replace('{phone}', static::CYTONN_PHONE, $message);

        return $message;
    }

    protected function send($phones, $message)
    {
        \SMS::send($phones, $message);
    }
}
