<?php

namespace App\Jobs\USSD;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Data\Iprs;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\USSD\USSDRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessClientOnboarding implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $application;

    protected $ussdRepo;

    public function __construct(ClientInvestmentApplication $application)
    {
        $this->application = $application;

        $this->ussdRepo = new USSDRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
         \DB::transaction(function () {
            $app = $this->application;

            (new ClientApplicationRepository())->checkAndNotifyRiskyClient($app->form->toArray(), "CRIMS-USSD");

            $this->ussdRepo->createClientUser($app, $app->client);

            $this->requestIprs($app->client);

            $this->ussdRepo->createMpesaAccount($app->client);
         });
    }

    public function requestIprs($client)
    {
        (new Iprs())->createRequest($client);
    }
}
