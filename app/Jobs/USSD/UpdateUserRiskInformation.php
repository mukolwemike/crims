<?php

namespace App\Jobs\USSD;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateUserRiskInformation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    protected $phone;

    public function __construct($data, $phone)
    {
        $this->data = $data;

        $this->phone = $phone;
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        return \DB::transaction(function () {
            $this->updateClientFilledApplication();

            $this->sendMessage();
        });
    }

    protected function updateClientFilledApplication()
    {
        $application = $this->getApplication();

        $this->data['progress_risk'] = 1;

        unset($this->data['account_option'], $this->data['average_income']);

        $application->update($this->data);
    }

    protected function sendMessage()
    {
        (new SendMessages($this->phone, 'risk-updated'));
    }

    protected function getApplication()
    {
        list($phone, $wZero, $w254) = $this->cleanUpForm();

        return ClientFilledInvestmentApplication::where(function ($app) use ($phone, $wZero, $w254) {
            $app->where('telephone_home', $phone)
                ->orWhere('telephone_home', $wZero)
                ->orWhere('telephone_home', $w254);
        })->latest();
    }

    protected function cleanUpForm(): array
    {
        $phone = ltrim($this->phone, '+');
        $phone = ltrim($phone, '254');
        $phone = ltrim($phone, '0');

        $wZero = '0' . $phone;

        $w254 = '254' . $phone;

        return array($phone, $wZero, $w254);
    }
}
