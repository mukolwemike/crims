<?php

namespace App\Jobs\Portfolio;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Models\Portfolio\DepositHoldingRepayments;
use Cytonn\Presenters\BooleanPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PortfolioWithHoldingTax implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $start;

    protected $end;

    protected $fundManager;

    /**
     * PortfolioWithHoldingTax constructor.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     * @param FundManager $fundManager
     */
    public function __construct(Carbon $start, Carbon $end, User $user, FundManager $fundManager)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user = $user;
        $this->fundManager = $fundManager;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = $this->start;
        $end = $this->end;
        $user = $this->user;
        $fundManager = $this->fundManager;

        $repayments = DepositHoldingRepayments::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->get()
            ->map(
                function (DepositHoldingRepayments $repayment) {
                    $out = new EmptyModel();

                    $net = (float)$this->netInterestOnRepayment($repayment);
                    $tax = $this->taxOnRepayment($repayment);
                    $gross = $net + $tax;

                    $arr = [
                        'Investor' => $repayment->investment->security->fundManager->name,
                        'Institution' => $repayment->investment->security->investor->name,
                        'Principal' => (float)$repayment->investment->amount,
                        'Taxable' => BooleanPresenter::presentYesNo($repayment->investment->taxable),
                        'Action Date' => $repayment->date->toFormattedDateString(),
                        'Action Type' => $repayment->type->name,
                        'Amount' => (float)$repayment->amount,
                        'Gross Interest' => $gross,
                        'Net Interest' => $net,
                        'W/Tax' => $tax
                    ];

                    return $out->fill($arr);
                }
            );

        $withdrawals = DepositHolding::where('withdrawn', 1)
            ->fundManager($fundManager)
            ->where('withdrawal_date', '>=', $start)
            ->where('withdrawal_date', '<=', $end)
            ->get()
            ->map(
                function (DepositHolding $investment) use ($end) {
                    $out = new EmptyModel();

                    $net = $investment->repo->getNetInterestAfterRepayments();
                    $tax = $this->taxOnWithdrawal($investment);
                    $gross = $net + $tax;

                    $arr = [
                        'Investor' => $investment->security->fundManager->name,
                        'Institution' => $investment->security->investor->name,
                        'Principal' => (float)$investment->amount,
                        'Taxable' => BooleanPresenter::presentYesNo($investment->taxable),
                        'Action Date' => $investment->withdrawal_date->toFormattedDateString(),
                        'Action Type' => 'Withdrawal',
                        'Amount' => (float)$investment->withdraw_amount,
                        'Gross Interest' => $gross,
                        'Net Interest' => $net,
                        'W/Tax' => $tax
                    ];

                    return $out->fill($arr);
                }
            );

        $coll = new Collection();
        $coll = $coll->merge($withdrawals->all())
            ->merge($repayments->all())
            ->groupBy('Investor');

        $fname = 'Withholding Tax report';

        Excel::fromModel($fname, $coll)->store('xlsx');

        $this->sendExcel(
            'Portfolio Withholding Tax Report',
            'Please find attached a report of withholding tax for the period between ' .
            $start->toFormattedDateString() . ' to ' . $end->toFormattedDateString(),
            $fname,
            [$user->email]
        );
    }

    private function taxOnRepayment($repayment)
    {
        if (!$repayment->investment->taxable) {
            return 0;
        }

        return setting('withholding_tax_rate') * $this->netInterestOnRepayment($repayment) / 100;
    }

    private function netInterestOnRepayment($repayment)
    {
        if ($repayment->type->slug == 'principal_repayment') {
            return 0;
        }

        return $repayment->amount;
    }

    private function taxOnWithdrawal(DepositHolding $investment)
    {
        if (!$investment->taxable) {
            return 0;
        }

        $net = $investment->repo->getNetInterestAfterRepayments();

        return setting('withholding_tax_rate') * $net / 100;
    }
}
