<?php

namespace App\Jobs\Portfolio;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Portfolio\Deposits\Analysis;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DepositAnalysis implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ExcelMailer;

    /**
     * @var FundManager
     */
    private $fundManager;
    /**
     * @var Carbon
     */
    private $date;
    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param FundManager $fundManager
     * @param Carbon $date
     * @param User $user
     */
    public function __construct(FundManager $fundManager, Carbon $date, User $user)
    {
        $this->fundManager = $fundManager;
        $this->date = $date;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currency = Currency::where('code', 'KES')->first();

        $classes = (new Analysis($currency, $this->fundManager, $this->date))->generate();

        $classes = $classes->map(function ($class) {
            return new EmptyModel($class);
        });

        Excel::fromModel('deposit_analysis', $classes->all())->store('xlsx');

        Mail::compose()
            ->to($this->user->email)
            ->subject('Deposit Analysis Report - ' . $this->fundManager->name)
            ->text('Please find attached the deposit analysis for ' .
                $this->fundManager->name . ' as at ' . $this->date->toFormattedDateString())
            ->excel(['deposit_analysis'])
            ->send();
    }
}
