<?php

namespace App\Jobs\Portfolio;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Portfolio\Equities\PortfolioValuation;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PorfolioEquitiesReport implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $fund;

    /**
     * ShareHoldingReport constructor.
     *
     * @param UnitFund $fund
     * @param User $user
     */
    public function __construct($fund, User $user)
    {
        $this->fund = (!is_null($fund)) ? UnitFund::findOrFail($fund) : null;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fund = $this->fund;

        $securities = $this->equitySecurities($fund);

        $securities = $securities->get()
            ->map(function (PortfolioSecurity $security) {
                $out = new EmptyModel();

                $analytics = $security->repo->setFund($this->fund);

                $out->{'Name'} = $security->name;
                $out->{'Cost Value'} = AmountPresenter::currency($analytics->averagePrice(), false, 2);
                $out->{'Market Value'} = AmountPresenter::currency($analytics->totalCurrentMarketValue(), false, 2);
                $out->{'Percentage Exposure'} = AmountPresenter::currency($this->calculateExposure($security), false, 2);

                return $out;
            });

        $fname = $fund ? $fund->name . ' Summary Equity Report' : 'Fund Summary Equities Report';

        Excel::fromModel($fname, $securities)->store('xlsx');

        $this->sendExcel(
            $fname,
            'Please find the attached fund summary equities report',
            $fname,
            [$this->user->email]
        );
    }

    private function equitySecurities(UnitFund $unitFund = null)
    {

        $eq = PortfolioSecurity::forAssetClass('equities');

        if ($unitFund) {
            $eq = $eq->whereHas('equityHoldings', function ($holdings) use ($unitFund) {
                $holdings->where('unit_fund_id', $unitFund->id);
            });
        }

        return $eq;
    }

    private function calculateExposure($security)
    {
        try {
            return 100 * $security
                    ->repo->totalCurrentMarketValue() / (new PortfolioValuation($security->fundManager))
                    ->portfolioValue();
        } catch (\Exception $e) {
            return 0;
        }
    }
}
