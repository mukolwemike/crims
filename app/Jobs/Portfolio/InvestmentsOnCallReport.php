<?php

namespace App\Jobs\Portfolio;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvestmentsOnCallReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $fundManager;

    /**
     * InvestmentsOnCallReport constructor.
     *
     * @param User $user
     * @param FundManager $fundManager
     */
    public function __construct(User $user, FundManager $fundManager)
    {
        $this->user = $user;
        $this->fundManager = $fundManager;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $investments = DepositHolding::where('on_call', true)
            ->fundManager($this->fundManager)
            ->get()
            ->map(
                function (DepositHolding $investment) {
                    $e = new EmptyModel();

                    $e->{'Institution'} = $investment->security->investor->name;
                    $e->{'FM'} = $investment->security->fundManager->name;
                    $e->{'Custodial Account'} = $investment->custodialAccount()->account_name;
                    $e->{'Taxable'} = BooleanPresenter::presentYesNo($investment->taxable);
                    $e->{'Fund Type'} = $investment->security->subAssetClass->name;
                    $e->{'Invested Date'} = $investment->invested_date;
                    $e->{'Maturity Date'} = $investment->maturity_date;
                    $e->{'Interest rate'} = $investment->interest_rate;
                    $e->{'Amount'} = $investment->amount;
                    $e->{'Withdrawn'} = BooleanPresenter::presentYesNo($investment->withdrawn);
                    $e->{'Rolled'} = BooleanPresenter::presentYesNo($investment->rolled);
                    $e->{'Withdrawal Date'} = $investment->withdrawal_date;
                    $e->{'Withdrawn By'} = UserPresenter::presentFullNames($investment->withdrawn_by);
                    $e->{'Withdraw Amount'} = $investment->withdraw_amount;
                    $e->{'Contact Person'} = $investment->contact_person;

                    return $e;
                }
            )->groupBy('FM');

        $file_name = 'Portfolio Investments On Call';

        Excel::fromModel($file_name, $investments)->store('xlsx');

        $this->mailResult(storage_path('exports/' . $file_name . '.xlsx'), $this->user);
    }

    /**
     * @param $file_path
     * @param User $user
     */
    private function mailResult($file_path, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->cc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Portfolio Investments On Call');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail(
            'Please find the attached the requested report of portfolio investments on call.'
        );

        \File::delete($file_path);
    }
}
