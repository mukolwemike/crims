<?php

namespace App\Jobs\Portfolio;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PortfolioSummary implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $date;

    protected $fundManager;

    public function __construct(Carbon $date, FundManager $fundManager, User $user)
    {
        $this->date = $date;
        $this->fundManager = $fundManager;
        $this->user = $user;
    }

    public function handle()
    {
        $portfolioInvestorIds = PortfolioSecurity::where('fund_manager_id', $this->fundManager->id)
            ->pluck('portfolio_investor_id')->toArray();
        $summary = PortfolioInvestor::whereIn('id', $portfolioInvestorIds)
            ->get()
            ->map(
                function (PortfolioInvestor $investor) {
                    $out = new EmptyModel();

                    $currency = Currency::where('code', 'KES')->latest()->first();

                    $gross_interest = $investor->repo->totalGrossInterest(
                        $currency,
                        $this->fundManager,
                        $this->date,
                        null
                    );
                    $interest_repayments = $investor->repo->totalRepayments(
                        $currency,
                        $this->fundManager,
                        $this->date,
                        'interest_repayment',
                        null
                    );

                    $arr = [
                        'id' => $investor->id,
                        'code' => $investor->code,
                        'name' => $investor->name,
                        'amount' => $investor->repo
                            ->totalDepositsPrincipal($currency, $this->fundManager, $this->date, null),
                        'value' => $investor->repo->totalDepositValue(
                            $currency,
                            $this->fundManager,
                            $this->date,
                            false,
                            null
                        ),
                        'gross_interest' => $gross_interest,
                        'interest_repayments' => $interest_repayments,
                        'adjusted_gross_interest' => $gross_interest - $interest_repayments,
                        'net_interest' => $investor->repo
                            ->totalNetInterest($currency, $this->fundManager, $this->date, null),
                        'interest_repaid' => $interest_repayments,
                        'withholding_tax' => $investor->repo
                            ->totalWithholdingTax($currency, $this->fundManager, $this->date, null)
                    ];

                    return $out->fill($arr);
                }
            );

        $fileName =
            "{$this->fundManager->name} Portfolio Summary Report : {$this->date->copy()->toFormattedDateString()}";

        Excel::fromModel($fileName, $summary)->store('xlsx');

        $this->sendExcel(
            $fileName,
            'Please find attached a report of the portfolio summary report as at ' .
            $this->date->copy()->toFormattedDateString(),
            $fileName,
            [$this->user->email]
        );
    }
}
