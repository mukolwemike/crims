<?php

namespace App\Jobs\Portfolio;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Portfolio\Summary\Analytics as AssetAnalytics;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ResidualIncomeAnalysis implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $fundManager;

    protected $date;

    /**
     * ResidualIncomeAnalysis constructor.
     *
     * @param FundManager $fundManager
     * @param Carbon $date
     * @param User $user
     */
    public function __construct(FundManager $fundManager, Carbon $date, User $user)
    {
        $this->fundManager = $fundManager;
        $this->date = $date;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fundManager = $this->fundManager;
        $date = $this->date;

        $inst = PortfolioSecurity::whereHas('investments', function ($i) use ($fundManager) {
            $i->fundManager($fundManager);
        })->get();

        $currency = Currency::where('code', 'KES')->first();

        $data = $inst->map(
            function (PortfolioSecurity $i) use ($fundManager, $date, $currency) {
                $out = new EmptyModel();
                $analytics = new AssetAnalytics($fundManager, $date);
                $analytics->setBaseCurrency($currency);
                $analytics->setPortfolioInstitution($i);

                $out->{'Name'} = $i->name;
                $out->{'Cost Value'} = $cost = $analytics->costValue();
                $out->{'Market Value'} = $analytics->marketValue();
                $out->{'Cash Bal'} = $bal = $analytics->custodialBalance();
                $out->{'Avg Rate'} = $analytics->getInvestments()->sum(
                    function (DepositHolding $inv) use ($cost, $date, $bal) {
                        $total = $cost;

                        return $inv->repo->currentRate($date) * ($inv->repo->principal($date) / $total);
                    }
                );

                $out->{'Weighted Rate'} = $analytics->weightedRate();

                return $out;
            }
        );

        $file = 'Residual Income Analysis for ' . $fundManager->name . ' as at ' . $date->toFormattedDateString();

        Excel::fromModel($file, ['Assets' => $data])->store('xlsx');

        $this->sendExcel($file, 'Please find attached the ' . $file, $file, [$this->user->email]);

        return;
    }
}
