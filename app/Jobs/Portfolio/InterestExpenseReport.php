<?php

namespace App\Jobs\Portfolio;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InterestExpenseReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $start;

    protected $end;

    protected $user;

    protected $fundManager;

    /**
     * Create a new job instance.
     * InterestExpenseReport constructor.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     * @param FundManager $fundManager
     */
    public function __construct(Carbon $start, Carbon $end, User $user, FundManager $fundManager)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user = $user;
        $this->fundManager = $fundManager;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = $this->start;
        $end = $this->end;
        $fundManager = $this->fundManager;

        $investments = DepositHolding::activeBetweenDates($start, $end)
            ->fundManager($fundManager)
            ->get();

        $report = $investments->map(
            function (DepositHolding $investment) use ($start, $end, $fundManager) {
                $out = new EmptyModel();
                $out->{'Security Name'} = $investment->security->name;
                $out->{'Custodial Account'} = $investment->investTransaction->custodialAccount->account_name;
                $out->{'Taxable'} = BooleanPresenter::presentYesNo($investment->taxable);
                $out->{'Principal'} = $investment->amount;
                $out->{'Value Date'} = $investment->invested_date;
                $out->{'Maturity Date'} = $investment->maturity_date;
                $out->{'Interest Rate'} = $investment->interest_rate;
                $out->{'Withdrawn'} = BooleanPresenter::presentYesNo($investment->withdrawn);
                $out->{'Withdrawal Date'} = $investment->withdrawal_date;
                $out->{'Fund Manager'} = $investment->security->fundManager->name;

                for ($date = $start->copy(); $date->lte($end); $date = $date->addMonthNoOverflow()) {
                    $started =
                        $investment->repo->grossInterestAccrued($date->copy()->startOfMonth(), false);
                    //use next day
                    $ended =
                        $investment->repo->grossInterestAccrued($date->copy()->endOfMonth(), true);

                    $out->{$date->format('M, Y') . ' Accrued'} = $ended - $started;
                    $out->{$date->format('M, Y') . ' Paid'} =
                        $investment->repo->grossInterestRepaid($date, true);
                }

                return $out;
            }
        );

        $fname = '[Portfolio] Monthly Interest Income';

        Excel::fromModel($fname, $report)->store('xlsx');

        $file_path = storage_path('exports/' . $fname . '.xlsx');

        $this->mailResult($file_path, $start, $end, $this->user);

        \File::delete($file_path);
    }

    private function mailResult($file_path, Carbon $start, Carbon $end, User $user)
    {
        $mailer = new GeneralMailer();

        $mailer->to($user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Portfolio Interest Income Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail(
            'Here is the interest accrued between ' . $start->toFormattedDateString() . ' and ' .
            $end->toFormattedDateString()
        );
    }
}
