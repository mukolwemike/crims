<?php

namespace App\Jobs\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\InterestPayment;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Base;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class InvestmentProductionReport
 *
 * @package App\Jobs\Investments
 */
class InvestmentProductionReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ExcelMailer;

    /**
     * @var Carbon
     */
    private $start;

    /**
     * @var Carbon
     */
    private $end;

    /**
     * @var array
     */
    private $users;

    /**
     * @var
     */
    private $granularity;


    /**
     * @var array
     */
    private $granularityUnits = ['daily'=>'day', 'monthly'=>'monthNoOverflow'];

    /**
     * Create a new job instance.
     *
     * @param  Carbon      $start
     * @param  Carbon      $end
     * @param  $granularity
     * @param  array       $users
     * @throws ClientInvestmentException
     */
    public function __construct(Carbon $start, Carbon $end, $granularity, array $users = [])
    {
        $this->start = $start;
        $this->end = $end;
        $this->users = $users;
        $this->granularity = $granularity;

        if (!in_array($granularity, array_keys($this->granularityUnits))) {
            throw new ClientInvestmentException("Granularity can either be daily or monthly");
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $periods = collect($this->getPeriods());

        $output = FundManager::has('products')->get()
            ->mapToAssoc(
                function (FundManager $fundManager) use ($periods) {
                    $currencies = Currency::whereHas('products', function ($products) use ($fundManager) {
                        $products->whereHas(
                            'investments',
                            function ($i) use ($fundManager) {
                                $i->fundManager($fundManager);
                            }
                        );
                    })->get();

                    $sheet = $periods->map(function ($period) use ($currencies, $fundManager) {
                        $arr = [
                            'Start Date' => $period->start->toDateString(),
                            'End Date' => $period->end->toDateString(),
                            ];

                        foreach ($currencies as $currency) {
                            $arr['Inflows - '.$currency->code] =
                                    (float) $this->getInflows(
                                        $period->start,
                                        $period->end,
                                        $fundManager,
                                        $currency
                                    );
                            $arr['Withdrawals - '.$currency->code] =
                                    (float) $this->getWithdrawals(
                                        $period->start,
                                        $period->end,
                                        $fundManager,
                                        $currency
                                    );
                            $arr['Interest Payments - '.$currency->code] =
                                    (float) $this->getInterestPayments(
                                        $period->start,
                                        $period->end,
                                        $fundManager,
                                        $currency
                                    );
                        }

                        return (new EmptyModel)->fill($arr);
                    });

                    return [$fundManager->name, $sheet];
                }
            );

        Excel::fromModel('Production', $output)->store('xlsx');

        $this->sendExcel(
            "Production Report",
            "Please find attached the production report from
             {$this->start->toFormattedDateString()} to
              {$this->end->toFormattedDateString()}",
            'Production',
            $this->users
        );
    }

    private function getInflows(Carbon $start, Carbon $end, FundManager $fundManager, Currency $currency)
    {
        return ClientInvestment::investedBetweenDates($start, $end)
            ->fundManager($fundManager)
            ->currency($currency)
            ->get()
            ->sum(function (ClientInvestment $investment) {
                return $investment->repo->inflow();
            });
    }

    private function getWithdrawals(Carbon $start, Carbon $end, FundManager $fundManager, Currency $currency)
    {
        return ClientInvestmentWithdrawal::where('withdraw_type', '!=', 'interest')
            ->columnBetween('date', $start, $end)
            ->ofType(Base::TYPE_SENT_TO_CLIENT)
            ->fundManager($fundManager)
            ->currency($currency)
            ->sum('amount');
    }

    private function getInterestPayments(Carbon $start, Carbon $end, FundManager $fundManager, Currency $currency)
    {
        return ClientInvestmentWithdrawal::where('withdraw_type', 'interest')
            ->columnBetween('date', $start, $end)
            ->ofType(Base::TYPE_SENT_TO_CLIENT)
            ->fundManager($fundManager)
            ->currency($currency)
            ->sum('amount');
    }

    /**
     * @return array
     */
    private function getPeriods()
    {
        $unit = ucfirst($this->granularityUnits[$this->granularity]);

        $periods = [];

        for ($date = $this->start->copy(); $date->lte($this->end->copy()); $date = $date->{'add'.$unit}()) {
            $periods[] = (object)['start'=>$date->copy(), 'end'=>$date->copy()->{'add'.$unit}()->subDay()];
        }

        return $periods;
    }
}
