<?php

namespace App\Jobs\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvestmentsWithPrincipal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $principal;

    protected $user;

    public function __construct($principal, User $user)
    {
        $this->principal = $principal;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $investments = ClientInvestment::active()
            ->where('amount', '>=', $this->principal)
            ->get()
            ->map(
                function ($investment) {
                    $out = new EmptyModel();

                    $out->{'Client Name'} = ClientPresenter::presentFullNames($investment->client->id);
                    $out->{'Client Code'} = $investment->client->client_code;
                    $out->{'Principal'} = $investment->amount;
                    $out->{'Investment Date'} = $investment->invested_date;
                    $out->{'Maturity Date'} = $investment->maturity_date;

                    return $out;
                }
            );

        if ($investments->count()) {
            $file_name = 'CHYS Clients with investments over ' . AmountPresenter::currency($this->principal);

            Excel::fromModel($file_name, $investments)->store('xlsx');

            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            $this->mailResult($file_path, $this->principal, $this->user);
        }
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $principal
     * @param User|null $user
     */
    private function mailResult($file_path, $principal, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('CHYS Clients with investments over ' . AmountPresenter::currency($principal));
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find attached the requested report of CMS Clients
         with investments over ' . AmountPresenter::currency($principal));
    }
}
