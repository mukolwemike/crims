<?php

namespace App\Jobs\Investments;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class InvestmentsWithCumulativePrincipal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $principal;

    public function __construct($principal, User $user)
    {
        $this->principal = $principal;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $clients = Client::whereHas(
            'investments',
            function ($investment) {
                $investment->active();
            }
        )->get()->map(
            function (Client $client) {
                $out = new EmptyModel();

                $out->{'Client Name'} = ClientPresenter::presentFullNames($client->id);
                $out->{'Client Code'} = $client->client_code;
                $out->{'Amount'} = $client->investments()->active()->sum('amount');

                return $out;
            }
        );
        if ($clients->count()) {
            $file_name = 'CHYS Clients with cumulative investments over ' . AmountPresenter::currency($this->principal);

            Excel::fromModel($file_name, $clients)->store('xlsx');

            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            $this->mailResult($file_path, $this->principal, $this->user);
        }
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $principal
     * @param User|null $user
     */
    private function mailResult($file_path, $principal, User $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('CHYS Clients with investments over ' . AmountPresenter::currency($principal));
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail(
            'Please find attached the requested report of CMS Clients with cumulative investments over ' .
            AmountPresenter::currency($principal)
        );
    }
}
