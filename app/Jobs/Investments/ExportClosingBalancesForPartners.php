<?php

namespace App\Jobs\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExportClosingBalancesForPartners implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $start;

    protected $end;

    protected $user;

    /**
     * ExportClosingBalancesForPartners constructor.
     *
     * @param $start
     * @param $end
     * @param User $user
     */
    public function __construct($start, $end, User $user)
    {
        $this->start = Carbon::parse($start);
        $this->end = Carbon::parse($end);
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file_path = $this->export($this->start, $this->end);

        $this->mailResult($file_path, $this->start, $this->end, $this->user);

        \File::delete($file_path);
    }

    /**
     * Export closing balances
     *
     * @param  $start
     * @param  $end
     * @return string
     */
    public function export(Carbon $start, Carbon $end)
    {
        $client_investments = $this->getInvestments($start, $end);
        $client_investments_grouped_by_product = $client_investments->groupBy('product_id');

        $file_name = 'Closing Balances for Partners Export';

        $start_date_next = $start->addDay();
        $end_date_next = $end->addDay();

        $fms = FundManager::all();

        Excel::create(
            $file_name,
            function ($excel) use (
                $client_investments_grouped_by_product,
                $start,
                $end,
                $start_date_next,
                $end_date_next,
                $fms
            ) {
                foreach ($client_investments_grouped_by_product as $product_id => $investments) {
                    $product = Product::findOrFail($product_id);
                    $excel->sheet(
                        $product->name,
                        function ($sheet) use ($investments, $start_date_next, $end_date_next) {
                            $investments_grouped_by_client = $investments->groupBy('client_id');
                            $sheet->loadView(
                                'exports.closing_balances',
                                ['investments_grouped_by_client' => $investments_grouped_by_client,
                                    'end_date' => $this->end, 'start_date' => $this->start,
                                    'start_date_next' => $start_date_next, 'end_date_next' => $end_date_next]
                            );
                        }
                    );
                }

                foreach ($fms as $fm) {
                    $portfolio_investments_grouped_by_investor = $this->getPortfolio($fm, $start, $end)
                        ->groupBy('portfolio_investor_id');
                    $excel->sheet(
                        'Portfolio - ' . $fm->name,
                        function ($sheet) use (
                            $portfolio_investments_grouped_by_investor,
                            $start_date_next,
                            $end_date_next,
                            $fms
                        ) {
                            $sheet->loadView(
                                'exports.portfolio_closing_balances',
                                [
                                    'portfolio_investments_grouped_by_investor' =>
                                        $portfolio_investments_grouped_by_investor,
                                    'end_date' => $this->end, 'start_date' => $this->start,
                                    'start_date_next' => $start_date_next, 'end_date_next' => $end_date_next
                                ]
                            );
                        }
                    );
                }
            }
        )->store('xlsx');

        return storage_path() . '/exports/' . $file_name . '.xlsx';
    }

    /**
     * @param $start
     * @param $end
     * @return Collection
     */
    private function getInvestments(Carbon $start, Carbon $end)
    {
        return ClientInvestment::activeBetweenDates($start, $end)->get();
    }

    /**
     * @param FundManager $fm
     * @param $start
     * @param $end
     * @return mixed
     */
    private function getPortfolio(FundManager $fm, Carbon $start, Carbon $end)
    {
        return DepositHolding::fundManager($fm)->activeBetweenDates($start, $end)->get();
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     */
    private function mailResult($file_path, Carbon $start, Carbon $end, User $user)
    {
        $mailer = new GeneralMailer();

        $mailer->to($user->email);
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Closing Balances for Partners Export');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail(
            'Here is the Closing Balances for Partners Export that you requested between ' .
            $start->toFormattedDateString() . ' and ' . $end->toFormattedDateString()
        );
    }
}
