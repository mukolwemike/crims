<?php

namespace App\Jobs\Investments;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PenaltyDeductions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $start;

    protected $end;

    protected $user;

    /**
     * Create a new job instance.
     *
     * PenaltyDeductions constructor.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     */
    public function __construct(Carbon $start, Carbon $end, User $user)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $deductions = ClientInvestmentWithdrawal::between($this->start, $this->end)
            ->where('type_id', 3)
            ->get()
            ->map(function (ClientInvestmentWithdrawal $withdrawal) {
                $out = new EmptyModel();

                $w = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
                    ->where('date', $withdrawal->date)
                    ->where('approval_id', $withdrawal->approval_id)
                    ->latest()
                    ->first();

                $amt = $w ? $w->amount : 0;

                $out->{'Client Name'} = ClientPresenter::presentFullNames($withdrawal->investment->client->id);
                $out->{'Client Code'} = $withdrawal->investment->client->client_code;
                $out->{'Principal'} = $withdrawal->investment->amount;
                $out->{'Withdrawn Amt'} = $amt;
                $out->{'Value Date'} = $withdrawal->investment->invested_date;
                $out->{'Maturity Date'} = $withdrawal->investment->maturity_date;
                $out->{'Penalty Amount'} = $withdrawal->amount;
                $out->{'Penalty Date'} = $withdrawal->date;
                $out->{'Product'} = $withdrawal->investment->product->name;
                $out->{'Description'} = $withdrawal->description;
                $out->{'Narration'} = $withdrawal->narration;

                return $out;
            });

        $file_name = 'Penalty Deductions - ' . $this->start->copy()->toFormattedDateString() . ' to ' .
            $this->end->copy()->toFormattedDateString();

        Excel::fromModel($file_name, $deductions)->store('xlsx');

        $this->mailResult($file_name, $this->start, $this->end, $this->user);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $start
     * @param $end
     * @param User|null $user
     */
    private function mailResult($file_name, Carbon $start, Carbon $end, User $user)
    {
        Mailer::compose()
            ->to($user->email)
            ->bcc(config('system.administrators'))
            ->subject('Penalty Deductions Report - ' . $start->copy()->toFormattedDateString() . ' to ' .
                $end->copy()->toFormattedDateString())
            ->text('Please find attached the requested report of Penalty Deductions from ' .
                $start->copy()->toFormattedDateString() . ' to ' . $end->copy()->toFormattedDateString())
            ->excel([$file_name])
            ->send();
    }
}
