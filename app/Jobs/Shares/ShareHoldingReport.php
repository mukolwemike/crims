<?php

namespace App\Jobs\Shares;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ShareHoldingReport implements ShouldQueue
{
    use ExcelMailer, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $entity;

    /**
     * ShareHoldingReport constructor.
     *
     * @param SharesEntity $entity
     * @param User $user
     */
    public function __construct(SharesEntity $entity, User $user)
    {
        $this->entity = $entity;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $entity = $this->entity;

        $shareHolders = ShareHolder::where('entity_id', $entity->id)
            ->get()
            ->map(
                function (ShareHolder $holder) {
                    $out = new EmptyModel();

                    $client = $holder->client;

                    $out->{'Name'} = ClientPresenter::presentJointFullNames($holder->client_id);
                    $out->{'Email'} = $client->contact->email;
                    $out->{'Phone'} = $client->contact->phone;
                    $out->{'Number'} = $holder->number;
                    $out->{'Date Joined'} = $holder->created_at->toDateString();
                    $out->{'Shares'} = (float)$holder->currentShares();

                    return $out;
                }
            );

        $fname = 'Share holders list';

        Excel::fromModel($fname, $shareHolders)->store('xlsx');

        $this->sendExcel(
            $fname,
            'Please find the attached list of shareholders for ' . $entity->name,
            $fname,
            [$this->user->email]
        );
    }
}
