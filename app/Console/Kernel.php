<?php namespace App\Console;

use App\Console\Commands\CassandraTest;
use App\Console\Commands\Clients\ActiveClientsExports;
use App\Console\Commands\Clients\CleanupClients;
use App\Console\Commands\Clients\ClientCommissionPayments;
use App\Console\Commands\Clients\ClientPaymentInstruction;
use App\Console\Commands\Clients\ClientsComplianceExport;
use App\Console\Commands\Clients\ClientSummaryExport;
use App\Console\Commands\Clients\InterClientCustodialTransfer;
use App\Console\Commands\Clients\IprsValidationCommand;
use App\Console\Commands\Clients\NotificationForNonCompliance;
use App\Console\Commands\Clients\RiskyClientsExport;
use App\Console\Commands\Clients\UnassignedClientPayments;
use App\Console\Commands\Clients\UpdateClientDocuments;
use App\Console\Commands\Clients\UpdateStaffClients;
use App\Console\Commands\Custodial\CurrencyReport;
use App\Console\Commands\Custodial\CustodialAccountTransfers;
use App\Console\Commands\Custodial\ReconcileAccounts;
use App\Console\Commands\CustomReportTesting;
use App\Console\Commands\Data\CentralBankDataUpdate;
use App\Console\Commands\Debug;
use App\Console\Commands\FAReports;
use App\Console\Commands\FAs\AwardValentineCommission;
use App\Console\Commands\FAs\CheckWithdrawnCommissionSchedules;
use App\Console\Commands\FAs\CommissionClawbackSummary;
use App\Console\Commands\FAs\ExportCombinedCommissionSummary;
use App\Console\Commands\FAs\FaCommissionBreakdownReport;
use App\Console\Commands\FAs\FaCommissionHistorySummary;
use App\Console\Commands\FAs\IfaClientsSummary;
use App\Console\Commands\FAs\OverrideSummary;
use App\Console\Commands\FAs\PensionCommissionSummary;
use App\Console\Commands\FAs\UpdateAdditionalCommission;
use App\Console\Commands\FAs\UpdateCommissionOverrides;
use App\Console\Commands\FAs\VerifyCommissionOverrides;
use App\Console\Commands\FAs\YearCommissionsExport;
use App\Console\Commands\FAs\YearProduction;
use App\Console\Commands\Finance\InflowsExport;
use App\Console\Commands\Finance\InterestExpenseExport;
use App\Console\Commands\Finance\OutflowsExport;
use App\Console\Commands\Finance\TransfersExport;
use App\Console\Commands\Finance\WithholdingTaxExport;
use App\Console\Commands\Investments\AdditionalRolloverCommissionAward;
use App\Console\Commands\Investments\AwardInvestmentEditCommission;
use App\Console\Commands\Investments\BackdateInvestmentTransactions;
use App\Console\Commands\Investments\ClientMaturitiesExport;
use App\Console\Commands\Investments\ClientsWithholdingTaxReport;
use App\Console\Commands\Investments\FundManagerMonthlyNetFlowReport;
use App\Console\Commands\Investments\InvestmentAnalyticsReport;
use App\Console\Commands\Investments\InvestmentInflowsReport;
use App\Console\Commands\Investments\InvestmentMaturityRolloverSummary;
use App\Console\Commands\Investments\OverdueInvestmentPaymentScheduleSummary;
use App\Console\Commands\Investments\RolloverCommissionAward;
use App\Console\Commands\Investments\WithholdingTaxItaxReport;
use App\Console\Commands\Payments\ExpressMMFWithdrawalPayments;
use App\Console\Commands\Payments\ExpressSPWithdrawalPayments;
use App\Console\Commands\PaymentSystem\AutomaticWithdrawalsSummary;
use App\Console\Commands\PaymentSystem\PaymentTransactionSummary;
use App\Console\Commands\Pensions\SyncPensionContributions;
use App\Console\Commands\Pensions\SyncPensionMembers;
use App\Console\Commands\Portfolio\CustodialAccountReport;
use App\Console\Commands\Portfolio\DepositHoldingsExport;
use App\Console\Commands\Portfolio\FundManagementSummary;
use App\Console\Commands\Portfolio\FundPricingReport;
use App\Console\Commands\Portfolio\FundValuationReport;
use App\Console\Commands\Portfolio\InvestorValuation;
use App\Console\Commands\Portfolio\LoanAmortizationSchedule;
use App\Console\Commands\Portfolio\OrderComplianceChecks;
use App\Console\Commands\Portfolio\SummaryValuationReport;
use App\Console\Commands\Portfolio\YearlyPortfolioInvestmentsReport;
use App\Console\Commands\RealEstate\AccrueRealEstateInterest;
use App\Console\Commands\RealEstate\InflowsReport;
use App\Console\Commands\RealEstate\RealEstateCommissionSummary;
use App\Console\Commands\RealEstate\RealEstateHoldingMonthlySummary;
use App\Console\Commands\RealEstate\RealEstateHoldingSummary;
use App\Console\Commands\RealEstate\RealestateStatementsExport;
use App\Console\Commands\RealEstate\UnitsWithLoosWithoutSA;
use App\Console\Commands\RealEstate\UpcomingRealEstatePayments;
use App\Console\Commands\RealEstate\UpdateLastRealEstatePaymentSchedule;
use App\Console\Commands\SendAuditConfirmations;
use App\Console\Commands\SendRealEstateAuditConfirmations;
use App\Console\Commands\Shares\CalculateSharePrice;
use App\Console\Commands\Statements\AutomaticStatementCampaigns;
use App\Console\Commands\Statements\InvestmentsStatementProcessor;
use App\Console\Commands\Statements\RealEstateStatementProcessor;
use App\Console\Commands\Statements\SharesStatementProcessor;
use App\Console\Commands\Statements\UnitizationStatementProcessor;
use App\Console\Commands\System\ClientMovementAudit;
use App\Console\Commands\System\ClientTenorReport;
use App\Console\Commands\System\CrmContactSync;
use App\Console\Commands\System\CrmUSSDLeadsSync;
use App\Console\Commands\System\DashboardUpdates;
use App\Console\Commands\System\PortfolioMovementAudit;
use App\Console\Commands\System\ProcessStandingOrders;
use App\Console\Commands\System\RandomExport;
use App\Console\Commands\System\TenorRangeExport;
use App\Console\Commands\System\UserAccessExport;
use App\Console\Commands\System\YearlyPersistencySummary;
use App\Console\Commands\Temporary\LoyaltyPointsReport;
use App\Console\Commands\Temporary\UpdateLoyaltyRateInvestmentsPurchases;
use App\Console\Commands\Unitization\AccountsCreatedSummary;
use App\Console\Commands\Unitization\AwardUnitizationCommissions;
use App\Console\Commands\Unitization\CancelOldUnitFundTransactions;
use App\Console\Commands\Unitization\IncompleteApplicationsReminder;
use App\Console\Commands\Unitization\InterestExpenseReport;
use App\Console\Commands\Unitization\IntraPaymentsSummary;
use App\Console\Commands\Unitization\PerformanceAttributionSummary;
use App\Console\Commands\Unitization\RecordDailyUnitFundPerformance;
use App\Console\Commands\Unitization\UnitFundBalanceSummary;
use App\Console\Commands\Unitization\UnitFundClientMovementAudit;
use App\Console\Commands\Unitization\UnitFundPurchasesReport;
use App\Console\Commands\Unitization\UnitFundTransactionSummary;
use App\Console\Commands\Unitization\UpdateUnitFundCommission;
use App\Console\Commands\Unitization\UpdateUnitFundInterestPaymentDate;
use App\Console\Commands\Unitization\UpdateUnitSalesWitholdingTax;
use App\Console\Commands\UpdateUnitFundInstructionsApprovals;
use App\Console\Commands\USSD\DigitalTransactionSummary;
use App\Console\Commands\Utilities\ExpressUTFUtilityPayments;
use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Project;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = array(
        \App\Console\Commands\FAs\MergeFARecords::class,
        \App\Console\Commands\FAs\GetFADetailsFromHR::class,
        \App\Console\Commands\Investments\WithholdingTaxReport::class,
        \App\Console\Commands\Investments\InvestmentsMonthlyMaturities::class,
        \App\Console\Commands\Investments\CommissionSummary::class,
        \App\Console\Commands\Investments\DailyWithdrawalReport::class,
        \App\Console\Commands\Investments\MonthlyWithdrawalReport::class,
        \App\Console\Commands\Investments\InvestmentScheduledActions::class,
        \App\Console\Commands\Investments\ExportClientStatements::class,
        \App\Console\Commands\Investments\PortfolioMaturityReport::class,
        \App\Console\Commands\Investments\ClientMaturityNotification::class,
        \App\Console\Commands\Investments\MaturityNotification::class,
        \App\Console\Commands\Investments\SendWeeklyReportsOnSaturday::class,
        \App\Console\Commands\Investments\CheckInvestmentsOnCallMaturity::class,
        \App\Console\Commands\Investments\ResidualIncome::class,
        \App\Console\Commands\Investments\ClientSummaryExport::class,
        \App\Console\Commands\Investments\InvestmentsExportCommand::class,
        \App\Console\Commands\Investments\InterestExpenseReport::class,
        \App\Console\Commands\Investments\InvestmentAudit::class,
        \App\Console\Commands\Investments\WithdrawalsReport::class,
        \App\Console\Commands\Investments\InvestmentsChurnReport::class,
        \App\Console\Commands\Clients\ClientsWithBirthdays::class,
        \App\Console\Commands\Clients\DailyBusinessConfirmationsReport::class,
        \App\Console\Commands\Clients\HappyBirthday::class,
        \App\Console\Commands\SendClientActivationEmails::class,
        \App\Console\Commands\Shares\CreateShareholders::class,
        \App\Console\Commands\Shares\ShareOrdersExpiryNotification::class,
        \App\Console\Commands\RealEstate\WeeklyLooReport::class,
        \App\Console\Commands\RealEstate\DepositPaymentExpiryNotification::class,
        \App\Console\Commands\RealEstate\PaymentSchedulesCheck::class,
        \App\Console\Commands\RealEstate\RealEstatePaymentSummary::class,
        \App\Console\Commands\RealEstate\LOOsAndSAsFollowUp::class,
        \App\Console\Commands\RealEstate\LOOExpiryNotification::class,
        \App\Console\Commands\RealEstate\LooUploadedNotification::class,
        \App\Console\Commands\RealEstate\RealEstatePenaltyForOverduePayments::class,
        \App\Console\Commands\RealEstate\SendPaymentRemindersForUpcomingRealEstatePayments::class,
        \App\Console\Commands\RealEstate\SendLOOsSentToClientsDailyReport::class,
        \App\Console\Commands\RealEstate\LooSalesAgreementReporting::class,
        \App\Console\Commands\RealEstate\RECommissionUpdate::class,
        \App\Console\Commands\RealEstate\SendTrancheReportsForEachProject::class,
        \App\Console\Commands\RealEstate\CashFlowGenerator::class,
        \App\Console\Commands\RealEstate\RealEstateUnitReservationSummary::class,
        \App\Console\Commands\RealEstate\SalesAgreementAndLOODateReconciliation::class,
        \App\Console\Commands\Custodial\DailyInflowsReport::class,
        \App\Console\Commands\Custodial\MonthlyInflowsReport::class,
        \App\Console\Commands\Custodial\ClientBalances::class,
        \App\Console\Commands\Coop\SendCoopClientsReport::class,
        \App\Console\Commands\System\DailySystemMaintenanceCommand::class,
        \App\Console\Commands\System\UUIDGenerator::class,
        \App\Console\Commands\System\SSOAccessRefresh::class,
        \App\Console\Commands\Custodial\ExchangeRateFetch::class,
        \App\Console\Commands\Investments\AssetLiabilityReconciliation::class,
        \App\Console\Commands\System\WeeklySystemReport::class,
        Debug::class,
        \App\Console\Commands\YearStatements::class,
        RecordDailyUnitFundPerformance::class,
        \App\Console\Commands\Investments\InterestPaymentSchedulesCheck::class,
        \App\Console\Commands\System\DailyElasticSearchReindex::class,
        \App\Console\Commands\Clients\SendClientToCrm::class,
        \App\Console\Commands\Clients\ClientDataExport::class,
        UnassignedClientPayments::class,
        InflowsReport::class,
        InvestmentInflowsReport::class,
        YearProduction::class,
        FundManagerMonthlyNetFlowReport::class,
        YearlyPortfolioInvestmentsReport::class,
        InvestorValuation::class,
        CurrencyReport::class,
        FAReports::class,
        ClientSummaryExport::class,
        CleanupClients::class,
        CustodialAccountReport::class,
        CustodialAccountTransfers::class,
        ClientMovementAudit::class,
        PortfolioMovementAudit::class,
        ClientTenorReport::class,
        YearlyPersistencySummary::class,
        TenorRangeExport::class,
        CrmContactSync::class,
        UnitsWithLoosWithoutSA::class,
        InvestmentAnalyticsReport::class,
        ClientsComplianceExport::class,
        NotificationForNonCompliance::class,
        UnitFundTransactionSummary::class,
        UnitFundBalanceSummary::class,
        ProcessStandingOrders::class,
        FundValuationReport::class,
        ActiveClientsExports::class,
        DashboardUpdates::class,
        CommissionClawbackSummary::class,
        PerformanceAttributionSummary::class,
        IfaClientsSummary::class,
        ClientPaymentInstruction::class,
        UpdateLastRealEstatePaymentSchedule::class,
        LoanAmortizationSchedule::class,
        ClientCommissionPayments::class,
        InterClientCustodialTransfer::class,
        FundPricingReport::class,
        SummaryValuationReport::class,
        UpdateClientDocuments::class,
        BackdateInvestmentTransactions::class,
        ClientMaturitiesExport::class,
        ClientsWithholdingTaxReport::class,
        RolloverCommissionAward::class,
        AwardUnitizationCommissions::class,
        DepositHoldingsExport::class,
        RiskyClientsExport::class,
        ExpressSPWithdrawalPayments::class,
        ExpressMMFWithdrawalPayments::class,
        FaCommissionHistorySummary::class,
        UpcomingRealEstatePayments::class,
        DepositHoldingsExport::class,
        CustomReportTesting::class,
        UnitFundPurchasesReport::class,
        InvestmentMaturityRolloverSummary::class,
        ExportCombinedCommissionSummary::class,
        InflowsExport::class,
        OutflowsExport::class,
        TransfersExport::class,
        InterestExpenseExport::class,
        WithholdingTaxExport::class,
        ExportCombinedCommissionSummary::class,
        AdditionalRolloverCommissionAward::class,
        InterestExpenseReport::class,
        AwardInvestmentEditCommission::class,
        UnitFundClientMovementAudit::class,
        UpdateCommissionOverrides::class,
        UserAccessExport::class,
        AwardValentineCommission::class,
        RealestateStatementsExport::class,
        SendAuditConfirmations::class,
        SendRealEstateAuditConfirmations::class,
        CheckWithdrawnCommissionSchedules::class,
        FundManagementSummary::class,
        PaymentTransactionSummary::class,
        OrderComplianceChecks::class,
        CentralBankDataUpdate::class,
        VerifyCommissionOverrides::class,
        UpdateStaffClients::class,
        RandomExport::class,
        CassandraTest::class,
        RealEstateCommissionSummary::class,
        IprsValidationCommand::class,
        YearCommissionsExport::class,
        ReconcileAccounts::class,
        FaCommissionBreakdownReport::class,
        AccrueRealEstateInterest::class,
        OverrideSummary::class,
        OverdueInvestmentPaymentScheduleSummary::class,
        PensionCommissionSummary::class,
        UpdateUnitFundInterestPaymentDate::class,
        DigitalTransactionSummary::class,
        IncompleteApplicationsReminder::class,
        RealEstateHoldingSummary::class,
        RealEstateHoldingMonthlySummary::class,
        IntraPaymentsSummary::class,
        AutomaticWithdrawalsSummary::class,
        UpdateUnitFundInstructionsApprovals::class,
        SyncPensionContributions::class,
        SyncPensionMembers::class,
        CancelOldUnitFundTransactions::class,
        UpdateUnitSalesWitholdingTax::class,
        WithholdingTaxItaxReport::class,
        UpdateUnitFundCommission::class,
        UpdateLoyaltyRateInvestmentsPurchases::class,
        UpdateAdditionalCommission::class,
        UpdateAdditionalCommission::class,
        AutomaticStatementCampaigns::class,
        InvestmentsStatementProcessor::class,
        RealEstateStatementProcessor::class,
        SharesStatementProcessor::class,
        UnitizationStatementProcessor::class,
        ExpressUTFUtilityPayments::class,
        UnitizationStatementProcessor::class,
        LoyaltyPointsReport::class,
        AccountsCreatedSummary::class,
        CrmUSSDLeadsSync::class
    );


    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->statements($schedule);
        $this->client($schedule);
        $this->coopSchedule($schedule);
        $this->custodial($schedule);
        $this->fASchedule($schedule);
        $this->investments($schedule);
        $this->realEstate($schedule);
        $this->shares($schedule);
        $this->system($schedule);
        $this->unitization($schedule);
        $this->complianceSchedule($schedule);
        $this->calculateSharePrice($schedule);
    }

    private function calculateSharePrice(Schedule $schedule)
    {
        $end_month = Carbon::today()->daysInMonth;

        $schedule->command(CalculateSharePrice::class)->monthlyOn($end_month, '08:00');
    }

    /*
     * NonCompliance Client Reminders
     *
     * @param Schedule $schedule
     */
    private function complianceSchedule(Schedule $schedule)
    {
        $schedule->command(NotificationForNonCompliance::class)->daily()->when(function () {
            $days = [1, 16];
            $today = Carbon::today();
            return in_array($today->day, $days);
        });
    }

    /**
     * Clients Command Schedules
     *
     * @param Schedule $schedule
     */
    private function client(Schedule $schedule)
    {
        $schedule->command('cytonn:daily-business-confirmations-report')->daily()->at('00:10'); //00:01
        $schedule->command('cytonn:birthdays')->daily()->at('08:10'); //8:00
        $schedule->command('crims:update_staff_clients')->dailyAt('00:45');
        $schedule->command(IprsValidationCommand::class)->everyTenMinutes();
    }

    /**
     * Coop Command Schedules
     *
     * @param Schedule $schedule
     */
    private function coopSchedule(Schedule $schedule)
    {
        $end_month = Carbon::today()->daysInMonth;

        $schedule->command('coop:clients-report')->monthlyOn($end_month, '08:10'); //8:00
    }

    /**
     * Custodial Command Schedules
     *
     * @param Schedule $schedule
     */
    private function custodial(Schedule $schedule)//check here
    {
        $eleven = Carbon::now()->hour >= 23 ? 'true' : 'false';

        $schedule->command('custody:client-balances')->daily()->at('04:40');//check permissions
        $schedule->command('custody:daily-inflows')->daily()->at('00:10'); //00:00
        $schedule->command("custody:fetch-rates --force $eleven")->hourlyAt(23);
        $schedule->command("clients:unassigned_client_payments")->dailyAt('00:30');
        $schedule->command(ReconcileAccounts::class)->everyFiveMinutes();
    }

    /**
     * FAs Command Schedules
     *
     * @param Schedule $schedule
     */
    private function fASchedule(Schedule $schedule)
    {
        $schedule->command('fa:get-hr-details')->hourlyAt(10); //hourly();

        $schedule->command('fa:get-hr-details 1')->dailyAt('00:50');

        try {
            $dates = (new BulkCommissionPayment())->overrideCutoffDates(Carbon::now());
            $start = Carbon::parse($dates['start'])->toDateString();


            $schedule->command("crims:verify_commission_overrides 0 1 0 $start")->hourlyAt(10); //hourly();
            $schedule->command("crims:verify_commission_overrides 0 1 0 $start")->dailyAt("02:40");
        } catch (\Exception $e) {
        }
    }

    /**
     * Investments Command Schedules
     *
     * @param Schedule $schedule
     */
    private function investments(Schedule $schedule)
    {
        $schedule->command('investments:on-call')->daily()->at('03:10'); //3:00
        $schedule->command('investments:send-maturity-notifications --target=client')->hourly()
            ->when(function () {
                $hr = Carbon::now()->hour;
                return ($hr >= 9) && ($hr <= 18);
            });

        $schedule->command('investments:send-maturity-notifications --target=fa')->mondays()->at('10:10');
        $schedule->command('investments:withdrawal-report')->daily()->at('00:10'); //00:01

        $schedule->command('investments:monthly-withdrawal-and-rollover-report')
            ->monthlyOn(1, '00:10'); //00:01

        $schedule->command('investments:audit')->daily()->at('04:10');//access violation // 4:00
        $schedule->command('cytonn:investment-scheduled-actions')->hourlyAt(10); //hourly();
        $schedule->command('inv:monthly-maturities')->mondays()->at('09:20');

        $schedule->command('cytonn:maturity')->daily()->at('04:30');
        $schedule->command('inv:portfolio-maturities')->daily()->at('05:00'); // 5:00

        $today = Carbon::today();
        $schedule->command("investments:residual-income {$today->toDateString()} --currency=KES")->monthly();
        $schedule->command('cytonn:saturday-report')->saturdays()->at('14:10');


        $schedule->command('investments:interest_payment_schedules_check')->hourlyAt(10); //hourly();
        $schedule->command('crims:ifas_client_summary')->monthly();

        $schedule->command('portfolio:order_compliance_checks')->daily()->at('08:10'); //8:00

        $schedule->command('crims:overdue_investment_payment_schedules')->dailyAt("00:35");
    }

    /**
     * Statements command schedules
     *
     * @param Schedule $schedule
     */
    private function statements(Schedule $schedule)
    {
        if (Carbon::today()->isLastOfMonth() || (Carbon::today()->day === 1)) {
            $schedule->command(InvestmentsStatementProcessor::class)->everyMinute()->runInBackground();
            $schedule->command(RealEstateStatementProcessor::class)->everyMinute()->runInBackground();
            $schedule->command(SharesStatementProcessor::class)->everyMinute()->runInBackground();
            $schedule->command(UnitizationStatementProcessor::class)->everyMinute()->runInBackground();
        }

        $schedule->command(AutomaticStatementCampaigns::class)->dailyAt('20:10')->when(function () { //20:00
            return Carbon::today()->isLastOfMonth();
        });
    }

    /**
     * RealEstate Command Schedules
     *
     * @param Schedule $schedule
     */
    private function realEstate(Schedule $schedule)
    {
        //schedule argument do not exist
        $schedule->command('re:deposit-payment-expiry-notifications')->monthly();
        $schedule->command('re:loo-expiry-notification')->monthly();

        $schedule->command('re:loo-sa-reporting')->daily()->at('20:10'); //20:00
        $schedule->command('re:loos-sas-follow-up')->daily()->at('18:10'); //18:00
        $schedule->command('loo:send-notifications')->hourlyAt(10); //hourly();

        $schedule->command('re:check-payment-schedules')->everyTenMinutes();

        $schedule->command('re:penalty-for-overdue-payments')->mondays()->at('06:10'); //06:00
        $schedule->command('crims:update_last_real_estate_payment_schedules')->hourlyAt(10); //hourly();

        $schedule->command('re:loo-report')->daily()->at('22:10'); // 22:00
        $schedule->command('re:upcoming-payments-reminders')->daily()->at('07:15');

        $schedule->command('re:tranche-reports')->fridays()->at('17:10');
        $schedule->command('re:weekly-loo-report')->saturdays()->at('14:10');//collection // 14:00

        $schedule->command('re:deposit-payment-expiry-notifications daily')->daily()->at('06:10'); //06:00
        $schedule->command('re:deposit-payment-expiry-notifications monthly')->monthly()->saturdays()
            ->at('14:10');//argument does not exist //14:00

        $schedule->command('re:reservations-summary ' . Carbon::today()->startOfMonth()->toDateString() .
            ' ' . Carbon::today()->endOfMonth()->toDateString() . ' pm@cytonn.com')->monthlyOn(1, '2:30');
        $schedule->command('re:reservations-summary ' . Carbon::today()->subDays(7)->toDateString() .
            ' ' . Carbon::today()->toDateString() . ' pm@cytonn.com')
            ->saturdays()->at('09:10'); //09:00

        $schedule->command('re:update-commissions')->dailyAt('01:30');

        $schedule->command('re:sa-loo-dates-reconciliation')->dailyAt('23:10'); //23:00

        $schedule->command('re:pending-sales-agreements')->fridays()->at('16:10'); //16:00

        $this->cashFlows($schedule);
    }

    private function cashFlows(Schedule $schedule)
    {
        if (Carbon::now()->hour == 15) {
            try {
                $projects = Project::pluck('id');

                foreach ($projects as $project) {
                    $schedule->command(
                        "re:cashflows $project null operations@cytonn.com,pm@cytonn.com,legal@cytonn.com"
                    )->fridays()->at('15:30')->runInBackground();
                }
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * Shares Command Schedules
     *
     * @param Schedule $schedule
     */
    private function shares(Schedule $schedule)
    {
        $schedule->command('shares:migrate')->everyFiveMinutes();
        $schedule->command('shares:orders-expiry-notifications')->daily()->at('05:10'); //05:00
    }

    /**
     * System Command Schedules
     *
     * @param Schedule $schedule
     */
    private function system(Schedule $schedule)
    {
        $schedule->command('maintenance:run')->daily()->at('03:10')->runInBackground(); //03:00
        $schedule->command('uuid:generate')->hourlyAt(23)->runInBackground();
        $schedule->command('system:weekly-report')->mondays()->at('07:10')->runInBackground(); //7:00
        $schedule->command('system:weekly-report --start=yesterday --end=yesterday')->dailyAt('00:05');
        $schedule->command('crims:users_sso_access')->dailyAt('02:10'); //02:00
        $schedule->command('system:refresh_elasticsearch_index')->dailyAt('02:30');
        $schedule->command(ProcessStandingOrders::class)->dailyAt('00:10');
        $schedule->command(DashboardUpdates::class)->hourlyAt(10) //2
            ->runInBackground()
            ->withoutOverlapping();

        $schedule->command(CentralBankDataUpdate::class)->dailyAt('08:10'); //8:00
        $schedule->command('cytonn:send-client-activations')->everyTenMinutes();
        $schedule->command('crims:clean-phones')->dailyAt('08:10'); //08:00
        $schedule->command(CancelOldUnitFundTransactions::class)->daily()->at('08:10'); // 08:00
        $schedule->command(IncompleteApplicationsReminder::class)->daily()->at('08:10'); // 08:00
        $schedule->command(ExpressUTFUtilityPayments::class)->everyMinute()
            ->withoutOverlapping(30)->runInBackground();
        $schedule->command(CrmUSSDLeadsSync::class)->everyThirtyMinutes()->runInBackground();
        $schedule->command(UpdateLoyaltyRateInvestmentsPurchases::class)->everyThirtyMinutes()->runInBackground();
    }

    /**
     * Unitization Command Schedules
     *
     * @param Schedule $schedule
     */
    private function unitization(Schedule $schedule)
    {
        $schedule->command('unitization:record-daily-performance')->daily()->at('23:30'); // 23:40
        $schedule->command('crims:update_unit_fund_interest_date')->monthlyOn(5, '00:30'); // 00:40
        $schedule->command(ExpressMMFWithdrawalPayments::class)->everyMinute()
            ->withoutOverlapping(30)
            ->runInBackground();

        $this->cypesa($schedule);
    }

    private function cypesa(Schedule $schedule)
    {
        $today = Carbon::today()->subDay()->endOfDay();
        $isMonday = Carbon::now()->isMonday();
        $morning = '07:10'; //07:00

        $yesterday = $isMonday ?
            $today->copy()->subDays(3)->startOfDay()->toDateTimeString()
            : $today->copy()->startOfDay()->toDateTimeString();

        $schedule->command(PaymentTransactionSummary::class, [
            $yesterday,
            $today,
            0,
            "smbitiru@cytonn.com,asudi@cytonn.com,awanjohi@cytonn.com"
        ])->dailyAt($morning);

        $schedule->command(AutomaticWithdrawalsSummary::class, [
            $yesterday,
            $today,
            0,
            "smbitiru@cytonn.com,asudi@cytonn.com,awanjohi@cytonn.com"
        ])->dailyAt($morning);

        $schedule->command(IntraPaymentsSummary::class, [
            $yesterday,
            $today,
            0,
            "smbitiru@cytonn.com,asudi@cytonn.com,awanjohi@cytonn.com"
        ])->dailyAt($morning);

        //Send report at six pm in the evening
        $sixToday = Carbon::today()->startOfDay()->addHours(18);

        $sixPrevious = $isMonday ?
            $sixToday->copy()->subDays(3)->toDateTimeString()
            : $sixToday->copy()->subDay(1)->toDateTimeString();

        $schedule->command(PaymentTransactionSummary::class, [
            $sixPrevious,
            $sixToday,
            0,
            "smbitiru@cytonn.com,asudi@cytonn.com,awanjohi@cytonn.com"
        ])->dailyAt('18:10'); //18:00

        $schedule->command(AutomaticWithdrawalsSummary::class, [
            $sixPrevious,
            $sixToday,
            0,
            "smbitiru@cytonn.com,asudi@cytonn.com,awanjohi@cytonn.com"
        ])->dailyAt('18:10'); //18:00

        $schedule->command(IntraPaymentsSummary::class, [
            $sixPrevious,
            $sixToday,
            0,
            "smbitiru@cytonn.com,asudi@cytonn.com,awanjohi@cytonn.com"
        ])->dailyAt('18:10'); //18:00

        $schedule->command(AccountsCreatedSummary::class, [
            $sixPrevious,
            $sixToday,
            0,
            "smbitiru@cytonn.com,asudi@cytonn.com,awanjohi@cytonn.com"
        ])->dailyAt('18:10'); //18:00
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        include base_path('routes/console.php');
    }
}
