<?php

namespace App\Console\Commands;

use App\Cytonn\Models\AccessLog;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cassandra;
use \Cassandra\Cluster\Builder;
use Illuminate\Console\Command;
use Webpatser\Uuid\Uuid;

class CassandraTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cassandra:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->test();
    }

    private function test()
    {
    }

    /**
     *
     */
    private function createSchema()
    {

        \Schema::create('test_users', function ($table) {
            $table->int('id');
            $table->text('name');
            $table->text('email');
            $table->text('password');
            $table->text('remember_token');
            $table->setCollection('phn', 'bigint');
            $table->listCollection('hobbies', 'text');
            $table->mapCollection('friends', 'text', 'text');
            $table->primary(['id']);
        });
    }

    private function testConnection()
    {
        /*
         * @var Builder
         */
        $cluster   = Cassandra::cluster()
            ->withCredentials('cassandra', 'Cassandra@pass')
            ->withContactPoints('172.31.38.218', '172.31.33.60', '172.31.46.159')
            ->withPort(9042)
            ->build();


        $keyspace  = 'system';
        $session   = $cluster->connect($keyspace);        // create session, optionally scoped to a keyspace
        $statement = new Cassandra\SimpleStatement(       // also supports prepared and batch statements
            'SELECT * FROM system.peers'
        );
        $future    = $session->executeAsync($statement);  // fully asynchronous and easy parallel execution
        $result    = $future->get();                      // wait for the result, with an optional timeout

        foreach ($result as $row) {                       // results and rows implement
            // Iterator, Countable and ArrayAccess
            dump($row);
        }

        return;
    }
}
