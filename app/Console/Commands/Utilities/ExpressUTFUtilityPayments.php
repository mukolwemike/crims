<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Utilities;

use App\Console\Commands\Utilities\Base\AbstractUtilityPayment;
use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\Billing\UtilityBillingStatus;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Events\Utilities\UtilityPaymentHasBeenMade;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Handlers\CreateUnitFundSale;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Presenters\ClientPresenter;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

class ExpressUTFUtilityPayments extends AbstractUtilityPayment
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:express_utf_utility_payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process the unit fund utility payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return UtilityBillingInstructions[]|\Illuminate\Database\Eloquent\Collection|Builder[]|Collection
     */
    protected function getInstructions()
    {
        $cancelled = UtilityBillingStatus::where('slug', 'cancelled')->first()->id;

        return UtilityBillingInstructions::whereNotNull('unit_fund_id')
            ->whereNull('approval_id')
            ->whereNull('reversal_approval_id')
            ->whereNull('withdrawal_id')
            ->where('billing_status_id', '!=', $cancelled)
            ->whereNull('unit_fund_sale_id')->get();
    }

    /**
     * @param $instruction
     * @throws CrimsException
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    protected function executeInstruction($instruction)
    {
        /*
         * TRANSFER OPTION
         * 1. Create Unit Fund Sale
         * 2. Transfer Cash To Utilities
         * 3. Create Utilities Payment
         * 4. Submit Utility Payment Request to Payment System
         *
         * WITHDRAW OPTION
         * 1. Create unit fund sale
         * 2. Add utilities inflow
         * 3. Create Utilities Payment
         * 4. Submit Utility Payment Request to Payment System
         */
        $unitSale = $this->approveTransaction($instruction);

        $this->processInTransferOption($unitSale);

//        $this->processWithdrawalOption($unitSale);

        $utilityPayment = $this->createUtilitiesPayment($unitSale, $instruction);

        $this->submitUtilityPayment($instruction, $utilityPayment);

        event(new UtilityPaymentHasBeenMade($instruction->fresh()));
    }

    /**
     * @param UtilityBillingInstructions $instruction
     * @return mixed
     * @throws CrimsException
     * @throws Exception
     */
    private function approveTransaction(UtilityBillingInstructions $instruction)
    {
        $input = ["utility_instruction_id" => $instruction->id];

        $approval = CreateUnitFundSale::create(
            $instruction->client,
            null,
            getSystemUser(),
            $input
        );

        $approval->systemExecute();

        return $instruction->fresh()->sale;
    }

    /**
     * @param UnitFundSale $unitSale
     * @throws CrimsException
     */
    private function processInTransferOption(UnitFundSale $unitSale)
    {
        $this->transferCashToUtilities($unitSale->payment);
    }

    /**
     * @param UnitFundSale $unitSale
     * @throws CrimsException
     */
    private function processWithdrawalOption(UnitFundSale $unitSale)
    {
        $this->addUtilitiesInflow($unitSale);
    }

    /**
     * @param ClientPayment $payment
     * @throws CrimsException
     */
    private function transferCashToUtilities(ClientPayment $payment)
    {
        $fa = $payment->client->getLatestFA('units');

        $utilitiesFund = $this->getUtilitesFund();

        $approvalData = [
            'previous_fund_id' => $payment->unit_fund_id,
            'new_fund_id' => $utilitiesFund->id,
            'amount' => $payment->amount,
            'date' => $payment->date,
            'exchange_rate' => 1,
            'source_custodial_account_id' => $payment->fund->getDisbursementAccount('auto')->id,
            'custodial_account_id' => $utilitiesFund->custodial_account_id,
            'sending_client_id' => $payment->client_id,
            'receiving_client_id' => $payment->client_id,
            'commission_recipient_id' => $fa ? $fa->id : null
        ];

        $approval = ClientTransactionApproval::make(
            $payment->client_id,
            'transfer_cash_to_another_client',
            $approvalData
        );

        $approval->systemExecute();
    }

    /**
     * @param UnitFundSale $sale
     * @return ClientPayment
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    private function createUtilitiesPayment(UnitFundSale $sale, UtilityBillingInstructions $instruction)
    {
        $salePayment = $sale->payment;

        $description = "Utility Payment of {$salePayment->amount} to {$instruction->utility->name}";

        $payment = Payment::make(
            $salePayment->client,
            $salePayment->recipient,
            null,
            null,
            null,
            null,
            $salePayment->date,
            $salePayment->amount,
            'UP',
            $description,
            $this->getUtilitesFund()
        )->credit();

        $instruction->update([
            'utility_payment_id' => $payment->id
        ]);

        return $payment;
    }

    /**
     * @param UtilityBillingInstructions $instruction
     * @param ClientPayment $utilityPayment
     * @throws CrimsException
     * @throws Exception
     */
    private function submitUtilityPayment(UtilityBillingInstructions $instruction, ClientPayment $utilityPayment)
    {
        $requestDetails = [
            'reference' => $utilityPayment->id . '-' . $instruction->id,
            'service' => $instruction->utility->slug,
            'account_number' => $instruction->account_number,
            'amount' => abs($utilityPayment->amount),
            'names' => ClientPresenter::presentFullNameNoTitle($utilityPayment->client_id),
            'msisdn' => $instruction->msisdn,
            'instruction_id' => $instruction->id
        ];

        $integration = new CytonnPaymentSystem();

        $response = $integration->sendUtilityPayment($requestDetails);

        if ($response->status == "success") {
            $instruction->update([
                'billing_status_id' => UtilityBillingStatus::where('slug', 'submitted')->first()->id
            ]);
        } else {
            $ex = "Utility Payment submission failed... || " . $response->reason .
                \GuzzleHttp\json_encode($requestDetails);

            logRollbar($ex);

            throw new CrimsException($ex);
        }
    }

    /*
     * WITHDRAW OPTION
     */
    /**
     * @param BankInstruction $bankInstruction
     * @param UnitFundSale $unitSale
     * @throws CrimsException
     */
    private function addUtilitiesInflow(UnitFundSale $unitSale)
    {
        $inflowData = $this->prepareUtilityInflowData($unitSale);

        $approval = ClientTransactionApproval::create([
            'client_id' => $inflowData['client_id'],
            'transaction_type' => 'add_cash_to_account',
            'payload' => $inflowData,
            'sent_by' => getSystemUser()->id,
            'approved' => false,
            'approved_by' => null,
            'approved_on' => null,
            'awaiting_stage_id' => 6
        ]);

        $approval->systemExecute();

        $payment = ClientPayment::where('approval_id', $approval->id)->first();

        $payment->update([
            'description' => 'Client funds deposited for Utility bill payment'
        ]);

        $custodialTransaction = $payment->custodialTransaction;

        $custodialTransaction->update([
            'outgoing_reference' =>  $unitSale->client->client_code . '-' . $unitSale->payment->id
        ]);
    }

    /**
     * @param BankInstruction $bankInstruction
     * @param UnitFundSale $unitSale
     * @return array
     */
    private function prepareUtilityInflowData(UnitFundSale $unitSale)
    {
        $input = array();

        $input['id'] = null;

        $input['type'] = CustodialTransactionType::where('name', 'FI')->first()->id;

        $utilitiesFund = $this->getUtilitesFund();

        $input['custodial_account_id'] = $utilitiesFund->custodial_account_id;

        $input['unit_fund_id'] = $utilitiesFund->id;

        $input['exchange_rate'] = 1;

        $input['source'] = 'deposit';

        $input['amount'] = abs($unitSale->payment->amount);

        $input['date'] = Carbon::parse($unitSale->payment->date)->toDateString();

        $input['client_id'] = $unitSale->client_id;

        $input['received_from'] = ClientPresenter::presentFullNames($unitSale->client_id);

        $fa = $unitSale->client->getLatestFa();

        $input['commission_recipient_id'] = $fa ? $fa->id : null;

        $input['description'] = 'Client funds deposited for Utility bill payment';

        $input['outgoing_reference'] = $unitSale->client->client_code . '-' . $unitSale->payment->id;

        return $input;
    }
}
