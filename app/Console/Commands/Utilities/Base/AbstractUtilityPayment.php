<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Utilities\Base;

use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\Billing\UtilityBillingStatus;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\System\Locks\LockException;
use Carbon\Carbon;
use Cytonn\Support\Mails\Mailer;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;

abstract class AbstractUtilityPayment extends Command
{
    use LocksTransactions;

    protected $utilitiesFund;

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $instructions = $this->getInstructions();

        $this->info("Executing {$instructions->count()} instructions");

        if ($instructions->count()) {
            logRollbar("Executing {$instructions->count()} utility instructions");
        }

        $instructions->each(function ($instruction) {
            $this->executeWithinAtomicLock(
                function () use ($instruction) {
                    $this->info("Processing Instruction Id {$instruction->id}");
                    try {
                        \DB::transaction(function () use ($instruction) {
                            $this->executeInstruction($instruction);
                        });
                    } catch (\Throwable $exception) {
                        $this->error("An error occurred processing $instruction->id");
                        $this->error(get_class($exception) . ' : ' . str_limit($exception->getMessage(), 100));

                        $this->reportFailed($exception, $instruction);

                        if ($exception instanceof \Exception) {
                            reportException($exception);
                        }
                    }
                },
                'express_utility_payment:' . get_class($instruction) . ':' . $instruction->id . rand(1, 1000),
                120,
                30
            );
        });

        return;
    }

    /**
     * @param \Throwable $exception
     * @param $instruction
     */
    protected function reportFailed(\Throwable $exception, $instruction)
    {
        if ($this->invalidException($exception)) {
            return;
        }

        if ($this->recentlyReported($instruction)) {
            return;
        }

        $data = \GuzzleHttp\json_encode([
            'id' => $instruction->id,
            'client_code' => $instruction->client->client_code,
            'number' => $instruction->number,
            'fund' => $instruction->unitFund->short_name
        ]);

        Mailer::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : Automatic Utility Payment Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
                <p>Data: <br>
                    <pre>" . $data . " </pre>
                </p>")
            ->queue();

        if (strpos($exception->getMessage(), 'are more than the available units')) {
            $instruction->billing_status_id = UtilityBillingStatus::where('slug', 'cancelled')->first()->id;
        }

        $instruction->reported_at = Carbon::now();

        $instruction->save();
    }

    /**
     * @param \Throwable $e
     * @return bool
     */
    private function invalidException(\Throwable $e)
    {
        $exceptions = [
            ClientException::class,
            \PDOException::class,
            LockException::class,
            \App\Cytonn\Models\Behaviours\LockException::class,
            QueryException::class,
            \Error::class
        ];


        foreach ($exceptions as $exception) {
            if ($e instanceof $exception) {
                return true;
            }

            if (str_contains($e->getMessage(), 'withdrawal guard')) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $instruction
     * @return bool
     */
    private function recentlyReported($instruction)
    {
        if (is_null($instruction->reported_at)) {
            return false;
        }

        return Carbon::parse($instruction->reported_at)->gt(Carbon::now()->subHours(1));
    }

    abstract protected function getInstructions();

    abstract protected function executeInstruction($instruction);

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    protected function getUtilitesFund()
    {
        if ($this->utilitiesFund) {
            return $this->utilitiesFund;
        }

        return UnitFund::where('short_name', '=', 'CUF')->first();
    }
}
