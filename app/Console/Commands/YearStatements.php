<?php

namespace App\Console\Commands;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Reporting\ClientStatementGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class YearStatements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:year_statements {start} {end}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        DB::transaction(

            function () {
                $clawbacks = CommissionClawback::where('date', '>=', '2017-12-08')->get();

                $test = array();

                foreach ($clawbacks as $clawback) {
                    $withdrawal = $clawback->withdrawal;

                    if (is_null($withdrawal)) {
                        $withdrawal = ClientInvestmentWithdrawal::where(
                            'investment_id',
                            $clawback->commission->investment_id
                        )->where(
                            'date',
                            $clawback->date
                        )->where(
                            'type_id',
                            1
                        )->where(
                            'withdraw_type',
                            'withdrawal'
                        )->first();

                        if (is_null($withdrawal)) {
                            //                        $test[] = [
                            //                            'id' => $clawback->id,
                            //                            'type' => $clawback->type,
                            //                            'com' => $clawback->commission_id
                            //                        ];
                        } else {
                            $investment = $withdrawal->investment;

                            $tenor = $investment->maturity_date->copy()->diffInDays($withdrawal->date);

                            $clawback->update(
                                [
                                    'amount' =>
                                        $withdrawal->amount * $investment->commission->rate * $tenor / (100 * 365)
                                ]
                            );
                        }
                    } else {
                        $investment = $withdrawal->investment;

                        $tenor = $investment->maturity_date->copy()->diffInDays($withdrawal->date);

                        $clawback->update(
                            [
                                'amount' => $withdrawal->amount * $investment->commission->rate * $tenor / (100 * 365)
                            ]
                        );
                    }
                }
            }
        );

        dd("Done");


        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));

        $clients = Client::whereHas(
            'investments',
            function ($investments) use ($start, $end) {
                $investments->statement($end, $start);
            }
        )->get();

        $clients->each(
            function (Client $client) use ($start, $end) {
                $products = Product::whereHas(
                    'investments',
                    function ($investment) use ($client, $end, $start) {
                        $investment->statement($end, $start)->where('client_id', $client->id);
                    }
                )->get();

                foreach ($products as $product) {
                    $this->saveStatement($client, $product, $start, $end);
                }
            }
        );

        $folder = storage_path('statement');

        exec("cd $folder && tar -cvzf misc.tar.gz misc");

        $this->sendEmail();
    }

    private function saveStatement(Client $client, Product $product, Carbon $start, Carbon $end)
    {
        $generator = new ClientStatementGenerator();

        $pdf = $generator->generateStatementForClient(
            $product->id,
            $client->id,
            null,
            22,
            $start,
            $end
        )->output();

        $filename = $client->id . '_' . $client->client_code . '.pdf';

        $folder = 'misc/statements_dump/' . strtotime(
            $start->toDateString()
        ) . '-' . strtotime($end->toDateString()) . '/' . $product->name;

        $this->upload($pdf, $filename, $folder);
    }

    private function upload($contents, $filename, $folder)
    {
        Storage::disk(env('STORAGE_DISK'))->put($folder . '/' . $filename, $contents);
    }

    private function sendEmail()
    {
        $data = [
            'email' => 'tkimathi@cytonn.com',
            'subject' => 'Statement Generation',
            'file' => storage_path('statement/misc.tar.gz'),
            'content' => 'Statement generation complete'
        ];

        Mail::send(
            'emails.general',
            $data,
            function ($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
                $message->attach($data['file']);
            }
        );
    }
}
