<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CurrencyReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'custody:currency_report {end_date} {start_date?} {account_id?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of currency report for the specified period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $accountId = $this->argument('account_id');
        $account = ($accountId) ? CustodialAccount::findOrFail($accountId) : null;

        $startDate = $this->argument('start_date');
        $startDate = $startDate ? Carbon::parse($startDate) : null;

        $endDate = $this->argument('end_date');
        $endDate = $endDate ? Carbon::parse($endDate) : Carbon::now();

        $report = $this->getCurrencyReport($endDate, $startDate, $account);

        $dateName = ($startDate)
            ? (\Cytonn\Presenters\DatePresenter::formatDate($startDate) . ' to ' .
            \Cytonn\Presenters\DatePresenter::formatDate($endDate))
            : \Cytonn\Presenters\DatePresenter::formatDate($endDate);
        $fileName = 'Currency Report - ' . $dateName;

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the currency report from ' . $dateName)
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the currenct report
     */
    public function getCurrencyReport(Carbon $endDate, Carbon $startDate = null, CustodialAccount $account = null)
    {
        $transactions = CustodialTransaction::whereNotNull('exchange_rate')->where('date', '<=', $endDate);

        if ($startDate) {
            $transactions->where('date', '>=', $startDate);
        }

        if ($account) {
            $transactions->where('custodial_account_id', $account->id);
        }

        return $transactions->get()->map(
            function ($transaction) {
                $payment = $transaction->clientPayment;

                $client = ($payment) ? $payment->client : null;

                return [
                    'Date' => DatePresenter::formatDate($transaction->date),
                    'Client Code' => ($client) ? $client->client_code : '',
                    'Client Name' => ($client) ? ClientPresenter::presentJointFullNames($client->id)
                        : $transaction->received_from . ' (Not On-boarded)',
                    'Payment For' => ($payment) ? $payment->present()->paymentFor : '',
                    'Initial Amount' => AmountPresenter::currency($transaction->amount),
                    'Exchange Rate' => $transaction->exchange_rate,
                    'Amount (KES)' => ($payment) ? AmountPresenter::currency($payment->amount)
                        : AmountPresenter::currency($transaction->amount * $transaction->exchange_rate),
                    'Custodial Account' => $transaction->custodialAccount->account_name,
                    'Transaction Type' => $transaction->transactionType->description
                ];
            }
        );
    }
}
