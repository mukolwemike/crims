<?php

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\CustodialAccount;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Custodial\Transact\ReconciliationPolicy;
use Illuminate\Console\Command;

class ReconcileAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounts:reconcile {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reconcile custodial accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->argument('date');

        $date = $date ? Carbon::parse($date) : Carbon::today()->subDay();

        CustodialAccount::whereHas('balanceTrail', function ($trail) use ($date) {

            $trail->where('date', $date->copy())
                ->whereNull('reconciled_on');
        })->get()
        ->each(function (CustodialAccount $account) use ($date) {

            (new ReconciliationPolicy($account, $date->copy()))->reconcile();

            $this->info("Completed check for account ".$account->alias.' '.$account->account_no);
        });

        return;
    }
}
