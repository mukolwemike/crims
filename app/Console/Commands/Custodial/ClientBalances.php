<?php

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ClientBalances extends Command
{
    use ExcelMailer;

    protected $minimumReport = 1;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'custody:client-balances {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $clients = Client::has('clientPayments')->get();

        $clients->each(function (Client $client) {
            $balances = [];

            $products = Product::whereHas('payments', function ($q) use ($client) {
                $q->where('client_id', $client->id);
            })->get();

            $projects = Project::whereHas('payments', function ($q) use ($client) {
                $q->where('client_id', $client->id);
            })->get();

            $entities = SharesEntity::whereHas('payments', function ($q) use ($client) {
                $q->where('client_id', $client->id);
            })->get();

            $unitFunds = UnitFund::whereHas('payments', function ($q) use ($client) {
                $q->where('client_id', $client->id);
            })->get();

            foreach ($products as $product) {
                $bal = ClientPayment::balance($client, $product, null, null, Carbon::today()->addYear());

                if (abs($bal) > $this->minimumReport) {
                    $balances['products'][$product->id] = $bal;
                }
            }

            foreach ($projects as $project) {
                $bal = ClientPayment::balance($client, null, $project, null, Carbon::today()->addYear());

                if (abs($bal) > $this->minimumReport) {
                    $balances['projects'][$project->id] = $bal;
                }
            }

            foreach ($entities as $entity) {
                $bal = ClientPayment::balance($client, null, null, $entity, Carbon::today()->addYear());

                if (abs($bal) > $this->minimumReport) {
                    $balances['entities'][$entity->id] = $bal;
                }
            }

            foreach ($unitFunds as $unitFund) {
                $bal = ClientPayment::balance($client, null, null, null, Carbon::today()->addYear(), $unitFund);

                if (abs($bal) > $this->minimumReport) {
                    $balances['unit_funds'][$unitFund->id] = $bal;
                }
            }

            $client->balances = $balances;
        })->filter(function ($c) {
            return count($c->balances) >= 1;
        });

        $excel = $clients->map(
            function (Client $client) {
                $output = [];
                foreach ($client->balances as $product_type => $balance) {
                    foreach ($balance as $product_id => $b) {
                        $c = new EmptyModel();
                        $c->fill([
                            'Client code' => $client->client_code,
                            'Name' => ClientPresenter::presentJointFullNames($client->id),
                            'Product' => $this->getProductName($product_type, $product_id),
                            'balance' => $b
                        ]);

                        $output[] = $c;
                    }
                }

                return $output;
            }
        )->flatten();

        $fname = 'Clients with balances';

        Excel::fromModel($fname, $excel)->store('xlsx');

        $message = 'Please find attached the clients with balances as at '.Carbon::today()->toFormattedDateString();

        $userId = $this->argument('user_id');

        $user = $userId ? User::find($userId) : null;

        $email = is_null($user) ? config('system.emails.statements') : [$user->email];

        $this->sendExcel($fname.' '.Carbon::parse()->toFormattedDateString(), $message, $fname, $email);
    }

    private function getProductName($type, $id)
    {
        switch ($type) {
            case 'products':
                return Product::find($id)->name;
            case 'projects':
                return Project::find($id)->name;
            case 'entities':
                return SharesEntity::find($id)->name;
            case 'unit_funds':
                return UnitFund::find($id)->name;
            default:
                throw new \InvalidArgumentException("Type $type not supported");
        }
    }
}
