<?php

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Custodial\Production\RealEstate;
use Cytonn\Custodial\Production\Structured;
use Cytonn\Mailers\Custodial\DailyInflowsReportMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Support\Collection;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class DailyInflowsReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'custody:daily-inflows {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a report of daily inflows.';

    /**
     * Create a new command instance.
     * DailyInflowsReport constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $user = User::find($this->option('user_id'));
        $today = Carbon::yesterday();

        $structuredProducts = (new Structured())->generate($today, $today);

        $re = (new RealEstate())->generate($today, $today);

        $attachments = [];
        foreach (FundManager::all() as $fm) {
            $excel = (new Structured())->excel($fm, $today->copy()->startOfWeek(), $today->copy()->endOfWeek());
            $excel->file->store('xlsx');
            $transactions = $excel->transactions;

            if (count($transactions)) {
                $attachments[] = $excel->stored_path;
            }
        }

        $re_file = (new RealEstate())->excel($today->copy()->startOfWeek(), $today->copy()->endOfWeek());
        $re_file->file->store('xlsx');
        $attachments[] = $re_file->stored_path;

        (new DailyInflowsReportMailer())->sendEmail($today, $structuredProducts, $re, $attachments, $user);
    }
}
