<?php

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\RealEstateRefund;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Custodial\Withdraw;
use Cytonn\Mailers\ExcelMailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CorrectCustodial extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'custody:correct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        dd('disabled');

        DB::transaction(
            function () {
                $this->refunds();
            }
        );
    }


    private function refunds()
    {
        $refunds = RealEstateRefund::doesntHave('clientPayment')->has('holding')->get();

        $refunds->each(
            function (RealEstateRefund $refund) {
                $matched = ClientPayment::where('date', $refund->date)
                    ->where('amount', abs($refund->amount))
                    ->where('project_id', $refund->holding->project->id)
                    ->get();

                if ($matched->count() == 0) {
                    return $this->createRefund($refund);
                } else {
                    $match = $matched->first();
                    //save match here
                    $refund->clientPayment()->associate($match);
                    $refund->save();
                    return;
                }
            }
        );

        $this->info($refunds->count() . ' refunds found');
    }

    private function createRefund(RealEstateRefund $refund)
    {
        $holding = $refund->holding;

        $description = 'RE Forfeiture for unit number ' . $holding->unit->number . ' ' .
            $holding->unit->size->name . ' ' . $holding->unit->type->name . ' in ' . $holding->project->name;

        // Client Payment
        $p = Payment::make(
            $holding->client,
            $holding->commission->recipient,
            null,
            null,
            $holding->project,
            null,
            $refund->date,
            $refund->amount,
            'W',
            $description
        )->debit();

        $refund->clientPayment()->associate($p);
        $refund->save();

        Withdraw::make(
            $refund->date,
            $holding->project->custodialAccount,
            $description,
            $holding->client,
            $p->amount,
            null,
            $holding->project,
            null,
            null,
            $p,
            null,
            null,
            null,
            null
        );
    }
}
