<?php

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;
use Cytonn\Models\ExchangeRate;
use Illuminate\Console\Command;

class ExchangeRateFetch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custody:fetch-rates {--force=false} {--date=today}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $base = Currency::where('code', config('system.base_currency'))->first();

        $currencies = Currency::where('code', '!=', $base->code)->get()->all();

        $converter = new Convert($base);

        $force = $this->option('force') !== "false";

        $date = Carbon::parse($this->option('date'));

        $rates = $converter->fetch($date, $currencies);

        foreach ($rates as $code => $rate) {
            if ($rate) {
                $this->update($base, $code, $rate, $date, $force);
            }
        }
    }

    protected function update(Currency $base, $code, $rate, Carbon $date, $force = false)
    {
        $to = Currency::where('code', $code)->first();

        $rates = ExchangeRate::where(['base_id'=> $base->id, 'to_id' => $to->id, 'date'=>$date])->get();

        if ($rates->count() > 0 && !$force) {
            return; //ignore if you are not replacing
        }

        $rates->each->delete();

        ExchangeRate::create(
            [
            'base_id' => $base->id,
            'to_id' => $to->id,
            'date' => $date,
            'rate' => $rate
            ]
        );

        $this->info("Saved rate from {$base->id} to {$to->id} on {$date->toDateString()} as {$rate}");
    }
}
