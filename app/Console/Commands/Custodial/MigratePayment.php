<?php

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CoopPayment;
use Exception;
use Illuminate\Console\Command;
use App\Cytonn\Models\InterestPayment;
use App\Cytonn\Models\MembershipConfirmation;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharesEntity;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MigratePayment extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'custody:migrate-payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        dd('until further notice');
        \DB::transaction(
            function () {
            }
        );
    }

    private function migrated()
    {
        $this->investmentInflow();
        $this->info('Done with investment inflows');
        $this->investmentWithdrawal();
        $this->info('Done with withdrawals');
        $this->interestPayments();
        $this->info('Done with interest payments');
        $this->coopMemberships();
        $this->info('Done with coop memberships');
    }

    private function notDone()
    {
        $this->shares();
        $this->reInflow();
        $this->reRefunds();
    }

    private function investmentInflow()
    {
        ClientInvestment::whereNull('client_payment_in')->get()
            ->each(
                function (ClientInvestment $investment) {
                    $inflow = $investment->repo->inflow();

                    \Cytonn\Custodial\Payments\Payment::make(
                        $investment->client,
                        $investment->commission->recipient,
                        $investment->custodialInvestTransaction,
                        $investment->product,
                        null,
                        null,
                        $investment->invested_date,
                        $inflow,
                        'FI',
                        'Investment of '.$investment->amount.' in '.$investment->product->name
                    )->debit();

                    $p = \Cytonn\Custodial\Payments\Payment::make(
                        $investment->client,
                        $investment->commission->recipient,
                        null,
                        $investment->product,
                        null,
                        null,
                        $investment->invested_date,
                        $inflow,
                        'I',
                        'Investment of '.$investment->amount.' in '.$investment->product->name
                    )->credit();

                    $investment->paymentIn()->associate($p);
                    $investment->save();
                }
            );
    }

    private function investmentWithdrawal()
    {
        ClientInvestment::whereNull('client_payment_out')
            ->where('withdrawn', 1)
            ->get()
            ->each(
                function (ClientInvestment $investment) {
                    $out = $investment->withdraw_amount;

                    \Cytonn\Custodial\Payments\Payment::make(
                        $investment->client,
                        $investment->commission->recipient,
                        null,
                        $investment->product,
                        null,
                        null,
                        $investment->withdrawal_date,
                        $out,
                        'W',
                        'Withdrawal of '.$out.' from '.$investment->product->name
                    )->debit();

                    $p = \Cytonn\Custodial\Payments\Payment::make(
                        $investment->client,
                        $investment->commission->recipient,
                        $investment->custodialWithdrawTransaction,
                        $investment->product,
                        null,
                        null,
                        $investment->withdrawal_date,
                        $out,
                        'FO',
                        'Withdrawal of '.$out.' from '.$investment->product->name
                    )->credit();

                    $investment->paymentOut()->associate($p);
                    $investment->save();
                }
            );
    }

    private function interestPayments()
    {
        InterestPayment::whereNull('payment_out_id')
            ->actionSlug('send_to_bank')
            ->get()
            ->each(
                function (InterestPayment $payment) {
                    $out = $payment->amount;
                    $investment = $payment->investment;

                    \Cytonn\Custodial\Payments\Payment::make(
                        $investment->client,
                        $investment->commission->recipient,
                        null,
                        $investment->product,
                        null,
                        null,
                        $payment->date_paid,
                        $out,
                        'W',
                        'Interest payment of '.$out.' from '.$investment->product->name
                    )->debit();

                    $p = \Cytonn\Custodial\Payments\Payment::make(
                        $investment->client,
                        $investment->commission->recipient,
                        $payment->custodialTransaction,
                        $investment->product,
                        null,
                        null,
                        $payment->date_paid,
                        $out,
                        'FO',
                        'Interest payment of '.$out.' from '.$investment->product->name
                    )->credit();

                    $payment->paymentOut()->associate($p);
                    $payment->save();
                }
            );
    }

    private function reInflow()
    {
    }

    private function reRefunds()
    {
    }

    private function coopMemberships()
    {
        $entity = SharesEntity::find(1);

        CoopPayment::where('type', 'membership')
            ->get()
            ->each(
                function (CoopPayment $payment) use ($entity) {
                    \Cytonn\Custodial\Payments\Payment::make(
                        $payment->client,
                        null,
                        null,
                        null,
                        null,
                        $entity,
                        $payment->date,
                        $payment->amount,
                        'FI',
                        'Membership fees'
                    )->debit();

                    $p = \Cytonn\Custodial\Payments\Payment::make(
                        $payment->client,
                        null,
                        null,
                        null,
                        null,
                        $entity,
                        $payment->date,
                        $payment->amount,
                        'M',
                        'Membership fees'
                    )->credit();

                    $m = MembershipConfirmation::where('payment_id', $payment->id)->first();

                    if ($m) {
                        $m->update(['client_payment_id'=> $p->id]);
                    }
                }
            );
    }

    private function shares()
    {
        $entity = SharesEntity::find(1);

        ShareHolding::all()
            ->each(
                function (ShareHolding $purchase) use ($entity) {
                    $price = $purchase->purchase_price * $purchase->number;

                    \Cytonn\Custodial\Payments\Payment::make(
                        $purchase->client,
                        null,
                        null,
                        null,
                        null,
                        $entity,
                        $purchase->date,
                        $price,
                        'FI',
                        'Purchase of'. $purchase->number.' shares @ '.$purchase->purchase_price
                    )->debit();

                    \Cytonn\Custodial\Payments\Payment::make(
                        $purchase->client,
                        null,
                        null,
                        null,
                        null,
                        $entity,
                        $purchase->date,
                        $price,
                        'I',
                        'Purchase of'. $purchase->number.' shares @ '.$purchase->purchase_price
                    )->credit();
                }
            );
    }
}
