<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Custodial\Production\RealEstate;
use Cytonn\Custodial\Production\Structured;
use Cytonn\Mailers\Custodial\MonthlyInflowsReportMailer;
use Illuminate\Console\Command;

class MonthlyInflowsReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'custody:monthly-inflows {user_id} {start_date} {end_date} {fundmanager?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a report of monthly inflows.';

    /**
     * Create a new command instance.
     * DailyInflowsReport constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $user = User::find($this->argument('user_id'));

        $startDate = Carbon::parse($this->argument('start_date'));

        $endDate = Carbon::parse($this->argument('end_date'));

        $fundManagerId = $this->argument('fundmanager');

        $attachments = [];

        if ($fundManagerId) {
            $fm = FundManager::findOrFail($fundManagerId);

            $excel = (new Structured())->excel($fm, $startDate, $endDate, true);

            $excel->file->store('xlsx');

            $transactions = $excel->transactions;

            if (count($transactions)) {
                $attachments[] = $excel->stored_path;
            }
        } else {
            foreach (FundManager::all() as $fm) {
                $excel = (new Structured())->excel($fm, $startDate, $endDate, true);

                $excel->file->store('xlsx');

                $transactions = $excel->transactions;

                if (count($transactions)) {
                    $attachments[] = $excel->stored_path;
                }
            }

            $re_file = (new RealEstate())->excel($startDate, $endDate, true);

            $re_file->file->store('xlsx');

            $attachments[] = $re_file->stored_path;
        }

        (new MonthlyInflowsReportMailer())->sendEmail($startDate, $endDate, $attachments, $user);
    }
}
