<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Custodial;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CustodialAccountTransfers extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'custody:account_transfers {start?} {end?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the custodial account transfers for the specified period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $startDate = ($this->argument('start')) ? Carbon::parse($this->argument('start')) : null;

        $endDate = ($this->argument('end')) ? Carbon::parse($this->argument('end')) : null;

        $transfers = $this->getCustodialTransfers($startDate, $endDate);

        $fileName = 'Custodial Transfers';

        ExcelWork::generateAndStoreSingleSheet($transfers, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the custodial account transfers report')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the custodial account transfers
     */
    public function getCustodialTransfers(Carbon $startDate = null, Carbon $endDate = null)
    {
        $transactions = CustodialTransaction::where('type', 11)
            ->where('amount', '!=', 0)
            ->latest('date')
            ->orderBy('id', 'desc');

        if ($startDate) {
            $transactions->where('date', '>=', $startDate);
        }

        if ($endDate) {
            $transactions->where('date', '<=', $endDate);
        }

        return $transactions->get()->map(
            function ($transaction) {
                if ($transaction->client) {
                    $code = $transaction->client->client_code;
                    $name = \Cytonn\Presenters\ClientPresenter::presentJointFullNames($transaction->client_id);
                } else {
                    $code = '';
                    $name = $transaction->received_from;
                }

                $transferIn = $transaction->transferIn;

                return [
                    'Entry Date' => \Cytonn\Presenters\DatePresenter::formatDate($transaction->entry_date
                        ? $transaction->entry_date : $transaction->date),
                    'Value Date' => \Cytonn\Presenters\DatePresenter::formatDate($transaction->date),
                    'Client Code' => $code,
                    'Client Name' => $name,
                    'Narrative' => $transaction->description,
                    'Type' => $transaction->typeName(),
                    'Receipt Amount' => \Cytonn\Presenters\CustodialAmountPresenter::presentReceiptAmount(
                        $transaction->amount,
                        false
                    ),
                    'Balance' => $transaction->balance(),
                    'Currency' => \Cytonn\Presenters\AmountPresenter::currency(
                        $transaction->custodialAccount->currency->exchange
                    ),
                    'Sending Account' => $transferIn ? $transferIn->sender->custodialAccount->account_name : '',
                    'Receiving Account' => $transaction->custodialAccount->account_name
                ];
            }
        );
    }
}
