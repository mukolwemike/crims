<?php

namespace App\Console\Commands\Coop;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ProductPlan;
use Carbon\Carbon;
use Cytonn\Mailers\Coop\CoopPaymentRemindersMailer;
use function foo\func;
use Illuminate\Console\Command;
use App\Cytonn\Models\SharePurchases;

class SendPaymentRemindersToCoopClients extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'coop:payment-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Payment Reminders to Coop Clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->error('disabled');
        return;
        $dueDate = Carbon::today()->addWeeks(2);
    }

    public function send($dueDate)
    {
        $clients = Client::whereHas(
            'productPlans',
            function ($plan) use ($dueDate) {
                $plan->paymentDue($dueDate);
            }
        )->get();

        $clients->each(
            function (Client $client) use ($dueDate) {
                $plans = $client->productPlans()->paymentDue($dueDate)->get()
                    ->each(
                        function (ProductPlan $plan) use ($dueDate, $client) {
                            if ($plan->shares > 0) {
                                $entity = $client->fundManager->defaultEntity;
                                $price = $entity->sharePrice($dueDate);

                                $plan->entity = $entity;
                                $plan->amount = $price * $plan->shares;
                            }
                        }
                    )
                ->filter(
                    function ($plan) use ($client, $dueDate) {
                        if ($plan->shares) {
                            return ($plan->amount > 0)
                            &&
                            !SharePurchases::whereHas(
                                'shareHolder',
                                function ($shareHolder) use ($client) {
                                    $shareHolder->where('client_id', $client->id);
                                }
                            )->where('date', '>=', $dueDate->copy()->subMonthNoOverflow())
                                ->exists();
                        }

                        return ($plan->amount > 0) && !$client->investments()
                            ->where('invested_date', '>=', $dueDate->copy()->subMonthNoOverflow())
                            ->where('product_id', $plan->product_id)
                            ->exists();
                    }
                );

                $client->plans = $plans;
            }
        );

        $mailer = new CoopPaymentRemindersMailer();

        $clients->filter(
            function ($client) {
                return count($client->plans) > 0;
            }
        )->each(
            function ($client) use ($mailer, $dueDate) {
                $mailer->sendReminder($client, $dueDate);
            }
        );
    }
}
