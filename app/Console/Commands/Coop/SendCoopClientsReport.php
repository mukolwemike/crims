<?php

namespace App\Console\Commands\Coop;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class SendCoopClientsReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'coop:clients-report {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Coop clients report.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $user = User::find($this->argument('user_id'));

        $coop_clients = Client::whereHas(
            'investments',
            function ($i) {
                $i->fundManager(FundManager::where('name', 'Coop')->first());
            }
        )->orHas('coopPayments')
            ->get()
            ->map(
                function ($client) {
                    $client->registration_fee = $client->coopPayments()->where('type', 'membership')
                        ->get()
                        ->sum(
                            function ($payment) {
                                return abs($payment->sum('amount'));
                            }
                        );

                    $client->no_of_shares = $client->shareHoldings->sum('number');

                    $client->total_value_of_shares = $client->shareHoldings->sum(
                        function ($holding) {
                            return $holding->number * $holding->purchase_price;
                        }
                    );

                    $client->total_value_of_shares = 0;


                    $products_arr = $client->investments->map(
                        function ($inv) {
                            return $inv->product->name;
                        }
                    )
                        ->toArray();

                    $client->products = implode(", ", Collection::make($products_arr)->unique()->all());

                    $client->total_value_of_product = $client->investments()->active()->sum('amount');

                    return $client;
                }
            );

        $output['Client Details'] = $coop_clients->map(
            function (Client $client) {
                $c = new EmptyModel();
                $c->{'Client\'s Name'} = ClientPresenter::presentFullNames($client->id);
                $c->{'Client Code'} = $client->client_code;
                $c->{'Telephone'} = $client->contact->phone;
                $c->{'E-Mail'} = $client->contact->email;
                $c->{'Registration Fee'} = AmountPresenter::currency($client->registration_fee);
                $c->{'Number of Shares'} = $client->no_of_shares;
                $c->{'Total Shares Value'} = AmountPresenter::currency($client->total_value_of_shares);
                $c->Products = $client->products;
                $c->{'Total Product Value'} = AmountPresenter::currency($client->total_value_of_product);

                return $c;
            }
        );

        $output['Shares'] = $coop_clients->map(
            function (Client $client) {
                return $client->shareHoldings()->latest('date')->get()->map(
                    function (ShareHolding $holding) use ($client) {
                        $c = new EmptyModel();
                        $c->{'Client\'s Name'} = ClientPresenter::presentFullNames($client->id);
                        $c->{'Client Code'} = $client->client_code;
                        $c->{'Date'} = Carbon::parse($holding->date)->toFormattedDateString();
                        $c->{'Number'} = $holding->number;
                        $c->{'Price'} = $holding->purchase_price;

                        return $c;
                    }
                )->all();
            }
        )->flatten();

        $file_name = 'Coop Clients Report';

        Excel::fromModel($file_name, $output)->store('xlsx');

        $this->mailResult(storage_path('exports/' . $file_name . '.xlsx'), $user);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $user
     */
    private function mailResult($file_path, $user = null)
    {
        $mailer = new GeneralMailer();
        if ($user) {
            $email = $user->email;
        } else {
            $email = ['mchaka@cytonn.com'];
        }
        $mailer->to($email);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Coop Clients Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Here is the export of coop clients report that you requested.');

        \File::delete($file_path);
    }
}
