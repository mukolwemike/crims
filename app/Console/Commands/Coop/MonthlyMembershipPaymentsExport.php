<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Coop;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class MonthlyMembershipPaymentsExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:monthly_coop_memberships {start} {end} {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of monthly coop memberships';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $userId = $this->argument('userId');

        $clients = $this->getReport($start, $end);

        $fileName = 'Monthly Coop Memberships';

        ExcelWork::generateAndStoreMultiSheet($clients, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the monthly memberships payments report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the payments
     */
    public function getReport(Carbon $start, Carbon $end)
    {
        $months = getDateRange($start, $end);

        $report = array();

        $summary = array();

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month);

            $endOfMonth = $startOfMonth->copy()->endOfMonth();

            $data = $this->getPayments($startOfMonth, $endOfMonth);

            $report[$startOfMonth->format('Y F')] = $data['data'];

            $summary[] = [
                'Month' => $startOfMonth->format('Y F'),
                'Amount' => AmountPresenter::currency($data['sum'], true)
            ];
        }

        $report['Summary'] = $summary;

        return $report;
    }

    /*
     * Get the payments for the specifed period
     */
    public function getPayments(Carbon $start, Carbon $end)
    {
        $payments = ClientPayment::where('type_id', 7)
            ->where('share_entity_id', 1)
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->get();

        $paymentArray = array();

        $sum = 0;

        foreach ($payments as $payment) {
            $client = $payment->client;

            $fa = $client->getLatestFA();

            $account = $this->getAccount($client, $payment);

            $sum += abs($payment->amount);

            $paymentArray[] = [
                'Date' => $payment->date,
                'Client Code' => $client->client_code,
                'Client' => ClientPresenter::presentFullNames($client->id),
                'Amount' => AmountPresenter::currency($payment->amount, true),
                'FA' => $fa ? $fa->name : '',
                'Account' => $account
            ];
        }

        $paymentArray[] = [
            'Date' => 'Total',
            'Client Code' => '',
            'Client' => '',
            'Amount' => AmountPresenter::currency($sum, true),
            'FA' => '',
            'Account' => ''
        ];

        return [
            'data' => $paymentArray,
            'sum' => $sum
        ];
    }

    /*
     * Get the linked account
     */
    public function getAccount(Client $client, $purchase)
    {
        $accounts = CustodialAccount::whereHas('transactions', function ($q) use ($client, $purchase) {
            $q->whereHas('clientPayment', function ($q) use ($client, $purchase) {
                $q->where('type_id', 1)->where('share_entity_id', 1)
                    ->where('client_id', $client->id)
                    ->where('date', '<=', $purchase->date);
            });
        })->get(['account_name'])->toArray();

        return implode(',', array_flatten($accounts));
    }
}
