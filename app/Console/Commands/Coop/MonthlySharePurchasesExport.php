<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Coop;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class MonthlySharePurchasesExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:monthly_coop_share_purchases {start} {end} {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of monthly coop share purchases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $userId = $this->argument('userId');

        $report = $this->getReport($start, $end);

        $fileName = 'Monthly Coop Share Purchases';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the monthly coop share purchases report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the payments
     */
    public function getReport(Carbon $start, Carbon $end)
    {
        $months = getDateRange($start, $end);

        $report = array();

        $summary = array();

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month);

            $endOfMonth = $startOfMonth->copy()->endOfMonth();

            $data = $this->getPurchases($startOfMonth, $endOfMonth);

            $report[$startOfMonth->format('Y F')] = $data['data'];

            $summary[] = [
                'Month' => $startOfMonth->format('Y F'),
                'Amount' => AmountPresenter::currency($data['sum'], true)
            ];
        }

        $report['Summary'] = $summary;

        return $report;
    }

    /*
     * Get the report
     */
    public function getPurchases(Carbon $start, Carbon $end)
    {
        $purchaseArray = array();

        $sum = 0;

        $purchases = ShareHolding::where('entity_id', 1)
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->get();

        foreach ($purchases as $purchase) {
            $payment = $purchase->payment;

            if (! $payment) {
                $payment = $purchase->coopPayment;
            }

            if ($payment) {
                $client = $payment->client;

                $fa = $client->getLatestFA();

                $account = $this->getAccount($client, $purchase);

                $sum += $payment->amount;

                $purchaseArray[] = [
                    'Date' => $purchase->date,
                    'Client Code' => $client->client_code,
                    'Client' => ClientPresenter::presentFullNames($client->id),
                    'Amount' => AmountPresenter::currency($payment->amount, true),
                    'FA' => ($fa) ? $fa->name : '',
                    'Account' => $account
                ];
            }
        }

        $purchaseArray[] = [
            'Date' => 'Total',
            'Client Code' => '',
            'Client' => '',
            'Amount' => AmountPresenter::currency($sum, true),
            'FA' => '',
            'Account' => ''
        ];

        return [
            'data' => $purchaseArray,
            'sum' => $sum
        ];
    }

    /*
     * Get the linked account
     */
    public function getAccount(Client $client, $purchase)
    {
        $accounts = CustodialAccount::whereHas('transactions', function ($q) use ($client, $purchase) {
            $q->whereHas('clientPayment', function ($q) use ($client, $purchase) {
                $q->where('type_id', 1)->where('share_entity_id', 1)
                    ->where('client_id', $client->id)
                    ->where('date', '<=', $purchase->date);
            });
        })->get(['account_name'])->toArray();

        return implode(',', array_flatten($accounts));
    }
}
