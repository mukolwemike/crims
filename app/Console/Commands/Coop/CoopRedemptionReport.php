<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Coop;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CoopRedemptionReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:coop_redemption_report {userId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a report of all the coop redemptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('userId');

        $clients = $this->getReport();

        $fileName = 'Coop Redemption Report';

        ExcelWork::generateAndStoreMultiSheet($clients, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the coop redemptions report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    public function getReport()
    {
        $dataArray = array();

        $shareSales = ShareSale::where('type', 2)->get();

        foreach ($shareSales as $shareSale) {
            $client = $shareSale->shareHolder->client;

            $salePrice = $shareSale->number * $shareSale->sale_price;

            $dataArray['Share Redemptions'][] = [
                'Redeem Date' => $shareSale->date,
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Type' => 'Share Redemption',
                'Number' => $shareSale->number,
                'Sale Price' => $shareSale->sale_price,
                'Amount' => AmountPresenter::currency($salePrice),
                'Net Interest' => AmountPresenter::currency($shareSale->redeem_interest),
                'Net Value' => AmountPresenter::currency($salePrice + $shareSale->redeem_interest),
            ];
        }

        $clientPayments = ClientPayment::where('type_id', 9)->get();

        foreach ($clientPayments as $clientPayment) {
            $client = $clientPayment->client;

            $dataArray['Membership Redemptions'][] = [
                'Redeem Date' => $clientPayment->date,
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Type' => 'Membership Redemption',
                'Amount' => AmountPresenter::currency($clientPayment->amount),
            ];
        }

        return $dataArray;
    }
}
