<?php

namespace App\Console\Commands\Statements;

use App\Cytonn\Models\RealestateCampaignStatementItem;
use App\Cytonn\Models\RealestateStatementCampaign;
use Carbon\Carbon;
use Cytonn\Realestate\Statements\StatementGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class RealEstateStatementProcessor extends Command
{
    use StatementProcessor;

    const MAX_TRIES = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statements:send-realestate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process and send ready real estate statements';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $this->sendRealEstate();

        $this->info('RealEstate Statement batch complete!');

        $this->report();

        return;
    }

    /**
     * Send Real Estate Statements
     * @param null $job
     * @throws \Throwable
     */
    public function sendRealEstate($job = null)
    {
        $readyStatements = RealestateCampaignStatementItem::where($this->toSendQuery())->pluck('id');

        $this->out('[RE] ' . count($readyStatements) . ' statements left');

        $generator = new StatementGenerator();
        $generator->setProtectOutput(true);

        $sendTo = $this->random($readyStatements);

        if ($sendTo) {
            $item = RealestateCampaignStatementItem::where('id', $sendTo)
                ->where($this->toSendQuery())->first();

            $this->out('[RE] attempting ' . is_null($item)?: $item->client_id);

            $closure = function ($item) use ($generator) {
                is_null($item) ?: $generator->sendRealEstateCampaignStatementItem($item);
            };

            $this->try($closure, $item);

            $this->out('[RE] sent successfully');

            \Queue::push(static::class . '@sendRealEstate');

            $this->sendRealEstate();
        }

        if ($job) {
            $job->delete();
        }
    }
}
