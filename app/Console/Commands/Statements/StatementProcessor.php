<?php

namespace App\Console\Commands\Statements;

use App\Cytonn\Models\RealestateStatementCampaign;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use App\Cytonn\System\Locks\Facade\Lock;
use App\Exceptions\Handler;
use Carbon\Carbon;
use Cytonn\Mailers\StatementMailer;
use Illuminate\Database\Eloquent\Model;

trait StatementProcessor
{
    /**
     * @param $coll
     * @return null
     */
    protected function random($coll)
    {
        try {
            return $coll->random();
        } catch (\InvalidArgumentException $exception) {
            return null;
        }
    }

    protected function out($msg)
    {
        echo $msg . PHP_EOL;

        \Log::info($msg);
    }

    protected function report()
    {
        if (!$campaigns = $this->shouldReport()) {
            $this->info("No campaigns to report");
            return;
        }

        $mailer = new StatementMailer();
        $mailer->report($campaigns);
    }

    /**
     * Query for statements to be sent
     *
     * @return \Closure
     */
    protected function toSendQuery()
    {
        return function ($query) {
            $query->where('ready', true)
                ->whereNull('failed_on')
                ->whereNull('processed_at')
                ->where(
                    function ($q) {
                        $q->where('sent', null)
                            ->orWhere('sent', 0);
                    }
                );
        };
    }

    /**
     * @param \Closure $closure
     * @param Model $item
     * @throws \Throwable
     * @throws \Throwable
     */
    protected function try(\Closure $closure, Model $item)
    {
        $toExec = function () use ($closure, $item) {
            $this->registerProcessing($item);
            $closure($item);
        };

        $key = 'send_statement_' . $item->getTable() . '_' . $item->getKey();
        $lock = null;

        try {
            $this->executeInLock($key, $toExec);
        } catch (\Exception $e) {
            $this->registerFailure($item, $e);
            app(Handler::class)->report($e);
        }
    }

    protected function shouldReport()
    {
        $date = Carbon::today()->subDays(3);

        $campaigns = [
            'investments' => StatementCampaign::where('send_date', '>=', $date)
                ->whereNull('report_sent_on')->latest('send_date')->latest()->get(),
            're' => RealestateStatementCampaign::where('send_date', '>=', $date)
                ->whereNull('report_sent_on')->latest('send_date')->latest()->get(),
//            'shares'      =>  SharesStatementCampaign::where('send_date', '>=', $date)
//               ->whereNull('report_sent_on')->latest('send_date')->latest()->first(),
            'unitization' => UnitFundStatementCampaign::where('send_date', '>=', $date)
                ->whereNull('report_sent_on')->latest('send_date')->latest()->get()
        ];

        $collected = collect($campaigns)->flatten(1);

        $res = $collected->reduce(function ($carry, $campaign) {
            if (is_null($campaign)) {
                return false;
            }

            $attempted = $campaign->items()->where(function ($q) {
                $q->whereNotNull('sent_on')->orWhereNotNull('failed_on');
            })->count();

            $all = $campaign->items()->count();
            $this->out(class_basename($campaign));
            $this->out("All $all attempted $attempted");

            return $carry && ($all == $attempted);
        }, true);

        if ($res && $collected->count() > 0) {
            return $campaigns;
        }

        return false;
    }

    /**
     * @param $key
     * @param \Closure $toExec
     * @throws \Throwable
     */
    protected function executeInLock($key, \Closure $toExec)
    {
        $lock = Lock::acquireLock($key, 900);

        if (!$lock) {
            $this->out("Lock failed, skipped - " . $key);
            return;
        }

        try {
            call_user_func($toExec);
        } catch (\Throwable $e) {
            @Lock::releaseLock($lock);

            throw $e;
        }

        @Lock::releaseLock($lock);
    }

    /**
     * @param $item
     */
    protected function registerFailure(Model $item, \Exception $e)
    {
        $tries = $item->tries + 1;

        $item->update([
            'tries' => $tries,
            'failed_on' => ($tries >= static::MAX_TRIES && is_null($item->failed_on))
                ? Carbon::now() : $item->failed_on,
            'exception' => class_basename($e) . ' \n' . $e->getTraceAsString(),
            'processed_at' => null
        ]);
    }

    protected function registerProcessing(Model $item)
    {
        $item->update([
            'processed_at' => Carbon::now()
        ]);
    }
}
