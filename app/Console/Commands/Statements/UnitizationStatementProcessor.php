<?php

namespace App\Console\Commands\Statements;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCampaignStatementItem;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class UnitizationStatementProcessor extends Command
{
    use StatementProcessor;

    const MAX_TRIES = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statements:send-unitization';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process and send ready unitization statements';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $this->sendUnitization();

        $this->info('Unitization Statement batch complete!');

        $this->report();

        return;
    }

    /**
     * Send Unitization Statements
     * @param null $job
     * @throws \Throwable
     */
    public function sendUnitization($job = null)
    {
        $readyStatements = UnitFundCampaignStatementItem::where($this->toSendQuery())->pluck('id');

        $this->out('[UTF] ' . count($readyStatements) . ' statements left');

        $generator = new \Cytonn\Unitization\Reporting\StatementGenerator();

        $sendTo = $this->random($readyStatements);

        if ($sendTo) {
            $item = UnitFundCampaignStatementItem::where('id', $sendTo)->where($this->toSendQuery())->first();

            if (!$item) {
                return;
            }

            $this->out('[UTF] attempting ' . $item->id);

            $closure = function ($item) use ($generator) {
                is_null($item) ?: $generator->sendCampaignStatementItem($item);
            };

            $this->try($closure, $item);

            $this->out('[UTF] sent successfully');

            \Queue::push(static::class . '@sendUnitization');

            $this->sendUnitization();
        }

        if ($job) {
            $job->delete();
        }
    }
}
