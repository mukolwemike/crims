<?php

namespace App\Console\Commands\Statements;

use App\Cytonn\Models\SharesCampaignStatementItem;
use App\Cytonn\Models\SharesStatementCampaign;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SharesStatementProcessor extends Command
{
    use StatementProcessor;

    const MAX_TRIES = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statements:send-shares';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process and send ready shares statements';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $this->sendShares();

        $this->info('Shares Statement batch complete!');

        $this->report();

        return;
    }

    /**
     * Send Shares Statements
     * @throws \Throwable
     */
    public function sendShares($job = null)
    {
        $readyStatements = SharesCampaignStatementItem::where($this->toSendQuery())->pluck('id');

        $this->out('[Sh] ' . count($readyStatements) . ' statements left');

        $generator = new \Cytonn\Shares\Reporting\StatementGenerator();

        $sendTo = $this->random($readyStatements);

        if ($sendTo) {
            $item = SharesCampaignStatementItem::where('id', $sendTo)->where($this->toSendQuery())->first();

            $this->out('[Sh] attempting ' . $item->id);

            $closure = function ($item) use ($generator) {
                is_null($item) ?: $generator->sendSharesCampaignStatementItem($item);
            };

            $this->try($closure, $item);

            $this->out('[Sh] sent successfully');

            \Queue::push(static::class . '@sendShares');

            $this->sendShares();
        }

        if ($job) {
            $job->delete();
        }
    }
}
