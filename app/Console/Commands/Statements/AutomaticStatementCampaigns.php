<?php

namespace App\Console\Commands\Statements;

use App\Cytonn\Models\RealestateStatementCampaign;
use App\Cytonn\Models\SharesStatementCampaign;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use Carbon\Carbon;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class AutomaticStatementCampaigns extends Command
{
    use StatementProcessor;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:automatic_statements_campaigns {dispatch?}';

    protected $campaigns = [];
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'creates the statements campaigns and makes them ready to be sent';

    protected $user;

    protected $date;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function fire()
    {
        $dispatch = $this->argument('dispatch');

        Mailer::compose()
            ->to(systemEmailGroups(['administrators']))
            ->subject('CRIMS : Statement Campaign Dispatch')
            ->text('CRIMS : Statement Campaign Dispatch Start')
            ->send();

        try {
            $response = [];

            if (!$dispatch) {
                $this->user = getSystemUser();

                $this->date = Carbon::now();

                $this->out('Starting Statement Dispatch');

                $response[] = $this->createInvCampaigns();

                $this->out('Investment Statement Dispatch Complete');

                $response[] = $this->createReCampaigns();

                $this->out('RE Statement Dispatch Complete');

                $response[] = $this->createUnitizationCampaigns();

                $this->out('UTF Statement Dispatch Complete');

                //$response[] = $this->createSharesCampaigns();
            }

            $this->dispatchCampaigns();
        } catch (\Exception $exception) {
            Mailer::compose()
                ->to(systemEmailGroups(['administrators']))
                ->subject('CRIMS : Statement Campaign Dispatch Failed')
                ->text($exception->getMessage())
                ->send();
        }

        Mailer::compose()
            ->to(systemEmailGroups(['administrators']))
            ->subject('CRIMS : Statement Campaign Dispatch')
            ->text(implode(',', $response))
            ->send();
    }

    private function createInvCampaigns()
    {
        $campaign = StatementCampaign::where('send_date', $this->date->toDateString())->latest()->first();

        if (!$campaign) {
            $campaign = StatementCampaign::create([
                'name' => $this->date->format('F Y') . ' Statements',
                'send_date' => $this->date->toDateString(),
                'type_id' => 2,
                'sender_id' => $this->user->id
            ]);
        }

        $campaign->repo->automaticallyAddClients();

        $this->campaigns[] = $campaign;

        return 'Investments Dispatched';
    }

    private function createReCampaigns()
    {
        $campaign = RealestateStatementCampaign::where('send_date', $this->date->toDateString())->latest()
            ->first();

        if (!$campaign) {
            $campaign = RealestateStatementCampaign::create([
                'name' => $this->date->format('F Y') . ' CRE Statements',
                'send_date' => $this->date->toDateString(),
                'type_id' => 2,
                'sender_id' => $this->user->id
            ]);
        }

        $campaign->repo->automaticallyAddClients();

        $this->campaigns[] = $campaign;

        return 'RE Dispatched';
    }

    private function createUnitizationCampaigns()
    {
        $shortNames = ['CMMF', 'CBF', 'CEF', 'CHYF'];

        foreach ($shortNames as $name) {
            $fund = UnitFund::where('short_name', $name)->first();

            $campaign = UnitFundStatementCampaign::where('send_date', $this->date->toDateString())
                ->where('unit_fund_id', $fund->id)
                ->latest()->first();

            if (! $campaign) {
                $campaign = UnitFundStatementCampaign::create([
                    'name' => $fund->name . ' ' . $this->date->format('F Y'),
                    'send_date' => $this->date->toDateString(),
                    'unit_fund_id' => $fund->id,
                    'type_id' => 2,
                    'sender_id' => $this->user->id
                ]);
            }

            $campaign->repo->automaticallyAddClients();

            $this->campaigns[] = $campaign;
        }

        return 'Unit Funds Dispatched';
    }

    private function createSharesCampaigns()
    {
        $campaign = SharesStatementCampaign::where('send_date', $this->date->toDateString())
            ->latest()
            ->first();

        if (!$campaign) {
            $campaign = SharesStatementCampaign::create([
                'name' => 'Coop Share Statements ' . $this->date->format('F Y'),
                'send_date' => $this->date->toDateString(),
                'type_id' => 2,
                'sender_id' => $this->user->id
            ]);
        }

        $campaign->repo->automaticallyAddClients();

        $this->campaigns[] = $campaign;

        return 'Shares Dispatched';
    }

    private function dispatchCampaigns()
    {
        foreach ($this->campaigns as $campaign) {
            $campaign->repo->sendCampaign($this->user);

            $campaign->close();
        }
    }
}
