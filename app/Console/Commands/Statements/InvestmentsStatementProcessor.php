<?php

namespace App\Console\Commands\Statements;

use App\Cytonn\Models\CampaignStatementItem;
use App\Cytonn\Models\StatementCampaign;
use Carbon\Carbon;
use Cytonn\Reporting\ClientStatementGenerator;
use Illuminate\Console\Command;

class InvestmentsStatementProcessor extends Command
{
    use StatementProcessor;

    const MAX_TRIES = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statements:send-investments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process and send ready investments statements';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $this->sendInvestments();

        $this->info('Investments Statement batch complete!');

        $this->report();

        return;
    }

    /**
     * Send Investments Statements
     * @param null $job
     * @throws \Throwable
     */
    public function sendInvestments($job = null)
    {
        $readyStatements = CampaignStatementItem::where($this->toSendQuery())->pluck('id');

        $this->out('[Inv] ' . count($readyStatements) . ' statements left');

        $generator = new ClientStatementGenerator();
        $generator->setProtectOutput(true);

        $sendTo = $this->random($readyStatements);

        if ($sendTo) {
            $item = CampaignStatementItem::where('id', $sendTo)->where($this->toSendQuery())->first();

            if (!$item) {
                return;
            }

            $this->out('[Inv] attempting ' . $item->client_id);

            $closure = function ($item) use ($generator) {
                is_null($item) ?: $generator->sendCampaignStatementItem($item);
            };

            $this->try($closure, $item);

            $this->out('[Inv] sent successfully');

            \Queue::push(static::class . '@sendInvestments');

            $this->sendInvestments();
        }

        if ($job) {
            $job->delete();
        }
    }
}
