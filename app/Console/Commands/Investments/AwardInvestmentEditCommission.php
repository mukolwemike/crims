<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Exceptions\CrimsException;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class AwardInvestmentEditCommission extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:award_investment_edit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Award investment edit commission';
    protected $investmentArray = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        \DB::transaction(function () {
            dd("Disabled");
            $approvals = ClientTransactionApproval::where('transaction_type', 'edit_investment')
                ->where('approved', 1)
                ->where('approved_on', '>=', Carbon::parse('2018-12-01'))
                ->get();

            foreach ($approvals as $approval) {
                $payload = $approval->payload;

                $approvalDate = Carbon::parse($approval->approved_on);

                $oldData = $payload['edited'];

                $oldMaturityDate = Carbon::parse($oldData['maturity_date']);

                $investment = ClientInvestment::find($oldData['id']);

                if (is_null($investment)) {
                    $investment = ClientInvestment::where('id', $oldData['id'])->withTrashed()->first();
                    $this->saveSchedule($investment, [], 'Investment Deleted', '');
                    continue;
                }

                $newData = $payload['new'];

                if (!array_key_exists('maturity_date', $newData)) {
                    $this->saveSchedule($investment, [], 'Maturity Date Not Edited', '');
                    continue;
                }

                $newMaturityDate = Carbon::parse($newData['maturity_date']);

                if ($newMaturityDate <= $oldMaturityDate) {
                    $this->saveSchedule(
                        $investment,
                        [],
                        'New Maturity Date Before Old',
                        '',
                        $oldMaturityDate->toDateString()
                    );
                    continue;
                }

                $recipient = $investment->commission->recipient;

                $rate = $recipient->repo->getInvestmentCommissionRate($investment->product, Carbon::now());

                if (is_null($rate)) {
                    throw new CrimsException("Could not find commission rate for recipient " . $recipient->id);
                }

                if ($rate->rate == 0 || $recipient->zero_commission == 1) {
                    $this->saveSchedule($investment, [], 'Zero Rate Commission', '');
                    continue;
                }

                $mDate = $investment->withdrawal_date ? Carbon::parse($investment->withdrawal_date) :
                    Carbon::parse($investment->maturity_date);

                $additionalDays = $mDate->diffInDays($oldMaturityDate);

                if ($additionalDays > 90) {
                    $rate = $recipient->repo->checkRolloverInvestmentReward(3, Carbon::now(), $rate);
                }

                $rateDifference = $rate->rate - $investment->commission->rate;

                if ($rateDifference <= 0) {
                    $this->saveSchedule(
                        $investment,
                        [],
                        'No Change in COmmission Rate',
                        0,
                        $oldMaturityDate->toDateString(),
                        $additionalDays
                    );
                    continue;
                }

                $addedCommission = ($rateDifference * $investment->amount * $additionalDays / (100 * 365));

                $commission = $investment->commission;

                $fullTenor = Carbon::parse($investment->maturity_date)
                    ->diffInDays(Carbon::parse($investment->invested_date));

                if ($fullTenor < 365) {
                    $this->makeStaggeredPayment(
                        $investment,
                        $addedCommission,
                        $rateDifference,
                        $approvalDate,
                        $oldMaturityDate,
                        $additionalDays
                    );
                } else {
                    $this->saveSchedule($investment, [
                        'commission_id' => $commission->id,
                        'date' => $approvalDate,
                        'amount' => $addedCommission,
                        'description' => 'Bonus for investment extension and Rollover Bonus',
                        'first' => false,
                        'commission_payment_schedule_type_id' => 2
                    ], 'Award One Time', $rateDifference, $oldMaturityDate->toDateString(), $additionalDays);
                }
            }

            $fileName = 'Rollover Commission';

            ExcelWork::generateAndStoreSingleSheet($this->investmentArray, $fileName);

            Mailer::compose()
                ->to([])
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the rollover commission')
                ->excel([$fileName])
                ->send();

            $path = storage_path() . '/exports/' . $fileName . '.xlsx';

            \File::delete($path);
        });
    }

    private function makeStaggeredPayment(
        ClientInvestment $investment,
        $addedCommission,
        $rateDifference,
        $date,
        $oldMaturity,
        $days
    ) {
        $months = getDateRange($date, Carbon::parse($investment->maturity_date));

        $monthsCount = count($months);

        if ($monthsCount == 0) {
            $this->saveSchedule($investment, [
                'commission_id' => $investment->commission->id,
                'date' => $date,
                'amount' => $addedCommission,
                'description' => 'Bonus for investment extension and Rollover Bonus',
                'first' => false,
                'commission_payment_schedule_type_id' => 2
            ], 'Staggering Commission', $rateDifference, $oldMaturity->toDateString(), $days);
            return CommissionPaymentSchedule::create();
        }

        $firstAmount = round($addedCommission / 2, 2);

        $staggerAmount = $addedCommission - $firstAmount;

        $monthlyStaggerAmount = round(($staggerAmount) / $monthsCount, 2);

        $paid = 0;

        foreach ($months as $key => $month) {
            if ($key == 0) {
                $this->saveSchedule($investment, [
                    'commission_id' => $investment->commission->id,
                    'date' => $month,
                    'amount' => $firstAmount,
                    'description' => 'Bonus for investment extension and Rollover Bonus',
                    'first' => false,
                    'commission_payment_schedule_type_id' => 2
                ], 'Staggering Commission', $rateDifference, $oldMaturity->toDateString(), $days);
            }

            $amountToStagger = $monthsCount == $key + 1 ? $staggerAmount - $paid : $monthlyStaggerAmount;

            $this->saveSchedule(
                $investment,
                [
                    'commission_id' => $investment->commission->id,
                    'date' => $month,
                    'amount' => $amountToStagger,
                    'description' => 'Bonus for investment extension and Rollover Bonus installment : ' . ($key + 1),
                    'first' => false,
                    'commission_payment_schedule_type_id' => 2
                ],
                'Staggering Commission',
                $rateDifference,
                $oldMaturity->toDateString(),
                $days
            );

            $paid += $amountToStagger;
        }
    }

    private function saveSchedule(
        ClientInvestment $investment,
        $data,
        $reason,
        $commDiff,
        $oldMaturityDate = '',
        $addedDays = ''
    ) {
        $schedule = count($data) > 0 ? CommissionPaymentSchedule::create($data) : null;

        $this->investmentArray[] = [
            'Inv ID' => $investment->id,
            'Client Code' => $investment->client->client_code,
            'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
            'FA' => @$investment->commission->recipient->name,
            'Reason' => $reason,
            'Amount' => $investment->amount,
            'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
            'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
            'Old Maturity Date' => $oldMaturityDate,
            'Added Days' => $addedDays,
            'Withdrawal Date' => $investment->withdrawal_date ?
                Carbon::parse($investment->withdrawal_date)->toDateString() : '',
            'Commission Rate' => @$investment->commission->rate,
            'Commission Difference' => $commDiff,
            'Additional Commission Amount' => $schedule ? $schedule->amount : '',
            'Date' => $schedule ? Carbon::parse($schedule->date)->toDateString() : '',
            'Description' => $schedule ? $schedule->description : ''
        ];
    }
}
