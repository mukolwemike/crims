<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Console\Command;

class YearlyInvestmentsExportCommand extends Command
{
    protected $end;

    protected $start;

    protected $product;

    protected $user;

    protected $signature = 'cytonn:export-yearly-withdrawals { start } { end } { product_id } { user_id? }';

    protected $description = 'Export CHYS clients who came in & exited the fund since 2014; on a year by year basis.';

    public function __construct()
    {
        parent::__construct();

        $this->info('Initializing...');

        ini_set('memory_limit', '512M');

        $this->start = new Carbon($this->argument('start'));
        $this->end = new Carbon($this->argument('end'));
        $this->user = User::find($this->argument('user_id'));
        $this->product = Product::find($this->argument('product_id'));

        if (is_null($this->start)) {
            $start = Carbon::today()->startOfYear();
        }

        if (is_null($this->argument('end'))) {
            $end = Carbon::today();
        }

        if ($start->copy()->gt($end->copy())) {
            throw new ClientInvestmentException('The end date must be after start date');
        }
    }

    public function fire()
    {
        $file = $this->exportInvestments($this->start->copy(), $this->end->copy(), $this->product);

        $this->info('Sending report to your inbox...');

        $this->mailResult($file, $this->start, $this->end, $this->product, $this->user);

        $this->info('Cleaning up...');

        \File::delete($file);

        $this->info('Command completed.');
    }

    public function exportInvestments(Carbon $start, Carbon $end, Product $product)
    {
        $this->info('Generating report...');

        $fileName = "{$product->name}'s Yearly Investments Withdrawals Export";

        \Excel::create(
            $fileName,
            function ($excel) use ($start, $end, $product) {
                while ($end->gt($start)) {
                    $investments = $this->getInvestments($start->copy(), $start->copy()->endOfYear(), $product);
                    $excel->sheet(
                        "{$start->year}",
                        function ($sheet) use ($investments) {
                            $sheet->loadView('exports.investments.yearly_withdrawals', ['investments' => $investments]);
                        }
                    );
                    $start->addYear();
                }
            }
        )->store('xlsx');

        return storage_path() . '/exports/' . $fileName . '.xlsx';
    }

    private function getInvestments(Carbon $start, Carbon $end, Product $product)
    {
        $query = ClientInvestment::where('product_id', $product->id)
            ->active(false)
            ->activeBetweenDates($start, $end);

        return $query->get()
            ->map(
                function (ClientInvestment $investment) use ($start, $end) {
                    $out = [
                        'Product' => $investment->product->name,
                        'Client Code' => $investment->client->client_code,
                        'Name' => ClientPresenter::presentJointFullNames($investment->client_id),
                        'Principal' => $investment->amount,
                        'Interest rate' => $investment->interest_rate,
                        'Interest Earned' => $investment->repo->getNetInterestForInvestmentAtDate($end),
                        'Gross Interest' => $investment->repo->getGrossInterestForInvestmentAtDate($end),
                        'Invested Date' => DatePresenter::formatDate($investment->invested_date),
                        'Maturity Date' => DatePresenter::formatDate($investment->maturity_date),
                        'Withdrawal Date' => DatePresenter::formatDate($investment->withdrawal_date),
                        'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                        'Amount Withdrawn' => $investment->repo->getWithdrawnAmount($end),
                        'FA' => @$investment->commission->recipient->name,
                        'Tenor' => $investment->repo->getTenorInMonths(),
                        'Rate' => @$investment->commission->rate,
                        'Year' => Carbon::parse($investment->withdrawal_date)->year
                    ];

                    return (new EmptyModel())->fill($out);
                }
            )->groupBy('Year');
    }

    private function mailResult($file_path, Carbon $start, Carbon $end, Product $product, $user = null)
    {
        $mail = Mail::compose()
            ->bcc(['mchaka@cytonn.com', 'tkimathi@cytonn.com'])
            ->from('support@cytonn.com')
            ->text('Here is the export of investments that you requested for the period between ' .
                $start->toFormattedDateString() . ' and ' . $end->toFormattedDateString())
            ->attach($file_path)
            ->subject("{$product->name}'s Yearly Investments Withdrawals Export");

        if ($user) {
            $mail = $mail->to($user->email);
        }

        $mail->send();
    }
}
