<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use App\Cytonn\Models\Client;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\System\Processing\Async;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use App\Cytonn\Models\Product;

class ClientSummaryExport extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:client-summary {start} {end} {--user_id=} {--fm=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export client summary to excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fm = FundManager::findOrFail($this->option('fm'));

        $products = $fm->products()->has('investments')->get();

        $fileName = $fm->name . ' Client Summary';

        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));

        $async = new Async();

        $products = $async->map(
            $products,
            function ($product) use ($start, $end) {
                $clients = (new ClientSummaryExport())->forProduct($product, $start, $end);

                $product->clients_processed = $clients;

                return $product;
            }
        );

        \Excel::create(
            $fileName,
            function ($excel) use ($products, $start, $end) {
                foreach ($products as $product) {
                    $excel->sheet(
                        $product->name,
                        function ($sheet) use ($product, $start, $end) {
                            $sheet->fromModel($product->clients_processed);
                        }
                    );
                }
            }
        )->store('xlsx');

        $path = storage_path().'/exports/'.$fileName.'.xlsx';

        $this->mailResult($path, $start, $end);

        return \File::delete($path);
    }

    public function forProduct($product, $start, $end)
    {
        print("Started processing $product->name".PHP_EOL);

        $clients = Client::whereHas(
            'investments',
            function ($i) use ($start, $end, $product) {
                $i->activeBetweenDates($start, $end)->where('product_id', $product->id);
            }
        )->get();

        $c = $clients->map(
            function (Client $client) use ($product, $end) {
                $total = $client->repo->getTodayTotalInvestmentsValueForProduct($product, $end, true);
                $reinvest = $client->repo->withdrawalsToBeReinvestedForProduct($product, $end);

                $fa = $client->getLatestFA();

                $out = [
                    'Client code' => $client->client_code,
                    'Name' => ClientPresenter::presentJointFullNames($client->id),
                    'First Name' => ClientPresenter::presentJointFirstNames($client->id),
                    'Phone' => $client->contact->phone,
                    'Email' => $client->contact->email,
                    'All Emails' => implode(', ', $client->getContactEmailsArray()),
                    'Country' => is_null($client->country) ? null : $client->country->name,
                    'FA' => $fa? $fa->name: '',
                    'FA Position' => $fa? $fa->type->name: '',
                    'Principal' => $client->repo->getTodayInvestedAmountForProduct($product, $end),
                    'Value' => $total,
                    'Re-investing' => $reinvest,
                    'Total' => $total + $reinvest
                ];

                return (new EmptyModel())->fill($out);
            }
        );

        print("Completed processing $product->name".PHP_EOL);

        return $c;
    }

    private function forFundManager()
    {
        $fm = 2;
        $currency = 1;

        $clients = Client::whereHas(
            'investments',
            function ($investments) use ($fm, $currency) {
                $investments->whereHas(
                    'product',
                    function ($product) use ($fm, $currency) {
                        $product->where('fund_manager_id', $fm)->where('currency_id', $currency);
                    }
                );
            }
        )
            ->get()
            ->map(
                function (Client $client) use ($fm, $currency) {
                    $a = new EmptyModel();

                    $investments = ClientInvestment::whereHas(
                        'product',
                        function ($product) use ($fm, $currency) {
                            $product->where('fund_manager_id', $fm)->where('currency_id', $currency);
                        }
                    )->where('client_id', $client->id)
                    ->activeOnDate(Carbon::today())
                    ->get();

                    $a->fill(
                        [
                        'Client code' => $client->client_code,
                        'Name' => ClientPresenter::presentJointFullNames($client->id),
                        'First Name' => ClientPresenter::presentJointFirstNames($client->id),
                        'Phone' => $client->contact->phone,
                        'Email' => $client->contact->email,
                        'All Emails' => implode(', ', $client->getContactEmailsArray()),
                        'Principal at End of Period' => $investments->sum('amount'),
                        'Value at End of Period' => $investments->sum(
                            function (ClientInvestment $investment) {
                                return $investment->repo->getTotalValueOfAnInvestment();
                            }
                        )
                        ]
                    );

                    return $a;
                }
            );

        $name = 'Coop client summary';

        Excel::fromModel($name, $clients)->store('xlsx');

        $this->sendExcel($name, 'Please find attached an excel sheet for all coop clients', $name);
    }



    /**
     * Send an email with the export
     *
     * @param $file_path
     */
    private function mailResult($file_path, $start, $end)
    {
        $email = [];

        $user = User::find($this->option('user_id'));

        if (!is_null($user)) {
            $email = [$user->email];
        }

        $mailer = new GeneralMailer();
        $mailer->to($email);
        $mailer->bcc([ 'mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Client summary export in excel');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached client summary for clients active between '
            .$start->toFormattedDateString().' and '.$end->toFormattedDateString());
    }
}
