<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Investment\Summary\Analytics as InvestmentAnalytics;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Portfolio\Summary\Analytics;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class AssetLiabilityReconciliation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investments:asset-recon {date} {--currency=} {--user=} {--fm=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assets and Liabilities Reconciliation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currency = Currency::where('code', $this->option('currency'))
            ->orWhere('id', $this->option('currency'))
            ->first();

        $date = Carbon::parse($this->argument('date'));

        return $this->generate($currency, $date);
    }

    private function generate(Currency $currency, Carbon $date)
    {
        $fms = FundManager::where('id', $this->option('fm'))
            ->get();

        $files = $fms->map(
            function (FundManager $fundManager) use ($currency, $date) {
                $result = new \stdClass();

                $result->name = $fundManager->name . '  - converted to ' . $currency->code;

                $p_analytics = new Analytics($fundManager, $date);
                $p_analytics->setBaseCurrency($currency);

                $result->assets = [
                    'Assets' => [
                        'cost_value' => $p_analytics->costValue(),
                        'adjusted_market_value' => $p_analytics->adjustedMarketValue(),
                        'market_value' => $p_analytics->marketValue()
                    ],
                ];

                $balances = $p_analytics->custodialAccountsBalances();

                foreach ($balances as $account => $balance) {
                    $result->assets[$account] = [
                        'cost_value' => $balance,
                        'adjusted_market_value' => $balance,
                        'market_value' => $balance
                    ];
                }

                $result->liabilities = [];

                foreach ($fundManager->products as $product) {
                    $i_analytics = new InvestmentAnalytics($fundManager, $date);
                    $i_analytics->setProduct($product);
                    $i_analytics->setBaseCurrency($currency);

                    $result->liabilities[$product->name] = [
                        'cost_value' => $i_analytics->costValue(),
                        'adjusted_market_value' => $i_analytics->adjustedMarketValue(),
                        'market_value' => $i_analytics->marketValue()
                    ];
                }

                $cashNotTransferred = (new InvestmentAnalytics($fundManager, $date))->clientBalances();

                $result->liabilities['Cash not transferred'] = [
                    'cost_value' => $cashNotTransferred,
                    'adjusted_market_value' => $cashNotTransferred,
                    'market_value' => $cashNotTransferred
                ];

                $assets = collect($result->assets);
                $liabilities = collect($result->liabilities);

                $assets_totals = $result->totals['assets'] = [
                    'cost_value' => $assets->sum('cost_value'),
                    'adjusted_market_value' => $assets->sum('adjusted_market_value'),
                    'market_value' => $assets->sum('market_value')
                ];

                $liability_totals = $result->totals['liabilities'] = [
                    'cost_value' => $liabilities->sum('cost_value'),
                    'adjusted_market_value' => $liabilities->sum('adjusted_market_value'),
                    'market_value' => $liabilities->sum('market_value')
                ];


                $result->totals['surplus'] = [
                    'cost_value' => $assets_totals['cost_value'] - $liability_totals['cost_value'],
                    'adjusted_market_value' =>
                        $assets_totals['adjusted_market_value'] - $liability_totals['adjusted_market_value'],
                    'market_value' => $assets_totals['market_value'] - $liability_totals['market_value']
                ];

                return $result;
            }
        );

        return $this->present($files, $date);
    }

    private function present(Collection $files, Carbon $date)
    {
        $attachments = [];

        foreach ($files as $data) {
            $attachments[$data->name . '.pdf'] = \PDF::loadView(
                'reports.assets_liability_reconciliation',
                ['data' => $data, 'date' => $date]
            )->output();
        }

        $this->mailResult($attachments, $date);
    }

    private function mailResult(array $attachments, Carbon $date)
    {
        $mailer = new GeneralMailer();

        $email = ['mchaka@cytonn.com'];
        $user = User::find($this->option('user'));

        if ($user) {
            $email = $user->email;
        }

        $mailer->to($email);
        $mailer->from('support@cytonn.com');
        $mailer->bcc(config('system.administrators'));
        $mailer->subject('Assets & Liabilities Reconciliation Report - ' . $date->toFormattedDateString());
        $mailer->dataFile($attachments);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find attached the assets and liabilities reconciliation report as at '
            . $date->toFormattedDateString());
    }
}
