<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Mailers\System\InvestmentAuditMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Console\Command;

class InvestmentAudit extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:audit';

    protected $files = [];

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run through investments and check for anomalies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $report = [];
        $report['Duplicate Investments'] = $this->duplicateInvestments();
        $report['Overdrawn Investments'] = $this->checkOverdrawnInvestments();
        $report['Duplicate Shares'] = $this->checkSharePurchaseDupes();
        $report['Missing Interest Payment Schedules'] = $this->missingInterestSchedules();
        $report['Clients with negative cash balances'] = $this->negativeCashBalances();
        $report['UTF Clients With negative Units balances'] = $this->negativeUTFUnitsBalance();

        $counts = collect($report)->map(function ($item, $key) {
            return (object)[
                'name' => $key,
                'count' => $item->count()
            ];
        })->filter(function ($item) {
            return $item->count != 0;
        })->all();

        $this->info("Sending email");

        (new InvestmentAuditMailer())->sendEmail($counts, $this->files);

        $this->info("Completed audit check");
    }

    protected function duplicateInvestments()
    {
        $columns = ['client_id', 'amount', 'invested_date', 'maturity_date'];

        $dupes = ClientInvestment::active()->duplicates($columns)->get();

        $investments = ClientInvestment::allDuplicates($dupes, $columns);

        $investments = $investments->map(
            function ($investment) {
                $out = new EmptyModel();
                $out->id = $investment->id;
                $out->{'Client_code'} = $investment->client->client_code;
                $out->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
                $out->{'Amount'} = $investment->amount;
                $out->{'Value Date'} = $investment->invested_date->toFormattedDateString();
                $out->{'Maturity Date'} = $investment->maturity_date->toFormattedDateString();

                return $out;
            }
        );

        $this->generateExcel($investments, 'Duplicate Investments');

        return $investments;
    }

    protected function checkOverdrawnInvestments()
    {
        $investments = ClientInvestment::statement(\Carbon\Carbon::today())->get();

        $overdrawn = $investments->each(function (ClientInvestment $investment) {
            $calc = $investment->calculate(Carbon::today(), true);
            $investment->principal = $calc->principal();

            $investment->value = $calc->total();
        })->filter(function (ClientInvestment $investment) {
            $qualify1 = ($investment->value < 0) && (abs($investment->value) > 1);
            //only highlight differences of more than a shilling
            $qualify2 = ($investment->principal < 0) && (abs($investment->principal) > 1);

            return $qualify1 || $qualify2;
        })->map(function (ClientInvestment $investment) {
            $i = new EmptyModel();
            $i->client_code = $investment->client->client_code;
            $i->name = \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id);
            $i->amount = $investment->amount;
            $i->value = $investment->value;
            $i->value_date = $investment->invested_date->toDateString();
            $i->final_principal = $investment->principal;
            $i->overdrawn = $investment->value;

            return $i;
        });

        $this->generateExcel($overdrawn, 'Overdrawn Investments');

        return $overdrawn;
    }

    protected function checkSharePurchaseDupes()
    {
        $columns = ['date', 'entity_id', 'category_id', 'purchase_price', 'number', 'client_id'];

        $dupes = SharePurchases::duplicates($columns)->get();

        $duplicates = SharePurchases::allDuplicates($dupes, $columns)
            ->map(
                function ($holding) {
                    $holding = ShareHolding::findOrFail($holding->id);
                    $client = $holding->client;

                    $s = new EmptyModel();
                    $s->Client_Code = $client ? $client->client_code : '';
                    $s->Client_Name = \Cytonn\Presenters\ClientPresenter::presentJointFullNames($holding->client_id);
                    $s->Shares = $holding->number;
                    $s->Purchase_Price = AmountPresenter::currency($holding->number);
                    $s->Entity = $holding->entity ? $holding->entity->name : '';
                    $s->Category = $holding->category->name;
                    $s->Date = DatePresenter::formatDate($holding->date);

                    return $s;
                }
            );

        $this->generateExcel($duplicates, 'Duplicate Share Purchases');

        return $duplicates;
    }

    protected function missingInterestSchedules()
    {
        $invs = ClientInvestment::where('interest_payment_interval', '>', 0)
            ->active()
            ->whereDoesntHave('interestSchedules')
            ->get()
            ->filter(function ($inv) {
                $tenor = $inv->maturity_date->diffInMonths($inv->invested_date);

                return $tenor > $inv->interest_payment_interval;
            })
            ->map(function (ClientInvestment $investment) {
                return (new EmptyModel())->fill([
                    'ID' => $investment->id,
                    'Client Code' => $investment->client->client_code,
                    'Name' => ClientPresenter::presentFullNames($investment->client_id),
                    'Amount' => AmountPresenter::currency($investment->amount),
                    'Value Date' => DatePresenter::formatDate($investment->invested_date),
                    'Maturity' => DatePresenter::formatDate($investment->maturity_date),
                    'Frequency' => $investment->interest_payment_interval . ' Months'
                ]);
            });

        $this->generateExcel($invs, 'Missing Interest Schedules');

        return $invs;
    }

    protected function negativeCashBalances()
    {
        $clients = ClientPayment::with('client', 'client.contact')
            ->whereNotNull('client_id')
            ->get()
            ->filter(function ($payment) {
                return !is_null($payment->client);
            })->groupBy('client_id')
            ->map(function ($payments, $client_id) {
                $client = $payments->first()->client;

                $allowed = $client->allowedMinimumBalance()->where('date', '>=', Carbon::today())->sum('amount');

                return (object)[
                    'client_id' => $client_id,
                    'client' => $client,
                    'payments' => $payments,
                    'balance' => $payments->sum('amount'),
                    'allowed_minimum' => -$allowed
                ];
            })->filter(function ($payments) {
                return $payments->balance <= -1;
            })->map(function ($payment) {
                return (new EmptyModel())->fill([
                    'Code' => $payment->client->client_code,
                    'Client' => ClientPresenter::presentFullNames($payment->client_id),
                    'Balance' => (int)$payment->balance,
                    'Allowed Minimum' => (int)$payment->allowed_minimum
                ]);
            });

        $this->generateExcel($clients, 'Clients with negative cash balances');

        return $clients;
    }
    

    public function negativeUTFUnitsBalance()
    {
        $funds = UnitFund::where('active', true)->get();

        $clients = $funds->map(function ($fund) {
            return Client::whereHas('unitFundPurchases', function ($purchase) use ($fund) {
                $purchase->where('unit_fund_id', $fund->id);
            })
            ->get()
            ->filter(function ($client) use ($fund) {
                return $client->repo->ownedNumberOfUnits($fund) <= -1;
            })
            ->map(function ($client) use ($fund) {
                $bal = (float) $client->repo->ownedNumberOfUnits($fund);
                return (new EmptyModel())->fill([
                    'Code' => $client->client_code,
                    'Client' => ClientPresenter::presentFullNames($client->id),
                    'Unit Fund' => $fund->name,
                    'Balance' => $bal,
                ]);
            });
        })->flatten();

        $this->generateExcel($clients, 'Unit Fund Clients with negative units balances');

        return $clients;
    }

    private function generateExcel(\Illuminate\Support\Collection $models, $filename)
    {
        $this->info("Generating $filename file");

        if (count($models) == 0) {
            return null;
        }

        $filename = str_replace(" ", '_', $filename);

        Excel::fromModel($filename, $models)->store('xlsx');

        $path = storage_path('exports/' . $filename . '.xlsx');

        array_push($this->files, $path);

        return $path;
    }
}
