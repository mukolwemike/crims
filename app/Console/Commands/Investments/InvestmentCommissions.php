<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Crims\Commission\Override;
use Crims\Investments\Commission\RecipientCalculator;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Mailers\GeneralMailer;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class InvestmentCommissions extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'inv:commissions {approval_id} {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $approval_id = $this->argument('approval_id');
        $user_id = $this->argument('user_id');

        $approval = ClientTransactionApproval::find($approval_id);

        $user = User::find($user_id);

        $data = $this->prepareView($approval);

        $filename = 'Commission payments';

        $models = function ($type) use ($data) {
            return CommissionRecepient::where('recipient_type_id', $type->id)->get()->map(
                function ($recipient) use ($data) {
                    $r = new EmptyModel();
                    $r->Name = $recipient->name;

                    foreach ($data['currencies'] as $currency) {
                        $calc = $data['calculate'];
                        $r->{$currency->code} = $calc($recipient, $currency);
                    }

                    return $r;
                }
            );
        };

        \Excel::create(
            $filename,
            function ($excel) use ($models, $data) {
                foreach (CommissionRecipientType::all() as $type) {
                    $excel->sheet(
                        $type->name,
                        function ($sheet) use ($models, $type) {
                            $sheet->fromModel($models($type));
                        }
                    );
                }

                // Compliant Commission
                foreach (CommissionRecepient::all() as $recipient) {
                    $bulk = $data['bulk'];
                    $currencies = (new CommissionRepository())->getCurrenciesForRecipient($recipient);

                    $currencies->each(
                        function ($c) use ($recipient, $bulk) {
                            $calculator = $recipient->calculator($c, $bulk->start, $bulk->end);

                            $summary = $calculator->summary();

                            $c->summary = $summary;
                            $c->calculator = $calculator;
                            $c->total_payable = $summary->finalCommission();
                            $c->total = $summary->finalCommission();
                        }
                    );

                    foreach ($currencies as $currency) {
                        $compliant = $currency->calculator->compliantSchedulesQuery()
                            ->get()
                            ->each(
                                function ($cs) {
                                    $cs->invested_date = $cs->investment->invested_date;
                                }
                            )->sortBy('invested_date');

                        $clawbacks = $currency->calculator->clawBackQuery()->get();

                        $reports = (new Override($recipient))->reports($recipient)
                            ->each(
                                function ($fa) use ($currency, $bulk, $recipient) {
                                    $commission =
                                        $fa->calculator($currency, $bulk->start, $bulk->end)->summary()->total();
                                    $rate = $recipient->rank->override_rate;
                                    $fa->commission = $commission;
                                    $fa->override = $commission * $rate / 100;
                                    $fa->rate = $rate;
                                }
                            )->filter(
                                function ($o) {
                                    return $o->override != 0;
                                }
                            );

                        $override = $recipient->calculator($currency, $bulk->start, $bulk->end)->summary()->override();

                        $excel->sheet(
                            str_limit($recipient->name, 20) . ' - ' . $currency->code,
                            function ($sheet) use ($compliant, $clawbacks, $reports, $override) {
                                $sheet->loadView('exports.commission.investments_schedules', ['schedules' => $compliant,
                                    'claw_backs' => $clawbacks, 'override' => $override, 'reports' => $reports]);
                            }
                        );
                    }
                }
            }
        )->store('xlsx');

        $file_path = storage_path('/exports/' . $filename . '.xlsx');

        $this->mailResult($file_path, $user);

        \File::delete($file_path);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $currencies = $this->currencies();

        $recipients = CommissionRecepient::paginate('10');

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        $calculate = function (CommissionRecepient $recipient, Currency $currency) use ($approval, $bulk) {
            if (!$approval->approved) {
                return (new RecipientCalculator($recipient, $currency, $bulk->start, $bulk->end))
                    ->summary()->finalCommission();
            }

            return $this->getPaymentsMade($recipient, $currency, $approval);
        };

        return ['currencies' => $currencies, 'calculate' => $calculate, 'recipients' => $recipients, 'bulk' => $bulk];
    }

    private function currencies()
    {
        return Currency::whereHas(
            'products',
            function ($products) {
                $products->has('investments');
            }
        )->get();
    }

    private function mailResult($file_path, $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->from('support@cytonn.com');
        $mailer->subject('CRIMS Commission Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Please find the attached Commission Report');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['approval_id', InputArgument::REQUIRED, 'Approval id.'],
            ['user_id', InputArgument::REQUIRED, 'User id.'],
        ];
    }
}
