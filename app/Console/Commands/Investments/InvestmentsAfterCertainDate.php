<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\InstitutionPresenter;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class InvestmentsAfterCertainDate extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'inv:investments-after-certain-date {start}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export clients with investments invested after a certain date';

    /**
     * Create a new command instance.
     * InvestmentsAfterCertainDate constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = new Carbon($this->argument('start'));

        $fileName = 'Investments Invested After '. $start->toFormattedDateString();

        $investments = ClientInvestment::where('invested_date', '>=', $start)
            ->get()
            ->each(
                function ($i) {
                    $i->product_name = $i->product->name;
                }
            )->map(
                function ($investment) {
                    $i = new EmptyModel();

                    $i->{'Client'} = ClientPresenter::presentJointFullNames($investment->client_id);
                    $i->{'Client Code'} = $investment->client->client_code;
                    $i->{'Amount'} = $investment->amount;
                    $i->{'Type'} = ucfirst($investment->investmentType->name);
                    $i->{'Inflow'} = $investment->repo->inflow();
                    $i->{'Interest Rate'} = $investment->interest_rate;
                    $i->{'Invested Date'} = $investment->invested_date;
                    $i->{'Maturity Date'} = $investment->maturity_date;
                    $i->{'FA'} = $investment->commission->recipient->name;
                    $i->{'Tenor'} = $investment->repo->getTenorInMonths();
                    $i->{'Commission Rate'} = $investment->commission->rate;

                    return $i;
                }
            )
            ->groupBy('product_name');

        Excel::fromModel($fileName, $investments)->store('xlsx');

        $this->sendExcel(
            'Investments Invested After ' . $start->toFormattedDateString(),
            'Find attached the export of investments invested after ' . $start->toFormattedDateString(),
            $fileName
        );
        return 0;
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['start', InputArgument::REQUIRED, 'Start Date'],
        ];
    }
}
