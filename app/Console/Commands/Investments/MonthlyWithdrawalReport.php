<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Mailers\Investments\SendDailyWithdrawalReportMailer;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\DailyWithdrawalReportExcelGenerator;
use Illuminate\Console\Command;

class MonthlyWithdrawalReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investments:monthly-withdrawal-and-rollover-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monthly withdrawal and rollover report';

    /**
     * Create a new command instance.
     * MonthlyWithdrawalReport constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->seed();

        $today = Carbon::today()->subDay();

        $start = $today->copy()->startOfMonth();
        $end = $today->copy()->endOfMonth();

        $withdrawals = ClientInvestmentWithdrawal::where('type_id', 1)
            ->where('withdraw_type', 'withdrawal')
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('amount', '>', 0)
            ->get();

        $rollovers = ClientInvestmentWithdrawal::where('type_id', 2)
            ->where('withdraw_type', 'withdrawal')
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('amount', '>', 0)
            ->get()
            ->each(
                function ($inv) {
                    $inv->type = 'Rollover';
                    $inv->withdrawal_date = new Carbon($inv->date);
                    $inv->withdrawn_amount = $inv->investment->repo->getWithdrawnAmount();
                }
            );

        $interest_payments = ClientInvestmentWithdrawal::where('withdraw_type', 'interest')
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('amount', '>', 0)
            ->get();

        if (count($withdrawals) > 0 or $interest_payments->count() or $rollovers->count()) {
            $file_name = 'Monthly Rollover and Withdrawal Report - ' . DatePresenter::formatDate(
                $start->copy()->toDateString()
            ) . ' to ' . DatePresenter::formatDate($end->copy()->toDateString());

            (new DailyWithdrawalReportExcelGenerator())->excel(
                $file_name,
                $withdrawals,
                $interest_payments,
                $rollovers,
                $start->copy(),
                $end->copy(),
                true
            )->store('xlsx');

            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            (new SendDailyWithdrawalReportMailer())->sendEmail($withdrawals, $file_path, $start, $end, true);

            \File::delete($file_path);

            $this->info('Monthly Rollover and Withdrawal Summary has been sent to your inbox');

            return true;
        }

        $this->info('No withdrawals found');

        return false;
    }

    /**
     * Seed the DB
     */
    private function seed()
    {
        ClientInvestment::where('withdrawn', 1)
            ->whereNull('withdrawn_on')
            ->chunk(
                100,
                function ($investments) {
                    foreach ($investments as $investment) {
                        $investment->withdrawn_on = is_null($investment->withdrawApproval)
                            ? null : $investment->withdrawApproval->approved_on;
                        $investment->save();
                    }
                }
            );
    }
}
