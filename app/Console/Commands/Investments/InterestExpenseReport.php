<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class InterestExpenseReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:interest-expense {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a report of the interest expense for a period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = new Carbon($this->argument('start'));

        $end = new Carbon($this->argument('end'));

        $user = User::find($this->argument('user_id'));

        $investments = ClientInvestment::activeBetweenDates($start, $end)->get();

        $total = $investments->count();
        $count = 0;
        $now = Carbon::now();
        $report = $investments->map(
            function (ClientInvestment $investment) use ($start, $end, &$count, $total, $now) {
                $out = new EmptyModel();
                $out->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
                $out->{'Client code'} = $investment->client->client_code;
                $out->{'Product'} = $investment->product->name;
                $out->{'Taxable'} = BooleanPresenter::presentYesNo($investment->client->taxable);
                $out->{'Principal'} = $investment->amount;
                $out->{'Value Date'} = $investment->invested_date;
                $out->{'Maturity Date'} = $investment->maturity_date;
                $out->{'Interest Rate'} = $investment->interest_rate;
                $out->{'Withdrawn'} = BooleanPresenter::presentYesNo(!$investment->repo->active($end));
                $out->{'Withdrawal Date'} = $investment->withdrawal_date;

                for ($date = $start->copy(); $date->lte($end); $date = $date->addMonthNoOverflow()) {
                    $started = $investment->repo->grossInterestBeforeDeductions($date->copy()
                        ->startOfMonth(), false);
                    $ended = $investment->repo->grossInterestBeforeDeductions($date->copy()
                        ->endOfMonth(), true); //use next day

                    $paid = $investment->withdrawals()
                        ->where('date', '>=', $date->copy()->startOfMonth())
                        ->where('date', '<=', $date->copy()->endOfMonth())
                        ->sum('amount');

                    $out->{$date->format('M, Y') . ' Accrued'} = $ended - $started;
                    $out->{$date->format('M, Y') . ' Paid'} = $paid;
                }

                $count++;
                if ($count % 100 == 0) {
                    $t = Carbon::now()->diffForHumans($now, true);
                    $this->info("Processed $count of $total: $t later");
                }

                return $out;
            }
        )->groupBy('Product');

        $fname = 'Monthly Interest Expense';

        Excel::fromModel($fname, $report)->store('xlsx');

        $this->mailResult(storage_path('exports/' . $fname . '.xlsx'), $start, $end, $user);

        return false;
    }

    private function mailResult($file_path, $start, $end, $user = null)
    {
        $mailer = new GeneralMailer();
        if ($user) {
            $email = $user->email;
        } else {
            $email = ['mchaka@cytonn.com'];
        }
        $mailer->to($email);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Interest Expense Report in excel');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Here is the interest accrued between ' . $start . ' and ' . $end);

        \File::delete($file_path);
    }
}
