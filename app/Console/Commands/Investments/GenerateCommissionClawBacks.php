<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/22/18
 * Time: 7:58 PM
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateCommissionClawBacks extends Command
{
    protected $signature = 'crims:generate_commission_clawback';
    
    protected $description = '';

    public function handle()
    {
        DB::transaction(function () {
            $investments = ClientInvestment::whereHas('withdrawals', function ($withdrawal) {
                $withdrawal->ofType('reinvested')->where('withdraw_type', 'withdrawal')
                    ->where('date', '>=', '2018-09-01');
            })->get();

            $investmentArray = array();

            foreach ($investments as $investment) {
                $total_commission = $investment->commission->schedules()->clawBack(false)->sum('amount');

                if ($total_commission == 0) {
                    continue;
                }

                $total_claw_backs = $investment->commission->clawBacks()->sum('amount');

                $commission_paid = abs($total_commission - $total_claw_backs);

                $dif_in_days = Carbon::parse($investment->invested_date)->diffInDays($investment->maturity_date);

                if ($dif_in_days > 365 && $dif_in_days <= 390) {
                    $dif_in_days = 365;
                }

                $commission_rate = $investment->commission->rate;

//                $amount = $investment->calculate($investment->maturity_date->subDay())->total();
                $amount = $investment->amount;

                $actual_commission_amount = ($amount * $commission_rate * $dif_in_days) / (100*365);

                $amount = ($commission_paid - $actual_commission_amount);

                if ($amount > 0) {
                    $this->recordClawBack($investment, $amount);

                    $investmentArray[] = [
                        'Inv ID' => $investment->id,
                        'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                        'Client Code' => $investment->client->client_code,
                        'FA' => $investment->commission->recipient->name,
                        'Principal' => $investment->amount,
                        'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
                        'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                        'Withdrawn' => $investment->withdrawn,
                        'Withdrawal Date' => Carbon::parse($investment->withdrawal_date)->toDateString(),
                        'Scheduled Comm' => $total_commission,
                        'Actual Deserved Commission' => $actual_commission_amount,
                        'Commission Clawed' => $total_claw_backs,
                        'Commission To Be Clawed' => $amount
                    ];
                }

                if (abs($amount) < 1) {
                    continue;
                }
            }

            $fileName = 'Rollover_Clawbacks';

            ExcelWork::generateAndStoreSingleSheet($investmentArray, $fileName);

            $email = [];

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text("Please find attached the client investments rollover clawbacks")
                ->excel([$fileName])
                ->send();
        });

        dd("Done");
    }

    public function recordClawBack(ClientInvestment $investment, $amount)
    {
        $date = Carbon::parse('2018-10-15');

        $withdrawal = $investment->withdrawals()->ofType('reinvested')
            ->where('withdraw_type', 'withdrawal')->first();


        $msg = $withdrawal ? $this->narration($withdrawal, 'rollover') :
            'Clawback for premature investment rollover';

        return CommissionClawback::create([
            'amount' => $amount,
            'commission_id' => $investment->commission->id,
            'date' => $date,
            'narration' => $msg,
            'type' => 'rollover',
        ]);
    }

    private function narration($withdrawal, $type)
    {
        $client = $withdrawal->investment->client;

        $withdrawn = AmountPresenter::currency($withdrawal->amount);
        $principal = $withdrawal->investment->amount;
        $date = DatePresenter::formatDate($withdrawal->date);
        $name = $client->present()->fullName;

        $typeDesc = ($type == 'rollover') ? "Premature rollover" : "Premature withdrawal";

        return "$typeDesc Client: $name Code: {$client->client_code} 
        Principal: $principal Date: $date Amount withdrawn: $withdrawn";
    }
}
