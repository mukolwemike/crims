<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\DatePresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class OverdueInvestmentPaymentScheduleSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:overdue_investment_payment_schedules {date?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the overdue investment payment schedules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('user_id');

        $date = $this->argument('date');

        $date = $date ? Carbon::parse($date) : Carbon::now();

        $report = $this->getInvestmentSchedules($date);

        if (count($report) == 0 && is_null($userId)) {
            return;
        }

        $fileName = 'Overdue Investment Schedules - '. $date->toDateString();

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [config('system.emails.operations_group')];
        }


        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the report for unpaid investment payment schedules that have exceeded 
            6 months since their schedule payment date as at ' . $date->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getInvestmentSchedules(Carbon $date)
    {
        $date = $date->copy()->subMonthNoOverflow(6);

        $schedules = $investmentSchedules = InvestmentPaymentSchedule::where('date', '<=', $date)
            ->paid(false)
            ->whereHas('parentInvestment', function ($investment) {
                $investment->active();
            })->get();

        return $schedules->map(function (InvestmentPaymentSchedule $schedule) {
            $investment = $schedule->parentInvestment;

            return [
                'Schedule Date' => DatePresenter::formatDate($schedule->date),
                'Client Code' => $investment->client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                'Schedule Amount' => AmountPresenter::currency($schedule->amount),
                'Principal' => AmountPresenter::currency($investment->amount),
                'Invested Date' => DatePresenter::formatDate($investment->invested_date),
                'Maturity Date' => DatePresenter::formatDate($investment->maturity_date),
                'Interest Rate' => $investment->interest_rate,
                'Product' => $investment->product->name,
                'Description' => $schedule->description
            ];
        });
    }
}
