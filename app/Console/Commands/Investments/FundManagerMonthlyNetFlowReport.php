<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class FundManagerMonthlyNetFlowReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'fund:monthly_netflow {fund_manager} {year} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a report of the monthly netflow for chys in the specified year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $year = $this->argument('year');

        $fundManagerId = $this->argument('fund_manager');

        $fundManager = FundManager::findOrFail($fundManagerId);

        $productIds = Product::where('fund_manager_id', $fundManagerId)->pluck('id')->toArray();

        $report = $this->getReport($year, $productIds);

        $fileName = $fundManager->name . ' Monthly Netflow - ' . $year;

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the monthly netflow report')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /*
     * Generate the report
     */
    public function getReport($year, $productIds)
    {
        $startDate = Carbon::createFromFormat('Y', $year)->startOfYear();

        $endDate = $startDate->copy()->endOfYear();

        $months = getDateRange($startDate, $endDate);

        $reportArray = array();

        foreach ($productIds as $productId) {
            $product = Product::findOrFail($productId);

            $monthsArray = array();

            $totalArray = [
                'Month' => 'Total',
                'New Investments' => 0,
                'Topups' => 0,
                'Total Inflow' => 0,
                'Client Withdrawals' => 0,
                'Interest Withdrawals' => 0,
                'Total Outflow' => 0,
                'Netflow' => 0
            ];

            foreach ($months as $month) {
                $monthValues = $this->getMonthSummary($month, $product, $totalArray);

                $monthsArray[] = $monthValues['month'];

                $totalArray = $monthValues['total'];
            }

            $monthsArray[] = $totalArray;

            $reportArray[$product->name] = $monthsArray;
        }

        return $reportArray;
    }

    /*
     * Get the data details for the month based on the product
     */
    public function getMonthSummary($month, $product, $totalArray)
    {
        $startOfMonth = Carbon::parse($month)->startOfMonth();

        $endOfMonth = $startOfMonth->copy()->endOfMonth();

        $newInvestments = $this->getInflows($startOfMonth, $endOfMonth, [$product->id], 1);

        $topUps = $this->getInflows($startOfMonth, $endOfMonth, [$product->id], 2);

        $clientWithdrawals = $this->getWithdrawals($startOfMonth, $endOfMonth, [$product->id], [1], ['withdrawal']);

        $interestWithdrawals = $this->getWithdrawals($startOfMonth, $endOfMonth, [$product->id], [1], ['interest']);

        $totalOutflow = $clientWithdrawals + $interestWithdrawals;

        $totalInflow = $newInvestments + $topUps;

        $totalArray['New Investments'] = $newInvestments + $totalArray['New Investments'];
        $totalArray['Topups'] = $topUps + $totalArray['Topups'];
        $totalArray['Total Inflow'] = $totalInflow + $totalArray['Total Inflow'];
        $totalArray['Client Withdrawals'] = $clientWithdrawals + $totalArray['Client Withdrawals'];
        $totalArray['Interest Withdrawals'] = $interestWithdrawals + $totalArray['Interest Withdrawals'];
        $totalArray['Total Outflow'] = $totalOutflow + $totalArray['Total Outflow'];
        $totalArray['Netflow'] = $totalInflow - $totalOutflow + $totalArray['Netflow'];

        $monthArray = [
            'Month' => $startOfMonth->format('F, Y'),
            'New Investments' => $newInvestments,
            'Topups' => $topUps,
            'Total Inflow' => $totalInflow,
            'Client Withdrawals' => $clientWithdrawals,
            'Interest Withdrawals' => $interestWithdrawals,
            'Total Outflow' => $totalOutflow,
            'Netflow' => $totalInflow - $totalOutflow
        ];

        return [
            'month' => $monthArray,
            'total' => $totalArray
        ];
    }

    /**
     * Get the inflows of a specific product on a monthly basis
     *
     * @param  $start
     * @param  $end
     * @param  $productIds
     * @return number
     */
    public function getInflows($start, $end, $productIds = null, $type = null)
    {
        $q = ClientInvestment::where('invested_date', '<=', $end)
            ->where('invested_date', '>=', $start);

        if ($type) {
            $q->where('investment_type_id', $type);
        }

        if ($productIds) {
            $q->whereIn('product_id', $productIds);
        }

        return $q->sum('amount');
    }

    /**
     * Get the monthly product withdrawal either for an FA or the company in total
     *
     * @param  $start
     * @param  $end
     * @param  $rec_id
     * @param  $productIds
     * @return mixed
     */
    public function getWithdrawals(
        $start,
        $end,
        $productIds = null,
        $type = [1, 2],
        $withdrawType = ['withdrawal', 'interest']
    ) {
        $q = ClientInvestmentWithdrawal::whereIn('type_id', $type)
            ->whereIn('withdraw_type', $withdrawType)
            ->where('date', '<=', $end)
            ->where('date', '>=', $start)
            ->whereHas(
                'investment',
                function ($q) use ($productIds) {
                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }
                }
            );

        return $q->sum('amount');
    }
}
