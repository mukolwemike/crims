<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/14/18
 * Time: 2:28 PM
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class BackdateInvestmentTransactions extends Command
{
    protected $signature = 'crims:backdated_transactions {start?} {end?} {user_id?}';

    protected $description = 'Report for backdated reports';

    public function fire()
    {
        $start = $this->argument('start') ? Carbon::parse($this->argument('start')) : Carbon::today();

        $end = ($this->argument('end')) ? Carbon::parse($this->argument('end')) : Carbon::today();

        $user = $this->argument('user_id') ? User::findOrFail($this->argument('user_id')) : null;

        $exportData = $this->transactions($start->copy(), $end->copy());

        $fileName = 'Backdated Investments Transactions between ' . $start->toDateString() . ' to ' .
            $end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($exportData, $fileName);

        $email = $user ? $user->email : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the backdated investment transactions reports')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    public function transactions(Carbon $start, Carbon $end)
    {
        return [
            'Investments' => $this->investmentTransactions($start->copy(), $end->copy()),
            'Withdrawals' => $this->withdrawalTransactions($start->copy(), $end->copy())
        ];
    }

    public function investmentTransactions(Carbon $start, Carbon $end)
    {
        $investments = ClientInvestment::active()
            ->whereBetween('created_at', [$start->startOfDay(), $end->endOfDay()])
            ->where('invested_date', '<', 'created_at')
            ->get();

        $investmentArray = array();

        foreach ($investments as $investment) {
            if (Carbon::parse($investment->invested_date)->startOfDay() <
                Carbon::parse($investment->created_at)->startOfDay()) {
                $investmentArray[] = [
                    'Client Name' => $investment->client->name(),
                    'Client Code' => $investment->client->client_code,
                    'Amount' => AmountPresenter::currency($investment->amount),
                    'Transaction Date' => Carbon::parse($investment->created_at)->toFormattedDateString(),
                    'Effective Date' => Carbon::parse($investment->invested_date)->toFormattedDateString(),
                    'Type' => ucfirst($investment->type->name)
                ];
            }
        }

        return collect($investmentArray);
    }

    public function withdrawalTransactions(Carbon $start, Carbon $end)
    {
        $withdrawals = ClientInvestmentWithdrawal::whereNotNull('approval_id')
            ->where('type_id', '!=', 2)
            ->whereBetween('created_at', [$start->startOfDay(), $end->endOfDay()])
            ->where('date', '<', 'created_at')
            ->get();

        $withdrawalArray = array();

        foreach ($withdrawals as $withdrawal) {
            if (Carbon::parse($withdrawal->date)->startOfDay() <
                Carbon::parse($withdrawal->created_at)->startOfDay()) {
                $withdrawalArray[] = [
                    'Client Name' => $withdrawal->investment->client->name(),
                    'Client Code' => $withdrawal->investment->client->client_code,
                    'Amount' => AmountPresenter::currency($withdrawal->amount),
                    'Transaction Date' => Carbon::parse($withdrawal->created_at)->toFormattedDateString(),
                    'Effective Date' => Carbon::parse($withdrawal->date)->toFormattedDateString(),
                    'Type' => ucfirst($withdrawal->withdraw_type)
                ];
            }
        }

        return collect($withdrawalArray);
    }
}
