<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Mailers\Investments\SendDailyWithdrawalReportMailer;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\DailyWithdrawalReportExcelGenerator;
use Illuminate\Console\Command;

class DailyWithdrawalReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:withdrawal-report {user_id?} {start?} {end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily withdrawal report.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->seed();
        $today = Carbon::today()->subDay()->subDay();

        $user = ($this->argument('user_id')) ? $this->argument('user_id') : null;


        $start = ($this->argument('start'))
            ? Carbon::parse($this->argument('start')) : $today->copy()->startOfDay();
        $end = ($this->argument('end')) ? Carbon::parse($this->argument('end')) : $today->copy()->endOfDay();

//        $monthly = is_null($this->option('start')) ? false : true;
        $monthly = is_null($this->argument('start')) ? false : true;

        $withdrawals = ClientInvestmentWithdrawal::where('type_id', 1)
            ->where('withdraw_type', 'withdrawal')
            ->where('amount', '>', 0)
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->get();

        $rollovers = ClientInvestmentWithdrawal::where('type_id', 2)
            ->where('withdraw_type', 'withdrawal')
            ->where('amount', '>', 0)
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->get()
            ->each(
                function ($inv) {
                    $inv->type = 'Rollover';
                    $inv->withdrawal_date = new Carbon($inv->date);
                    $inv->withdrawn_amount = $inv->investment->repo->getWithdrawnAmount();
                }
            );

        $interest_payments = ClientInvestmentWithdrawal::where('withdraw_type', 'interest')
            ->where('date', '>=', $start)
            ->where('amount', '>', 0)
            ->where('date', '<=', $end)
            ->get();

        if (count($withdrawals) > 0 or $interest_payments->count() or $rollovers->count()) {
            $file_name =
                'Daily Rollover and Withdrawal Report - ' . DatePresenter::formatDate($today->copy()->toDateString());

            if ($monthly) {
                $file_name = 'Monthly Rollover and Withdrawal Report - ' .
                    DatePresenter::formatDate($start->copy()->toDateString()) . ' to ' .
                    DatePresenter::formatDate($end->copy()->toDateString());
            }

            (new DailyWithdrawalReportExcelGenerator())->excel(
                $file_name,
                $withdrawals,
                $interest_payments,
                $rollovers,
                $start->copy(),
                $end->copy(),
                $monthly
            )->store('xlsx');

            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            (new SendDailyWithdrawalReportMailer())->sendEmail($withdrawals, $file_path, $start, $end, $monthly, $user);

            \File::delete($file_path);

            $monthly ? $this->info('Monthly Rollover and Withdrawal Summary has been sent to your inbox')
                : $this->info('Daily Rollover and Withdrawal Summary has been sent to your inbox');

            return true;
        }

        $this->info('No withdrawals found');

        return false;
    }

    private function seed()
    {
        ClientInvestment::where('withdrawn', 1)
            ->whereNull('withdrawn_on')
            ->chunk(
                100,
                function ($investments) {
                    foreach ($investments as $investment) {
                        $investment->withdrawn_on = is_null($investment->withdrawApproval)
                            ? null
                            : $investment->withdrawApproval->approved_on;
                        $investment->save();
                    }
                }
            );
    }
}
