<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Reporting\ClientStatementGenerator;
use Illuminate\Console\Command;

class ExportClientStatements extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:export-statements {product_id} {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export client statements for a product to excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //        $pids = explode(',', $this->argument('product_id'));
        //        $products = Product::whereIn('id', $pids)->get();
        $product = Product::findOrFail($this->argument('product_id'));
        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));
        $userId = $this->argument('user_id');
        $email = [];

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = [$user->email];
        }

        $fname = $product->name . ' : Statements from ' . $start->toFormattedDateString() . ' to ' .
            $end->toFormattedDateString();

        \Excel::create(
            $fname,
            function ($excel) use ($product, $start, $end) {
                $clients = Client::whereHas(
                    'investments',
                    function ($investments) use ($product, $start, $end) {
                        $investments->where('product_id', $product->id)
                            ->activeBetweenDates($start, $end);
                    }
                )->orderBy('client_code', 'ASC')
                    ->get();

                foreach ($clients as $client) {
                    $name = str_limit($client->client_code . ' ' .
                        \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id), 28);

                    $excel->sheet(
                        $name,
                        function ($sheet) use ($client, $product, $start, $end) {
                            (new ClientStatementGenerator())->excelSheet($sheet, $product, $client, $end, $start);

                            $sheet->setColumnFormat(
                                [
                                    'B' => '#,##0.00',
                                    'C' => '0%',
                                    'D' => 'dd/mm/yy',
                                    'E' => '#,##0.00',
                                    'F' => '#,##0.00',
                                    'G' => '#,##0.00',
                                    'H' => '#,##0.00',
                                    'I' => 'dd/mm/yy',
                                ]
                            );
                        }
                    );
                }
            }
        )->store('xlsx');

        $this->sendExcel(
            $product->name . ' : Client Statements Export',
            "Please find attached an excel workbook containing the client statements from " .
            $start->toFormattedDateString() . " to " . $end->toFormattedDateString(),
            $fname,
            $email
        );

        return;
    }
}
