<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class InvestmentMaturityRolloverSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:investment_maturity_rollover_summary {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the investment maturities for the selected period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));
        
        $report = $this->getClientMaturies($start, $end);

        $fileName = 'Client Maturities Analysis';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the client maturities analysis between ' .
                $start->toDateString() . ' and ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    private function getClientMaturies(Carbon $start, Carbon $end)
    {
        $investments = ClientInvestment::where('maturity_date', '>=', $start)
            ->where('maturity_date', '<=', $end)
            ->activeBetweenDates($start, $end)
            ->get();

        $investmentArray = array();
        $summaryArray = array();

        $today = Carbon::now();

        foreach ($investments as $investment) {
            $productName = $investment->product->name;


            $valueAtMaturity = $investment->repo->getTotalValueOfInvestmentAtDate(
                $investment->maturity_date->subDay(),
                true
            );

            $dataArray = [
                'ID' => $investment->id,
                'Client Code' => $investment->client->client_code,
                'Name' => ClientPresenter::presentJointFullNames($investment->client_id),
                'Product' => $investment->product->name,
                'Principal' => (float)$investment->amount,
                'Value Date' => Carbon::parse($investment->invested_date)->toDateString(),
                'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                'Value at Maturity' => $valueAtMaturity,
                'FA' => $investment->commission->recipient->name,
                'FA Position' => $investment->commission->recipient->present()->getCurrentRank,
                'FA Department Unit' => $investment->commission->recipient->present()->getDepartmentUnit,
                'FA Branch' => $investment->commission->recipient->present()->getBranch,
                'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                'Rolled' => BooleanPresenter::presentYesNo($investment->rolled)
            ];

            if ($investment->withdrawn == 1) {
                $amountWithdrawn = $this->getWithdrawalAmount($investment, 1, $start, $end);
                $amountRolled = $this->getWithdrawalAmount($investment, 2, $start, $end);
                $currentValue = $investment->repo->getTotalValueOfInvestmentAtDate($today);
                $topupAmount = $this->getTopupAmount($investment, $start, $end);
                $amountBulkRolled = $this->getBulkRolloverAmount($investment, $start, $end);
            } else {
                $amountWithdrawn = $amountRolled = $amountBulkRolled = $topupAmount = 0;
                $currentValue = $investment->repo->getTotalValueOfInvestmentAtDate($today);
            }

            $dataArray['Amount Withdrawn'] = $amountWithdrawn;
            $dataArray['Amount Rolled'] = $amountRolled;
            $dataArray['Topup Amount'] = $currentValue;
            $dataArray['Not Processed'] = $currentValue;

            $investmentArray[$productName][] = $dataArray;

            if (isset($summaryArray[$productName])) {
                $summaryArray[$productName]['Value At Maturity'] = $summaryArray[$productName]['Value At Maturity']
                    + $valueAtMaturity;
                $summaryArray[$productName]['Amount Withdrawn'] = $summaryArray[$productName]['Amount Withdrawn']
                    + $amountWithdrawn;
                $summaryArray[$productName]['Amount Rolled'] = $summaryArray[$productName]['Amount Rolled']
                    + $amountRolled;
                $summaryArray[$productName]['Topup Amount'] = $summaryArray[$productName]['Topup Amount']
                    + $topupAmount;
                $summaryArray[$productName]['Not Processed'] = $summaryArray[$productName]['Not Processed']
                    + $currentValue;
                $summaryArray[$productName]['Amount Bulk Rolled'] = $summaryArray[$productName]['Amount Bulk Rolled']
                    + $amountBulkRolled;
            } else {
                $summaryArray[$productName] = [
                    'Product Name' => $productName,
                    'Value At Maturity' => $valueAtMaturity,
                    'Amount Withdrawn' => $amountWithdrawn,
                    'Amount Rolled' => $amountRolled,
                    'Amount Bulk Rolled' => $amountBulkRolled,
                    'Topup Amount' => $topupAmount,
                    'Not Processed' => $currentValue
                ];
            }
        }

        foreach ($summaryArray as $key => $summary) {
            $summaryArray[$key]['Rollover Percentage'] =
                percentage($summary['Amount Rolled'], $summary['Value At Maturity']);
        }

        $investmentArray['Summary'] = $summaryArray;

        return $investmentArray;
    }

    /**
     * @param ClientInvestment $investment
     * @param $type
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    private function getWithdrawalAmount(ClientInvestment $investment, $type, Carbon $start, Carbon $end)
    {
        return ClientInvestmentWithdrawal::where('investment_id', $investment->id)
            ->where('type_id', $type)->where('withdraw_type', 'withdrawal')
            ->between($start, $end)
            ->sum('amount');
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    private function getBulkRolloverAmount(ClientInvestment $investment, Carbon $start, Carbon $end)
    {
        return ClientInvestmentWithdrawal::where('investment_id', $investment->id)
            ->where('withdraw_type', 'withdrawal')
            ->where('type_id', 2)
            ->between($start, $end)
            ->whereHas('approval', function ($q) {
                $q->where('transaction_type', 'bulk_rollover_investment');
            })->sum('amount');
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    private function getTopupAmount(ClientInvestment $investment, Carbon $start, Carbon $end)
    {
        return ClientInvestmentTopup::where('topup_to', $investment->id)
            ->where('date', '>=', $start)->where('date', '<=', $end)
            ->sum('amount');
    }
}
