<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ContactPresenter;
use Cytonn\Presenters\InvestmentPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class RefreshInterestPaymentSchedules extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:refresh_interest_payment_schedules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh the interest payment schedules';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set('memory_limit', '512M');

        $investments = ClientInvestment::active()->whereHas(
            'interestSchedules',
            function ($q) {
            }
        )->get();

        $investmentArray = array();

        foreach ($investments as $investment) {
            $firstSchedule = $investment->interestSchedules()->orderBy('date')->first();

            $newSchedules = $this->getNewSchedules($investment);

            if (!$this->compareFirstDates($firstSchedule, $newSchedules, $investment)) {
                $investmentArray[] = [
                    'ID' => $investment->id,
                    'Client' => ContactPresenter::fullName($investment->client->contact_id),
                    'Amount' => $investment->amount,
                    'Tenor' => InvestmentPresenter::tenorInMonths($investment->id),
                    'Invested Date' => $investment->invested_date->toDateString(),
                    'Maturity Date' => $investment->maturity_date->toDateString(),
                    'Old Interest Start' => Carbon::parse($firstSchedule->date)->toDateString(),
                    'New Interest Start' => (count($newSchedules) > 0)
                        ? $newSchedules[0]['date']->toDateString() : null,
                    'Interest Payment Interval' =>
                        $investment->interest_payment_interval . ' ' . str_plural('Month'),
                    'Interest Payment Date' => $investment->interest_payment_date
                ];
            }
        }

        ExcelWork::generateAndStoreSingleSheet($investmentArray, 'Interest_Payment_Schedules');

        Mailer::compose()
            ->to('tkimathi@cytonn.com')
            ->subject('Interest Payment Schedules')
            ->text('Please find attached list of investments with interest payment schedules
             that need to be modified')
            ->excel(['Interest_Payment_Schedules'])
            ->send();
    }

    /*
     * Generate the new schedules
     */
    public function getNewSchedules($investment)
    {
        if (is_null($investment->interest_payment_date)) {
            $investment->interest_payment_date = 31;
        }

        $endMonth = $investment->interest_payment_date == 31 || $investment->interest_payment_date == 0;
        $amountPaid = 0;

        $date = $endMonth ? null : $investment->interest_payment_date;

        $startDate = ($investment->interest_payment_start_date)
            ? Carbon::parse($investment->interest_payment_start_date) : $investment->invested_date;

        $schedules = $this->schedule(
            $startDate,
            $investment->maturity_date,
            $investment->interest_payment_interval,
            $investment->invested_date,
            $endMonth,
            $date
        );

        return $schedules;
    }

    public function schedule(Carbon $start, Carbon $end, $interval, $investedDate, $endMonth = false, $date = null)
    {
        $schedules = [];
        $run_date = $this->firstPaymentDate($start);
        $run_date = $run_date->addMonthsNoOverflow($interval - 1)->copy();
        $counter = 1;

        if ($date) {
            $run_date = $run_date->startOfMonth()->addDays($date - 1);
        }

        if ($endMonth) {
            $run_date = $run_date->endOfMonth();
        }

        while ($end->gt($run_date) && $counter == 1) {
            $schedules[] = [
                'date' => $run_date->copy(),
                'amount' => 0,
                'description' => 'Interest payment after ' . ($run_date->diffInMonths($investedDate) + 1) . ' months'
            ];

            $run_date = $run_date->addMonthsNoOverflow($interval);
            if ($endMonth) {
                $run_date = $run_date->endOfMonth();
            }

            $counter++;
        }

        return $schedules;
    }

    private function firstPaymentDate($date)
    {
        return $date->day > 25
            ? $date->copy()->addMonthNoOverflow()
            : $date->copy();
    }

    public function compareFirstDates($first, $newFirst, $investment)
    {
        if (is_null($first) || count($newFirst) == 0) {
            return false;
        }

        $old = Carbon::parse($first->date);

        $new = Carbon::parse($newFirst[0]['date']);

        //        && $old->lt(Carbon::now())
        if ($old->toDateString() != $new->toDateString()) {
            return false;
        }

        return true;
    }
}
