<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Excels\ExcelWork;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class InvestmentsExportCommand extends Command
{
    protected $end_date;

    protected $start_date;

    protected $count = 0;

    protected $signature = 'investments:export-investment { start } { end } { --user_id= } {--fund=}';

    protected $description = 'Export investments and send via email';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws ClientInvestmentException
     * @throws \Exception
     */
    public function fire()
    {
        $start = new Carbon($this->argument('start'));
        $end = new Carbon($this->argument('end'));
        $user = User::find($this->option('user_id'));
        $fund = UnitFund::findOrFail($this->option('fund'));

        if (is_null($this->argument('start'))) {
            $start = Carbon::today()->startOfYear();
        }

        if (is_null($this->argument('end'))) {
            $end = Carbon::today();
        }

        if ($start->copy()->gt($end->copy())) {
            throw new ClientInvestmentException('The end date must be after start date');
        }

        $files = [$this->exportInvestments($start, $end, $fund)];

        $this->mailResult($files, $fund, $start, $end, $user);

        foreach ($files as $file) {
            \File::delete($file);
        }
    }

    /**
     * @param $start
     * @param $end
     * @return string
     * @throws \Exception
     */
    public function exportInvestments($start, $end, $fund)
    {
        ini_set('memory_limit', '512M');
        $this->end_date = $end;

        $this->start_date = $start;

        $client_investments = $this->getInvestments($start, $end, $fund);

        print("Received all client investments" . PHP_EOL);

        $fileName = 'Investments export';

        ExcelWork::generateAndStoreMultiSheet($fileName, $client_investments);

        return storage_path() . '/exports/' . $fileName . '.xlsx';
    }



    /**
     * @param $start
     * @param $end
     * @return mixed
     * @throws \Exception
     */
    private function getInvestments($start, $end, $fund)
    {
        print("Export client investments" . PHP_EOL);

        $query = ClientInvestment::activeBetweenDates($start, $end)->with(
            'withdrawals',
            'client',
            'topupsTo',
            'commission',
            'commission.recipient',
            'investmentType',
            'product'
        )->forFund($fund);

        $timed = Carbon::now();

        $total = $query->count();

        print("Counted $total investments" . PHP_EOL);

        $investments = $query->get();

        cache()->put('just_a_test', 1, Carbon::now()->addDay());

        return $investments->map(function (ClientInvestment $investment) use ($start, $end, $total, $timed) {
            $product = $investment->product->name;

            $out = [
                'Product' => $product,
                'Client Code' => $investment->client->client_code,
                'Name' => \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id),
                'Amount' => $investment->amount,
                'Type' => ucfirst($investment->investmentType->name),
                'Inflow' => $investment->repo->inflow(),
                'Interest rate' => $investment->interest_rate,
                'Invested Date' => DatePresenter::formatDate($investment->invested_date),
                'Maturity Date' => DatePresenter::formatDate($investment->maturity_date),
                'Withdrawal Date' => DatePresenter::formatDate($investment->withdrawal_date),
                'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                'Amount Withdrawn' => $investment->repo->getWithdrawnAmount($end),
                'To Be Reinvested' => $reinvest = $investment->repo->withdrawalToBeReinvested($end),
                'Total' => $investment->repo->getTotalValueOfInvestmentAtDate($end, true) + $reinvest,
                'Top up' => $investment->repo->topupAmount(),
                'FA' => @$investment->commission->recipient->name,
                'Tenor' => $investment->repo->getTenorInMonths(),
                'Rate' => @$investment->commission->rate
            ];

            $count = cache()->get('just_a_test');

            if ($count % 100 == 0 || $count == $total) {
                $diff = Carbon::now()->diffForHumans($timed);
                print("Time: $diff Now at $count/$total...." . PHP_EOL);
            }

            cache()->increment('just_a_test');

            return (new EmptyModel())->fill($out);
        })->groupBy('Product');
    }



    /**
     * @param $file_path
     * @param Carbon $start
     * @param Carbon $end
     * @param null $user
     */
    private function mailResult($file_path, $fund, Carbon $start, Carbon $end, $user = null)
    {
        $mail = Mail::compose()
            ->bcc(config('system.administrators'))
            ->from('support@cytonn.com')
            ->text('Here is the export of investments that you requested for the period between ' .
                $start->toFormattedDateString() . ' and ' . $end->toFormattedDateString())
            ->attach($file_path)
            ->subject('Client Investment Exports - '.$fund->name);

        if ($user) {
            $mail = $mail->to($user->email);
        }

        $mail->send();
    }
}
