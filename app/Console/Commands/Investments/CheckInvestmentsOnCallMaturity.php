<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Portfolio\DepositHolding;
use Carbon\Carbon;
use Cytonn\Mailers\Investments\InvestmentsOnCallMailer;
use Illuminate\Console\Command;

class CheckInvestmentsOnCallMaturity extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:on-call';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if investment will mature within 2 weeks and increase maturity date by a month';

    /**
     * Create a new command instance.
     * CheckPortfolioInvestmentsOnCallMaturity constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $two_weeks_from_now = Carbon::today()->addWeeks(2);

        $depositHolding_investments = DepositHolding::where('maturity_date', '<=', $two_weeks_from_now)
            ->where('withdrawn', '<>', 1)
            ->where('on_call', 1)
            ->get()
            ->map(
                function ($investment) {
                    $investment->update(
                        [
                        'maturity_date'         =>  (new Carbon($investment->maturity_date))->addMonth()
                        ]
                    );
                    $investment->old_maturity_date = $investment->maturity_date;
                    return $investment;
                }
            );
        $client_investments = ClientInvestment::where('maturity_date', '<=', $two_weeks_from_now)
            ->where('withdrawn', '<>', 1)
            ->where('on_call', 1)
            ->get()
            ->map(
                function ($investment) {
                    $investment->update(
                        [
                        'maturity_date'         =>  (new Carbon($investment->maturity_date))->addMonth()
                        ]
                    );
                    $investment->old_maturity_date = $investment->maturity_date;
                    return $investment;
                }
            );

        if ($depositHolding_investments->count() > 0 || $client_investments->count() > 0) {
            (new InvestmentsOnCallMailer())->sendEmail($depositHolding_investments, $client_investments);
        }
    }
}
