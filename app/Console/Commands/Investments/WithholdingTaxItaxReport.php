<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Product;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\User;
use App\Cytonn\Models\WithholdingTax;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class WithholdingTaxItaxReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:withholding_tax_itax_report {start} {end} {product_id?} {unit_fund_id?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a report for the wht itax upload';

    /**
     * Create a new command instance.
     * MigratePortfolioData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $product = $this->argument('product_id') ? Product::findOrFail($this->argument('product_id'))
            : null;

        $unitFund = $this->argument('unit_fund_id') ? UnitFund::findOrFail($this->argument('unit_fund_id'))
            : null;

        $report = $this->getReport($start, $end, $product, $unitFund);

        $fileName = 'Withholding Tax Itax Report - ' . $end->toDateString();

        Excel::create($fileName, function ($excel) use ($report, $fileName) {
            $excel->sheet(
                trimText($fileName, 30, false),
                function ($sheet) use ($report) {
                    $sheet->fromArray($report);
                }
            );
        })->store('csv', storage_path() . '/exports/');

        $userId = $this->argument('user_id');

        $email = [];
        if ($userId) {
            $user = User::findOrFail($userId);

            $email[] = $user->email;
        }

        $path = storage_path() . '/exports/' . $fileName . '.csv';

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the requested withholding tax itax report for the period between '
                . $start->toDateString() . " and " . $end->toDateString())
            ->attach([$path])
            ->send();

        return \File::delete($path);
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param Product|null $product
     * @param UnitFund|null $unitFund
     * @return array
     */
    private function getReport(Carbon $start, Carbon $end, Product $product = null, UnitFund $unitFund = null)
    {
        $dataArray = array();

        $taxes = $this->getTaxes($start, $end, $product, $unitFund);

        foreach ($taxes as $tax) {
            if ($tax->withdrawal) {
                $client = $tax->withdrawal->investment->client;
                $transactionType = $this->getWithdrawalType($tax->withdrawal);
            } else {
                $client = $tax->unitFundSale->client;
                $transactionType = $this->getUnitSaleType($tax->unitFundSale);
            }

            $dataArray[] = [
                'Nature of Transaction' => $transactionType,
                'Residential Status' => $client->present()->getResidency,
                'Date of Payment to Withholdee' => Carbon::parse($tax->date)->format('d/m/Y'),
                'Pin of Withholdee' => $client->pin_no,
                'Gross Amount' => (float)$tax->gross_interest_amount
            ];
        }

        return $dataArray;
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param Product|null $product
     * @param UnitFund|null $unitFund
     * @return mixed
     */
    private function getTaxes(Carbon $start, Carbon $end, Product $product = null, UnitFund $unitFund = null)
    {
        $taxes = WithholdingTax::between($start, $end)->orderBy('date');

        if ($unitFund) {
            return $taxes->whereHas('unitFundSale', function ($q) use ($unitFund) {
                $q->where('unit_fund_id', $unitFund->id);
            })->get();
        } elseif ($product) {
            return $taxes->whereHas('withdrawal', function ($q) use ($product) {
                $q->whereHas('investment', function ($q) use ($product) {
                    $q->where('product_id', $product->id);
                });
            });
        } else {
            return $taxes->get();
        }
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @return string
     */
    private function getWithdrawalType(ClientInvestmentWithdrawal $withdrawal)
    {
        return $withdrawal->withdraw_type == "interest" ? "Interest Payment" : "Withdrawal Payment";
    }

    /**
     * @param UnitFundSale $sale
     * @return string
     */
    private function getUnitSaleType(UnitFundSale $sale)
    {
        return $sale->sale_type == "interest" ? "Interest Payment" : "Units Sale";
    }
}
