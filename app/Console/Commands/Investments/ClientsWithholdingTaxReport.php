<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/25/18
 * Time: 1:04 PM
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Investment\Tax\GenerateWithholdingTax;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\ExcelMailer;
use Illuminate\Console\Command;

class ClientsWithholdingTaxReport extends Command
{
    use ExcelMailer;

    protected $signature = 'investments:client_tax {start} {end} {user_id?} {product_id?} {unit_fund_id?}';

    protected $description = 'Export a report of clients withholding tax';

    public function fire()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $userId = $this->argument('user_id');

        $user = $userId ? User::find($userId) : null;

        $productId = $this->argument('product_id');

        $product = $productId ? Product::find($productId) : null;

        $fund = $this->argument('unit_fund_id');

        $fund = $fund ? UnitFund::findOrFail($fund) : null;

        (new GenerateWithholdingTax($user, $start->copy(), $end->copy(), $product, null, $fund))->send();
    }
}
