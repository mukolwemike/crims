<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Commission\Generator;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AdditionalRolloverCommissionAward extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:additional_rollover_commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Award additional rollover commission';
    protected $investmentArray = [];
    protected $oldCommission = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        DB::transaction(function () {
            $investments = ClientInvestment::where('invested_date', '>=', '2018-09-17')
                ->where('investment_type_id', 3)
                ->get();

            $generator = new Generator();

            foreach ($investments as $investment) {
                $tenor = $this->getTenorInDays($investment);

                $commission = $investment->commission;

                if ($tenor == 90 && in_array($commission->rate, [0.5, 1])) {
                    $commission = $investment->commission;
                    $this->oldCommission = $commission->rate;

                    $oldRate = $commission->rate;

                    //Update the rate to a new one
                    $commission->rate = $oldRate * 1.2;
                    $commission->save();

                    $bonusAmount = $this->computeNewCommission($investment, $oldRate, $tenor);

                    if ($investment->withdrawn) {
                        $this->makeOneTimePayment($investment, $bonusAmount);
                    } else {
                        if ($this->noPaidCommission($investment)) {
                            $generator->recreateSchedules($investment);

                            $this->investmentArray[] = [
                                'Inv ID' => $investment->id,
                                'Client Code' => $investment->client->client_code,
                                'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                                'FA' => @$investment->commission->recipient->name,
                                'Amount' => $investment->amount,
                                'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
                                'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                                'Withdrawal Date' => $investment->withdrawal_date ?
                                    Carbon::parse($investment->withdrawal_date)->toDateString() :
                                    '',
                                'Tenor In Days' => $this->getTenorInDays($investment),
                                'Old Rate' => $this->oldCommission,
                                'Commission Rate' => $investment->commission->rate,
                                'Additional Commission Amount' => "No commission paid. Regenerate new schedules",
                                'Date' => '',
                                'Description' => '',
                            ];
                        } else {
                            if ($tenor >= 365) {
                                $this->makeOneTimePayment($investment, $bonusAmount);
                            } else {
                                $this->makeStaggeredPayment($investment, $bonusAmount);
                            }
                        }
                    }

                    $this->oldCommission = null;
                }
            }

            $fileName = 'Rollover Commission';

            ExcelWork::generateAndStoreSingleSheet($this->investmentArray, $fileName);

            Mailer::compose()
                ->to([])
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the rollover commission')
                ->excel([$fileName])
                ->send();

            $path = storage_path().'/exports/' . $fileName .'.xlsx';

            \File::delete($path);

            dd("Done");
        });
    }

    private function getTenorInDays(ClientInvestment $investment)
    {
        $maturityDate = $investment->withdrawal_date ? Carbon::parse($investment->withdrawal_date) :
            Carbon::parse($investment->maturity_date);

        return Carbon::parse($investment->invested_date)->diffInDays($maturityDate);
    }

    protected function noPaidCommission(ClientInvestment $investment)
    {
        $bulk = BulkCommissionPayment::orderBy('end', 'DESC')->first();

        if (is_null($bulk)) {
            return true;
        }

        return $investment->commission->schedules()->where('date', '<=', $bulk->end)->count() == 0;
    }

    private function computeNewCommission(ClientInvestment $investment, $oldRate, $tenor)
    {
        return round($investment->amount * $oldRate * 0.2 * $tenor / (100 * 365), 2);
    }

    private function makeOneTimePayment(ClientInvestment $investment, $bonusAmount)
    {
        $this->saveSchedule($investment, [
            'commission_id' => $investment->commission->id,
            'date' => Carbon::parse('2018-12-16'),
            'amount' => $bonusAmount,
            'description' => 'Rollover bonus commission',
            'first' => false
        ]);
    }

    private function makeStaggeredPayment(ClientInvestment $investment, $bonusAmount)
    {
        $months = getDateRange(Carbon::parse('2018-12-16'), Carbon::parse($investment->maturity_date));

        $monthsCount = count($months);

        if ($monthsCount == 0) {
            return $this->makeOneTimePayment($investment, $bonusAmount);
        }

        $firstAmount = round($bonusAmount / 2, 2);

        $staggerAmount = $bonusAmount - $firstAmount;

        $monthlyStaggerAmount = round(($staggerAmount) / $monthsCount, 2);

        $paid = 0;

        foreach ($months as $key => $month) {
            if ($key == 0) {
                $this->saveSchedule($investment, [
                    'commission_id' => $investment->commission->id,
                    'date' => $month,
                    'amount' => $firstAmount,
                    'description' => 'Additional Upfront Rollover commission',
                    'first' => false
                ]);
            }

            $amountToStagger = $monthsCount == $key + 1 ? $staggerAmount - $paid : $monthlyStaggerAmount;

            $this->saveSchedule($investment, [
                'commission_id' => $investment->commission->id,
                'date' => $month,
                'amount' => $amountToStagger,
                'description' => 'Additional Rollover Commission installment : ' . ($key + 1),
                'first' => false
            ]);

            $paid += $amountToStagger;
        }
    }

    private function saveSchedule(ClientInvestment $investment, $data)
    {
        $schedule = CommissionPaymentSchedule::create($data);

        $this->investmentArray[] = [
            'Inv ID' => $investment->id,
            'Client Code' => $investment->client->client_code,
            'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
            'FA' => @$investment->commission->recipient->name,
            'Amount' => $investment->amount,
            'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
            'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
            'Withdrawal Date' => $investment->withdrawal_date ?
                Carbon::parse($investment->withdrawal_date)->toDateString() : '',
            'Tenor In Days' => $this->getTenorInDays($investment),
            'Old Rate' => $this->oldCommission,
            'Commission Rate' => $investment->commission->rate,
            'Additional Commission Amount' => $schedule->amount,
            'Date' => Carbon::parse($schedule->date)->toDateString(),
            'Description' => $schedule->description
        ];
        return $schedule;
    }
}
