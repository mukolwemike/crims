<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class WithdrawalsReport extends Command
{
    use WithdrawalsTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:withdrawals {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A report of all withdrawals between some certain dates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));
        $userId = $this->argument('user_id');

        $withdrawals = ClientInvestmentWithdrawal::where('date', '>=', $start)
            ->with([
                'investment', 'investment.client', 'investment.client.contact', 'investment.commission',
                'investment.commission.recipient', 'investment.product'
            ])
            ->where('date', '<=', $end)
            ->where('amount', '>', 0)
            ->where('type_id', '=', 1)
            ->oldest('date')
            ->get();

        $withdrawalsArray = array();

        foreach ($withdrawals as $withdrawal) {
            $investment = $withdrawal->investment;

            $withdrawalDate = Carbon::parse($withdrawal->date);

            $recipient = @$investment->commission->recipient;

            $position = @$recipient->repo->getRecipientUserPositionByDate($withdrawalDate);

            $withdrawalsArray[$investment->product->name][] = [
                'Withdrawn On' => $withdrawalDate->toFormattedDateString(),
                'Client Code' => $investment->client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                'Client Email' => $investment->client->contact->email,
                'Actual Amount Withdrawn' => (float)$this->getCompanyWithdrawnValue($withdrawal),
                'Withdrawal Amount' => (float)$withdrawal->amount,
                'Principal' => (float)$investment->amount,
                'Value Date' => Carbon::parse($investment->invested_date)->toDateString(),
                'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                'Interest Rate' => $investment->interest_rate,
                'Product' => $investment->product->name,
                'Investment Type' => formatSlug($investment->type->name),
                'Withdrawal Type' => ucfirst($withdrawal->withdraw_type),
                'Withdrawal Action' => $withdrawal->type->name,
                'FA' => @$investment->commission->recipient->name,
                'FA Type' => @$investment->commission->recipient->repo->getRecipientType($withdrawalDate)->name,
                'FA Branch' => $position ? $position->present()->getDepartmentBranch : ($recipient ?
                    $recipient->present()->getBranch : null),
                'FA Department Unit' => $position ? $position->present()->getDepartmentUnit : ($recipient ?
                    $recipient->present()->getDepartmentUnit : null),
                'FA Status' => @$investment->commission->recipient->present()->getActive,
                'Premature' => BooleanPresenter::presentYesNo($withdrawalDate->lt($investment->maturity_date))
            ];
        }

        $fname = 'Withdrawals_from_' . $start->toDateString() . '_to_' . $end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($withdrawalsArray, $fname);

        $email = [];

        if ($userId) {
            $user = User::find($userId);

            if ($user) {
                $email = [$user->email];
            }
        }

        $this->mailResult($fname, $start, $end, $email);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $start
     * @param $end
     */
    private function mailResult($fname, $start, $end, $email)
    {
        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject('Withdrawals export in excel')
            ->text('Here is the export of withdrawals that you requested for the period between ' .
                $start->toDateString() . ' and  ' . $end->toDateString())
            ->excel([$fname])
            ->send();

        \File::delete(storage_path('exports/' . $fname . '.xlsx'));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['start', InputArgument::REQUIRED, 'Start date of the export'],
            ['end', InputArgument::REQUIRED, 'End date of the export']
        ];
    }
}
