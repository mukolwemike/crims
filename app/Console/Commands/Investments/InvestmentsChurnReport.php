<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class InvestmentsChurnReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cytonn:inv-churn {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a report of clients who have churned';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Initializing...');

        $user = $id = User::find($this->argument('user_id'));

        $this->info('Fetching clients fitting criteria...');

        $churned_investments = ClientInvestment::where('withdrawn', 1)
            ->where('rolled', '!=', 1)
            ->get();

        $this->info('Preparing data for export...');
        $out = $churned_investments->map(
            function (ClientInvestment $investment) {
                $c = new EmptyModel();
                $c->{'Client Code'} = $investment->client->client_code;
                $c->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
                $c->{'First Name'} = $investment->client->contact->firstname;
                $c->{'Gender'} = is_null($investment->client->contact->gender)
                    ? null : $investment->client->contact->gender->abbr;
                $c->{'Country'} = is_null($investment->client->country)
                    ? null : $investment->client->country->name;
                if ($investment->client->clientType->name == 'corporate') {
                    $c->{'First Name'} = $investment->client->contact_person_fname;
                }
                $c->{'Invested Date'} = $investment->invested_date;
                $c->{'Exit Date'} = $investment->withdrawn_on;

                return $c;
            }
        );

        $this->info('Generating export...');

        $file_name = 'Client Investments Churn Report';

        \Excel::create(
            $file_name,
            function ($excel) use ($out) {
                $excel->sheet(
                    'Investments',
                    function ($sheet) use ($out) {
                        $sheet->fromModel($out);
                    }
                );
            }
        )->store('xlsx');

        $this->info('Sending to your inbox...');

        $this->mailResult(storage_path('exports/' . $file_name . '.xlsx'), $user);

        $this->info('Sent.');
        $this->info('Completed command');
    }

    private function mailResult($file_path, $user = null)
    {
        $mailer = new GeneralMailer();
        if ($user) {
            $email = $user->email;
        } else {
            $email = ['mchaka@cytonn.com'];
        }
        $mailer->to($email);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Client Investments Churn Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Here is the export of client investments churn report you requested.');

        \File::delete($file_path);
    }
}
