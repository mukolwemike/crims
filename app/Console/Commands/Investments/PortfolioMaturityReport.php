<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Portfolio\DepositHolding;
use Carbon\Carbon;
use Cytonn\Mailers\Investments\PortfolioMaturityReminderMailer;
use Illuminate\Console\Command;

class PortfolioMaturityReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'inv:portfolio-maturities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Portfolio Maturity Report.';

    /**
     * Create a new command instance.
     * PortfolioMaturityReport constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $today = Carbon::today()->toDateString();

        $investments = DepositHolding::where('maturity_date', '<', $today)
            ->where(
                function ($q) {
                    $q->where('withdrawn', null)
                        ->orWhere('withdrawn', 0);
                }
            )->get();

        if ($investments->count()) {
            (new PortfolioMaturityReminderMailer())->sendEmail($investments);
        }

        return $this->info('Portfolio maturity reminders have been sent');
    }
}
