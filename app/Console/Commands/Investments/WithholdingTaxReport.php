<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Investment\CalculatorTrait;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\System\Processing\Async;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Input\InputArgument;

class WithholdingTaxReport extends Command
{
    use ExcelMailer, CalculatorTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:tax {start} {end} {user_id?} {product_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a report of withholding tax for a period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $userId = $this->argument('user_id');

        $user = $userId ? User::find($userId) : null;

        $productId = $this->argument('product_id');

        $product = $productId ? Product::find($productId) : null;

        $investmentQueryBuilder = is_null($product) ? new ClientInvestment() : $product->investments();

        $withdrawals = new Collection();

        $investmentQueryBuilder->whereHas(
            'withdrawals',
            function ($w) use ($start, $end) {
                $w->where('date', '>=', $start)->where('date', '<=', $end);
            }
        )->chunk(
            1000,
            function (Collection $investments) use ($start, $end, $withdrawals) {
                $run = app(static::class);

                $cl = function ($investment) use ($run, $start, $end) {
                    return $run->processInvestment($investment, $start, $end);
                };



            //                $coll = (new Async())->map($investments, $cl);
                $coll = $investments->map($cl);

                $withdrawals->push($coll)->flatten();
            }
        );

        $report = Collection::make($withdrawals)->flatten()->sortBy('Client code');

        $fname = 'Withholding tax report';

        Excel::fromModel($fname, $report)->store('xlsx');

        if ($user) {
            $this->sendExcel('Withholding Tax Report', 'Please find the attached w/tax report for ' .
                $start->toFormattedDateString() . ' to ' . $end->toFormattedDateString(), $fname, [$user->email]);
        } else {
            $this->sendExcel('Withholding Tax Report', 'Please find the attached w/tax report for ' .
                $start->toFormattedDateString() . ' to ' . $end->toFormattedDateString(), $fname);
        }
    }

    public function processInvestment(ClientInvestment $investment, Carbon $start, Carbon $end)
    {
        $prepared = $investment->repo->getPrepared();

        return collect($prepared->actions)->filter(
            function ($action) use ($start, $end) {
                $d = Carbon::parse($action->date);
                return $d->gte($start) && $d->lte($end);
            }
        )->map(
            function ($action) use ($investment) {
                $net = $action->net_interest;
                $tax = $action->wtax;
                $gross = $action->gross_interest_cumulative;
                $actionDate = $action->date;
                $type = $action->type;
                $description = $action->description;
                $withdrawal = $action->action;
                if ($withdrawal && $withdrawal->type_id == 3) {
                    $withdrawn = '';
                    $penalty = (float)$withdrawal->amount;
                } else {
                    $withdrawn = (float)$withdrawal->amount;
                    $penalty = '';
                }

                $e = new EmptyModel();
                $e->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
                $e->{'Client code'} = $investment->client->client_code;
                $e->{'PIN'} = $investment->client->pin_no;
                $e->{'Product'} = $investment->product->name;
                $e->{'Principal'} = (float)$investment->amount;
                $e->{'Action Date'} = $actionDate->toDateString();
                $e->{'Type'} = ucfirst($type);
                $e->{'Gross Interest'} = (float)$gross;
                $e->{'Net Interest'} = (float)$net;
                $e->{'W/Tax'} = (float)$tax;
                $e->{'Description'} = $description;
                $e->{'Penalty'} = $penalty;
                $e->{'Withdrawn Amount'} = $withdrawn;

                return $e;
            }
        );
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['start', InputArgument::REQUIRED, 'Start date'],
            ['end', InputArgument::REQUIRED, 'End date'],
        ];
    }
}
