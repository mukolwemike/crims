<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Handlers\InterestPaymentBulkApproval;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CheckInterestBulkApprovalTransaction extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:check_interest_bulk_approval_transaction {approval}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if the interest bulk approval transaction completed successfully';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $approval = ClientTransactionApproval::findOrFail($this->argument('approval'));
//        $approval = ClientTransactionApproval::findOrFail(119284);

        $handler = new InterestPaymentBulkApproval();

        $scheduleArray = $handler->separateSchedules($approval);

        $normalArray = $this->checkNormalSchedules($scheduleArray['normal'], $approval);
        $this->info("Done with normal");

        $combineArray = $this->checkCombinedSchedules($scheduleArray['combine'], $approval);
        $this->info("Done with combined");

        $fileName = "Interest Bulk Approval Check";

        $dataArray = [
            'Normal Okay' => $normalArray['normal'],
            'Normal Failed' => $normalArray['normal failed'],
            'Combined Okay' => $combineArray['combine'],
            'Combined Failed' => $combineArray['combine failed']
        ];

        ExcelWork::generateAndStoreMultiSheet($dataArray, $fileName);

        Mailer::compose()
            ->to('tkimathi@cytonn.com')
            ->subject($fileName)
            ->text('Please find attached the interest bulk approval check')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function checkCombinedSchedules($schedules, $approval)
    {
        $combinedArray = array();

        $failed = array();

        $schedulesArray = array();

        foreach ($schedules as $schedule) {
            $schedule->client_id = $schedule->investment->client_id;

            $schedulesArray[$schedule->investment->client_id . '_' . $schedule->investment->product_id][] = $schedule;
        }

        foreach ($schedulesArray as $key => $scheduleGroup) {
            $client = $scheduleGroup[0]->investment->client;

            $reinvestmentCount = ClientInvestment::where('approval_id', $approval->id)
                ->where('investment_type_id', 5)
                ->where('client_id', $client->id)->count();

            $reinvestment = ClientInvestment::where('approval_id', $approval->id)
                ->where('investment_type_id', 5)
                ->where('client_id', $client->id)->first();

            if (is_null($reinvestment)) {
                $failed[] = [
                    'Reason' => 'No Reinvest Investment',
                    'Reinvestment' => '',
                    'Schedule Id' => $key,
                    'Withdrawal ID' => ''
                ];
                continue;
            }

            foreach ($scheduleGroup as $schedule) {
                $investment = $schedule->investment;

                $action = $investment->interestAction ? $investment->interestAction->slug : null;

                $withdrawalType = $action == 'reinvest' ? 2 : 1;

                $withdrawalCount = ClientInvestmentWithdrawal::where('investment_id', $investment->id)
                    ->where('approval_id', $approval->id)
                    ->where('type_id', $withdrawalType)
                    ->where('withdraw_type', 'interest')
                    ->count();

                $withdrawal = ClientInvestmentWithdrawal::where('investment_id', $investment->id)
                    ->where('approval_id', $approval->id)
                    ->where('type_id', $withdrawalType)
                    ->where('withdraw_type', 'interest')
                    ->first();

                if (is_null($withdrawal)) {
                    $failed[] = [
                        'Reason' => 'Schedule No WIthdrawal',
                        'Reinvestment' => $reinvestment->id,
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => ''
                    ];

                    continue;
                }

                if ($withdrawalType == 1) {
                    $failed[] = [
                        'Reason' => 'Misplaced WIthdrawal',
                        'Reinvestment' => $reinvestment->id,
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => $withdrawal->id
                    ];
                    continue;
                }

                $newInvestment = $withdrawal->reinvestedTo;

                if (is_null($newInvestment)) {
                    $failed[] = [
                        'Reason' => 'No Reinvestment Investment',
                        'Reinvestment' => $reinvestment->id,
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => $withdrawal->id
                    ];

                    continue;
                }

                $newInvestmentId = $newInvestment->id;

                $combinedArray[] = [
                    'Schedule' => $schedule->id,
                    'Re Investment Count' => $reinvestmentCount,
                    'Re Invest ID' => $reinvestment->id,
                    'Withdrawal' => $withdrawal->id,
                    'Withdrawal Count' => $withdrawalCount,
                    'Action' =>  $withdrawalType,
                    'New Investment' => $newInvestmentId
                ];
            }
        }

        return [
            'combine' => $combinedArray,
            'combine failed' => $failed
        ];
    }

    private function checkNormalSchedules($schedules, $approval)
    {
        $normalArray = array();

        $failed = array();

        foreach ($schedules as $schedule) {
            $investment = $schedule->investment;

            $action = $investment->interestAction ? $investment->interestAction->slug : null;

            $withdrawalType = $action == 'reinvest' ? 2 : 1;

            $withdrawalCount = ClientInvestmentWithdrawal::where('investment_id', $investment->id)
                ->where('approval_id', $approval->id)
                ->where('type_id', $withdrawalType)
                ->where('withdraw_type', 'interest')
                ->count();

            $withdrawal = ClientInvestmentWithdrawal::where('investment_id', $investment->id)
                ->where('approval_id', $approval->id)
                ->where('type_id', $withdrawalType)
                ->where('withdraw_type', 'interest')
                ->first();

            if (is_null($withdrawal)) {
                $failed[] = [
                    'Reason' => 'No WIthdrawal',
                    'Schedule Id' => $schedule->id,
                    'Withdrawal ID' => '',
                    'Withdraw Payment ID' => '',
                    'Child Id' => '',
                    'Transaction Id' => '',
                    'Bank Instruction Id' => '',
                    'New Inv ID' => ''
                ];

                continue;
            }

            if ($withdrawalType == 1) {
                $withdrawPayment = $withdrawal->payment;

                if (is_null($withdrawPayment)) {
                    $failed[] = [
                        'Reason' => 'No WIthdrawal Payment',
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => $withdrawal->id,
                        'Withdraw Payment ID' => '',
                        'Child Id' => '',
                        'Transaction Id' => '',
                        'Bank Instruction Id' => '',
                        'New Inv ID' => ''
                    ];
                    continue;
                }

                $withdrawPaymentId = $withdrawPayment->id;

                $child = $withdrawPayment->child;

                if (is_null($child)) {
                    $failed[] = [
                        'Reason' => 'No WIthdrawal Child Payment',
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => $withdrawal->id,
                        'Withdraw Payment ID' => $withdrawPaymentId,
                        'Child Id' => '',
                        'Transaction Id' => '',
                        'Bank Instruction Id' => '',
                        'New Inv ID' => ''
                    ];

                    continue;
                }

                $childId = $child->id;

                $transaction = $child->custodialTransaction;

                if (is_null($transaction)) {
                    $failed[] = [
                        'Reason' => 'No WIthdrawal Custodial Transaction',
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => $withdrawal->id,
                        'Withdraw Payment ID' => $withdrawPaymentId,
                        'Child Id' => $childId,
                        'Transaction Id' => '',
                        'Bank Instruction Id' => '',
                        'New Inv ID' => ''
                    ];

                    continue;
                }

                $transactionId = $transaction->id;

                $bankInstruction = $transaction->bankInstruction;

                if (is_null($transaction)) {
                    $failed[] = [
                        'Reason' => 'No WIthdrawal Bank Instruction',
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => $withdrawal->id,
                        'Withdraw Payment ID' => $withdrawPaymentId,
                        'Child Id' => $childId,
                        'Transaction Id' => $transactionId,
                        'Bank Instruction Id' => '',
                        'New Inv ID' => ''
                    ];

                    continue;
                }

                $bankInstructionId = $bankInstruction->id;

                $newInvestmentId = '';
            } else {
                $newInvestment = $withdrawal->reinvestedTo;

                if (is_null($newInvestment)) {
                    $failed[] = [
                        'Reason' => 'No Reinvestment Investment',
                        'Schedule Id' => $schedule->id,
                        'Withdrawal ID' => $withdrawal->id,
                        'Withdraw Payment ID' => '',
                        'Child Id' => '',
                        'Transaction Id' => '',
                        'Bank Instruction Id' => '',
                        'New Inv ID' => ''
                    ];

                    continue;
                }

                $newInvestmentId = $newInvestment->id;

                $withdrawPaymentId = $childId = $transactionId = $bankInstructionId = '';
            }

            $normalArray[] = [
                'Schedule' => $schedule->id,
                'Withdrawal' => $withdrawal->id,
                'Withdrawal Count' => $withdrawalCount,
                'Action' =>  $withdrawalType,
                'Withdraw Payment' => $withdrawPaymentId,
                'Child Payment' => $childId,
                'Transaction' => $transactionId,
                'Bank Instruction' => $bankInstructionId,
                'New Investment' => $newInvestmentId
            ];
        }

        return [
            'normal' => $normalArray,
            'normal failed' => $failed
        ];
    }
}
