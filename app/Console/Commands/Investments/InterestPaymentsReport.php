<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\InterestPayment;
use Cytonn\Mailers\Investments\InterestPaymentsMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\InterestPaymentsReportExcelGenerator;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class InterestPaymentsReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:interest-payments {start_date} {end_date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate and send interest payments report.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $args = $this->input->getArguments();
        $start_date = $args['start_date'];
        $end_date = $args['end_date'];
        $payments = InterestPayment::where('date_paid', '>=', $start_date)
            ->where('date_paid', '<=', $end_date)
            ->get()
            ->groupBy('investment_id')
            ->filter(
                function ($group) {
                    return $group->count() > 1;
                }
            )
            ->flatten()
            ->each(
                function ($ip) {
                    $ip->product_id = $ip->investment->product_id;
                }
            )
            ->groupBy('product_id');

        $file_name = 'Interest Payments Reports - ' . DatePresenter::formatDate($args['start_date']) . ' to ' .
            DatePresenter::formatDate($args['end_date']);

        if (count($payments)) {
            (new InterestPaymentsReportExcelGenerator())->excel($payments, $file_name)->store('xlsx');

            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            (new InterestPaymentsMailer())->sendEmailReport($payments, $file_path, $start_date, $end_date);

            \File::delete($file_path);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['start_date', InputArgument::REQUIRED, 'Start Date.'],
            ['end_date', InputArgument::REQUIRED, 'End Date.'],
        ];
    }
}
