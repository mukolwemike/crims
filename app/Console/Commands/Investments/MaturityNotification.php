<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Portfolio\DepositHolding;
use Cytonn\Mailers\MaturityNotificationMailer;
use Cytonn\Mailers\MaturityReminderMailer;
use Cytonn\Mailers\PortfolioMaturityMailer;
use Illuminate\Console\Command;
use Carbon\Carbon;

/**
 * Class MaturityNotification
 */
class MaturityNotification extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cytonn:maturity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends out maturity notifications to staff';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->clientMaturities();
        $this->portfolioMaturities();
        $this->clientReminders();
    }

    /**
     * Send client investments maturing
     */
    protected function clientMaturities()
    {
        //check for investments maturing in a week
        $nextWeek = Carbon::today()->addDays(7)->toDateString();
        $investments = ClientInvestment::where('maturity_date', $nextWeek)->active()->get();

        if ($investments->count() != 0) {
            $mailer = new MaturityNotificationMailer();
            $mailer->inAWeek($investments);
        }

        //check for investments maturing today
        $investments = ClientInvestment::where('maturity_date', Carbon::today()->toDateString())->active()->get();

        if ($investments->count() != 0) {
            $todayMailer = new MaturityNotificationMailer();
            $todayMailer->today($investments);
        }


        $this->info('Client Maturity notifications have been sent');
    }

    /**
     * Send portfolio investments to mature
     */
    protected function portfolioMaturities()
    {
        //check for investments maturing in a week
        $nextWeek = Carbon::today()->addDays(7)->toDateString();

        $investmentsGroupedByFundManager = DepositHolding::where('maturity_date', $nextWeek)
            ->active()
            ->get()
            ->each(
                function (DepositHolding $investment) {
                    $investment->fund_manager = $investment->security->fundManager->name;
                }
            )->groupBy('fund_manager');

        if ($investmentsGroupedByFundManager->count() != 0) {
            $mailer = new PortfolioMaturityMailer();
            $mailer->inAWeek($investmentsGroupedByFundManager);
        }

        //check for investments maturing today
        $investmentsGroupedByFundManager = DepositHolding::where('maturity_date', Carbon::today()->toDateString())
            ->active()
            ->get()
            ->each(
                function (DepositHolding $investment) {
                    $investment->fund_manager = $investment->security->fundManager->name;
                }
            )
            ->groupBy('fund_manager');

        if ($investmentsGroupedByFundManager->count() != 0) {
            $todayMailer = new PortfolioMaturityMailer();
            $todayMailer->today($investmentsGroupedByFundManager);
        }

        $this->info('Portfolio Maturity notifications have been sent');
    }

    /**
     * Reminders for client investments that have matured
     */
    protected function clientReminders()
    {
        $today = Carbon::today()->toDateString();
        $investments = ClientInvestment::where('maturity_date', '<=', $today)
            ->active()->get();

        if ($count = $investments->count() != 0) {
            $mailer = new MaturityReminderMailer();
            $mailer->remind($investments);
        }

        $this->info("$count Maturity Reminders have been sent");
    }
}
