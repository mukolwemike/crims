<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class ExportContactsOfInactiveClients extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:inactive-clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export contacts of inactive investment clients per product.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fileName = 'Clients With Inactive Investments';

        \Excel::create(
            $fileName,
            function ($excel) {
                $excel->sheet(
                    'Inactive Clients',
                    function ($sheet) {
                        $clients = Client::has('investments')->get()->filter(
                            function ($client) {
                                return $client->repo->doesNotHaveActiveInvestments();
                            }
                        );
                        $date = new Carbon();

                        $c = $clients->map(
                            function ($client) use ($date) {
                                $client->name = ClientPresenter::presentJointFullNames($client->id);
                                $client->email = $client->contact->email;
                                $client->phone = $client->contact->phone;
                                $client->country = $client->country->name;
                                $client->last_inactive = $client->investments()->latest('withdrawn_on')
                                    ->first()->withdrawn_on;

                                return $client;
                            }
                        );

                        $sheet->loadView('exports.inactive_clients', ['clients'=>$c, 'date'=>$date]);
                    }
                );
            }
        )->store('xlsx');

        $path = storage_path().'/exports/'.$fileName.'.xlsx';

        $this->mailResult($path);

        \File::delete($path);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     */
    private function mailResult($file_path)
    {
        $mailer = new GeneralMailer();
        $mailer->to([ 'mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Investments Inactive Clients export in excel');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached report of inactive investment clients');
    }
}
