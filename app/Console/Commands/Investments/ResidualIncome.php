<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Investment\Summary\Analytics;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\System\Processing\Async;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Excel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class ResidualIncome
 *
 * @package Investments
 */
class ResidualIncome extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:residual-income {date} {end?} {user_id?} {--currency=} {--fm=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a report of residual income for the month';

    /**
     * @var
     */
    protected $date;


    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $c = $this->option('currency');
        $baseCurrency = Currency::where('code', $c)->orWhere('id', $c)->first();

        $date = $this->argument('date');
        $end = $this->argument('end');
        $user = User::find($this->argument('user_id'));

        if (!$date) {
            $date = Carbon::parse($date)->subMonthNoOverflow();
        }

        $date = new Carbon($date);

        $this->date = $date;

        if (!$end) {
            return $this->generate($user, $baseCurrency);
        }

        $end = new Carbon($end);

        if ($end->gt($date) && $end->copy()->diffInMonths($date) >= 1) {
            $working = $date->copy();

            while ($working->lte($end->copy())) {
                $this->date = $working;

                $this->info($this->date);

                $this->generate($user, $baseCurrency);

                $working = $working->addMonthNoOverflow();
            }
        }
        return 0;
    }

    protected function generate($user = null, Currency $baseCurrency = null)
    {
        $residualFiles = FundManager::where('id', $this->option('fm'))
            ->get()
            ->map(
                function ($f) use ($baseCurrency) {
                    return $this->calculate($f, $baseCurrency);
                }
            );


        $keys = $residualFiles->map(
            function ($file) {
                return last(explode('/', $file));
            }
        )->all();

        $this->mailResult(array_combine($keys, $residualFiles->all()), $user);

        $this->info("Report sent!");
    }

    /**
     * @param FundManager   $fundManager
     * @param Currency|null $baseCurrency
     * @return string
     */
    protected function calculate(FundManager $fundManager, Currency $baseCurrency)
    {
        $start = $this->date->copy()->startOfMonth();
        $end = $this->date->copy()->endOfMonth();

        $file = $fundManager->name;

        $model = $this->model($fundManager, $baseCurrency, $start, $end)
            ->sortByDate('day', 'ASC')
            ->each(
                function ($day) {
                    unset($day->day);
                }
            );

        \Excel::create(
            $file,
            function ($excel) use ($baseCurrency, $fundManager, $start, $end, $model) {
                $excel->sheet(
                    $baseCurrency->code,
                    function ($sheet) use ($fundManager, $baseCurrency, $start, $end, $model) {
                        $sheet->fromModel($model);
                    }
                );
            }
        )->store('xlsx');

        return storage_path('exports/'.$file.'.xlsx');
    }

    /**
     * @param FundManager   $fundManager
     * @param Currency|null $base
     * @param Currency      $currency
     * @param Carbon        $start
     * @param Carbon        $end
     * @return Collection
     */
    protected function model(FundManager $fundManager, Currency $base, Carbon $start, Carbon $end)
    {
        $async = new Async();

        $class = static::class;

        $days =  Collection::make(Carbon::daysBetweenArray($start->copy(), $end->copy()));

        $results =  $async->map(
            $days,
            function (Carbon $day) use ($class, $base, $fundManager) {
                $s = app($class);
                return $s->calculateForDay($day, $base, $fundManager);
            }
        );

        return collect($results);
    }

    public function calculateForDay($day, $base, $fundManager)
    {
        $ident = $day->toFormattedDateString();

        print("Calculating for ".$ident.PHP_EOL);

        $p_analytics = new \Cytonn\Portfolio\Summary\Analytics($fundManager, $day);
        $i_analytics = new Analytics($fundManager, $day);
        $p_analytics->setBaseCurrency($base);
        $i_analytics->setBaseCurrency($base);

        $assetYield = $p_analytics->weightedRate();
        $liabilityYield = $i_analytics->weightedRate();
        $spread = $assetYield - $liabilityYield;
        $aum = $i_analytics->costValue();

        $out = new EmptyModel();
        $out->fill(
            [
            'day' => $day,
            'Date' => $day->toFormattedDateString(),
            'Asset Yield' => $assetYield,
            'Liability Yield' => $liabilityYield,
            'Spread' => $spread,
            'AUM' => $aum,
            'Residual Income' => (float) ($spread/(100 * 365)) * $aum * 0.85,
            'Asset CV' => $p_analytics->costValue(),
            'Asset MV' => $p_analytics->marketValue(),
            'Liability CV' => (float) $i_analytics->costValue(),
            'Liability MV' => $i_analytics->marketValue(),
            'Bank Balance' => $p_analytics->custodialBalance()
            ]
        );

        print("Completed for ".$ident.PHP_EOL);

        return $out;
    }

    /**
     * Send an email with the export
     *
     * @param array $files
     * @param $user
     */
    private function mailResult(array $files, $user = null)
    {
        $mailer = new GeneralMailer();
        if ($user) {
            $email = $user->email;
        } else {
            $email = [ 'mchaka@cytonn.com'];
        }
        $mailer->to($email);
        $mailer->from('support@cytonn.com');
        $mailer->bcc(config('system.emails.administrators'));
        $mailer->subject('Residual Income Report');
        $mailer->file($files);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached residual income report');
    }
}
