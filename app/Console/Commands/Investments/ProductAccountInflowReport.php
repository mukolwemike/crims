<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ProductAccountInflowReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'products:account_inflow {product_id} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the products account inflow report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $startDate = Carbon::parse($this->argument('start'));

        $endDate = Carbon::parse($this->argument('end'));

        $productId = $this->argument('product_id');

        $product = Product::findOrFail($productId);

        $inflows = $this->getAccountInflows($product, $startDate, $endDate);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName =
            $product->name . ' Accounts Inflows ' . $startDate->toDateString() . ' to ' . $endDate->toDateString();

        ExcelWork::generateAndStoreSingleSheet($inflows, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the product accounts inflows reports')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the accounts inflows for the specified product
     */
    public function getAccountInflows(Product $product, Carbon $startDate, Carbon $endDate)
    {
    }
}
