<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RolloverCommissionAward extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:rollover_commission_award {type} {date} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Award rollover investments commissions for the set period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $date = Carbon::parse($this->argument('date'));

        $runDates = [
            '2018-12-31',
            '2019-03-31',
            '2019-06-30',
            '2019-09-30',
        ];

        if (!in_array($date->toDateString(), $runDates)) {
            $this->info("This is not a set run date");
            return;
        }

        //0 for just generate a report; 1 for updating the schedules
        $runType = $this->argument('type');

        $report = [];

        DB::transaction(function () use ($runType, $date, &$report) {
            $report = $this->getInvestmentData($runType, $date);
        });

        $userId = $this->argument('user_id');

        $fileName = 'Rollover Commission Award ' . $date->toDateString();

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the rollover commission award as at ' .
                $date->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getInvestmentData($runType, Carbon $endDate)
    {
        $investments = ClientInvestment::where('invested_date', '>=', '2018-09-17')
            ->where('investment_type_id', 3)->get();

        $investmentArray = array();

        foreach ($investments as $investment) {
            $tenorDays = $this->getTenorDays($investment, $endDate);

            $paidCommissionsCount = $investment->commission->schedules()->scheduleType(2)->count();

            $count = 1;

            while ($count <= 4) {
                if ($tenorDays >= (90 * $count) && $paidCommissionsCount < $count) {
                    $commissionAmount = $investment->amount * 0.2 / (100 * 4);

                    $description = "Rollover commission award " . $count;

                    if ($runType == 1) {
                        $this->awardCommission(
                            $commissionAmount,
                            $investment,
                            $endDate,
                            $description
                        );
                    }

                    $investmentArray[] = [
                        'Award Date' => $endDate->toDateString(),
                        'Client Code' => $investment->client->client_code,
                        'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                        'FA' => $investment->commission->recipient->name,
                        'Principal' => $investment->amount,
                        'Investment Type' => $investment->type->name,
                        'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
                        'Withdrawal Date' => Carbon::parse($investment->withdrawal_date)->toDateString(),
                        'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                        'Elapsed Days' => $tenorDays,
                        'Period End Date' => $endDate->toDateString(),
                        'Awarded Commission' => $commissionAmount,
                        'Description' => $description
                    ];

                    $paidCommissionsCount++;
                }

                $count++;
            }
        }

        return $investmentArray;
    }

    private function awardCommission($amount, $investment, Carbon $date, $description)
    {
        return CommissionPaymentSchedule::create([
            'commission_id' => $investment->commission->id,
            'date' => $date,
            'amount' => $amount,
            'description' => $description,
            'commission_payment_schedule_type_id' => 2
        ]);
    }

    private function getTenorDays(ClientInvestment $investment, Carbon $endDate)
    {
        if ($investment->withdrawal_date) {
            $withdrawalDate = Carbon::parse($investment->withdrawal_date);

            $endDate = $withdrawalDate <= $endDate ? $withdrawalDate : $endDate;
        }

        $maturityDate = Carbon::parse($investment->maturity_date);

        $endDate = $maturityDate <= $endDate ? $maturityDate : $endDate;

        $startDate = Carbon::parse($investment->invested_date);

        $tenor = $startDate->diffInDays($endDate);

        return $tenor;
    }
}
