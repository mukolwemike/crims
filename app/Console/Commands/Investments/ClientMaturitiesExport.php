<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientMaturitiesExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:client_maturities_export {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the investments set to mature in the specified dates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $report = $this->getClientMaturies($start, $end);

        $fileName = 'Client Maturities Summary';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the client maturities summary between ' .
                $start->toDateString() . ' and ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getClientMaturies(Carbon $start, Carbon $end)
    {
        $investments = ClientInvestment::where('maturity_date', '>=', $start)
            ->where('maturity_date', '<=', $end)
            ->activeBetweenDates($start, $end)
            ->get()
            ->map(function (ClientInvestment $investment) {
                return [
                    'Client Code' => $investment->client->client_code,
                    'Name' => ClientPresenter::presentJointFullNames($investment->client_id),
                    'Product' => $investment->product->name,
                    'Principal' => (float)$investment->amount,
                    'Value Date' => Carbon::parse($investment->invested_date)->toDateString(),
                    'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                    'Value at Maturity' =>
                        $investment->repo->getFinalValueOnWithdrawal(),
                    'Withdrawal Date' => $investment->withdrawal_date ?
                        Carbon::parse($investment->withdrawal_date)->toDateString() : '',
                    'FA' => $investment->commission->recipient->name,
                    'FA Position' => $investment->commission->recipient->type->name,
                    'FA Department' => $investment->commission->recipient->present()->getDepartmentUnit,
                    'FA Branch' => $investment->commission->recipient->present()->getBranch
                ];
            });

        return $investments->groupBy('Product');
    }
}
