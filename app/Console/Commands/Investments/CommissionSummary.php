<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class CommissionSummary extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:commission-summary {fund_manager_id} {date} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commission summary for a fund manager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fundManager = FundManager::findOrFail($this->argument('fund_manager_id'));

        $date = Carbon::parse($this->argument('date'))->startOfMonth();

        $user = User::find($this->argument('user_id'));

        $fas = CommissionRecepient::whereHas(
            'commissions',
            function ($commission) use ($fundManager) {
                $commission->whereHas(
                    'investment',
                    function ($investment) use ($fundManager) {
                        $investment->whereHas(
                            'product',
                            function ($q) use ($fundManager) {
                                $q->where('fund_manager_id', $fundManager->id);
                            }
                        );
                    }
                );
            }
        )->get();

        $bulk = BulkCommissionPayment::includes($date)->first();

        $start = $date->copy()->subMonthNoOverflow()->addDays(19);

        $end = $date->copy()->addDays(20);

        if ($bulk) {
            $start = $bulk->start;
            $end = $bulk->end;
        }

        $faCommissionTotals = $fas->map(
            function (CommissionRecepient $fa) use ($fundManager, $start, $end) {
                $faSum = 0;

                foreach ($fundManager->products as $product) {
                    $gen = $fa->calculator($product->currency, $start, $end);
                    $gen->filterForProduct($product);
                    $faSum += $gen->summary()->total();
                }

                return (new EmptyModel())->fill(
                    [
                        'Name' => $fa->name,
                        'Amount' => $faSum
                    ]
                );
            }
        );

        $mapped = [];
        foreach ($fas as $fa) {
            $mapped[$fa->name] = $fa;
        }

        $fas = \Cytonn\Core\DataStructures\Collection::make($mapped);

        $worksheet = $fas->map(
            function (CommissionRecepient $fa) use ($fundManager, $start, $end) {
                $schedulesArray = array();

                foreach ($fundManager->products as $product) {
                    $gen = $fa->calculator($product->currency, $start, $end);
                    $gen->filterForProduct($product);

                    $schedules = $gen->compliantSchedulesQuery()->get();

                    foreach ($schedules as $schedule) {
                        $investment = $schedule->commission->investment;

                        $schedulesArray[] = (new EmptyModel())->fill(
                            [
                                'Principal' => $investment->amount,
                                'Product' => $investment->product->name,
                                'Value Date' => $investment->invested_date,
                                'Maturity Date' => $investment->maturity_date,
                                'Description' => $schedule->description,
                                'Comm Date' => $schedule->date,
                                'Comm Amt' => $schedule->amount
                            ]
                        );
                    }
                }

                return $schedulesArray;
            }
        );

        $excel = new Collection();
        $excel['Summary'] = $faCommissionTotals;
        $excel = $excel->merge($worksheet->all());

        $file = 'Commission for ' . $fundManager->name;

        Excel::fromModel($file, $excel)->store('xlsx');

        if ($user) {
            $this->sendExcel(
                $file . ' - ' . $date->format('M, Y'),
                'Please find attached the commission paid for the fund manager ' .
                $fundManager->name . ' in ' . $date->format('M, Y'),
                $file,
                [$user->email]
            );
        } else {
            $this->sendExcel(
                $file . ' - ' . $date->format('M, Y'),
                'Please find attached the commission paid for the fund manager ' .
                $fundManager->name . ' in ' . $date->format('M, Y'),
                $file
            );
        }

        return 0;
    }
}
