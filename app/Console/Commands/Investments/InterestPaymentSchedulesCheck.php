<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Investment\InterestPayments\ScheduleGenerator;
use Illuminate\Console\Command;

class InterestPaymentSchedulesCheck extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:interest_payment_schedules_check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for any investments without interest payment schedules';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $scheduleGenerator = new ScheduleGenerator();

        ClientInvestment::active()->whereDoesntHave(
            'interestSchedules',
            function ($q) {
            }
        )->get()
            ->filter(
                function ($investment) {
                    $invested_date = new Carbon($investment->invested_date);

                    $maturity_date = new Carbon($investment->maturity_date);

                    $tenorInMonths = $invested_date->copy()->diffInMonths($maturity_date);

                    if ($investment->interest_payment_interval <= 0 ||
                        $investment->interest_payment_interval > $tenorInMonths) {
                        return false;
                    }

                    return true;
                }
            )->each(
                function ($investment) use ($scheduleGenerator) {
                    $scheduleGenerator->generate($investment);
                }
            );
    }
}
