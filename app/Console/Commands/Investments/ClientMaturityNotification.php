<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\FCM\FcmManager;
use Cytonn\Handlers\ExceptionHandler;
use Cytonn\Mailers\ClientMaturityNotificationMailer;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;

/**
 * Class ClientMaturityNotification
 *
 * @package Investments
 */
class ClientMaturityNotification extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:send-maturity-notifications {--target=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send maturity notifications to clients with investments maturing in a week';

    protected $failures;

    protected $sent;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        $toClient = true;
        $updateInv = true;
        $days = 7;

        if ($this->option('target') == 'fa') {
            $toClient = false;
            $updateInv = false;
            $days = 30;
        }

        $investments = $this->todaysMaturitiesQuery($days, $toClient)
            ->whereNull('maturity_notification_sent_on')
            ->get()
            ->filter(function (ClientInvestment $i) {
                return !$i->maturity_notification_sent_on && ($i->repo->getTotalValueAsAtEndOfDay() > 1);
            });

        $investmentGroups = $this->groupByClientAndProduct($investments);

        $this->info("Sending for " . $investments->count() . " investments");

        $this->dispatchNotifications($investmentGroups, $toClient, $updateInv);

        $this->notify();

        $this->info('Complete');

        return true;
    }

    /*
     * Group the investments by their client id and product id
     */
    private function groupByClientAndProduct($investments)
    {
        $investmentsArray = array();

        foreach ($investments as $investment) {
            $investmentsArray[$investment->client_id . '-' . $investment->product_id][] = $investment;
        }

        return $investmentsArray;
    }

    /*
     * Dispatch the maturity notifications to the clients based on the groups
     */
    private function dispatchNotifications($investmentGroups, $toClient = true, $update = true)
    {
        foreach ($investmentGroups as $investmentGroup) {
            try {
                $notifier = new ClientMaturityNotificationMailer();
                $notifier->sendToClient = $toClient;
                $notifier->notifyWeekBefore($investmentGroup, null);

                if ($toClient) {
                    $this->sendFcmNotification($investmentGroup[0]);
                }

                if ($update) {
                    $this->updateSentMaturityNotifications($investmentGroup);
                }
            } catch (\Exception $e) {
                (new ClientMaturityNotificationMailer())->notifyFailure($investmentGroup);

                app(ExceptionHandler::class)->reportException($e);
            }
        }
    }

    /*
     * Send an FCM notification
     */
    /**
     * @param ClientInvestment $investment
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    private function sendFcmNotification(ClientInvestment $investment)
    {
        $client = $investment->client;

        $title = 'Maturity Notification';

        $body = 'Maturity Notification - ' . ClientPresenter::presentJointFirstNames($client->id) . ' - Client Code ' .
            $client->client_code;

        (new FcmManager())->sendFcmMessageToMultipleDevices(
            $client,
            $investment->uuid,
            $investment->product->id,
            $title,
            $body,
            'maturity'
        );
    }

    /*
     * Update the maturity notifications once sent
     */
    private function updateSentMaturityNotifications($investmentGroup)
    {
        foreach ($investmentGroup as $investment) {
            $investment->update(['maturity_notification_sent_on' => Carbon::now()]);
        }
    }

    /**
     * @param int $days
     * @return Builder
     */
    private function todaysMaturitiesQuery($days = 7, $toClient = true)
    {
        $date = Carbon::today()->addDays($days);

        $query =  ClientInvestment::where('maturity_date', '<=', $date)
            ->where('maturity_date', '>', Carbon::today())
            ->whereHas('product', function ($product) {
                $product->where('active', 1);
            })
            ->where(function ($q) {
                $q->doesntHave('schedule')->orWhereHas('schedule', function ($q) {
                    $q->where('action_date', '<', Carbon::now());
                });
            })
            ->doesntHave('rolloverInstruction')
            ->where(function ($query) {
                $query->where('on_call', 0)
                    ->orWhereNull('on_call');
            })->active();

        if ($toClient) {
            $query->whereHas('client', function ($client) {
                $client->doesntHave('settings')
                    ->orWhereHas('settings', function ($settings) {
                        $settings->where('disable_maturities', false);
                    });
            });
        }

        return $query;
    }

    /**
     *
     */
    private function notify()
    {
        $failures = $this->todaysMaturitiesQuery()
            ->where('maturity_date', '>=', Carbon::today())
            ->whereNull('maturity_notification_sent_on')
            ->get()
            ->filter(
                function (ClientInvestment $i) {
                    return !$i->repo->getTotalValueAsAtEndOfDay() > 1;
                }
            );

        $sent = $this->todaysMaturitiesQuery()
            ->where('maturity_date', '>=', Carbon::today())
            ->whereNotNull('maturity_notification_sent_on')
            ->get();

        $failures = $failures->map(
            function (ClientInvestment $investment) {
                $out = new EmptyModel();
                $out->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
                $out->{'Client code'} = $investment->client->client_code;
                $out->{'Product'} = $investment->product->name;
                $out->{'Principal'} = (float)$investment->amount;
                $out->{'Value Date'} = $investment->invested_date;
                $out->{'Maturity Date'} = $investment->maturity_date;
                $out->{'Value at Maturity'} =
                    $investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date);
                $out->{'FA'} = $investment->commission->recipient->name;
                $out->{'FA Position'} = $investment->commission->recipient->type->name;

                return $out;
            }
        )->groupBy('Product');

        $success = $sent->map(
            function (ClientInvestment $investment) {
                $out = new EmptyModel();
                $out->{'ID'} = $investment->id;
                $out->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
                $out->{'Client code'} = $investment->client->client_code;
                $out->{'Product'} = $investment->product->name;
                $out->{'Principal'} = (float)$investment->amount;
                $out->{'Value Date'} = $investment->invested_date;
                $out->{'Maturity Date'} = $investment->maturity_date;
                $out->{'Value at Maturity'} =
                    $investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date);
                $out->{'FA'} = $investment->commission->recipient->name;
                $out->{'FA Position'} = $investment->commission->recipient->type->name;

                return $out;
            }
        )->groupBy('Product');

        $recent = $this->todaysMaturitiesQuery()
            ->where('maturity_notification_sent_on', '>=', Carbon::now()->subMinutes(5))
            ->where('maturity_notification_sent_on', '<=', Carbon::now()->addMinute())
            ->count();

        if ($failures->count() == 0 && $recent == 0) {
            return;
        }

        $failed_fname = 'Failed Maturity Notifications';
        $success_fname = 'Successful Maturity Notifications';

        Excel::fromModel($failed_fname, $failures)->store('xlsx');
        Excel::fromModel($success_fname, $success)->store('xlsx');

        $failed_file_path = storage_path('exports/' . $failed_fname . '.xlsx');
        $success_file_path = storage_path('exports/' . $success_fname . '.xlsx');

        $mailer = new GeneralMailer();
        $mailer->to('operations@cytonn.com');
        $mailer->bcc(config('system.administrators'));
        $mailer->from('support@cytonn.com');
        $mailer->subject('Maturity Notification Report - ' . Carbon::today()->toFormattedDateString());
        $mailer->file([$failed_file_path, $success_file_path]);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Please find attached the maturity notifications 
        that were sent and those that failed on ' . Carbon::today()->toFormattedDateString());

        $this->info('Report sent');
        \File::delete($failed_file_path);
        \File::delete($success_file_path);
    }
}
