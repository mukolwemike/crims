<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Mail\Mail;
use Carbon\Carbon;
use App\Cytonn\Models\ClientInvestment;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Handlers\ExceptionHandler;
use Cytonn\Presenters\ClientPresenter;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Console\Command;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Commander\Events\DispatchableTrait;
use Cytonn\Investment\Approvals\Events\Actions\InvestmentActionApproved;

/**
 * Checks for scheduled investment actions, when the time has reached, action is performed
 * Class InvestmentScheduledActions
 */
class InvestmentScheduledActions extends Command
{
    use EventGenerator, DispatchableTrait, LocksTransactions;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cytonn:investment-scheduled-actions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for and runs scheduled actions on investments';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $approvals = ClientTransactionApproval:: whereNull('run_date')
            ->where('scheduled', true)
            ->where('action_date', '<=', Carbon::now()->startOfDay())
            ->get();

        if (!$count = $approvals->count()) {
            return;
        }

        $approvals->each(
            function ($approval) {
                try {
                    $investment = $approval->investment;

                    if ($investment->withdrawn) {
                        throw new ClientInvestmentException("Investment $investment->id is already withdrawn");
                    }

                    $this->runSchedule($approval);
                } catch (\Exception $e) {
                    $this->reportFailure($approval, $e, $investment->withdrawn);
                }
            }
        );

        $this->info("$count scheduled actions have been checked & performed");
    }


    /**
     * @param ClientTransactionApproval $approval
     * @return bool
     * @internal param $schedule
     */
    private function runSchedule(ClientTransactionApproval $approval)
    {
        $handler = $approval->handler();

        \DB::transaction(function () use ($handler, $approval) {
                $approve = new Approval($approval);
//                $approve->lockForUpdate();

                $handler->setRunningSchedule(true)
                    ->runTransaction($approval);

                $handler->updateRunTransaction($approval);
        });
    }

    private function reportFailure(ClientTransactionApproval $approval, Exception $e, $cancel = false)
    {
        $investment = $approval->investment;

        $principal = AmountPresenter::currency($investment->amount);
        $client_code = $investment->client->client_code;
        $client = ClientPresenter::presentJointFullNames($investment->client_id);

        $report = [
            'title' => 'A scheduled action failed to run',
            'table' => [
                'Principal'   => $principal,
                'Client code' => $client_code,
                'Action Taken' => $cancel ? 'Schedule cancelled ': 'None',
                'Name'        => $client,
                'Exception Message' => $e->getMessage(),
                'Exception Trace'   => '<pre>'.$e->getTraceAsString().'</pre>',
            ]
        ];

        if ($cancel) {
            $approval->update(
                [
                'action_date'   =>  null
                ]
            );
        }

        Mail::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(systemEmailGroups(['administrators']))
            ->subject('Investment schedule failed to run')
            ->view('emails.reports.generic', ['blocks' => [$report]])
            ->send();

        app(\Illuminate\Contracts\Debug\ExceptionHandler::class)->report($e);
    }
}
