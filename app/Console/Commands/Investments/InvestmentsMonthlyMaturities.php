<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\Investments\InvestmentsMonthlyMaturitiesMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InvestmentsMonthlyMaturities extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'inv:monthly-maturities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Client investments monthly maturities.';

    /**
     * Create a new command instance.
     * InvestmentsMonthlyMaturities constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $today = Carbon::today();
        $next_month = $today->copy()->addWeeks(4);
//        $next_month = Carbon::parse('2026-01-01');

        $file_name = 'Maturities for the Month, ' . $today->copy()->toFormattedDateString()
            . ' to ' . $next_month->copy()->toFormattedDateString();

        $maturities_grouped_by_product = ClientInvestment::where(
            'maturity_date',
            '<=',
            $next_month->copy()->toDateString()
        )->where('maturity_date', '>=', $today->copy()->toDateString())
            ->where('withdrawn', 0)//NOTE: initially 1, changed to 0
            ->whereNull('withdrawal_date')
            // This is to remove already withdrawn investments from the generated report of upcoming maturities
            ->get()
            ->map(
                function (ClientInvestment $investment) {
                    $out = new EmptyModel();

                    $out->{'Name'} = ClientPresenter::presentJointFullNames($investment->client_id);
                    $out->{'Client Code'} = $investment->client->client_code;
                    $out->{'Product'} = $investment->product->name;
                    $out->{'Principal'} = (float)$investment->amount;
                    $out->{'Value Date'} = $investment->invested_date;
                    $out->{'Maturity Date'} = $investment->maturity_date;
                    $out->{'Value at Maturity'} = $investment
                        ->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date, true);
                    $out->{'FA'} = $investment->client->getLatestFa('investment')->name;

                    return $out;
                }
            )
            ->groupBy('Product');

        Excel::fromModel($file_name, $maturities_grouped_by_product)->store('xlsx');

        $file_path = storage_path('exports/' . $file_name . '.xlsx');

        (new InvestmentsMonthlyMaturitiesMailer())->sendEmail(
            $maturities_grouped_by_product,
            $file_path,
            $today->copy(),
            $next_month->copy()
        );

        $this->info('Maturities for the month have been sent');
        File::delete($file_path);
    }
}
