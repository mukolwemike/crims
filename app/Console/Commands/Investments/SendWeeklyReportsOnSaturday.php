<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use Carbon\Carbon;
use Cytonn\Mailers\SaturdayWeeklyReportMailer;
use Cytonn\Portfolio\Summary\Analytics;
use Illuminate\Console\Command;

class SendWeeklyReportsOnSaturday extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cytonn:saturday-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the chys weekly report every saturday';

    protected $mailer;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        $this->mailer = new SaturdayWeeklyReportMailer();

        parent::__construct();
    }


    /**
     * Compose and send the report
     *
     * @return mixed
     * @throws \Exception
     */
    public function fire()
    {
        $today = Carbon::today();


        //assume it's saturday
        $nextWeekStart = $today->copy()->addWeek()->startOfWeek();
        $nextWeekEnd = $today->copy()->addWeek()->endOfWeek();
        $this->mailer->nextWeekStart = $nextWeekStart;

        $otherWeekStart = $today->copy()->addWeeks(2)->startOfWeek();
        $otherWeekEnd = $today->copy()->addWeeks(2)->endOfWeek();
        $this->mailer->otherWeekStart = $otherWeekStart;

        $nextWeekInvestments = ClientInvestment::where('maturity_date', '>=', $nextWeekStart)
            ->where('maturity_date', '<=', $nextWeekEnd)
            ->where('withdrawn', 0)
            ->get();

        $otherWeekInvestments = ClientInvestment::where('maturity_date', '>=', $otherWeekStart)
            ->where('maturity_date', '<=', $otherWeekEnd)
            ->where('withdrawn', 0)
            ->get();


        $nextWeekPortfolio = DepositHolding::where('maturity_date', '>=', $nextWeekStart)
            ->where('maturity_date', '<=', $nextWeekEnd)
            ->where('withdrawn', null)
            ->get();

        $otherWeekPortfolio = DepositHolding::where('maturity_date', '>=', $otherWeekStart)
            ->where('maturity_date', '<=', $otherWeekEnd)
            ->where('withdrawn', null)
            ->get();

        $analytics = function (FundManager $fundManager) use ($today) {
            $currency = Currency::where('code', 'KES')->first();
            $p_analytics = new Analytics($fundManager, $today);
            $p_analytics->setBaseCurrency($currency);
            $i_analytics = new \Cytonn\Investment\Summary\Analytics($fundManager, $today);
            $i_analytics->setBaseCurrency($currency);

            $asset_yield = $p_analytics->weightedRate();
            $liability_yield = $i_analytics->weightedRate();

            return [
                'asset_yield' => $asset_yield,
                'liability_yield' => $liability_yield,
                'asset_tenor' => $p_analytics->weightedTenor(),
                'liability_tenor' => $i_analytics->weightedTenor(),
                'spread' => $asset_yield - $liability_yield
            ];
        };

        $this->mailer->prepareReport(
            $nextWeekInvestments,
            $otherWeekInvestments,
            $nextWeekPortfolio,
            $otherWeekPortfolio,
            $analytics,
            $today->copy()
        );

        $this->info('The report has been sent');
    }
}
