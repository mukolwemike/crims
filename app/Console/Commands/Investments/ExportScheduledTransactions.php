<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class ExportScheduledTransactions extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'inv:export-scheduled-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all scheduled transactions at a given time.';

    /**
     * Create a new command instance.
     * ExportScheduledTransactions constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $date = Carbon::today();

        $transactions =
            ClientTransactionApproval::where('action_date', '>=', $date->copy()->startOfDay())
                ->where('scheduled', true)
                ->whereNotNull('action_date')
                ->has('investment')
                ->get()
                ->map(
                    function (ClientTransactionApproval $transaction) {
                        $e = new EmptyModel();

                        $e->{'Client Name'} = ClientPresenter::presentFullNames($transaction->investment->client_id);
                        $e->{'Client Code'} = $transaction->investment->client->client_code;
                        $e->{'Principal'} = $transaction->investment->amount;
                        $e->{'Interest Rate'} = $transaction->investment->interest_rate;
                        $e->{'Invested Date'} = $transaction->investment->invested_date;
                        $e->{'Maturity Date'} = $transaction->investment->maturity_date;
                        $e->{'Done'} = BooleanPresenter::presentYesNo($transaction->done);
                        $e->{'Scheduled Time'} = $transaction->action_date;
                        $e->{'Scheduled Action'} = ucfirst($transaction->transaction_type);

                        return $e;
                    }
                );

        if ($transactions->count()) {
            $file_name = 'Scheduled Client Transactions';

            Excel::fromModel($file_name, $transactions)->store('xlsx');

            $this->sendExcel(
                $file_name . ' - ' . $date->copy()->toFormattedDateString(),
                'Please find attached the requested report of scheduled client transactions for ' .
                $date->copy()->toFormattedDateString(),
                $file_name
            );

            $this->info('The report has been sent to your inbox');
        } else {
            $this->info('No scheduled client transactions were found');
        }
    }
}
