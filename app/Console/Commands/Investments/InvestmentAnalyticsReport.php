<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Product;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Http\Controllers\Api\InvestmentAnalyticsController;
use App\Mail\Mail;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InvestmentAnalyticsReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:investment_analytics_values {type} {entity_id} {from_date} {date} {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the investment analytics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $analyticsController = new InvestmentAnalyticsController();

        $entity_id = $this->argument('entity_id');
        $type = $this->argument('type');
        $from_date = Carbon::parse($this->argument('from_date'));
        $date = Carbon::parse($this->argument('date'));
        $email = $this->argument('email');

        $entity = $type == 'products' ? Product::find($entity_id) : Currency::find($entity_id);

        $data = [
            'As At Date' => $date->toFormattedDateString(),
            'From Date' => $from_date->toFormattedDateString(),
            'Entity' => $entity->name
        ];

        $c = function ($amount, $positive = false, $decimals = 2) {
            return AmountPresenter::currency($amount, $positive, $decimals);
        };

        $output = [
            'Cost Value' => $c($analyticsController->costValue($type, $entity_id, $date)),
            'Market Value' => $c($analyticsController->marketValue($type, $entity_id, $date)),
            'Custody Fees' => $c($analyticsController->custodyFees($type, $entity_id, $date)),
            'Withholding Tax' => $analyticsController->withholdingTax($type, $entity_id, $date),
            'Residual Income' => $analyticsController->residualIncome($type, $entity_id, $date),
            'Weighted Rate' => $analyticsController->weightedRate($type, $entity_id, $date),
            'Weighted Tenor' => $analyticsController->weightedTenor($type, $entity_id, $date),
            'Persistence' => $c($analyticsController->persistence($type, $entity_id, $date, $from_date), false, 4),
        ];

        Mail::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject('Investment Analytics Report')
            ->view(
                'emails.reports.generic',
                ['blocks' => [
                    [
                        'intro' => 'See below the analytics for the following data',
                        'table' => $data
                    ],
                    [
                        'table' => $output
                    ]
                ]]
            )->send();
    }
}
