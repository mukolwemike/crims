<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class InvestmentInflowsReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'investments:inflows {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A report of all the invetments inflows for a specified date period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));
        $userId = $this->argument('user_id');

        $investments = ClientInvestment::where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->whereIn('investment_type_id', [1, 2])
            ->oldest('invested_date')
            ->get();

        $investmentsArray = array();

        foreach ($investments as $investment) {
            $recipient = @$investment->commission->recipient;

            $position = @$recipient->repo->getRecipientUserPositionByDate(Carbon::parse($investment->invested_date));

            $investmentsArray[$investment->product->name][] = [
                'Invested Date' => DatePresenter::formatDate($investment->invested_date),
                'Client Code' => $investment->client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                'Amount' => AmountPresenter::currency($investment->amount),
                'Product' => $investment->product->name,
                'Investment Type' => ucfirst($investment->type->name),
                'Tenor' => $investment->repo->getTenorInMonths(),
                'Interest Rate' =>  $investment->interest_rate,
                'Maturity Date' => DatePresenter::formatDate($investment->maturity_date),
                'FA' => @$investment->commission->recipient->name,
                'FA Type' => @$investment->commission->recipient->repo
                    ->getRecipientType(Carbon::parse($investment->invested_date))->name,
                'FA Branch' => $position ? $position->present()->getDepartmentBranch : ($recipient ?
                    $recipient->present()->getBranch : null),
                'FA Department Unit' => $position ? $position->present()->getDepartmentUnit : ($recipient ?
                    $recipient->present()->getDepartmentUnit : null),
                'FA Status' => @$recipient->present()->getActive,
                'Rolled' => BooleanPresenter::presentYesNo($investment->rolled),
                'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                'Amount Withdrawn' => $investment->repo->getWithdrawnAmount($end),
                'Withdrawal Date' => DatePresenter::formatDate($investment->withdrawal_date),
            ];
        }

        $topups = ClientInvestmentTopup::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->oldest('date')
            ->get();

        foreach ($topups as $topup) {
            $investment = $topup->toInvestment;

            $recipient = @$investment->commission->recipient;

            $position = @$recipient->repo->getRecipientUserPositionByDate(Carbon::parse($investment->invested_date));

            $investmentsArray[$investment->product->name][] = [
                'Invested Date' => DatePresenter::formatDate($investment->invested_date),
                'Client Code' => $investment->client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                'Amount' => AmountPresenter::currency($topup->amount),
                'Product' => $investment->product->name,
                'Investment Type' => ucfirst($investment->type->name),
                'Tenor' => $investment->repo->getTenorInMonths(),
                'Interest Rate' =>  $investment->interest_rate,
                'Maturity Date' => DatePresenter::formatDate($investment->maturity_date),
                'FA' => @$investment->commission->recipient->name,
                'FA Type' => @$recipient->repo->getRecipientType(Carbon::parse($investment->invested_date))->name,
                'FA Branch' => $position ? $position->present()->getDepartmentBranch : ($recipient ?
                    $recipient->present()->getBranch : null),
                'FA Department Unit' => $position ? $position->present()->getDepartmentUnit : ($recipient ?
                    $recipient->present()->getDepartmentUnit : null),
                'FA Status' => @$investment->commission->recipient->present()->getActive,
                'Rolled' => BooleanPresenter::presentYesNo($investment->rolled),
                'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                'Amount Withdrawn' => $investment->repo->getWithdrawnAmount($end),
                'Withdrawal Date' => DatePresenter::formatDate($investment->withdrawal_date),
            ];
        }

        $fname = 'Investments_Inflows_from_'. $start->toDateString().'_to_'.$end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($investmentsArray, $fname);

        $email = [];

        if ($userId) {
            $user = User::find($userId);

            if ($user) {
                $email = [$user->email];
            }
        }

        $this->mailResult($fname, $start, $end, $email);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $start
     * @param $end
     */
    private function mailResult($fname, $start, $end, $email)
    {
        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject('Investment Inflows Export in excel')
            ->text('Here is the export of investments inflows that you requested for the period between ' .
                $start->toDateString() . ' and  ' . $end->toDateString())
            ->excel([$fname])
            ->send();

        \File::delete(storage_path('exports/'.$fname.'.xlsx'));
    }
}
