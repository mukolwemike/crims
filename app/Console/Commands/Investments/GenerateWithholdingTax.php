<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/5/18
 * Time: 8:27 AM
 */

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\WithholdingTax;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Console\Command;

class GenerateWithholdingTax extends Command
{
    protected $signature = 'investments:generate-wht {start?} {end?} {product_id?}';
    
    protected $description = 'Generate and save withholding tax';

    public function fire()
    {
        dd("out of use");
        
        $input = $this->argument();

        $today = Carbon::today();

        $start = isset($input['start']) ? Carbon::parse($input['start']) : Carbon::parse('2015-01-01');

        $end = isset($input['end']) ? Carbon::parse($input['end']) : $today;

        $product = isset($input['product_id']) ? Product::find($input['product_id']) : null;

        $investments = $this->investments($start->copy(), $end->copy(), $product);

        $this->info(count($investments) . ' Investments');

        $investments->each(function ($investment) use ($start, $end) {

            if ($investment->id % 1000 == 0) {
                $this->info($investment->id . ' Investment');
            }

            return $this->process($investment, $start->copy(), $end->copy());
        });
    }

    public function investments(Carbon $start, Carbon $end, Product $product = null)
    {
        $investmentQueryBuilder = is_null($product) ? new ClientInvestment() : $product->investments();

        return $investmentQueryBuilder
            ->whereHas('withdrawals', function ($withdrawals) use ($start, $end) {
                $withdrawals->whereBetween('date', [ $start->startOfDay(), $end->endOfDay() ]);
            })
            ->get();
    }

    public function process(ClientInvestment $investment, Carbon $start, Carbon $end)
    {
        $actions = collect($investment->repo->getPrepared()->actions)
            ->filter(function ($action) use ($start, $end) {
                $d = Carbon::parse($action->date);
                return $d->gte($start) && $d->lte($end);
            });

        $actions->each(function ($action) use ($actions, $investment) {

            if (!$action || $action->wtax == 0 || $action->wtax < 0) {
                return;
            }

            $withdrawal = ($action->type == 'topup')
                ? $actions->where('type', 'withdrawal')
                    ->where('date', $action->date)
                    ->first()
                    ->action
                : $action->action;

            $description = 'Withholding tax from a ' . $action->type . ' of amount ' .
                $investment->product->currency->code . ' ' . number_format($withdrawal->amount);

            return $this->record($action, $withdrawal, $description);
        });
    }

    public function record($action, ClientInvestmentWithdrawal $withdrawal, $description)
    {
        return WithholdingTax::create([
            'client_withdrawal_id' => $withdrawal->id,
            'amount' => $action->wtax,
            'gross_interest_amount' => $action->gross_interest_cumulative,
            'net_interest_amount' => $action->net_interest_cumulative,
            'type' => $action->type,
            'date' => $action->action->date,
            'description' => $description
        ]);
    }
}
