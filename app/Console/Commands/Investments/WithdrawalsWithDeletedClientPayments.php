<?php

namespace App\Console\Commands\Investments;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Cytonn\Support\Mails\Mailer;

class WithdrawalsWithDeletedClientPayments extends Command
{
    protected $signature = 'inv:withdrawals-with-deleted-client-payments {user_id?}';

    protected $description = 'Withdrawals with deleted client payments';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $user = $id = (new User())->find($this->argument('user_id'));

        $this->info('Querying Payments');

        $withdrawals = (new ClientPayment())->withTrashed()->whereNotNull('deleted_at')
            ->where(
                function ($q) {
                    $q->has('withdrawnFromInvestment')->orHas('withdrawal');
                }
            )->get()
            ->map(
                function (ClientPayment $payment) {
                    $this->info('Payment ID : ' . $payment->id);

                    $client = $payment->client;
                    $product = $payment->product;
                    $project = $payment->project;
                    $entity = $payment->shareEntity;
                    $recipient = $payment->recipient;

                    $e = new EmptyModel();

                    $e->{'Withdrawn On'}        =   Carbon::parse($payment->date)->toFormattedDateString();
                    $e->{'Client Code'}         =   $client->client_code;
                    $e->{'Client Name'}         =   ClientPresenter::presentFullNames($client->id);
                    $e->{'Client E-Mail'}       =   $client->contact->email;
                    $e->{'Amount Withdrawn'}    =   (double) $payment->amount;
                    $e->{'Product'}             =   @$product->name;
                    $e->{'Project'}             =   @$project->name;
                    $e->{'Shares Entity'}       =   @$entity->name;
                    $e->{'FA'}                  =   @$recipient->name;
                    $e->{'Narrative'}           =   $payment->description;

                    return $e;
                }
            );

        $this->info('Generating the report');

        $fileName = 'Withdrawals With Deleted Client Payments';

        Excel::fromModel($fileName, ['Withdrawals' => $withdrawals])->store('xlsx');

        $this->info('Sending to your inbox');

        $this->mailResult($fileName, $user);
    }

    private function mailResult($fileName, $user = null)
    {
        if ($user) {
            $email = $user->email;
        } else {
            $email = [ 'mchaka@cytonn.com', 'tkimathi@cytonn.com'];
        }
        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Here is the requested export of withdrawals with deleted client payments')
            ->excel([$fileName])
            ->send();

        \File::delete(storage_path('exports/'.$fileName.'.xlsx'));
    }
}
