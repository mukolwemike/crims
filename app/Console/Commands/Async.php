<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ClientInvestment;
use Illuminate\Console\Command;

class Async extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'async:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $w = $this->worker();

        $this->info($w);
        $p = $this->pool();

        dump($p);
    }

    public function worker()
    {
        return (new \Cytonn\System\Processing\Async())->processClosure(
            function ($inv) {
                return $inv;
            },
            ClientInvestment::first()->repo->principal()
        );
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function pool()
    {
        $calc = function ($inv) {
            return $inv->repo->getTotalValueAsAtEndOfDay();
        };

        $investments = ClientInvestment::take(20)->get();

        return (new \Cytonn\System\Processing\Async())->map($investments, $calc);
    }
}
