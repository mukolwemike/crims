<?php

namespace App\Console\Commands;

use App\Cytonn\Models\AuditConfirmation;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Handlers\ExceptionHandler;
use Exception;
use Illuminate\Console\Command;

class SendAuditConfirmations extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'audit:send-confirmations';

    const TEST = false;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send audit confirmations to clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $batchId = 4;

        $batch = AuditConfirmation::where('batch', $batchId)
            ->orderBy('id', 'ASC')
            ->whereNull('sent')
            ->get();

        $batch->each(
            function ($item) {
                try {
                    $this->send($item);
                } catch (Exception $exception) {
                    app(ExceptionHandler::class)->reportException($exception);
                }
            }
        );

        return 0;
    }

    private function send($item)
    {
        $this->info('Sending item id ' . $item->id);

        $client = Client::where('client_code', $item->client_code)->first();

        if (is_null($client)) {
            $this->error('Could not find code ' . $item->client_code);
            return;
        }

        $audit_date = new Carbon('2018-12-31');

        $sender = User::where('email', 'lmugo@cytonn.com')->first();

        $pdf = \PDF::loadView('clients.audit.confirmation_letter', [
            'audit_date' => $audit_date,
            'client' => $client,
            'amount' => $item->balance,
            'sender' => $sender,
            'currency' => $item->currency,
            'product' => $item->product,
            'spv' => $item->spv
        ]);

        $pdf->setEncryption($client->repo->documentPassword());

        $confirmation = $pdf->output();

        $mailer = new \Cytonn\Mailers\AuditConfirmation();

        $mailer->sendToClient($client, $confirmation, $sender, $audit_date, $item, static::TEST);

        if (!static::TEST) {
            $item->sent = true;

            $item->save();
        }

        return;
    }
}
