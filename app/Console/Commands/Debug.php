<?php

namespace App\Console\Commands;

use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Data\Iprs;
use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Kyc\IprsRequest;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationDocument;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\RealestateStatementCampaign;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Reporting\Custom\Persistence\Build;
use App\Cytonn\Reporting\Custom\Query\Filter as CustFilter;
use App\Cytonn\USSD\USSDRepository;
use App\Http\Controllers\InvestmentReportsController;
use App\Jobs\Clients\ClientContactsExportCommand;
use App\Jobs\Instructions\ExportBankInstructions;
use App\Jobs\Portfolio\InterestExpenseReport;
use App\Jobs\Unitization\UnitFundReverseTransfer;
use App\Jobs\Unitization\UnitTrustClientsContacts;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Handlers\AddInflowFromTransaction;
use Cytonn\Clients\Approvals\Handlers\UnitFundReverseTransaction;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Mailers\StatementMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\Custom\Report;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Performance;
use Cytonn\Unitization\Trading\Reverse;
use Cytonn\Users\UserClientRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use ServiceBus\Events\Distribution\MonthlyReports\ConsolidatedBusinessMonthlyReport;

class Debug extends Command
{
    use ExcelMailer, LocksTransactions;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'debug:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run any code in the debug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $transfer = UnitFundTransfer::findOrFail(949);

        $reverse = new Reverse();

        $reverse->transfer($transfer);

        dd('ds');

        dispatch((new UnitFundReverseTransfer($transfer))->onQueue(config('queue.priority.high')));

        dd('ds');

        $start = Carbon::parse('2020-02-24');
        $end = Carbon::parse('2020-02-24');

        $user = User::findOrFail(3);
        $user2 = User::findOrFail(196);
        dispatch((new ExportBankInstructions($start, $end, $user))->onQueue(config('queue.priority.medium')));
        dispatch((new ExportBankInstructions($start, $end, $user2))->onQueue(config('queue.priority.high')));
        dd('ds');
        $this->getNonCompliantUTFClients();
    }

    public function getNonCompliantUTFClients()
    {
        $clients = Client::where('fund_manager_id', 6)->get();

        $data = [];

        foreach ($clients as $client) {
            $compliant = $client->repo->compliant();

            if (!$compliant) {
                $data [] = [
                    'Client Code' => $client->client_code,
                    'Client Name' => ClientPresenter::presentFullNames($client->id),
                    'ID or Passport' => $client->id_or_passport ? $client->id_or_passport : ''
                ];
            }
        }

        $date = Carbon::today();

        $fileName = 'Non-Compliant UTF Clients as at ' . DatePresenter::formatDate($date);

        $this->sendReports($data, $fileName);
    }

    public function getInactiveClientsDetails()
    {
        $data = [];

        $clientUsers = ClientUser::where('active', '!=', 1)
            ->where('updated_at', '>', '2020-01-22 19:00:00')->get();

        foreach ($clientUsers as $clientUser) {
            foreach ($clientUser->clients as $client) {
                $data[] = [
                    'First Name' => $client->contact->firstname,
                    'Last name' => $client->contact->lastname,
                    'Email' => $client->contact->email,
                    'Client Code' => $client->client_code,
                    'Phone Number' => $client->contact->phone,
                ];
            }
        }


        $this->sendReports($data, 'Inactive Clients Details');
    }

    private function testUnitFundDepositCodeExtraction()
    {
        $unitFunds = UnitFund::whereNotNull('deposit_code')->get();

        $reference = trim('1614hp');

        $fundCode = '';
        $newReference = '';

        foreach ($unitFunds as $unitFund) {
            $lastPartOfReference = strtoupper(substr($reference, -strlen($unitFund->deposit_code)));
            $extractedClientCode = substr($reference, 0, -strlen($unitFund->deposit_code));

            if ($lastPartOfReference == strtoupper($unitFund->deposit_code)) {
                $fundCode = $unitFund->deposit_code;

                $newReference = $extractedClientCode;
            }
        }

        dd($reference, $extractedClientCode, $lastPartOfReference, $fundCode, $newReference);
    }

    private function disableClientUser()
    {
        UnitFundPurchase::where('number', '<', '100.00')->get()->each(function ($unitFundPurchase) {
            $unitFundPurchase->client->clientUsers->each(function ($clientUser) {
                $clientUser->update(['active' => 0]);
            });
        });
    }

    private function exportInterTransferClients()
    {
        $fileName = 'Inter Transfer Clients';

        $transfers = UnitFundTransfer::orderBy('date', 'DESC')->get();

        $data = $transfers->map(function (UnitFundTransfer $transfer) {
            $purchaseValue = $transfer->purchase->number * $transfer->purchase->price;

            $saleValue = $transfer->sale->number * $transfer->sale->price;

            $senderBalance = $transfer->sender->calculateFund($transfer->fund, Carbon::now())->totalUnits();

            $recipientBalance = $transfer->recipient->calculateFund($transfer->fund, Carbon::now())->totalUnits();

            return [
                'Date' => Carbon::parse($transfer->date)->toDateTimeString(),
                'Sender' => ClientPresenter::presentFullNames($transfer->transferer_id),
                'Sender Client Code' => $transfer->sender->client_code,
                'Sender Account Balance' => AmountPresenter::currency($senderBalance),
                'Recipient' => ClientPresenter::presentFullNames($transfer->transferee_id),
                'Recipient Client Code' => $transfer->recipient->client_code,
                'Recipient Account Balance' => AmountPresenter::currency($recipientBalance),
                'Number Of Units' => AmountPresenter::currency($transfer->number),
                'Purchase Value' => AmountPresenter::currency($purchaseValue),
                'Sale Value' => AmountPresenter::currency($saleValue)
            ];
        });

        $this->sendReports($data, $fileName);
    }

    private function activateClientUserAccounts()
    {
        $clientsIds = [9627,'9627B',9643,9646,11184,11706,14841,15049,15908,15963,15965,16830,16831,16832,16833,16834,
        16835,15694,16145,16146,16147,16149,16920,16921,16922,16923,16924,16925,15705,16181,16182,16184,16185,16914,
        16915,16916,16917,16918,15709,16207,16208,16209,16210,16211,16990,16991,16992,16993,16994,15711,15715,16168,
        16169,16171,16173,16174,16910,16911,16912,16913,15778,16215,16216,16218,16219,16220,16221,17053,17055,17058,
        17059,17060,16243,16244,16245,16246,16247,16250,16251,17027,17028,17030,17031,15792,16223,16224,16225,16226,
        16227,16230,16231,17063,17065,17066,17067,17068,16283,16290,15797,16232,16233,16234,16235,16236,16237,16238,
        16239,17042,17044,17045,17046,17047,15800,16254,16256,16257,16258,16259,16260,16261,16263,17036,17037,17038,
        17039,17040,16177,16178,16201,16202,16203,16204,16205,16995,16996,16997,16998,17000,17049,17106,17108,17109,
        17111,17113,17115,17116,17122,17123,17124,17126,17127,17132,17134,17136,17139,17143,17149,17151,17190,17191,
            17193,17195,17198,17209,17210];

        Client::whereNotIn('client_code', $clientsIds)->get()->each(function ($client) {
            $client->clientUsers->each(function ($clientUser) {
                $clientUser->update(['active' => 1]);
            });
        });

        dd('done');

        UnitFundPurchase::where('number', '<', '100.00')->get()->each(function ($unitFundPurchase) {
            $unitFundPurchase->client->clientUsers->each(function ($clientUser) {
                $clientUser->update(['active' => 1]);
            });
        });
        dd('done');

        $date = Carbon::now()->subDay(6);

        $accs = ClientUser::where('active', '!=', 1)->where('updated_at', '>', $date)
            ->get();

        $this->info('Accounts ' . count($accs));

        foreach ($accs as $clientUser) {
            $clientUser->update(['active' => 1]);
        }
    }

    private function exportInactiveClientUsers()
    {
        $date = Carbon::now()->subDay(4);

        $fileName = 'Clients Deactivated on ' . DatePresenter::formatDateTime($date);

        $data = [];

        $accs = ClientUser::where('active', 0)->where('updated_at', '>', $date)
            ->get();

        foreach ($accs as $acc) {
            $clients = $acc->clients;

            foreach ($clients as $client) {
                $data [] = [
                    'Client Code' => $client->client_code,
                    'Client Name' => ClientPresenter::presentFullNames($client->id),
                    'Email' => $client->contact->email,
                    'Phone' => $client->contact->phone,
                    'Joint' => BooleanPresenter::presentYesNo($client->joint),
                    'Client Type' => $client->clientType->name,
                    'Date Joined' => DatePresenter::formatDateTime($client->contact->created_at)
                ];
            }
        }

        $this->sendReports($data, $fileName);
    }

    private function sendReports($data, $fileName)
    {
        ExcelWork::generateAndStoreSingleSheet($data, $fileName);

        $email = ['awanjohi@cytonn.com'];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find the attached report generated')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    public function consolidatedBusinessMonthlyReport()
    {

        $data = [
            'start_date' => '2019-01-01',
            'end_date' => '2019-12-31',
            'department_unit' => 0,
            'exclude_others' => 1
        ];

        //$filters = json_encode($data);

        (new ConsolidatedBusinessMonthlyReport())->handle($data);
    }

    public function clientContactsExportCommand()
    {
        $this->info('Started ' . Carbon::now()->toDateTimeString());

        $user = User::find(71);

        $export = new ClientContactsExportCommand('combined', $user);

        $export->handle();

        $this->info('Exported ' . Carbon::now()->toDateTimeString());
    }

    public function unitTrustClientsContacts()
    {
        $this->info('UT Started ' . Carbon::now()->toDateTimeString());

        $user = User::findOrFail(71);

        $funds = UnitFund::all();

        foreach ($funds as $fund) {
            $test = new UnitTrustClientsContacts($user, $fund);
            $test->handle();
        }

        $this->info('UT Exported ' . Carbon::now()->toDateTimeString());
    }

    public function statementsReport()
    {
        $date = Carbon::today()->subDays(3);

        $campaigns = [
            'investments' => StatementCampaign::where('send_date', '>=', $date)
                ->whereNull('report_sent_on')->latest('send_date')->latest()->get(),
            're' => RealestateStatementCampaign::where('send_date', '>=', $date)
                ->whereNull('report_sent_on')->latest('send_date')->latest()->get(),
            'unitization' => UnitFundStatementCampaign::where('send_date', '>=', $date)
                ->whereNull('report_sent_on')->latest('send_date')->latest()->get()
        ];

        (new StatementMailer())->report($campaigns);
    }

    public function approvalLock()
    {
        $approval = \App\Cytonn\Models\ClientTransactionApproval::find(406124);

        $key = get_class($approval->handler()) . '_client_16375';

        $engine = (new \Cytonn\Clients\Approvals\Engine\Approval($approval));

        $lock = $this->getAtomicLock($key);

        dd($lock);
    }

    public function clientTransactionApproval()
    {
        \DB::transaction(function () {
            $client = \App\Cytonn\Models\Client::find(16375);

            $client->update([
                'joint' => 1
            ]);

            dd("Done");

            $approval = ClientTransactionApproval::create([
                'client_id' => 16375,
                'transaction_type' => 'add_cash_to_account',
                'payload' => [],
                'sent_by' => getSystemUser()->id,
                'approved' => false,
                'approved_by' => null,
                'approved_on' => null,
                'awaiting_stage_id' => 6
            ]);

            dd($approval);
        });
    }

    public function paymentsProcessor()
    {
        $data = "{\"id\":71683,\"amount\":\"500.00\",\"narration\":\"Mpesa deposit. JOSEPH M. NDUATI  Phone 254721758251 Amount: 500.00\",\"date\":\"2019-11-18\",\"entry_date\":\"2019-11-18\",\"reference\":\"11336\",\"transaction_id\":\"NKI32EVR1D\",\"source_number\":\"254721758251\",\"source_identity\":\"MPESA\",\"payer_name\":\"JOSEPH M. NDUATI\",\"payer_email\":null,\"available_balance\":\"536446.00\",\"exchange_rate\":1,\"outgoing_reference\":null,\"account_no\":\"775093\",\"account_name\":\"Cytonn Asset Managers Ltd - Mpesa\",\"bank_name\":\"Mpesa\",\"bank_swift\":\"MPESA\"}";

        $data2 = "{\"id\":71188,\"amount\":\"30.00\",\"narration\":\"Mpesa deposit. JOSEPH M. NDUATI  Phone 254721758251 Amount: 30.00\",\"date\":\"2019-11-17\",\"entry_date\":\"2019-11-17\",\"reference\":\"11336\",\"transaction_id\":\"NKH31C2ZCB\",\"source_number\":\"254721758251\",\"source_identity\":\"MPESA\",\"payer_name\":\"JOSEPH M. NDUATI\",\"payer_email\":null,\"available_balance\":\"1372955.00\",\"exchange_rate\":1,\"outgoing_reference\":null,\"account_no\":\"775093\",\"account_name\":\"Cytonn Asset Managers Ltd - Mpesa\",\"bank_name\":\"Mpesa\",\"bank_swift\":\"MPESA\"}";

        $data = \GuzzleHttp\json_decode($data2);

        $test = new \App\Jobs\Payments\PaymentsProcessor($data);

        $test->handle();
    }

    public function interestExpenseReport()
    {
        $fundManager = FundManager::find(1);
        $start = Carbon::parse('2015-01-01');
        $end = Carbon::parse('2019-10-22');

        $user = User::find(37);

        $test = new InterestExpenseReport($start, $end, $user, $fundManager);
        $test->handle();
    }

    public function yieldCalculation()
    {
        $fund = UnitFund::find('32');

        $perf = new Performance($fund, Carbon::today());

        dd($perf->portfolioReturn());

        $sec = PortfolioSecurity::find(4);

        dd($sec->repo->yieldCalculation());
    }

    public function unitFundReverseTransaction()
    {
        \DB::transaction(function () {
            $sales = [2610, 2613, 2616];

            foreach ($sales as $s) {
                $sale = UnitFundSale::find($s);

                $approval = UnitFundReverseTransaction::create(
                    $client = $sale->client,
                    null,
                    getSystemUser(),
                    [
                        'sale_id' => $s,
                        'type' => 'unit_fund_sale',
                        'unit_fund_id' => 17,
                        'client_id' => $client->id,
                        'clientId' => $client->id,
                        'reason' => 'Incorrect account'
                    ]
                );

                $approval->systemExecute();
            }
        });
    }

    public function requestMpesaPayment()
    {
        $sys = new CytonnPaymentSystem();

        $sys->requestMpesaPayment(
            '597745',
            '254704385456',
            5000,
            '1951',
            'CYT'
        );
    }

    public function unitFundCommissionSchedule()
    {
        $schedules = UnitFundCommissionSchedule::doesntHave('commission')->get(['id'])->toArray();

        $schedules2 = UnitFundCommissionSchedule::whereHas('commission', function ($q) {
            $q->doesntHave('purchase');
        })->get(['id'])->toArray();

        dd($schedules, $schedules2);
    }

    public function iprs()
    {
        $appl = ClientFilledInvestmentApplication::where('channel_id', 3)->get();

        $appl->each(function ($ap) {
            $client = @$ap->application->client;

            if ($client) {
                (new Iprs())->createRequest($client);
            }
        });
    }

    private function clientsNoPhoneNumbers()
    {
        $clients = Client::whereNull('phone')
            ->whereNull('telephone_home')
            ->whereNull('telephone_office')
            ->get()
            ->filter(function ($client) {
                return isNotEmptyOrNull($client->contact->phone);
            })
            ->map(function ($client) {
                $out = new EmptyModel();

                $out->{'Id'} = $client->id;
                $out->{'Client Code'} = $client->client_code;
                $out->{'Client Name'} = ClientPresenter::presentFullNames($client->id);
                $out->{'Email'} = Arr::first($client->getContactEmailsArray());

                return $out;
            });


        $fileName = 'Clients with no phone recorded';

        ExcelWork::generateAndStoreSingleSheet($clients, $fileName);

        $filePath = storage_path('exports/' . $fileName . '.xlsx');

        Mailer::compose()
            ->to(['molukaka@cytonn.com', 'ikipngeno@cytonn.com'])
            ->from('support@cytonn.com')
            ->subject($fileName)
            ->text('Please find attached an export of clients with no phone recorded')
            ->excel([$fileName])
            ->send();

        \File::delete($filePath);
    }

    private function clientUsersNoPhoneNumbers()
    {
        $users = ClientUser::whereNull('phone')
            ->get()
            ->map(function ($user) {
                $out = new EmptyModel();

                $out->{'Id'} = $user->id;
                $out->{'Username'} = $user->username;
                $out->{'Email'} = $user->email;
                $out->{'First Name'} = $user->firstname;
                $out->{'Last Name'} = $user->lastname;
                $out->{'Active Status'} = $user->active ? 'Active' : 'Not Active';

                return $out;
            });

        $fileName = 'Client Users with no phone recorded';

        ExcelWork::generateAndStoreSingleSheet($users, $fileName);

        $filePath = storage_path('exports/' . $fileName . '.xlsx');

        Mailer::compose()
            ->to(['molukaka@cytonn.com', 'ikipngeno@cytonn.com'])
            ->from('support@cytonn.com')
            ->subject($fileName)
            ->text('Please find attached an export of client users with no phone recorded')
            ->excel([$fileName])
            ->send();

        \File::delete($filePath);
    }

    private function createAccounts()
    {
        $clients = Client::has('unitFundPurchases')
            ->doesntHave('clientUsers')
            ->get();

        $this->info("Found {$clients->count()} clients, fixing ...");


        $clients->each(function (Client $client) {
            $this->info("Provisioning " . $client->client_code . ' ' . ClientPresenter::presentFullNames($client->id));

            $phone = count($client->getContactPhoneNumbersArray());

            if ($client->type->name != 'individual') {
                $this->error("Client is not individual - " . $client->client_code);

                return;
            }

            if ($phone == 0) {
                $this->error("Client has no phone numbers - " . $client->client_code);

                return;
            }

            try {
                (new UserClientRepository())->provisionForClient($client);
            } catch (\Exception $e) {
                $this->error('Failed...');
            }
        });
    }

    private function complianceTest()
    {
        $fms = [3, 4, 5, 6];

        $required = ClientComplianceChecklist::where('fund_manager_id', 1)->get();

        foreach ($fms as $fm) {
            foreach ($required as $kyc) {
                $data = Arr::only($kyc->toArray(), ['required', 'name', 'slug', 'client_type_id']);
                $data['fund_manager_id'] = $fm;

                ClientComplianceChecklist::create($data);
            }
        }

        return;

        $client = Client::find('1015');

        dd($client->repo->requiredPendingKYCDocuments());
    }

    private function bonds()
    {
        $inv = DepositHolding::find('5238');

        dd($inv->calculate()->getPrepared());
    }

    private function createPayment()
    {
        return;
        $transaction = CustodialTransaction::find('257348');

        $approval = AddInflowFromTransaction::create(
            null,
            null,
            getSystemUser(),
            ['transaction_id' => $transaction->id]
        );

        $approval->systemExecute();
    }

    private function getChannelsData($startDate = null, $endDate = null)
    {
        $startDate = Carbon::parse($startDate)->startOfDay()->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($endDate)->endOfDay()->format('Y-m-d H:i:s');

        Channel::all()->each(function ($channel) use ($startDate, $endDate) {
            $apps = ClientFilledInvestmentApplication::where('channel_id', $channel->id)
                ->whereBetween('created_at', [$startDate, $endDate])->count();

            $this->info("{$apps} {$channel->name} applications found");

            $withInv = ClientFilledInvestmentApplication::where('channel_id', $channel->id)
                ->whereBetween('created_at', [$startDate, $endDate])
                ->whereHas('application', function ($appl) {
                    $appl->whereHas('client', function ($client) {
                        $client->has('unitFundPurchases');
                    });
                })->get();

            $withInvCount = count($withInv);

            $this->info("{$withInvCount} {$channel->name} have invested");

            $amountInvested = $withInv->map(function ($clientFilledApplication) {
                return $clientFilledApplication->application->client->unitFundPurchases()->sum('number');
            })->toArray();

            $totalAmountInvested = number_format(array_sum($amountInvested), 2);

            $this->info("{$totalAmountInvested} {$channel->name} amount invested");

            $this->info(" ");
        });
    }


    public function testReporting()
    {
        \DB::connection()->enableQueryLog();

        $r = \App\Cytonn\Models\Reporting\Report::latest()->first();

        $b = new Build();
        $rep = $b->build($r);

        dd($rep->export());

        $report = new Report(ClientInvestment::class);

        $grpd1 = CustFilter::where('id', '>', 101);
        $grpd2 = CustFilter::where('id', '<', 10000);
        $nested = CustFilter::where('id', '>', 100);
        $report->filter(CustFilter::where('amount', '>=', 1000000));
        $report->filter(CustFilter::where('amount', '<=', 1000000));
        $report->filter(CustFilter::scope('statement', Carbon::today()));
        $report->filter(CustFilter::whereHas('client', $nested));
        $report->filter(CustFilter::where([$grpd1, $grpd2]));


        $report->with(['client', 'withdrawals', 'topupsTo', 'topupsFrom', 'client.contact']);
        $report->run();
    }

    /**
     * @throws \Exception
     */
    private function lockTest()
    {
        $key = 'testing_____';

        $this->executeWithinAtomicLock(function () {
            $this->info('Executing...');

            sleep(20);
        }, $key);

        return;

//        dd(cache()->get('nnsns'));

        $lock = $this->getAtomicLock($key, 60, 5);

        $this->info('Lock acquired ' . Carbon::now()->toDateTimeString());

        sleep(60);

        $this->info('Done ' . Carbon::now()->toDateTimeString());

        $this->releaseAtomicLock($lock);
    }


    private function testWS()
    {
        $iprs = new Iprs();

        dd($iprs->test('2844350500'));
    }

    private function correctAppls()
    {
        $this->correctIndividual();
        $this->info('Done individual');

        $this->correctCorporate();
        $this->info('Done corporate');
    }

    private function correctIndividual()
    {
        $clients = Client::whereHas('contact', function ($contact) {
            $contact->where('entity_type_id', 2)
                ->where(function ($w) {
                    $w->whereNotNull('lastname')
                        ->where('lastname', '!=', "");
                })
                ->where(function ($w) {
                    $w->whereNull("corporate_registered_name")
                        ->orWhere('corporate_registered_name', '');
                });
        })
//            ->take(5)
            ->get();

        $this->info("Correcting {$clients->count()} individual clients");

        $clients->each(function (Client $client) {
            $contact = $client->contact;

            $form = $client->applications()->first()->form;

            $contact->update([
                'entity_type_id' => 1,
                'firstname' => $form->firstname,
                'middlename' => $form->middlename,
                'lastname' => $form->lastname,
                'phone' => $form->telephone_home
            ]);

            $client->update([
                'client_type_id' => 1,
                'phone' => $form->telephone_home
            ]);
        });
    }

    private function correctCorporate()
    {
    }

    private function addMpesaAccounts()
    {
//        ClientBankAccount::where('account_number', 'not like', '254%')
//            ->where('branch_id', '1514')
//            ->get()
//            ->each(function ($acc) {
//                $no = $acc->account_number;
//
//                if (starts_with($no, '0')) {
//                    $no = '254'.ltrim($no, '0');
//
//                    $acc->update(['account_number' => str_replace(" ", "", $no)]);
//                }
//            });
//
//        return;

        $clients = Client::wherehas('applications', function ($app) {
            $app->where('unit_fund_id', 17);
        })->get()
            ->reject(function (Client $client) {
                return ClientBankAccount::where('client_id', $client->id)
                    ->where('branch_id', '1514')->exists();
            })
            ->each(function (Client $client) {
                (new USSDRepository())->createMpesaAccount($client);
            });

        $this->info("Modified {$clients->count()} clients");
    }


    private function correctUsers()
    {
        $users = ClientUser::where('phone_country_code', '+25')
            ->get()
            ->each(function ($user) {
                $phone = $user->phone_country_code . $user->phone;

//                dd(['phone' => substr($phone, 4), 'phone_country_code' => '+254']);

                $user->update(
                    ['phone' => '0' . substr($phone, 4), 'phone_country_code' => '+254']
                );
            });

        dd('');

        $users = ClientUser::where('phone', 'like', '% %')->get()
            ->each(function (ClientUser $user) {
                if (str_contains($user->phone, " ")) {
                    $user->update(['phone' => str_replace(" ", "", $user->phone)]);

                    $this->info("Updated " . $user->username);
                }
            });

        $this->info("Updated {$users->count()} users");
    }

    private function testSMS()
    {
        \Config::set('cytonn_support.sms.default', 'africastalking');

        \SMS::send(
            ['0776318088', '0738210720', '0704385456', '0720023598'],
            'CY - AT - Test @ ' . Carbon::now()->toFormattedDateString() . ' ' . Carbon::now()->toTimeString()
        );
        dd('end');
    }

    private function resetAppl()
    {
        $emails = [
            'mchaka@cytonn.com',
            'ikipngeno@cytonn.com',
            'test@gmail.com',
            'molukaka@cytonn.com',
            'cytonntesters@gmail.com'
        ];

        DB::transaction(function () use ($emails) {
            $clients = Client::whereHas('contact', function ($c) use ($emails) {
                $c->whereIn('email', $emails);
            })->get();

            $clients->each(function (Client $client) use ($emails) {
                IprsRequest::where('client_id', $client->id)->delete();

                DB::table('client_iprs_validations')->where('client_id', $client->id)->delete();

                DB::table('client_user_access')->where('client_id', $client->id)->delete();

                $client->applications->each(function ($appl) {

                    if ($appl->form) {
                        $docs = $appl->form->filledDocuments;

                        $docs->each(function (ClientFilledInvestmentApplicationDocument $dc) {
//                            $dc->document()->forceDelete();
                            $dc->update(['document_id' => null]);

                            $dc->forceDelete();
                        });

                        $appl->form->filledDocuments()->forceDelete();

                        $appl->form()->forceDelete();
                    }
                });

                $client->applications()->forceDelete();

                $client->bankAccounts()->forceDelete();

                $client->contact()->forceDelete();
                $client->forceDelete();

                $users = ClientUser::whereIn('email', $emails)->get();

                $users->each(function ($user) {
                    DB::table('client_password_reminders')->where('user_id', $user->id)->delete();
                    DB::table('client_user_access')->where('user_id', $user->id)->delete();

                    $user->forceDelete();
                });

                $this->info("Deleted " . $client->client_code);
            });
        });

        $this->info("Removed clients");

        ClientFilledInvestmentApplication::whereIn('email', $emails)
            ->get()
            ->each(function (ClientFilledInvestmentApplication $app) {
                $app->application()->forceDelete();

                $app->documents()->forceDelete();

                $app->filledDocuments()->forceDelete();

                $app->forceDelete();
            });


        $this->info("Removed appls");
    }

    public function deactivateBanks()
    {
        $account_numbers = ['1222159546'];

        $banks = ClientBankAccount::whereIn('account_number', $account_numbers)->get();

        foreach ($banks as $bank) {
            $bank->update(['active' => 1]);
        }
    }

    public function exportUTFInterestExpense(UnitFund $fund = null, $requests = null)
    {
        $day = Carbon::now()->subDays(20); //setting date inside month required

        $start = Carbon::parse($day)->startOfMonth()->toDateString();

        $end = Carbon::parse($day)->endOfMonth()->toDateString();

        $user = 94; // user_id used to get recipient email

        $fund = 17; // unit_fund_id

        \Artisan::call('utf:interest-expense', [
            'start' => $start,
            'end' => $end,
            'user_id' => $user,
            'fund_id' => $fund
        ]);
    }
}
