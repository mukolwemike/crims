<?php

namespace App\Console\Commands\Temporary;

use App\Cytonn\Clients\ClientLoyaltyPoints\UnitFundLoyaltyCalculator;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LoyaltyPointsReport extends Command
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $date;
    protected $user;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:loyalty-points-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Estimate the total amount of points that will be awarded on the launch date';

    /**
     * Create a new command instance.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->user = getSystemUser();

        $this->date = Carbon::now();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $invPoints = $this->getInvestmentsPoints();

        $fundPoints = $this->getUnitTrustsPoints();

        $rePoints = $this->getHoldingPoints();

        $out = [
            'Investments Points' => $invPoints,
            'Unit Trust Points' => $fundPoints,
            'Holding(RE) Points ' => $rePoints
        ];

        $fileName = 'Loyalty Points Summary';

        ExcelWork::generateAndStoreMultiSheet($out, $fileName);

        $invSum = round($invPoints->sum(function ($inv) {
            return (float)(str_replace(',', '', $inv->Points));
        }));

        $fundSum = round($fundPoints->sum(function ($fund) {
            return (float)(str_replace(',', '', $fund->Points));
        }));

        $reSum = round($rePoints->sum(function ($re) {
            return (float)(str_replace(',', '', $re->Points));
        }));

        Mailer::compose()
            ->to([$this->user->email])
            ->bcc(['ikipngeno@cytonn.com','molukaka@cytonn.com'])
            ->subject('Client Loyalty Points report')
            ->view('emails.client_loyalty_report', [
                'invPoints' => $invSum, 'fundPoints' => $fundSum, 'rePoints' => $reSum])
            ->excel([$fileName])
            ->send();

        $file_path = storage_path() . '/exports/' . $fileName . '.xlsx';

        \File::delete($file_path);
    }

    private function getInvestmentsPoints()
    {
        return Client::whereHas('investments')
            ->orderBy('client_code', 'ASC')
            ->get()
            ->map(function ($client) {
                $investments = $client->investments()->get();

                $points = 0;
                foreach ($investments as $investment) {
                    $points += ($investment->calculateLoyalty($this->date)->totalPoints());
                }

                $c = new EmptyModel();
                $c->Client_Code = $client->client_code;
                $c->Name = ClientPresenter::presentJointFullNames($client->id);
                $c->Points = AmountPresenter::currency($points, false, 2);

                return $c;
            });
    }

    private function getUnitTrustsPoints()
    {
        return Client::whereHas('unitFundPurchases')
            ->orderBy('client_code', 'ASC')
            ->get()
            ->map(function ($client) {
                $purchases = $client->unitFundPurchases()->get();

                $points = 0;
                foreach ($purchases as $purchase) {
                    $points += (new  UnitFundLoyaltyCalculator($client, $purchase->unitFund, $this->date))
                        ->totalPoints();
                }

                $c = new EmptyModel();
                $c->Client_Code = $client->client_code;
                $c->Name = ClientPresenter::presentJointFullNames($client->id);
                $c->Points = AmountPresenter::currency($points, false, 2);

                return $c;
            });
    }

    private function getHoldingPoints()
    {
        return Client::whereHas('unitHoldings')
            ->orderBy('client_code', 'ASC')
            ->get()
            ->map(function ($client) {
                $holdings = $client->unitHoldings()->get();

                $points = 0;
                foreach ($holdings as $holding) {
                    $points += ($holding->calculateLoyalty($this->date)->totalPoints());
                }

                $c = new EmptyModel();
                $c->Client_Code = $client->client_code;
                $c->Name = ClientPresenter::presentJointFullNames($client->id);
                $c->Points = AmountPresenter::currency($points, false, 2);

                return $c;
            });
    }
}
