<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/24/18
 * Time: 9:50 AM
 */

namespace App\Console\Commands\Temporary;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\ExcelMailer;
use Illuminate\Console\Command;

class MonthlyMaturityNotifications extends Command
{
    use ExcelMailer;

    protected $signature = 'crims:monthly_maturities {start?} {end?} {user?} {product?}';

    protected $description = 'Monthly maturities report for CHYS for the year 2019';

    public function handle()
    {
        $this->generate();
    }

    public function generate()
    {
        $options = $this->argument();

        $start = isset($options['start']) ? Carbon::parse($options['start']) : Carbon::today();

        $end = isset($options['end']) ? Carbon::parse($options['end']) : $start->copy()->endOfYear();

        $user = isset($options['user']) ? User::findOrFail($options['user']): null;

        $months = $this->getMonths($start->copy(), $end->copy());

        $email = $user ? [$user->email] : config('system.administrators');

        $products = [];

        isset($options['product']) ? array_push($products, $options['product'])
            : $products = Product::all()->pluck('id')->toArray();

        foreach ($products as $id) {
            $product = Product::where('id', $id)->active()->first();
            
            $fname = $product->name . ' Maturity Report';

            \Excel::create($fname, function ($excel) use ($product, $months) {

                foreach ($months as $month) {
                    $startMonth = Carbon::parse($month)->startOfMonth();

                    $endMonth = Carbon::parse($month)->endOfMonth();

                    $name = $startMonth->copy()->toFormattedDateString() . ' - '
                        .$endMonth->copy()->toFormattedDateString();

                    $excel->sheet($name, function ($sheet) use ($product, $startMonth, $endMonth) {
                        $this->excelSheet($product, $sheet, $startMonth, $endMonth);
                    });
                }
            })->store('xlsx');

            $this->sendExcel(
                ucwords($fname) . ' Report',
                "Please find attached",
                $fname,
                $email
            );
        }
    }

    public function excelSheet(Product $product, $sheet, $startMonth, $endMonth)
    {
        $investments = ClientInvestment::where('product_id', $product->id)
            ->whereBetween('maturity_date', [ $startMonth->copy()->startOfDay(), $endMonth->endOfDay() ])
            ->active()
            ->get();

        return $sheet->loadView('investment.reports.maturity_report', [
            'investments' => $investments
        ]);
    }

    private function getMonths(Carbon $startDate, Carbon $endDate)
    {
        $months = [];

        for ($start = $startDate->copy(); $start->lte($endDate->copy()); $start = $start->addMonthNoOverflow()) {
            $months[] = $start->copy()->startOfMonth();
        }

        return $months;
    }
}
