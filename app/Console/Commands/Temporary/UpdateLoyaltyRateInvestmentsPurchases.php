<?php

namespace App\Console\Commands\Temporary;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateLoyaltyRateInvestmentsPurchases extends Command
{
    protected $signature = 'crims:update_loyalty_rates_investments_purchases';

    protected $description = 'Update loyalty rates to clients investments or unit fund purchases';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->updateClientsInvestments();

        $this->updateClientsUnitFundPurchases();
    }

    protected function updateClientsInvestments()
    {
        $investments = ClientInvestment::whereNull('loyalty_rate')->get();

        foreach ($investments as $investment) {
            $tenor = $investment->client->repo->getConsecutiveMonthsInvested(Carbon::parse($investment->invested_date));

            if ($tenor < 36) {
                $investment->loyalty_rate = 0.00025;
            } elseif ($tenor >= 36 && $tenor < 72) {
                $investment->loyalty_rate = 0.00035;
            } elseif ($tenor >= 72 && $tenor < 132) {
                $investment->loyalty_rate = 0.00045;
            } elseif ($tenor >= 132) {
                $investment->loyalty_rate = 0.0005;
            }
            $investment->save();
        }
    }

    public function updateClientsUnitFundPurchases()
    {
        $purchases = UnitFundPurchase::whereNull('loyalty_rate')->get();

        foreach ($purchases as $purchase) {
            $tenor = $purchase->date ? $purchase->client->repo->getConsecutiveMonthsInvested(Carbon::parse($purchase->date)) : 0;

            if ($tenor < 36) {
                $purchase->loyalty_rate = 0.00025;
            } elseif ($tenor >= 36 && $tenor < 72) {
                $purchase->loyalty_rate = 0.00035;
            } elseif ($tenor >= 72 && $tenor < 132) {
                $purchase->loyalty_rate = 0.00045;
            } elseif ($tenor >= 132) {
                $purchase->loyalty_rate = 0.0005;
            }

            $purchase->save();
        }
    }
}
