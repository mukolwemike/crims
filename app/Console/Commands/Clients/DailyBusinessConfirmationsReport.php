<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\ClientBusinessConfirmation;
use App\Cytonn\Models\RealEstateBusinessConfirmation;
use App\Cytonn\Models\SharesBusinessConfirmation;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\Client\DailyBusinessConfirmationsReportMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\DailyBusinessConfirmationsReportExcelGenerator;
use Illuminate\Console\Command;

class DailyBusinessConfirmationsReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cytonn:daily-business-confirmations-report {start_date?} {end_date?} {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily business confirmations report.';

    /**
     * Create a new command instance.
     * DailyBusinessConfirmationsReport constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = ($this->argument('start_date'))
            ? Carbon::parse($this->argument('start_date'))
            : Carbon::parse()->subDay();

        $end = ($this->argument('end_date'))
            ? Carbon::parse($this->argument('end_date'))
            : Carbon::parse()->subDay();

        $inv_bcs =
            ClientBusinessConfirmation::where('created_at', '>=', $start->copy()->startOfDay())
                ->has('investment')
                ->where('created_at', '<=', $end->copy()->endOfDay())
                ->get();

        $re_bcs =
            RealEstateBusinessConfirmation::where('created_at', '>=', $start->copy()->startOfDay())
                ->has('payment')
                ->where('created_at', '<=', $end->copy()->endOfDay())
                ->get();

        $shares_bcs =
            SharesBusinessConfirmation::where('created_at', '>=', $start->copy()->startOfDay())
                ->has('holding')
                ->where('created_at', '<=', $end->copy()->endOfDay())
                ->get();

        if ($inv_bcs->count() or $re_bcs->count() or $shares_bcs->count()) {
            $file_name =
                'Daily Business Confirmations Report - ' . DatePresenter::formatDate($start->copy()->toDateString());

            (new DailyBusinessConfirmationsReportExcelGenerator())
                ->excel($file_name, $inv_bcs, $re_bcs, $shares_bcs, $start->copy())->store('xlsx');
            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            $user = User::find($this->option('user_id'));

            (new DailyBusinessConfirmationsReportMailer())->sendEmail($file_path, $start, $user);

            \File::delete($file_path);

            $this->info('Daily business confirmations report has been sent!');
            return true;
        }

        $this->info('No business confirmations were found!');

        return false;
    }
}
