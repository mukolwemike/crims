<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\User;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\ContactPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CleanupClients extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:cleanup_clients {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of clients that need to be cleaned up';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = 61;

        $clients = $this->getClients();

        $fileName = 'Duplicate Clients';

        ExcelWork::generateAndStoreSingleSheet($clients, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the client duplicates report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the clients
     */
    public function getClients()
    {
        return $this->getDuplicates();
    }

    /*
     * Get the duplicates
     */
    public function getDuplicates()
    {
        $columnsArray = [
            ['phone'],
            ['pin_no'],
            ['id_or_passport'],
        ];

        $clientArray = array();

        foreach ($columnsArray as $columns) {
            $clients = Client::where(
                function ($q) use ($columns) {
                    foreach ($columns as $column) {
                        $q->whereNotIn($column, ['-', '', 'N/A'])->whereNotNull($column);
                    }
                }
            )->duplicates($columns)->get()->toArray();

            foreach ($clients as $key => $client) {
                $clientRecords = Client::where(
                    function ($q) use ($columns, $client) {
                        foreach ($columns as $column) {
                            $q->where($column, $client[$column]);
                        }
                    }
                )->get();

                foreach ($clientRecords as $recordKey => $record) {
                    $clientArray[$record->id] = $this->getReportClientData($record);
                }
            }
        }

        foreach ($clientArray as $client) {
            if ($client['Valid Duplicate'] == 'Yes') {
                $clientArray = $this->checkParentCode($client['Client Code'], $clientArray);
            }

            $clientArray = $this->checkCoopClients($client, $clientArray);
        }

        return $clientArray;
    }

    public function getReportClientData($client)
    {
        return [
            'ID' => $client->id,
            'Contact_ID' => $client->contact_id,
            'Client Code' => $client->client_code,
            'Joint Client' => BooleanPresenter::presentYesNo($client->joint),
            'Name' => ClientPresenter::presentFullNames($client->id),
            'Phone' => $client->phone,
            'Pin' => $client->pin_no,
            'Passport' => $client->id_or_passport,
            'Valid Duplicate' => $this->checkValidDuplicate($client->client_code),
            'Duplicate Phone Exists' => $this->checkRecord('phone', $client->phone, $client->id),
            'Duplicate KRA Pin Exists' => $this->checkRecord('pin_no', $client->pin_no, $client->id),
            'Duplicate ID_or_Passport Exists' => $this->checkRecord(
                'id_or_passport',
                $client->id_or_passport,
                $client->id
            )
        ];
    }

    public function checkValidDuplicate($code)
    {
        preg_match('/^[0-9]{4}[A-Z]{1}$/', $code, $test);

        if (count($test) > 0) {
            return "Yes";
        } else {
            return "No";
        }
    }

    /*
     * Check parent client code
     */
    public function checkParentCode($code, $clientArray)
    {
        $code = substr($code, 0, 4);

        $client = Client::where('client_code', $code)->first();

        if ($client) {
            $clientArray[$client->id]['Valid Duplicate'] = "Yes";
        }

        return $clientArray;
    }

    public function checkCoopClients($client, $clientArray)
    {
        $model = new Client();

        if ($client['Duplicate Phone Exists'] == 'Yes') {
            $model = $model->where('phone', $client['Phone']);
        }

        if ($client['Duplicate KRA Pin Exists'] == 'Yes') {
            $model = $model->where('pin_no', $client['Pin']);
        }

        if ($client['Duplicate ID_or_Passport Exists'] == 'Yes') {
            $model = $model->where('id_or_passport', $client['Passport']);
        }

        $clients = $model->get();

        foreach ($clients as $oldClient) {
            $code = substr(strtolower($oldClient->client_code), 0, 4);

            if ($code == 'coop') {
                $clientArray[$oldClient->id]['Valid Duplicate'] = 'Coop Duplicate';
            }
        }

        return $clientArray;
    }

    public function checkRecord($column, $record, $id)
    {
        $exists = Client::where($column, $record)->whereNotNull($column)
            ->whereNotIn($column, ['-', '', 'N/A'])->where('id', '!=', $id)
            ->exists();

        return $exists ? 'Yes' : 'No';
    }

    public function getIndividualColumnDuplicates()
    {
        $columnsArray = [
            ['phone'],
            ['pin_no'],
            ['id_or_passport'],
            ['phone', 'pin_no'],
            ['phone', 'id_or_passport'],
            ['pin_no', 'id_or_passport'],
            ['phone', 'pin_no', 'id_or_passport']
        ];

        $clientArray = array();

        foreach ($columnsArray as $columns) {
            $stringKey = $this->columnToString($columns);

            $clients = Client::where(
                function ($q) use ($columns) {
                    foreach ($columns as $column) {
                        $q->whereNotIn($column, ['-', ''])->whereNotNull($column);
                    }
                }
            )->duplicates($columns)->take(5)->get()->toArray();

            foreach ($clients as $key => $client) {
                $clientString = $this->columnToString($client);

                $clientRecords = Client::where(
                    function ($q) use ($columns, $client) {
                        foreach ($columns as $column) {
                            $q->where($column, $client[$column]);
                        }
                    }
                )->get();

                foreach ($clientRecords as $recordKey => $record) {
                    $clientArray[$stringKey][$clientString][$record->id] =
                        $this->getClientData($record, $clientString, $recordKey);
                }
            }
        }

        return $clientArray;
    }

    public function columnToString($columns)
    {
        $string = '';

        $index = 0;

        foreach ($columns as $column) {
            if ($index != 0) {
                $string .= '_';
            }

            $string .= $column;

            $index ++;
        }

        return $string;
    }

    public function getClientData($client, $key, $recordKey)
    {
        return [
            'Key' => ($recordKey == 0) ? $key : '',
            'ID' => $client->id,
            'Contact_ID' => $client->contact_id,
            'Code' => $client->client_code,
            'Name' => ClientPresenter::presentFullNames($client->id),
            'Phone' => $client->phone,
            'Pin' => $client->pin_no,
            'Passport' => $client->id_or_passport
        ];
    }

    public function getContactData($contact, $key, $recordKey)
    {
        return [
            'Key' => ($recordKey == 0) ? $key : '',
            'ID' => $contact->id,
            'Name' => ContactPresenter::fullName($contact->id),
            'Phone' => $contact->phone,
            'Email' => $contact->email,
            'Registration' => $contact->registration_number
        ];
    }
}
