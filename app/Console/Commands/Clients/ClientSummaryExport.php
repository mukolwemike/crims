<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientSummaryExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:export-client-summary {product_id} {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export client summary to excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $productId = $this->argument('product_id');

        $userId = $this->argument('user_id');

        $product = Product::findOrFail($productId);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = $product->name.' Worksheet '. $date;

        $dataArray = $this->generateReport($product);

        ExcelWork::generateAndStoreMultiSheet($dataArray, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($product->name . ' Client Summary')
            ->text('Please find attached the ' . $product->name . ' Client Summary')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Generate a report for the same
     */
    public function generateReport(Product $product)
    {
        $dataArray = array();

        $dataArray['Client Summary'] = $this->getClientSummary($product);

        return $dataArray;
    }

    /*
     * Get the client summary
     */
    public function getClientSummary(Product $product)
    {
        return Client::whereHas(
            'investments',
            function ($q) use ($product) {
                $q->where('product_id', $product->id)->active();
            }
        )->get()->map(
            function ($client) use ($product) {
                return [
                    'ClientCode' => $client->client_code,
                    'Name' => ClientPresenter::presentFullNames($client->id),
                    'TodayInvestedAmount' => AmountPresenter::currency(
                        $client->repo->getTodayInvestedAmountForProduct($product)
                    ),
                    'CustodyFees' => AmountPresenter::currency($client->repo->getCustodyFeesForProduct($product))
                    ];
            }
        );
    }
}
