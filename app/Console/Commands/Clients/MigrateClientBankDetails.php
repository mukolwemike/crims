<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use Illuminate\Console\Command;

class MigrateClientBankDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:migrate-bank-details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate client bank details to client_bank_accounts table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Initializing...');

        $this->info('Fetching all clients...');

        $clients = Client::all();

        $this->info('Migrating clients\' bank details...');

        $clients->each(
            function ($client) {
                if ($this->clientHasBankAccount($client) and !$this->duplicateExists($client)) {
                    ClientBankAccount::create(
                        [
                        'client_id'         =>  $client->id,
                        'account_name'      =>  $client->investor_account_name,
                        'account_number'    =>  $client->investor_account_number,
                        'bank'              =>  $client->investor_bank,
                        'branch'            =>  $client->investor_bank_branch,
                        'clearing_code'     =>  $client->investor_clearing_code,
                        'swift_code'        =>  $client->investor_swift_code,
                        'default'           =>  true,
                        ]
                    );
                }
            }
        );

        $this->info('Completed');

        return true;
    }

    /**
     * @param Client $client
     * @return mixed
     */
    private function duplicateExists(Client $client)
    {
        return ClientBankAccount::where(
            [
            'client_id'         =>  $client->id,
            'account_name'      =>  $client->investor_account_name,
            'account_number'    =>  $client->investor_account_number,
            'bank'              =>  $client->investor_bank,
            'branch'            =>  $client->investor_bank_branch,
            'clearing_code'     =>  $client->investor_clearing_code,
            'swift_code'        =>  $client->investor_swift_code,
            'default'           =>  true,
            ]
        )->exists();
    }

    private function clientHasBankAccount(Client $client)
    {
        return $client->whereNotNull('investor_account_name')
            ->whereNotNull('investor_account_number')
            ->whereNotNull('investor_bank')
            ->whereNotNull('investor_bank_branch')
            ->first()
            ->count();
    }
}
