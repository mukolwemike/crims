<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Data\Iprs;
use App\Cytonn\Models\Client\Kyc\IprsRequest;
use Illuminate\Console\Command;

class IprsValidationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iprs:validate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Iprs
     */
    protected $iprs;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->iprs = new Iprs();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        logRollbar("Checking for pending IPRS validations");

        $this->iprs->testConnection();

        $requests = IprsRequest::whereNull('status')->get();

        $requests->each(function ($request) {
            try {
                $this->validate($request);
            } catch (\Exception $e) {
                reportException($e);
                $this->info("Error occurred:: ".$e->getMessage());
            }
        });

        $this->info($requests->count()." IPRS requests have been made");
        return;
    }

    public function validate(IprsRequest $request)
    {
        $status = $this->iprs->makeRequest($request);

        if (!is_null($status)) {
            $tries = $request->tries + 1;

            $st = $status === true || $status === false ? $status : null;

            if ($tries >= 10 && ($status === false ||  is_null($status))) {
                $st = false;
            }

            $request->update([
                'status' => $st,
                'tries' => $tries
            ]);
        }
    }
}
