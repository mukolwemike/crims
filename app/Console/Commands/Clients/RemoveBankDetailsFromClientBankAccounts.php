<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Bank;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use Illuminate\Console\Command;

class RemoveBankDetailsFromClientBankAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:separate-bank-details-from-client-bank-accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Separate Bank and branches from Client Bank Accounts';

    /**
     * Create a new command instance.
     * RemoveBankDetailsFromClientBankAccounts constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $banks = $this->getAccounts();
        $start = $banks->count();

        $banks->each(
            function ($account) {
                $this->match($account);
            }
        );

        $end = $this->getAccounts()->count();

        $processed = $start - $end;

        $this->info("$processed records processed, $end records not processed");
    }

    private function getAccounts()
    {
        return ClientBankAccount::whereNull('branch_id')
            ->orderBy('bank')
            ->orderBy('branch')
            ->whereNotIn('branch', ['-', '.'])
            ->whereNotIn('bank', ['-', '.'])
            ->get();
    }

    private function match(ClientBankAccount $account)
    {
        $branch_name = $account->branch;
        $bank_name = $account->bank;

        $banks = ClientBank::all()->filter(
            function (ClientBank $bank) use ($bank_name) {
                $aliases = explode(',', $bank->alias);

                return $this->eq($bank->name, $bank_name) || $this->contains($bank_name, $aliases);
            }
        );

        $branches = ClientBankBranch::whereHas(
            'bank',
            function ($bank) use ($banks) {
                $bank->whereIn('id', $banks->pluck('id')->all());
            }
        )->get();

        $branch = $branches->filter(
            function (ClientBankBranch $branch) use ($branch_name) {
                $aliases = explode(',', $branch->alias);


                return $this->eq($branch->name, $branch_name)
                || $this->contains($branch_name, $aliases)
                || $this->eq(str_replace(
                    " branch",
                    '',
                    strtolower($branch->name)
                ), str_replace(" branch", '', strtolower($branch_name)));
            }
        )->first();

        if ($branch) {
            $account->update(['branch_id' => $branch->id]);
        } else {
            $account->update(['branch_id' => null]);
            $this->info("Failed to match branch: $branch_name => $bank_name");
        }
    }

    private function eq($str1, $str2)
    {
        return $this->clean($str1) == $this->clean($str2);
    }

    private function contains($str, $array)
    {
        foreach ($array as $item) {
            $out = $this->eq($item, $str);

            if ($out) {
                return true;
            }
        }

        return false;
    }

    private function clean($str)
    {
        return strtolower(preg_replace('/[^A-Za-z0-9]/', "", $str));
    }
}
