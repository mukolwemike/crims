<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 12/09/2018
 * Time: 15:40
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class InterClientCustodialTransfer extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:inter_client_custodial_transfer {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of all the inter client custodial transfer with regards to a client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $report = $this->getReport($start, $end);

        $fileName = 'Inter Client Custodial Transfer';

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text("Please find attached the Inter Client Custodial transfers summary for the period from " .
                $start->toDateString() . ' to ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }


    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    private function getReport(Carbon $start, Carbon $end)
    {
        $clientPayments = ClientPayment::ofType('TI')->where('date', '>=', $start)->where('date', '<=', $end)
            ->whereNotNull('parent_id')->get();

        $dataArray = array();

        foreach ($clientPayments as $destinationPayment) {
            $destinationClient = $destinationPayment->client;

            $destinationTransaction = $destinationPayment->custodialTransaction;

            $sourcePayment = $destinationPayment->parent;

            $sourceClient = $sourcePayment ? $sourcePayment->client : null;

            $sourceTransaction = $sourcePayment->custodialTransaction;

            $dataArray[] = [
                'Date' => $destinationPayment->date,
                'Source Payment ID' => $sourcePayment ? $sourcePayment->id : '',
                'Source Client Code' => $sourceClient ? $sourceClient->client_code : '',
                'Source Client Name' => $sourceClient ? ClientPresenter::presentFullNames($sourceClient->id) : '',
                'Source Amount' => $sourcePayment ? $sourcePayment->amount : '',
                'Source Payment For' => $sourcePayment ? $sourcePayment->present()->paymentFor : '',
                'Source Custodial Account' => $sourceTransaction ? $sourceTransaction->custodialAccount->account_name
                    : '',
                'Source Currency' => $sourceTransaction ? $sourceTransaction->custodialAccount->currency->code : '',
                'Destination Payment ID' => $destinationPayment ? $destinationPayment->id : '',
                'Destination Client Code' => $destinationClient ? $destinationClient->client_code : '',
                'Destination Client Name' => $destinationClient ?
                    ClientPresenter::presentFullNames($destinationClient->id) : '',
                'Destination Amount' => $destinationPayment ? $destinationPayment->amount : '',
                'Destination Payment For' => $destinationPayment ? $destinationPayment->present()->paymentFor : '',
                'Destination Custodial Account' => $destinationTransaction ?
                    $destinationTransaction->custodialAccount->account_name : '',
                'Destination Currency' => $destinationTransaction ?
                    $destinationTransaction->custodialAccount->currency->code : '',
            ];
        }

        return $dataArray;
    }
}
