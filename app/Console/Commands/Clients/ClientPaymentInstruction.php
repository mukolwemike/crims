<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 8/13/18
 * Time: 10:05 AM
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientPaymentInstruction extends Command
{
    protected $signature = 'clients:client_payment_instruction {date}';

    protected $description = 'Export clients payment instructions';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $date = Carbon::parse($this->argument('date'));

        $instruction = $this->paymentInstructions($date);

        $fileName = 'Client Payment Instructions for '. $date->toFormattedDateString();

        ExcelWork::generateAndStoreSingleSheet($instruction, $fileName);

        Mailer::compose()
            ->to(['mchaka@cytonn.com'])
            ->bcc(config('system.administrators'))
            ->subject('Client Payment Instructions for '. $date->toFormattedDateString())
            ->text('Please find attached the client payment instructions for ' .$date->toFormattedDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    public function paymentInstructions($date)
    {
        $instructions = BankInstruction::whereBetween('date', [
                $date->copy()->startOfDay(),
                $date->copy()->endOfDay()
            ])->get();

        return $instructions->map(function ($instruction) {
            $client = $instruction->custodialTransaction->client;

            return [
                'customer ref' => '',
                'Name' => ClientPresenter::presentFullNames($client->id),
                'Beneficiary Account' => $instruction->clientBankAccount->account_number,
                'Amount' => AmountPresenter::currency($instruction->amount),
                'Payment Type' => $instruction->type,
                'Beneficiary bank code' => $instruction->clientBankAccount->branch->bank->swift_code,
                'Payment Details' => $instruction->description,
                'Email ID' => $client->contact->email,
                'Beneficiary bank local clearing code' => $instruction->clientBankAccount->branch->bank->clearing_code
            ];
        });
    }
}
