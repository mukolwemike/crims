<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Document;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CountryClientDataAnalysis extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'client:country_data_analysis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export an analysis of country client data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fileName = 'Contact_Data_Analysis';

        $clients = Client::whereHas('contact', function ($q) {
            $q->whereIn('country_id', [73, 81, 108, 231]);
        })->get();

        $contactArray = array();

        foreach ($clients as $client) {
            $contact = $client->contact;

            $dataArray = [
                'Id' => $client->id,
                'Name' => ClientPresenter::presentFullNames($client->id),
                'Country' => $contact->country->name,
                'Client Type' => ClientPresenter::presentClientType($client->id),
                'Email' => $this->checkValue($contact->email),
                'Phone' => $this->checkValue($contact->phone),
                'Corporate Registered Name' => $this->checkValue($contact->corporate_registered_name),
                'Corporate Trade Name' => $this->checkValue($contact->corporate_trade_name),
                'Uploaded Docs' => $this->checkComplianceDocs($client)
            ];

            $contactArray[] = $dataArray;
        }

        ExcelWork::generateAndStoreSingleSheet($contactArray, $fileName);

        Mailer::compose()
            ->to([])
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the client data summary')
            ->excel([$fileName])
            ->send();
    }

    /*
     * Check compliance docs
     */
    public function checkComplianceDocs($client)
    {
        $stringArray = array();

        $complianceDocs = ClientUploadedKyc::where('client_id', $client->id)->get();

        foreach ($complianceDocs as $doc) {
            $name = $doc->type->name;

            if (! in_array($name, $stringArray)) {
                $stringArray[] = $name;
            }
        }

        $documents = Document::whereDoesntHave('clientUploadedKyc')
            ->where('client_id', $client->id)
            ->get();

        foreach ($documents as $document) {
            $name = $document->title;

            if (! in_array($name, $stringArray)) {
                $stringArray[] = $name;
            }
        }

        return implode(', ', $stringArray);
    }

    /*
     * Check if a value is present or not
     */
    public function checkValue($value)
    {
        if ($value == '' || is_null($value)) {
            return "No";
        } else {
            return "Yes";
        }
    }
}
