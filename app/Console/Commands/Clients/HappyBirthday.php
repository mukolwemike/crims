<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\EmptyModel;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Mailers\HappyBirthdayMailer;
use Cytonn\Presenters\ContactPresenter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class HappyBirthday extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cytonn:birthdays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send birthday notifications to clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new Client();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $mailer = new HappyBirthdayMailer();

        if (Carbon::parse()->daysIntoYear() == 1) {
            die('Don\'t send birthday notifications for 1st');
        }

        $clients = Client::birthday()->get()->map(function ($client) {
            $preferredname = ContactPresenter::preferredName($client->contact_id);

            $fa = $client->getLatestFa();

            $fae = $fa ? $fa->email : null;

            return (object) ['email' => $client->contact->email, 'preferredname' => $preferredname, 'fa' => $fae];
        });

        $joints = ClientJointDetail::birthday()->get()->map(function ($j) {
            $client = $j->client;

            if($client) {
                $fa = $client->getLatestFa();

                $fae = $fa ? $fa->email : null;
            }

            return (object) ['email'=>$j->email, 'preferredname'=>$j->firstname, 'fa' => $fae ];
        });

        $all = Collection::make($clients->all())->merge($joints->all())->unique('email');

        $all->each(function ($client) use ($mailer) {
            $mailer->sendBirthdayNotification($client->email, $client->preferredname, $client->fa);
        });

        return $this->birthdaysIn14Days();
    }

    /**
     * Send list of clients with birthdays in 14 days
     */
    public function birthdaysIn14Days()
    {
        $date = Carbon::now()->addDays(14);

        $clients_with_bdays = Client::birthday($date)
            ->get()
            ->map(function ($client) {
                return (new EmptyModel())->fill([
                        'Email'=>$client->contact->email,
                        'Name'=>$client->contact->firstname . ' ' . $client->contact->lastname,
                        'Date'=>Carbon::parse($client->dob)->formatLocalized('%d %B')
                    ]);
            });

        $j_with_bdays = ClientJointDetail::birthday($date)
            ->get()
            ->map(function ($j) {
                return (new EmptyModel())->fill(['Email'=>$j->email, 'Name'=>$j->firstname . ' ' . $j->lastname,
                        'Date'=> Carbon::parse($j->dob)->formatLocalized('%d %B')
                    ]);
            });

        $all = Collection::make($clients_with_bdays->all())
            ->merge($j_with_bdays->all())
            ->unique('Email');
        
        if ($all->count() == 0) {
            return;
        }

        $fname = 'Clients With Birthday in 14 Days';
        $file = storage_path('exports/'.$fname.'.xlsx');

        //Export and store
        \Excel::create(
            $fname,
            function ($excel) use ($all) {
                $excel->sheet(
                    'Clients',
                    function ($sheet) use ($all) {
                        $sheet->fromModel($all);
                    }
                );
            }
        )->store('xlsx');
        //        Excel::fromModel($fname, $all)->store('xlsx');

        //Send Email to Diaspora
        $mailer = new HappyBirthdayMailer();
        $mailer->birthdaysIn14Days($all, $file);

        //Remove file after sending mail
        File::delete($file);
    }
}
