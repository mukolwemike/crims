<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ActiveClientsExports extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:active_clients_exports {date} {type} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a list of clients active as a specified date depending on type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $date = Carbon::parse($this->argument('date'));

        $type = $this->argument('type');

        $clients = $this->getActiveClients($date, $type);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'Active Clients - '. $date;

        ExcelWork::generateAndStoreSingleSheet($clients, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the active clients report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getActiveClients(Carbon $date, $type)
    {
        $clients = $this->filterClients($date, $type);

        return $clients->map(function ($client) {
            return [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Email' => $client->contact->email,
                'Phone' =>$client->contact->phone,
                'Franchise' => BooleanPresenter::presentYesNo($client->franchise),
                'Joint' => BooleanPresenter::presentYesNo($client->joint),
                'Client Type' => $client->clientType->name,
                'Date Joined' => $client->contact->created_at
            ];
        });
    }

    private function filterClients(Carbon $date, $type)
    {
        $scope = $this->getDataScope($type);

        return Client::activeOnDate(
            $date,
            $scope['investment'],
            $scope['realestate'],
            false,
            $scope['unit_funds']
        )->get();
    }

    public function getDataScope($type)
    {
        $scopeArray = [
            'investment' => false,
            'realestate' => false,
            'unit_funds' => false
        ];

        if ($type == 'combined') {
            $scopeArray = [
                'investment' => true,
                'realestate' => true,
                'unit_funds' => true
            ];
        } elseif ($type == 'investment') {
            $scopeArray['investment'] = true;
        } elseif ($type == 'realestate') {
            $scopeArray['realestate'] = true;
        } elseif ($type == 'unit_funds') {
            $scopeArray['unit_funds'] = true;
        }

        return $scopeArray;
    }
}
