<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/3/18
 * Time: 4:32 PM
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Clients\ClientComplianceChecklist;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\Document;
use Cytonn\Core\DataStructures\Carbon;
use Illuminate\Console\Command;

class UpdateClientDocuments extends Command
{
    protected $signature = 'client:update-clients-uploaded-kyc';

    protected $description = 'Update clients uploaded kycs';

    public function fire()
    {
        $date = Carbon::today();

        return Client::whereBetween('created_at', [ $date->copy()->subMonth(), $date->copy() ])
            ->whereDoesntHave('uploadedKyc')
            ->get()
            ->each(function ($client) {

                $application = ClientInvestmentApplication::where('client_id', $client->id)->first();

                $form = $application ? $application->form : null;

                if (!$form) {
                    return;
                }

                $documents = $application->form->clientRepo->getDocumentDetails('kyc');

                $documents->each(function ($doc) use ($application) {

                    $document = Document::findOrFail($doc['document_id']);

                    $kyc = new ClientUploadedKyc();

                    $kyc->application_id = $application->form->id;

                    $kyc->client_id = $application->client_id;

                    $kyc->document_id = $document->id;

                    $kyc->filename = $document->filename;

                    $kyc->kyc_id = ClientComplianceChecklist::where('slug', $doc['doc_type'])->first()->id;

                    $kyc->save();
                });
            });
    }
}
