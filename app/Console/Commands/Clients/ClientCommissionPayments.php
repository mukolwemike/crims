<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientCommissionPayments extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:client_commission_payments {client_id} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of all the commission payments with regards to a client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $client = Client::find($this->argument('client_id'));

        if (is_null($client)) {
            return;
        }

        $userId = $this->argument('user_id');

        $report = $this->getReport($client);

        $fileName = 'Client Commission Payments';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the client commission payments report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getReport(Client $client)
    {
        $dataArray = array();

        $dataArray['Commission Schedules'] = $this->getCommissionSchedules($client);

        $dataArray['Commission Clawbacks'] = $this->getCommissionClawbacks($client);

        $dataArray['Real Estate Commission'] = $this->getRealEstateCommission($client);

        return $dataArray;
    }

    private function getCommissionSchedules(Client $client)
    {
        $commissionSchedules = CommissionPaymentSchedule::whereHas('commission', function ($q) use ($client) {
            $q->fromClient($client);
        })->get();

        return $commissionSchedules->map(function ($commissionSchedule) {
            $investment = $commissionSchedule->commission->investment;

            $recipient = $commissionSchedule->commission->recipient;

            $client = $investment->client;

            return [
                'Date' => Carbon::parse($commissionSchedule->date)->toDateString(),
                'Amount' => $commissionSchedule->amount,
                'FA' => $recipient->name,
                'Description' => $commissionSchedule->description,
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Principal' => $investment->amount,
                'Invested Date' => $investment->invested_date,
                'Maturity Date' => $investment->maturity_date
            ];
        });
    }

    private function getCommissionClawbacks(Client $client)
    {
        $commissionClawbacks = CommissionClawback::whereHas('commission', function ($q) use ($client) {
            $q->fromClient($client);
        })->get();

        return $commissionClawbacks->map(function ($commissionClawback) {
            $investment = $commissionClawback->commission->investment;

            $recipient = $commissionClawback->commission->recipient;

            $client = $investment->client;

            return [
                'Date' => Carbon::parse($commissionClawback->date)->toDateString(),
                'Amount' => $commissionClawback->amount,
                'FA' => $recipient->name,
                'Narration' => $commissionClawback->narration,
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Principal' => $investment->amount,
                'Invested Date' => $investment->invested_date,
                'Maturity Date' => $investment->maturity_date
            ];
        });
    }

    private function getRealEstateCommission(Client $client)
    {
        $commissionSchedules = RealEstateCommissionPaymentSchedule::whereHas('commission', function ($q) use ($client) {
            $q->forClient($client);
        })->get();

        return $commissionSchedules->map(function ($commissionSchedule) {
            $holding = $commissionSchedule->commission->holding;

            $recipient = $commissionSchedule->commission->recipient;

            $client = $holding->client;

            return [
                'Date' => Carbon::parse($commissionSchedule->date)->toDateString(),
                'Amount' => $commissionSchedule->amount,
                'FA' => $recipient->name,
                'Type' => $commissionSchedule->type->name,
                'Description' => $commissionSchedule->description,
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Unit Price' => $holding->price(),
                'Unit Number' => $holding->unit->number,
                'Project' => $holding->unit->project->name
            ];
        });
    }
}
