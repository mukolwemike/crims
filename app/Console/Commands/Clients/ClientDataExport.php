<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientDataExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:client-data-summary {product_id} {user_id} {end_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export client summary to excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $productId = $this->argument('product_id');

        $userId = $this->argument('user_id');

        $product = Product::findOrFail($productId);

        $endDate = $this->argument('end_date');

        $date = $endDate ? Carbon::parse($endDate) : Carbon::today();

        $dateString = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::parse($date));

        $fileName = $product->name.' Worksheet '. $dateString;

        $file =  (new \Cytonn\Reporting\ClientSummaryExcelGenerator())->excel($product, $fileName, $date);

        $file->store('xlsx');

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        $user = User::find($userId);

        $email = ($user) ? [$user->email] : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($product->name . ' Client Summary')
            ->text('Please find attached the ' . $product->name . ' Client Summary')
            ->excel([$product->name.' Worksheet '. $dateString])
            ->send();

        return \File::delete($path);
    }
}
