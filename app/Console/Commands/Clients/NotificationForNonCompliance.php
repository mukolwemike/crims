<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/9/18
 * Time: 2:39 PM
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Mailers\Client\ComplianceReminderMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\SMS\Unitization\NonComplianceSMSNotify;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotificationForNonCompliance extends Command
{
    protected $signature = 'client:client-compliance-notification {start?}{end?}';

    protected $description = 'Notify non compliant clients';

    public function fire()
    {
        return;

        $start = ($this->argument('start')) ? Carbon::parse($this->argument('start')) : Carbon::now()->subWeek(1);

        $end = ($this->argument('end')) ? Carbon::parse($this->argument('end')) : Carbon::now();

        $this->nonCompliantClients($start, $end);

        return;
    }

    public function nonCompliantClients($start, $end)
    {
        return Client::active()
            ->whereHas('unitFundPurchases')
//            ->whereBetween('created_at', [ $start->copy()->startOfDay(), $end->copy()->endOfDay() ])
            ->where(function ($client) {
                $client->where('compliant', 0)->orWhereNull('compliant');
            })
            ->get()
            ->reject(function ($client) {
                return $client->repo->compliant();
            })
            ->each(function ($client) {

                $purchase = $client->unitFundPurchases()->first();

                (new ComplianceReminderMailer())->sendNotification($client, $purchase->unitFund);

                (new NonComplianceSMSNotify())->notify($client, $purchase->unitFund);

                return;
            });
    }
}
