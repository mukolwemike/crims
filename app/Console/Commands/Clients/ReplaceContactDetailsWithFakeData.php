<?php

namespace App\Console\Commands\Clients;

use Faker\Factory;
use Illuminate\Console\Command;

class ReplaceContactDetailsWithFakeData extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'contacts:fake-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Replace contacts\' emails and names with fake data.';

    /**
     * Create a new command instance.
     * ReplaceContactDetailsWithFakeData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (\App::environment() != 'local') {
            dd('End here');
            return;
        }

        $this->info('Update clients');

        //        \Client::all()
        //            ->each(function($client)
        //            {
        //                $contact = $client->contact;
        //                $faker = Factory::create();
        //
        //                $contact->email = $faker->email;
        //                $contact->phone = $faker->phoneNumber;
        //                $contact->firstname = $faker->firstName;
        //                $contact->lastname = $faker->lastName;
        //                $contact->middlename = $faker->lastName;
        //                $contact->corporate_registered_name = $faker->company;
        //                $contact->corporate_trade_name = $faker->company;
        //                $contact->save();
        //
        //                $client->phone = $faker->phoneNumber;
        //                $client->save();
        //            });
        //
        //        $this->info('Update portfolio');
        //
        //        \PortfolioInvestor::all()
        //            ->each(function (\PortfolioInvestor $investor)
        //            {
        //                $faker = Factory::create();
        //
        //                $investor->update(['name'=>$faker->company]);
        //            });

        //        $this->info('Update FAs');
        //        \CommissionRecepient::all()
        //            ->each(function ($rec)
        //            {
        //                $faker = Factory::create();
        //
        //                $rec->update(['name'=>$faker->name, 'email'=>$faker->companyEmail]);
        //            });
    }
}
