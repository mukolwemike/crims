<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/4/18
 * Time: 10:19 AM
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use Illuminate\Console\Command;

class CleanJointClients extends Command
{
    protected $signature = 'clients:cleanup_joint_clients';

    protected $description = 'Update clients saved as joint but with no joint holder records';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        return Client::where('joint', 1)
            ->whereDoesntHave('jointHolders')
            ->each(function (Client $client) {
                $client->joint = null;
                $client->save();
            });
    }
}
