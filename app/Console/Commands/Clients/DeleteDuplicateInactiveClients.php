<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class DeleteDuplicateInactiveClients extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:delete';


    const KEY = 'deleted_duplicate_clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        \Excel::load(
            storage_path('clients_to_be_deleted.xlsx'),
            function ($reader) {
                $results = $reader->all();

                $failed = [];

                foreach ($results as $client) {
                    $id = $client->id;

                    try {
                        $c = Client::findOrFail($id);
                    } catch (\Exception $e) {
                        if (!in_array($id, $this->getDeleted())) {
                            $this->info("Could not find $id");
                        }
                        continue;
                    }

                    try {
                        $this->delete($c);
                    } catch (\Exception $e) {
                        $this->info("Could not delete $c->id");
                        array_push($failed, $c->id);
                    }

                    $this->info(json_encode($failed));
                }
            }
        );
    }

    public function delete(Client $client)
    {
        if ($client->investments->count()) {
            throw new Exception('Client '.$client->id.' has an investment');
        }

        if ($client->unitHoldings->count()) {
            throw new Exception('Client '.$client->id.' has RE units');
        }

        if ($client->shareHolders->count()) {
            throw new Exception('Client '.$client->id.' is linked to shareholders');
        }

        $client->delete();

        $this->saveDeleted($client->id);

        $this->info("Deleted ".$client->id);
    }

    private function saveDeleted($id)
    {
        $deleted = $this->getDeleted();

        array_push($deleted, $id);

        Cache::forever(static::KEY, $deleted);
    }


    private function getDeleted()
    {
        if (Cache::has(static::KEY)) {
            return Cache::get(static::KEY);
        }

        return [];
    }
}
