<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 7/22/18
 * Time: 12:55 PM
 */

namespace App\Console\Commands\Clients;

use Cytonn\FCM\FcmManager;
use Illuminate\Console\Command;

class MoveDeviceTokensToClientUserDeviceTokens extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:moveDeviceTokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move device fcm tokens from client users table to client user device tokens table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fcmManager = new FcmManager();

        $fcmManager->moveClientDeviceTokens();

        $this->info('Done moving tokens');
    }
}
