<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Hr\HrManager;
use Illuminate\Console\Command;

class UpdateStaffClients extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:update_staff_clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the client records with staff data from hr';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $manager = new HrManager();

        $manager->getStaffClients();
    }
}
