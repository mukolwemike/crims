<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Symfony\Component\DependencyInjection\Tests\Compiler\C;

class ClientInvestmentTenorReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:client_investment_tenor {date} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of clients that have a tenor exceeding a given value';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $date = Carbon::parse($this->argument('date'));

//        $userId = 61;

//        $date = Carbon::parse('2017-12-31');

        $fundmanager = FundManager::find(1);

        $clients = $this->getClients($date, $fundmanager);

        $fileName = 'Investments Client Tenor';

        ExcelWork::generateAndStoreSingleSheet($clients, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the investments client tenor report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the clients for the report
     */
    public function getClients(Carbon $date, FundManager $fundmanager)
    {
        $reportArray = array();

        $investments = ClientInvestment::activeOnDate($date)->fundManager($fundmanager)->get();

        foreach ($investments as $investment) {
            $client = $investment->client;

            $tenor = $this->checkTenor($investment);

            if ($tenor > 380) {
                $reportArray[] = [
                    'Client Code' => $client->client_code,
                    'Client Name' => ClientPresenter::presentFullNames($client->id),
                    'Email' => $client->contact->email,
                    'Phone' => $client->contact->phone,
                    'Principal' => AmountPresenter::currency($investment->amount),
                    'Invested Date' => $investment->invested_date,
                    'Maturity Date' => $investment->maturity_date,
                    'Tenor In Days' => $tenor,
                    'Interest Rate' => $investment->interest_rate,
                    'Product' => $investment->product->name,
                    'Type' => ucfirst($investment->investmentType->name),
                    'Withdrawal Date' => $investment->withdrawal_date,
                ];
            }
        }

        return $reportArray;
    }

    /*
     * Check the tenor of the investment if it exceeds the limit
     */
    public function checkTenor(ClientInvestment $investment)
    {
        $investedDate = Carbon::parse($investment->invested_date);

        $maturityDate = Carbon::parse($investment->maturity_date);

        return $maturityDate->diffInDays($investedDate);
    }

    /*
     * Get the number of active investments for the client
     */
    public function getActiveInvestments($client, $date, $fundmanager)
    {
        return $client->investments()->activeOnDate($date)->fundManager($fundmanager)->count();
    }
}
