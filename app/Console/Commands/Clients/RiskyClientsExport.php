<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 07/11/2018
 * Time: 21:50
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client\RiskyClient;
use App\Cytonn\Models\User;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\CountryPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class RiskyClientsExport extends Command
{
    protected $signature = 'crims:risky_clients_export {user_id}';

    protected $description = 'Command will generate Risky Clients Excel and also email user...';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function handle()
    {
        $user = User::findOrFail($this->argument('user_id'));

        $riskyClients = RiskyClient::all()->map(function ($riskyClient) {
            return [
                'Client Type' => ($riskyClient->type == 1) ? 'Individual' : 'Corporate',
                'Name' => ($riskyClient->type == 1)
                    ? $riskyClient->firstname . ' ' . $riskyClient->lastname
                    : $riskyClient->organization,
                'Email' => $riskyClient->email,
                'Country' => CountryPresenter::present($riskyClient->country_id),
                'Status' => \Cytonn\Presenters\ClientPresenter::presentRiskyStatus($riskyClient->risky_status_id),
                'Flagged By' => UserPresenter::presentFullNames($riskyClient->flagged_by),
                'Affiliations' => $riskyClient->affiliations,
                'Risk Source' => $riskyClient->risk_source,
                'Risk Reason' => $riskyClient->reason,
                'Date Flagged' => \Cytonn\Presenters\DatePresenter::formatDate($riskyClient->date_flagged),
            ];
        });

        $this->sendMail($user, $riskyClients);
    }

    public function sendMail(User $user, $riskyClients)
    {
        ExcelWork::generateAndExportSingleSheet($riskyClients, 'RiskyClientSummary');

        Mailer::compose()
            ->to($user->email)
            ->bcc(config())
            ->subject('IFA Client Investments Value')
            ->text('Please find attached the Risky Clients summary')
            ->excel(['RiskyClientSummary'])
            ->send();

        $path = storage_path() . '/exports/' . 'RiskyClientSummary' . '.xlsx';

        return \File::delete($path);
    }
}
