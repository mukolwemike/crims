<?php

namespace App\Console\Commands\Clients;

use Cytonn\Clients\ClientManager;
use Illuminate\Console\Command;

/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

class SendClientToCrm extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:send_to_crm {type} {record_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the given client to crm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $manager = new ClientManager();

        $manager->sendDataToCrm($this->argument('type'), $this->argument('record_id'));


//        $manager->testCrmApi($this->argument('type'), $this->argument('record_id'));
    }
}
