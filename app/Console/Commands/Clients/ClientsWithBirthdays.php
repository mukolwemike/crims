<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\ClientJointDetail;
use Carbon\Carbon;
use App\Cytonn\Models\Client;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use App\Cytonn\Models\EmptyModel;
use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use App\Cytonn\Models\ClientType;

/**
 * Date: 24/05/2016
 * Time: 12:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientsWithBirthdays extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:birthdays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export clients with birthdays';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //        $start = Carbon::today()->startOfMonth();
        //        $end = Carbon::today()->endOfMonth();

        $clients = Client::where(
            function ($q) {
                $q->has('investments', '>', 0)->orHas('unitHoldings', '>', 0);
            }
        )->get();

        $clients->each(
            function ($c) {
                $c->Name = ClientPresenter::presentFullNames($c->id);
                $c->Type = $c->clientType->name;
                if ($c->joint) {
                    $c->Type = $c->Type.' & joint';
                }
                $c->CHYS = $c->investments()->count() > 0 ? 'Yes' : 'No';
                $c->RE = $c->unitHoldings()->count() > 0 ? 'Yes' : 'No';
                $c->FA = @$c->getLatestFa()->name;
            }
        );


        $joints = ClientJointDetail::whereHas(
            'client',
            function ($client) {
                $client->has('investments')->orHas('unitHoldings');
            }
        )->get();

        $joints->each(
            function ($joint) use ($clients) {
                if (!$joint->client) {
                    return;
                }
                $joint->Name = @$joint->title->name.' '.$joint->firstname.' '.$joint->middlename.' '.$joint->lastname;
                $joint->Type = 'individual & joint';
                $joint->client_code = @$joint->client->client_code;
                $joint->CHYS = $joint->client->investments()->count() > 0 ? 'Yes' : 'No';
                $joint->RE = $joint->client->unitHoldings()->count() > 0 ? 'Yes' : 'No';
                $joint->FA = @$joint->client->getLatestFa()->name;

                $clients->push($joint);
            }
        );


        $out = $clients->map(
            function ($client) {
                $out = new EmptyModel();
                $out->Client_code = $client->client_code;
                $out->Name = $client->Name;
                $out->Type = $client->Type;
                $out->DOB = (new Carbon($client->dob))->formatLocalized('%d %B');
                $out->CHYS = $client->CHYS;
                $out->RE = $client->RE;
                $out->FA = $client->FA;

                return $out;
            }
        );

        \Excel::create(
            'Client_birthdays',
            function ($excel) use ($out) {
                $excel->sheet(
                    'clients',
                    function ($sheet) use ($out) {
                        $sheet->fromModel($out);
                    }
                );
            }
        )->store('xlsx');

        $mailer = new GeneralMailer();
        $mailer->to(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Clients with birthdays');
        $mailer->file(storage_path('exports/Client_birthdays.xlsx'));
        $mailer->sendGeneralEmail('Please find the attached client list');

        File::delete(storage_path('exports/Client_birthdays.xlsx'));

        $this->info('done!');
    }
}
