<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class UnassignedClientPayments extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clients:unassigned_client_payments {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of client payments that are overdue by two weeks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $clientPayments = $this->getClientPayments();

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'Unassigned Funds - '. $date;

        ExcelWork::generateAndStoreSingleSheet($clientPayments, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = config('system.emails.statements');
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the unassigned funds report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the client payments
     */
    public function getClientPayments()
    {
        $twoWeeks = Carbon::now()->subWeeks(2)->startOfDay();

        $clientPayments = ClientPayment::whereNull('client_id')
            ->where('date', '<=', $twoWeeks)
            ->get();

        return $clientPayments->map(function ($clientPayment) {
            $name = '';

            $trans = $clientPayment->custodialTransaction;

            if ($trans) {
                $name = $trans->received_from;
            }

            return [
                'Name' => $name,
                'Date' => Carbon::parse($clientPayment->date)->toDateString(),
                'Amount' => AmountPresenter::currency($clientPayment->amount),
                'Value' => AmountPresenter::currency($clientPayment->value),
                'Payment For' => $clientPayment->present()->paymentFor,
                'Type' => $clientPayment->type->name,
                'Description' => $clientPayment->description,
            ];
        });
    }
}
