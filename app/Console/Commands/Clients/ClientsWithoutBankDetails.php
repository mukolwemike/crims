<?php

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\EmptyModel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class ClientsWithoutBankDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clients:without-bank-details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export list of clients without banks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->softDeleteEmptyAccounts();

        $out = ClientBankAccount::whereNull('branch_id')
            ->whereNotNull('client_id')
            ->get()
            ->filter(
                function ($account) {
                    return ! is_null($account->client);
                }
            )
            ->map(
                function (ClientBankAccount $account) {
                    $client = $account->client;

                    $out = new EmptyModel();
                    $out->{'Client Code'} = $client->client_code;
                    $out->{'Name'} = ClientPresenter::presentFullNames($client->id);
                    $out->{'E-mail'} = $client->contact->email;
                    $out->{'Phone'} = $client->contact->phone;
                    $out->{'Account Name'} = $account->account_name;
                    $out->{'Account Number'} = $account->account_number;
                    $out->{'Bank'} = $account->bank;
                    $out->{'Branch'} = $account->branch;

                    return $out;
                }
            );

        Excel::create(
            'Clients Without Bank Details',
            function ($excel) use ($out) {
                $excel->sheet(
                    'Clients',
                    function ($sheet) use ($out) {
                        $sheet->fromModel($out);
                    }
                );
            }
        )->store('xlsx');

        $mailer = new GeneralMailer();
        $mailer->to(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Clients Without Bank Details');
        $mailer->file(storage_path('exports/Clients Without Bank Details.xlsx'));
        $mailer->sendGeneralEmail('Please find the attached the requested export of clients without bank details.');

        File::delete(storage_path('exports/Clients Without Bank Details.xlsx'));

        $this->info('done!');
    }

    private function softDeleteEmptyAccounts()
    {
        $params = ['-', '.', null];
        ClientBankAccount::whereNull('branch_id')
            ->get()
            ->each(
                function (ClientBankAccount $account) use ($params) {
                    if (in_array(str_replace(' ', '', $account->account_name), $params)
                        and in_array(str_replace(' ', '', $account->account_number), $params)
                        and in_array(str_replace(' ', '', $account->bank), $params)
                        and in_array(str_replace(' ', '', $account->branch), $params)
                    ) {
                        $account->delete();
                    }
                }
            );
    }
}
