<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 18/04/2018
 * Time: 12:54
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendBusinessConfirmation extends Command
{
    protected $signature = 'client:send-business-confirmation';

    protected $description = 'Send client business confirmation';

    public function __construct()
    {
        parent:: __construct();
    }

    public function fire()
    {
        $start = Carbon::today()->subDay();
        $end = Carbon::today();

        $investments = ClientInvestment::active()
                        ->investedBetweenDates($start->copy(), $end->copy())
                        ->whereDoesntHave('confirmation')
                        ->get();

        $investments->each(function ($investment) {
            $investment->repo->sendBusinessConfirmation();
        });
    }
}
