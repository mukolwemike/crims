<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 22/03/2018
 * Time: 16:14
 */

namespace App\Console\Commands\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientsComplianceExport extends Command
{
    protected $signature = 'client:client-compliance { user_id }{start?}{end?}';

    protected $description = 'Export clients compliance report to excel';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = ($this->argument('start'))? Carbon::parse($this->argument('start')) : Carbon::now();
        $end = ($this->argument('end'))? Carbon::parse($this->argument('end')) : Carbon::now();

        $this->info("Started generation");

        $dataArray = $this->generateReport($start->copy(), $end->copy());

        $this->info("Completed generation");

        $fileName = 'Client Compliance Report';

        ExcelWork::generateAndStoreMultiSheet($dataArray, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);
            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the clients compliance report for the period ' .
                $start->toDateString() . ' to ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    public function generateReport($start, $end)
    {
        $dataArray = array();

        $dataArray['Client Compliance Report'] = $this->getClients($start, $end);

        return $dataArray;
    }

    public function getClients($start, $end)
    {
        return Client::active()
            ->whereBetween('created_at', [ $start->copy()->startOfDay(), $end->copy()->endOfDay() ])
            ->get()
            ->map(function ($client) {
                $missing = $client->repo->requiredPendingKYCDocuments()->map->name;

                $missing = $missing->toArray();

                if (!$client->compliant && count($missing) == 0) {
                    $client->compliant = 1;
                    $client->save();
                }

                return [
                    'Code' => $client->client_code,
                    'Name' => ClientPresenter::presentFullNames($client->id),
                    'ID/ Passport' => $client->id_or_passport,
                    'Pin No' => $client->pin_no,
                    'Email Address' => $client->contact->email,
                    'Compliant' => ($client->compliant)? 'Compliant' : 'Non-compliant',
                    'Pending Kyc' => (! $client->compliant)
                        ? implode(", ", $missing)
                        : 'None'
                ];
            });
    }
}
