<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Crims\Realestate\Commission\RecipientCalculator as RecipientCalculator_RE;
use Crims\Shares\Commission\RecipientCalculator as RecipientCalculator_Shares;
use Crims\Unitization\Commission\RecipientCalculator as RecipientCalculator_Unitization;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class OverrideSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:fa_override_summary {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export an fa override summary for the period';

    private $currencies;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $report = $this->getReport($start, $end);

        $fileName = 'Override Summary ' . $start->toDateString() . ' To ' . $end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the override summary report between '
                . $start->toDateString() . ' and ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    public function getReport(Carbon $start, Carbon $end)
    {
        $firstBulk = BulkCommissionPayment::where('end', '>=', $start)
            ->orderBy('start')
            ->where('end', '<=', $end)->first();

        $lastBulk = BulkCommissionPayment::where('end', '>=', $start)
            ->latest('start')
            ->where('end', '<=', $end)->first();

        $this->currencies = $this->getCurrencies();

        $reportArray = array();

        $recipients = CommissionRecepient::with('type')
            ->where('id', 26)
            ->whereIn('rank_id', [3, 4, 7])->active()
            ->get();

        $reportArray["Total Override"] = $this->getBulkReport(
            Carbon::parse($firstBulk->start),
            Carbon::parse($lastBulk->end),
            $recipients
        );

        return $reportArray;
    }

    /**
     * @return array
     */
    private function getBulkReport(Carbon $start, Carbon $end, $recipients)
    {
        $recipientArray = array();

        foreach ($recipients as $recipient) {
            $recipientArrayData = [
                'Employee ID' => $recipient->employee_id,
                'Employee Number' => $recipient->employee_number,
                'Name' => $recipient->name,
                'Rank' => $recipient->rank->name,
                'Type' => $recipient->type->name,
            ];

            $recipientArrayData = array_merge($recipientArrayData, $this->getInvestmentData($recipient, $start, $end));

            $recipientArrayData = array_merge($recipientArrayData, $this->getRealEstateData($recipient, $start, $end));

            $recipientArrayData = array_merge($recipientArrayData, $this->getSharesData($recipient, $start, $end));

            $recipientArrayData = array_merge($recipientArrayData, $this->getUnitFundData($recipient, $start, $end));

            $recipientArrayData['Overrall_Earned'] = $recipientArrayData['SP_Earned'] + $recipientArrayData['RE_Earned']
                + $recipientArrayData['OTC_Earned'] + $recipientArrayData['UTF_Earned'];

            $recipientArrayData['Overrall_Override'] = $recipientArrayData['SP_Override'] + $recipientArrayData['RE_Override']
                + $recipientArrayData['OTC_Override'] + $recipientArrayData['UTF_Override'];

            $recipientArrayData['Overrall_Unit_Override'] = $recipientArrayData['SP_Unit_Override'] + $recipientArrayData['RE_Unit_Override']
                + $recipientArrayData['OTC_Unit_Override'] + $recipientArrayData['UTF_Unit_Override'];

            $recipientArrayData['Overall_Non_Self_Override'] = $recipientArrayData['SP_Non_Self_Override'] + $recipientArrayData['RE_Non_Self_Override']
                + $recipientArrayData['OTC_Non_Self_Override'] + $recipientArrayData['UTF_Non_Self_Override'];

            $recipientArrayData['Overrall_Total'] = $recipientArrayData['SP_Total'] + $recipientArrayData['RE_Total']
                + $recipientArrayData['OTC_Total'] + $recipientArrayData['UTF_Total'];

            $recipientArrayData['Overrall_Non_Self_Total'] = $recipientArrayData['SP_Non_Self_Total'] + $recipientArrayData['RE_Non_Self_Total']
                + $recipientArrayData['OTC_Non_Self_Total'] + $recipientArrayData['UTF_Non_Self_Total'];

            $recipientArray[] = $recipientArrayData;
        }

        return $recipientArray;
    }

    /**
     * @param CommissionRecepient $recipient
     * @return mixed
     */
    private function getInvestmentData(CommissionRecepient $recipient, Carbon $start, Carbon $end)
    {
        $earned = $override = $unitOverride = 0;

        foreach ($this->currencies as $currency) {
            $calculator = $recipient->calculator($currency, $start->copy(), $end->copy());

            $summary = $calculator->summary();

            $earned += convert(($summary->getCompliant() - $summary->getClawBacks()), $currency, $end);

            $override += convert($summary->overrideOnRecipient($recipient), $currency, $end);

            $unitOverride += convert($summary->override(), $currency, $end);
        }

        return [
            'SP_Earned' => (float) $earned,
            'SP_Override' => (float) $override,
            'SP_Unit_Override' => (float) $unitOverride,
            'SP_Non_Self_Override' => (float) ($unitOverride - $override),
            'SP_Total' => (float) ($earned + $override),
            'SP_Non_Self_Total' => (float) ($earned + $unitOverride - $override)
        ];
    }

    /**
     * @param CommissionRecepient $recepient
     * @return array
     */
    private function getRealEstateData(CommissionRecepient $recepient, Carbon $start, Carbon $end)
    {
        $calculator = new RecipientCalculator_RE($recepient, $start, $end);

        $summary = $calculator->summary();

        $earned = $summary->total();

        $override = $summary->overrideOnRecipient($recepient);

        $unitOverride = $summary->override();

        return [
            'RE_Earned' => (float) $earned,
            'RE_Override' => (float) $override,
            'RE_Unit_Override' => (float) $unitOverride,
            'RE_Non_Self_Override' => (float) ($unitOverride - $override),
            'RE_Total' => (float) ($earned + $override),
            'RE_Non_Self_Total' => (float) ($earned + $unitOverride - $override)
        ];
    }

    /**
     * @param CommissionRecepient $recepient
     * @return array
     */
    private function getSharesData(CommissionRecepient $recepient, Carbon $start, Carbon $end)
    {
        $calculator = new RecipientCalculator_Shares($recepient, $start, $end);

        $summary = $calculator->summary();

        $earned = $summary->total();

        $override = $summary->overrideOnRecipient($recepient);

        $unitOverride = $summary->override();

        return [
            'OTC_Earned' => (float) $earned,
            'OTC_Override' => (float) $override,
            'OTC_Unit_Override' => (float) $unitOverride,
            'OTC_Non_Self_Override' => (float) ($unitOverride - $override),
            'OTC_Total' => (float) ($earned + $override),
            'OTC_Non_Self_Total' => (float) ($earned + $unitOverride - $override)
        ];
    }

    /**
     * @param CommissionRecepient $recepient
     * @return array
     */
    private function getUnitFundData(CommissionRecepient $recepient, Carbon $start, Carbon $end)
    {
        $calculator = new RecipientCalculator_Unitization($recepient, $start, $end);

        $summary = $calculator->summary();

        $earned = $summary->total();

        $override = $summary->overrideOnRecipient($recepient);

        $unitOverride = $summary->override();

        return [
            'UTF_Earned' => (float) $earned,
            'UTF_Override' => (float) $override,
            'UTF_Unit_Override' => (float) $unitOverride,
            'UTF_Non_Self_Override' => (float) ($unitOverride - $override),
            'UTF_Total' => (float) ($earned + $override),
            'UTF_Non_Self_Total' => (float) ($earned + $unitOverride - $override)
        ];
    }

    /**
     * @return Currency[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getCurrencies()
    {
        return Currency::whereHas('products', function ($q) {
            $q->active()->has('investments');
        })->get();
    }
}
