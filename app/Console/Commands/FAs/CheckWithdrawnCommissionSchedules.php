<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CheckWithdrawnCommissionSchedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:check_withdrawn_schedules {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a report of the commission schedules for withdrawn investments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::transaction(function () {
            $userId = $this->argument('user_id');

            $commissions = $this->getReport();

            $fileName = 'Withdrawn Commission Schedules';

            ExcelWork::generateAndStoreMultiSheet($commissions, $fileName);

            if ($userId) {
                $user = User::findOrFail($userId);

                $email = $user ? $user->email : [];
            } else {
                $email = [];
            }

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text("Please find attached the commission schedules for withdrawn investments")
                ->excel([$fileName])
                ->send();

            $path = storage_path() . '/exports/' . $fileName . '.xlsx';

            dd("Done");

            \File::delete($path);
        });
    }

    private function getReport()
    {
        $schedules = CommissionPaymentSchedule::where('date', '>', '2019-03-20')
            ->clawBack(false)->get();

        $scheduleArray = array();

        foreach ($schedules as $schedule) {
            $commission = $schedule->commission;

            if (is_null($commission)) {
                $commission = $schedule->commission()->withTrashed()->first();

                $inv = $commission->investment()->withTrashed()->first();

                $scheduleArray['No Commission'][] = $this->getDetails($schedule, $inv);

                $this->clearSchedules($commission);

                continue;
            }

            $investment = $commission->investment;

            if (is_null($investment)) {
                $inv = $commission->investment()->withTrashed()->first();

                $scheduleArray['No Investment'][] = $this->getDetails($schedule, $inv);

                $this->clearSchedules($commission);

                continue;
            }

            $afterMaturity = (Carbon::parse($schedule->date) > Carbon::parse($investment->maturity_date));

            if (is_null($investment->withdrawal_date)) {
                $afterWithdrawal = false;
            } else {
                $afterWithdrawal = (Carbon::parse($schedule->date) > Carbon::parse($investment->withdrawal_date));
            }

            $withdrawn = ($investment->withdrawn == 1);

            if ($withdrawn && $afterMaturity) {
                $scheduleArray['Withdrawn and After Maturity'][] = $this->getDetails($schedule, $investment);
                $this->updateScheduleDate($schedule);
            } elseif ($withdrawn && $afterWithdrawal) {
                $scheduleArray['Withdrawn'][] = $this->getDetails($schedule, $investment);
                $this->updateScheduleDate($schedule);
            } elseif ($afterMaturity) {
                $scheduleArray['After Maturity'][] = $this->getDetails($schedule, $investment);
                $this->updateScheduleDate($schedule, $investment->maturity_date);
            }
        }

        return $scheduleArray;
    }

    private function getDetails($schedule, $investment)
    {
        return [
            'Schedule Date' => Carbon::parse($schedule->date)->toDateString(),
            'Schedule Amount' => $schedule->amount,
            'Schedule Description' => $schedule->description,
            'Inv ID' => $investment->id,
            'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
            'Client Code' => $investment->client->client_code,
            'Principal' => $investment->amount,
            'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
            'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
            'Withdrawn' => $investment->withdrawn,
            'Withdraw Date' =>
                $investment->withdrawal_date ? Carbon::parse($investment->withdrawal_date)->toDateString() : '',
            'Days Difference' => Carbon::parse($schedule->date)->diffInDays($investment->maturity_date),
            'Withdrawal Days Difference' => $investment->withdrawal_date ?
                Carbon::parse($schedule->date)->diffInDays($investment->withdrawal_date) : ''
        ];
    }

    private function updateScheduleDate($schedule, $date = null)
    {
        $schedule->date = $date ? $date : '2019-03-21';

        $schedule->save();
    }

    private function clearSchedules($commission)
    {
        $commission->schedules()->delete();
    }
}
