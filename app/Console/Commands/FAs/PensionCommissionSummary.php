<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\CommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\RealEstateCommissionPayment;
use App\Cytonn\Models\Unitization\UnitFundCommissionPayment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\Shares\ShareCommissionPayment;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class PensionCommissionSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:pension_commission_summary {bcp_id} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the commission for pension for the specified period';

    protected $start;

    protected $end;

    protected $approval;

    protected $bulk;

    protected $currencies;

    protected $investmentTypes;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $bcpId = $this->argument('bcp_id');

        $userId = $this->argument('user_id');

        $this->bulk = BulkCommissionPayment::findOrFail($bcpId);

        $this->approval = $this->bulk->combinedApproval;

        $this->start = Carbon::parse($this->bulk->start);

        $this->end = Carbon::parse($this->bulk->end);

        $report = $this->getReport();

        $fileName = "Pension Commission " . $this->start->toDateString() . ' to ' . $this->end->toDateString();

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the pension commissions export between '
                . $this->start->toDateString() . ' to ' . $this->end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @return array
     */
    private function getReport()
    {
        $recipients = CommissionRecepient::with('type')->whereIn('recipient_type_id', [2, 4, 5])->active()->get();

        $this->currencies = $this->getCurrencies();

        $this->investmentTypes = $this->getInvestmentTypes();

        $recipientArray = array();

        foreach ($recipients as $recipient) {
            $recipientArrayData = [
                'Employee ID' => $recipient->employee_id,
                'Employee Number' => $recipient->employee_number,
                'Name' => $recipient->name,
                'FA Type' => $recipient->type->name,
                'Active' => BooleanPresenter::presentYesNo($recipient->active),
                'Account Number' => $recipient->account_number,
                'KRA Pin' => $recipient->kra_pin,
                'Bank Code' => $recipient->bank_code,
                'Branch Code' => $recipient->branch_code,
            ];

            $investmentCommission = $this->getInvestmentData($recipient);
            $recipientArrayData['SP_Rollover_Commission'] = $investmentCommission['rollover_commission'];
            $recipientArrayData['SP_Commission'] = $investmentCommission['pension'];

            $reCommission = $this->getRealEstateCommission($recipient);
            $recipientArrayData['RE_Commission'] = $reCommission;

            $shareCommission = $this->getShareCommission($recipient);
            $recipientArrayData['Share_Commission'] = $shareCommission;

            $utfCommission = $this->getUnitFundCommission($recipient);
            $recipientArrayData['UTF_Commission'] = $utfCommission;

            if ($investmentCommission['pension'] == 0 && $reCommission == 0 && $shareCommission == 0 && $utfCommission == 0) {
                continue;
            }

            $recipientArrayData['Total'] = $investmentCommission['pension'] + $reCommission + $shareCommission + $utfCommission;

            $recipientArray[] = $recipientArrayData;
        }

        return $recipientArray;
    }

    /**
     * @return ClientInvestmentType[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getInvestmentTypes()
    {
        return ClientInvestmentType::whereIn('id', [3, 5])->get();
    }

    /**
     * @param CommissionRecepient $recipient
     * @return float|int
     */
    private function getInvestmentData(CommissionRecepient $recipient)
    {
        $pensionAmount = $rolloverAmount = 0;

        foreach ($this->currencies as $currency) {
            $totalRolloverCommission = $this->investmentTypes->sum(function (ClientInvestmentType $investmentType) use ($recipient, $currency) {
                $calculator = $recipient->calculator($currency, $this->start->copy(), $this->end->copy());

                $calculator->filterForInvestmentType($investmentType);

                $summary = $calculator->summary();

                return $summary->getCompliant() - $summary->getClawBacks();
            });

            $payment = CommissionPayment::where('currency_id', $currency->id)
                ->where('approval_id', $this->approval->id)
                ->where('recipient_id', $recipient->id)
                ->first();

            $paidAmount = $payment ? $payment->amount : 0;

            $pensionAmount += convert($paidAmount - $totalRolloverCommission, $currency, $this->end);

            $rolloverAmount += convert($totalRolloverCommission, $currency, $this->end);
        }

        return [
            'pension' => $pensionAmount,
            'rollover_commission' => $rolloverAmount
        ];
    }

    /**
     * @return Currency[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getCurrencies()
    {
        return Currency::whereHas('products', function ($q) {
            $q->active()->has('investments');
        })->get();
    }

    /**
     * @param CommissionRecepient $recipient
     * @return int|mixed
     */
    private function getRealEstateCommission(CommissionRecepient $recipient)
    {
        $payment = RealEstateCommissionPayment::where('approval_id', $this->approval->id)
            ->where('recipient_id', $recipient->id)
            ->first();

        return $payment ? $payment->amount : 0;
    }

    /**
     * @param CommissionRecepient $recipient
     * @return int|mixed
     */
    private function getShareCommission(CommissionRecepient $recipient)
    {
        $payment = ShareCommissionPayment::where('approval_id', $this->approval->id)
            ->where('commission_recipient_id', $recipient->id)
            ->first();

        return $payment ? $payment->amount : 0;
    }

    /**
     * @param CommissionRecepient $recipient
     * @return int|mixed
     */
    private function getUnitFundCommission(CommissionRecepient $recipient)
    {
        $payment = UnitFundCommissionPayment::where('client_transaction_approval_id', $this->approval->id)
            ->where('commission_recipient_id', $recipient->id)
            ->first();

        return $payment ? $payment->amount : 0;
    }
}
