<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class YearProduction extends Command
{
    /*
     * Get the report traits
     */
    use WithdrawalsTrait,
        InFlowsTrait,
        RealEstateSalesTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'fas:year_production {start} {end} {type?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of fa production for the period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = $this->argument('start');

        $end = $this->argument('end');

        $faType = $this->argument('type');

        $userId = $this->argument('user_id');

        $start = Carbon::parse($start)->startOfDay();

        $end = Carbon::parse($end)->endOfDay();

        $faTypes = $faType
            ? CommissionRecipientType::where('id', $faType)->get() : CommissionRecipientType::all();

        foreach ($faTypes as $faType) {
            $report = $this->getYearProduction($start, $end, $faType->id);

//        $report = $this->getProductionClientData($start, $end, $faType);

            $fileName = $faType->name . '_FA_Production';

            ExcelWork::generateAndStoreViewExcel($report, $fileName, 'exports.fas.year_production');

//        ExcelWork::generateAndStoreMultiSheet($report[0], $fileName);

            if ($userId) {
                $user = User::findOrFail($userId);

                $email = $user ? $user->email : [];
            } else {
                $email = [];
            }

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text(
                    'Please find attached the fa production report from ' .
                    $start->toDateString() . ' to ' . $end->toDateString()
                )
                ->excel([$fileName])
                ->send();
        }
    }

    /*
     * Get the client payments
     */
    public function getYearProduction(Carbon $start, Carbon $end, $faType)
    {
        $months = getDateRange($start, $end);

        $recipients = CommissionRecepient::where('recipient_type_id', $faType)
            ->get();

        $reportArray = array();

        foreach ($recipients as $recipient) {
            $recipientArray = [
                'name' => $recipient->name,
                'email' => $recipient->email,
                'reports_to' => $recipient->present()->getCurrentSupervisorName,
                'rank' => $recipient->present()->getCurrentRank,
                'type' => $recipient->present()->getCurrentType,
                'months' => $this->getMonthsProduction($months, $recipient)
            ];

            $reportArray[] = $recipientArray;
        }

        return [
            'Year Production' => $reportArray
        ];
    }

    /*
     * Get the month to month production
     */
    public function getMonthsProduction($months, $recipient)
    {
        $monthlyProduction = array();

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month)->startOfMonth();

            $endOfMonth = Carbon::parse($month)->endOfMonth();

            $monthlyProduction[$month->format('F-Y')] = [
                'new_money' => $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 1),
                'topups' => $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 2),
                'rollovers' => $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 3),
                'rollover_inflow' => $this->getRolloverInflows($startOfMonth, $endOfMonth, $recipient->id),
                'topup_inflow' => $this->getRolloverTopups($startOfMonth, $endOfMonth, $recipient->id),
                'reinvested_interest' =>
                    $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 5),
                'partial_withdrawals' =>
                    $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 4),
                'reinvested_withdrawals' => $this->getCompetitionWithdrawals(
                    $startOfMonth,
                    $endOfMonth,
                    $recipient->id,
                    null,
                    [2]
                ),
                'sent_to_client_withdrawals' => $this->getCompetitionWithdrawals(
                    $startOfMonth,
                    $endOfMonth,
                    $recipient->id,
                    null,
                    [1]
                ),
                'withdrawals' => $this->getCompetitionWithdrawals(
                    $startOfMonth,
                    $endOfMonth,
                    $recipient->id,
                    null,
                    [1, 2]
                ),
                'real_estate_actuals' => $this->realEstateActualPayments($startOfMonth, $endOfMonth, $recipient->id),
                'period_withdrawals' =>
                    $this->getNormalInvestmentWithdrawals($startOfMonth, $endOfMonth, $recipient->id)
            ];
        }

        $startOfMonth = Carbon::parse($months[0])->startOfMonth();

        $endOfMonth = Carbon::parse($months[count($months) - 1])->endOfMonth();

        $monthlyProduction['Total'] = [
            'new_money' => $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 1),
            'topups' => $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 2),
            'rollovers' => $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 3),
            'reinvested_interest' =>
                $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 5),
            'partial_withdrawals' =>
                $this->getInflows($startOfMonth, $endOfMonth, $recipient->id, null, 4),
            'rollover_inflow' => $this->getRolloverInflows($startOfMonth, $endOfMonth, $recipient->id),
            'topup_inflow' => $this->getRolloverTopups($startOfMonth, $endOfMonth, $recipient->id),
            'reinvested_withdrawals' => $this->getCompetitionWithdrawals(
                $startOfMonth,
                $endOfMonth,
                $recipient->id,
                null,
                [2]
            ),
            'sent_to_client_withdrawals' => $this->getCompetitionWithdrawals(
                $startOfMonth,
                $endOfMonth,
                $recipient->id,
                null,
                [1]
            ),
            'withdrawals' => $this->getCompetitionWithdrawals(
                $startOfMonth,
                $endOfMonth,
                $recipient->id,
                null,
                [1, 2]
            ),
            'real_estate_actuals' => $this->realEstateActualPayments($startOfMonth, $endOfMonth, $recipient->id),
            'period_withdrawals' => $this->getNormalInvestmentWithdrawals($startOfMonth, $endOfMonth, $recipient->id)
        ];

        return $monthlyProduction;
    }

    /*
     * Get the individual client data
     */
    public function getProductionClientData($start, $end, $faType)
    {
        $months = getDateRange($start, $end);

        $recipients = CommissionRecepient::where('recipient_type_id', $faType)
            ->get();

        $reportArray = array();

        foreach ($recipients as $recipient) {
            $reportArray[] = $this->getMonthsProductionClient($months, $recipient);
        }

        return $reportArray;
    }

    /*
     * Get the month to month production
     */
    public function getMonthsProductionClient($months, $recipient)
    {
        $monthlyProduction = array();

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month)->startOfMonth();

            $endOfMonth = Carbon::parse($month)->endOfMonth();

            $monthlyProduction[$month->format('F-Y') . ' Reinvested Withdrawals'] =
                $this->getCompetitionWithdrawalClients($startOfMonth, $endOfMonth, $recipient->id, null, [2]);
            $monthlyProduction[$month->format('F-Y') . ' Sent to Client'] =
                $this->getCompetitionWithdrawalClients($startOfMonth, $endOfMonth, $recipient->id, null, [1]);
            $monthlyProduction[$month->format('F-Y') . ' Withdrawals'] = $this->getCompetitionWithdrawalClients(
                $startOfMonth,
                $endOfMonth,
                $recipient->id,
                null,
                [1, 2]
            );
        }

        $startOfMonth = Carbon::parse($months[0])->startOfMonth();

        $endOfMonth = Carbon::parse($months[count($months) - 1])->endOfMonth();

        $monthlyProduction['Total Reinvested Withdrawals'] =
            $this->getCompetitionWithdrawalClients($startOfMonth, $endOfMonth, $recipient->id, null, [2]);
        $monthlyProduction['Total Sent to Client'] =
            $this->getCompetitionWithdrawalClients($startOfMonth, $endOfMonth, $recipient->id, null, [1]);
        $monthlyProduction['Total Withdrawals'] =
            $this->getCompetitionWithdrawalClients($startOfMonth, $endOfMonth, $recipient->id, null, [1, 2]);

        return $monthlyProduction;
    }
}
