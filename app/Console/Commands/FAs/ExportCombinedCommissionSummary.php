<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\DatePresenter;
use Carbon\Carbon;
use Cytonn\Reporting\CombinedCommissionsExcelGenerator;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ExportCombinedCommissionSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:export_combined_commission_summary {bcp_id} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the combined commission excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $id = $this->argument('bcp_id');

        $start = Carbon::now();

        $bcp = BulkCommissionPayment::findOrFail($id);

        (new CombinedCommissionsExcelGenerator())->excel($bcp)->store('xlsx');

        $userId = $this->argument('user_id');

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject('Combined Commission Report - '.DatePresenter::formatDate($bcp->end))
            ->text("See attached report for {$bcp->start->toFormattedDateString()} 
                    to {$bcp->end->toFormattedDateString()} 
                    (Time taken: ".Carbon::now()->diffInMinutes($start->copy()).' minutes)')
            ->excel(['Combined_commissions'])
            ->send();

        $path = storage_path().'/exports/Combined_commissions.xlsx';

        return \File::delete($path);
    }
}
