<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Crims\Commission\OverrideManager;
use Illuminate\Console\Command;

class VerifyCommissionOverrides extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    //Live Mode : 1 or 0 ; With live mode on override schedules are deleted
    // and new recreated afresh with no verification
    //Update Schedules : 1 or 0 : With update schedules on
    // and live mode off schedules will be updated after verification if need be
    //Scope : 0 (All), 1 (Investments), 2 (RE), 3 (Shares), 4 (UnitFunds)
    protected $signature = 'crims:verify_commission_overrides {live_mode} {update_schedules} 
    {scope} {start?} {end?} {recipient_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify the commission overrides table appropriately';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $starDate = $this->argument('start') ? Carbon::parse($this->argument('start')) : null;

        $endDate = $this->argument('end') ? Carbon::parse($this->argument('end')) : null;

        $recipient = $this->argument('recipient_id') ?
            CommissionRecepient::find($this->argument('recipient_id')) : null;

        $manager = new OverrideManager($starDate, $endDate, $recipient);

        $liveMode = $this->argument('live_mode') == 1 ? true : false;

        $updateSchedules = $this->argument('update_schedules') == 1 ? true : false;

        $scope = $this->argument('scope');

        $manager->setLiveMode($liveMode);

        $manager->setUpdateSchedules($updateSchedules);

        $manager->awardOverrides($scope);

        $manager->sendVerificationReport();
    }
}
