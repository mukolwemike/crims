<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Commands\RegisterCommissionClawBackCommandHandler;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CheckReinvestedInterestClawbacks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:check_reinvested_interest_clawbacks {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a report of the reinvested interest clawbacks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::transaction(function () {
            $userId = $this->argument('user_id');

            $commissions = $this->getReport();

            $fileName = 'Reinvested Interest Clawbacks';

            ExcelWork::generateAndStoreSingleSheet($commissions, $fileName);

            if ($userId) {
                $user = User::findOrFail($userId);

                $email = $user ? $user->email : [];
            } else {
                $email = [];
            }

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text("Please find attached the client investments reinvested interest clawbacks")
                ->excel([$fileName])
                ->send();

            $path = storage_path().'/exports/' . $fileName .'.xlsx';

//            dd("Done");

            \File::delete($path);
        });
    }

    private function getReport()
    {
        $investments = ClientInvestment::where('investment_type_id', 5)
            ->where('invested_date', '>=', '2018-04-26')->get();

        $investments = ClientInvestment::where('investment_type_id', 5)
            ->whereHas('withdrawals', function ($q) {
                $q->where('type_id', 2)->where('withdraw_type', 'withdrawal');
            })->get();

        $withdrawals = ClientInvestmentWithdrawal::where('type_id', 2)
            ->where('date', '>=', '2018-04-26')
            ->where('withdraw_type', 'withdrawal')
            ->whereHas('investment', function ($q) {
                $q->where('investment_type_id', 5);
            })->get();

        $investmentArray = array();

        foreach ($withdrawals as $finalWithdrawal) {
            $oldInvestment = $finalWithdrawal->investment;

            $tenor =
                Carbon::parse($oldInvestment->invested_date)->diffInDays(Carbon::parse($oldInvestment->maturity_date));

            $dueComm = $oldInvestment->amount * $oldInvestment->commission->rate * $tenor / (100 * 365);

            $clawed = $oldInvestment->commission->clawBacks()->sum('amount');

            $schedules = $oldInvestment->commission->schedules()->sum('amount');

            $toBeClawed = $schedules - $clawed - $dueComm;

            if ($schedules != 0 && $toBeClawed > 0) {
                $investmentArray[] = [
//                    'Current Inv ID' => $investment->id,
                    'Old Inv ID' => $oldInvestment->id,
                    'Client Name' => ClientPresenter::presentFullNames($oldInvestment->client_id),
                    'Client Code' => $oldInvestment->client->client_code,
                    'FA' => $oldInvestment->commission->recipient->name,
                    'Old Principal' => $oldInvestment->amount,
                    'Old Type' => $oldInvestment->investment_type_id,
                    'Old Invested Date' => Carbon::parse($oldInvestment->invested_date)->toDateString(),
                    'Old Maturity Date' => Carbon::parse($oldInvestment->maturity_date)->toDateString(),
                    'Old Withdrawn' => $oldInvestment->withdrawn,
                    'Old Withdrawal Date' => Carbon::parse($oldInvestment->withdrawal_date)->toDateString(),
                    'Due Comm' => $dueComm,
                    'Total Schedule' => $schedules,
                    'Clawed' => $clawed,
                    'To Be Clawed' => $toBeClawed,
                ];

                $name = ClientPresenter::presentFullNames($oldInvestment->client_id);

                CommissionClawback::create([
                    'commission_id' => $oldInvestment->commission->id,
                    'amount' => $toBeClawed,
                    'date' => '2018-09-17',
                    'narration' => "Premature Rollover Client: $name Code: {$oldInvestment->client->client_code} 
                    Principal: $oldInvestment->amount Date: $finalWithdrawal->date 
                    Amount withdrawn: $finalWithdrawal->amount",
                    'withdrawal_id' => $finalWithdrawal->id,
                    'type' => 'withdraw',
                ]);
            }
        }

//        foreach ($investments as $investment)
//        {
//            $withdrawalGroups = ClientInvestmentWithdrawal::where('reinvested_to', $investment->id)
//                ->whereHas('investment', function ($q)
//                {
//                    $q->where('investment_type_id', 5);
//                })->get()->groupBy('investment_id');
//
//            foreach ($withdrawalGroups as $withdrawalGroup)
//            {
//                $finalWithdrawal = $withdrawalGroup[0];
//
//                $oldInvestment = $finalWithdrawal->investment;
//
//                $tenor = Carbon::parse($oldInvestment->invested_date)->diffInDays(Carbon::parse($oldInvestment->maturity_date));
//
//                $dueComm = $oldInvestment->amount * $oldInvestment->commission->rate * $tenor / (100 * 365);
//
//                $clawed = $oldInvestment->commission->clawBacks()->sum('amount');
//
//                $schedules = $oldInvestment->commission->schedules()->sum('amount');
//
//                if($schedules == 0)
//                {
//                    $toBeClawed = $schedules - $clawed - $dueComm;
//
//                    $investmentArray[] = [
//                        'Current Inv ID' => $investment->id,
//                        'Old Inv ID' => $oldInvestment->id,
//                        'Client Name' => ClientPresenter::presentFullNames($oldInvestment->client_id),
//                        'Client Code' => $oldInvestment->client->client_code,
//                        'Old Principal' => $oldInvestment->amount,
//                        'Old Type' => $oldInvestment->investment_type_id,
//                        'Old Invested Date' => Carbon::parse($oldInvestment->invested_date)->toDateString(),
//                        'Old Maturity Date' => Carbon::parse($oldInvestment->maturity_date)->toDateString(),
//                        'Old Withdrawn' => $oldInvestment->withdrawn,
//                        'Old Withdrawal Date' => Carbon::parse($oldInvestment->withdrawal_date)->toDateString(),
//                        'Due Comm' => $dueComm,
//                        'Total Schedule' => $schedules,
//                        'Clawed' => $clawed,
//                        'To Be Clawed' => $toBeClawed,
//                    ];
//                }
//            }
//        }

        return $investmentArray;
    }
}
