<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionClawbackPayment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateCommissionClawbacks extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:update_commission_clawbacks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the commission clawbacks in the system';

    /**
     * Create a new command instance.
     * MergeFARecords constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $date = Carbon::parse('2018-07-19');

        CommissionClawback::where('id', '>', 0)->update(['fully_paid_on' => null]);

        $clawbacks = CommissionClawback::where('date', '<=', $date)->get();

        foreach ($clawbacks as $clawback) {
            CommissionClawbackPayment::create([
                'commission_clawback_id' => $clawback->id,
                'amount' => $clawback->amount,
                'date' => $clawback->date
            ]);

            $clawback->fully_paid_on =  $clawback->date;
            $clawback->save();
        }
    }
}
