<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CommissionRecipientDetailsReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:commission_details_summary {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the commission recipient details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $clientPayments = $this->getReport();

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'Commission Recipients - '. $date;

        ExcelWork::generateAndStoreMultiSheet($clientPayments, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the commission recipients report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getReport()
    {
        $recipientArray = array();

        $recipients = CommissionRecepient::orderBy('name')->get();

        foreach ($recipients as $recipient) {
            $recipientArray['FA Details'][] = [
                'ID' => $recipient->id,
                'CRM ID' => $recipient->crm_id,
                'Name' => $recipient->name,
                'Email' => $recipient->email,
                'Phone' => $recipient->phone,
                'Rank' => $recipient->rank->name,
                'Type' => $recipient->type->name,
                'Reports To' => $recipient->reportsTo ? $recipient->reportsTo->name : '',
                'Employee ID' => $recipient->employee_id,
                'Employee Number' => $recipient->employee_number,
                'Active' => $recipient->active == 1 ? 'Active' : 'Inactive',
                'Zero Commission' => $recipient->zero_commission == 1 ? 'Yes' : 'No'
            ];
        }

        $types = CommissionRecipientType::all();

        foreach ($types as $type) {
            $recipients = CommissionRecepient::orderBy('name')->where('recipient_type_id', $type->id)->get();

            $typeArray = array();

            foreach ($recipients as $recipient) {
                $positions = $recipient->commissionRecipientPositions()->orderBy('start_date')->get();

                if (count($positions) == 0) {
                    $recipientArray['No Positions'][] = [
                        'ID' => $recipient->id,
                        'Name' => $recipient->name,
                        'Email' => $recipient->email,
                        'Phone' => $recipient->phone,
                        'Active' => $recipient->active == 1 ? 'Active' : 'Inactive',
                    ];
                } else {
                    foreach ($positions as $key => $position) {
                        if ($key == 0) {
                            $id = $recipient->id;
                            $name = $recipient->name;
                            $email = $recipient->email;
                            $phone = $recipient->phone;
                            $active = $recipient->active == 1 ? 'Active' : 'Inactive';
                        } else {
                            $id = $name = $email = $phone = $active = '';
                        }

                        $typeArray[] = [
                            'ID' => $id,
                            'Name' => $name,
                            'Email' => $email,
                            'Phone' => $phone,
                            'Active' => $active,
                            'Type' => $position->type->name,
                            'Rank' => $position->rank->name,
                            'Reports To' => $position->reportsTo ? $position->reportsTo->name : '',
                            'Start Date' => $position->start_date,
                            'End Date' => $position->end_date
                        ];
                    }
                }
            }

            $recipientArray[$type->name] = $typeArray;
        }

        return $recipientArray;
    }
}
