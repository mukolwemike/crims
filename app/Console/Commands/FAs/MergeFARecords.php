<?php

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealestateCommission;
use App\Cytonn\Models\RealEstateCommissionPayment;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundCommissionPayment;
use App\Cytonn\Models\Unitization\UnitFundCommissionPaymentSchedule;
use Cytonn\Models\Shares\ShareCommissionPayment;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;

class MergeFARecords extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cytonn:merge-fa-records {original_fa_id} {duplicate_fa_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Merge FA Records.';

    /**
     * Create a new command instance.
     * MergeFARecords constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $original_fa_id = $this->argument('original_fa_id');

        $duplicate_fa_id = $this->argument('duplicate_fa_id');

        $changeRecords = 0;

        $duplicateRecipient = CommissionRecepient::findOrFail($duplicate_fa_id);

        DB::transaction(function () use ($original_fa_id, $duplicateRecipient, &$changeRecords) {
            $changeRecords += $duplicateRecipient->commissions()->update(['recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->commissionPayments()->update(['recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->realEstateCommissions()->update(['recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->realEstateCommissionPayments()
                ->update(['recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->shareCommissionPaymentSchedules()
                ->update(['commission_recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->shareCommissionPayments()
                ->update(['commission_recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->unitFundCommissionPaymentSchedules()
                ->update(['commission_recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->unitFundCommissionPayments()
                ->update(['commission_recipient_id' => $original_fa_id]);

            $changeRecords += $duplicateRecipient->unitFundCommissions()
                ->update(['commission_recipient_id' => $original_fa_id]);

            $duplicateRecipient->userAccounts()->detach();
        });

        $recipient = CommissionRecepient::find($duplicate_fa_id);

        $recipient->commissionRecipientPositions()->delete();

        $recipient->delete();

        $this->info($changeRecords . ' records affected');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['original_fa_id', InputArgument::REQUIRED, 'The original FA id.'],
            ['duplicate_fa_id', InputArgument::REQUIRED, 'The duplicate FA id.'],
        ];
    }
}
