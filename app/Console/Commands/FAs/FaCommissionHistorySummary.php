<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionPayment;
use App\Cytonn\Models\RealEstateCommissionPayment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class FaCommissionHistorySummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:fa_commission_history_summary {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the commissions paid to the fas for the period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $commissions = $this->getFaCommissions($start, $end);

        $fileName = 'FA Commissions History ' . $start->toDateString() . ' To ' . $end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($commissions, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the fa commissions history summary report between '
                . $start->toDateString() . ' and ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    private function getFaCommissions(Carbon $start, Carbon $end)
    {
        $commissionArray = array();

        $commissionArray['Investments'] = $this->getInvestmentCommissions($start, $end);

        $commissionArray['Real Estate'] = $this->getRealEstateCommissions($start, $end);

        return $commissionArray;
    }

    private function getInvestmentCommissions(Carbon $start, Carbon $end)
    {
        $paymentGroups = CommissionPayment::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->whereHas('commissionRecipient', function ($q) {
                $q->where('rank_id', 3)->whereNotIn('recipient_type_id', [1, 3]);
            })->orderBy('date')->get()->groupBy('recipient_id');

        $paymentArray = array();

        foreach ($paymentGroups as $paymentGroup) {
            foreach ($paymentGroup as $key => $payment) {
                $recipient = $payment->commissionRecipient;

                $earned = $payment->amount - $payment->override;

                $paymentArray[] = [
                    'FA Name' => $key == 0 ? $recipient->name : '',
                    'FA Email' => $key == 0 ? $recipient->email : '',
                    'FA Rank' => $key == 0 ? $recipient->rank->name : '',
                    'FA Type' => $key == 0 ? $recipient->type->name : '',
                    'Payment Date' => $payment->date,
                    'Currency' => $payment->currency->code,
                    'Final Commission' => $payment->amount,
                    'Converted Final Commission' =>
                        convert($payment->amount, $payment->currency, Carbon::parse($payment->date)),
                    'Earned Commission' => $earned,
                    'Converted Earned Commission' =>
                        convert($earned, $payment->currency, Carbon::parse($payment->date)),
                    'Clawback Amount' => $payment->claw_back,
                    'Converted Clawback Amount' =>
                        convert($payment->claw_back, $payment->currency, Carbon::parse($payment->date)),
                    'Override Amount' => $payment->overrides,
                    'Converted Override Amount' =>
                        convert($payment->overrides, $payment->currency, Carbon::parse($payment->date)),
                    'Description' => $payment->description
                ];
            }
        }

        return $paymentArray;
    }

    private function getRealEstateCommissions(Carbon $start, Carbon $end)
    {
        $paymentGroups = RealEstateCommissionPayment::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->whereHas('recipient', function ($q) {
                $q->where('rank_id', 3)->whereNotIn('recipient_type_id', [1, 3]);
            })->orderBy('date')->get()->groupBy('recipient_id');

        $paymentArray = array();

        foreach ($paymentGroups as $paymentGroup) {
            foreach ($paymentGroup as $key => $payment) {
                $recipient = $payment->recipient;

                $earned = $payment->amount - $payment->override;

                $paymentArray[] = [
                    'FA Name' => $key == 0 ? $recipient->name : '',
                    'FA Email' => $key == 0 ? $recipient->email : '',
                    'FA Rank' => $key == 0 ? $recipient->rank->name : '',
                    'FA Type' => $key == 0 ? $recipient->type->name : '',
                    'Payment Date' => $payment->date,
                    'Final Commission' => $payment->amount,
                    'Earned Commission' => $earned,
                    'Override Amount' => $payment->overrides,
                    'Description' => $payment->description
                ];
            }
        }

        return $paymentArray;
    }
}
