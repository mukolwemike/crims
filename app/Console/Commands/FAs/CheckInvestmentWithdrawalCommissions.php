<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionClawback;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckInvestmentWithdrawalCommissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:check_withdrawn_investment_commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a report of the withdrawn investments commissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        \DB::transaction(function () {
            $commissions = $this->getReport();

            $fileName = 'Withdrawn_Investments';

            ExcelWork::generateAndStoreMultiSheet($commissions, $fileName);

            $email = [];

            Mailer::compose()
//                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text("Please find attached the client investments withdrawal clawbacks")
                ->excel([$fileName])
                ->send();

            $path = storage_path().'/exports/' . $fileName .'.xlsx';

            \File::delete($path);
        });

        dd("Done");
    }

    private function getClawbackAmount(ClientInvestment $investment, ClientInvestmentWithdrawal $withdrawal)
    {
        $maturity = Carbon::parse($investment->maturity_date)->copy();

        $tenor = $maturity->diffInDays(Carbon::parse($withdrawal->date));

        return $withdrawal->amount * $investment->commission->rate * $tenor / (100 * 365);
    }

    private function narration($withdrawal, $type)
    {
        $client = $withdrawal->investment->client;

        $withdrawn = AmountPresenter::currency($withdrawal->amount);
        $principal = $withdrawal->investment->amount;
        $date = DatePresenter::formatDate($withdrawal->date);
        $name = $client->present()->fullName;

        $typeDesc = ($type == 'rollover') ? "Premature rollover" : "Premature withdrawal";

        return "$typeDesc Client: $name Code: {$client->client_code} 
        Principal: $principal Date: $date Amount withdrawn: $withdrawn";
    }

    private function record(ClientInvestment $investment, $withdrawal, $amount)
    {
        CommissionClawback::create([
            'commission_id' => $investment->commission->id,
            'amount' => $amount,
            'date' => '2018-10-16',
            'narration' => $this->narration($withdrawal, 'withdrawal'),
            'withdrawal_id' => $withdrawal->id,
            'type' => 'withdraw',
        ]);
    }

    private function getReport()
    {
        $withdrawalGroups = ClientInvestmentWithdrawal::where('date', '>=', '2018-09-01')
            ->where('withdraw_type', 'withdrawal')->where('type_id', 1)
//            ->whereIn('investment_id', [14723])
            ->get()
            ->groupBy('investment_id');

        $investmentArray = array();
        $multipleArray = array();

        foreach ($withdrawalGroups as $withdrawalGroup) {
            $investment = $withdrawalGroup[0]->investment;

            $rollover = $investment->withdrawals()->ofType('reinvested')->where('withdraw_type', 'withdrawal')->first();

            if ($rollover) {
                continue;
            }

            $withdrawalGroup = $investment->withdrawals()->where('withdraw_type', 'withdrawal')->where('type_id', 1)
                ->get();

            $commission = $withdrawalGroup[0]->investment->commission;

            $scheduleAmount = $commission->schedules()->clawBack(false)->sum('amount');

            if ($scheduleAmount == 0) {
                continue;
            }

            $clawedAmount = $commission->clawBacks()->sum('amount');

            $requiredClawback = 0;

            $withdrawalArray = array();

            foreach ($withdrawalGroup as $key => $withdrawal) {
                $investment = $withdrawal->investment;

//                $prepared = $this->getPrepared($investment, $withdrawal);

//                $validate = $this->validate($investment, $withdrawal, $prepared);

                $amount = $this->getClawbackAmount($investment, $withdrawal);

                $requiredClawback += $amount;

                $withdrawalArray[] = [
                    'Inv ID' => $key == 0 ? $investment->id : '',
                    'Client Name' => $key == 0 ? ClientPresenter::presentFullNames($investment->client_id) : '',
                    'Client Code' => $key == 0 ? $investment->client->client_code : '',
                    'FA' => $key == 0 ? $investment->commission->recipient->name : '',
                    'Principal' => $key == 0 ? $investment->amount : '',
                    'Invested Date' => $key == 0 ? Carbon::parse($investment->invested_date)->toDateString() : '',
                    'Maturity Date' => $key == 0 ? Carbon::parse($investment->maturity_date)->toDateString() : '',
                    'Withdrawal Count' => $key == 0 ? count($withdrawalGroup) : '',
                    'Withdrawn' => $key == 0 ?  $investment->withdrawn : '',
                    'Commission Rate' => $key == 0 ? $investment->commission->rate : '',
                    'Scheduled Comm' => $key == 0 ?  $scheduleAmount : '',
                    'Currently Clawed' => $key == 0 ?  $clawedAmount : '',
                    'Withdrawal Date' => $withdrawal->date ? Carbon::parse($withdrawal->date)->toDateString() : '',
                    'Withdrawn Amount' => $withdrawal->amount,
                    'Clawback For Withdrawal' => $amount,
                    'Cumulative Commission To Be Clawed' => $requiredClawback,
                    'Difference' => $key == count($withdrawalGroup) - 1 ? $requiredClawback - $clawedAmount : ''
                ];
            }

            if ($requiredClawback == 0) {
                continue;
            }

            if ($requiredClawback > $scheduleAmount) {
                continue;
            }

            $diff = $requiredClawback - $clawedAmount;

            if (abs($diff) < 1) {
                continue;
            }

            if ($diff > 0) {
                $this->record($investment, $withdrawal, $diff);

                if (count($withdrawalArray) != 0) {
                    if (count($withdrawalGroup) == 1) {
                        $investmentArray = array_merge($investmentArray, $withdrawalArray);
                    } else {
                        $multipleArray = array_merge($multipleArray, $withdrawalArray);
                    }
                }
            }
        }

        return [
            'Single Withdrawal' => $investmentArray,
            'Multiple Partial Withdrawals' => $multipleArray
        ];
    }

    private function getPrepared(ClientInvestment $investment, ClientInvestmentWithdrawal $withdrawal)
    {
        return collect($investment->calculate($withdrawal->date, true, null, false, true)->getPrepared()->actions)
            ->filter(
                function ($action) use ($withdrawal) {
                    if (isset($action->action)) {
                        if ($action->action instanceof ClientInvestmentWithdrawal) {
                            return $action->action->id == $withdrawal->id;
                        }
                    }

                    return false;
                }
            )->first();
    }

    private function validate(ClientInvestment $investment, ClientInvestmentWithdrawal $withdrawal, $prepared)
    {
        $amount = 0;
        if ($prepared) {
            $amount =  $prepared->original_principal - $prepared->principal;
        }

        if ($amount <= 1) {
            return [
                'clawback' => false,
                'reason' => 'No withdrawn Principal'
            ];
        }

        return [
            'clawback' => true,
            'reason' => 'Save Clawback'
        ];
    }
}
