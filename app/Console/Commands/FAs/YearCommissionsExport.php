<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class YearCommissionsExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:year_commissions_export {start} {end} {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));
        $user = User::findOrFail($this->argument('user_id'));

        $investmentsArray = array();

        $investments = ClientInvestment::whereIn('investment_type_id', [1, 2])
            ->where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->get();

        foreach ($investments as $investment) {
            $investmentsArray['New_Investments'][] = $this->formatInvestment($investment);
        }

        $investments = ClientInvestment::whereIn('investment_type_id', [3])
            ->where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->get();

        foreach ($investments as $investment) {
            $investmentsArray['Rollovers'][] = $this->formatInvestment($investment);
        }

        $investments = ClientInvestment::whereIn('investment_type_id', [5])
            ->where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->get();

        foreach ($investments as $investment) {
            $investmentsArray['Rollover Interest'][] = $this->formatInvestment($investment);
        }

        $investments = ClientInvestment::where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->whereHas(
                'withdrawals',
                function ($q) {
                    $q->where('type_id', 1)->where('withdraw_type', 'withdrawal');
                }
            )
            ->get();

        foreach ($investments as $investment) {
            if ($investment->withdrawal_date && \Cytonn\Core\DataStructures\Carbon::parse($investment->withdrawal_date)
                    ->eq(Carbon::parse($investment->maturity_date))) {
            } else {
                $investmentsArray['Premature Withdrawn'][] = $this->formatInvestment($investment);
            }
        }

        ExcelWork::generateAndStoreMultiSheet($investmentsArray, 'CommissionReport');

        Mailer::compose()
            ->to($user->email)
            ->bcc(config('system.administrators'))
            ->subject('CommissionReport')
            ->text('Please find attached the commission report')
            ->excel(['CommissionReport'])
            ->send();

        $path = storage_path() . '/exports/' . 'CommissionReport' . '.xlsx';

        return \File::delete($path);
    }

    public function formatInvestment($investment)
    {
        $invRepo = (new InvestmentsRepository($investment));

        return [
            'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
            'Client Code' => $investment->client->client_code,
            'Investment Type' => $investment->type->name,
            'Principal' => $investment->amount,
            'InterestRate' => AmountPresenter::currency($investment->interest_rate),
            'ValueDate' => DatePresenter::formatDate($investment->invested_date),
            'MaturityDate' => DatePresenter::formatDate($investment->maturity_date),
            'Tenor' => $invRepo->getTenorInMonths(),
            'WithdrawalDate' => DatePresenter::formatDate($investment->withdrawal_date),
            'Rolled' => BooleanPresenter::presentYesNo($investment->rolled),
            'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
            'InterestEarned' => AmountPresenter::currency($invRepo->getNetInterestForInvestment()),
            'Total ScheduledCommission' => AmountPresenter::currency($invRepo->getScheduledCommissions()),
            'Total Clawbacks' => AmountPresenter::currency($invRepo->getCommissionClawbacks()),
            'Net_Commission' => AmountPresenter::currency($invRepo->getCommissionsPaidFromInvestment()),
        ];
    }
}
