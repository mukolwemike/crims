<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Commission\Generator;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class AwardValentineCommission extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:award_valentine_commission {type} {start?} {end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Award the valentine commission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
//        $start = Carbon::parse($this->argument('start'));
//        $end = Carbon::parse($this->argument('end'));

        $type = $this->argument('type');

        if ($type == 'new') {
            $this->processNewInvestments('2020-02-10', '2020-02-29');
        } elseif ($type == 'rollover') {
            $this->processRolloverInvestments('2020-02-01', '2020-02-29');
        }
    }

    private function processNewInvestments($startDate, $endDate)
    {
        \DB::transaction(function () use ($startDate, $endDate) {
            $start = Carbon::parse($startDate);

            $end = Carbon::parse($endDate);

            $fileName = 'New Investments Additional Commission';

            $investmentArray = $this->getNewInvestments($start, $end);

            ExcelWork::generateAndStoreSingleSheet($investmentArray, $fileName);

            Mailer::compose()
                ->to([])
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the new investments additional commission')
                ->excel([$fileName])
                ->send();

            $path = storage_path() . '/exports/' . $fileName . '.xlsx';

            dd("New Investments Done");

            \File::delete($path);
        });
    }

    private function processRolloverInvestments($startDate, $endDate)
    {
        \DB::transaction(function () use ($startDate, $endDate) {
            $start = Carbon::parse($startDate);

            $end = Carbon::parse($endDate);

            $fileName = 'Rollover Additional Commission';

            $investmentArray = $this->getRolloverInvestments($start, $end);

            ExcelWork::generateAndStoreSingleSheet($investmentArray, $fileName);

            Mailer::compose()
                ->to([])
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the rollover additional commission')
                ->excel([$fileName])
                ->send();

            $path = storage_path() . '/exports/' . $fileName . '.xlsx';

            dd("Rollover Done");

            \File::delete($path);
        });
    }

    private function getNewInvestments(Carbon $start, Carbon $end)
    {
        $investments = ClientInvestment::whereIn('investment_type_id', [1, 2])
            ->whereIn('product_id', [1, 2, 3, 11, 13, 15]) //CPN and CHYS
            ->where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->get();

        $transferArray = [];

        $investmentArray = array();

        $generator = new Generator();

        foreach ($investments as $investment) {
            $product = $investment->product;

            $commission = $investment->commission;

            $recipient = $commission->recipient;

            $tenorInDays = $investment->repo->getTenorInDays();

            if ($tenorInDays < 90) {
                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "Below Minimum Tenor");
                continue;
            }

//            if (in_array($investment->id, $transferArray)) {
//                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "Transfer from CHYS");
//                continue;
//            }
//
//            if ($product->id == 11 && $investment->amount < 1000000) {
//                $investmentArray[] = $this->getInvestmentArray(
//                    $investment,
//                    $commission->rate,
//                    "Below minimim principal
//                   ");
//                continue;
//            }

            if ($recipient->zero_commission || $commission->rate == 0) {
                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "Zero Commission");
                continue;
            }

            $rate = $commission->rate + 0.25;

//            if (in_array($recipient->recipient_type_id, [3, 4, 5])) {
//                $rate = 1.5;
//
//                if ($product->id == 11 && $tenorInDays > 372) {
//                    $rate = $this->getAverageTenor($tenorInDays, $investment, $rate, 1);
//                }
//            } else {
//                $rate = 0.75;
//
//                if ($product->id == 11 && $tenorInDays > 372) {
//                    $rate = $this->getAverageTenor($tenorInDays, $investment, $rate, 0.5);
//                }
//            }

            if ($commission->rate != $rate) {
                $oldRate = $commission->rate;

                $commission->rate = $rate;

                $commission->save();

                $investment = $investment->fresh();

                $generator->recreateSchedules($investment);

                $investmentArray[] = $this->getInvestmentArray($investment, $oldRate, "Update Schedules");
            } else {
                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "No rate change");
            }
        }

        return $investmentArray;
    }

    private function getRolloverInvestments(Carbon $start, Carbon $end)
    {
        $investments = ClientInvestment::where('investment_type_id', 3)
            ->whereIn('product_id', [1, 2, 3, 11, 13, 15]) //CPN and CHYS
            ->where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->get();

        $transferArray = [];

        $investmentArray = array();

        $generator = new Generator();

        foreach ($investments as $investment) {
            $product = $investment->product;

            $commission = $investment->commission;

            $recipient = $commission->recipient;

            $tenorInDays = $investment->repo->getTenorInDays();

//            if (in_array($investment->id, $transferArray)) {
//                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "Transfer from CHYS");
//                continue;
//            }

//            if ($tenorInDays < 180) {
//                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "Below Minimum Tenor");
//                continue;
//            }
//
//            if ($product->id == 11 && $investment->amount < 1000000) {
//                $investmentArray[] = $this->getInvestmentArray(
//                    $investment,
//                    $commission->rate,
//                    "Below minimim principal
//                   ");
//                continue;
//            }

            if ($recipient->zero_commission || $commission->rate == 0) {
                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "Zero Commission");
                continue;
            }

            $rate = $commission->rate + 0.25;

            if ($commission->rate != $rate) {
                $oldRate = $commission->rate;

                $commission->rate = $rate;

                $commission->save();

                $investment = $investment->fresh();

                $generator->recreateSchedules($investment);

                $investmentArray[] = $this->getInvestmentArray($investment, $oldRate, "Update Schedules");
            } else {
                $investmentArray[] = $this->getInvestmentArray($investment, $commission->rate, "No rate change");
            }
        }

        return $investmentArray;
    }

    private function getInvestmentArray(ClientInvestment $investment, $oldRate, $verdict)
    {
        return [
            'Inv ID' => $investment->id,
            'Client Code' => $investment->client->client_code,
            'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
            'FA' => @$investment->commission->recipient->name,
            'Investment Type' => $investment->investmentType->name,
            'Amount' => $investment->amount,
            'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
            'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
            'Product' => $investment->product->name,
            'Tenor In Days' => $investment->repo->getTenorInDays(),
            'Old Rate' => $oldRate,
            'New Rate' => $investment->commission->rate,
            'Total Commission' => $investment->commission->fresh()->schedules()->sum('amount'),
            'Action' => $verdict
        ];
    }

    private function getAverageTenor($days, $investment, $firstRate, $secondRate)
    {
        $excessDays = $days - 372;

        $firstCommission = $firstRate * $investment->amount / 100;

        $secondCommission = $secondRate * $excessDays * $investment->amount / (100 * 365);

        $totalCommission = $firstCommission + $secondCommission;

        return round(($totalCommission * 100 * 365) / ($investment->amount * $days), 2) + 0.01;
    }
}
