<?php

namespace App\Console\Commands\FAs;

use App\Cytonn\Hr\HrManager;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Api\AuthorizeAPI;
use Illuminate\Console\Command;

class GetFADetailsFromHR extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'fa:get-hr-details {all?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     * GetFADetailsFromHR constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $all = $this->argument('all');

        $manager = new HrManager();

        $manager->getCommissionRecipientData($all);
//        $api = new AuthorizeAPI();
//
//        $url = '/api/employee/';
//
//        $this->info('Getting recipients with null employee IDs and numbers');
//
//        CommissionRecepient::where([
//            'employee_id' => null,
//            'employee_number' => null,
//        ])
//            ->get()
//            ->each(
//                function ($recipient) use ($api, $url) {
//                    if (str_contains($recipient->email, 'cytonn')) {
//                        $this->info('Fetching details for ' . $recipient->email);
//
//                        $email = str_replace('/', '', $recipient->email);
//
//                        $response = $api->apiRequest($url . $email);
//
//                        if ($response->getStatusCode() != 404) {
//                            $data = json_decode($response->getBody());
//                            if (count($data)) {
//                                $this->info('Successfully fetched details for ' . $recipient->email);
//
//                                $data = $data[0];
//                                $recipient->employee_id = $data->id;
//                                $recipient->employee_number = $data->employee_number;
//                                $recipient->save();
//                            } else {
//                                $this->error('Failed to get details for ' . $recipient->email . ' from HR');
//                            }
//                        } else {
//                            $this->error('HR system returned ' . $response->getStatusCode());
//                        }
//                    }
//                }
//            );
    }
}
