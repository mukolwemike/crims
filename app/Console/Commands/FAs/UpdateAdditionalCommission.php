<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\Investment\AdditionalCommission;
use App\Cytonn\Models\Investment\AdditionalCommissionRepayment;
use Cytonn\Models\Investment\AdvanceInvestmentCommission;
use Cytonn\Models\RealEstate\AdvanceRealEstateCommission;
use DB;
use Illuminate\Console\Command;

class UpdateAdditionalCommission extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:update_additional_commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the additional commission table appropriately';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        DB::transaction(function () {
            $investmentCommissions = AdvanceInvestmentCommission::all();

            foreach ($investmentCommissions as $investmentCommission) {
                $repayments = $investmentCommission->commissionRepayments;

                $commission = AdditionalCommission::create([
                    'recipient_id' => $investmentCommission->recipient_id,
                    'date' => $investmentCommission->date,
                    'amount' => $investmentCommission->amount,
                    'paid_on' => $investmentCommission->paid_on,
                    'type_id' => $investmentCommission->advance_commission_type_id,
                    'repayable' => $investmentCommission->repayable,
                    'description' => $investmentCommission->description
                ]);

                foreach ($repayments as $repayment) {
                    AdditionalCommissionRepayment::create([
                        'commission_id' => $commission->id,
                        'date' => $repayment->date,
                        'amount' => $repayment->amount
                    ]);
                }
            }

            $realEstateCommissions = AdvanceRealEstateCommission::all();

            foreach ($realEstateCommissions as $realEstateCommission) {
                $repayments = $realEstateCommission->commissionRepayments;

                $commission = AdditionalCommission::create([
                    'recipient_id' => $realEstateCommission->recipient_id,
                    'date' => $realEstateCommission->date,
                    'amount' => $realEstateCommission->amount,
                    'paid_on' => $realEstateCommission->paid_on,
                    'type_id' => $realEstateCommission->advance_commission_type_id,
                    'repayable' => $realEstateCommission->repayable,
                    'description' => $realEstateCommission->description,
                    'holding_id' => $realEstateCommission->holding_id
                ]);

                foreach ($repayments as $repayment) {
                    AdditionalCommissionRepayment::create([
                        'commission_id' => $commission->id,
                        'date' => $repayment->date,
                        'amount' => $repayment->amount
                    ]);
                }
            }
        });
    }
}
