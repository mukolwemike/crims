<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use Crims\Commission\OverrideManager;
use Cytonn\Core\DataStructures\Carbon;
use Illuminate\Console\Command;

class UpdateCommissionOverrides extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:update_commission_overrides {start?} {end?} {recipient_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the commission overrides table appropriately';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $startTime = microtime(true);

        $starDate = $this->argument('start') ? Carbon::parse($this->argument('start')) : null;

        $endDate = $this->argument('end') ? Carbon::parse($this->argument('end')) : null;

        $recipient = $this->argument('recipient_id') ?
            CommissionRecepient::find($this->argument('recipient_id')) : null;

        $manager = new OverrideManager($starDate, $endDate, $recipient);

        $manager->setLiveMode(false);

        $manager->setUpdateSchedules(true);

        $manager->awardOverrides();

        $manager->sendVerificationReport();

        dd(microtime(true) - $startTime);
    }
}
