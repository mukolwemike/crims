<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CommissionClawbackSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:commission_clawbacks_summary {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a report of the commission clawbacks for the period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $commissions = $this->getReport($start, $end);

        $fileName = 'Commission Clawback Summary';

        ExcelWork::generateAndStoreViewExcel(
            $commissions,
            $fileName,
            'exports.commission.commission_clawback_summary'
        );

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text(
                "Please find attached the commission clawbacks summary for the period from " .
                $start->toDateString() . ' to ' . $end->toDateString()
            )
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getReport(Carbon $start, Carbon $end)
    {
        $startDate = $start->copy();

        $endDate = $startDate->copy()->endOfMonth();

        $endDate = ($endDate >= $end) ? $end->copy() : $endDate;

        $reportArray = array();

        while ($startDate <= $end) {
            $reportArray[$startDate->format('Y-F')] =
                $this->getMonthReport($startDate->copy(), $endDate->copy());

            $startDate = $startDate->addMonthNoOverflow()->startOfMonth();

            $endDate = $startDate->copy()->endOfMonth();

            $endDate = ($endDate >= $end) ? $end->copy() : $endDate;
        }

        return $reportArray;
    }

    private function getMonthReport(Carbon $startDate, Carbon $endDate)
    {
        $recipients = CommissionRecepient::active()->get();

        $dataArray = array();

        foreach ($recipients as $recipient) {
            $totalCommission = $totalOverride = $totalClawback = 0;

            $investmentArray = $this->getInvestmentSummary($recipient, $startDate, $endDate);
            $totalCommission += $investmentArray['data']['total']['commission'];
            $totalClawback += $investmentArray['data']['total']['clawback'];
            $totalOverride += $investmentArray['data']['total']['override'];
            $recipientArray['investments'] = $investmentArray;

            $realEstateArray = $this->getRealEstateSummary($recipient, $startDate, $endDate);
            $totalCommission += $realEstateArray['commission'];
            $totalClawback += $realEstateArray['clawback'];
            $totalOverride += $realEstateArray['override'];
            $recipientArray['real_estate'] = $realEstateArray;

            $sharesArray = $this->getSharesSummary($recipient, $startDate, $endDate);
            $totalCommission += $sharesArray['commission'];
            $totalClawback += $sharesArray['clawback'];
            $totalOverride += $sharesArray['override'];
            $recipientArray['shares'] = $sharesArray;

            $unitizationArray = $this->getUnitizationSummary($recipient, $startDate, $endDate);
            $totalCommission += $unitizationArray['commission'];
            $totalClawback += $unitizationArray['clawback'];
            $totalOverride += $unitizationArray['override'];
            $recipientArray['unitization'] = $unitizationArray;

            $dataArray[] = [
                'fa' => $this->getRecipientDetails($recipient),
                'data' => $recipientArray,
                'total' => [
                    'commission' => $totalCommission,
                    'override' => $totalOverride,
                    'clawback' => $totalClawback,
                    'net_total' => $totalCommission + $totalOverride - $totalClawback
                ]
            ];
        }

        return $dataArray;
    }

    private function getRecipientDetails(CommissionRecepient $recepient)
    {
        return [
            'name' => $recepient->name,
            'rank' => $recepient->rank->name,
            'type' => $recepient->type->name,
            'employee_id' => $recepient->employee_id,
            'employee_number' => $recepient->employee_number
        ];
    }

    private function getInvestmentSummary(CommissionRecepient $recipient, Carbon $startDate, Carbon $endDate)
    {
        $investmentArray = array();

        $currencies = Currency::where('id', 1)->get();

        $totalCommission = $totalClawback = $totalOverride = 0;

        foreach ($currencies as $currency) {
            $investmentSummary = $recipient->calculator($currency, $startDate, $endDate)->summary();

            $commission = $investmentSummary->earned();
            $totalCommission += convert($commission, $currency, $endDate);

            $override = $investmentSummary->override();
            $totalOverride += convert($override, $currency, $endDate);

            $clawback = $investmentSummary->getClawBacks();
            $totalClawback += convert($clawback, $currency, $endDate);

            $investmentArray[$currency->code] = [
                'commission' => $commission,
                'clawback' => $clawback,
                'override' => $override,
                'net_total' => $commission + $override - $clawback
            ];
        }

        $investmentArray['total'] = [
            'commission' => $totalCommission,
            'clawback' => $totalClawback,
            'override' => $totalOverride,
            'net_total' => $totalCommission + $totalOverride - $totalClawback
        ];

        return [
            'data' => $investmentArray,
            'currencies' => $currencies
        ];
    }

    private function getRealEstateSummary(CommissionRecepient $recipient, Carbon $startDate, Carbon $endDate)
    {
        $reSummary = $recipient->reCommissionCalculator($startDate, $endDate)->summary();

        $commission = $reSummary->total();

        $override = $reSummary->override();

        return [
            'commission' => $commission,
            'override' => $override,
            'clawback' => 0,
            'net_total' => $commission + $override
        ];
    }

    private function getSharesSummary(CommissionRecepient $recipient, Carbon $startDate, Carbon $endDate)
    {
        $sharesSummary = $recipient->shareCommissionCalculator($startDate, $endDate)->summary();

        $commission = $sharesSummary->total();

        $override = $sharesSummary->override();

        return [
            'commission' => $commission,
            'override' => $override,
            'clawback' => 0,
            'net_total' => $commission + $override
        ];
    }

    private function getUnitizationSummary(CommissionRecepient $recipient, Carbon $startDate, Carbon $endDate)
    {
        $unitizationSummary = $recipient->unitizationCommissionCalculator($startDate, $endDate)->summary();

        $commission = $unitizationSummary->total();

        $override = $unitizationSummary->override();

        return [
            'commission' => $commission,
            'override' => $override,
            'clawback' => 0,
            'net_total' => $commission + $override
        ];
    }
}
