<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\User;
use Crims\Realestate\Commission\RecipientCalculator as RecipientCalculator_RE;
use Crims\Shares\Commission\RecipientCalculator as RecipientCalculator_Shares;
use Crims\Unitization\Commission\RecipientCalculator as RecipientCalculator_Unitization;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class FaCommissionBreakdownReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:fa_commission_breakdown {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Breakdown of the investment commission for an fa';

    private $currencies;

    private $investmentTypes;

    private $investmentDataArray;

    private $start;

    private $end;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $userId = $this->argument('user_id');
//        $start = Carbon::parse('2019-05-29');
//        $end = Carbon::parse('2019-06-28');
//        $userId = 61;

        $this->start = $start;

        $this->end = $end;

        $report = $this->getReport();

        $fileName = "FA Commissions " . $start->toDateString() . ' to ' . $end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the fa commissions export between '
                . $start->toDateString() . ' to ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }


    /**
     * @return array
     */
    private function getReport()
    {
        $recipients = CommissionRecepient::with('type')->whereIn('rank_id', [2,3,4,7,8,9])->active()->get();

        $this->currencies = $this->getCurrencies();

        $this->investmentTypes = $this->getInvestmentTypes();

        $this->investmentDataArray = $this->getInvestmentArray();

        $recipientArray = array();

        foreach ($recipients as $recipient) {
            $recipientArrayData = [
                'Employee ID' => $recipient->employee_id,
                'Employee Number' => $recipient->employee_number,
                'Name' => $recipient->name,
                'Active' => BooleanPresenter::presentYesNo($recipient->active),
                'Account Number' => $recipient->account_number,
                'KRA Pin' => $recipient->kra_pin,
                'Bank Code' => $recipient->bank_code,
                'Branch Code' => $recipient->branch_code,
            ];

            $recipientArrayData = array_merge($recipientArrayData, $this->getInvestmentData($recipient));

            $recipientArrayData = array_merge($recipientArrayData, $this->getRealEstateData($recipient));

            $recipientArrayData = array_merge($recipientArrayData, $this->getSharesData($recipient));

            $recipientArrayData = array_merge($recipientArrayData, $this->getUnitFundData($recipient));

            $recipientArrayData['Overrall_Total'] = $recipientArrayData['SP_Total'] + $recipientArrayData['RE_Total']
                + $recipientArrayData['OTC_Total'] + $recipientArrayData['UTF_Total'];

            $recipientArray[$recipient->type->name][] = $recipientArrayData;
        }

        return $recipientArray;
    }

    /**
     * @param CommissionRecepient $recipient
     * @return mixed
     */
    private function getInvestmentData(CommissionRecepient $recipient)
    {
        $investmentArray = $this->investmentDataArray;

        foreach ($this->currencies as $currency) {
            foreach ($this->investmentTypes as $investmentType) {
                $calculator = $recipient->calculator($currency, $this->start->copy(), $this->end->copy());

                $calculator->filterForInvestmentType($investmentType);

                $summary = $calculator->summary();

                $amount = $summary->getCompliant() - $summary->getClawBacks();

                $amount = convert($amount, $currency, $this->end);

                $investmentArray[ucfirst($investmentType->name)] += $amount;

                $investmentArray['SP_Earned_Total'] += $amount;
            }

            $calculator = $recipient->calculator($currency, $this->start->copy(), $this->end->copy());

            $calculator->filterForInvestmentType($investmentType);

            $summary = $calculator->summary();

            $amount = $amount = convert($summary->override(), $currency, $this->end);

            $investmentArray['SP_Override'] += $amount;
        }

        $investmentArray['SP_Total'] = $investmentArray['SP_Earned_Total'] + $investmentArray['SP_Override'];

        return $investmentArray;
    }

    /**
     * @param CommissionRecepient $recepient
     * @return array
     */
    private function getRealEstateData(CommissionRecepient $recepient)
    {
        $calculator = new RecipientCalculator_RE($recepient, $this->start, $this->end);

        $summary = $calculator->summary();

        $earned = $summary->total();

        $override = $summary->override();

        return [
            'RE_Earned' => $earned,
            'RE_Override' => $override,
            'RE_Total' => $earned + $override
        ];
    }

    /**
     * @param CommissionRecepient $recepient
     * @return array
     */
    private function getSharesData(CommissionRecepient $recepient)
    {
        $calculator = new RecipientCalculator_Shares($recepient, $this->start, $this->end);

        $summary = $calculator->summary();

        $earned = $summary->total();

        $override = $summary->override();

        return [
            'OTC_Earned' => $earned,
            'OTC_Override' => $override,
            'OTC_Total' => $earned + $override
        ];
    }

    /**
     * @param CommissionRecepient $recepient
     * @return array
     */
    private function getUnitFundData(CommissionRecepient $recepient)
    {
        $calculator = new RecipientCalculator_Unitization($recepient, $this->start, $this->end);

        $summary = $calculator->summary();

        $earned = $summary->total();

        $override = $summary->override();

        return [
            'UTF_Earned' => $earned,
            'UTF_Override' => $override,
            'UTF_Total' => $earned + $override
        ];
    }

    /**
     * @return ClientInvestmentType[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getInvestmentTypes()
    {
        return ClientInvestmentType::get();
    }

    /**
     * @return Currency[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getCurrencies()
    {
        return Currency::whereHas('products', function ($q) {
            $q->active()->has('investments');
        })->get();
    }

    /**
     * @return array
     */
    private function getInvestmentArray()
    {
        $data = array();

        foreach ($this->investmentTypes as $investmentType) {
            $data[ucfirst($investmentType->name)] = 0;
        }

        $data['SP_Earned_Total'] = 0;

        $data['SP_Override'] = 0;

        return $data;
    }
}
