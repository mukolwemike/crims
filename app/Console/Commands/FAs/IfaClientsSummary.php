<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 8/3/18
 * Time: 11:34 AM
 */

namespace App\Console\Commands\FAs;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class IfaClientsSummary extends Command
{
    protected $signature = 'crims:ifas_client_summary {user_id}';

    protected $description = 'IFA clients summary';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $user = User::findOrFail($this->argument('user_id'));

        $recipient = CommissionRecepient::active()
            ->ofType('ifa')
            ->get();

        $recipientsData = $recipient->map(function ($recipient) {
            return $this->clientsDetails($recipient);
        })->collapse();

        $this->sendMail($user, $recipientsData);
    }

    public function sendMail(User $user, $recipientsData)
    {
        ExcelWork::generateAndExportSingleSheet($recipientsData, 'ClientSummary');

        Mailer::compose()
            ->to($user->email)
            ->bcc(config())
            ->subject('IFA Client Investments Value')
            ->text('Please find attached the ifas client summary')
            ->excel(['ClientSummary'])
            ->send();

        $path = storage_path().'/exports/' . 'ClientSummary' .'.xlsx';

        return \File::delete($path);
    }

    public function clientsDetails($recipient)
    {
        return $recipient->repo->clients()
            ->get()
            ->map(function ($client) use ($recipient) {
                return [
                    'FA' => $recipient->name,
                    'CLIENT' => ClientPresenter::presentFullNames($client->id),
                    'AMOUNT' => AmountPresenter::currency($client->repo->getTodayTotalInvestmentsValueForAllProducts())
                ];
            });
    }
}
