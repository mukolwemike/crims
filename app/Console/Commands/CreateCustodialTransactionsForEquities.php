<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ActiveStrategyDividend;
use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Illuminate\Console\Command;

class CreateCustodialTransactionsForEquities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cfsf:create-trans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::transaction(function () {
            $this->purchases();

            $this->sales();

            $this->dividends();
        });
    }

    protected function purchases()
    {
        EquityHolding::doesntHave('custodialTransaction')
            ->get()
            ->each(function (EquityHolding $holding) {
                $cost = $holding->number * $holding->cost;
                $additional = $cost * $holding->getTransactionCostRate()/100;

                $amount = $cost + $additional;

                $fund = $holding->unitFund;

                $trans = PortfolioTransaction::build(
                    $fund->custodialAccount,
                    'I',
                    $holding->date,
                    'Purchase of shares: '.$holding->security->name. ' Fund: '.$fund->name,
                    $amount,
                    new PortfolioTransactionApproval(),
                    $holding->security->investor
                )->credit();

                $holding->custodialTransaction()->associate($trans);
                $holding->save();
            });
    }

    protected function sales()
    {
        EquityShareSale::doesntHave('custodialTransaction')
            ->get()
            ->each(function (EquityShareSale $sale) {
                $amount = $sale->number * $sale->sale_price;
                $fund = $sale->unitFund;

                $trans = PortfolioTransaction::build(
                    $fund->custodialAccount,
                    'R',
                    $sale->date,
                    'Sale of shares: '.$sale->security->name. ' Fund: '.$fund->name,
                    $amount,
                    new PortfolioTransactionApproval(),
                    $sale->security->investor
                )->debit();

                $sale->custodialTransaction()->associate($trans);
                $sale->save();
            });
    }

    protected function dividends()
    {
        EquityDividend::doesntHave('custodialTransaction')
            ->get()
            ->each(function (EquityDividend $dividend) {
                $amount = $dividend->number * $dividend->dividend;
                $fund = $dividend->unitFund;


                $trans = PortfolioTransaction::build(
                    $fund->custodialAccount,
                    'R',
                    $dividend->date,
                    'Received cash dividend: '.$dividend->security->name. ' Fund: '.$fund->name,
                    $amount,
                    new PortfolioTransactionApproval(),
                    $dividend->security->investor
                )->debit();

                $dividend->custodialTransaction()->associate($trans);
                $dividend->save();
            });
    }
}
