<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Reporting\Report as ReportModel;
use App\Cytonn\Models\User;
use App\Cytonn\Reporting\Custom\Persistence\Build;
use App\Cytonn\Reporting\Custom\Query\Filter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Reporting\Custom\Report;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use App\Cytonn\Reporting\Custom\Query\Filter as CustFilter;

class CustomReportTesting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom-reports:test {report_id} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->testBuildingReport();
//        $this->testReporting();
    }

    public function testReporting()
    {
        \DB::connection()->enableQueryLog();

        $report = new Report(ClientInvestment::class);

        $grpd1 = CustFilter::where('id', '>', 101);
        $grpd2 = CustFilter::where('id', '<', 10000);
        $nested = CustFilter::where('id', '>', 100);
        $report->filter(Filter::where('amount', '>=', 1000000));
        $report->filter(CustFilter::where('amount', '<=', 1000000));
        $report->filter(CustFilter::scope('statement', Carbon::today()));
        $report->filter(CustFilter::whereHas('client', $nested));
        $report->filter(CustFilter::where([$grpd1, $grpd2]));


        $report->with(['client', 'withdrawals', 'topupsTo', 'topupsFrom', 'client.contact']);
        $report->run();

        $count = $report->count();

        dump('Report rows: '.$count);

        $rows = $report->arguments([
            'principal' => Carbon::today()
        ])
            ->labels([
                'client.contact.firstname' => 'First Name',
                'invested_date' => 'Value Date'
            ])
            ->format([
                'principal' => 'currency',
                'invested_date' => 'date'
            ])
            ->columns([
                'client_code',
                'principal',
                'client.contact.firstname',
                'invested_date'
            ])->export();

        $report->save();


        dump("1 row of report => ", $rows->first());

        $queries = \DB::getQueryLog();
        dump(collect($queries)->pluck('query')->all());
    }

    public function testBuildingReport()
    {
        \DB::connection()->enableQueryLog();

        $r = ReportModel::find($this->argument('report_id'));

        $user_id = $this->argument('user_id');

        $user = User::find($user_id);

        $b = new Build();

        $rep = $b->build($r);

        $rows = $rep->export()->toArray();

        $this->send($rows, $user);

//        dump("1 row of report => ", $rows->first());
//
//        $queries = \DB::getQueryLog();
//
//        dump(collect($queries)->pluck('query')->all());
    }

    public function send($data, User $user = null)
    {
        $fileName = "Custom Report";

        $data = json_decode(json_encode($data), true);

        ExcelWork::generateAndStoreSingleSheet($data, $fileName);

        $email = $user ? $user->email : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find the attached reportgenerated')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }
}
