<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use CytonnSB\Client;
use Illuminate\Console\Command;

class ServiceBusTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service-bus:test {arg?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (range(1, 10) as $item) {
            $this->testCommand();
            sleep(1);
        }
    }

    private function testCommand()
    {
        $client = new Client();

        $this->info(" Sent ".Carbon::now()->toDateTimeString());

        $client->command('servicebus_test', [], 'crims-admin');
        $response = $client->query('servicebus_test', [], 'crims-admin');
        $client->event('servicebus_test', [], ['crims-admin']);

        $this->info(" Received ".Carbon::now()->toDateTimeString());
        $this->warn($response);
    }
}
