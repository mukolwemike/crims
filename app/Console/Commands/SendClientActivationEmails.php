<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ClientUser;
use Cytonn\Authentication\Authy;
use Cytonn\Mailers\UserMailer;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SendClientActivationEmails extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'cytonn:send-client-activations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send activation emails to clients when they have been entered and assigned accounts';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        \Log::info('starting email check');

        $users = ClientUser::where(function ($cl) {
            $cl->where('emailed', 0)->orWhereNull('emailed');
        })
        ->whereNotNull('authy_id')
        ->whereNotNull('email')
        ->where(function ($q) {
            $q->whereHas('access', function ($q) {
                $q->where('active', 1);
            })->orHas('financialAdvisors');
        })->get();
        
        $users->each(function ($user) {
            try {
                $this->processUser($user);
            } catch (\Exception $e) {
                reportException($e);
            }
        });

        \Log::info($users->count(). 'clients processed');

        $this->addMissingAuthyIds();

        $this->info($users->count().' activation emails sent');
    }

    public function processUser(ClientUser $user)
    {
        $activationKey = $user->repo->generateActivationKey();

        $mailer = new UserMailer();
        $mailer->sendClientActivationLink($user, $activationKey);

        $user->emailed = true;
        $user->save();
    }

    private function addMissingAuthyIds()
    {
        ClientUser::whereNull('authy_id')->latest()->get()
            ->each(function ($user) {
                (new Authy())->registerUser($user);
            });
    }
}
