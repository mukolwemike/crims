<?php

namespace App\Console\Commands\Payments;

use App\Console\Commands\Payments\Partials\AbstractExpressWithdrawal;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Events\Investments\Actions\UnitFundSaleHasBeenMade;
use App\Events\Unitization\UnitFundSaleHasBeenPaid;
use App\Events\Unitization\UnitFundWithdrawalFailed;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Handlers\CreateUnitFundSale;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\BankDetails;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Unitization\Trading\Sell;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class ExpressMMFWithdrawalPayments extends AbstractExpressWithdrawal
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payments:express-mmf-withdrawal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Withdraw client UTF investments directly';

    protected $funds = ['CMMF', 'CHYF'];

    const UPPER_LIMIT = 70000;
    const LOWER_LIMIT = 10;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @throws CRIMSGeneralException
     * @throws \App\Exceptions\CrimsException
     */
    protected function executeInstruction($instruction)
    {
        $this->validate($instruction);

        $this->approveTransaction($instruction);

        $bankInstruction = $this->generateInstruction($instruction);

        $this->submitPayment($bankInstruction);

        event(new UnitFundSaleHasBeenPaid($instruction->fresh(), $bankInstruction));
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @throws CRIMSGeneralException
     */
    private function validate(UnitFundInvestmentInstruction $instruction)
    {
        if ($instruction->type->slug !== 'sale') {
            throw new \InvalidArgumentException("Only sales can be processed");
        }

        if (Carbon::parse($instruction->date)->startOfDay()->isFuture()) {
            throw new CRIMSGeneralException("Cannot process future withdrawals");
        }
    }


    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @throws \App\Exceptions\CrimsException
     * @throws \Exception
     */
    private function approveTransaction(UnitFundInvestmentInstruction $instruction)
    {
        $input = ["instruction_id" => $instruction->id];

        $approval = CreateUnitFundSale::create(
            $instruction->client,
            null,
            getSystemUser(),
            $input
        );

        $instruction->update(['approval_id' => $approval->id]);

        $approval->systemExecute();
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @return \App\Cytonn\Models\BankInstruction
     */
    private function generateInstruction(UnitFundInvestmentInstruction $instruction)
    {
        $sale = UnitFundSale::where('approval_id', $instruction->approval_id)
            ->where('number', $instruction->number)
            ->first();

        $instr =  (new Sell($instruction->unitFund, $instruction->client, Carbon::today()))
            ->generateInstruction($sale)
            ->getInstruction();

        $instr->update(['identifier' => 'unit_fund_instruction_id_'.$instruction->id]);

        return $instr;
    }

    /**
     * @param BankInstruction $instruction
     * @throws CrimsException
     * @throws \Exception
     */
    private function submitPayment(BankInstruction $instruction)
    {
        $account = $this->getPayingAccount($instruction);

        $response = $this->submit($instruction, $account);

        if (count($response->failed) > 0) {
            $ex = "Payment submission failed... || " .@json_encode($response->failed);

            logRollbar($ex);

            throw new CrimsException(
                $ex
            );
        }
    }

    /**
     * @param BankInstruction $instruction
     * @return CustodialAccount | null
     */
    private function getPayingAccount(BankInstruction $instruction)
    {
        $trans = $instruction->custodialTransaction;

        /** @var UnitFund $fund */
        $fund = $trans->clientPayment->fund;

        if ($mpesa = $this->checkForPaymentToMpesa($instruction, $fund)) {
            return $mpesa;
        }

        return $fund->repo->getDisbursementAccount('auto');
    }

    private function checkForPaymentToMpesa(BankInstruction $instruction, UnitFund $fund)
    {
        $payment = $instruction->custodialTransaction->clientPayment;

        $bank = new BankDetails(null, $payment->client, $instruction->clientBankAccount);

        $swift = $bank->swiftCode();

        if (strtoupper($swift) !== 'MPESA') {
            return false;
        }

        return $fund->repo->getDisbursementAccount('mpesa', true);
    }

    /**
     * @return Collection
     */
    protected function instructions()
    {
        $instructions =  UnitFundInvestmentInstruction::ofType('sale')
            ->doesntHave('approval')
            ->whereHas('unitFund', function ($fund) {
                $fund->whereIn('short_name', $this->funds);
            })
            ->whereNotNull('user_id')
            ->whereNull('unit_fund_purchase_id')
            ->whereNull('unit_fund_sale_id')
            ->whereNull('unit_fund_transfer_id')
            ->whereNull('unit_fund_switch_id')
            ->whereNull('cancelled')
            ->where('created_at', '>', Carbon::now()->subHours(24))
            ->get();

        return $this->validateInstructions($instructions);
    }

    /**
     * @param Collection $instructions
     * @return Collection
     */
    protected function validateInstructions(Collection $instructions)
    {
        return $instructions->reject(function (UnitFundInvestmentInstruction $instruction) {
            return $this->checkValidInstruction($instruction);
        });
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @return bool
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function checkValidInstruction(UnitFundInvestmentInstruction $instruction)
    {
        $instructionAmount = $this->getUnitSaleAmount($instruction);

        if (! $instructionAmount) {
            logRollbar("Instruction {$instruction->id} has no amount: Amt: $instructionAmount");
            return true;
        }

        $withinLimit = $this->checkAmountWithinLimit($instructionAmount);

        if (!$withinLimit) {
            //above 70k
//            logRollbar("Instruction {$instruction->id} not within limit: Amt: $instructionAmount");

            return true;
        }

        //limit of one withdrawal no longer enforced
//        $eligibleForAutomaticWithdrawal = $instruction->client->repo->eligibleForAutomaticWithdrawal($instruction->unitFund);
//
//        if (! $eligibleForAutomaticWithdrawal) {
//            return true;
//        }

        $aboveAmountWithdrawable = $this->aboveAmountWithdrawable($instruction, $instructionAmount);

        if ($aboveAmountWithdrawable) {
//            logRollbar("Instr {$instruction->id} above withdrawable");
            $this->handleAboveAvailable($instruction, $instructionAmount);

            return true;
        }

        return false;
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @param $amount
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function handleAboveAvailable(UnitFundInvestmentInstruction $instruction, $amount)
    {
        $currency = $instruction->unitFund->currency->code;

        $requested = AmountPresenter::currency($amount);
        $withdrawable = (new Sell($instruction->unitFund, $instruction->client, Carbon::parse($instruction->date)))
            ->withdrawableUnits();

        $available = AmountPresenter::currency($instruction->unitFund->unitPrice() * $withdrawable);

        $reason = "Amount requested {$currency} {$requested} is more than amount available {$currency} {$available}.";

        $this->handleFailure($instruction, $reason);
    }

    private function handleFailure(UnitFundInvestmentInstruction $instruction, string $reason)
    {
        event(new UnitFundWithdrawalFailed($instruction, $reason));
    }

    /**
     * @param $amount
     * @return bool
     */
    private function checkAmountWithinLimit($amount)
    {
        return $amount >= self::LOWER_LIMIT && $amount <= self::UPPER_LIMIT;
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @return float|int|mixed
     */
    private function getUnitSaleAmount(UnitFundInvestmentInstruction $instruction)
    {
        $price = $instruction->unitFund->unitPrice(Carbon::today());

        $amt = $instruction->number * $price;

        return $amt ? $amt : $instruction->amount;
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @param $amount
     * @return bool
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function aboveAmountWithdrawable(UnitFundInvestmentInstruction $instruction, $amount)
    {
        return (new Sell($instruction->unitFund, $instruction->client, Carbon::parse($instruction->date)))
            ->aboveWithdrawalLimit($amount);
    }
}
