<?php

namespace App\Console\Commands\Payments\Partials;

/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 09/07/2019
 * Time: 12:58
 */

use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\System\Locks\LockException;
use Carbon\Carbon;
use Cytonn\Clients\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;

abstract class AbstractExpressWithdrawal extends Command
{
    use LocksTransactions;

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $instructions = $this->instructions();

        $this->info("Executing {$instructions->count()} instructions");

        if ($instructions->count()) {
            logRollbar("Executing {$instructions->count()} instructions");
        }

        $instructions->each(function ($instruction) {
            $this->executeWithinAtomicLock(
                function () use ($instruction) {
                    try {
                        \DB::transaction(function () use ($instruction) {
                            $this->executeInstruction($instruction);
                        });
                    } catch (\Throwable $exception) {
                        $this->error("An error occurred processing $instruction->id");
                        $this->error(get_class($exception).' : '.str_limit($exception->getMessage(), 100));

                      //  reportException($exception);

                        $this->reportFailed($exception, $instruction);
                    }
                },
                'express_withdrawal:'.get_class($instruction).':'.$instruction->id.rand(1, 1000),
                120,
                30
            );
        });

        return;
    }

    abstract protected function instructions();

    abstract protected function executeInstruction($instruction);

    /**
     * @param BankInstruction $instruction
     * @param CustodialAccount|null $account
     * @return mixed
     * @throws \Exception
     */
    protected function submit(BankInstruction $instruction, CustodialAccount $account = null)
    {
        $trans = $instruction->custodialTransaction;

        $account = $account ? $account : $trans->custodialAccount;

        //where separate account is defined for automated transactions, use that account
        if ($account->id !== $trans->custodial_account_id) {
            $trans->update(['custodial_account_id' => $account->id]);
        }

        $this->info("Submitting {$instruction->id}  instructions for account {$account->account_no}");

        $integration = new CytonnPaymentSystem();

        return $integration->submit(collect([$instruction]), $account);
    }

    /**
     * @param \Exception $exception
     * @param $data
     */
    protected function reportFailed(\Throwable $exception, $instruction)
    {
        if ($this->invalidException($exception)) {
            return;
        }

        if ($this->recentlyReported($instruction)) {
            return;
        }

        $client = $instruction->client;

        $data = \GuzzleHttp\json_encode([
            'id' => $instruction->id,
            'client_code' => $client->client_code,
            'name' => \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id),
            'number' => $instruction->number,
            'fund' => $instruction->unitFund->short_name
        ]);


        Mailer::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : Automatic Withdrawal Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
                <p>Data: <br>
                    <pre>" . $data . " </pre>
                </p> 
                <p>Code: <pre>{$exception->getFile()} : {$exception->getLine()} </pre></p>")
            ->queue();

        $instruction->update(['reported_at' => Carbon::now()]);
    }

    /**
     * @param \Throwable $e
     * @return bool
     */
    private function invalidException(\Throwable $e)
    {
        $exceptions = [
            ClientException::class,
            \PDOException::class,
            LockException::class,
            \App\Cytonn\Models\Behaviours\LockException::class,
            QueryException::class,
            \Error::class
        ];


        foreach ($exceptions as $exception) {
            if ($e instanceof $exception) {
                return true;
            }

            if (str_contains($e->getMessage(), 'withdrawal guard')) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $instruction
     * @return bool
     */
    private function recentlyReported($instruction)
    {
        if (is_null($instruction->reported_at)) {
            return false;
        }

        return Carbon::parse($instruction->reported_at)->gt(Carbon::now()->subHours(1));
    }
}
