<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/6/18
 * Time: 11:51 AM
 */

namespace App\Console\Commands\Payments;

use App\Console\Commands\Payments\Partials\AbstractExpressWithdrawal;
use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Investment\ClientInstruction\ExpressProcess\ClientInvestmentInstructionProcessor;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\CustodialAccount;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ExpressSPWithdrawalPayments extends AbstractExpressWithdrawal
{
    use LocksTransactions;

    protected $signature = 'payments:express-sp-withdraw';

    protected $description = 'Withdraw client SP investments directly';

    /**
     * @param ClientInvestmentInstruction $withdrawInstruction
     * @throws \Exception
     */
    protected function executeInstruction($withdrawInstruction)
    {
        $instruction = $this->process($withdrawInstruction);

        $instruction->update(['identifier' => 'investment_instruction_id_'.$withdrawInstruction->id]);

        $response = $this->submit($instruction);

        if (count($response->failed) > 0) {
            throw new CrimsException("Payment submission failed... || ".@$response->failed[0]->reasons);
        }

//        dump($response);

        $this->info("Investment instruction ".$instruction->id." processed successfully");
    }

    /**
     * @param $instruction
     * @return BankInstruction
     */
    private function process($instruction)
    {
        $processor = new ClientInvestmentInstructionProcessor($instruction);

        $bankInstruction =  $processor->process(false);

        $bankInstruction->client_account_id = $this->bankAccount($instruction);
        $bankInstruction->save();

        return $bankInstruction;
    }

    protected function instructions()
    {
//        return collect([]);

        return ClientInvestmentInstruction::whereNull('approval_id')
            ->ofType('withdraw')
            ->whereDoesntHave('approval')
            ->where(['inactive' => 0])
            ->where('created_at', '>', Carbon::now()->startOfDay()->subHour(1))
            ->whereHas('investment', function ($i) {
                $i->where('client_id', 1015);
            })
//            ->whereNotNull('user_id') // client originated
//            ->where('user_id', 1)->take(1) //mine -- remove for production
            ->get();
    }

    private function bankAccount(ClientInvestmentInstruction $instruction)
    {
        if ($instruction->client_account_id) {
            return $instruction->client_account_id;
        }

        if ($account =  $instruction->investment->bankAccount) {
            return $account->id;
        };

        $acc =  $instruction->investment->client
            ->bankAccounts()
            ->where('active', 1)
            ->orderBy('default', 'DESC')
            ->first();

        return $acc ? $acc->id : null;
    }


//    private function useBankInstructions()
//    {
//
//        $payments =  BankInstruction::whereHas('custodialTransaction', function ($trans) {
//            $trans->whereHas('custodialAccount', function ($acc) {
//                $acc->where('id', 1);
//            });
//        })->latest()
//            ->skip(120)
//            ->take(2)
//            ->get();
//
//        $payments->groupBy(function ($instr) {
//            return $instr->custodialTransaction->custodial_account_id;
//        })->each(function ($instructions, $acc_id) {
//            $account = CustodialAccount::find($acc_id);
//
//            $response = $this->submit($instructions, $account);
//
//            dump($response);
//        });
//    }
}
