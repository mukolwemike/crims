<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\PaymentSystem;

use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\BankDetails;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class AutomaticWithdrawalsSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:automatic_withdrawals_summary {start} {end} {user_id?} {emails?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export an automatic withdrawals summary report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $startDate = Carbon::parse($this->argument('start'));

        $endDate = Carbon::parse($this->argument('end'));

        $paymentTransactions = $this->getPaymentSystemTransactions($startDate, $endDate);

        $transactions = $this->getReport($startDate, $endDate, $paymentTransactions);

        $fileName = 'Automatic Withdrawal Transactions';

        ExcelWork::generateAndStoreMultiSheet($transactions, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        if ($this->argument('emails')) {
            $email = explode(',', $this->argument('emails'));
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the automatic withdrawal transactions for the period from '
                . $startDate->toDateTimeString() . ' to ' . $endDate->toDateTimeString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return mixed
     * @throws \Exception
     */
    private function getReport(Carbon $startDate, Carbon $endDate, $paymentTransactions)
    {
        $systemUser = getSystemUser();

        $sales = UnitFundSale::whereHas('approval', function ($q) use ($systemUser) {
            $q->where('approved', 1)->where('approved_by', $systemUser->id);
        })->whereHas('instruction', function ($q) use ($startDate, $endDate) {
            $q->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);
        })->get();

        $salesArray = array();

        foreach ($sales as $sale) {
            $client = $sale->client;

            $payment = $sale->payment;

            $instruction = $sale->instruction;

            $childPayment = $payment->child;

            $bank = new BankDetails(null, $payment->client, $instruction->bankAccount);


            if(!$client || !$childPayment){
                $this->info("Sale does not have a client or child payment {$sale->id}");

                continue;
            }

            $reference = $client->client_code . '-' . $childPayment->id;

            if (array_key_exists($reference, $paymentTransactions)) {
                $paymentTransaction = $paymentTransactions[$reference];
                $paymentStatus = $paymentTransaction['Status'];
                $paymentTransactions[$reference]['Matched'] = "Matched";
            } else {
                $paymentTransaction = null;
                $paymentStatus = '';
            }

            $salesArray[] = [
                'Date' => Carbon::parse($sale->instruction->created_at)->toDateTimeString(),
                'Sale Date' => Carbon::parse($sale->date)->toDateString(),
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Number Of Units' => $sale->number,
                'Price' => $sale->price,
                'Amount' => $payment->amount,
                'Unit Fund' => $sale->unitFund->short_name,
                'Account Name' => $bank->accountName(),
                'Account Number' => $bank->accountNumber(),
                'Bank Name' => $bank->bankName(),
                'Swift Code' => $bank->swiftCode(),
                'Clearing Code' => $bank->clearingCode(),
                'Approval Date' => Carbon::parse($sale->approval->approved_on)->toDateTimeString(),
                'Submission Status' => $childPayment->submit_status,
                'Submitted On' => $childPayment->submitted_on ? Carbon::parse($childPayment->submitted_on)->toDateTimeString()
                    : '',
                'Payment System Status' => $paymentStatus,
                'Payment Reference' => $reference
            ];
        }

        $salesArray = collect($salesArray)->sortBy('Date');

        return [
            'Withdrawal Transactions' => $salesArray,
            'Payment System Transactions' => $paymentTransactions
        ];
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     * @throws \Exception
     */
    private function getPaymentSystemTransactions(Carbon $startDate, Carbon $endDate)
    {
        $data = [
            'start' => $startDate->toDateString(),
            'end' => $endDate->toDateString()
        ];

        $integration = new CytonnPaymentSystem();

        $transactions = $integration->requestOutflowTransactions($data);

        $transactions = collect($transactions)->map(function ($transaction) {
            return (array)$transaction;
        });

        return $transactions->toArray();
    }
}
