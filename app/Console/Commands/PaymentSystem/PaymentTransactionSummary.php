<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\PaymentSystem;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class PaymentTransactionSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:payment_transaction_summary {start} {end} {user_id?} {emails?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a payment transaction summary report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $startDate = Carbon::parse($this->argument('start'));

        $endDate = Carbon::parse($this->argument('end'));

        $transactions = $this->getPaymentTransactions($startDate, $endDate);

        $fileName = 'Payment System Transactions';

        ExcelWork::generateAndStoreMultiSheet($transactions, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        if ($this->argument('emails')) {
            $email = explode(',', $this->argument('emails'));
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the payment system transactions for the period from '
                . $startDate->toDateTimeString() . ' to ' . $endDate->toDateTimeString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getPaymentTransactions(Carbon $startDate, Carbon $endDate)
    {
        return [
            'Inflow Transactions' => $this->getClientPayments($startDate, $endDate),
            'Reconciled Transactions' => $this->getReconciledTransactions($startDate, $endDate),
            'Suspense Transactions' => $this->getSuspenseTransactions($startDate, $endDate)
        ];
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return ClientPayment[]|\Illuminate\Database\Eloquent\Builder[]|
     * \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getClientPayments(Carbon $startDate, Carbon $endDate)
    {
        $clientPayments = ClientPayment::where('type_id', 1)
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->whereHas('approval', function ($q) {
                $q->where('approved_by', 1);
            })->get();

        return $clientPayments->map(function (ClientPayment $clientPayment) {
            $custodialTransaction = $clientPayment->custodialTransaction;

            $name = $clientPayment->client ?
                ClientPresenter::presentFullNames($clientPayment->client_id) :
                ($custodialTransaction ? $custodialTransaction->received_from : '');

            return [
                'Id' => $clientPayment->id,
                'Date' => Carbon::parse($clientPayment->date)->toDateString(),
                'Name' => $name,
                'Amount' => $clientPayment->amount == 0 ? $clientPayment->value : $clientPayment->amount,
                'Match Type' => $clientPayment->inflow_match_type,
                'Status' => $clientPayment->inflow_match_type === 'COMPLETE' ? 'BC Sent' : 'Inflow Added',
                'Valued' => $clientPayment->amount == 0 ? "No" : "Yes",
                'Custodial Account' => @$custodialTransaction->custodialAccount->account_name,
                'Payment For' => $clientPayment->present()->paymentFor,
                'Type' => $clientPayment->type->name,
                'Bank Reference No' => @$custodialTransaction->bank_reference_no,
                'Mpesa Confirmation Code' => @$custodialTransaction->mpesa_confirmation_code,
                'Bank Transaction Id' => @$custodialTransaction->bank_transaction_id,
                'Source' => strtoupper(@$custodialTransaction->source),
                'Description' => $clientPayment->description,
            ];
        });
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return mixed
     */
    private function getReconciledTransactions(Carbon $startDate, Carbon $endDate)
    {
        $transactions = CustodialTransaction::whereNotNull('bank_transaction_id')
            ->doesntHave('clientPayment')
            ->between($startDate, $endDate)
            ->get();

        return $transactions->map(function (CustodialTransaction $transaction) {
            return [
                'Id' => $transaction->id,
                'Date' => Carbon::parse($transaction->date)->toDateString(),
                'Entry Date' => $transaction->entry_date,
                'Type' => $transaction->typeName(),
                'Custodial Account' => $transaction->custodialAccount->account_name,
                'Amount' => AmountPresenter::currency($transaction->amount),
                'Description' => $transaction->description,
                'Received From' => $transaction->received_from,
                'Bank Reference No' => $transaction->bank_reference_no,
                'Mpesa Confirmation Code' => $transaction->mpesa_confirmation_code,
                'Bank Transaction Id' => $transaction->bank_transaction_id,
            ];
        });
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return mixed
     */
    private function getSuspenseTransactions(Carbon $startDate, Carbon $endDate)
    {
        $suspenseTransactions = SuspenseTransaction::where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)->matched(false)->get();

        return $suspenseTransactions->map(function (SuspenseTransaction $transaction) {
            return [
                'Id' => $transaction->id,
                'Date' => $transaction->date,
                'Custodial Account' => $transaction->custodialAccount->account_name,
                'Account Number' => $transaction->account_no,
                'Matched' => BooleanPresenter::presentYesNo($transaction->matched),
                'Amount' => $transaction->amount,
                'Reference' => $transaction->reference,
                'Bank Reference' => $transaction->bank_reference,
                'Transaction Id' => $transaction->transaction_id,
                'Outgoing Reference' => $transaction->outgoing_reference,
                'Transaction Type' => $transaction->source_identity,
            ];
        });
    }
}
