<?php

namespace App\Console\Commands\Data;

use App\Cytonn\Models\Unitization\Benchmark;
use App\Cytonn\Models\Unitization\BenchmarkValue;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class CentralBankDataUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:cbk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update key rates from CBK';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->saveTbills();

        $this->info("Saved rates from CBK");
    }


    protected function saveTbills()
    {
        $tenors = [91, 182, 364];

        collect($tenors)->each(function ($tenor) {
            $this->tBill($tenor);
        });
    }

    private function tBill($tenor)
    {
        $date = Carbon::today()->format('d/m/Y');
        $startOfWeek = Carbon::today()->subWeek()->format('d/m/Y');


        $response = $this->client()->post(
            '/wp-admin/admin-ajax.php',
            [
                'query' => [
                    'action' => 'get_wdtable',
                    'table_id' => 141
                ],
                'form_params' => [
                    'draw' => 1,
                    'columns' => [
                        [
                            'data' => "0",
                            'name' => 'date_r',
                            'searchable' => "true",
                            'orderable' => "true",
                            'search' => [
                                'value' => $startOfWeek . '~' . $date,
                                'regex' => "false"
                            ]
                        ],
                        [
                            'data' => "1",
                            'name' => 'Issue',
                            'searchable' => "true",
                            'orderable' => "true",
                            'search' => [
                                'value' => "",
                                'regex' => "false"
                            ]
                        ],
                        [
                            'data' => "2",
                            'name' => 'Tenor',
                            'searchable' => "true",
                            'orderable' => "true",
                            'search' => [
                                'value' => $tenor,
                                'regex' => false
                            ]
                        ],
                        [
                            'data' => "3",
                            'name' => 'MarketAverageRate',
                            'searchable' => "true",
                            'orderable' => "true",
                            'search' => [
                                'value' => "",
                                'regex' => false
                            ]
                        ]
                    ],
                    'order' => [
                        'column' => "0",
                        'dir' => 'desc'
                    ],
                    'start' => "0",
                    'length' => "10",
                    'search' => [
                        'value' => null,
                        'regex' => false
                    ],
                    'sRangeSeparator' => '~'
                ]
            ]
        );

        $raw = \GuzzleHttp\json_decode($response->getBody()->getContents());

        $results = $raw->data;

        if (!count($results)) {
            return;
        }

        $benchmark = Benchmark::where('name', 'T-bill-'.$tenor)->first();

        foreach ($results as $data) {
            $bdate = Carbon::createFromFormat('d/m/Y', $data[0]);
            $exists = BenchmarkValue::where('date', $bdate->copy()->toDateString())
                ->where('benchmark_id', $benchmark->id)
                ->exists();

            if(!$exists) {

                $benchmark->benchmarkValues()->create([
                    'date' => $bdate,
                    'value' => $data[3]
                ]);
            }
        }
    }

    protected function client()
    {
        return new Client([
            'base_uri' => 'https://www.centralbank.go.ke'
        ]);
    }
}
