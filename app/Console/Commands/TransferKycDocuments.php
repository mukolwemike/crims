<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use Illuminate\Console\Command;

class TransferKycDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:kycDocuments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'transfer kyc doucments to documents';

    /**
     * TransferKycDocuments constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $kycDocs = ClientUploadedKyc::where('document_id', null)->get();
        $kycDocs->each(
            function ($kycDoc) {
                $document = Document::create(
                    [
                        'type_id' => DocumentType::where('slug', 'kyc')->first()->id,
                        'filename' => $kycDoc->filename,
                        'client_id' => ($kycDoc->application->application)
                            ? $kycDoc->application->application->client_id : null,
                        'title' => $kycDoc->type->name
                    ]
                );

                $kycDoc->document_id = $document->id;
                $kycDoc->save();
            }
        );
    }
}
