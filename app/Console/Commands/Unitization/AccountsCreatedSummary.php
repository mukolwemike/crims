<?php
/**
 *
 * @author: Isaac Kuttoh <kuttohisaac@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class AccountsCreatedSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:accounts_created_summary {start} {end} {user_id?} {emails?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Unit Trust Fund accounts created summary report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        logRollbar('Unit Trust Funds Accounts Created report started - ' . Carbon::now()->toDateTimeString());

        $userId = $this->argument('user_id');

        $startDate = Carbon::parse($this->argument('start'));

        $endDate = Carbon::parse($this->argument('end'));

        $accounts = $this->getReport($startDate, $endDate);

        $fileName = 'Unit Trust Funds Client Accounts Created on ' . Carbon::parse($startDate)->toDateString();

        ExcelWork::generateAndStoreSingleSheet($accounts, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        if ($this->argument('emails')) {
            $email = explode(',', $this->argument('emails'));
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the Unit Trust Fund accounts created for the period from '
                . $startDate->toDateTimeString() . ' to ' . $endDate->toDateTimeString())
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        logRollbar('Unit Trust Funds Accounts Created report ended - ' . Carbon::now()->toDateTimeString());

        return \File::delete($path);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return mixed
     */
    private function getReport(Carbon $startDate, Carbon $endDate)
    {
        $clients = Client::where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)->whereHas('unitFundPurchases')
            ->get();

        $data = [];

        foreach ($clients as $client) {
            try {
                $clientInvestmentApplication = ClientInvestmentApplication::where('client_id', $client->id)
                    ->orderBy('created_at', 'ASC')->first();

                $clientFilledInvestmentApplication = ClientFilledInvestmentApplication::find($clientInvestmentApplication->form_id);

                $data[] = [
                    'Date' => Carbon::parse($client->created_at)->toDateTimeString(),
                    'Client Code' => $client->client_code,
                    'Name' => ClientPresenter::presentJointFullNames($client->id),
                    'Phone' => $client->contact->phone,
                    'Email' => $client->contact->email,
                    'Channel' => $clientFilledInvestmentApplication->channel->name,
                    'Unit Fund' => $clientInvestmentApplication->unitFund->short_name,
                    'Amount' => $clientInvestmentApplication->amount,
                ];
            } catch (\Exception $exception) {
            }
        }

        return $data;
    }
}
