<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/22/18
 * Time: 2:38 PM
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Unitization\UnitFundSaleRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LinkUnitTrustSalesToPurchases extends Command
{
    protected $signature = 'crims:link_sales_purchases {fund_id?}';

    protected $description = 'link unit trust sales to purchases';

    public function handle()
    {
        $failedArray = array();

        DB::transaction(function () use (&$failedArray) {
            UnitFundPurchase::query()->update([
                'sold' => null,
                'sale_date' => null
            ]);

            $sales = UnitFundSale::whereDoesntHave('purchaseSales')
                ->orderBy('date')
//            ->nonFees(true)
                ->get();

            $this->info(count($sales), 'Linking');

            $sales->each(function ($sale) use (&$failedArray) {

                $this->info($sale->id);

                $repo = new UnitFundSaleRepository($sale->fund, $sale);

                $data = $repo->linkToPurchase($sale->fund, $sale);

                if (! is_null($data)) {
                    $failedArray[] = $data;
                }
            });

            $this->info('Done');

            dd("Done", $failedArray);
        });

        dd("Done", $failedArray);
    }
}
