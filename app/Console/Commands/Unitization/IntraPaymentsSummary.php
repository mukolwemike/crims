<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFundTransfer;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class IntraPaymentsSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:intra_payments_summary {start} {end} {user_id?} {emails?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export an intra payments summary report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        logRollbar('Intra payments report started - ' . Carbon::now()->toDateTimeString());

        $userId = $this->argument('user_id');

        $startDate = Carbon::parse($this->argument('start'));

        $endDate = Carbon::parse($this->argument('end'));

        $transactions = $this->getReport($startDate, $endDate);

        $fileName = 'Intra Payments Transactions';

        ExcelWork::generateAndStoreSingleSheet($transactions, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        if ($this->argument('emails')) {
            $email = explode(',', $this->argument('emails'));
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the intra payments transactions for the period from '
                . $startDate->toDateTimeString() . ' to ' . $endDate->toDateTimeString())
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        logRollbar('Intra payments report ended - ' . Carbon::now()->toDateTimeString());

        return \File::delete($path);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return mixed
     */
    private function getReport(Carbon $startDate, Carbon $endDate)
    {
        $transfers = UnitFundTransfer::between($startDate, $endDate)->get();

        $data = [];

        foreach ($transfers as $transfer) {
            try {
                $purchaseValue = $transfer->purchase->number * $transfer->purchase->price;

                $saleValue = $transfer->sale->number * $transfer->sale->price;

                $senderBalance = $transfer->sender->calculateFund($transfer->fund, $endDate)->totalUnits();

                $recipientBalance = $transfer->recipient->calculateFund($transfer->fund, $endDate)->totalUnits();

                $data[] = [
                    'Date' => Carbon::parse($transfer->date)->toDateTimeString(),
                    'Sender' => ClientPresenter::presentFullNames($transfer->transferer_id),
                    'Sender Client Code' => $transfer->sender->client_code,
                    'Sender Account Balance' => AmountPresenter::currency($senderBalance),
                    'Recipient' => ClientPresenter::presentFullNames($transfer->transferee_id),
                    'Recipient Client Code' => $transfer->recipient->client_code,
                    'Recipient Account Balance' => AmountPresenter::currency($recipientBalance),
                    'Number Of Units' => AmountPresenter::currency($transfer->number),
                    'Purchase Value' => AmountPresenter::currency($purchaseValue),
                    'Sale Value' => AmountPresenter::currency($saleValue)
                ];

            } catch (\Exception $exception) {
            }
        }

        return $data;
    }
}
