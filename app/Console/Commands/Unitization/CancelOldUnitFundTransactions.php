<?php

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Illuminate\Console\Command;

class CancelOldUnitFundTransactions extends Command
{
    protected $signature = 'unitization:cancel_old_un_processed_transaction';

    protected $description = 'Cancel all transactions that are two weeks old and not processed';

    /**
     * CancelOldUnitFundTransactions constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws CrimsException
     */
    public function fire()
    {
        $this->cancelTransactions();

        return;
    }

    /**
     * @throws CrimsException
     */
    private function cancelTransactions()
    {
        $day = Carbon::now()->subDays(14);

        $instructions = UnitFundInvestmentInstruction::whereNull('approval_id')
            ->whereNull('unit_fund_purchase_id')
            ->whereNull('unit_fund_sale_id')
            ->whereNull('unit_fund_transfer_id')
            ->whereNull('unit_fund_switch_id')
            ->whereNull('cancelled')
            ->where('created_at', '<=', $day)
            ->get();

        foreach ($instructions as $instruction) {
            $this->processCancel($instruction);
        }
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @throws CrimsException
     */
    private function processCancel(UnitFundInvestmentInstruction $instruction)
    {
        $input['reason'] = 'The Transaction has been active for more than two weeks unprocessed.';
        $input['instruction_id'] = $instruction->id;

        $approval = ClientTransactionApproval::add([
            'client_id' => $instruction->client ? $instruction->client->id : null,
            'transaction_type' => 'cancel_unit_fund_instruction',
            'payload' => $input
        ]);

        $instruction->approval_id = $approval->id;

        $instruction->save();

        $approve = new Approval($approval);

        $approve->systemExecute();
    }
}
