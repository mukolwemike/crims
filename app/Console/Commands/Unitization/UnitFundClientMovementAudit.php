<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use App\Cytonn\Unitization\Trading\Client\Analytics;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class UnitFundClientMovementAudit extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:unit_fund_client_movement_audit {fund} {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report on client movement audit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->generateReport();
    }

    /*
     * Generate the report
     */
    private function generateReport()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $subStart = $start->copy()->subDay();

        $addEnd = $end->copy()->addDay();

        $unitFund = UnitFund::findOrFail($this->argument('fund'));

        $report = array();

        $report['Opening ' . $subStart->format('M d Y')] =
            $this->getBalances($unitFund, $subStart->toDateString());

        $report['Opening ' . $start->format('M d Y')] =
            $this->getBalances($unitFund, $start->toDateString());

        $report['Monthly Movement'] = $this->getMonthlyMovement($unitFund);

        $report['Closing ' . $end->format('M d Y')] = $this->getBalances($unitFund, $end->toDateString());

        $report['Closing ' . $addEnd->format('M d Y')] =
            $this->getBalances($unitFund, $addEnd->toDateString());

        $fileName = 'Unit Funds Movement Report';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        $userId = $this->argument('user_id');

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the unit funds movement report')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    private function getBalances(UnitFund $unitFund, $date)
    {
        $date = Carbon::parse($date);

        $analytics = new Analytics($unitFund, $date);

        $marketValue = $analytics->marketValue();

        $costValue = $analytics->costValue();

        return [
            [
                'Name' => 'Principal Balance (Cost Value)',
                'Date' => $date->toDateString(),
                'Value' => $costValue
            ],
            [
                'Name' => 'Market Value',
                'Date' => $date->toDateString(),
                'Value' => $marketValue
            ],
            [
                'Name' => 'Interest (Net)',
                'Date' => $date->toDateString(),
                'Value' => $marketValue - $costValue
            ]
        ];
    }

    private function getMonthlyMovement(UnitFund $unitFund)
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'))->endOfDay();

        $months = getDateRange($start, $end);

        $monthArray = array();

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month);

            $endMonth = $startOfMonth->copy()->endOfMonth();

            $newPurchases = UnitFundPurchase::betweenDates($startOfMonth, $endMonth)
                ->forUnitFund($unitFund)->get()
                ->sum(function ($purchase) {
                    return convert(
                        $purchase->number * $purchase->price,
                        $purchase->unitFund->currency,
                        Carbon::parse($purchase->date)
                    );
                });

            $purchases = UnitFundPurchase::activeBetweenDates($startOfMonth, $endMonth)
                ->forUnitFund($unitFund)->get();

            $active = $interestAccrued = $totalSales = 0;

            foreach ($purchases as $purchase) {
                $purchaseCurrency = $purchase->unitFund->currency;

                $active += convert(
                    $purchase->number * $purchase->price,
                    $purchaseCurrency,
                    Carbon::parse($endMonth)
                );

                $startCalc = $purchase->calculate($startOfMonth, false, $startOfMonth->copy()->subDay());
                $started = convert($startCalc->getPrepared()->gross_interest, $purchaseCurrency, $startOfMonth);

                $endCalc = $purchase->calculate($endMonth, false, $endMonth);
                $ended = convert($endCalc->getPrepared()->gross_interest, $purchaseCurrency, $endMonth);

                $interestAccrued += ($ended - $started);

                $sales = $purchase->purchaseSales()->betweenDates($startOfMonth, $endMonth)
                    ->get()->sum(function ($purchaseSale) use ($purchaseCurrency) {
                        $sale = $purchaseSale->sale;
                        return convert(
                            $sale->number * $sale->price,
                            $purchaseCurrency,
                            Carbon::parse($sale->date)
                        );
                    });

                $totalSales += $sales;
            }

            $monthArray[] = [
                'Month' => $startOfMonth->format('M, Y'),
                'New Unit Purchases' => $newPurchases,
                'Active' => $active,
                'Interest Accrued' => $interestAccrued,
                'Unit Sales' => $totalSales
            ];
        }

        return $monthArray;
    }
}
