<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 1/16/19
 * Time: 8:50 AM
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Calculate\MoneyMarketCalculator;
use Illuminate\Console\Command;

class InterestExpenseReport extends Command
{
    protected $signature = 'utf:interest-expense {start} {end} {user_id?} {fund_id?}';

    protected $description = 'Report for interest expense for money market funds';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return bool
     */
    public function handle()
    {
        $params = $this->arguments();

        $start = Carbon::parse($params['start']);

        $end = Carbon::parse($params['end']);

        $user = $this->argument('user_id') ? User::find($params['user_id']) : null;

        $funds = $this->argument('fund_id') ? UnitFund::where('id', $this->argument('fund_id'))->get()
            : UnitFund::whereHas('purchases', function ($q) use ($start, $end) {
                $q->whereBetween('date', [$start, $end]);
            })->get();

        foreach ($funds as $fund) {
            $category = $fund->category;

            if (is_null($category)) {
                continue;
            }

            $calc = $category->calculation->slug;

            switch ($calc) {
                case 'variable-unit-price':
//                    return $this->exportForEquity();
                    break;
                case 'daily-yield':
                    return $this->exportMoneyMarketFund($fund, $start, $end, $user);
                    break;
                default:
//                    throw new CRIMSGeneralException("Unit Fund Type not supported");
            }
        }
    }

    /**
     * @param UnitFund $fund
     * @param Carbon $start
     * @param Carbon $end
     * @param User $user
     * @return bool
     */
    public function exportMoneyMarketFund(UnitFund $fund, Carbon $start, Carbon $end, User $user)
    {
        $clients = Client::whereHas('unitFundPurchases', function ($q) use ($start, $end, $fund) {
            $q->where('unit_fund_id', $fund->id)->activeBetweenDates($start, $end);
        })->get();

        $clients = $clients->map(function (Client $client) use ($start, $end, $fund) {
            $out = new EmptyModel();

            $out->{'Name'} = ClientPresenter::presentJointFullNames($client->id);
            $out->{'Client code'} = $client->client_code;
            $out->{'Fund'} = $fund->name;
            $out->{'Pin'} = $client->pin_no;

            for ($date = $start->copy(); $date->lte($end); $date = $date->addMonthNoOverflow()) {
                $calculator = $client->calculateFund($fund, $date->copy()->endOfMonth(), $date->startOfMonth())
                    ->getPrepared();

                $out->{$date->format('M, Y') . ' Opening Balance'} = $calculator->opening_balance;
                $out->{$date->format('M, Y') . ' Closing Balance'} = $calculator->closing_balance;
                $out->{$date->format('M, Y') . ' Purchases'} = $calculator->purchases;
                $out->{$end->format('M, Y') . ' Sales'} = $calculator->sales;
                $out->{$date->format('M, Y') . ' Price'} = $calculator->price;
                $out->{$date->format('M, Y') . ' Gross Interest'} = $calculator->gross_interest;
                $out->{$date->format('M, Y') . ' Net Interest'} = $calculator->net_interest;
                $out->{$date->format('M, Y') . ' Withholding Tax'} = $calculator->withholding_tax;
            }

            return $out;
        });

//        $purchases = UnitFundPurchase::where('unit_fund_id', $fund->id)
//            ->activeBetweenDates($start, $end)
//            ->get();
//
//        $purchases = $purchases->map(function ($purchase) use ($start, $end) {
//
//            $calculator = new MoneyMarketCalculator(
//                $purchase->client,
//                $purchase->unitFund,
//                $end,
//                $start,
//                true,
//                $purchase
//            );
//
//            $calculator = $calculator->getPrepared();
//
//            $out = new EmptyModel();
//
//            $out->{'Name'} = ClientPresenter::presentJointFullNames($purchase->client_id);
//            $out->{'Client code'} = $purchase->client->client_code;
//            $out->{'Fund'} = $purchase->unitFund->name;
//            $out->{'Principal'} = $calculator->purchases;
//            $out->{'Value Date'} = $purchase->invested_date;
//            $out->{'Price'} = $calculator->price;
//            $out->{'Sold'} = BooleanPresenter::presentYesNo($purchase->sold);
//            $out->{'Sale Date'} = $purchase->sale_date;
//            $out->{$end->format('M, Y') . ' Net Interest'} = $calculator->net_interest;
//            $out->{$end->format('M, Y') . ' Sold'} = $calculator->sales;
//
//            return $out;
//        });

        $fname = $fund->short_name . ' Monthly  '. ucwords($fund->category->name).' Interest Expense';

        Excel::fromModel($fname, $clients)->store('xlsx');

        $this->send($fname, $start, $end, $user);

        return false;
    }

    /**
     * @throws CRIMSGeneralException
     */
    public function exportForEquity()
    {
        throw new CRIMSGeneralException("Equity Unit Fund Type not supported for export");
    }

    /**
     * @param $fileName
     * @param $start
     * @param $end
     * @param null $user
     */
    private function send($fileName, $start, $end, $user = null)
    {
        $email = $user ? $user->email : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Here is the interest expense report between ' . $start . ' and ' . $end)
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        \File::delete($path);
    }
}
