<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFundCommission;
use App\Cytonn\Models\Unitization\UnitFundCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundCommissionRate;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateCommissionSchedulesData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unitization:migrate-commission-schedule-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate commission schedule data';

    /**
     * Create a new command instance.
     * MigratePortfolioData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            $this->migrateCommissionSchedules();
        });

        dd("Done");
    }

    private function migrateCommissionSchedules()
    {
        $schedules = UnitFundCommissionPaymentSchedule::all();

        foreach ($schedules as $schedule) {
            $comm = UnitFundCommission::where('unit_fund_purchase_id', $schedule->unit_fund_purchase_id)
                ->first();

            if ($comm) {
                dd($comm, "Dup");
            }

            $recipient = $schedule->recipient;

            if ($recipient->zero_commission) {
                $rate = 0;
            } else {
                $rate = UnitFundCommissionRate::where(
                    'commission_recipient_type_id',
                    $schedule->recipient->recipient_type_id
                )
                    ->where('unit_fund_id', $schedule->purchase->unit_fund_id)
                    ->where('date', '<=', $schedule->purchase->date)
                    ->first();

                if (is_null($rate)) {
                    dd($schedule, "NR");
                }

                $rate = $rate->rate;
            }

            UnitFundCommission::create([
                'unit_fund_purchase_id' => $schedule->unit_fund_purchase_id,
                'commission_recipient_id' => $schedule->commission_recipient_id,
                'date' => $schedule->purchase->date,
                'rate' => $rate,
                'description' => 'Commission Payment'
            ]);
        }
    }
}
