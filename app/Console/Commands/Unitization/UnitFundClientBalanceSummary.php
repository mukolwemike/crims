<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class UnitFundClientBalanceSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:unit_fund_client_balance_summary {fund_id} {date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a report of the client balances for the specified unit fund';

    /**
     * Create a new command instance.
     * MigratePortfolioData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::parse($this->argument('date'));

        $fund = UnitFund::findOrFail($this->argument('fund_id'));

        $report = $this->getReport($fund, $date);

        $fileName = 'Unit Fund Client Balances - '. $date->toDateString();

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        $email = [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the unit fund client balances as at '
                . $date->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon $date
     * @return array
     */
    private function getReport(UnitFund $unitFund, Carbon $date)
    {
        $clients = Client::whereHas('unitFundPurchases', function ($purchase) use ($unitFund) {
            return $purchase->forUnitFund($unitFund);
        })->with(['unitFundSales'])->get();

        $clientArray = array();

        foreach ($clients as $client) {
            $sheetName = $client->unitFundSales->count() > 0 ? "Has Units Sold" : "No Sold Units";

            $firstPurchase = $client->unitFundPurchases()->orderBy('date')->first();

            $firstPurchase = $firstPurchase ? Carbon::parse($firstPurchase->date) : null;

            $oldCalculator = $client->calculateFund($unitFund, $date, Carbon::parse($firstPurchase));

            $newCalculator = $client->calculateFund($unitFund, $date, $firstPurchase, true, null, true);

            $clientData = [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Unit Fund' => $unitFund->name,
                'Total Units Bought' => $client->unitFundPurchases()->sum('number'),
                'Total Units Sold' => $client->unitFundSales()->sum('number'),
            ];

            $clientData['Old Calculated Purchase'] = $oldCalculator->totalPurchases();
            $clientData['New Calculated Purchase'] = $newCalculator->totalPurchases();
            $clientData['Diff Calculated Purchase'] = $clientData['New Calculated Purchase'] - $clientData['Old Calculated Purchase'];

            $clientData['Old Calculated Sales'] = $oldCalculator->totalSales();
            $clientData['New Calculated Sales'] = $newCalculator->totalSales();
            $clientData['Diff Calculated Sales'] = $clientData['New Calculated Sales'] - $clientData['Old Calculated Sales'];

            $clientData['Old Current Value'] = $oldCalculator->totalUnits();
            $clientData['New Current Value'] = $newCalculator->totalUnits();
            $clientData['Diff Current Value'] = $clientData['New Current Value'] - $clientData['Old Current Value'];

            $clientArray[$sheetName][] = $clientData;
        }

        return $clientArray;
    }
}
