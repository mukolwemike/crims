<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AwardUnitizationCommissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unitization:award_unpaid_commissions {bcp_id} {date?} {fund?} {recipient?} {trial?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Award any unpaid unitization commissions';

    /**
     * Create a new command instance.
     * MigratePortfolioData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            $bcpId = $this->argument('bcp_id');

            $recipientId = $this->argument('recipient');

            $recipient = $recipientId ? CommissionRecepient::find($recipientId) : null;

            $fundId = $this->argument('fund');

            $fund = $fundId ? UnitFund::find($fundId) : null;

            $trail = $this->argument('trial');

            $bcp = BulkCommissionPayment::findOrFail($bcpId);

            $startDate = Carbon::parse($bcp->start);

            $endDate = Carbon::parse($bcp->end);

            $awardDate = $this->argument('date');

            $awardDate = $awardDate ? Carbon::parse($awardDate) : $endDate;

            $purchases = UnitFundPurchase::whereHas('unitFundCommission', function ($q) use ($recipient) {
                if ($recipient) {
                    $q->where('commission_recipient_id', $recipient->id);
                }
            })->activeBetweenDates($startDate, $endDate);

            if ($fund) {
                $purchases->where('unit_fund_id', $fund->id);
            }

            $purchases = $purchases->get();

            $commissionArray = array();

            foreach ($purchases as $purchase) {
                if (! $this->checkUnitFundPaymentPeriod($purchase, $awardDate) && $trail != 1) {
                    continue;
                }

                $schedule = UnitFundCommissionSchedule::where('unit_fund_commission_id', $purchase->unitFundCommission->id)->exists();

                if ($purchase->unitFund->short_name == 'CHYF' && is_null($schedule))
                {
                    $awardDate = Carbon::parse($awardDate)->addMonths(3);
                }

                $calc = $purchase->commissionCalculator($awardDate);

                $commission = roundDown($calc->unpaidCommission(), 2);

                if ($commission < 0.5) {
                    continue;
                }

                $recipient = $purchase->unitFundCommission->recipient;

                $commissionArray[] = [
                    'ID' => $purchase->id,
                    'Client Name' => ClientPresenter::presentFullNames($purchase->client_id),
                    'Fund' => $purchase->unitFund->name,
                    'Purchase Date' => Carbon::parse($purchase->date)->toDateString(),
                    'Purchase Number' => $purchase->number,
                    'Purchase Price' => $purchase->price,
                    'Purchase Value' => $purchase->number * $purchase->price,
                    'FA' => $recipient->name,
                    'FA Type' => $recipient->type->name,
                    'Rate' => $purchase->unitFundCommission->rate,
                    'Awarded Amount' => $commission,
                    'Award Date' => $awardDate->toDateString(),
                    'Paid Commission' => $calc->paidCommissions(),
                    'Sold' => $purchase->sold,
                    'Sale Date' => $purchase->sale_date,
                ];

                if ($trail != 1) {
                    UnitFundCommissionSchedule::create([
                        'unit_fund_commission_id' => $purchase->unitFundCommission->id,
                        'date' => $awardDate,
                        'amount' => $commission,
                        'description' => $endDate->format('F Y') . ' Commission',
                    ]);
                }
            }

            $fileName = 'Unit Fund Commissions - ' . $endDate->toDateString();

            ExcelWork::generateAndStoreSingleSheet($commissionArray, $fileName);

            Mailer::compose()
                ->to(['operations@cytonn.com'])
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the unit fund commission report as at ' . $endDate->toDateString())
                ->excel([$fileName])
                ->send();
        });
    }

    /**
     * @param UnitFundPurchase $purchase
     * @param Carbon $date
     * @return bool
     */
    private function checkUnitFundPaymentPeriod(UnitFundPurchase $purchase, Carbon $date)
    {
        if ($purchase->unitFund->category->slug != "pension-fund") {
            return true;
        }

        return in_array($date->month, [3, 6, 9, 12]) ? true : false;
    }
}
