<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCommission;
use App\Cytonn\Models\Unitization\UnitFundCommissionRate;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateUnitFundCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:update_unit_fund_commission {fund} {start} {end}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the unit fund commission';

    /**
     * Create a new command instance.
     * MigratePortfolioData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            $fund = UnitFund::findOrFail($this->argument('fund'));

            $start = Carbon::parse($this->argument('start'));

            $end = Carbon::parse($this->argument('end'));

            $report = [
                'CMMF' => $this->updateNegativeCommission($fund, $start, $end),
            ];

            $fileName = 'Unit Fund Commission - ' . $end->toDateString();

            ExcelWork::generateAndStoreMultiSheet($report, $fileName);

            $email = [];

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the requested report')
                ->excel([$fileName])
                ->send();

            $path = storage_path() . '/exports/' . $fileName . '.xlsx';

            dd("Done");

            return \File::delete($path);
        });
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    private function updateCmmfCommission(UnitFund $unitFund, Carbon $start, Carbon $end)
    {
        $commissions = UnitFundCommission::whereHas('purchase', function ($q) use ($start, $end, $unitFund) {
            $q->betweenDates($start, $end)->where('unit_fund_id', $unitFund->id);
        })->get();

        $this->info(count($commissions));

        $commissionArray = array();

        $bulkEnd = Carbon::parse('2019-09-25');

        $secondEnd = Carbon::parse('2019-10-25');

        $thirdEnd = Carbon::parse('2019-11-25');

        foreach ($commissions as $key => $commission) {
            if ($key % 1000 == 0) {
                $this->info($key);
            }

            $oldRate =  $commission->rate;

//            if (in_array($commission->recipient->recipient_type_id, [1, 3])) {
//                $newRate = $oldRate / 0.75;
//            } else {
//                $newRate = $oldRate;
//            }

            $newRate = $this->determineRateForRecipient(
                $commission->recipient,
                $unitFund,
                Carbon::parse($commission->purchase->date)
            );

            $diff = $newRate != 0 ? $oldRate / $newRate : 0;

            $commission->update([
                'rate' => $newRate
            ]);

            $purchase = $commission->fresh()->purchase;

//            $calc = $purchase->commissionCalculator($bulkEnd);
//
//            $calc2 = $purchase->commissionCalculator($secondEnd);
//
//            $calc3 = $purchase->commissionCalculator($thirdEnd);

            $commissionArray[] = [
                'Id' => $commission->id,
                'Commission Date' => Carbon::parse($commission->date)->toDateString(),
                'Purchase Date' => Carbon::parse($purchase->date)->toDateString(),
                'Client Name' => ClientPresenter::presentFullNames($purchase->client_id),
                'FA Name' => $commission->recipient->name,
                'Fa Type' => $commission->recipient->repo->getRecipientType($purchase->date)->name,
                'Old Rate' => $oldRate,
                'New Rate' => $newRate,
                'Change Factor' => $diff,
                'Purchase Amount' => $purchase->number * $purchase->price,
                'Sold' => $purchase->sold == 1 ? "Yes" : "No",
                'Sale Date' => $purchase->sale_date ? Carbon::parse($purchase->sale_date)->toDateString() : '',
//                'Commission Paid As At September' => (float)$calc->paidCommissions(),
//                'Unpaid Commission As At September' => (float)$calc->unpaidCommission(),
//                'Commission Paid As At October' => (float)$calc2->paidCommissions(),
//                'Unpaid Commission As At October' => (float) $calc2->unpaidCommission(),
//                'Commission Paid As At November' => (float)$calc3->paidCommissions(),
//                'Unpaid Commission As At November' => (float) $calc3->unpaidCommission(),
            ];
        }

        return $commissionArray;
    }

    /**
     * @param UnitFund $unitFund
     * @return array
     */
    private function updatePensionCommission(UnitFund $unitFund)
    {
        $unitFund = UnitFund::findOrFail(32);

        $commissions = UnitFundCommission::whereHas('purchase', function ($q) use ($unitFund) {
            $q->where('unit_fund_id', $unitFund->id);
        })->get();

        $this->info(count($commissions));

        $commissionArray = array();

        foreach ($commissions as $commission) {
            $recipient = $commission->recipient;

            $rate = $this->determineRateForRecipient($recipient, $unitFund, Carbon::parse($commission->purchase->date));

            $oldRate = $commission->rate;

//            $commission->rate = $rate;
//
//            $commission->save();
//
//            $commission->schedules->each(function (UnitFundCommissionSchedule $schedule) {
//                $schedule->delete();
//            });

            $commissionArray[] = [
                'Commission Id' => $commission->id,
                'Recipient' => $recipient->name,
                'FA Type' => $commission->recipient->repo->getRecipientType($commission->purchase->date)->name,
                'Old Rate' => $oldRate,
                'New Rate' => $rate,
            ];
        }

        return $commissionArray;
    }

    /**
     * @param CommissionRecepient $recepient
     * @param UnitFund $unitFund
     * @param Carbon $date
     * @return int|mixed
     */
    private function determineRateForRecipient(CommissionRecepient $recepient, UnitFund $unitFund, Carbon $date)
    {
        if ($recepient->zero_commission) {
            return $this->rate = 0;
        }

        $date = is_null($date) ? Carbon::now() : $date;

        $portfolio = UnitFundPurchase::where('unit_fund_id', $unitFund->id)->before($date)
            ->whereHas('unitFundCommission', function ($q) use ($recepient) {
                $q->where('commission_recipient_id', $recepient->id);
            })->get()->sum(function ($purchase) {
                return $purchase->number * $purchase->price;
            });

        $type = $recepient->repo->getRecipientType($date);

        $rate = UnitFundCommissionRate::where('commission_recipient_type_id', $type->id)
            ->where('unit_fund_id', $unitFund->id)
            ->where('min_amount', '<=', $portfolio)
            ->where('date', '<=', $date)
            ->orderBy('date', 'DESC')
            ->orderBy('min_amount', 'DESC')
            ->first();

        return $rate->rate;
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    private function processReport(UnitFund $unitFund, Carbon $start, Carbon $end)
    {
        $commissionGroups = UnitFundCommission::whereHas('purchase', function ($q) use ($start, $end, $unitFund) {
            $q->betweenDates($start, $end)->where('unit_fund_id', $unitFund->id);
        })->get()->groupBy('commission_recipient_id');

        $commissionArray = array();

        $bulkStart = Carbon::parse('2019-08-26');
        $bulkEnd = Carbon::parse('2019-09-25');
        $currency = Currency::findOrFail(1);

        foreach ($commissionGroups as $commissionGroup) {
            $first = true;
            foreach ($commissionGroup as $commission) {
                $purchase = $commission->purchase;

                $calc = $purchase->commissionCalculator($bulkEnd);

                $recipient = $commission->recipient;

                $unpaidCommission = (float) $calc->unpaidCommission();

                $dataArray = [
                    'Id' => $commission->id,
                    'Commission Date' => Carbon::parse($commission->date)->toDateString(),
                    'Purchase Date' => Carbon::parse($purchase->date)->toDateString(),
                    'Client Name' => ClientPresenter::presentFullNames($purchase->client_id),
                    'FA Name' => $commission->recipient->name,
                    'Fa Type' => $commission->recipient->repo->getRecipientType($purchase->date)->name,
                    'Purchase Amount' => $purchase->number * $purchase->price,
                    'Sold' => $purchase->sold == 1 ? "Yes" : "No",
                    'Sale Date' => $purchase->sale_date ? Carbon::parse($purchase->sale_date)->toDateString() : '',
                    'Commission' => $unpaidCommission,
                ];

                if ($first) {
                    $investmentCalc = $recipient->calculator($currency, $bulkStart, $bulkEnd);
                    $reCalc = $recipient->reCommissionCalculator($bulkStart, $bulkEnd);
                    $cmmfCalc = $recipient->unitizationCommissionCalculator($bulkStart, $bulkEnd);

                    $dataArray['Inv Comm'] = $investmentCalc->summary()->finalCommission();
                    $dataArray['RE Comm'] = $reCalc->summary()->finalCommission();
                    $dataArray['UTF Comm'] = $cmmfCalc->summary()->finalCommission();
                } else {
                    $dataArray['Inv Comm'] = '';
                    $dataArray['RE Comm'] = '';
                    $dataArray['UTF Comm'] = '';
                }

                $commissionArray[] = $dataArray;
                $first = false;
            }
        }

        return $commissionArray;
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    private function commissionTotals(UnitFund $unitFund, Carbon $start, Carbon $end)
    {
        $commissionGroups = UnitFundCommission::whereHas('purchase', function ($q) use ($start, $end, $unitFund) {
            $q->betweenDates($start, $end)->where('unit_fund_id', $unitFund->id);
        })->get()->groupBy('commission_recipient_id');

        $commissionArray = array();

        $bulkStart = Carbon::parse('2019-08-26');
        $bulkEnd = Carbon::parse('2019-09-25');
        $currency = Currency::findOrFail(1);

        foreach ($commissionGroups as $commissionGroup) {
            $unpaidCommission = $commissionGroup->sum(function (UnitFundCommission $commission) use ($bulkEnd) {
                $purchase = $commission->purchase;

                if ($purchase->sold != 1) {
                    return 0;
                }

                $calc = $purchase->commissionCalculator($bulkEnd);

                $unpaidCommission = (float)$calc->unpaidCommission();

                return $unpaidCommission;
            });

            $recipient = $commissionGroup->first()->recipient;

            $dataArray = [
                'FA Name' => $recipient->name,
                'Fa Type' => $recipient->type->name,
                'Unpaid Commission' => $unpaidCommission,
            ];

            $investmentCalc = $recipient->calculator($currency, $bulkStart, $bulkEnd);
            $reCalc = $recipient->reCommissionCalculator($bulkStart, $bulkEnd);
            $cmmfCalc = $recipient->unitizationCommissionCalculator($bulkStart, $bulkEnd);

            $dataArray['Inv Comm'] = $investmentCalc->summary()->finalCommission();
            $dataArray['RE Comm'] = $reCalc->summary()->finalCommission();
            $dataArray['UTF Comm'] = $cmmfCalc->summary()->finalCommission();

            $commissionArray[] = $dataArray;
        }

        return $commissionArray;
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon $start
     * @param Carbon $end
     * @return array
     */
    private function updateNegativeCommission(UnitFund $unitFund, Carbon $start, Carbon $end)
    {
        $commissionGroups = UnitFundCommission::whereHas('purchase', function ($q) use ($start, $end, $unitFund) {
            $q->betweenDates($start, $end)->where('unit_fund_id', $unitFund->id);
        })->get()->groupBy('commission_recipient_id');

        $commissionArray = array();

        $bulkEnd = Carbon::parse('2019-09-25');

        foreach ($commissionGroups as $commissionGroup) {
            foreach ($commissionGroup as $commission) {
                $purchase = $commission->purchase;

                if ($purchase->sold != 1) {
                    continue;
                }

                $calc = $purchase->commissionCalculator($bulkEnd);

                $unpaidCommission = (float) $calc->unpaidCommission();

                $record = round($unpaidCommission, 2) < 0 ? true : false;

                if (!$record) {
                    continue;
                }

                UnitFundCommissionSchedule::create([
                    'unit_fund_commission_id' => $purchase->unitFundCommission->id,
                    'date' => $bulkEnd,
                    'amount' => round($unpaidCommission, 2),
                    'description' => $bulkEnd->format('F Y') . ' Commission Clawback',
                    'automatic' => 2
                ]);

                $commissionArray[] = [
                    'Id' => $commission->id,
                    'Purchase Id' => $purchase->id,
                    'Commission Date' => Carbon::parse($commission->date)->toDateString(),
                    'Purchase Date' => Carbon::parse($purchase->date)->toDateString(),
                    'Client Name' => ClientPresenter::presentFullNames($purchase->client_id),
                    'FA Name' => $commission->recipient->name,
                    'Fa Type' => $commission->recipient->repo->getRecipientType($purchase->date)->name,
                    'Purchase Amount' => $purchase->number * $purchase->price,
                    'Sold' => $purchase->sold == 1 ? "Yes" : "No",
                    'Sale Date' => $purchase->sale_date ? Carbon::parse($purchase->sale_date)->toDateString() : '',
                    'Commission' => $unpaidCommission,
                    'Record' => $record ? "Yes" : "No"
                ];
                ;
            }
        }

        return $commissionArray;
    }
}
