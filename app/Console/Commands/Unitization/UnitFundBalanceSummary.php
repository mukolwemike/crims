<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Unitization\Reports\UnitFundBalancesSummary;
use Illuminate\Console\Command;

class UnitFundBalanceSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:unit_fund_balances {unit_fund} {date} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generete a report for the balances for a given unitfund';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unitFund = UnitFund::findOrFail($this->argument('unit_fund'));

        $date = Carbon::parse($this->argument('date'));

        $userId = $this->argument('user_id');

        $user = $userId ? User::findOrFail($userId) : null;

        $test = new UnitFundBalancesSummary($unitFund, $date, null, $user);

        $test->generate();
    }
}
