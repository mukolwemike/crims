<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class UnitFundPurchasesReport extends Command
{
    /**
     * The console command name
     *
     * @var string
     */
    protected $signature = 'crims:unit_fund_purchases_report {start} {end} {unit_fund?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the unit fund purchases for the specified period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $unitFundId = $this->argument('unit_fund');

        $unitFund = $unitFundId ? UnitFund::find($unitFundId) : null;

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $purchases = $this->getPurchases($start, $end, $unitFund);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'UnitFundPurchases - '. $start->toDateString() . '-To-' . $end->toDateString();

        ExcelWork::generateAndStoreSingleSheet($purchases, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the unit fund purchases report for the period between '
                . $start->toDateString() . ' to ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the client unit fund purchases
     */
    public function getPurchases(Carbon $start, Carbon $end, UnitFund $unitFund = null)
    {
        $purchases = UnitFundPurchase::whereBetween('date', [ $start->startOfDay(), $end->endOfDay()]);

        if ($unitFund) {
            $purchases = $purchases->where('unit_fund_id', $unitFund->id);
        }

        $purchases = $purchases->get();

        return $purchases->map(function (UnitFundPurchase $purchase) {

            $recipient = $purchase->getRecipient();

            $client = $purchase->client;

            $position = $recipient ? $recipient->repo->getRecipientUserPositionByDate(Carbon::parse($purchase->date))
                : null;

            return [
                'Purchase Date' => $purchase->date,
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Client Email' => $client->contact->email,
                'Units Purchased' => $purchase->number,
                'Purchase Price' => $purchase->price,
                'Purchase Value' => (float) $purchase->number * $purchase->price,
                'Fund' => $purchase->unitFund->name,
                'FA' => $recipient ? $recipient->name : null,
                'FA Type' => $recipient ? @$recipient->repo->getRecipientType(Carbon::parse($purchase->date))->name : null,
                'FA Branch' => $position ? $position->present()->getDepartmentBranch : ($recipient ?
                    $recipient->present()->getBranch : null),
                'FA Department Unit' => $position ? $position->present()->getDepartmentUnit : ($recipient ?
                    $recipient->present()->getDepartmentUnit : null),
                'FA Status' => $recipient ? $recipient->present()->getActive : null,
            ];
        });
    }
}
