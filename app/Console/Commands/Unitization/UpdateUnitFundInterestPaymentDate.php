<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFundInterestSchedule;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateUnitFundInterestPaymentDate extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:update_unit_fund_interest_date {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the unit fund clients interest payment dates at the end of the month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        DB::transaction(function () {
            $date = $this->argument('date');

            $date = $date ? Carbon::parse($date) : Carbon::now()->startOfMonth();

            $report = $this->getSchedules($date);

            $fileName = 'Updated UnitFund Interest Dates - '. $date->toDateString();

            ExcelWork::generateAndStoreSingleSheet($report, $fileName);

            $email = [];

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the unit fund interest payment dates that have been updated as at '
                . $date->toDateString())
                ->excel([$fileName])
                ->send();

            $path = storage_path().'/exports/' . $fileName .'.xlsx';

            return \File::delete($path);
        });
    }

    /**
     * @param Carbon $date
     * @return array
     */
    private function getSchedules(Carbon $date)
    {
        $schedules = UnitFundInterestSchedule::where('next_payment_date', '<=', $date)
            ->get();

        $scheduleArray = array();

        foreach ($schedules as $schedule) {
            $oldDate = $schedule->next_payment_date;

            $schedule = $this->updateNextPaymentDate($schedule, $date);

            $client = $schedule->client;

            $unitFund = $schedule->unitFund;

            $scheduleArray[] = [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Unit Fund' => $unitFund->name,
                'Old Payment Date' => $oldDate,
                'New Payment Date' => $schedule->next_payment_date,
                'Interest Start Date' => $schedule->interest_payment_start_date,
                'Interval' => $schedule->interest_payment_interval
            ];
        }

        return $scheduleArray;
    }

    /**
     * @param UnitFundInterestSchedule $schedule
     * @param Carbon $date
     * @return UnitFundInterestSchedule|null
     */
    private function updateNextPaymentDate(UnitFundInterestSchedule $schedule, Carbon $date)
    {
        $currentDate = $schedule->next_payment_date ? Carbon::parse($schedule->next_payment_date) : Carbon::now();

        $client = $schedule->client;

        $unitFund = $schedule->unitFund;

        while ($currentDate <= $date) {
            $client->unitFundClientRepo->updateNextInterestPaymentDate($unitFund);

            $schedule = $schedule->fresh();

            $currentDate = Carbon::parse($schedule->next_payment_date);
        }

        return $schedule;
    }
}
