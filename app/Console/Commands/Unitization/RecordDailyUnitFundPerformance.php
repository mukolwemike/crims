<?php

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeeParameter;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundPerformance;
use App\Cytonn\Models\Unitization\UnitFundPriceTrail;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Unitization\Trading\Performance;
use Cytonn\Unitization\Trading\Trade;
use Illuminate\Console\Command;

class RecordDailyUnitFundPerformance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unitization:record-daily-performance {--date=} {--initial=} {--fund=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Record daily fund performance and calculate and set new unit price.';

    /**
     * Create a new command instance.
     * RecordDailyUnitFundPerformance constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Initializing...');

        return;

        \DB::transaction(function () {
            $d = $this->option('date');

            $date = $d ? Carbon::parse($d) : Carbon::today();

            $this->info("Calculating funds as at ".$date->toDateString());

            $fund = $this->option('fund');

            $initial = $this->option('initial');

            if ($fund) {
                return $this->calc(UnitFund::find($fund), $date, $initial);
            }

            UnitFund::all()->each(function (UnitFund $fund) use ($date) {
                $this->calc($fund, $date);
            });
        });

        return;
    }

    /**
     * @param UnitFund $fund
     * @param Carbon $date
     * @param bool $initial
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function calc(UnitFund $fund, Carbon $date, $initial = false)
    {
        $this->info("Calculating $fund->name for {$date->toDateString()}...");

        $previous = $fund->previousPerformance($date->copy());

        if (!($previous || $initial)) {
            $this->error("Previous performance for $fund->name is missing");

            reportException(new \LogicException("Previous performance for $fund->name is missing"));

            return;
        }

        $dontCheck = !$initial;

        $this->calculateFund($fund, $date, $dontCheck);

        $this->info("Done calculating $fund->name ...");
    }

    /**
     * @param UnitFund $fund
     * @param Carbon $date
     * @param $dontCheck
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function calculateFund(UnitFund $fund, Carbon $date, $dontCheck)
    {
        $fund->repo->recordPerformance($date, $dontCheck);
    }
}
