<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PerformanceAttributionSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:performance_attribution_summary {start} {end} {--user_id=} {--fm=} {--fund=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generete a report for the performance attribution report for a fund manager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fundManager = FundManager::findOrFail($this->option('fm'));

        $fund = UnitFund::find($this->option('fund'));

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $userId = $this->option('user_id');

        $user = $userId ? User::findOrFail($userId) : null;

        $report = new \Cytonn\Unitization\Reports\PerformanceAttributionSummary(
            $start,
            $end,
            $fundManager,
            $fund,
            $user
        );

        $report->generate();
    }
}
