<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Tax\WithholdingTaxRepository;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateUnitSalesWitholdingTax extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:update_unit_sales_wht {fund?} {start?} {end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the unit fund sales wht';

    /**
     * Create a new command instance.
     * MigratePortfolioData constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            $fund = $this->argument('fund');

            $fund = $fund ? UnitFund::findOrFail($fund) : null;

            $start = $this->argument('start');

            $start = $start ? Carbon::parse($start) : null;

            $end = $this->argument('end');

            $end = $end ? Carbon::parse($end) : Carbon::now();

            $report = $this->processUnitSales($end, $start, $fund);

            $fileName = 'Unit Fund WHT - ' . $end->toDateString();

            ExcelWork::generateAndStoreSingleSheet($report, $fileName);

            $email = [];

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the requested report')
                ->excel([$fileName])
                ->send();

            $path = storage_path() . '/exports/' . $fileName . '.xlsx';

            return \File::delete($path);
        });
    }

    /**
     * @param Carbon $end
     * @param Carbon|null $start
     * @param UnitFund|null $fund
     * @return array
     */
    private function processUnitSales(Carbon $end, Carbon $start = null, UnitFund $fund = null)
    {
        $sales = UnitFundSale::nonFees()->where('date', '<=', $end);

        if ($fund) {
            $sales->where('unit_fund_id', $fund->id);
        }

        if ($start) {
            $sales->where('date', '>=', $start);
        }

        $salesGroups = $sales->orderBy('date')->get()->groupBy('client_id');

        $salesArray = array();

        foreach ($salesGroups as $sales) {
            foreach ($sales as $sale) {
                $tax = (new WithholdingTaxRepository())->recordForUnitSale($sale);

                $key = $sale->unit_fund_id . '_' . $sale->client_id . '_' . Carbon::parse($sale->date)->toDateString();

                if (array_key_exists($key, $salesArray)) {
                    continue;
                }

                $salesArray[$key] = [
                    'Id' => $sale->id,
                    'Client Name' => ClientPresenter::presentFullNames($sale->client_id),
                    'Fund' => $sale->unitFund->name,
                    'Date' => Carbon::parse($sale->date)->toDateString(),
                    'Number' => $sale->number,
                    'Price' => $sale->price,
                    'Amount' => $sale->number * $sale->price,
                    'Tax Date' => $tax ? Carbon::parse($tax->date)->toDateString() : '',
                    'WHT' => $tax ? $tax->amount : '',
                    'Gross Interest' => $tax ? $tax->gross_interest_amount : '',
                    'Net Interest' => $tax ? $tax->net_interest_amount : '',
                    'Type' => $tax ? $tax->type : '',
                    'Description' => $tax ? $tax->description : ''
                ];
            }
        }

        return $salesArray;
    }
}
