<?php

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Jobs\USSD\SendMessages;
use Carbon\Carbon;
use Cytonn\Clients\ClientRepository;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class IncompleteApplicationsReminder extends Command
{
    protected $signature = 'crims:incomplete-applications-notification';

    protected $description = 'Sends a reminder to users with incomplete application from ussd/web/mobile platforms';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $startOfDay = Carbon::now()->subDays(14)->startOfDay();

        $endOfDay = Carbon::now()->subDays(14)->endOfDay();

        $clients = (new ClientRepository())->getDigitalClientsWithNoInvestments()
            ->where(function ($q) use ($startOfDay, $endOfDay) {
                $q->where(function ($q) use ($startOfDay, $endOfDay) {
                    $q->whereNull('incomplete_application_reminder')
                        ->whereBetween('created_at', [$startOfDay, $endOfDay]);
                })->orWhere(function ($q) use ($startOfDay, $endOfDay) {
                    $q->whereNotNull('incomplete_application_reminder')
                        ->whereBetween('incomplete_application_reminder', [$startOfDay, $endOfDay]);
                });
            })
            ->get();

        if ($clients->count() > 0) {
            foreach ($clients as $client) {
                $client->repo->sendNotification();
            }
        }
        return;
    }
}
