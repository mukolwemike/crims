<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Unitization;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Unitization\Reports\UnitFundTransactionsSummary;
use Illuminate\Console\Command;

class UnitFundTransactionSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:unit_fund_transactions {unit_fund} {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generete a report for the transactions for a given unitfund';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unitFund = UnitFund::findOrFail($this->argument('unit_fund'));

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $userId = $this->argument('user_id');

        $user = $userId ? User::findOrFail($userId) : null;

        $test = new UnitFundTransactionsSummary($unitFund, $start, $end, null, $user);

        $test->generate();
    }
}
