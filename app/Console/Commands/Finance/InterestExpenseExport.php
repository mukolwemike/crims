<?php
/**
 * Created by PhpStorm.
 * User: eogoma
 * Date: 1/23/19
 * Time: 10:27 AM
 */

namespace App\Console\Commands\Finance;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;
use Illuminate\Console\Command;
use Cytonn\Mailers\ExcelMailer;

class InterestExpenseExport extends Command
{
    use ExcelMailer;

    protected $signature = 'sap:interest-expense {user_id} {start} {end}';

    protected $description = 'Export interest expense for SAP integrations';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $input = $this->arguments();

        $user = isset($input['user_id']) ? User::findOrFail($input['user_id']) : null;

        $endDate = isset($input['end']) ? Carbon::parse($input['end']) : Carbon::today();

        $startDate = isset($input['start']) ? Carbon::parse($input['start']) : $endDate->copy()->subMonth();
        
        $this->export($startDate, $endDate, $user);

        $this->info('Done');
    }

    public function export(Carbon $startDate, Carbon $endDate, User $user = null)
    {
        $filename = 'Interest Expense Export';

        $products = Product::active(true)
            ->whereHas('investments', function ($investments) use ($startDate, $endDate) {
                $investments->activeBetweenDates($startDate, $endDate);
            })
            ->get();

        \Excel::create(
            $filename,
            function ($excel) use ($products, $startDate, $endDate) {

                foreach ($products as $product) {
                    $name = $product->name;

                    $excel->sheet($name, function ($sheet) use (
                        $product,
                        $startDate,
                        $endDate
                    ) {

                        $data = $this->getForProduct(
                            $startDate,
                            $endDate,
                            $product
                        );

                        $sheet->loadView('reports.finance.interest', [
                            'data' => $data,
                            'currency' => $product->currency
                        ]);
                    });
                }
            }
        )->store('xlsx');

        $email = $user ? $user->email : config('system.administrators');

        $this->sendExcel(
            ucwords($filename),
            'Please find attached interest expense report for SAP',
            $filename,
            [$email]
        );
    }

    public function getForProduct(Carbon $start, Carbon $end, Product $product)
    {
        return ClientInvestment::activeBetweenDates($start, $end)
            ->whereHas('product', function ($prod) use ($product) {
                $prod->where('id', $product->id);
            })
            ->get()
            ->map(function (ClientInvestment $investment) use ($start, $end) {

                for ($date = $start->copy(); $date->lte($end); $date = $date->addMonthNoOverflow()) {
                    $started = $investment->repo->grossInterestBeforeDeductions($date->copy()
                        ->startOfMonth(), false);

                    $ended = $investment->repo->grossInterestBeforeDeductions($date->copy()
                        ->endOfMonth(), true);

                    $paid = $investment->withdrawals()
                        ->where('date', '>=', $date->copy()->startOfMonth())
                        ->where('date', '<=', $date->copy()->endOfMonth())
                        ->sum('amount');

                    $investment->accrued = $ended - $started;
                    $investment->paid = $paid;
                    $investment->desc = "Interest Accrued for the Month " . $date->format('M, Y');
                }

                $currency = $investment->product->currency;

                $baseCurrency = getBaseCurrency($date);

                $investment->amount_in_ksh = convert($investment->accrued, $currency);

                $investment->conversion_rate = (new Convert($baseCurrency))->enableCaching(60)->read($currency, $date);

                return $investment;
            })->filter(function ($investment) {
                return $investment->accrued > 0 && $investment->paid > 0;
            });
    }
}
