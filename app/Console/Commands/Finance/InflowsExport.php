<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 1/9/19
 * Time: 11:02 AM
 */

namespace App\Console\Commands\Finance;

use App\Cytonn\Finance\Inflow;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InflowsExport extends Command
{
    protected $signature = 'sap:inflows {user_id} {start} {end}';

    protected $description = 'Export inflows for SAP integrations';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $input = $this->arguments();

        $user = isset($input['user_id']) ? User::findOrFail($input['user_id']) : null;

        $endDate = isset($input['end']) ? Carbon::parse($input['end']) : Carbon::today();

        $startDate = isset($input['start']) ? Carbon::parse($input['start']) : $endDate->copy()->subMonth();

        (new Inflow())->export($startDate, $endDate, $user, true);

        $this->info('Done');
    }
}
