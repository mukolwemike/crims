<?php
/**
 * Created by PhpStorm.
 * User: eogoma
 * Date: 1/23/19
 * Time: 10:25 AM
 */

namespace App\Console\Commands\Finance;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\User;
use App\Cytonn\Models\WithholdingTax;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;
use Illuminate\Console\Command;
use Cytonn\Mailers\ExcelMailer;

class WithholdingTaxExport extends Command
{
    use ExcelMailer;

    protected $signature = 'sap:wht-export {user_id} {start} {end}';

    protected $description = 'Export withholding tax for SAP integrations';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $input = $this->arguments();

        $user = isset($input['user_id']) ? User::findOrFail($input['user_id']) : null;

        $endDate = isset($input['end']) ? Carbon::parse($input['end']) : Carbon::today();

        $startDate = isset($input['start']) ? Carbon::parse($input['start']) : $endDate->copy()->subMonth();

        $this->export($startDate, $endDate);

        $this->info('Done');
    }

    public function export(Carbon $startDate, Carbon $endDate, User $user = null)
    {
        $filename = 'Withholding Tax Export';

        $currencies = Currency::all();

        \Excel::create(
            $filename,
            function ($excel) use ($currencies, $startDate, $endDate) {

                foreach ($currencies as $currency) {
                    $name = $currency->code . ' - WHT';

                    $excel->sheet($name, function ($sheet) use ($currency, $startDate, $endDate) {

                        $data = $this->getForCurrency($startDate, $endDate, $currency);

                        $sheet->loadView('reports.finance.wht', [
                            'data' => $data,
                            'currency' => $currency
                        ]);
                    });
                }
            }
        )->store('xlsx');

        $email = $user ? $user->email : config('system.administrators');

        $this->sendExcel(
            ucwords($filename),
            'Please find attached withholding tax report for SAP',
            $filename,
            [$email]
        );
    }

    public function getForCurrency($startDate, $endDate, Currency $currency)
    {
        return WithholdingTax::whereNotNull('paid')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->whereHas(
                'custodialTransaction',
                function ($transactions) use ($currency) {
                    $transactions->whereHas(
                        'custodialAccount',
                        function ($custodial) use ($currency) {
                            $custodial
                                ->where('currency_id', $currency->id);
                        }
                    );
                }
            )
            ->get()
            ->map(function ($tax) {

                $currency = $tax->withdrawal->investment->product->currency;

                $date = Carbon::parse($tax->date);

                $baseCurrency = getBaseCurrency($date);

                $tax->amount_in_ksh = convert($tax->amount, $currency);

                $tax->conversion_rate = (new Convert($baseCurrency))->enableCaching(60)->read($currency, $date);

                return $tax;
            });
    }
}
