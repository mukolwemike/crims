<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 1/11/19
 * Time: 11:14 AM
 */

namespace App\Console\Commands\Finance;

use App\Cytonn\Finance\Transfer;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TransfersExport extends Command
{
    protected $signature = 'sap:transfers {user_id} {start} {end} {type}';
    
    protected $description = 'Export for inter client and inter account cash transfers';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $input = $this->arguments();

        $user = isset($input['user_id']) ? User::findOrFail($input['user_id']) : null;

        $end = isset($input['end']) ? Carbon::parse($input['end']) : Carbon::today();

        $start = isset($input['start']) ? Carbon::parse($input['start']) : $end->copy()->subMonth();

        $type = isset($input['type']) ? $input['type'] : 'inter-client';

        (new Transfer())->export($start, $end, $user, $type);

        $this->info('Done');
    }
}
