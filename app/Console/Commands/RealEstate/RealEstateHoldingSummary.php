<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use App\Cytonn\Realestate\Payments\InterestAccrual\ProjectAccrualManager;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class RealEstateHoldingSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:re_holdings_summary {project_id} {date} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of real estate holdings summary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $project = Project::findOrFail($this->argument('project_id'));

        $userId = $this->argument('user_id');

        $date = Carbon::parse($this->argument('date'));

        $report = $this->getReport($project, $date);

        $fileName = 'Real Estate Holdings Summary - ' . $date->toDateString();

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text("Please find attached the real estate holdings summary for $project->name as at "
                . $date->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Project $project
     * @param Carbon $date
     */
    public function getReport(Project $project, Carbon $date)
    {
        $holdings = UnitHolding::inProjectIds([$project->id])->active()->get();

        return $holdings->map(function (UnitHolding $holding) use ($date) {
            $client = $holding->client;

            $unit = $holding->unit;

            $reservationDate = $holding->repo->getReservationDate();

            return [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Unit Number' => $unit->number,
                'Project' => $unit->project->name,
                'Unit Price' => $holding->price(),
                'Total Payments' => $holding->repo->netUnitTotalPaid($date),
                'Amount Overdue' => $holding->repo->amountOverdue(),
                'Interest Accrued' => $holding->interest($date),
                'Payment Plan' => $holding->paymentPlan ? $holding->paymentPlan->name : '',
                'Tranche' => $holding->tranche ? $holding->tranche->name : '',
                'Size' => $holding->unit->size->name,
                'Type' => $holding->unit->type->name,
                'Reservation Date' => $reservationDate ? $reservationDate->toDateString() : '',
                'LOO Signed Date' => $holding->loo ? $holding->loo->present()->dateSigned : '',
                'Sales Agreement Signed Date' => $holding->salesAgreement ?
                    $holding->salesAgreement->present()->dateSigned : '',
            ];
        });
    }
}
