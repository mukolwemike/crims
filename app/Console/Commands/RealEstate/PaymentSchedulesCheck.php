<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PaymentSchedulesCheck extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:check-payment-schedules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if any schedules have been paid and mark them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        DB::transaction(
            function () {
                $this->getTrans();
            }
        );
    }

    protected function getTrans()
    {
        RealEstatePaymentSchedule::whereHas('holding', function ($holding) {
            $holding->has('payments');
        })->chunk(100, function ($schedules) {
            $schedules->each(function ($schedule) {
                $this->checkIfPaid($schedule);
            });
        });
    }

    protected function checkIfPaid(RealEstatePaymentSchedule $schedule)
    {
        if ($schedule->paid() == $schedule->paid) {
            return;
        }

        if ($schedule->paid()) {
            return $schedule->update(['paid'=>true]);
        }

        return $schedule->update(['paid'=>false]);
    }
}
