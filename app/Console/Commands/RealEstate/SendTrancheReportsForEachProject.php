<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\UnitHolding;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\TrancheReportsForEachProjectMailer;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Console\Command;

class SendTrancheReportsForEachProject extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:tranche-reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show tranche reports for each project.';

    /**
     * Create a new command instance.
     * SendTrancheReportsForEachProject constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $tranches_grouped_by_project = RealEstateUnitTranche::has('project')
            ->get()
            ->groupBy('project_id');

        if (count($tranches_grouped_by_project)) {
            $sold = function (
                RealEstateUnitTranche $tranche,
                RealEstatePaymentPlan $paymentPlan,
                RealestateUnitSize $size
            ) {
                return UnitHolding::whereHas(
                    'unit',
                    function ($unit) use ($size) {
                        $unit->where('size_id', $size->id);
                    }
                )
                    ->where('tranche_id', $tranche->id)
                    ->where('payment_plan_id', $paymentPlan->id)
                    ->where('active', 1)
                    ->count();
            };

            $file['Tranche Reports - ' . DatePresenter::formatDate(Carbon::today()->toDateString()) . '.pdf'] =
                PDF::loadView(
                    'reports.tranche_reports',
                    ['tranches_grouped_by_project' => $tranches_grouped_by_project, 'sold' => $sold]
                )->output();

            (new TrancheReportsForEachProjectMailer())->sendEmail($tranches_grouped_by_project, $file);
        }
    }
}
