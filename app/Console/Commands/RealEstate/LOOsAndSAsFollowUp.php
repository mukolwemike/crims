<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstateSalesAgreement;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\LOOsAndSAsFollowUpMailer;
use Illuminate\Console\Command;

class LOOsAndSAsFollowUp extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:loos-sas-follow-up';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LOOs and SAs Follow Up.';

    /**
     * Create a new command instance.
     * LOOsAndSAsFollowUp constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $today = Carbon::today();

        $loos_sas_follow_up = Project::all()->map(
            function ($project) use ($today) {
                $p = new EmptyModel();

                $query = function ($holding, $project, Carbon $today = null) {
                    $holding->active()->date($today)->whereHas(
                        'unit',
                        function ($unit) use ($project) {
                            $unit->where('project_id', $project->id);
                        }
                    );
                };

                $p->{'Project'} = $project->name;

                $p->{'Units Reserved'} = UnitHolding::active()->whereHas(
                    'unit',
                    function ($unit) use ($project) {
                        $unit->where('project_id', $project->id);
                    }
                )->count();

                $p->{'Units Reserved Today'} = UnitHolding::active()->date($today)->whereHas(
                    'unit',
                    function ($unit) use ($project) {
                        $unit->where('project_id', $project->id);
                    }
                )->count();

                $p->{'Signed SAs'} = RealEstateSalesAgreement::whereNotNull('date_signed')->whereHas(
                    'holding',
                    function ($holding) use ($project, $query) {
                        $query($holding, $project);
                    }
                )->count();

                $p->{'Signed SAs Today'} = RealEstateSalesAgreement::whereNotNull('date_signed')->whereHas(
                    'holding',
                    function ($holding) use ($project, $query, $today) {
                        $query($holding, $project, $today);
                    }
                )->count();

                $p->{'Signed LOOs'} = RealestateLetterOfOffer::whereNotNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project, $query) {
                            $query($holding, $project);
                        }
                    )->count() +
                RealestateLetterOfOffer::whereNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project) {
                            $holding->active()
                                ->date(null)
                                ->whereHas(
                                    'unit',
                                    function ($unit) use ($project) {
                                        $unit->where('project_id', $project->id);
                                    }
                                )->whereHas(
                                    'salesAgreement',
                                    function ($salesAgreement) {
                                        $salesAgreement->whereNotNull('date_signed');
                                    }
                                );
                        }
                    )->count();

                $p->{'Signed LOOs Today'} = RealestateLetterOfOffer::whereNotNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project, $query, $today) {
                            $query($holding, $project, $today);
                        }
                    )->count() + RealestateLetterOfOffer::whereNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project, $today) {
                            $holding->active()
                                ->date($today)
                                ->whereHas(
                                    'unit',
                                    function ($unit) use ($project) {
                                        $unit->where('project_id', $project->id);
                                    }
                                )->whereHas(
                                    'salesAgreement',
                                    function ($salesAgreement) {
                                        $salesAgreement->whereNotNull('date_signed');
                                    }
                                );
                        }
                    )->count();
                ;

                $p->{'LOOs Not Signed'} = RealestateLetterOfOffer::whereNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project, $query) {
                            $query($holding, $project);
                        }
                    )->count() - UnitHolding::active()
                    ->doesntHave('loo')
                    ->date(null)
                    ->whereHas(
                        'unit',
                        function ($unit) use ($project) {
                            $unit->where('project_id', $project->id);
                        }
                    )->count();

                $p->{'LOOs Not Signed Today'} = RealestateLetterOfOffer::whereNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project, $query, $today) {
                            $query($holding, $project, $today);
                        }
                    )->count() - UnitHolding::active()
                    ->doesntHave('loo')
                    ->date($today)
                    ->whereHas(
                        'unit',
                        function ($unit) use ($project) {
                            $unit->where('project_id', $project->id);
                        }
                    )->count();

                $p->{'LOOs To Be Prepared'} = RealestateLetterOfOffer::whereNull('document_id')
                    ->whereNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project, $query) {
                            $query($holding, $project);
                        }
                    )->count();

                $p->{'LOOs To Be Prepared Today'} = RealestateLetterOfOffer::whereNull('document_id')
                    ->whereNull('date_signed')
                    ->whereHas(
                        'holding',
                        function ($holding) use ($project, $query, $today) {
                            $query($holding, $project, $today);
                        }
                    )->count();

                $p->{'Total CRE Clients'} = Client::whereHas(
                    'unitHoldings',
                    function ($holding) use ($project, $query) {
                        $query($holding, $project);
                    }
                )->count();

                $p->{'Total CRE Clients Today'} = Client::whereHas(
                    'unitHoldings',
                    function ($holding) use ($project, $query, $today) {
                        $query($holding, $project, $today);
                    }
                )->count();

                $p->{'Forfeitures'} = Client::whereHas(
                    'unitHoldings',
                    function ($holding) use ($project) {
                        $holding->active(false)->whereHas(
                            'unit',
                            function ($unit) use ($project) {
                                $unit->where('project_id', $project->id);
                            }
                        );
                    }
                )->count();

                $p->{'Forfeitures Today'} = Client::whereHas(
                    'unitHoldings',
                    function ($holding) use ($project, $today) {
                        $holding->active(false)->date($today)->whereHas(
                            'unit',
                            function ($unit) use ($project) {
                                $unit->where('project_id', $project->id);
                            }
                        );
                    }
                )->count();

                return $p;
            }
        );

        (new LOOsAndSAsFollowUpMailer())->sendEmail($loos_sas_follow_up);

        $this->info('LOOs & SAs Follow Up has been sent!');
    }
}
