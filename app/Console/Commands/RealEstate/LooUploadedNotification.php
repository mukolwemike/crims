<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealestateLetterOfOffer;
use Carbon\Carbon;
use Cytonn\Mailers\LOOMailer;
use Illuminate\Console\Command;

class LooUploadedNotification extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'loo:send-notifications';

    const REQUIRED_PERCENTAGE = 10;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification when client has signed loo and paid 10%';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        RealestateLetterOfOffer::whereNull('sales_agreement_notification_sent_on')
                    ->whereNotNull('date_signed')
                    ->chunk(
                        100,
                        function ($loos) {
                            $this->send($loos);
                        }
                    );
    }
    
    protected function send($loos)
    {
        $paid = $loos->filter(
            function (RealestateLetterOfOffer $loo) {
                $holding = $loo->holding;

                $paid = $holding->payments()->sum('amount');

                if ($holding->price() == 0) {
                    return false;
                }

                return 100 * ($paid/ $holding->price()) >= static::REQUIRED_PERCENTAGE;
            }
        );

        $paid->each(
            function ($loo) {
                (new LOOMailer())->sendEmailToLegalWhenLOOHasBeenUploaded($loo);

                $loo->sales_agreement_notification_sent_on = Carbon::now();
                $loo->save();
            }
        );
    }
}
