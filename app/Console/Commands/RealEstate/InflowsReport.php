<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class InflowsReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:inflows {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A report of all the real estate inflows for ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));
        $userId = $this->argument('user_id');

        $payments = RealEstatePayment::between($start, $end)->whereHas('holding', function ($q) {
            $q->whereHas('unit', function ($q) {
                $q->has('project');
            });
        })->get();

        $paymentsArray = array();

        foreach ($payments as $payment) {
            $holding = $payment->holding;

            $loo = $holding->loo;

            $salesAgreement = $holding->salesAgreement;

            $recipient = @$holding->commission->recipient;

            $faType = @$recipient->repo->getRecipientType(Carbon::parse($payment->date));

            $position = @$recipient->repo->getRecipientUserPositionByDate(Carbon::parse($payment->date));

            $paymentsArray[$holding->unit->project->name][] = [
                'Date Paid' => $payment->date->toFormattedDateString(),
                'Client Code' => ($holding->client) ? $holding->client->client_code : '',
                'Client Name' => ($holding->client) ? $holding->client->name() : '',
                'Amount' => round($payment->amount, 0),
                'Project' => $holding->unit->project->name,
                'Unit Number' => $holding->unit->number,
                'Value' => $holding ? $holding->price() : null,
                'Size' => $holding->unit->size->name,
                'Type' => $holding->unit->type->name,
                'FA' => $holding->commission->recipient->name,
                'FA Type' => @$faType->name,
                'FA Branch' => $position ? $position->present()->getDepartmentBranch : ($recipient ?
                    $recipient->present()->getBranch : null),
                'FA Department Unit' => $position ? $position->present()->getDepartmentUnit : ($recipient ?
                    $recipient->present()->getDepartmentUnit : null),
                'FA Status' => @$recipient->present()->getActive,
                'Reservation Date' => $holding->reservation_date,
                'Loo Signed Date' => $loo ? $loo->date_signed : '',
                'Sas Signed Date' => $salesAgreement ? $salesAgreement->date_signed : '',
                'Forfeiture Date' => $holding->forfeit_date,
                'Commission Paid' => $holding->repo->commissionPaid(),
            ];
        }

        $fname = 'Real_Estate_Inflows_from_' . $start->toDateString() . '_to_' . $end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($paymentsArray, $fname);

        $email = [];

        if ($userId) {
            $user = User::find($userId);

            if ($user) {
                $email = [$user->email];
            }
        }

        $this->mailResult($fname, $start, $end, $email);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $start
     * @param $end
     */
    private function mailResult($fname, $start, $end, $email)
    {
        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject('Real Estate Inflows Export in excel')
            ->text('Here is the export of real estate inflows that you requested for the period between ' .
                $start->toDateString() . ' and  ' . $end->toDateString())
            ->excel([$fname])
            ->send();

        \File::delete(storage_path('exports/' . $fname . '.xlsx'));
    }
}
