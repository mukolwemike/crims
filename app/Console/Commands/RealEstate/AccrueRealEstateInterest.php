<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Realestate\Payments\InterestAccrual\ProjectAccrualManager;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AccrueRealEstateInterest extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:accrue_re_interest {project_id} {start} {end} {report?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Accrue real estate interest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $project = Project::findOrFail($this->argument('project_id'));

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $report = $this->argument('report');

        $report = $report == 1 ? true : false;

        (new ProjectAccrualManager($project, $start, $end))
            ->setReportMode($report)
            ->process();
    }
}
