<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class RealEstateHoldingMonthlySummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:re_holdings_monthly_summary {project_id} {end} {start?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of real estate holdings monthly summary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $startTime = Carbon::now();

        $project = Project::findOrFail($this->argument('project_id'));

        $start = $this->argument('start');

        $endDate = Carbon::parse($this->argument('end'));

        $firstPayment = RealEstatePayment::forProjects([$project->id])->oldest('date')->first();

        if ($start) {
            $startDate = Carbon::parse($this->argument('start'));

            $firstPaymentDate = $firstPayment ? Carbon::parse($firstPayment->date) : Carbon::now();

            $startDate = $firstPaymentDate > $startDate ? $firstPaymentDate : $startDate;
        } else {
            $startDate = $firstPayment ? Carbon::parse($firstPayment->date) : $endDate->copy()->subMonth()
                ->startOfMonth();
        }

        $userId = $this->argument('user_id');

        $report = $this->getReport($project, $startDate->copy(), $endDate->copy());

        $fileName = 'Real Estate Monthly Holdings Summary - ' . $endDate->toDateString();

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text("Please find attached the real estate holdings summary for $project->name between "
                . $startDate->toDateString() . " and " . $endDate->toDateString() . "<br> Time Taken : "
                . Carbon::now()->diffInMinutes($startTime) . " minutes")
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Project $project
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getReport(Project $project, Carbon $startDate, Carbon $endDate)
    {
        $holdings = UnitHolding::inProjectIds([$project->id])->activeAtDate($endDate)->get();

        $holdingArray = array();

        foreach ($holdings as $holding) {
            $client = $holding->client;

            $unit = $holding->unit;

            $reservationDate = $holding->repo->getReservationDate();

            $holdingDetails = [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Unit Number' => $unit->number,
                'Project' => $unit->project->name,
                'Unit Price' => $holding->price(),
                'Payment Plan' => $holding->paymentPlan ? $holding->paymentPlan->name : '',
                'Tranche' => $holding->tranche ? $holding->tranche->name : '',
                'Size' => $holding->unit->size->name,
                'Type' => $holding->unit->type->name,
                'Reservation Date' => $reservationDate ? $reservationDate->toDateString() : '',
                'LOO Signed Date' => $holding->loo ? $holding->loo->present()->dateSigned : '',
                'Sales Agreement Signed Date' => $holding->salesAgreement ?
                    $holding->salesAgreement->present()->dateSigned : '',
            ];

            $monthlyDetails = $this->getMonthlyData($holding, $startDate, $endDate);

            $holdingDetails = array_merge($holdingDetails, $monthlyDetails);

            $holdingArray[] = $holdingDetails;
        }

        return $holdingArray;
    }

    /**
     * @param UnitHolding $holding
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getMonthlyData(UnitHolding $holding, Carbon $startDate, Carbon $endDate)
    {
        $firstSchedule = $holding->paymentSchedules()->inPricing()->oldest('date')->first();

        $firstScheduleDate = Carbon::parse(@$firstSchedule->date);

        $startingInterest = $startDate >= $firstScheduleDate ? $holding->interest($startDate->copy()) : 0;

        $firstPayment = $holding->payments()->oldest('date')->first();

        $firstPaymentDate = Carbon::parse(@$firstPayment->date);

        $dataArray = array();

        $dataArray['Net Total Payments At Start'] = $startDate >= $firstPaymentDate ?
            $holding->repo->netUnitTotalPaid($endDate->copy()) : 0 ;

        $dataArray['Interest Accrued At Start'] = $startingInterest;

        while ($startDate <= $endDate) {
            $endOfMonth = $startDate->copy()->endOfMonth();

            $endOfMonth = $endOfMonth >= $endDate ? $endDate->copy() : $endOfMonth;

            $payments = $refunds = $closingInterest = 0;

            if ($startDate >= $firstPaymentDate) {
                $payments = $holding->payments()->between($startDate, $endOfMonth)->sum('amount');

                $refunds = $holding->refunds()->between($startDate, $endOfMonth)->sum('amount');
            }

            if ($startDate >= $firstScheduleDate) {
                $closingInterest = $holding->interest($endOfMonth->copy());
            }

            $monthInterest = $closingInterest - $startingInterest;

            $name = $startDate->format('F Y');

            $dataArray[$name . ' Payments'] = $payments;

            $dataArray[$name . ' Refunds'] = $refunds;

            $dataArray[$name . ' Accrued Interest'] = $monthInterest;

            $startingInterest = $closingInterest;

            $startDate = $endOfMonth->addMonthsNoOverflow(1)->startOfMonth();
        }

        $dataArray['Net Total Payments At End'] = $holding->repo->netUnitTotalPaid($endDate->copy());

        $dataArray['Interest Accrued At End'] = $holding->interest($endDate->copy());

        return $dataArray;
    }
}
