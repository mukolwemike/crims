<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealestateLetterOfOffer;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class LOOExpiryNotification extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:loo-expiry-notification {scheduled}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LOOs Expiry Notifications.';

    /**
     * Create a new command instance.
     * LOOExpiryNotification constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Initializing...');

        $today = Carbon::today();
        $date_60_days_ago = $today->copy()->subDays(60);

        $type = $this->input->getArgument('scheduled');

        switch ($type) {
            case 'daily':
                $this->sendDailyLOOExpiryNotification($today->copy(), $date_60_days_ago->copy(), $type);
                break;
            case 'weekly':
                $this->sendWeeklyRemindersOfUnprocessedOverdueSAs($today->copy(), $date_60_days_ago->copy(), $type);
                break;
            default:
                $this->info('Invalid argument!');
        }
    }

    /**
     * @param Carbon $today
     * @param Carbon $date_60_days_ago
     * @param $type
     */
    private function sendDailyLOOExpiryNotification(Carbon $today, Carbon $date_60_days_ago, $type)
    {
        $this->info('Processing...');

        $loos = RealestateLetterOfOffer::whereHas(
            'holding',
            function ($holding) {
                $holding->whereHas(
                    'salesAgreement',
                    function ($sa) {
                        $sa->whereNull('date_signed');
                    }
                );
            }
        )
            ->where('date_signed', $date_60_days_ago)
            ->get();

        $this->exportToExcel($loos, $today, $type);
    }

    /**
     * @param Carbon $today
     * @param Carbon $date_60_days_ago
     * @param $type
     */
    private function sendWeeklyRemindersOfUnprocessedOverdueSAs(Carbon $today, Carbon $date_60_days_ago, $type)
    {
        $this->info('Processing...');

        $loos = RealestateLetterOfOffer::whereHas(
            'holding',
            function ($holding) use ($date_60_days_ago) {
                $holding->doesntHave('salesAgreement')->orWhereHas(
                    'salesAgreement',
                    function ($sa) {
                        $sa->whereNull('date_signed');
                    }
                );
            }
        )
            ->where('date_signed', '<=', $date_60_days_ago)
            ->get();

        $this->exportToExcel($loos, $today, $type);
    }

    /**
     * @param $file_path
     * @param Carbon $today
     * @param $type
     */
    private function mailResult($file_path, Carbon $today, $type)
    {
        $mailer = new GeneralMailer();
        $mailer->to('legal@cytonn.com');
        $mailer->cc('operations@cytonn.com');
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject(
            ucfirst($type) . ' LOO Expiry Notifications - ' . $today->copy()->toFormattedDateString()
        );
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail(
            'Please find the attached the ' . ucfirst($type) . ' LOO expiry notifications report - ' .
            $today->toFormattedDateString()
        );

        \File::delete($file_path);
    }

    /**
     * @param $loos
     * @param Carbon $today
     * @param $type
     */
    private function exportToExcel($loos, Carbon $today, $type)
    {
        if ($loos->count()) {
            $loos = $loos->map(
                function ($loo) {
                    $e = new EmptyModel();

                    $e->{'Client Name'} = ClientPresenter::presentFullNames($loo->holding->client_id);
                    $e->{'Client Code'} = $loo->holding->client->client_code;
                    $e->{'Project'} = $loo->holding->project->name;
                    $e->{'Unit'} = $loo->holding->unit->number;
                    $e->{'Size/Type'} = $loo->holding->unit->size->name . ' ' . $loo->holding->unit->type->name;
                    $e->{'Tranche'} = $loo->holding->tranche->name;
                    $e->{'Payment Plan'} = $loo->holding->paymentPlan->name;
                    $e->{'Price'} = $loo->holding->price();
                    $e->{'Date Signed'} = $loo->date_signed;
                    $e->{'Advocate'} = $loo->advocate->name;
                    $e->{'PM Approver'} = UserPresenter::presentFullNames($loo->pm_approval_id);
                    $e->{'PM Approved On'} = $loo->pm_approved_on;
                    $e->{'Sent By'} = UserPresenter::presentFullNames($loo->sent_by);
                    $e->{'Sent On'} = $loo->sent_on;

                    return $e;
                }
            );

            $this->info('Exporting to excel...');

            $file_name = ucfirst($type) . ' LOO Expiry Notifications - ' . $today->copy()->toFormattedDateString();
            Excel::fromModel($file_name, [ucfirst($type) . ' LOO Expiry Notifications' => $loos])->store('xlsx');

            $this->info('Sending to your inbox...');
            $this->mailResult(storage_path('exports/' . $file_name . '.xlsx'), $today, $type);

            $this->info('[' . ucfirst($type) . '] Sent.');
        } else {
            $this->info('No expired LOOs were found');
        }
    }

    /**
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['scheduled', InputArgument::REQUIRED, 'Either "daily" or "weekly"']
        ];
    }
}
