<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Payments\Interest;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class UpcomingRealEstatePayments extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:upcoming_real_estate_payments {start} {end} {project_id?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the upcoming real estate payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $projectId = $this->argument('project_id');

        $project = $projectId ? Project::find($projectId) : null;

        $payments = $this->getUpcomingPayments($start, $end, $project);

        $fileName = 'Upcoming Real Estate Payments - '. $end->toDateString();

        ExcelWork::generateAndStoreSingleSheet($payments, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the upcoming real estate payments for the period from '
                . $start->toDateString()
             . ' to ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function getUpcomingPayments(Carbon $start, Carbon $end, Project $project = null)
    {
        $payments = RealEstatePaymentSchedule::between($start, $end)->whereHas('holding', function ($q) {
            $q->active();
        });

        if ($project) {
            $payments->forProjects([$project->id]);
        }

        $paymentArray = array();

        foreach ($payments->get() as $schedule) {
            $client = $schedule->holding->client;

            $unit = $schedule->holding->unit;

            $project = $unit->project;

            $paid  = $schedule->paid();

            if (!$paid) {
                $paymentArray[] = [
                    'ID' => $schedule->id,
                    'Client Code' => $client->client_code,
                    'Client Name' => ClientPresenter::presentJointFullNames($client->id),
                    'Date' => Carbon::parse($schedule->date)->toDateString(),
                    'Amount' => $schedule->amount,
                    'Amount Pending' => $schedule->overduePayments(),
                    'Project' => $project->name,
                    'Unit Number' => $unit->number,
                    'Unit Size' => $unit->present()->unitSizeName,
                    'Unit Type' => $unit->present()->unitTypeName,
                    'Payment Plan' => @$schedule->holding->paymentPlan->name,
                    'Description' => $schedule->description,
                    'Schedule Type' => $schedule->type->name,
                    'Overdue Amount' => $schedule->repo->overdue(),
                ];
            }
        }

        return $paymentArray;
    }
}
