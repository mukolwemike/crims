<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\UnitHolding;
use Illuminate\Console\Command;

class UpdateLastRealEstatePaymentSchedule extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:update_last_real_estate_payment_schedules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the last real estate payment schedules';

    /**
     * Create a new command instance.
     * MergeFARecords constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $holdings = UnitHolding::whereHas('paymentSchedules', function ($q) {
            $q->inPricing();
        })->get();

        $holdings->each(function (UnitHolding $holding) {
            \DB::transaction(function () use ($holding) {
                $holding->repo->setLastSchedule();
            });
        });
    }
}
