<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Core\DataStructures\Str;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class RealEstatePaymentSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:payments {project_id} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Real Estate payment summary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $user = User::find($this->argument('user_id'));
        $project = Project::find($this->argument('project_id'));

        $holdings = UnitHolding::whereHas(
            'unit',
            function ($unit) use ($project) {
                if ($project) {
                    $unit->where('project_id', $project->id);
                }
            }
        )->has('client')->get();

        $all_types = RealEstatePaymentType::all();

        $data = $holdings->map(
            function (UnitHolding $holding) use ($all_types, $project) {
                $out = new EmptyModel();
                $out->Name = ClientPresenter::presentJointFullNames($holding->client_id);
                $out->Client_code = $holding->client->client_code;
                if (is_null($project)) {
                    $out->Project = $holding->unit->project->name;
                }
                $out->Unit_Size = $holding->unit->present()->unitSizeName;
                $out->Unit_Type = $holding->unit->present()->unitTypeName;
                $out->Payment_Plan = @$holding->paymentPlan->name;
                $out->Forfeited = ($holding->active == 0) ? 'Yes' : 'No';
                $out->Price = $holding->price();

                foreach ($all_types as $type) {
                    $out->{Str::snakeToWords($type->slug)} =
                        $holding->payments()->where('payment_type_id', $type->id)->sum('amount');
                }

                return $out;
            }
        );

        $filename = 'Payments Summary';

        Excel::fromModel($filename, $data)->store('xlsx');

        $this->mailResult(storage_path('/exports/' . $filename . '.xlsx'), $user);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $start
     * @param $end
     */
    private function mailResult($file_path, $user = null)
    {
        $mailer = new GeneralMailer();

        $admin = \config('system.administrators');
        if ($user) {
            $email = $user->email;
        } else {
            $email = $admin;
        }

        $mailer->to($email);
        $mailer->bcc($admin);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Real Estate payments summary');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached client payments summary');

        \File::delete($file_path);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['project_id', InputArgument::REQUIRED, 'Project id'],
            ['user_id', InputArgument::OPTIONAL, 'get user id']
        ];
    }
}
