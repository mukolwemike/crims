<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MarkPenaltyIneligible extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'penalty:mark-ineligible';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark the schedules ineligible for penalty';

    const PENALTY_CUT_OFF = '2017-01-01';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function fire()
    {
        DB::transaction(
            function () {
                $this->markSchedules();
            }
        );
    }

    public function markSchedules()
    {
        $schedules = RealEstatePaymentSchedule::has('holding')
            ->where('date', '<', static::PENALTY_CUT_OFF)->get();

        $schedules->each(
            function (RealEstatePaymentSchedule $schedule) {
                $holding = $schedule->holding;

                $paid = $holding->payments()->before(Carbon::parse(static::PENALTY_CUT_OFF)
                    ->addDays(30))->sum('amount');
                $scheduled = $holding->paymentSchedules()->inPricing()->before($schedule->date)->sum('amount');

                if ($paid >= $scheduled) {
                    $schedule->update(['penalty' => false]);
                }
            }
        );
    }
}
