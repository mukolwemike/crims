<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealestateLetterOfOffer;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class ReassignLOOAdvocate extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:assign-loo-advocates {client_id?} {advocate_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //        $this->import();

        $this->change();
    }

    //    public function import()
    //    {
    //        \Excel::load(storage_path('loos_no_advocate.xlsx'), function ($reader)
    //        {
    //            foreach ($reader->all() as $loo_data)
    //            {
    //                $loo = \RealestateLetterOfOffer::find($loo_data['id']);
    //
    //                try{
    //                    if($loo) $loo->update(['advocate_id'=>$loo_data['advocate_id']]);
    //
    //                    $this->info('success '.$loo->id);
    //                }catch (\Exception $e){
    //                    $this->error('failed '.$loo->id.' to '.$loo_data['advocate_id']);
    //                }
    //
    //            }
    //        });
    //    }

    private function change()
    {
        $client = Client::findOrFail($this->argument('client_id'));

        $advocate = Advocate::findOrFail($this->argument('advocate_id'));

        RealestateLetterOfOffer::whereHas(
            'holding',
            function ($holding) use ($client) {
                return $holding->where('client_id', $client->id);
            }
        )->get()
            ->each(
                function ($loo) use ($advocate) {
                    $loo->advocate_id = $advocate->id;

                    $loo->save();
                }
            );
    }

    /**
     * Get the client id and the advocate id
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['client_id', InputArgument::OPTIONAL, 'The client id'],
            ['advocate_id', InputArgument::OPTIONAL, 'The advocate id']
        ];
    }
}
