<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\PaymentRemindersForUpcomingRealEstatePaymentsMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Realestate\Payments\PaymentRemindersForUpcomingRealEstatePaymentsExcelGenerator;
use Illuminate\Console\Command;

class SendPaymentRemindersForUpcomingRealEstatePayments extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:upcoming-payments-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send payment reminders for upcoming real estate payments.';

    /**
     * Create a new command instance.
     * SendPaymentRemindersForUpcomingRealEstatePayments constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $two_weeks_from_now = Carbon::today()->addDays(14);
        $reminders = RealEstatePaymentSchedule::has('holding')
            ->where('date', $two_weeks_from_now)->get();

        if ($reminders->count() > 0) {
            $file_name = 'Payment Reminders for Upcoming Real Estate Payments - ' .
                DatePresenter::formatDate($two_weeks_from_now->toDateString());
            (new PaymentRemindersForUpcomingRealEstatePaymentsExcelGenerator())
                ->excel($file_name, $reminders)->store('xlsx');
            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            (new PaymentRemindersForUpcomingRealEstatePaymentsMailer())->sendEmail($reminders, $file_path);

            \File::delete($file_path);
        }
    }
}
