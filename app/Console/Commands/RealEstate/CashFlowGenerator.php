<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

/**
 * Class CashFlowGenerator
 *
 * @package RealEstate
 */
class CashFlowGenerator extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:cashflows {project_id?} {user_id?} {mailing_lists?} {date?}';

    /**
     * @var int
     */
    protected $cacheFor = 1;
    protected $startTime;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate cash flows for a project or all projects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->startTime = Carbon::now();

        $project = Project::find($this->argument('project_id'));

        if ($project) {
            $projects = [$project->id];
        } else {
            $projects = Project::all()->lists('id');
        }

        $user = User::find($this->argument('user_id'));

        if ($user) {
            $email = $user->email;
        } elseif ($list = $this->argument('mailing_lists')) {
            $email = explode(',', $list);
        } else {
            $email = ['mchaka@cytonn.com'];
        }

        $date = $this->argument('date') ? Carbon::parse($this->argument('date')) : Carbon::now();

        $this->send($projects, $email, $date);
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function send($projects, $email, $date)
    {
        $this->start = $date;

        $clients = Client::whereHas(
            'unitHoldings',
            function ($holding) use ($projects) {
                $holding->inProjectIds($projects)->active();
            }
        )->get();

        $dates = RealEstatePaymentSchedule::whereHas(
            'holding',
            function ($holding) use ($projects) {
                $holding->inProjectIds($projects)->active();
            }
        )->whereNotNull('date')->get()->lists('date');

        $payment_dates = RealEstatePayment::whereHas(
            'holding',
            function ($holding) use ($projects) {
                $holding->inProjectIds($projects)->active();
            }
        )->get()->lists('date');

        $months = Collection::make($dates)->merge(Collection::make($payment_dates))
            ->map(
                function ($date) {
                    return Carbon::parse($date)->startOfMonth();
                }
            )->unique()
            ->sort(
                function ($a, $b) {
                    return $a->gt($b);
                }
            );

        $paid = function (UnitHolding $holding, Carbon $month) {
            $date = $month->copy()->endOfMonth();
            $schedules = $holding->paymentSchedules()
                //                ->remember($this->cacheFor)
                ->inPricing()
                ->before($date)
                ->sum('amount');

            $payments = $holding->payments()
                //                ->remember($this->cacheFor)
                ->sum('amount');

            $diff = $schedules - $payments;

            if ($payments > $schedules || abs($diff) <= 1) {
                return true;
            }

            return false;
        };

        $summaryForMonth = function (UnitHolding $holding, Carbon $date) {
            $start = $date->copy()->startOfMonth();
            $end = $date->copy()->endOfMonth();

            $scheduled = $holding->paymentSchedules()
                //                ->remember($this->cacheFor)
                ->inPricing()
                ->between($start, $end)
                ->sum('amount');

            $paid = $holding->payments()
                //                ->remember($this->cacheFor)
                ->between($start, $end)
                ->sum('amount');

            return [
                'scheduled' => $scheduled,
                'paid' => $paid
            ];
        };

        $due_today = function (UnitHolding $holding) use ($date) {
            $paid = $holding->payments()
                //                ->remember($this->cacheFor)
                ->before($date)->sum('amount');
            $scheduled = $holding->paymentSchedules()
                //                ->remember($this->cacheFor)
                ->inPricing()->before($date)->sum('amount');

            if ($paid < $scheduled) {
                return $scheduled - $paid;
            }

            return 0;
        };

        $payments_due = function ($mon) use ($projects) {
            $paid = RealEstatePayment::before($mon)
                //                ->remember($this->cacheFor)
                ->whereHas(
                    'holding',
                    function ($holding) use ($projects) {
                        $holding->inProjectIds($projects);
                    }
                )->sum('amount');

            $scheduled = RealEstatePayment::before($mon)
                //                ->remember($this->cacheFor)
                ->whereHas(
                    'holding',
                    function ($holding) use ($projects) {
                        $holding->inProjectIds($projects);
                    }
                )->sum('amount');

            if ($paid > $scheduled) {
                return $paid - $scheduled;
            }

            return 0;
        };

        $overdueFunc = function ($holding) {
            return $holding->paymentSchedules()
                ->inPricing()
                ->where('date', '<', $this->start)
                ->get()
                ->sum(function ($schedule) {
                    if (Carbon::parse($schedule->date) > $this->start) {
                        return 0;
                    }
                    return $schedule->repo->overdue($this->start);
                });
        };

        $overdueDateFunc = function ($holding) {
            $overdueDate = $holding->repo->getDateOverdue($this->start);

            return $overdueDate ? $this->start->diffInDays($overdueDate) : '';
        };

        $filename = 'Cash Flows';

        \Excel::create(
            $filename,
            function ($excel) use (
                $clients,
                $projects,
                $months,
                $paid,
                $date,
                $due_today,
                $payments_due,
                $summaryForMonth,
                $overdueFunc,
                $overdueDateFunc
            ) {
                $excel->sheet('Summary', function ($sheet) use (
                    $clients,
                    $projects,
                    $months,
                    $paid,
                    $date,
                    $due_today,
                    $payments_due,
                    $summaryForMonth
                ) {
                    $sheet->loadView(
                        'exports.re_cashflows.summary',
                        ['clients' => $clients, 'months' => $months,
                            'project_ids' => $projects, 'paid' => $paid, 'due_today' => $due_today,
                            'cache_for' => $this->cacheFor, 'date' => $date, 'payments_due' => $payments_due,
                            'summaryForMonth' => $summaryForMonth,
                        ]
                    );
                });

                // $excel->sheet(
                //    'CREST Import Summary',
                //    function ($sheet)
                //    use ($clients, $projects, $months, $paid, $date, $due_today, $payments_due, $summaryForMonth) {
                //        $sheet->loadView(
                //            'exports.re_cashflows.import_summary',
                //            ['clients' => $clients, 'months' => $months,
                //               'project_ids' => $projects, 'summaryForMonth' => $summaryForMonth
                //           ]
                //        );
                //    }
                // );

                $excel->sheet(
                    'Paid',
                    function ($sheet) use ($clients, $projects, $months, $paid, $overdueFunc, $overdueDateFunc) {
                        $sheet->loadView(
                            'exports.re_cashflows.paid',
                            [
                                'clients' => $clients, 'months' => $months, 'project_ids' => $projects, 'paid' => $paid,
                                'overdueFunc' => $overdueFunc, 'overdueDateFunc' => $overdueDateFunc
                            ]
                        );
                    }
                );

                foreach ($clients as $client) {
                    $name = ClientPresenter::presentShortName($client->id);

                    $sheet_name = $client->client_code . ' ' . $name;

                    $sheet_name = preg_replace('/\W/', '', $sheet_name);

                    $excel->sheet(
                        substr($sheet_name, 0, 30),
                        function ($sheet) use ($client, $projects) {
                            $sheet->loadView(
                                'exports.re_cashflows.client',
                                ['client' => $client, 'project_ids' => $projects]
                            );
                        }
                    );
                }
            }
        )->store('xlsx');

        $this->mailResult(storage_path() . '/exports/' . $filename . '.xlsx', $email, $projects);
    }

    /**
     * Send an email with the export
     *
     * @param $file_path
     */
    private function mailResult($file_path, $email, $projects)
    {
        $mailer = new GeneralMailer();

        $mailer->to($email);
        $project_name = 'All projects';

        if (count($projects) == 1) {
            $project_name = Project::whereIn('id', $projects)->first()->name;
        }

        $mailer->bcc(config('system.emails.administrators'));
        $mailer->from(['operations@cytonn.com' => 'CRIMS']);
        $mailer->subject(
            'Real Estate Cash Flows - ' . $project_name . ' - ' . Carbon::today()->toFormattedDateString()
        );
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail(
            "Please find the attached the Cash Flows and payment schedules for all clients. <br>Time taken : "
            . Carbon::now()->diffInMinutes($this->startTime) . " minutes"
        );

        \File::delete($file_path);
    }
}
