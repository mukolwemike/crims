<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\RealEstatePenaltyForOverduePaymentsMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Payments\Interest;
use Helper\Unit;
use Cytonn\Core\DataStructures\Files\Excel;
use Illuminate\Console\Command;

class RealEstatePenaltyForOverduePayments extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:penalty-for-overdue-payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate penalty for overdue RE payments.';

    /**
     * Create a new command instance.
     *
     * RealEstatePenaltyForOverduePayments constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $cut_off = $this->cutOffDate()->copy();

        $overdue = UnitHolding::with('unit')->active()->whereHas(
            'paymentSchedules',
            function ($schedules) use ($cut_off) {
                $schedules->where('date', '>', $cut_off);
            }
        )
            ->get()
            ->filter(
                function (UnitHolding $holding) {
                    return $this->hasOverDueSchedules($holding);
                }
            );

        $overdue = $overdue->map(
            function (UnitHolding $holding) {
                $interest = $holding->interest();

                $out = new EmptyModel();
                $out->{'Client Name'} = ClientPresenter::presentJointFullNames($holding->client_id);
                $out->{'Client Code'} = $holding->client->client_code;
                $out->Project = $holding->project->name;
                $out->{'Unit Number'} = $holding->unit->number;
                $out->{'Topology'} = $holding->unit->size->name.' '.$holding->unit->type->name;
                $out->Price = $holding->price();
                $out->{'Amount Overdue'} = $holding->repo->amountOverdue();
                $out->{'Interest Accrued'} = $interest;
                $out->{'New Price'} = $holding->price() + $interest;

                return $out;
            }
        )->groupBy('Project');

        $fname = 'Overdue payments';

        $file = storage_path('exports/'.$fname.'.xlsx');

        Excel::fromModel($fname, $overdue)->store('xlsx');

        (new RealEstatePenaltyForOverduePaymentsMailer())->sendEmail($file);
    }

    protected function hasOverDueSchedules(UnitHolding $holding)
    {
        $schedules = $holding->paymentSchedules()
            ->where('date', '<=', $this->cutOffDate())
            ->isLast(false)
            ->get();

        foreach ($schedules as $schedule) {
            if (!$schedule->paid()) {
                return true;
            }
        }

        return false;
    }

    protected function cutOffDate()
    {
        return Carbon::today()->subDays(Interest::GRACE_PERIOD);
    }
}
