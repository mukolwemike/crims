<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\User;
use Crims\Commission\Override;
use Crims\Realestate\Commission\RecipientCalculator;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Commissions\Recipients;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class RealEstateCommissions extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:commissions {approval_id} {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $approval_id = $this->argument('approval_id');
        $user_id = $this->argument('user_id');
        $approval = ClientTransactionApproval::findOrFail($approval_id);

        $bulk = BulkCommissionPayment::find($approval->payload['bcp_id']);

        $recipients = (new Recipients())->forDates($bulk->start, $bulk->end)
            ->get();

        $recipients_grouped_by_type = $recipients->each(
            function (CommissionRecepient $r) {
                return $r->type = $r->type->name;
            }
        )->groupBy('type');

        $filename = 'Real Estate Commissions';

        $user = User::find($user_id);

        $this->exportDetails($recipients_grouped_by_type, $recipients, $bulk, $user, $filename);
    }

    public function exportDetails($recipients_grouped_by_type, $recipients, $bulk, $user, $filename)
    {
        \Excel::create(
            $filename,
            function ($excel) use ($recipients_grouped_by_type, $recipients, $bulk) {
                foreach ($recipients_grouped_by_type as $type => $recipients_arr) {
                    $excel->sheet(
                        str_limit($type, 20),
                        function ($sheet) use ($recipients_arr, $bulk) {
                            $recipients = Collection::make($recipients_arr)->map(
                                function ($recipient) use ($bulk) {
                                    $out = new EmptyModel();

                                    $override = $recipient
                                        ->reCommissionCalculator($bulk->start, $bulk->end)->summary()->override();

                                    $out->{'Name'} = $recipient->name;
                                    $out->{'Type'} = $recipient->type;
                                    $out->{'Amount'} = $recipient
                                        ->reCommissionCalculator($bulk->start, $bulk->end)->summary()->total();
                                    $out->{'Override'} = $override;
                                    $out->{'Total'} = $recipient->reCommissionCalculator($bulk->start, $bulk->end)
                                        ->summary()->finalCommission();

                                    return $out;
                                }
                            );

                            $sheet->fromModel($recipients);
                        }
                    );
                }

                foreach ($recipients as $recipient) {
                    $excel->sheet(
                        str_limit($recipient->name, 20),
                        function ($sheet) use ($recipient, $bulk) {
                            $schedules = $recipient->reCommissionCalculator($bulk->start, $bulk->end)->schedules();

                            $schedules = $schedules->map(
                                function (RealEstateCommissionPaymentSchedule $schedule) {
                                    $out = new EmptyModel();

                                    $holding = $schedule->commission->holding;

                                    $out->{'Client Code'} = $holding->client->client_code;
                                    $out->{'Client Name'} = ClientPresenter::presentJointFullNames($holding->client_id);
                                    $out->{'Project'} = $holding->project->name;
                                    $out->{'Unit'} = $holding->unit->number;
                                    $out->{'Description'} = $schedule->description;
                                    $out->{'Loo Date'} = $holding->loo->date_received;
                                    $out->{'Sales Ag. Date'} = $holding->salesAgreement->date_received;
                                    $out->{'Commission Date'} = $schedule->date;
                                    $out->{'Amount'} = $schedule->amount;

                                    return $out;
                                }
                            );

                            $sheet->fromModel($schedules);
                        }
                    );
                }

                $recipientsWithOverride = CommissionRecepient::all()
                    ->filter(
                        function ($recipient) use ($bulk) {
                            $o = (new RecipientCalculator($recipient, $bulk->start, $bulk->end))->summary()->override();

                            return $o != 0;
                        }
                    );

                foreach ($recipientsWithOverride as $overrideRecipient) {
                    $excel->sheet(
                        str_limit('Override - ' . $overrideRecipient->name, 29),
                        function ($sheet) use ($overrideRecipient, $bulk) {
                            $o = new Override($overrideRecipient);
                            $reports = $o->reports($overrideRecipient)
                                ->map(
                                    function ($r) use ($bulk, $overrideRecipient) {
                                        $commission =
                                            $r->reCommissionCalculator($bulk->start, $bulk->end)->summary()->total();

                                        $arr = [
                                            'Name' => $r->name,
                                            'Rank' => @$r->rank->name,
                                            'Commission' => $commission,
                                            'Rate' => $overrideRecipient->rank->override_rate,
                                            'Override' => $overrideRecipient->rank->override_rate * $commission / 100
                                        ];
                                        return (new EmptyModel())->fill($arr);
                                    }
                                )->filter(
                                    function ($c) {
                                        return $c->{'Override'} != 0;
                                    }
                                );

                            $sheet->fromModel($reports);
                        }
                    );
                }
            }
        )->store('xlsx');

        $this->mailResult(storage_path('exports/' . $filename . '.xlsx'), $user);
    }

    private function mailResult($file_path, $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->from('support@cytonn.com');
        $mailer->subject('CRIMS Commission Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Please find the attached Real Estate Commission Report');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['approval_id', InputArgument::REQUIRED, 'Approval id.'],
            ['user_id', InputArgument::REQUIRED, 'User id.'],
        ];
    }
}
