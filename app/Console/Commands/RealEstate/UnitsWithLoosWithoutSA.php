<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 20/03/2018
 * Time: 10:43
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Presenters\General\DatePresenter;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\MonthlyHoldingsReportMailer;
use Cytonn\Realestate\Units\UnitsWithLoosWithoutSAExcelGenerator;
use Illuminate\Console\Command;

class UnitsWithLoosWithoutSA extends command
{
    protected $signature = 're:pending-sales-agreements';

    protected $description = 'send a monthly report of units with loos sent to client but 
    without signed sales agreement';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $date = Carbon::today()->subMonth(1);

        $unitHoldings = UnitHolding::looSentBefore($date)
            ->active()
            ->withoutSalesAgreement()
            ->get()
            ->each(function ($holding) {
                $holding->project_id = $holding->unit->project_id;
            })->groupBy('project_id');

        if (count($unitHoldings) > 0) {
            $file_name = 'Units with Letters of Offer and no Sale Agreements as at - ' .
                DatePresenter::formatDate(Carbon::now()->toDateString());

            (new UnitsWithLoosWithoutSAExcelGenerator())->excel($file_name, $unitHoldings)->store('xlsx');
            $file_path = storage_path() . '/exports/' . $file_name . '.xlsx';

            (new MonthlyHoldingsReportMailer())->sendEmail($unitHoldings, $file_path);
            $this->info("The report has been sent");

            \File::delete($file_path);

            return;
        }

        $this->warn("There were no units to report on");
    }
}
