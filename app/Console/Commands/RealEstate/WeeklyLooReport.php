<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealestateLetterOfOffer;
use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Mailers\RealEstate\ExcelReportOfAllLoosSentForAProjectMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\AllLOOsSentExcelGenerator;
use Illuminate\Console\Command;

class WeeklyLooReport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:weekly-loo-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send excel report of all LOOs sent for a project.';

    /**
     * Create a new command instance.
     *  SendExcelReportOfAllLoosSentForAProject constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $loos_grouped_by_project = RealestateLetterOfOffer::whereNotNull('sent_on')
            ->has('holding')
            ->get()
            ->each(
                function ($loo) {
                    $loo->project_id = $loo->holding->unit->project->id;
                }
            )->groupBy('project_id');

        // Only generate and send email if there's something to send
        if (count($loos_grouped_by_project)) {
            $file_name = 'LOOs Sent - ' . DatePresenter::formatDate(Carbon::today()->toDateString());
            (new AllLOOsSentExcelGenerator())->excel($file_name, $loos_grouped_by_project)->store('xlsx');
            $file_path = storage_path().'/exports/'.$file_name.'.xlsx';

            (new ExcelReportOfAllLoosSentForAProjectMailer())->sendEmail($file_path);

            \File::delete($file_path);
        }
    }
}
