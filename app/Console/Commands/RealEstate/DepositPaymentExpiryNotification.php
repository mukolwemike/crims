<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class DepositPaymentExpiryNotification extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:deposit-payment-expiry-notifications {schedule}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deposit payment expiry notifications.';

    /**
     * Create a new command instance.
     * DepositPaymentExpiryNotification constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Initializing...');

        $today = Carbon::today();
        $date_30_days_ago = $today->copy()->subDays(30);

        $type = $this->input->getArgument('schedule');

        switch ($type) {
            case 'daily':
                $this->sendDailyDepositPaymentExpiryNotification($today->copy(), $date_30_days_ago->copy(), $type);
                break;
            case 'monthly':
                $this->sendMonthlyDepositPaymentExpiryNotification($today->copy(), $date_30_days_ago->copy(), $type);
                break;
            default:
                $this->info('Invalid argument!');
        }
    }

    /**
     * @param Carbon $today
     * @param Carbon $date_30_days_ago
     * @param $type
     */
    private function sendDailyDepositPaymentExpiryNotification(Carbon $today, Carbon $date_30_days_ago, $type)
    {
        $this->info('Processing...');

        $holdings = UnitHolding::whereBetween(
            'created_at',
            [$date_30_days_ago->copy()->startOfDay(), $date_30_days_ago->copy()->endOfDay()]
        )->get()->filter(
            function ($holding) {
                $deposit = 0.1 * $holding->price();
                return $holding->payments()->sum('amount') < $deposit;
            }
        );


        $this->exportToExcel($holdings, $today, $type);
    }

    /**
     * @param Carbon $today
     * @param Carbon $date_30_days_ago
     * @param $type
     */
    private function sendMonthlyDepositPaymentExpiryNotification(Carbon $today, Carbon $date_30_days_ago, $type)
    {
        $this->info('Processing...');

        $holdings = UnitHolding::where('created_at', '<=', $date_30_days_ago->copy()->endOfDay())
            ->get()
            ->filter(
                function ($holding) {
                    $deposit = 0.1 * $holding->price();
                    return $holding->payments()->sum('amount') < $deposit;
                }
            );


        $this->exportToExcel($holdings, $today, $type);
    }

    /**
     * @param $holdings
     * @param Carbon $today
     * @param $type
     */
    private function exportToExcel($holdings, Carbon $today, $type)
    {
        if ($holdings->count()) {
            $holdings = $holdings->map(
                function ($holding) {
                    $e = new EmptyModel();

                    $payment_type = RealEstatePaymentType::where('slug', 'deposit')->first();

                    $e->{'Client Name'} = ClientPresenter::presentFullNames($holding->client_id);
                    $e->{'Client Code'} = $holding->client->client_code;
                    $e->{'Project'} = $holding->project->name;
                    $e->{'Unit'} = $holding->unit->number;
                    $e->{'Size/Type'} = $holding->unit->size->name . ' ' . $holding->unit->type->name;
                    $e->{'Tranche'} = $holding->tranche->name;
                    $e->{'Reservation Date'} = $holding->created_at;
                    $e->{'Expiry Date'} = (new Carbon($holding->created_at))->addDays(30);
                    $e->{'Payment Plan'} = $holding->paymentPlan->name;
                    $e->{'Unit Price'} = $holding->price();
                    $e->{'Scheduled Deposit'} = $holding->price() * 0.1;
                    $e->{'Paid Deposit'} = $holding->payments()
                        ->where('payment_type_id', $payment_type->id)
                        ->sum('amount');

                    return $e;
                }
            )->sortBy('Project');

            $this->info('Exporting to excel...');

            $file_name = ucfirst($type) . ' Deposit Payment Expiry Notifications - ' .
                $today->copy()->toFormattedDateString();
            Excel::fromModel($file_name, [ucfirst($type) . ' Expiry Notifications' => $holdings])->store('xlsx');

            $this->info('Sending to your inbox...');
            $this->mailResult(storage_path('exports/' . $file_name . '.xlsx'), $today, $type);

            $this->info('[' . ucfirst($type) . '] Sent.');
        } else {
            $this->info('No records were found');
        }
    }

    /**
     * @param $file_path
     * @param Carbon $today
     * @param $type
     */
    private function mailResult($file_path, Carbon $today, $type)
    {
        $mailer = new GeneralMailer();
        $mailer->to('legal@cytonn.com');
        $mailer->cc('operations@cytonn.com');
        $mailer->bcc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject(ucfirst($type) . ' Deposit Payment Expiry Notifications - ' .
            $today->copy()->toFormattedDateString());
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find the attached the ' . ucfirst($type) .
            ' Deposit Payment expiry notifications report - ' . $today->toFormattedDateString());

        \File::delete($file_path);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['schedule', InputArgument::REQUIRED, 'Either "daily" or "monthly".'],
        ];
    }
}
