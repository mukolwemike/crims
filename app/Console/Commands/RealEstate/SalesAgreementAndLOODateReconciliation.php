<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstateSalesAgreement;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SalesAgreementAndLOODateReconciliation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 're:sa-loo-dates-reconciliation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'If client has signed SA and no LOO, CRIMS should populate the dates of the 
    LOO with the dates of the SA.';

    /**
     * Create a new command instance.
     *
     * SalesAgreementAndLOODateReconciliation constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();

        // If any SAs uploaded today
        RealEstateSalesAgreement::where('created_at', '>=', $today->copy()->startOfDay())
            ->where('created_at', '<', $today->copy()->endOfDay())
            ->whereNotNull('date_signed')
            ->get()
            ->each(
                function (RealEstateSalesAgreement $sa) {
                    $loo = $sa->holding->loo;
                    if ($loo and is_null($loo->date_signed)) {
                        $loo->date_signed = $sa->date_signed;
                        $loo->save();
                    }
                }
            );

        // If any LOOs uploaded today
        RealestateLetterOfOffer::where('created_at', '>=', $today->copy()->startOfDay())
            ->where('created_at', '<', $today->copy()->endOfDay())
            ->whereNull('date_signed')
            ->get()
            ->each(
                function (RealestateLetterOfOffer $loo) {
                    $sa = $loo->holding->salesAgreement;
                    if ($sa and $sa->date_signed) {
                        $loo->date_signed = $sa->date_signed;
                        $loo->save();
                    }
                }
            );
    }
}
