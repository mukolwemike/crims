<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealestateLetterOfOffer;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\DailyLOOsReportMailer;
use Illuminate\Console\Command;

class SendLOOsSentToClientsDailyReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:loo-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a daily report of the loos that were sent to clients.';

    /**
     * Create a new command instance.
     * SendLOOsSentToClientsDailyReport constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $now = Carbon::now();

        $loos_today_grouped_by_project =
            RealestateLetterOfOffer::where('sent_on', '>=', $now->copy()->subHours(24))
            ->where('sent_on', '<=', $now)
            ->get()
            ->each(
                function ($loo) {
                    $loo->project_id = $loo->holding->project->id;
                }
            )->groupBy('project_id');

        $loos_this_week_grouped_by_project =
            RealestateLetterOfOffer::where('sent_on', '>=', $now->copy()->startOfWeek())
            ->where('sent_on', '<=', $now->copy()->endOfWeek())
            ->get()
            ->each(
                function ($loo) {
                    $loo->project_id = $loo->holding->project->id;
                }
            )->groupBy('project_id');

        $loos_this_month_grouped_by_project =
            RealestateLetterOfOffer::where('sent_on', '>=', $now->copy()->startOfMonth())
            ->where('sent_on', '<=', $now->copy()->endOfMonth())
            ->get()
            ->each(
                function ($loo) {
                    $loo->project_id = $loo->holding->project->id;
                }
            )->groupBy('project_id');

        // Only send email if there's something to send
        if (count($loos_this_month_grouped_by_project)) {
            $mailer = new DailyLOOsReportMailer();
            $mailer->sendEmail(
                $loos_today_grouped_by_project,
                $loos_this_week_grouped_by_project,
                $loos_this_month_grouped_by_project
            );
        }
    }
}
