<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Realestate\Commissions\AwardHandler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RECommissionUpdate extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:update-commissions {holding_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed & update Real Estate Commissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function fire()
    {
        $holdings = $this->getHoldings();

        DB::transaction(function () use ($holdings) {
            $holdings->each(
                function (UnitHolding $holding) {
                    $payment = $holding->payments()->oldest('date')->first();

                    if ($payment) {
                        $holding->update(['reservation_date' => $payment->date]);
                    }

                    if ($holding->price() > 0) {
                        $this->updateForHolding($holding);
                    }
                }
            );
        });

        $this->info("Check complete");
    }

    public function updateForHolding(UnitHolding $holding)
    {
        $award = new AwardHandler();

        $this->info("Checking unit holding {$holding->id} "
            . "unit: {$holding->unit->number} project: {$holding->project->name}");

        if ($sa = $holding->salesAgreement) {
            $award->onSalesAgreementSigned($sa);
        }

        if ($loo = $holding->letterOfOffer) {
            $award->onLooSigned($loo);
        }

        $payments = $holding->payments()->oldest()->get();

        $payments->each(function (RealEstatePayment $payment) use ($award) {
            $award->onPaymentMade($payment);
        });
    }

    /**
     * @return UnitHolding[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getHoldings()
    {
        $holdingId = $this->argument('holding_id');

        if ($holdingId) {
            return UnitHolding::where('id', $holdingId)->get();
        }

        $date = Carbon::now()->subMonthsNoOverflow(1);

        return UnitHolding::whereHas('payments', function ($q) use ($date) {
            $q->where('date', '>=', $date);
        })
            ->orWhereHas('letterOfOffer', function ($loo) use ($date) {
                $loo->where('date_received', '>=', $date);
            })
            ->orWhereHas('salesAgreement', function ($sa) use ($date) {
                $sa->where('date_received', '>=', $date);
            })->where('active', 1)
            ->get();
    }
}
