<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstateSalesAgreement;
use Carbon\Carbon;
use Cytonn\Mailers\RealEstate\LooSaMailer;
use Illuminate\Console\Command;

class LooSalesAgreementReporting extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:loo-sa-reporting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LOOs and SAs reporting.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $today = Carbon::today();

        $loos_sent_grouped_by_project =
            RealestateLetterOfOffer::where('sent_on', '>=', $today->copy()->startOfDay())
                ->where('sent_on', '<=', $today->copy()->endOfDay())
                ->get()
                ->each(
                    function ($loo) {
                        $loo->project_id = $loo->holding->project->id;
                    }
                )->groupBy('project_id');

        $loos_signed_grouped_by_project =
            RealestateLetterOfOffer::where('date_signed', $today->copy()->toDateString())
                ->get()
                ->each(
                    function ($loo) {
                        $loo->project_id = $loo->holding->project->id;
                    }
                )->groupBy('project_id');

        $sas_signed_grouped_by_project =
            RealEstateSalesAgreement::where('date_signed', $today->copy()->toDateString())
                ->get()
                ->each(
                    function ($sa) {
                        $sa->project_id = $sa->holding->project->id;
                    }
                )->groupBy('project_id');

        if (count($loos_sent_grouped_by_project) > 0 or
            count($loos_signed_grouped_by_project) > 0 or
            count($sas_signed_grouped_by_project) > 0
        ) {
            (new LooSaMailer())->sendEmail(
                $loos_sent_grouped_by_project,
                $loos_signed_grouped_by_project,
                $sas_signed_grouped_by_project,
                $today
            );
        }

        $this->info('Report sent!');
    }
}
