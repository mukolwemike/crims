<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class RealEstateUnitReservationSummary extends Command
{
    use ExcelMailer;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 're:reservations-summary {start} {end} {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Real Estate Unit Reservations over a certain period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = new Carbon($this->argument('start'));

        $end = new Carbon($this->argument('end'));

        $email = $this->argument('email');

        $purchases = $this->purchases($start, $end);

        $forfeitures = $this->forfeiture($start, $end);

        $fname = 'RE Reservations Summary';

        ExcelWork::generateAndStoreMultiSheet(['Reservations' => $purchases, 'Forfeitures' => $forfeitures], $fname);

        $users = $email ? explode(',', $email) : [];

        $this->sendExcel($fname, 'Please find attached for ' . $start->toFormattedDateString() . ' to ' .
            $end->toFormattedDateString(), $fname, $users);
    }

    private function purchases($start, $end)
    {
        $holdings = UnitHolding::whereHas('unit', function ($q) {
            $q->has('project');
        })->whereHas('payments', function ($q) use ($start, $end) {
            $q->between($start, $end);
        })->get();

        $holdingArray = array();

        foreach ($holdings as $holding) {
            $firstPayment = $holding->payments()->oldest('date')->first();

            if ($firstPayment->date->copy()->gte($start) && $firstPayment->date->copy()->lte($end)) {
                $holdingArray[] = $this->mapHolding($holding, $firstPayment, $end);
            }
        }

        return collect($holdingArray)->sortBy('Project');
    }

    private function forfeiture($start, $end)
    {
        $holdings = UnitHolding::has('payments')->whereHas('unit', function ($q) {
            $q->has('project');
        })->where('forfeit_date', '>=', $start)->where('forfeit_date', '<=', $end)
            ->get();

        $holdingArray = array();

        foreach ($holdings as $holding) {
            $firstPayment = $holding->payments()->oldest('date')->first();

            $holdingArray[] = $this->mapHolding($holding, $firstPayment, $end);
        }

        return collect($holdingArray)->sortBy('Project');
    }

    /**
     * @param UnitHolding $holding
     * @param RealEstatePayment $firstPayment
     * @return array
     */
    private function mapHolding(UnitHolding $holding, RealEstatePayment $firstPayment, Carbon $end)
    {
        $recipient = $holding->commission->recipient;

        $comms = $this->getCommissionPayable($holding);

        $faType = $recipient->repo->getRecipientType(Carbon::parse($firstPayment->date));

        $position = $recipient->repo->getRecipientUserPositionByDate(Carbon::parse($firstPayment->date));

        return [
            'Project' => $holding->project->name,
            'Unit' => $holding->unit->number,
            'Client Name' => ClientPresenter::presentFullNames($holding->client_id),
            'Topology' => $holding->unit->size->name . ' ' . $holding->unit->type->name,
            'Reservation Date' => $firstPayment->date->toDateString(),
            'LOO Signed Date' => !$holding->loo ? null : $holding->loo->present()->dateSigned,
            'Sales Agreement Signed Date' => !$holding->salesAgreement
                ? null : $holding->salesAgreement->present()->dateSigned,
            'Forfeiture Date' => $holding->forfeit_date,
            'Price' => (float)$holding->price(),
            'FA' => $recipient->name,
            'FA Type' => @$faType->name,
            'FA Branch' => $position ? $position->present()->getDepartmentBranch : ($recipient ?
                $recipient->present()->getBranch : null),
            'FA Department Unit' => $position ? $position->present()->getDepartmentUnit : ($recipient ?
                $recipient->present()->getDepartmentUnit : null),
            'Total Payments As At End' => $this->getRealEstatePayments($holding, $end),
            'Total Paid Commission As At End' => $this->getPaidCommission($holding, $end),
            'Total Commission To Be Paid' => $holding->commission->amount,
            'FA Commission Rate' => $holding->commission->commission_rate,
            'Total Comms' => $comms['total'],
            'Payable to FA' => $comms['fa'],
            'Payable to CIM' => $comms['cim']
        ];
    }

    /**
     * @param UnitHolding $holding
     * @return mixed
     */
    private function getRealEstatePayments(UnitHolding $holding, Carbon $end)
    {
        return RealEstatePayment::where('holding_id', $holding->id)->where('date', '<=', $end)
            ->sum('amount');
    }

    /**
     * @param UnitHolding $holding
     * @return mixed
     */
    private function getPaidCommission(UnitHolding $holding, Carbon $end)
    {
        return RealEstateCommissionPaymentSchedule::whereHas('commission', function ($q) use ($holding) {
            $q->where('holding_id', $holding->id);
        })->where('date', '<=', $end)->sum('amount');
    }

    private function getCommissionPayable(UnitHolding $holding)
    {
        $highestRate = $holding->project->commissionRates()->orderBy('amount', 'DESC')->first();

        $type = $highestRate->type;
        switch ($type) {
            case 'percentage':
                $val = $holding->price();
                $totalComms = $highestRate->amount * $val / 100;
                $fa = $holding->commission->commission_rate * $val / 100;
                $rate = $holding->commission->commission_rate;

                return ['total' => $totalComms, 'fa' => $fa, 'cim' => $totalComms - $fa, 'rate' => $rate];
                break;
            case 'amount':
                $totalComms = $highestRate->amount;
                $fa = $holding->commission->commission_rate;

                return ['total' => $totalComms, 'fa' => $fa, 'cim' => $totalComms - $fa, 'rate' => $fa];
                break;
            default:
                throw new \InvalidArgumentException("Commission scheme $type is not supported");
        }
    }
}
