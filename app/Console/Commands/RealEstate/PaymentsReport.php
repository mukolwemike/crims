<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PaymentsReport extends Command
{
    use ExcelMailer;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 're:payments-report {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $paid = function ($holding, $slug) {
            return (float) $holding->payments()->whereHas(
                'type',
                function ($type) use ($slug) {
                    $type->where('slug', $slug);
                }
            )->sum('amount');
        };

        
        $data = UnitHolding::orderBy('client_id', 'ASC')
            ->get()
            ->map(
                function (UnitHolding $holding) use ($paid) {
                    $d = [
                    'Client Code' => $holding->client->client_code,
                    'Name' => ClientPresenter::presentJointFullNames($holding->client_id),
                    'Project' => $holding->project->name,
                    'Unit Number' => $holding->unit->number,
                    'Size' => $holding->unit->size->name,
                    'Type' => $holding->unit->type->name,
                    'Reservation Fees' => $paid($holding, 'reservation_fee'),
                    'Deposit' => $paid($holding, 'deposit'),
                    'Installments' => $paid($holding, 'installment')
                    ];

                    return (new EmptyModel())->fill($d);
                }
            );

        $fname = 'RE Payments report';

        $recipient = User::find($this->option('user_id'));

        if (!$recipient) {
            $recipient = \config('system.administrators');
        } else {
            $recipient = [$recipient->email];
        }

        Excel::fromModel($fname, $data)->store('xlsx');
        $this->sendExcel(
            'Real Estate Payments Summary',
            'Please find attached a summary of all payments made for Real Estate Units',
            $fname,
            $recipient
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['user_id', null, InputOption::VALUE_OPTIONAL, 'User requesting report', null],
        ];
    }
}
