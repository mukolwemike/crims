<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Statements\StatementGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class RealestateStatementsExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:re_statements_export {date} {sender_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::parse($this->argument('date'));

        $sender = User::findOrFail($this->argument('sender_id'));

        $clients = Client::whereHas('unitHoldings', function ($q) {
            $q->where('active', 1);
        })->get();

        $clients->each(function (Client $client) use ($date, $sender) {
            $this->info($client->id);

            $projects = Project::whereHas('units', function ($q) use ($date, $sender, $client) {
                $q->whereHas(
                    'holdings',
                    function ($q) use ($date, $sender, $client) {
                        $q->where('client_id', $client->id);
                    }
                );
            })->get();

            foreach ($projects as $project) {
                $this->saveStatement($client, $project, $sender, $date);
            }
        });

        $folder = storage_path();

        exec("cd $folder && tar -cvzf misc.tar.gz misc");
    }

    private function saveStatement(Client $client, Project $project, User $sender, Carbon $date)
    {
        $generator = new StatementGenerator();

        $pdf = $generator->generate($client, $project, $date, $sender)->output();

        $filename = Excel::cleanSheetName(ClientPresenter::presentFullNameNoTitle($client->id));

        $filename = $filename . '_' . $client->client_code.'.pdf';

        $folder = 'misc/statements_dump/'.strtotime($date->toDateString()).'/'.$project->name;

        $this->upload($pdf, $filename, $folder);
    }

    private function upload($contents, $filename, $folder)
    {
        Storage::disk(env('STORAGE_DISK'))->put($folder . '/'. $filename, $contents);
    }
}
