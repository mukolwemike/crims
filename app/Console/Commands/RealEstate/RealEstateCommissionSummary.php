<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstateCommissionPayment;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\DatePresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class RealEstateCommissionSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:re_commission_summary {start} {end} {project?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of real estate commissions for the selected parameters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $projectId = $this->argument('project');

        $project = $projectId ? Project::findOrFail($projectId) : null;

        $report = $this->getReport($start, $end, $project);

        $fileName = 'RE Commissions - ' . DatePresenter::formatDate($start) . ' To ' . DatePresenter::formatDate($end);

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the real estate commission report for the period between '
                . $start->toDateString() . ' to ' . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param Project|null $project
     * @return array
     */
    private function getReport(Carbon $start, Carbon $end, Project $project = null)
    {
        $schedules = $this->getCommissions($start, $end, $project);

        $scheduleArray = array();

        foreach ($schedules as $schedule) {
            $holding = $schedule->commission->holding;

            $projectName = $holding->unit->project->name;

            $scheduleArray[$projectName][] = [
                'Date' => Carbon::parse($schedule->date)->toDateString(),
                'Amount' => $schedule->amount,
                'Commission Type' => $schedule->type->name,
                'Project' => $projectName,
                'Client Code' => ($holding->client) ? $holding->client->client_code : '',
                'Client Name' => ($holding->client) ? $holding->client->name() : '',
                'Unit Number' => $holding->unit->number,
                'Value' => $holding ? $holding->price() : null,
                'Size' => $holding->unit->size->name,
                'Type' => $holding->unit->type->name,
                'FA' => $holding->commission->recipient->name,
                'Description' => $schedule->description
            ];
        }

        foreach ($scheduleArray as $key => $projectSchedule) {
            $scheduleArray[$key] = collect($projectSchedule)->sortBy('FA');
        }

        $scheduleArray['Overrides'] = $this->getOverrides($start, $end);

        return $scheduleArray;
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param Project|null $project
     * @return RealEstateCommissionPaymentSchedule[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getCommissions(Carbon $start, Carbon $end, Project $project = null)
    {
        $commissions = RealEstateCommissionPaymentSchedule::where('date', '>=', $start)
            ->where('date', '<=', $end);

        if ($project) {
            $commissions = $commissions->whereHas('commission', function ($q) use ($project) {
                $q->whereHas('holding', function ($q) use ($project) {
                    $q->inProjectIds([$project->id]);
                });
            });
        }

        return $commissions->orderBy('date')->get();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return \Illuminate\Support\Collection
     */
    private function getOverrides(Carbon $start, Carbon $end)
    {
        $payments = RealEstateCommissionPayment::where('date', '>=', $start)
            ->where('overrides', '>', 0)->where('date', '<=', $end)
            ->orderBy('date')->get();

        $payments = $payments->map(function (RealEstateCommissionPayment $payment) {
            return [
                'Date' => Carbon::parse($payment->date)->toDateString(),
                'Amount' => $payment->overrides,
                'FA' => $payment->recipient->name,
                'Description' => $payment->description
            ];
        });

        return $payments->sortBy('FA');
    }
}
