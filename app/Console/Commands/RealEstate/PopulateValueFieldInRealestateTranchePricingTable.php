<?php

namespace App\Console\Commands\RealEstate;

use App\Cytonn\Models\RealEstateUnitTranchePricing;
use Illuminate\Console\Command;

class PopulateValueFieldInRealestateTranchePricingTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 're:populate-value-in-tranche-pricing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate the value field in realestate_unit_tranche_pricing table';

    /**
     * Create a new command instance.
    /**
     * PopulateValueFieldInRealestateTranchePricingTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        RealEstateUnitTranchePricing::all()
            ->each(
                function ($pricing) {
                    if (is_null($pricing->value)) {
                        $pricing->value = $pricing->price;
                        $pricing->save();
                    }
                }
            );
    }
}
