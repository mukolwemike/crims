<?php

namespace App\Console\Commands;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Withdraw;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Shares\Trading\Redeem;
use Illuminate\Console\Command;

class RedeemCoop extends Command
{
    use ExcelMailer;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coop:redeem';

    const DATE = '2018-06-29';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $fm;

    protected $user;

    protected $products;

    protected $entity;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->user = User::where('username', 'system')->first();
//
//        $fm = $this->fm();
//
//        $this->products = $fm->products;
//
//        $this->entity = SharesEntity::find(1);

        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        dd('');

        \DB::transaction(function () {
            $this->fm();

            $payments = Client::all()->map(function ($client) {
                $bal = $this->balance($client);
                $array = $this->redeem($client);
                $array->client = $client;
                $array->balance_before = $bal;

                return $array;
            })->filter(function ($client) {
                $zero = ($client->shares == 0) && ($client->membership == 0) && ($client->investments == 0);

                return !$zero;
            });

            $this->report($payments);

//            throw new ClientInvestmentException("Rollback transaction");
        });
    }

    protected function redeem(Client $client)
    {
        //investments
        $inv = $client->investments()->fundManager($this->fm())->active()->get();

        $withdrawals = $inv->map(function (ClientInvestment $investment) {
            $approval = $this->createWithdrawalApproval($investment);

            $premature = $investment->maturity_date->gt(carbon(static::DATE));

            $date = $premature ? carbon(static::DATE) : $investment->maturity_date;

            $w = (new Withdraw())->all($investment, $date, $approval, null, ['generate_instruction' => true]);

            $approval->approve($this->user);

            return $w;
        });

        $sharesC = $client->shareHolders()->where('entity_id', 1)->get()
            ->map(function ($holder) {
                return $this->shares($holder);
            });

        $shares = $sharesC->sum(function ($c) {
            $ded = $c['shares'];
            return $ded ? $ded->sum(function ($d) {
                return $d->payment->amount;
            }) : 0;
        });

        $membership = $sharesC->sum(function ($p) {
            $ded = $p['payment'];
            return $ded ? $ded->amount : 0;
        });

        return (object)[
            'shares' => $shares,
            'membership' => $membership,
            'investments' => $withdrawals->sum('amount')
        ];
    }

    /**
     * @param ShareHolder $holder
     * @return array
     * @throws ClientInvestmentException
     */
    protected function shares(ShareHolder $holder)
    {
        $redeem = new Redeem($holder);

        return $redeem->massRedeem(carbon(static::DATE));
    }

    /**
     * @param ClientInvestment $investment
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     */
    protected function createWithdrawalApproval(ClientInvestment $investment)
    {
        $payload = [
            'description' => 'Withdrawal',
            'narration' => 'Closing coop accounts',
            'callback' => 'on',
            'deduct_penalty' => 1,
            'type' => 'withdrawal'
        ];

        $approval = ClientTransactionApproval::create([
            'transaction_type' => 'withdrawal',
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'awaiting_stage_id' => 1,
            'sent_by' => 1,
            'payload' => $payload
        ]);

        $premature = $investment->maturity_date->gt(carbon(static::DATE));

        ClientInvestmentInstruction::create([
            'approval_id' => $approval->id,
            'type_id' => 2,
            'filled_by' => 1,
            'amount_select' => 'principal_interest',
            'withdraw_type' => 'withdraw',
            'withdrawal_stage' => $premature ? 'premature' : 'mature',
            'due_date' => $premature ? carbon(static::DATE) : $investment->maturity_date,
            'reason' => 'Closing coop accounts'
        ]);

        return $approval;
    }

    protected function report($clients)
    {
        $report = $clients->map(function ($client) {
            return new EmptyModel([
                'code' => $client->client->client_code,
                'Name' => \Cytonn\Presenters\ClientPresenter::presentFullNames($client->client->id),
                'Investments' => $client->investments,
                'Shares' => $client->shares,
                'Membership' => $client->membership,
                'Total' => $client->investments + $client->membership + $client->shares,
                'Cash Bal Before' => $client->balance_before,
                'Cash Bal After' => $this->balance($client->client)
            ]);
        });

        $fname = 'Withdrawn Coop Clients';

        Excel::fromModel($fname, $report)->store('xlsx');

        $this->sendExcel(
            '[CRIMS] ' . $fname,
            'PFA list of coop clients auto-withdrawn',
            $fname,
            systemEmailGroups(['operations'])
        );
    }


    protected function balance(Client $client)
    {
        $p_bal = $this->products->sum(function ($product) use ($client) {
            return ClientPayment::balance($client, $product, null, null, Carbon::today());
        });

        $e_bal = ClientPayment::balance($client, null, null, $this->entity, Carbon::today());

        return $p_bal + $e_bal;
    }

    protected function fm()
    {
        if (!$this->fm) {
            $this->fm = FundManager::find(2);
        }

        return $this->fm;
    }
}
