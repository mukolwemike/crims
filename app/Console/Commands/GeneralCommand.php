<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class GeneralCommand extends Command
{
    protected $signature = 'cytonn:general {user_id?}';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $user = $id = User::find($this->argument('user_id'));

        $out = (new ClientInvestment())->whereHas(
            'withdrawals',
            function ($withdrawal) {
                return $withdrawal->where('date', '2017-11-30');
            }
        )
            ->get()
            ->map(
                function ($investment) {
                    $c = new EmptyModel();
                    $c->{'Client Code'}     = $investment->client->client_code;
                    $c->{'Name'}            = ClientPresenter::presentJointFullNames($investment->client_id);
                    if ($investment->client->clientType->name =='corporate') {
                        $c->{'First Name'}  = $investment->client->contact_person_fname;
                    }
                    $c->{'Invested Date'}   = $investment->invested_date;
                    $c->{'Maturity Date'}   = $investment->maturity_date;
                    $c->{'Amount'}          = $investment->amount;
                    $c->{'Interest Rate'}   = $investment->interest_rate;

                    return $c;
                }
            )
            ->groupBy('investment_id')
            ->filter(
                function ($investmentGroup) {
                    return $investmentGroup->count() == 2;
                }
            );

        if ($out->count()) {
            $fileName = 'Investments That Have 2 Interest Payments on 30th Nov';
            Excel::fromModel($fileName, $out)->store('xlsx');
            $this->mailResult(storage_path('exports/'.$fileName.'.xlsx'), $user);

            return true;
        } else {
            $this->info('No records found');
            return true;
        }
    }

    private function mailResult($filePath, $user = null)
    {
        $mailer = new GeneralMailer();
        if ($user) {
            $email = $user->email;
        } else {
            $email = [ 'mchaka@cytonn.com'];
        }
        $mailer->to($email);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Investments That Have 2 Interest Payments on 30th Nov');
        $mailer->file($filePath);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Here is the report you requested.');

        \File::delete($filePath);
    }
}
