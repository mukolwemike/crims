<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\USSD;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\Investment\UssdTransactionLog;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Clients\ClientRepository;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class DigitalTransactionSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:digital_transactions {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the contact details of the digital transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $start = Carbon::parse($this->argument('start'))->startOfDay();

        $end = Carbon::parse($this->argument('end'))->endOfDay();

        $report = $this->getTransactions($start, $end);

        $inactiveClients = $this->getClients();

        $investedClients = $this->getInvestedClients($start, $end);

        $reportArray = [
            'Incomplete Transactions' => $report,
            'Uninvested Clients' => $inactiveClients,
            'Invested Clients' => $investedClients
        ];

        $fileName = 'Digital Clients Summary - ' . $end->toDateString();

        ExcelWork::generateAndStoreMultiSheet($reportArray, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached a report on the digital clients transactions summary for the period between 
            ' . $start->toDateString() . " and " . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return UssdTransactionLog[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getTransactions(Carbon $start, Carbon $end)
    {
        $transactions = UssdTransactionLog::where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->get();

        return $transactions->map(function (UssdTransactionLog $transactionLog) {
            return [
                'Date' => $transactionLog->created_at,
                'Name' => $transactionLog->name,
                'Phone' => $transactionLog->phone,
                'Email' => $transactionLog->email
            ];
        });
    }

    /**
     * @return Client[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getClients()
    {
        $clients = (new ClientRepository())->getDigitalClientsWithNoInvestments()->get();

        return $clients->map(function (Client $client) {
            $filledApplication = $this->getLatestApplication($client);

            if ($filledApplication) {
                $channel = $filledApplication->channel;

                $product = $filledApplication->product;

                $unitFund = $filledApplication->unitFund;

                $recipient = $filledApplication->commissionRecipient;
            } else {
                $channel = $product = $unitFund = $recipient = null;
            }

            $latestFa = $unitFund ? $client->getLatestFA('units') : $client->getLatestFA();

            return [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Email' => implode(", ", $client->getContactEmailsArray()),
                'Phone' => implode(", ", $client->getContactPhoneNumbersArray()),
                'Channel' => $channel ? $channel->name : '',
                'Last Application Date' => $filledApplication ? Carbon::parse($filledApplication->created_at)->toDateString() : '',
                'Product' => $product ? $product->name : '',
                'Unit Fund' => $unitFund ? $unitFund->name : '',
                'Application FA' => $recipient ? $recipient->name : '',
                'Client Latest FA' => $latestFa ? $latestFa->name : ''
            ];
        });
    }

    /**
     * @param Client $client
     * @return ClientFilledInvestmentApplication|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null
     */
    private function getLatestApplication(Client $client)
    {
        return ClientFilledInvestmentApplication::whereHas('application', function ($q) use ($client) {
            $q->where('client_id', $client->id);
        })->whereIn('channel_id', [1, 2, 3])->latest()->first();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    private function getInvestedClients(Carbon $start, Carbon $end)
    {
        $clients = Client::where(function ($q) {
            $q->has('unitFundPurchases')->orHas('unitHoldings')
                ->orHas('investments')->orHas('portfolioInvestments')
                ->orHas('shareHolders')->orHas('clientTransactionApprovals');
        })->whereHas('applications', function ($q) use ($start, $end) {
            $q->whereHas('form', function ($q) use ($start, $end) {
                $q->createdBetween($start, $end)->whereIn('channel_id', [1, 2, 3]);
            });
        })->get();

        return $clients->map(function (Client $client) {
            $filledApplication = $this->getLatestApplication($client);

            if ($filledApplication) {
                $channel = $filledApplication->channel;

                $product = $filledApplication->product;

                $unitFund = $filledApplication->unitFund;

                $recipient = $filledApplication->commissionRecipient;
            } else {
                $channel = $product = $unitFund = $recipient = null;
            }

            $latestFa = $unitFund ? $client->getLatestFA('units') : $client->getLatestFA();

            return [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Email' => implode(", ", $client->getContactEmailsArray()),
                'Phone' => implode(", ", $client->getContactPhoneNumbersArray()),
                'Channel' => $channel ? $channel->name : '',
                'Last Application Date' => $filledApplication ? Carbon::parse($filledApplication->created_at)->toDateString() : '',
                'Product' => $product ? $product->name : '',
                'Unit Fund' => $unitFund ? $unitFund->name : '',
                'Application FA' => $recipient ? $recipient->name : '',
                'Client Latest FA' => $latestFa ? $latestFa->name : ''
            ];
        });
    }
}
