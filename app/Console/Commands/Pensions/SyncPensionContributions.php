<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Pensions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Unitization\Pensions\PensionManager;
use Illuminate\Console\Command;

class SyncPensionContributions extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:sync_pension_contributions {fund?} {date?} {client?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize the pension member contributions for a specified fund';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $fund = UnitFund::findOrFail($this->argument('fund'));

        $date = $this->argument('date');

        $date = $date ? Carbon::parse($date)->startOfDay() : null;

        $client = $this->argument('client');

        $client = $client ? Client::findOrFail($client) : null;

        $test = new PensionManager($fund, $date, $client);

        $test->syncContributions();
    }
}
