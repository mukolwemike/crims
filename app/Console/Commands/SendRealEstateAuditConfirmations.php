<?php

namespace App\Console\Commands;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealEstateAuditConfirmation;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Handlers\ExceptionHandler;
use Exception;
use Illuminate\Console\Command;

class SendRealEstateAuditConfirmations extends Command
{
    protected $signature = 'audit:re-confirmations';

    const TEST = false;

    protected $description = 'Send audit confirmations to clients';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $batchId = 5;

        $batch = (new RealEstateAuditConfirmation())->where('batch', $batchId)
            ->orderBy('id', 'ASC')
            ->whereNull('sent')
            ->get();

        $batch->each(
            function ($item) {
                try {
                    $this->send($item);
                } catch (Exception $exception) {
                    app(ExceptionHandler::class)->reportException($exception);
                }
            }
        );

        return 0;
    }

    private function send($item)
    {
        $this->info('Sending item id ' . $item->id);

        $client = Client::where('client_code', $item->client_code)->first();

        if (is_null($client)) {
            $this->error('Could not find code ' . $item->client_code);
            return;
        }

        $audit_date = new Carbon('2018-12-31');

        $sender = User::where('email', 'lmugo@cytonn.com')->first();

        $confirmation = \PDF::loadView('clients.audit.real_estate_confirmation_letter', [
            'audit_date' => $audit_date,
            'client' => $client,
            'house_number' => $item->house_number,
            'sender' => $sender,
            'product' => $item->product,
            'spv' => $item->spv
        ])->output();

        $mailer = new \Cytonn\Mailers\RealEstateAuditConfirmation();

        $mailer->sendToClient($client, $confirmation, $sender, $audit_date, $item, static::TEST);

        if (!static::TEST) {
            $item->sent = true;

            $item->save();
        }
        return;
    }
}
