<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Notification;
use Cytonn\Authorization\Permission;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStage;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStep;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalType;
use Illuminate\Console\Command;

class SeedApprovalTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approval-types:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd('disabled');

        $this->types();
        $this->steps();
        $this->tagStep();
        $this->notifications();
    }

    private function types()
    {
        $types = ClientTransactionApproval::pluck('transaction_type')->unique();

        foreach ($types as $type) {
            if (!ClientTransactionApprovalType::where('slug', $type)->exists()) {
                ClientTransactionApprovalType::create(
                    [
                    'name' => ucfirst(str_replace('_', ' ', $type)),
                    'slug' => $type
                    ]
                );
            }
        }

        $this->info("Added approval types");
    }

    private function tagStep()
    {
        $stage = ClientTransactionApprovalStage::orderBy('weight', 'ASC')->first();

        ClientTransactionApproval::doesntHave('steps')->get()
            ->each(
                function ($appro) use ($stage) {
                    $appro->update(['awaiting_stage_id' => $stage->id]);
                }
            );

        $this->info("Tagged next step");
    }

    private function steps()
    {
        $this->info("Adding approvals step");

        $stage = ClientTransactionApprovalStage::where('applies_to_all', true)->orderBy('weight', 'ASC')->first();

        ClientTransactionApproval::where('approved', 1)
            ->doesntHave('steps')
            ->each(
                function ($approval) use ($stage) {
                    ClientTransactionApprovalStep::create(
                        [
                        'approval_id' => $approval->id,
                        'user_id' => $approval->approved_by,
                        'stage_id' => $stage->id
                        ]
                    );
                }
            );

        $this->info("All approved transactions have next step");
    }

    private function notifications()
    {
        $this->info("Migrating notifications to new format");

        $notifications = Notification::whereNotNull('target_model')->get();

        $notifications->each(function (Notification $notification) {
            $permission = Permission::find($notification->model_id);

            (new \Cytonn\Notifier\Internal\Notification($notification))->subscribeByPermission($permission);
        });

        $this->info("Done migrating notifications");
    }
}
