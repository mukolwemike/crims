<?php

namespace App\Console\Commands;

use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Illuminate\Console\Command;

class UpdateUnitFundInstructionsApprovals extends Command
{
    protected $signature = 'crims:update_instructions_approvals';

    protected $description = 'Updating missing approvals on unit funds';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->updatePurchasesInstructions();

        $this->updateSalesInstructions();

        dd('done');
    }

    private function updatePurchasesInstructions()
    {
        $instructions = UnitFundInvestmentInstruction::whereNull('approval_id')->get();

        foreach ($instructions as $instruction) {
            if ($instruction->purchase) {
                $id = $instruction->purchase->approval_id;

                $instruction->approval_id = $id;

                $instruction->save();
            }
        }
    }

    private function updateSalesInstructions()
    {
        $instructions = UnitFundInvestmentInstruction::whereNull('approval_id')->get();

        foreach ($instructions as $instruction) {
            if ($instruction->sale) {
                $id = $instruction->sale->approval_id;

                $instruction->approval_id = $id;

                $instruction->save();
            }
        }
    }
}
