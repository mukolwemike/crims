<?php

namespace App\Console\Commands\System;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Authorization\Role;
use Cytonn\Mailers\GeneralMailer;
use Illuminate\Console\Command;

class UserAccessExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'users:access';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export users and their access roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $users = User::orderBy('active', 'DESC')->get()
            ->each(
                function (User $user) {
                    $user->all_roles = implode(", ", $user->roles()->get()->lists('name'));
                }
            );

        $roles = Role::all()
            ->each(
                function (Role $role) {
                    $role->permission_access = $role->permissions;
                }
            );

        $output = \PDF::loadView('users.reports.access', ['users' => $users, 'roles' => $roles])->output();

        $this->send($output);

        return;
    }

    private function send($file)
    {
        $mailer = new GeneralMailer();
        $mailer->from('support@cytonn.com');
        $mailer->to('molukaka@cytonn.com');
        $mailer->cc('gweru@cytonn.com');
//        $mailer->bcc(config('system.administrators'));
        $mailer->subject('Crims Users & Access as at ' . Carbon::today()->toFormattedDateString());
        $mailer->dataFile = $file;
        $mailer->dataFileDetails = 'Crims Users Report.pdf';
        $mailer->sendGeneralEmail('Please find attached a report detailing the users and their levels of 
        access in the system');
    }
}
