<?php

namespace App\Console\Commands\System;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplication;
use Illuminate\Console\Command;

class CleanPhones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:clean-phones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'removes spaces on tables with client phone numbers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->cleanFilledInvestmentsApplications();

        $this->cleanInvestmentsApplications();

//        $this->cleanClients();
    }

    private function cleanFilledInvestmentsApplications()
    {
        $applications = ClientFilledInvestmentApplication::all();

        foreach ($applications as $application) {
            $office = !is_null($application->telephone_office) ? str_replace(' ', '', $application->telephone_office) : null;
            $home = !is_null($application->telephone_home) ? str_replace(' ', '', $application->telephone_home) : null;

            $application->telephone_office = $office;
            $application->telephone_home = $home;

            $application->save();
        }
    }

    private function cleanInvestmentsApplications()
    {
        $applications = ClientInvestmentApplication::all();

        foreach ($applications as $application) {
            $phone = !is_null($application->phone) ? str_replace(' ', '', $application->phone) : null;
            $office = !is_null($application->telephone_office) ? str_replace(' ', '', $application->telephone_office) : null;
            $home = !is_null($application->telephone_home) ? str_replace(' ', '', $application->telephone_home) : null;

            $application->phone = $phone;
            $application->telephone_office = $office;
            $application->telephone_home = $home;

            $application->save();
        }
    }

    private function cleanClients()
    {
        $clients = Client::all();

        foreach ($clients as $client) {
            $phone = !is_null($client->phone) ? str_replace(' ', '', $client->phone) : null;
            $office = !is_null($client->telephone_office) ? str_replace(' ', '', $client->telephone_office) : null;
            $home = !is_null($client->telephone_home) ? str_replace(' ', '', $client->telephone_home) : null;

            $client->phone = $phone;
            $client->telephone_office = $office;
            $client->telephone_home = $home;

            $client->save();
        }
    }
}
