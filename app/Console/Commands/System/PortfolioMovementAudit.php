<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Portfolio\Summary\Analytics;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class PortfolioMovementAudit extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:portfolio_movement_audit {fundmanager} {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report on portfolio movement audit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->generateReport();
    }

    public function generateReport()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $fundManager = FundManager::find($this->argument('fundmanager'));

        $report = array();

        $subStart = $start->copy()->subDay();

        $addEnd = $end->copy()->addDay();

        $report['Opening ' . $subStart->format('M d Y')] =
            $this->getBalances($fundManager, $subStart->toDateString());

        $report['Opening ' . $start->format('M d Y')] =
            $this->getBalances($fundManager, $start->toDateString());

        $report['Monthly Movement - All'] = $this->getMonthlyMovement($fundManager);

        $report['Repayments'] = $this->getRepayments($fundManager);

        $report['Withdrawals'] = $this->getWithdrawals($fundManager);

        $report['Rollovers'] = $this->gerRollovers($fundManager);

        $report['Closing ' . $end->format('M d Y')] =
            $this->getBalances($fundManager, $end->toDateString());

        $report['Closing ' . $addEnd->format('M d Y')] =
            $this->getBalances($fundManager, $addEnd->toDateString());

        $fileName = 'Portfolio Investments Movement Report';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        $userId = $this->argument('user_id');

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the portfolio investments movement report')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    public function gerRollovers($fundManager)
    {
        $rollovers = DepositHolding::fundManager($fundManager)->whereHas('parent', function ($q) {
        })->get();

        return $rollovers->map(function ($investment) {
            $parent = $investment->parent;

            return [
                'Invested Date' => $investment->invested_date,
                'Security' => $investment->security->name,
                'Principal Amount' => $investment->amount,
                'Maturity Date' => $investment->maturity_date,
                'Interest Rate' => $investment->interest_rate,
                'Type' => ($investment->type_id) ? $investment->type->name : 'Undefined',
                'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                'Rolled' => BooleanPresenter::presentYesNo($investment->rolled),
                'Source Investment Amount' => $parent->amount,
                'Source Invested Date' => $parent->invested_date,
                'Source Maturity Date' => $parent->maturity_date,
            ];
        });
    }

    public function getRepayments($fundManager)
    {
        return PortfolioInvestmentRepayment::whereHas('investment', function ($q) use ($fundManager) {
            $q->fundManager($fundManager);
        })->get()->map(function ($repayment) {
            return [
                'Date' => $repayment->date,
                'Security' => $repayment->investment->security->name,
                'Repayment Amount' => $repayment->amount,
                'Principal Amount' => $repayment->investment->amount,
                'Invested Date' => $repayment->investment->invested_date,
                'Maturity Date' => $repayment->investment->maturity_date,
                'Interest Rate' => $repayment->investment->interest_rate,
                'Type' => $repayment->type->name,
                'Narrative' => $repayment->narrative
            ];
        });
    }

    public function getWithdrawals($fundManager)
    {
        return DepositHolding::active(false)->fundManager($fundManager)->get()
            ->map(function ($investment) {
                return [
                    'Withdrawal Date' => $investment->withdrawal_date,
                    'Withdrawal Amount' => $investment->withdraw_amount,
                    'Security' => $investment->security->name,
                    'Principal Amount' => $investment->amount,
                    'Invested Date' => $investment->invested_date,
                    'Maturity Date' => $investment->maturity_date,
                    'Interest Rate' => $investment->interest_rate,
                    'Type' => ($investment->type_id) ? $investment->type->name : 'Undefined',
                    'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                    'Rolled' => BooleanPresenter::presentYesNo($investment->rolled)
                ];
            });
    }

    public function getBalances($fundManager, $date)
    {
        $date = Carbon::parse($date);

        $investors = PortfolioSecurity::where('fund_manager_id', $fundManager->id)->get();

        $reportArray = array();

        foreach ($investors as $investor) {
            $analytics = new Analytics($fundManager, $date);
            $analytics->setCurrency(Currency::find(1));
            $analytics->setPortfolioInstitution($investor);

            $marketValue = $analytics->marketValue();

            $grossInterest = $analytics->grossInterest();

            $costValue = $analytics->costValue();

            $reportArray[] = [
                'Name' => $investor->name,
                'Date' => $date->toDateString(),
                'Principal Balance (Cost Value)' => $costValue,
                'Interest (Gross)' => $grossInterest,
                'Interest (Net)' => ($marketValue - $costValue),
                'Balance (Market Value)' => $marketValue
            ];
        }

        return $reportArray;
    }

    public function getMonthlyMovement($fundManager)
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'))->endOfDay();

        $months = getDateRange($start, $end);

        $investorsArray = array();

        $investors = PortfolioSecurity::where('fund_manager_id', $fundManager->id)->get();

        foreach ($investors as $investor) {
            $monthArray = array();

            $monthArray['Name'] = $investor->name;

            foreach ($months as $month) {
                $startOfMonth = Carbon::parse($month);

                $endMonth = $startOfMonth->copy()->endOfMonth();

                $newInvestments = DepositHolding::investedBetweenDates($startOfMonth, $endMonth)
                    ->fundManager($fundManager)
                    ->forSecurity($investor);

                $allNew = $newInvestments->get()->sum(function ($investment) use ($endMonth) {
                    $account = $investment->custodialAccount();

                    $currency = ($account) ? $account->currency : Currency::find(1);

                    return convert($investment->amount, $currency, Carbon::parse($endMonth));
                });

                $withdrawals = DepositHolding::where('withdrawal_date', '>=', $startOfMonth)
                    ->where('withdrawal_date', '<=', $endMonth)
                    ->fundManager($fundManager)
                    ->forSecurity($investor)
                    ->get()->sum(function ($investment) use ($endMonth) {
                        $account = $investment->custodialAccount();

                        $currency = ($account) ? $account->currency : Currency::find(1);

                        return convert($investment->withdraw_amount, $currency, Carbon::parse($endMonth));
                    });

                $active = $interestAccrued = $interestRepayments = $principalRepayments = $fullRepayments = 0;

                DepositHolding::activeBetweenDates($startOfMonth, $endMonth)
                    ->fundManager($fundManager)
                    ->forSecurity($investor)
                    ->each(function ($investment) use (
                        $startOfMonth,
                        $endMonth,
                        &$interestAccrued,
                        &$interestRepayments,
                        &$principalRepayments,
                        &$fullRepayments,
                        &$active
                    ) {
                        $account = $investment->custodialAccount();

                        $currency = ($account) ? $account->currency : Currency::find(1);

                        $active += convert($investment->amount, $currency, Carbon::parse($endMonth));

                        $started = convert($investment->repo
                            ->grossInterestBeforeDeductions($startOfMonth, false), $currency, $startOfMonth);

                        $ended = convert($investment->repo
                            ->grossInterestBeforeDeductions($endMonth, true), $currency, $endMonth);

                        $interestAccrued += ($ended - $started);

                        $repayments = $investment->repayments()->where('date', '>=', $startOfMonth)
                            ->where('date', '<=', $endMonth);

                        $interestRepayments += $repayments->where('type_id', 1)->get()
                            ->sum(
                                function ($repayment) use ($currency) {
                                    return convert($repayment->amount, $currency, Carbon::parse($repayment->date));
                                }
                            );

                        $principalRepayments += $repayments->where('type_id', 2)->get()
                            ->sum(
                                function ($repayment) use ($currency) {
                                    return convert($repayment->amount, $currency, Carbon::parse($repayment->date));
                                }
                            );

                        $fullRepayments += $repayments->where('type_id', 3)->get()
                            ->sum(
                                function ($repayment) use ($currency) {
                                    return convert($repayment->amount, $currency, Carbon::parse($repayment->date));
                                }
                            );
                    });

                $monthArray[$startOfMonth->format('M, Y') . ' - New'] = $allNew;
                $monthArray[$startOfMonth->format('M, Y') . ' - Active'] = $active;
                $monthArray[$startOfMonth->format('M, Y') . ' - Interest Accrued'] = $interestAccrued;
                $monthArray[$startOfMonth->format('M, Y') . ' - Interest Repayments'] = $interestRepayments;
                $monthArray[$startOfMonth->format('M, Y') . ' - Principal Repayments'] = $principalRepayments;
                $monthArray[$startOfMonth->format('M, Y') . ' - Full Repayments'] = $fullRepayments;
                $monthArray[$startOfMonth->format('M, Y') . ' - Withdrawals'] = $withdrawals;
            }

            $investorsArray[] = $monthArray;
        }

        return $investorsArray;
    }
}
