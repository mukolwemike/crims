<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\System;

use Cytonn\SearchOperations\ElasticSearchManager;
use Illuminate\Console\Command;

class DailyElasticSearchReindex extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'system:refresh_elasticsearch_index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run daily reindex of elasticsearch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ElasticSearchManager::performReindex();
    }
}
