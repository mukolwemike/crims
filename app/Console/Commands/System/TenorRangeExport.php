<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class TenorRangeExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:tenor_range {start} {end} {fundmanager} {user_id} {type_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the range of tenors for the portfolio loans';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fundManager = FundManager::findOrFail($this->argument('fundmanager'));

        $start = Carbon::parse($this->argument('start'))->startOfDay();

        $end = Carbon::parse($this->argument('end'))->endOfDay();

        $type = ($this->argument('type_id')) ? $this->argument('type_id') : 1;

        $report = $this->getReport($fundManager, $start, $end, $type);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'Loans Tenor Range - ' . $date;

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        $userId = $this->argument('user_id');

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the loans tenor report')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    public function getReport(FundManager $fundManager, Carbon $start, Carbon $end, $type)
    {
        $depositsArray = array();

        $deposits = DepositHolding::fundManager($fundManager)
            ->activeOnDate($end)
            ->where('maturity_date', '>=', $end)
//            ->where(function($q) use ($start, $end)
//            {
//                $q->where(function ($q) use ($start, $end)
//                {
//                    $q->columnBetween('invested_date', $start, $end);
//                })->orWhere(function ($q) use ($start, $end)
//                {
//                    $q->columnBetween('maturity_date', $start, $end);
//                })->orWhere(function ($q) use ($start, $end)
//                {
//                    $q->whereNotNull('withdrawal_date')->columnBetween('withdrawal_date', $start, $end);
//                });
//            })
            ->get();

        foreach ($deposits as $deposit) {
            $tenor = $this->getTenor($deposit, $end, $type);

            $depositsArray[$tenor['range']][] = [
                'ID' => $deposit->id,
                'Portfolio Security' => $deposit->security->name,
                'Principal Amount' => AmountPresenter::currency($deposit->amount),
                'Value as at End' => AmountPresenter::currency(
                    $deposit->repo->getKesConvertedValue(
                        $deposit->repo->getTotalValueOfAnInvestmentAsAtDate($end, true)
                    )
                ),
                'Invested Date' => $deposit->invested_date,
                'Maturity Date' => $deposit->maturity_date,
                'Withdrawal Date' => $deposit->withdrawal_date,
                'Tenor in Days' => $tenor['days'],
                'Type' => ($deposit->type) ? $deposit->type->name : 'Undefined',
                'Interest Rate' => $deposit->interest_rate,
                'Rolled' => BooleanPresenter::presentYesNo($deposit->rolled),
                'Withdrawn' => BooleanPresenter::presentYesNo($deposit->withdrawn),
                'Withdrawal Amount' => ($deposit->withdraw_amount)
                    ? AmountPresenter::currency($deposit->withdraw_amount) : '',
            ];
        }

        return $depositsArray;
    }

    public function getTenor($deposit, $end, $type)
    {
        $investedDate = ($type == 1) ? Carbon::parse($deposit->invested_date) : $end;

        $maturityDate = Carbon::parse($deposit->maturity_date);

        $withdrawalDate = ($deposit->withdrawal_date) ? Carbon::parse($deposit->withdrawal_date) : null;

        if ($withdrawalDate && $withdrawalDate <= $maturityDate) {
            $days = $withdrawalDate->diffInDays($investedDate);
        } else {
            $days = $maturityDate->diffInDays($investedDate);
        }

        if ($days <= 90) {
            $range = 'Below 3 Months';
        } elseif ($days > 90 && $days <= 180) {
            $range = '3 to 6 Months';
        } elseif ($days > 180 && $days <= 270) {
            $range = '6 to 9 Months';
        } elseif ($days > 270 && $days <= 365) {
            $range = '9 to 12 Months';
        } elseif ($days > 365) {
            $range = 'Above 12 Months';
        } else {
            $range = "Undefined";
        }

        return [
            'days' => $days,
            'range' => $range
        ];
    }
}
