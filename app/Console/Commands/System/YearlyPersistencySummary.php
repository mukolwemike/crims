<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Summary\Analytics;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class YearlyPersistencySummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature =
        'crims:yearly_persistency {fundmanager} {end} {start?} {--currency=} {--product=} {--months=12}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report on yearly persistency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fundManager = FundManager::findOrFail($this->argument('fundmanager'));

        if ($this->argument('start')) {
            $start = Carbon::parse($this->argument('start'))->startOfDay();
        } else {
            $investment = ClientInvestment::fundManager($fundManager)->orderBy('invested_date', 'ASC')->first();

            $start = Carbon::parse($investment->invested_date)->startOfMonth();
        }

        $months = $this->option('months');

        $currency = ($this->option('currency')) ? Currency::findOrFail($this->option('currency')) : null;

        $product = ($this->option('product')) ? Product::findOrFail($this->option('product')) : null;

        $end = Carbon::parse($this->argument('end'))->endOfDay();

        $clients = $this->getReport($start, $end, $fundManager, $currency, $product, $months);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'Persistency Report - ' . $date;

        ExcelWork::generateAndStoreSingleSheet($clients, $fileName);

        $email = [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the persistency report for ' . $fundManager->name . ' for the period '
                . $start->toDateString() . ' to '
                . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /*
     * Generate the report
     */
    private function getReport(Carbon $start, Carbon $end, $fundManager, $currency, $product, $months)
    {
        $reportArray = array();

        $startOfMonth = $start->endOfMonth();

        $endOfYear = $startOfMonth->copy()->addMonthsNoOverflow($months);

        while ($endOfYear <= $end) {
            $analytics = new Analytics($fundManager, $endOfYear);

            if ($currency) {
                $analytics->setBaseCurrency($currency);
            }

            if ($product) {
                $analytics->setProduct($product);
            }

            $values = $analytics->persistenceValues($startOfMonth, $endOfYear);

            $reportArray[] = [
                'Month' => $startOfMonth->format('M Y'),
                'Start Date' => $startOfMonth->toDateString(),
                'End Date' => $endOfYear->toDateString(),
                'Minimum' => AmountPresenter::currency($values['minimum']),
                'Investments At Start' => AmountPresenter::currency($values['started']),
                'Persistency' => AmountPresenter::accounting($values['persistency'])
            ];

            $startOfMonth = $startOfMonth->addMonthNoOverflow()->endOfMonth();

            $endOfYear = $startOfMonth->copy()->addMonthsNoOverflow($months);
        }

        return $reportArray;
    }
}
