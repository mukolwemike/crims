<?php

namespace App\Console\Commands\System;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Contact;
use Illuminate\Console\Command;

class CleanupStagingData extends Command
{
    protected $signature = 'crims:cleanup_staging_data';

    protected $description = 'Update clients data with fakes ones in staging';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->updateClientContactsNames();

        dd('Updated');
    }

    private function updateClientContactsNames()
    {
//        $clients = Client::all();
        $clients = ClientUser::all();

        foreach ($clients as $client) {
            $new = factory(ClientUser::class)->create();

//            $old = $client->contact;

            $client->username = $new->username;
            $client->firstname = $new->firstname;
            $client->lastname = $new->lastname;
            $client->password = $new->password;
            $client->email = $new->email;
            $client->pin = $new->pin;

            $client->save();

//            $clientUsers = $client->clientUsers;
//
//            foreach ($clientUsers as $user) {
//                $new = factory(Contact::class)->create();
//
//                $user->firstname = $new->firstname;
//                $user->lastname = $new->lastname;
//
//                $user->save();
//            }
        }
    }
}
