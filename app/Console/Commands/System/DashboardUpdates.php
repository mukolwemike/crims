<?php

namespace App\Console\Commands\System;

use App\Cytonn\Dashboard\DashboardChartsSummary;
use App\Cytonn\Models\FundManager;
use Cytonn\Dashboard\DashboardGenerator;
use Illuminate\Console\Command;

class DashboardUpdates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @throws \Exception
     * @return mixed
     */
    public function handle()
    {
        $fm = FundManager::all();

        $summary = new DashboardChartsSummary();
        $summary->setForceUpdate(true);
        $summary->runInSync(true);

        $gen = new DashboardGenerator();
        $gen->setForceUpdate(true);
        $gen->runInSync(true);

        $this->info("Starting...");

        $fm->each(function ($fm) use ($summary, $gen) {
            $this->info("Generating charts for ".$fm->name);

            $summary->fundValuation($fm);
            $gen->inflows($fm);
            $gen->withdrawals($fm);
            $gen->aum($fm);
            $gen->clients($fm);

            $this->info("Completed charts for ".$fm->name);
        });

        $this->info("Completed Investment stats");

        $summary->reValuationChart();

        $this->info("Completed RE charts");
    }
}
