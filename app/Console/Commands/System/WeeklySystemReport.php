<?php

namespace App\Console\Commands\System;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use App\Mail\Mail;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Models\CrimsAdminLoginTrail;
use Cytonn\Models\CrimsClientLoginTrail;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Console\Command;

class WeeklySystemReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:weekly-report {--start=?} {--end=?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        memoryLimit('4096M');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::today()->subDays(7);
        if ($this->option('start') != '?') {
            $start = Carbon::parse($this->option('start'))->startOfDay();
        }

        $end = Carbon::today()->subDay()->endOfDay();
        if ($this->option('end') != '?') {
            $end = Carbon::parse($this->option('end'))->endOfDay();
        }

        $transactions = ClientTransactionApproval::columnBetween('approved_on', $start->copy(), $end->copy())
            ->oldest('approved_on')
            ->get()
            ->map(
                function (ClientTransactionApproval $approval) {
                    $client = $approval->client;

                    return [
                        'Code' => $client ? $client->client_code : '',
                        'Client' => \Cytonn\Presenters\ClientPresenter::presentJointFullNames($approval->client_id),
                        'Transaction' => $approval->present()->type,
                        'Sent By' => UserPresenter::presentFullNamesNoTitle($approval->sent_by),
                        'Approved By' => UserPresenter::presentFullNamesNoTitle($approval->approved_by),
                        'Approved On' => DatePresenter::formatFullDate($approval->approved_on)
                    ];
                }
            );

        $this->info("Found {$transactions->count()} client approvals ");

        $portfolio_trans = PortfolioTransactionApproval::columnBetween('approved_on', $start->copy(), $end->copy())
            ->get()
            ->map(
                function (PortfolioTransactionApproval $approval) {
                    $ins = $approval->institution;

                    return [
                        'Institution' => $ins ? $ins->name : '',
                        'Transaction' => ucfirst(str_replace('_', ' ', $approval->transaction_type)),
                        'Sent By' => UserPresenter::presentFullNamesNoTitle($approval->sent_by),
                        'Approved By' => UserPresenter::presentFullNamesNoTitle($approval->approved_by),
                        'Approved On' => DatePresenter::formatFullDate($approval->approved_on)
                    ];
                }
            );

        $this->info("Found {$portfolio_trans->count()} portfolio approvals ");

//        $loginAdmin = CrimsAdminLoginTrail::allowFiltering(true)
//            ->where('created_at', '>=', $start)
//            ->where('created_at', '<=', $end)
//            ->get()
//            ->groupBy('user_id')
//            ->map(
//                function ($group) {
//                    $trail = $group->first();
//
//                    $user = User::find($trail->user_id);
//
//                    return [
//                        'Name' => UserPresenter::presentFullNames($user->id),
//                        'No. of Logins' => $group->count(),
//                        'Last Login' => $group->sort(
//                            function ($first, $second) {
//                                return $first->created_at->lt($second->created_at);
//                            }
//                        )->first()->created_at
//                    ];
//                }
//            );
//
//        $this->info("Found {$loginAdmin->count()} admin logins");
//
//        $loginClient = CrimsClientLoginTrail::allowFiltering(true)
//            ->where('created_at', '>=', $start)
//            ->where('created_at', '<=', $end)
//            ->get()
////            ->filter(function ($login){
////
////                $user = ClientUser::find($login->user_id);
////
////                return $user->clients()->count() > 0;
////            })
//            ->groupBy('user_id')
//            ->map(
//                function ($group) {
//                    $trail = $group->first();
//                    $user = ClientUser::find($trail->user_id);
//
//                    return [
//                        'Name' => $user->firstname . ' ' . $user->lastname,
//                        'No of Logins' => $group->count(),
//                        'Last Login' => $group->sort(
//                            function ($first, $second) {
//                                return $first->created_at->lt($second->created_at);
//                            }
//                        )->first()->created_at
//                    ];
//                }
//            );
//
//        $this->info("Found {$loginClient->count()} client logins");

        ExcelWork::generateAndStoreMultiSheet([
            'Client Transactions' => $transactions,
            'Portfolio Transactions' => $portfolio_trans
        ], "Transactions_Report");

        $files = ['Transactions_Report'];

//        $files = [
////            'Login report.pdf' => \PDF::loadView(
////                'reports.system.weeklyreport',
////                [
////                    'start' => $end, 'end' => $end,
////                    'sections' => [
////                        'User Logins: CRIMS-Admin' => $loginAdmin,
////                        'User Logins: CRIMS-Client' => $loginClient
////                    ]
////                ]
////            )->output(),
//
//            'Client Transaction report.'
//            'Client Transaction report.pdf' => \PDF::loadView(
//                'reports.system.weeklyreport',
//                [
//                    'start' => $end, 'end' => $end,
//                    'sections' => [
//                        'Client Transactions' => $transactions
//                    ]
//                ]
//            )->output(),
//            'Portfolio transaction report.pdf' => \PDF::loadView(
//                'reports.system.weeklyreport',
//                [
//                    'start' => $end, 'end' => $end,
//                    'sections' => [
//                        'Portfolio Transactions' => $portfolio_trans
//                    ]
//                ]
//            )->output()
//        ];

        $this->info("Sending...");

        $this->mail($files, $start, $end);

        $this->info("Done...");
    }

    protected function mail($files, $start, $end)
    {
        $s = $start->toFormattedDateString();
        $e = $end->toFormattedDateString();

        Mail::compose()
            ->to(systemEmailGroups(['operations_mgt', 'operations_admins']))
            ->bcc(systemEmailGroups(['super_admins']))
            ->subject("[CRIMS] Activity Report : $s - $e")
            ->excel($files)
//            ->attachData($files)
            ->text('Please find attached the CRIMS activity report as at ' . $e)
            ->send();
    }
}
