<?php

namespace App\Console\Commands\System;

use App\Cytonn\Models\AccessLog;
use App\Cytonn\System\CrimsClientLoginTrail;
use Carbon\Carbon;
use Cytonn\Models\CrimsAdminLoginTrail;
use Cytonn\Models\CrimsAdminPageView;
use Cytonn\Models\CrimsClientPageView;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DailySystemMaintenanceCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'maintenance:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run daily system maintenance tasks';

    protected $forbidden = [
        CrimsAdminPageView::class,
        CrimsClientPageView::class,
        AccessLog::class
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->trimAccesslog();
    }

    private function trimAccesslog()
    {
        $limit = Carbon::today()->subYears(2);

        foreach ($this->forbidden as $forbidden) {
            AccessLog::whereIn('model_name', $forbidden)->forceDelete();
        }

        \DB::table('failed_jobs')->where('failed_at', '<=', Carbon::now()->subDays(7))->delete();

//        CrimsAdminLoginTrail::where('created_at', '<', $limit)->forceDelete();
//
//        CrimsClientLoginTrail::where('created_at', '<', $limit)->forceDelete();
    }
}
