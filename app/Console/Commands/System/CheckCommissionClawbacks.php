<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Commands\RegisterCommissionClawBackCommandHandler;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CheckCommissionClawbacks extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:check_commission_clawbacks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check commission clawbacks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
//        $this->updateSchedules();

        \DB::transaction(function () {
            $this->checkClawbacks();
        });
    }

    public function updateSchedules()
    {
        $handler = new RegisterCommissionClawBackCommandHandler();

        $commissions = Commission::whereHas('schedules', function ($q) {
            $q->where('date', '>=', '2018-06-20')->where('date', '<=', '2018-07-19')->clawBack(false);
        })->get();

        $dataArray = array();

        foreach ($commissions as $commission) {
            $investment = $commission->investment;

            if ($investment) {
                $unpaid = $handler->unPaidSchedules($investment, Carbon::parse('2018-06-20'))
                    ->count();

                $value = $investment->calculate()->principal();

                if ($value < 0.5) {
                    $dataArray[] = [
                        'ID' => $investment->id,
                        'Client Code' => $investment->client->client_code,
                        'Total Schedules' => $unpaid
                    ];
                }
            }
        }

        ExcelWork::generateAndExportSingleSheet($dataArray, "Schedules");
    }

    public function checkClawbacks()
    {
        $clawbacks = CommissionClawback::where('date', '>=', '2018-06-20')
            ->where('type', 'withdraw')
            ->get();

        $dataArray = array();

        $handler = new RegisterCommissionClawBackCommandHandler();

        foreach ($clawbacks as $clawback) {
            $commission = $clawback->commission;

            $principal = $commission->investment->calculate(Carbon::parse($clawback->date))->principal();

            $scheduled = $commission->schedules()->sum('amount');

            $paid = $handler->paidSchedules($commission->investment, Carbon::parse($clawback->date))
                ->sum('amount');

            $unpaid = $handler->unPaidSchedules($commission->investment, Carbon::parse($clawback->date))
                ->sum('amount');

            $clawbackAmount = $clawback->amount > $scheduled ? $scheduled : $clawback->amount;

            if ($principal < 0.5) {
                $name = 'Fully Withdrawn Investments';
                $finalClawback = $finalPaid = $finalUnpaid = "Undefined";
                $clawbackDifference = $clawbackAmount - $unpaid;

                if ($clawbackDifference == 0) {
                    $this->clearUnpaidSchedules($handler, $commission, $clawback->date);
                    $clawback->delete();
                    $finalClawback = 0;
                }

                if ($clawbackDifference > 0) {
                    $this->clearUnpaidSchedules($handler, $commission, $clawback->date);
                    $this->updateClawback($clawback, $clawbackDifference);
                    $finalClawback = $clawbackDifference;
                }

                if ($clawbackDifference < 0) {
                    $this->clearUnpaidSchedules($handler, $commission, $clawback->date);
                    $finalClawback = 0;
                    $clawback->delete();
                    $this->generateSchedule(abs($clawbackDifference), $commission, $clawback->date);
                }

                $finalPaid = $handler->paidSchedules($commission->investment, Carbon::parse($clawback->date))
                    ->sum('amount');

                $finalUnpaid2 = $commission->schedules()->clawBack(false)
                    ->where('date', '>', '2018-07-19')
                    ->sum('amount');

                $finalUnpaid = $handler->unPaidSchedules($commission->investment, Carbon::parse($clawback->date))
                    ->sum('amount');
            } else {
                $name = 'Not Fully Withdrawn Investments';
                $finalClawback = $clawback->amount;
                $finalPaid = $paid;
                $finalUnpaid = $unpaid;
                $finalUnpaid2 = $commission->schedules()->clawBack(false)
                    ->where('date', '>', '2018-07-19')->sum('amount');
            }

            $clawbackChange = $finalClawback - $clawback->amount;
            $paidChange = $finalPaid - $paid;
            $unpaidChange = $finalUnpaid - $unpaid;

            $data = [
                'ID' => $clawback->id,
                'Date' => $clawback->date,
                'Comm' => $clawback->commission_id,
                'Inv Id' => $commission->investment_id,
                'Client' => $commission->investment->client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($commission->investment->client_id),
                'Starting Principal' => $commission->investment->amount,
                'Final Principal' => $principal,
                'Scheduled Commission' => $scheduled,
                'Current Clawback' => $clawback->amount,
                'Final Clawback' => $finalClawback,
                'Change in Clawback' => $clawbackChange,
                'Paid Schedules' => $paid,
                'Final Paid' => $finalPaid,
                'Change in Paid' => $paidChange,
                'Unpaid Schedules' => $unpaid,
                'Final Unpaid' => $finalUnpaid,
                'Final Unpaid (2018-07-19)' => $finalUnpaid2,
                'Change in Upaid' => $unpaidChange
            ];

            if ($clawbackChange != 0 || $paidChange != 0 || $unpaidChange != 0) {
                $dataArray["Clawbacks To Be Changed"][] = $data;
            } else {
                $dataArray[$name][] = $data;
            }
        }

        $fileName = 'Clawbacks';

        ExcelWork::generateAndStoreMultiSheet($dataArray, $fileName);

        Mailer::compose()
            ->to([])
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the clawbacks report')
            ->excel([$fileName])
            ->send();
    }

    private function clearUnpaidSchedules($handler, $commission, $date)
    {
        $handler->unPaidSchedules($commission->investment, Carbon::parse($date))
            ->each(function ($schedule) {
                $schedule->claw_back = true;
                $schedule->save();
            });
    }

    private function updateClawback($clawback, $amount)
    {
        $clawback->amount = $amount;

        $clawback->save();
    }

    private function generateSchedule($amount, $commission, $date)
    {
        CommissionPaymentSchedule::create([
            'commission_id' => $commission->id,
            'amount' => $amount,
            'first' => true,
            'full' => true,
            'date' => $date,
            'description' => 'Full commission payment'
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function work()
    {
        $instructions = ClientInvestmentInstruction::where('type_id', 1)
            ->where('due_date', '>=', '2018-05-01')
            ->where('due_date', '<=', '2018-06-20')
            ->whereHas('approval', function ($q) {
                $q->where('transaction_type', 'rollover_investment')->where('approved', 1);
            })
            ->get();

        $this->info("Processing {$instructions->count()} instructions");

        $withdrawalArray = array();

        foreach ($instructions as $instruction) {
            $investments = $instruction->allInvestments();

            foreach ($investments as $investment) {
                $approvalData = $investment->approval->payload;

                $maturityDate = array_key_exists('maturity_date', $approvalData) ?
                    Carbon::parse($approvalData['maturity_date']) : null;

                $topupForm = $investment->approval->topupForm;

                if ($topupForm) {
                    $maturityDate = $topupForm->repo->maturityDate();
                }

                $rForm = $investment->approval->withdrawInstruction;

                if ($rForm) {
                    $maturityDate = $rForm->repo->maturityDate();
                }

                $withdrawals = $investment->withdrawals()->where('withdraw_type', 'withdrawal')
                    ->where('amount', '>', 0)
                    ->where('date', $instruction->due_date)->get();

                foreach ($withdrawals as $withdrawal) {
                    $clawback = CommissionClawback::where('withdrawal_id', $withdrawal)->first();

                    $interest = collect($investment->calculate($withdrawal->date, true)->getPrepared()->actions)
                        ->filter(function ($action) use ($instruction) {
                            return $action->date->toDateString() == $instruction->due_date->toDateString();
                        })
                        ->sum('net_interest');

                    $principal = ($withdrawal->amount - $instruction->topup_amount - $interest);

                    if (!$clawback && $maturityDate && $maturityDate->gt($withdrawal->date)) {
                        $tenor = 'None';
                        $amount = '';

                        if ($maturityDate) {
                            $tenor = $maturityDate->diffInDays($withdrawal->date);

                            $amount = $principal * $investment->commission->rate * $tenor / (100 * 365);

                            if ($amount < 0) {
                                $amount = 0;
                            }

                            $clawbackData['Desired Clawback Amount'] = (float)$amount;

//                            $clawback = CommissionClawback::create([
//                                    'amount' => $amount,
//                                    'commission_id' => $investment->commission->id,
//                                    'date' => $withdrawal->date,
//                                    'narration' => 'Client rollover before maturity',
//                                    'type' => 'withdraw',
//                                    'withdrawal_id' => $withdrawal->id
//                                ]
//                            );

                            $this->info("Recording...");
                        }

                        $clawbackData = [
                            'Client Name' =>
                                \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id),
                            'Client Code' => $investment->client->client_code,
                            'FA' => $investment->commission->recipient->name,
                            'Investment ID' => $investment->id,
                            'Withdrawal ID' => $withdrawal->id,
                            'Approval ID' => $investment->approval_id,
                            'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
                            'Withdrawal Date' => Carbon::parse($withdrawal->date)->toDateString(),
                            'Maturity Date' => $maturityDate ? $maturityDate->toDateString() : '',
                            'Principal' => (float)$investment->amount,
                            'Withdrawal Amount' => (float)$withdrawal->amount,
                            'Principal_used' => (float)$principal,
                            'Cl_Tenor_used' => $tenor,
                            'Com Rate Used' => $investment->commission->rate,
                            'Desired Clawback Amount' => $amount
                        ];

                        $withdrawalArray[] = $clawbackData;
                    }
                }
            }
        }

        $fileName = 'Clawbacks';

        ExcelWork::generateAndStoreSingleSheet($withdrawalArray, $fileName);

        Mailer::compose()
            ->to([])
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the clawbacks report (Final)')
            ->excel([$fileName])
            ->send();

        $this->info("Done!");
    }
}
