<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use SSOManager\Facades\SSO;

class SSOAccessRefresh extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:users_sso_access';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh access for crims users via sso';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->refreshSSOAccess();
    }

    /*
     * Refresh the access for CRIMS users via SSO
     */
    private function refreshSSOAccess()
    {
        $userChunks = User::where('active', 1)->get()->chunk(1000);

        $userChunks->each(
            function ($users) {
                $userData = SSO::syncUsers($users);

                $this->processResponse($userData);
            }
        );
    }

    /**
     * Process the response from the sync process
     */
    public function processResponse($userData)
    {
        if ($userData['verdict'] == 1) {
            return;
        }

        if (array_key_exists('invalidUsers', $userData)) {
            $this->deactivateUsers($userData['invalidUsers']);
        }

        if (array_key_exists('inactiveUsers', $userData)) {
            $this->deactivateUsers($userData['inactiveUsers']);
        }
    }

    /**
     * Deactivate the users under the supplied email
     */
    public function deactivateUsers($users)
    {
        foreach ($users as $user) {
            $user = User::where('email', $user)->first();

            if ($user) {
                $user->active = 0;

                $user->save();
            }
        }
    }
}
