<?php

namespace App\Console\Commands\System;

use App;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\CommissionRecepient;
use Illuminate\Console\Command;
use Webpatser\Uuid\Uuid;

class UUIDGenerator extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'uuid:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate UUIDs for models';

    protected $monitor = [
        Client::class,
        ClientInvestmentApplication::class,
        ClientInvestment::class,
        CommissionRecepient::class,
        App\Cytonn\Models\ClientFilledInvestmentApplication::class,
        App\Cytonn\Models\ClientTopupForm::class,
        App\Cytonn\Models\Document::class
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     * Generates UUIDs for models. The UUID is used for identification in the client app
     *
     * @return mixed
     */
    public function fire()
    {
        foreach ($this->monitor as $model) {
            \App::make($model)->whereNull('uuid')
                ->orWhere('uuid', '')
                ->get()
                ->each(
                    function ($model) {
                        $this->info('Generating '.get_class($model).' id '.$model->id);
                        $model->uuid = Uuid::generate()->string;
                        $model->save();
                    }
                );
        }

        return 0;
    }
}
