<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Investment\AdditionalCommission;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\DatePresenter;
use App\Jobs\Unitization\UnitTrustClientsContacts;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RandomExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:random_export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a random report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fund = UnitFund::findOrFail(17);
        $user = User::findOrFail(71);

        $test = new UnitTrustClientsContacts($user, $fund);
        $test->handle();

        dd("Handle");
        
        DB::transaction(function () {
            $report = $this->getReport();

            $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

            $fileName = 'Random Export - ' . $date;

            ExcelWork::generateAndStoreSingleSheet($report, $fileName);

            $email = [];

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($fileName)
                ->text('Please find attached the requested report')
                ->excel([$fileName])
                ->send();

            dd("Done");

            $path = storage_path() . '/exports/' . $fileName . '.xlsx';

            return \File::delete($path);
        });
    }

    private function getReport()
    {
        $dataArray = array();

        $holdings = UnitHolding::where('reservation_date', '>=', '2019-01-01')->active()->has('commission')->get();

        foreach ($holdings as $holding) {
            $commission = $holding->commission;

            $recipient = $commission->recipient;

            $reservationDate = Carbon::parse($holding->reservation_date);

            if ($reservationDate >= Carbon::parse('2018-12-03') && $reservationDate <= Carbon::parse('2019-01-31')) {
                $reservationDate = Carbon::parse('2019-02-01');
            }

            if ($recipient->zero_commission) {
                $rate = 0;
            } else {
                $rate = $recipient->repo->getRealEstateCommissionRate(
                    $holding->unit->project,
                    $reservationDate
                );

                $rate = $rate ? $rate->amount : 'No Commission';
            }

            $currentRate = $commission->commission_rate;

            $paidCommission = $commission->schedules()->sum('amount');

            $scheduledCommission = $commission->amount;

            $newScheduleCommission = $rate * $holding->price() / 100;

            $commissionDifference = $paidCommission - $newScheduleCommission;

            $paidClawback = $holding->advanceCommissions()->where('type_id', 2)->sum('amount');

            $clawback = $commissionDifference + $paidClawback;

            if ($clawback > 0) {
                $action = "Add Clawback";

                AdditionalCommission::create([
                    'recipient_id' => $recipient->id,
                    'date' => '2019-11-25',
                    'amount' => - $clawback,
                    'type_id' => 2,
                    'repayable' => 0,
                    'holding_id' => $holding->id,
                    'description' => "Clawback for overpaid commission"
                ]);
            } else {
                $clawback = 0;
                $action = "";
            }

            if ($currentRate != $rate) {
                $action = $action ." Update Rate";

                $commission->update([
                    'amount' => $newScheduleCommission,
                    'commission_rate' => $rate
                ]);
            }

            $dataArray[] = [
                'ID' => $holding->id,
                'Commission Id' => $holding->commission->id,
                'Client Code' => $holding->client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($holding->client_id),
                'Reservation Date' => $holding->reservation_date,
                'Project' => $holding->unit->project->name,
                'Type' => $holding->paymentPlan->name,
                'Price' => $holding->price(),
                'Recipient ID' => $commission->recipient_id,
                'Recipient' => $commission->recipient->name,
                'Amount' => $scheduledCommission,
                'New Amount' => $newScheduleCommission,
                'Total Paid' => $paidCommission,
                'Commission Difference' => $commissionDifference,
                'Current Rate' => $currentRate,
                'Correct Rate' => $rate,
                'Difference' => $currentRate - $rate,
                'Paid Clawback' => abs($paidClawback),
                'Clawback' => $clawback,
                'Action' => $action,
            ];
        }

        return $dataArray;
    }
}
