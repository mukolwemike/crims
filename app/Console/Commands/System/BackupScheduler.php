<?php

namespace App\Console\Commands\System;

use App;
use App\Cytonn\Models\Backup;
use BackupManager\Filesystems\Destination;
use BackupManager\Manager;
use Carbon\Carbon;
use Cytonn\Core\Storage\LocalStorage;
use Cytonn\Core\Storage\S3Storage;
use Cytonn\Mailers\GeneralMailer;
use Exception;
use Illuminate\Console\Command;

use Cytonn\Core\Storage\StorageInterface;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\FileNotFoundException;

/**
 * Class BackupScheduler
 *
 * @package System
 */
class BackupScheduler extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'backup:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run scheduled db backup';

    /**
     * @var
     */
    private $filesystem;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->filesystem = App::make(StorageInterface::class)->filesystem();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $manager = App::make(Manager::class);

        try {
            $this->runBackup($manager);
        } catch (Exception $e) {
            $mail = (new GeneralMailer());
            $mail->to('mchaka@cytonn.com');
            $mail->subject('CRIMS Backup');
            $mail->sendGeneralEmail('The hourly backup failed to run');

            throw $e;
        }
    }

    /**
     * Run the actual backup
     *
     * @param Manager $manager
     */
    public function runBackup(Manager $manager)
    {
        $this->info('Beginning backup');

        $filename = $this->getFilename();

        $default = \config('filesystems.default');

        $manager->makeBackup()->run(
            'mysql',
            [
            new Destination($default, $filename)
            ],
            'gzip'
        );

        //        if($default == 's3')
        //        {
        //            $this->upload($filename);
        //        }

        $this->registerBackup($filename);

        $this->deleteOldHourlyBackups();

        $this->info('Backup completed');
    }

    private function upload($filename)
    {
        $filename = $filename.'.gz';
        $start = time();

        $this->info("Uploading to s3");
        Storage::disk('backup')->putFileAs(null, new File(storage_path('backups/'.$filename)), $filename);
        $duration = time() - $start;
        $this->info("Uploaded in {$duration} seconds");
    }

    /**
     * compose the filename of the backup
     *
     * @return string
     */
    private function getFilename()
    {
        $time = Carbon::now()->toDayDateTimeString();

        $underscored = str_replace(' ', '_', $time);

        return strtolower(str_replace(',', '', $underscored)).'.sql';
    }

    /**
     * Save the backup details to the db
     *
     * @param $filename
     */
    private function registerBackup($filename)
    {
        $backup = new Backup();
        $backup->filename = $filename;
        $backup->daily = Carbon::now()->hour == 0;
        $backup->save();
    }


    /**
     * Delete old backups that are no longer necessary
     */
    private function deleteOldHourlyBackups()
    {
        $cutOff = Carbon::now()->subDay()->toDateString();

        $toBeDeletedQuery = Backup::where('created_at', '<', $cutOff)->where('daily', '!=', 1);

        $toBeDeleted = $toBeDeletedQuery->get();

        $toBeDeleted->each(
            function ($backup) {
                //Amazon s3
                try {
                    //check length, just for safety
                    if (strlen($backup->filename) > 5) {
                        $this->getBackupFilesystem()->delete('crims/'.$backup->filename.'.gz');
                    }
                } catch (FileNotFoundException $e) {
                    //file does not exist or has already been deleted
                }
            }
        );

        $toBeDeletedQuery->delete();
    }

    /**
     * Get the filesystem used for backups
     *
     * @return mixed
     */
    public function getBackupFilesystem()
    {
        $driver = \config('filesystems.default');

        $config = \config('backup-manager.s3');

        if ($driver == 's3') {
            $filesystem = new S3Storage();
            $filesystem->configure($config['key'], $config['secret'], $config['bucket'], $config['region']);

            return $filesystem->filesystem();
        }

        return $this->filesystem;
    }
}
