<?php

namespace App\Console\Commands\System;

use App\Cytonn\Clients\StandingOrders\RealEstateStandingOrdersProcessor;
use App\Cytonn\Clients\StandingOrders\RolloverStandingOrderProcessor;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientStandingOrder;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Console\Command;

class ProcessStandingOrders extends Command
{
    use ExcelMailer;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:process-standing-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process standing orders for clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $report = [
            'Rollover' => $this->rollovers(),
            'Transfer to RE' => $this->realEstateTransfers()
        ];

        $count = collect($report)->sum(function ($i) {
            return $i->count();
        });

        if ($count == 0) {
            return;
        }

        $this->info("Executed {$count} actions from standing orders");

        $name = 'Executed standing orders';

        Excel::fromModel($name, $report)->store('xlsx');

        $this->sendExcel(
            '[CRIMS] ' . $name,
            'Please see attached standing orders executed today',
            $name,
            systemEmailGroups(['operations'])
        );
    }

    protected function rollovers()
    {
        $this->info("Processing rollover standing orders");

        $investments = ClientStandingOrder::active()->ofType('auto_rollover')->get()
            ->map(function ($order) {
                $process = new RolloverStandingOrderProcessor($order);

                $investments = $process->handle();

                return $investments;
            })->flatten();

        $for_reporting = $investments->map(function (ClientInvestment $inv) {
            return new EmptyModel([
                'Code' => $inv->client->client_code,
                'Name' => ClientPresenter::presentFullNames($inv->client_id),
                'Principal' => $inv->amount,
                'Value Date' => DatePresenter::formatDate($inv->invested_date),
                'Maturity Date' => DatePresenter::formatDate($inv->maturity_date),
            ]);
        });

        return $for_reporting;
    }

    protected function realEstateTransfers()
    {
        $this->info("Processing RE standing orders");

        $deductions = ClientStandingOrder::active()
            ->ofType('transfer_to_project')
            ->get()
            ->groupBy('client_id')
            ->map(function ($clientOrders) {
                $client = $clientOrders->first()->client;

                $process = new RealEstateStandingOrdersProcessor($client, $clientOrders);

                $deductions = $process->handle();

                return $deductions;
            })->flatten();

        $for_reporting = collect($deductions)->map(function ($d) {
            $inv = ClientInvestment::findOrFail($d->investment_id);
            $holding = UnitHolding::findOrFail($d->holding_id);
            $amount = $d->amount;


            return new EmptyModel([
                'Code' => $inv->client->client_code,
                'Name' => ClientPresenter::presentFullNames($inv->client_id),
                'Principal' => $inv->amount,
                'Value Date' => DatePresenter::formatDate($inv->invested_date),
                'Maturity Date' => DatePresenter::formatDate($inv->maturity_date),
                'Project' => $holding->project->name,
                'Unit' => $holding->unit->name,
                'Amoount' => $amount
            ]);
        });

        return $for_reporting;
    }
}
