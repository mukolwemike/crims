<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\ClientInvestment;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Console\Command;

class ModifyWithdrawals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:modify_withdrawals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify the client investment withdrawals table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //        $this->updateInterestPaymentDescription();
        //
        //        $this->updatePastInterestPayments();
        //
        //        $this->updatePastWithdrawals();
        //
        //        $this->updatePastDeductions();

        //        $this->updateWithdrawnInvestments();

        $this->updatedRolledInvestments();
    }

    public function updateInterestPaymentDescription()
    {
        ClientInvestmentWithdrawal::where('withdraw_type', 'interest')
            ->where('description', 'Withdrawal')
            ->where('type_id', 1)
            ->update(['description' => 'Interest Payment']);

        ClientInvestmentWithdrawal::where('withdraw_type', 'interest')
            ->where('description', 'Withdrawal')
            ->where('type_id', 2)
            ->update(['description' => 'Interest Reinvested']);
    }

    public function updatePastInterestPayments()
    {
        ClientInvestmentWithdrawal::whereNull('withdraw_type')
            ->where('type_id', 2)
            ->update(['withdraw_type' => 'interest']);

        ClientInvestmentWithdrawal::whereNull('withdraw_type')
            ->where('type_id', 1)
            ->where('description', 'Interest payment')
            ->update(['withdraw_type' => 'interest']);
    }

    public function updatePastWithdrawals()
    {
        ClientInvestmentWithdrawal::whereNull('withdraw_type')
            ->where('type_id', 1)
            ->where('description', 'Rollover')
            ->update(['withdraw_type' => 'withdrawal', 'type_id' => 2]);

        ClientInvestmentWithdrawal::whereNull('withdraw_type')
            ->where('type_id', 1)
            ->where('description', 'Interest payment')
            ->update(['withdraw_type' => 'interest']);

        ClientInvestmentWithdrawal::whereNull('withdraw_type')
            ->where('type_id', 1)
            ->where('description', 'Withdrawal')
            ->update(['withdraw_type' => 'withdrawal']);
    }

    public function updatePastDeductions()
    {
        ClientInvestmentWithdrawal::whereNull('withdraw_type')
            ->where('type_id', 3)
            ->update(['withdraw_type' => 'deduction']);
    }

    public function updateWithdrawnInvestments()
    {
        $investments = ClientInvestment::where('withdrawn', '!=', 1)->has('withdrawals')->get();

        $investments->each(
            function ($investment) {
                $lastWithdrawal = $investment->withdrawals()->orderBy('date', 'DESC')->first();

                $principal = $investment->calculate($lastWithdrawal->date, false)->value();

                if ($principal < 1) {
                    $this->info("Modified ".$investment->id);
                    $investment->update(
                        [
                        'withdrawn' => true,
                        'withdrawal_date' => $lastWithdrawal->date
                        ]
                    );
                }
            }
        );
    }

    public function updatedRolledInvestments()
    {
        $investments = ClientInvestment::where('rolled', 1)
            ->where(
                function ($q) {
                    $q->where('withdrawn', '!=', 1)->orWhereNull('withdrawal_date');
                }
            )->get();

        $investments->each(
            function ($investment) {
                $lastWithdrawal = $investment->withdrawals()->orderBy('date', 'DESC')->first();

                $investment->update(
                    [
                    'withdrawn' => true,
                    'withdrawal_date' => $lastWithdrawal->date
                    ]
                );
            }
        );
    }
}
