<?php

namespace App\Console\Commands\System;

use App;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\User;
use Cytonn\Authorization\Permission;
use Exception;
use Illuminate\Console\Command;
use Laracasts\TestDummy\Factory;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class DataSeeder
 */
class DataSeeder extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'db:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate random fake data in db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     * @return mixed
     */
    public function fire()
    {
        if (App::environment('production')) {
            throw new Exception('You cannot run this in production!!');
        }

        Factory::$factoriesPath = 'app/database/factories';

        $this->generate($this->argument('number'));
    }

    /**
     * @param $count
     */
    public function generate($count)
    {
        Factory::times($count)->create(ClientInvestmentApplication::class);

        Factory::times($count)->create(\App\Cytonn\Models\Portfolio\DepositHolding::class);

        //        $this->createProjects();

        $this->createUsers();
    }

    private function createUsers()
    {
        if (User::count() == 0) {
            Factory::create(User::class, [
                'username' => 'mchaka',
                'email' => 'mchaka@cytonn.com',
                'password' => 'secret',
                'active' => 1]);

            Factory::times(4)->create(User::class, ['password' => 'secret']);
        }

        $permissions = Permission::all();

        User::where('username', 'mchaka')->first()->userPermissions()->saveMany($permissions->all());
    }

    private function createProjects()
    {
        if (Project::count() == 0) {
            Factory::times(5)->create(Project::class);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['number', InputArgument::REQUIRED, 'Number of records to generate'],
        ];
    }
}
