<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use Cytonn\Clients\ClientManager;
use Illuminate\Console\Command;

class CrmContactSync extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:crm_contacts_sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync contacts to crm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $manager = new ClientManager();

        $manager->sendDataToCrm('contacts');
    }
}
