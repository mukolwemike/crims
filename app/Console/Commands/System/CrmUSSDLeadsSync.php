<?php

namespace App\Console\Commands\System;

use App\Cytonn\Models\Investment\UssdTransactionLog;
use App\Cytonn\USSD\USSDRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CrmUSSDLeadsSync extends Command
{
    protected $signature = 'crims:crm_ussd_leads_sync';

    protected $description = 'Sync USSD logs to CRM Leads';

    /**
     * CrmUSSDLeadsSync constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function fire()
    {
        $transactionLogs = UssdTransactionLog::where('sent_to_crm', 0)
            ->where('created_at', '<', Carbon::now()->subMinutes(20)->toDateTimeString())
            ->get();

        if ($transactionLogs->count()) {
            logRollbar("Transferring {$transactionLogs->count()} USSD logs to crm");
        }

        foreach ($transactionLogs as $transactionLog) {
            $phone = $transactionLog->phone;
            $name = $transactionLog->name;
            $source = 'USSD';
            $email = $this->getEmail($transactionLog->email);

            (new USSDRepository())->requestCallFromCRM($phone, $name, $source, $email);

            $transactionLog->sent_to_crm = 1;
            $transactionLog->save();
        }
    }

    private function getEmail($email)
    {
        return (boolean)filter_var($email, FILTER_VALIDATE_EMAIL) ? $email : null;
    }

}
