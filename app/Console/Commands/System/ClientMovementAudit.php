<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Summary\Analytics;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientMovementAudit extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:client_movement_audit {fundmanager} {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report on client movement audit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->generateReport();
    }

    /*
     * Generate the report
     */
    private function generateReport()
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $subStart = $start->copy()->subDay();

        $addEnd = $end->copy()->addDay();

        $fundManager = FundManager::find($this->argument('fundmanager'));

        $report = array();

        $report['Opening ' . $subStart->format('M d Y')] =
            $this->getBalances($fundManager, $subStart->toDateString());

        $report['Opening ' . $start->format('M d Y')] =
            $this->getBalances($fundManager, $start->toDateString());

        $report['Monthly Movement'] = $this->getMonthlyMovement($fundManager);

        $report['Closing ' . $end->format('M d Y')] = $this->getBalances($fundManager, $end->toDateString());

        $report['Closing ' . $addEnd->format('M d Y')] =
            $this->getBalances($fundManager, $addEnd->toDateString());

        $fileName = 'Client Investments Movement Report';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        $userId = $this->argument('user_id');

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the investments movement report')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    public function getBalances($fundManager, $date)
    {
        $date = Carbon::parse($date);

        $analytics = new Analytics($fundManager, $date);
        $analytics->setBaseCurrency(Currency::find(1));

        $marketValue = $analytics->marketValue();

        $adjustedMarketValue = $analytics->adjustedMarketValue();

        $costValue = $analytics->costValue();

        return [
            [
                'Name' => 'Principal Balance (Cost Value)',
                'Date' => $date->toDateString(),
                'Value' => $costValue
            ],
            [
                'Name' => 'Interest (Gross)',
                'Date' => $date->toDateString(),
                'Value' => $adjustedMarketValue - $costValue
            ],
            [
                'Name' => 'Interest (Net)',
                'Date' => $date->toDateString(),
                'Value' => $marketValue - $costValue
            ]
        ];
    }

    public function getMonthlyMovement($fundManager)
    {
        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'))->endOfDay();

        $months = getDateRange($start, $end);

        $monthArray = array();

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month);

            $endMonth = $startOfMonth->copy()->endOfMonth();

            $newInvestments = ClientInvestment::investedBetweenDates($startOfMonth, $endMonth)
                ->fundManager($fundManager)
                ->whereIn('investment_type_id', [1, 2])
                ->get()
                ->sum(function ($investment) {
                    return convert(
                        $investment->amount,
                        $investment->product->currency,
                        Carbon::parse($investment->invested_date)
                    );
                });

            $investments = ClientInvestment::activeBetweenDates($startOfMonth, $endMonth)
                ->fundManager($fundManager)
                ->get();

            $active = $interestAccrued = $totalInterestPaid = $totalInterestSentToClient
                = $totalInterestReinvested = $totalWithdrawals = 0;

            foreach ($investments as $investment) {
                $investmentCurrecy = $investment->product->currency;

                $active += convert($investment->amount, $investmentCurrecy, Carbon::parse($endMonth));

                $started = convert($investment->repo
                    ->grossInterestBeforeDeductions($startOfMonth, false), $investmentCurrecy, $startOfMonth);

                $ended = convert($investment->repo
                    ->grossInterestBeforeDeductions($endMonth, true), $investmentCurrecy, $endMonth);

                $interestAccrued += ($ended - $started);

                $interestPaid = $investment->withdrawals()->between($startOfMonth, $endMonth)
                    ->where('withdraw_type', 'interest')
                    ->get()
                    ->sum(function ($withdrawal) use ($investmentCurrecy) {
                        return convert($withdrawal->amount, $investmentCurrecy, Carbon::parse($withdrawal->date));
                    });

                $totalInterestPaid += $interestPaid;

                $interestSentToClients = $investment->withdrawals()->between($startOfMonth, $endMonth)
                    ->where('withdraw_type', 'interest')->where('type_id', 1)
                    ->get()
                    ->sum(function ($withdrawal) use ($investmentCurrecy) {
                        return convert($withdrawal->amount, $investmentCurrecy, Carbon::parse($withdrawal->date));
                    });

                $totalInterestSentToClient += $interestSentToClients;

                $interestReinvested = $investment->withdrawals()->between($startOfMonth, $endMonth)
                    ->where('withdraw_type', 'interest')->where('type_id', 2)
                    ->get()
                    ->sum(function ($withdrawal) use ($investmentCurrecy) {
                        return convert($withdrawal->amount, $investmentCurrecy, Carbon::parse($withdrawal->date));
                    });

                $totalInterestReinvested += $interestReinvested;

                $withdrawals = $investment->withdrawals()->between($startOfMonth, $endMonth)
                    ->where('withdraw_type', 'withdrawal')->where('type_id', 1)
                    ->get()
                    ->sum(function ($withdrawal) use ($investmentCurrecy) {
                        return convert($withdrawal->amount, $investmentCurrecy, Carbon::parse($withdrawal->date));
                    });

                $totalWithdrawals += $withdrawals;
            }

            $monthArray[] = [
                'Month' => $startOfMonth->format('M, Y'),
                'New Investments' => $newInvestments,
                'Active' => $active,
                'Interest Accrued' => $interestAccrued,
                'Interest Paid' => $totalInterestPaid,
                'Interest Sent To Client' => $totalInterestSentToClient,
                'Interest Reinvested' => $totalInterestReinvested,
                'Withdrawals' => $totalWithdrawals
            ];
        }

        return $monthArray;
    }
}
