<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Illuminate\Console\Command;

class UpdateUnitFunds extends Command
{
    /**
     * The console command name
     *
     * @var string
     */
    protected $signature = 'crims:update_unit_funds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update unit funds in the corresponding tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        return;

        DepositHolding::whereHas('security', function ($q) {
            $q->where('fund_manager_id', 1);
        })->update([
            'unit_fund_id' => 4
        ]);

        DepositHolding::whereHas('security', function ($q) {
            $q->where('fund_manager_id', 3);
        })->update([
            'unit_fund_id' => 5
        ]);

        EquityHolding::where('id', '>', 0)->update([
            'unit_fund_id' => 3
        ]);

        ActiveStrategyShareSale::where('id', '>', 0)->update([
            'unit_fund_id' => 3
        ]);
    }
}
