<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\System;

use function Amp\Promise\rethrow;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\ContactPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class LegalAuditReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:legal_audit_report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a legal team audit report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usersFile = $this->generateLegalUsersAccess();

        $approvalsFile = $this->generateApprovalsReport();

        Mailer::compose()
            ->to([])
            ->bcc(config('system.administrators'))
            ->subject('Legal Audit Report')
            ->text('Please find attached the legal audit report')
            ->excel([$usersFile, $approvalsFile])
            ->send();

        $path = storage_path().'/exports/' . $usersFile .'.xlsx';

        \File::delete($path);

        $path2 = storage_path().'/exports/' . $approvalsFile .'.xlsx';

        \File::delete($path2);
    }

    public function generateApprovalsReport()
    {
        $approvals = ClientTransactionApproval::where('approved', 1)
            ->whereIn('transaction_type', ['loo_uploading', 'sales_agreement_uploading'])
            ->get();

        $approvalsArray = array();

        foreach ($approvals as $approval) {
            $approvalsArray[] = [
                'Id' => $approval->id,
                'Transaction Type' => ucfirst(str_replace('_', ' ', $approval->transaction_type)),
                'Client Name' => ClientPresenter::presentFullNames($approval->client_id),
                'Sent By' => UserPresenter::presentFullNamesNoTitle($approval->sent_by),
                'Sent On' => Carbon::parse($approval->created_at)->toDateString(),
                'Approved' => UserPresenter::presentFullNames($approval->approved_by),
                'Approved On' => Carbon::parse($approval->approved_on)->toDateString()
            ];
        }

        $fileName = 'Upload_Approvals';

        ExcelWork::generateAndStoreSingleSheet($approvalsArray, 'Upload_Approvals');

        return $fileName;
    }

    public function generateLegalUsersAccess()
    {
        $users = User::whereIn('id', [84, 36])->get();

        $usersArray = array();

        $usersArray[] = [
            'Name' => '',
            'Description' => ''
        ];

        foreach ($users as $user) {
            $usersArray[] = [
                'Name' => UserPresenter::presentFullNamesNoTitle($user->id),
                'Description' => ''
            ];

            $usersArray[] = [
                'Name' => '',
                'Description' => ''
            ];

            foreach ($user->roles as $role) {
                foreach ($role->permissions as $permission) {
                    $usersArray[] = [
                        'Name' => $permission->name,
                        'Description' => $permission->description
                    ];
                }
            }

            $usersArray[] = [
                'Name' => '',
                'Description' => ''
            ];
        }

        $fileName = 'Legal_Users_Permissions';

        ExcelWork::generateAndStoreSingleSheet($usersArray, 'Legal_Users_Permissions');

        return $fileName;
    }
}
