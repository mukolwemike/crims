<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\System;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ClientTenorReport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:client_tenor {fundmanager} {end} {start?} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a report of client tenor for the period';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fundManager = FundManager::findOrFail($this->argument('fundmanager'));

        if ($this->argument('start')) {
            $start = Carbon::parse($this->argument('start'))->startOfDay();
        } else {
            $investment = ClientInvestment::fundManager($fundManager)->orderBy('invested_date', 'ASC')->first();

            $start = Carbon::parse($investment->invested_date)->startOfMonth();
        }

        $end = Carbon::parse($this->argument('end'))->endOfDay();

        $clients = $this->getReport($start, $end, $fundManager);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'Client Tenor Report - ' . $date;

        ExcelWork::generateAndStoreSingleSheet($clients, $fileName);

        $userId = $this->argument('user_id');

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the client tenor report for ' . $fundManager->name . ' for the period '
                . $start->toDateString() . ' to '
                . $end->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the client tenor report
     */
    public function getReport($start, $end, $fundManager)
    {
        $clients = Client::whereHas('investments', function ($q) use ($fundManager, $start, $end) {
            $q->fundManager($fundManager)->activeBetweenDates($start, $end);
        })->get();

        $investment = ClientInvestment::fundManager($fundManager)->orderBy('invested_date', 'ASC')->first();

        $firstStart = Carbon::parse($investment->invested_date)->startOfMonth();

        $clientArray = array();

        foreach ($clients as $client) {
            $dataArray = [
                'Client Code' => $client->client_code,
                'Name' => ClientPresenter::presentFullNames($client->id),
                'Email' => $client->contact->email,
            ];

            $clientData = $this->getClientReport($client, $firstStart, $end, $fundManager);

            $clientArray[] = $dataArray + $clientData;
        }

        return $clientArray;
    }

    public function getClientReport(Client $client, Carbon $start, Carbon $end, $fundManager)
    {
        $yearArray = array();

        $startOfYear = $start->copy();

        $endOfYear = $start->copy()->endOfYear();

        $endOfYear = $endOfYear > $end ? $end : $endOfYear;

        $firstInvestment = $client->investments()->fundManager($fundManager)->orderBy('invested_date', 'ASC')->first();

        $firstDate = Carbon::parse($firstInvestment->invested_date)->startOfMonth();

//        $legacyTenor = $this->getTenorMonths($client, $firstDate, $startOfYear->copy()->subDay()
//       ->endOfYear(), $fundManager, $firstDate);

        $yearArray['First Investment Date'] = $firstDate->toDateString();
        $yearArray['Value On Maturity ~ As at Today'] = $client->investments()->fundManager($fundManager)
            ->activeOnDate(Carbon::today())
            ->get()
            ->sum(function (ClientInvestment $investment) {
                return $investment->calculate($investment->maturity_date, true)->total();
            });

//        $yearArray['Cumulative Legacy Tenor'] = $legacyTenor;
        $legacyTenor = 0;

        while ($startOfYear <= $end) {
            $ytdTenor = $this->getTenorMonths($client, $startOfYear, $endOfYear, $fundManager, $firstDate);

            $yearArray[$startOfYear->format('Y M') . ' - Ytd Tenor'] = $ytdTenor . '';

            $legacyTenor = $legacyTenor + $ytdTenor;

            $yearArray[$endOfYear->format('Y M') . ' - Cumulative Tenor'] = $legacyTenor . '';

            $yearArray[$endOfYear->format('Y M') . ' - Investments Value'] =
                $this->getInvestmentsValue($client, $fundManager, $endOfYear);

            $yearArray[$endOfYear->format('Y M') . ' - Average Tenor'] =
                $this->getAverageTenor($client, $fundManager, $startOfYear, $endOfYear);

            $yearArray[$endOfYear->format('Y M') . ' - Average Tenor To Maturity'] =
                $this->getAverageTenorToMaturity($client, $fundManager, $startOfYear, $endOfYear);

            $startOfYear = $startOfYear->addYear()->startOfYear();

            $endOfYear = $startOfYear->copy()->endOfYear();

            $endOfYear = $endOfYear > $end ? $end : $endOfYear;
        }

        return $yearArray;
    }

    public function getTenorMonths($client, $start, $end, $fundManager, $firstDate)
    {
        $months = getDateRange($start, $end);

        $total = 0;

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month)->startOfMonth();

            $endOfMonth = Carbon::parse($month)->endOfMonth();

            if ($startOfMonth >= $firstDate) {
                $activeInvestments = $client->investments()->fundManager($fundManager)
                    ->activeBetweenDates($startOfMonth, $endOfMonth)->count();

                if ($activeInvestments > 0) {
                    $total += 1;
                }
            }
        }

        return $total;
    }

    public function getInvestmentsValue($client, $fundManager, $date)
    {
        $sum = $client->investments()
            ->fundManager($fundManager)
            ->activeOnDate($date)
            ->get()
            ->sum(function (ClientInvestment $investment) use ($date) {
                $amount = $investment->repo->getTotalValueOfInvestmentAtDate($date);

                return convert($amount, $investment->product->currency, $date);
            });

        return AmountPresenter::currency($sum);
    }

    /*
     * Get the average tenor for the period
     */
    public function getAverageTenor($client, $fundManager, $start, $end)
    {
        $sum = $client->investments()
            ->fundManager($fundManager)
            ->activeOnDate($end)
            ->get()
            ->sum(function (ClientInvestment $investment) use ($end) {
                return $investment->repo->getTenorInMonths();
            });

        $count = $client->investments()
            ->fundManager($fundManager)
            ->activeOnDate($end)
            ->count();

        return dividend($sum, $count, 1);
    }

    /*
     * Get average tenor to maturity
     */
    public function getAverageTenorToMaturity($client, $fundManager, $start, $end)
    {
        $sum = $client->investments()
            ->fundManager($fundManager)
            ->activeOnDate($end)
            ->get()
            ->sum(function (ClientInvestment $investment) use ($end) {
                return $investment->repo->getTenorToMaturityInMonths($end);
            });

        $count = $client->investments()
            ->fundManager($fundManager)
            ->activeOnDate($end)
            ->count();

        return dividend($sum, $count, 1);
    }
}
