<?php

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FundValuationReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'portfolio:fund-valuation {fund_id} {--date=} {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fund = UnitFund::findOrFail($this->argument('fund_id'));

        $date = carbon($this->option('date'));

        $calculation = $fund->category->calculation->slug;

        switch ($calculation) {
            case 'variable-unit-price':
                $this->navCalculation($fund, $date);
                break;
            case 'daily-yield':
                $this->yieldCalculation($fund, $date);
                break;
            default:
                throw new \InvalidArgumentException("Calculation type $calculation not handled");
        }
    }

    protected function yieldCalculation(UnitFund $fund, Carbon $date)
    {
    }

    public function navCalculation(UnitFund $fund, Carbon $date)
    {
    }
}
