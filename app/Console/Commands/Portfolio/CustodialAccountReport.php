<?php

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Portfolio\CustodialAccounts\Export;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class CustodialAccountReport extends Command
{
    protected $signature = 'portfolio:custodial_account_report {user_id} {account} {start} {end}';

    protected $description = 'Generate a report on the transactions for a custodial account';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $name = 'Custodial Account Details';

        $start = Carbon::parse($this->argument('start'));

        $end = Carbon::parse($this->argument('end'));

        $user = (new User())->findOrFail($this->argument('user_id'));

        $account = (new CustodialAccount())->findOrFail($this->argument('account'));

        (new Export())->transactions($account, $start, $end, $name);

        $path = storage_path().'/exports/'.$name.'.xlsx';

        $this->mailResult($start, $end, $account, $user, $name);

        \File::delete($path);
    }

    public function mailResult($start, $end, $account, $user, $name)
    {
        Mailer::compose()
            ->to([$user->email])
            ->bcc(config('system.administrators'))
            ->subject($name)
            ->text(
                'Please find attached the custodial account transactions from '
                .$start->toFormattedDateString().' to '
                .$end->toFormattedDateString().' for the account '
                .$account->account_name.' - '
                .$account->alias
            )
            ->excel([$name])
            ->send();
    }
}
