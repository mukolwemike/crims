<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class YearlyPortfolioInvestmentsReport extends Command
{
    /**
     * The console command name
     *
     * @var string
     */
    protected $signature = 'portfolio:yearly_investments_summary {fund_manager_id} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a yearly summary of all the porfolio investments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');
        $fundManagerId = $this->argument('fund_manager_id');

        $fundManager = FundManager::find($fundManagerId);

        $investmentsData = $this->getReport($fundManager);

        $fileName = 'Yearly Deposit Holdings Summary';

        ExcelWork::generateAndStoreMultiSheet($investmentsData, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the monthly deposit holdings summary')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get a report of the yearly investments summary
     */
    public function getReport($fundManager)
    {
        $startDate = DepositHolding::orderBy('invested_date', 'ASC')
                    ->fundManager($fundManager)
                    ->first()
                    ->invested_date;

        $endDate = DepositHolding::orderBy('invested_date', 'DESC')
                    ->fundManager($fundManager)
                    ->first()
                    ->invested_date;

        $startDate = \Cytonn\Core\DataStructures\Carbon::parse($startDate);

        $endDate = \Cytonn\Core\DataStructures\Carbon::parse($endDate);

        $investmentsArray = array();

        while ($startDate->lessThan($endDate)) {
            $startDate = Carbon::parse($startDate)->startOfYear();

            $endOfYear = $startDate->copy()->endOfYear();

            $investmentsArray[$startDate->format('Y')] = $this->getYearInvestments($startDate, $endOfYear);

            $startDate = $startDate->addYear(1)->startOfYear();
        }

        return $investmentsArray;
    }

    /*
     * Get the year investments
     */
    public function getYearInvestments(Carbon $startDate, Carbon $endDate)
    {
        $holdings = DepositHolding::where('invested_date', '>=', $startDate)
            ->where('invested_date', '<=', $endDate)
            ->whereHas(
                'security',
                function ($q) {
                    $q->where('fund_manager_id', 1);
                }
            )->get();

        return $holdings->map(
            function ($holding) {
                return [
                'Portfolio Security' => $holding->security->name,
                'Code' => $holding->security->code,
                'Invested Date' => $holding->invested_date,
                'Maturity Date' => $holding->maturity_date,
                'Invested Amount' => AmountPresenter::currency($holding->amount),
                'Interest Rate' => $holding->interest_rate,
                'Withdrawn' => BooleanPresenter::presentYesNo($holding->withdrawn),
                'Rolled' => BooleanPresenter::presentYesNo($holding->rolled),
                'Withdrawn Amount' => AmountPresenter::currency($holding->withdraw_amount),
                'Withdrawal Date' => $holding->withdrawal_date
                ];
            }
        );
    }
}
