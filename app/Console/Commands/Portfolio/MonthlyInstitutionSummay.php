<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class MonthlyInstitutionSummay extends Command
{
    /**
     * The console command name
     *
     * @var string
     */
    protected $signature = 'portfolio:institutions_summary {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of monthly value of the porfolio institutions investments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $startDate = Carbon::parse($this->argument('start'));

        $endDate = Carbon::parse($this->argument('end'));

        $institutionsData = $this->getPortfolioInstitutions($startDate, $endDate);

        $fileName = 'Monthly_Institutions_Summary';

        ExcelWork::generateAndStoreSingleSheet($institutionsData, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the monthly portfolio institutions summary')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the portfolio institutions investments and values
     */
    public function getPortfolioInstitutions(Carbon $startDate, Carbon $endDate)
    {
        $startDate = $startDate->endOfMonth();

        $endDate = $endDate->addMonthNoOverflow()->endOfMonth();

        $institutions = PortfolioInvestor::all();

        $institutionsArray = array();

        foreach ($institutions as $institution) {
            $dataArray = array();

            $dataArray['Name'] = $institution->name;

            $start = $startDate->copy();

            while ($start->lt($endDate)) {
                $dataArray[$start->format('F_Y')] = $institution->repo->totalDepositValueForAllProducts($start, true);

                $start = $start->addMonthNoOverflow()->endOfMonth();
            }

            $institutionsArray[] = $dataArray;
        }

        return $institutionsArray;
    }
}
