<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class PortfolioInvestmentsReport extends Command
{
    /**
     * The console command name
     *
     * @var string
     */
    protected $signature = 'portfolio:investments_summary {investor} {date} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of the portfolio investments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $date = Carbon::parse($this->argument('date'))->endOfDay();

        $investor = PortfolioInvestor::findOrFail($this->argument('investor'));

        $investments = $this->getInvestments($investor, $date);

        $fileName = 'Portfolio_Investments_Summary ' . $date->format('M_Y');

        ExcelWork::generateAndStoreSingleSheet($investments, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the portfolio investments summary')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        return \File::delete($path);
    }

    public function getInvestments($investor, Carbon $date)
    {
        $asAtNextDay = true;

        $investments = \App\Cytonn\Models\Portfolio\DepositHolding::where(
            'portfolio_investor_id',
            $investor->id
        )->activeOnDate($date)->get();

        return $investments->map(
            function ($investment) use ($date, $asAtNextDay) {
                return [
                    'Invested Date' => $investment->invested_date,
                    'Investor' => $investment->portfolioInvestor->name,
                    'Principal' => $investment->amount,
                    'KES Value At Date' => $investment->repo
                        ->getKesConvertedValue($investment->repo->getTotalValueOfAnInvestmentAsAtDate($date, true)),
                    'Custodial Account' => $investment->custodialAccount->account_name,
                    'Maturity Date' => $investment->maturity_date,
                    'Interest Rate' => $investment->interest_rate,
                    'Withdrawn' => BooleanPresenter::presentYesNo($investment->withdrawn),
                    'Rolled' => BooleanPresenter::presentYesNo($investment->rolled),
                    'Withdrawal Date' => $investment->withdrawal_date,
                    'Withdraw Amount' => $investment->withdraw_amount
                ];
            }
        );
    }
}
