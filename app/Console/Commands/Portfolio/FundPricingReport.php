<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/12/18
 * Time: 11:45 AM
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use App\Cytonn\Portfolio\Summary\FundPricingSummary;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Api\Transformers\Portfolio\DepositHoldingSummaryTransformer;
use Cytonn\Api\Transformers\Portfolio\EquityHoldingSummaryTransformer;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Performance;
use Illuminate\Console\Command;

class FundPricingReport extends Command
{
    protected $signature = 'portfolio:fund-pricing {unit_fund_id} {user_id?} {fund_manager_id?} {date?}';

    protected $description = 'Export fund pricing summary';

    private $fund;

    private $date;

    /**
     * @var Performance
     */
    private $performance;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $fund = UnitFund::findOrFail($this->argument('unit_fund_id'));

        $user = User::find($this->argument('user_id'));

        $this->fund = $fund;

        $date = $this->argument('date') ? Carbon::parse($this->argument('date')) : Carbon::today();

        $this->performance = new Performance($fund, $date->copy());

        $this->date = $date->copy();

        $fileName = 'fund_pricing_summary';

        $pricingData = $this->getPricingData($fund, $date->copy());

        ExcelWork::generateAndStoreMultiSheet($pricingData, $fileName);

        $email = $user ? $user->email : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the fund pricing summary reports')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    public function getPricingData(UnitFund $fund, Carbon $date)
    {
        $liabilities = (new FundPricingSummary($fund, $date->copy()))->liabilities();

        $summary = $this->summary($fund, $date->copy())->map(function ($amount, $key) {
            return [
                '' => $key,
                'Value' => $amount,
            ];
        });

        return [
            'Pricing Summary' => $summary,
            'Deposits' => $this->depositAssets($fund, $date),
            'Equities' => $this->equityAssets($fund),
            'Liabilities' => $liabilities
        ];
    }

    public function depositAssets(UnitFund $fund, Carbon $date)
    {
        return DepositHolding::where('unit_fund_id', $fund->id)
            ->activeOnDate($date)
            ->get()
            ->map(function ($holding) {
                return app(DepositHoldingSummaryTransformer::class)->export($holding);
            });
    }

    public function equityAssets(UnitFund $fund)
    {
        return EquityHolding::where('unit_fund_id', $fund->id)
            ->get()
            ->map(function ($holding) {
                return app(EquityHoldingSummaryTransformer::class)->export($holding);
            });
    }

    public function summary(UnitFund $fund, Carbon $date)
    {
        $category = $fund->category->calculation->slug;

        $details = [
            'Fund' => $this->fund->name,
            'Date' => $this->date->toFormattedDateString(),
            'Assets' => $this->performance->assetCostValue(),
        ];

        foreach ($this->performance->accounts() as $account) {
            $details['Balance - '.$account->account_name ] = $account->balance($date);
        }

        $details['Assets Under Management'] = $this->performance->aumAtCost();

        switch ($category) {
            case 'daily-yield':
                return collect($details + $this->moneyMarketFundTotals());
                break;

            case 'variable-unit-price':
                return collect($details + $this->navFundTotals());
                break;
            default:
                return collect($details + $this->navFundTotals());
                break;
        }
    }

    private function navFundTotals()
    {
        $performance = $this->performance;

        return [
            'Liabilities' => $performance->liabilities(),
            'Net Asset Value' => $performance->nav(),
            'Units' => $performance->totalUnits(),
            'Price' => $performance->unitPrice(),
            'Return' => $performance->portfolioReturn()
        ];
    }

    private function moneyMarketFundTotals()
    {
        $performance = $this->performance;

        return [
            'Gross Daily Income' => $performance->grossIncomeForDay(),
            'Today Liabilities' => $performance->todayLiabilities(),
            'Net Income Per Unit' => $performance->netIncomePerUnit(),
            'Gross Daily Yield' => $performance->grossDailyYield(),
            'Out performance percent' => $performance->todayOutPerformancePercent(),
            'Out performance amount' => $performance->todayOutPerformanceAmount(),
            'Net Daily Yield' => $performance->netDailyYield(),
            'Gross Annual Yield' => $performance->grossAnnualYield(),
            'Net Annual Yield' => $performance->netAnnualYield(),
        ];
    }
}
