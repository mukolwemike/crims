<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/4/18
 * Time: 4:01 PM
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;

use App\Cytonn\Portfolio\Deposits\LoanAmortizationGenerator;
use Cytonn\Excels\ExcelWork;
use Illuminate\Console\Command;

class LoanAmortizationSchedule extends Command
{
    protected $signature = 'portfolio:loan_amortization_schedule {holding_id}';

    protected $description = 'Generate a loan amortization schedule';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $holdingId = $this->argument('holding_id');

        $holding = DepositHolding::findOrFail($holdingId);

        $schedule = (new LoanAmortizationGenerator($holding))->generate();

        return ExcelWork::generateAndExportSingleSheet($schedule, 'Loan Amortization Schedule');
    }
}
