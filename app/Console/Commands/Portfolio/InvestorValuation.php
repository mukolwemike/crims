<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\Summary\Analytics;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class InvestorValuation extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'portfolio:investor_valuation {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a summary of investor valuations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $fundManager = FundManager::find(1);

        $report = $this->generateReport($fundManager);

        $date = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::today());

        $fileName = 'Investor Monthly Valuation - '. $date;

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        if ($userId) {
            $user = User::findOrFail($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the investors valuation report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the client payments
     */
    public function generateReport($fundManager)
    {
        $reportArray = array();

        $startDate = Carbon::parse('2014-01-01');

        $endDate = Carbon::parse('2017-12-31');

        while ($startDate->lt($endDate)) {
            $endMonth = $startDate->copy()->endOfMonth();

            $value = $this->getValue($endMonth, $fundManager);

            $reportArray[$endMonth->format('Y')][] = [
                'Month' => $endMonth->format('F, Y'),
                'Number of Clients' => $this->getClients($endMonth, $fundManager),
                'Cost Value of Fund' => AmountPresenter::currency($value['cost_value']),
                'Market Value of Fund' => AmountPresenter::currency($value['market_value'])
            ];

            $startDate->addMonthNoOverflow();
        }


        return $reportArray;
    }

    /*
     * Get the total clients at that date
     */
    public function getClients(Carbon $date, $fundManager)
    {
        return Client::whereHas(
            'investments',
            function ($q) use ($date, $fundManager) {
                $q->activeOnDate($date)->fundManager($fundManager);
            }
        )->count();
    }

    /*
     * Get the value of the fund at that date
     */
    public function getValue(Carbon $date, $fundManager)
    {
        $currency = Currency::find(1);

        $analytics = (new Analytics($fundManager, $date))->setBaseCurrency($currency);

        return [
            'cost_value' => $analytics->costValue(),
            'market_value' => $analytics->marketValue()
        ];
    }
}
