<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 4/10/19
 * Time: 10:29 AM
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Portfolio\Orders\Compliance;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class OrderComplianceChecks extends Command
{
    protected $signature = 'portfolio:order_compliance_checks';

    protected $description = 'Notifications for portfolio order compliance checks';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $orders = PortfolioOrder::approved()->get();

        $order_checks = [];

        foreach ($orders as $order) {
            $checks = $this->checkCompliance($order);

            if (count($checks) > 0) {
                $order_checks[] = [
                    'order' => $order,
                    'checks' => $checks
                ];
            }
        }

        if (count($order_checks) > 0) {
            $this->sendMail($order_checks);
        }
    }

    /**
     * @param PortfolioOrder $order
     * @return array
     */
    public function checkCompliance(PortfolioOrder $order)
    {
        $orders = $order->unitFundOrders->groupBy('unit_fund_id');

        $status = [];

        foreach ($orders as $fund_order) {
            $amount = $fund_order->sum('amount');

            $security = $order->security;

            $fund = $fund_order->first()->unitFund;

            $fundCompliance = new Compliance($fund, $security, $amount);

            $statuses = $fundCompliance->status();

            if (count($statuses) > 0) {
                $status[] = [
                    'total_amount' => $amount,
                    'fund' => $fund,
                    'fund_order' => $fund_order->first(),
                    'status' => (object) [
                        'reason' => trim($this->getReasons($statuses))
                    ]
                ];
            }
        }

        return $status;
    }

    /**
     * @param $statuses
     * @return mixed
     */
    private function getReasons($statuses)
    {
        return collect($statuses)->reduce(function ($reason, $status) {
            return $reason . $status->reason . ', ';
        }, '');
    }

    /**
     * @param array $order_checks
     */
    private function sendMail($order_checks = [])
    {
        $fileName = 'Portfolio Order Compliance';

        Mailer::compose()
            ->to(systemEmailGroups(['operations', 'operations_mgt']))
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->view('emails.portfolio.fund_compliance_check', [
                'order_checks' => $order_checks,
            ])
            ->send();
    }
}
