<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStep;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalType;
use Illuminate\Console\Command;

class UpdatePortfolioTransactionApprovals extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crims:update_portfolio_approvals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the portfolio transaction approvals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->addTransactionTypes();

        $this->updateTransactionApprovalSteps();

        $this->updateApprovals();

        dd("Done");
    }

    private function addTransactionTypes()
    {
        $types = PortfolioTransactionApproval::distinct('transaction_type')->pluck('transaction_type');

        foreach ($types as $type) {
            if (! PortfolioTransactionApprovalType::where('slug', $type)->exists()) {
                PortfolioTransactionApprovalType::create([
                        'name' => ucfirst(str_replace('_', ' ', $type)),
                        'slug' =>  $type
                    ]);
            }
        }
    }

    private function updateTransactionApprovalSteps()
    {
        $approvals = PortfolioTransactionApproval::where('approved', 1)->get();

        foreach ($approvals as $approval) {
            $step = PortfolioTransactionApprovalStep::where('approval_id', $approval->id)
                ->where('stage_id', 1)->first();

            if (is_null($step)) {
                PortfolioTransactionApprovalStep::create([
                    'approval_id' => $approval->id,
                    'stage_id' => 1,
                    'user_id' => $approval->approved_by
                ]);
            }
        }
    }

    private function updateApprovals()
    {
        PortfolioTransactionApproval::withTrashed()->update([
            'awaiting_stage_id' => 1
        ]);
    }
}
