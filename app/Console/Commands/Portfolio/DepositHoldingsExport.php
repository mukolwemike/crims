<?php

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Console\Command;

class DepositHoldingsExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'portfolio:export-deposits { start } { end } { --user_id=? } {--fund=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function handle()
    {
        $start = new Carbon($this->argument('start'));
        $end = new Carbon($this->argument('end'));
        $user = User::find($this->option('user_id'));
        $fund = UnitFund::findOrFail($this->option('fund'));

        if (is_null($this->argument('start'))) {
            $start = Carbon::today()->startOfYear();
        }

        if (is_null($this->argument('end'))) {
            $end = Carbon::today();
        }

        if ($start->copy()->gt($end->copy())) {
            throw new ClientInvestmentException('The end date must be after start date');
        }

        $files = [$this->exportPortfolio($start, $end, $fund)];

        $this->mailResult($files, $fund, $start, $end, $user);

        foreach ($files as $file) {
            \File::delete($file);
        }
    }

    public function exportPortfolio($start, $end, $fund)
    {
        $this->end_date = $end;

        $this->start_date = $start;

        $portfolio = $this->getPortfolio($start, $end, $fund);

        $fileName = 'Portfolio export';

        Excel::fromModel($fileName, $portfolio)->store('xlsx');

        return storage_path() . '/exports/' . $fileName . '.xlsx';
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    private function getPortfolio($start, $end, $fund)
    {
        print("Exporting portfolio" . PHP_EOL);

        $p = DepositHolding::activeBetweenDates($start, $end)
            ->forFund($fund)
            ->get()
            ->map(function (DepositHolding $investment) use ($start, $end) {
                $fm = $investment->security->fundManager->name;
                $calculated = $investment->calculate($start, true);

                $principal = $calculated->principal();
                $pr = $investment->amount - $principal;

                $out = [
                        'Fund' => $fm,
                        'Institution' => $investment->security->name,
                        'Type' => $investment->security->subAssetClass->name,
                        'Principal' => $investment->amount,
                        'Principal Repaid' => $pr,
                        'Principal Balance' => $principal,
                        'Interest rate' => $investment->interest_rate,
                        'Invested Date' => $investment->invested_date,
                        'Maturity Date' => $investment->maturity_date,
                        'Withdrawal Date' => $investment->withdrawal_date,
                        'Withdrawn' => \Cytonn\Presenters\BooleanPresenter::presentYesNo(
                            $investment->withdrawn && $investment->withdrawal_date <= $end
                        ),
                        'Gross Interest' => $calculated->grossInterestBeforeDeductions(),
                        'Net Interest' => $calculated->netInterestBeforeDeductions(),
                        'Interest Repaid' => $calculated->netInterestRepaid(),
                        'Interest Balance' => $calculated->netInterest(),
                        'Adjusted Market Value' => $calculated->adjustedMarketValue(),
                        'Market Value' => $calculated->total()
                    ];

                    return (new EmptyModel())->fill($out);
            })->groupBy('Fund');

        print "Done with portfolio" . PHP_EOL;

        return $p;
    }

    /**
     * @param $file_path
     * @param Carbon $start
     * @param Carbon $end
     * @param null $user
     */
    private function mailResult($file_path, $fund, Carbon $start, Carbon $end, $user = null)
    {
        $mail = Mail::compose()
            ->bcc(['mchaka@cytonn.com'])
            ->from('support@cytonn.com')
            ->text('Here is the export of investments that you requested for the period between ' .
                $start->toFormattedDateString() . ' and ' . $end->toFormattedDateString())
            ->attach($file_path)
            ->subject('Portfolio Deposits Exports - '.$fund->name);

        if ($user) {
            $mail = $mail->to($user->email);
        }

        $mail->send();
    }
}
