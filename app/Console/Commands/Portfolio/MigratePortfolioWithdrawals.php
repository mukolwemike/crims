<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenMade;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class MigratePortfolioWithdrawals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crims:modify_portfolio_withdrawals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify the deposit holdings table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        dd("This is executed");

        \DB::transaction(function () {
            $this->updateRepayments();
        });
    }

    private function updateRepayments()
    {
        $depositHoldings = DepositHolding::all();

        $dataArray = array();

        foreach ($depositHoldings as $depositHolding) {
            $initialPrepared = $depositHolding->calculate(Carbon::now())->getPrepared();

            $depositArray = [
                'ID' => $depositHolding->id,
                'Security' => $depositHolding->security->name,
                'Amount' => $depositHolding->amount,
                'Withdraw Amount' => $depositHolding->withdraw_amount,
                'Withdrawn' => $depositHolding->withdrawn,
                'Rolled' => $depositHolding->rolled,
                'Initial Principal' => $initialPrepared->total->principal,
                'Initial Net' => $initialPrepared->total->total
            ];

            foreach ($initialPrepared->actions as $action) {
                if (is_null($action->action)) {
                    $actionType = $action->description == 'Rollover' ? 2 : 1;

                    if (abs($action->amount) > 0.5) {
                        $repayment = PortfolioInvestmentRepayment::create([
                            'amount' => abs($action->amount),
                            'date' => Carbon::parse($action->date),
                            'investment_id' => $depositHolding->id,
                            'type_id' => 3,
                            'custodial_transaction_id' => $depositHolding->custodial_withdraw_transaction_id,
                            'narrative' => $action->description,
                            'approval_id' => $depositHolding->withdraw_transaction_approval_id,
                            'portfolio_repayment_action_id' => $actionType
                        ]);

                        try {
                            event(new PortfolioRepaymentHasBeenMade($depositHolding, $repayment));
                        } catch (\Exception $e) {
                            dd($e, $repayment);
                        }
                    }
                }
            }

            $depositHolding = $depositHolding->fresh();

            $newPrepared = $prepare = $depositHolding->calculate(Carbon::now())->getPrepared();

            $depositArray['New Principal'] = $newPrepared->total->principal;

            $depositArray['New Net'] = $newPrepared->total->total;

            $depositArray['Principal Variance'] = AmountPresenter::accounting($depositArray['Initial Principal']
                - $depositArray['New Principal']);

            $depositArray['Net Variance'] =
                AmountPresenter::accounting($depositArray['Initial Net'] - $depositArray['New Net']);

            $dataArray[] = $depositArray;
        }

        $fileName = 'Portfolio Investments Migration';

        ExcelWork::generateAndStoreSingleSheet($dataArray, $fileName);

        Mailer::compose()
            ->to([])
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the portfolio investments report')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        \File::delete($path);
    }

    private function updateApproval()
    {
        $portfolioRepayments = PortfolioInvestmentRepayment::whereNull('approval_id')->get();

        foreach ($portfolioRepayments as $repayment) {
            $transaction = $repayment->custodialTransaction;

            if ($transaction) {
                $repayment->approval_id = $transaction->approval_id;

                $repayment->save();
            }
        }
    }
}
