<?php

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Unitization\Trading\Performance;
use Illuminate\Console\Command;

class SummaryValuationReport extends Command
{
    use ExcelMailer;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'portfolio:summary-valuation {start} {end} {fund_id} {--user=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $balances;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));

        $quarters = $this->getQuarters($start, $end);

        $fund = UnitFund::findOrFail($this->argument('fund_id'));

        $performances = collect($quarters)->map(function ($quarter) use ($fund) {
            $perf = new Performance($fund, $quarter->end->copy());

            $opening = $this->balance($fund, $quarter->start->copy()->subDay());
            $contributions = $perf->cashInflows($quarter->start);
            $withdrawals = $perf->cashOutflows($quarter->start);
            $closing = $this->balance($fund, $quarter->end);
            $total = $opening + $contributions + $withdrawals;
            $gross_growth = $closing - $total;

            return (new EmptyModel())->fill([
                'opening_balance' => $opening,
                'contributions' => $contributions,
                'withdrawals' => $withdrawals,
                'net_growth' => $gross_growth,
                'gross_growth' => $gross_growth,
                'closing_balance' => $closing,
                'weighted_return' =>
                    $total == 0 ? 0 : number_format(100 * $gross_growth / $total, 2) . '%'
            ]);
        });

        $file = 'Summary_Valuation_Report';

        \Excel::create($file, function ($excel) use ($start, $end, $quarters, $fund, $performances) {
            $excel->sheet('Summary Valuation', function ($sheet) use ($start, $end, $quarters, $fund, $performances) {
                $sheet->loadView('portfolio.reports.summary_valuation_report', [
                    'start' => $start,
                    'end' => $end,
                    'quarters' => $quarters,
                    'fund' => $fund,
                    'performances' => $performances
                ]);
            });
        })->store('xlsx');

        $user = User::find($this->option('user'));

        $email = $user ? [$user->email] : [];

        $this->sendExcel('Summary Valuation Report', 'PFA', $file, $email);
    }

    private function balance(UnitFund $fund, Carbon $date)
    {
        $key = "fund_$fund->id" . '_date_' . $date->toDateString();

        if (isset($this->balances[$key])) {
            return $this->balances[$key];
        }

        $perf = new Performance($fund, $date);

        return $this->balances[$key] = $perf->aum();
    }

    private function getQuarters(Carbon $start, Carbon $end)
    {
        $dates = [];

        $date = $start->copy();

        while ($end->gte($date)) {
            $ends = $date->copy()->subDay()->addMonthNoOverflow(3);

            $ends = $ends->lte($end) ? $ends : $end->copy();

            array_push($dates, (object)['start' => $date, 'end' => $ends]);

            $date = $ends->copy()->addDay();
        }

        return $dates;
    }
}
