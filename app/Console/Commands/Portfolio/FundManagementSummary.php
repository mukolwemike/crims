<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Console\Commands\Portfolio;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use App\Cytonn\Portfolio\Summary\FundPricingSummary;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Performance;
use Illuminate\Console\Command;

class FundManagementSummary extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'portfolio:fund_management_summary {unit_fund_id} {user_id?} {start?} {end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export a portolio fund management summary';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = $this->argument('user_id');

        $fund = UnitFund::findOrFail($this->argument('unit_fund_id'));

        $startDate = $this->argument('start') ? Carbon::parse($this->argument('start')) : Carbon::now()->subDay();

        $endDate = $this->argument('end') ? Carbon::parse($this->argument('end')) : Carbon::now();

        $summary = $this->getFundSummary($fund, $startDate, $endDate);

        $fileName = $fund->short_name . ' Fund Management Summary';

        ExcelWork::generateAndStoreSingleSheet($summary, $fileName);

        if ($userId) {
            $user = User::find($userId);

            $email = $user ? $user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the fund management summary for '
                . $fund->short_name . " for the period between " . $startDate->toDateString()
                . ' and ' . $endDate->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /**
     * @param UnitFund $fund
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getFundSummary(UnitFund $fund, Carbon $startDate, Carbon $endDate)
    {
        $summaryArray = array();

        $start = $startDate->copy();

        while ($start <= $endDate) {
            $performance = new Performance($fund, $start->copy());

            $summary = $this->summary($fund, $start->copy(), $performance);

            $liabilities = (new FundPricingSummary($fund, $start->copy()))->liabilities();

            foreach ($liabilities as $liability) {
                $summary[$liability['type']] = $liability['today'];
            }

            $summaryArray[] = $summary->toArray();

            $start->addDay();
        }

        return $summaryArray;
    }

    /**
     * @param UnitFund $fund
     * @param Carbon $date
     * @param Performance $performance
     * @return \Illuminate\Support\Collection
     */
    public function summary(UnitFund $fund, Carbon $date, Performance $performance)
    {
        $category = $fund->category->calculation->slug;

        $details = [
            'Fund' => $fund->name,
            'Date' => $date->toFormattedDateString(),
            'Assets' => $performance->assetCostValue(),
        ];

        foreach ($performance->accounts() as $account) {
            $details['Balance - '.$account->account_name ] = $account->balance($date);
        }

        $details['Assets Under Management'] = $performance->aumAtCost();

        switch ($category) {
            case 'daily-yield':
                return collect($details + $this->moneyMarketFundTotals($performance));
                break;

            case 'variable-unit-price':
                return collect($details + $this->navFundTotals($performance));
                break;
            default:
                throw new \InvalidArgumentException("Not Supported");
                break;
        }
    }

    /**
     * @param Performance $performance
     * @return array
     */
    private function navFundTotals(Performance $performance)
    {
        return [
            'Liabilities' => $performance->liabilities(),
            'Net Asset Value' => $performance->nav(),
            'Units' => $performance->totalUnits(),
            'Price' => $performance->unitPrice()
        ];
    }

    /**
     * @param Performance $performance
     * @return array
     */
    private function moneyMarketFundTotals(Performance $performance)
    {
        return [
            'Gross Daily Income' => $performance->grossIncomeForDay(),
            'Today Liabilities' => $performance->todayLiabilities(),
            'Net Income Per Unit' => $performance->netIncomePerUnit(),
            'Gross Daily Yield' => $performance->grossDailyYield(),
            'Net Daily Yield' => $performance->netDailyYield(),
            'Gross Annual Yield' => $performance->grossAnnualYield(),
            'Net Annual Yield' => $performance->netAnnualYield(),
        ];
    }
}
