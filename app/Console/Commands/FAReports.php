<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Console\Commands;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class FAReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature =  'crims:fa_random_reports {start} {end} {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::parse($this->argument('start'));
        $end = Carbon::parse($this->argument('end'));
        $user = User::findOrFail($this->argument('user_id'));

        $realEstateActuals = $this->getREActualPayments($start, $end);

        ExcelWork::generateAndStoreMultiSheet($realEstateActuals, 'Real_Estate_Actuals');

        Mailer::compose()
            ->to($user->email)
            ->bcc(config('system.administrators'))
            ->subject('Real_Estate_Actuals')
            ->text('Please find attached the real estate actuals')
            ->excel(['Real_Estate_Actuals'])
            ->send();

        $path = storage_path().'/exports/' . 'Real_Estate_Actuals' .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Get the real estate actual payments
     */
    public function getREActualPayments($start, $end)
    {
        $payments = RealEstatePayment::where('date', '<=', $end)->where('date', '>=', $start)->get();

        $paymentsArray = array();

        foreach ($payments as $payment) {
            $holding = $payment->holding;

            $paymentsArray[$holding->unit->project->name][] = [
                'Date' => Carbon::parse($payment->date)->toDateString(),
                'Name' => ($holding->client) ? $holding->client->name() : '',
                'Amount' => round($payment->amount, 0),
                'Project' => $holding->unit->project->name . ' - ' . $holding->unit->number
            ];
        }

        return $paymentsArray;
    }
}
