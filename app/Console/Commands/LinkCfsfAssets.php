<?php

namespace App\Console\Commands;

use App\Cytonn\Models\ActiveStrategyDividend;
use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Setting;
use Illuminate\Console\Command;

class LinkCfsfAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cfsf:link-assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd('done');

        \DB::transaction(function () {
            $this->deposits();

            PortfolioSecurity::forAssetClass('equities')->get()
                ->each->update(['fund_manager_id' => 5]);

            EquityHolding::all()->each->update(['unit_fund_id' => 3]);

            EquityShareSale::all()->each->update(['unit_fund_id' => 3]);

            EquityDividend::all()->each->update(['unit_fund_id' => 3]);

            $this->info("Done");
        });
    }

    protected function deposits()
    {
        DepositHolding::withTrashed()->get()->each(function (DepositHolding $holding) {
            if (is_null($holding->tax_rate_id)) {
                if ($holding->taxable) {
                    $holding->update([
                        'tax_rate_id' => Setting::where('key', 'withholding_tax_rate')->first()->id
                    ]);
                } else {
                    $holding->update([
                        'tax_rate_id' => Setting::where('key', 'zero_tax')->first()->id
                    ]);
                }
            }

            if (is_null($holding->nominal_value)) {
                $holding->update(['nominal_value' => $holding->amount]);
            }
        });

        $this->info("Added nominal value and tax rates");

        DepositHolding::withTrashed()->whereHas('security', function ($q) {
            $q->where('fund_manager_id', 1);
        })->update([
            'unit_fund_id' => 4
        ]);

        $this->info("Done chys");

        DepositHolding::withTrashed()->whereHas('security', function ($q) {
            $q->where('fund_manager_id', 3);
        })->update([
            'unit_fund_id' => 5
        ]);

        $this->info("Done cpn");
    }
}
