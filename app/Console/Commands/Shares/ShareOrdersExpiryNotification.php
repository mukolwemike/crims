<?php

namespace App\Console\Commands\Shares;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Console\Command;

class ShareOrdersExpiryNotification extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'shares:orders-expiry-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron to notify ops of orders 1 week to expiry';

    /**
     * Create a new command instance.
     * ShareOrdersExpiryNotification constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Getting started...');
        $today = Carbon::today();
        $week_from_now = $today->copy()->addWeek();

        $this->info('Querying orders...');

        $sale_orders = SharesSalesOrder::matched(false)
            ->where('expiry_date', $week_from_now)
            ->get()
            ->map(
                function ($order) {
                    $sold = $order->matchedPurchaseOrders()->get()->sum(
                        function (SharesPurchaseOrder $order) {
                            return $order->pivot->number;
                        }
                    );

                    $e = new EmptyModel();

                    $e->{'Shareholder No.'} = $order->seller->number;
                    $e->{'Shareholder'} = ClientPresenter::presentFullNames($order->seller->client_id);
                    $e->{'Shares Entity'} = $order->seller->entity->name;
                    $e->{'No. of Shares'} = $order->number;
                    $e->{'Price'} = $order->price;
                    $e->{'Request Date'} = $order->request_date;
                    $e->{'Expiry Date'} = $order->expiry_date;
                    $e->{'Cancelled'} = BooleanPresenter::presentYesNo($order->cancelled);
                    $e->{'Sold Shares'} = $sold;
                    $e->{'Remaining Shares'} = $order->number - $sold;

                    return $e;
                }
            );

        $purchase_orders = SharesPurchaseOrder::matched(false)
            ->where('expiry_date', $week_from_now)
            ->get()
            ->map(
                function ($order) {
                    $bought = $order->matchedSaleOrders()->get()->sum(
                        function (SharesSalesOrder $order) {
                            return $order->pivot->number;
                        }
                    );

                    $e = new EmptyModel();

                    $e->{'Shareholder No.'} = $order->buyer->number;
                    $e->{'Shareholder'} = ClientPresenter::presentFullNames($order->buyer->client_id);
                    $e->{'Shares Entity'} = $order->buyer->entity->name;
                    $e->{'No. of Shares'} = $order->number;
                    $e->{'Price'} = $order->price;
                    $e->{'Request Date'} = $order->request_date;
                    $e->{'Expiry Date'} = $order->expiry_date;
                    $e->{'Cancelled'} = BooleanPresenter::presentYesNo($order->cancelled);
                    $e->{'Bought Shares'} = $bought;
                    $e->{'Remaining Shares'} = $order->number - $bought;
                    $e->{'Commission Recipient'} = is_null($order->recipient) ? null : $order->recipient->name;
                    $e->{'Commission rate'} = $order->rate;

                    return $e;
                }
            );

        if ($sale_orders->count() or $purchase_orders->count()) {
            $this->info('Exporting to Excel...');

            $file_name = 'Shares Sale & Purchase Orders Expiry Notification';

            Excel::fromModel($file_name, [
                'Sale Orders' => $sale_orders,
                'Purchase Orders' => $purchase_orders
            ])->store('xlsx');

            $this->info('Sending to your inbox...');

            $this->mailResult(storage_path('exports/' . $file_name . '.xlsx'));
        } else {
            $this->info('No orders were found');
        }
    }

    /**
     * @param $file_path
     */
    private function mailResult($file_path)
    {
        $mailer = new GeneralMailer();
        $mailer->to('operations@cytonn.com');
        $mailer->cc(['mchaka@cytonn.com']);
        $mailer->from('support@cytonn.com');
        $mailer->subject('Shares Sale & Purchase Orders Expiry Notification');
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail('Please find attached the Shares Sale & Purchase Orders Expiry report.');

        \File::delete($file_path);
    }
}
