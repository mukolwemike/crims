<?php

namespace App\Console\Commands\Shares;

use App\Cytonn\Models\SharesEntity;
use Illuminate\Console\Command;

class CalculateSharePrice extends Command
{
    protected $signature = 'shares:calculate-share-price';

    protected $description = 'Calculate the share price monthly for given share entity';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $entities = SharesEntity::all();

        foreach ($entities as $entity) {
            $totalShares = $entity->maximumIssues();
            $soldMonthly = $entity->soldShares();

            $price = $totalShares / $soldMonthly;

            foreach ($entity->priceTrails as $trail) {
                $trail->price = $price;
                $trail->save();
            }
        }
    }
}
