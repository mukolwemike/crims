<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/17/18
 * Time: 12:09 PM
 */
namespace App\Console\Commands\Shares;

use App\Cytonn\Mailers\Shares\UnmatchedSalesOrdersMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesSalesOrder;
use Carbon\Carbon;
use Cytonn\Api\Transformers\SharesSalesOrderTransformer;
use Cytonn\Shares\ShareTradingRepository;
use Cytonn\Shares\Trading\Buy;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class ReverseSharePurchasesPendingPayments extends Command
{
    protected $signature = 'shares:reverse-share-purchases-pending-payments';

    protected $description = 'reverse share purchases that are pending payments';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $purchases = $this->purchases();

        return $purchases->each(function ($purchase) {

            $buy = new Buy($purchase->shareHolder);

            $shareSaleOrder = $purchase->saleOrder;

            $period = Carbon::parse($purchase->date)->diffInDays(Carbon::now());

            if ($period > 14) {
                return $buy->reverse($shareSaleOrder, $purchase);
            }

            return;
        });
    }

    public function purchases()
    {
        return SharePurchases::whereNotNull('shares_sales_order_id')
            ->whereNull('client_payment_id')
            ->get();
    }
}
