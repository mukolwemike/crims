<?php

namespace App\Console\Commands\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ShareHolder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreateShareholders extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'shares:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Share holders to otc table structure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Client::has('shareHoldings')->get()
        ->each(
            function (Client $client) {
                DB::transaction(
                    function () use ($client) {
                        $this->migrate($client);
                    }
                );
            }
        );
    }

    private function migrate(Client $client)
    {
        $holdings = $client->shareHoldings()->get();

        $entities = $holdings->unique('entity_id')->lists('entity_id');

        foreach ($entities as $entity) {
            $holder = ShareHolder::where('entity_id', $entity)->where('client_id', $client->id)->first();
            $duplicate_client_code = ShareHolder::where('number', $client->client_code)->exists();

            if (!$holder and !$duplicate_client_code) {
                $holder = ShareHolder::create(
                    [
                    'client_id'=>$client->id,
                    'entity_id'=>$entity,
                    'number'   =>$client->client_code
                    ]
                );
            }

            if ($holder) {
                $holdings->each(
                    function ($holding) use ($holder) {
                        $holding->update(['share_holder_id'=>$holder->id]);
                    }
                );
            }
        }
    }
}
