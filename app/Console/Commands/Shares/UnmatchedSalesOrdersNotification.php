<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/17/18
 * Time: 12:09 PM
 */

namespace App\Console\Commands\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Api\Transformers\SharesSalesOrderTransformer;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Console\Command;

class UnmatchedSalesOrdersNotification extends Command
{
    protected $signature = 'shares:unmatched-sales-orders-notifications';

    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $sellers = $this->sellers();

        $sellers->each(function ($seller) {
            $this->sendNotification($seller);
        });
    }

    public function sellers()
    {
        return Client::whereHas('shareHolders', function ($holder) {
            $holder->whereHas('shareSalesOrders', function ($order) {
                $order->matched(false)->cancelled(false);
            });
        });
    }

    public function sendNotification(Client $client)
    {
        $orders = $this->orders($client);

        $view = 'emails.shares.unmatched_share_sales_orders';

        $data = [
            'orders' => $orders,
            'client' => $client
        ];

        Mailer::compose()
            ->to([$client->getContactEmailsArray()])
            ->cc(['operations@cytonn.com'])
            ->bcc(config('system.administrators'))
            ->subject('Unmatched Share Sales Orders')
            ->view($view, $data)
            ->send();

        $this->info('Unmatched share sales orders have been sent');

        return false;
    }

    public function orders(Client $client)
    {
        return SharesSalesOrder::matched(false)->cancelled(false)
            ->whereHas('seller', function ($seller) use ($client) {
                $seller->where('client_id', $client->id);
            })
            ->get()->map(function ($order) {
                return (new SharesSalesOrderTransformer())->transform($order);
            });
    }
}
