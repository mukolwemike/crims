<?php

namespace App\Exceptions;

use Cytonn\Exceptions\AuthorizationDeniedException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Handlers\ExceptionHandler as CytonnExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Laracasts\Validation\FormValidationException;
use Rollbar\Payload\Level;
use Rollbar\Rollbar;
use SSOManager\Exceptions\InvalidStateException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Auth\AuthenticationException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Validation\ValidationException::class,
        FormValidationException::class,
        CRIMSGeneralException::class,
        AuthorizationDeniedException::class,
        ModelNotFoundException::class,
        HttpException::class,
        \Swift_RfcComplianceException::class,
        TokenMismatchException::class,
        InvalidStateException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     * @throws Exception
     */
    public function report(Exception $e)
    {
        if (property_exists($e, 'dontReport')) {
            if ($e->dontReport) {
                return;
            }
        }

        if ($this->shouldntReport($e)) {
            return;
        }

        //only use rollbar for staging and production
        if (!in_array(app()->environment(), ['local', 'testing'])) {
            $this->reportNewRelic($e);
            $this->reportRollbar($e);
        }

        parent::report($e);

        if (method_exists($e, 'report')) {
            return $e->report();
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (method_exists($e, 'render')) {
            return $e->render($request);
        }

        $handled = app(CytonnExceptionHandler::class)->renderKnownClasses($request, $e);

        if ($handled) {
            return $handled;
        }

        // Custom error 500 view on production
        if (app()->environment() == 'production') {
            $ef = FlattenException::create($e);
            if ($ef->getStatusCode() == 500) {
                if ($request->wantsJson()) {
                    return response()->json([
                        'error' => 'Internal Server Error',
                        'message' => $ef->getMessage()
                    ], 500);
                }
                return response()->view('errors.500', [
                    'code'=>$ef->getStatusCode(),
                    'message'=>$ef->getMessage()
                ], 500);
            }
        }

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request                 $request
     * @param  \Illuminate\Auth\AuthenticationException $e
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $e)
    {
        if ($request->wantsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        } else {
            return redirect('/sso/authorize');
        }
    }

    private function getUser()
    {
        $u = auth()->user();

        $user = ['id'=>'guest', 'email'=>'guest@email.ex'];

        if (!is_null($u)) {
            return ['id'=>''.$u->id, 'username'=>$u->username, 'email'=>$u->email];
        }

        return $user;
    }

    private function reportRollbar($e)
    {
        $config = config('services.rollbar');
        $config['person'] = $this->getUser();

        Rollbar::init($config);

        Rollbar::log(Level::error(), $e);
    }

    private function reportNewRelic($e)
    {
        //send errors to new relic
        if (extension_loaded('newrelic')) {
            try {
                newrelic_set_appname('crimsAdmin');
                newrelic_notice_error(null, $e);
            } catch (Exception $ex) {
            }
        }
    }
}
