<?php

namespace App\Exceptions\CrimsClient;

use App\Exceptions\AuthorizationDeniedException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Rollbar\Payload\Level;
use Rollbar\Rollbar;
use Symfony\Component\HttpFoundation\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $e)
    {
        if (property_exists($e, 'dont_report')) {
            if ($e->dontReport) {
                return;
            }
        }

        if ($this->shouldntReport($e)) {
            return;
        }

        $this->reportNewRelic($e);

        //only use rollbar for staging and production
        if (app()->environment() != 'local') {
            $this->reportRollbar($e);
        }

        parent::report($e);

        if (method_exists($e, 'report')) {
            return $e->report();
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof TokenMismatchException and $request->expectsJson()) {
            return response()->json(['error'=>'TokenMismatch'], 500);
        }

        if ($e instanceof AuthorizationDeniedException && $request->wantsJson()) {
            return response()->json([
                'message' => 'No access to this client',
                'status' => Response::HTTP_UNAUTHORIZED
            ], Response::HTTP_UNAUTHORIZED);
        }

        if (method_exists($e, 'render')) {
            return $e->render($request);
        }

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request                 $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }

    private function getUser()
    {
        $u = auth()->user();

        $user = ['id'=>'guest', 'email'=>'guest@email.ex'];

        if (!is_null($u)) {
            return ['id'=>''.$u->id, 'username'=>$u->username, 'email'=>$u->email];
        }

        return $user;
    }

    private function reportRollbar($e)
    {
        $config = config('services.rollbar');
        $config['person'] = $this->getUser();

        Rollbar::init($config);

        Rollbar::log(Level::error(), $e);
    }

    private function reportNewRelic($e)
    {
        //send errors to new relic
        if (extension_loaded('newrelic')) {
            try {
                newrelic_set_appname('crimsClient');
                newrelic_notice_error(null, $e);
            } catch (Exception $ex) {
            }
        }
    }
}
