<?php
/**
 * Date: 15/07/2016
 * Time: 4:37 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Exceptions;

class CrimsException extends \Exception
{
    protected $code = 500;

    public function __construct($message = "", $code = 500, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
