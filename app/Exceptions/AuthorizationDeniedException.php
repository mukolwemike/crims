<?php

namespace App\Exceptions;

class AuthorizationDeniedException extends \Exception
{
    protected $code = 401;

    protected $message = "Authorization denied!";
}
