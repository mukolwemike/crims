<?php
/**
 * Date: 03/11/2017
 * Time: 10:24
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Requests\Investments;

use App\Cytonn\Models\ClientInvestment;
use Illuminate\Foundation\Http\FormRequest;

class Withdrawal extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $investment = ClientInvestment::find($this->route('id'));
        $maturity = $investment->maturity_date->toDateString();
        $invested = $investment->invested_date->toDateString();
        $value = $investment->repo->getTotalValueOfAnInvestment();

        return [
            'callback' => 'required',
            'end_date' => "nullable|required_if:premature,1|before:$maturity|after_or_equal:$invested",
            'amount'   => "nullable|required_if:partial_withdraw,1|numeric|max:$value"
        ];
    }


    public function authorize()
    {
        return true;
    }
}
