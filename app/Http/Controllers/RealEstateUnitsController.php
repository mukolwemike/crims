<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\Floor;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateCommissionRate;
use App\Cytonn\Models\RealestateForfeitureNotice;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstateSalesAgreement;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\RealestateUnitType;
use App\Cytonn\Models\Type;
use App\Cytonn\Models\UnitHolding;
use App\Events\RealEstate\UnitHasBeenEdited;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Realestate\Forms\ReservationForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Date: 23/05/2016
 * Time: 2:46 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateUnitsController extends Controller
{
    protected $reservationForm;

    /**
     * RealEstateUnitsController constructor.
     *
     * @param $reservationForm
     */
    public function __construct(ReservationForm $reservationForm)
    {
        $this->reservationForm = $reservationForm;

        parent::__construct();
    }

    /**
     * Display form to create a unit
     *
     * @param  $id
     * @param  null $unitId
     * @return mixed
     */
    public function getCreate($id, $unitId = null)
    {
        $project = Project::findOrFail($id);

        $unitTypes = RealestateUnitType::all()->lists('name', 'id');
        //        $unitSizes = $project->availableSizes();
        $groups = $project->unitGroups()->pluck('name', 'id')->all();

        is_null($unitId) ? $unit = new RealestateUnit() : $unit = RealestateUnit::findOrFail($unitId);

        $floors = Floor::whereHas('projectFloors', function ($q) use ($project) {
            $q->where('project_id', $project->id);
        })->pluck('floor', 'id')->toArray();

        $types = Type::whereHas('realEstateTypes', function ($q) use ($project) {
            $q->where('project_id', $project->id);
        })->pluck('name', 'id')->toArray();

        return view(
            'realestate.projects.units.create',
            [
                'title' => 'Create a unit',
                'project' => $project,
                'unit' => $unit,
                'unitTypes' => $unitTypes,
                'groups' => $groups,
                'floors' => $floors,
                'types' => $types
            ]
        );
    }

    /**
     * Store a unit from the form data
     *
     * @param  $id
     * @param  null $unitId
     * @return mixed
     */
    public function postStore(Request $request, $id, $unitId = null)
    {
        // Validation, check unit number is unique for each project
        $project = Project::findOrFail($id);

        $checkExistingUnit = function ($project, $unitNumber, $unitId) {
            $unit = $project->units()->where('number', $unitNumber);

            if ($unitId) {
                $unit->where('id', '!=', $unitId);
            }

            return $unit->exists();
        };

        if (trim($request->get('number'))
            && $checkExistingUnit($project, trim($request->get('number')), $unitId)) {
            \Flash::error('That unit number already exists for this project!');

            return redirect('/dashboard/realestate/projects/show/' . $project->id);
        }

        is_null($unitId) ? $unit = new RealestateUnit() : $unit = RealestateUnit::findOrFail($unitId);

        if ($unit->activeHolding && $unit->activeHolding->loo) {
            $holding = $unit->activeHolding;

            $original_data = [
                'loo_signed_date' => $holding->loo->sent_on,
                'unit_size' => $holding->unit->size->name,
                'unit_number' => $holding->unit->number,
                'unit_price' => (int) $holding->price()
            ];

            event(new UnitHasBeenEdited($unit->activeHolding, $original_data));
        }

        $input = \Request::only('type_id', 'size_id', 'number', 'group_id', 'real_estate_type_id', 'project_floor_id');

        if ($input['real_estate_type_id']) {
            $input['real_estate_type_id'] =
                $project->projectTypes()->where('type_id', $input['real_estate_type_id'])->first()->id;
        }

        if ($input['project_floor_id']) {
            $input['project_floor_id'] =
                $project->projectFloors()->where('floor_id', $input['project_floor_id'])->first()->id;
        }

        $unit->fill($input);
        $unit->project_id = $project->id;
        $unit->save();

        \Flash::success('The unit has been saved');

        return \redirect('/dashboard/realestate/projects/show/' . $project->id);
    }

    /**
     * @param $projectId
     * @param $unitId
     * @return mixed
     */
    public function getShow($projectId, $unitId)
    {
        $project = Project::findOrFail($projectId);

        $unit = RealestateUnit::findOrFail($unitId);

        $holding = UnitHolding::where('unit_id', $unitId)->where('active', 1)->first();

        $tranches = RealEstateUnitTranche::where('project_id', $projectId)->get()->lists('name', 'id');

        $payment_plans = RealEstatePaymentPlan::all()->lists('name', 'id');

        $recipients = ['' => 'Select Fa'] + (new CommissionRecepient())->repo->getRecipientsForSelect();

        $payments = new Collection();
        $paid = false;
        $total = 0;
        $loo = null;
        $sales_agreement = null;
        $previous_sas = null;
        $rateName = '';
        $balance = null;
        $refunds = null;


        if ($holding) {
            $payments = $holding->payments()->orderBy('date', 'ASC')->get();
            $paid = (bool)$payments->first();
            $total = $holding->repo->netUnitTotalPaid();
            $balance = $payments->sum('amount') - $holding->refundedAmount();
            $refunds = $holding->refunds;
            is_null($holding->salesAgreement) ?
                $sales_agreement = RealEstateSalesAgreement::make($holding, \Carbon\Carbon::today())
                : $sales_agreement = $holding->salesAgreement;

            $loo = $holding->loo;

            $history = $sales_agreement->history;

            if (!is_array($history)) {
                $history = [];
            }

            $previous_sas = Document::whereIn('id', $history)
                ->latest()
                ->get()
                ->each(
                    function ($doc) {
                        $doc->name =
                            'Uploaded on ' . \Cytonn\Presenters\DatePresenter::formatDateTime($doc->created_at);
                    }
                );

            $currentRate = $holding->commission ? $holding->commission->rate : null;

            $rateName = $currentRate ? $currentRate->recipient_type_and_name : "Zero Commission - 0%";
        }

        return view('realestate.projects.units.show', [
            'title' => $project->name . ' unit ' . $unit->number,
            'unit' => $unit,
            'project' => $project,
            'holding' => $holding,
            'payments' => $payments,
            'paid' => $paid,
            'total' => $total,
            'loo' => $loo,
            'sales_agreement' => $sales_agreement,
            'tranches' => $tranches,
            'payment_plans' => $payment_plans,
            'recipients' => $recipients,
            'previous_sas' => $previous_sas,
            'commissionRateName' => $rateName,
            'balance' => $balance,
            'refunds' => $refunds,
        ]);
    }

    public function getShowForfeiture($id)
    {
        $notice = RealestateForfeitureNotice::findOrFail($id);

        $holding = $notice->holding;

        $project = $holding->unit->project;

        $unit = $holding->unit;

        return view('realestate.projects.units.show_forfeiture_notice', [
            'title' => $project->name . ' unit ' . $unit->number,
            'unit' => $unit,
            'project' => $project,
            'holding' => $holding,
            'forfeiture' => $notice,
        ]);
    }

    /**
     * @param $projectId
     * @param $unitId
     * @param $client_id
     * @return mixed
     */
    public function getCancelled($projectId, $unitId, $client_id)
    {
        $project = Project::findOrFail($projectId);

        $unit = RealestateUnit::findOrFail($unitId);

        $holding = UnitHolding::where(['unit_id' => $unitId, 'client_id' => $client_id])
            ->where('active', 0)->first();

        $isCurrentlyReservedByAnotherClient = UnitHolding::where('unit_id', $unitId)
            ->where('active', 1)->exists();

        $tranches = RealEstateUnitTranche::where('project_id', $projectId)->lists('name', 'id');

        $payment_plans = RealEstatePaymentPlan::lists('name', 'id');

        $recipients = (new CommissionRecepient())->repo->getRecipientsForSelect();

        $payments = new Collection();
        $paid = false;
        $total = 0;
        $loo = null;
        $sales_agreement = null;
        $previous_sas = null;
        if ($holding) {
            $payments = $holding->payments()->orderBy('date', 'ASC')->get();
            $paid = (bool)$payments->first();
            $total = $payments->sum('amount');
            is_null($holding->salesAgreement) ?
                $sales_agreement = RealEstateSalesAgreement::make($holding, \Carbon\Carbon::today())
                : $sales_agreement = $holding->salesAgreement;

            $loo = $holding->loo;
            is_null($sales_agreement->history) ?:
                $previous_sas = Document::whereIn('id', $sales_agreement->history)
                    ->latest()
                    ->get()
                    ->each(
                        function ($doc) {
                            $doc->name = 'Uploaded on ' . DatePresenter::formatDateTime($doc->created_at);
                        }
                    );

            $currentRate = $holding->commission ? $holding->commission->rate : null;

            $rateName = $currentRate ? $currentRate->recipient_type_and_name : "Zero Commission - 0%";
        }
        $commission_rates = RealestateCommissionRate::where('project_id', $unit->project->id)->get();

        return view(
            'realestate.projects.units.show_forfeited',
            [
                'title' => $project->name . ' unit ' . $unit->number, 'unit' => $unit,
                'project' => $project, 'holding' => $holding,
                'payments' => $payments, 'paid' => $paid, 'total' => $total, 'loo' => $loo,
                'sales_agreement' => $sales_agreement, 'tranches' => $tranches, 'payment_plans' => $payment_plans,
                'recipients' => $recipients, 'commission_rates' => $commission_rates, 'previous_sas' => $previous_sas,
                'isCurrentlyReservedByAnotherClient' => $isCurrentlyReservedByAnotherClient,
                'commissionRateName' => $rateName
            ]
        );
    }

    /**
     * @param $projectId
     * @param $unitId
     * @return mixed
     */
    public function getTransfer($projectId, $unitId)
    {
        $project = Project::findOrFail($projectId);
        $unit = RealestateUnit::findOrFail($unitId);
        $holding = UnitHolding::where('unit_id', $unitId)->where('active', 1)->first();

        return view(
            'realestate.projects.units.transfer',
            [
                'title' => $project->name . ' unit ' . $unit->number . ' transfer', 'unit' => $unit,
                'project' => $project, 'holding' => $holding
            ]
        );
    }

    /**
     * @param $projectId
     * @param $unitId
     * @return mixed
     */
    public function getReserve($projectId, $unitId)
    {
        $unit = RealestateUnit::findOrFail($unitId);

        $signingMandates = (new ClientRepository())->signingMandates();

        return view('realestate.projects.units.reserve', [
            'title' => 'Reserve Unit Application',
            'type' => 'reserve_unit',
            'signingMandates' => $signingMandates,
            'edit' => false,
            'applicationId' => null,
            'unit' => $unit
        ]);
    }

    /**
     * @param $projectId
     * @param $unitId
     * @param $client_id
     * @return mixed
     */
    public function getForfeiture($projectId, $unitId, $client_id)
    {
        $project = Project::findOrFail($projectId);

        $unit = RealestateUnit::findOrFail($unitId);

        $holding = UnitHolding::where(['unit_id' => $unitId, 'client_id' => $client_id])
            ->where('active', 1)->first();

        return \View::make(
            'realestate.projects.units.forfeitures',
            [
                'title' => $project->name . ' unit ' . $unit->number . ' forfeiture',
                'unit' => $unit, 'holding' => $holding, 'project' => $project
            ]
        );
    }

    public function sendForfeitureNotice($id)
    {
        $holding = UnitHolding::findOrFail($id);

        (new \Cytonn\Mailers\ForfeitureLetterMailer())->sendMail($holding, Auth::user());

        \Flash::success('Notice Approved and Letter sent');

        return \Redirect::back();
    }

    public function sendForfeitureNoticeApproval($id)
    {
        $holding = UnitHolding::findOrFail($id);

        $notice = $holding->repo->currentActiveFeitureNotice();

        if ($notice) {
            \Flash::error('Forfeiture Notice already sent for approval');

            return \Redirect::back();
        }

        RealestateForfeitureNotice::create([
            'unit_holding_id' => $id,
            'sent_by' => auth()->user()->id,
        ]);

        \Flash::success('Forfeiture Notice sent for approval');

        return \Redirect::back();
    }

    public function approveForfeitureNotice($id)
    {
        RealestateForfeitureNotice::where('unit_holding_id', $id)
            ->where('status', 0)->first()
            ->update(['reviewed_by' => auth()->user()->id, 'status' => 1]);

        $this->sendForfeitureNotice($id);

        return \Redirect::back();
    }

    public function cancelForfeitureNotice(Request $request)
    {
        if (is_null($request->review_reason)) {
            \Flash::error('Please provide a reason for cancel!');
            return \Redirect::back();
        }

        RealestateForfeitureNotice::where('unit_holding_id', $request->unit_id)
            ->where('status', 0)->first()
            ->update(['reviewed_by' => auth()->user()->id, 'status' => 2, 'review_reason' => $request->review_reason]);

        \Flash::success('Forfeiture Notice canceled');

        return \Redirect::back();
    }

    public function getForfeitureNotices()
    {
        $projects = Project::all();

        return view('realestate.projects.units.forfeiture_notices_table', [
            'projects' => $projects,
            'title' => 'Realestate Forfeitures'
        ]);
    }

    public function activate($id)
    {
        $holding = UnitHolding::findorFail($id);

        $holding->receive_campaign_statement = true;

        $holding->update();

        \Flash::success('Monthly Statements Activated Successfully');

        return back();
    }

    public function deactivate($id)
    {
        $holding = UnitHolding::findorFail($id);

        $holding->receive_campaign_statement = false;

        $holding->update();

        \Flash::success('Monthly Statements Deactivated Successfully');

        return back();
    }
}
