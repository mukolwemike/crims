<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientContactPersonRelationship;
use App\Cytonn\Models\CoopMembershipForm;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Notifier\FlashNotifier;
use Illuminate\Support\Facades\Request;

/**
 * Class CoopMembershipsFormController
 */
class CoopMembershipFormsController extends Controller
{

    /**
     * @var Authorizer
     */
    protected $authorizer;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
        $this->authorizer->checkAuthority('viewclients');
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('coop.memberships.index', ['title'=>'Coop Membership Forms']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getEdit($id)
    {
        $membership_form = CoopMembershipForm::findOrFail($id);
        if ($membership_form->approval_id) {
            Flash::error('Cannot edit an approved coop membership!');
            return redirect('/dashboard/coop/memberships');
        }
        $this->authorizer->checkAuthority('editclients');
        $relationships = ClientContactPersonRelationship::lists('relationship', 'id');
        return view('coop.memberships.edit', ['form'=>$membership_form, 'relationships'=>$relationships]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function putUpdate($id)
    {
        $this->authorizer->checkAuthority('editclients');
        $input = \Request::except(
            '_token',
            'client_contact_person_name',
            'id_number',
            'relationship_id',
            'percentage_share'
        );

        $client = new Client();

        $duplicate = $client->repo->checkIfIndividualClientExists($input);

        if ($duplicate) {
            throw new ClientInvestmentException('Another client already exists with these details.');
        }

        $contact_persons = Collection::make(
            Request::only(['client_contact_person_name', 'id_number', 'relationship_id', 'percentage_share'])
        )->transpose()->toArray();

        $input['contact_persons'] = $contact_persons;

        if (strlen(trim($input['referee_id'])) == 0) {
            $input['referee_id'] = null;
        }

        $form = CoopMembershipForm::findOrFail($id);
        $form->update($input);
        $form->save();

        Flash::message('Coop membership details have been updated. Awaiting approval.');
        return redirect('/dashboard/clients');
    }
}
