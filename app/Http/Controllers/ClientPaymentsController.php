<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientPaymentAllowedMinimum;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use App\Cytonn\Models\EmptyModel;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Custodial\Instruction;
use Cytonn\Custodial\Withdraw;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\Forms\TransferCashToAnotherClientForm;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use function foo\func;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class ClientPaymentsController
 */
class ClientPaymentsController extends Controller
{



    /**
     * @var $transferCashToAnotherClientForm
     */
    protected $transferCashToAnotherClientForm;

    /**
     * ClientPaymentsController constructor.
     *
     * @param TransferCashToAnotherClientForm $transferCashToAnotherClientForm
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function __construct(TransferCashToAnotherClientForm $transferCashToAnotherClientForm)
    {
        parent::__construct();
        $this->authorizer->checkAuthority('viewinvestments');
        $this->transferCashToAnotherClientForm = $transferCashToAnotherClientForm;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $projects = Project::all();
        $products = Product::all();
        $funds = UnitFund::active()->get();

        return view(
            'investment.payments.index',
            ['title' => 'Client Payments', 'projects' => $projects, 'products' => $products, 'funds' => $funds]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $payment = ClientPayment::findOrFail($id);
        $client = is_null($payment->client_id) ? new Client() : $payment->client;
        $recipient = is_null($payment->recipient_id) ? new CommissionRecepient() : $payment->recipient;
        $transfers = ClientPayment::whereHas(
            'type',
            function ($type) {
                $type->where('slug', 'TO');
            }
        )->where('client_id', $client->id)->get();
        $users = User::all()->lists('full_name', 'id');
        $transaction = $payment->custodialTransaction;

        return view(
            'investment.payments.show',
            [
                'title' => 'Client Payment Details', 'payment' => $payment, 'users' => $users,
                'client' => $client, 'recipient' => $recipient, 'transfers' => $transfers, 'transaction' => $transaction
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getClient($id)
    {
        $client = Client::findOrFail($id);
        $payments = ClientPayment::where('client_id', $client->id)->get();

        $client_products = Product::whereIn('id', $payments->unique('product_id')->lists('product_id'))
            ->get()
            ->map(function ($product) use ($client) {
                $p = new EmptyModel();
                $p->type = 'product';
                $p->id = $product->id;
                $p->name = $product->name;
                $p->balance = (new ClientPayment())->balance($client, $product);
                return $p;
            });

        $client_projects = Project::whereIn('id', $payments->unique('project_id')->lists('project_id'))
            ->get()
            ->map(
                function ($project) use ($client) {
                    $p = new EmptyModel();
                    $p->type = 'project';
                    $p->id = $project->id;
                    $p->name = $project->name;
                    $p->balance = (new ClientPayment())->balance($client, null, $project);
                    return $p;
                }
            );

        $client_entities = SharesEntity::whereIn(
            'id',
            $payments
                ->unique('share_entity_id')
                ->lists('share_entity_id')
        )
            ->get()
            ->map(
                function ($entity) use ($client) {
                    $e = new EmptyModel();
                    $e->type = 'shares';
                    $e->id = $entity->id;
                    $e->type_id = $e->type . '-' . $e->id;
                    $e->name = $entity->name;
                    $e->balance = (new ClientPayment())->balance($client, null, null, $entity);
                    return $e;
                }
            );

        $client_unit_fund =
            UnitFund::whereIn('id', $payments->unique('unit_fund_id')->lists('unit_fund_id'))
                ->get()
                ->map(function ($unitFund) use ($client) {
                    $f = new EmptyModel();
                    $f->type = 'unitFund';
                    $f->id = $unitFund->id;
                    $f->name = $unitFund->name;
                    $f->balance = (new ClientPayment())->balance(
                        $client,
                        null,
                        null,
                        null,
                        null,
                        $unitFund
                    );
                    return $f;
                });

        $show_link = true;

        $balances = (new Collection())
            ->merge($client_products->all())
            ->merge($client_projects->all())
            ->merge($client_entities->all())
            ->merge($client_unit_fund->all());

        $projects = Project::all();
        $products = Product::all();
        $shareentities = SharesEntity::all();

        $banksCollection = $client->bankAccounts
            ->filter(
                function ($account) {
                    return !is_null($account->branch_id);
                }
            )
            ->map(
                function ($account) {
                    $branch = ClientBankBranch::find($account->branch_id);

                    return [
                        $account->id => $branch->bank->name . ' - ' . $account->account_name . ' - ' .
                            $account->account_number
                    ];
                }
            );

        $banks = [];

        foreach ($banksCollection as $item) {
            $banks[key($item)] = $item[key($item)];
        }

        $users = User::all()->mapWithKeys(
            function ($user) {
                return [$user['id'] => $user['full_name']];
            }
        );

        return view(
            'investment.payments.client',
            [
                'title' => 'Client Transactions : ' . ClientPresenter::presentFullNames($client->id),
                'payments' => $payments,
                'client' => $client,
                'balances' => $balances,
                'show_link' => $show_link,
                'users' => $users,
                'projects' => $projects,
                'products' => $products,
                'shareentities' => $shareentities,
                'banks' => $banks
            ]
        );
    }

    /**
     * @param $client_id
     * @return mixed
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    public function postWithdraw($client_id)
    {
        $client = Client::findOrFail($client_id);
        $date = Carbon::parse(\Request::get('date'));
        $approval = null;
        $current_payment = null;

        // Save Instruction and generate it
        $user1 = User::findOrFail(\Request::get('first_signatory'));
        $user2 = User::findOrFail(\Request::get('second_signatory'));
        $bank_id = \Request::get('client_bank_account_id');
        $client_bank_account = ClientBankAccount::find($bank_id);

        // Distinguish between payment withdrawal and balance withdrawal
        $balance_id = \Request::get('balance_id');

        if (is_null($balance_id)) {
            $current_payment = ClientPayment::findOrFail(\Request::get('payment_id'));

            $amount = $current_payment->amount;

            $approval = $current_payment->getWithdrawApproval();

            if ($product = $current_payment->product) {
                $account = $product->custodialAccount;
            }
            if ($project = $current_payment->project) {
                $account = $project->custodialAccount;
            }
            if ($entity = $current_payment->shareEntity) {
                $account = $entity->custodialAccount;
            }

            if ($fund = $current_payment->fund) {
                $account = $fund->getDisbursementAccount();
            }

            if ($current_payment->child && ($current_payment->type->slug != 'TO')) {
                throw new CRIMSGeneralException('Amount has already been withdrawn');
            }
        } else {
            // Either product or project
            $product = $project = $entity = $fund = null;

            $type = \Request::get('balance_type');

            if ($type == 'product') {
                $product = Product::findOrFail($balance_id);
                $account = $product->custodialAccount;
            } elseif ($type == 'project') {
                $project = Project::findOrFail($balance_id);
                $account = $project->custodialAccount;
            } elseif ($type == 'shares') {
                $entity = SharesEntity::findOrFail($balance_id);
                $account = $entity->custodialAccount;
            } elseif ($type == 'unitFund') {
                $fund = UnitFund::findOrFail($balance_id);

                $disbursementAcc = $fund->operatingAccounts()->wherePivot('role', 'disbursement')->first();

                $account = $fund->custodialAccount;
                if ($disbursementAcc) {
                    $account = $disbursementAcc;
                }
            }

            $amount = \Request::get('amount');
        }

        if (!$project && !$product && !$entity && !$fund) {
            throw new CRIMSGeneralException('Payment must be linked to product/project/entity');
        }

        if (!$account) {
            throw new CRIMSGeneralException('Withdrawal must be linked to an account');
        }
        if ($current_payment) {
            if ($current_payment->type->slug == 'TO') {
                $amount = abs($amount);
            }
        }

        $description = 'Withdrawal of ' . AmountPresenter::currency($amount) . ' by ' .
            ClientPresenter::presentFullNames($client->id);

        if ($amount <= 0) {
            throw new ClientInvestmentException('Amount to be transferred cannot be zero');
        }

        return Withdraw::make(
            $date,
            $account,
            $description,
            $client,
            $amount,
            $product,
            $project,
            $entity,
            $approval,
            $current_payment,
            $user1,
            $user2,
            $client_bank_account,
            null,
            $fund
        )->view();
    }

    public function postScheduledInstruction($schedule_id)
    {
        return \DB::transaction(
            function () use ($schedule_id) {
                return $this->runScheduledInstruction($schedule_id);
            }
        );
    }

    /**
     * @param $schedule_id
     * @return mixed
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    private function runScheduledInstruction($schedule_id)
    {
        $schedule = ClientTransactionApproval::findOrFail($schedule_id);

        $client = $schedule->investment->client;
        $product = $schedule->investment->product;
        $account = $product->custodialAccount;

        $withdrawal_date = Carbon::parse($schedule->action_date);
        $product_id = $schedule->investment->product_id;
        $client_id = $schedule->investment->client_id;

        $value = $schedule->investment->calculate($withdrawal_date, false)->value();
        $penalty = $schedule->handler()->calculatePenalty($schedule);
        $withdraw = $schedule->handler()->calculateAmountWithdrawn($schedule);

        $amount = $withdraw - $penalty;
        if ($value >= ($penalty + $withdraw)) {
            $amount = $withdraw;
        }

        //check if exists
        if (ClientPaymentAllowedMinimum::where('approval_id', $schedule_id)->exists()) {
            throw new ClientInvestmentException("Instruction has already been generated for this schedule");
        }

        ClientPaymentAllowedMinimum::create(
            [
                'approval_id' => $schedule_id,
                'product_id' => $product_id,
                'client_id' => $client_id,
                'date' => $withdrawal_date,
                'amount' => $amount
            ]
        );

        $date = Carbon::parse(\Request::get('date'));

        // Save Instruction and generate it
        $user1 = User::findOrFail(\Request::get('first_signatory'));
        $user2 = User::findOrFail(\Request::get('second_signatory'));
        $bank_id = \Request::get('client_bank_account_id');
        $client_bank_account = ClientBankAccount::find($bank_id);

        if (!$product) {
            throw new CRIMSGeneralException('Payment must be linked to a product');
        }

        $description = 'Withdrawal of ' . AmountPresenter::currency($amount) . ' by ' .
            ClientPresenter::presentFullNames($client->id);

        if ($amount <= 0) {
            throw new ClientInvestmentException('Amount to be transferred cannot be zero');
        }

        $instr = Withdraw::make(
            $date,
            $account,
            $description,
            $client,
            $amount,
            $product,
            null,
            null,
            null,
            null,
            $user1,
            $user2,
            $client_bank_account
        );

        $schedule->investment->update(['withdrawal_instruction_id' => $instr->getInstruction()->id]);

        return $instr->view();
    }


    /**
     * @param $payment_id
     * @return mixed
     * @throws CRIMSGeneralException
     */
    public function getPdf($payment_id)
    {
        $payment = ClientPayment::findOrFail($payment_id);

        $transaction = $payment->custodialTransaction;

        if (!$transaction) {
            throw new CRIMSGeneralException('This transaction cannot have an instruction');
        }

        $instruction = $transaction->bankInstruction;

        if (!$instruction) {
            throw new CRIMSGeneralException("Instruction has not been generated for this transaction");
        }

        return (new Instruction($instruction))->view();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTransact($id)
    {
        $client = Client::findOrFail($id);
        $payments = ClientPayment::where('client_id', $client->id)->get();

        $client_products = Product::whereIn('id', $payments->lists('product_id'))
            ->get()
            ->map(
                function ($product) use ($client) {
                    $p = new EmptyModel();
                    $p->type = 'product';
                    $p->id = $product->id;
                    $p->type_id = $p->type . '-' . $p->id;
                    $p->name = $product->name;
                    $p->balance = (new ClientPayment())->balance($client, $product);
                    return $p;
                }
            );

        $client_projects = Project::whereIn('id', $payments->lists('project_id'))
            ->get()
            ->map(
                function ($project) use ($client) {
                    $p = new EmptyModel();
                    $p->type = 'project';
                    $p->id = $project->id;
                    $p->type_id = $p->type . '-' . $p->id;
                    $p->name = $project->name;
                    $p->balance = (new ClientPayment())->balance($client, null, $project);
                    return $p;
                }
            );

        $client_entities = SharesEntity::whereIn('id', $payments->lists('share_entity_id'))
            ->get()
            ->map(
                function ($entity) use ($client) {
                    $e = new EmptyModel();
                    $e->type = 'shares';
                    $e->id = $entity->id;
                    $e->type_id = $e->type . '-' . $e->id;
                    $e->name = $entity->name;
                    $e->balance = (new ClientPayment())->balance($client, null, null, $entity);
                    return $e;
                }
            );

        $clientUnitFunds = UnitFund::whereIn('id', $payments->lists('unit_fund_id'))
            ->get()
            ->map(
                function ($fund) use ($client) {
                    $e = new EmptyModel();
                    $e->type = 'funds';
                    $e->id = $fund->id;
                    $e->type_id = $e->type . '-' . $e->id;
                    $e->name = $fund->name;
                    $e->balance = (new ClientPayment())->balance($client, null, null, null, null, $fund);
                    return $e;
                }
            );

        $users = User::all()->lists('full_name', 'id');
        $projects = Project::all()->lists('name', 'id');
        $products = Product::all()->lists('name', 'id');
        $shareentities = SharesEntity::all()->lists('name', 'id');
        $funds = UnitFund::all()->lists('name', 'id');

        $show_link = false;

        $balances = (new Collection())->merge($client_products->all())
            ->merge($client_projects->all())->merge($client_entities->all())
            ->merge($clientUnitFunds->all());

        $banks = $client->bankAccounts()->lists('account_name', 'id');

        return view(
            'investment.payments.transact',
            [
                'title' => 'Client Transactions | Tranfer Cash : ' . ClientPresenter::presentFullNames($client->id),
                'payments' => $payments, 'client' => $client, 'balances' => $balances,
                'show_link' => $show_link, 'users' => $users, 'projects' => $projects,
                'products' => $products, 'shareentities' => $shareentities, 'funds' => $funds, 'banks' => $banks
            ]
        );
    }

    /**
     * @return mixed
     */
    public function getExportBalances()
    {
        $user = Auth::user()->id;

        $this->queue(
            function () use ($user) {
                Artisan::call('custody:client-balances', ['user_id' => $user]);
            }
        );

        \Flash::message('The balances are being calculated, a report will be mailed soon');

        return \Redirect::back();
    }

    /**
     * @param $client_id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postTransferToAnotherClient($client_id)
    {
        $data = \Request::all();
        $data['recipient_known'] = (bool)$data['recipient_known'];

        $this->transferCashToAnotherClientForm->validate($data);

        // Get the source account
        $balance_type_id = \Request::get('balance_type_id'); // e.g 'project-3'
        $balance_type_id_array = explode('-', $balance_type_id);
        $type = $balance_type_id_array[0];
        $id = $balance_type_id_array[1];

        // Get the destination account
        $sending_client = Client::findOrFail($client_id);
        $receiving_client = Client::findOrFail(\Request::get('client_id'));

        // Get Sending Client's project/product/entity
        if ($type == 'project') {
            $previous_project = Project::findOrFail($id);
            $input['previous_project_id'] = $previous_project->id;
            $balance = (new ClientPayment())->balance($sending_client, null, $previous_project);
        } elseif ($type == 'product') {
            $previous_product = Product::findOrFail($id);
            $input['previous_product_id'] = $previous_product->id;
            $balance = (new ClientPayment())->balance($sending_client, $previous_product);
        } elseif ($type == 'shares') {
            $previous_entity = SharesEntity::findOrFail($id);
            $input['previous_entity_id'] = $previous_entity->id;
            $balance = (new ClientPayment())->balance($sending_client, null, null, $previous_entity);
        } elseif ($type == 'funds') {
            $previousFund = UnitFund::findOrFail($id);
            $input['previous_fund_id'] = $previousFund->id;
            $balance = (new ClientPayment())->balance($sending_client, null, null, null, null, $previousFund);
        }

        // Validate Amount
        $amount = \Request::get('amount');
        if ($amount < 0) {
            \Flash::error('The amount to be transferred cannot be less than zero!');
            return \Redirect::back();
        } elseif ($amount > $balance) {
            \Flash::error('The amount to be transferred cannot be more than the balance in that product/project!');
            return \Redirect::back();
        }

        // Get Receiving Client's project/product/entity
        if (\Request::get('category') == 'project') {
            $new_project = Project::findOrFail(\Request::get('project_id'));
            $input['new_project_id'] = $new_project->id;
            $receiving_account = $new_project->custodialAccount;
        } elseif (\Request::get('category') == 'product') {
            $new_product = Product::findOrFail(\Request::get('product_id'));
            $input['new_product_id'] = $new_product->id;
            $receiving_account = $new_product->custodialAccount;
        } elseif (\Request::get('category') == 'shares') {
            $new_entity = SharesEntity::findOrFail(\Request::get('entity_id'));
            $input['new_entity_id'] = $new_entity->id;
            $receiving_account = $new_entity->custodialAccount;
        } elseif (\Request::get('category') == 'funds') {
            $newFund = UnitFund::findOrFail(\Request::get('unit_fund_id'));
            $input['new_fund_id'] = $newFund->id;
            $receiving_account = $newFund->custodialAccount;
        }

        // Get the commission recipient of receiving client
        $recipient = (bool)\Request::get('recipient_known') == true
            ? CommissionRecepient::findOrFail(\Request::get('commission_recipient_id'))
            : $receiving_client->getLatestFA();
        $rate = (\Request::has('effective_rate') && !is_null(\Request::get('effective_rate')))
            ? \Request::get('effective_rate') : \Request::get('exchange_rate');
        $input['amount'] = \Request::get('amount');
        $input['date'] = \Request::get('date');
        $input['exchange_rate'] = $rate;
        $input['custodial_account_id'] = $receiving_account->id;
        $input['sending_client_id'] = $sending_client->id;
        $input['receiving_client_id'] = $receiving_client->id;
        $input['commission_recipient_id'] = is_null($recipient) ? null : $recipient->id;
        $input['amount'] = abs(\Request::get('amount'));

        ClientTransactionApproval::make($sending_client->id, 'transfer_cash_to_another_client', $input);

        \Flash::message('The transfer request has been saved for approval');

        return \redirect('/dashboard/investments/client-payments/client/' . $sending_client->id);
    }

    public function exemptClientPayment($clientId)
    {
        $client = Client::findOrFail($clientId);

        $input = request()->except('_token');

        $input['client_id'] = $client->id;

        $input['user_id'] = Auth::id();

        ClientTransactionApproval::add([
            'client_id' => $client->id,
            'transaction_type' => 'exempt_client_withdrawal',
            'scheduled' => false,
            'payload' => $input
        ]);

        \Flash::message('Client Payment Exemption has been saved for approval');

        return \Redirect::back();
    }

    public function deleteDestroy($id)
    {
        $validator = Validator::make(\Request::all(), [
            'reason' => 'required|min:10',
        ]);

        if ($validator->fails()) {
            \Flash::error('The reason must be at least 10 characters.');
            return \Redirect::back();
        }

        $transfer = ClientPayment::findOrFail($id);

        $transfer_to = ClientPayment::where('parent_id', $id)->whereNull('custodial_transaction_id')->first();

        if (!$transfer_to) {
            \Flash::error('You cannot reverse this transaction here.');
            return \Redirect::back();
        }

        //identify types for amount validation
        $projectId = $transfer_to->project_id;
        $productId = $transfer_to->product_id;
        $shareEntityId = $transfer_to->share_entity_id;
        $unitFundId = $transfer_to->unit_fund_id;

        //get recipient
        $receiving_client = Client::findOrFail($transfer_to->client_id);

        //validate recipient balance
        if ($projectId) {
            $project = Project::findOrFail($projectId);
            $balance = (new ClientPayment())->balance($receiving_client, null, $project);
        } elseif ($productId) {
            $product = Product::findOrFail($productId);
            $balance = (new ClientPayment())->balance($receiving_client, $product);
        } elseif ($shareEntityId) {
            $entity = SharesEntity::findOrFail($shareEntityId);
            $balance = (new ClientPayment())->balance($receiving_client, null, null, $entity);
        } elseif ($unitFundId) {
            $unitFund = UnitFund::findOrFail($unitFundId);
            $balance = (new ClientPayment())->balance($receiving_client, null, null, null, null, $unitFund);
        }

        if ($balance < abs($transfer->amount)) {
            \Flash::error(ClientPresenter::presentFullNames($transfer_to->client_id) . ' does not have enough cash balance');
            return \Redirect::back();
        }

        $input['payment_id'] = $id;
        $input['date'] = $transfer->date;
        $input['amount'] = abs($transfer->amount);
        $input['reason'] = \Request::get('reason');
        $input['transfer_description'] = $transfer->description;

        ClientTransactionApproval::make($transfer->client_id, 'reverse_cash_transfer', $input);

        \Flash::message('Reverse Cash Transfer has been saved for approval');

        return \Redirect::back();
    }
}
