<?php

namespace App\Http\Controllers;

use App\Cytonn\Dashboard\InvestmentsDashboardGenerator;
use App\Cytonn\Mailers\Investments\AcknowledgementNotificationMailer;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientBusinessConfirmation;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\InterestPayment;
use App\Cytonn\Models\InterestPaymentSchedule;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Dashboard\DashboardGenerator;
use Cytonn\Exceptions\ClientInvestmentException as Error;
use Cytonn\Forms\InvestmentBankInstructionvalidator;
use Cytonn\Investment\Forms\BusinessConfirmationForm;
use Cytonn\Investment\Forms\ClientRolloverForm;
use Cytonn\Investment\Forms\ClientTopupForm;
use Cytonn\Investment\Forms\CombinedRolloverForm;
use Cytonn\Investment\Forms\EditInvestmentForm;
use Cytonn\Investment\Forms\InterestPaymentScheduleForm;
use Cytonn\Investment\Forms\PaymentForm;
use Cytonn\Investment\Forms\WithdrawalForm;
use Cytonn\Investment\InterestRepository;
use Cytonn\Mailers\ClientMaturityNotificationMailer;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Reporting\BankInstructionGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

/**
 * Handles Clients Investments Operations
 * Class InvestmentsController
 */
class InvestmentsController extends Controller
{

    /**
     * @var \Cytonn\Investment\Forms\BusinessConfirmationForm
     */
    protected $confirmationForm;

    /**
     * @var \Cytonn\Investment\Forms\WithdrawalForm
     */
    protected $withdrawalForm;

    /**
     * @var
     */
    protected $rolloverForm;

    /**
     * @var PaymentForm
     */
    protected $paymentForm;

    /**
     * @var ClientTopupForm
     */
    protected $topupForm;

    /**
     * @var EditInvestmentForm
     */
    protected $editInvestmentForm;

    /**
     * @var InvestmentBankInstructionvalidator
     */
    protected $investmentBankInstructionForm;

    /**
     * @var InterestPaymentScheduleForm
     */
    protected $interestPaymentScheduleForm;

    /**
     * Used to check for authorization
     *
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;


    /**
     * InvestmentsController constructor.
     * Inject the form classes for validation
     *
     * @param BusinessConfirmationForm $confirmationForm
     * @param WithdrawalForm $withdrawalForm
     * @param ClientRolloverForm $rolloverForm
     * @param ClientTopupForm $topupForm
     * @param PaymentForm $paymentForm
     * @param EditInvestmentForm $editInvestmentForm
     * @param InvestmentBankInstructionvalidator $investmentBankInstructionvalidator
     * @param CombinedRolloverForm $combinedRolloverForm
     * @param InterestPaymentScheduleForm $interestPaymentScheduleForm
     */
    public function __construct(
        BusinessConfirmationForm $confirmationForm,
        WithdrawalForm $withdrawalForm,
        ClientRolloverForm $rolloverForm,
        ClientTopupForm $topupForm,
        PaymentForm $paymentForm,
        EditInvestmentForm $editInvestmentForm,
        InvestmentBankInstructionvalidator $investmentBankInstructionvalidator,
        CombinedRolloverForm $combinedRolloverForm,
        InterestPaymentScheduleForm $interestPaymentScheduleForm
    )
    {
        parent::__construct();

        $this->withdrawalForm = $withdrawalForm;

        $this->confirmationForm = $confirmationForm;

        $this->rolloverForm = $rolloverForm;

        $this->topupForm = $topupForm;

        $this->paymentForm = $paymentForm;

        $this->editInvestmentForm = $editInvestmentForm;

        $this->investmentBankInstructionForm = $investmentBankInstructionvalidator;

        $this->combinedRolloverForm = $combinedRolloverForm;

        $this->interestPaymentScheduleForm = $interestPaymentScheduleForm;
    }


    /**
     * Display the clients investments menu
     *
     * @return Response
     */
    public function index()
    {
        $gen = new DashboardGenerator();

        $manager = $this->fundManager();

        $invGenerator = new InvestmentsDashboardGenerator();

        return view(
            'investment.menu',
            [
                'title' => 'Client Investments Menu',
                'gen' => $gen,
                'invGenerator' => $invGenerator,
                'manager' => $manager
            ]
        );
    }

    /**
     * Display the clients applications menu
     *
     * @return mixed
     */
    public function applications()
    {
        return view('investment.applications.index', ['title' => 'Investment Applications']);
    }

    /**
     * Display the investments grid table
     *
     * @return mixed
     */
    public function investmentsGrid()
    {
        return view('investment.investmentsgrid', ['title' => 'Client Investments']);
    }

    public function unitFundInvestments()
    {
        return view('unitization.client_summary', [
            'title' => 'Unit Fund Investment Summary'
        ]);
    }

    /**
     * Display the deductions grid table
     *
     * @return mixed
     */
    public function deductionsGrid()
    {
        return view('investment.deductionsgrid', ['title' => 'Client Deductions']);
    }

    /**
     * Display the withdrawals grid table
     *
     * @return mixed
     */
    public function withdrawalsGrid()
    {
        $date = new Carbon(Request::get('date'));

        $withdrawals = ClientInvestment::where('withdrawn', 1)
            ->withdrawnBetween($date->startOfYear(), $date)
            ->get();

        $interest_payments = InterestPayment::where('date_paid', $date)->get();

        $collection = new Collection();

        // I used foreach because merge() was overriding some records
        foreach ($withdrawals as $withdrawal) {
            $collection->push($withdrawal);
        }

        foreach ($interest_payments as $payment) {
            $collection->push($payment);
        }


        $withdrawals = $collection->map(
            function ($investment) use ($date) {
                if ($investment->date_paid) {
                    $ip = $investment;
                    $ip_inv = $ip->investment;
                    $investment->withdrawal = $investment->amount;
                    $investment->amount = $ip_inv->amount;
                    $investment->invested_date = $ip_inv->invested_date;
                    $investment->maturity_date = $ip_inv->maturity_date;
                    $investment->interest_rate = $ip_inv->interest_rate;
                    $investment->withdrawn_on = $ip_inv->date_paid;
                    $investment->product = $ip_inv->product;
                    $investment->bank_account_id = $ip_inv->bank_account_id;
                    $investment->client = $ip_inv->client;
                    $investment->type = 'Interest Payment';
                } else {
                    $investment->type = 'Withdrawal';
                    $investment->withdrawal = $investment->repo->getWithdrawnAmount();
                }

                $investment->client_code = $investment->client->client_code;
                $investment->fullName = ClientPresenter::presentJointFullNames($investment->client->id);
                $investment->invested_date = $investment->invested_date->toDateString();
                $investment->maturity_date = $investment->maturity_date->toDateString();

                $investment->currency = $investment->product->currency->code;
                $investment->withdrawn_on = DatePresenter::formatDate($date);

                return $investment;
            }
        );

        return view('investment.withdrawalsgrid', ['title' => 'Client Withdrawals', 'withdrawals' => $withdrawals]);
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function maturity()
    {
        $date = Request::get('date');

        is_null($date) ? $date = Carbon::now()->addWeek() : $date = new Carbon($date);

        $investments = ClientInvestment::where('maturity_date', $date->toDateString())
            ->where('withdrawn', '!=', 1)->paginate(20);

        return view('investment.maturity', ['investments' => $investments,
            'title' => 'Client Investments Maturity', 'date' => $date->toDateString()]);
    }

    public function viewBulkRolloverMaturities()
    {
        return view('investment.maturities.bulk_rollover_maturity');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postBulkRolloverMaturities(\Illuminate\Http\Request $request)
    {
        $date = $request->get('date');

        $investments = ClientInvestment::where('maturity_date', $date)
            ->active()->whereHas('product', function ($product) {
                $product->where('active', 1);
            })->where(function ($q) {
                $q->doesntHave('schedule')->orWhereHas('schedule', function ($q) {
                    $q->where('action_date', '<', Carbon::now());
                });
            })
            ->doesntHave('rolloverInstruction')
            ->where(function ($q) {
                $q->doesntHave('instructions')->orWhereHas('instructions', function ($q) {
                    $q->whereHas('approval', function ($q) {
                        $q->where('approved', 1);
                    });
                });
            })
            ->where(function ($query) {
                $query->where('on_call', 0)->orWhereNull('on_call');
            })->pluck('id')->toArray();

        if (count($investments) == 0) {
            \Flash::error('There are no unprocessed maturing investments for the selected date');

            return Redirect::back();
        }

        ClientTransactionApproval::add([
            'client_id' => null,
            'transaction_type' => 'bulk_rollover_investment',
            'payload' => [
                'investments' => $investments,
                'date' => $date,
                'removed' => []
            ]]);

        \Flash::message('The client investments have been sent for bulk rollover approval');

        return Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function investmentMaturity($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        return view('investment.investment_maturity', ['investment' => $investment, 'title' => 'Investment Maturity']);
    }

    public function sendInvestmentMaturity($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $mailer = new ClientMaturityNotificationMailer();

        $mailer->notifyWeekBefore([$investment], Auth::user());

        \Flash::success('The maturity notification has been sent!');

        return Redirect::back();
    }

    /**
     * Send Maturity Notifications to clients
     *
     * @return mixed
     */
    public function sendMaturityNotifications()
    {
        $input = Request::all();

        unset($input['_token']);

        ClientTransactionApproval::add(
            ['client_id' => null, 'scheduled' => 0, 'payload' => $input, 'transaction_type' => 'maturity_notification']
        );

        \Flash::message('Maturity notifications have been sent for approval');

        return Redirect::back();
    }

    /**
     * @return mixed
     */
    public function maturityAnalysis()
    {
        is_null(Request::get('start')) ? $start = Carbon::now() : $start = new Carbon(Request::get('start'));

        is_null(Request::get('end'))
            ? $end = Carbon::now()->addMonths(2) : $end = new Carbon(Request::get('end'));

        $products = Product::all();

        return view(
            'investment.maturityanalysis',
            [
                'title' => 'Maturity Analysis', 'start' => $start->toDateString(), 'end' => $end->toDateString(),
                'products' => $products
            ]
        );
    }

    public function scheduledgrid()
    {
        $investments = ClientInvestment::has('schedule')->active()->paginate();
        //get the value of the net interest

        return view(
            'investment.scheduledgrid',
            ['investments' => $investments, 'title' => 'Client Scheduled Investment']
        );
    }

    /**
     * Show the investment details
     * Display details about a selected investment
     *
     * @param  $id
     * @return mixed
     */
    public function investmentDetails($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $schedule = $investment->approvals()->where('scheduled', true)
            ->whereNotNull('action_date')->whereNull('run_date')->latest()->first();

        return view(
            'investment.investmentdetails',
            ['title' => 'Client Investment', 'investment' => $investment, 'schedule' => $schedule]
        );
    }

    /**
     * Show the application details
     * Display details about an investment application
     *
     * @param  $id
     * @return mixed
     */
    public function applicationDetails($id)
    {
        $application = ClientInvestmentApplication::findOrFail($id);

        $client = $application->client;

        $filled_application = ClientFilledInvestmentApplication::find($application->form_id);

        $jointHolders = $filled_application->jointHolders;

//        $jointHolders = ClientJointDetail::where('application_id', $application->id)->get();

        $invested = ClientInvestment::where('application_id', $application->id)->first();

        $title = Title::find($application->contact_person_title);

        $clientTitles = Title::all()->lists('name', 'id');

        $genders = Gender::all()->lists('abbr', 'id');

        $countries = Country::all()->lists('name', 'id');

        $documents = $application->form->filledDocuments;

        if ($application->type_id == 2) {
            $corporateinvestortype = $application->client
                ? CorporateInvestorType::findOrFail($application->client->corporate_investor_type) : null;

            return view('investment.applications.details', [
                'invested' => $invested,
                'jointHolders' => $jointHolders,
                'title' => 'Application Details',
                'application' => $application,
                'client' => $client,
                'corporateinvestortype' => $corporateinvestortype,
                'client_title' => $title,
                'documents' => $documents
            ]);
        }

        $clientEmails = ($application->client && $application->client->emails) ? $application->client->emails : [];

        return view('investment.applications.details', [
            'invested' => $invested,
            'jointHolders' => $jointHolders,
            'title' => 'Application Details',
            'application' => $application,
            'client' => $client,
            'clientTitles' => $clientTitles,
            'gender' => $genders,
            'countries' => $countries,
            'clientEmails' => $clientEmails,
            'documents' => $documents
        ]);
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function interestPayments()
    {
        $tenors = [
            null => 'All',
            1 => 'Monthly',
            3 => 'Quarterly',
            6 => 'Bi-Annually',
            12 => 'Annually'
        ];

        return view('investment.interestpayment', ['title' => 'Interest Payments', 'tenors' => $tenors]);
    }

    /**
     * @return mixed
     */
    public function sendBulkForApproval()
    {
        $date = Request::get('date');

        $interest_payment_schedules = InterestPaymentSchedule::where('date', $date)
            ->whereHas('investment', function ($investment) {
                $investment->active();
            })
            ->get()
            ->lists('id');

        if (count($interest_payment_schedules) == 0) {
            \Flash::error('No interest payment schedules found for the selected dat');

            return Redirect::back();
        }

        ClientTransactionApproval::add(['client_id' => null, 'transaction_type' => 'interest_payment_bulk_approval',
            'payload' => ['interest_payment_schedules' => $interest_payment_schedules, 'removed' => [], 'date' => $date]]);

        \Flash::message('The interest payments have been sent for bulk approval');

        return Redirect::back();
    }

    /**
     * Show the form for payment of interest to a client
     *
     * @param  $id
     * @return mixed
     */
    public function pay($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $remaining = $investment->repo
                ->getTotalAvailableInterestAtNextDay(Carbon::today()) - $investment->repo->getTotalPayments();

        $payment = $investment->repo->getWithdrawnAmount();

        $schedules = $investment->interestSchedules->sortBy('date');

        $interestactions = InterestAction::all()->mapWithKeys(
            function ($interestaction) {
                return [$interestaction['id'] => $interestaction['name']];
            }
        );

        return view(
            'investment.payment',
            [
                'title' => 'Interest payment for Investment', 'investment' => $investment, 'schedules' => $schedules,
                'remaining' => $remaining, 'payment_total' => $payment, 'interestaction' => $interestactions
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function reversePayment($id)
    {
        $payment = InterestPayment::findOrFail($id);

        $investment = $payment->investment;

        ClientTransactionApproval::add(['client_id' => $investment->client_id,
            'transaction_type' => 'reverse_interest_payment', 'scheduled' => false, 'payload' => ['payment_id' => $id]]);

        \Flash::message('The interest payment reversal has been saved for approval');

        return redirect('/dashboard/investments');
    }

    /**
     * Save the payment
     *
     * @param  $id
     * @return mixed
     * @throws Error
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postPay($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        if ($investment->withdrawn) {
            throw new Error('The investment is already withdrawn');
        }

        $remaining = $investment->repo->getTotalAvailableInterestAtNextDay(\Request::get('date_paid'));

        $date_paid = new Carbon(Request::get('date_paid'));

        //TODO make validator
        if (Request::get('amount') > $remaining + 1) {//allow rounding off
            throw new Error('The interest withdrawn cannot be more than ' .
                AmountPresenter::currency($remaining) . ' currently available');
        } elseif ($date_paid->copy()->lt(new Carbon($investment->invested_date)) ||
            $date_paid->copy()->gt((new Carbon($investment->maturity_date))->addDay())) {
            throw new Error('The payment date should be between value date and withdrawal date');
        }

        if ((new Carbon($date_paid))->gt(Carbon::today()->addMonthNoOverflow()->startOfMonth())) {
            throw new Error('The payment date should not be after the end of this month');
        }

        $input = Request::all();
        unset($input['_token']);
        $input = array_add($input, 'investment_id', $investment->id);

        $validation_data = array_add($input, 'maturity_date', (new Carbon($investment->maturity_date))->addDay());
        $validation_data = array_add($validation_data, 'invested_date', $investment->invested_date);

        $this->paymentForm->validate($validation_data);

        ClientTransactionApproval::add(['client_id' => $investment->client_id,
            'transaction_type' => 'interest_payment', 'scheduled' => false, 'payload' => $input]);

        \Flash::message('Payment has been saved for approval');

        return redirect('/dashboard/investments');
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postAddSchedule($id)
    {
        $input = Request::except('_token');

        if (!array_key_exists('fixed', $input)) {
            $input['fixed'] = 0;
        }

        $investment = ClientInvestment::findOrFail($id);

        $input['investment_id'] = $investment->id;

        $this->interestPaymentScheduleForm->validate($input);

        ClientTransactionApproval::add(['client_id' => $investment->client_id,
            'transaction_type' => 'add_interest_payment_schedule', 'scheduled' => false, 'payload' => $input]);

        \Flash::message('Interest payment schedule has been saved for approval');

        return redirect('/dashboard/investments');
    }

    public function putUpdateSchedule($id)
    {
        $input = Request::except('_token');

        $schedule = InterestPaymentSchedule::findOrFail($id);
        $input['schedule_id'] = $schedule->id;

        if (!array_key_exists('fixed', $input)) {
            $input['fixed'] = 0;
        }

        $investment = ClientInvestment::findOrFail($input['investment_id']);

        ClientTransactionApproval::add(
            [
                'client_id' => $investment->client_id,
                'transaction_type' => 'edit_interest_payment_schedule',
                'scheduled' => null,
                'payload' => $input
            ]
        );

        \Flash::message('Interest payment schedule edit has been saved for approval');

        return redirect('/dashboard/investments/pay/' . $investment->id);
    }

    /**
     * Show the business confirmations grid
     *
     * @return mixed
     */
    public function confirmations()
    {
        return view('investment.confirmation.confirmations', [
            'title' => 'Business Confirmations'
        ]);
    }

    /**
     * Show the details if the business confirmations
     *
     * @param  $id
     * @return mixed
     */
    public function confirmationDetails($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $previousConfirmations = ClientBusinessConfirmation::where('investment_id', $id)->latest()->get();

        $is_bc_sent = (bool)ClientBusinessConfirmation::where('investment_id', $id)->count() > 0;

        $pendingKycs = $investment->client->repo->requiredPendingKYCDocuments()->map->name;

        return view('investment.confirmation.confirm', [
            'title' => 'Business Confirmation',
            'investment' => $investment,
            'previousConfirmations' => $previousConfirmations,
            'is_bc_sent' => $is_bc_sent,
            'pendingKycs' => $pendingKycs
        ]);
    }

    public function confirmationAcknowledgementNotification($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $pendingKycs = $investment->client->repo->requiredPendingKYCDocuments()->map->name;

        (new AcknowledgementNotificationMailer())->sendEmail($investment, $pendingKycs);

        \Flash::success('Acknowledgement Notification has been sent successfully to the client');

        return Redirect::back();
    }

    /**
     * Save a business confirmation for approval
     *
     * @param  $id
     * @return mixed
     */
    public function saveConfirmationForApproval($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $investment->repo->sendBusinessConfirmation();

        \Flash::success('The Business Confirmation has been sent');

        return Redirect::back();
    }

    /**
     * Invest the client application, save for approval
     *
     * @param  $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveBusinessConfirmation($id)
    {
        $app_id = Crypt::decrypt(Request::get('app_id'));

        $application = ClientInvestmentApplication::findOrFail($app_id);

        $investment = ClientInvestment::where('application_id', $app_id)->first();

        if (!is_null($investment)) {
            \Flash::warning('This application has already been confirmed, you cannot confirm it again');

            return Redirect::back();
        }

        $this->confirmationForm->validate(Request::all());

        $input = Request::all();
        unset($input['app_id']);
        unset($input['_token']);

        $approval_data = array_add($input, 'application_id', $app_id);

        ClientTransactionApproval::add(['client_id' => $application->client_id,
            'transaction_type' => 'investment', 'payload' => $approval_data, 'scheduled' => 0]);

        \Flash::message('The business confirmation is pending for approval');

        return Redirect::back();
    }

    /**
     * Show the taxation grid tables
     *
     * @return mixed
     */
    public function taxation()
    {
        return view('investment.taxation', ['title' => 'Client Taxation']);
    }

    /**
     * Show the full interest payment schedule
     *
     * @return mixed
     */
    public function fullInterestSchedule()
    {
        return view('investment.interest.fullInterestSchedule', ['title' => 'Full Interest payment schedule']);
    }

    /**
     * Show the interests to be paid this month
     *
     * @return mixed
     */
    public function monthlySchedule()
    {
        $repo = new InterestRepository();
        $clients = $repo->getClientsWithSchedulesThisMonth();

        return view(
            'investment.interest.monthlyInterestSchedule',
            ['title' => 'Interest payment schedule', 'clients' => $clients, 'repo' => $repo]
        );
    }

    /**
     * Get the recipient schedule for a client
     *
     * @param  $id
     * @return mixed
     */
    public function recipientSchedule($id)
    {
        return view(
            'investment.interest.recipientSchedule',
            ['title' => 'Client Interest Schedule', 'client' => $id]
        );
    }

    /**
     * Show the tax due for all invested/rolled over client investments
     *
     * @return mixed
     */
    public function clientsTaxationRecords()
    {
        return view('investment.clienttax', ['title' => 'Clients Tax Due']);
    }

    /**
     * Show the Tax details for a client
     *
     * @param  $id
     * @return mixed
     */
    public function taxationInvestmentsDetails($id)
    {
        return view(
            'investment.taxationinvestmentsdetails',
            ['title' => 'Client Taxation Investments Details', 'client_id' => $id]
        );
    }

    /**
     * Schedule a transaction to be performed in future, show form to select transaction type and enter details
     *
     * @param  $id
     * @return mixed
     */
    public function scheduleTransaction($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $interest_payment_intervals = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months',
            5 => 'Every 5 months', 6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months',
            8 => 'Every 8 months', 9 => 'Every 9 months', 10 => 'Every 10 months',
            11 => 'Every 11 months', 12 => 'Every 12 months (Annually)'];

        $commissionrecepients = CommissionRecepient::all()->mapWithKeys(
            function ($commissionrecepient) {
                return [$commissionrecepient['id'] => $commissionrecepient['name']];
            }
        );

        $commissiorates = CommissionRate::forForm($investment);

        $interestactions = InterestAction::all()->mapWithKeys(
            function ($interestaction) {
                return [$interestaction['id'] => $interestaction['name']];
            }
        );

        return view(
            'investment.actions.schedule.index',
            [
                'title' => 'Schedule Investment Transaction', 'investment' => $investment, 'schedule_transaction' => true,
                'premature' => \Session::pull('premature'), 'partial_withdraw' => \Session::pull('partial_withdraw'),
                'end_date' => \Session::pull('end_date'), 'interest_payment_intervals' => $interest_payment_intervals,
                'commissionrecepients' => $commissionrecepients, 'commissionrates' => $commissiorates,
                'interestactions' => $interestactions
            ]
        );
    }

    /**
     * Schedule a rollover
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function scheduleInvestmentRollover()
    {
        $input = Request::all();

        $input['investment'] = Crypt::decrypt($input['investment_id']);
        unset($input['investment_id']);

        $oldInvestment = ClientInvestment::findOrFail($input['investment']);

        $validation_data = $input;
        $validation_data =
            array_add($validation_data, 'old_investment_maturity_date', $oldInvestment->maturity_date);
        $validation_data = array_add(
            $validation_data,
            'old_investment_amount',
            (float)$oldInvestment->repo->getTotalValueOfAnInvestment()
        );
        $validation_data['amount'] = (float)$validation_data['old_investment_amount'];


        if ($input['amount'] > $validation_data['old_investment_amount']) {
            \Flash::error('The amount rolled over should be less than total value');

            return Redirect::back()->withInput();
        }

        $this->rolloverForm->validate($validation_data);

        unset($input['_token']);
        $input['scheduled_by'] = Auth::user()->id;

        ClientTransactionApproval::add(
            ['client_id' => $oldInvestment->client_id,
                'transaction_type' => 'rollover',
                'payload' => $input,
                'scheduled' => 1]
        );

        \Flash::message('The investment rollover has been scheduled');

        return redirect('/dashboard/investments');
    }

    /**
     * Schedule a withdrawal
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function scheduleInvestmentWithdrawal()
    {
        $investment = ClientInvestment::findOrFail(Crypt::decrypt(Request::get('investment_id')));

        if ($investment->withdrawn == 1) {
            \Flash::error('This investment has already been withdrawn, you cannot withdraw it again');

            return Redirect::back();
        }

        $input = Request::all();
        $input['investment_id'] = Crypt::decrypt($input['investment_id']);
        $input['scheduled_by'] = Auth::user()->id;

        if (Request::get('premature') == 'true') {
            $this->withdrawalForm->premature();

            $validation_data = $input;
            $validation_data = array_add($validation_data, 'invested_date', $investment->invested_date);
            $validation_data = array_add($validation_data, 'maturity_date', $investment->maturity_date);

            $this->withdrawalForm->validate($validation_data);
        } else {
            unset($input['withdraw_date']);

            $this->withdrawalForm->validate($input);
        }

        unset($input['_token']);

        ClientTransactionApproval::add(
            ['client_id' => $investment->client_id,
                'transaction_type' => 'withdraw',
                'payload' => $input, 'scheduled' => 1]
        );

        \Flash::message('The Schedule for the Investment Withdrawal has been saved for approval');

        return redirect('/dashboard/investments');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function investmentScheduleDetails($id)
    {
        $users = User::all()->lists('full_name', 'id');

        $investment = ClientInvestment::findOrFail($id);

        $schedule = $investment->approvals()->where('scheduled', true)->latest()->first();

        $recipient = function ($rec) {
            return CommissionRecepient::find($rec);
        };

        $sigs = User::all()->lists('full_name', 'id');

        $penalty = $schedule->handler()->calculatePenalty($schedule);
        $total = $schedule->handler()->calculateAmountWithdrawn($schedule);

        $penalty = $penalty ? (int)$penalty->amount : 0;
        $amount = $total - $penalty;

        $accounts = $investment->client->bankAccounts->lists('account_name_and_number', 'id');

        return view(
            'investment.investmentscheduledetails',
            [
                'title' => 'Transaction Schedule Details', 'clientinvestment' => $investment, 'schedule' => $schedule,
                'users' => $users, 'recipient' => $recipient, 'sigs' => $sigs, 'accounts' => $accounts,
                'total' => $total, 'penalty' => $penalty, 'amount' => $amount
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function cancelInvestmentSchedule($id)
    {
        //        $schedule = ClientScheduledTransaction::where('investment_id', $id)->first();
        //        $input['schedule_id'] = $schedule->id;
        //
        //        ClientTransactionApproval::add([
        //            'client_id'         =>  $schedule->investment->client_id,
        //            'transaction_type'  =>  'cancel_investment_schedule',
        //            'payload'           =>  $input
        //        ]);
        //
        //        \Flash::message('The request to cancel the investment schedule has been sent for approval');
        //
        //        return redirect('/dashboard/investments');

        $investment = ClientInvestment::findOrFail($id);
        $schedule = $investment->approvals()->where('scheduled', true)->latest()->first();

        $input['schedule_id'] = $schedule->id;

        ClientTransactionApproval::add(
            [
                'client_id' => $schedule->client_id,
                'transaction_type' => 'cancel_investment_schedule',
                'payload' => $input
            ]
        );

        \Flash::message('The request to cancel the investment schedule has been sent for approval');

        return redirect('/dashboard/investments');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function instruction($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $signatories = User::with('contact')->has('staff')->get();

        $signatory_coll = new \Illuminate\Support\Collection();

        foreach ($signatories as $signatory) {
            $name = UserPresenter::presentFullNamesNoTitle($signatory->id);
            $id = $signatory->id;
            $signatory_coll->put($id, $name);
        }

        $instructions = BankInstruction::where('investment_id', $investment->id)->get();

        return view(
            'investment.prepareinstruction',
            ['title' => 'Generate Bank Instructions',
                'investment' => $investment,
                'signatories' => $signatory_coll,
                'instructions' => $instructions]
        );
    }

    /**
     * @param $investment_id
     * @param $instruction_id
     * @return mixed
     */
    public function instructionSignatories($investment_id, $instruction_id)
    {
        $investment = ClientInvestment::findOrFail($investment_id);

        $signatories = User::with('contact')->has('staff')->get();

        $signatory_coll = new Collection();

        foreach ($signatories as $signatory) {
            $name = UserPresenter::presentFullNamesNoTitle($signatory->id);
            $id = $signatory->id;
            $signatory_coll->put($id, $name);
        }

        $instruction = BankInstruction::findOrFail($instruction_id);

        return view(
            'investment.selectsignatory',
            [
                'title' => 'Generate Bank Instructions',
                'investment' => $investment, 'signatories' => $signatory_coll,
                'instruction' => $instruction
            ]
        );
    }

    /**
     * @param $investment_id
     * @param $instruction_id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function generateInstruction($investment_id, $instruction_id)
    {
        $input = Request::only(['amount', 'first_signatory', 'second_signatory']);

        $investment = ClientInvestment::findOrFail($investment_id);

        $instruction_data = BankInstruction::findOrFail($instruction_id);

        $this->investmentBankInstructionForm->validate($input);

        if ($instruction_data->viewed) {
            $mailer = new GeneralMailer();

            $mailer->to('operations@cytonn.com');

            $mailer->subject('Instruction Viewed for the Second Time');
            $content = UserPresenter::presentFullNamesNoTitle(Auth::user()->id) . '
             printed a bank instruction of amount ' . $instruction_data->amount . ' for the second time.<br/>
                            The instruction belongs to ' .
                ClientPresenter::presentJointFirstNameLastName($investment->client_id);

            $mailer->sendGeneralEmail($content);
        }

        $input['amount'] = round($input['amount']);

        $instruction = (new BankInstructionGenerator())
            ->generateInstructionForClientInvestment($input, $investment, $instruction_data);

        DB::beginTransaction();

        try {
            $instruction_data->viewed = true;
            $instruction_data->save();

            $instr = $instruction->stream();
        } catch (\Exception $e) {
            DB::rollback();
        }

        DB::commit();

        return $instr;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function associateAccount($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        Request::get('bank_acc') == 0 ? $acc = null : $acc = Request::get('bank_acc');

        $investment->bank_account_id = $acc;
        $investment->save();

        \Flash::success('Default Bank Account for Investment Saved');

        return Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function commission($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $commission = $investment->commission;

        $schedules = $commission->schedules()->whereNull('claw_back')->oldest('date')->get();

        $latestBulk = BulkCommissionPayment::orderBy('end', 'DESC')->first();

        $unpaidClawbacks = $commission->clawBacks()
            ->where('date', '>=', $latestBulk->end)->get();

        return view(
            'investment.commission',
            ['title' => 'Commission for Investment', 'commission' => $commission,
                'schedules' => $schedules, 'investment' => $investment, 'unpaidClawbacks' => $unpaidClawbacks]
        );
    }
}
