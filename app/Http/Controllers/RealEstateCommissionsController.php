<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Crims\Realestate\Commission\RecipientCalculator;
use Cytonn\Models\Investment\AdvanceCommissionType;
use Cytonn\Models\RealEstate\AdvanceRealEstateCommission;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Forms\UpdateCommissionPaymentScheduleForm;
use Illuminate\Http\Request;

/**
 * Class RealEstateCommissionsController
 */
class RealEstateCommissionsController extends Controller
{
    protected $realEstateCommissionRepository;
    /**
     * @var UpdateCommissionPaymentScheduleForm
     */
    private $commissionPaymentScheduleForm;

    public function __construct(UpdateCommissionPaymentScheduleForm $commissionPaymentScheduleForm)
    {
        $this->realEstateCommissionRepository = new \Cytonn\Realestate\Commissions\RealEstateCommissionRepository();

        parent::__construct();

        $this->commissionPaymentScheduleForm = $commissionPaymentScheduleForm;
    }

    public function getIndex()
    {
        return \view('realestate.commissions.index', ['title' => 'Real Estate Commissions']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postBulkPayments($id)
    {
        $bcp = BulkCommissionPayment::findOrFail($id);

        $input['bcp_id'] = $bcp->id;

        $approval = ClientTransactionApproval::add(
            [
                'client_id' => null,
                'transaction_type' => 'bulk_realestate_commission_payments',
                'payload' => $input,
                'scheduled' => false
            ]
        );

        $bcp->realEstateApproval()->associate($approval);
        $bcp->save();

        \Flash::success('Real Estate commission payments have been saved for approval');

        return \Redirect::back();
    }

    public function getForUnit($unit_holding_id)
    {
        $unitHolding = UnitHolding::findOrFail($unit_holding_id);

        $unit = $unitHolding->unit;

        $commission = $unitHolding->commission;

        $schedules = $commission->schedules()->oldest('date')->get();

        $advanceCommissionTypes = AdvanceCommissionType::where('id', '!=', 3)->pluck('name', 'id')->toArray();

        return view('realestate.commissions.unit', [
            'holding' => $unitHolding,
            'commission' => $commission,
            'schedules' => $schedules,
            'unit' => $unit,
            'title' => 'Commission for ' . $unit->number,
            'advanceCommissionTypes' => $advanceCommissionTypes
        ]);
    }

    public function postSchedule($id)
    {
        $this->commissionPaymentScheduleForm->validate(\Request::all());

        $schedule = RealEstateCommissionPaymentSchedule::findOrFail($id);

        $input = \Request::except('_token');
        $input['schedule_id'] = $id;

        ClientTransactionApproval::add(
            [
                'client_id' => $schedule->commission->holding->client_id,
                'transaction_type' => 'update_realestate_commission_payment_schedule',
                'payload' => $input
            ]
        );

        \Flash::success('The change has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @param $recipientId
     * @return mixed
     */
    public function getPayment($recipientId)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $date = (new Carbon(\Request::get('date')))->endOfMonth();

        $bulk = BulkCommissionPayment::where('end', '>', $date->copy()->startOfMonth())
            ->where('end', '<=', $date)->latest()->first();

        if (!$bulk) {
            $start = BulkCommissionPayment::latest()->first()->end->addDay();
            $end = Carbon::today();
        } else {
            $start = $bulk->start;
            $end = $bulk->end;
        }

        $calculator = new RecipientCalculator($recipient, $start, $end);

        $schedules = $calculator->schedules();

        $schedules->each(
            function (RealEstateCommissionPaymentSchedule $schedule) {
                $holding = $schedule->commission->holding;

                $schedule->client_code = $holding->client->client_code;
                $schedule->client_name = ClientPresenter::presentJointFullNames($holding->client_id);
                $schedule->project_name = $holding->project->name;
                $schedule->unit_number = $holding->unit->number;
                $schedule->holding = $holding;
                $schedule->unit_id = $holding->unit->id;
                $schedule->project_id = $holding->project->id;
            }
        );

        $total = $calculator->summary()->total();

        return view(
            'realestate.commissions.payment',
            [
                'title' => 'Real Estate Commission payments', 'recipient' => $recipient, 'schedules' => $schedules,
                'commissions' => $recipient->realEstateCommissions, 'date' => $date,
                'start' => $start, 'end' => $end, 'total' => $total
            ]
        );
    }

    /**
     * @param Request $request
     * @param $holdingId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function awardCommission(Request $request, $holdingId)
    {
        $holding = UnitHolding::findOrFail($request->get('holding_id'));

        $this->queue(
            function () use ($holding) {
                \Artisan::call(
                    're:update-commissions',
                    ['holding_id' => $holding->id]
                );
            }
        );

        \Flash::success('The commission award has been executed successfully');

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function awardAdditionCommission(Request $request, $id = null)
    {
        $input = $request->except(['_token']);

        $holding = UnitHolding::findOrFail($input['holding_id']);

        $input['id'] = $id;

        if ($id) {
            $advanceCommission = AdvanceRealEstateCommission::findOrFail($id);

            if (!$advanceCommission->present()->canEdit) {
                \Flash::error('The advance commission cannot be edited as the bulk commission period is closed');

                return \Redirect::back();
            }
        }

        $input['recipient_id'] = $holding->commission->recipient_id;

        ClientTransactionApproval::add([
            'client_id' => $holding->client_id,
            'transaction_type' => 'store_advance_real_estate_commission',
            'payload' => $input
        ]);

        \Flash::success('The record has been saved for approval');

        return \Redirect::back();
    }
}
