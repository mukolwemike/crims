<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Notifier\FlashNotifier;
use Illuminate\Support\Facades\Redirect;
use \Flash;

/**
 * Class CoopProductPlansController
 */
class CoopProductPlansController extends Controller
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function postAddProductPlan()
    {
        $input = \Request::except('_token');

        ClientTransactionApproval::add(
            [ 'transaction_type'=>'add_coop_product_plan', 'client_id'=>$input['client_id'], 'payload'=>$input ]
        );

        Flash::message('Coop product plan has been saved for approval');

        return Redirect::back();
    }

    public function putEditProductPlan($id)
    {
        $input = \Request::except('_token');
        $input['id'] = $id;

        ClientTransactionApproval::add(
            [ 'transaction_type'=>'edit_coop_product_plan', 'client_id'=>$input['client_id'], 'payload'=>$input ]
        );

        Flash::message('Coop product plan update has been saved for approval');

        return Redirect::back();
    }

    public function deleteRemoveProductPlan($id)
    {
        $input = \Request::except('_token');
        $input['id'] = $id;

        ClientTransactionApproval::add(
            [ 'transaction_type'=>'delete_coop_product_plan', 'client_id'=>$input['client_id'], 'payload'=>$input ]
        );

        Flash::message('Coop product plan delete request has been saved for approval');

        return Redirect::back();
    }

    public function putTerminateProductPlan($id)
    {
        $input = \Request::except('_token');
        $input['id'] = $id;

        ClientTransactionApproval::add(
            [ 'transaction_type'=>'terminate_coop_product_plan', 'client_id'=>$input['client_id'], 'payload'=>$input ]
        );

        Flash::message('Coop product plan terminate request has been saved for approval');

        return Redirect::back();
    }
}
