<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Project;
use Cytonn\Realestate\Forms\AdvocatesForm;

/**
 * Class AdvocatesController
 */
class AdvocatesController extends Controller
{

    /**
     * @var AdvocatesForm $advocatesForm
     */
    public $advocatesForm;

    /**
     * AdvocatesController constructor.
     *
     * @param AdvocatesForm $advocatesForm
     */
    public function __construct(AdvocatesForm $advocatesForm)
    {
        $this->advocatesForm = $advocatesForm;

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $projects = Project::all()->lists('name', 'id');

        return view('realestate.advocates.index', ['title'=>'Advocates', 'projects'=>$projects]);
    }

    /**
     * @param Advocate $advocate
     * @return mixed
     * @internal param $id
     */
    public function show(Advocate $advocate)
    {
        $projects = $advocate->projectAssignments->map(
            function ($assignment) {
                return $assignment->project->name;
            }
        )->all();

        $projects = implode(', ', $projects);
        return view('realestate.advocates.show', compact('advocate', 'projects'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $projects = Project::all()->lists('name', 'id');
        $countries = Country::all()->lists('name', 'id');

        return view('realestate.advocates.create', compact('advocate', 'projects', 'countries'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $input = request()->all();

        $this->advocatesForm->validate($input);

        ClientTransactionApproval::make(null, 'create_advocate', $input);

        \Flash::message('The advocate has been saved for approval');

        return redirect('/dashboard/realestate/advocates');
    }

    /**
     * @param Advocate $advocate
     * @return mixed
     * @internal param $id
     */
    public function edit(Advocate $advocate)
    {
        $projects = Project::all()->lists('name', 'id');
        $countries = Country::all()->lists('name', 'id');

        return view('realestate.advocates.edit', compact('advocate', 'projects', 'countries'));
    }

    /**
     * @param Advocate $advocate
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Advocate $advocate)
    {
        $input = request()->all();

        $this->advocatesForm->validate($input);

        $input['advocate_id'] = $advocate->id;

        ClientTransactionApproval::make(null, 'update_advocate', $input);

        \Flash::message('The request to update the advocate has been saved for approval');

        return redirect("/dashboard/realestate/advocates/{$advocate->id}");
    }

    /**
     * @param Advocate $advocate
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function toggleActive(Advocate $advocate)
    {
        $input['advocate_id'] = $advocate->id;
        $input['status'] = ! (bool) $advocate->active;

        $str = $input['status'] ? 'activate' : 'deactivate';

        ClientTransactionApproval::make(null, 'change_advocate_active_status', $input);

        \Flash::message("The request to {$str} the advocate has been saved for approval");

        return redirect("/dashboard/realestate/advocates/{$advocate->id}");
    }
}
