<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers;

use App\Cytonn\Mailers\Client\LoyaltyPointsRedeemMailer;
use App\Cytonn\Models\ActiveStrategyPriceTrail;
use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\RiskyClient;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Exemption;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\User;
use App\ServiceBus\Queries\RealEstate\GetProjectsSummary;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\System\Exemptions\ExemptionRepository;
use Cytonn\System\Exemptions\ExemptionRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ExemptionController extends Controller
{
    /*
     *  Get the exemption rules
     */
    use ExemptionRules, LocksTransactions;

    /*
     * Get the repositories
     */
    protected $exemptionRepository;

    /*
     * Setup the constructor for the controller
     */
    public function __construct(ExemptionRepository $exemptionRepository)
    {
        parent::__construct();

        $this->exemptionRepository = $exemptionRepository;
    }

    /*
     * Get a listing of all the exemptions
     */
    public function index()
    {
        return view('system.exemptions.index', [
            'title' => 'Exemptions',
        ]);
    }

    private function upload()
    {
        $trails = Excel::load(storage_path('/') . '/StockPrices.xlsx', function ($reader) {
            $results = collect($reader->all())->toArray();

            [279, 282, 285, 288, 291, 294, 297, 303, 306];
            //TODO:: remaining 0

            foreach ($results[1] as $data) {
                $trail = new ActiveStrategyPriceTrail();

                $trail->security_id = 282;
                $trail->date = Carbon::parse($data['date'])->toDateString();
                $trail->price = $data['rate'];

                $trail->save();
            }
        });

        dd($trails);
        $clientArray = array();

        dd('done');
    }

    public function randomCommand($id = null)
    {
//        $now = Carbon::now()->toDateTimeString();
//
//        $lock = (new RedisLock())->test($id);
//
//        return response()->json([
//            'acquired' => $lock !== false,
//            'at' => $now,
//            'id' => $id
//        ]);

        $this->queue(function () {
            \Artisan::call('crims:automatic_statements_campaigns');
        });
    }

    public function cleanUp()
    {
        $approval = ClientTransactionApproval::findOrFail(253120);

        ClientPayment::whereHas('investedInInvestment', function ($q) use ($approval) {
            $q->where('approval_id', $approval->id);
        })->delete();

        ClientInvestment::where('approval_id', $approval->id)
            ->delete();

        $approval->steps()->where('user_id', 61)->delete();

        $approval->update([
            'approved' => null,
            'approved_on' => null,
            'approved_by' => null
        ]);

        dd("Done Cleanup");
    }

    public function testQueue($job, $data)
    {
//        $sleep = $data['item_id'] * 2;
//
//        sleep($sleep);

        Auth::login(User::find(61));

        $approval = ClientTransactionApproval::findOrFail(253120);

        $handler = new Approval($approval);

        //run the action
        \DB::transaction(
            function () use ($handler) {
                $handler->approve();
            }
        );

        $job->delete();
    }

    /*
     * Import risky db clients
     */
    public function importRisky()
    {
        \DB::transaction(function () {
            $clients = Excel::load(storage_path('/') . '/audit_clients_re.xlsx', function ($reader) {
            })->get()->toArray();

            $clientArray = array();

            foreach ($clients as $client) {
                $project = Project::where('name', $client['project'])->first();

                $clientCode = $clientName = $unitNumber = $activeUnit = '';

                if (isNotEmptyOrNull($client['matched_client_code'])) {
                    $clientObject = Client::where('client_code', $client['matched_client_code'])->first();

                    $clientCode = $client['matched_client_code'];

                    if ($clientObject) {
                        $clientName = ClientPresenter::presentFullNames($clientObject->id);

                        $unit = RealestateUnit::where('project_id', $project->id)
                            ->where('number', $client['matched_unit_number'])
                            ->whereHas('holdings', function ($q) use ($clientObject) {
                                $q->where('client_id', $clientObject->id);
                            })->first();

                        if ($unit) {
                            $unitNumber = $unit->number;

                            $holding = $unit->holdings()->latest()->first();

                            $activeUnit = 'Active';

                            if ($holding->active != 1) {
                                if (isNotEmptyOrNull($holding->forfeit_date) &&
                                    Carbon::parse($holding->forfeit_date) < Carbon::parse('2018-12-31')) {
                                    $activeUnit = 'Inactive';
                                }
                            }
                        }
                    }
                }

                $number = substr($client['unit_number'], 0, 1) . '-' .
                    substr($client['unit_number'], 1);

                $unit = RealestateUnit::where('project_id', $project->id)
                    ->where('number', $number)->first();

                if ($unit) {
                    $holding = $unit->holdings()->latest()->first();

                    $unitNumber = $unit->number;

                    if ($holding) {
                        $clientCode = $holding->client->client_code;

                        $clientName = ClientPresenter::presentFullNames($holding->client_id);
                    }
                }

                $clientArray[] = [
                    'Account Number' => $client['account_number'],
                    'Matched Client Code' => $clientCode,
                    'Project' => $project->name,
                    'Name' => $client['name'],
                    'Matched Client Name' => $clientName,
                    'Unit Number' => $client['unit_number'],
                    'Matched Unit Number' => $unitNumber,
                    'Active Unit' => $activeUnit
                ];
            }


            ExcelWork::generateAndExportSingleSheet($clientArray, 'RE_Audit_Clients');

            dd("Done");

            $foundArray = $missingArray = array();

            foreach ($clients as $row) {
                if ($row['active_unit'] == 'Active') {
                    $r = \App\Cytonn\Models\RealEstateAuditConfirmation::create([
                        'batch' => 5,
                        'client_code' => $row['matched_client_code'],
                        'name' => $row['matched_client_name'],
                        'house_number' => $row['matched_unit_number'],
                        'product' => $row['project']
                    ]);

                    $foundArray[] = $row;
                } else {
                    $missingArray[] = $row;
                }
            }

//            ExcelWork::generateAndExportMultiSheet([
//                'Sending' => $foundArray,
//                'Not Sending' => $missingArray
//            ], 'RE_Audit_Clients');
        });

        dd("Done");


        \DB::transaction(function () {
            $riskyClients = Excel::load(storage_path('/exports') . '/risky_clients_database.xlsx', function ($reader) {
            })->get()->toArray();

            foreach ($riskyClients as $row) {
                $r = RiskyClient::create([
                    'type' => (is_null($row['first_name'])) ? 2 : 1,
                    'firstname' => $row['first_name'],
                    'lastname' => $row['last_name'],
                    'organization' => $row['organization'],
                    'email' => $row['email'],
                    'affiliations' => $row['affiliationsrelationships'],
                    'risk_source' => $row['risk_source_from_source_page_link'],
                    'reason' => $row['reason_flagged'],
                    'country_id' => (is_null($row['country_id'])) ? 114 : $row['country_id'],
                    'date_flagged' => Carbon::parse($row['date_flagged'])->toDateString(),
                    'flagged_by' => 141
                ]);
            }
        });


        dd("Done");

        return redirect('/')->with('success', 'All good!');
    }

//    public function importExcel()
//    {
//        $prices = Excel::load(storage_path('/exports') . '/test.xlsx', function ($reader) {
//        })->get()->toArray();
//
//        $project = Project::find(9);
//
//        $tranche = RealEstateUnitTranche::findOrFail(39);
//
//        foreach ($prices as $price) {
//            $floor = Floor::where('floor', $price['floor'])->first();
//
//            if (is_null($floor)) {
//                dd("Floor", $price);
//            }
//
//            $projectFloor = $project->projectFloors()->where('floor_id', $floor->id)->first();
//
//            if (is_null($projectFloor)) {
//                dd("Project Floor", $price);
//            }
//
//            $type = Type::where('name', $price['type'])->first();
//
//            if (is_null($type)) {
//                dd("Type", $price);
//            }
//
//            $reType = $project->projectTypes()->where('type_id', $type->id)->first();
//
//            if (is_null($reType)) {
//                dd("RE Type", $price);
//            }
//
//            $size = RealestateUnitSize::findOrFail($price['size']);
//
//            $sizing = RealEstateUnitTrancheSizing::where('size_id', $size->id)
//                ->where('tranche_id', $tranche->id)
//                ->where('project_floor_id', $projectFloor->id)
//                ->where('real_estate_type_id', $reType->id)
//                ->first();
//
//            if (is_null($sizing)) {
//                $sizing = RealEstateUnitTrancheSizing::create([
//                    'tranche_id' => $tranche->id,
//                    'size_id' => $size->id,
//                    'project_floor_id' => $projectFloor->id,
//                    'real_estate_type_id' => $reType->id
//                ]);
//            }
//
//            $paymentPlan = RealEstatePaymentPlan::where('name', $price['payment_plan'])->first();
//
//            if (is_null($paymentPlan)) {
//                dd("Payment Plan", $price);
//            }
//
//            $pricing = RealEstateUnitTranchePricing::where('payment_plan_id', $paymentPlan->id)
//                ->where('sizing_id', $sizing->id)
//                ->first();
//
//            if ($pricing) {
//                dd("Duplicate Price Exists");
//            }
//
//            RealEstateUnitTranchePricing::create([
//                'payment_plan_id' => $paymentPlan->id,
//                'sizing_id' => $sizing->id,
//                'price' => $price['price'],
//                'value' => $price['price']
//            ]);
//        }
//    }

    /*
     * Supply the view to create or update an exemption
     */
    public function create($id = null)
    {
        $exemption = $id ? $this->exemptionRepository->getExemptionById($id) : new Exemption();

        $approvals = $this->getUnverifiedApprovals();

        return view('system.exemptions.create', [
            'exemption' => $exemption,
            'title' => 'Add/Edit an exemption',
            'approvals' => $approvals
        ]);
    }

    /*
     * Get any unapproved approvals
     */
    public function getUnverifiedApprovals()
    {
        $approvalsArray = array();

        $approvals = ClientTransactionApproval::whereNull('approved')
            ->whereIn('transaction_type', [
                'withdrawal',
                'withdraw',
                'edit_investment',
                'bulk_remove_real_estate_payment_schedules',
                'delete_real_estate_payment',
                'make_real_estate_payment',
                'real_estate_delete_holding',
                'real_estate_reverse_forfeiture',
                'real_estate_unit_forfeiture',
                'real_estate_unit_transfer',
                'remove_real_estate_payment_schedule',
                'sales_agreement_signing',
                'sales_agreement_uploading',
                'update_realestate_payment',
                'update_real_estate_payment_schedule',
                'update_real_estate_pricing_details',
                'create_unit_fund_sale',
                'create_unit_fund_transfer'
            ])
            ->whereNotNull('client_id')->latest()->get();

        $approvalsArray[''] = 'Select Approval';

        foreach ($approvals as $approval) {
            if ($approval->client) {
                $approvalsArray[$approval->id] = $approval->id . ' - ' .
                    $approval->client->present()->clientCodeAndFullname . ' ' .
                    ucfirst(str_replace('_', ' ', $approval->transaction_type)) .
                    ' ' . $approval->created_at->toRfc850String();
            } else {
                $approvalsArray[$approval->id] = $approval->id . ' - ' . "Unspecified";
            }
        }

        ksort($approvalsArray);

        return $approvalsArray;
    }

    /*
     * Add approval to store or update an exemption
     */
    public function store(Request $request, $id = null)
    {
        $validation = $this->createExemption($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $input = $request->except('_token');

        $approval = ClientTransactionApproval::findOrFail($input['client_transaction_approval_id']);

        ClientTransactionApproval::add(
            ['client_id' => $approval->client_id, 'transaction_type' => 'exemption',
                'payload' => $input, 'scheduled' => 0]
        );

        \Flash::message('Exemption has been saved for approval');

        return redirect('/dashboard/system/exemptions');
    }
}
