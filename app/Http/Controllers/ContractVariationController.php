<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/29/19
 * Time: 11:46 AM
 */

namespace App\Http\Controllers;

use App\Cytonn\Mailers\RealEstate\ContractVariationMailer;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\RealEstate\RealEstateContractVariation;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Reporting\ContractVariationGenerator;
use Cytonn\Core\DataStructures\Carbon;

class ContractVariationController extends Controller
{
    public function generate($loo_id)
    {
        $loo = RealestateLetterOfOffer::findOrFail($loo_id);

        $variation = new RealEstateContractVariation();

        $input = request()->all();

        $prev_holding = UnitHolding::findOrFail($input['prev_holding_id']);

        $original_data = [
            'loo_signed_date' => $prev_holding->loo->sent_on,
            'unit_size' => $prev_holding->unit->size->name,
            'unit_number' => $prev_holding->unit->number,
            'unit_price' => (int) $prev_holding->price(),
            'prev_loo_id' => $prev_holding->loo->id
        ];

        if ($loo->contractVariation->first()) {
            $variation = $loo->contractVariation->first();

            $variation->update($original_data);
        } else {
            $variation->fill($original_data);
        }

        $variation->generated_on = Carbon::today();
        $variation->generated_by = auth()->user()->id;
        $variation->loo_id = $loo->id;

        $variation->save();

        \Flash::message('Contract variation generated');

        return \Redirect::back();
    }

    public function preview($contract_variation_id)
    {
        $variation = RealEstateContractVariation::findOrFail($contract_variation_id);

        $generator = new ContractVariationGenerator();

        return $generator->generate($variation)->stream();
    }

    public function approve($contract_variation_id)
    {
        $variation = RealEstateContractVariation::findOrFail($contract_variation_id);

        $variation->approved_by = auth()->user()->id;
        $variation->approved_on = Carbon::now();
        $variation->approval = true;

        $variation->save();

        \Flash::message('Contract variation approved by pm successfully');

        return \Redirect::back();
    }

    public function reject($contract_variation_id)
    {
        $input = request()->all();

        $variation = RealEstateContractVariation::findOrFail($contract_variation_id);

        $variation->approved_by = auth()->user()->id;
        $variation->approved_on = Carbon::now();
        $variation->reason = $input['reason'];
        $variation->approval = false;

        $variation->save();

        \Flash::message('Contract variation has been rejected');

        return \Redirect::back();
    }

    public function send($contract_variation_id)
    {
        $variation = RealEstateContractVariation::findOrFail($contract_variation_id);

        $mailer = new ContractVariationMailer();

        $mailer->sendEmail($variation);

        \Flash::success('The contract variation has been sent to the client');

        return \Redirect::back();
    }
}
