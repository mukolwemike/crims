<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Http\Controllers\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Investment\InvestmentPaymentSchedules\InvestmentPaymentScheduleGenerator;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Cytonn\Models\Investment\InvestmentPaymentScheduleDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvestmentPaymentScheduleController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        return view("investment.investment_payment_schedules.index", [
            'investment' => $investment,
            'paymentScheduleDetail' => $investment->investmentPaymentScheduleDetail,
            'paymentIntervals' => $this->interestIntervals()
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($id)
    {
        $paymentSchedule = InvestmentPaymentSchedule::findOrFail($id);

        return view('investment.investment_payment_schedules.details', [
            'investment' => $paymentSchedule->parentInvestment,
            'paymentSchedule' => $paymentSchedule
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function process(Request $request, $id)
    {
        $schedule = InvestmentPaymentSchedule::findOrFail($id);

        $investment = $schedule->parentInvestment;

        $investedDate = $request->get('invested_date') ? Carbon::parse($request->get('invested_date'))
            : Carbon::parse($schedule->date);

        $maturityDate = Carbon::parse($investment->maturity_date);

        ClientTopupForm::create([
            'client_id' => $investment->client_id,
            'user_id' => Auth::id(),
            'amount' => $request->get('amount'),
            'agreed_rate' => $request->get('interest_rate'),
            'product_id' => $investment->product_id,
            'tenor' => $maturityDate->diffInMonths($investedDate),
            'schedule_id' => $schedule->id,
            'invested_date' => $request->get('invested_date')
        ]);

        \Flash::success('Payment schedule instruction submitted for processing successfully');

        return redirect('dashboard/investments/investment_payment_schedules/details/' . $schedule->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function previewPaymentSchedule($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $generator = new InvestmentPaymentScheduleGenerator();

        return $generator->generate($investment)->stream();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateScheduleDetail(Request $request, $id)
    {
        $scheduleDetail = InvestmentPaymentScheduleDetail::findOrFail($id);

        $investment = $scheduleDetail->investment;

        $paidSchedules = $investment->childInvestmentPaymentSchedules()->whereNotNull('investment_id')->exists();

        if ($paidSchedules) {
            \Flash::error("There are existing schedules that are already paid");

            return back();
        }

        $input = $request->except(['_token']);

        $input['id'] = $scheduleDetail->id;
        $input['amount_before'] = $scheduleDetail->amount;
        $input['payment_date_before'] = $scheduleDetail->payment_date;
        $input['payment_interval_before'] = $scheduleDetail->payment_interval;
        $input['investment_id'] = $scheduleDetail->investment_id;

        ClientTransactionApproval::make(
            $scheduleDetail->investment->client_id,
            'store_investment_payment_schedule_detail',
            $input
        );

        \Flash::success('Investment payment schedule detail saved for approval successfully');

        return redirect('/dashboard/investments/investment_payment_schedules/' . $scheduleDetail->investment_id);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listing()
    {
        return view('investment.investment_payment_schedules.listing');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendBulkReminders(Request $request)
    {
        $startDate = Carbon::parse($request->get('start_date'));

        $endDate = Carbon::parse($request->get('end_date'));

        $investmentSchedules = InvestmentPaymentSchedule::between($startDate, $endDate)
            ->paid(false)
            ->whereHas('parentInvestment', function ($investment) {
                $investment->active();
            })
            ->get()->lists('id');

        if (count($investmentSchedules) == 0) {
            \Flash::error('No investment payment schedules found for the selected dates');

            return back();
        }

        ClientTransactionApproval::add([
            'client_id' => null,
            'transaction_type' => 'send_investment_payment_schedule_reminders',
            'payload' => [
                'investment_schedules' => $investmentSchedules,
                'removed' => [],
                'start' => $startDate->toDateString(),
                'end' => $endDate->toDateString()
            ]
        ]);

        \Flash::message('The investment payment schedule reminders have been sent for bulk approval');

        return back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function extendPlan(Request $request, $id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $date = Carbon::parse($request->get('maturity_date'));

        if ($date <= $investment->maturity_date) {
            \Flash::error("The new maturity date should be after the current maturity date");

            return back();
        }

        $input = $request->except(['_token']);
        $input['maturity_date_before'] = Carbon::parse($investment->maturity_date)->toDateString();

        ClientTransactionApproval::make(
            $investment->client_id,
            'extend_investment_payment_schedule',
            $input
        );

        \Flash::message('The investment payment schedule extension has been sent for approval');

        return back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendContractTermination(Request $request, $id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $input = $request->except(['_token']);

        ClientTransactionApproval::make(
            $investment->client_id,
            'send_investment_contract_termination',
            $input
        );

        \Flash::message('The investment payment schedule contract termination notice has been sent for approval');

        return back();
    }
}
