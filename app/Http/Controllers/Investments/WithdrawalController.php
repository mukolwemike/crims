<?php
/**
 * Date: 03/11/2017
 * Time: 10:19
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Http\Controllers\Controller;
use App\Http\Requests\Investments\Withdrawal;
use Carbon\Carbon;

class WithdrawalController extends Controller
{
    /**
     * Show the form to withdraw a client investment
     *
     * @param  $id
     * @return mixed
     */
    public function create($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $kycValidated = $investment->client->repo->checkClientKycValidated();

        if ($investment->withdrawn == 1) {
            \Flash::warning('This investment has already been withdrawn');

            return redirect()->back();
        }

        return \view('investment.actions.withdraw.clientwithdraw', [
            'title' => 'Withdraw Investment',
            'investment' => $investment,
            'kycValidated' => $kycValidated,
            'premature' => \Session::pull('premature'), 'partial_withdraw' => \Session::pull('partial_withdraw'),
            'end_date' => \Session::pull('end_date')
        ]);
    }

    /**
     * Save a withdrawal for approval
     *
     * @param Withdrawal $request
     * @return mixed
     */
    public function store(Withdrawal $request, $id)
    {
        dd('disabled');

        $investment = ClientInvestment::findOrFail($id);

        $action_date = $request->get('premature') == 1
            ? $request->get('end_date') : $investment->maturity_date;

        $action_date = Carbon::parse($action_date);

        if ($action_date->isFuture()) {
            \Flash::overlay('Withdrawal scheduled for to run on ' .
                $action_date->toFormattedDateString(), 'Withdrawal');
        }

        \Flash::message('Withdrawal has been saved for approval');

        $data = $request->except('_token') + ['investment_id' => $investment->id];

        if ($action_date->copy()->startOfDay()->lt($investment->maturity_date->copy()->startOfDay())) {
            $data['premature'] = 1;
        }

        ClientTransactionApproval::add($transaction = [
            'client_id' => $investment->client_id,
            'investment_id' => $investment->id,
            'transaction_type' => 'withdrawal',
            'payload' => $data
        ]);

        return redirect('/dashboard/investments');
    }
}
