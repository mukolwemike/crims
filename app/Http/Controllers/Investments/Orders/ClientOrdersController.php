<?php
/**
 * Date: 13/02/2018
 * Time: 12:00
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Investments\Orders;

use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Http\Controllers\Controller;
use Cytonn\Clients\Approvals\Handlers\NewInvestment;
use Cytonn\Clients\Approvals\Handlers\RolloverInvestment;
use Cytonn\Clients\Approvals\Handlers\Withdrawal;
use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Http\Request;

class ClientOrdersController extends Controller
{
    public function topup(Request $request, $formId)
    {
        $form = ClientTopupForm::findOrFail($formId);

        if ($form->approval) {
            throw new ClientInvestmentException("This form has already been sent for approval");
        }

        $investment = $form->latestInvestment();

        $data = $request->except('_token');

        $approvalId = \DB::transaction(
            function () use ($form, $investment, $data) {
                $approval = NewInvestment::create($form->client, $investment, \Auth::user(), $data);

                $form->update(['approval_id' => $approval->id]);

                return $approval->id;
            }
        );

        \Flash::message('The investment has been saved successfully');

        if ($form->user_id) {
            return redirect()->to('/dashboard/investments/client-instructions/topup/' . $formId);
        }

        return redirect()->to('/dashboard/investments/client-instructions/' . $approvalId . '/approval-document');
    }

    public function rollover(Request $request, $instrId)
    {
        $instruction = ClientInvestmentInstruction::findOrFail($instrId);

        if ($instruction->approval) {
            throw new ClientInvestmentException("This form has already been sent for approval");
        }

        $data = $request->except('_token');

        $approvalId = \DB::transaction(
            function () use ($instruction, $data) {
                $investment = $instruction->investment;

                $approval = RolloverInvestment::create($investment->client, $investment, \Auth::user(), $data);

                $instruction->update(['approval_id' => $approval->id]);

                return $approval->id;
            }
        );

        \Flash::message('The investment has been saved successfully for approval');

        if ($instruction->user_id) {
            return redirect()->to('/dashboard/investments/client-instructions/rollover/' . $instrId);
        }

        return redirect()->to('/dashboard/investments/client-instructions/' . $approvalId . '/approval-document');
    }

    public function withdraw(Request $request, $instr_id)
    {
        $instr = ClientInvestmentInstruction::findOrFail($instr_id);

        $investment = $instr->investment;

        $data = $request->except('_token');

        $approvalId = \DB::transaction(
            function () use ($instr, $investment, $data) {
                $approval = Withdrawal::create($investment->client, $investment, \Auth::user(), $data);

                $instr->update(['approval_id' => $approval->id]);

                return $approval->id;
            }
        );

        if ($instr->due_date->isFuture()) {
            \Flash::overlay(
                'Withdrawal scheduled for to run on ' . $instr->due_date->toFormattedDateString(),
                'Withdrawal'
            );
        }

        \Flash::message('Withdrawal has been saved for approval');

        if ($instr->user_id) {
            return redirect()->to('/dashboard/investments/client-instructions/rollover/' . $instr_id);
        }

        return redirect()->to('/dashboard/investments/client-instructions/' . $approvalId . '/approval-document');
    }
}
