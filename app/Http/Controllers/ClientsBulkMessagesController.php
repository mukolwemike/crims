<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientBulkMessage;
use App\Cytonn\Models\ClientTransactionApproval;

/**
 * Class ClientsBulkMessagesController
 */
class ClientsBulkMessagesController extends Controller
{

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('investment.messages.index', ['title'=>'Clients Bulk Messages']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $message = ClientBulkMessage::find($id);
        return view('investment.messages.show', ['title'=>'Clients Bulk Message Details', 'message'=>$message]);
    }

    public function getCreate($id = null)
    {
        $message = is_null($id) ? new ClientBulkMessage() : ClientBulkMessage::findOrFail($id);
        return view('investment.messages.create', ['title'=>'New Clients Bulk Message', 'message'=>$message]);
    }

    public function postSend($id = null)
    {
        $message = is_null($id) ? new ClientBulkMessage() : ClientBulkMessage::findOrFail($id);

        $input = \Request::except('_token', '_method', 'files');

        $input['message_id'] = $message->id;
        $input['composer'] = \Auth::id();

        ClientTransactionApproval::make(null, 'client_bulk_messaging', $input);

        \Flash::message('The message has been saved for approval');

        return \redirect('/dashboard/investments/bulk-messages');
    }
}
