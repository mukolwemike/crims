<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Forms\DeductInvestmentForm;

/**
 * Date: 12/03/2016
 * Time: 11:06 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

class InvestmentDeductionsController extends Controller
{
    protected $deductInvestmentForm;

    /**
     * InvestmentDeductionsController constructor.
     *
     * @param DeductInvestmentForm $deductInvestmentForm
     */
    public function __construct(DeductInvestmentForm $deductInvestmentForm)
    {
        parent::__construct();

        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->deductInvestmentForm = $deductInvestmentForm;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function index($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $investment = ClientInvestment::findOrFail($id);

        $total_deductions = $investment->repo->getTotalDeductions();

        return view(
            'investment.deductions.index',
            ['investment'=>$investment, 'title'=>'Investment Deductions', 'total_deductions'=>$total_deductions]
        );
    }

    public function create($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $investment = ClientInvestment::findOrFail($id);

        if ($investment->withdrawn) {
            throw new ClientInvestmentException('You cannot deduct a withdrawn investment');
        }

        $input = \Request::all();
        $input['maturity_date'] = $investment->maturity_date->toDateString();
        $input['interest_accrued'] =
            (float) $investment->repo->getNetInterestForInvestmentAtDate(\Request::get('date'));

        $this->deductInvestmentForm->validate($input);

        ClientTransactionApproval::add(
            [
                'client_id'=>$investment->client_id, 'transaction_type'=>'deduct_investment',
                'payload'=>['investment_id'=>$investment->id, 'amount'=>\Request::get('amount'),
                    'narrative'=>\Request::get('narrative'), 'date'=>\Request::get('date')]
            ]
        );

        \Flash::message('The investment deduction has been saved for approval');

        return \redirect('/dashboard/investments');
    }
}
