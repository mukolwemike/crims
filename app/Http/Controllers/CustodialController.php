<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Bank;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialAccountBalanceTrail;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\CustodialWithdrawType;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Forms\BankForm;
use Cytonn\Portfolio\Forms\CustodialDepositForm;
use Cytonn\Portfolio\Forms\CustodialForm;
use Cytonn\Portfolio\Forms\CustodialReconcileForm;
use Cytonn\Portfolio\Forms\CustodialTransferForm;
use Cytonn\Portfolio\Forms\CustodialWithdrawForm;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

/**
 * Date: 16/10/2015
 * Time: 8:12 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

/**
 * Class CustodialController
 */
class CustodialController extends Controller
{

    /**
     * @var CustodialForm
     */
    protected $custodialForm;
    /**
     * @var BankForm
     */
    protected $bankForm;
    /**
     * @var CustodialWithdrawForm
     */
    protected $custodialWithdrawForm;
    /**
     * @var CustodialTransferForm
     */
    protected $custodialTransferForm;
    /**
     * @var CustodialReconcileForm
     */
    protected $custodialReconcileForm;
    /**
     * @var $custodialDepositForm
     */
    protected $custodialDepositForm;

    /**
     * CustodialController constructor.
     *
     * @param CustodialForm $custodialForm
     * @param BankForm $bankForm
     * @param CustodialWithdrawForm $custodialWithdrawForm
     * @param CustodialTransferForm $custodialTransferForm
     * @param CustodialReconcileForm $custodialReconcileForm
     * @param CustodialDepositForm $custodialDepositForm
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function __construct(
        CustodialForm $custodialForm,
        BankForm $bankForm,
        CustodialWithdrawForm $custodialWithdrawForm,
        CustodialTransferForm $custodialTransferForm,
        CustodialReconcileForm $custodialReconcileForm,
        CustodialDepositForm $custodialDepositForm
    ) {
        parent::__construct();
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $this->custodialForm = $custodialForm;
        $this->bankForm = $bankForm;
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->custodialWithdrawForm = $custodialWithdrawForm;
        $this->custodialTransferForm = $custodialTransferForm;
        $this->custodialReconcileForm = $custodialReconcileForm;
        $this->custodialDepositForm = $custodialDepositForm;
    }


    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function custodials()
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $custodials = CustodialAccount::forFundManager()->get();

        return view(
            'portfolio.custodial.custodials',
            ['title' => 'Custodial accounts', 'custodials' => $custodials]
        );
    }

    /**
     * @param null $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function addCustodial($id = null)
    {
        $this->authorizer->checkAuthority('addcustodialaccount');

        is_null($id) ? $custodial = new CustodialAccount() : $custodial = CustodialAccount::findOrFail($id);

        $banks = Bank::all()->mapWithKeys(
            function ($bank) {
                return [$bank['id'] => $bank['name']];
            }
        );

        $currencies = Currency::all()->mapWithKeys(
            function ($currency) {
                return [$currency['id'] => $currency['code']];
            }
        );

        $fund_managers = FundManager::all()->mapWithKeys(
            function ($manager) {
                return [$manager['id'] => $manager['fullname']];
            }
        );

        return view(
            'portfolio.custodial.addcustodial',
            [
                'title' => 'Add/Edit a custodial account', 'custodial' => $custodial,
                'banks' => $banks, 'currencies' => $currencies, 'fund_managers' => $fund_managers
            ]
        );
    }

    /**
     * @param null $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function storeCustodial($id = null)
    {
        $this->authorizer->checkAuthority('addcustodialaccount');

        $this->custodialForm->validate(Request::all());

        is_null($id) ? $custodial = new CustodialAccount() : $custodial = CustodialAccount::findOrFail($id);

        $input = Request::except(['files', '_token']);

        unset($input['bank_id']);

        $input = array_add($input, 'bank_name', Bank::findOrFail(Request::get('bank_id'))->name);

        $custodial->fill($input);
        $custodial->save();

        \Flash::success('Custodial account succesfully saved');

        return redirect('/dashboard/portfolio/custodials');
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function custodialDetails($id)
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $search_str = \Request::get('search_str');
        $search_date_start = \Request::get('search_date_start');
        $search_date_end = \Request::get('search_date_end');
        $search_type = \Request::get('search_type');

        $custodial = CustodialAccount::findOrFail($id);

        if (!is_null($search_str) and !empty($search_str)) {
            $trans = $custodial->transactions()->where('amount', '<>', 0)
                ->where('description', 'like', '%' . $search_str . '%')
                ->latest('date')->orderBy('id', 'desc')->paginate(25);
        }
        if (!is_null($search_date_start) and !empty($search_date_start) or
            !is_null($search_date_end) and !empty($search_date_end)) {
            $trans = $this->filterByDate($search_date_start, $search_date_end, $custodial);
        }
        if (!is_null($search_type) and !empty($search_type)) {
            $trans = $custodial->transactions()->where('amount', '<>', 0)->whereHas(
                'transactionType',
                function ($type) use ($search_type) {
                    return $type->where('name', $search_type);
                }
            )->latest('date')->orderBy('id', 'desc')->paginate(25);
        }

        $transaction_types = CustodialTransactionType::all()
            ->map(
                function ($type) {
                    return ['id' => $type->id, 'description' => $type->description . ' - ' . $type->name];
                }
            );

        $products = Product::where('active', 1)->get()->lists('name', 'id');

        $shareentities = SharesEntity::whereNull('deleted_at')->get()->lists('name', 'id');

        $projects = Project::whereNull('deleted_at')->get()->lists('name', 'id');

        $funds = UnitFund::whereNull('deleted_at')->get()->lists('name', 'id');

        $unitFunds = $custodial->funds;
        $accountProducts = $custodial->accountProducts;
        $shares = $custodial->shares;
        $reProjects = $custodial->projects;

        return view(
            'portfolio.custodial.custodialdetails',
            [
                'title' => 'Custodial Accounts',
                'custodial' => $custodial,
                'search_str' => $search_str,
                'search_date_start' => $search_date_start,
                'search_date_end' => $search_date_end,
                'search_type' => $search_type,
                'transaction_types' => $transaction_types,
                'products' => $products,
                'projects' => $projects,
                'shareentities' => $shareentities,
                'funds' => $funds,
                'unitFunds' => $unitFunds,
                'accountProducts' => $accountProducts,
                'shares' => $shares,
                'reProjects' => $reProjects
            ]
        );
    }

    /*
     * Display the listing for the custodial currencies exchange rates
     */
    public function exchangeRates()
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $baseCurrencies = ['0' => 'All'] + Currency::whereHas(
            'baseExchangeRates',
            function ($q) {
            }
        )->pluck('name', 'id')->toArray();

        $toCurrencies = ['0' => 'All'] + Currency::whereHas(
            'toExchangeRates',
            function ($q) {
            }
        )->pluck('name', 'id')->toArray();

        return view(
            'portfolio.custodial.custodial_exchange_rates',
            [
                'title' => 'Currency Exchange Rates',
                'baseCurrencies' => $baseCurrencies,
                'toCurrencies' => $toCurrencies
            ]
        );
    }

    public function viewAccountBalanceTrail($id)
    {
        $custodial = CustodialAccount::findOrFail($id);

        return view('portfolio.custodial.accountbalancetrail', ['title' => 'Custodial Account Balance Trail',
            'custodial' => $custodial]);
    }

    /**
     * @param $id
     */
    public function exportAccountBalanceTrails($id)
    {
        $custodial = CustodialAccount::findOrFail($id);

        return \Excel::create(
            $custodial->account_name . ' Account Balance Trails',
            function ($excel) use ($custodial) {
                $excel->sheet(
                    'Balance Trails',
                    function ($sheet) use ($custodial) {
                        $date = new Carbon();

                        $t = $custodial->balanceTrail->map(
                            function ($trail) use ($date) {
                                $trail->date = \Cytonn\Presenters\DatePresenter::formatDate($trail->date);
                                $trail->amount = \Cytonn\Presenters\AmountPresenter::currency($trail->amount);

                                return $trail;
                            }
                        );

                        $sheet->loadView(
                            'exports.account_balance_trails',
                            ['custodial' => $custodial, 'trails' => $t, 'date' => $date]
                        );
                    }
                );
            }
        )->download('xlsx');
    }


    public function deleteBalanceTrail($trail_id)
    {
        $trail = CustodialAccountBalanceTrail::findOrFail($trail_id);

        $input['trail_id'] = $trail->id;

        PortfolioTransactionApproval::add(
            ['institution_id' => null, 'transaction_type' => 'delete_custodial_account_balance_trail',
                'payload' => $input]
        );

        \Flash::message('Request to delete the custodial account balance trail has been sent for approval');

        return redirect('/dashboard/portfolio/custodials/account-balance-trail/' . $trail->account_id);
    }

    /**
     * @param $search_date_start
     * @param $search_date_end
     * @param CustodialAccount $custodial
     * @return mixed
     */
    protected function filterByDate($search_date_start, $search_date_end, CustodialAccount $custodial)
    {
        // If both are defined
        if (!is_null($search_date_start) and !empty($search_date_start) and
            !is_null($search_date_end) and !empty($search_date_end)) {
            return $custodial->transactions()->where('amount', '<>', 0)
                ->where('date', '>=', $search_date_start)
                ->where('date', '<=', $search_date_end)->latest('date')
                ->orderBy('id', 'desc')->paginate(25);
        } elseif (!is_null($search_date_start) and !empty($search_date_start)) { // If only start date is defined
            return $custodial->transactions()->where('amount', '<>', 0)
                ->where('date', '>=', $search_date_start)->latest('date')
                ->orderBy('id', 'desc')->paginate(25);
        } elseif (!is_null($search_date_end) and !empty($search_date_end)) { // If only the end date is defined
            return $custodial->transactions()->where('amount', '<>', 0)
                ->where('date', '<=', $search_date_end)->latest('date')
                ->orderBy('id', 'desc')->paginate(25);
        }
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function storeBank()
    {
        $this->authorizer->checkAuthority('addcustodialaccount');

        $bank = new Bank();
        $this->bankForm->validate(Request::all());
        $bank->fill(Request::all());
        $bank->save();

        \Flash::success('Bank successfully added');

        return Redirect::back();
    }

    /**
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function exportToExcel()
    {
        $userId = auth()->user()->id;

        $input = request()->all();

        if ((new Carbon($input['to']))->lt(new Carbon($input['from']))) {
            throw new ClientInvestmentException('Please check your dates');
        }

        $account = (new CustodialAccount())->findOrFail($input['id'])->id;

        $start = Carbon::parse($input['from'])->toDateString();

        $end = Carbon::parse($input['to'])->toDateString();

        $this->queue(
            function () use ($userId, $account, $start, $end) {
                \Artisan::queue(
                    'portfolio:custodial_account_report',
                    ['user_id' => $userId, 'account' => $account, 'start' => $start, 'end' => $end]
                );
            }
        );

        \Flash::success('The custodial account export will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function transact($id)
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $account = CustodialAccount::findOrFail($id);

        $otherAccounts = CustodialAccount::where('id', '!=', $id)->get()->lists('full_name', 'id');

        $withdrawTypes = CustodialWithdrawType::pluck('name', 'id')->all();

        return view(
            'portfolio.custodial.withdraw',
            ['title' => 'Withdraw from Cutodial Account', 'account' => $account,
                'other_accounts' => $otherAccounts, 'withdrawTypes' => $withdrawTypes
            ]
        );
    }

    public function createWht($id)
    {
        return view('portfolio.custodial.wht', [
            'title' => 'Withholding Tax'
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveWithdraw($id)
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $this->custodialWithdrawForm->validate(\Request::all());

        $input = Request::all();

        unset($input['_token']);

        CustodialAccount::findOrFail($id);

        $input = array_add($input, 'custodial_id', $id);

        PortfolioTransactionApproval::add(['institution_id' => null, 'transaction_type' => 'custodial_payment',
            'payload' => $input]);

        \Flash::message('Transaction saved for approval');

        return redirect('dashboard/portfolio/custodials/details/'. $id);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function transfer($id)
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');
        $this->custodialTransferForm->validate(\Request::all());

        $input = \Request::all();
        $input['reverse'] = \Request::get('reverse') == 'true';
        CustodialAccount::findOrFail($id);
        $input['source_account'] = $id;
        unset($input['_token']);
        
        PortfolioTransactionApproval::add(['institution_id' => null, 'transaction_type' => 'transfer',
            'payload' => $input]);

        \Flash::message('The transfer has been saved for approval');

        return redirect('dashboard/portfolio/custodials/details/'. $id);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function reconcile($id)
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $this->custodialReconcileForm->validate(\Request::all());

        $input = Request::all();
        unset($input['_token']);

        CustodialAccount::findOrFail($id);

        $input = array_add($input, 'custodial_id', $id);
        PortfolioTransactionApproval::add(['institution_id' => null, 'transaction_type' => 'custodial_reconciliation',
            'payload' => $input]);
        \Flash::message('Reconciliation saved for approval');

        return redirect('dashboard/portfolio/custodials/details/'. $id);
    }

    public function getEditTransaction($id)
    {
        $transaction = CustodialTransaction::findOrFail($id);

        return view('portfolio.custodial.editcustodialtransaction', ['title' => 'Edit Custodial Transaction',
            'transaction' => $transaction]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function updateTransaction($id)
    {
        $this->custodialReconcileForm->validate(\Request::all());

        $input = Request::all();

        unset($input['_token']);

        CustodialAccount::findOrFail($id);

        $input = array_add($input, 'custodial_id', $id);

        PortfolioTransactionApproval::add(['institution_id' => null,
            'transaction_type' => 'update_custodial_transaction', 'payload' => $input]);

        \Flash::message('Reconciliation update saved for approval');

        return redirect('dashboard/portfolio/custodials/details/'. $id);
    }

    public function deposit($id)
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $this->custodialDepositForm->validate(\Request::all());

        $input = Request::all();

        unset($input['_token']);

        CustodialAccount::findOrFail($id);

        $input = array_add($input, 'custodial_id', $id);

        PortfolioTransactionApproval::add(['institution_id' => null, 'transaction_type' => 'custodial_deposit',
            'payload' => $input]);

        \Flash::message('Deposit transaction saved for approval');

        return redirect('dashboard/portfolio/custodials/details/'. $id);
    }

    public function postBalance($id)
    {
        $data = \Request::except('_token');

        $data['account_id'] = $id;

        PortfolioTransactionApproval::add(
            [
                'institution_id' => null,
                'transaction_type' => 'custodial_account_bank_balance',
                'fund_manager_id' => CustodialAccount::findOrFail($id)->fund_manager_id,
                'payload' => $data
            ]
        );

        \Flash::message('The balance has been saved for approval');

        return \Redirect::back();
    }

    public function linkProductProject()
    {
        $input = request()->all();

        unset($input['_token']);

        $id = $input['id'];

        $account = CustodialAccount::findOrFail($id);

        $input = array_add($input, 'custodial_id', $account->id);

        PortfolioTransactionApproval::add(
            ['institution_id' => null,
            'transaction_type' => 'link_receiving_account', 'payload' => $input]
        );

        \Flash::message('Link Product/ Project to receiving account saved for approval');

        return \Redirect::back();
    }

    public function getTransferMismatch($id)
    {
        return view(
            'portfolio.custodial.transfer_mismatch',
            ['title'=>'Transfer Mismatch Inflow Details', 'id' => $id]
        );
    }
}
