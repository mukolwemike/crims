<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalBatch;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalRejection;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStage;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStep;
use Cytonn\Notifier\NotificationRepository;
use Cytonn\Portfolio\Approvals\Engine\Approval;
use Cytonn\Portfolio\Events\ApprovalIsReady;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Cytonn\Portfolio\Forms\InvestForm;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use ReflectionException;

class PortfolioApprovalController extends Controller
{
    use \Laracasts\Commander\Events\DispatchableTrait;

    private $nextAction = null;


    /**
     * PortfolioApprovalController constructor.
     */
    public function __construct(InvestForm $investForm)
    {
        $this->investForm = $investForm;
        $this->authorizer = new \Cytonn\Authorization\Authorizer();

        parent::__construct();
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function index()
    {
        $this->authorizer->checkAuthority('approveportfoliotransaction');

        $institutions = PortfolioInvestor::all()->lists('name', 'id');
        $transaction_types = PortfolioTransactionApproval::distinct()->get()->lists('transaction_type');
        $stages = PortfolioTransactionApprovalStage::pluck('name', 'id');

        foreach ($transaction_types as $key => $type) {
            $transaction_types[$type] = ucfirst(str_replace('_', ' ', $type));
            unset($transaction_types[$key]);
        }

        return view('portfolio.approval.approvals', [
            'title' => 'Approve Portfolio Transactions',
            'transaction_types' => $transaction_types,
            'institutions' => $institutions,
            'stages' => $stages
        ]);
    }

    public function batches()
    {
        $stages = PortfolioTransactionApprovalStage::where('can_be_batched', 1)->get();

        $batches = PortfolioTransactionApprovalBatch::all();

        $b = [];

        foreach ($stages as $stage) {
            $b[] = $batches->map(
                function ($batch) use ($stage) {
                    $types = $batch->types->lists('slug');

                    $count = PortfolioTransactionApproval::notApproved()
                        ->where('awaiting_stage_id', $stage->id)
                        ->whereIn('transaction_type', $types)
                        ->notRejected()
                        ->count();

                    return (object)[
                        'batch' => $batch,
                        'stage' => $stage,
                        'transaction_count' => $count
                    ];
                }
            );
        }

        $batches = collect($b)->flatten()->all();

        return view(
            'portfolio.approval.batches',
            ['title' => 'Batch Transaction Approvals', 'batches' => $batches]
        );
    }

    public function batch($stageId, $batchId)
    {
        $batch = PortfolioTransactionApprovalBatch::findOrFail($batchId);
        $stage = PortfolioTransactionApprovalStage::findOrFail($stageId);

        $types = $batch->types->lists('slug');

        $transactions = PortfolioTransactionApproval::notApproved()
            ->where('awaiting_stage_id', $stage->id)
            ->whereIn('transaction_type', $types)
            ->notRejected()
            ->take(20)
            ->get();

        $columns = $batch->columns()->orderBy('weight', 'ASC')->get();

        return view(
            'portfolio.approval.batch',
            [
                'title' => 'Batch Approvals: ' . $batch->name, 'batch' => $batch,
                'stage' => $stage,
                'transactions' => $transactions,
                'columns' => $columns
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function showApproval($id)
    {
        $this->authorizer->checkAuthority('approveportfoliotransaction');

        $approval = PortfolioTransactionApproval::find($id);

        if (is_null($approval)) {
            \Flash::error('Portfolio approval transaction could not be found. It has probably been deleted');
            return \Redirect::back();
        }

        $stage = (new Approval($approval))->nextStage();

        $checkApprovedStep = function ($approval, $stage) {
            return PortfolioTransactionApprovalStep::where('approval_id', $approval->id)
                ->where('stage_id', $stage->id)
                ->first();
        };

        $rejections = $approval->rejections()->whereNull('resolved_by')->get();

        try {
            $viewData = $approval->handler()->prepareView($approval);
        } catch (ReflectionException $e) {
            throw new \Cytonn\Exceptions\ClientInvestmentException('Transaction not supported!');
        }

        if (!isset($viewData['data'])) {
            $viewData['data'] = $approval->payload;
        }

        $viewFolder = 'portfolio.approval.transactions';

        return view(
            'portfolio.approval.main',
            $viewData + [
                'title' => 'Approve Portfolio Transaction',
                'approval' => $approval,
                'view_folder' => $viewFolder,
                'approved_step' => $checkApprovedStep,
                'stage' => $stage,
                'rejections' => $rejections
            ]
        );
    }

    /**
     * @param $id
     * @param $stageId
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function approve($id, $stageId)
    {
        $this->authorizer->checkAuthority('approveportfoliotransaction');

        $approval = PortfolioTransactionApproval::findOrFail($id);
        $stage = PortfolioTransactionApprovalStage::findOrFail($stageId);

        $this->validateApproval($approval, $stage);

        \Event::listen(
            'Cytonn.Portfolio.Events.ApprovalSuccessful',
            function (ApprovalSuccessful $event) {
                $this->nextAction = $event->next;
            }
        );

        $approval->raise(new ApprovalIsReady($approval));

        $this->dispatchEventsFor($approval);

        if ($this->nextAction) {
            return $this->nextAction;
        } else {
            Flash::error('Transaction type is not supported');

            return \Redirect::back();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function saveApproval($id)
    {
        $approval = PortfolioTransactionApproval::findOrFail($id);

        $input = Request::all();
        unset($input['_token']);

        if ($input['premature'] == 'on') {
            $input['premature'] = true;
        }

        $approval->payload = $input;
        $approval->sent_by = Auth::user()->id;
        $approval->save();

        Flash::message('Approval request edited');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function remove($id)
    {

        if (!(strtolower(\Request::get('confirm')) == 'delete')) {
            \Flash::error('Invalid input given. The request has not been removed.');
            return \Redirect::back();
        }

        $reason = \Request::get('reason');

        if (!(strtolower($reason))) {
            \Flash::error('You must provide a reason for cancelling the approval request.');
            return \Redirect::back();
        }

        $transaction = PortfolioTransactionApproval::findOrFail($id);

        if ($transaction->approved) {
            \Flash::error('You cannot remove an approval that is already approved');
            return \Redirect::back();
        }

        $handler = $transaction->handler();

        if (method_exists($handler, 'onReject')) {
            $handler->onReject($transaction, $reason);
        }

        $transaction->reason = $reason;

        $transaction->save();

        $transaction->delete();

        \Flash::message('Portfolio Approval request has been removed');

        return redirect('/dashboard/portfolio/approve');
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @param PortfolioTransactionApprovalStage $stage
     * @throws ClientInvestmentException
     */
    private function validateApproval(PortfolioTransactionApproval $approval, PortfolioTransactionApprovalStage $stage)
    {
        if (!(\App::environment('testing') || \App::environment('local'))) {
            if ($approval->sent_by == \Auth::user()->id) {
                throw new ClientInvestmentException('You cannot approve a transaction that you originated.');
            }

            if (in_array(\Auth::user()->id, $approval->steps->lists('user_id'))) {
                throw new ClientInvestmentException("You have already approved this transaction in a previous 
                stage, you cannot approve it again");
            }
        }

        if ($approval->approved) {
            throw new ClientInvestmentException('This transaction has already been approved');
        }

        if ($approval->steps()->where('stage_id', $stage->id)->exists()) {
            throw new ClientInvestmentException('This transaction is already approved in this stage');
        }

        if ($approval->rejections()->whereNull('resolved_by')->exists()) {
            throw new ClientInvestmentException(
                'Transaction was rejected, please resolve the issues before proceeding'
            );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $approvalId
     * @param $stageId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Throwable
     */
    public function reject(\Illuminate\Http\Request $request, $approvalId, $stageId)
    {
        $approval = PortfolioTransactionApproval::findOrFail($approvalId);
        $stage = PortfolioTransactionApprovalStage::find($stageId);

        (new Approval($approval))->checkAccess($stage);

        if ($approval->approved) {
            throw new ClientInvestmentException("Transaction is already approved");
        }

        if (in_array($stage->id, $approval->steps->lists('stage_id'))) {
            throw new ClientInvestmentException("Transaction is already approved in this stage");
        }

        if ($approval->rejections()->whereNull('resolved_by')->exists()) {
            throw new ClientInvestmentException('Transaction was rejected, you cannot reject it again');
        }

        \DB::transaction(
            function () use ($request, $approval, $stage) {
                PortfolioTransactionApprovalRejection::create([
                    'approval_id' => $approval->id,
                    'stage_id' => $stage->id,
                    'reject_by' => \Auth::user()->id,
                    'reason' => $request->get('reason')
                ]);

                (new NotificationRepository())->createNotificationWhenClientTransactionRejected($approval);
            }
        );

        if ($request->wantsJson()) {
            return response()->json(['status' => 'success', 'message' => 'Transaction successfully rejected']);
        }

        \Flash::success('Transaction successfully rejected');

        return \redirect()->back();
    }

    public function action($id, $action, $param = null)
    {
        $approval = PortfolioTransactionApproval::find($id);

        try {
            $className = ucfirst(camel_case($approval->transaction_type));

            $namespace = 'Cytonn\Portfolio\Approvals\Handlers';

            $handler = App::make($namespace.'\\'.$className);

            return $handler->action($approval, $action, $param);
        } catch (ReflectionException $e) {
            return \abort(404);
        }
    }
}
