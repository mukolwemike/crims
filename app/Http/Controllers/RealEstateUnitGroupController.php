<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers;

use App\Cytonn\Models\Project;
use Cytonn\Realestate\RealEstateUnitGroups\RealEstateUnitGroupRepository;
use Cytonn\Realestate\RealEstateUnitGroups\RealEstateUnitGroupRules;

class RealEstateUnitGroupController extends Controller
{
    /*
     * Get the real estate unit group rules
     */
    use RealEstateUnitGroupRules;

    /*
     * Specify the required repositories
     */
    protected $realEstateUnitGroupRepository;

    /*
     * Setup the constructor for the controller
     */
    public function __construct(RealEstateUnitGroupRepository $realEstateUnitGroupRepository)
    {
        $this->realEstateUnitGroupRepository = $realEstateUnitGroupRepository;
    }

    /*
     * Supply the view to list the real estate unit groups
     */
    public function index()
    {
        $projects = ['' => 'All'] + Project::pluck('name', 'id')->toArray();

        return view(
            'realestate.unitgroups.index',
            [
            'projects' => $projects
            ]
        );
    }
}
