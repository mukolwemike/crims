<?php
/**
 * Date: 24/01/2018
 * Time: 12:21
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\System;

use App\Cytonn\Models\User;
use App\Http\Controllers\Controller;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStage;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalType;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStage;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalType;
use Illuminate\Http\Request;

class ApprovalsController extends Controller
{
    public function index()
    {
        $stages = ClientTransactionApprovalStage::all();

        $approvers = User::active()->get();
        $transactions = ClientTransactionApprovalType::all();

        $portfolioStages = PortfolioTransactionApprovalStage::all();
        $portfolioTransactions = PortfolioTransactionApprovalType::all();

        return view('system.approvals.index', [
            'stages' => $stages,
            'title' => 'Approvals Setup',
            'approvers' => $approvers,
            'transactions' => $transactions,
            'portfolioStages' => $portfolioStages,
            'portfolioTransactions' => $portfolioTransactions
        ]);
    }

    public function storeApprover(Request $request, $stageId)
    {
        $user = User::findOrFail($request->get('user'));
        $stage = ClientTransactionApprovalStage::findOrFail($stageId);

        if (in_array($user->id, $stage->approvers()->get()->lists('id'))) {
            \Flash::error('User is already an approver');

            return redirect()->back();
        }

        $stage->approvers()->save(
            $user,
            [
            'granted_by' => \Auth::user()->id,
            ]
        );

        \Flash::success('Approver added successfully');

        return redirect()->back();
    }

    public function storeTransaction(Request $request, $stageId)
    {
        $stage = ClientTransactionApprovalStage::findOrFail($stageId);
        $trans = ClientTransactionApprovalType::findOrFail($request->get('transaction'));

        if (in_array($trans->id, $stage->transactionTypes()->get()->lists('id'))) {
            \Flash::error('Transaction is already added to this stage');

            return redirect()->back();
        }

        $stage->transactionTypes()->save($trans);

        \Flash::success('The transaction has been added to this stage');

        return redirect()->back();
    }

    public function deleteApprover(Request $request, $stageId, $userId)
    {
        $user = User::findOrFail($userId);
        $stage = ClientTransactionApprovalStage::findOrFail($stageId);

        $stage->approvers()->detach($user);

        \Flash::success('User successfully removed');

        return redirect()->back();
    }

    public function deleteTransaction(Request $request, $stageId, $transTypeId)
    {
        $stage = ClientTransactionApprovalStage::findOrFail($stageId);
        $trans = ClientTransactionApprovalType::findOrFail($transTypeId);

        $stage->transactionTypes()->detach($trans);

        \Flash::success('The transaction has been removed from this stage');

        return redirect()->back();
    }

    public function storePortfolioTransaction(Request $request, $stageId)
    {
        $stage = PortfolioTransactionApprovalStage::findOrFail($stageId);
        $trans = PortfolioTransactionApprovalType::findOrFail($request->get('transaction'));

        if (in_array($trans->id, $stage->transactionTypes()->get()->lists('id'))) {
            \Flash::error('Transaction is already added to this stage');

            return redirect()->back();
        }

        $stage->transactionTypes()->save($trans);

        \Flash::success('The transaction has been added to this stage');

        return redirect()->back();
    }

    public function deletePortfolioTransaction(Request $request, $stageId, $transTypeId)
    {
        $stage = PortfolioTransactionApprovalStage::findOrFail($stageId);
        $trans = PortfolioTransactionApprovalType::findOrFail($transTypeId);

        $stage->transactionTypes()->detach($trans);

        \Flash::success('The transaction has been removed from this stage');

        return redirect()->back();
    }

    public function storePortfolioApprover(Request $request, $stageId)
    {
        $user = User::findOrFail($request->get('user'));
        $stage = PortfolioTransactionApprovalStage::findOrFail($stageId);

        if (in_array($user->id, $stage->approvers()->get()->lists('id'))) {
            \Flash::error('User is already an approver');

            return redirect()->back();
        }

        $stage->approvers()->save($user, [
                'granted_by' => \Auth::user()->id,
            ]);

        \Flash::success('Approver added successfully');

        return redirect()->back();
    }

    public function deletePortfolioApprover(Request $request, $stageId, $userId)
    {
        $user = User::findOrFail($userId);
        $stage = PortfolioTransactionApprovalStage::findOrFail($stageId);

        $stage->approvers()->detach($user);

        \Flash::success('User successfully removed');

        return redirect()->back();
    }
}
