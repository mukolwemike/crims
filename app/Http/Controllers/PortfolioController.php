<?php

namespace App\Http\Controllers;

use App\Cytonn\Dashboard\PortfolioDashboardGenerator;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\DepositHoldingType;
use App\Cytonn\Models\FundType;
use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use App\Events\Portfolio\Clients\PortfolioInvestorClientAdded;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Lib\NumberToWords;
use Cytonn\Portfolio\ExportToExcel;
use Cytonn\Portfolio\Forms\InstitutionForm;
use Cytonn\Portfolio\Forms\InvestForm;
use Cytonn\Portfolio\Forms\RolloverForm;
use Cytonn\Portfolio\Forms\WithdrawForm;
use Cytonn\Portfolio\PortfolioRepository;
use Cytonn\Portfolio\Rules\PortfolioSecurityRules;
use Cytonn\Portfolio\Statement\Generator;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Realestate\Forms\ReservationForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;

/**
 * Class PortfolioController
 */
class PortfolioController extends Controller
{
    use PortfolioSecurityRules;

    /**
     * @var InvestForm
     */
    protected $investForm;

    /**
     * @var WithdrawForm
     */
    protected $withdrawForm;

    /**
     * @var RolloverForm
     */
    protected $rolloverForm;

    /**
     * @var InstitutionForm
     */
    protected $institutionForm;

    /**
     * @var ReservationForm
     */
    protected $reservationForm;

    /**
     * PortfolioController constructor.
     *
     * @param WithdrawForm $withdrawForm
     * @param RolloverForm $rolloverForm
     * @param InvestForm $investForm
     * @param InstitutionForm $institutionForm
     * @param ReservationForm $reservationForm
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function __construct(
        WithdrawForm $withdrawForm,
        RolloverForm $rolloverForm,
        InvestForm $investForm,
        InstitutionForm $institutionForm,
        ReservationForm $reservationForm
    ) {
        parent::__construct();

        $this->withdrawForm = $withdrawForm;
        $this->rolloverForm = $rolloverForm;
        $this->investForm = $investForm;
        $this->institutionForm = $institutionForm;
        $this->reservationForm = $reservationForm;
        $this->authorizer = new \Cytonn\Authorization\Authorizer();

        $this->authorizer->checkAuthority('addportfoliodeposit');
    }

    /**
     * Display the portfolio menu
     *
     * @return Response
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function index()
    {
        $gen = new PortfolioDashboardGenerator();

        $manager = $this->fundManager();

        return view('portfolio.menu', [
            'title' => 'Portfolio operations',
            'gen' => $gen,
            'manager' => $manager
        ]);
    }

    /**
     * Display the institutions grid table
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function institutions()
    {
        $this->authorizer->checkAuthority('addportfolioinstitution');
        return view('portfolio.institutions', ['title' => 'Portfolio institutions']);
    }

    /**
     * show the details of an institution
     *
     * @param  $id
     * @return mixed
     */
    public function institutionDetail($id)
    {
        $institution = PortfolioSecurity::findOrFail($id);

        $currencies = Currency::all()->mapWithKeys(
            function ($currency) {
                return [$currency['id'] => $currency['name']];
            }
        );

        return view(
            'portfolio.institutiondetail',
            [
                'title' => 'Institution details', 'security' => $institution, 'currencies' => $currencies
            ]
        );
    }

    /**
     * Show the form to add or edit an institution
     *
     * @param null $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function addInstitution($id = null)
    {
        $this->authorizer->checkAuthority('addportfolioinstitution');

        is_null($id) ? $institution = new PortfolioInvestor() : $institution = PortfolioInvestor::findOrFail($id);

        return view(
            'portfolio.addinstitution',
            ['title' => 'Add or edit an institution', 'institution' => $institution]
        );
    }

    /**
     * Save an institution details
     *
     * @param Request $request
     * @param null $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function storeInstitution(Request $request, $id = null)
    {
        $this->authorizer->checkAuthority('addportfolioinstitution');

        $input = $request->except(['files']);

        $this->institutionForm->validate($input);

        $input['id'] = $id;

        PortfolioTransactionApproval::add(
            [
                'institution_id' => $id,
                'transaction_type' => 'create_edit_portfolio_institution',
                'payload' => $input]
        );

        is_null($id)
            ? \Flash::message('Request to create institution has been saved for approval')
            : \Flash::message('Request to update institution details has been saved for approval');

        return redirect('/dashboard/portfolio/institutions');
    }

    /**
     * Show the deposits grid table
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function showInvestmentsGrid()
    {
        $this->authorizer->checkAuthority('addportfoliodeposit');

        $fund_types = FundType::all()->lists('name');

        $institutions = PortfolioInvestor::all()->lists('name', 'id');

        $currencies = Currency::all();

        return view(
            'portfolio.investments',
            [
                'title' => 'Portfolio Deposits', 'fund_types' => $fund_types,
                'institutions' => $institutions, 'currencies' => $currencies
            ]
        );
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function showInvestmentDetails($id)
    {
        $this->authorizer->checkAuthority('addportfoliodeposit');

        $deposit = DepositHolding::findOrFail($id);

        return redirect(
            "/dashboard/portfolio/securities/{$deposit->portfolio_security_id}/deposit-holdings/{$deposit->id}"
        );
        //        $maturity_date = new Carbon($deposit->maturity_date);
        //        if ($maturity_date->isPast()) {
        //            $deposit['rolloverStatus'] = 1;
        //        } else {
        //            $deposit['rolloverStatus'] = 0;
        //        }
        //
        //        return view('portfolio.investmentdetails', ['title' => 'Investment Details', 'deposit' => $deposit]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSendInstructions($id)
    {
        $deposit = DepositHolding::findOrFail($id);

        $users = User::orderBy('firstname')->get();

        return view('portfolio.senddepositplacementinstructions', [
            'title' => 'Send deposit Placement Instructions',
            'deposit' => $deposit,
            'users' => $users
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postSendInstructions(Request $request, $id)
    {
        $deposit = DepositHolding::findOrFail($id);

        $investor = $deposit->portfolioInvestor;

        $deposit_in_words = (new NumberToWords())->convertToWords(floatval($deposit->amount));

        $value_date = DatePresenter::formatDate($deposit->invested_date);

        $maturity_date = DatePresenter::formatDate($deposit->maturity_date);

        $first_signatory = User::findOrFail($request->get('first_signatory'));

        $second_signatory = User::findOrFail($request->get('second_signatory'));

        $file = \PDF::loadView('reports.deposit_placement_instructions', [
            'investor' => $investor,
            'deposit' => $deposit,
            'logo' => $deposit->security->fundManager->logo,
            'maturity_date' => $maturity_date, 'value_date' => $value_date,
            'deposit_in_words' => $deposit_in_words,
            'first_signatory' => $first_signatory,
            'second_signatory' => $second_signatory,
        ]);

        return $file->stream();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function withdraw($id)
    {
        $this->authorizer->checkAuthority('addportfoliodeposit');

        $deposit = DepositHolding::findOrFail($id);

        if ($deposit->withdrawn == 1) {
            \Flash::warning('This deposit has already been withdrawn');
        }

        return view('portfolio.withdrawinvestment', ['title' => 'Redeem Investment', 'deposit' => $deposit]);
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postWithdraw(Request $request)
    {
        $this->authorizer->checkAuthority('addportfoliodeposit');

        $id = Crypt::decrypt($request->get('deposit_id'));

        $deposit = DepositHolding::findOrFail($id);

        $institution = $deposit->security->investor;

        $input = $request->all();
        unset($input['deposit_id']);
        $input['deposit_id'] = $id;
        unset($input['_token']);

        if ($deposit->withdrawn == 1) {
            \Flash::error('This deposit has already been withdrawn');

            return Redirect::back();
        }

        $maturity_date = new Carbon($deposit->maturity_date);

        if ($input['premature'] == 'true') {
            if ($maturity_date->lte(new Carbon($input['end_date']))) {
                \Flash::error('This deposit is mature, you can only do a mature redemption or rollover');

                return Redirect::back();
            } else {
                $this->withdrawForm->premature();

                $validation_data = $input;
                $validation_data = array_add($validation_data, 'invested_date', $deposit->invested_date);
                $validation_data = array_add($validation_data, 'maturity_date', $deposit->maturity_date);

                if ($input['partial'] == 'true') {
                    $this->withdrawForm->partial();

                    if ((float)$request->get('amount') > $deposit->repo->getTotalValueOfAnInvestment()) {
                        \Flash::error('The amount redeemed is more than the deposit current value');

                        return Redirect::back();
                    }
                }

                $this->withdrawForm->validate($validation_data);

                PortfolioTransactionApproval::add(['institution_id' => $institution->id,
                    'transaction_type' => 'withdrawal', 'payload' => $input]);
            }
        } else {
            if ($maturity_date->isPast()) {
                unset($input['withdraw_date']);

                $this->withdrawForm->validate($input);

                PortfolioTransactionApproval::add(['institution_id' => $institution->id,
                    'transaction_type' => 'withdrawal', 'payload' => $input]);
            } else {
                \Flash::error('This deposit is not yet mature. You can only do a premature withdrawal');

                return Redirect::back();
            }
        }

        \Flash::message('The withdrawal has been saved for confirmation');

        return redirect('/dashboard/portfolio');

        //return view('portfolio.withdrawsuccess',
        // ['title'=>'Investment Withdrawn', 'deposit'=>$deposit, 'withdrawal'=>$withdrawal]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function rollover($id)
    {
        $this->authorizer->checkAuthority('addportfoliodeposit');

        $deposit = DepositHolding::findOrFail($id);

        if ($deposit->withdrawn == 1) {
            \Flash::warning('This deposit has already been withdrawn');
            return \Redirect::back();
        } elseif ((new Carbon($deposit->maturity_date))->isFuture()) {
            \Flash::warning('The deposit has not matured, you cannot rollover');

            return \Redirect::back();
        }

        return view('portfolio.rolloverinvestment', ['title' => 'Rollover Investment', 'deposit' => $deposit]);
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postRollover(Request $request)
    {
        $this->authorizer->checkAuthority('addportfoliodeposit');

        $input = $request->all();

        $id = Crypt::decrypt($request->get('deposit'));

        $oldInvestment = DepositHolding::findOrFail($id);
        $institution = $oldInvestment->security->investor;

        unset($input['deposit']);
        $input['deposit'] = $id;
        unset($input['_token']);

        if ($oldInvestment->withdrawn == 1) {
            \Flash::error('This deposit has already been withdrawn, you can only rollover active deposits');

            return Redirect::back();
        } elseif ((new Carbon($oldInvestment->maturity_date))->isFuture()) {
            \Flash::warning('The deposit has not matured, you cannot rollover');

            return \Redirect::back();
        }

        $validation_data = $input;
        $validation_data = array_add(
            $validation_data,
            'old_deposit_maturity_date',
            (new Carbon($oldInvestment->maturity_date))->subDay()->toDateString()
        );

        if ($oldInvestment
                ->repo->getTotalValueOfAnInvestmentAsAtDate(Carbon::parse($request->get('maturity_date'))) <
            (float)$request->get('amount') - 0.1
        ) {
            \Flash::error('You can only rollover an amount less than the value of an deposit today');

            return Redirect::back()->withInput();
        }

        if (Carbon::parse($input['invested_date']) < \Carbon\Carbon::parse($oldInvestment->maturity_date)) {
            \Flash::error('The new investment date should not be before or on the maturity date of the old investment');

            return Redirect::back()->withInput();
        }

        $this->rolloverForm->validate($validation_data);

        PortfolioTransactionApproval::add(
            ['institution_id' => $institution->id, 'transaction_type' => 'rollover', 'payload' => $input]
        );

        \Flash::message('The rollover has been saved for confirmation');

        return redirect('/dashboard/portfolio');
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function summary($type = '')
    {
        $currencies = Currency::whereHas(
            'accounts',
            function ($accounts) {
                $accounts->whereHas(
                    'fundManager',
                    function ($fundManager) {
                        $fundManager->whereHas(
                            'securities',
                            function ($security) {
                                $security->has('depositHoldings');
                            }
                        );
                    }
                );
            }
        )->get();

        $subAssetClasses = ['' => 'All'] + SubAssetClass::pluck('name', 'id')->toArray();

        $fundManager = $this->fundManager();

        $unitFunds = UnitFund::where('fund_manager_id', $fundManager->id)
            ->pluck('name', 'id')
            ->toArray();

        return view('portfolio.summary', [
            'title' => 'Deposit Summary',
            'currencies' => $currencies,
            'type' => $type,
            'subAssetClasses' => $subAssetClasses,
            'unitFunds' => $unitFunds
        ]);
    }

    public function assetSummary($type = '')
    {
        $unitFunds = UnitFund::all()->lists('name', 'id');

        $assetClasses = AssetClass::all()->lists('name', 'id');

        return view(
            'portfolio.asset_allocation',
            [
                'title' => 'Portfolio Asset Allocations',
                'unitFunds' => $unitFunds,
                'assetClasses' => $assetClasses
            ]
        );
    }

    public function export(Request $request)
    {
        $input = $request->all();
        $security = PortfolioSecurity::findOrFail($input['investor_id']);

        $as_at_date = Carbon::parse($input['date']);

        return (new ExportToExcel())
            ->investmentsForInstitution($security, $this->fundManager(), $as_at_date)->download('xlsx');
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function analysis()
    {
        $currencies = Currency::all();

        return view('portfolio.analysis', ['title' => 'Portfolio analysis', 'currencies' => $currencies]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function maturity(Request $request)
    {
        $date = $request->get('date');

        is_null($date) ? $date = Carbon::now()->addWeek() : $date = new Carbon($date);

        $comparator = '<=';

        if (is_null($request->get('before'))) {
            $comparator = '<';
        }

        $deposits = DepositHolding::where('maturity_date', '>=', Carbon::today())
            ->where('maturity_date', $comparator, $date->toDateString())
            ->active()
            ->get();

        return view('portfolio.maturity', ['title' => 'Portfolio Investments Maturity',
            'date' => $date->toDateString(), 'before' => $request->get('before'), 'deposits' => $deposits,
            'investments' => $deposits]);
    }

    /**
     * @return mixed
     */
    public function maturityAnalysis(Request $request)
    {
        is_null($request->get('start'))
            ? $start = Carbon::now() : $start = new Carbon($request->get('start'));

        is_null($request->get('end'))
            ? $end = Carbon::now()->addMonths(2) : $end = new Carbon($request->get('end'));

        $currencies = Currency::all();

        return view(
            'portfolio.maturityanalysis',
            [
                'title' => 'Funds Maturity Analysis', 'start' => $start->toDateString(),
                'end' => $end->toDateString(), 'currencies' => $currencies
            ]
        );
    }

    public function statement(Request $request, $securityId)
    {
        ini_set('max_execution_time', 300);

        ini_set('memory_limit', '1024M');

        $security = PortfolioSecurity::findOrFail($securityId);

        $currency = Currency::findOrFail($request->get('currency_id'));

        $date = Carbon::parse($request->get('date'));
        $from = $request->get('from_date');
        if ($from) {
            $from = Carbon::parse($from);
        }

        return (
        new Generator())->generate(
            $this->fundManager(),
            $currency,
            $security,
            $date,
            $from,
            \Auth::user()
        )->stream();
    }

    /*
     * Supply the view to create a client to link to the portfolio investor
     */
    public function addClient($institutionId)
    {
        $institution = PortfolioSecurity::find($institutionId);

        $clientTypes = ClientType::all()->lists('name', 'id');

        $titles = Title::all()->lists('name', 'id');

        $countries = Country::all()->lists('name', 'id');

        return view(
            'portfolio.clients.create',
            [
                'institution' => $institution,
                'clientTypes' => $clientTypes,
                'titles' => $titles,
                'countries' => $countries
            ]
        );
    }

    /*
     * Save the data for the client and send it for approval
     */
    public function storeClient(Request $request, $institutionId)
    {
        $input = $request->except('_token');

        $portfolioSecurity = PortfolioSecurity::findOrFail($institutionId);

        $input['existing_client'] = ($request->get('new_client') == 0) && $request->get('client_id');

        event(new PortfolioInvestorClientAdded($input, $portfolioSecurity));

        \Flash::success('The portfolio security client has been created');

        return \redirect('/dashboard/portfolio/security/' . $portfolioSecurity->id . '/details');
    }

    public function savePortfolioLimits()
    {
        $validator = $this->portfolioAllocation(request());

        if ($validator) {
            \Flash::error('There are errors in the form, please fill and submit again');

            return \Redirect::back();
        }

        $input = request()->except('_token', 'asset_class_id');

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'portfolio_asset_allocation',
            'payload' => $input
        ]);

        return \Redirect::back();
    }
}
