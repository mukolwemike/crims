<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\StatementCampaignType;
use App\Cytonn\Models\StatementLog;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Date: 24/02/2016
 * Time: 10:00 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class InvestmentSummaryController extends Controller
{
    /**
     * InvestmentSummaryController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->authorizer = new Authorizer();
    }

    /**
     * Show the cms client summary
     *
     * @return \Response
     */
    public function summary()
    {
        $products = Product::forFundManager()->has('investments')->get();

        return view(
            'clients.summary',
            ['title' => 'Client Summary', 'products' => $products, 'fundManager'=>$this->fundManager()]
        );
        //return view('investment.summary.summary',
        // ['title' => 'Client Summary', 'products' => $products, 'fundManager'=>$this->fundManager()]);
    }

    /**
     * Show the summary for a client
     *
     * @param  $id
     * @return mixed
     */
    public function clientSummary($id)
    {
        $client = Client::findOrFail($id);

        $stmt_log = StatementLog::where('client_id', $client->id)->latest()->first();

        $open_campaigns_arr = StatementCampaign::where('closed', null)
            ->latest()->get()->lists('name', 'id');

        $products = Product::all();


        return view(
            'investment.summary.clientsummary',
            [
                'title' => 'Client CHYS Summary', 'client' => $client, 'products' => $products, 'last_stmt'=>$stmt_log,
                'open_campaigns_arr'=>$open_campaigns_arr
            ]
        );
    }

    /**
     * List the statements
     *
     * @return \Response
     */
    public function statements()
    {
        $campaigns = StatementCampaign::latest()->paginate(10);

        $campaignTypes = StatementCampaignType::all()->lists('name', 'id');

        return view(
            'investment.statement.statements',
            ['title' => 'Client CHYS Statements', 'campaigns'=>$campaigns, 'campaign_types'=>$campaignTypes]
        );
    }

    /**
     * Show the expenses report for cms
     *
     * @return mixed
     */
    public function expenses()
    {
        $start = \Request::get('start');
        $end = \Request::get('end');

        is_null($start)
            ? $startDate = Carbon::now()->subMonths(3)->startOfMonth()
            : $startDate = new Carbon($start);
        is_null($end)
            ? $endDate = $startDate->copy()->addMonths(6)->endOfMonth()
            : $endDate = new Carbon($end);

        $dates_arr = [];
        for ($date = $startDate->copy(); $date->lte($endDate->copy()); $date = $date->addMonth()) {
            array_push($dates_arr, $date->copy());
        }

        return view(
            'investment.summary.expenses',
            ['title' => 'CHYS Expenses Report', 'dates' => $dates_arr,
                'start' => $startDate->toDateString(), 'end' => $endDate->toDateString()]
        );
    }

    /**
     * Show the cms summary history
     *
     * @return mixed
     */
    public function history()
    {
        return view('investment.summary.history', ['title' => 'CHYS Summary History']);
    }

    /**
     * View all stats about client investments
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function analytics()
    {
        $currencies = Currency::all();

        $products = Product::all();

        $from_date = Carbon::today()->subMonthsNoOverflow(6)->toDateString();

        $date = Carbon::today()->toDateString();

        return view(
            'investment.summary.analytics',
            ['title' => 'Client Investments Analytics',
            'currencies' => $currencies, 'products'=>$products, 'from_date'=>$from_date, 'date'=>$date
            ]
        );
    }
}
