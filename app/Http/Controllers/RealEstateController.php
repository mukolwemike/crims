<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Date: 18/05/2016
 * Time: 4:46 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function menu()
    {
        $total_units = Project::all()
            ->sum(
                function ($project) {
                    return $project->repo->countNumberOfUnits();
                }
            );

        $reserved_units = Project::all()->sum(
            function ($project) {
                return $project->repo->countReservedUnits();
            }
        );

        $total_clients = Client::wherehas(
            'unitHoldings',
            function ($holding) {
                $holding->where('active', true);
            }
        )->count();

        $cash_paid = Project::all()->sum(
            function ($project) {
                return $project->repo->sumOfPayments();
            }
        );

        $today = Carbon::today();

        $cash_expected_this_month =
            RealEstatePaymentSchedule::where('date', '>=', $today->copy()->startOfMonth())
                ->where('date', '<=', $today->copy()->endOfMonth())
                ->sum('amount');

        $total_re_instructions = RealEstateInstruction::whereDoesntHave('approval')->remember(30)->count();

        return \view('realestate.menu', [
            'title' => 'Real Estate Dashboard',
            'reserved_units' => $reserved_units,
            'cash_paid' => $cash_paid,
            'total_units' => $total_units,
            'total_clients' => $total_clients,
            'cash_expected_this_month' => $cash_expected_this_month,
            'total_re_instructions' => $total_re_instructions]);
    }
}
