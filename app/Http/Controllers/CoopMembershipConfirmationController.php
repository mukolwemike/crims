<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\MembershipConfirmation;
use Cytonn\Coop\Memberships\BusinessConfirmationGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

/**
 * Date: 17/09/2016
 * Time: 11:49 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class CoopMembershipConfirmationController extends Controller
{
    /**
     * @var BusinessConfirmationGenerator
     */
    protected $generator;

    /**
     * CoopMembershipConfirmationController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->generator = new BusinessConfirmationGenerator();
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('coop.memberships.confirmation.index', ['title'=>'Coop Membership Confirmations']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $payment = ClientPayment::findOrFail($id);

        $payments = ClientPayment::where('client_id', $payment->client_id)
            ->ofType('I')
            ->where('amount', '<', 0)
            ->where('id', '!=', $id)
            ->oldest('date')
            ->where('date', $payment->date)
            ->get();

        $previous = MembershipConfirmation::where('client_payment_id', $id)->latest()->get();
        $is_bc_sent = (bool) MembershipConfirmation::where('client_payment_id', $id)->count() > 0;

        return \view(
            'coop.memberships.confirmation.show',
            [
            'title'=>'Coop Membership Confirmations',
            'payment'=>$payment,
            'client'=>$payment->client,
            'payments'=>$payments,
            'previous'=>$previous,
            'is_bc_sent'=>$is_bc_sent
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postShow($id)
    {
        $payment = ClientPayment::findOrFail($id);

        $payments_arr = Request::get('payment_id');

        $payments_arr[] = $id;

        if (!boolval(\Request::get('send'))) {
            return $this->generator->generate($payment, $payments_arr, Auth::user()->id)->stream();
        } else {
            $this->generator->send($payment, $payments_arr);

            \Flash::success('The membership confirmation has been sent.');

            return \Redirect::back();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReplay($id)
    {
        return $this->generator->getBusinessConfirmation($id)->stream();
    }
}
