<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\CampaignStatementItem;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBusinessConfirmation;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharesBusinessConfirmation;
use App\Cytonn\Models\StatementBatch;
use App\Cytonn\Models\StatementJob;
use App\Cytonn\Models\StatementSendRequest;
use Barryvdh\DomPDF\PDF;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Reporting\BusinessConfirmationGenerator;
use Cytonn\Reporting\ClientStatementGenerator;
use Cytonn\Shares\Reporting\BusinessConfirmationGenerator as SharesBusinessConfirmationGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use InvalidArgumentException;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use stdClass;

//use Illuminate\Support\Facades\Request;

/**
 * Date: 9/16/15
 * Time: 9:31 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


/**
 * Class ReportsController
 */
class ReportsController extends Controller
{
    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        parent::__construct();
    }

    /**
     * @return StatementBatch
     */
    public function statements()
    {
        $request = StatementSendRequest::findOrFail(Request::get('id'));

        if ($request->used) {
            return response('Request has already been used', 500);
        }
        if ($request->approved_by == null) {
            return response('Request has not been approved', 500);
        }

        sleep(rand(1, 10));
        usleep(rand(100, 2340));
        time_nanosleep(0, rand(20, 20030));

        //double check to avoid duplication
        $request = StatementSendRequest::findOrFail(Request::get('id'));
        if ($request->used) {
            return response('Request has already been used', 500);
        }

        //record batch
        $batch = new StatementBatch();
        $batch->name = \Request::get('name');
        $batch->sent_by = \Auth::user()->id;
        $batch->request_id = $request->id;
        $batch->save();

        $statement = new ClientStatementGenerator();

        $statement->sendAllStatements($batch);

        $request->useRequest();

        \Queue::push(
            'Cytonn\Reporting\ClientStatementGenerator@fireCheckStatementSendStatus',
            ['batch_id' => $batch->id]
        );

        return $batch;
    }

    public function sendIndividualStatement($clientId)
    {
        $this->authorizer->checkAuthority('sendstatements');
        $client = Client::findOrFail($clientId);

        if ($client->investments()->active()->count() == 0 && $client->portfolioInvestments()->active()->count() == 0) {
            \Flash::error('Could not send statement! This client does not have any active investments.');

            return \Redirect::back();
        }

        $item = CampaignStatementItem::where('client_id', $client->id)->latest()->first();

        if (is_null($item)) {
            \Flash::error('Could not send statement! This client has not been added to a statement campaign');

            return \Redirect::back();
        }

        \Queue::push(
            'Cytonn\Reporting\ClientStatementGenerator@fireIndividualStatement',
            [
                'client_id' => $client->id, 'batch_id' => null,
                'user_id' => \Auth::user()->id, 'item_id' => $item->id
            ]
        );

        \Flash::message('The statement is being sent to the client');

        return \Redirect::back();
    }

    /**
     * @param $productId
     * @param $clientId
     * @return mixed
     */
    public function showClientStatement(Request $request, $productId, $clientId)
    {
        $date = (new Carbon($request->get('date')));
        $date->startOfDay();

        $statement = new ClientStatementGenerator();

        Client::findOrFail($clientId); //just to check it exists
        Product::findOrFail($productId);

        $template = $request->get('template');
        $format = $request->get('format');
        $from = $request->get('from');

        if (empty($template)) {
            $template = null;
        }
        if (empty($from)) {
            $from = null;
        }
        if (empty($format)) {
            $format = null;
        }

        $statement = $statement->generateStatementForClient(
            $productId,
            $clientId,
            null,
            \Auth::user()->id,
            $from,
            $date,
            $template,
            $format
        );

        if ($statement instanceof PDF) {
            return $statement->stream();
        }

        if ($statement instanceof LaravelExcelWriter) {
            return $statement->download('xlsx');
        }

        throw new InvalidArgumentException("Unknown format $format");
    }

    /**
     * @return mixed
     */
    public function batches()
    {
        $batches = StatementBatch::latest()->paginate(10);

        $approval_cut_off = \Carbon\Carbon::now()->subDay()->toDateTimeString();

        $requests = StatementSendRequest::where('used', null)
            ->where('approved_on', '>', $approval_cut_off)->lists('name', 'id');

        return view('investment.statement.batches', [
            'title' => 'Statement batches sent', 'batches' => $batches, 'requests' => $requests
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function showBatchStatus($id)
    {
        $batch = StatementBatch::findOrFail($id);

        return view('investment.statement.status', ['title' => 'Statement status', 'batch' => $batch]);
    }

    /**
     * @param $id
     * @return float
     */
    public function checkBatchStatus($id)
    {
        $batch = StatementBatch::findOrFail($id);

        $complete_jobs = StatementJob::where('complete', 1)->where('batch_id', $id)->count();

        $progress = 0;

        if ($batch->total_jobs != 0) {
            $progress = $complete_jobs / $batch->total_jobs;
        }


        if ($progress == 1) {
            $batch->sent = 1;
            $batch->save();
        }

        return $progress * 100; //return percentage
    }

    /**
     * @param $id
     * @return mixed
     */
    public function businessConfirmationFromInvestment($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $data = new StdClass();
        $data->principal = $investment->amount;
        $data->interest_rate = $investment->interest_rate;
        $data->duration = $investment->repo->getTenorInDays();
        $data->invested_date = $investment->invested_date;
        $data->maturity_date = $investment->maturity_date;
        $data->investment_id = $investment->id;
        $data->on_call = $investment->on_call;
        $data->sender = Auth::user()->id;

        $data = (array)$data;

        $confirmation = new BusinessConfirmationGenerator();

        return $confirmation->generateBusinessConfirmation($investment->client, $data)->stream();
    }

    /**
     * Preview a Shares business confirmation
     *
     * @param  $id
     * @return mixed
     */
    public function sharesBusinessConfirmation($id)
    {
        $holding = ShareHolding::findOrFail($id);

        $confirmation = new SharesBusinessConfirmationGenerator();

        return $confirmation->generate($holding, Auth::user()->id)->stream();
    }

    /**
     * Preview a previous shares business confirmation
     *
     * @param  $id
     * @return mixed
     */
    public function sharesBusinessConfirmationFromData($id)
    {
        $previousConfirmation = SharesBusinessConfirmation::findOrFail($id);

        $holding = ShareHolding::where('id', $previousConfirmation->holding_id)->first();

        $confirmation = new SharesBusinessConfirmationGenerator();

        return $confirmation->generate($holding, Auth::user()->id)->stream();
    }

    /**
     * Retrieve a saved business confirmation
     *
     * @param  $id
     * @return mixed
     */
    public function businessConfirmationFromData($id)
    {
        $previousConfirmation = ClientBusinessConfirmation::findOrFail($id);

        $investment = ClientInvestment::where('id', $previousConfirmation->investment_id)->first();

        $data = new \StdClass();
        $data->principal = $previousConfirmation->payload['principal'];
        $data->interest_rate = $previousConfirmation->payload['interest_rate'];
        $data->duration = $previousConfirmation->payload['duration'];
        $data->invested_date = $previousConfirmation->payload['invested_date'];
        $data->maturity_date = $previousConfirmation->payload['maturity_date'];
        $data->investment_id = $investment->id;

        isset($previousConfirmation->payload['on_call'])
            ? $data->on_call = $previousConfirmation->payload['on_call'] : $data->on_call = false;

        $approval = ClientTransactionApproval::find($investment->approval_id);
        is_null($approval) ? $data->sender = null : $data->sender = $approval->sent_by;

        $data = (array)$data;

        $confirmation = new BusinessConfirmationGenerator();

        return $confirmation->generateBusinessConfirmation($investment->client, $data)->stream();
    }

    /**
     * @return mixed
     */
    public function addStatementSendRequest()
    {
        $this->authorizer->checkAuthority('sendstatements');

        StatementSendRequest::add(Request::all());

        Flash::message('Request Added');

        return Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function approveStatementRequest($id)
    {
        //        $this->authorizer->checkAuthority('approvestatements');

        $request = StatementSendRequest::findOrFail($id);
        $request->approve();
        Flash::success('Request approved');
        return Redirect::back();
    }
}
