<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\SharesEntity;
use Cytonn\Authorization\Authorizer;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Cytonn\Shares\Forms\AddShareEntityForm;
use Laracasts\Validation\FormValidationException;

/**
 * Class SharesEntityController
 */
class SharesEntityController extends Controller
{
    /**
     * @var Authorizer
     */
    protected $authorizer;

    /**
     * @var
     */
    protected $addShareEntityForm;

    public function __construct(AddShareEntityForm $addShareEntityForm)
    {
        $this->authorizer = new Authorizer();
        $this->authorizer->checkAuthority('view_share_entities');
        $this->addShareEntityForm = $addShareEntityForm;
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $entities = SharesEntity::all();

        $fundManagers = FundManager::all()->lists('fullname', 'id');
        $custodialAccounts = CustodialAccount::all()->lists('account_name', 'id');
        $currencies = Currency::all()->lists('code', 'id');

        return view(
            'shares.entities.index',
            [
                'title' => 'Share Entities', 'entities' => $entities, 'fundManagers' => $fundManagers,
                'custodialAccounts' => $custodialAccounts, 'currencies' => $currencies
            ]
        );
    }

    /**
     * @param $entity_id
     * @return mixed
     */
    public function getShow($entity_id)
    {
        $entity = SharesEntity::findOrFail($entity_id);
        return view('shares.entities.show', ['title' => $entity->name, 'entity' => $entity]);
    }

    /**
     * @param $entity_id
     * @return mixed
     */
    public function postIssue($entity_id)
    {
        $input = \Request::except('_token');
        $entity = SharesEntity::findOrFail($entity_id);
        $input['entity_id'] = $entity->id;

        ClientTransactionApproval::add(
            [
                'client_id' => null,
                'transaction_type' => 'make_shares_entity_issue',
                'payload' => $input
            ]
        );

        Flash::message('Share Entity details have been saved for approval');

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function editShareEntity(Request $request, $id)
    {
        $input = $request->except('_token');

        $entity = SharesEntity::findOrFail($id);
        $input['id'] = $entity->id;

        ClientTransactionApproval::add(
            [
                'client_id' => null,
                'transaction_type' => 'edit_share_entity',
                'payload' => $input
            ]
        );

        Flash::message('Request to edit share entity has been saved for approval');

        return Redirect::back();
    }

    public function editSharePrice(Request $request, $id)
    {
        $input = $request->except('_token');

        $entity = SharesEntity::findOrFail($id);
        $input['id'] = $entity->id;

        ClientTransactionApproval::add(
            [
                'client_id' => null,
                'transaction_type' => 'edit_shares_price',
                'payload' => $input
            ]
        );

        Flash::message('Request to edit share price has been saved for approval');

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws FormValidationException
     */
    public function postShareEntity(Request $request)
    {
        $input = $request->except('_token');

        $this->addShareEntityForm->validate($input);

        ClientTransactionApproval::add(
            [
                'client_id' => null,
                'transaction_type' => 'add_share_entity',
                'payload' => $input
            ]
        );

        Flash::message('Share entity has been saved for approval');

        return Redirect::back();
    }

    public function postBuyShares($entity_id)
    {
    }
}
