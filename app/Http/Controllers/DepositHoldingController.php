<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use Cytonn\Portfolio\Bonds\InterestPayments\ScheduleGenerator;
use Cytonn\Portfolio\Forms\EditInvestmentform;
use Illuminate\Http\Request;

class DepositHoldingController extends Controller
{
    /*
     * Get the repositories
     */
    protected $editInvestmentForm;
    protected $authorizer;


    /*
     * Setup the constructor
     */
    public function __construct(EditInvestmentForm $editInvestmentForm)
    {
        parent::__construct();

        $this->editInvestmentForm = $editInvestmentForm;
    }

    /*
     * Supply the view to edit the portfolio investment
     */
    public function getEdit($id)
    {
        $depositHolding = DepositHolding::findOrFail($id);

        $taxes = Setting::all()->map(function ($setting) {
            return [
                'name' => $setting->key . ' ' .$setting->value .'%',
                'id' => $setting->id
            ];
        })->pluck('name', 'id');

        $portfolioinvestmenttypes = PortfolioInvestmentType::pluck('name', 'id');

        $portfolioSecurities = PortfolioSecurity::pluck('name', 'id');

        $custodialaccounts = CustodialAccount::active()->pluck('account_name', 'id');

        return view(
            'portfolio.investment.actions.edit.index',
            [
                'title' => 'Edit Portfolio Investment',
                'deposit' => $depositHolding,
                'portfolioinvestmenttype' => $portfolioinvestmenttypes,
                'custodialaccounts' => $custodialaccounts,
                'portfolioSecurities' => $portfolioSecurities,
                'taxes' => $taxes
            ]
        );
    }

    /*
     * Update the portfolio investment
     */
    public function postEdit(Request $request, $id)
    {
        $this->editInvestmentForm->validate($request->all());

        $deposit = DepositHolding::findOrFail($id);

        if ($deposit->withdrawn) {
            \Flash::message('The investment has been withdrawn, you cannot edit it');

            return \Redirect::back();
        }

        $input = $request->all();

        unset($input['_token']);

        $input['on_call'] = $request->get('on_call') == 'true';

        $input['investment_id'] = $deposit->id;

        $input['institution_id'] = $deposit->security->portfolio_investor_id;

        PortfolioTransactionApproval::add([
            'institution_id' => $deposit->security->portfolio_investor_id,
            'transaction_type' => 'edit_investment',
            'payload'=>$input
        ]);

        \Flash::message('The edited investment has been saved for approval');

        return redirect('/dashboard/portfolio/securities/' . $deposit->portfolio_security_id);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getInterestSchedules($id)
    {
        $holding = DepositHolding::findOrFail($id);

        return view('portfolio.securities.deposits.interest_schedules', [
            'holding' => $holding
        ]);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generateInterestSchedules($id)
    {
        $holding = DepositHolding::findOrFail($id);

        $this->queue(function () use ($id) {
            $holding = DepositHolding::findOrFail($id);

            (new ScheduleGenerator())->regenarateSchedules($holding);
        });

        \Flash::message('Deposit Holding interest payment schedules scheduled for generation successfully');

        return redirect('/dashboard/portfolio/investments/' . $holding->id . '/holdings/interest-schedules');
    }
}
