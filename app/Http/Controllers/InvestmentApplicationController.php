<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Title;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Investment\ClientApplications;
use Cytonn\Investment\ContactPersonAddCommand;
use Cytonn\Investment\Forms\ApplicationEditingForm;
use Cytonn\Investment\Forms\ContactPersonAddForm;
use Cytonn\Investment\Forms\CorporateApplicationForm;
use Cytonn\Investment\Forms\IndividualApplicationForm;
use Cytonn\Investment\Forms\JointHolderForm;
use Cytonn\Investment\Forms\RemoveApplicationForm;
use Cytonn\Investment\JointInvestorAddCommand;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Laracasts\Commander\CommanderTrait;

/**
 * Class InvestmentApplicationController
 */
class InvestmentApplicationController extends Controller
{
    use CommanderTrait, ValidatorTrait;

    /**
     * @var ApplicationsRepository
     */
    protected $repo;
    /**
     * @var IndividualApplicationForm
     */
    protected $individualForm;
    /**
     * @var CorporateApplicationForm
     */
    protected $corporateForm;
    /**
     * @var ApplicationEditingForm
     */
    protected $applicationEditingForm;
    /**
     * @var ContactPersonAddForm
     */
    protected $contactPersonAddForm;
    /**
     * @var RemoveApplicationForm
     */
    protected $removeApplicationForm;
    /**
     * @var JointHolderForm
     */
    protected $jointForm;


    /**
     * Constructor
     *
     * @param \Cytonn\Investment\Forms\ApplicationEditingForm $applicationEditingForm
     * @param \Cytonn\Investment\Forms\IndividualApplicationForm $individualForm
     * @param \Cytonn\Investment\Forms\CorporateApplicationForm $corporateForm
     * @param JointHolderForm $jointHolderForm
     * @param ContactPersonAddForm $contactPersonAddForm
     * @param RemoveApplicationForm $removeApplicationForm
     */
    public function __construct(
        ApplicationEditingForm $applicationEditingForm,
        IndividualApplicationForm $individualForm,
        CorporateApplicationForm $corporateForm,
        JointHolderForm $jointHolderForm,
        ContactPersonAddForm $contactPersonAddForm,
        RemoveApplicationForm $removeApplicationForm
    ) {
        parent::__construct();

        $this->individualForm = $individualForm;
        $this->corporateForm = $corporateForm;
        $this->applicationEditingForm = $applicationEditingForm;
        $this->jointForm = $jointHolderForm;
        $this->contactPersonAddForm = $contactPersonAddForm;
        $this->removeApplicationForm = $removeApplicationForm;
        $this->repo = new ApplicationsRepository();
    }


    /**
     * Show the form for cms application
     *
     * @param  $type
     * @return mixed
     */
    public function apply($type, $id = null)
    {
        $application = new ClientInvestmentApplication();

        $titles = Title::all()->lists('name', 'id');
        $titles[null] = 'Select title';


        $genders = Gender::all()->lists('abbr', 'id');
        $genders[null] = 'Select gender';

        $employment_types = Employment::all()->lists('name', 'id');
        $contact_methods = ContactMethod::all()->lists('description', 'id');
        $investor_types = CorporateInvestorType::all()->lists('name', 'id');
        $sources_of_funds = FundSource::all()->lists('name', 'id');
        $products = Product::active()->get()->lists('name', 'id');

        $countries = Country::all()->lists('name', 'id');

        $banks = [null => 'Select Bank'] +
            ClientBank::orderBy('name', 'asc')->get()->lists('name', 'id');

        return view(
            'investment.applications.' . $type,
            [
                'application' => $application, 'title' => ucfirst($type) . ' Investment Application',
                'client_titles' => $titles, 'genders' => $genders, 'employment_types' => $employment_types,
                'contact_methods' => $contact_methods, 'investor_types' => $investor_types,
                'sources_of_funds' => $sources_of_funds, 'products' => $products,
                'countries' => $countries, 'banks' => $banks
            ]
        );
    }

    /**
     * Save the application
     *
     * @param Request $request
     * @param  $type
     * @param null $id
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     * @throws \Throwable
     */
    public function postApply(Request $request, $type, $id = null)
    {
        $this->authorizer->checkAuthority('editclients');
        $input = Request::except("_token");
        if (isset($input['title'])) {
            $input['title_id'] = $input['title'];
        }

        if (!in_array($type, ['individual', 'corporate'])) {
            throw new ClientInvestmentException('The application type can only be individual or corporate');
        }

        $form = $this->individualForm;
        $client = new ClientRepository();

        if ($type == 'corporate') {
            $form = $this->corporateForm;
        }

        $allow_duplicate = Request::get('new_client') == 2 ? true : false;

        $input['new_client'] = boolval(Request::get('new_client'));
        $complete = boolval(Request::get('complete'));

        if (!$complete) {
            $form->incomplete();
        }
        if (!$input['new_client']) {
            $this->validateExistingClient($input['client_id'], $type);
            $form->applicationDetails();

            DB::transaction(
                function () use ($input, $type) {
                    $input['type_id'] = ClientType::where('name', $type)->first()->id;

                    if ($type == 'individual') {
                        (new ClientApplications())->individualApplication($input);
                    } else {
                        (new ClientApplications())->corporateApplication($input);
                    }
                }
            );
        } else {
            $form->validate(Request::all());

            if (!$allow_duplicate) {
                $duplicate = $type == 'corporate'
                    ? $client->checkIfCorporateClientExists($input) : $client->checkIfIndividualClientExists($input);
                if ($duplicate) {
                    throw new ClientInvestmentException('Another client already exists with these details.');
                }
            }

            DB::transaction(
                function () use ($input, $type) {
                    $contact = $this->repo->saveContactFromApplicationForm($type, $input);

                    $client = $this->repo->saveClientFromApplicationForm($type, $contact, $input);

                    $input['client_id'] = $client->id;

                    $input['type_id'] = ClientType::where('name', $type)->first()->id;

                    if ($type == 'individual') {
                        (new ClientApplications())->individualApplication($input);
                    } else {
                        (new ClientApplications())->corporateApplication($input);
                    }
                }
            );
        }

        return view(
            'investment.actions.investment.investmentapplicationsuccess',
            ['title' => 'Individual Investment Application']
        );
    }


    /**
     * Display the form for editing investments application and show the current information
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function editApplicationDetails($id)
    {
        $this->authorizer->checkAuthority('editclients');

        $application = ClientInvestmentApplication::findOrFail($id);

        $products = Product::all()->lists('name', 'id');

        $fundsources = FundSource::all()->lists('name', 'id');

        $countries = Country::all()->lists('name', 'id');

        $employment_types = Employment::all()->lists('name', 'id');

        $contact_methods = ContactMethod::all()->lists('description', 'id');

        $client_titles = Title::all()->lists('name', 'id');

        $genders = Gender::all()->lists('abbr', 'id');

        if ($application->repo->status() == 'invested') {
            Flash::error('The application has already been invested, you can no longer edit it');

            return view('investment.applications.index', [
                'title' => 'Investment Applications',
                'products' => $products,
                'fundsources' => $fundsources
            ]);
        }

        return view('investment.applications.edit', [
            'title' => 'Edit Application Details',
            'application' => $application,
            'products' => $products,
            'fundsources' => $fundsources,
            'countries' => $countries,
            'employment_types' => $employment_types,
            'contact_methods' => $contact_methods,
            'client_titles' => $client_titles,
            'genders' => $genders
        ]);
    }

    /**
     * Edit client investment application
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function editClientApplication($id)
    {
        $this->authorizer->checkAuthority('editclients');

        $this->applicationEditingForm->validate(Request::all());

        $app = ClientInvestmentApplication::findOrFail($id);

        if ($app->repo->status() == 'invested') {
            \Flash::error('The application has already been invested, you can no longer edit it');

            return Redirect::back();
        }

        $input = request()->except('_token');

        \DB::transaction(
            function () use ($app, $input) {
                $oldApplication = array_only(
                    $app->toArray(),
                    [
                        'product_id',
                        'amount',
                        'tenor',
                        'agreed_rate',
                        'funds_source_id',
                        'funds_source_other'
                    ]
                );

                $oldApplication['application_id'] = $app->id;

                $oldApplication['edited_application'] = 1;

                $app->fill($input);

                $app->save();

                if ($app->client) {
                    $this->syncApplicationtoClientOnEdit($app);
                }


                if ($app->approval) {
                    $app->approval->payload = $oldApplication;

                    $app->approval->save();

                    if ($app->approval->approved) {
                        $app->approval->resend();
                    }
                }
            }
        );

        \Flash::message('The changes have been saved');

        return \redirect('dashboard/investments/client-instructions');
    }

    /**
     * Display joint holders form
     *
     * @param  $id
     * @param  null $jointID
     * @return mixed
     */
    public function addJointApplicant($id, $jointID = null)
    {
        $this->authorizer->checkAuthority('editclients');

        $app = ClientInvestmentApplication::findOrFail($id);
        $titles = Title::all()->mapWithKeys(
            function ($title) {
                return [$title['id'] => $title['name']];
            }
        );

        $gender = Gender::all()->mapWithKeys(
            function ($gender) {
                return [$gender['id'] => $gender['abbr']];
            }
        );

        $countries = Country::all()->mapWithKeys(
            function ($country) {
                return [$country['id'] => $country['name']];
            }
        );

        $contactmethods = ContactMethod::all()->mapWithKeys(
            function ($contactmethod) {
                return [$contactmethod['id'] => $contactmethod['description']];
            }
        );

        is_null($jointID) ? $joint = new ClientJointDetail() : $joint = ClientJointDetail::findOrFail($jointID);

        return view(
            'investment.applications.joint',
            [
                'title' => 'Joint Holders', 'app' => $app, 'joint' => $joint,
                'titles' => $titles, 'gender' => $gender, 'countries' => $countries,
                'contactmethods' => $contactmethods
            ]
        );
    }

    /**
     * Add a joint applicant
     *
     * @param  $id
     * @param  null $jointID
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveJointApplicant($id, $jointID = null)
    {
        $this->authorizer->checkAuthority('editclients');

        $this->jointForm->validate(Request::all());

        $input = array_add(Request::all(), 'application_id', $id);
        $input = array_add($input, 'joint_id', $jointID);

        $this->execute(JointInvestorAddCommand::class, ['data' => $input]);
        \Flash::message('Joint holder successfully added');

        return redirect('/dashboard/investments/applications/' . $id);
    }

    /**
     * Send an application for approval
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function sendApprovalRequest($id)
    {
        $this->authorizer->checkAuthority('editclients');

        $application = ClientInvestmentApplication::findOrFail($id);

        if (is_null($application->approval)) {
            $approval = ClientTransactionApproval::add(
                ['client_id' => $application->client_id, 'transaction_type' => 'application',
                    'payload' => ['application_id' => $application->id], 'scheduled' => 0]
            );

            \Flash::message('The request has been added for approval');

            $application->approval_id = $approval->id;

            $application->save();
        } else {
            \Flash::warning('This application already has a pending approval');
        }

        return Redirect::back();
    }

    /**
     * Add a contact person to a client
     *
     * @param  $app_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function addContactPerson($app_id)
    {
        $this->authorizer->checkAuthority('editclients');

        $input = Request::all();

        $application = ClientInvestmentApplication::findOrFail($app_id);

        $this->contactPersonAddForm->validate($input);

        $input = array_add($input, 'client_id', $application->client_id);

        $this->execute(ContactPersonAddCommand::class, ['data' => $input]);

        \Flash::success('Contact person added');

        return Redirect::back();
    }

    /**
     * Add additional email addresses to a client
     *
     * @param  $app_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function addAdditionalEmails($app_id)
    {
        $this->authorizer->checkAuthority('editclients');

        $app = ClientInvestmentApplication::findOrFail($app_id);

        $client = $app->client;

        $input = explode(
            ',',
            str_replace(' ', '', Request::get('emails'))
        ); //remove spaces and convert comma separated string to array

        foreach ($input as $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                \Flash::error('One or more of the emails in invalid');

                return Redirect::back()->withInput();
            }
        }

        $client->emails = $input;

        $client->save();

        \Flash::success('Emails saved successfully');

        return Redirect::back();
    }

    /**
     * Remove an application and the connected client
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function delete($id)
    {
        $this->authorizer->checkAuthority('editclients');

        $application = ClientInvestmentApplication::findOrFail($id);

        if ($application->repo->status() == 'invested') {
            \Flash::error('This application is already invested, you cannot delete it');

            return \Redirect::back();
        }

        $reason = Request::get('reason');

        $this->removeApplicationForm->validate(Request::all());

        ClientTransactionApproval::add(
            ['transaction_type' => 'delete_application',
            'client_id' => $application->client_id, 'scheduled' => false,
                'payload' => ['application_id' => $application->id, 'reason' => $reason]]
        );

        \Flash::message('Deleting application saved for approval');

        return \redirect('/dashboard/investments');
    }

    /**
     * @param ClientInvestmentApplication $application
     * @return mixed
     */
    private function syncApplicationtoClientOnEdit(ClientInvestmentApplication $application)
    {
        $client = $application->client;

        $input = $application->toArray();

        $client_data = $this->filter($input, Client::getTableColumnsAsArray());

        $client->update($client_data);

        return $client;
    }

    /**
     * @param $clientId
     * @param $type
     * @throws ClientInvestmentException
     */
    private function validateExistingClient($clientId, $type)
    {
        $client = Client::find($clientId);

        if (is_null($client)) {
            throw new ClientInvestmentException('The selected client could not be found');
        }

        if ($client->type->name != $type) {
            throw new ClientInvestmentException("The selected client is not $type");
        }

        if (ClientInvestmentApplication::where('client_id', $clientId)->count() > 0) {
            throw new ClientInvestmentException('There\'s an existing application for the client');
        }
    }
}
