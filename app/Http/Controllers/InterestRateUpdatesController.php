<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Product;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Notifier\FlashNotifier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

/**
 * Class InterestRateUpdatesController
 */
class InterestRateUpdatesController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->authorizer = new Authorizer();
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $this->authorizer->checkAuthority('view_interest_rates');

        $products = Product::all();
        $productslists = Product::all()->mapWithKeys(
            function ($product) {
                return [$product['id'] => $product['name']];
            }
        );

        return view(
            'investment.interest_rate_updates.index',
            ['title'=>'Interest Rates', 'products'=>$products, 'productslists'=>$productslists]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $this->authorizer->checkAuthority('view_interest_rates');
        $update = InterestRateUpdate::findOrFail($id);
        $rates = $update->rates;

        $productslists = Product::all()->mapWithKeys(
            function ($product) {
                return [$product['id'] => $product['name']];
            }
        );

        return view(
            'investment.interest_rate_updates.show',
            ['update'=>$update, 'rates'=>$rates, 'productslists'=>$productslists]
        );
    }

    public function getRates($id)
    {
        $this->authorizer->checkAuthority('view_interest_rates');
        $update = InterestRateUpdate::findOrFail($id);
        $rates = $update->rates;

        return view('investment.interest_rate_updates.interest_rates.show', ['update'=>$update, 'rates'=>$rates]);
    }

    /**
     * @param null $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function postSaveUpdate(Request $request, $id = null)
    {
        $this->authorizer->checkAuthority('view_interest_rates');

        $input = $request->except('_token', 'tenor', 'rate', 'min_amount', 'hidden');
        $input['update_id'] = $id;

        $rates = Collection::make($request->only('tenor', 'rate', 'min_amount', 'hidden'))->transpose()->all();

        $input['rates'] = $rates;

        ClientTransactionApproval::add([
            'client_id'         =>  null,
            'transaction_type'  =>  'save_interest_rate_update',
            'payload'           =>  $input
        ]);

        \Flash::message('The interest rate update has been saved for approval');

        return Redirect::back();
    }

    /**
     * @param $id
     */
    public function postSaveRates($id)
    {
        $this->authorizer->checkAuthority('view_interest_rates');
        $update = InterestRateUpdate::findOrFail($id);

        $rates = Collection::make(\Request::only(['tenor', 'rate']))->transpose()->toArray();

        $input['rates'] = $rates;
        $input['interest_rate_update_id'] = $id;

        ClientTransactionApproval::add(
            [
            'client_id'         =>  null,
            'transaction_type'  =>  'save_interest_rates_for_update',
            'payload'           =>  $input
            ]
        );

        Flash::message('The interest rates have been saved for approval');

        return redirect('/dashboard/investments/interest-rates/show/'.$update->id);
    }
}
