<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/9/18
 * Time: 12:37 PM
 */

namespace App\Http\Controllers;

class CustomReportController extends Controller
{
    public function show($id)
    {
        return view('investment.reports.custom.show');
    }
}
