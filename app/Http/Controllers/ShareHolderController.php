<?php

namespace App\Http\Controllers;

use App\Cytonn\Mailers\Shares\PaymentAcknowledgementMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesStatementCampaign;
use App\Cytonn\Models\Title;
use App\Cytonn\Shares\Reporting\PaymentAcknowledgementGenerator;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Mailers\Shares\StatementMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Shares\Forms\RegisterShareHolderForm;
use Cytonn\Shares\Reporting\StatementGenerator;
use Request;

//use Illuminate\Http\Request;

/**
 * Class ShareHolderController
 */
class ShareHolderController extends Controller
{
    /**
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;

    /**
     * @var $registerShareHolderForm
     */
    protected $registerShareHolderForm;

    /**
     * ShareHolderController constructor.
     *
     * @param RegisterShareHolderForm $registerShareHolderForm
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function __construct(RegisterShareHolderForm $registerShareHolderForm)
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->authorizer->checkAuthority('viewclients');
        $this->registerShareHolderForm = $registerShareHolderForm;

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $shareentities = SharesEntity::all();

        return view(
            'shares.shareholders.index',
            [
                'title' => 'All Shareholders', 'shareentities' => $shareentities
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $share_holder = ShareHolder::findOrFail($id);

        $purchased_shares = $share_holder->boughtShares();

        $sold_shares = $share_holder->soldShares();

        $current_shares = $share_holder->currentShares();

        $open_campaigns_arr = SharesStatementCampaign::where('closed', null)
            ->latest()
            ->get()
            ->lists('name', 'id');

        return view('shares.shareholders.show', [
            'title' => 'Shares - ' . ClientPresenter::presentJointFullNames($share_holder->client_id),
            'share_holder' => $share_holder,
            'open_campaigns_arr' => $open_campaigns_arr,
            'purchased_shares' => AmountPresenter::currency($purchased_shares, true, 0),
            'sold_shares' => AmountPresenter::currency($sold_shares, true, 0),
            'current_shares' => AmountPresenter::currency($current_shares, true, 0)
        ]);
    }

    /**
     * @return mixed
     */
    public function getRegister()
    {
        $signingMandates = (new ClientRepository())->signingMandates();

        return view('clients.application.index', [
            'title' => 'Shareholder Registration Application',
            'type'=> 'register_shareholder',
            'signingMandates' => $signingMandates,
            'edit' => false,
            'applicationId' => null
        ]);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function postDeposit($share_holder_id)
    {
        $holder = ShareHolder::findOrFail($share_holder_id);

        $input = Request::except('_token');
        $input['share_holder_id'] = $holder->id;

        ClientTransactionApproval::add([
            'client_id' => $holder->client_id,
            'transaction_type' => 'deposit_money_for_share_purchase',
            'payload' => $input
        ]);

        \Flash::message('Money deposit for shareholder has been saved for approval');

        return redirect('/dashboard/shares/shareholders/show/' . $holder->id);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function postStatement($share_holder_id)
    {
        $date = new Carbon(\Request::get('date'));
        $generator = new StatementGenerator();
        $holder = ShareHolder::findOrFail($share_holder_id);

        return $generator->generate($holder, $date, \Auth::user())->stream();
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function postMailStatement($share_holder_id)
    {
        $date = new Carbon(\Request::get('date'));
        $holder = ShareHolder::findOrFail($share_holder_id);

        $generator = new StatementGenerator();
        $mailer = new StatementMailer();

        $fname = 'Cytonn Shares - ' . ClientPresenter::presentJointFirstNameLastName($holder->client_id) . '.pdf';

        $statements[$fname] = $generator->generate($holder, $date, \Auth::user())->output();

        $mailer->sendStatementToClient($holder->client, $statements, $date, \Auth::user());

        \Flash::success('The shares statement(s) has been sent to the shareholder');

        return \Redirect::back();
    }

    /*
     * Display the view to redeem the shares
     */
    public function getRedeem($share_holder_id)
    {
        $share_holder = ShareHolder::findOrFail($share_holder_id);

        $date = Carbon::now()->toDateString();

        return view('shares.shareholders.redeem', [
            'share_holder' => $share_holder,
            'date' => $date
        ]);
    }

    /*
     * Submit the redeem shares request for approval
     */
    public function saveRedeemShares($id)
    {
        $input = Request::except('_token');

        $input['date'] = is_null($input['date']) ? Carbon::now()->toDateString() : $input['date'];

        $holder = ShareHolder::findOrFail($input['share_holder_id']);

        ClientTransactionApproval::add([
            'client_id' => $holder->client_id,
            'transaction_type' => 'redeem_shares',
            'payload' => $input
        ]);

        \Flash::message('Redeem shares for client has been saved for approval');

        return redirect('/dashboard/shares/shareholders/show/' . $holder->id);
    }

    /*
     * Delete share holder details...
     */
    public function removeShareHolder($id)
    {
        $input = Request::except('_token');

        $share_holder = ShareHolder::findOrFail($id);

        $input['share_holder_id'] = $id;

        ClientTransactionApproval::add([
            'client_id'         =>  $share_holder->client_id,
            'transaction_type'  =>  'delete_shareholder',
            'payload'           =>  $input
        ]);

        \Flash::message('Share Holder Delete process has been saved for approval');

        return redirect('/dashboard/shares/shareholders');
    }

    public function payDuty($id)
    {
        $input = request()->except('_token');

        $holder = ShareHolder::findOrfail($id);

        $input['share_holder_id'] = $holder->id;

        ClientTransactionApproval::add([
            'client_id'         =>  $holder->client_id,
            'transaction_type'  =>  'share_purchase_custom_duty',
            'payload'           =>  $input
        ]);

        \Flash::message('Custom duty payment has been save for approval');

        return redirect()->back();
    }

    public function acknowledgement($id, $type)
    {
        $purchase = SharePurchases::findOrFail($id);

        if ($type == 'preview') {
            return (new PaymentAcknowledgementGenerator())->generate($purchase)->stream();
        }

        (new PaymentAcknowledgementMailer())->notify($purchase);

        $purchase->sent = true;

        $purchase->save();

        \Flash::success('Shares Payment Purchase Acknowledgement Notification sent.');

        return redirect()->back();
    }
}
