<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientContactPersonRelationship;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\CoopMembershipForm;
use App\Cytonn\Models\CoopPayment;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\SharesCategory;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Title;
use Cytonn\Coop\Clients\CoopClientRules;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\ClientPresenter;
use Flash;
use Illuminate\Http\Request;

/**
 * Class CoopClientsController
 */
class CoopClientsController extends Controller
{

    /**
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;

    /*
     * Get the rules trait
     */
    use CoopClientRules;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->authorizer->checkAuthority('viewclients');
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('coop.clients.index', ['title' => 'Coop Clients']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPayments($id)
    {
        $client = Client::findOrFail($id);
        $payments = CoopPayment::where('client_id', $client->id)->get();

        foreach ($payments as $payment) {
            $payment->date = (new Carbon($payment->date))->format('l jS \\of F Y');
        }

        return view(
            'coop.clients.show',
            ['title' => ClientPresenter::presentFullNames($client->id), 'client' => $client, 'payments' => $payments]
        );
    }

    /**
     * @return mixed
     */
    public function getApply()
    {
        $coop_products = Product::all()->lists('name', 'id');
        $share_categories = SharesCategory::all()->lists('name', 'id');
        $share_entities = SharesEntity::all()->lists('name', 'id');
        $corporate_entities = CorporateInvestorType::all()->lists('name', 'id');
        $client_types = ClientType::all()->lists('name', 'id');
        $titles = Title::all()->lists('name', 'id');
        $employments = Employment::all()->lists('name', 'id');
        $countries = Country::all()->lists('name', 'id');
        $contactMethods = ContactMethod::all()->lists('description', 'id');

        $relationships = ['' => 'Select relationship type'] +
            ClientContactPersonRelationship::all()->lists('relationship', 'id');

        return view(
            'coop.clients.create',
            [
                'title' => 'Cytonn Cooperative Application', 'coop_products' => $coop_products,
                'relationships' => $relationships, 'titles' => $titles, 'employments' => $employments,
                'countries' => $countries, 'share_categories' => $share_categories, 'share_entities' => $share_entities,
                'corporate_entities' => $corporate_entities, 'client_types' => $client_types,
                'contactMethods' => $contactMethods
            ]
        );
    }

    /**
     * @param Request $request
     * @param null $id
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function postStore(Request $request, $id = null)
    {
        $this->authorizer->checkAuthority('editclients');

        if ($request->get('client_type_id') == 1) {
            $validation = ($request->get('complete') == 1)
                ? $this->individualCompleteClientCreate($request) : $this->individualInCompleteClientCreate($request);
        } elseif ($request->get('client_type_id') == 2) {
            $validation = ($request->get('complete') == 1)
                ? $this->corporateCompleteClientCreate($request) : $this->corporateInCompleteClientCreate($request);
        } else {
            $validation = null;
        }

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $client = new Client();

        $duplicate = $request->get('client_type_id') == 2
            ? $client->repo->checkIfCorporateClientExists($request->all())
            : $client->repo->checkIfIndividualClientExists($request->all());

        if ($duplicate && $request->get('client_status') != 2) {
            throw new ClientInvestmentException('Another client already exists with these details.');
        }

        $input = $request->except(
            '_token',
            'client_contact_person_name',
            'id_number',
            'relationship_id',
            'percentage_share',
            'client_status'
        );

        $contact_persons = Collection::make(
            $request->only(['client_contact_person_name', 'id_number', 'relationship_id', 'percentage_share'])
        )->transpose()->toArray();

        $input['contact_persons'] = $contact_persons;

        if (strlen(trim($input['referee_id'])) == 0) {
            $input['referee_id'] = null;
        }

        $product_id = $request->get('product_id');

        if ($product_id) {
            $fm = Product::findOrFail($product_id)->fund_manager_id;
        } else {
            $input['product_id'] = null;
            $fm = $this->fundManager()->id;
        }

        $input['fund_manager_id'] = $fm;


        $form = new CoopMembershipForm();

        foreach ($input['contact_persons'] as $key => $person) {
            if ($person['relationship_id'] == null) {
                unset($input['contact_persons'][$key]);
            }
        }
        $form->fill($input);
        $form->save();

        $client = (new \Cytonn\Clients\Approvals\Handlers\AddCoopClient())->createClient($form);
        $form->client_id = $client->id;
        $form->save();

        $approval = ClientTransactionApproval::add(
            ['transaction_type' => 'add_coop_client', 'client_id' => $client->id,
                'payload' => ['form_id' => $form->id, 'client_id' => $client->id], 'fund_manager_id' => $fm]
        );

        $form->approval_id = $approval->id;
        $form->save();

        Flash::message('Coop client details have been saved for approval');

        return redirect('/dashboard/clients');
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function getEdit($id)
    {
        $client = Client::findOrFail($id);

        $this->authorizer->checkAuthority('editclients');

        $relationships = ClientContactPersonRelationship::all()->lists('relationship', 'id');
        $client_types = ClientType::lists('name', 'id');
        $titles = Title::all()->lists('name', 'id');
        $employments = Employment::all()->lists('name', 'id');
        $countries = Country::all()->lists('name', 'id');

        return view(
            'coop.clients.edit',
            [
                'title' => 'Cytonn Cooperative Application', 'relationships' => $relationships,
                'titles' => $titles, 'employments' => $employments, 'countries' => $countries,
                'client_types' => $client_types, 'client' => $client
            ]
        );
    }
}
