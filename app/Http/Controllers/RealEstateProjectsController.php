<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Floor;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\ProjectType;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstateSizeNumber;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\RealEstateUnitTranchePricing;
use App\Cytonn\Models\Type;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Realestate\Commissions\Calculator;
use Cytonn\Realestate\Forms\LOODetailsForm;
use Cytonn\Realestate\Forms\ProjectForm;
use Cytonn\Realestate\Forms\TrancheForm;
use Cytonn\Realestate\RealestateLandSizes\RealestateLandSizeRepository;
use Cytonn\Realestate\RealestateUnitSizes\RealestateUnitSizeRepository;
use Illuminate\Http\Request;

/**
 * Date: 18/05/2016
 * Time: 5:28 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateProjectsController extends Controller
{
    /**
     * @var ProjectForm
     */
    private $projectForm;

    /**
     * @var LOODetailsForm
     */
    private $detailsForm;

    /*
     * Get the required repositories
     */
    private $realestateLandSizeRepository;
    private $realestateUnitSizeRepository;

    /**
     * RealEstateProjectsController constructor.
     *
     * @param TrancheForm $trancheForm
     * @param ProjectForm $projectForm
     * @param LOODetailsForm $detailsForm
     * @param RealestateLandSizeRepository $realestateLandSizeRepository
     * @param RealestateUnitSizeRepository $realestateUnitSizeRepository
     */
    public function __construct(
        TrancheForm $trancheForm,
        ProjectForm $projectForm,
        LOODetailsForm $detailsForm,
        RealestateLandSizeRepository $realestateLandSizeRepository,
        RealestateUnitSizeRepository $realestateUnitSizeRepository
    ) {
        $this->authorizer = new Authorizer();

        parent::__construct();

        $this->projectForm = $projectForm;

        $this->trancheForm = $trancheForm;

        $this->detailsForm = $detailsForm;

        $this->realestateLandSizeRepository = $realestateLandSizeRepository;

        $this->realestateUnitSizeRepository = $realestateUnitSizeRepository;
    }

    /**
     * Show the projects grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        return view('realestate.projects.index', ['title' => 'Real Estate Projects']);
    }

    /**
     * Display form to create a project
     *
     * @param  null $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function getCreate($id = null)
    {
        is_null($id) ? $project = new Project() : $project = Project::findOrFail($id);

        $this->authorizer->checkAuthority('create_realestate_projects');

        $types = ProjectType::all()->lists('name', 'id');
        $accounts = CustodialAccount::all()->lists('full_name', 'id');
        $currencies = Currency::all()->lists('full_name', 'id');

        $commissionCalculators = (new Calculator())->getCalculatorList();

        return view(
            'realestate.projects.create',
            ['project' => $project, 'title' => 'Create a project', 'types' => $types,
                'commissionCalculators' => $commissionCalculators, 'accounts' => $accounts, 'currencies' => $currencies
            ]
        );
    }

    /**
     * Store a project
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function postStore(Request $request, $id = null)
    {
        is_null($id) ?: Project::findOrFail($id);

        $this->authorizer->checkAuthority('create_realestate_projects');

        $this->projectForm->validate($request->all());

        $input = $request->except('_token');

        if ($id) {
            $input['project_id'] = $id;
        }

        ClientTransactionApproval::make(null, 'create_realestate_project', $input);

        \Flash::message('The project has been saved for approval');

        return redirect('/dashboard/realestate/projects');
    }

    /**
     * Show project details and unit listings
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function getShow($id)
    {
        $project = Project::findOrFail($id);

        $this->authorizer->checkAuthority('create_realestate_projects');

        return view('realestate.projects.show', [
            'title' => 'Real Estate - ' . $project->name,
            'project' => $project
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function getCancelled($id)
    {
        $project = Project::findOrFail($id);

        $this->authorizer->checkAuthority('create_realestate_projects');

        return view('realestate.projects.cancelled', ['title' => 'Real Estate - ' .
            $project->name, 'project' => $project]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSetup($id)
    {
        $project = Project::findOrFail($id);

        $types = ProjectType::all()->lists('name', 'id');

        $sizes = $this->realestateUnitSizeRepository->getUnitSizesNames();
        $floors = $project->projectFloors()->count() == 0 ? null :
            Floor::whereIn('id', $project->projectFloors->lists('floor_id'))
                ->get()->lists('floor', 'id');
        $real_estate_types = $project->projectTypes()->count() == 0 ? null :
            Type::whereIn('id', $project->projectTypes->lists('type_id'))
                ->get()->lists('name', 'id');

        $payment_plans = RealEstatePaymentPlan::all()->lists('name', 'id');

        $available_sizes = RealEstateSizeNumber::where('project_id', $project->id)->get();

        $available_size_arr = $project->availableSizes()->lists('name', 'id');

        $type = $project->type;

        $tranches = $project->tranches()->latest('date')->get();

        $commissionRates = $project->commissionRates;

        $recipient_types = CommissionRecipientType::all()->lists('name', 'id');

        $unitsize = RealestateUnitSize::all();

        return view(
            'realestate.projects.setup',
            [
                'title' => 'Setup project', 'project' => $project, 'projectTypes' => $types,
                'type' => $type, 'sizes' => $sizes, 'available_sizes' => $available_sizes,
                'payment_plans' => $payment_plans, 'recipient_types' => $recipient_types,
                'available_size_arr' => $available_size_arr, 'commission_rates' => $commissionRates,
                'tranches' => $tranches, 'unitsize' => $unitsize, 'floors' => $floors,
                'real_estate_types' => $real_estate_types
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postSetupUnitNumbers($id)
    {
        Project::findOrFail($id);

        $input = \Request::except('_token');

        $input['project_id'] = $id;

        ClientTransactionApproval::make(null, 'set_project_unit_numbers', $input);

        \Flash::message('The unit numbers have been saved for approval');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @param $tranche_id
     * @return mixed
     */
    public function postTranche($id, $tranche_id = null)
    {
        $input = \Request::only('name', 'date');

        if ($tranche_id) {
            $input['tranche_id'] = $tranche_id;
        }

        if ($tranche_id) {
            $sizes = is_null(request('floor_id')) ?
                Collection::make(\Request::only('ident', 'size_id', 'number', 'sizing_id'))->transpose() :
                Collection::make(\Request::only('ident', 'size_id', 'number', 'sizing_id', 'floor_id', 'type_id'))
                    ->transpose();

            $prices = Collection::make(\Request::only('price', 'value', 'payment_plan_id', 'price_id'))->transpose();
        } else {
            $sizes = is_null(request('floor_id')) ?
                Collection::make(\Request::only('ident', 'size_id', 'number'))->transpose() :
                Collection::make(\Request::only('ident', 'size_id', 'number', 'floor_id', 'type_id'))->transpose();

            $prices = Collection::make(\Request::only('price', 'value', 'payment_plan_id'))->transpose();
        }

        $prices = $prices->map(
            function ($p) {
                return Collection::make($p)->transpose()->all();
            }
        );

        $input['tranches'] = $sizes->map(
            function ($size) use ($prices) {
                $size['prices'] = $prices[$size['ident']];
                unset($size['ident']);

                return $size;
            }
        )->toArray();

        if (!$this->validateTranche($input['tranches'])) {
            return \Redirect::back()->withInput();
        }

        Project::findOrFail($id);

        $input['project_id'] = $id;

        ClientTransactionApproval::make(null, 'real_estate_project_tranche', $input);

        \Flash::message('The tranche has been saved for approval');

        return \Redirect::back();
    }

    protected function validateTranche(array $tranche)
    {
        //sizes should be unique
        $sizes = Collection::make($tranche)->map(function ($size) {
            $f = $t = "";

            if (isset($size['floor_id'])) {
                $f = "f" . $size['floor_id'];
            }

            if (isset($size['type_id'])) {
                $t = "t" . $size['type_id'];
            }

            $size['unique'] = "s" . $size['size_id'] . $f . $t;

            return $size;
        })->pluck('unique');
        ;

        $sizes_valid = $sizes->count() == $sizes->unique()->count();

        if (!$sizes_valid) {
            \Flash::error('There are duplicates in the tranche sizes');
        }

        //in each size, payment plans should be unique
        $prices = Collection::make($tranche)->pluck('prices');

        $plans_valid = true;

        foreach ($prices as $price) {
            $plans = Collection::make($price)->pluck('payment_plan_id');

            $plans_valid = $plans_valid && ($plans->count() == $plans->unique()->count());
        }

        if (!$plans_valid) {
            \Flash::error('There are duplicates in the payment plans');
        }

        return $plans_valid && $sizes_valid;
    }

    /**
     * @param $tranche_id
     * @return mixed
     */
    public function getTranchePrices($tranche_id)
    {
        $tranche = RealEstateUnitTranche::findOrFail($tranche_id);

        return $tranche->prices;
    }

    public function getTranchePricing($tranche_id)
    {
        $pricing = RealEstateUnitTranchePricing::where('tranche_id', $tranche_id)
            ->with('paymentPlan')->get();

        return $pricing;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function putTranche($id)
    {
        $input = \Request::only('tranche_id', 'name', 'date');

        $input['project_id'] = RealEstateUnitTranche::findOrFail($id)->project_id;

        $tranches = \Request::only('id', 'size_id', 'payment_plan_id', 'price', 'number');

        dd("The data does not get here, Ronnie to correct");

        dd($tranches);
        $tranches = Collection::make($tranches)->transpose();
        $input['tranches'] = $tranches->toArray();

        $unique = $tranches->map(
            function ($tranche) use ($input) {
                $this->trancheForm->validate($input + $tranche);

                $tranche['unique'] = 'size' . $tranche['size_id'] . 'plan' . $tranche['payment_plan_id'];
                return $tranche;
            }
        )->unique('unique');

        if ($unique->count() != $tranches->count()) {
            \Flash::error('The tranches should be unique in combination of payment plans and size');

            return \Redirect::back();
        }

        ClientTransactionApproval::make(null, 'real_estate_project_tranche', $input);

        \Flash::message('The tranche has been updated for approval');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSetupLooDetails($id)
    {
        $project = Project::findOrFail($id);
        return \view(
            'realestate.projects.loo_details',
            ['title' => 'Edit LOO Details for Project', 'project' => $project]
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function putUpdateLOODetails($id)
    {
        $input = \Request::except(['_token', '_method', 'files']);
        $this->detailsForm->validate($input);
        $input['project_id'] = $id;
        Project::findOrFail($id);

        ClientTransactionApproval::make(null, 'update_project_loo_details', $input);
        \Flash::message('The LOO details for the project have been saved for approval.');
        return \redirect('/dashboard/realestate/projects/setup/' . $id);
    }

    /*
     * Add or update the real estate unit groups for a given
     */
    public function postSetupUnitGroups(Request $request, $id = null)
    {
        $input = $request->except(['_token']);

        $project = Project::findOrFail($input['project_id']);

        ClientTransactionApproval::make(null, 'setup_project_unit_group', $input);

        \Flash::message('The unit group details have been saved for approval.');

        return redirect('/dashboard/realestate/projects/setup/' . $project->id);
    }
}
