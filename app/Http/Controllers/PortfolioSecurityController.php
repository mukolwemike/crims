<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundType;
use App\Cytonn\Models\PortfolioInvestor;
use Illuminate\Http\Request;

class PortfolioSecurityController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $fund_types = FundType::all()->lists('name');

        $institutions = PortfolioInvestor::all()->lists('name', 'id');

        $currencies = Currency::all();

        return view('portfolio.securities.index', [
            'title' => 'Portfolio Securities',
            'fund_types' => $fund_types,
            'institutions' => $institutions,
            'currencies' => $currencies
        ]);
    }

    public function show($id)
    {
        return view('portfolio.securities.show');
    }

    public function showHoldings()
    {
        return view('portfolio.securities.deposit_holdings');
    }
}
