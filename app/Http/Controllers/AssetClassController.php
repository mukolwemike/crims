<?php

namespace App\Http\Controllers;

class AssetClassController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('portfolio.securities.assetclass');
    }

    public function show()
    {
        return view('portfolio.securities.assetclass');
    }
}
