<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\User;
use Cytonn\Forms\CreatePassWordForm;
use Cytonn\Forms\RegisterUserForm;
use Cytonn\Notifier\FlashNotifier as Flash;
use Illuminate\Support\Facades\Request;

/**
 * Class RegistrationController
 */
class RegistrationController extends Controller
{
    /**
     * @var RegisterUserForm
     */
    private $registrationForm;

    /**
     * @var CreatePassWordForm
     */
    private $passwordForm;

    /**
     * RegistrationController constructor.
     *
     * @param $registrationForm
     */
    public function __construct(RegisterUserForm $registrationForm, CreatePassWordForm $passWordForm)
    {
        $this->registrationForm = $registrationForm;

        $this->passwordForm = $passWordForm;
    }

    /**
     * Activate a new account by creating a new password for the account
     *
     * @param  $username
     * @param  $key
     * @return string
     */
    public function activate($username, $key)
    {
        $user = User::where('username', $username)->first();

        return view('registration.create_password');

        if ($user) {
            if (is_null($user->activation_key) && $user->active == 1) {
                Flash::error('Account is already activated, you can now login');

                return redirect('/');
            }
            if ($user->activation_key == $key) {
                if (!$user->authRepository->checkActivationKeyExpiry()) {
                    Flash::error('Activation Key has expired, please request a new one');

                    return redirect('/');
                }

                Flash::success('Account activated succesfully. Now create a new password for your account');

                return view('registration.create_password');
            } else {
                Flash::error('Could not activate that user, check that the activation link is correct.');

                return redirect('/');
            }
        } else {
            Flash::error(
                'Could not activate account, check that the activation link is correct or whether account is
                 already activated.'
            );

            return redirect('/');
        }
    }

    /**
     * Create a password for registered user after account is validated
     *
     * @param  $username
     * @param  $key
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function createPassword($username, $key)
    {
        $this->passwordForm->validate(Request::all());

        $user = User::where('username', $username)->first();

        if ($user->activation_key = $key) {
            if (!$user->authRepository->checkActivationKeyExpiry()) {
                Flash::error('Activation Key has expired, please request a new one');

                return redirect('/');
            }

            $user->activation_key = null;
            $user->active = true;

            $user->password = Request::get('password');

            $user->save();


            Flash::success('Password created succesfully. You can now login');

            return redirect('/login');
        } else {
            Flash::error('Could not activate that user, check that the activation link is correct.');

            return redirect('/');
        }
    }
}
