<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\CampaignStatementItem;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\StatementCampaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Date: 09/03/2016
 * Time: 12:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientStatementController extends Controller
{
    /**
     * ClientStatementController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();

        parent::__construct();
    }

    /**
     * Create a campaign to be used to send statements
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Throwable
     */
    public function createCampaign($id = null)
    {
        $this->authorizer->checkAuthority('approvestatements');

        is_null($id) ? $campaign = new StatementCampaign() : $campaign = StatementCampaign::findOrFail($id);

        $campaign->fill(\Request::only('name', 'send_date', 'type_id'));

        $campaign->sender_id = Auth::user()->id;

        $campaign->save();

        if ($campaign->type->slug == 'automated') {
            \Queue::push(
                'Cytonn\Reporting\StatementCampaignRepository@fireAutomaticallyAddClients',
                ['campaign_id'=>$campaign->id]
            );
        }

        \Flash::message('Campaign has been saved, sending can now start');

        return \Redirect::back();
    }


    /**
     * Show the campaign
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function showCampaign($id)
    {
        $this->authorizer->checkAuthority('sendstatements');

        $campaign = StatementCampaign::findOrFail($id);

        $sent_count = $campaign->repo->countSentItems();
        $total_count = $campaign->repo->countAllItems();

        return \view(
            'investment.statement.campaign_detail',
            [
                'campaign'=>$campaign,
                'title'=>'Client Statement Campaigns',
                'sent_count'=>$sent_count,
                'total_count'=>$total_count
            ]
        );
    }

    /**
     * Add a client to a campaign so that you can send statement
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function addClientToCampaign()
    {
        $this->authorizer->checkAuthority('sendstatements');

        $campaign = StatementCampaign::findOrFail(\Request::get('campaign'));

        $client = Client::findOrFail(\Request::get('client'));

        if ($campaign->repo->checkIfClientIsInCampaign($client)) {
            \Flash::warning('The client has already been added to the campaign');

            return \Redirect::back();
        }

        $campaign->repo->addClientToCampaign($client, \Auth::user());

        \Flash::message('Client has been added to campaign');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function removeClientFromCampaign($id)
    {
        $item = CampaignStatementItem::findOrFail($id);

        if ($item->campaign->closed) {
            \Flash::message('The campaign is already closed');

            return \Redirect::back();
        }

        $item->delete();

        \Flash::message('Client has removed from campaign');

        return \Redirect::back();
    }

    /**
     * @param $campaignId
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function sendCampaign($campaignId)
    {
        $this->authorizer->checkAuthority('sendstatements');

        $campaign = StatementCampaign::findOrFail($campaignId);

        if ($campaign->sender_id == \Auth::user()->id) {
            throw new \Cytonn\Exceptions\ClientInvestmentException(
                'The sender cannot be the person who created the campaign'
            );
        }

        $campaign->repo->sendCampaign();

        if ($campaign->type->slug == 'automated') {
            $campaign->close();
        }

        \Flash::message('The statements are being sent to the clients');

        return \Redirect::back();
    }

    /**
     * @param $campaignId
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function closeCampaign($campaignId)
    {
        $this->authorizer->checkAuthority('approvestatements');

        $campaign = StatementCampaign::findOrFail($campaignId);

        if (!$campaign->closed) {
            $campaign->closed = true;
            $campaign->save();
        }

        \Flash::message('Campaign has been closed, clients can no longer be added');

        return \Redirect::back();
    }

    /*
     * Add or update a campaign message
     */
    public function storeCampaignMessage(Request $request, $campaignId)
    {
        $campaign = StatementCampaign::findOrFail($campaignId);

        $campaign->statement_message = $request->get('statement_message');

        $campaign->save();

        \Flash::message('Campaign has been saved successfully');

        return \Redirect::back();
    }
}
