<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/6/18
 * Time: 11:34 AM
 */

namespace App\Http\Controllers;

class PortfolioComplianceController extends Controller
{
    public function index()
    {
        return view('portfolio.compliance.index');
    }

    public function show($id)
    {
        return view('portfolio.compliance.view');
    }
}
