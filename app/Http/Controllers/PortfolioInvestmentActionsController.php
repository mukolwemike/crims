<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundType;
use \App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Forms\EditInvestmentform;
use Illuminate\Support\Facades\Request;

/**
 * Date: 16/03/2016
 * Time: 11:08 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class PortfolioInvestmentActionsController extends Controller
{

    //TODO merge with action controller
    /**
     * PortfolioInvestmentActionsController constructor.
     */
    public function __construct(EditInvestmentForm $editInvestmentForm)
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->editInvestmentForm = $editInvestmentForm;
    }

    /**
     * Show form to edit the investment
     *
     * @param  $id
     * @return mixed
     */
    public function getEdit($id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $investment = \App\Cytonn\Models\Portfolio\DepositHolding::findOrFail($id);

        $portfolioinvestors = PortfolioInvestor::all()->lists('name', 'id');

        $fundtypes = FundType::all()->lists('name', 'id');

        $portfolioinvestmenttype = PortfolioInvestmentType::get()->lists('name', 'id');

        $custodialaccounts = CustodialAccount::active()->get()->lists('full_name', 'id');

        return view(
            'portfolio.investment.actions.edit.index',
            [
                'investment'=>$investment, 'portfolioinvestors'=>$portfolioinvestors,
                'fundtypes'=>$fundtypes, 'portfolioinvestmenttype'=>$portfolioinvestmenttype,
                'custodialaccounts'=>$custodialaccounts
            ]
        );
    }

    /**
     * Save edited investment for approval
     *
     * @param  $id
     * @return mixed
     */
    public function postEdit($id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->editInvestmentForm->validate(\Request::all());

        $investment = \App\Cytonn\Models\Portfolio\DepositHolding::findOrFail($id);

        if ($investment->withdrawn) {
            \Flash::message('The investment has been withdrawn, you cannot edit it');

            return \Redirect::back();
        }
        $input = Request::all();

        unset($input['_token']);
        $input['on_call'] = Request::get('on_call') == 'true';
        $input['investment_id'] = $investment->id;

        PortfolioTransactionApproval::add(['institution_id'=>$investment->portfolio_investor_id,
            'transaction_type'=>'edit_investment', 'payload'=>$input]);

        \Flash::message('The edited investment has been saved for approval');

        return redirect('/dashboard/portfolio');
    }
}
