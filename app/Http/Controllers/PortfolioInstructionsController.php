<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\PortfolioInstruction;
use App\Cytonn\Models\User;
use Barryvdh\DomPDF\Facade;

/**
 * Class PortfolioInstructionsController
 */
class PortfolioInstructionsController extends Controller
{

    /**
     * @var $authorizer
     */
    protected $authorizer;

    public function __construct()
    {
        parent::__construct();
        $this->authorizer->checkAuthority('viewportfolio');
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $custodialaccounts = CustodialAccount::all();

        return view('portfolio.instructions.index', [
                'title'=>'Portfolio Instructions',
                'custodialaccounts'=>$custodialaccounts
            ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $instruction = PortfolioInstruction::findOrFail($id);
        $users = User::all()->lists('full_name', 'id');

        return view('portfolio.instructions.show', [
            'title'=>'Portfolio Instruction Details',
            'instruction'=>$instruction,
            'users'=>$users
        ]);
    }

    public function postGeneratePDF($id)
    {
        $instruction = PortfolioInstruction::findOrFail($id);

        $first_signatory = User::findOrFail(\Request::get('first_signatory_id'));

        $second_signatory = User::findOrFail(\Request::get('second_signatory_id'));

        return \PDF::loadView('portfolio.instructions.pdf.index', [
            'instruction'=>$instruction,
            'first_signatory'=>$first_signatory,
            'second_signatory'=>$second_signatory
        ])->stream();
    }
}
