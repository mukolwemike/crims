<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Cytonn\Investment\BankDetails;

/**
 * Class PaymentInstructionsController
 */
class PaymentInstructionsController extends Controller
{

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $projects = Project::all();
        $funds = UnitFund::ofType('cis')->active()->get();

        return view(
            'investment.payments.instructions.index',
            ['title' => 'Payments Instructions', 'projects' => $projects, 'funds'=>$funds]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $instruction = BankInstruction::findOrFail($id);
        $transaction = $instruction->custodialTransaction;
        $payment = $transaction->clientPayment;
        $client = $payment->client;
        $users = User::all()->lists('fullname', 'id');
        $account = new BankDetails(null, $client, $instruction->clientBankAccount);

        return view(
            'investment.payments.instructions.show',
            [
                'title' => 'Payment Instruction : ' . \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id),
                'client' => $client,
                'instruction' => $instruction,
                'transaction' => $transaction,
                'payment' => $payment,
                'users' => $users,
                'account' => $account
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postUpdate($id)
    {
        $instruction = BankInstruction::findOrFail($id);
        $client = $instruction->custodialTransaction->clientPayment->client;
        $first_signatory_id = \Request::get('first_signatory_id');
        $second_signatory_id = \Request::get('second_signatory_id');

        // Confirming that they are valid signatories
        $first_signatory = User::findOrFail($first_signatory_id);
        $second_signatory = User::findOrFail($second_signatory_id);

        $input['first_signatory_id'] = $first_signatory->id;
        $input['second_signatory_id'] = $second_signatory->id;
        $input['instruction_id'] = $instruction->id;

        ClientTransactionApproval::make($client->id, 'update_payment_instruction', $input);

        \Flash::message('The request to update the payment instruction been saved for approval.');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postReverse($id)
    {
        $instruction = BankInstruction::findOrFail($id);
        $client = $instruction->custodialTransaction->clientPayment->client;

        $input['instruction_id'] = $instruction->id;

        ClientTransactionApproval::make($client->id, 'reverse_payment_instruction', $input);

        \Flash::message('The request to reverse the payment instruction been saved for approval.');

        return \Redirect::back();
    }
}
