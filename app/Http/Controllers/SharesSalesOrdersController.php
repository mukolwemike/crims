<?php

namespace App\Http\Controllers;

use App\Cytonn\Mailers\Shares\UnmatchedSalesOrdersMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Api\Transformers\ShareHoldingsTransformer;
use Cytonn\Api\Transformers\SharesSalesOrderTransformer;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\ClientPresenter;
use Flash;
use Illuminate\Support\Facades\Redirect;

/**
 * Class SharesSalesOrdersController
 */
class SharesSalesOrdersController extends Controller
{

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('shares.sales.index', ['title' => 'Share Sales Orders']);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function getShareholders($share_holder_id)
    {
        $holder = ShareHolder::findOrFail($share_holder_id);

        $current_shares = $holder->currentShares();

        return view(
            'shares.sales.shareholder',
            [
                'title' => ClientPresenter::presentFullNames($holder->client_id) . ' - Shares Sales Orders',
                'holder' => $holder, 'current_shares' => $current_shares
            ]
        );
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function getShow($order_id)
    {
        $order = SharesSalesOrder::findOrFail($order_id);

        $sold = $order->numberSold();

        $seller = $order->seller;

        $purchases = SharesPurchaseOrder::matched(false)
//            ->where('price', '>=', $order->price)
            ->where('buyer_id', '!=', $seller->id)
            ->forEntity($seller->entity)
            ->orderBy('request_date', 'ASC')
            ->orderBy('price', 'ASC')
            ->get();

        $pendingPurchases = $order->pendingSharePurchases()->map(function ($purchase) {
            return (new ShareHoldingsTransformer())->details($purchase);
        });

        return view(
            'shares.sales.show',
            [
                'title' => 'Shares Sales Order #' . $order_id,
                'seller' => $seller, 'order' => $order,
                'purchases' => $purchases,
                'sold' => $sold,
                'pendingPurchases' => $pendingPurchases,
            ]
        );
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function postMatch($order_id)
    {
        $order = SharesSalesOrder::findOrFail($order_id);

        $input['purchase_order_ids'] = \Request::get('purchase_order_id');
        $input['date'] = \Request::get('date');
        $input['bargain_price'] = \Request::exists('bargain_price') ? \Request::get('bargain_price') : null;
        $input['sales_order_id'] = $order->id;

        ClientTransactionApproval::add([
            'client_id' => $order->seller->client_id,
            'transaction_type' => 'match_sale_to_purchase',
            'payload' => $input
        ]);

        Flash::message('Match for shares sales order has been saved for approval');

        return redirect('/dashboard/shares/sales/shareholders/' . $order->seller_id);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function postOrder($share_holder_id)
    {
        $seller = ShareHolder::findOrFail($share_holder_id);

        $input = \Request::except('_token');

        $input['good_till_filled_cancelled'] = (bool)isset($input['good_till_filled_cancelled']);

        if ($input['good_till_filled_cancelled']) {
            $input['expiry_date'] = null;
        }

        if ($input['number'] > $seller->currentShares($input['request_date'])) {
            throw new ClientInvestmentException(
                'The number of shares to be sold cannot be more than those owned.'
            );
        }

        ClientTransactionApproval::add(
            [
                'client_id' => $seller->client_id,
                'transaction_type' => 'make_shares_sales_order',
                'payload' => $input
            ]
        );

        Flash::message('Shares sales order has been saved for approval');

        return redirect('/dashboard/shares/shareholders/show/' . $seller->id);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function postCancel($order_id)
    {
        $order = SharesSalesOrder::findOrFail($order_id);
        $input['order_id'] = $order->id;

        ClientTransactionApproval::add([
            'client_id' => $order->seller->client_id,
            'transaction_type' => 'cancel_shares_sale_order',
            'payload' => $input
        ]);

        Flash::message('The request to cancel the shares sale order has been sent for approval');

        return Redirect::back();
    }

    public function notifyUnmatched()
    {
        $clients = Client::whereHas('shareHolders', function ($holder) {
            $holder->whereHas('shareSalesOrders', function ($order) {
                $order->matched(false)->cancelled(false);
            });
        })->get();

        foreach ($clients as $client) {
            $orders = $this->orders($client);

            dispatch((new UnmatchedSalesOrdersMailer())->sendEmail($orders, $client));
        }

        Flash::success('Unmatched sale orders notification successfully sent');

        return Redirect::back();
    }

    public function orders(Client $client)
    {
        return SharesSalesOrder::matched(false)->cancelled(false)
            ->whereHas('seller', function ($seller) use ($client) {
                $seller->where('client_id', $client->id);
            })
            ->get()->map(function ($order) {
                return (new SharesSalesOrderTransformer())->transform($order);
            });
    }
}
