<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealestateCommissionRate;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Realestate\Forms\RealEstatePaymentForm;
use Cytonn\Realestate\Payments\Payment;
use Cytonn\Realestate\Tranches\RealEstateUnitTrancheRepository;

/**
 * Date: 06/06/2016
 * Time: 12:29 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstatePaymentsController extends Controller
{
    protected $paymentForm;

    public function __construct(RealEstatePaymentForm $paymentForm)
    {
        $this->paymentForm = $paymentForm;

        parent::__construct();
    }

    public function getIndex()
    {
        return view('realestate.payments.index', ['title' => 'Real Estate Payments']);
    }

    public function getShow($id)
    {
        //TODO add showing previous business confirmations
        $payment = RealEstatePayment::findOrFail($id);

        $holding = $payment->holding;

        $payments = $holding->payments()->latest('date')->get();
        $inPricingPayments = $holding->payments()->inPricingPayments()->sum('amount');

        //convert to custom collection to make use of additional methods
        $payments = Collection::makeFromCollection($payments);

        $paymentTypes = RealEstatePaymentType::all();

        $bc_status = (new Payment($payment))->checkBCStatus();

        $schedules = RealEstatePaymentSchedule::where('unit_holding_id', $holding->id)
            ->get()->lists('description', 'id');

        $unit = $holding->unit;

        $project = $unit->project;

        $realestatePaymentType = RealEstatePaymentType::all()->lists('name', 'id');

        return view(
            'realestate.payments.show',
            [
                'title' => 'Real Estate Payment', 'payment' => $payment, 'holding' => $holding, 'payments' => $payments,
                'unit' => $unit, 'project' => $project, 'payment_types' => $paymentTypes, 'bc_status' => $bc_status,
                'schedules' => $schedules, 'realestatePaymentType' => $realestatePaymentType,
                'inPricingPayments' => $inPricingPayments
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function putUpdate($id)
    {
        $payment = RealEstatePayment::findOrFail($id);
        $holding = $payment->holding;
        $input = \Request::except(['_token', '_method']);

        if (isset($input['date'])) {
            $input['date'] = (new Carbon($input['date']))->toDateString();
        }

        $input['payment_id'] = $id;

        ClientTransactionApproval::add(
            [
                'client_id' => $holding->client_id,
                'transaction_type' => 'update_realestate_payment',
                'payload' => $input
            ]
        );

        \Flash::message('The payment update request has been saved for approval');

        return \redirect(
            '/dashboard/realestate/projects/' . $holding->unit->project_id . '/units/show/' . $holding->unit_id
        );
    }

    public function getCreate($holdingID, $type_slug = null)
    {
        $holding = UnitHolding::findOrFail($holdingID);

        $schedule = $holding->payments()->count() == 0;

        $schedules = RealEstatePaymentSchedule::where('unit_holding_id', $holdingID)
            ->get()->lists('description', 'id');

        if (is_null($schedule)) {
            throw new ClientInvestmentException('The payment type could not be found');
        }

        $recipients = ['' => 'Select FA'] + (new CommissionRecepient())->repo->getRecipientsForSelect();

        $currentRecipient = $holding->client->getLatestFA('real_estate');

        $currentRecipient = ($currentRecipient) ? $currentRecipient->id : null;

        $commission_rates = RealestateCommissionRate::where('project_id', $holding->project->id)
            ->orWhereNull('project_id')->get();

        $payments_balance = (new ClientPayment())
            ->balance($holding->client, null, $holding->project);

        return \view(
            'realestate.payments.create',
            ['holding' => $holding, 'schedule' => $schedule, 'schedules' => $schedules,
                'unit' => $holding->unit, 'title' => 'Make a payment',
                'recipients' => $recipients, 'commission_rates' => $commission_rates,
                'payments_balance' => $payments_balance, 'currentRecipient' => $currentRecipient
            ]
        );
    }

    public function postCreate($holdingId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        $input = \Request::except("_token");

        $input['date'] = (new Carbon($input['date']))->toDateString();

        $input['holding_id'] = $holding->id;

        //clean up missing keys
        foreach ($input as $key => $val) {
            if (is_null($val) || strlen($val) == 0) {
                unset($input[$key]);
            }
        }

        if (!(bool)$holding->payments()->first()) { //no previous payment
            $this->paymentForm->firstPayment();

            //tranche check
            $tranche = RealEstateUnitTranche::findOrFail($input['tranche_id']);

            $trancheRepo = new RealEstateUnitTrancheRepository($tranche);

            if ($trancheRepo->trancheCheck($holding)) {
                return \Redirect::back();
            };
        }

        $this->paymentForm->validate($input);

        $approval = ClientTransactionApproval::add([
            'client_id' => $holding->client_id,
            'transaction_type' => 'make_real_estate_payment',
            'payload' => $input
        ]);

        \Flash::message('The payment has been saved for approval');

        return redirect()->to('/dashboard/investments/client-instructions/' . $approval->id . '/approval-document');

//        return \redirect(
//            '/dashboard/realestate/projects/'.$holding->unit->project_id.'/units/show/'.$holding->unit_id
//        );
    }

    public function addPaymentType($holdingId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        $input = \Request::except("_token");

        if (!isset($input['name'])) {
            \Flash::error('Please fill the form to save payment type');

            return \Redirect::back();
        }

        ClientTransactionApproval::add(
            [
                'client_id' => $holding->client_id,
                'transaction_type' => 'add_real_estate_payment_type',
                'payload' => $input
            ]
        );

        \Flash::message('The payment type has been saved for approval');

        return \Redirect::back();
    }

    public function deletePaymentType($holdingId, $typeId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        $input = ['id' => $typeId];

        ClientTransactionApproval::make(
            $holding->client_id,
            'remove_real_estate_payment_type',
            $input
        );

        \Flash::message('The payment type delete request has been saved for approval');

        return \Redirect::back();
    }

    public function updatePaymentType($holdingId, $typeId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        $input = \Request::except("_token", "_method");

        if (!isset($input['name'])) {
            \Flash::error('Please fill the form to save payment type');

            return \Redirect::back();
        }

        $input['id'] = $typeId;

        ClientTransactionApproval::add(
            [
                'client_id' => $holding->client_id,
                'transaction_type' => 'add_real_estate_payment_type',
                'payload' => $input
            ]
        );

        \Flash::message('The payment type edit request has been saved for approval');

        return \Redirect::back();
    }

    public function putUpdatePaymentInformation($holdingId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        $input = \Request::except("_token", "_method");

        $input['holding_id'] = $holding->id;

        ClientTransactionApproval::add(
            [
                'client_id' => $holding->client_id,
                'transaction_type' => 'update_real_estate_pricing_details',
                'payload' => $input
            ]
        );

        \Flash::message('The payment information has been saved for approval');

        return \redirect(
            '/dashboard/realestate/projects/' . $holding->unit->project_id . '/units/show/' . $holding->unit_id
        );
    }

    public function getPreviewBc($id)
    {
        $payment = RealEstatePayment::findOrFail($id);

        $handler = new Payment($payment);

        return $handler->generateBusinessConfirmation()->stream();
    }

    public function getDelete($id)
    {
        $payment = RealEstatePayment::findOrFail($id);

        ClientTransactionApproval::add(
            [
                'client_id' => $payment->holding->client_id,
                'transaction_type' => 'delete_real_estate_payment',
                'payload' => ['payment_id' => $id]
            ]
        );

        \Flash::message('The payment will be deleted after approval');

        return \Redirect::back();
    }

    public function getSendBc($id)
    {
        $payment = RealEstatePayment::findOrFail($id);

        $handler = new Payment($payment);

        $handler->sendBusinessConfirmation();

        \Flash::message('The business confirmation has been sent');

        return \Redirect::back();
    }

    public function refund($id)
    {
        $holding = UnitHolding::findOrFail($id);

        $input = request()->except('_token');

        $input['holding_id'] = $holding->id;

        $balance = $holding->totalPayments() - $holding->refundedAmount();

        if ($input['amount'] > $balance) {
            throw new ClientInvestmentException('The amount to refund is more than balance');
        }

        ClientTransactionApproval::add([
            'client_id' => $holding->client_id,
            'transaction_type' => 'real_estate_unit_refund',
            'payload' => $input
        ]);

        \Flash::message('The refund has been saved for approval');

        return \Redirect::back();
    }
}
