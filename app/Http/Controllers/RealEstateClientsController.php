<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateStatementCampaign;
use App\Cytonn\Models\StatementCampaignType;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Reports\PaymentPlan;
use Cytonn\Realestate\Statements\StatementGenerator;

/**
 * Class RealEstateClientsController
 */
class RealEstateClientsController extends Controller
{


    /**
     * RealEstateClientsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the clients grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        $campaignTypes = StatementCampaignType::all()->lists('name', 'id');
        return view(
            'realestate.clients.index',
            [
                'title' => 'Real Estate Clients', 'campaign_types' => $campaignTypes
            ]
        );
    }

    /**
     * Show client details and unit listings
     *
     * @param  $id
     * @return mixed
     */
    public function getShow($id)
    {
        $client = Client::findOrFail($id);

        $projects = $client->unitHoldings->map(
            function ($unitHolding) {
                return $unitHolding->unit->project;
            }
        )->filter(function ($project) {
            return (bool) $project;
        })->unique();

        $open_campaigns_arr = RealestateStatementCampaign::where('closed', null)
            ->latest()->get()->lists('name', 'id');

        return view(
            'realestate.clients.show',
            ['title' => 'Real Estate - ' . \Cytonn\Presenters\ClientPresenter::presentJointFullNames($id),
                'client' => $client, 'projects' => $projects, 'open_campaigns_arr' => $open_campaigns_arr]
        );
    }

    /**
     * @param $clientId
     * @param $projectId
     * @return mixed
     */
    public function postStatement($clientId, $projectId)
    {
        $date = new \Carbon\Carbon(\Request::get('date'));
        $generator = new StatementGenerator();
        $client = Client::findOrFail($clientId);
        $project = Project::findOrFail($projectId);

        return $generator->generate($client, $project, $date, \Auth::user())->stream();
    }

    public function getStatement($clientId, $projectID)
    {
        return $this->postStatement($clientId, $projectID);
    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function postMailStatement($clientId)
    {
        $date = new \Carbon\Carbon(\Request::get('date'));
        $client = Client::findOrFail($clientId);

        $generator = new StatementGenerator();
        $generator->setProtectOutput(true);
        $mailer = new \Cytonn\Mailers\RealEstate\StatementMailer();

        $projectIdsCollection = new Collection(\Request::get('projects'));
        $projects = $projectIdsCollection->map(
            function ($id) {
                return Project::findOrFail($id);
            }
        );

        foreach ($projects as $project) {
            $statements[$project->name . ' Estate Statement - ' .
            ClientPresenter::presentJointFirstNameLastName($client->id) . '.pdf'] =
                $generator->generate($client, $project, $date, \Auth::user())->output();
        }

        $mailer
            ->sendStatementToClient($client, $projects, $statements, $date, \Illuminate\Support\Facades\Auth::user());

        \Flash::success('The statement(s) has been sent to the client');
        return \Redirect::back();
    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function getPreviewPaymentPlan($clientId)
    {
        $client = Client::findOrFail($clientId);

        return (new PaymentPlan())->generate($client)->stream();
    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function getMailPaymentPlan($clientId)
    {
        $client = Client::findOrFail($clientId);

        (new PaymentPlan())->send($client);

        \Flash::message('The payment plan has been sent');

        return \Redirect::back();
    }
}
