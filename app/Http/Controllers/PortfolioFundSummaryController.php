<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/27/18
 * Time: 9:56 AM
 */

namespace App\Http\Controllers;

class PortfolioFundSummaryController extends Controller
{
    public function index()
    {
        $fundManager = $this->fundManager();

        return view('portfolio.funds.summary', [
            'title' => 'Portfolio Fund Summary',
            'fundManager' => $fundManager->id
        ]);
    }

    public function pricingSummary($id = null)
    {
        $fundManager = $this->fundManager();

        return view('portfolio.funds.pricing', [
            'title' => 'Portfolio Fund Pricing Summary',
            'fundManager' => $fundManager->id,
            'unit_fund_id' => $id
        ]);
    }
}
