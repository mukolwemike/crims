<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers;

use App\Cytonn\Models\RealestateLandSize;
use Cytonn\Realestate\RealestateUnitSizes\RealestateUnitSizeForm;
use Cytonn\Realestate\RealestateUnitSizes\RealestateUnitSizeRepository;
use Illuminate\Http\Request;

class RealEstateUnitSizeController extends Controller
{
    /*
     * Get the repositories
     */
    private $realestateUnitSizeRepository;

    /*
     * Get the forms
     */
    private $realestateUnitSizeForm;

    /*
     * Setup the constructor
     */
    public function __construct(
        RealestateUnitSizeRepository $realestateUnitSizeRepository,
        RealestateUnitSizeForm $realestateUnitSizeForm
    ) {
        parent::__construct();

        $this->realestateUnitSizeRepository = $realestateUnitSizeRepository;

        $this->realestateUnitSizeForm = $realestateUnitSizeForm;
    }

    /*
     * Display the listing for the real estate unit sizes
     */
    public function index()
    {
        $landSizes = ['' => 'Select land size (Optional)'] + RealestateLandSize::all()->pluck('name', 'id')
                ->toArray();

        return view(
            'realestate.unitsizes.index',
            [
                'landSizes' => $landSizes
            ]
        );
    }

    /*
     * Supply the view to edit a unit size
     */
    public function create($id)
    {
        $landSizes = ['' => 'Select land size (Optional)'] + RealestateLandSize::all()->pluck('name', 'id')
                ->toArray();

        $unitSize = $this->realestateUnitSizeRepository->getRealestateUnitSizeById($id);

        return view(
            'realestate.unitsizes.create',
            [
                'landSizes' => $landSizes,
                'unitSize' => $unitSize
            ]
        );
    }

    /*
     * Store or update a real estate unit size
     */
    public function store(Request $request, $id = null)
    {
        $this->realestateUnitSizeForm->validate($request->all());

        $this->realestateUnitSizeRepository->store($request->all(), $id);

        if ($id) {
            \Flash::message('The unit size has been successfully updated');
        } else {
            \Flash::message('The unit size has been successfully saved');
        }

        return redirect('/dashboard/realestate/unitsizes');
    }
}
