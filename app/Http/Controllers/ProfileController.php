<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Title;
use Cytonn\Authentication\Forms\RegisterUserForm;
use Cytonn\Core\DataStructures\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use PragmaRX\Google2FA\Google2FA;

/**
 * Date: 19/10/2016
 * Time: 08:56
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ProfileController extends Controller
{
    /**
     * @var \Cytonn\Authentication\Forms\RegisterUserForm
     */
    protected $registerForm;

    /**
     * UsersController constructor.
     *
     * @param $registerForm
     */
    public function __construct(RegisterUserForm $registerForm)
    {
        parent::__construct();

        $this->registerForm = $registerForm;
    }


    /**
     * @return mixed
     */
    public function show()
    {
        $user = \Auth::user();

        $g2fa = new Google2FA();

        $google2fa_url = $g2fa->getQRCodeGoogleUrl(
            'Cytonn CRIMS',
            $user->email,
            $user->google2fa_secret
        );

        $titles = Title::all()->pluck('name', 'id');

        return \view(
            'users.profile',
            ['title'=>'User Profile', 'google2fa_url'=>$google2fa_url, 'sudo_mode'=>$this->sudoMode(), 'user'=>$user,
            'titles'=>$titles
            ]
        );
    }

    /**
     * @return mixed
     */
    public function confirmSudoMode()
    {
        return \view('users.sudo');
    }

    /**
     * set a user to sudo mode for two minutes
     *
     * @return mixed
     */
    public function setSudoMode($intended = null)
    {
        $password = \Request::get('password');
        $user = \Auth::user();

        if (\Hash::check($password, $user->password)) {
            $expiry = Carbon::now()->addMinutes(2)->toDateTimeString();

            \Session::put('sudo_mode', $expiry);

            \Flash::warning('You are now in sudo mode for the next two minutes');
        } else {
            \Flash::error('Could not validate credentials');
        }

        if ($intended) {
            return \Redirect::intended($intended);
        }

        return \Redirect::back();
    }

    /**
     * Edit User Profile
     *
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function editUser()
    {
        $user = Auth::user();

        $this->registerForm->existingAcc();

        $this->registerForm->validate(Request::all());

        $user->title_id = Request::get('title_id');
        $user->firstname = Request::get('firstname');
        $user->middlename = Request::get('middlename');
        $user->lastname = Request::get('lastname');
        $user->email = Request::get('email');
        $user->jobtitle = Request::get('jobtitle');
        $user->save();

        \Flash::success('User details succesfully saved');

        return Redirect::back();
    }
}
