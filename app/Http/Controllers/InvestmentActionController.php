<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Jobs\ProcessQueuedClosure;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Forms\BusinessConfirmationForm;
use Cytonn\Investment\Forms\ClientRolloverForm;
use Cytonn\Investment\Forms\ClientTopupForm;
use Cytonn\Investment\Forms\CombinedRolloverForm;
use Cytonn\Investment\Forms\DeductInvestmentForm;
use Cytonn\Investment\Forms\EditInvestmentForm;
use Cytonn\Investment\Forms\InvestmentTransferForm;
use Cytonn\Investment\Forms\RollbackInvestmentForm;
use Cytonn\Investment\Forms\WithdrawalForm;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Notifier\FlashNotifier as Flash;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

/**
 * Date: 29/02/2016
 * Time: 8:00 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

/**
 * Class InvestmentActionController
 */
class InvestmentActionController extends Controller
{
    /**
     * @var RollbackInvestmentForm
     */
    protected $rollbackInvestmentForm;
    /**
     * @var EditInvestmentForm
     */
    protected $editInvestmentForm;
    /**
     * @var ClientTopupForm
     */
    protected $clientTopupForm;
    /**
     * @var BusinessConfirmationForm
     */
    protected $investApplicationForm;

    /**
     * @var WithdrawalForm
     */
    protected $withdrawalForm;


    public function __construct(
        RollbackInvestmentForm $rollbackInvestmentForm,
        EditInvestmentForm $editInvestmentForm,
        ClientTopupForm $clientTopupForm,
        BusinessConfirmationForm $investApplicationForm,
        ClientRolloverForm $clientRolloverForm,
        CombinedRolloverForm $combinedRolloverForm,
        WithdrawalForm $withdrawalForm
    ) {
        parent::__construct();

        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->rollbackInvestmentForm = $rollbackInvestmentForm;
        $this->editInvestmentForm = $editInvestmentForm;
        $this->clientTopupForm = $clientTopupForm;
        $this->investApplicationForm = $investApplicationForm;
        $this->clientRolloverForm = $clientRolloverForm;
        $this->combinedRolloverForm = $combinedRolloverForm;
        $this->withdrawalForm = $withdrawalForm;
    }

    /**
     * Rollback an investment
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function rollback($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $this->rollbackInvestmentForm->validate(\Request::all());

        $investment = ClientInvestment::findOrFail($id);

        ClientTransactionApproval::add(
            ['client_id' => $investment->client_id, 'transaction_type' => 'rollback_investment',
                'payload' => ['investment_id' => $investment->id, 'reason' => \Request::get('reason')]
            ]
        );

        \Flash::message('The investment rollback has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function reverseWithdraw($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $this->rollbackInvestmentForm->validate(\Request::all());

        $withdrawal = ClientInvestmentWithdrawal::findOrFail($id);

        $child = $withdrawal->reinvestedTo;

        if (!is_null($child)) {
            \Flash::error(
                'This withdrawal was reinvested to an investment that still exists, please roll it back first'
            );

            return \Redirect::back();
        }

        ClientTransactionApproval::add([
            'client_id' => $withdrawal->investment->client_id, 'transaction_type' => 'reverse_withdrawal',
            'payload' => ['withdrawal_id' => $withdrawal->id, 'reason' => \Request::get('reason')]]);

        \Flash::message('The withdrawal reversal has been saved for approval');

        return \Redirect::back();
    }

    /**
     * Confirm an investment application and send business confirmation.
     * Show form for the confirmation
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function investApplication($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $app = ClientInvestmentApplication::findOrFail($id);

        $code = (new ClientRepository())->suggestClientCode();

        try {
            $approved = $app->approval->approved;
        } catch (Exception $e) {
            $approved = false;
        }

        if (!$approved) {
            \Flash::error('This application has not been approved');

            return Redirect::back();
        }

        if (!is_null($app->investment)) {
            Flash::warning('This application has already been confirmed');
        }

        $interest_payment_intervals = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months', 5 => 'Every 5 months',
            6 => 'Every 6 months (Semi anually)',
            7 => 'Every 7 months', 8 => 'Every 8 months', 9 => 'Every 9 months', 10 => 'Every 10 months',
            11 => 'Every 11 months', 12 => 'Every 12 months (Annually)'];

        $productlists = Product::all()->lists('name', 'id');

        $commissionrecepients = ['' => 'Select commission recipient'] + CommissionRecepient::all()->lists('name', 'id');

        $commissionrates = CommissionRate::forForm($app->product);

        $interestactions = InterestAction::all()->lists('name', 'id');

        return view(
            'investment.actions.investment.investapplication',
            [
                'title' => 'Invest the application', 'application' => $app, 'code' => $code,
                'interest_payment_intervals' => $interest_payment_intervals,
                'commissionrecepients' => $commissionrecepients, 'commissionrates' => $commissionrates,
                'productlists' => $productlists, 'interestactions' => $interestactions
            ]
        );
    }

    /**
     * Invest the application
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveInvestApplication()
    {
        $this->authorizer->checkAuthority('addinvestment');

        $app_id = Crypt::decrypt(Request::get('app_id'));

        $application = ClientInvestmentApplication::findOrFail($app_id);

        $investment = ClientInvestment::where('application_id', $app_id)->first();

        if (!is_null($investment)) {
            Flash::warning('This application has already been confirmed, you cannot confirm it again');

            return Redirect::back();
        }

        $this->investApplicationForm->validate(Request::all());

        $input = Request::all();
        unset($input['app_id']);
        unset($input['_token']);
        if (!isset($input['on_call'])) {
            $input['on_call'] = false;
        }

        if (isset($input['commission_rate_input'])) {
            unset($input['commission_rate_input']);
        }

        $approval_data = array_add($input, 'application_id', $app_id);

        ClientTransactionApproval::add(
            ['client_id' => $application->client_id, 'transaction_type' => 'investment',
                'payload' => $approval_data, 'scheduled' => 0]
        );

        \Flash::message('The Investment has been saved for approval');

        return redirect('/dashboard/investments');
    }

    /**
     * Edit a client investment
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function editInvestment($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $investment = ClientInvestment::findOrFail($id);
        $value_date = $investment->invested_date->toDateString();
        $maturity_date = $investment->maturity_date->toDatestring();
        $interest_payment_intervals = $this->interestIntervals();

        $interestactions = InterestAction::all()->lists('name', 'id');

        $commissionRecepient = (new CommissionRecepient())->repo->getRecipientsForSelect();

        if ($investment->interest_payment_interval == 0) {
            $investment->interest_payment_interval = null;
        }

        $commissionStartDate = ($investment->commission) ? $investment->commission->start_date : null;

        return view(
            'investment.actions.edit.edit',
            [
                'title' => 'Edit Client Investment', 'investment' => $investment,
                'value_date' => $value_date, 'maturity_date' => $maturity_date,
                'interest_payment_intervals' => $interest_payment_intervals,
                'interestactions' => $interestactions, 'commissionRecepient' => $commissionRecepient,
                'commissionStartDate' => $commissionStartDate
            ]
        );
    }

    /**
     * Save an edited investment for approval
     *
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postEditInvestment()
    {
        $input = \Request::except("_token");

        $this->editInvestmentForm->validate(Request::all());

        $id = Crypt::decrypt(Request::get('investment'));

        $investment = ClientInvestment::findOrFail($id);

        $data['investment'] = $id;
        if (isset($input['on_call'])) {
            $input['on_call'] = boolval(\Request::get('on_call'));
        }

        switch ($input['edit_type']) {
            case 'investment_interest_rate':
                $transType = 'edit_investment_interest_rate';
                break;
            case 'investment_maturity_date':
                $transType = 'edit_investment_maturity_date';
                break;
            case 'investment_commission':
                $transType = 'edit_investment_commission';
                break;
            case 'investment_payment_details':
                $transType = 'edit_investment_payment_details';
                break;
            default:
                $transType = 'edit_investment';
                break;
        }

        unset($input['investment'], $input['edit_type']);
        $data['new'] = $input;

        $approval = ClientTransactionApproval::add(['client_id' => $investment->client_id,
            'transaction_type' => $transType, 'payload' => $data, 'scheduled' => 0]);

        \Flash::message('Edited investment has been saved for approval');

        if (isset($input['interest_rate']) || isset($input['commission_recepient'])) {
            return redirect()->to('/dashboard/investments/client-instructions/' . $approval->id . '/approval-document/edit');
        }

        return redirect('/dashboard/investments');
    }

    /**
     * Show the topup form
     *
     * @param  $id
     * @return mixed
     */
    public function topup($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $payments_balance = (new ClientPayment())->balance($investment->client, $investment->product);

        $interest_payment_intervals = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months',
            5 => 'Every 5 months', 6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months',
            8 => 'Every 8 months', 9 => 'Every 9 months', 10 => 'Every 10 months',
            11 => 'Every 11 months', 12 => 'Every 12 months (Annually)'];

        $products = Product::active()->get()->lists('name', 'id');

        $recipients = CommissionRecepient::all()->pluck('name', 'id');

        $investment->repo->getCommissionRate();

        $interestActions = InterestAction::all()->lists('name', 'id');

        return view(
            'investment.actions.topup.topup',
            ['title' => 'Investment topup', 'investment' => $investment,
                'interest_payment_intervals' => $interest_payment_intervals, 'payments_balance' => $payments_balance,
                'products' => $products, 'recipients' => $recipients, 'interestActions' => $interestActions
            ]
        );
    }

    /**
     * Save the topup to await approval
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveTopup()
    {
        $this->authorizer->checkAuthority('addinvestment');

        $input = Request::all();

        $input['investment'] = Crypt::decrypt($input['investment']);

        $input['on_call'] = $input['on_call'] == 'true';

        $input['investment_type_id'] = ClientInvestmentType::where('name', 'topup')->first()->id;

        $this->clientTopupForm->validate($input);

        unset($input['_token']);

        $oldInvestment = ClientInvestment::findOrFail($input['investment']);

        $input['client_id'] = $oldInvestment->client_id;

        if ((new InvestmentsRepository())->checkIfTopUpIsDuplicate($input)) {
            Flash::error('Duplicate! This Top-up already exists!');

            return Redirect::back();
        }

        ClientTransactionApproval::add(
            ['client_id' => $oldInvestment->client_id, 'transaction_type' => 'topup',
                'payload' => $input, 'scheduled' => 0]
        );

        Flash::message('The topup has been saved for approval');

        return redirect('/dashboard/investments');
    }

    /**
     * Show the rollover form
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function rollover($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $investment = ClientInvestment::findOrFail($id);
        if ($investment->withdrawn == 1) {
            \Flash::warning('This investment has already been withdrawn');
        }

        $interest_payment_intervals = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months',
            5 => 'Every 5 months', 6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months',
            8 => 'Every 8 months', 9 => 'Every 9 months', 10 => 'Every 10 months',
            11 => 'Every 11 months', 12 => 'Every 12 months (Annually)'];

        $commissionrecepients = CommissionRecepient::all()->lists('name', 'id');

        $interestactions = InterestAction::all()->lists('name', 'id');

        return view(
            'investment.actions.rollover.rollover',
            [
                'title' => 'Investment Rollover', 'investment' => $investment,
                'interest_payment_intervals' => $interest_payment_intervals,
                'commissionrecepients' => $commissionrecepients, 'interestactions' => $interestactions,
            ]
        );
    }

    public function rolloverInvestment($id)
    {
        $this->authorizer->checkAuthority('addinvestment');
        $investment = (new ClientInvestment())->findOrFail($id);

        if ($investment->withdrawn == 1) {
            \Flash::warning('This investment has already been withdrawn');
        }

        $same_day_investments = (new ClientInvestment())->where('client_id', $investment->client_id)
            ->where('product_id', $investment->product_id)
            ->where('id', '!=', $investment->id)
            ->active()
            ->get();

        return view(
            'investment.actions.rollover.rollover-investment',
            [
                'title' => 'Investment Rollover', 'investment' => $investment,
                'same_day_investments' => $same_day_investments
            ]
        );
    }

    /**
     * Save the rollover for approval
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveRollover()
    {
        $this->authorizer->checkAuthority('addinvestment');

        $input = Request::all();

        $input['investment'] = Crypt::decrypt($input['investment']);
        $input['on_call'] = Request::get('on_call') == 'true';
        $input['automatic_rollover'] = \Request::get('automatic_rollover') == 'true' ? true : false;

        $oldInvestment = ClientInvestment::findOrFail($input['investment']);

        $validation_data = $input;

        //allow same day
        $validation_data = array_add(
            $validation_data,
            'old_investment_maturity_date',
            (new Carbon($oldInvestment->maturity_date))->subDay()->toDateString()
        );
        $validation_data = array_add(
            $validation_data,
            'old_investment_amount',
            (float)$oldInvestment->repo->getTotalValueOfAnInvestment()
        );
        $validation_data['amount'] = (float)$validation_data['old_investment_amount'];

        $input['amount'] = round($input['amount'], 2);

        if ($input['amount'] > $validation_data['amount']) {
            \Flash::error('The amount rolled over should be less than total value');

            return \Redirect::back()->withInput();
        }

        $this->clientRolloverForm->validate($validation_data);

        unset($input['_token']);

        $oldInvestment = ClientInvestment::findOrFail(Crypt::decrypt(Request::get('investment')));

        $scheduled = false;

        if (Request::get('schedule')) {
            $scheduled = true;
            $input['scheduled_by'] = \Auth::user()->id;

            \Flash::message('The Rollover scheduling has been saved for approval');
        } else {
            \Flash::message('The Rollover has been saved for approval');
        }

        ClientTransactionApproval::add(
            ['client_id' => $oldInvestment->client_id,
                'transaction_type' => 'rollover', 'payload' => $input, 'scheduled' => $scheduled]
        );

        return redirect('/dashboard/investments');
    }

    /**
     * Show table to select investments to rollover
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function combinedRollover($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $investment = ClientInvestment::findOrFail($id);

        $same_day_investments = ClientInvestment::where('client_id', $investment->client_id)
            ->where('product_id', $investment->product_id)
            //                                                ->where('maturity_date', $investment->maturity_date)
            ->where('id', '!=', $investment->id)
            ->active()
            ->get();

        return view(
            'investment.actions.rollover.combinedrollover',
            ['title' => 'Combine and Rollover Investment',
                'investment' => $investment, 'same_day_investments' => $same_day_investments]
        );
    }

    /**
     * Combine the selected investments and show rollover form
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function combineSelectedRollover($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $investment = ClientInvestment::findOrFail($id);

        $input = Request::all();

        unset($input['_token']);

        $investments = new Collection();

        $combineDate = Carbon::parse($input['combine_date']);

        $maturityDate = Carbon::parse($investment->maturity_date);

        if ($combineDate <= $maturityDate) {
            $maturityDate = $combineDate;
        } else {
            \Flash::error('No client investment should have it maturity date before the combine date');

            return \Redirect::back()->withInput();
        }

        $investment->value = $investment->repo->getTotalValueOfInvestmentAtDate($maturityDate);

        $investments->push($investment);

        foreach (array_keys($input) as $inv_id) {
            $inv = ClientInvestment::find($inv_id);

            if ($inv) {
                $maturityDate = Carbon::parse($inv->maturity_date);

                if ($combineDate <= $maturityDate) {
                    $maturityDate = $combineDate;
                } else {
                    \Flash::error('No investment should have its maturity date before the combine date');

                    return \Redirect::back()->withInput();
                }

                $inv->value = $inv->repo->getTotalValueOfInvestmentAtDate($maturityDate);

                $investments->push($inv);
            }
        }

        $interestActions = InterestAction::all()->lists('name', 'id');

        $recipients = CommissionRecepient::all()->lists('name', 'id');

        $interest_payment_intervals = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months',
            5 => 'Every 5 months', 6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months', 8 => 'Every 8 months',
            9 => 'Every 9 months', 10 => 'Every 10 months',
            11 => 'Every 11 months', 12 => 'Every 12 months (Annually)'];

        return view(
            'investment.actions.rollover.performcombinedrollover',
            [
                'title' => 'Combine and Rollover Investments',
                'investments' => $investments,
                'interest_payment_intervals' => $interest_payment_intervals,
                'interestActions' => $interestActions,
                'recipients' => $recipients,
                'combineDate' => $combineDate,
                'mainInvestment' => $investment
            ]
        );
    }

    /**
     * Save the combined rollover
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function saveCombineSelectedRollover($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $input = Request::all();

        unset($input['_token']);

        $input['investment'] = Crypt::decrypt($input['investment']);

        $input['on_call'] = Request::get('on_call') == 'true';

        $oldInvestment = ClientInvestment::findOrFail($input['investment']);

        $validation_data = $input;

        //allow same day
        $validation_data = array_add(
            $validation_data,
            'old_investment_maturity_date',
            (new Carbon($input['combine_date']))->subDay()->toDateString()
        );
        $validation_data = array_add(
            $validation_data,
            'old_investment_amount',
            (float)$oldInvestment->repo->getTotalValueOfInvestmentAtDate($input['combine_date'])
        );
        $validation_data['amount'] = (float)$validation_data['old_investment_amount'];

        $validator = $this->combinedRolloverForm->validates($validation_data);

        if (!$validator->valid) {
            $error_list = '';

            foreach ($validator->errors->all() as $error) {
                $error_list = $error_list . '<li>' . $error . '</li>';
            }

            \Flash::error('The following errors occured: <br/>' . $error_list);

            return Redirect::back()->withInput();
        }

        //validate the amounts for when reinvesting or withdrawing
        if ($input['reinvest'] == 'withdraw' || $input['reinvest'] == 'reinvest') {
            $investments = new Collection();

            foreach (json_decode($input['investments']) as $inv_id) {
                $inv = ClientInvestment::find($inv_id);

                $inv->value = $inv->repo->getTotalValueOfInvestmentAtDate($input['combine_date']);

                $investments->push($inv);
            }

            if ($input['amount'] > round($investments->sum('value'), 2)) {
                \Flash::message(
                    'The amount withdrawn/reinvested cannot be more than the total value of the combined investments'
                );

                return Redirect::back()->withInput();
            }
        }

        ClientTransactionApproval::add(
            ['client_id' => $oldInvestment->client_id,
                'transaction_type' => 'combined_rollover', 'payload' => $input, 'scheduled' => 0]
        );

        \Flash::message('Rollover transaction saved for approval');

        return redirect('/dashboard/investments');
    }


    /**
     * @param $id
     * @return mixed
     */
    public function transfer($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $investment = ClientInvestment::findOrFail($id);

        return view(
            'investment.actions.transfer.index',
            ['title' => 'Transfer Investments', 'investment' => $investment]
        );
    }

    /**
     *
     * @param $id
     * @return mixed
     */
    public function postTransfer($id)
    {
        $this->authorizer->checkAuthority('addinvestment');

        app(InvestmentTransferForm::class)->validate(Request::all());

        $investment = ClientInvestment::findOrFail($id);
        Client::findOrFail(Request::get('client_id'));

        $input = Request::except('_token');
        $input['investment_id'] = $investment->id;

        ClientTransactionApproval::add(
            ['client_id' => $investment->client->id, 'transaction_type' => 'transfer_investment', 'payload' => $input]
        );

        \Flash::message('The transfer has been saved for approval');

        return \redirect('/dashboard/investments');
    }
}
