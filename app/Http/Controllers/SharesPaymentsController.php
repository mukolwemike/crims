<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ShareHolder;
use Cytonn\Presenters\ClientPresenter;

/**
 * Class SharesPaymentsController
 */
class SharesPaymentsController extends Controller
{

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('shares.payments.index', ['title' => 'Share Payments']);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function getShareholders($share_holder_id)
    {
        $holder = ShareHolder::findOrFail($share_holder_id);

        return view('shares.payments.shareholder', [
            'title' => ClientPresenter::presentFullNames($holder->client_id) . ' - Share Payments',
            'holder'=>$holder
        ]);
    }
}
