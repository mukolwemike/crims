<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientRank;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Jobs\Clients\ClientContactsExportCommand;
use App\Jobs\Clients\ClientProfilingExport;
use App\Jobs\Clients\ExportFAClients;
use App\Jobs\Clients\InactiveCMSClients;
use App\Jobs\Clients\UntaxedClients;
use App\Jobs\FAs\DistributionStructureReport;
use App\Jobs\FAs\FaCommissionProjectionReport;
use App\Jobs\FAs\FAsCommissionSummaryBetweenDates;
use App\Jobs\Instructions\ExportBankInstructions;
use App\Jobs\Investments\ExportClosingBalancesForPartners;
use App\Jobs\Investments\InvestmentProductionReport;
use App\Jobs\Investments\PenaltyDeductions;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class InvestmentReportsController extends Controller
{

    /**
     * InvestmentReportsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display the  investments reports
     *
     * @return Response
     */
    public function getIndex()
    {
        $categories = ['investment' => 'Investment', 'realestate' => 'Real Estate', 'combined' => 'Combined'];
        $activeStatuses = ['all' => 'All Clients', 'active' => 'Active Clients', 'inactive' => 'Inactive Clients'];

        $products = Product::pluck('name', 'id');
        $projects = Project::pluck('name', 'id');

        $fas = CommissionRecepient::orderBy('name')->active()->get()->lists('name', 'id');

        $faTypes = CommissionRecipientType::pluck('name', 'id');

        $faRanks = CommissionRecipientRank::pluck('name', 'id');

        $fundManagers = FundManager::pluck('name', 'id');

        $accounts = CustodialAccount::pluck('account_name', 'id')->toArray();

        $accountTypes = [null => 'All', 'joint' => 'Joint', 'individual' => 'Individual', 'corporate' => 'Corporate'];

        $genders = [null => 'All'] + Gender::pluck('abbr', 'id')->toArray();

        $unitFunds = UnitFund::pluck('name', 'id');

        return view('investment.reports.reports', [
            'title' => 'Investments Reports',
            'categories' => $categories,
            'fas' => $fas,
            'products' => $products,
            'faTypes' => $faTypes,
            'fundManagers' => $fundManagers,
            'accounts' => $accounts,
            'projects' => $projects,
            'activeStatuses' => $activeStatuses,
            'accountTypes' => $accountTypes,
            'genders' => $genders,
            'faRanks' => $faRanks,
            'unitFunds' => $unitFunds
        ]);
    }

    /**
     * Display the  investments reports: Clients Summaries
     *
     * @return Response
     */
    public function getClientsSummaries()
    {
        return view('investment.reports.clientsummaries', ['title' => 'Clients Summaries']);
    }

    /**
     * Download the investments reports: Clients Summaries
     */
    public function postClientsSummaries()
    {
        $start = Request::get('start');
        $end = Request::get('end');
        $fm = Request::get('fundmanager');
        $user_id = \Auth::user()->id;

        $this->queue(function () use ($start, $user_id, $end, $fm) {
            \Artisan::call(
                'investments:client-summary',
                ['start' => $start, 'end' => $end, '--user_id' => $user_id, '--fm' => $fm]
            );
        });

        \Flash::message('Download successful! An email with the attachment has been sent to your account.');

        return Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportClientContacts()
    {
        $input = Request::all();

        $productIds = array_key_exists('product_ids', $input) ? $input['product_ids'] : [];

        $projectIds = array_key_exists('project_ids', $input) ? $input['project_ids'] : [];

        dispatch(
            (new ClientContactsExportCommand(
                $input['category'],
                \Auth::user(),
                $projectIds,
                $productIds,
                $input['active']
            ))->onQueue(config('queue.priority.high'))
        );

        \Flash::success('The client contacts will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export the client profiling report
     */
    public function exportClientProfiling()
    {
        $input = Request::all();

        if ($input['min_age'] && $input['max_age'] && $input['max_age'] < $input['min_age']) {
            \Flash::success('The minimum age should not be lower than the maximum age');

            return \Redirect::back();
        }

        if ($input['min_investment'] &&
            $input['max_investment'] &&
            $input['max_investment'] < $input['min_investment']) {
            \Flash::success('The minimum age should not be lower than the maximum age');

            return \Redirect::back();
        }

        dispatch((new ClientProfilingExport(
            \Auth::user(),
            $input['category'],
            $input['active'],
            $input['gender'],
            $input['account_type'],
            $input['min_investment'],
            $input['max_investment'],
            $input['min_age'],
            $input['max_age'],
            $input['re_option']
        ))->onQueue(config('queue.priority.high')));

        \Flash::success('The client profiling report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportUntaxedClients()
    {
        dispatch((new UntaxedClients(\Auth::user()))->onQueue(config('queue.priority.high')));

        \Flash::success('The list of untaxed clients will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportInactiveCMSClients()
    {
        $input = Request::all();
        $category = $input['category'];

        dispatch((new InactiveCMSClients(\Auth::user(), $category))->onQueue(config('queue.priority.high')));

        \Flash::success('The list of inactive CHYS clients will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportInvestments()
    {
        $input = Request::all();
        $user_id = \Auth::user()->id;
        $this->queue(
            function () use ($input, $user_id) {
                \Artisan::call(
                    'cytonn:export-investment',
                    ['start' => $input['start_date'], 'end' => $input['end_date'], 'user_id' => $user_id]
                );
            }
        );

        $this->allow('investments:admin-clients-reports');

        \Flash::success('The investments will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportClosingBalancesForPartners()
    {
        $input = Request::all();

        dispatch((new ExportClosingBalancesForPartners($input['start_date'], $input['end_date'], \Auth::user()))
            ->onQueue(config('queue.priority.high')));

        \Flash::success('The closing balances for partners report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportInterestExpense()
    {
        $input = Request::all();

        $user_id = \Auth::user()->id;

        $this->queue(
            function () use ($input, $user_id) {
                \Artisan::call(
                    'investments:interest-expense',
                    ['start' => $input['start_date'], 'end' => $input['end_date'], 'user_id' => $user_id]
                );
            }
        );

        \Flash::success('The interest expense report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function exportResidualIncome()
    {
        $input = Request::all();
        $user_id = \Auth::user()->id;
        $fm_id = $this->fundManager()->id;

        $this->allow('investments:admin-fund-reports');

        $this->queue(
            function () use ($input, $user_id, $fm_id) {
                \Artisan::call(
                    'investments:residual-income',
                    ['date' => $input['start_date'], 'end' => $input['end_date'], 'user_id' => $user_id,
                        '--currency' => 'KES', '--fm' => $fm_id]
                );
            }
        );

        \Flash::success('The residual income report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportCommissionSummary()
    {
        $input = Request::all();
        $user_id = \Auth::user()->id;
        $this->queue(
            function () use ($input, $user_id) {
                \Artisan::call(
                    'investments:commission-summary',
                    ['fund_manager_id' => $input['fund_manager_id'], 'date' => $input['date'], 'user_id' => $user_id]
                );
            }
        );

        \Flash::success('The commission summary report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function exportStatement()
    {
        $this->allow('investments:admin-clients-reports');

        $this->queue(
            function () {
                \Artisan::call(
                    'crims:year_statements',
                    ['start' => '2016-09-01', 'end' => '2017-08-31']
                );
            }
        );

        \Flash::success('The statements will be generated shortly.');
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function exportCoopClients()
    {
        $user_id = \Auth::user()->id;
        $this->queue(
            function () use ($user_id) {
                \Artisan::call('coop:clients-report', ['user_id' => $user_id]);
            }
        );

        \Flash::success('The coop clients report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function exportWithholdingTax()
    {
        $input = Request::all();
        $user_id = \Auth::user()->id;
        $this->queue(
            function () use ($input, $user_id) {
                \Artisan::call(
                    'investments:tax',
                    ['start' => $input['start_date'], 'end' => $input['end_date'],
                        'product_id' => $input['product_id'], 'user_id' => $user_id]
                );
            }
        );

        \Flash::success('The withholding tax report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function exportWithholdingTaxReport()
    {
        $input = request()->all();

        $user_id = \Auth::user()->id;

        $this->queue(
            function () use ($input, $user_id) {
                \Artisan::call(
                    'investments:client_tax',
                    [
                        'start' => $input['start_date'],
                        'end' => $input['end_date'],
                        'product_id' => $input['product_id'],
                        'user_id' => $user_id,
                        'unit_fund_id' => $input['unit_fund_id']
                    ]
                );
            }
        );

        \Flash::success('The withholding tax report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function exportWithholdingTaxItaxReport()
    {
        $input = request()->all();

        $user_id = \Auth::user()->id;

        $this->queue(
            function () use ($input, $user_id) {
                \Artisan::call(
                    'crims:withholding_tax_itax_report',
                    [
                        'start' => $input['start_date'],
                        'end' => $input['end_date'],
                        'product_id' => $input['product_id'],
                        'user_id' => $user_id,
                        'unit_fund_id' => $input['unit_fund_id']
                    ]
                );
            }
        );

        \Flash::success('The withholding tax itax report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportInvestmentsChurn()
    {
        $user_id = \Auth::user()->id;
        $this->queue(
            function () use ($user_id) {
                \Artisan::call('cytonn:inv-churn', ['user_id' => $user_id]);
            }
        );

        \Flash::success('The client investments churn report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportFAClients()
    {
        $fa_ids = Request::get('fa_ids');

        $faType = Request::get('fa_type');

        $faRank = Request::get('fa_rank');

        if ($faType || $faRank) {
            if ($faType) {
                $fas = CommissionRecepient::where('recipient_type_id', $faType);
            }

            if ($faRank) {
                $fas = CommissionRecepient::where('rank_id', $faRank);
            }

            $fas = $fas->active()->get();
        } else {
            $fas = CommissionRecepient::whereIn('id', [$fa_ids])->get();
        }

        $start = Request::get('start_date');

        $end = Request::get('end_date');

        dispatch((new ExportFAClients(\Auth::user(), $fas, $start, $end))->onQueue(config('queue.priority.high')));

        \Flash::success('The FA Clients report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportDistributionStructure()
    {
        dispatch((new DistributionStructureReport(\Auth::user()))->onQueue(config('queue.priority.high')));

        \Flash::success('The Distribution Structure report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function exportCommissionSummaryForFAs()
    {
        $input = Request::all();
        $start = Carbon::parse($input['start']);
        $end = Carbon::parse($input['end']);

        dispatch((new FAsCommissionSummaryBetweenDates($start, $end, \Auth::user()))
            ->onQueue(config('queue.priority.high')));

        \Flash::success('The Commission Summary for FAs report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportBankInstructions()
    {
        $input = Request::all();
        $start = Carbon::parse($input['start_date']);
        $end = Carbon::parse($input['end_date']);

        dispatch((new ExportBankInstructions($start, $end, \Auth::user()))->onQueue(config('queue.priority.high')));

        \Flash::success('The Bank Instructions report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportPenaltyDeductions()
    {
        $input = Request::all();
        $start = Carbon::parse($input['start']);
        $end = Carbon::parse($input['end']);

        dispatch((new PenaltyDeductions($start, $end, \Auth::user()))->onQueue(config('queue.priority.high')));

        \Flash::success('The Penalty Deductions report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportAssetLiabilityReconciliation()
    {
        $input = Request::all();
        $date = Carbon::parse($input['date']);

        $currency = Currency::findOrFail($input['currency_id']);

        $this->allow('investments:admin-fund-reports');

        $user_id = \Auth::user()->id;

        $this->queue(
            function () use ($date, $user_id, $currency) {
                \Artisan::call('investments:asset-recon', ['date' => $date->toDateString(),
                    '--user' => $user_id, '--currency' => $currency->code]);
            }
        );

        \Flash::success('The asset liability reconciliation report will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Generate the production report
     */
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function generateProductionReport(\Illuminate\Http\Request $request)
    {
        $this->dispatch((new InvestmentProductionReport(
            Carbon::parse($request->get('start_date')),
            Carbon::parse($request->get('end_date')),
            $request->get('granularity'),
            [\Auth::user()->email]
        ))->onQueue(config('queue.priority.high')));

        \Flash::message('The production report will be sent shortly via email');

        return back();
    }

    /*
     * Generate the monthly inflows report
     */
    public function generateMonthlyInflowsReport(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        if (is_null($input['start_date']) || is_null($input['end_date'])) {
            \Flash::error('Please specify the start date or end date for the report');

            return back();
        }

        $user_id = Auth::id();

        $this->queue(
            function () use ($input, $user_id) {
                \Artisan::call(
                    'custody:monthly-inflows',
                    ['start_date' => $input['start_date'], 'end_date' => $input['end_date'],
                        'user_id' => $user_id, 'fundmanager' => $input['fundmanager']]
                );
            }
        );

        \Flash::message('The production report will be sent shortly via email');

        return back();
    }

    /**
     * Export a report of unaccounted funds
     */
    public function exportUnaccountedFunds()
    {
        $userId = \Auth::user()->id;

        $this->queue(
            function () use ($userId) {
                \Artisan::call(
                    'clients:unassigned_client_payments',
                    ['user_id' => $userId]
                );
            }
        );

        \Flash::success('The unaccounted funds will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * Export FA Commissions Projections report
     *
     * @return mixed
     */
    public function exportFaCommissionProjections()
    {
        $fa_ids = Request::get('fa_ids');

        $fas = CommissionRecepient::whereIn('id', $fa_ids)->get();

        $start = Carbon::parse(Request::get('start'));
        $end = Carbon::parse(Request::get('end'));


        dispatch((new FaCommissionProjectionReport(\Auth::user(), $fas, $start, $end))
            ->onQueue(config('queue.priority.high')));

        \Flash::success('The FA Commission Projections report will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export withdrawals report
     */
    public function exportWithdrawalsReport()
    {
        $userId = \Auth::user()->id;
        $start = Request::get('start');
        $end = Request::get('end');

        $this->queue(
            function () use ($userId, $start, $end) {
                \Artisan::call(
                    'investments:withdrawals',
                    ['user_id' => $userId, 'start' => $start, 'end' => $end]
                );
            }
        );

        \Flash::success('The withdrawals report will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export inflows report
     */
    public function exportInflowsReport()
    {
        $userId = \Auth::user()->id;
        $start = Request::get('start');
        $end = Request::get('end');

        $this->queue(function () use ($userId, $start, $end) {
            \Artisan::call(
                'investments:inflows',
                ['user_id' => $userId, 'start' => $start, 'end' => $end]
            );
        });

        \Flash::success('The investment inflows report will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export client statements export report
     */
    public function exportClientStatementsReport()
    {
        $userId = \Auth::user()->id;
        $start = Carbon::parse(Request::get('start'));
        $end = Carbon::parse(Request::get('end'));
        $productId = Request::get('product_id');

        $this->queue(
            function () use ($userId, $start, $end, $productId) {
                \Artisan::call(
                    'investments:export-statements',
                    ['user_id' => $userId, 'start' => $start, 'end' => $end, 'product_id' => $productId]
                );
            }
        );

        \Flash::success('The investment client statements export will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export the fa year production
     */
    public function exportFaYearProductionReport()
    {
        $userId = \Auth::user()->id;
        $year = Request::get('year');
        $faType = Request::get('fa_type');

        $this->queue(
            function () use ($year, $faType, $userId) {
                \Artisan::call(
                    'fas:year_production',
                    ['user_id' => $userId, 'type' => $faType, 'year' => $year]
                );
            }
        );

        \Flash::success('The investment fa year production export will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export the fundmanager monthly netflow report
     */
    public function exportFundManagerMonthlyNetflow()
    {
        $userId = \Auth::user()->id;

        $year = Request::get('year');

        $fundManager = Request::get('fundmanager');

        $this->queue(
            function () use ($year, $fundManager, $userId) {
                \Artisan::call(
                    'fund:monthly_netflow',
                    ['user_id' => $userId, 'fund_manager' => $fundManager, 'year' => $year]
                );
            }
        );

        \Flash::success('The fundmanager monthly netflow report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function exportDailyBusinessConfirmations()
    {
        $startDate = Request::get('start_date');
        $endDate = Request::get('end_date');

        $id = \Auth::user()->id;

        $this->queue(
            function () use ($id, $startDate, $endDate) {
                \Artisan::call(
                    'cytonn:daily-business-confirmations-report',
                    ['--user_id' => $id, 'start_date' => $startDate, 'end_date' => $endDate]
                );
            }
        );

        \Flash::success('The daily business confirmations export will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export the currency report
     */
    public function exportCurrencyReport()
    {
        $startDate = \Request::get('start_date');
        $endDate = \Request::get('end_date');
        $accountId = \Request::get('account_id');
        $userId = Auth::id();

        $this->queue(
            function () use ($startDate, $endDate, $accountId, $userId) {
                \Artisan::call('custody:currency_report', ['end_date' => $endDate,
                    'start_date' => $startDate, 'account_id' => $accountId, 'user_id' => $userId]);
            }
        );

        \Flash::success('The currency report will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export client movement audit
     */
    public function exportClientMovementAudit()
    {
        $input = Request::all();
        $user = Auth::id();
        $fundmanager = $input['fund_manager_id'];
        $start = $input['start_date'];
        $end = $input['end_date'];

        $this->queue(
            function () use ($fundmanager, $start, $end, $user) {
                \Artisan::call(
                    'crims:client_movement_audit',
                    [
                        'fundmanager' => $fundmanager,
                        'start' => $start,
                        'end' => $end,
                        'user_id' => $user
                    ]
                );
            }
        );

        \Flash::success('The client movement report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /*
     * Export client tenor report
     */
    public function exportClientTenorReport()
    {
        $input = Request::all();
        $user = Auth::id();
        $fundmanager = $input['fund_manager_id'];
        $start = $input['start_date'];
        $end = $input['end_date'];

        $this->queue(
            function () use ($fundmanager, $start, $end, $user) {
                \Artisan::call(
                    'crims:client_tenor',
                    [
                        'fundmanager' => $fundmanager,
                        'start' => $start,
                        'end' => $end,
                        'user_id' => $user
                    ]
                );
            }
        );

        \Flash::success('The client tenor report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportDailyWithdrawalReport()
    {
        $input = Request::all();
        $user = Auth::id();
        $start = $input['start_date'];
        $end = $input['end_date'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call(
                    'investments:withdrawal-report',
                    [
                        'start' => $start,
                        'end' => $end,
                        'user_id' => $user
                    ]
                );
            }
        );

        \Flash::success('The daily withdrawal report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportActiveClientsReport()
    {
        $input = Request::all();
        $user = Auth::id();
        $date = $input['date'];
        $type = $input['type'];

        $this->queue(
            function () use ($date, $type, $user) {
                \Artisan::call(
                    'clients:active_clients_exports',
                    [
                        'date' => $date,
                        'type' => $type,
                        'user_id' => $user
                    ]
                );
            }
        );

        \Flash::success('The active clients report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportClawbacksReport()
    {
        $input = Request::all();

        $user = Auth::id();
        $start = $input['start_date'];
        $end = $input['end_date'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:commission_clawbacks_summary', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The clawbacks report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportClientCustodialTransfer()
    {
        $input = Request::all();

        $user = Auth::id();
        $start = $input['start_date'];
        $end = $input['end_date'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:inter_client_custodial_transfer', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );
        \Flash::success('The Inter Client Custodial Transfer report will be generated and sent to
         you shortly via email');

        return \Redirect::back();
    }

    public function exportClientCommissionPayments()
    {
        $input = Request::all();

        $user = Auth::id();
        $client_id = $input['client_id'];

        $client = Client::find($client_id);

        if (is_null($client)) {
            \Flash::error('Could not locate the specified client');

            return \Redirect::back();
        }

        $this->queue(
            function () use ($client_id, $user) {
                \Artisan::call('crims:client_commission_payments', [
                    'client_id' => $client_id,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The Client Commision Payment report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportIfaClients()
    {
        $user = Auth::id();

        $this->queue(
            function () use ($user) {
                \Artisan::call('crims:ifas_client_summary', [
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The ifs client summary report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function clientPayments()
    {
        \Artisan::call('clients:client_payment_instruction');

        dd("here");
    }

    public function exportBackdatedTransactions()
    {
        $input = request()->all();

        $user = Auth::id();

        $start = $input['start_date'];

        $end = $input['end_date'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:backdated_transactions', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The backdated transactions report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportUnitFundsClientInvestments()
    {
        $input = request()->all();

        $user = Auth::id();

        $start = $input['start'];

        $end = $input['end'];

        $fund = \request()->get('unit_fund_id');

        $this->queue(
            function () use ($start, $end, $user, $fund) {
                \Artisan::call('investments:export-investment', [
                    'start' => $start,
                    'end' => $end,
                    '--user_id' => $user,
                    '--fund' => $fund
                ]);
            }
        );

        \Flash::success('The client investments report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportFundInflows()
    {
        $input = request()->all();

        $user_id = Auth::id();

        $start = $input['start'];

        $end = $input['end'];

        $this->queue(function () use ($user_id, $start, $end) {
            \Artisan::call('sap:inflows', [
                'user_id' => $user_id,
                'start' => $start,
                'end' => $end
            ]);
        });

        \Flash::success('The fund inflows will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportFundOutflows()
    {
        $input = request()->all();

        $user_id = Auth::id();

        $start = $input['start'];

        $end = $input['end'];

        $this->queue(function () use ($user_id, $start, $end) {
            \Artisan::call('sap:outflows', [
                'user_id' => $user_id,
                'start' => $start,
                'end' => $end
            ]);
        });

        \Flash::success('The fund outflows will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportCashTransfers()
    {
        $input = request()->all();

        $user_id = Auth::id();

        $start = $input['start'];

        $end = $input['end'];

        $type = $input['type'];

        $this->queue(function () use ($user_id, $start, $end, $type) {
            \Artisan::call('sap:transfers', [
                'user_id' => $user_id,
                'start' => $start,
                'end' => $end,
                'type' => $type
            ]);
        });

        \Flash::success('The cash transfers will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportSapInterestExpense()
    {
        $input = request()->all();

        $user_id = Auth::id();

        $start = $input['start'];

        $end = $input['end'];

        $this->queue(function () use ($user_id, $start, $end) {
            \Artisan::call('sap:interest-expense', [
                'user_id' => $user_id,
                'start' => $start,
                'end' => $end
            ]);
        });

        \Flash::success('The interest expenses will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportFundMaturityReport()
    {
        $input = request()->all();

        $user = \Auth::id();

        $start = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now();

        $end = isset($input['end_date']) ? Carbon::parse($input['end_date']) : $start->copy()->addMonth(1);

//        $product = $input['product_id'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:investment_maturity_rollover_summary', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The fund maturity report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportPaymentTransactionsSummary()
    {
        $input = request()->all();

        $user_id = Auth::id();

        $start = Carbon::parse($input['start'])->startOfDay()->toDateTimeString();

        $end = Carbon::parse($input['end'])->endOfDay()->toDateTimeString();

        $this->queue(function () use ($user_id, $start, $end) {
            \Artisan::call('crims:payment_transaction_summary', [
                'user_id' => $user_id,
                'start' => $start,
                'end' => $end
            ]);
        });

        \Flash::success('The payment transaction summary will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportClientMaturityReport()
    {
        $input = request()->all();

        $user = \Auth::id();

        $start = $input['start_date'];

        $end = $input['end_date'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:client_maturities_export', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The client maturity export will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportFaCommissionHistory()
    {
        $input = request()->all();

        $user = \Auth::id();

        $start = $input['start'];

        $end = $input['end'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:fa_commission_history_summary', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The fa commission history will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportDigitalTransactionSummary()
    {
        $input = request()->all();

        $user = \Auth::id();

        $start = $input['start'];

        $end = $input['end'];

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:digital_transactions', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The digital transactions summary will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportIntraPaymentsSummary()
    {
        $input = request()->all();

        $user = \Auth::id();

        $start = Carbon::parse($input['start'])->toDateTimeString();

        $end = Carbon::parse($input['end'])->toDateTimeString();

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:intra_payments_summary', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The intra payments summary will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportAutomaticWithdrawalSummary()
    {
        $input = request()->all();

        $user = \Auth::id();

        $start = Carbon::parse($input['start'])->toDateTimeString();

        $end = Carbon::parse($input['end'])->toDateTimeString();

        $this->queue(
            function () use ($start, $end, $user) {
                \Artisan::call('crims:automatic_withdrawals_summary', [
                    'start' => $start,
                    'end' => $end,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The automatic withdrawals summary will be generated and sent to you shortly via email');

        return \Redirect::back();
    }
}
