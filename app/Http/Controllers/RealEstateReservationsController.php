<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\RealEstateSalesAgreement;
use App\Cytonn\Models\ReservationForm;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Forms\JointHolderForm;
use Cytonn\Investment\JointInvestorAddCommand;
use Cytonn\Realestate\Forms\ForfeitUnitForm;
use Cytonn\Realestate\Forms\UploadLooForm;
use Cytonn\Realestate\Forms\UploadSalesAgreementForm;
use Cytonn\Realestate\Sale\LetterOfOffer;
use Cytonn\Realestate\Sale\SalesAgreement;

/**
 * Date: 03/06/2016
 * Time: 10:36 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateReservationsController extends Controller
{
    use \Laracasts\Commander\Events\DispatchableTrait;
    /**
     * @var JointHolderForm
     */
    protected $jointHolderForm;

    /**
     * @var UploadLooForm
     */
    protected $uploadLooForm;

    /**
     * @var $uploadSalesAgreementForm
     */
    protected $uploadSalesAgreementForm;

    /**
     * @var ReservationForm
     */
    protected $reservationForm;

    /**
     * @var ForfeitUnitForm
     */
    protected $forfeitUnitform;

    /**
     * RealEstateReservationsController constructor.
     *
     * @param JointHolderForm $jointHolderForm
     * @param UploadLooForm $uploadLooForm
     * @param ReservationForm $reservationForm
     * @param ForfeitUnitForm $forfeitUnitForm
     * @param UploadSalesAgreementForm $agreementForm
     */
    public function __construct(
        JointHolderForm $jointHolderForm,
        UploadLooForm $uploadLooForm,
        ReservationForm $reservationForm,
        ForfeitUnitForm $forfeitUnitForm,
        UploadSalesAgreementForm $agreementForm
    ) {
        $this->jointHolderForm = $jointHolderForm;
        $this->uploadLooForm = $uploadLooForm;
        $this->reservationForm = $reservationForm;
        $this->forfeitUnitform = $forfeitUnitForm;
        $this->uploadSalesAgreementForm = $agreementForm;

        parent::__construct();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $form = ReservationForm::findOrFail($id);
        $titles = function ($id) {
            return Title::find($id);
        };

        return view(
            'realestate.reservations.show',
            [
                'form' => $form, 'client' => $form->client, 'joints' => $form->jointClients, 'titles' => $titles
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getEdit($id)
    {
        $form = ReservationForm::findOrFail($id);

        $clientTypes = ClientType::all()->lists('name', 'id');
        $titles = Title::all()->lists('name', 'id');
        $countries = Country::all()->lists('name', 'id');

        return view(
            'realestate.projects.units.reserve',
            [
                'form' => $form, 'client' => $form->client,
                'joints' => $form->jointClients,
                'clientTypes' => $clientTypes, 'titles' => $titles,
                'countries' => $countries
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function putUpdate($id)
    {
        $form = ReservationForm::findOrFail($id);

        $input = \Request::except('_token');

        $this->reservationForm->validate($input);

        $input['existing_client'] = \Request::get('new_client') && \Request::get('client_code');

        $input = array_except($input, ['new_client', 'existing_client']);

        $form->update($input);

        $input['employer_name'] = $input['corporate_registered_name'];
        $input['employer_address'] = $input['registered_address'];

        $form->client->contact->update(
            [
                'firstname' => $input['firstname'],
                'lastname' => $input['lastname'],
                'middlename' => $input['middlename'],
            ]
        );

        $input = array_except(
            $input,
            ['title_id', 'firstname', 'middlename', 'lastname', 'corporate_registered_name',
                'corporate_trade_name', 'registered_address', 'registration_number',
                'email', 'nationality_id', 'client_code']
        );

        $form->client->update($input);

        $form->save();

        \Flash::success('The reservation has been updated.');

        return \redirect('/dashboard/realestate/reservations/show/' . $id);
    }

    /**
     * @param $id
     * @param null $jointId
     * @return mixed
     */
    public function getJoint($id, $jointId = null)
    {
        $reservation = ReservationForm::findOrFail($id);
        $titles = Title::all()->lists('name', 'id');
        $gender = Gender::all()->lists('abbr', 'id');
        $countries = Country::all()->lists('name', 'id');
        $contactMethods = ContactMethod::all()->lists('description', 'id');
        is_null($jointId) ? $joint = new ClientJointDetail() : $joint = ClientJointDetail::findOrFail($jointId);

        return view(
            'realestate.reservations.joint_form',
            [
                'joint' => $joint, 'client' => $reservation->client, 'form' => $reservation,
                'titles' => $titles, 'gender' => $gender,
                'countries' => $countries, 'contactMethods' => $contactMethods
            ]
        );
    }

    /**
     * @param $id
     * @param null $jointId
     */
    public function postJoint($id, $jointId = null)
    {
        $input = \Request::except('_token');

        $this->jointHolderForm->validate($input);

        $reservation = ReservationForm::findOrFail($id);

        $input['reservation_form_id'] = $reservation->id;

        $input['joint_id'] = $jointId;

        $this->execute(JointInvestorAddCommand::class, ['data' => $input]);

        \Flash::message('Joint holder successfully added');

        return redirect('/dashboard/realestate/reservations/show/' . $id);
    }


    /**
     * @param $holdingId
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postUploadSalesAgreement($holdingId)
    {
        $input = \Request::except('_token', '_method');
        $this->uploadSalesAgreementForm->validate(\Request::all());

        $holding = UnitHolding::findOrFail($holdingId);

        $file = \Request::file('file');

        $date_sent_to_lawyer = \Request::get('date_sent_to_lawyer');

        is_null($holding->salesAgreement) ?
            $salesAgreement = RealEstateSalesAgreement::make($holding, $date_sent_to_lawyer)
            : $salesAgreement = $holding->salesAgreement;

        if (\Request::hasFile('file')) {
            $document = (new SalesAgreement($salesAgreement))
                ->upload(file_get_contents($file), $file->getClientOriginalExtension(), $input['title']);
            $input['document_id'] = $document->id;
        }

        unset($input['file']);
        $input['sales_agreement_id'] = $salesAgreement->id;

        ClientTransactionApproval::make(
            $salesAgreement->holding->client_id,
            'sales_agreement_uploading',
            $input
        );

        \Flash::message('The sales agreement has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postForfeit($id)
    {
        $input = \Request::except('_token');

        $this->forfeitUnitform->validate($input);

        $holding = UnitHolding::findOrFail($id);
        $input['holding_id'] = $id;

        if ($input['amount'] > $holding->totalPayments() - $holding->refundedAmount()) {
            throw new ClientInvestmentException('The amount refunded cannot be more than the the amount paid');
        }

        ClientTransactionApproval::make($holding->client_id, 'real_estate_unit_forfeiture', $input);

        \Flash::message('The forfeiture has been saved for approval');

        return \Redirect::back();
    }

    public function postReverseCancelled($id)
    {
        $input = \Request::except('_token');

        $holding = UnitHolding::findOrFail($id);
        $input['holding_id'] = $id;

        $isCurrentlyReservedByAnotherClient =
            UnitHolding::where('unit_id', $holding->unit_id)->where('active', 1)->exists();
        if ($isCurrentlyReservedByAnotherClient) {
            \Flash::error('You cannot reverse this forfeiture because another client has already reserved it.');
            return \Redirect::back();
        }

        ClientTransactionApproval::make($holding->client_id, 'real_estate_reverse_forfeiture', $input);

        \Flash::message('The request to reverse the forfeiture has been saved for approval');

        return \Redirect::back();
    }

    public function postTransfer($id)
    {
        $holding = UnitHolding::findOrFail($id);
        $input = \Request::only('client_id');
        $input['holding_id'] = $holding->id;
        ClientTransactionApproval::make($holding->client_id, 'real_estate_unit_transfer', $input);

        \Flash::message('The unit transfer has been saved for approval');
        return \Redirect::back();
    }

    /**
     * Delete a unit holding
     *
     * @param  $id
     * @return mixed
     */
    public function postDelete($id)
    {
        $input = \Request::except('_token');

        $holding = UnitHolding::findOrFail($id);
        $input['holding_id'] = $id;

        ClientTransactionApproval::make($holding->client_id, 'real_estate_delete_holding', $input);

        \Flash::message('Deletion of the unit holding has been saved for approval');

        return \Redirect::back();
    }

    public function getGenerateAndViewLoo($holdingId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        return (new LetterOfOffer())->generateLoo($holding)->stream();
    }

    public function getGenerateAndViewLooWord($holdingId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        return (new LetterOfOffer())->generateLoo($holding, 'word')->stream();
    }

    public function getGenerateAndUploadLoo($holdingId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        if (!$holding->loo->pm_approval_id) {
            \Flash::warning('The LOO should be approved before saving');

            return \Redirect::back();
        }

        $loo = (new LetterOfOffer())->createLoo($holding);


        if ($loo->document) {
            \Flash::success('The LOO was generated and saved');
        } else {
            \Flash::error('The document could not be generated');
        }

        return \Redirect::back();
    }
}
