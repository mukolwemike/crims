<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Presenters\ClientPresenter;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redirect;

/**
 * Class SharesPurchaseOrdersController
 */
class SharesPurchaseOrdersController extends Controller
{

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('shares.purchases.index', ['title' => 'Share Purchase Orders']);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function getShareholders($share_holder_id)
    {
        $holder = ShareHolder::findOrFail($share_holder_id);

        $current_shares = $holder->shareHoldings()->sum('number');
        $commissionRecepient = (new CommissionRecepient())->repo->getRecipientsForSelect();

        return view(
            'shares.purchases.shareholder',
            [
                'title' => ClientPresenter::presentFullNames($holder->client_id) . ' - Shares Purchase Orders',
                'holder' => $holder, 'current_shares' => $current_shares, 'commissionRecepient' => $commissionRecepient
            ]
        );
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function getShow($order_id)
    {
        $order = SharesPurchaseOrder::findOrFail($order_id);
        
        $buyer = $order->buyer;

        $bought = SharePurchases::where('shares_purchase_order_id', $order->id)->sum('number');

        $sales = SharesSalesOrder::where('account_id', $order->account_id)
            ->where('matched', null)
            ->get();

        $commissionRecepient = (new CommissionRecepient())->repo->getRecipientsForSelect();

        return view('shares.purchases.show', [
            'title' => 'Shares Purchase Order #' . $order_id,
            'buyer' => $buyer,
            'order' => $order,
            'sales' => $sales,
            'bought' => $bought,
            'recipients' => $commissionRecepient
        ]);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function postMatch($order_id)
    {
        $order = SharesPurchaseOrder::findOrFail($order_id);

        $input['sales_order_id'] = \Request::get('sales_order_id');
        $input['sold_date'] = \Request::get('sold_date');
        $input['buyer_id'] = $order->buyer->id;
        $input['purchase_order_id'] = $order->id;

        ClientTransactionApproval::add(
            [
                'client_id' => $order->buyer->id,
                'transaction_type' => 'match_purchase_to_sale',
                'payload' => $input
            ]
        );

        Flash::message('Match for shares purchase order has been saved for approval');

        return redirect('/dashboard/shares/purchases/shareholders/' . $order->buyer_id);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function postOrder($share_holder_id)
    {
        $buyer = ShareHolder::findOrFail($share_holder_id);

        $input = \Request::except('_token');

        $input['good_till_filled_cancelled'] = (bool)isset($input['good_till_filled_cancelled']);

        if ($input['good_till_filled_cancelled']) {
            $input['expiry_date'] = null;
        }

        $total_purchase_amount = (double)$input['number'] * $input['price'];

        if ($total_purchase_amount > $buyer->sharePaymentsBalance() + $total_purchase_amount) {
            Flash::error('The buyer does not have enough funds to make a shares purchase order.');

            return Redirect::back();
        }

        ClientTransactionApproval::add(
            [
                'client_id' => $buyer->client_id,
                'transaction_type' => 'make_shares_purchase_order',
                'payload' => $input
            ]
        );

        Flash::message('Shares purchase order has been saved for approval');

        return redirect('/dashboard/shares/shareholders/show/' . $buyer->id);
    }

    /**
     * @param $share_holder_id
     * @return mixed
     */
    public function postBuyEntityShares($share_holder_id)
    {
        $buyer = ShareHolder::findOrFail($share_holder_id);

        $input = \Request::except('_token');

        $entity = $buyer->entity;

        $price = isset($input['exemption']) ?
            $input['exemption'] = abs($input['exemption'])
            : $entity->sharePrice($input['request_date']);

        $cost = $input['number'] * $price;

        // Check if the buyer has enough funds to buy the shares
        if ((double)$cost > $buyer->sharePaymentsBalance() + $cost) {
            Flash::error('The buyer does not have enough funds to buy the number of shares.');

            return Redirect::back();
        }

        // Check if the entity has enough shares to meet the request
        if ($input['number'] > $entity->remainingShares($input['request_date'])) {
            Flash::error('The entity does not have enough shares to match the request.');

            return Redirect::back();
        }

        ClientTransactionApproval::add(
            [
                'client_id' => $buyer->client_id,
                'transaction_type' => 'buy_entity_shares',
                'payload' => $input
            ]
        );

        Flash::message('Purchase of entity shares has been saved for approval');

        return redirect('/dashboard/shares/shareholders/show/' . $buyer->id);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function postCancel($order_id)
    {
        $order = SharesPurchaseOrder::findOrFail($order_id);
        $input['order_id'] = $order->id;

        ClientTransactionApproval::add(
            [
                'client_id' => $order->buyer->client_id,
                'transaction_type' => 'cancel_shares_purchase_order',
                'payload' => $input
            ]
        );

        Flash::message('The request to cancel the shares purchase order has been sent for approval');

        return Redirect::back();
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function postReverseFromEntity($order_id)
    {
        $order = SharesPurchaseOrder::findOrFail($order_id);

        if (!$order->boughtFromEntity()) {
            Flash::error('The shares were not purchased from an entity');

            return Redirect::back();
        }

        $input['order_id'] = $order->id;

        ClientTransactionApproval::add(
            [
                'client_id' => $order->buyer->client_id,
                'transaction_type' => 'reverse_shares_purchase_from_entity',
                'payload' => $input
            ]
        );

        Flash::message('The request to cancel the shares purchase order has been sent for approval');

        return Redirect::back();
    }

    /**
     * @param $order_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $order_id)
    {
        $order = SharesPurchaseOrder::findOrFail($order_id);

        $input = $request->all();
        $input['existing_commission_recipient'] = $order->commission_recipient_id;
        $input['existing_commission_rate'] = $order->commission_rate;
        $input['exiting_commission_start_date'] = $order->commission_start_date;
        $input['order_id'] = $order->id;

        ClientTransactionApproval::add([
            'client_id' => $order->buyer->client->id,
            'transaction_type' => 'edit_commission_details_purchase_order',
            'payload' => $input
        ]);

        Flash::message('Commission Edit for shares purchase order has been saved for approval');

        return Redirect::back();
    }
}
