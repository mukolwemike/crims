<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Carbon\Carbon;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalType;
use Illuminate\Support\Facades\Request;

/**
 * Date: 21/12/2015
 * Time: 8:59 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */
class ActivityController extends Controller
{

    /**
     * ActivityController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        is_null(Request::get('start'))
            ? $start = \Carbon\Carbon::now()->subMonth() : $start = new \Carbon\Carbon(Request::get('start'));

        is_null(Request::get('end'))
            ? $end = Carbon::today()->endOfDay() : $end = Carbon::parse(Request::get('end'))->endOfDay();


        $institutions = PortfolioInvestor::all()->lists('name');

        $portfolio_transaction_types = PortfolioTransactionApproval::groupBy('transaction_type')
            ->pluck('transaction_type')->toArray();

        foreach ($portfolio_transaction_types as $key => $type) {
            $portfolio_transaction_types[$type] = ucfirst(str_replace('_', ' ', $type));
            unset($portfolio_transaction_types[$key]);
        }

        $client_transaction_types = ClientTransactionApproval::groupBy('transaction_type')
            ->pluck('transaction_type')->toArray();

        foreach ($client_transaction_types as $key => $type) {
            $client_transaction_types[$type] = ucfirst(str_replace('_', ' ', $type));
            unset($client_transaction_types[$key]);
        }

        return view('investment.activity.index', ['title'=>'Activity Log',  'start'=>$start->toDateString(),
            'end'=>$end->toDateString(), 'portfolio_transaction_types'=>$portfolio_transaction_types,
            'client_transaction_types'=>$client_transaction_types, 'institutions'=>$institutions]);
    }
}
