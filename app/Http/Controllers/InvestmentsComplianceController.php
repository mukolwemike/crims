<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Investment\Summary\Analytics;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

/**
 * Class InvestmentsComplianceController
 */
class InvestmentsComplianceController extends Controller
{

    /**
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;

    /**
     * InvestmentsComplianceController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('investment.compliance.index', ['title' => 'Client Compliance']);
    }

    /**
     * @return mixed
     */
    public function graph()
    {
        $date_ranges = [];
        $compliance_data = [];

        $date = Carbon::today()->subWeeks(4);

        while ($date->lte(Carbon::today())) {
            $analytics = (new Analytics($this->fundManager(), $date))
                ->setBaseCurrency(Currency::where('code', 'KES')->first());

            $date_ranges[] = DatePresenter::formatDate($date->toDateString());

            $compliance_data[] = $analytics->compliantAum($date, true)
                / $analytics->aum($date) * 100;

            $date->addWeek();
        }

        $chart = \Charts::multi('line', 'material')
            ->title("Graph of Client Non-Compliance")
            ->dimensions(0, 500)
            ->template("material")
            ->dataset('Non Compliance', $compliance_data)
            ->labels($date_ranges);

        return view('investment.compliance.graph', ['title' => 'Graph of Client Non Compliance', 'chart' => $chart]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function details($id)
    {
        $client = Client::findOrFail($id);

        $checklist = $client->repo->complianceChecklist();
        $checked_items = ClientUploadedKyc::where('client_id', $client->id)->pluck('kyc_id');

        $clientName = \Cytonn\Presenters\ClientPresenter::presentJointFullNames($id);
        $documents = $client->documents;

        $products = Product::all()->lists('name', 'id');
        $products = [0 => 'All Products'] + $products;

        $projects = Project::all()->lists('name', 'id');
        $projects = [0 => 'All Projects'] + $projects;

        $portfolioInvestors = [0 => 'All Investors'] + PortfolioInvestor::pluck('name', 'id')->toArray();
        $document_types = DocumentType::where('direct_upload', true)->get()->lists('name', 'id');

        $appDocuments = null;
        foreach ($client->applications as $app) {
            $appDocuments = $app->form ? $app->form->filledDocuments : null;
        }

        return view(
            'investment.compliance.details',
            [
                'title' => 'Client Compliance',
                'client' => $client,
                'checklist' => $checklist,
                'name' => $clientName,
                'checked_items' => $checked_items,
                'products' => $products,
                'projects' => $projects,
                'portfolioInvestors' => $portfolioInvestors,
                'document_types' => $document_types,
                'documents' => $documents,
                'appDocuments' => $appDocuments
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function uploadKYCs($id)
    {
        $this->authorizer->checkAuthority('checkkyc');

        $input = Request::only(['doc']);

        if (is_null($input['doc'])) {
            \Flash::success('Kindly add documents to upload');
            return Redirect::back();
        }

        $documents = Collection::make(Request::only(['doc']))
            ->transpose()
            ->filter(
                function ($document) {
                    return !is_null($document['doc']);
                }
            )->toArray();

        $client = Client::findOrFail(Crypt::decrypt(Request::get('client_id')));

        (new ClientUploadedKyc())->upload($documents, $client);

        \Flash::success('Compliance document(s) have been uploaded.');
        return Redirect::back();
    }

    public function uploadComplianceForm()
    {
        $this->authorizer->checkAuthority('checkkyc');

        $doc = Request::only(['doc']);

        if (is_null($doc['doc'])) {
            \Flash::error('No form was attached. Kindly attach a form to upload.');
            return Redirect::back();
        }

        $kycs_in_doc = Collection::make(Request::only(['kycs']));

        if (!$kycs_in_doc['kycs']) {
            \Flash::error('No KYC document was checked from the list.');
            return back()->withInput();
        } else {
            $kycs_in_doc = $kycs_in_doc->transpose()->toArray();
        }

        $client = Client::findOrFail(Crypt::decrypt(Request::get('client_id')));

        $document = (new ClientUploadedKyc())->uploadForm($doc, $kycs_in_doc, $client);

        $input['document_id'] = $document->id;
        $input['kycs'] = $kycs_in_doc;
        $input['file_name'] = $doc['doc']->getClientOriginalExtension();

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'transaction_type' => 'upload_compliance_form',
                'payload' => $input
            ]
        );


        \Flash::message('Compliance document form has been sent for approval.');
        return Redirect::back();
    }

    public function export()
    {
        $user = Auth::user()->id;
        $start = request()->get('start_date');
        $end = request()->get('end_date');

        $this->queue(
            function () use ($user, $start, $end) {
                Artisan::call('client:client-compliance', ['user_id' => $user, 'start' => $start, 'end' => $end]);
            }
        );

        \Flash::message('The client report will be mailed to you soon');

        return \Redirect::back();
    }
}
