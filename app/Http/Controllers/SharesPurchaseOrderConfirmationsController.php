<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesPurchaseOrderConfirmation;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Mailers\Shares\SharesPurchaseOrderConfirmationMailer;
use Cytonn\Shares\Reporting\SharesPurchaseOrderConfirmationGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Class SharesPurchaseOrderConfirmationsController
 */
class SharesPurchaseOrderConfirmationsController extends Controller
{

    /**
     * Used to check for authorization
     *
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;


    /**
     * SharesPurchaseOrderConfirmationsController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        parent::__construct();
    }

    /**
     * Show the shares purchase order confirmations' grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        // TODO
    }

    /**
     * Show the details of the purchase order confirmation
     *
     * @param  $purchase_order_id
     * @return mixed
     */
    public function getShow($purchase_order_id)
    {
        $purchase_order = SharesPurchaseOrder::findOrFail($purchase_order_id);
        $previousConfirmations =
            SharesPurchaseOrderConfirmation::where('share_purchase_order_id', $purchase_order->id)
                ->latest()
                ->get();
        $is_poc_sent =
            (bool)SharesPurchaseOrderConfirmation::where('share_purchase_order_id', $purchase_order->id)
                ->count();
        return view(
            'shares.purchases.confirmations.show',
            [
                'title' => 'Shares Purchase Order Confirmation',
                'purchase_order' => $purchase_order,
                'buyer' => $purchase_order->buyer,
                'previousConfirmations' => $previousConfirmations,
                'is_poc_sent' => $is_poc_sent
            ]
        );
    }

    /**
     * @param $purchase_order_id
     * @return mixed
     */
    public function postSend($purchase_order_id)
    {
        $purchase_order = SharesPurchaseOrder::findOrFail($purchase_order_id);
        $sender = Auth::user();
        $number = $purchase_order->matchedSaleOrders()->get()->sum(
            function (SharesSalesOrder $order) {
                return $order->pivot->number;
            }
        );

        $successful = $number > 0;

        $confirmation =
            (new SharesPurchaseOrderConfirmationGenerator())->generate($purchase_order, $sender, $successful)->stream();

        (new SharesPurchaseOrderConfirmationMailer())->sendEmail($purchase_order, $confirmation);

        $payload = [
            'number' => $number,
            'sender' => $sender,
            'share_purchase_order_id' => $purchase_order->id,
            'date' => $purchase_order->request_date
        ];

        $data = [
            'share_purchase_order_id' => $purchase_order->id,
            'successful' => $successful,
            'payload' => $payload
        ];

        SharesPurchaseOrderConfirmation::add($data);

        \Flash::success('The Shares Purchase Order Confirmation has been sent');
        return Redirect::back();
    }

    /**
     * @param $purchase_order_id
     * @return mixed
     */
    public function getPreview($purchase_order_id)
    {
        $purchase_order = SharesPurchaseOrder::findOrFail($purchase_order_id);
        $sender = Auth::user();
        $number = $purchase_order->matchedSaleOrders()->get()->sum(
            function (SharesSalesOrder $order) {
                return $order->pivot->number;
            }
        );

        $successful = $number > 0;

        $confirmation = new SharesPurchaseOrderConfirmationGenerator();

        return $confirmation->generate($purchase_order, $sender, $successful)->stream();
    }

    /**
     * @param $confirmation_id
     * @return mixed
     */
    public function getPreviousPreview($confirmation_id)
    {
        $previousConfirmation = SharesPurchaseOrderConfirmation::findOrFail($confirmation_id);
        $purchase_order = $previousConfirmation->purchaseOrder;
        $sender = Auth::user();

        $confirmation = new SharesPurchaseOrderConfirmationGenerator();

        return $confirmation->generate($purchase_order, $sender)->stream();
    }
}
