<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('feedback.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
            'subject'=>'required|min:10|max:255',
            'description'=>'required|min:5',
            'file.*' => 'nullable|max:5000'
            ]
        );

        $http = new Client();

        $parameters = [
            [ 'name' => 'subject',  'contents' =>$request->input('subject')],
            [ 'name' => 'description', 'contents' => $request->input('description')],
            [ 'name' => 'project_id', 'contents' => 6 ],
            [ 'name' => 'client_id', 'contents' => \Auth::id()],
            [ 'name' => 'client_email', 'contents' => \Auth::user() ? \Auth::user()->email : null],
            [ 'name' => 'source', 'contents' => 'CRIMS']
        ];

        $data['multipart'] = $parameters;

        if ($file = $request->file('file')) {
            $file_path = $file->getPathname();
            $file_mime = $file->getmimeType();
            $file_org  = $file->getClientOriginalName();
            $data['multipart'] = array_merge(
                $data['multipart'],
                [
                [
                    'name' => 'file',
                    'filename' => $file_org,
                    'Mime-Type'=> $file_mime,
                    'contents' => fopen($file_path, 'r')
                ]
                ]
            );
        }
        
        try {
            $http->post(getenv('PM_FEEDBACK_URL'), $data);
        } catch (RequestException $e) {
            \Flash::error('An error occurred while submitting your feedback. Please try again later.');
            return redirect()->back();
        }

        \Flash::success('Your feedback was successfully submitted.');
        return redirect('/Dashboard');
    }
}
