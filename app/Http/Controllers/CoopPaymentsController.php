<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ProductPlan;
use Cytonn\Coop\Payments\CoopPaymentRepository;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Notifier\FlashNotifier;
use Cytonn\Shares\ShareHoldingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use \Flash;

/**
 * Class CoopPaymentsController
 */
class CoopPaymentsController extends Controller
{
    protected $coopPaymentRepository;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->coopPaymentRepository = new CoopPaymentRepository();
        parent::__construct();
    }

    public function getIndex()
    {
        return view('coop.payments.index', ['title'=>'Coop Payments']);
    }

    /**
     * @return mixed
     */
    public function postMakePayment(Request $request)
    {
        $input = $request->except('_token');
        $input['type'] = 'payment';

        ClientTransactionApproval::add([ 'transaction_type'=>'make_coop_payment',
            'client_id'=>$input['client_id'], 'payload'=>$input ]);

        Flash::message('Coop payment have been saved for approval');

        return Redirect::back();
    }

    /**
     * @return mixed
     */
    public function postInvestInProduct(Request $request)
    {
        $input = $request->except('_token');
        $clientInvestmentType = ClientInvestmentType::where('name', 'investment')->first();
        $input['investment_type_id'] = $clientInvestmentType->id;
        $input['type'] = 'product';

        ClientTransactionApproval::add(
            [ 'transaction_type'=>'make_coop_product_investment', 'client_id'=>$input['client_id'], 'payload'=>$input ]
        );

        \Flash::message('Coop product investment has been saved for approval!');

        return \Redirect::back();
    }

    public function postTopupInvestment(Request $request)
    {
        $input = $request->except('_token');
        $clientInvestmentType = ClientInvestmentType::where('name', 'topup')->first();
        $input['investment_type_id'] = $clientInvestmentType->id;
        $input['type'] = 'product';
        $input['product_id'] = ProductPlan::findOrFail($input['product_plan_id'])->product_id;

        if ((new InvestmentsRepository())->checkIfTopUpIsDuplicate($input)) {
            Flash::error('Duplicate! This Top-up already exists!');
            return Redirect::back();
        }

        ClientTransactionApproval::add(
            [ 'transaction_type'=>'make_coop_investment_topup',
            'client_id'=>$input['client_id'], 'payload'=>$input ]
        );

        \Flash::message('Coop investment top-up has been saved for approval!');

        return \Redirect::back();
    }

    public function postBuyShares(Request $request)
    {
        $input = $request->except('_token');

        if ((new ShareHoldingRepository())->checkIfSharePurchaseIsDuplicate($input)) {
            Flash::error('Duplicate! This share purchase already exists!');
            return Redirect::back();
        }

        ClientTransactionApproval::add(
            [ 'transaction_type'=>'buy_coop_shares',
            'client_id'=>$input['client_id'], 'payload'=>$input ]
        );

        Flash::message('Purchase of Coop shares have been saved for approval!');

        return Redirect::back();
    }
}
