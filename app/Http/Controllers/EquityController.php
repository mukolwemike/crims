<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Jobs\Portfolio\PorfolioEquitiesReport;
use Carbon\Carbon;
use Cytonn\Api\Transformers\EquityDividendTransformer;
use Cytonn\Portfolio\Equities\PortfolioValuation;
use Illuminate\Http\Request;

class EquityController extends Controller
{
    public function index()
    {
        $valuation = new PortfolioValuation($this->fundManager(), Carbon::today());

        $fundManager = $this->fundManager();

        $unitFunds = UnitFund::where('fund_manager_id', $fundManager->id)
            ->pluck('name', 'id')
            ->toArray();

        return view('portfolio.equities.index', [
            'title' => 'Portfolio Equities',
            'valuation' => $valuation,
            'unitFunds' => $unitFunds
        ]);
    }

    public function exportReport(Request $request)
    {
        $input = $request->all();

        $fundId = (isset($input['unit_fund_id'])) ? $input['unit_fund_id'] : null;

        dispatch(new PorfolioEquitiesReport($fundId, \Auth::user()));

        \Flash::success('The report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function viewDividends($security_id)
    {
        $security = (new PortfolioSecurity())->findOrFail($security_id);

        return view(
            'portfolio.equities.dividends',
            ['title' => $security->name . ' - Dividends', 'security' => $security]
        );
    }

    public function showDividend($securityId, $dividendId)
    {
        $security = PortfolioSecurity::findOrFail($securityId);

        $dividend = EquityDividend::findorFail($dividendId);

        $dividend = (new EquityDividendTransformer())->transform($dividend);

        return view('portfolio.equities.show_dividend', [
            'dividend' => $dividend,
            'security' => $security
        ]);
    }

    public function receiveDividend($securityId, $dividendId)
    {
        $security = PortfolioSecurity::findOrFail($securityId);

        $input = request()->except('_token');

        $input['security_id'] = $securityId;

        $input['dividend_id'] = $dividendId;

        PortfolioTransactionApproval::add([
            'institution_id' => $security->portfolio_investor_id,
            'transaction_type' => 'receive_equity_share_dividend',
            'payload' => $input
        ]);

        \Flash::message('Saved successfully for approval');

        return \Redirect::back();
    }

    /*
     * Supply the view to create a view to reverse the shares buy
     */
    public function showReverseBuyShares($id)
    {
        $equityHolding = EquityHolding::findOrFail($id);

        return view(
            'portfolio.equities.share_buy_details',
            [
                'equityHolding' => $equityHolding
            ]
        );
    }

    /*
     * Submit the reverse shares buy
     */
    public function submitReverseBuyShares(Request $request, $id)
    {
        $input = $request->except(['_token']);

        $equityHolding = EquityHolding::findOrFail($input['id']);

        PortfolioTransactionApproval::add(
            ['institution_id' => $equityHolding->security->portfolio_investor_id,
                'transaction_type' => 'reverse_buy_equity_share', 'payload' => $input]
        );

        \Flash::message('The equity share purchase reversal has been saved for approval');

        return \Redirect::back();
    }

    /*
     * Edit the trade/settlement purchase dates
     */
    public function editDatesBuyShares(Request $request, $id)
    {
        $input = $request->except(['_token']);

        $equityHolding = EquityHolding::findOrFail($id);

        $input['id'] = $id;

        PortfolioTransactionApproval::add(
            ['institution_id' => $equityHolding->security->portfolio_investor_id,
                'transaction_type' => 'edit_date_buy_equity_share', 'payload' => $input]
        );

        \Flash::message('The equity share dates edit has been saved for approval');

        return \Redirect::back();
    }

    /*
     * Edit the trade/settlement sales dates
     */
    public function editDatesSaleShares(Request $request, $id)
    {
        $input = $request->except(['_token']);

        $equitySale = EquityShareSale::findOrFail($id);

        $input['id'] = $id;

        PortfolioTransactionApproval::add(
            ['institution_id' => $equitySale->security->portfolio_investor_id,
                'transaction_type' => 'edit_date_sale_equity_share', 'payload' => $input]
        );

        \Flash::message('The equity share date edit has been saved for approval');

        return \Redirect::back();
    }


    /*
     * Supply the view to create a view to reverse the shares sale
     */
    public function showSaleDetails($id)
    {
        $equitySale = EquityShareSale::findOrFail($id);

        return view(
            'portfolio.equities.share_sale_details',
            [
                'equitySale' => $equitySale
            ]
        );
    }

    /*
     * Submit the reverse shares sale
     */
    public function submitReverseSellShares(Request $request, $id)
    {
        $input = $request->except(['_token']);

        $equityHolding = EquityShareSale::findOrFail($input['id']);

        PortfolioTransactionApproval::add(
            ['institution_id' => $equityHolding->security->portfolio_investor_id,
                'transaction_type' => 'reverse_sell_equity_share', 'payload' => $input]
        );

        \Flash::message('The equity share sale reversal has been saved for approval');

        return \Redirect::back();
    }

    public function targetPrice($id)
    {
        return view('portfolio.securities.show');
    }

    public function marketPrice($id)
    {
        return view('portfolio.securities.show');
    }
}
