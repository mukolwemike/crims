<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Http\Controllers\Shares;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class ShareCommissionController
 * @package App\Http\Controllers\Shares
 */
class ShareCommissionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('shares.commissions.index');
    }

    public function recipientDetails($id)
    {
        $recipient = CommissionRecepient::findOrFail($id);

        $date = (new Carbon(\Request::get('date')));

        $commissionDates = (new BulkCommissionPayment())->commissionDates($date);

        return view('shares.commissions.recipient_details', [
            'start' => $commissionDates['start'],
            'end' => $commissionDates['end'],
            'recipient' => $recipient,
            'date' => $date
        ]);
    }

    /**
     * @param $bcpId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendBulkApproval($bcpId)
    {
        $bcp = BulkCommissionPayment::findOrFail($bcpId);
        $input['bcp_id'] = $bcp->id;

        $approval = ClientTransactionApproval::add([
            'client_id' => null,
            'transaction_type' => 'bulk_shares_commission_payments',
            'payload' => $input,
            'scheduled' => false
        ]);

        $bcp->sharesApproval()->associate($approval);
        $bcp->save();

        \Flash::message('Shares commission payments have been saved for approval');

        return \Redirect::back();
    }

    public function editSchedule(Request $request)
    {
        $schedule = SharesCommissionPaymentSchedule::findOrFail($request->get('id'));

        $input = $request->except(['_token']);

        $input['share_purchase_id'] = $schedule->share_purchase_id;
        $input['share_sale_id'] = $schedule->share_sale_id;
        $input['commission_recipient_id'] = $schedule->commission_recipient_id;

        ClientTransactionApproval::add([
            'client_id' => $schedule->sharePurchase->shareHolder->client_id,
            'transaction_type' => 'store_share_commission_payment_schedule',
            'payload' => $input
        ]);

        \Flash::message('Request to edit the share commission payment schedule has been saved for approval');

        return back();
    }

    public function addSchedule(Request $request)
    {
        $oldSchedule = SharesCommissionPaymentSchedule::findOrFail($request->get('schedule_id'));

        $input = $request->except(['_token', 'schedule_id']);

        $input['id'] = null;
        $input['share_purchase_id'] = $oldSchedule->share_purchase_id;
        $input['share_sale_id'] = $oldSchedule->share_sale_id;

        ClientTransactionApproval::add([
            'client_id' => $oldSchedule->sharePurchase->shareHolder->client_id,
            'transaction_type' => 'store_share_commission_payment_schedule',
            'payload' => $input
        ]);

        \Flash::message('Request to edit the share commission payment schedule has been saved for approval');

        return back();
    }
}
