<?php

namespace App\Http\Controllers;

use App\Cytonn\Dashboard\ClientDashboardGenerator;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\ConsentForm;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientFilledInvestmentApplicationJointHolders;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientStandingOrderType;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\GuardType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharesCategory;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Title;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Clients\ClientRules;
use Cytonn\Clients\Forms\AddBankDetailsForm;
use Cytonn\Clients\Forms\ConsentFormDataForm;
use Cytonn\Clients\Forms\CorporateClientForm;
use Cytonn\Clients\Forms\EmailIndemnityForm;
use Cytonn\Clients\Forms\IndividualClientForm;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\Storage\StorageInterface;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Forms\ContactPersonAddForm;
use Cytonn\Investment\Forms\JointHolderForm;
use Cytonn\Mailers\System\FlaggedRiskyClientMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Class ClientsController
 */
class ClientsController extends Controller
{
    use ClientRules;

    protected $authorizer;

    protected $mailer;

    protected $addBankDetailsForm;

    public function __construct(
        ContactPersonAddForm $contactPersonAddForm,
        AddBankDetailsForm $addBankDetailsForm,
        JointHolderForm $jointHolderForm,
        EmailIndemnityForm $emailIndemnityForm,
        IndividualClientForm $individualClientForm,
        CorporateClientForm $corporateClientForm,
        ConsentFormDataForm $consentFormDataForm,
        FlaggedRiskyClientMailer $flaggedRiskyClientMailer
    ) {
        parent::__construct();

        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->contactPersonAddForm = $contactPersonAddForm;
        $this->addBankDetailsForm = $addBankDetailsForm;
        $this->jointHolderForm = $jointHolderForm;
        $this->emailIndemnityForm = $emailIndemnityForm;
        $this->individualClientForm = $individualClientForm;
        $this->corporateClientForm = $corporateClientForm;
        $this->consentFormDataForm = $consentFormDataForm;
        $this->mailer = $flaggedRiskyClientMailer;

        $this->authorizer->checkAuthority('viewclients');
    }

    public function menu()
    {
        $gen = new ClientDashboardGenerator();

        $manager = $this->fundManager();

        return view('clients.menu', [
            'title' => 'Clients Menu',
            'gen' => $gen,
            'manager' => $manager
        ]);
    }

    public function summary()
    {
        $products = Product::forFundManager()->get();

        return view(
            'clients.summary',
            ['title' => 'Clients Summary', 'products' => $products, 'fundManager' => $this->fundManager()]
        );
    }

    public function index()
    {
        $this->authorizer->checkAuthority('viewclients');

        return view('clients.index', ['title' => 'Clients']);
    }

    /**
     * @param null $filename
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function iprsFilename($filename = null)
    {
        $type = DocumentType::where('slug', 'iprs_files')->remember(1000)->first();

        $filepath = $filename ? '/' . $type->path() . '/' . $filename : '/' . $type->path() . '/avatar.jpg';

        $filesystem = app(StorageInterface::class)->filesystem();

        $stream = $filesystem->readStream($filepath);

        return \Response::stream(
            function () use ($stream) {
                fpassthru($stream);
            },
            200,
            [
                "Content-Type" => $filesystem->getMimetype($filepath),
                "Content-Length" => $filesystem->getSize($filepath),
                "Content-disposition" => "inline; filename=\"" . basename($filepath) . "\"",
            ]
        );
    }

    public function details($id)
    {
        $client = Client::findOrFail($id);

        !$client->joint ? $jointHolders = null : $jointHolders = $client->jointHolders;

        $indemnities = $client->emailIndemnity;

        $consentForms = $client->consentForm;

        $coop_product_plans = $client->coopProductPlans->filter(
            function ($plan) {
                return !is_null($plan->product_id);
            }
        );

        $shares_product_plans = $client->coopProductPlans->filter(
            function ($plan) {
                return is_null($plan->product_id);
            }
        );

        $products = Product::pluck('name', 'id')->all();

        $share_holdings = ShareHolding::where('client_id', $id)->get();

        $custodial_accounts = CustodialAccount::all()->lists('account_name', 'id');

        $share_entities = SharesEntity::all()->lists('name', 'id');

        $share_categories = SharesCategory::all()->lists('name', 'id');

        $commissionrecepients = (new CommissionRecepient())->repo->getRecipientsForSelect();

        $signingMandates = [
            '0' => 'Singly',
            '1' => 'All to sign',
            '2' => 'Either to sign',
            '3' => 'At Least Two to sign'
        ];

        $contactArray = [
            'name' => $client->contact->present()->fullname,
            'id' => ''
        ];

        $signatureHolders = $this->signatureHolders($client, $contactArray);

        $signatureContacts = $this->signatureContactPersons($client, $contactArray);

        $signatureRelations = $this->signatureRelations($client, $contactArray);

        $banks = ClientBank::orderBy('name', 'asc')->get()->lists('name', 'id');

        $guardTypes = ['' => 'Select guard type'] + GuardType::pluck('name', 'id')->toArray();

        $portfolioInvestors = ['' => 'Select portfolio investment'] + PortfolioInvestor::whereHas(
            'investments',
            function ($q) {
                    $q->where('maturity_date', '>', Carbon::now()->toDateString());
            }
        )->pluck('name', 'id')->toArray();

        $clientAccounts = $client->bankAccounts->lists('account_name_and_number', 'id');

        $investments = $this->investmentData($client);

        $projects = $client->projects();

//        $orderTypes = ClientStandingOrderType::all();

        $orders = $client->standingOrders;

        $currencies = Currency::all()->lists('name', 'id');

        return view(
            'clients.details',
            [
                'title' => 'Client Details',
                'client' => $client,
                'jointHolders' => $jointHolders,
                'indemnities' => $indemnities,
                'consentForms' => $consentForms,
                'coop_product_plans' => $coop_product_plans,
                'shares_product_plans' => $shares_product_plans,
                'custodial_accounts' => $custodial_accounts,
                'share_entities' => $share_entities,
                'share_categories' => $share_categories,
                'share_holdings' => $share_holdings,
                'commissionrecepients' => $commissionrecepients,
                'products' => $products,
                'signingMandates' => $signingMandates,
                'signatureHolders' => $signatureHolders,
                'signatureContactPersons' => $signatureContacts,
                'signatureRelationshipPersons' => $signatureRelations,
                'guardTypes' => $guardTypes,
                'portfolioInvestors' => $portfolioInvestors,
                'banks' => $banks,
                'currencies' => $currencies,
                'clientAccounts' => $clientAccounts,
                'investments' => $investments,
                'projects' => $projects,
                'orders' => $orders,
                'iprs_validations' => $client->iprsValidations
            ]
        );
    }

    public function signatureHolders(Client $client, $contactArray)
    {
        $signatureHolders = [];

        if ($client->jointHolders) {
            foreach ($client->jointHolders as $holder) {
                $h = [
                    'name' => $holder->details->present()->fullname,
                    'id' => $holder->id
                ];

                array_push($signatureHolders, $h);
            }
        }

//        array_push($signatureHolders, $contactArray);

        return $signatureHolders;
    }

    public function signatureContactPersons(Client $client, $contactArray)
    {
        $signatureContacts = [];

        if ($client->contactPersons) {
            foreach ($client->contactPersons as $contactPerson) {
                $h = [
                    'name' => $contactPerson->name,
                    'id' => $contactPerson->id
                ];

                array_push($signatureContacts, $h);
            }
        }

        array_push($signatureContacts, $contactArray);

        return $signatureContacts;
    }

    public function signatureRelations(Client $client, $contactArray)
    {
        $signatureRelations = [];

        $relationshipPerson = $client->relationshipperson;

        if (!is_null($relationshipPerson)) {
            $person = [
                'name' => $relationshipPerson->name,
                'id' => $relationshipPerson->id
            ];

            array_push($signatureRelations, $person);
        }

        return $signatureRelations;
    }

    public function investmentData(Client $client)
    {
        return $client->investments()
            ->active()
            ->get()
            ->map(function ($investment) {
                $description = 'Investment of ' . $investment->amount . ' on '
                    . $investment->product->name . ' from '
                    . DatePresenter::formatDate($investment->invested_date) . ' to '
                    . DatePresenter::formatDate($investment->maturity_date);

                return [
                    'id' => $investment->id,
                    'name' => $description
                ];
            });
    }

    public function indemnity(Request $request, $clientId, $id = null)
    {
        $input = $request->except('_token');

        $this->authorizer->checkAuthority('add_email_indemnity');

        $this->emailIndemnityForm->validate($request->all());

        Client::findOrFail($clientId);

        $file = $request->file('file');

        unset($input['file']);

        $doc =
            Document::make(file_get_contents($file), $file->getClientOriginalExtension(), 'email_indemnity');

        $input['document_id'] = $doc->id;
        $input['client_id'] = $clientId;

        ClientTransactionApproval::make($clientId, 'add_email_indemnity', $input);

        \Flash::message('The form has been uploaded pending approval');

        return \Redirect::back();
    }

    /**
     * @param Request $request
     * @param $clientId
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function consent(Request $request, $clientId, $id = null)
    {
        $input = $request->except('_token');

        $this->consentFormDataForm->validate($request->all());

        Client::findOrFail($clientId);

        $file = $request->file('file');

        if (isset($input['file'])) {
            unset($input['file']);

            $doc =
                Document::make(file_get_contents($file), $file->getClientOriginalExtension(), 'consent_form');

            $input['document_id'] = $doc->id;
        }

        $input['client_id'] = $clientId;

        ClientTransactionApproval::make($clientId, 'add_consent_form', $input);

        \Flash::message('The form has been uploaded pending approval');

        return \Redirect::back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteConsent(Request $request, $id)
    {
        $validation = $this->deleteWithReasonRule($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $data = $request->all();
        $data['consent_form_id'] = $id;

        $consentForm = ConsentForm::findOrFail($id);

        ClientTransactionApproval::add([
            'consent_form' => $consentForm,
            'client_id' => $data['client_id'],
            'transaction_type' => 'delete_consent_form',
            'payload' => $data
        ]);

        \Flash::message('Consent Form delete has been saved awaiting approval');

        return \redirect('/dashboard/clients/details/' . $data['client_id']);
    }


    /**
     * @param $clientId
     * @param $id
     * @param $toggle
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function toggleIndemnityStatus($clientId, $id, $toggle)
    {
        $this->authorizer->checkAuthority('add_email_indemnity');

        $indemnity = \App\Cytonn\Models\EmailIndemnity::findOrFail($id);

        $client = Client::findOrFail($clientId);

        $input['indemnity_id'] = $indemnity->id;
        $input['action'] = strtolower($toggle);

        $indemnityType = $input['action'] == 'delete' ? 'delete_email_indemnity' : 'activate_deactivate_email_indemnity';

        ClientTransactionApproval::add(
            [
                'transaction_type' => $indemnityType,
                'client_id' => $client->id,
                'payload' => $input
            ]
        );

        \Flash::message("The request to $toggle the email indemnity has been sent for approval");

        return \Redirect::back();
    }

    /**
     * @param null $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function create($id = null)
    {
        $this->authorizer->checkAuthority('editclients');

        if (is_null($id)) {
            $client = new Client();

            $contact = new Contact();
        } else {
            $client = Client::find($id);

            $contact = $client->contact;
        }

        $titles = Title::all()->lists('name', 'id');

        $countries = Country::all()->lists('name', 'id');

        $contactmethods = ContactMethod::all()->lists('description', 'id');

        return view(
            'clients.create',
            [
                'title' => 'Add/Edit Client', 'client' => $client, 'contact' => $contact,
                'titles' => $titles, 'countries' => $countries,
                'contactmethods' => $contactmethods
            ]
        );
    }

    /**
     * Save a client
     *
     * @param Request $request
     * @param string $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function store(Request $request, $id)
    {
        $input = $request->all();

        $client = Client::findOrFail($id);

        $complete = boolval($request->get('complete'));

        if ($client->type->name == 'individual') {
            if (!$complete) {
                $this->individualClientForm->incomplete();
            }
            $this->individualClientForm->validate($input);
        } elseif ($client->type->name == 'corporate') {
            if (!$complete) {
                $this->corporateClientForm->incomplete();
            }
            $this->corporateClientForm->validate($input);
        }

        unset($input['individual_entity_type']);

        strlen(trim($input['client_code'])) != 0 ?: $input['client_code'] = null; //clean up client code

        ClientTransactionApproval::add(['transaction_type' => 'edit_client', 'client_id' => $id, 'payload' => $input]);

        \Flash::message('Client details have been saved for approval');

        return redirect('/dashboard/clients');
    }

    /**
     * Show details for a joint investment partner
     *
     * @param  $id
     * @return mixed
     */
    public function jointHolderDetails($id)
    {
        $jointHolder = ClientFilledInvestmentApplicationJointHolders::findOrFail($id);

        return view('clients.jointholderdetail', ['title' => 'Joint Holder Detail', 'holder' => $jointHolder]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function editJointHolder($id)
    {
        $this->authorizer->checkAuthority('editclients');

        $jointHolder = ClientJointDetail::findOrFail($id);

        return view('clients.edit_joint_holder', ['title' => 'Edit Joint Holder', 'joint' => $jointHolder]);
    }

    /*
     *  Delete joint holder details
     */
    public function deleteJointHolder(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $validation = $this->deleteWithReasonRule($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $data = $request->all();

        $jointHolder = ClientJointDetail::findOrFail($id);

        $data['client_id'] = $jointHolder->client_id;

        ClientTransactionApproval::add([
            'joint_holder' => $jointHolder,
            'client_id' => $jointHolder->client_id,
            'transaction_type' => 'delete_joint_holder',
            'payload' => $data
        ]);

        \Flash::message('Joint holder delete has been saved awaiting approval');

        return \redirect('/dashboard/clients/details/' . $jointHolder->client_id);
    }

    /**
     * Delete Tax Exemption
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function deleteTaxExemption(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $validation = $this->deleteWithReasonRule($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $data = $request->all();

        $data['tax_id'] = $id;

        $instruction = ClientTransactionApproval::add([
            'client_id' => (isset($data['id'])) ? $data['id'] : null,
            'transaction_type' => 'delete_tax_exemption',
            'payload' => $data
        ]);

        \Flash::success('Tax Exemption deletion saved successfully for approval');

        return \redirect('/dashboard/clients/details/' . $data['id']);
    }

    /**
     * Save the joint holder to await approval
     *
     * @param Request $request
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveJointHolder(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $this->jointHolderForm->validate($request->all());

        $input = $request->all();
        $input = array_add($input, 'joint_holder_id', $id);
        unset($input['_token']);

        $jointHolder = ClientJointDetail::findOrFail($id);

        ClientTransactionApproval::add(
            ['client_id' => $jointHolder->client_id, 'transaction_type' => 'edit_joint_holder', 'payload' => $input]
        );

        \Flash::message('Joint holder details have been saved awaiting approval');

        return \redirect('/dashboard/clients/details/' . $jointHolder->client_id);
    }

    /**
     * Add a contact person to a client
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function addContactPerson(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $client = Client::findOrFail($id);

        $input = $request->all();

        $this->contactPersonAddForm->validate($input);

        unset($input['_token']);

        $input = array_add($input, 'client_id', $client->id);

        ClientTransactionApproval::add(
            ['client_id' => $client->id, 'transaction_type' => 'add_contact_person',
                'payload' => $input, 'scheduled' => false]
        );

        \Flash::message('Contact person details saved for approval');

        return \Redirect::back();
    }

    public function deleteContactPerson($id, $contactId)
    {
        $this->authorizer->checkAuthority('editclients');

        $client = Client::findOrFail($id);

        $input = ['contact_id' => $contactId ];

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'transaction_type' => 'delete_contact_person',
                'payload' => $input,
                'scheduled' => false
            ]
        );

        \Flash::message('Request to delete contact person has been saved for approval');

        return \Redirect::back();
    }

    /*
     * Add alternative emails for a client
     *
     * @param $id
     */
    public function addAlternativeEmails(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $client = Client::findOrFail($id);

        $input = explode(',', str_replace(' ', '', $request->get('emails')));

        foreach ($input as $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                \Flash::error('One or more of the emails in invalid');

                return Redirect::back()->withInput();
            }
        }

        ClientTransactionApproval::add(
            ['client_id' => $client->id, 'transaction_type' => 'edit_alternative_emails',
                'scheduled' => false, 'payload' => ['emails' => $input]]
        );

        \Flash::message('New Client Emails saved for approval');

        return Redirect::back();
    }

    /*
     * @param $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function addBankDetails(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $client = Client::findOrFail($id);

        $this->addBankDetailsForm->validate($request->all());

        $input = $request->all();
        unset($input['_token']);

        if (!isset($input['active'])) {
            $input['active'] = 0;
        }

        $input = array_add($input, 'client_id', $client->id);

        ClientTransactionApproval::add(
            ['client_id' => $client->id, 'payload' => $input,
                'scheduled' => false, 'transaction_type' => 'add_bank_details']
        );

        \Flash::message('Bank details saved for confirmation');

        return \Redirect::back();
    }

    public function setDefaultAccount($id)
    {
        $input = request()->except('_token');

        $client = Client::findOrFail($id);

        ClientTransactionApproval::add([
            'client_id' => $client->id,
            'payload' => $input,
            'scheduled' => false,
            'transaction_type' => 'set_default_bank_account'
        ]);

        \Flash::message('Setting has been saved for approval');
        return \Redirect::back();
    }

    /**
     * @param Request $request
     * @param $id
     * @param $account_id
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function deleteBankDetails(Request $request, $id, $account_id)
    {
        $this->authorizer->checkAuthority('editclients');

        $client = Client::findOrFail($id);

        $input['account_id'] = $account_id;

        $input['client_id'] = $client->id;

        ClientTransactionApproval::add(
            ['client_id' => $client->id, 'payload' => $input, 'scheduled' => false,
                'transaction_type' => 'delete_bank_details']
        );

        \Flash::message('Request to delete Bank details has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function addRelationshipPerson(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');
        $client = Client::findOrFail($id);
        $input = $request->except('_token');
        $input = array_add($input, 'client_id', $client->id);
        ClientTransactionApproval::add(
            ['client_id' => $client->id, 'payload' => $input, 'scheduled' => false,
                'transaction_type' => 'add_relationship_person']
        );
        \Flash::message('Relationship person has been saved for approval');
        return \Redirect::back();
    }


    public function addIncomingFa(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $client = Client::findOrFail($id);
        $input = $request->except('_token');

        $input = array_add($input, 'client_id', $client->id);

        ClientTransactionApproval::add(
            ['client_id' => $client->id, 'payload' => $input, 'scheduled' => false,
                'transaction_type' => 'add_incoming_fa']
        );
        \Flash::message('Incoming FA has been saved for approval');

        return \Redirect::back();
    }

    public function editIncomingFa(Request $request, $id)
    {
        $this->authorizer->checkAuthority('editclients');

        $client = Client::findOrFail($id);

        $input = $request->except('_token');

        $input = array_add($input, 'client_id', $client->id);

        ClientTransactionApproval::add(
            ['client_id' => $client->id, 'payload' => $input, 'scheduled' => false,
                'transaction_type' => 'edit_incoming_fa']
        );

        \Flash::message('Edit Incoming FA has been saved for approval');

        return \Redirect::back();
    }

    /*
     * Update the signing mandate for the client
     */
    public function updateSigningMandate(Request $request, $id)
    {
        $this->authorizer->checkAuthority('update_signing_mandate');

        $client = Client::findOrFail($id);

        $input = $request->except('_token');

        $input = array_add($input, 'client_id', $client->id);

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'payload' => $input,
                'scheduled' => false,
                'transaction_type' => 'update_signing_mandate'
            ]
        );

        \Flash::message('Signing mandate update saved for approval');

        return redirect('/dashboard/clients/details/' . $client->id);
    }

    /*
     * Upload the client signature for the client
     */
//    public function uploadClientSignature(Request $request, $id)
//    {
//        $this->authorizer->checkAuthority('update_signing_mandate');
//
//        $validation = $this->clientSignatureUpload($request);
//
//        if ($validation) {
//            return back()->withErrors($validation)->withInput();
//        }
//
//        $client = Client::findOrFail($id);
//
//        $input = $request->except('_token');
//
//        $file = $request->file('file');
//
//        unset($input['file']);
//
//        if (array_key_exists('joint_client_holder_id', $input) && is_null($input['joint_client_holder_id'])) {
//            unset($input['joint_client_holder_id']);
//        }
//
//        $clientSignature = ClientSignature::where('client_id', $input['client_id']);
//
//        if (array_key_exists('joint_client_holder_id', $input)) {
//            $clientSignature =
//                $clientSignature->where('joint_client_holder_id', $input['joint_client_holder_id']);
//        }
//
//        $clientSignature = $clientSignature->first();
//
//        if ($clientSignature) {
//            $document = $clientSignature->document;
//
//            $document->updateDoc(file_get_contents($file), $file->getClientOriginalExtension(), 'client_signatures');
//        } else {
//            $document = Document::make(
//                file_get_contents($file),
//                $file->getClientOriginalExtension(),
//                'client_signatures'
//            );
//        }
//
//        $input['document_id'] = $document->id;
//
//        ClientTransactionApproval::add(
//            [
//                'client_id' => $client->id,
//                'payload' => $input,
//                'scheduled' => false,
//                'transaction_type' => 'add_client_signature'
//            ]
//        );
//
//        \Flash::message('Client signature saved for approval');
//
//        return redirect('/dashboard/clients/details/' . $client->id);
//    }

    public function deactivateSignature($id)
    {
        $signature = ClientSignature::findOrFail($id);

        $client = $signature->client;

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'payload' => request()->except('_token'),
                'scheduled' => false,
                'transaction_type' => 'deactivate_client_signature'
            ]
        );

        \Flash::message('Client signature deactivation saved for approval');

        return redirect('/dashboard/clients/details/' . $client->id);
    }

    /*
     * Store client guards
     */
    public function storeClientGuards(Request $request, $clientId, $id = null)
    {
        $this->authorizer->checkAuthority('store_client_guards');

        $validation = $this->createGuard($request);

        if ($validation) {
            \Flash::error('There were errors in the submitted form. Please check on them and retry');

            return back()->withErrors($validation)->withInput();
        }

        $client = Client::findOrFail($clientId);

        if ($client->repo->checkExistingGaurd($request->get('guard_type_id'), $id)) {
            \Flash::error('There is an existing guard of the selected type for the client');

            return back()->withInput();
        }

        $input = $request->except('_token');

        $input = array_add($input, 'id', $id);

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'payload' => $input,
                'scheduled' => false,
                'transaction_type' => 'store_guard'
            ]
        );

        \Flash::message('Client guard saved for approval');

        return redirect('/dashboard/clients/details/' . $client->id);
    }

    /*
     * Delete a client guard
     */
    public function deleteClientGuards(Request $request, $clientId, $id)
    {
        $client = Client::findOrFail($clientId);

        $input = $request->except('_token');

        $input = array_add($input, 'id', $id);

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'payload' => $input,
                'scheduled' => false,
                'transaction_type' => 'delete_guard'
            ]
        );

        \Flash::message('Client guard deletion saved for approval');

        return redirect('/dashboard/clients/details/' . $client->id);
    }

    /**
     * @param $clientId
     * @return Redirect
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function delete($clientId)
    {
        $client = Client::findOrFail($clientId);

        $this->authorizer->checkAuthority('editclients');

        if ($client->repo->hasProducts()) {
            throw new ClientInvestmentException('The client is already linked to a product
          or payment and thus cant be deleted');
        }

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'payload' => ['client_id' => $client->id],
                'scheduled' => false,
                'transaction_type' => 'delete_unused_client_account'
            ]
        );

        \Flash::message('Request to delete the client account has been sent for approval');

        return redirect('/dashboard/clients/details/' . $client->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeInterestReinvestmentPlan($id)
    {
        $input = request()->except('_token');

        $client = Client::findOrFail($input['client_id']);

        ClientTransactionApproval::add([
            'client_id' => $client->id,
            'payload' => $input,
            'scheduled' => false,
            'transaction_type' => 'activate_combined_interest_reinvestment'
        ]);

        \Flash::message('Request to activate combined interest reinvestment plan has been sent for approval');

        return redirect('/dashboard/clients/details/' . $client->id);
    }

    public function editTenor()
    {
        $input = request()->except('_token');
        $client = Client::findOrFail($input['client_id']);

        ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'payload' => $input,
                'scheduled' => false,
                'transaction_type' => 'edit_client_tenor'
            ]
        );

        \Flash::message('Request to edit client tenor has been sent for approval');
        return redirect('/dashboard/clients/details/' . $client->id);
    }

    public function createJointHolders($id)
    {
        $client = Client::findOrFail($id);

        return view('clients.add_joint_holders', ['client' => $client]);
    }

    /*
     * Add joint holder
     */

    public function addJointHOlder($id)
    {
        $client = Client::findOrFail($id);

        $holders = request()->get('holders');

        $holder = json_decode($holders);

        ClientTransactionApproval::add([
            'client_id' => $client->id,
            'payload' => $holder,
            'scheduled' => false,
            'transaction_type' => 'add_client_joint_holder'
        ]);

        \Flash::message('Request to add joint holder to client has been sent for approval');

        return redirect('/dashboard/clients/details/' . $client->id);
    }


    public function riskyClientsIndex()
    {
        return view('clients.risky.index', ['title' => 'Risky Clients']);
    }

    public function riskyClientsCreate()
    {
        return view('clients.risky.create', ['title' => 'Create Risky Client', 'edit' => false, 'risk_id' => null]);
    }

    public function riskyClientsEdit($id)
    {
        return view('clients.risky.create', ['title' => 'Edit Risky Clients', 'edit' => true, 'risk_id' => $id]);
    }

    public function riskyClientsShow($id)
    {
        $riskClient = Client\RiskyClient::findOrFail($id);

        return view('clients.risky.show', ['title' => 'Show Risky Clients', 'riskClient' => $riskClient]);
    }

    public function riskyClientsCleared(Request $request)
    {
        $input = $request->all();

        $riskClient = Client\RiskyClient::findOrFail($input['risk_id']);

        $riskClient->risky_status_id = 3;
        $riskClient->reason = $input['clear_reason'];
        $riskClient->save();

        $this->mailer->sendClearRiskyClientNotification($riskClient);

        \Flash::success('Risky Client details have been cleared after due-diligence');

        return redirect::back();
    }

    public function riskyClientsRejected(Request $request)
    {
        $input = $request->all();

        $riskClient = Client\RiskyClient::findOrFail($input['risk_id']);

        $riskClient->risky_status_id = 2;
        $riskClient->reason = $input['reject_reason'];
        $riskClient->save();

        \Flash::success('Risky Client details have been rejected after due-diligence');

        return redirect::back();
    }

    public function riskyClientsDelete(Request $request)
    {
        $input = $request->all();

        $riskClient = Client\RiskyClient::findOrFail($input['risk_id']);
        $riskClient->delete();

        \Flash::success('Risky Client details have been deleted after due-diligence');

        return redirect('/dashboard/clients/risky');
    }

    public function exportRiskyClients()
    {
        $user = Auth::id();

        $this->queue(
            function () use ($user) {
                \Artisan::call('crims:risky_clients_export', [
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The Risky Clients Report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function loyaltyPointsIndex()
    {
        return view('clients.loyalty.index', ['title' => 'Clients Loyalty Points']);
    }

    public function loyaltyPointsValuesIndex()
    {
        return view('clients.loyalty.values.index', ['title' => 'Loyalty Points Values']);
    }

    public function loyaltyPointsVouchersIndex()
    {
        return view('clients.loyalty.vouchers.index', ['title' => 'Loyalty Points Values']);
    }

    public function loyaltyPointsRedeemIndex()
    {
        return view('clients.loyalty.redeem.index', ['title' => 'Loyalty Points Values']);
    }
}
