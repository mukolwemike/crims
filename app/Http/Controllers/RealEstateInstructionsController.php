<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 04/02/2019
 * Time: 11:26
 */

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\PaymentSchedule;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;

class RealEstateInstructionsController extends Controller
{
    public function index()
    {
        return view('realestate.re-instructions.index', [
            'title' => 'Real Estate Instructions',
        ]);
    }

    public function show($id)
    {
        $instruction = RealEstateInstruction::findOrFail($id);

        $client = ($instruction->client_id) ? $instruction->client : null;

        $holding = $instruction->holding;

        $unit_price = $holding->price();

        $total_paid = $holding->repo->unitTotalPaid($holding);

        $balance = $unit_price - $total_paid;

        $document = ($instruction->document_id) ? Document::findOrFail($instruction->document_id) : null;

        $paymentSchedules = $holding->paymentSchedules;

        $paymentsExisting = RealEstatePayment::where('holding_id', $holding->id)->get();

        $payload = $instruction->payload;

//        $schedule = RealEstatePaymentSchedule::find($payload->schedule_id);

        return view('realestate.re-instructions.details', [
            'title' => 'Real Estate Instructions',
            'instruction' => $instruction,
            'client' => $client,
            'holding' => $holding,
            'document' => $document,
            'balance' => $balance,
            'total_paid' => $total_paid,
            'unit_price' => $unit_price,
            'payment' => $instruction->payload,
            'paymentSchedules' => $paymentSchedules,
            'paymentsExisting' => $paymentsExisting,
//            'schedule' => $schedule
        ]);
    }

    public function process($id)
    {
        $instruction = RealEstateInstruction::findOrFail($id);

        $payload = $instruction->payload;

        $payload->instruction_id = $instruction->id;

        $client = $instruction->client;

        $input = collect($payload)->toArray();

        $approval = ClientTransactionApproval::add([
            'client_id' => $client->id,
            'transaction_type' => 'make_real_estate_payment',
            'payload' => $input
        ]);

        $instruction->update([
            'approval_id' => $approval->id
        ]);

        \Flash::success('The payment instruction has been saved for approval');

        return redirect()->back();
    }
}
