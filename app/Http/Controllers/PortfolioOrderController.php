<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/20/18
 * Time: 2:17 PM
 */

namespace App\Http\Controllers;

use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderType;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Orders\PortfolioOrderGenerator;

class PortfolioOrderController extends Controller
{
    public function index()
    {
        $fundManager = $this->fundManager();

        return view('portfolio.orders.index', [
            'title' => 'Portfolio Orders',
            'fundmanager' => $fundManager
        ]);
    }

    public function create()
    {
        return view('portfolio.orders.create');
    }

    public function show($id)
    {
        return view('portfolio.orders.show', [
            'title' => 'Portfolio Order Details'
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function generateOrderDocument($id)
    {
        $order = PortfolioOrder::findOrFail($id);

        $generator = new PortfolioOrderGenerator();

        return $generator->generate($order)->stream();
    }
}
