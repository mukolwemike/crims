<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\ProductType;
use Cytonn\Investment\ProductDocuments\ProductDocumentRepository;
use Cytonn\Investment\Products\ProductRepository;
use Cytonn\Investment\Products\ProductRules;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /*
     * Get the prduct rules trait
     */
    use ProductRules;

    /*
     * Define the repositories
     */
    protected $productRepository;
    protected $productDocumentRepository;

    /*
     * Setup the constructor for the controller
     */
    public function __construct(
        ProductRepository $productRepository,
        ProductDocumentRepository $productDocumentRepository
    ) {
        $this->productRepository = $productRepository;
        $this->productDocumentRepository = $productDocumentRepository;

        parent::__construct();
    }

    /*
     * Display the listing of all the products
     */
    public function index()
    {
        return view(
            'investment.products.index',
            [
            'title' => 'Products'
            ]
        );
    }

    /*
     * Supply the view to create or edit a product
     */
    public function create($id = null)
    {
        $product = $id ? $this->productRepository->getProductById($id) : new Product();

        $currencies = ['' => 'Select Currency'] + Currency::pluck('name', 'id')->toArray();

        $custodialAccounts = ['' => 'Select Custodial Account'] +
            CustodialAccount::all()->pluck('full_name', 'id')->toArray();

        $fundManagers = ['' => 'Select Fund Manager'] + FundManager::pluck('name', 'id')->toArray();

        $productTypes = ['' => 'Select Product Type'] + ProductType::pluck('name', 'id')->toArray();

        return view(
            'investment.products.create',
            [
            'product' => $product,
            'currencies' => $currencies,
            'custodialAccounts' => $custodialAccounts,
            'fundManagers' => $fundManagers,
            'productTypes' => $productTypes,
            'title' => 'Add/Edit Product'
            ]
        );
    }

    /*
     * Store or update a product
     */
    public function store(Request $request, $id = null)
    {
        $validation = $this->createProduct($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $input = $request->except('_token');

        $input['id'] = $id;

        ClientTransactionApproval::make(null, 'store_product', $input);

        \Flash::message('The product has been saved for approval');

        return redirect('Dashboard/products');
    }

    /*
     * Show the details regarding a product
     */
    public function details($id)
    {
        $product = $this->productRepository->getProductById($id);

        return view(
            'investment.products.details',
            [
            'product' => $product,
            'title' => 'Product Details'
            ]
        );
    }

    /*
     * Upload a product document
     */
    public function uploadDocument(Request $request, $productId, $id = null)
    {
        $validation = $this->uploadProductDocument($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $product = $this->productRepository->getProductById($request->get('product_id'));

        $file = $request->file('file');

        if ($id) {
            $productDocument = $this->productDocumentRepository->getProductDocumentById($id);

            $document = $productDocument->document;

            $document->updateDoc(file_get_contents($file), $file->getClientOriginalExtension(), 'product_documents');
        } else {
            $document = Document::make(
                file_get_contents($file),
                $file->getClientOriginalExtension(),
                'product_documents'
            );
        }

        $input = $request->except('_token', 'file');

        $input['document_id'] = $document->id;

        $input['id'] = $id;

        ClientTransactionApproval::make(null, 'store_product_document', $input);

        \Flash::message('Document saved for approval');

        return redirect('/dashboard/products/details/' . $product->id);
    }
}
