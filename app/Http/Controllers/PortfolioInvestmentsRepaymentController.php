<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\PortfolioRepaymentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use \Carbon\Carbon;
use Cytonn\Models\Portfolio\DepositHoldingRepayments;
use Cytonn\Portfolio\Forms\InvestmentRepaymentForm;
use Cytonn\Portfolio\Forms\PortfolioRepaymentDeleteForm;
use Illuminate\Support\Facades\Redirect;

/**
 * Date: 22/06/2016
 * Time: 12:08 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class PortfolioInvestmentsRepaymentController extends Controller
{
    protected $depositRepaymentForm;
    /**
     * @var PortfolioRepaymentDeleteForm
     */
    private $deleteForm;

    /**
     * DepositHoldingsRepaymentController constructor.
     *
     * @param InvestmentRepaymentForm      $depositRepaymentForm
     * @param PortfolioRepaymentDeleteForm $deleteForm
     */
    public function __construct(
        InvestmentRepaymentForm $depositRepaymentForm,
        PortfolioRepaymentDeleteForm $deleteForm
    ) {
        $this->depositRepaymentForm = $depositRepaymentForm;

        parent::__construct();

        $this->deleteForm = $deleteForm;
    }


    public function index($id)
    {
        $deposit = DepositHolding::findOrFail($id);

        $date_eff = new Carbon(\Request::get('date_eff'));

        $prepared = $deposit->calculate($date_eff, false)->getPrepared();

        $all_repayments = $deposit->repayments()->where('date', '<=', $date_eff)->oldest('date')->get();

        $principal_repayments = $deposit->repayments()->ofType('principal_repayment')
            ->where('date', '<=', $date_eff)->oldest('date')->get();

        $interest_repayments = $deposit->repayments()->ofType('interest_repayment')
            ->where('date', '<=', $date_eff)->oldest('date')->get();

        $repaymenttypes = PortfolioRepaymentType::all()->mapWithKeys(
            function ($repaymenttype) {
                return [$repaymenttype['id'] => $repaymenttype['name']];
            }
        );

        return view(
            'portfolio.actions.repayment.index',
            [
                'deposit'=>$deposit, 'principal_repayments'=>$principal_repayments,
                'interest_repayments'=>$interest_repayments,
                'all_repayments'=>$all_repayments,  'title'=>'Portfolio Loan Repayment', 'date_eff'=>$date_eff,
                'repaymenttypes'=>$repaymenttypes, 'prepared' => $prepared
            ]
        );
    }

    public function store($id)
    {
        $input = \Request::except('_token');

        $this->depositRepaymentForm->validate($input);

        $deposit = DepositHolding::findOrFail($id);

        if ($input['payment_date_type_id'] == 1) {
            $input['date'] = $deposit->maturity_date;
        }

        if ($input['payment_amount_type_id'] == 0) {
            if ((float) $input['amount'] > $deposit->repo->getTotalValueOfAnInvestment()) {
                \Flash::error('The amount redeemed is more than the deposit current value');

                return Redirect::back();
            }
        }

        $input['deposit_id'] = $id;
        $input['date'] = (new Carbon($input['date']))->toDateString();

        PortfolioTransactionApproval::add(['institution_id'=>$deposit->security->investor->id,
            'transaction_type'=>'portfolio_investment_repayment', 'payload'=>$input]);

        \Flash::message('The repayment transaction has been saved for approval');

        return \Redirect::back();
    }

    public function destroy($id)
    {
        $this->deleteForm->validate(\Request::all());

        $input = \Request::except('_token', '_method');
        $input['repayment_id'] = $id;

        $repayment = PortfolioInvestmentRepayment::findOrFail($id);


        PortfolioTransactionApproval::add(['institution_id'=>$repayment->investment->security->investor->id,
            'transaction_type'=>'reverse_portfolio_investment_repayment', 'payload'=>$input]);

        \Flash::message('The repayment transaction reversal has been saved for approval');

        return \Redirect::back();
    }
}
