<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use Carbon\Carbon;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\ClientDeductionsExcelGenerator;
use Cytonn\Reporting\ClientWithdrawalsExcelGenerator;
use Cytonn\Reporting\CombinedCommissionsExcelGenerator;
use Cytonn\Reporting\InterestPaymentSchedulesUpForApprovalExcelGenerator;
use Illuminate\Support\Facades\Auth;

//namespace Api;
//use BaseController;

//use Product;

/**
 * Date: 10/11/2015
 * Time: 4:18 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ExportController extends Controller
{
    use ExcelMailer;

    /**
     * ExportController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->authorizer->checkAuthority('viewportfoliosummary');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportClientData($productId)
    {
        $userId = Auth::id();

        $this->queue(
            function () use ($productId, $userId) {
                \Artisan::call(
                    'clients:client-data-summary',
                    ['product_id' => $productId, 'user_id' => $userId]
                );
            }
        );

        \Flash::message('Download successful! An email with the attachment has been sent to your account.');

        return back();
    }

    public function exportClientDataAtDate($productId)
    {
        $userId = Auth::id();
        $endDate = \Request::get('end_date');

        $this->queue(
            function () use ($productId, $userId, $endDate) {
                \Artisan::call(
                    'clients:client-data-summary',
                    ['product_id' => $productId, 'user_id' => $userId, 'end_date' => $endDate]
                );
            }
        );

        \Flash::message('Download successful! An email with the attachment has been sent to your account.');

        return back();
    }

    public function exportProjectCashFlows($projectId)
    {
        $startDate = (new Carbon(\Request::get('startDate')))->startOfMonth();
        //$endDate = (new Carbon(\Request::get('endDate')))->startOfMonth()->addMonth();
        $endDate = (new Carbon(\Request::get('endDate')))->endOfMonth()->addDay();

        $project = Project::findOrFail($projectId);

        $file =  (new \Cytonn\Reporting\ProjectCashFlowsExcelGenerator())->excel($project, $startDate, $endDate);

        $file->export('xlsx');
    }

    public function exportClientDeductions()
    {
        $startDate = empty(\Request::get('startDate'))
            ? Carbon::today()->startOfMonth()
            : Carbon::parse(\Request::get('startDate'));

        $endDate = empty(\Request::get('endDate')) ?
            Carbon::today()->endOfMonth()->addDay()
            : Carbon::parse(\Request::get('endDate'))->endOfDay();

        $file = (new ClientDeductionsExcelGenerator())->excel($startDate, $endDate);
        $file->export('xlsx');
    }

    public function exportClientWithdrawals()
    {
        $startDate = empty(\Request::get('startDate')) ?
            Carbon::today()->startOfMonth() : Carbon::parse(\Request::get('startDate'));

        $endDate = empty(\Request::get('endDate')) ?
            Carbon::today()->endOfMonth()->addDay() : Carbon::parse(\Request::get('endDate'))->endOfDay();

        $file = (new ClientWithdrawalsExcelGenerator())->excel($startDate, $endDate);
        $file->export('xlsx');
    }


    public function exportCombinedCommissions()
    {
        $id = \Request::get('bcp_id');
        $email = \Auth::user()->email;

        $this->queue(function () use ($id, $email) {
            $start = Carbon::now();

            $bcp = BulkCommissionPayment::findOrFail($id);

            (new CombinedCommissionsExcelGenerator())->excel($bcp)->store('xlsx');

            $this->sendExcel(
                'Combined Commission Report - '.DatePresenter::formatDate($bcp->end),
                "See attached report for {$bcp->start->toFormattedDateString()} 
                    to {$bcp->end->toFormattedDateString()} 
                    (Time taken: ".Carbon::now()->diffInMinutes($start->copy()).' minutes)',
                'Combined_commissions',
                [$email]
            );
        });

        \Flash::message('The report will be sent to your email');

        return redirect()->back();
    }
}
