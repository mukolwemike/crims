<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\UnitHolding;
use App\Jobs\RealEstate\OverduePaymentsReport;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Realestate\Forms\PaymentSchedulesForm;
use function array_merge;

/**
 * Date: 09/06/2016
 * Time: 2:54 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstatePaymentScheduleController extends Controller
{
    protected $paymentSchedulesForm;

    /**
     * RealEstatePaymentScheduleController constructor.
     *
     * @param $paymentSchedulesForm
     */
    public function __construct(PaymentSchedulesForm $paymentSchedulesForm)
    {
        $this->paymentSchedulesForm = $paymentSchedulesForm;

        parent::__construct();
    }

    /**
     * Show the grid table for the upcoming payments
     *
     * @return mixed
     */
    public function getIndex()
    {
        $projects = Project::all()->lists('name', 'id');

        $projects = $projects + [null => 'All projects'];

        return view(
            'realestate.payments.schedules.index',
            [
                'title' => 'Real Estate Upcoming Payments', 'projects' => $projects
            ]
        );
    }

    /**
     * @param $holdingId
     * @return mixed
     */
    public function getShow($holdingId)
    {
        $holding = UnitHolding::findOrFail($holdingId);

        $schedules = $holding->paymentSchedules;

        $total = $schedules->sum('amount');

        $paymentTypes = RealEstatePaymentType::all();

        return \view(
            'realestate.payments.schedules.show',
            ['holding' => $holding, 'title' => 'Payment Schedules', 'schedules' => $schedules,
                'total' => $total, 'paymentTypes' => $paymentTypes
            ]
        );
    }

    /**
     * @param $holdingId
     * @return mixed
     */
    public function postStore($holdingId)
    {
        $input = \Request::except('_token', 'description', 'payment_type_id', 'date', 'amount');

        $items = array_has($input, 'penalty')
            ? ['description', 'payment_type_id', 'date', 'amount', 'penalty']
            : ['description', 'payment_type_id', 'date', 'amount'];

        $schedules = Collection::make(\Request::only($items))->transpose()->toArray();

        $input['schedules'] = $schedules;

        Collection::make($schedules)->map(
            function ($schedule) {
                //             $this->paymentSchedulesForm->validate($schedule);
            }
        );

        $holding = UnitHolding::findOrFail($holdingId);

        $input['holding_id'] = $holdingId;

        ClientTransactionApproval::make($holding->client_id, 'add_real_estate_payment_schedule', $input);

        \Flash::message('The schedule item has been saved for approval');

        return \Redirect::back();
    }

    public function deleteRemove($id)
    {
        $paymentSchedule = RealEstatePaymentSchedule::findOrFail($id);
        $holding = $paymentSchedule->holding;

        $input = ['id' => $id, 'holding_id' => $holding->id];

        ClientTransactionApproval::make(
            $holding->client_id,
            'remove_real_estate_payment_schedule',
            $input
        );

        \Flash::message('The schedule payment delete request has been saved for approval');

        return \Redirect::back();
    }

    public function deleteBulkRemove($holding_id)
    {
        $input = \Request::only('items');

        $holding = UnitHolding::findOrFail($holding_id);

        $input['holding_id'] = $holding->id;

        foreach ($input['items'] as $item) {
            $paymentSchedule = RealEstatePaymentSchedule::findOrFail($item);

            if ($paymentSchedule->payments()->count() > 0) {
                throw new ClientInvestmentException(
                    'There is an existing payment for one of the schedules, you cannot bulk delete'
                );
            }
        }

        ClientTransactionApproval::make(
            $holding->client_id,
            'bulk_remove_real_estate_payment_schedules',
            $input
        );

        \Flash::message('The schedule payments delete request have been saved for approval');

        return \Redirect::back();
    }

    public function putUpdate($id)
    {
        $paymentSchedule = RealEstatePaymentSchedule::findOrFail($id);
        $holding = $paymentSchedule->holding;

        $input = \Request::except(['_token', '_method']);

        if (!array_key_exists('penalty', $input)) {
            $input['penalty'] = 0;
        }

        ClientTransactionApproval::make(
            $holding->client_id,
            'update_real_estate_payment_schedule',
            $input
        );

        \Flash::message('The schedule payment update request has been saved for approval');

        return \Redirect::back();
    }

    public function postSendBulkReminders()
    {
        $start_date = \Request::get('start_date');
        $end_date = \Request::get('end_date');
        $project_id = \Request::get('project_id');

        $payment_schedules = RealEstatePaymentSchedule::where('date', '>=', $start_date)
            ->where('date', '<=', $end_date)
            ->whereHas(
                'holding',
                function ($holding) use ($project_id) {
                    $holding->where('active', 1);

                    if ($project_id) {
                        $holding->whereHas(
                            'unit',
                            function ($unit) use ($project_id) {
                                $unit->where('project_id', $project_id);
                            }
                        );
                    }
                }
            )
            ->isLast(false)
            ->get()
            ->lists('id');

        ClientTransactionApproval::add([
            'client_id' => null, 'transaction_type' => 'send_bulk_upcoming_payment_reminders',
            'payload' => ['payment_schedules' => $payment_schedules, 'removed' => [],
                'project_id' => \Request::get('project_id'), 'start_date' => $start_date,
                'end_date' => $end_date]
        ]);

        \Flash::message('The upcoming payments have been sent for bulk approval');
        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function getOverdue()
    {
        $projects = array_merge(['0' => 'All Project'], Project::all()->lists('name', 'id'));

        return view(
            'realestate.payments.schedules.overdue.index',
            [
                'title' => 'Overdue payments', 'projects' => $projects
            ]
        );
    }

    /**
     * @return mixed
     */
    public function postExportOverdue()
    {
        dispatch(new OverduePaymentsReport(\Auth::user()));

        \Flash::success('The overdue payments report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @param $job
     * @param array $data
     */
    public function export($job, array $data)
    {
        \Artisan::call('re:overdue-payments', ['user_id' => $data['user_id']]);
        $job->delete();
    }

    /**
     * @return mixed
     */
    public function postSendOverdueBulkReminders()
    {
        $start_date = \Request::get('start_date');
        $end_date = \Request::get('end_date');
        $project_id = \Request::get('project_id');

        $overdue_payment_schedules = RealEstatePaymentSchedule::where('date', '>=', $start_date)
            ->where('date', '<=', $end_date)
            ->whereHas(
                'holding',
                function ($holding) use ($project_id) {
                    $holding->where('active', 1);
                    if ($project_id) {
                        $holding->whereHas(
                            'unit',
                            function ($unit) use ($project_id) {
                                $unit->where('project_id', $project_id);
                            }
                        );
                    }
                }
            )
            ->inPricing()
            ->isLast(false)
            ->where('paid', false)
            ->oldest('date')
            ->pluck('id')->all();

        ClientTransactionApproval::add([
            'client_id' => null, 'transaction_type' => 'send_bulk_overdue_payment_reminders',
            'payload' => ['overdue_payment_schedules' => $overdue_payment_schedules, 'removed' => [],
                'project_id' => \Request::get('project_id'), 'start_date' => $start_date, 'end_date' => $end_date]
        ]);
        
        \Flash::message('The overdue payments have been sent for bulk approval');
        return \Redirect::back();
    }
}
