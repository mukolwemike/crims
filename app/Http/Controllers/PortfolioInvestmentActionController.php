<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Portfolio\Forms\CombinedRolloverForm;
use Cytonn\Portfolio\Forms\ReverseInvestmentForm;
use Cytonn\Portfolio\Forms\RollbackWithdrawalForm;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Request;

/**
 * Date: 21/03/2016
 * Time: 7:34 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */


/**
 * Class PortfolioInvestmentActionController
 */
class PortfolioInvestmentActionController extends Controller
{

    /**
     * @var ReverseInvestmentForm
     */
    protected $reverseInvestmentForm;

    /**
     * @var RollbackWithdrawalForm
     */
    protected $rollbackWithdrawalForm;

    protected $combinedRolloverForm;


    /**
     * PortfolioInvestmentActionController constructor.
     *
     * @param $reverseInvestmentForm
     */
    public function __construct(
        ReverseInvestmentForm $reverseInvestmentForm,
        RollbackWithdrawalForm $rollbackWithdrawalForm,
        CombinedRolloverForm $combinedRolloverForm
    ) {
        $this->reverseInvestmentForm = $reverseInvestmentForm;

        $this->authorizer = new \Cytonn\Authorization\Authorizer();

        $this->rollbackWithdrawalForm = $rollbackWithdrawalForm;

        $this->combinedRolloverForm = $combinedRolloverForm;
    }

    /**
     * Reverse an investment
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function reverse($id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->reverseInvestmentForm->validate(\Request::all());

        $investment = DepositHolding::findOrFail($id);

        if ($investment->withdrawn) {
            \Flash::error('This investment is withdrawn, you first need to rollback the withdrawal.');

            return \Redirect::back();
        }

        $input = \Request::all();
        unset($input['_token']);
        $input['investment_id'] = $investment->id;

        PortfolioTransactionApproval::add(
            [
                'institution_id'=>$investment->security->investor->id,
                'transaction_type'=>'reverse_investment',
                'payload'=>$input
            ]
        );

        \Flash::message('The investment reversal has been saved for approval');

        return \redirect('/dashboard/portfolio');
    }

    /**
     * Rollback a withdrawal
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function rollbackWithdrawal($id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->rollbackWithdrawalForm->validate(\Request::all());

        $investment = DepositHolding::findOrFail($id);

        $child = DepositHolding::where(
            'invest_transaction_approval_id',
            $investment->withdraw_transaction_approval_id
        )->latest()->first();

        if (!is_null($child)) {
            \Flash::error('The investment was rolled over to an existing 
            investment, you cannot reverse the withdrawal');

            return \Redirect::back();
        }

        $input = \Request::all();
        unset($input['_token']);
        $input['investment_id'] = $investment->id;

        PortfolioTransactionApproval::add(['institution_id'=>$investment->security->investor->id,
            'transaction_type'=>'rollback_withdrawal', 'payload'=>$input]);

        \Flash::message('The investment withdrawal rollback has been saved for approval');

        return \redirect('/dashboard/portfolio');
    }

    public function combinedRollover($id)
    {
        $investment = DepositHolding::findOrFail($id);
        $investment->value = $investment->repo->getTotalValueOfAnInvestment();

        $same_day_investments = DepositHolding::whereHas(
            'security',
            function ($sec) use ($investment) {
                return $sec->where('portfolio_investor_id', $investment->security->investor->id);
            }
        )
            ->where('maturity_date', $investment->maturity_date)
            ->where('id', '!=', $investment->id)
            ->where('withdrawn', null)->orWhere('withdrawn', 0)
            ->get();

        return view(
            'portfolio.actions.rollover.combined',
            ['deposit'=>$investment, 'same_day_investments'=>$same_day_investments,
            'title'=>'Combine & Rollover Investments']
        );
    }

    public function performCombinedRollover($id)
    {
        $investment = DepositHolding::findOrFail($id);
        $investment->value = $investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->invested_date);

        $input = Request::all();

        unset($input['_token']);

        $investments = DepositHolding::whereIn('id', array_keys($input))->get();
        $investments->push($investment);

        $investments->map(
            function ($inv) {
                $inv->value = $inv->repo->getTotalValueOfAnInvestmentAsAtDate($inv->maturity_date);
            }
        );

        return view('portfolio.actions.rollover.performcombinedrollover', ['same_day_investments'=>$investments,
            'investment'=>$investment, 'title'=>'Combine & Rollover investments']);
    }

    public function saveCombinedRollover($id)
    {
        $input = Request::all();

        if ($input['taxable'] == 1 || $input['taxable'] === true) {
            $input['taxable'] = true;
        }

        unset($input['_token']);

        $input['investment'] = Crypt::decrypt($input['investment']);

        $oldInvestment  = DepositHolding::findOrFail($input['investment']);

        $validation_data = $input;

        //allow same day
        $validation_data = array_add(
            $validation_data,
            'old_investment_maturity_date',
            (new Carbon($oldInvestment->maturity_date))->subDay()->toDateString()
        );
        $validation_data = array_add(
            $validation_data,
            'old_investment_amount',
            (float) $oldInvestment->repo->getTotalValueOfAnInvestment()
        );

        $this->combinedRolloverForm->validate($validation_data);

        PortfolioTransactionApproval::add(['institution_id'=>$oldInvestment->security->investor->id,
            'transaction_type'=>'combined_rollover', 'payload'=>$input]);

        \Flash::message('The rollover has been sent for approval');

        return redirect('/dashboard/portfolio');
    }
}
