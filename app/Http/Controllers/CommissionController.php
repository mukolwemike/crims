<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPayment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientRank;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Investment\AdditionalCommission;
use App\Cytonn\Models\Investment\AdditionalCommissionType;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Investment\Forms\CommissionRecepientForm;
use Cytonn\Investment\Forms\CommissionRecipientTypeForm;
use Cytonn\Models\Investment\AdvanceCommissionType;
use Cytonn\Models\Investment\AdvanceInvestmentCommission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

/**
 * Class CommissionController
 */
class CommissionController extends Controller
{

    /**
     * @var CommissionRecepientForm
     */
    protected $recepientForm;

    /**
     * @var CommissionRecipientTypeForm
     */
    protected $commissionRecipienttypeForm;

    public function __construct(
        CommissionRecepientForm $recepientForm,
        CommissionRecipientTypeForm $commissionRecipientTypeForm
    ) {
        parent::__construct();

        //        $this->authorizer->checkAuthority('addinvestment');

        $this->recepientForm = $recepientForm;

        $this->commissionRecipienttypeForm = $commissionRecipientTypeForm;
    }


    /**
     * Display the grid of commission recepients
     *
     * @return Response
     */
    public function index()
    {
        $currencies = Currency::all();

        return view('investment.commission.index', ['title' => 'Commission', 'currencies' => $currencies]);
    }

    /**
     * Display the grid of commission payment dates
     *
     * @return View
     */
    public function getPaymentDates()
    {
        return view('investment.commission.payment_dates', ['title' => 'Commission Payment Dates']);
    }

    public function postSavePaymentDate()
    {
        $input = Request::except('_token');

        $end_day = Carbon::parse($input['end'])->day;

        $previous = BulkCommissionPayment::where('start', '<=', $input['end'])->first();

        if (!is_null($previous)) {
            if ($previous->end->copy()->diffInDays(Carbon::parse($input['start'])) <= 0 ||
                Carbon::parse($input['start'])->lte($previous->end->copy())) {
                throw new ClientInvestmentException('The date is before last months end date');
            }
        }

        ClientTransactionApproval::add(
            ['client_id' => null, 'transaction_type' => 'save_commission_payment_dates',
                'payload' => $input, 'scheduled' => false]
        );

        \Flash::message('The commission payment dates have been saved for approval');

        return redirect('/dashboard/investments/commission/payment-dates/list');
    }

    public function showPaymentDate($id)
    {
        $bcp = BulkCommissionPayment::findOrFail($id);

        return view('investment.commission.payment_date', ['title' => 'Commission Payment Dates', 'bcp' => $bcp]);
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function addRecepient($id = null)
    {
        is_null($id) ? $recepient = new CommissionRecepient() : $recepient = CommissionRecepient::findOrFail($id);

        $recepienttypes = CommissionRecipientType::all()->mapWithKeys(
            function ($recepienttype) {
                return [$recepienttype['id'] => $recepienttype['name']];
            }
        );

        $recepientbanks = CommissionRecipientRank::all()->mapWithKeys(
            function ($recepientbank) {
                return [$recepientbank['id'] => $recepientbank['name']];
            }
        );

        $commissionrates = CommissionRate::all()->mapWithKeys(
            function ($commissionrate) {
                return [$commissionrate['id'] => $commissionrate['name']];
            }
        );

        $hasAccount = $recepient->repo->hasAccount();

        return view(
            'investment.commission.recepient',
            [
                'title' => 'Add/Edit FA details', 'recipient' => $recepient,
                'has_account' => $hasAccount, 'recepienttypes' => $recepienttypes,
                'recepientbanks' => $recepientbanks, 'commissionrates' => $commissionrates
            ]
        );
    }

    /**
     * @param null $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveRecepient($id = null)
    {
        if ($id) {
            $this->recepientForm->editing();
        }


        $this->recepientForm->validate(\Request::all());

        $input = \Request::except('_token');
        $input['zero_commission'] = isset($input['zero_commission']) ? true : false;

        is_null($id) ?
            $recipient = new CommissionRecepient()
            : $recipient = CommissionRecepient::findOrFail($id);

        DB::transaction(
            function () use ($recipient, $input) {
                unset($input['create_account']);
                $recipient->fill($input);

                $recipient->save();

                if (!$recipient->referral_code) {
                    $recipient->repo->assignReferralCode();
                }

                if (boolval(\Request::get('create_account'))) {
                    $recipient->repo->createAccount();
                }
            }
        );

        \Flash::success('The FA records have been saved successfully');

        return \redirect("/dashboard/investments/commission/owner/$recipient->id");
    }

    /**
     * Show details about a commission recipient
     *
     * @param  $id
     * @return mixed
     */
    public function recipient($id)
    {
        //TODO add time, filter by active
        $recipient = CommissionRecepient::findOrFail($id);

        $commissions = Currency::all()->map(
            function ($currency) use ($recipient) {
                $cut_off_date = (new Carbon(\Request::get('date')))->startOfMonth()->addDays(20);

                $product_ids = Product::where('currency_id', $currency->id)->get()->lists('id');

                $c = new \stdClass();

                $commissions = Commission::where('recipient_id', $recipient->id)->whereHas(
                    'investment',
                    function ($q) use ($product_ids, $cut_off_date) {
                        $q->whereIn('product_id', $product_ids)
                            ->where('invested_date', '<=', $cut_off_date->copy()->addDay())
                            ->where(
                                function ($query) use ($cut_off_date) {
                                    // add all active or withdrawn within last month
                                    $query->where('withdrawn', null)->orWhere('withdrawn', 0)
                                        ->orWhere('withdrawn', true)->where(
                                            'withdrawal_date',
                                            '>=',
                                            $cut_off_date->copy()->subMonthNoOverflow()
                                        );
                                }
                            );
                    }
                )->get();

                $c->commissions = $commissions;
                $c->totals = \Cytonn\Presenters\InvestmentPresenter::getCommissionTotals($commissions);
                $c->currency = $currency;

                return $c;
            }
        );

        return view(
            'investment.commission.recipientdetail',
            ['title' => 'FA Commission', 'recipient' => $recipient, 'commissions' => $commissions]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function payments($id)
    {
        $recipient = CommissionRecepient::findOrFail($id);

        $date = (new Carbon(\Request::get('date')));

        $currencies = (new CommissionRepository())->getCurrenciesForRecipient($recipient);

        $bulk = BulkCommissionPayment::includes($date)->first();

        if (!$bulk) {
            $start = BulkCommissionPayment::latest()->first()->end->addDay();
            $end = Carbon::today();
        } else {
            $start = $bulk->start;
            $end = $bulk->end;
        }

        $currencies->each(function ($c) use ($recipient, $date, $start, $end) {
            $calculator = $recipient->calculator($c, $start, $end);

            $summary = $calculator->summary();

            $c->summary = $summary;
            $c->calculator = $calculator;
            $c->total = $summary->total();
            $c->compliant = $c->calculator->schedules();
        });

        $additionalCommissionCalculator = $recipient->additionalCommissionCalculator($start, $end);

        $advanceCommissionTypes = ['' => 'Selected Additional Commission Type'] + AdditionalCommissionType::where('id', '!=', 3)->pluck('name', 'id')->toArray();

        return view('investment.commission.payments', [
            'title' => 'Commission payments',
            'recipient' => $recipient,
            'currencies' => $currencies,
            'date' => $date,
            'bulk' => $bulk,
            'start' => $start,
            'end' => $end,
            'advanceCommissionTypes' => $advanceCommissionTypes,
            'additionalCommissionCalculator' => $additionalCommissionCalculator
        ]);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function bulkPayments($id)
    {
        $bcp = BulkCommissionPayment::findOrFail($id);

        $input['bcp_id'] = $bcp->id;

        $approval = ClientTransactionApproval::add(
            [
                'client_id' => null,
                'transaction_type' => 'bulk_commission_payments',
                'payload' => $input,
                'scheduled' => false
            ]
        );

        $bcp->investmentsApproval()->associate($approval);
        $bcp->save();

        \Flash::message('Commission payments have been saved for approval');

        return \Redirect::back();
    }

    public function combinedBulkPayments($id)
    {
        $bcp = BulkCommissionPayment::findOrFail($id);

        $input['bcp_id'] = $bcp->id;

        $approval = ClientTransactionApproval::add([
            'client_id' => null,
            'transaction_type' => 'bulk_combined_commission_payments',
            'payload' => $input,
            'scheduled' => false
        ]);

        $bcp->combinedApproval()->associate($approval);
        $bcp->save();

        \Flash::message('Combined Commission payments have been saved for approval');

        return \Redirect::back();
    }

    /**
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function addNewFaType()
    {
        $this->commissionRecipienttypeForm->validate(Request::all());

        $recipientType = new CommissionRecipientType();
        $recipientType->fill(Request::all());
        $recipientType->save();

        \Flash::message('New FA Type Added');

        return Redirect::back();
    }

    /**
     * @param $recipient_id
     * @return mixed
     */
    public function addCommissionPaymentSchedule($recipient_id)
    {
        $investment_id = \Request::get('investment_id');
        $recipient = CommissionRecepient::findOrFail($recipient_id);
        $investment = ClientInvestment::findOrFail($investment_id);

        $input['investment_id'] = $investment->id;
        $input['commission_recipient_id'] = $recipient->id;
        $input['date'] = \Request::get('date');
        $input['amount'] = \Request::get('amount');
        $input['description'] = \Request::get('description');

        ClientTransactionApproval::add(
            [
                'client_id' => $investment->client_id,
                'transaction_type' => 'add_commission_payment_schedule',
                'payload' => $input
            ]
        );

        \Flash::message('Request to add a new commission payment schedule has been saved for approval');
        return Redirect::back();
    }

    /**
     * @param $scheddule_id
     * @return mixed
     */
    public function editCommissionPaymentSchedule($scheddule_id)
    {
        $schedule = CommissionPaymentSchedule::findOrFail($scheddule_id);
        $recipient = $schedule->commission->recipient;
        $investment = $schedule->commission->investment;

        $input['investment_id'] = $investment->id;
        $input['schedule_id'] = $schedule->id;
        $input['commission_recipient_id'] = $recipient->id;
        $input['date'] = \Request::get('date');

        ClientTransactionApproval::add(
            [
                'client_id' => $investment->client_id,
                'transaction_type' => 'edit_commission_payment_schedule',
                'payload' => $input
            ]
        );

        \Flash::message('Request to edit the commission payment schedule has been saved for approval');
        return Redirect::back();
    }

    /**
     * @param $recipient_id
     * @return mixed
     */
    public function addCommissionClawback($recipient_id)
    {
        $recipient = CommissionRecepient::findOrFail($recipient_id);
        $investment_id = \Request::get('investment_id');
        $investment = ClientInvestment::findOrFail($investment_id);

        $input['investment_id'] = $investment->id;
        $input['commission_recipient_id'] = $recipient->id;
        $input['date'] = \Request::get('date');
        $input['amount'] = \Request::get('amount');
        $input['narration'] = \Request::get('narration');

        ClientTransactionApproval::add(
            [
                'client_id' => $investment->client_id,
                'transaction_type' => 'add_commission_clawback',
                'payload' => $input
            ]
        );

        \Flash::message('Request to add a new commission clawback has been saved for approval');
        return Redirect::back();
    }

    public function updateCommissionClawback($id)
    {
        $clawback = CommissionClawback::findOrFail($id);
        $commission = $clawback->commission;
        $investment = $commission->investment;

        $input['date'] = \Request::get('date');
        $input['amount'] = \Request::get('amount');
        $input['narration'] = \Request::get('narration');
        $input['id'] = $clawback->id;

        ClientTransactionApproval::add(
            [
                'client_id' => $investment->client_id,
                'transaction_type' => 'update_commission_clawback',
                'payload' => $input
            ]
        );

        \Flash::message('Request to update the commission clawback has been sent for approval');
        return Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteCommissionClawback($id)
    {
        $clawback = CommissionClawback::findOrFail($id);
        $commission = $clawback->commission;
        $investment = $commission->investment;

        ClientTransactionApproval::add(
            [
                'client_id' => $investment->client_id,
                'transaction_type' => 'delete_commission_clawback',
                'payload' => ['id' => $clawback->id]
            ]
        );

        \Flash::message('Request to delete the commission clawback has been sent for approval');
        return Redirect::back();
    }

    public function staggerCommissionClawbacks($id)
    {
        $input = \Request::except(['_token']);

        $commission = Commission::findOrFail($id);
        $investment = $commission->investment;

        if ($input['clawback_id']) {
            $input['eligible_clawbacks'] = [$input['clawback_id']];
        } else {
            $latestBulk = BulkCommissionPayment::orderBy('end', 'DESC')->first();

            $eligibleClawbacks = $commission->clawBacks()
                ->where('date', '>=', $latestBulk->end)->pluck('id')->toArray();

            $input['eligible_clawbacks'] = $eligibleClawbacks;
        }

        ClientTransactionApproval::add([
            'client_id' => $investment->client_id,
            'transaction_type' => 'stagger_commission_clawback',
            'payload' => $input
        ]);

        \Flash::message('Request to stagger the commission clawback has been sent for approval');

        return Redirect::back();
    }

    /*
     * View all the commission recipients in the system
     */
    public function viewCommissionRecipients()
    {
        return view('investment.commission.commission_recipients');
    }

    /*
     * View all the positions linked to a give commission recipient
     */
    public function viewCommissionRecipientPositions($id)
    {
        $commissionRecipient = CommissionRecepient::findOrFail($id);

        return view('investment.commission.commission_recipient_positions', [
            'commissionRecipient' => $commissionRecipient
        ]);
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAdditionalCommission($id = null)
    {
        $input = \Request::except(['_token']);

        $input['id'] = $id;

        if ($id) {
            $advanceCommission = AdditionalCommission::findOrFail($id);

            if (!$advanceCommission->present()->canEdit) {
                \Flash::error('The advance commission cannot be edited as the bulk commission period is closed');

                return \Redirect::back();
            }
        }

        ClientTransactionApproval::add([
            'client_id' => null,
            'transaction_type' => 'store_advance_investment_commission',
            'payload' => $input
        ]);

        \Flash::message('Transaction has been sent for approval');

        return Redirect::back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportPensionCommission($id)
    {
        $input = request()->all();

        $user = \Auth::id();

        $bcpId = $input['bcp_id'];

        $bulk = BulkCommissionPayment::findOrFail($bcpId);

        $processed = CommissionPayment::where('approval_id', $bulk->combined_approval_id)->exists();

        if (!$bulk->combinedApproval->approved || ! $processed) {
            \Flash::error("Please approve the combined commission approval first");

            return back();
        }

        $this->queue(
            function () use ($bcpId, $user) {
                \Artisan::call('crims:pension_commission_summary', [
                    'bcp_id' => $bcpId,
                    'user_id' => $user
                ]);
            }
        );

        \Flash::success('The pension commission report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }
}
