<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Jobs\Custodial\CustodialAccountsExport;
use App\Jobs\Portfolio\DepositAnalysis;
use App\Jobs\Portfolio\ResidualIncomeAnalysis;
use App\Jobs\Portfolio\InvestmentsOnCallReport;
use App\Jobs\Portfolio\PortfolioSummary;
use App\Jobs\Portfolio\PortfolioWithHoldingTax;
use App\Jobs\Portfolio\InterestExpenseReport;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Investment\Reports\Builders\ClientSummary;
use Cytonn\Notifier\FlashNotifier;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

class PortfolioReportsController extends Controller
{




    /**
     * InvestmentReportsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display the  portfolio reports
     *
     * @return Response
     */
    public function getIndex()
    {
        $fundManagers = ['' => 'Select fund manager'] + FundManager::pluck('name', 'id')->toArray();

        return view('portfolio.reports.reports', [
            'title' => 'Portfolio Reports',
            'fundManagers' => $fundManagers
        ]);
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function exportInterestExpense()
    {
        $this->authorizer->authorize('portfolio:reports');

        $input = Request::all();
        $start = Carbon::parse($input['start_date']);
        $end = Carbon::parse($input['end_date']);
        $fundManager = FundManager::find($this->fundManager()->id);

        dispatch(new InterestExpenseReport($start, $end, \Auth::user(), $fundManager));

        \Flash::success('The interest expense report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportWithholdingTax()
    {
        $this->authorizer->authorize('portfolio:reports');
        $input = Request::all();
        $start = Carbon::parse($input['start_date']);
        $end = Carbon::parse($input['end_date']);
        $fundManager = FundManager::find($this->fundManager()->id);

        dispatch(new PortfolioWithHoldingTax($start, $end, \Auth::user(), $fundManager));

        \Flash::success('The withholding tax report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function exportCustodialAccounts()
    {
        $this->authorizer->authorize('portfolio:reports');
        $userId = auth()->user()->id;
        $input = request()->all();

        $account = (new CustodialAccount())->findOrFail($input['custodial_account_id'])->id;
        $start = Carbon::parse($input['start'])->toDateString();
        $end = Carbon::parse($input['end'])->toDateString();

        $this->queue(
            function () use ($userId, $account, $start, $end) {
                \Artisan::queue(
                    'portfolio:custodial_account_report',
                    [ 'user_id' => $userId, 'account' => $account, 'start' => $start, 'end'=>$end ]
                );
            }
        );

        \Flash::success('The custodial account export will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportResidualIncomeAnalysis()
    {
        $this->authorizer->authorize('portfolio:reports');
        $input = Request::all();
        $fundManager = FundManager::findOrFail($input['fund_manager_id']);
        $date = Carbon::parse($input['date']);

        dispatch(new ResidualIncomeAnalysis($fundManager, $date, \Auth::user()));

        \Flash::success('The Residual Income Analysis export will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportDepositAnalysis()
    {
        $this->authorizer->authorize('portfolio:reports');
        $input = Request::all();
        $fundManager = FundManager::findOrFail($input['fund_manager_id']);
        $date = Carbon::parse($input['date']);

        dispatch(new DepositAnalysis($fundManager, $date, \Auth::user()));

        \Flash::success('The deposit analysis export will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportInvestmentsOnCall()
    {
        $this->authorizer->authorize('portfolio:reports');

        $fund_manager_id = $this->fundManager()->id;
        $fundManager = FundManager::findOrFail($fund_manager_id);

        dispatch(new InvestmentsOnCallReport(\Auth::user(), $fundManager));

        \Flash::success('The investments on-call report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportPortfolioSummary()
    {
        $this->authorizer->authorize('portfolio:reports');

        $input = request()->all();
        $fundManager = FundManager::findOrFail($input['fund_manager_id']);
        $date = Carbon::parse($input['date']);

        dispatch(new PortfolioSummary($date, $fundManager, auth()->user()));

        \Flash::success('The portfolio summary will be sent to your inbox shortly');

        return \Redirect::back();
    }

    /*
     * Export the year deposit holdings report
     */
    public function exportYearDepositHoldings()
    {
        $this->authorizer->authorize('portfolio:reports');
        $userId = \Auth::user()->id;
        $fundManager = $this->fundManager()->id;

        $this->queue(
            function () use ($userId, $fundManager) {
                \Artisan::call(
                    'portfolio:yearly_investments_summary',
                    ['user_id' => $userId, 'fund_manager_id' => $fundManager]
                );
            }
        );

        \Flash::success('The deposit holdings report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /*
     * Export the investor valuation report
     */
    public function exportInvestorValuation()
    {
        $this->authorizer->authorize('portfolio:reports');
        $userId = \Auth::user()->id;

        $this->queue(
            function () use ($userId) {
                \Artisan::call(
                    'portfolio:investor_valuation',
                    ['user_id' => $userId]
                );
            }
        );

        \Flash::success('The investor valuation report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /*
     * Export portfolio movement audit
     */
    public function exportPortfolioMovementAudit()
    {
        $this->authorizer->authorize('portfolio:reports');
        $input = Request::all();
        $user = Auth::id();
        $fundmanager = $input['fund_manager_id'];
        $start = $input['start_date'];
        $end = $input['end_date'];

        $this->queue(
            function () use ($fundmanager, $start, $end, $user) {
                \Artisan::call(
                    'crims:portfolio_movement_audit',
                    [
                        'fundmanager' => $fundmanager,
                        'start' => $start,
                        'end' => $end,
                        'user_id' => $user
                    ]
                );
            }
        );

        \Flash::success('The portfolio movement report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /*
     * Export loan tenor range report
     */
    public function exportLoanTenorRangeReport()
    {
        $this->authorizer->authorize('portfolio:reports');

        $input = Request::all();
        $user = Auth::id();
        $fundmanager = $input['fund_manager_id'];
        $start = $input['start_date'];
        $end = $input['end_date'];
        $type = $input['type_id'];

        $this->queue(
            function () use ($fundmanager, $start, $end, $user, $type) {
                \Artisan::call(
                    'crims:tenor_range',
                    [
                        'fundmanager' => $fundmanager,
                        'start' => $start,
                        'end' => $end,
                        'user_id' => $user,
                        'type_id' => $type
                    ]
                );
            }
        );

        \Flash::success('The loan tenor range report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /*
     * Export unit fund transaction report
     */
    public function exportUnitFundTransactionSummary()
    {
        $this->authorizer->authorize('investments:admin-fund-reports');

        $input = Request::all();
        $user = Auth::id();
        $unitFund = $input['unit_fund_id'];
        $start = $input['start'];
        $end = $input['end'];

        $this->queue(
            function () use ($unitFund, $start, $end, $user) {
                \Artisan::call(
                    'crims:unit_fund_transactions',
                    [
                        'unit_fund' => $unitFund,
                        'start' => $start,
                        'end' => $end,
                        'user_id' => $user,
                    ]
                );
            }
        );

        \Flash::success('The unit fund transactions report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /*
     * Export unit fund holdings report
     */
    public function exportUnitFundHoldingsSummary()
    {
        $this->authorizer->authorize('investments:admin-fund-reports');

        $input = Request::all();
        $user = Auth::id();
        $unitFund = $input['unit_fund_id'];
        $date = $input['date'];

        $this->queue(
            function () use ($unitFund, $date, $user) {
                \Artisan::call(
                    'crims:unit_fund_balances',
                    [
                        'unit_fund' => $unitFund,
                        'date' => $date,
                        'user_id' => $user,
                    ]
                );
            }
        );

        \Flash::success('The unit fund holdings report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    /*
     * Export performance attribution summary
     */
    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function exportPerformanceAttributionSummary()
    {
        $this->authorizer->authorize('investments:admin-fund-reports');

        $input = Request::all();
        $user = Auth::id();
        $fundmanger = request()->get('fundmanager');
        $start = $input['start'];
        $end = $input['end'];
        $fund = \request()->get('unit_fund_id');

        if (!FundManager::find($fundmanger)) {
            $f = UnitFund::findOrFail($fund);
            $fundmanger = $f->manager->id;
        }

        $this->queue(
            function () use ($fundmanger, $start, $end, $user, $fund) {
                \Artisan::call(
                    'crims:performance_attribution_summary',
                    [
                        'start' => $start,
                        'end' => $end,
                        '--fm' => $fundmanger,
                        '--user_id' => $user,
                        '--fund' => $fund,
                    ]
                );
            }
        );

        \Flash::success('The performance attribution report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportFundValuationReport($id = null)
    {
        $this->authorizer->authorize('investments:admin-fund-reports');

        if (!$id) {
            $id = request()->get('unit_fund_id');
        }

        $user = Auth::id();

        $end = \request('end');
        $start = \request('start');

        $this->queue(
            function () use ($id, $user, $start, $end) {
                \Artisan::call('portfolio:summary-valuation', [
                    'start' => $start,
                    'end' => $end,
                    'fund_id' => $id,
                    '--user' => $user
                ]);
            }
        );

        \Flash::success('The summary valuation report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function exportFundDepositHoldings()
    {
        $this->authorizer->authorize('investments:admin-fund-reports');

        $id = request()->get('unit_fund_id');

        $user = Auth::id();
        $end = \request('end');
        $start = \request('start');

        $this->queue(
            function () use ($id, $user, $start, $end) {
                \Artisan::call('portfolio:export-deposits', [
                    'start' => $start,
                    'end' => $end,
                    '--fund' => $id,
                    '--user_id' => $user
                ]);
            }
        );

        \Flash::success('The deposit holdings report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportFundPricingReport($id = null)
    {
        if (!$id) {
            $id = request()->get('unit_fund_id');
        }

        $user = Auth::id();

        $fundManager = $this->fundManager()->id;

        $date = \request('date');

        $this->queue(
            function () use ($id, $user, $fundManager, $date) {
                \Artisan::call('portfolio:fund-pricing', [
                    'unit_fund_id' => $id,
                    'user_id' => $user,
                    'fund_manager_id' => $fundManager,
                    'date' => $date
                ]);
            }
        );

        \Flash::success('The fund pricing report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }

    public function exportFundManagementSummaryReport($id = null)
    {
        if (!$id) {
            $id = request()->get('unit_fund_id');
        }

        $user = Auth::id();

        $start = \request('start');

        $end = \request('end');

        $this->queue(
            function () use ($id, $user, $start, $end) {
                \Artisan::call('portfolio:fund_management_summary', [
                    'unit_fund_id' => $id,
                    'user_id' => $user,
                    'start' => $start,
                    'end' => $end,
                ]);
            }
        );

        \Flash::success('The fund management summary report will be generated and sent to you shortly via email');

        return \Redirect::back();
    }
}
