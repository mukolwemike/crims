<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Company;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\ContactEntity;
use Cytonn\Notifier\FlashNotifier;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

/**
 * Date: 8/27/15
 * Time: 10:49 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


class ContactsController extends Controller
{
    /**
     * @var \Cytonn\Contacts\AddContactsForm
     */
    protected $contactForm;

    protected $authorizer;



    /**
     * ContactsController constructor.
     *
     * @param $contactForm
     */
    public function __construct(\Cytonn\Contacts\AddContactsForm $contactForm)
    {
        $this->contactForm = $contactForm;

        $this->authorizer = new \Cytonn\Authorization\Authorizer();
    }


    /**
     * Show the contacts table
     *
     * @return mixed
     */
    public function index()
    {
        $this->authorizer->checkAuthority('viewcontacts');

        return view('contacts.index', ['title'=>'Contacts']);
    }


    /**
     * Show the form to create or edit a contact
     *
     * @param  null $id
     * @return mixed
     */
    public function create($id = null)
    {
        $this->authorizer->checkAuthority('addcontacts');


        if ($id==null) {
            $contact = new Contact();
        } else {
            $contact = Contact::find($id);

            if (is_null($contact)) {
                return 'The contact you are editing does not exist';
            }
        }

        return view('contacts.create', ['title'=>'Create/Edit Contact', 'contact'=>$contact]);
    }

    /**
     * Save the contact
     *
     * @param  null $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function store($id = null)
    {
        $this->authorizer->checkAuthority('addcontacts');

        $input = Request::all();

        if (Request::get('individual_entity_type') == 1) {
            $input = array_add(
                $input,
                'entity_type_id',
                ContactEntity::where('name', 'individual')->first()->id
            );
        } else {
            $input = array_add(
                $input,
                'entity_type_id',
                ContactEntity::where('name', 'corporate')->first()->id
            );
        }

        unset($input['individual_entity_type']);

        $input = $this->getCompany($input);


        if (is_null($id)) {
            $contact = new Contact();

            $this->contactForm->createrules();
        } else {
            $contact = Contact::findOrFail($id);

            $this->contactForm->updaterules();
        }


        $this->contactForm->validate($input);

        $contact->fill($input);

        if ($contact->save()) {
            Flash::success('Contact saved succesfully');

            try {
                return Redirect::back();
            } catch (\Exception $e) {
                return redirect('/dashboard/contacts');
            }
        } else {
            Flash::error('Could not save contact');

            return Redirect::back();
        }
    }

    /**
     * Adds company_id from input  array and returns array with company_id
     *
     * @param  $input Form Input
     * @return array
     */
    protected function getCompany($input)
    {
        $company_name = trim($input['company']);

        $id = null;

        if (strlen($company_name) <= 1) {
            $id = null;
        }

        $company = Company::where('name', $company_name)->first();

        if ($company) {
            $id = $company->id;
        } else {
            $company = new Company();
            $company->name = $company_name;
            $company->sector_id = null;
            $company->save();

            $id = $company->id;
        }
        unset($input['company_id']);//remove initial company id to allow for updates

        $input = array_add($input, 'company_id', $id);

        unset($input['company']);



        return $input;
    }
}
