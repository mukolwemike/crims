<?php
/**
 * Date: 13/07/2017
 * Time: 09:36
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers;

class SystemController extends Controller
{
    public function index()
    {
        return view('system.index');
    }
}
