<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharesBusinessConfirmation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Class SharesBusinessConfirmationsController
 */
class SharesBusinessConfirmationsController extends Controller
{

    /**
     * Used to check for authorization
     *
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;


    /**
     * SharesController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();

        parent::__construct();
    }

    /**
     * Show the business confirmations' grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        return view('shares.confirmations.index', ['title' => 'Shares Business Confirmations']);
    }

    /**
     * Show the details of the business confirmations
     *
     * @param  $id
     * @return mixed
     */
    public function getShow($id)
    {
        $holding = ShareHolding::findOrFail($id);

        $previousConfirmations = SharesBusinessConfirmation::where('holding_id', $id)->latest()->get();

        $is_bc_sent = (bool) SharesBusinessConfirmation::where('holding_id', $id)->count() > 0;

        return view(
            'shares.confirmations.show',
            [
                'title'                     =>  'Shares Business Confirmation',
                'holding'                   =>  $holding,
                'previousConfirmations'     =>  $previousConfirmations,
                'is_bc_sent'                =>  $is_bc_sent
            ]
        );
    }

    public function postSend($id)
    {
        $holding = ShareHolding::findOrFail($id);

        $sender = Auth::user()->id;

        $confirmation = new \Cytonn\Shares\Reporting\BusinessConfirmationGenerator();

        $bc = $confirmation->generate($holding, $holding->approval->approved_by)->stream();

        $client = Client::findOrFail($holding->shareHolder->client_id);

        (new \Cytonn\Mailers\Shares\BusinessConfirmationMailer())->sendBC($client, $bc, $holding);

        $payload = [
            'number' => $holding->number, 'sender' => $sender, 'holding_id' => $holding->id,
            'purchase_price' => $holding->puchase_price, 'date' => $holding->date
        ];

        $data = ['holding_id' => $holding->id, 'payload' => $payload];

        SharesBusinessConfirmation::add($data);

        \Flash::success('The Shares Business Confirmation has been sent');

        return Redirect::back();
    }
}
