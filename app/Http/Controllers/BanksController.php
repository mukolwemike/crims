<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Banks\Forms\BankForm;
use Cytonn\Banks\Forms\BranchForm;

/**
 * Class BanksController
 */
class BanksController extends Controller
{

    /**
     * @var BankForm $bankForm
     */
    public $bankForm;

    /**
     * @var BranchForm $branchForm
     */
    private $branchForm;

    /**
     * BanksController constructor.
     *
     * @param    BankForm   $bankForm
     * @param    BranchForm $branchForm
     * @internal param BanksController $bankForm
     */
    public function __construct(BankForm $bankForm, BranchForm $branchForm)
    {
        $this->bankForm = $bankForm;

        $this->branchForm = $branchForm;

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('banks.index', ['title'=>'Banks']);
    }

    /**
     * @param ClientBank $bank
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(ClientBank $bank)
    {
        return view('banks.show', compact('bank'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('banks.create');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {
        $input = request()->all();

        $this->bankForm->validate($input);

        ClientTransactionApproval::make(null, 'create_bank', $input);

        \Flash::message('The bank has been saved for approval');

        return redirect('dashboard/banks');
    }

    /**
     * @param ClientBank $bank
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ClientBank $bank)
    {
        return view('banks.edit', compact('bank'));
    }

    /**
     * @param ClientBank $bank
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ClientBank $bank)
    {
        $input = request()->all();

        $this->bankForm->validate($input);

        $input['bank_id'] = $bank->id;

        ClientTransactionApproval::make(null, 'update_bank', $input);

        \Flash::message('The request to update the bank has been saved for approval');

        return redirect("dashboard/banks/{$bank->id}");
    }

    /**
     * @param ClientBank $bank
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addBranch(ClientBank $bank)
    {
        $input = request()->all();

        $this->branchForm->validate($input);

        $input['bank_id'] = $bank->id;

        ClientTransactionApproval::make(null, 'create_bank_branch', $input);

        \Flash::message('The branch has been saved for approval');

        return redirect("dashboard/banks/{$bank->id}");
    }

    /**
     * @param ClientBank $bank
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateBranch(ClientBank $bank)
    {
        $input = request()->all();

        $this->branchForm->validate($input);

        $input['bank_id'] = $bank->id;

        ClientTransactionApproval::make(null, 'update_bank_branch', $input);

        \Flash::message('The request to update the branch has been saved for approval');

        return redirect("dashboard/banks/{$bank->id}");
    }
}
