<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use Cytonn\Exceptions\CRIMSGeneralException;

/**
 * Date: 14/07/2016
 * Time: 9:38 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class DocumentController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function main()
    {
        $projects = Project::all()
            ->each(
                function ($project) {
                    $project->documentTypes = DocumentType::all()
                        ->each(
                            function ($type) use ($project) {
                                $type->numberOfDocuments =
                                    $type->documents()->where('project_id', $project->id)->approved()->count();
                            }
                        );
                }
            );

        $products = Product::all()
            ->each(
                function ($product) {
                    $product->documentTypes = DocumentType::all()
                        ->each(
                            function ($type) use ($product) {
                                $type->numberOfDocuments =
                                    $type->documents()->where('product_id', $product->id)->approved()->count();
                            }
                        );
                }
            );

        $otherTypes = DocumentType::whereHas(
            'documents',
            function ($document) {
                return $document->whereNull('project_id')->whereNull('product_id');
            }
        )
            ->get()
            ->each(
                function ($type) {
                    $type->numberOfDocuments = $type->documents()->whereNull('project_id')
                        ->whereNull('product_id')->approved()->count();
                }
            );

        $portfolioInvestors = PortfolioInvestor::orderBy('name')->get()
            ->each(
                function ($investor) {
                    $investor->alphabet = strtoupper(substr($investor->name, 0, 1));
                    $investor->documentTypes = DocumentType::all()
                        ->each(
                            function ($type) use ($investor) {
                                $type->numberOfDocuments = $type->documents()
                                    ->where('portfolio_investor_id', $investor->id)->approved()->count();
                            }
                        );
                }
            )->groupBy('alphabet');

        return view(
            'documents.main',
            ['projects' => $projects, 'products' => $products,
                'portfolioInvestorGroups' => $portfolioInvestors, 'otherTypes' => $otherTypes]
        );
    }

    /**
     * @param $module
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($module, $slug, $sectionId = null)
    {
        if (!in_array($module, ['investments', 'realestate', 'others', 'portfolio_investors'])) {
            \Flash::error('Unknown module ' . $module);

            return back();
        }

        $products = Product::all()->lists('name', 'id');

        $products = [0 => 'All Products'] + $products;

        $projects = Project::all()->lists('name', 'id');

        $projects = [0 => 'All Projects'] + $projects;

        $portfolioInvestors = [0 => 'All Investors'] + PortfolioInvestor::pluck('name', 'id')->toArray();

        $document_types = DocumentType::where('direct_upload', true)->get()->lists('name', 'id');

        return view(
            'documents.index',
            [
                'title' => 'CRIMS Documents', 'document_types' => $document_types, 'products' => $products,
                'projects' => $projects, 'portfolioInvestors' => $portfolioInvestors, 'module' => $module,
                'slug' => $slug, 'sectionId' => $sectionId
            ]
        );
    }

    /**
     * @return mixed
     * @throws CRIMSGeneralException
     */
    public function upload()
    {
        $file = \Request::file('file');

        $document_type = DocumentType::find(\Request::get('type'));
        $title = \Request::get('title');
        $date = \Request::get('date');
        $project_id = \Request::get('project_id');
        $product_id = \Request::get('product_id');
        $portfolioInvestorId = \Request::get('portfolio_investor_id');
        $compliance_document = (bool)\Request::get('compliance_document');

        if (!\Request::hasFile('file')) {
            throw new CRIMSGeneralException('No document attached!');
        }

        $data = request()->all();

        $client_id = isset($data['client_id']) ? $data['client_id'] : null;

        $document = (new Document())->upload(
            file_get_contents($file),
            $file->getClientOriginalExtension(),
            $document_type->slug,
            $title,
            $date,
            $project_id,
            $product_id,
            $compliance_document,
            $portfolioInvestorId,
            $client_id,
            auth()->user()->id
        );

        $input['document_id'] = $document->id;

        ClientTransactionApproval::make(null, 'upload_document', $input);

        \Flash::message('The document has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @return mixed
     * @throws CRIMSGeneralException
     */
    public function update()
    {
        $old_doc = Document::findOrFail(\Request::get('document_id'));
        $file = \Request::file('file');

        $document_type = DocumentType::find(\Request::get('type'));
        $title = \Request::get('title');
        $date = \Request::get('date');
        $project_id = \Request::get('project_id');
        $product_id = \Request::get('product_id');
        $portfolioInvestorId = \Request::get('portfolio_investor_id');
        $compliance_document = (bool)\Request::get('compliance_document');

        if (!\Request::hasFile('file')) {
            throw new CRIMSGeneralException('No document attached!');
        }

        $document = $old_doc->updateDoc(
            file_get_contents($file),
            $file->getClientOriginalExtension(),
            $document_type->slug,
            $title,
            $date,
            $project_id,
            $product_id,
            $compliance_document,
            $portfolioInvestorId
        );

        if ($old_doc) {
            $document->addHistory($old_doc);
        }

        $input['document_id'] = $document->id;

        ClientTransactionApproval::make(null, 'update_document', $input);

        \Flash::message('The request to update the document has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function delete()
    {
        $document = Document::findOrFail(\Request::get('document_id'));

        $input['document_id'] = $document->id;

        ClientTransactionApproval::make(null, 'delete_document', $input);

        \Flash::message('The request to delete the document has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);

        return $this->streamFile($document->path());
    }
}
