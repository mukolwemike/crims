<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\User;
use App\Events\Roles\RoleAdded;
use App\Events\Roles\RolePermissionsUpdated;
use App\Events\Users\UserAssignedPermission;
use Cytonn\Authorization\Permission;
use Cytonn\Authorization\Role;
use Cytonn\Notifier\FlashNotifier;
use Cytonn\Roles\RoleRepository;
use Cytonn\Roles\RoleRules;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use \Cytonn\Authorization\AddRoleForm;
use \Flash;

/**
 * Date: 9/22/15
 * Time: 10:26 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class UserAccessController extends Controller
{
    /*
     * Get the roles trait
     */
    use RoleRules;

    /*
     * Get the required repositories
     */
    protected $roleRepository;

    /*
     * Setup the constructor for the controller
     */
    public function __construct(RoleRepository $roleRepository)
    {
        parent::__construct();

        $this->authorizer = new \Cytonn\Authorization\Authorizer();

        $this->authorizer->checkAuthority('manageusers');

        $this->roleRepository = $roleRepository;
    }


    /**
     * Assign a role to a user
     *
     * @return mixed
     */
    public function assignUserRole()
    {
        $user = User::findOrFail(Crypt::decrypt(Request::get('user_id')));

        $role = Role::findOrFail(Request::get('role'));

        $user->addRole($role);

        \Flash::success('The user now has the role: ' . $role->description);

        return Redirect::back();
    }

    /**
     * Assign a permission to a user
     *
     * @return mixed
     */
    public function assignUserpermission()
    {
        $user = User::findOrFail(Crypt::decrypt(Request::get('user_id')));

        $permission = Permission::findOrFail(Request::get('permission'));

        $user->allow($permission);

        event(new UserAssignedPermission($user, $permission));

        Flash::success('The use has now been allowed the permission: ' . $permission->description);

        return Redirect::back();
    }

    /**
     * Show the form to create/edit a role
     *
     * @param  null $id
     * @return mixed
     */
    public function getAddRole($id = null)
    {
        $role = Role::findOrNew($id);

        return view('users.addrole', ['title' => 'Add a new role', 'role' => $role]);
    }


    /**
     * Add/edit a role
     *
     * @param  null $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function addRole(\Illuminate\Http\Request $request, $id = null)
    {
        $validation = $id ? $this->rolesUpdate($request, $id) : $this->rolesCreate($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $role = $this->roleRepository->save($request->all(), $id);

        if (is_null($id)) {
            event(new RoleAdded($role));
        }

        \Flash::success('Role succesfully saved');

        return redirect('/dashboard/users/roles');
    }

    /**
     * Grant permission to a role
     *
     * @return mixed
     */
    public function grantRolePermission(\Illuminate\Http\Request $request)
    {
        $role = $this->roleRepository->getRoleById($request->get('role'));

        $permissions = $role->permissions()->get()->lists('name');

        $inputs = $request->except(['_token', 'role']);

        $total = count($inputs);

        $count = 0;

        foreach (array_keys($inputs) as $input) {
            if (! in_array($input, $permissions)) {
                $permission = Permission::where('name', $input)->first();

                $role->permissions()->save($permission);

                $count++;
            }
        }

        if ($count > 0) {
            event(new RolePermissionsUpdated($role));
        }

        if ($count < $total) {
            \Flash::message('Some permissions were already assigned, the new ones have been added');
        } else {
            \Flash::success('Permission was successfully granted to role');
        }

        return back();
    }

    /**
     * Revoke permission on a user
     *
     * @return mixed
     */
    public function revokeRolePermission()
    {
        $role = Role::findOrFail(Request::get('role'));

        $permission = Permission::findOrFail(Request::get('permission'));

        $role->permissions()->detach($permission->id);

        Flash::success('Permission was revoked on the role');

        return Redirect::back();
    }

    /**
     * Remove a member from a role
     *
     * @return mixed
     * @throws \Exception
     */
    public function removeMember()
    {
        $membership = \Cytonn\Authorization\Membership::findOrFail(Request::get('member'));

        $membership->delete();

        Flash::success('Member has been removed from role');

        return Redirect::back();
    }
}
