<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Authentication\Events\UserHasLoggedIn;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Forms\LoginForm;
use Cytonn\Forms\OTPForm;
use Cytonn\Investment\FundManager\FundManagerScope;
use Cytonn\Notifier\FlashNotifier as Flash;
use Cytonn\Users\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use PragmaRX\Google2FA\Google2FA;
use SSOManager\Facades\SSO;

/**
 * Class LoginController
 */
class LoginController extends Controller
{
    use \Laracasts\Commander\Events\DispatchableTrait;

    /**
     * @var \Cytonn\Forms\LoginForm
     */
    private $loginForm;

    /**
     * @var Request
     */
    private $request;

    /*
     * @var int
     * How long the OTP are valid (minutes)
     */
    protected $window = 10;


    /**
     * @var \Cytonn\Forms\OTPForm
     */
    private $OTPForm;

    /*
     * Get the required repositories
     */
    private $userRepository;

    /**
     * LoginController constructor.
     *
     * @param LoginForm $loginForm
     * @param Request $request
     * @param OTPForm $OTPForm ;
     * @param UserRepository $userRepository
     */
    public function __construct(
        LoginForm $loginForm,
        Request $request,
        OTPForm $OTPForm,
        UserRepository $userRepository
    ) {
        $this->loginForm = $loginForm;

        $this->request = $request;

        $this->OTPForm = $OTPForm;

        $this->userRepository = $userRepository;

        parent::__construct();
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return redirect('/sso/authorize');
    }

    /*
     * Start the authorization process with SSO
     */
    public function ssoAuthorize()
    {
        if (\App::environment('local')) {
            return $this->bypassAuth();
        }

        return SSO::requestAuthorization();
    }

    private function bypassAuth()
    {
        $user = User::where('email', env('SUPPORT'))->first();

        auth()->login($user);
        $user->repo->store([], $user->id);
        return redirect()->intended('/dashboard');
    }

    /*
     * Show the login error page in case of an error
     */
    public function loginError()
    {
        return view('login.sso_login', ['title' => 'Sign in']);
    }

    /*
     * Receive the response from SSO and complete the authorization
     */
    public function ssoCompleteAuth(Request $request)
    {
        $ssoUser = SSO::user();

        $user = $this->userRepository->getUserByEmail($ssoUser->email);

        if (!$user) {
            Flash::error('No user exists in the system under the email used for login');

            return redirect('/login_error');
        }

        $loggedIn = serialize(Hash::make($user->username));

        $request->session()->flash('cytonn_username', $loggedIn);

        Flash::message('Please enter your CRIMS password to confirm login');

        return view('login.confirm_login', ['username' => $user->username, 'title' => 'Sign in']);
    }

    /*
     * Receive the CRIMS password and complete the login process
     */
    public function completeLogin(Request $request)
    {
        $username = $request->get('username');

        $password = $request->get('password');

        if ($this->request->session()->has('cytonn_username')) {
            $usernameHash = unserialize($this->request->session()->get('cytonn_username'));

            if (!Hash::check($username, $usernameHash)) {
                Flash::error('You are not logged in');

                return redirect('/sso/authorize');
            }

            $user = User::where('username', $username)->first();

            if (is_null($user)) {
                Flash::error('No user exists in the system under the username used for login');

                return redirect('/login_error');
            }

            if ($user->active == 0) {
                Flash::error('Your account is not active. Contact admin on how to activate it.');

                return redirect('/login_error');
            }

            if ($user->expired()) {
                Flash::error('Your account expired, you can no longer log in');

                return redirect('/login_error');
            }

            if ($user->login_attempt >= 5) {
                $login_attempt = (new Carbon($user->last_login_attempt));

                $now = Carbon::now();

                $minutes_elapsed = $login_attempt->copy()->diffInMinutes($now);

                if ($minutes_elapsed <= 30) {
                    Flash::warning(
                        'Too many incorrect login attempts, please wait for 30 minutes before you can login'
                    );

                    return redirect('/login_error');
                }
            }

            if (Auth::validate(['username' => $username, 'password' => $password])) {
                Auth::login($user);

                $user->authRepository->addLastLogin();

                $user->authRepository->resetLoginAttempts();

                $user->raise(new \Cytonn\Authentication\Events\UserHasBeenAuthenticated($user));

                $this->dispatchEventsFor($user);

                $scope = app(FundManagerScope::class);

                $fundManager = Session::has('selected_fund_manager_id') ? Session::get('selected_fund_manager_id')
                    : $scope::DEFAULT_FUND_MANAGER;

                $scope->setSelectedFundManager($fundManager);

                Flash::success('You are now logged in');

                return redirect()->intended('/');
            }

            $user->authRepository->addLoginAttempt();

            $user->authRepository->addInvalidLoginAttempt();

            Flash::error('Username and/or password does not match');

            $loggedIn = serialize(Hash::make($user->username));

            $request->session()->flash('cytonn_username', $loggedIn);

            return view('login.confirm_login', ['username' => $user->username, 'title' => 'Sign in']);
        } else {
            Flash::error('You are not logged in');

            return redirect('/sso/authorize');
        }
    }

    /**
     * Login user using form input, send OTP key to user to confirm authentication
     *
     * @param  Request $request
     * @return Response
     * @throws CRIMSGeneralException
     * @throws \Laracasts\Validation\FormValidationException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     * @throws \PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException
     */
    public function store(Request $request)
    {
        $username = $request->get('username');

        $password = $request->get('password');

        $this->loginForm->validate($request->all());

        if (\App::environment('testing')) {
            return $this->testingLoginHook();
        }

        //check if account is active
        $user = User::where('username', $username)->first();

        //check if username exists, avoid errors due to properties of undefined user object.
        if (is_null($user)) {
            Flash::error('Username and/or password does not match');

            return Redirect::back();
        }

        //check whether user account is active
        if ($user->active == 0) {
            Flash::error('Your account is not active. Contact admin on how to activate it.');

            return Redirect::back();
        }

        //check login attempts
        if ($user->login_attempt >= 5) {
            //check last login
            $login_attempt = (new Carbon($user->last_login_attempt));

            $now = Carbon::now();

            $minutes_elapsed = $login_attempt->copy()->diffInMinutes($now);

            if ($minutes_elapsed <= 30) {
                Flash::warning('Too many incorrect login attempts, please wait for 30 minutes before you can login');

                return Redirect::back();
            }
        }

        if (Auth::validate(['username' => $username, 'password' => $password])) {
            if ($user->expired()) {
                throw new CRIMSGeneralException('Your account expired, you can no longer log in');
            }

            //generate and save otp
            $otp = $this->getOTP($user);

            $user->raise(new UserHasLoggedIn($user, $otp));

            $this->dispatchEventsFor($user);

            //update last login time
            $user->authRepository->addLoginAttempt();

            //reset login attempts
            $user->authRepository->resetLoginAttempts();

            //Encrypt username and flash it to session

            $loggedIn = serialize(Hash::make($username));


            $this->request->session()->put('cytonn_username', $loggedIn);
            $this->request->session()->save();

            Flash::message('Enter the 6 digit pin you received in your email to confirm login');

            return view('login.confirm', ['username' => $user->username, 'title' => 'Sign in']);
        }

        //update last login time
        $user->authRepository->addLoginAttempt();

        //increment login attempts
        $user->authRepository->addInvalidLoginAttempt();

        Flash::error('Username and/or password does not match');

        return Redirect::back();
    }


    /**
     * Confirm a user logging in by authenticating their otp
     *
     * @param Request $request
     * @param  $username
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function confirm(Request $request, $username)
    {
        $this->OTPForm->validate($request->all());

        if ($this->request->session()->has('cytonn_username')) {
            $usernameHash = unserialize($this->request->session()->get('cytonn_username'));

            //confirm session variable matches the route posted to
            if (!Hash::check($username, $usernameHash)) {//user could not be confirmed as logged in
                Flash::error('You are not logged in');

                return redirect('/login');
            }

            //confirm the otp
            $user = User::where('username', $username)->first();
            $key = $request->get('one_time_key');

            $g2fa = new Google2FA();

            if (!$g2fa->verifyKey($user->google2fa_secret, $key, $this->window)) {
                Flash::error('Invalid login token. Please enter the correct token you received in your email');

                return redirect('/login');
            }

            $minutes_elapsed = (new Carbon($user->one_time_key_created_at))->copy()
                ->diffInMinutes(new Carbon('now'));

            if ($minutes_elapsed > $this->window) {
                Flash::error('Login Token expired. Login again to receive a new token');

                return redirect('/login');
            }

            if ($user->expired()) {
                throw new ClientInvestmentException('Your account expired, you can no longer log in');
            }

            //now log in user using username and key
            Auth::login($user);

            //update last login
            $user->authRepository->addLastLogin();

            $user->authRepository->resetLoginAttempts();

            //events
            $user->raise(new \Cytonn\Authentication\Events\UserHasBeenAuthenticated($user));
            $this->dispatchEventsFor($user);

            Flash::success('You are now logged in');

            return \redirect()->intended('/');
        } else {
            Flash::error('You are not logged in');

            return redirect('/login');
        }
    }

    /**
     * Log out a user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        if (Auth::guest()) {
            Flash::error('You are not logged in');

            return Redirect::back();
        }

        Auth::logout();

        Flash::message('You have now been Logged out');

        return Redirect::back();
    }

    /*
     * Receive a logout request fro SSO and logout user
     */
    public function ssoLogout($email)
    {
        $user = $this->userRepository->getUserByEmail($email);

        SSO::destroySession($user->last_sessid);

        $user->last_sessid = null;

        $user->save();
    }


    /**
     * Generate a one time password for a user, save the key used to generate the password
     *
     * @param  $user
     * @return string
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     * @throws \PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException
     */
    protected function getOTP($user)
    {
        $g2fa = new Google2FA();

        if (!$user->google2fa_secret) {
            $user->google2fa_secret = $g2fa->generateSecretKey();
        }

        $otp = $g2fa->getCurrentOtp($user->google2fa_secret);

        $user->one_time_key_created_at = new Carbon('now');

        $user->save();

        return $otp;
    }

    /**
     * Create a separate login hook for testing environments
     *
     * @return mixed
     */
    private function testingLoginHook()
    {
        $request = app(Request::class);

        if (app('APP_ENV') == 'testing') {
            $creds = $request->only('username', 'password');

            if (!Auth::attempt($creds)) {
                \Flash::message('We were unable to sign you in. Please check your credentials and try again!');

                return \redirect()->back()->withInput();
            } else {
                \Flash::message('Welcome back!');

                return \redirect()->intended('/');
            }
        }

        \abort(404);
    }
}
