<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSaleOrderConfirmation;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Mailers\Shares\SharesSaleOrderConfirmationMailer;
use Cytonn\Shares\Reporting\SharesSaleOrderConfirmationGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Class SharesSaleOrderConfirmationsController
 */
class SharesSaleOrderConfirmationsController extends Controller
{

    /**
     * Used to check for authorization
     *
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;


    /**
     * SharesSaleOrderConfirmationsController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        parent::__construct();
    }

    /**
     * Show the shares sale order confirmations' grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        // TODO
    }

    /**
     * Show the details of the sale order confirmation
     *
     * @param  $sale_order_id
     * @return mixed
     */
    public function getShow($sale_order_id)
    {
        $sale_order = SharesSalesOrder::findOrFail($sale_order_id);
        $previousConfirmations =
            SharesSaleOrderConfirmation::where('share_sale_order_id', $sale_order->id)
            ->latest()
            ->get();
        $is_soc_sent = (bool)SharesSaleOrderConfirmation::where('share_sale_order_id', $sale_order->id)
            ->count();
        return view(
            'shares.sales.confirmations.show',
            [
                'title' => 'Shares Sales Order Confirmation',
                'sale_order' => $sale_order,
                'seller' => $sale_order->seller,
                'previousConfirmations' => $previousConfirmations,
                'is_soc_sent' => $is_soc_sent
            ]
        );
    }

    /**
     * @param $sale_order_id
     * @return mixed
     */
    public function postSend($sale_order_id)
    {
        $sale_order = SharesSalesOrder::findOrFail($sale_order_id);
        $sender = Auth::user();
        $number = $sale_order->matchedPurchaseOrders()->get()->sum(
            function (SharesPurchaseOrder $order) {
                return $order->pivot->number;
            }
        );

        $successful = $number > 0;

        $confirmation = (new SharesSaleOrderConfirmationGenerator())->generate($sale_order, $sender, $successful)
            ->stream();

        (new SharesSaleOrderConfirmationMailer())->sendEmail($sale_order, $confirmation);

        $payload = [
            'number' => $number,
            'sender' => $sender,
            'share_sale_order_id' => $sale_order->id,
            'date' => $sale_order->request_date
        ];
        $data = ['share_sale_order_id' => $sale_order->id, 'successful' => $successful, 'payload' => $payload];

        SharesSaleOrderConfirmation::add($data);

        \Flash::success('The Shares Sale Order Confirmation has been sent');
        return Redirect::back();
    }

    /**
     * @param $sale_order_id
     * @return mixed
     */
    public function getPreview($sale_order_id)
    {
        $sale_order = SharesSalesOrder::findOrFail($sale_order_id);
        $sender = Auth::user();
        $number = $sale_order->matchedPurchaseOrders()->get()->sum(
            function (SharesPurchaseOrder $order) {
                return $order->pivot->number;
            }
        );

        $successful = $number > 0;

        $confirmation = new SharesSaleOrderConfirmationGenerator();

        return $confirmation->generate($sale_order, $sender, $successful)->stream();
    }

    /**
     * @param $confirmation_id
     * @return mixed
     */
    public function getPreviousPreview($confirmation_id)
    {
        $previousConfirmation = SharesSaleOrderConfirmation::findOrFail($confirmation_id);
        $sale_order = $previousConfirmation->saleOrder;
        $sender = Auth::user();

        $confirmation = new SharesSaleOrderConfirmationGenerator();

        return $confirmation->generate($sale_order, $sender)->stream();
    }
}
