<?php

namespace App\Http\Controllers;

use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Clients\Approvals\Events\ApprovalIsReady;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException as Error;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalBatch;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalRejection;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStage;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStep;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalType;
use Cytonn\Notifier\FlashNotifier as Flash;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Notifier\NotificationRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Commander\Events\DispatchableTrait;
use \Cytonn\Investment\Forms\BusinessConfirmationForm;
use ReflectionException;

/**
 * Class ApprovalsController
 */
class ApprovalsController extends Controller
{
    use DispatchableTrait;

    /**
     * @var BusinessConfirmationForm
     */
    public $confirmationForm;

    private $nextAction  = null;


    /**
     * ApprovalsController constructor
     *
     * @param BusinessConfirmationForm $confirmationForm
     */
    public function __construct(BusinessConfirmationForm $confirmationForm)
    {
        parent::__construct();
        $this->confirmationForm = $confirmationForm;
    }

    /**
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function index()
    {
        $transaction_types = ClientTransactionApproval::notApproved()->distinct()->get()->lists('transaction_type');
        $stages = ClientTransactionApprovalStage::all()->lists('name', 'id');

        foreach ($transaction_types as $key => $type) {
            $transaction_types[$type] = ucfirst(str_replace('_', ' ', $type));
            unset($transaction_types[$key]);
        }

        return view('clients.approvals.approvals', [
            'title' => 'Approve Client Transactions',
            'transaction_types' => $transaction_types,
            'stages' => $stages
        ]);
    }

    public function batches()
    {
        $stages = ClientTransactionApprovalStage::where('can_be_batched', true)->get();

        $batches = ClientTransactionApprovalBatch::all();

        $b = [];

        foreach ($stages as $stage) {
            $b[] = $batches->map(function ($batch) use ($stage) {
                    $types = $batch->types->lists('slug');

                    $count = ClientTransactionApproval::notApproved()
                        ->where('awaiting_stage_id', $stage->id)
                        ->whereIn('transaction_type', $types)
                        ->notRejected()
                        ->count();

                    return (object)[
                        'batch' => $batch,
                        'stage' => $stage,
                        'transaction_count' => $count
                    ];
            });
        }

        $batches = collect($b)->flatten()->all();

        return view('clients.approvals.batches', [
            'title' => 'Batch Transaction Approvals',
            'batches' => $batches
        ]);
    }

    public function batch($stageId, $batchId)
    {
        $batch = ClientTransactionApprovalBatch::findOrFail($batchId);
        $stage = ClientTransactionApprovalStage::findOrFail($stageId);

        $types = $batch->types->lists('slug');

        $transactions = ClientTransactionApproval::notApproved()
            ->where('awaiting_stage_id', $stage->id)
            ->whereIn('transaction_type', $types)
            ->notRejected()
            ->take(20)
            ->get();

        $columns = $batch->columns()->orderBy('weight', 'ASC')->get();

        return view('clients.approvals.batch', ['title' => 'Batch Approvals: '.$batch->name,
                'batch' => $batch,
                'stage'=> $stage,
                'transactions' => $transactions,
                'columns'=>$columns
            ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function show($id)
    {
        $approval = ClientTransactionApproval::findOrFail($id);
        $stage = (new Approval($approval))->nextStage();

        $checkApprovedStep = function ($approval, $stage) {
            return ClientTransactionApprovalStep::where('approval_id', $approval->id)
                ->where('stage_id', $stage->id)
                ->first();
        };

        $rejections = $approval->rejections()->whereNull('resolved_by')->get();

        try {
            $viewData = $approval->handler()->prepareView($approval);
        } catch (ReflectionException $e) {
            throw new \Cytonn\Exceptions\ClientInvestmentException('Transaction not supported!');
        }

        if (!isset($viewData['data'])) {
            $viewData['data'] = $approval->payload;
        }

        $viewFolder = 'clients.approvals.transactions'; //folder containing the view for this transaction

        return view(
            'clients.approvals.main',
            $viewData + ['title'=>'Approve transaction', 'approval'=>$approval, 'view_folder'=>$viewFolder,
                'approved_step' => $checkApprovedStep, 'stage' => $stage, 'rejections' => $rejections]
        );
    }


    /**
     * @param $id
     * @param $stageId
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function approve($id, $stageId)
    {
        $approval = ClientTransactionApproval::findOrFail($id);
        $stage = ClientTransactionApprovalStage::findOrFail($stageId);


        $this->validateApproval($approval, $stage);

        \Event::listen(
            'Cytonn.Clients.Approvals.Events.ApprovalSuccessful',
            function (ApprovalSuccessful $event) use ($approval) {
                $this->nextAction =  $event->next;
            }
        );

        $approval->raise(new ApprovalIsReady($approval));

        $this->dispatchEventsFor($approval);

        if ($this->nextAction) {
            return $this->index();

            //Disable redirects
            return $this->nextAction;
        } else {
            Flash::error('Transaction type is not supported');

            return Redirect::back();
        }
    }

    public function action($id, $action, $param = null)
    {
        $approval = ClientTransactionApproval::find($id);

        try {
            $className = ucfirst(camel_case($approval->transaction_type));

            $namespace = 'Cytonn\Clients\Approvals\Handlers';

            $handler = App::make($namespace.'\\'.$className);

            return $handler->action($approval, $action, $param);
        } catch (ReflectionException $e) {
            return \abort(404);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     */
    public function postAction(\Illuminate\Http\Request $request, $id)
    {
        $approval = ClientTransactionApproval::find($id);

        $action = $request->get('action_type');

        $input = $request->except(['_token', 'action_type']);

        try {
            $className = ucfirst(camel_case($approval->transaction_type));

            $namespace = 'Cytonn\Clients\Approvals\Handlers';

            $handler = App::make($namespace.'\\'.$className);

            return $handler->action($approval, $action, $input);
        } catch (ReflectionException $e) {
            return \abort(404);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $approvalId
     * @param $stageId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function reject(\Illuminate\Http\Request $request, $approvalId, $stageId)
    {
        $approval = ClientTransactionApproval::findOrFail($approvalId);
        $stage = ClientTransactionApprovalStage::find($stageId);

        (new Approval($approval))->checkAccess($stage);

        if ($approval->approved) {
            throw new ClientInvestmentException("Transaction is already approved");
        }

        if (in_array($stage->id, $approval->steps->lists('stage_id'))) {
            throw new ClientInvestmentException("Transaction is already approved in this stage");
        }

        if ($approval->rejections()->whereNull('resolved_by')->exists()) {
            throw new Error('Transaction was rejected, you cannot reject it again');
        }

        \DB::transaction(
            function () use ($request, $approval, $stage) {
                ClientTransactionApprovalRejection::create(
                    [
                    'approval_id' => $approval->id,
                    'stage_id' => $stage->id,
                    'reject_by' => \Auth::user()->id,
                    'reason' => $request->get('reason')
                    ]
                );

                (new NotificationRepository())->createNotificationWhenClientTransactionRejected($approval);
            }
        );

        if ($request->wantsJson()) {
            return response()->json(['status' => 'success', 'message' => 'Transaction successfully rejected']);
        }

        \Flash::success('Transaction successfully rejected');

        return $this->index();
    }

    public function resolve($approvalId, $rejectionId)
    {
        $approval = ClientTransactionApproval::findOrFail($approvalId);

        $rejection = $approval->rejections()->where('id', $rejectionId)->first();

        if (!$approval->approved) {
            $rejection->update(['resolved_by'=> \Auth::user()->id]);

            (new NotificationRepository())->setClientApprovalNotificationPermanentlyReadOnResolve($approval);
        }

        \Flash::message('Issue has been marked as resolved, approval can now proceed');

        return \redirect()->back();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Exception
     */
    public function delete($id)
    {
        if (! (strtolower(\Request::get('confirm')) == 'delete')) {
            \Flash::error('Invalid input given. The request has not been removed.');
            return \Redirect::back();
        }

        $reason = \Request::get('reason');
        if (! (strtolower($reason))) {
            \Flash::error('You must provide a reason for cancelling the approval request.');
            return \Redirect::back();
        }

        $approval = ClientTransactionApproval::findOrFail($id);

        if ($approval->approved) {
            \Flash::error('You cannot remove an approval that is already approved');
            return \Redirect::back();
        }
        $approval->reason = $reason;
        $approval->save();

        $approval->delete();

        \Flash::message('Approval request has been removed');

        return $this->index();

        return redirect('/dashboard/investments/approve');
    }

    private function validateApproval(ClientTransactionApproval $approval, ClientTransactionApprovalStage $stage)
    {
        if (!(\App::environment('testing') || \App::environment('local'))) {
            if ($approval->sent_by == \Auth::user()->id) {
                throw new Error('You cannot approve a transaction that you originated.');
            }

            if (in_array(\Auth::user()->id, $approval->steps->lists('user_id'))) {
                throw new Error(
                    "You have already approved this transaction in a previous 
                    stage, you cannot approve it again"
                );
            }
        }

        if ($approval->approved) {
            throw new Error('This transaction has already been approved');
        }

        if ($approval->steps()->where('stage_id', $stage->id)->exists()) {
            throw new Error('This transaction is already approved in this stage');
        }

        if ($approval->rejections()->whereNull('resolved_by')->exists()) {
            throw new Error('Transaction was rejected, please resolve the issues before proceeding');
        }
    }
}
