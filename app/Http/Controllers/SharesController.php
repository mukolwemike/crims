<?php

namespace App\Http\Controllers;

use App\Cytonn\Dashboard\SharesDashboardGenerator;
use App\Cytonn\Models\FundManager;

class SharesController extends Controller
{
    public function menu()
    {
        $gen = new SharesDashboardGenerator();

        $manager = $this->fundManager();

        return \view('shares.menu', [
            'title'=>'Shares Dashboard',
            'gen' => $gen,
            'manager' => $manager
        ]);
    }
}
