<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\ClientUserAccess;
use App\Cytonn\Models\CommissionRecepient;
use App\Jobs\System\ClientUserAccessExport;
use Cytonn\Authentication\Authy;
use Cytonn\Authorization\Authorizer;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Users\Forms\UserClientForm;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

/**
 * Date: 15/01/2016
 * Time: 3:04 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

/**
 * Class ClientUserController
 */
class ClientUserController extends Controller
{
    /**
     * @var UserClientForm
     */
    public $userClientForm;

    /**
     * ClientUserController constructor.
     *
     * @param $userClientForm
     */
    public function __construct(UserClientForm $userClientForm)
    {
        $this->userClientForm = $userClientForm;
        $this->authorizer = new Authorizer();
        $this->authorizer->checkAuthority('manageclientusers');

        parent::__construct();
    }

    /**
     * Show a list of the user accounts
     *
     * @return mixed
     */
    public function index()
    {
        return view('users.client.index', [
            'title' => 'Client\'s user accounts'
        ]);
    }

    /**
     * Show form to create a new user
     *
     * @param null $id
     * @return mixed
     */
    public function create($id = null)
    {
        $user = is_null($id) ? new ClientUser() : ClientUser::findOrFail($id);

        return view('users.client.create', [
            'title' => 'Create a new user for a client',
            'user' => $user
        ]);
    }

    /**
     * @param null $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function store($id = null)
    {
        if (is_null($id)) {
            $this->userClientForm->newUser();
            $this->userClientForm->validate(Request::all());

            $user = new ClientUser();
            $user->fill(Request::all());
            $user->username = trim(Request::get('username'));
            $user->save();

            $user->repo->provisionUser();

            \Flash::message('User created successfully. An email will be sent when you approve at least one account');

            return \redirect('/dashboard/users/clients');
        } else {
            $this->userClientForm->validate(Request::all());
            $user = ClientUser::findOrFail($id);
            $user->fill(Request::all());
            $user->username = trim(Request::get('username'));
            $user->save();

            $user->repo->updateUser();

            \Flash::message('User details have been saved successfully');
        }

        if (is_null($user->authy_id)) {
            \Flash::error('The user was not registered in Authy, please correct the phone number and country code');
        }

        return \Redirect::action('ClientUserController@show', ['id' => $user->id]);
    }

    public function show($id)
    {
        $user = ClientUser::findOrFail($id);

        $access = ClientUserAccess::where('user_id', $id)->has('client')->where('active', 1);

        $client_ids = $access->get(['client_id']);

        $authy_status = (new Authy())->status($user->authy_id)->success;

        $fas = $user->financialAdvisors()->paginate(10);

        $signatures = ClientSignature::whereIn('client_id', $client_ids)->get();

        $signature_options = [];

        foreach ($signatures as $sign_option) {
            $signature_options[$sign_option->id]
                = $sign_option->present()->getName . ' - ' .
                $sign_option->present()->getEmail . ' for ' .
                $sign_option->client->name();
        }

        return view('users.client.details', [
            'signature_options' => $signature_options,
            'authy_status' => $authy_status,
            'title' => 'User Details',
            'access' => $access->get(),
            'user' => $user,
            'fas' => $fas
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function reactivate($id)
    {
        $user = ClientUser::findOrFail($id);

        $user->repo->reactivate();

        \Flash::success('User activated successfully');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deactivate($id)
    {
        $user = ClientUser::findOrFail($id);

        $user->repo->deactivate();

        \Flash::success('User deactivated successfully');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function resendLink($id)
    {
        $id = Crypt::decrypt($id);

        $user = ClientUser::findOrFail($id);

        $user->repo->resendActivationKey();

        \Flash::success('Activation key succesfully sent');

        return Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function resetPassword($id)
    {
        $user = ClientUser::findOrFail(Crypt::decrypt($id));

        $user->repo->resetPassword();

        \Flash::success('A password reset link has been sent to the user');

        return Redirect::back();
    }

    /**
     * @param $userID
     * @return mixed
     * @throws ClientInvestmentException;
     */
    public function assignAccess($userID)
    {
        $user = ClientUser::findOrFail($userID);

        $client = Client::findOrFail(Request::get('client_id'));

        if (ClientUserAccess::where('client_id', $client->id)->where('user_id', $userID)
            ->where('active', 1)->first()) {
            throw new ClientInvestmentException('The user already has access. No need to reassign access');
        }

        ClientTransactionApproval::add([
            'client_id' => $client->id,
            'transaction_type' => 'assign_user_account',
            'scheduled' => false,
            'payload' => ['user_id' => $user->id]]);

        \Flash::message('User account has been added for approval');

        return Redirect::back();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function assignFa($userId)
    {
        $user = ClientUser::findOrFail($userId);

        $fa = CommissionRecepient::findOrFail(\Request::get('fa_id'));

        $user->financialAdvisors()->save($fa);

        \Flash::success('Access has been granted to ' . $fa->name);

        return \Redirect::back();
    }

    /**
     * @param $accessId
     * @return mixed
     */
    public function revokeAccess($accessId)
    {
        $access = ClientUserAccess::findOrFail($accessId);

        ClientTransactionApproval::add(
            ['client_id' => $access->client_id, 'transaction_type' => 'revoke_user_account',
                'scheduled' => false, 'payload' => ['access_id' => $accessId]]
        );

        \Flash::message('Access will be revoked when transaction is approved');

        return Redirect::back();
    }


    /**
     * @param $faId
     * @param $userId
     * @return mixed
     */
    public function revokeFa($faId, $userId)
    {
        $user = ClientUser::findOrFail($userId);
        $fa = CommissionRecepient::findOrFail($faId);

        $user->financialAdvisors()->detach($fa->id);

        \Flash::error('Access to ' . $fa->name . ' has been revoked');

        return Redirect::back();
    }

    public function assignSignature($userId)
    {
        $input = request()->except('_token');

        $clientSignature = ClientSignature::findOrFail($input['client_signature_id']);

        ClientTransactionApproval::add([
            'client_id' => $clientSignature->client_id,
            'transaction_type' => 'assign_user_client_signature',
            'scheduled' => false,
            'payload' => $input
        ]);

        \Flash::message('Client signature assignment has been saved for approval');

        return Redirect::back();
    }

    public function removeSignature($user_id, $signature_id)
    {
        $user = ClientUser::findOrFail($user_id);

        $signature = ClientSignature::findOrFail($signature_id);

        $input['client_user_id'] = $user->id;

        $input['client_signature_id'] = $signature->id;

        ClientTransactionApproval::add([
            'client_id' => $signature->client_id,
            'transaction_type' => 'remove_user_client_signature',
            'scheduled' => false,
            'payload' => $input
        ]);

        \Flash::message('Client signature removal has been saved for approval');

        return Redirect::back();
    }


    public function exportUserAccounts()
    {
        $input = Request::all();

        if (!isset($input['start'])) {
            \Flash::error('Please provide a start date filter.');

            return \Redirect::back();
        }

        dispatch((new ClientUserAccessExport(
            $input['start'],
            $input['end'],
            $input['active'],
            $input['account_type']
        )));

        \Flash::success('The client user report will be sent shortly via email.');

        return \Redirect::back();
    }
}
