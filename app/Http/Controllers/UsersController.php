<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Title;
use App\Cytonn\Models\User;
use App\Events\Users\UserDeactivated;
use App\Events\Users\UserReactivated;
use App\Events\Users\UserRegisteredNotification;
use Cytonn\Authentication\Forms\RegisterUserForm;
use Cytonn\Authorization\Authorizer;
use Cytonn\Authorization\Permission;
use Cytonn\Authorization\Role;
use Cytonn\Users\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Commander\Events\DispatchableTrait;
use SSOManager\Facades\SSO;

/**
 * Class UsersController
 */
class UsersController extends Controller
{
    use DispatchableTrait;

    /**
     * @var RegisterUserForm
     */
    protected $registerForm;

    /*
     * Get the required repositories
     */
    protected $userRepository;

    /**
     * UsersController constructor.
     *
     * @param RegisterUserForm $registerForm
     * @param UserRepository $userRepository
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function __construct(
        RegisterUserForm $registerForm,
        UserRepository $userRepository
    ) {
        parent::__construct();

        $this->registerForm = $registerForm;
        $this->authorizer = new Authorizer();
        $this->authorizer->checkAuthority('manageusers');
        $this->userRepository = $userRepository;
    }


    /**
     * Display the users grid table
     *
     * @return Response
     */
    public function index()
    {
        return view('users.index', ['title' => 'Users']);
    }

    /**
     * Show the user details
     *
     * @param  $id
     * @return mixed
     */
    public function details($id)
    {
        $user = User::with('contact')->findOrFail($id);

        if (is_null($user->password)) {
            $status = 'not-activated';
        } else {
            $user->active == 1 ? $status = 'active' : $status = 'deactivated';
        }

        $roles = Role::all()->lists('description', 'id');
        $permissions = Permission::all()->lists('description', 'id');

        return view(
            'users.show',
            [
                'title' => 'User Details', 'user' => $user, 'status' => $status,
                'roles' => $roles, 'permissions' => $permissions
            ]
        );
    }

    /**
     * Get the permissions in a grid table
     *
     * @return mixed
     */
    public function permissions()
    {
        return view('users.permissions', ['title' => 'System Permissions']);
    }

    /**
     * Show details about a permission
     *
     * @param  $id
     * @return mixed
     */
    public function permissionDetail($id)
    {
        $permission = \Cytonn\Authorization\Permission::findOrFail($id);

        return view('users.permissiondetail', ['title' => 'Permission Detail', 'permission' => $permission]);
    }

    /**
     * Show the roles in a grid table
     *
     * @return mixed
     */
    public function roles()
    {
        return view('users.roles', ['title' => 'User Roles']);
    }

    /**
     * Show the details of a role
     *
     * @param  $id
     * @return mixed
     */
    public function roleDetail($id)
    {
        $role = \Cytonn\Authorization\Role::findOrFail($id);

        //TODO remove old permissions and the where clause

        $groups = \Cytonn\Authorization\Permission::where('id', '>=', 47)
            ->orderBy('name', 'ASC')->get()
            ->groupBy(
                function ($perm) {
                    return explode(':', $perm->name)[0];
                }
            );

        $assigned = $role->permissions()->get()->lists('id');

        return view(
            'users.roledetail',
            [
                'title' => 'Role Details',
                'role' => $role, 'groups' => $groups,
                'assigned' => $assigned
            ]
        );
    }

    /**
     * Show form for creating a new user
     */
    public function create($id = null)
    {
        $user = User::findOrNew($id);
        $titles = Title::all()->lists('name', 'id');

        return view('users.create', ['title' => 'Add a new user', 'user' => $user, 'titles' => $titles]);
    }

    /**
     * Save a user
     *
     * @param Request $request
     * @param null $id
     * @return mixed
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function store(Request $request, $id = null)
    {
        $input = $request->only(['title_id', 'firstname', 'middlename', 'lastname', 'username', 'email', 'jobtitle']);

        if ($id) {
            $this->registerForm->existingAcc();

            $this->registerForm->validate($request->all());

            $user = $this->userRepository->store($input, $id);

            SSO::grantAccess($user->email);

            \Flash::success('User successfully registered, an email has been sent to user to activate account');
        } else {
            $this->registerForm->newAcc();

            $this->registerForm->validate($request->all());

            $user = $this->userRepository->store($input, $id);

            $user->raise(new \Cytonn\Authentication\Events\UserRegistered($user));

            $this->dispatchEventsFor($user);

            event(new UserRegisteredNotification($user));

            SSO::grantAccess($user->email);

            \Flash::success('User details succesfully saved');
        }

        return redirect('/dashboard/users');
    }

    /**
     * Deactivate a user account
     *
     * @param  $id
     * @return mixed
     */
    public function deactivate($id)
    {
        $id = Crypt::decrypt($id);

        $user = User::findOrFail($id);

        if ($user->active == 0) {
            \Flash::message('User account is not active');

            return Redirect::back();
        }

        $user->repo->deactivate();

        event(new UserDeactivated($user));

        SSO::revokeAccess($user->email);

        \Flash::success('User deactivated successfully');

        return Redirect::back();
    }

    /**
     * Activate a deactivated user account
     *
     * @param  $id
     * @return mixed
     */
    public function reactivate($id)
    {
        $id = Crypt::decrypt($id);

        $user = User::findOrFail($id);

        if ($user->active) {
            \Flash::message('User is already active');

            return Redirect::back();
        }

        $user->repo->reactivate();

        event(new UserReactivated($user));

        SSO::grantAccess($user->email);

        \Flash::success('User Reactivated successfully');

        return Redirect::back();
    }

    /**
     * Resend the link to activate an account
     *
     * @param  $id
     * @return mixed
     */
    public function resendLink($id)
    {
        $id = Crypt::decrypt($id);

        $user = User::findOrFail($id);

        if ($user->active || !is_null($user->password)) {
            \Flash::message('User has already activated');

            return Redirect::back();
        }

        $user->authRepository->resendActivationLink();

        \Flash::message('Activation link has been sent to user');

        return Redirect::back();
    }
}
