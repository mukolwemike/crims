<?php

namespace App\Http\Controllers;

/**
 * Date: 02/10/2015
 * Time: 5:05 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


class SetupController extends Controller
{
    public function index()
    {
        return view('setup.menu', ['title'=>'System setup']);
    }
}
