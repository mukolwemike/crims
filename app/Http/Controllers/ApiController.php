<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Notifier\NotificationRepository;
use Cytonn\Portfolio\MaturityRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Cytonn\Models\Company;
use Carbon\Carbon;

/**
 * Class ApiController
 * All Api End points are handled in this file
 */
class ApiController extends Controller
{


    /**
     * Returns all companies as json
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCompanies()
    {
        return Company::all();
    }


    /**
     * Gets all the users, eager loads contact information
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getUsers()
    {
        return User::with('contact')->get();
    }

    /**
     * Gets all authorization permission
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPermissions()
    {
        return \Cytonn\Authorization\Permission::all();
    }

    /**
     * Gets all the roles
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRoles()
    {
        return \Cytonn\Authorization\Role::all();
    }

    /**
     * Gets all portfolio insitutions
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPortfolioInstitutions()
    {
        return PortfolioInvestor::all();
    }


    /**
     * Returns all products
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getProducts()
    {
        return Product::all();
    }



    /**
     * Gets the balance for a custodial account
     *
     * @param  $id
     * @return mixed
     */
    public function custodialBalance($id)
    {
        $account = CustodialAccount::findOrFail($id);

        return $account->balance();
    }



    public function loginCheck()
    {
        if (\Auth::check()) {
            if (\Auth::user()->expired()) {
                return 0;
            }

            return 1;
        }

        return 0;
    }

    public function getUnreadCount()
    {
        return (new NotificationRepository())->getUnreadNotificationCount(Auth::user());
    }

    public function getMaturityProfileData($currency_id)
    {
        $currency = Currency::findOrFail($currency_id);

        $start = Request::get('start');

        $end = Request::get('end');

        $p_weeklies = (new MaturityRepository())->getWeeklyMaturityAnalysisFor($start, $end, $currency);

        $p_amounts = $p_weeklies->lists('amount');
        $p_dates = $p_weeklies->lists('start');

        $p_in_millions = new Collection();
        foreach ($p_amounts as $amount) {
            $p_in_millions->push(number_format($amount/1000000, 2, '.', ''));
        }
        $p_amounts = $p_in_millions;


        $product = Product::where('currency_id', $currency->id)->first();

        if (!is_null($product)) {
            $c_weeklies = (new MaturityRepository())->getWeeklyMaturityAnalysisFor($start, $end, $product);

            $c_amounts = $c_weeklies->lists('amount');

            $in_millions = new Collection();

            foreach ($c_amounts as $amount) {
                $in_millions->push(number_format($amount/1000000, 2, '.', ''));
            }

            $c_amounts = $in_millions;
        } else {
            $c_amounts = 0;
            $c_dates = null;
        }

        //format dates
        $dates = new Collection();

        foreach ($p_dates as $date) {
            $dates->push($date->toFormattedDateString());
        }

        return [$p_amounts, $dates, $c_amounts];
    }

    public function getCommissionRatesForProduct($id)
    {
        $product = Product::find($id);

        !is_null($product) ? : $product = Product::where('id', '!=', null)->first();

        return CommissionRate::where('currency_id', $product->currency_id)->get();
    }

    /*
     * Get the commission rates based on the current product and recipient type
     */
    public function getCommissionRatesForProductAndRecipient($productId, $recipientId, $date = null)
    {
        $product = Product::find($productId);

        $recipient = CommissionRecepient::find($recipientId);

        return (new InvestmentsRepository())->getCommissionRatesForProductAndRecipient($product, $recipient, $date);
    }

    public function getClientUsers()
    {
        return ClientUser::all();
    }
}
