<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/4/18
 * Time: 12:18 PM
 */

namespace App\Http\Controllers\Portfolio\Deposits;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class LoanAmortizationController extends Controller
{
    public function show($id)
    {
        $holding = DepositHolding::findOrFail($id);

        return view('portfolio.securities.deposits.loan_amortization', [
            'title' => 'Loan Amortization',
            'holding' => $holding,
            'security' => $holding->security
        ]);
    }

    public function export($id)
    {
        $holding = DepositHolding::findOrFail($id);

        $this->queue(function () use ($holding) {
            \Artisan::call('portfolio:loan_amortization_schedule', [
                'holding_id' => $holding->id
            ]);
        });

        \Flash::message('Download successful!');

        return Redirect::back();
    }
}
