<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Http\Controllers\Portfolio;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\CustodialWithdrawType;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Portfolio\SuspenseTransactions\SuspenseTransactionRepository;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SuspenseTransactionController extends Controller
{
    
    protected $suspenseTransactionRepository;

    public function __construct(SuspenseTransactionRepository $suspenseTransactionRepository)
    {
        parent::__construct();

        $this->suspenseTransactionRepository = $suspenseTransactionRepository;
    }
    
    public function index($id)
    {
        $custodialAccount = CustodialAccount::findOrFail($id);

        return view('portfolio.suspense_transactions.index', [
            'title' => 'Custodial Suspense Transactions',
            'custodialAccount' => $custodialAccount,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($id)
    {
        $transaction = $this->suspenseTransactionRepository->getSuspenseTransactionById($id);

        $withdrawTypes = CustodialWithdrawType::pluck('name', 'id')->toArray();

        $custodialTransactionTypes = CustodialTransactionType::pluck('description', 'name')->toArray();

        $products = Product::where('active', 1)->pluck('name', 'id')->toArray();

        $shareentities = SharesEntity::pluck('name', 'id')->toArray();

        $projects = Project::pluck('name', 'id')->toArray();

        $funds = UnitFund::pluck('name', 'id')->toArray();

        return view('portfolio.suspense_transactions.details', [
            'title' => 'Suspense Transaction',
            'transaction' => $transaction,
            'withdrawTypes' => $withdrawTypes,
            'transactionTypes' => $custodialTransactionTypes,
            'products' => $products,
            'projects' => $projects,
            'funds' => $funds,
            'shareEntities' => $shareentities
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function recordFees(Request $request, $id)
    {
        $transaction = $this->suspenseTransactionRepository->getSuspenseTransactionById($id);

        if ($transaction->repo->hasApproval()) {
            \Flash::message('There is a existing transaction approval already linked to the suspense transaction');

            return back();
        }

        $input = $request->except(['_token']);

        $input['amount'] = abs($transaction->amount);

        $input['custodial_id'] = $transaction->custodial_account_id;

        $input['date'] = Carbon::parse($transaction->date)->toDateString();

        $approval = PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'custodial_payment',
            'payload' => $input
        ]);

        $transaction->approval()->associate($approval);

        $transaction->save();

        \Flash::message('Transaction saved for approval');

        return redirect('dashboard/portfolio/suspense-transactions/details/'. $id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function bounceCheque(Request $request, $id)
    {
        $transaction = $this->suspenseTransactionRepository->getSuspenseTransactionById($id);

        if ($transaction->repo->hasApproval()) {
            return \Illuminate\Support\Facades\Response::json([
                'errors' => 'There is a existing transaction approval already linked to the suspense transaction',
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
        }

        $input = $request->except(['_token']);

        $input['date'] = Carbon::parse($transaction->date)->toDateString();

        $approval = ClientTransactionApproval::make(
            $transaction->client_id,
            'add_cheque_as_bounced',
            $input,
            false,
            \Auth::id()
        );

        $transaction->clientApproval()->associate($approval);

        $transaction->save();

        return \Illuminate\Support\Facades\Response::json([
            'data' => ['message' => 'Transaction submitted successfully for approval'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function recordDeposits(Request $request, $id)
    {
        $transaction = $this->suspenseTransactionRepository->getSuspenseTransactionById($id);

        if ($transaction->repo->hasApproval()) {
            \Flash::message('There is a existing transaction approval already linked to the suspense transaction');

            return back();
        }

        $input = $request->except(['_token']);

        $input['amount'] = $transaction->amount;

        $input['custodial_id'] = $transaction->custodial_account_id;

        $input['date'] = Carbon::parse($transaction->date)->toDateString();

        $approval = PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'custodial_deposit',
            'payload' => $input
        ]);

        $transaction->approval()->associate($approval);

        $transaction->save();

        \Flash::message('Deposit Transaction saved for approval');

        return redirect('dashboard/portfolio/suspense-transactions/details/'. $id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function recordClientInflow(Request $request, $id)
    {
        $transaction = $this->suspenseTransactionRepository->getSuspenseTransactionById($id);

        if ($transaction->repo->hasApproval()) {
            return \Illuminate\Support\Facades\Response::json([
                'errors' => 'There is a existing transaction approval already linked to the suspense transaction',
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
        }

        $input = $request->except(['_token']);

        $input = $this->prepareInflowApprovalData($input, $transaction);

        $approval = ClientTransactionApproval::make(
            $input['client_id'],
            'add_cash_to_account',
            $input
        );

        $transaction->clientApproval()->associate($approval);

        $transaction->save();

        return \Illuminate\Support\Facades\Response::json([
            'data' => ['message' => 'Transaction submitted successfully for approval'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @return array
     */
    private function prepareInflowApprovalData($data, SuspenseTransaction $transaction)
    {
        $input = array();

        $input['id'] = null;

        $input['type'] = CustodialTransactionType::where('name', 'FI')->first()->id;

        $input['custodial_account_id'] = $transaction->custodial_account_id;

        $paymentToData = $this->getPaymentToData($data);

        $input = array_merge($input, $paymentToData);

        $input['exchange_rate'] = 1;

        $source = $this->getPaymentSource($transaction);

        $input['source'] = $source;

        if ($source == 'cheque') {
            $input['cheque_number'] = $this->getChequeNumber($transaction->transaction_id);
        }

        $input['amount'] = $transaction->amount;

        $input['date'] = Carbon::parse($transaction->date)->toDateString();

        $input = array_merge($input, $this->getClientInflowDetails($data, $transaction));

        $input['description'] = 'Client funds deposited - ' . $transaction->narration;

        $input = array_merge($input, $this->getSourceIdentityCodes($transaction));

        return $input;
    }

    /**
     * @param $data
     * @return array
     */
    private function getPaymentToData($data)
    {
        $category = $data['category'];

        if ($category == 'product') {
            $key = 'product_id';
        } elseif ($category == 'project') {
            $key = 'project_id';
        } elseif ($category == 'shares') {
            $key = 'entity_id';
        } elseif ($category == 'funds') {
            $key = 'unit_fund_id';
        }

        return [
            $key => $data['payment_to_id']
        ];
    }

    /**
     * @param SuspenseTransaction $transaction
     * @return string
     */
    private function getPaymentSource(SuspenseTransaction $transaction)
    {
        if ($transaction->source_identity == 'MPESA') {
            return 'mpesa';
        } elseif ($transaction->source_identity == 'CHEQUE') {
            return 'cheque';
        } else {
            return 'deposit';
        }
    }

    /**
     * @param $transactionId
     * @return mixed
     */
    private function getChequeNumber($transactionId)
    {
        $codeArray = explode('#', $transactionId);

        return $codeArray[count($codeArray) - 1];
    }

    /**
     * @return array
     */
    private function getClientInflowDetails($data, $transaction)
    {
        $input = array();

        if (isNotEmptyOrNull($data['client_id'])) {
            $input['client_id'] = $data['client_id'];

            $client = Client::findOrFail($data['client_id']);

            $input['received_from'] = ClientPresenter::presentFullNames($data['client_id']);
        } else {
            $input['client_id'] = null;

            $paylod = $transaction->payload;

            $client = null;

            $input['received_from'] = isset($paylod['payer_name']) ? $paylod['payer_name'] : null;
        }

        if (isNotEmptyOrNull($data['commission_recipient_id'])) {
            $input['commission_recipient_id'] = $data['commission_recipient_id'];
        } else {
            $fa = $client ? $client->getLatestFa() : null;

            $input['commission_recipient_id'] = $fa ? $fa->id : null;
        }

        return $input;
    }

    /**
     * @return array
     */
    private function getSourceIdentityCodes(SuspenseTransaction $transaction)
    {
        $codeArray = array();

        if ($transaction->source_identity == 'MPESA') {
            $codeArray['mpesa_confirmation_code'] = $transaction->transaction_id;

            $codeArray['bank_reference_no'] = $transaction->transaction_id;
        } else {
            $codeArray['bank_reference_no'] = $transaction->transaction_id;

            $codeArray['mpesa_confirmation_code'] = null;
        }

        return $codeArray;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function matchCustodialTransaction(Request $request, $id)
    {
        $transaction = $this->suspenseTransactionRepository->getSuspenseTransactionById($id);

        if ($transaction->repo->hasApproval()) {
            return \Illuminate\Support\Facades\Response::json([
                'errors' => 'There is a existing transaction approval already linked to the suspense transaction',
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
        }

        $input = $request->except(['_token']);

        $approval = PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'match_suspense_to_custodial',
            'payload' => $input
        ]);

        $transaction->approval()->associate($approval);

        $transaction->save();

        return \Illuminate\Support\Facades\Response::json([
            'data' => ['message' => 'Transaction submitted successfully for approval'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function matchSuspenseTransaction(Request $request, $id)
    {
        $transaction = $this->suspenseTransactionRepository->getSuspenseTransactionById($id);

        if ($transaction->repo->hasApproval()) {
            return \Illuminate\Support\Facades\Response::json([
                'errors' => 'There is a existing transaction approval already linked to the suspense transaction',
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
        }

        $input = $request->except(['_token']);

        $matchSuspenseTransaction =
            $this->suspenseTransactionRepository->getSuspenseTransactionById($input['suspense_transaction_id']);

        if ($matchSuspenseTransaction->repo->hasApproval()) {
            return \Illuminate\Support\Facades\Response::json([
                'errors' =>
                    'There is a existing transaction approval already linked to the selected suspense transaction',
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
        }

        if ($transaction->amount > 0) {
            $creditTransaction = $transaction;
            $debitTransaction = $matchSuspenseTransaction;
        } else {
            $creditTransaction = $matchSuspenseTransaction;
            $debitTransaction = $transaction;
        }

        $data = [
            'amount' => abs($debitTransaction->amount),
            'credit_amount' => abs($creditTransaction->amount),
            'transfer_date' => Carbon::parse($creditTransaction->date)->toDateString(),
            'effective_rate' => 1 / $creditTransaction->exchange_rate,
            'narrative' => $creditTransaction->narration,
            'destination_id' => $creditTransaction->custodial_account_id,
            'exchange' => $creditTransaction->exchange_rate,
            'reverse' => false,
            'source_account' => $debitTransaction->custodial_account_id
        ];

        $approval = PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'transfer',
            'payload' => $data
        ]);

        $transaction->approval()->associate($approval);

        $matchSuspenseTransaction->update([
            'approval_id' => $approval->id
        ]);

        $transaction->save();

        return \Illuminate\Support\Facades\Response::json([
            'data' => ['message' => 'Transaction submitted successfully for approval'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }
}
