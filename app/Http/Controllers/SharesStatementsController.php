<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesStatementCampaign;
use App\Cytonn\Models\StatementCampaignType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SharesStatementsController extends Controller
{

    /**
     * SharesStatementsController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        parent::__construct();
    }

    /**
     * Show the campaigns grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        $campaignTypes = StatementCampaignType::all()->lists('name', 'id');

        $campaigns = SharesStatementCampaign::latest()->paginate(10);

        return \view(
            'shares.statements.index',
            ['title' => 'Shares Statements', 'campaign_types' => $campaignTypes, 'campaigns' => $campaigns]
        );
    }

    /**
     * Create a campaign to be used to send statements
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function postCreateCampaign()
    {
        $campaign = new SharesStatementCampaign();

        $campaign->fill(\Request::only('name', 'send_date', 'type_id'));
        $campaign->sender_id = Auth::user()->id;

        $campaign->save();

        if ($campaign->type->slug == 'automated') {
            \Queue::push(
                'Cytonn\Reporting\SharesStatementCampaignRepository@fireAutomaticallyAddClients',
                ['campaign_id' => $campaign->id]
            );
        }

        \Flash::message('Shares campaign has been saved, sending can now start');

        return \Redirect::back();
    }


    /**
     * Show the campaign
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function getCampaigns($id)
    {
        $campaign = SharesStatementCampaign::findOrFail($id);

        $sent_count = $campaign->repo->countSentItems();
        $total_count = $campaign->repo->countAllItems();

        return \view(
            'shares.statements.show',
            [
                'campaign' => $campaign,
                'title' => 'Shares Statement Campaigns',
                'sent_count' => $sent_count,
                'total_count' => $total_count
            ]
        );
    }

    /**
     * Add a share holder to a campaign so that you can send statement
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function postAddShareHolderToCampaign()
    {
        $this->authorizer->checkAuthority('sendstatements');

        $campaign = SharesStatementCampaign::findOrFail(\Request::get('campaign'));

        $holder = ShareHolder::findOrFail(\Request::get('share_holder_id'));

        if ($campaign->repo->checkIfShareHolderIsInCampaign($holder)) {
            \Flash::warning('The shareholder has already been added to the campaign');

            return \Redirect::back();
        }

        $campaign->repo->addShareHolderToCampaign($holder, \Auth::user());

        \Flash::message('Shareholder has been added to campaign');

        return \Redirect::back();
    }

    /**
     * @param $campaignId
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function sendCampaign($campaignId)
    {
        $this->authorizer->checkAuthority('sendstatements');


        $campaign = SharesStatementCampaign::findOrFail($campaignId);

        if ($campaign->sender_id == \Auth::user()->id) {
            throw new \Cytonn\Exceptions\ClientInvestmentException(
                'The sender cannot be the person who created the campaign'
            );
        }

        $campaign->repo->sendCampaign();

        if ($campaign->type->slug == 'automated') {
            $campaign->close();
        }

        \Flash::message('The shares statements are being dispatched to the clients');

        return \Redirect::back();
    }

    /**
     * @param $campaignId
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function closeCampaign($campaignId)
    {
        $this->authorizer->checkAuthority('approvestatements');

        $campaign = SharesStatementCampaign::findOrFail($campaignId);

        if (!$campaign->closed) {
            $campaign->closed = true;
            $campaign->save();
        }

        \Flash::message('Campaign has been closed, shareholders can no longer be added');

        return \Redirect::back();
    }

    public function storeCampaignMessage(Request $request, $campaignId)
    {
        $campaign = SharesStatementCampaign::findOrFail($campaignId);

        $campaign->statement_message = $request->get('statement_message');

        $campaign->save();

        \Flash::message('Campaign has been saved successfully');

        return \Redirect::back();
    }
}
