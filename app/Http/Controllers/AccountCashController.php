<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Handlers\AddInflowFromTransaction;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Custodial\Validate\Inflow;
use Cytonn\Investment\Forms\AccountInflowForm;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Rules\Rules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

/**
 * Class AccountCashController
 */
class AccountCashController extends Controller
{

    use Rules;
    /**
     * @var $authorizer
     */
    protected $authorizer;

    /**
     * @var $form
     */
    protected $form;

    public function __construct(AccountInflowForm $form)
    {
        parent::__construct();
        $this->authorizer->checkAuthority('addinvestment');
        $this->form = $form;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        $products = Product::all();
        $projects = Project::all();
        $shareEntities = SharesEntity::all();
        $custodialaccount = CustodialAccount::all();

        return view(
            'investment.account_cash.index',
            [
            'title' => 'Accounts Inflow',
            'products' => $products,
            'projects' => $projects,
            'shareEntities' => $shareEntities,
            'custodialaccount' => $custodialaccount
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $transaction = CustodialTransaction::findOrFail($id);
        $payment = ClientPayment::where('custodial_transaction_id', $transaction->id)->first();
        $client = is_null($transaction->client_id) ? new Client() : $transaction->client;
        $recipient = is_null($transaction->recipient_id) ? new CommissionRecepient() : $transaction->recipient;

        return view(
            'investment.account_cash.show',
            ['title'=>'Account Inflow Details', 'transaction'=>$transaction,
            'client'=>$client, 'recipient'=>$recipient, 'payment'=>$payment]
        );
    }

    public function postLinkToClientAndFA($id)
    {
        $transaction = CustodialTransaction::findOrFail($id);

        $input = \Request::except('_token');
        $input['id'] = $transaction->id;
        $input['type'] = CustodialTransactionType::where('name', 'FI')->first()->id;

        $client = Client::findOrFail($input['client_id']);
        //        $this->form->update($input);

        if (!$transaction->clientPayment) {
            $account = $transaction->custodialAccount;

            $hasP = $account->product_id || $account->project_id || $account->unit_fund_id || $account->entity_id;

            if (!$hasP) {
                \Flash::error('The linked custodial account doesnt accept inflow. Please transfer to an inflow account first');

                return \Redirect::back();
            }

            $approval = AddInflowFromTransaction::create(
                null,
                null,
                getSystemUser(),
                ['transaction_id' => $transaction->id]
            );

            $approval->systemExecute();
        }

        ClientTransactionApproval::make($client->id, 'link_cash_to_client_and_fa', $input);

        \Flash::message('The request to link inflow to client has been saved for approval');

        return \redirect('/dashboard/investments/accounts-cash/show/'.$transaction->id);
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function getCreate($id = null)
    {
        $transaction = is_null($id) ? new CustodialTransaction() : CustodialTransaction::findOrFail($id);

        $products = Product::where('active', 1)->get()->lists('name', 'id');

        $shareentities = SharesEntity::all()->lists('name', 'id');

        $projects = Project::all()->lists('name', 'id');

        $funds = UnitFund::all()->lists('name', 'id');

        $custodialaccounts = CustodialAccount::all()->lists('account_name', 'id');

        $commissionRecepient = CommissionRecepient::all()->lists('name', 'id');

        return view(
            'investment.account_cash.create',
            [
                'title'=>'Account Inflow',
                'transaction'=>$transaction,
                'products'=>$products,
                'shareentities'=>$shareentities,
                'projects'=>$projects,
                'custodialaccounts'=>$custodialaccounts,
                'funds'=>$funds,
                'commissionRecepient' => $commissionRecepient,
            ]
        );
    }

    /**
     * @param Request $request
     * @param null $id
     * @return mixed
     * @throws \Throwable
     */
    public function postSave(Request $request, $id = null)
    {
        \DB::transaction(function () use ($id, $request) {
            $transaction = is_null($id) ? new CustodialTransaction() : CustodialTransaction::findOrFail($id);

            $input = $request->except('_token');

            $input['id'] = $transaction->id;
            $input['recipient_known'] = (bool) $input['recipient_known'];
            $input['new_client'] = (bool) $input['new_client'];
            $input['received_from'] = isset($input['received_from']) ? $input['received_from'] : null;
            $input['type'] = CustodialTransactionType::where('name', 'FI')->first()->id;
            $input['client_id'] = $input['new_client'] == true ? null : $input['client_id'];
            $input['commission_recipient_id'] = isset($request['commission_recipient_id'])
                ? $request['commission_recipient_id'] : null;
            $input['custodial_account_id'] = $request['custodial_account_id'];

            if (!$input['new_client']) {
                $client = Client::findOrFail($input['client_id']);
                $recipient = $client->getLatestFA();
                $input['commission_recipient_id'] = $request['commission_recipient_id'];
//                $input['commission_recipient_id'] = is_null($recipient) ? null : $recipient->id;
                $input['commission_recipient_id'] = is_null($recipient) ? null : $recipient->id;

//                $c = Client::where('id', $client->id)->lockForUpdate()->get();
//                $c->first() ? $c->first()->update(['updated_at' => Carbon::now()]) : null;
            }

            $inflow = new Inflow();
            $input = $inflow->cleanPaymentTo($input);
            $inflow->validate($input);
            $inflow->checkIfDuplicate($input);

            $this->form->validate($input);

            $client_name = ClientPresenter::presentFullNames($input['client_id']);

            if ($request->hasFile('cheque_file')) {
                $file = $request->file('cheque_file');
                $document = Document::upload(
                    file_get_contents($file),
                    $file->getClientOriginalExtension(),
                    'cheque',
                    isset($input['received_from']) ? $input['received_from'] : $client_name
                );

                $input['cheque_document_id'] = $document->id;
            }

            ClientTransactionApproval::make(
                $input['client_id'],
                'add_cash_to_account',
                array_except($input, ['recipient_known', 'new_client', 'category', 'cheque_file'])
            );
        });

        \Flash::message('The add inflow to account request has been saved for approval');

        return \redirect('/dashboard/investments/accounts-cash');
    }

    public function deleteDestroy(Request $request, $id)
    {
        $transaction = CustodialTransaction::findOrFail($id);

        if ($transaction->transactionType->name != 'FI') {
            throw new \Cytonn\Exceptions\CRIMSGeneralException('You can only reverse an inflow');
        }

        if (!is_null($this->validateReason($request))) {
            \Flash::error('The reason must be at least 10 characters.');

            return \Redirect::back();
        }

        $input = $request->except('_token');

        $input['transaction_id'] = $id;

        ClientTransactionApproval::make($transaction->client_id, 'reverse_cash_inflow', $input);

        \Flash::message('Reversal has been saved for approval');

        return \Redirect::back();
    }

    public function validateReason($request)
    {
        $rule = [
            'reason'=>'required|min:10'
        ];

        return $this->verdict($request, $rule);
    }

    public function postValueCheque(Request $request, $id)
    {
        $transaction = CustodialTransaction::findOrFail($id);

        ClientTransactionApproval::make(
            $transaction->client_id,
            'add_cheque_as_valued',
            ['transaction_id' => $id],
            false,
            \Auth::user()->id
        );

        \Flash::message('The cheque value will be effected after approval');

        return redirect()->back();
    }

    public function postBounceCheque(Request $request, $id)
    {
        $date = $request->get('date');
        $this->validate($request, ['date' => 'required|date']);
        $transaction = CustodialTransaction::findOrFail($id);

        ClientTransactionApproval::make(
            $transaction->client_id,
            'add_cheque_as_bounced',
            ['transaction_id' => $id, 'date' => $date],
            false,
            \Auth::user()->id
        );

        \Flash::message('The cheque will be bounced after approval');

        return redirect()->back();
    }

    /**
     * @return mixed
     */
    public function getDailyInflows()
    {
        $id = Auth::user()->id;

        $this->queue(
            function () use ($id) {
                Artisan::call('custody:daily-inflows', ['--user_id'=>$id]);
            }
        );

        \Flash::success('The daily inflows will be sent shortly via email');

        return \Redirect::back();
    }
}
