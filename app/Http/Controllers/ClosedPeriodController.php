<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers;

use App\Cytonn\Models\ClosedPeriod;
use Cytonn\Investment\ClosedPeriods\ClosedPeriodRepository;
use Cytonn\Investment\ClosedPeriods\ClosedPeriodRules;
use Illuminate\Http\Request;

class ClosedPeriodController extends Controller
{
    /*
     * Get the rules
     */
    use ClosedPeriodRules;

    /*
     * Get the required repositories
     */
    protected $closedPeriodRepository;

    /*
     * Setup the constructor for the controller
     */
    public function __construct(ClosedPeriodRepository $closedPeriodRepository)
    {
        parent::__construct();
        
        $this->closedPeriodRepository = $closedPeriodRepository;
    }
    
    /*
     * Display a listing for all the closed periods
     */
    public function index()
    {
        return view(
            'system.closed_periods.index',
            [
            'title' => 'Closed Periods'
            ]
        );
    }

    /*
     * Supply the form to create or edit a closed period
     */
    public function create($id = null)
    {
        if ($id) {
            $closedPeriod =  $this->closedPeriodRepository->getClosedPeriodById($id);
        } else {
            $closedPeriod = new ClosedPeriod();
        }

        return view(
            'system.closed_periods.create',
            [
            'closedPeriod' => $closedPeriod,
            'title' => 'Add/Edit a closed period',
            ]
        );
    }

    /*
     * Store of update a closed period
     */
    public function store(Request $request, $id = null)
    {
        $validation = $this->closedPeriodCreate($request);

        if ($validation) {
            return back()->withErrors($validation)->withInput();
        }

        $this->closedPeriodRepository->save($request->all(), $id);

        if ($id) {
            \Flash::success("Closed period updated successfully");
        } else {
            \Flash::success("Closed period added successfully");
        }

        return redirect('/dashboard/system/closed_periods');
    }
}
