<?php namespace App\Http\Controllers;

use App\Jobs\ProcessQueuedClosure;
use Carbon\Carbon;
use Cytonn\Investment\FundManager\FundManagerScope;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Cytonn\Authentication\AuthRepository;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\Storage\StorageInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Laracasts\Commander\CommanderTrait;
use League\Flysystem\FileNotFoundException;
use SuperClosure\Serializer;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    use CommanderTrait;

    /**
     * @var \Cytonn\Authorization\Authorizer
     */
    protected $authorizer;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param $permission
     * @return bool
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    protected function allow($permission)
    {
        return $this->authorizer->authorize($permission);
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = view($this->layout);
        }
    }

    protected function getRootFolder()
    {
        return "private";
    }

    /**
     * Stream a file from the filesystem
     *
     * @param  $path
     * @return \Illuminate\Http\Response;
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function streamFile($path)
    {
        $fs = Storage::disk(env('STORAGE_DISK', 'local'));

        if (!$fs->has($path)) {
            \Flash::error('File not found');

            return \redirect()->back();
        }

        $file = $fs->get($path);

        $headers = [
            'Content-Type' => $fs->mimeType($path),
            'Content-Length' => $fs->size($path),
            'Content-Disposition' => "inline; filename=\"" .basename($path) . "\"",
        ];

        return Response::make($file, 200, $headers);
    }

    /**
     * Check if user is in sudo mode
     *
     * @return mixed
     */
    protected function sudoMode()
    {
        return (new AuthRepository(Auth::user()))->isInSudoMode();
    }

    public function fundManager()
    {
        return app(FundManagerScope::class)->getSelectedFundManager();
    }

    public function queue(\Closure $closure, $key = null, $overwrite = false, $queue = null)
    {
        if (!$queue) {
            $queue = config('queue.priority.high');
        }

        sendToQueue($closure, $key, $overwrite, $queue);
    }

    public function interestIntervals()
    {
        return [
            null => 'On Maturity',
            1 => 'Every 1 month',
            2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)',
            4 => 'Every 4 months',
            5 => 'Every 5 months',
            6 => 'Every 6 months (Semi anually)',
            7 => 'Every 7 months',
            8 => 'Every 8 months',
            9 => 'Every 9 months',
            10 => 'Every 10 months',
            11 => 'Every 11 months',
            12 => 'Every 12 months (Annually)'
        ];
    }
}
