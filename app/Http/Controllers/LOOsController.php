<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealestateLetterOfOfferReject;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Mailers\LOOMailer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Realestate\Forms\UploadLooForm;
use Cytonn\Realestate\Sale\LetterOfOffer;
use Cytonn\Reporting\LooGenerator;
use Illuminate\Support\Facades\Auth;

/**
 * Class LOOsController
 */
class LOOsController extends Controller
{
    /**
     * @var UploadLooForm
     */
    private $uploadLooForm;

    /**
     * LOOsController constructor.
     *
     * @param UploadLooForm $uploadLooForm
     */
    public function __construct(UploadLooForm $uploadLooForm)
    {
        $this->uploadLooForm = $uploadLooForm;

        parent::__construct();
    }


    /**
     * @return mixed
     */
    public function getIndex()
    {
        $projects = Project::all();
        return view(
            'realestate.loos.index',
            ['title' => 'Real Estate Letters of Offers', 'projects' => $projects]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $loo = RealestateLetterOfOffer::findOrFail($id);
        $unitHolding = $loo->holding;
        $client = $unitHolding->client;
        $unit = $unitHolding->unit;
        $project = $unitHolding->project;

        $previous_loos = null;

        is_null($loo->history) ?:

            $previous_loos = Document::whereIn('id', $loo->history)
                ->latest()
                ->get()
                ->each(
                    function ($doc) {
                        $doc->name = 'Uploaded on ' . DatePresenter::formatDateTime($doc->created_at);
                    }
                );
        $advocate = Advocate::all()->lists('name', 'id');

        $contract_variation = $loo->contractVariation->first();

        $client_holdings = UnitHolding::active(false)
            ->where('id', '!=', $unitHolding->id)
            ->where('client_id', $client->id)
            ->get()
            ->map(function ($holding) {

                return [
                    'id' => $holding->id,
                    'name' => $holding->unit->size->name .
                        ' - ' . $holding->unit->number . ' in ' . $holding->project->name
                ];
            })->lists('name', 'id');

        return view(
            'realestate.loos.show',
            [
                'unitHolding' => $unitHolding,
                'client' => $client,
                'unit' => $unit,
                'project' => $project,
                'loo' => $loo,
                'is_loo_sent' => (bool)$loo->sent_on,
                'previous_loos' => $previous_loos,
                'advocate' => $advocate,
                'next_id' => $loo->present()->nextUnapprovedLOO,
                'prev_id' => $loo->present()->prevUnapprovedLOO,
                'contract_variation' => $contract_variation,
                'client_holdings' => $client_holdings
            ]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postSend($id)
    {
        $loo = RealestateLetterOfOffer::findOrFail($id);

        $this->authorizer->checkAuthority('send_loo');

        if (!$loo->document_id) {
            \Flash::warning('The LOO has not been generated, please generate the document first');
            return \Redirect::back();
        } elseif (is_null($loo->pm_approved_on)) {
            \Flash::warning('The LOO has not been approved by PM.');
            return \Redirect::back();
        }

        $loo_compliance_documents = Document::complianceDocuments($loo->holding->project)->get();

        $mailer = new LOOMailer();
        $mailer->sendLOOToClient($loo, $loo_compliance_documents);

        \Flash::success('The Letter of Offer has been sent to the client');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postApprove($id)
    {
        $this->authorizer->checkAuthority('pm_approve_loo');

        $loo = RealestateLetterOfOffer::findOrFail($id);

        if (!$loo->pm_approval_id) {
            $loo->pmApprove();

            \Flash::success('The Letter of Offer has been approved by PM');
        } else {
            \Flash::warning('The LOO has already been approved');
        }

        return \Redirect::back();
    }

    public function postReject($id)
    {
        $this->authorizer->checkAuthority('pm_approve_loo');

        $loo = RealestateLetterOfOffer::findOrFail($id);
        $reason = \Request::get('reason');

        (new RealestateLetterOfOfferReject())->reject($loo, $reason);

        \Flash::success('The Letter of Offer has been rejected by PM');
        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function putRegenerateAndUpdate($id)
    {
        $loo = RealestateLetterOfOffer::findOrFail($id);
        (new LetterOfOffer())->updateLooDocument($loo);

        \Flash::success('The Letter of Offer has been regenerated! Click &quot;View the Loo&quot; to preview.');
        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getLOOAuthorizationForm($id)
    {
        $loo = RealestateLetterOfOffer::findOrFail($id);

        return (new LetterOfOffer())->generateLooForm($loo->holding)->stream();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postSendLOOAuthorizationForm($id)
    {
        $loo = RealestateLetterOfOffer::findOrFail($id);

        if ($loo->authorization_sent_on) {
            \Flash::warning('This LOO Authorization has been sent again');
        }

        $mailer = new LOOMailer();
        $mailer->sendLOOForm($loo);

        \Flash::success('The LOO form has been sent to PM, FA and Operations');

        return \Redirect::back();
    }

    public function previewContractVariation($id)
    {
        $loo = RealestateLetterOfOffer::findOrFail($id);

        return (new LetterOfOffer())->generateContractVariation($loo->holding)->stream();
    }

    /**
     * @param $loo_id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function postUpload($loo_id)
    {
        $input = \Request::except('_token', '_method');

        $this->uploadLooForm->validate(\Request::all());

        $loo = RealestateLetterOfOffer::findOrFail($loo_id);

        $file = \Request::file('file');

        if (\Request::hasFile('file')) {
            $document =
                (new LetterOfOffer($loo))->uploadLoo(file_get_contents($file), $file->getClientOriginalExtension());
            $input['document_id'] = $document->id;
        }

        unset($input['file']);
        $input['loo_id'] = $loo->id;

        ClientTransactionApproval::make($loo->holding->client_id, 'loo_uploading', $input);

        \Flash::success('The LOO has been saved for approval');

        return \Redirect::back();
    }
}
