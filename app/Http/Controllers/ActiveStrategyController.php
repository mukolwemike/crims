<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ActiveStrategyHolding;
use App\Cytonn\Models\ActiveStrategyPriceTrail;
use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\ActiveStrategyTargetPrice;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Carbon\Carbon;
use Cytonn\Portfolio\ActiveStrategy\PortfolioValuation;
use Cytonn\Portfolio\ActiveStrategy\SaveDividendsForm;
use Cytonn\Portfolio\Forms\ActiveStrategyPriceTrailForm;
use Cytonn\Portfolio\Forms\ActiveStrategyTargetPriceForm;
use Cytonn\Portfolio\Forms\BuyActiveStrategySharesForm;
use Cytonn\Portfolio\Forms\CreateActiveStrategySecurity;
use Cytonn\Portfolio\Forms\SellActiveStrategySharesForm;

/**
 * Date: 01/12/2015
 * Time: 9:22 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


/**
 * Class ActiveStrategyController
 */
class ActiveStrategyController extends Controller
{

    /**
     * @var CreateActiveStrategySecurity
     */
    public $createActiveStrategySecurity;

    /**
     * @var BuyActiveStrategySharesForm
     */
    public $buyActiveStrategySharesForm;

    /**
     * @var ActiveStrategyPriceTrailForm
     */
    public $activeStrategyPriceTrailForm;

    /**
     * @var ActiveStrategyTargetPriceForm
     */
    public $activeStrategyTargetPriceForm;

    /**
     * @var SaveDividendsForm
     */
    public $saveDividendsForm;


    public $sellActiveStrategySharesForm;

    /**
     * @param CreateActiveStrategySecurity  $createActiveStrategySecurity
     * @param BuyActiveStrategySharesForm   $buyActiveStrategySharesForm
     * @param ActiveStrategyPriceTrailForm  $activeStrategyPriceTrailForm
     * @param ActiveStrategyTargetPriceForm $activeStrategyTargetPriceForm
     * @param SaveDividendsForm             $saveDividendsForm
     * @param SellActiveStrategySharesForm  $sellActiveStrategySharesForm
     */
    public function __construct(
        CreateActiveStrategySecurity $createActiveStrategySecurity,
        BuyActiveStrategySharesForm $buyActiveStrategySharesForm,
        ActiveStrategyPriceTrailForm $activeStrategyPriceTrailForm,
        ActiveStrategyTargetPriceForm $activeStrategyTargetPriceForm,
        SaveDividendsForm $saveDividendsForm,
        SellActiveStrategySharesForm $sellActiveStrategySharesForm
    ) {
        $this->createActiveStrategySecurity = $createActiveStrategySecurity;
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        $this->buyActiveStrategySharesForm = $buyActiveStrategySharesForm;
        $this->activeStrategyPriceTrailForm = $activeStrategyPriceTrailForm;
        $this->activeStrategyTargetPriceForm = $activeStrategyTargetPriceForm;
        $this->saveDividendsForm = $saveDividendsForm;
        $this->sellActiveStrategySharesForm = $sellActiveStrategySharesForm;

        parent::__construct();
    }


    /**
     * Show the shares grid
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function index()
    {
        $valuation = new PortfolioValuation($this->fundManager(), Carbon::today());

        return view('portfolio.activestrategy.grid', ['title'=>'Portfolio Equities', 'valuation'=>$valuation]);
    }

    /**
     * Create a new security (show form)
     *
     * @param  null $id
     * @return mixed
     */
    public function createSecurity($id = null)
    {
        is_null($id) ? $security = new ActiveStrategySecurity(): $security = ActiveStrategySecurity::findOrFail($id);

        return view('portfolio.activestrategy.createsecurity', [
            'title'=>'Create a Security',
            'security'=>$security
        ]);
    }

    /**
     * Save a new security from form input
     *
     * @param  null $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveSecurity($id = null)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->createActiveStrategySecurity->validate(\Request::all());

        is_null($id) ? $security = new ActiveStrategySecurity(): $security = ActiveStrategySecurity::findOrFail($id);

        $input = \Request::all();
        unset($input['_token']);

        PortfolioTransactionApproval::add([
            'institution_id'=>null,
            'transaction_type'=>'create_active_strategy_security',
            'payload'=>$input
        ]);

        \Flash::message('New security has been saved for approval');

        return \redirect('/dashboard/portfolio/activestrategy');
    }

    /**
     * Show the details of a security
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function security($id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $security = ActiveStrategySecurity::findOrFail($id);

        return view('portfolio.activestrategy.security', [
            'title'=>'Active Strategy - '.$security->name,
            'security'=>$security
        ]);
    }

    /**
     * Buy new shares for a security
     *
     * @param  $security_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function buy($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->buyActiveStrategySharesForm->validate(\Request::all());

        $security = ActiveStrategySecurity::findOrFail($security_id);

        $input = \Request::all();
        unset($input['_token']);
        $input['security_id'] = $security->id;

        PortfolioTransactionApproval::add([
            'payload'=>$input,
            'transaction_type'=>'buy_active_strategy_shares',
            'institution_id'=>null
        ]);

        \Flash::message('The new shares bought have been saved for approval');

        return redirect('/dashboard/portfolio/activestrategy');
    }

    public function editBought($holding_id)
    {
        $holding = ActiveStrategyHolding::findOrFail($holding_id);

        return view('portfolio.activestrategy.editsharesbought', [
            'title'=>'Edit Active Strategy Shares Bought', 'holding'=>$holding
        ]);
    }

    public function updateBought($holding_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->buyActiveStrategySharesForm->validate(\Request::all());

        $holding = ActiveStrategyHolding::findOrFail($holding_id);

        $input = \Request::all();
        unset($input['_token']);
        $input['holding_id'] = $holding->id;

        PortfolioTransactionApproval::add(['payload'=>$input,
            'transaction_type'=>'update_bought_active_strategy_shares',
            'institution_id'=>null]);

        \Flash::message('The update request for shares bought has been saved for approval');

        return redirect('/dashboard/portfolio/activestrategy');
    }

    public function sell($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->sellActiveStrategySharesForm->validate(\Request::all());

        $security = ActiveStrategySecurity::findOrFail($security_id);

        $input = \Request::all();
        unset($input['_token']);
        $input['security_id'] = $security->id;

        PortfolioTransactionApproval::add([
            'payload'=>$input,
            'transaction_type'=>'sell_active_strategy_shares',
            'institution_id'=>null
        ]);

        \Flash::message('The shares sold have been saved for approval');

        return redirect('/dashboard/portfolio/activestrategy');
    }

    public function editSold($share_sale_id)
    {
        $share_sale = ActiveStrategyShareSale::findOrFail($share_sale_id);

        return view('portfolio.activestrategy.editsharessold', [
            'title'=>'Edit Active Strategy Shares Sold',
            'share_sale'=>$share_sale
        ]);
    }

    public function updateSold($share_sale_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->sellActiveStrategySharesForm->validate(\Request::all());

        $share_sale = ActiveStrategyShareSale::findOrFail($share_sale_id);

        $input = \Request::all();
        $input['sale_price'] = $input['price'];
        unset($input['price']);
        unset($input['_token']);
        $input['share_sale_id'] = $share_sale->id;

        PortfolioTransactionApproval::add([
            'payload'=>$input,
            'transaction_type'=>'update_sold_active_strategy_shares',
            'institution_id'=>null
        ]);

        \Flash::message('The update request for shares sold has been saved for approval');

        return redirect('/dashboard/portfolio/activestrategy');
    }

    /**
     * @param $security_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function marketPrice($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $security = ActiveStrategySecurity::findOrFail($security_id);

        return view('portfolio.activestrategy.pricetrail', [
            'title'=>'Market price trail',
            'security'=>$security,
            'priceTrail'=>$security->priceTrail()->latest()->paginate(10)
        ]);
    }

    /**
     * @param $security_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveMarketPrice($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->activeStrategyPriceTrailForm->validate(\Request::all());

        $trail = ActiveStrategyPriceTrail::where('date', \Request::get('date'))
            ->where('security_id', $security_id)
            ->first();

        is_null($trail)
            ? $trail = new ActiveStrategyPriceTrail()
            : $security = ActiveStrategySecurity::findOrFail($security_id);
        ;

        $trail->fill(\Request::all());
        $trail->security_id = $security_id;
        $trail->save();

        \Flash::success('Price has been saved');

        return \Redirect::back();
    }

    /**
     * @param $security_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function targetPrice($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $security = ActiveStrategySecurity::findOrFail($security_id);

        return view('portfolio.activestrategy.targetpricetrail', [
            'title'=>'Target Price Trail',
            'security'=>$security,
            'priceTrail'=>$security->targetPrices()->latest()->paginate(10)
        ]);
    }

    /**
     * @param $security_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveTargetPrice($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $this->activeStrategyTargetPriceForm->validate(\Request::all());

        $trail = ActiveStrategyTargetPrice::where('date', \Request::get('date'))
            ->where('security_id', $security_id)
            ->first();

        is_null($trail) ?
            $trail = new ActiveStrategyTargetPrice()
            : $security = ActiveStrategySecurity::findOrFail($security_id);

        $trail->fill(\Request::all());
        $trail->security_id = $security_id;
        $trail->save();

        \Flash::success('Price has been saved');

        return \Redirect::back();
    }

    /**
     * Save the dividends for a security
     *
     * @param  $security_id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function saveDividends($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        dd('jkl');
        $this->saveDividendsForm->validate(\Request::all());

        $input = \Request::all();
        unset($input['_token']);

        ActiveStrategySecurity::findOrFail($security_id);

        $input['security_id'] = $security_id;

        PortfolioTransactionApproval::add([
            'payload'=>$input,
            'institution_id'=>null,
            'transaction_type'=>'active_strategy_dividend'
        ]);

        \Flash::message('The dividends have been saved for approval');

        return \redirect('/dashboard/portfolio/activestrategy');
    }

    public function viewDividends($security_id)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $security = ActiveStrategySecurity::findOrFail($security_id);

        return view('portfolio.activestrategy.dividends', [
            'title'=>$security->name . ' - Dividends',
            'security'=>$security
        ]);
    }
}
