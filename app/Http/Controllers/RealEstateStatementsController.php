<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealestateStatementCampaign;
use App\Cytonn\Models\StatementCampaignType;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Realestate\Statements\StatementGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RealEstateStatementsController extends Controller
{


    /**
     * RealEstateClientsController constructor.
     */
    public function __construct()
    {
        $this->authorizer = new \Cytonn\Authorization\Authorizer();
        parent::__construct();
    }

    /**
     * Show the clients grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        $campaignTypes = StatementCampaignType::all()->lists('name', 'id');
        $campaigns = RealestateStatementCampaign::latest()->paginate(10);
        return view(
            'realestate.statements.index',
            [
                'title'=>'Real Estate Statements', 'campaign_types'=>$campaignTypes, 'campaigns'=>$campaigns
            ]
        );
    }

    /**
     * Create a campaign to be used to send statements
     *
     * @return mixed
     * @throws \Throwable
     */
    public function postCreateCampaign()
    {
        $campaign = new RealestateStatementCampaign();

        $campaign->fill(\Request::only('name', 'send_date', 'type_id'));
        $campaign->sender_id = Auth::user()->id;

        $campaign->save();

        if ($campaign->type->slug == 'automated') {
            \Queue::push(
                'Cytonn\Reporting\RealestateStatementCampaignRepository@fireAutomaticallyAddClients',
                ['campaign_id'=>$campaign->id]
            );
        }

        \Flash::message('Campaign has been saved, sending can now start');

        return \Redirect::back();
    }


    /**
     * Show the campaign
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function getCampaigns($id)
    {
        $campaign = RealestateStatementCampaign::findOrFail($id);

        $sent_count = $campaign->repo->countSentItems();
        $total_count = $campaign->repo->countAllItems();

        return \view(
            'realestate.statements.campaign_detail',
            ['campaign'=>$campaign, 'title'=>'Real Estate Statement Campaigns',
            'sent_count'=>$sent_count,
            'total_count'=>$total_count]
        );
    }

    /**
     * Show the campaign
     *
     * @param  $id
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function showCampaign($id)
    {
        $this->authorizer->checkAuthority('sendstatements');

        $campaign = RealestateStatementCampaign::findOrFail($id);

        $sent_count = $campaign->repo->countSentItems();
        $total_count = $campaign->repo->countAllItems();

        return \view(
            'realestate.statement.campaign_detail',
            ['campaign'=>$campaign, 'title'=>'Client Statement Campaigns',
            'sent_count'=>$sent_count,
            'total_count'=>$total_count]
        );
    }

    /**
     * Add a client to a campaign so that you can send statement
     *
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function postAddClientToCampaign()
    {
        $this->authorizer->checkAuthority('sendstatements');

        $campaign = RealestateStatementCampaign::findOrFail(\Request::get('campaign'));

        $client = Client::findOrFail(\Request::get('client'));

        if ($campaign->repo->checkIfClientIsInCampaign($client)) {
            \Flash::warning('The client has already been added to the campaign');

            return \Redirect::back();
        }

        $campaign->repo->addClientToCampaign($client, \Auth::user());

        \Flash::message('Client has been added to campaign');

        return \Redirect::back();
    }

    /**
     * @param $campaignId
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function sendCampaign($campaignId)
    {
        $this->authorizer->checkAuthority('sendstatements');

        $campaign = RealestateStatementCampaign::findOrFail($campaignId);

        if ($campaign->sender_id == \Auth::user()->id) {
            throw new \Cytonn\Exceptions\ClientInvestmentException(
                'The sender cannot be the person who created the campaign'
            );
        }

        $campaign->repo->sendCampaign();

        if ($campaign->type->slug == 'automated') {
            $campaign->close();
        }

        \Flash::message('The statements are being sent to the clients');

        return \Redirect::back();
    }

    /**
     * @param $campaignId
     * @return mixed
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function closeCampaign($campaignId)
    {
        $this->authorizer->checkAuthority('approvestatements');

        $campaign = RealestateStatementCampaign::findOrFail($campaignId);

        if (!$campaign->closed) {
            $campaign->closed = true;
            $campaign->save();
        }

        \Flash::message('Campaign has been closed, clients can no longer be added');

        return \Redirect::back();
    }

    /*
     * Store the campaign message
     */
    public function storeCampaignMessage(Request $request, $campaignId)
    {
        $campaign = RealestateStatementCampaign::findOrFail($campaignId);

        $campaign->statement_message = $request->get('statement_message');

        $campaign->save();

        \Flash::message('Campaign has been saved successfully');

        return \Redirect::back();
    }
}
