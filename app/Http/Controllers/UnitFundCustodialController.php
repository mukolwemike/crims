<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Api\Transformers\Unitization\UnitFundTransformer;

class UnitFundCustodialController extends Controller
{
    public function index()
    {
        $this->authorizer->checkAuthority('viewcustodialaccount');

        $funds = UnitFund::active()->get()
            ->map(function ($fund) {
                return app(UnitFundTransformer::class)->transform($fund);
            });

        return view(
            'portfolio.custodial.unitfund.index',
            ['title' => 'UnitFund Custodial Accounts', 'funds' => $funds]
        );
    }

    public function show($id)
    {
        $fund = UnitFund::find($id);

        $operatingAccs = $fund->operatingAccounts()->get();

        $receivingAccounts = $fund->receivingAccounts()->get();

        $custodials = CustodialAccount::forFundManager()
            ->get()->map(function ($acc) {
                return [
                    'value' => $acc->id,
                    'label' => $acc->account_name
                ];
            })->toArray();

        return view(
            'portfolio.custodial.unitfund.details',
            [
                'title' => 'UnitFund Custodial Accounts',
                'fund' => (new UnitFundTransformer())->transform($fund),
                'custodials' => $custodials,
                'operatingAccs' => $operatingAccs,
                'receivingAccounts' => $receivingAccounts,
            ]
        );
    }

    public function delete()
    {
        $input = request()->all();

        $transaction = $input['type'] == 'operating' ? 'remove_operating_account' : 'remove_receiving_account';

        unset($input['_token'], $input['type']);

        PortfolioTransactionApproval::add(
            ['institution_id' => null,
                'transaction_type' => $transaction, 'payload' => $input]
        );

        \Flash::message('Remove account from fund saved for approval');

        return \Redirect::back();
    }

    public function linkAccount()
    {
        $input = request()->all();

        $type = $input['type'];

        $transaction = $type == 'operating' ? 'link_operating_account' : 'link_receiving_account';

        unset($input['_token'], $input['type']);

        PortfolioTransactionApproval::add(
            ['institution_id' => null,
                'transaction_type' => $transaction, 'payload' => $input]
        );

        \Flash::message("Link unit fund to $type account saved for approval");

        return \Redirect::back();
    }
}
