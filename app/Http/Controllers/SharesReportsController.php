<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\SharesEntity;
use App\Jobs\Shares\ShareHoldingReport;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Request;

class SharesReportsController extends Controller
{

    /**
     * SharesReportsController constructor.
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function __construct()
    {
        parent::__construct();
        $this->authorizer->checkAuthority('viewinvestments');
    }

    /**
     * Display the  shares reports
     *
     * @return Response
     */
    public function getIndex()
    {
        return view('shares.reports.reports', ['title' => 'Shares Reports']);
    }

    /**
     * @return mixed
     */
    public function exportShareholdingsReport()
    {
        $input = Request::all();
        $entity = SharesEntity::findOrFail($input['share_entity_id']);
        dispatch(new ShareHoldingReport($entity, \Auth::user()));

        \Flash::success('The shareholdings report will be sent shortly via email.');

        return \Redirect::back();
    }
}
