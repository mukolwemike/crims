<?php

namespace App\Http\Controllers;

use App\Cytonn\Api\Transformers\Clients\ClientApprovalTransformer;
use App\Cytonn\Api\Transformers\UtilityBillingInstructionTransformer;
use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationJointHolders;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Jobs\Instructions\ExportBankInstructions;
use App\Jobs\Instructions\ExportUtilityBillingInstructions;
use Carbon\Carbon;
use Cytonn\Api\Transformers\Unitization\UnitFundInvestmentInstructionTransformer;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\Storage\StorageInterface;
use Cytonn\Investment\Rules\InvestmentApplicationRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ClientInstructionController extends Controller
{
    use InvestmentApplicationRules;

    public function orders()
    {
        return view('investment.client-instruction.orders', ['title' => 'Structured product orders']);
    }

    public function index()
    {
        $unitFunds = UnitFund::all();

        return view('investment.client-instruction.index', [
            'title' => 'Client Investments Instructions',
            'unitFunds' => $unitFunds
        ]);
    }

    public function topup($id)
    {
        $topup = ClientTopupForm::findOrFail($id);

        $signatories = (new ClientApprovalTransformer())->transformTopup($topup);

        $investment = $topup->latestInvestment();

        return view('investment.client-instruction.topup', [
            'title' => 'Investment/Top up Form',
            'topup' => $topup,
            'investment' => $investment,
            'approved' => $topup->repo->approvedByClient(),
            'signatories' => $signatories
        ]);
    }

    public function editTopup($id)
    {
        $topup = (new ClientTopupForm())->findOrFail($id);

        $investment = $topup->latestInvestment();

        return view('investment.client-instruction.actions.edit_topup', [
            'topup' => $topup,
            'investment' => $investment
        ]);
    }

    public function cancelTopup($id)
    {
        $topup = ClientTopupForm::findOrFail($id);

        $input['topup_id'] = $topup->id;

        ClientTransactionApproval::make($topup->client_id, 'cancel_client_topup_instruction', $input);

        \Flash::message('The client top-up instruction cancel request has been saved for approval.');

        return \Redirect::back();
    }

    public function cancelWithdrawRollover($id)
    {
        $instruction = ClientInvestmentInstruction::findOrFail($id);

        $input['instruction_id'] = $instruction->id;

        ClientTransactionApproval::make(
            $instruction->investment->client_id,
            'cancel_client_withdraw_rollover_instruction',
            $input
        );

        \Flash::message(
            'The client ' . $instruction->type->name . ' instruction cancel request has been saved for approval.'
        );

        return \Redirect::back();
    }

    public function processTopup($id)
    {
        $input = request()->all();

        $topup = ClientTopupForm::findOrFail($id);

        $recipient_id = '';

        if ($topup->investment) {
            \Flash::error('This form has already been invested');

            return \Redirect::back();
        }

        $reason = isset($input['reason']) ? $input['reason'] : null;

        $details = [
            'product_id' => $topup->product_id,
            'tenor' => $topup->tenor,
            'amount' => $topup->amount,
            'interest_rate' => $topup->agreed_rate,
            'topup_form_id' => $topup->id,
            'invested_date' => $topup->repo->valueDate()->toDateString(),
            'maturity_date' => $topup->repo->maturityDate()->toDateString(),
            'interest_payment_interval' => $topup->interest_payment_interval,
            'interest_payment_date' => $topup->interest_payment_interval <= 0 ? null : 31,
            'topup_form' => $topup->id,
            'reason' => $reason
        ];

        $product = $topup->product;

        if ($topup->investmentSchedule && $product->present()->isSeip) {
            $investment = $topup->investmentSchedule->parentInvestment;
        } else {
            $investment = $topup->latestInvestment();
        }

        if ($investment) {
            $recipient_id = ($investment->client->incomingFa)
                ? $investment->client->incomingFa->id
                : $investment->repo->getCommissionRecipientId();
        }

        $recipients = ['' => 'Select FA'] + (new CommissionRecepient())->repo->getCommissionRecipientByDisplayName();

        $interestActions = InterestAction::all()->lists('name', 'id');

        return view(
            'investment.client-instruction.process.topup',
            [
                'form' => $topup,
                'details' => $details,
                'client' => $topup->client,
                'investment' => $investment,
                'product' => $product,
                'recipients' => $recipients,
                'recipient_id' => $recipient_id,
                'interest_intervals' => $this->interestIntervals(),
                'interestActions' => $interestActions,
                'recipientsDetails' => $recipients,
                'investment_type_id' => 2,
                'investmentPaymentSchedule' => $topup->investmentSchedule
            ]
        )->withInput($details);
    }


    public function pdfTopup($id)
    {
        $topup = ClientTopupForm::findOrFail($id);
        $investment = $topup->latestInvestment();

        return \PDF::loadView('investment.client-instruction.pdf.topup-pdf', [
            'topup' => $topup, 'investmment' => $investment
        ])->stream();
    }

    public function rollover($id)
    {
        $instruction = ClientInvestmentInstruction::findOrFail($id);

        $investment = $instruction->repo->getInvestment();

        $approved = $instruction->repo->approvedByClient();

        $signatories = (new ClientApprovalTransformer())->transformInstruction($instruction);

        $kycValidated = $instruction->investment->client->repo->checkClientKycValidated();

        return view('investment.client-instruction.rollover', [
            'instruction' => $instruction,
            'investment' => $investment,
            'title' => 'Withdraw & Rollover Instructions',
            'approved' => $approved,
            'signatories' => $signatories,
            'kycValidated' => $kycValidated
        ]);
    }

    public function editRollover($type, $id)
    {
        $instruction = (new ClientInvestmentInstruction())->findOrFail($id);
        $investment = $instruction->repo->getInvestment();

        if ($type == 'rollover') {
            $instructionRollovers = collect($instruction->rolloverInstructions()->where('deleted_at', null)->get());

            $instruction->combinedRollover = $instructionRollovers->map(function ($instruction) {
                return (new ClientInvestment())->find($instruction->investment_id)->uuid;
            });
        }

        $view = ($type == 'withdraw') ? 'investment.actions.withdraw.edit' : 'investment.actions.rollover.edit';

        return view(
            $view,
            [
                'instruction' => $instruction,
                'investment' => $investment,
                'title' => 'Withdraw & Rollover Instructions'
            ]
        );
    }

    public function processRollover($id)
    {
        $instruction = ClientInvestmentInstruction::findOrFail($id);

        $data = request()->all();

        if ($instruction->repo->scheduledApproval()) {
            \Flash::error("There is an existing scheduled approval for the investment");

            return back();
        }

        if ($instruction->type->name == 'withdraw') {
            return $this->processWithdrawal($instruction, $data);
        }

        return $this->processRolloverInstruction($instruction, $data);
    }

    private function processWithdrawal(ClientInvestmentInstruction $instruction, $data = [])
    {
        $investment = $instruction->investment;

        $type = 'withdrawal';

        if ($instruction->amount_select == 'interest') {
            $type = 'interest';
        }

        $value = $investment->repo->getTotalValueOfInvestmentAtDate($instruction->due_date);

        $interest = $investment->repo->getNetInterestForInvestmentAtDate($instruction->due_date);

        return view(
            'investment.client-instruction.process.withdrawal',
            [
                'title' => 'Withdraw Investment',
                'investment' => $investment,
                'type' => $type,
                'instruction' => $instruction,
                'amount' => $instruction->repo->calculateAmountAffected(),
                'currency' => $investment->product->currency->code,
                'value' => $value, 'interest' => $interest,
                'reason' => isset($data['reason']) ? $data['reason'] : null
            ]
        );
    }

    private function processRolloverInstruction(ClientInvestmentInstruction $instruction, $data = [])
    {
        $invested = $instruction->repo->valueDate();

        $maturity = $instruction->repo->maturityDate();

        $investment = $instruction->investment;

        $investments = $instruction->allInvestments()
            ->each(function (ClientInvestment $investment) use ($instruction) {
                $c = $investment->calculate($instruction->due_date, false);
                $investment->total_value = $c->total();
                $investment->net_interest = $c->total();
            });

        $recipients = ['' => 'Select FA'] + (new CommissionRecepient())->repo->getCommissionRecipientByDisplayName();

        $interestActions = InterestAction::all()->lists('name', 'id');

        $recipient_id = ($investment->client->incomingFa)
            ? $investment->client->incomingFa->id
            : $investment->repo->getCommissionRecipientId();

        $details = [
            'interest_rate' => $instruction->agreed_rate,
            'invested_date' => $instruction->repo->valueDate()->toDateString(),
            'maturity_date' => $instruction->repo->maturityDate()->toDateString(),
            'reason' => isset($data['reason']) ? $data['reason'] : null
        ];

        $title = 'Rollover Investment - ' .
            \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id);

        return view(
            'investment.client-instruction.process.rollover',
            [
                'title' => $title,
                'investment' => $investment,
                'product' => $investment->product,
                'investments' => $investments,
                'instruction' => $instruction,
                'amount' => $instruction->repo->calculateAmountAffected(),
                'currency' => $investment->product->currency->code,
                'recipients' => $recipients,
                'recipient_id' => $recipient_id,
                'investment_type_id' => 3,
                'interest_intervals' => $this->interestIntervals(),
                'interestActions' => $interestActions,
                'invested_date' => $invested, 'maturity_date' => $maturity,
                'details' => $details,
                'recipientsDetails' => $recipients,
                'client' => $investment->client,
                'investmentPaymentSchedule' => null
            ]
        );
    }

    public function pdfRollover($id)
    {
        $rollover = ClientInvestmentInstruction::with('investment')->findOrFail($id);

        $client = $rollover->investment->client;

        $bankDetails = $rollover->investment->bankDetails();

        return \PDF::loadView('investment.client-instruction.pdf.rollover_withdraw', [
            'rollover' => $rollover, 'client' => $client, 'bank' => $bankDetails
        ])->stream();
    }

    public function application($id)
    {
        $application = ClientFilledInvestmentApplication::findOrFail($id);

        $client = ($application->client_id) ? $application->client : null;

        $investortype = $application->corporate_investor_type ?
            CorporateInvestorType::findOrFail($application->corporate_investor_type)
            : null;

        $fundsource = FundSource::find($application->funds_source_id);

        $clientTitles = Title::all()->lists('name', 'id');

        $titles = Title::find($application->title);

        $genders = Gender::all()->lists('abbr', 'id');

        $countries = Country::all()->lists('name', 'id');

        $duplicates = ($application->new_client != 2)
            ? (new ClientRepository())->duplicate($application->toArray())
            : null;

        $riskyClient = (new ClientApplicationRepository())->getRiskyClientDetails($application);

        return view('clients.application.details', [
            'title' => 'Investment Application',
            'application' => $application,
            'product' => $application->product,
            'investortype' => $investortype,
            'fundsource' => $fundsource,
            'titles' => $titles,
            'jointHolders' => $application->jointHolders,
            'clientTitles' => $clientTitles,
            'gender' => $genders,
            'countries' => $countries,
            'duplicate' => $duplicates,
            'contactPersons' => $application->contactPersons,
            'unitFund' => $application->unitFund,
            'client' => $client,
            'kycDocuments' => $application->clientRepo->getDocumentDetails('kyc'),
            'paymentDocuments' => $application->clientRepo->getDocumentDetails('payment'),
            'emailIndemnity' => $application->clientRepo->getDocumentDetails('email_indemnity'),
            'clientSignature' => $application->clientRepo->getDocumentDetails('client_signatures'),
            'riskyClient' => $riskyClient,
        ]);
    }

    public function editApplication($id)
    {
        $application = (new ClientFilledInvestmentApplication())->findOrFail($id);

        $jointHolders = (new ClientFilledInvestmentApplicationJointHolders())
            ->where('application_id', $application->id)
            ->get();

        $application->product_category = ($application->product) ? 'products' : 'funds';

        return view('investment.client-instruction.actions.application-edit', [
            'application' => $application,
            'jointHolders' => $jointHolders,
        ]);
    }

    public function pdfApplication($id)
    {
        $application = ClientFilledInvestmentApplication::findOrFail($id);

        $documents = $application->documents()->orderBy('document_id', 'ASC')->get();

        $product = $application->product;

        $unitFund = $application->unitFund;

        $investortype = $application->corporate_investor_type
            ? CorporateInvestorType::findOrFail($application->corporate_investor_type)
            : null;

        $fundsource = FundSource::find($application->funds_source_id);

        $clientTitles = Title::all()->lists('name', 'id');

        $titles = Title::find($application->title);

        $genders = Gender::all()->lists('abbr', 'id');

        $countries = Country::all()->lists('name', 'id');

        $jointHolders = $application->jointHolders;

        $contactPersons = $application->contactPersons;

        $duplicate = (new ClientRepository())->duplicate($application->toArray());

        return \PDF::loadView('clients.application.pdf', [
            'title' => 'Investment Application',
            'application' => $application,
            'documents' => $documents,
            'product' => $product,
            'investortype' => $investortype,
            'fundsource' => $fundsource,
            'titles' => $titles,
            'jointHolders' => $jointHolders,
            'clientTitles' => $clientTitles,
            'gender' => $genders,
            'countries' => $countries,
            'duplicate' => $duplicate,
            'contactPersons' => $contactPersons,
            'unitFund' => $unitFund,
        ])->stream();
    }

    public function processApplication($id)
    {
        $application = ClientFilledInvestmentApplication::findOrFail($id);

        $array = $application->toArray();

        $validator = ($application->complete == 1)
            ? $this->validateApplication($array)
            : null;

        $signingMandates = (new ClientRepository())->signingMandates();

        if ($validator) {
            \Flash::error('The applications instruction is not complete, kindly complete it and proceed');

            return view('clients.application.index', [
                'edit' => true,
                'applicationId' => $application->id,
                'type' => 'client_investment',
                'errors' => $validator->errors(),
                'signingMandates' => $signingMandates
            ]);
        }

        $is_duplicate = (new ClientRepository())->duplicate($array)->exists;

        $new = (int)$application->new_client;

        $allow_duplicate = ($new == 3 || $new == 2);

        if ((!$allow_duplicate) && $is_duplicate) {
            \Flash::error('This client already exists, you can only add the client as a duplicate');

            return view('clients.application.index', [
                'edit' => true,
                'applicationId' => $application->id,
                'type' => 'client_investment',
                'signingMandates' => $signingMandates
            ]);
        }

        $application->clientRepo->process();

        \Flash::success('The application has been added successfully and investment created for processing');

        return \redirect()->to('/dashboard/investments/client-instructions/application/' . $application->id);
    }

    public function validateApplication($application)
    {
        if (isset($application['client_id'])) {
            return null;
        }

        return ($application['individual'] == 1)
            ? $this->individualApplicationForm($application)
            : $this->corporateApplicationForm($application);
    }

    public function editJointHolder(Request $request, $id, $joint_id)
    {
        $application = ClientFilledInvestmentApplication::findOrFail($id);

        if ($application->filled) {
            $holderDetails = $request->except('holder_id');
            $jointHolders = ClientJointDetail::findOrFail($joint_id);

            $jointHolders->fill($holderDetails);
            $jointHolders->save();
        } else {
            $holderDetails = $request->except('_token');
            $jointHolder = ClientFilledInvestmentApplicationJointHolders::findOrFail($joint_id);

            $jointHolder->fill($holderDetails);
            $jointHolder->save();
        }

        \Flash::success('The joint holder has been updated successfully');

        return Redirect::back();
    }


    public function showDocument($id)
    {
        $filesystem = app(StorageInterface::class)->filesystem();

        $document = ClientUploadedKyc::findOrFail($id);

        $path = $document->repo->getLocation() . '/' . $document->filename;

        try {
            $stream = $filesystem->readStream($path);
        } catch (\League\Flysystem\FileNotFoundException $e) {
            \Flash::error('File not found');

            return \Redirect::back();
        }

        return \Response::stream(
            function () use ($stream) {
                fpassthru($stream);
            },
            200,
            [
                "Content-Type" => $filesystem->getMimetype($path),
                "Content-Length" => $filesystem->getSize($path),
                "Content-disposition" => "inline; filename=\"" . basename($path) . "\"",
            ]
        );
    }

    public function filledApplicationDocuments($id)
    {
        $filesystem = app(StorageInterface::class)->filesystem();

        $document = Document::findOrFail($id);

        $path = $document->path();

        try {
            $stream = $filesystem->readStream($path);
        } catch (\League\Flysystem\FileNotFoundException $e) {
            \Flash::error('File not found');

            return \Redirect::back();
        }

        return \Response::stream(
            function () use ($stream) {
                fpassthru($stream);
            },
            200,
            [
                "Content-Type" => $filesystem->getMimetype($path),
                "Content-Length" => $filesystem->getSize($path),
                "Content-disposition" => "inline; filename=\"" . basename($path) . "\"",
            ]
        );
    }

    public function postExport()
    {
        $end = new Carbon(\Request::get('end'));
        $start = new Carbon(\Request::get('start'));

        $instructions = BankInstruction::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->has('custodialTransaction')
            ->get();

        return \PDF::loadView('reports.custody.instructions.summary', [
            'instructions' => $instructions,
            'start' => $start,
            'end' => $end
        ])->stream();
    }

    public function apply()
    {
        $signingMandates = (new ClientRepository())->signingMandates();

        return view('clients.application.index', [
            'title' => 'Client Investments Application',
            'signingMandates' => $signingMandates,
            'type' => 'client_investment',
            'applicationId' => null,
            'edit' => false,
            'unit' => null
        ]);
    }

    public function edit($id)
    {
        $application = ClientFilledInvestmentApplication::findOrFail($id);

        $signingMandates = (new ClientRepository())->signingMandates();

        return view('clients.application.index', [
            'title' => 'Client Investments Application',
            'signingMandates' => $signingMandates,
            'applicationId' => $application->id,
            'type' => 'client_investment',
            'edit' => true,
            'unit' => null
        ]);
    }

    public function investmentTopup()
    {
        return view('investment.client-instruction.actions.topup', [
            'title' => 'Client Investments Instructions',
        ]);
    }

    public function createUnitPurchase()
    {
        $funds = UnitFund::all()->map(function ($fund) {
            return (object)['label' => $fund->name, 'value' => $fund->id];
        });

        return view('investment.client-instruction.actions.purchase-units', [
            'title' => 'Client Investments Instructions', 'funds' => $funds
        ]);
    }

    public function unitFundInvestmentInstruction($id)
    {
        $instruction = UnitFundInvestmentInstruction::findOrFail($id);

        $client = $instruction->client;

        $instruction = (new UnitFundInvestmentInstructionTransformer())->transform($instruction);

        return view('investment.client-instruction.unit_fund_investment', [
            'title' => 'Investment/Unit Fund Investment',
            'instruction' => $instruction,
            'client' => $client
        ]);
    }

    public function editUnitFundInvestmentInstruction($id)
    {
        $instruction = UnitFundInvestmentInstruction::findOrFail($id);

        $client = $instruction->client;

        return view('investment.client-instruction.edit_unit_fund_investment', [
            'title' => 'Investment/Unit Fund Investment',
            'instruction' => $instruction,
            'client' => $client
        ]);
    }

    public function approvalInstructionDocument($approvalId, $editInv = null)
    {
        return view('clients.approvals.uploads', ['approvalId' => $approvalId, 'editInv' => $editInv]);
    }

    public function utilityBillingInstruction($id)
    {
        $instruction = UtilityBillingInstructions::findOrFail($id);

        $client = $instruction->client;

        $instruction = (new UtilityBillingInstructionTransformer())->transform($instruction);

        return view('investment.client-instruction.utility_billing', [
            'title' => 'Utility Billing Instruction',
            'instruction' => $instruction,
            'client' => $client
        ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function exportUtilityBillingInstruction()
    {
        $input = \request()->all();
        $start = Carbon::parse($input['start']);
        $end = Carbon::parse($input['end']);

        dispatch((new ExportUtilityBillingInstructions(getSystemUser(), $start, $end))->onQueue(config('queue.priority.high')));

        \Flash::success('The Utility Billing Instructions report will be sent shortly via email.');

        return \Redirect::back();
    }
}
