<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\BankInstruction;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Investment\BankInstructionRepository;
use Illuminate\Support\Facades\Request;

/**
 * Date: 22/07/2016
 * Time: 8:38 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientBankInstructionController extends Controller
{
    /**
     * ClientBankInstructionController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Show the instructions grid
     *
     * @return mixed
     */
    public function getIndex()
    {
        return \view('clients.bank-instructions.index', ['title' => 'Bank Instructions']);
    }

    /**
     * Show an instruction
     *
     * @param  $id
     * @return mixed
     */
    public function getShow($id)
    {
        $instruction = BankInstruction::findOrFail($id);

        $investment = $instruction->investment;

        $client = $investment->client;

        $bank_account_id = $investment->bank_account_id;

        $similarInstructions = BankInstruction::where('date', $instruction->date)
            ->where('id', '!=', $instruction->id)
            ->where('combined', false)
            ->whereNull('combined_to')
            ->whereHas(
                'investment',
                function ($investment) use ($bank_account_id, $client) {
                    $investment->where('bank_account_id', $bank_account_id)
                        ->whereHas(
                            'client',
                            function ($c) use ($client) {
                                $c->where('client_id', $client->id);
                            }
                        );
                }
            )->get();

        $bankDetails = $investment->bankDetails();

        $combination_of = $instruction->combinationOf;

        return \view(
            'clients.bank-instructions.show',
            ['title' => 'Bank Instruction', 'instruction' => $instruction,
                'investment' => $investment, 'bankDetails' => $bankDetails,
                'client_id' => $investment->client_id, 'similar_instructions' => $similarInstructions,
                'combination_of' => $combination_of
            ]
        );
    }

    /**
     * @param $instr_id
     * @return mixed
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function postCombine($instr_id)
    {
        $instructions = Request::get('instr_id');

        if (is_null(Request::get('instr_id'))) {
            throw new \Cytonn\Exceptions\ClientInvestmentException(
                'Please select at least one investment to combine'
            );
        }

        $instructions = Collection::make($instructions);
        $instructions->push($instr_id);

        $instructions = $instructions->map(
            function ($instruction) {
                return BankInstruction::findOrFail($instruction);
            }
        );

        $combined = (new BankInstructionRepository())->combine($instructions);

        \Flash::message('The Instructions have been combined');

        return \redirect("/dashboard/investments/instructions/$combined->investment_id/$combined->id");
    }

    /**
     * @param $instr_id
     * @return mixed
     */
    public function getDissolve($instr_id)
    {
        $combined = BankInstruction::findOrFail($instr_id);
        $combined->members = $combined->combinationOf()->lists('id');
        $combined->save();

        $combined->combinationOf->each(
            function ($i) {
                $i->combined_to = null;
                $i->save();
            }
        );

        $instruction = $combined->combinationOf->first();

        //TODO send for approval

        $combined->delete();

        \Flash::message('The instruction has been disscolved to it\'s individual components');

        return \redirect("/dashboard/clients/bank-instructions/show/$instruction->id");
    }
}
