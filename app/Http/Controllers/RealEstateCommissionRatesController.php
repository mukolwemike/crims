<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\ClientTransactionApproval;

/**
 * Class RealEstateCommissionRatesController
 */
class RealEstateCommissionRatesController extends Controller
{
    /**
     * @var \Cytonn\Realestate\Commissions\RealEstateCommissionRateRepository
     */
    protected $realEstateCommissionRateRepository;

    /**
     *
     */
    public function __construct()
    {
        $this->realEstateCommissionRateRepository =
            new \Cytonn\Realestate\Commissions\RealEstateCommissionRateRepository();

        parent::__construct();
    }


    /**
     * @return mixed
     */
    public function postRate()
    {
        $data = array_except(\Request::all(), ['_token']);

        ClientTransactionApproval::make(null, 'save_real_estate_commission_rate', $data);

        \Flash::message('The rate has been saved for approval');

        return \Redirect::back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function putRate($id)
    {
        $data = array_except(\Request::all(), ['_token']);

        $data['rate_id'] = $id;

        ClientTransactionApproval::make(null, 'save_real_estate_commission_rate', $data);

        \Flash::message('The rate has been saved for approval');

        return \Redirect::back();
    }

    //    /**
    //     * @param $id
    //     * @return mixed
    //     */
    //    public function deleteRate($id)
    //    {
    //        $this->realEstateCommissionRateRepository->deleteRate($id);
    //
    //        \Flash::message('The rate has been deleted');
    //
    //        return \Redirect::back();
    //    }
}
