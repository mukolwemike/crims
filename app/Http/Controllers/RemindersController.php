<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\PasswordReminder;
use App\Cytonn\Models\PasswordReminderRequest;
use App\Cytonn\Models\User;
use Cytonn\Authentication\Forms\PasswordReminderForm;
use Cytonn\Forms\CreatePassWordForm;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

/**
 * Class RemindersController
 */
class RemindersController extends Controller
{

    /**
     * @var PasswordReminderForm
     */
    protected $reminderForm;

    /**
     * @var CreatePassWordForm
     */
    protected $createPassWordForm;


    /**
     * @param PasswordReminderForm $reminderForm
     * @param CreatePassWordForm $createPassWordForm
     */
    public function __construct(PasswordReminderForm $reminderForm, CreatePassWordForm $createPassWordForm)
    {
        $this->reminderForm = $reminderForm;

        $this->createPassWordForm = $createPassWordForm;
    }


    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind()
    {
        return view('password.remind');
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind()
    {
        if (strlen(trim(Request::get('username'))) == 0 && strlen(trim(Request::get('email'))) == 0) {
            \Flash::error('Please fill at least one field before submitting');

            return Redirect::back();
        }

        //if email is set
        if (strlen(trim(Request::get('email'))) != 0) {
            $this->reminderForm->validate(Request::all());
        }

        $reminder = new PasswordReminderRequest();

        $reminder->fill(Request::only('username', 'email'));

        $reminder->save();

        (new \Cytonn\Mailers\UserMailer())
            ->informAdminOfPasswordRequest(Request::get('username'), Request::get('email'));

        \Flash::success('Password reset request received, we will review the request and send you a reset link.');

        return redirect('/login');
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string $token
     * @return Response
     */
    public function sendresetLink($id)
    {
        $id = Crypt::decrypt($id);

        $user = User::findOrFail($id);

        $userRepo = new \Cytonn\Users\UserRepository();
        $userRepo->user = $user;
        $userRepo->sendPasswordResetLink();

        \Flash::success('Password Reset link sent successfully');

        return Redirect::back();
    }

    /**
     * Display form to reset a users password
     */
    public function getReset($username, $token)
    {
        $user = User::where('username', $username)->first();

        if (is_null($user)) {
            \Flash::message('There\'s no such user, check that the link is correct');

            return redirect('/login_error');
        }

        $reminder = PasswordReminder::where('email', $user->email)->latest()->first();

        $correct = Hash::check($token, $reminder->token);

        if (!$correct) {
            \Flash::error(
                'We could not reset your password, please check that the link is correct or request admin asssistance'
            );

            return redirect('/login_error');
        }

        //correct
        $expiry = config('auth.passwords.users.expire');

        $time_sent = new \Carbon\Carbon($reminder->created_at);

        $time_elapsed = $time_sent->copy()->diffInMinutes(\Carbon\Carbon::now());

        if ($time_elapsed > $expiry) { //link expired
            \Flash::error(
                'The link has expired. Please request a new one. Password Reset links expire in ' . $expiry .
                ' minutes.'
            );

            return redirect('/login_error');
        }

        //reset password
        return view('password.reset', ['title' => 'Reset password', 'username' => $username, 'token' => $token]);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset($username, $token)
    {
        $user = User::where('username', $username)->first();

        if (is_null($user)) {
            \Flash::message('There\'s no such user, check that the link is correct');

            return redirect('/login_error');
        }

        $reminder = PasswordReminder::where('email', $user->email)->latest()->first();

        $correct = Hash::check($token, $reminder->token);

        if (!$correct) {
            \Flash::error(
                'We could not reset your password, please check that the link is correct or request admin asssistance'
            );

            return redirect('/login_error');
        }

        $this->createPassWordForm->validate(Request::all());

        $user->password = Request::get('password');
        $user->login_attempt = 0;

        $user->save();

        \Flash::message('Password has been reset successfully, you can now login');

        return redirect('/login');
    }
}
