<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\BankInstruction;
use Cytonn\Api\Transformers\PaymentInstructionTransformer;

/**
 * Class PaymentInstructionsController
 *
 * @package Api
 */
class PaymentInstructionsController extends ApiController
{
    /**
     * Generate the LOOs grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new BankInstruction(),
            new PaymentInstructionTransformer(),
            function ($model) {
                $model = $model->whereNull('investment_id')->whereHas(
                    'custodialTransaction',
                    function ($transaction) {
                        $transaction->whereHas(
                            'clientPayment',
                            function ($payment) {
                            }
                        );
                    }
                );
                $model = $this->searchByClientCode($model);
                $model = $this->filterByDates($model);
                $model = $this->searchByCustodialAccount($model);
                $model = $this->searchByProduct($model);
                $model = $this->searchByFund($model);
                return $this->searchByProject($model);
            }
        );
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function searchByClientCode($instruction)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $instruction->whereHas(
                'custodialTransaction',
                function ($trans) use ($code) {
                    $trans->whereHas(
                        'clientPayment',
                        function ($payment) use ($code) {
                            $payment->whereHas(
                                'client',
                                function ($client) use ($code) {
                                    $client->where('client_code', 'like', '%' . $code . '%');
                                }
                            );
                        }
                    );
                }
            );
        }
        return $instruction;
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function searchByProject($instruction)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['project'])) {
            $project = $state['search']['predicateObject']['project'];
            return $instruction->whereHas(
                'custodialTransaction',
                function ($trans) use ($project) {
                    $trans->whereHas(
                        'clientPayment',
                        function ($payment) use ($project) {
                            $payment->where('project_id', $project);
                        }
                    );
                }
            );
        }
        return $instruction;
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function searchByProduct($instruction)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['product'])) {
            $product = $state['search']['predicateObject']['product'];
            return $instruction->whereHas(
                'custodialTransaction',
                function ($trans) use ($product) {
                    $trans->whereHas(
                        'clientPayment',
                        function ($payment) use ($product) {
                            $payment->where('product_id', $product);
                        }
                    );
                }
            );
        }
        return $instruction;
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function searchByFund($instruction)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['fund'])) {
            $fund = $state['search']['predicateObject']['fund'];
            return $instruction->whereHas(
                'custodialTransaction',
                function ($trans) use ($fund) {
                    $trans->whereHas(
                        'clientPayment',
                        function ($payment) use ($fund) {
                            $payment->where('unit_fund_id', $fund);
                        }
                    );
                }
            );
        }
        return $instruction;
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function searchByCustodialAccount($instruction)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['account'])) {
            $account = $state['search']['predicateObject']['account'];
            return $instruction->whereHas(
                'custodialTransaction',
                function ($trans) use ($account) {
                    $trans->where('custodial_account_id', $account);
                }
            );
        }
        return $instruction;
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function filterByDates($instruction)
    {
        $state = \Request::get('tableState');
        $startDate = null;
        $endDate = null;

        if (isset($state['search']['predicateObject']['date'])) {
            $startDate = isset($state['search']['predicateObject']['date']['after'])
                ? $state['search']['predicateObject']['date']['after'] : null;
        }
        if (isset($state['search']['predicateObject']['date'])) {
            $endDate = isset($state['search']['predicateObject']['date']['before'])
                ? $state['search']['predicateObject']['date']['before'] : null;
        }

        return $instruction->betweenDates($startDate, $endDate);
    }
}
