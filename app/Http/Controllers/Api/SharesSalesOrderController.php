<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Api\Transformers\SharesSalesOrderTransformer;

/**
 * Class SharesSalesOrderController
 *
 * @package Api
 */
class SharesSalesOrderController extends ApiController
{
    /**
     * Generate the sales grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new SharesSalesOrder(),
            new SharesSalesOrderTransformer(),
            function ($sales) {
                $sales = $this->filterByCancelled($sales);
                return $this->filterByMatch($sales)->orderBy('request_date', 'DESC');
            }
        );
    }

    /**
     * @param $share_holder_id
     * @return string
     */
    public function shareholderSales($share_holder_id)
    {
        return $this->processTable(
            new SharesSalesOrder(),
            new SharesSalesOrderTransformer(),
            function ($model) use ($share_holder_id) {
                $model = $this->filterByMatch($model)->where('seller_id', $share_holder_id);
                $model = $this->filterByCancelled($model)->where('seller_id', $share_holder_id);
                return $model->where('seller_id', $share_holder_id);
            }
        );
    }

    /**
     * @param $sales
     * @return mixed
     */
    private function filterByMatch($sales)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['matched'])) {
            $state = $state['search']['predicateObject']['matched'];
            switch ($state) {
                case 'matched':
                    return $sales->where('matched', 1);
                    break;
                case 'unmatched':
                    return $sales->whereNull('matched')->orWhere('matched', 0);
                    break;
                default:
                    return $sales;
            }
        }
        return $sales;
    }

    /**
     * @param $sales
     * @return mixed
     */
    private function filterByCancelled($sales)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['cancelled'])) {
            $state = $state['search']['predicateObject']['cancelled'];
            switch ($state) {
                case 'cancelled':
                    return $sales->where('cancelled', 1);
                    break;
                case 'active':
                    return $sales->whereNull('cancelled')->orWhere('cancelled', 0);
                    break;
                default:
                    return $sales;
            }
        }
        return $sales;
    }

    public function store()
    {
        $input = request()->all();

        $order = SharesSalesOrder::findOrFail($input['sales_order_id']);

        ClientTransactionApproval::add(
            [
                'client_id' => $order->seller->client_id,
                'transaction_type' => 'settle_sale_to_purchase',
                'payload' => $input
            ]
        );

        \Flash::message('Settlement for shares purchases has been saved for approval');

        return redirect('/dashboard/shares/sales/shareholders/' . $order->seller_id);
    }

    public function unmatch()
    {
        $input = request()->all();

        $order = SharesSalesOrder::findOrFail($input['sales_order_id']);

        ClientTransactionApproval::add(
            [
                'client_id' => $order->seller->client_id,
                'transaction_type' => 'unmatch_sale_order_purchases',
                'payload' => $input
            ]
        );

        \Flash::message('Unmatching of shares purchases has been saved for approval');

        return redirect('/dashboard/shares/sales/shareholders/' . $order->seller_id);
    }
}
