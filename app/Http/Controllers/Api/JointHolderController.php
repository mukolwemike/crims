<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 15/03/2018
 * Time: 09:17
 */

namespace App\Http\Controllers\Api;

use Cytonn\Rules\JointHolderRules;

class JointHolderController extends ApiController
{
    use JointHolderRules;

    public function validateJointHolder()
    {
        $validator = $this->jointHolder(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        return response(['status' => 202, 'message' => 'Data successfully validated']);
    }
}
