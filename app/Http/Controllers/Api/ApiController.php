<?php
/**
 * Date: 27/01/2016
 * Time: 8:21 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\DataTables\SortFilterPaginateInterface;
use Cytonn\Api\DataTables\SortFilterPaginateTrait;
use League\Fractal\Manager;
use \Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

use App\Http\Controllers\Controller;

abstract class ApiController extends Controller implements SortFilterPaginateInterface
{
    use SortFilterPaginateTrait;
    //
    protected $fractal;
    /**
     * @var int
     */
    private $statusCode = SymfonyResponse::HTTP_OK;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->fractal = new Manager();

        parent::__construct();
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param $data
     * @param array $headers
     * @return Response
     */
    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param string $message
     * @return Response
     */
    public function respondWithError($message = 'Not Found')
    {
        return $this->respond(
            [
            'error'=>[
                'message'=>$message,
                'status_code'=>$this->getStatusCode()
            ]
            ]
        );
    }

    /**
     * @param string $message
     * @return Response
     */
    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(SymfonyResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Response
     */
    public function respondInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }
}
