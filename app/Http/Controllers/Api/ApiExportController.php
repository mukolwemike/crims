<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\InterestPaymentSchedule;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Reporting\ClientDeductionsExcelGenerator;
use Cytonn\Reporting\InterestPaymentExcelGenerator;

class ApiExportController extends Controller
{
    public function exportInterestPayments()
    {
        $state = \Request::all();
        //        $state = json_decode($state['search']);
        //        $date = $state->predicateObject->date;

        \Log::info($state);

        $start = (new Carbon('2016-08-08'))->startOfMonth()->addDay();
        $end = (new Carbon('2016-08-08'))->endOfMonth()->addDay();
        //filter
        if (isset($state['search'])) {
            if (isset($state['search']['predicateObject'])) {
                $state = json_decode($state['search']);
                $date = $state->predicateObject->date;
                if (isset($date->before) && isset($date->after)) {
                    $start = new Carbon($date->after);
                    $end = new Carbon($date->before);
                } elseif (isset($date->before)) {
                    $end = (new Carbon($date->before));
                    $start = $end->copy()->startOfMonth();
                } elseif (isset($date->after)) {
                    $start = new Carbon($date->after);
                    $end = $start->copy()->endOfMonth();
                }
            }
        }

        $schedule = (new InterestPaymentSchedule());

        $filtered_grouped_by_interest_action = $schedule
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('description', 'not like', '%maturity%')
            ->whereHas(
                'investment',
                function ($q) use ($end) {
                    $q->where('interest_payment_interval', '>', 0)
                        ->where('withdrawn', '!=', 1);
                    //                    ->
                //  orWhere(function ($q) use ($end) {
                //  $q->where('withdrawn', 1)->where('withdrawal_date', '<=', $end->toDateString());
                //  });
                }
            )
            ->get()
            ->each(
                function ($schedule) {
                    $schedule->interest_action_id = $schedule->investment->interest_action_id;
                }
            )
            ->groupBy(
                function ($item, $key) {
                    $key = $item->interest_action_id;
                    if (is_null($key)) {
                        $key = InterestAction::where('slug', 'send_to_bank')->first()->id;
                    }
                    return $key;
                }
            );

        if ($filtered_grouped_by_interest_action->count()) {
            $file = (new InterestPaymentExcelGenerator($end->copy()->endOfMonth()))
                ->excel($start, $end, $filtered_grouped_by_interest_action);
            return $file->export('xlsx');
        }
        return;
    }

    /**
     *
     */
    public function exportDeductions()
    {
        $state = \Request::all();
        \Log::info($state);

        $start = (new Carbon('2016-08-08'))->startOfMonth()->addDay();
        $end = (new Carbon('2016-08-08'))->endOfMonth()->addDay();

        //filter
        if (isset($state['search'])) {
            $state = json_decode($state['search']);
            $date = $state->predicateObject->date;
            if (isset($date->before) && isset($date->after)) {
                $start = new Carbon($date->after);
                $end = new Carbon($date->before);
            } elseif (isset($date->before)) {
                $end = (new Carbon($date->before));
                $start = $end->copy()->startOfMonth();
            } elseif (isset($date->after)) {
                $start = new Carbon($date->after);
                $end = $start->copy()->endOfMonth();
            }
        }

        $deduction = (new ClientInvestment());

        $filtered = $deduction
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('description', 'not like', '%maturity%')
            ->whereHas(
                'investmentType',
                function ($q) {
                    $q->where('name', 'partial_withdraw');
                }
            );

        $file =
            (new ClientDeductionsExcelGenerator($end->copy()->endOfMonth()))->excel($start, $end, $filtered->get());
        return $file->export('xlsx');
    }
}
