<?php

namespace App\Http\Controllers\Api\Loyalty;

use Cytonn\Rules\Rules;

trait ClientLoyaltyRules
{
    use Rules;

    public function voucherCreate($request)
    {
        $rules = [
            'name' => 'required',
            'value' => 'required|numeric',
            'number' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
