<?php

namespace App\Http\Controllers\Api\Loyalty;

use App\Cytonn\Api\Transformers\Clients\ClientLoyaltyPointsTransformer;
use App\Cytonn\Api\Transformers\Clients\LoyaltyRedeemInstructionsTransformer;
use App\Cytonn\Api\Transformers\Clients\VoucherTransformer;
use App\Cytonn\Clients\ClientLoyaltyPoints\LoyaltyRedeemInstructionsRepository;
use App\Cytonn\Mailers\Client\LoyaltyPointsRedeemMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Models\LoyaltyValues;
use App\Cytonn\Models\RewardVoucher;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientsLoyaltyController extends Controller
{
    use AlternateSortFilterPaginateTrait, ClientLoyaltyRules;

    public function index()
    {
        return $this->sortFilterPaginate(
            new Client(),
            [],
            function ($client) {
                return app(ClientLoyaltyPointsTransformer::class)->transform($client);
            },
            function ($model) {
                return $model->orderBy('id')->forFundManager();
            }
        );
    }

    public function details($id)
    {
        $client = Client::findOrFail($id);

        $details = (new LoyaltyRedeemInstructionsRepository())->getCalculatedDetails($client);

        $details['clientCode'] = $client->client_code;

        $details['client'] = ClientPresenter::presentFullNames($client->id);

        return response(['fetched' => true, 'status' => 202, 'details' => $details]);
    }

    public function loyaltyValues()
    {
        return $this->paginate(
            new LoyaltyValues(),
            function ($val) {
                return [
                    'id' => $val->id,
                    'duration' => $val->duration,
                    'value' => $val->value,
                ];
            }
        );
    }

    public function loyaltyVouchers()
    {
        return $this->sortFilterPaginate(
            new RewardVoucher(),
            [],
            function ($val) {
                return app(VoucherTransformer::class)->transform($val);
            },
            function ($model) {
                return $model->where('number', '>', 0);
            }
        );
    }

    public function processVoucher()
    {
        $validator = $this->voucherCreate(request());

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        $input = request()->all();

        isset($input['id']) ? $this->editVoucher($input) : $this->saveVoucher($input);

        return response(['created' => true, 'status' => 202]);
    }

    public function saveVoucher($input)
    {
        $file = isset($input['file']) ? $input['file'] : null;

        $voucher = new RewardVoucher();
        $voucher->name = $input['name'];
        $voucher->value = (int)$input['value'];
        $voucher->number = (int)$input['number'];
        $voucher->document_id = $this->saveDoc($file);

        $voucher->save();
    }

    public function editVoucher($input)
    {
        $file = isset($input['file']) ? $input['file'] : null;

        $voucher = RewardVoucher::findOrFail($input['id']);

        $voucher->update(
            [
                'name' => $input['name'],
                'value' => (int)$input['value'],
                'number' => $input['number'],
                'document_id' => $this->saveDoc($file)
            ]
        );
    }

    /**
     * @param $file
     * @return mixed|null
     */
    public function saveDoc($file)
    {
        $document = $file ? Document::make(file_get_contents($file), $file->getClientOriginalExtension(), 'voucher') : null;

        return $document ? $document->id : null;
    }

    public function getVoucherDetails($id)
    {
        $voucher = RewardVoucher::findOrFail($id);

        $voucher = [
            'name' => $voucher->name,
            'value' => $voucher->value,
            'number' => $voucher->number,
            'document' => $voucher->document_id ? $voucher->document_id : null,
        ];

        return response(['fetched' => true, 'status' => 202, 'details' => $voucher]);
    }

    public function deleteVoucher($voucherId)
    {
        $voucher = RewardVoucher::findOrFail($voucherId);

        if ($voucher->used) {
            return response([
                'errors' => 'Unable to delete voucher as its being redeemed',
                'created' => false,
                'status' => 422
            ]);
        }

        $voucher->delete();

        return response(['deleted' => true, 'status' => 202]);
    }

    /**
     * @param Request $request
     * @return ResponseFactory|Response
     */
    public function redeemVoucher(Request $request)
    {
        $data = $request->all();

        $client = Client::findOrFail($data['client_id']);

        $clientPoints = $client->calculateLoyalty()->getTotalPoints();
        $redeemableSum = $this->getRedeemableSum($data);

        if ($redeemableSum > $clientPoints) {
            return response([
                'errors' => 'Redeemable points are more than the client total points',
                'created' => false,
                'status' => 422
            ]);
        }

        $pendingInstruction = LoyaltyPointsRedeemInstructions::where('client_id', $client->id)
            ->whereNull('approval_id')
            ->exists();

        if ($pendingInstruction) {
            return response([
                'errors' => 'There is a pending request to redeem loyalty points',
                'created' => false,
                'status' => 422
            ]);
        } else {
            $instruction = app(LoyaltyPointsRedeemInstructions::class)->repo->saveInstruction($data, $redeemableSum);

            $input['instruction_id'] = $instruction->id;

            $approval = ClientTransactionApproval::add(
                [
                    'client_id' => $client->id,
                    'transaction_type' => 'redeem_loyalty_points',
                    'payload' => $input
                ]
            );

            (new LoyaltyPointsRedeemMailer())->notifyForRequest($client, $instruction);

            return response(['created' => true, 'approvalId' => $approval->id, 'status' => 200]);
        }
    }

    private function getRedeemableSum($data)
    {
        $voucher_ids = $data['voucher_ids'];

        $sum = 0;
        foreach ($voucher_ids as $id) {
            $voucher = RewardVoucher::find($id);

            $sum += $voucher->value;
        }

        return $sum;
    }

    public function recentActivities($clientId)
    {
        return $this->sortFilterPaginate(
            new LoyaltyPointsRedeemInstructions(),
            [],
            function ($instruction) {
                return app(LoyaltyRedeemInstructionsTransformer::class)->transform($instruction);
            },
            function ($model) use ($clientId) {
                return $model->where('client_id', '=', $clientId);
            }
        );
    }
}
