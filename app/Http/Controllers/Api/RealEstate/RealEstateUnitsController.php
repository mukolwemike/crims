<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 30/10/2018
 * Time: 10:15
 */

namespace App\Http\Controllers\Api\RealEstate;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealestateUnit;
use App\Http\Controllers\Api\ApiController;
use Cytonn\Clients\ClientRepository;
use Cytonn\Mailers\System\FlaggedRiskyClientMailer;
use Cytonn\Realestate\Forms\ReservationForm;
use Cytonn\Realestate\RealEstateUnits\RealEstateUnitsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * Class RealEstateUnitsController
 * @package App\Http\Controllers\Api\RealEstate
 */
class RealEstateUnitsController extends ApiController
{
    protected $reservationForm;

    protected $repoRE;

    protected $mailer;

    /**
     * RealEstateUnitsController constructor.
     *
     * @param $reservationForm
     */
    public function __construct(ReservationForm $reservationForm)
    {
        $this->reservationForm = $reservationForm;

        $this->repoRE = new RealEstateUnitsRepository();

        $this->mailer = new FlaggedRiskyClientMailer();

        parent::__construct();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function postReserve(Request $request)
    {
        $validator = $this->repoRE->validateReserveUnitApplication(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        //check for risky clients
        if (!Arr::exists(request()->all(), 'risk_reason')) {
            $riskyClients = (new ClientApplicationRepository())->checkRiskyClients(request());

            if (!is_null($riskyClients)) {
                $this->mailer->sendFlaggedRiskyClientNotification($riskyClients);

                return response(
                    [
                        'warning' => [
                            'risk_client' => 'The client details entered belong to a client who has been deemed RISKY'
                        ],
                        'status' => 'risk'
                    ]
                );
            }
        }

        $client = null;

        if (isset($input['client_id'])) {
            $client = Client::find($request->get('client_id'));
        }

        if ($client) {
            $repo = $client->repo;
        }

        $new_client = intval($request->get('new_client'));

        if ($new_client !== 2) {
            $allow_duplicate = ($new_client == 3) ? true : false;

            $duplicate = (new ClientRepository())->duplicate(collect($request)->toArray())->exists;

            if ($duplicate && !$allow_duplicate) {
                return response(
                    [
                        'warning' => [
                            'duplicate_client' => 'The Client for this entity with these details
                             already exists!Apply as a Duplicate'
                        ],
                        'status' => 'duplicate'
                    ]
                );
            }
        }

        $unit = RealestateUnit::findorFail($request->get('unit_id'));

        $input = $request->except('_token');

        $input['existing_client'] =
            ($request->get('new_client') == 2) && Arr::exists($request->all(), 'client_id');

        $input['unit_id'] = $unit->id;

        $instruction = ClientTransactionApproval::add([
            'client_id' => (Arr::exists($request->all(), 'client_id')) ? $input['client_id'] : null,
            'transaction_type' => 'reserve_unit',
            'payload' => $input
        ]);

        return response(['status' => 202, 'transactionID' => $instruction->id]);
    }
}
