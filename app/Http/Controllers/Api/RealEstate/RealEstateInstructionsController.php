<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 04/02/2019
 * Time: 12:46
 */

namespace App\Http\Controllers\Api\RealEstate;

use App\Cytonn\Api\Transformers\RealEstate\RealEstateInstructionTransformer;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Http\Controllers\Api\ApiController;

class RealEstateInstructionsController extends ApiController
{
    public function applications()
    {
        return $this->processTable(
            new RealEstateInstruction(),
            new RealEstateInstructionTransformer(),
            function ($instruction) {
                return $instruction->latest();
            }
        );
    }
}
