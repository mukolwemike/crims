<?php
/**
 * Date: 14/03/2016
 * Time: 9:39 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Api\Transformers\Portfolio\DepositHoldingTransfomer;
use Cytonn\Api\Transformers\PortfolioApprovalsTransformer;
use League\Fractal\Resource\Collection;

/**
 * Class PortfolioInvestmentController
 *
 * @package Api
 */
class PortfolioInvestmentController extends ApiController
{

    /**
     * Display the approvals grid
     *
     * @return string
     */
    public function approvals()
    {
        return $this->processTable(
            new PortfolioTransactionApproval(),
            new PortfolioApprovalsTransformer(),
            function ($trans) {
                $trans = $this->searchByTransactionType($trans);
                $trans = $this->searchByInstitution($trans, 'approvals');
                //                $trans = $this->searchByAwaitingStage($trans);
                return $trans->where(
                    function ($q) {
                        $q->where('approved', null)->orWhere('approved', 0);
                    }
                )->latest();
            }
        );
    }

    /**
     * @param $currency_id
     * @return string
     */
    public function investments($currency_id)
    {
        return $this->processTable(
            new DepositHolding(),
            new DepositHoldingTransfomer(),
            function ($trans) use ($currency_id) {
                $trans = $this->searchByTransactionType($trans);
                $trans = $this->searchByOnCall($trans);
                $trans = $this->searchByInstitution($trans, 'investments');
                return $trans->forFundManager()
                    ->whereHas(
                        'investTransaction',
                        function ($transaction) use ($currency_id) {
                            $transaction->whereHas(
                                'custodialAccount',
                                function ($q) use ($currency_id) {
                                    $q->where('currency_id', $currency_id);
                                }
                            );
                        }
                    )->orderBy('withdrawn', 'ASC');
            }
        );
    }

    /**
     * @param $trans
     * @param $type
     * @return mixed
     */
    private function searchByInstitution($trans, $type)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['institution'])) {
            $institution_id = $state['search']['predicateObject']['institution'];

            if ($type == 'investments') {
                return $trans->whereHas(
                    'security',
                    function ($security) use ($institution_id) {
                        $security->where('portfolio_investor_id', $institution_id);
                    }
                );
            }

            //            return $trans->where('institution_id', $institution_id);
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByTransactionType($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['transaction_type'])) {
            $type = $state['search']['predicateObject']['transaction_type'];
            return $trans->where('transaction_type', $type);
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByOnCall($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['on_call'])) {
            $on_call = (bool)$state['search']['predicateObject']['on_call'];
            return $trans->where('on_call', $on_call);
        }
        return $trans;
    }

    private function searchByAwaitingStage($trans)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['awaiting_stage_id'])) {
            $stage = (int)$state['search']['predicateObject']['awaiting_stage_id'];
            return $trans->where('awaiting_stage_id', $stage);
        }
        return $trans;
    }
}
