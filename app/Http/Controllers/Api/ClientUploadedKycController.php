<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Jobs\USSD\SendMessages;
use Cytonn\Investment\ClientInstruction\ClientUploadedKycRepository;
use Cytonn\Support\Mails\Mailer;

class ClientUploadedKycController extends ApiController
{
    public function checkValidated($uuid)
    {
        $client = Client::findByUuid($uuid);

        return response([
            'validated_id' => $client->repo->checkValidatedID(),
            'validated_pin' => $client->repo->checkValidatedPIN()
        ]);
    }

    public function validateClient($uuid)
    {
        $data = request()->all();
        $client = Client::findByUuid($uuid);

        if ($data['type'] === 'id') {
            $id_number = isset($data['number']) ? isset($data['number']) : $client->id_or_passport;

            (new ClientUploadedKycRepository())->createIPRSRequest($client, $id_number);
        }

        $this->saveClientKyc($data, $client);

        return response(
            ['validated_pin' => $client->repo->checkValidatedID(), 'validated_id' => $client->repo->checkValidatedPIN()]
        );
    }

    public function requestProof($uuid)
    {
        $client = Client::findByUuid($uuid);
        $data = request()->all();

        $subject = $data['type'] === 'pin_no' ? 'Tax Pin' : 'ID Number';

        $phones = $client->getContactPhoneNumbersArray();
        $emails = $client->getContactEmailsArray();

        if ($phones) {
            foreach ($phones as $phone) {
                $data['client_id'] = $client->id;
//                dispatch(new SendMessages($phone, 'request-kyc-proof', $data));
            }
        }

        if ($emails) {
            foreach ($emails as $email) {
                $doc = $data['type'] == 'pin_no' ? 'Tax Pin' : 'ID/Passport';
//                Mailer::compose()
//                    ->from(['operations@cytonn.com' => 'Cytonn Investments'])
//                    ->to($email)
//                    ->subject($subject . ' Compliance Notification')
//                    ->view('emails.client.validation.admin_validate_kyc', [
//                        'client' => $client, 'doc' => $doc])
//                    ->send();
            }
        }

        return response(['sent' => true]);
    }

    /**
     * @param array $data
     * @param $client
     */
    protected function saveClientKyc(array $data, $client)
    {
        $kyc = $data['type'] === 'pin_no'
            ? ClientComplianceChecklist::where('slug', 'pin_individual')->first()
            : ClientComplianceChecklist::where('slug', 'id_or_passport')->first();

        $existingUploadedKyc = ClientUploadedKyc::where('client_id', $client->id)->where('kyc_id', $kyc->id)->first();

        if ($existingUploadedKyc) {
            (new ClientUploadedKycRepository())->updateKyc($data, $existingUploadedKyc, $client);
        } else {
            (new ClientUploadedKycRepository())->save($data, $kyc, $client);
        }
    }
}
