<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundBusinessConfirmationTransformer;
use Cytonn\Mailers\Unitization\BusinessConfirmationMailer;
use Cytonn\Unitization\Reporting\BusinessConfirmationGenerator;

class UnitFundBusinessConfirmationController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundPurchase(),
            [],
            function ($purchase) {
                return app(UnitFundBusinessConfirmationTransformer::class)->transform($purchase);
            },
            function ($model) {
                $filter = \request()->get('filters');

                if ($filter) {
                    $filter = \GuzzleHttp\json_decode($filter, true);

                    $model = $model->where('is_confirmation_sent', $filter);
                }

                return $model->orderBy('id', 'desc');
            }
        );
    }

    public function show($id)
    {
        $purchase = UnitFundPurchase::find($id);

        $client = $purchase->client;

        $client->name = $client->name();

        $fund = $purchase->unitFund;

        return response([
            'purchase' => $purchase,
            'client' => $client,
            'fund' => $fund,
            'status' => 200,
        ]);
    }

    public function preview($id)
    {
        $purchase = UnitFundPurchase::find($id);

        return (new BusinessConfirmationGenerator())
            ->generate($purchase, auth()->user())
            ->stream();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function sendBC($id)
    {
        $purchase = UnitFundPurchase::find($id);

        (new BusinessConfirmationMailer())->sendBC($purchase, true);

        $purchase->is_confirmation_sent = true;

        $purchase->save();

        return response(['sent' => true, 'status' => 202]);
    }
}
