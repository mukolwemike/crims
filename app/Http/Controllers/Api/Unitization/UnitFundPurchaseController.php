<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Api\Transformers\Unitization\UnitFundCommissionScheduleTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCommission;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundPurchaseTransformer;
use Cytonn\Unitization\Rules\UnitFundPurchaseRules;

class UnitFundPurchaseController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundPurchaseRules;

    public function index($holderId)
    {
        return $this->sortFilterPaginate(
            new UnitFundPurchase(),
            [],
            function ($purchase) {
                return app(UnitFundPurchaseTransformer::class)->transform($purchase);
            },
            function ($model) use ($holderId) {
                return $model->where('unit_fund_holder_id', $holderId);
            }
        );
    }

    public function store($fundId, $clientId)
    {
        $input = request()->all();

        $client = Client::findOrFail($clientId);

        $fund = UnitFund::findOrFail($fundId);

        $validator = $this->purchaseCreate(request(), $client, $fund);

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        if (count($client->applications) == 0) {
            return response([
                'errors' => 'Selected Client has no existing Application. Please create an application as Existing Client to continue',
                'status' => 423
            ]);
        }

        $input = request()->all();

        (new UnitFundInstructionsRepository())->purchase($input, $client, $fund);

//        event(new UnitFundPurchaseHasBeenMade($purchase));

        return response(['created' => true], 202);
    }

    public function show($holderId, $clientId, $id)
    {
        $purchase = UnitFundPurchase::find($id);

        $purchase = (new UnitFundPurchaseTransformer())->transform($purchase);

        $recipients = CommissionRecepient::all()->map(function ($recipient) {
            return [
                'name' => $recipient->name . ' - ' . $recipient->email,
                'id' => $recipient->id
            ];
        });

        return response([
            'fetched' => true,
            'data' => $purchase,
            'recipients' => $recipients
        ], 200);
    }

    public function destroy($holderId, $id)
    {
        $holder = UnitFundHolder::findOrFail($holderId);

        $purchase = UnitFundPurchase::find($id);

        $input['purchase_id'] = $purchase->id;

        ClientTransactionApproval::make($holder->client_id, 'reverse_unit_fund_purchase', $input);

        return response(['reversed' => true], 202);
    }

    public function reverse($fundId, $clientId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFail($clientId);

        $validator = $this->reversePurchaseMake(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        $input['client_id'] = $client->id;
        $input['unit_fund_id'] = $fund->id;
        $input['type'] = 'unit_fund_purchase';

        ClientTransactionApproval::make($client->id, 'unit_fund_reverse_transaction', $input);

        return response(['created' => true], 202);
    }

    public function updateFa()
    {
        $input = request()->all();

        $purchase = UnitFundPurchase::find($input['fund_purchase_id']);

        $client = $purchase->client;

        ClientTransactionApproval::make(
            $client->id,
            'unit_fund_purchase_commission_recipient',
            $input
        );

        return response(['created' => true], 202);
    }

    public function getCommissionPurchase($purchaseId)
    {
        $unitCommission = UnitFundCommission::where('unit_fund_purchase_id', $purchaseId)->first();

        if ($unitCommission) {
            $commissionSchedules = UnitFundCommissionSchedule::where('unit_fund_commission_id', $unitCommission->id)->get();

            $schedules = $commissionSchedules->map(function ($schedule) {
                return (new UnitFundCommissionScheduleTransformer())->transform($schedule);
            });
        } else {
            $schedules = collect([]);
        }

        return response([
            'schedules' => $schedules,
        ], 200);
    }

    public function editCommissionPurchase($scheduleId, $scheduleDate)
    {
        $schedule = UnitFundCommissionSchedule::findOrFail($scheduleId);
        $client = $schedule->purchase->client;

        $input['schedule_date'] = $scheduleDate;
        $input['schedule_id'] = $scheduleId;
        $input['old_schedule_date'] = Carbon::parse($schedule->date)->toDateString();

        ClientTransactionApproval::make(
            $client->id,
            'edit_unit_fund_commission_schedule',
            $input
        );

        return response(['created' => true], 202);
    }
}
