<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Crims\Commission\Override;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundCommissionRecipientTransformer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Facades\Response;

class UnitFundCommissionController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index($fundId)
    {
        $date = $this->getDate();

        $dates = $this->getBulkCommissionPaymentDates($date->copy());
        $start = $dates['start'];
        $end = $dates['end'];

        return $this->sortFilterPaginate(
            new CommissionRecepient(),
            [],
            function ($recipient) use ($start, $end) {
                return app(
                    UnitFundCommissionRecipientTransformer::class,
                    ['start' => $start, 'end' => $end]
                )->transform($recipient);
            },
            function ($recipient) use ($start, $end, $fundId) {
                return $recipient->whereHas(
                    'unitFundCommissions',
                    function ($commission) use ($start, $end, $fundId) {
                        return $commission->whereHas(
                            'purchase',
                            function ($purchase) use ($fundId) {
                                return $purchase->where('unit_fund_id', $fundId);
                            }
                        )->where('date', '<=', $end);
                    }
                );
            }
        );
    }

    private function getBulkCommissionPaymentDates($date)
    {
        $bulk = BulkCommissionPayment::where('start', '<=', $date)
            ->where('end', '>=', $date)
            ->first();

        if ($bulk) {
            $start = $bulk->start;
            $end = $bulk->end;
        } else {
            $start = BulkCommissionPayment::latest()->first()->end->addDay();
            $end = Carbon::today();
        }

        return ['start' => $start, 'end' => $end];
    }

    private function getDate()
    {
        $state = request()->all();

        if (isset($state['filters']['data']) && $state['filters'] != '') {
            return Carbon::parse($state['filters']['data']['date']);
        }

        return Carbon::today();
    }

    public function getCommissionTotals($recipientId, $start, $end)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $start = Carbon::parse($start);

        $end = Carbon::parse($end);

        $shareSummary = $recipient->unitizationCommissionCalculator($start, $end)->summary();

        $earned = $shareSummary->total();

        $override = $shareSummary->override();

        $data = [
            'earned' => $earned,
            'override' => $override,
            'total' => $earned + $override
        ];

        return Response::json($data);
    }

    public function getCommissionSchedules($recipientId, $start, $end)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $start = Carbon::parse($start);

        $end = Carbon::parse($end);

        $schedules = $recipient->unitizationCommissionCalculator($start, $end)->schedules()->orderBy('date')->get();

        $schedulesArray = array();

        foreach ($schedules as $schedule) {
            $purchase = $schedule->purchase;

            $data = [
                'id' => $schedule->id,
                'client_name' => ClientPresenter::presentFullNames($purchase->client_id),
                'units_num' => $purchase->number,
                'purchase_price' => $purchase->price,
                'total' => $purchase->number * $purchase->price,
                'value_date' => Carbon::parse($purchase->date)->toDateString(),
                'rate' => $schedule->unitFundCommission->rate,
                'date' => $schedule->date,
                'description' => $schedule->description,
                'amount' => $schedule->amount
            ];

            $data['combined_description'] = "Client : " . $data['client_name'] . ', Units Num : ' . $data['units_num']
                . ', Price : ' . $data['purchase_price'] . ', Date : ' . $data['value_date'];

            $schedulesArray[] = $data;
        }

        return Response::json($schedulesArray);
    }

    public function getCommissionOverrides($recipientId, $start, $end)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $start = Carbon::parse($start);

        $end = Carbon::parse($end);

        $overrideClass = (new Override($recipient, $end, 'unitization', $start));

        $overrideRate = $overrideClass->getOverrideRate();

        $recipientCalc = $recipient->unitizationCommissionCalculator($start, $end);

        $recipientSummary = $recipientCalc->summary();

        $reports = $recipientCalc->getReports();

        $totalSum = 0;

        $overrideArray = array();

        foreach ($reports as $fa) {
            $override = $recipientSummary->overrideOnRecipient($fa);

            $totalSum += $override;

            $overrideArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $recipientSummary->overrideCommission($fa),
                'override' => $override,
                'rate' => $overrideRate
            ];
        }

        return Response::json($overrideArray);
    }
}
