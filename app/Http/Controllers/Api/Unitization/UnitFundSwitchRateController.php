<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundSwitchRate;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundSwitchRateTransformer;
use Cytonn\Unitization\Rules\UnitFundSwitchRateRules;

class UnitFundSwitchRateController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundSwitchRateRules;

    /**
     * Display a listing of the resource.
     *
     * @param  $fundId
     * @return string
     */
    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundSwitchRate(),
            [],
            function ($rate) {
                return app(UnitFundSwitchRateTransformer::class)->transform($rate);
            },
            function ($rate) use ($fundId) {
                return $rate->where('unit_fund_id', $fundId)->latest();
            }
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $fundId
     * @return \Illuminate\Http\Response
     */
    public function store($fundId)
    {
        $validator = $this->switchRateCreate(request());

        $input = request()->all();
        $input['fund_id'] = $fundId;

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        ClientTransactionApproval::make(null, 'create_unit_fund_switch_rate', $input);

        return response(['created' => true], 202);
    }

    /**
     * Display the specified resource.
     *
     * @param  $fundId
     * @param  id
     * @return UnitFundSwitchRate
     */
    public function show($fundId, $id)
    {
        $rate = UnitFundSwitchRate::find($id);

        return response(['fetched' => true, 'data' => $rate], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  @param fundId
     * @param  @param id
     * @return \Illuminate\Http\Response
     */
    public function update($fundId, $id)
    {
        $rate = UnitFundSwitchRate::find($id);

        $validator = $this->switchRateUpdate(request(), $id);

        $input = request()->all();
        $input['fund_id'] = $fundId;

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        $input['switch_rate_id'] = $rate->id;

        ClientTransactionApproval::make(null, 'update_unit_fund_switch_rate', $input);

        return response(['updated' => true], 202);
    }
}
