<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundFeeChargeFrequency;
use App\Cytonn\Models\Unitization\UnitFundFeeChargeType;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundFeeTypeTransformer;
use Cytonn\Unitization\Rules\UnitFundFeeTypeRules;

class UnitFundFeeTypeController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundFeeTypeRules;
    
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundFeeType(),
            [],
            function ($type) {
                return app(UnitFundFeeTypeTransformer::class)->transform($type);
            }
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validator = $this->feeTypeCreate(request());

        $input = request()->all();

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        ClientTransactionApproval::make(null, 'create_unit_fund_fee_type', $input);

        return response(['created' => true], 202);
    }

    /**
     * Display the specified resource.
     *
     * @param  id
     * @return UnitFund
     */
    public function show($id)
    {
        $type = UnitFundFeeType::find($id);

        return response(['fetched' => true, 'data' => $type], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  @param id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $type = UnitFundFeeType::find($id);

        $validator = $this->feeTypeUpdate(request(), $id);

        $input = request()->all();

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        $input['type_id'] = $type->id;

        ClientTransactionApproval::make(null, 'update_unit_fund_fee_type', $input);

        return response(['updated' => true], 202);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getList()
    {
        $typesList = UnitFundFeeType::all()->map(
            function ($type) {
                return ['label' => $type->name, 'value' => $type->id];
            }
        )->toArray();

        return response(['fetched' => true, 'data' => $typesList], 200);
    }

    public function getFundFeePartials()
    {
        $typesList = UnitFundFeeType::all()->map(
            function ($type) {
                return ['label' => $type->name, 'value' => $type->id];
            }
        )->toArray();

        $feeChargeTypes = UnitFundFeeChargeType::all()->map(
            function ($charge) {
                return ['label' => $charge->name, 'value' => $charge->id];
            }
        )->toArray();

        $feeChargeFrequencies = UnitFundFeeChargeFrequency::all()->map(
            function ($freq) {
                return ['label' => $freq->name, 'value' => $freq->id];
            }
        )->toArray();

        return response(
            [
            'status' => 200,
            'fetched' => true,
            'typesList' => $typesList,
            'feeChargeTypes' => $feeChargeTypes,
            'feeChargeFrequencies' => $feeChargeFrequencies,
            ]
        );
    }
}
