<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/7/18
 * Time: 10:26 AM
 */

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Api\Transformers\Unitization\UnitFundFeePaymentTransformer;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFeePayment;
use App\Cytonn\Models\Unitization\UnitFundFeePaymentRecipient;
use App\Cytonn\Unitization\Rules\UnitFundFeePaymentRules;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Unitization\UnitFundRepository;

class UnitFundFeePaymentController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundFeePaymentRules;

    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundFeePayment(),
            [],
            function ($feePayment) {
                return app(UnitFundFeePaymentTransformer::class)->transform($feePayment);
            },
            function ($feePayment) use ($fundId) {
                return $feePayment->where('unit_fund_id', $fundId)->latest();
            }
        );
    }

    public function store($fundId)
    {
        $validator = $this->feePayment(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $data = request()->all();


        ClientTransactionApproval::add(
            [
                'client_id' => null,
                'transaction_type' => 'unit_fund_fee_payment',
                'payload' => $data
            ]
        );

        return response(['status' => 200, 'message' => 'Fee payment is successfully saved for approval']);
    }

    public function getRecipients()
    {
        $recipients = UnitFundFeePaymentRecipient::whereNull('deleted_at')->get();

        return response([ 'status' => 200, 'data' => $recipients ]);
    }
}
