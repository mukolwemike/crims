<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\StatementCampaignType;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundStatementCampaignTransformer;
use Cytonn\Unitization\Rules\UnitFundStatementCampaignRules;

class UnitFundStatementCampaignController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundStatementCampaignRules;

    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundStatementCampaign(),
            [],
            function ($campaign) {
                return app(UnitFundStatementCampaignTransformer::class)->transform($campaign);
            },
            function ($campaign) {
                return $campaign->latest();
            }
        );
    }

    public function getCampaigns($id)
    {
        $unitFund = UnitFund::findOrFail($id);

        return $this->sortFilterPaginate(
            new UnitFundStatementCampaign(),
            [],
            function ($campaign) use ($unitFund) {
                return app(UnitFundStatementCampaignTransformer::class)->transform($campaign);
            },
            function ($campaign) use ($unitFund) {
                return $campaign->where('unit_fund_id', $unitFund->id)
                    ->latest();
            }
        );
    }

    public function store()
    {
        $validator = $this->campaignCreate(request());

        $input = request()->all();

        $input['sender_id'] = auth()->id();

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        ClientTransactionApproval::make(
            null,
            'create_unit_fund_statement_campaign',
            $input
        );

        return response(['created' => true], 202);
    }

    public function show($id)
    {
        $campaign = UnitFundStatementCampaign::with(
            ['type', 'sender']
        )->findOrFail($id);

        return response([
            'fetched' => true,
            'data' => $campaign
        ], 200);
    }

    public function update($id)
    {
        $campaign = UnitFundStatementCampaign::find($id);

        $validator = $this->campaignUpdate(request(), $id);

        $input = request()->all();

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        $input['campaign_id'] = $campaign->id;

        ClientTransactionApproval::make(null, 'update_unit_fund_statement_campaign', $input);

        return response(['updated' => true], 202);
    }

    public function getTypesList()
    {
        $typesList = StatementCampaignType::all()->map(
            function ($type) {
                return ['label' => $type->name, 'value' => $type->id];
            }
        )->toArray();

        return response(['fetched' => true, 'data' => $typesList], 200);
    }

    public function getOpenCampaigns($fid, $cid)
    {
        $unitFund = UnitFund::findOrFail($fid);

        $client = Client::findOrFail($cid);

        $openCampaign = $unitFund->statementCampaigns->where('closed', null);

        return response($openCampaign);
    }

    public function sendCampaign($cid)
    {
        $campaign = UnitFundStatementCampaign::findOrFail($cid);

        if ($campaign->sender_id == auth()->id()) {
            return response([
                'warning' => 'The sender cannot be the person who created the campaign',
                'status' => 422
            ]);
        }

        $campaign->repo->sendCampaign();

        if ($campaign->type->slug == 'automated') {
            $campaign->close();
        }

        return response(['sent' => true], 202);
    }

    public function closeCampaign($id, $cid)
    {
        $campaign = UnitFundStatementCampaign::findOrFail($cid);

        if (!$campaign->closed) {
            $campaign->close();
        }

        return response(['closed' => true], 202);
    }
}
