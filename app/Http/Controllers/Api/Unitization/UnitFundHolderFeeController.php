<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Unitization\UnitFundHolderFee;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundHolderFeeTransformer;

class UnitFundHolderFeeController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    /**
     * Display a listing of the resource.
     *
     * @param  $fundId
     * @return string
     */
    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundHolderFee(),
            [],
            function ($holderFee) {
                return app(UnitFundHolderFeeTransformer::class)->transform($holderFee);
            },
            function ($holderFee) use ($fundId) {
                return $holderFee->whereHas(
                    'holder',
                    function ($holder) use ($fundId) {
                        return $holder->where('unit_fund_id', $fundId);
                    }
                );
            }
        );
    }
}
