<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/5/18
 * Time: 12:15 PM
 */

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Unitization\UnitFundClientRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundClientSummaryTransfomer;
use Cytonn\Core\DataStructures\Files\Excel;

class UnitFundClientController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function indexForFund($fund_id)
    {
        $fund = UnitFund::findOrFail($fund_id);

        return $this->sortFilterPaginate(
            new Client(),
            [],
            function ($client) use ($fund) {
                return app(UnitFundClientSummaryTransfomer::class)->transform($client, $fund);
            },
            function ($model) use ($fund) {
                return $model->whereHas('unitFundPurchases', function ($purchase) use ($fund) {
                    $purchase
                        ->withTrashed()
                        ->where('unit_fund_id', $fund->id);
                });
            }
        );
    }

    public function show($fundId, $clientId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFail($clientId);

        return response((new UnitFundClientRepository($client, $fund))->fundDetails());
    }

    public function exportCalculation($fundId, $id)
    {
        /** @var Client $client */
        $client = Client::findOrFail($id);

        $calc = $client->calculateFund(UnitFund::findOrFail($fundId), Carbon::today());


        $actions = $calc->getDailies()->map(function ($action) {
            $action->date = $action->date->toFormattedDateString();
            unset($action->purchases);
            unset($action->sales);

            return new EmptyModel((array)$action);
        });

        $actions= json_decode(json_encode($actions), true);

        return \Excel::create(
            'Calculations - ' . Excel::cleanSheetName($client->present()->fullName),
            function ($excel) use ($client, $actions) {
                $excel->sheet(Excel::cleanSheetName($client->present()->fullName), function ($sheet) use ($actions) {
                    $sheet->fromModel($actions);
                });
            }
        )->download('xlsx');
    }

    public function sendStatement($fundId, $clientId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFail($clientId);

        $input = request()->all();

        $date = isset($input['date']) ? Carbon::parse($input['date']) : Carbon::today();

        try {
            $start = isset($input['start_date']) ? Carbon::parse($input['start_date']) : null;
        } catch (\Exception $e) {
            $start = null;
        }


        return (new UnitFundClientRepository(
            $client,
            $fund,
            $date->copy(),
            $start
        ))->sendStatement();
    }

    public function previewStatement($fundId, $clientId, $date = null, $start = null)
    {
        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFail($clientId);

        return (new UnitFundClientRepository(
            $client,
            $fund,
            $date,
            $start
        ))->previewStatement(false);
    }

    public function balance($fundId, $clientId)
    {
        $client = Client::findOrFail($clientId);

        $fund = UnitFund::findOrFail($fundId);

        return response((new UnitFundClientRepository($client, $fund))->balance());
    }

    public function unitsOwned($fundId, $clientId, $date = null)
    {
        $input = request()->all();

        $date = isset($input['date']) ? Carbon::parse($input['date']) : Carbon::today();

        $client = Client::findOrFail($clientId);

        $fund = UnitFund::findOrFail($fundId);

        return response((new UnitFundClientRepository($client, $fund, $date))->ownedNumberOfUnits());
    }
}
