<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/18/18
 * Time: 10:51 AM
 */

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInterestSchedule;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Http\Controllers\Controller;
use Cytonn\Api\Transformers\Unitization\UnitFundInvestmentInstructionTransformer;
use Cytonn\Core\DataStructures\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UnitFundInvestmentController extends Controller
{
    public function investApplication()
    {
        $input = request()->all();

        $app = UnitFundInvestmentInstruction::findOrFail($input['instruction_id']);

        $app = $this->updateInstruction($app, $input);

        if (isset($input['client_code'])) {
            $available = Client::where('client_code', $input['client_code'])->first();

            if ($available) {
                return response(['error' => 'Client code is already used', 'status' => 405]);
            }
        }

        $approval = ClientTransactionApproval::add(
            [
                'client_id' => $app->client->id,
                'transaction_type' => 'unit_fund_investment',
                'payload' => $input
            ]
        );

        return response(['created' => true, 'approvalId' => $approval->id, 'userId' => $app->user_id], 202);
    }

    public function updateInstruction(UnitFundInvestmentInstruction $instruction, $input)
    {
        $instruction->date = $input['date'];

        $instruction->save();

        return $instruction;
    }

    public function saleApplication()
    {
        $input = request()->all();

        $app = UnitFundInvestmentInstruction::findOrFail($input['instruction_id']);

        $approval = ClientTransactionApproval::add([
            'client_id' => $app->client->id,
            'transaction_type' => 'create_unit_fund_sale',
            'payload' => $input
        ]);

        $app->update(['approval_id' => $approval->id]);

        return response(['created' => true, 'approvalId' => $approval->id, 'userId' => $app->user_id], 202);
    }

    public function transferApplication()
    {
        $input = request()->all();

        $app = UnitFundInvestmentInstruction::findOrFail($input['instruction_id']);

        $approval = ClientTransactionApproval::add(
            [
                'client_id' => $app->client->id,
                'transaction_type' => 'create_unit_fund_transfer',
                'payload' => $input
            ]
        );

        return response(['created' => true, 'approvalId' => $approval->id, 'userId' => $app->user_id], 202);
    }

    public function viewPdf($instructionId)
    {
        $app = UnitFundInvestmentInstruction::findOrFail($instructionId);
        $client = $app->client;
        $fund = $app->unitFund;

        $data = (new UnitFundInvestmentInstructionTransformer())->transform($app);

        return \PDF::loadView('investment.client-instruction.pdf.fund-instructions-pdf', [
            'app' => $app, 'client' => $client, 'fund' => $fund, 'data' => $data
        ])->stream();
    }

    public function cancelApplication()
    {
        $input = request()->all();

        $app = UnitFundInvestmentInstruction::findOrFail($input['instruction_id']);

        $approval = ClientTransactionApproval::add(
            [
                'client_id' => $app->client->id,
                'transaction_type' => 'cancel_unit_fund_instruction',
                'payload' => $input
            ]
        );

        return response(['created' => true, 'approvalId' => $approval->id, 'userId' => $app->user_id], 202);
    }

    public function editUnitFundInstruction(Request $request, $id)
    {
        $instruction = UnitFundInvestmentInstruction::findOrFail($id);

        if ($instruction->approval) {
            return app()->abort(401);
        }

        $instruction->unit_fund_id = $request->get('unit_fund_id');
        $instruction->amount = $request->get('amount');
        $instruction->number = $request->get('number');

        //TODO should go through approval first

        $instruction->save();

        return response([
            'status' => 200,
            'message' => 'Unit fund investment instruction saved successfully'
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postBulkInterest(Request $request)
    {
        $input = $request->all();

        $date = Carbon::parse($input['date']);

        $fund = UnitFund::findOrFail($input['unit_fund_id']);

        $clients = Client::whereHas('unitFundPurchases', function ($q) use ($date, $fund) {
            $q->active()->where('unit_fund_id', $fund->id);
        })->whereHas('unitFundInterestSchedules', function ($q) use ($date, $fund) {
            $q->where('unit_fund_id', $fund->id)
                ->afterDate($date)
                ->where('next_payment_date', $date);
        })->pluck('id')->toArray();

        if (count($clients) == 0) {
            return response()->json([
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'There are no clients eligible for interest payments for the specified date'
            ]);
        }

        ClientTransactionApproval::add([
            'client_id' => null,
            'transaction_type' => 'unit_fund_bulk_interest_payment',
            'payload' => [
                'clients' => $clients,
                'unit_fund_id' => $fund->id,
                'date' => $date->toDateString()
            ]
        ]);

        return response()->json([
            'status' => Response::HTTP_CREATED,
            'message' => 'The unit fund interest payments have been sent for approval'
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveInterestSchedule(Request $request)
    {
        $input = $request->all();

        if ($input['id']) {
            $schedule = UnitFundInterestSchedule::findOrFail($input['id']);

            $input['old_interest_payment_interval'] = $schedule->interest_payment_interval;

            $input['old_interest_payment_start_date'] = $schedule->interest_payment_start_date;
        } else {
            $schedule = UnitFundInterestSchedule::where('client_id', $input['client_id'])
                ->where('unit_fund_id', $input['unit_fund_id'])->exists();

            if ($schedule) {
                return response()->json([
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message' =>
                        'The client already has an existing interest schedule. Please edit it to update the details'
                ]);
            }
        }

        ClientTransactionApproval::add([
            'client_id' => $input['client_id'],
            'transaction_type' => 'store_unit_fund_interest_schedule',
            'payload' => $input
        ]);

        return response()->json([
            'status' => Response::HTTP_CREATED,
            'message' => 'The unit fund interest payment schedule has been sent for approval'
        ]);
    }

    public function viewInterestSchedule($fund_id, $client_id)
    {
        $fund = UnitFund::find($fund_id);

        $client = Client::find($client_id);

        $schedules = UnitFundInterestSchedule::where('client_id', $client->id)
            ->where('unit_fund_id', $fund->id)
            ->get()
            ->map(function ($schedule) use ($client, $fund) {
                return [
                    'id' => $schedule->id,
                    'client_name' => $client->name(),
                    'fund_name' => $fund->name,
                    'payment_interval' => $schedule->interest_payment_interval,
                    'start_date' => isNotEmptyOrNull($schedule->interest_payment_start_date) ?
                        Carbon::parse($schedule->interest_payment_start_date)->toFormattedDateString() : '',
                    'next_payment_date' => Carbon::parse($schedule->next_payment_date)->toFormattedDateString(),
                    'show_url' => $schedule->path($fund, $client),
                ];
            });

        return response()->json(['data' => $schedules]);
    }

    public function show($fund_id, $client_id, $id)
    {
        $fund = UnitFund::find($fund_id);

        $client = Client::find($client_id);

        $client = [
            'id' => $client->id,
            'client_name' => $client->name(),
            'client_code' => $client->client_code,
            'email' => $client->contact->email,
        ];

        $schedule = UnitFundInterestSchedule::findOrFail($id);

        return response()->json([
            'fund' => $fund,
            'client' => $client,
            'schedule' => $schedule
        ]);
    }
}
