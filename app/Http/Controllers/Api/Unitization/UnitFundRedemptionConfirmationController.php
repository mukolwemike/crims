<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-12
 * Time: 10:55
 */

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundRedemptionConfirmationTransformer;
use Cytonn\Mailers\Unitization\RedemptionConfirmationMailer;
use Cytonn\Unitization\Reporting\RedemptionConfirmationGenerator;

class UnitFundRedemptionConfirmationController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundSale(),
            [],
            function ($sale) {
                return app(UnitFundRedemptionConfirmationTransformer::class)->transform($sale);
            },
            function ($model) {
                $model = $this->filterSentStatus($model);

                return $model;
            }
        );
    }

    private function filterSentStatus($model)
    {
        $request = \Request::get('filters');

        $option = isset($request['sent_option']) ? $request['sent_option'] : null;

        if ($option) {
            return ($option == 'sent') ? $model->whereNotNull('is_confirmation_sent') : $model->whereNUll('is_confirmation_sent');
        }

        return $model;
    }

    public function show($id)
    {
        $sale = UnitFundSale::find($id);

        $client = $sale->client;

        $client->name = $client->name();

        $fund = $sale->fund;

        $payment = $sale->payment;

        return response([
            'sale' => $sale,
            'client' => $client,
            'fund' => $fund,
            'payment' => $payment,
            'status' => 200,
        ]);
    }

    public function preview($id)
    {
        $sale = UnitFundSale::find($id);

        return (new RedemptionConfirmationGenerator())
            ->generate($sale, auth()->user())
            ->stream();
    }

    public function sendRC($id)
    {
        $sale = UnitFundSale::find($id);

        (new RedemptionConfirmationMailer())->sendRC($sale);

        $sale->is_confirmation_sent = 1;

        $sale->save();

        return response(['sent' => true, 'status' => 202]);
    }
}
