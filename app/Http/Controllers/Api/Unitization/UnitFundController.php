<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Portfolio\FundComplianceBenchmark;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\FundComplianceBenchmarkTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundTransformer;
use Cytonn\Models\Unitization\FundCategory;
use Cytonn\Unitization\Rules\UnitFundRules;

class UnitFundController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundRules;

    public function index()
    {
        $fundManager = $this->fundManager();

        return $this->sortFilterPaginate(
            new UnitFund(),
            [],
            function ($fund) {
                return app(UnitFundTransformer::class)->transform($fund);
            },
            function ($model) use ($fundManager) {
                return $model->where('fund_manager_id', $fundManager->id);
            }
        );
    }

    public function store()
    {
        $validator = $this->fundCreate(request());

        $input = request()->all();

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        ClientTransactionApproval::make(
            null,
            'create_unit_fund',
            $input
        );

        return response(['created' => true], 202);
    }

    public function show($id)
    {
        $fund = (new UnitFundTransformer())->transform(UnitFund::find($id));

        return response(['fetched' => true, 'data' => $fund], 200);
    }

    public function update($id)
    {
        $fund = UnitFund::find($id);

        $validator = $this->fundUpdate(request(), $id);

        $input = request()->all();

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input['fund_id'] = $fund->id;

        ClientTransactionApproval::make(null, 'update_unit_fund', $input);

        return response(['updated' => true], 202);
    }

    public function getUnitFunds()
    {
        $fundManager = $this->fundManager();

        $fundsList = UnitFund::where('fund_manager_id', $fundManager->id)
            ->get()
            ->each(
                function ($fund) {
                    $fund->label = $fund->name;
                    $fund->value = $fund->id;
                }
            )
            ->toArray();

        return response(['fetched' => true, 'data' => $fundsList], 200);
    }

    public function getShareEntities()
    {
        $fundManager = $this->fundManager();

        $entityList = SharesEntity::where('fund_manager_id', $fundManager->id)
            ->get()
            ->each(
                function ($entity) {
                    $entity->label = $entity->name;
                    $entity->value = $entity->id;
                }
            )
            ->toArray();

        return response(['fetched' => true, 'data' => $entityList], 200);
    }

    public function getAllFunds()
    {
        $fundsList = UnitFund::all()
            ->each(
                function ($fund) {
                    $fund->label = $fund->name;
                    $fund->value = $fund->id;
                }
            )
            ->toArray();

        return response(['fetched' => true, 'data' => $fundsList], 200);
    }

    public function fundCategories()
    {
        $fundCategories = FundCategory::all();

        return response($fundCategories);
    }

    public function compliance($fundId)
    {
        $unitFund = UnitFund::findOrFail($fundId);

        return $this->sortFilterPaginate(
            new FundComplianceBenchmark(),
            [],
            function ($compliance) {
                return app(FundComplianceBenchmarkTransformer::class)->transform($compliance);
            },
            function ($model) use ($unitFund) {
                return $model->whereHas('fundCompliance', function ($compliance) use ($unitFund) {
                    $compliance->where('unit_fund_id', $unitFund->id);
                });
            }
        );
    }

    public function linkCompliance($id)
    {
        $fund = UnitFund::find($id);

        $input = request()->all();

        ClientTransactionApproval::make(null, 'unit_fund_compliance', $input);

        return response(['updated' => true], 200);
    }
}
