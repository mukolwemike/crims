<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Api\Transformers\Unitization\UnitFundFeePaymentTransformer;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeePayment;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundFeeTransformer;
use Cytonn\Unitization\Rules\UnitFundFeeRules;

class UnitFundFeeController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundFeeRules;

    /**
     * Display a listing of the resource.
     *
     * @param  $fundId
     * @return string
     */
    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundFee(),
            [],
            function ($fee) {
                return app(UnitFundFeeTransformer::class)->transform($fee);
            },
            function ($fee) use ($fundId) {
                return $fee->where('unit_fund_id', $fundId);
            }
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $fundId
     * @return \Illuminate\Http\Response
     */
    public function store($fundId)
    {
        $validator = $this->feeCreate(request());

        $input = request()->all();
        $input['fund_id'] = $fundId;

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        ClientTransactionApproval::make(null, 'create_unit_fund_fee', $input);

        return response(['created' => true], 202);
    }

    /**
     * Display the specified resource.
     *
     * @param  $fundId
     * @param  id
     * @return UnitFundFee
     */
    public function show($fundId, $id)
    {
        $fee = UnitFundFee::find($id);

        $fee = (new UnitFundFeeTransformer())->transform($fee);

        return response()->json($fee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  @param fundId
     * @param  @param id
     * @return \Illuminate\Http\Response
     */
    public function update($fundId, $id)
    {
        $fee = UnitFundFee::find($id);

        $validator = $this->feeUpdate(request(), $id);

        $input = request()->all();
        $input['fund_id'] = $fundId;

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input['fee_id'] = $fee->id;

        ClientTransactionApproval::make(null, 'update_unit_fund_fee', $input);

        return response(['updated' => true], 202);
    }

    public function addFeeParameter($id)
    {
        $fee = UnitFundFee::find($id);

        $validator = $this->feePercetage(request());

        $input = request()->all();

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        ClientTransactionApproval::make(null, 'add_unit_fund_fee_percentage', $input);

        return response([ 'updated' => true, 'status' => 202 ]);
    }
}
