<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundClientSummaryTransfomer;
use Cytonn\Api\Transformers\Unitization\UnitFundHolderSummaryTransformer;

class UnitFundSummaryController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index($fundId)
    {
        $fund = UnitFund::findOrFail($fundId);

        return $this->sortFilterPaginate(
            new Client(),
            [],
            function ($client) use ($fund) {
                return app(UnitFundClientSummaryTransfomer::class)->transform($client, $fund);
            },
            function ($model) use ($fund) {
                return $model->whereHas('unitFundPurchases', function ($purchase) use ($fund) {
                    $purchase
                        ->whereNull('deleted_at')
                        ->where('unit_fund_id', $fund->id);
                });
            }
        );
    }
}
