<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPerformance;
use App\Cytonn\Models\Unitization\UnitFundPriceTrail;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundPriceTrailTransformer;
use Cytonn\Unitization\Rules\UnitFundPriceTrailRules;
use Cytonn\Unitization\Trading\Performance;
use Illuminate\Http\Request;

class UnitFundPriceTrailController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundPriceTrailRules;

    /**
     * Display a listing of the resource.
     *
     * @param  $fundId
     * @return string
     */
    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundPerformance(),
            [],
            function ($trail) {
                return app(UnitFundPriceTrailTransformer::class)->transform($trail);
            },
            function ($trail) use ($fundId) {
                return $trail->where('unit_fund_id', $fundId)->latest('date');
            }
        );
    }

    public function indexForChart($fundId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $calc = $fund->category->calculation->slug;

        $key = 'utf_prices_chart'.$fundId;

        if (cache()->has($key)) {
            $trails = cache()->get($key);
        } else {
            $trails = $fund->performances()->oldest('date')
//            ->take('100')
                ->get()
                ->map(function ($trail) use ($calc) {
                    $date = Carbon::parse($trail->date)->toDateString();

                    $price = $trail->price;

                    if ($calc == 'daily-yield') {
                        $price = $trail->net_daily_yield;
                    }

                    return [$date, (double) $price];
                });

            cache()->put($key, $trails, Carbon::now()->addHours(6));
        }
        
        return response(['fetched' => true, 'data' => $trails], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $fundId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $fundId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $date = Carbon::parse($request->get('date'));

        $performance = new Performance($fund, $date);

        $aum = $performance->aum();

        return response()->json([
            'data' => [
                'fund'          => $fund->name,
                'fund_calculator' => $fund->category->calculation->slug,
                'date'          => $date->toDateString(),
                'assets'        =>  $performance->assetMarketValue(),
                'cash'          =>  $performance->cashBalance(),
                'aum'           =>  $aum,
                'liabilities'   =>  $performance->liabilities(),
                'nav'           =>  $performance->nav(),
                'total_units'   =>  $performance->totalUnits(),
                'price'         =>  $performance->unitPrice(),
                'yield'         => $performance->netDailyYield()
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  $fundId
     * @param  id
     * @return UnitFundPriceTrail
     */
    public function show($fundId, $id)
    {
        $trail = UnitFundPriceTrail::find($id);

        return response(['fetched' => true, 'data' => $trail], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  @param fundId
     * @param  @param id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $fundId, $date)
    {

        $input = $request->except(['_token', '_method']);

        $input['fund_id'] = $fundId;
        $input['date'] = $date;

        ClientTransactionApproval::make(null, 'create_unit_fund_price_trail', $input);

        return response(['created' => true], 202);
    }
}
