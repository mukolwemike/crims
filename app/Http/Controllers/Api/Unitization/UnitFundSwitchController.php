<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundSwitch;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundSwitchTransformer;
use Cytonn\Unitization\Rules\UnitFundSwitchRules;

class UnitFundSwitchController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundSwitchRules;

    public function indexAll()
    {
        return $this->sortFilterPaginate(
            new UnitFundSwitch(),
            [],
            function ($switch) {
                return app(UnitFundSwitchTransformer::class)->transform($switch);
            }
        );
    }

    public function store($fundId, $clientId)
    {
        $validator = $this->switchCreate(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        $client = Client::findOrFail($clientId);

        $fund = UnitFund::findOrFail($fundId);

        $input['client_id'] = $client->id;

        $input['fund_id'] = $fund->id;

        ClientTransactionApproval::make($client->id, 'create_unit_fund_switch', $input);

        return response(['created' => true, 'status' => 202]);
    }

    public function show($holderId, $id)
    {
        $switch = UnitFundSwitch::with(
            [
            'holder.client.contact.title', 'previousFund', 'currentFund', 'rate'
            ]
        )->findOrFail($id);

        return response(['fetched' => true, 'data' => $switch], 200);
    }
}
