<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCampaignStatementItem;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundCampaignStatementItemTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundHolderTransformer;
use Cytonn\Unitization\Rules\UnitFundCampaignStatementItemRules;

class UnitFundCampaignStatementItemController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundCampaignStatementItemRules;

    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundCampaignStatementItem(),
            [],
            function ($item) {
                return app(UnitFundCampaignStatementItemTransformer::class)->transform($item);
            },
            function ($item) {
                return $item->latest();
            }
        );
    }

    public function indexOfCampaign($id, $cid)
    {
        $fund = UnitFund::findOrFail($id);

        $campaign = UnitFundStatementCampaign::findOrFail($cid);

        return $this->sortFilterPaginate(
            new UnitFundCampaignStatementItem(),
            [],
            function ($item) {
                return app(UnitFundCampaignStatementItemTransformer::class)->transform($item);
            },
            function ($item) use ($campaign) {
                return $item->latest()->where('unit_fund_statement_campaign_id', $campaign->id);
            }
        );
    }

    public function missingClients($id, $cid)
    {
        $unitFund = UnitFund::findorFail($id);

        $campaign = UnitFundStatementCampaign::findOrFail($cid);

        $clientIds = UnitFundCampaignStatementItem::where('unit_fund_statement_campaign_id', $campaign->id)
            ->get(['client_id'])
            ->lists('client_id');

        return $this->sortFilterPaginate(
            new Client(),
            [],
            function ($client) use ($unitFund) {
                return app(UnitFundHolderTransformer::class)->transform($client, $unitFund);
            },
            function ($client) use ($clientIds) {
                return $client->has('unitFundPurchases')->whereNotIn('id', $clientIds);
            }
        );
    }

    public function store()
    {
        $validator = $this->itemCreate(request());

        $input = request()->all();

        $item = UnitFundStatementCampaign::findOrFail($input['unit_fund_statement_campaign_id']);

        $client = Client::findOrFail(request('client_id'));

        if ($item->repo->checkIfHolderIsInCampaign($client)) {
            return response([
                'warning' => 'The unit fund client already exists in the campaign',
                'status' => 422
            ]);
        }

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $item->repo->addClientToCampaign($client, auth()->user());

        return response(['created' => true], 202);
    }
}
