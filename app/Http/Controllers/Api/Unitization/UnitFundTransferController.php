<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundTransferTransformer;
use Cytonn\Unitization\Rules\UnitFundTransferRules;

class UnitFundTransferController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundTransferRules;

    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundTransfer(),
            [],
            function ($transfer) {
                return app(UnitFundTransferTransformer::class)->transform($transfer);
            }
        );
    }

    /**
     * @param $fundId
     * @param $clientId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function store($fundId, $clientId)
    {
        $client = Client::findOrFail($clientId);

        $fund = UnitFund::findorFail($fundId);

        $validator = $this->transferCreate(request(), $client, $fund);

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input = request()->all();

        $input['transferer_id'] = $client->id;

        $input['unit_fund_id'] = $fund->id;

        $instruction = (new UnitFundInstructionsRepository())->transfer($client, $fund, $input);

        $approval = ClientTransactionApproval::make($client->id, 'create_unit_fund_transfer', $input);

        $instruction->approval_id = $approval->id;

        $instruction->save();

        return response(['created' => true], 202);
    }

    public function show($senderId, $id)
    {
        $transfer = UnitFundTransfer::find($id);

        return response(['fetched' => true, 'data' => $transfer], 200);
    }

    public function destroy($senderId, $id)
    {
        $sender = UnitFundHolder::findOrFail($senderId);

        $transfer = UnitFundTransfer::find($id);

        $input['transfer_id'] = $transfer->id;

        ClientTransactionApproval::make($sender->client_id, 'reverse_unit_fund_transfer', $input);

        return response(['reversed' => true], 202);
    }

    public function reverse($fundId, $clientId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFail($clientId);

        $validator = $this->reverseTransferMake(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        $input['client_id'] = $client->id;
        $input['unit_fund_id'] = $fund->id;
        $input['type'] = 'unit_fund_transfer';

        ClientTransactionApproval::make($client->id, 'unit_fund_reverse_transaction', $input);

        return response(['created' => true], 202);
    }

    public function getFunds($fund)
    {
        $unitFund = UnitFund::findOrFail($fund);
        $fundManager = $this->fundManager();

        return UnitFund::where('fund_manager_id', $fundManager->id)
            ->where('id', '!=', $unitFund->id)
            ->get();
    }
}
