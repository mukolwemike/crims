<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Authentication\MailAuth\Auth;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\Controllers\Controller;
use App\Jobs\Unitization\UnitFundNonCompliantClients;
use App\Jobs\Unitization\UnitFundPurchasesReport;
use App\Jobs\Unitization\UnitFundSalesReport;
use App\Jobs\Unitization\UnitFundValuationReport;
use App\Jobs\Unitization\UnitTrustClientsContacts;
use App\Jobs\Unitization\UnitTrustClientSummary;
use Cytonn\Core\DataStructures\Carbon;

class UnitFundReportController extends Controller
{
    public function index()
    {
        $this->{camel_case(request('action'))}(null, []);

        return response(['sent' => true], 202);
    }

    public function clientsContacts(UnitFund $fund = null, $requests = null)
    {
        if (request('unit_fund_id') || isset($requests['unit_fund_id'])) {
            $id = isset($requests['unit_fund_id']) ? $requests['unit_fund_id'] : request('unit_fund_id');

            $fund = UnitFund::find($id);
        }

        $start = isset($requests['start']) ? $requests['start'] : request('start');
        $end = isset($requests['end']) ? $requests['end'] : request('end');

        dispatch(new UnitTrustClientsContacts(
            auth()->user(),
            $fund,
            $start,
            $end
        ));

        return true;
    }

    public function holderPurchases(UnitFund $fund = null, $requests = null)
    {
        if (request('unit_fund_id')) {
            $fund = UnitFund::find(request('unit_fund_id'));
        }

        dispatch(new UnitFundPurchasesReport($fund, auth()->user()));

        return true;
    }

    public function clientSummary(UnitFund $fund = null, $requests = null)
    {
        if (request('unit_fund_id')) {
            $fund = UnitFund::find(request('unit_fund_id'));
        }

        dispatch(new UnitTrustClientSummary(
            auth()->user(),
            $fund,
            request('end')
        ));

        return true;
    }

    public function valuation(UnitFund $fund = null, $requests = null)
    {
        if (request('unit_fund_id')) {
            $fund = UnitFund::find(request('unit_fund_id'));
        }

        dispatch(new UnitFundValuationReport($fund, auth()->user()));

        return true;
    }

    public function fundSales(UnitFund $fund = null, $requests = null)
    {
        dispatch(new UnitFundSalesReport(
            auth()->user(),
            $fund,
            request('start'),
            request('end')
        ));
    }

    /**
     * @param UnitFund|null $fund
     * @param null $requests
     */
    public function interestExpense(UnitFund $fund = null, $requests = null)
    {
        $end = request()->has('end') ? request('end') : Carbon::today()->toDateString();

        $start = request()->has('start') ? request('start') : Carbon::parse($end)->startOfMonth()->toDateString();

        $user = \Auth::id();

        $fund = request()->has('unit_fund_id') ? request('unit_fund_id') : null;

        $this->queue(function () use ($fund, $start, $end, $user) {
            \Artisan::call('utf:interest-expense', [
                'start' => $start,
                'end' => $end,
                'user_id' => $user,
                'fund_id' => $fund
            ]);
        });
    }

    public function fundPurchases()
    {
        $start = Carbon::parse(request('start'))->toDateString();

        $end = Carbon::parse(request('end'))->toDateString();

        $fundId = request('unit_fund_id');

        $user = \Auth::id();

        $this->queue(function () use ($fundId, $start, $end, $user) {
            \Artisan::call('crims:unit_fund_purchases_report', [
                'start' => $start,
                'end' => $end,
                'unit_fund' => $fundId,
                'user_id' => $user
            ]);
        });
    }

    public function fundClientMovement()
    {
        $start = Carbon::parse(request('start'))->toDateString();

        $end = Carbon::parse(request('end'))->toDateString();

        $fundId = request('unit_fund_id');

        $user = \Auth::id();

        $this->queue(function () use ($fundId, $start, $end, $user) {
            \Artisan::call('crims:unit_fund_client_movement_audit', [
                'fund' => $fundId,
                'start' => $start,
                'end' => $end,
                'user_id' => $user
            ]);
        });
    }

    public function nonCompliantClients($requests = null)
    {
        dispatch(new UnitFundNonCompliantClients(
            $this->fundManager(),
            auth()->user(),
            request('start'),
            request('end')
        ));
    }
}
