<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundDividend;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundDividendTransformer;
use Cytonn\Unitization\Rules\UnitFundDividendRules;

class UnitFundDividendController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundDividendRules;

    /**
     * Display a listing of the resource.
     *
     * @param  $fundId
     * @return string
     */
    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundDividend(),
            [],
            function ($dividend) {
                return app(UnitFundDividendTransformer::class)->transform($dividend);
            },
            function ($dividend) use ($fundId) {
                return $dividend->where('unit_fund_id', $fundId)->latest();
            }
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $fundId
     * @return \Illuminate\Http\Response
     */
    public function store($fundId)
    {
        $validator = $this->dividendCreate(request());

        $input = request()->all();
        $input['fund_id'] = $fundId;

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        ClientTransactionApproval::make(null, 'create_unit_fund_dividend', $input);

        return response(['created' => true], 202);
    }

    /**
     * Display the specified resource.
     *
     * @param  $fundId
     * @param  id
     * @return UnitFundDividend
     */
    public function show($fundId, $id)
    {
        $dividend = UnitFundDividend::find($id);

        return response(['fetched' => true, 'data' => $dividend], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  @param fundId
     * @param  @param id
     * @return \Illuminate\Http\Response
     */
    public function update($fundId, $id)
    {
        $dividend = UnitFundDividend::find($id);

        $validator = $this->dividendUpdate(request(), $id);

        $input = request()->all();
        $input['fund_id'] = $fundId;

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        $input['dividend_id'] = $dividend->id;

        ClientTransactionApproval::make(null, 'update_unit_fund_dividend', $input);

        return response(['updated' => true], 202);
    }
}
