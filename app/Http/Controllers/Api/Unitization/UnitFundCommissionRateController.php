<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Unitization\UnitFundCommissionRate;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundCommissionRateTransformer;
use Cytonn\Unitization\Rules\UnitFundCommissionRateRules;

class UnitFundCommissionRateController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundCommissionRateRules;

    /**
     * Display a listing of the resource.
     *
     * @param  $fundId
     * @return string
     */
    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundCommissionRate(),
            [],
            function ($rate) {
                return app(UnitFundCommissionRateTransformer::class)->transform($rate);
            },
            function ($rate) use ($fundId) {
                return $rate->where('unit_fund_id', $fundId)->latest('date');
            }
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $fundId
     * @return \Illuminate\Http\Response
     */
    public function store($fundId)
    {
        $validator = $this->rateCreate(request());

        $input = request()->all();
        $input['fund_id'] = $fundId;

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        ClientTransactionApproval::make(null, 'create_unit_fund_commission_rate', $input);

        return response(['created' => true], 202);
    }

    /**
     * Display the specified resource.
     *
     * @param  $fundId
     * @param  id
     * @return UnitFundCommissionRate
     */
    public function show($fundId, $id)
    {
        $rate = UnitFundCommissionRate::find($id);

        return response(['fetched' => true, 'data' => $rate], 200);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCommissionRecipientTypesList()
    {
        $types = CommissionRecipientType::all()->map(
            function ($type) {
                return ['label' => $type->name, 'value' => $type->id];
            }
        )->toArray();

        return response(['fetched' => true, 'data' => $types], 200);
    }
}
