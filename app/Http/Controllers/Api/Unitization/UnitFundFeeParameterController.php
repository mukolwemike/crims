<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundFeeParameter;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundFeeParameterTransformer;
use Cytonn\Unitization\Rules\UnitFundFeeParameterRules;

class UnitFundFeeParameterController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundFeeParameterRules;
    
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundFeeParameter(),
            [],
            function ($parameter) {
                return app(UnitFundFeeParameterTransformer::class)->transform($parameter);
            }
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validator = $this->feeParameterCreate(request());

        $input = request()->all();

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        ClientTransactionApproval::make(null, 'create_unit_fund_fee_parameter', $input);

        return response(['created' => true], 202);
    }

    /**
     * Display the specified resource.
     *
     * @param  id
     * @return UnitFund
     */
    public function show($id)
    {
        $parameter = UnitFundFeeParameter::find($id);

        return response(['fetched' => true, 'data' => $parameter], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  @param id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $parameter = UnitFundFeeParameter::find($id);

        $validator = $this->feeParameterUpdate(request(), $id);

        $input = request()->all();

        if ($validator) {
            return response(
                [
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
                ]
            );
        }

        $input['parameter_id'] = $parameter->id;

        ClientTransactionApproval::make(null, 'update_unit_fund_fee_parameter', $input);

        return response(['updated' => true], 202);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getList()
    {
        $typesList = UnitFundFeeParameter::all();

        return response(['fetched' => true, 'data' => $typesList], 200);
    }
}
