<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundHolderFee;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundFeeChargedTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundHolderFeeTransformer;

class UnitFundFeesChargedController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index($fundId)
    {
        return $this->sortFilterPaginate(
            new UnitFundFeesCharged(),
            [],
            function ($feeCharged) {
                return app(UnitFundFeeChargedTransformer::class)->transform($feeCharged);
            },
            function ($feeCharged) use ($fundId) {
                return $feeCharged->where('unit_fund_id', $fundId);
            }
        );
    }
}
