<?php

namespace App\Http\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use App\Events\Investments\Actions\UnitFundInstructionHasBeenMade;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundSaleTransformer;
use Cytonn\Unitization\Rules\UnitFundSaleRules;
use Cytonn\Unitization\Trading\Sell;

class UnitFundSaleController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundSaleRules;

    public function index()
    {
        return $this->sortFilterPaginate(
            new UnitFundSale(),
            [],
            function ($sale) {
                return app(UnitFundSaleTransformer::class)->transform($sale);
            }
        );
    }

    public function store($fundId, $clientId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFail($clientId);

        $validator = $this->saleCreate(request(), $client, $fund);

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input = request()->all();

        $withdrawableUnits = (new Sell($fund, $client, Carbon::parse($input['date'])))->withdrawableUnits();

        if ((int)$input['number'] > $withdrawableUnits) {
            return response([
                'errors' => [
                    'number' => [
                        "The units to be withdrawn (" . $input['number'] . ") are more than the withdrawable units
                         (" . (int)$withdrawableUnits . ")"
                    ]
                ],
                'status' => 422
            ]);
        }

        $input['unit_fund_id'] = $fund->id;

        $input['client_id'] = $client->id;

        (new UnitFundInstructionsRepository())->sale($client, $fund, $input);

        return response(['created' => true], 202);
    }

    public function show($holderId, $id)
    {
        $sale = UnitFundSale::find($id);

        return response(['fetched' => true, 'data' => $sale], 200);
    }

    public function destroy($holderId, $id)
    {
        $holder = UnitFundHolder::findOrFail($holderId);

        $sale = UnitFundSale::find($id);

        $input['sale_id'] = $sale->id;

        ClientTransactionApproval::make($holder->client_id, 'reverse_unit_fund_purchase', $input);

        return response(['reversed' => true], 202);
    }

    public function reverse($fundId, $clientId)
    {
        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFail($clientId);

        $validator = $this->reverseSaleMake(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        $input['client_id'] = $client->id;
        $input['unit_fund_id'] = $fund->id;
        $input['type'] = 'unit_fund_sale';

        ClientTransactionApproval::make($client->id, 'unit_fund_reverse_transaction', $input);

        return response(['created' => true], 202);
    }
}
