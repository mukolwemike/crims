<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 06/11/2018
 * Time: 11:19
 */

namespace App\Http\Controllers\Api\Client;

use App\Cytonn\Clients\RiskyClientRepository;
use App\Cytonn\Clients\Rules\RiskyClientApplicationRules;
use App\Cytonn\Models\Client;
use App\Http\Controllers\Api\ApiController;
use Cytonn\Api\Transformers\Clients\RiskyClientsTransformer;
use Cytonn\Mailers\System\FlaggedRiskyClientMailer;
use League\Fractal\Resource\Collection;

class RiskyClientsController extends ApiController
{
    use RiskyClientApplicationRules;

    protected $mailer;

    protected $transformer;

    public function __construct()
    {
        $this->mailer = new FlaggedRiskyClientMailer();

        $this->transformer = new RiskyClientsTransformer();
    }

    public function index()
    {
        $state = \Request::get('tableState');

        $filterFunction = function ($model) use ($state) {
            return $this->filterByStatus($model, $state);
        };

        return $this->processTable(
            new Client\RiskyClient(),
            new RiskyClientsTransformer(),
            $filterFunction
        );
    }

    private function filterByStatus($model, $state)
    {
        if (isset($state['search']['predicateObject']['status'])) {
            $filter = $state['search']['predicateObject']['status'];

            if ($filter) {
                $model = $model->status($filter);
            }
        }

        return $model;
    }


    public function riskyClientCreate()
    {
        $validator = $this->validateRiskyClientForm(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        if (isset($input['risk_id'])) {
            (new RiskyClientRepository())->updateRiskyClient($input);

            return response(['status' => 202, 'updated' => true]);
        }

        $riskyClient = (new RiskyClientRepository())->saveRiskyClient($input);

        $this->mailer->sendAddRiskyClientNotification($riskyClient);

        return response(['created' => true, 'status' => 202]);
    }

    public function validateRiskyClientForm($request)
    {
        return ($request->get('type') == 1)
            ? $this->validateIndividualForm($request) : $this->validateCorporateForm($request);
    }


    public function riskyClient($id)
    {
        $client = Client\RiskyClient::findOrFail($id);

        return response([
            'id' => $client->id,
            'firstname' => $client->firstname,
            'lastname' => $client->lastname,
            'email' => $client->email,
            'organization' => $client->organization,
            'date_flagged' => $client->date_flagged,
            'country_id' => $client->country_id,
            'affiliations' => $client->affiliations,
            'reason' => $client->reason,
            'risk_source' => $client->risk_source,
            'type' => $client->type,
            'risky_status_id' => $client->risky_status_id,
        ]);
    }

    public function search()
    {
        $name = request()->get('name');

        $clients = Client\RiskyClient::whereNull('risky_clients.deleted_at')
            ->search($name)
            ->where('risky_clients.deleted_at', null)
            ->paginate(20);

        $resource = new Collection($clients, $this->transformer);

        return $this->fractal->createData($resource)->toJson();
    }

    public function allRiskyClients()
    {
        $clients = Client\RiskyClient::search(request()->get('query'))->paginate(50);

        $resource = new Collection($clients, $this->transformer);

        return $this->fractal->createData($resource)->toJson();
    }

    public function riskStatus()
    {
        return (new Client\RiskyStatus())->get(['id', 'name'])->toArray();
    }
}
