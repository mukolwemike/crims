<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/20/18
 * Time: 1:51 PM
 */

namespace App\Http\Controllers\Api\Client;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Mailers\System\PaymentProofUploadedMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationContactPersons;
use App\Cytonn\Models\ClientFilledInvestmentApplicationJointHolders;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use App\Http\Controllers\Api\ApiController;
use App\Http\CrimsClient\Transformers\ClientTransformer;
use Cytonn\Api\Transformers\InvestmentApplicationTransformer;
use Cytonn\Clients\ClientRepository;
use Cytonn\Investment\Rules\InvestmentApplicationRules;
use Cytonn\Mailers\System\FlaggedRiskyClientMailer;
use Illuminate\Support\Arr;

class ClientApplicationController extends ApiController
{
    use InvestmentApplicationRules;

    protected $mailer;

    protected $repo;

    protected $repoClient;

    public function __construct()
    {
        $this->repo = new ClientApplicationRepository();
        $this->mailer = new FlaggedRiskyClientMailer();
        $this->repoClient = new ClientRepository();
    }

    public function editApplication($id)
    {
        $application = ClientFilledInvestmentApplication::findOrFail($id);

        $client = $application->client ? $application->client->name() : null;

        $contactPersons = $application->contactPersons;

        $jointHolders = $application->jointHolders;

        return response([
            'application' => $application,
            'contactPersons' => $contactPersons,
            'jointHolders' => $jointHolders,
            'kycDocuments' => $application->clientRepo->getDocumentDetails('kyc'),
            'paymentDocuments' => $application->clientRepo->getDocumentDetails('payment'),
            'emailIndemnity' => $application->clientRepo->getDocumentDetails('email_indemnity'),
            'clientSignature' => $application->clientRepo->getDocumentDetails('client_signatures'),
            'client' => $client
        ]);
    }

    public function validateInvestment()
    {
        $validator = $this->investmentDetails(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        return response(['status' => 202]);
    }

    public function validateShareholderAccountDetails()
    {
        $validator = $this->shareholderAccountDetails(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        return response(['status' => 202]);
    }

    public function saveInvestment()
    {
        $validator = $this->repo->validateEntryLevelApplication(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        //check for risky clients
        if (!Arr::exists(request()->all(), 'risk_reason')) {
            $riskyClients = $this->repo->checkRiskyClients(request());

            if (!is_null($riskyClients)) {
                $this->mailer->sendFlaggedRiskyClientNotification($riskyClients);

                return response(
                    [
                        'warning' => [
                            'risk_client' => 'The client details entered belong to a client who has been deemed RISKY'
                        ],
                        'status' => 'risk'
                    ]
                );
            }
        }

        $input = $this->repo->cleanApplicationData(request()->all());

        $data = request()->except('jointHolders', 'product_category', 'client', 'type');

        if ($data['new_client'] !== 2) {
            $allow_duplicate = ($data['new_client'] == 3) ? true : false;

            $duplicate = $this->repoClient->duplicate($input)->exists;

            if ($duplicate && !$allow_duplicate) {
                return response(
                    [
                        'warning' => [
                            'duplicate_client' => 'The details provided belong to another client! 
                            Please apply as a Duplicate'
                        ],
                        'status' => 'duplicate'
                    ]
                );
            }
        }

        if (isset($input['id'])) {
            unset($input['type']);

            $application = ClientFilledInvestmentApplication::findorFail($input['id']);

            $application = $application->clientRepo->updateApplicationInstruction($input);

            return response(['status' => 202, 'application' => $application->id]);
        }

        if (isset($data['client_id'])) {
            $data = $this->existingClientDetails($data);
        }

        if (isset($data['contact_persons'])) {
            $data['contact_persons'] = json_encode($data['contact_persons']);
        }

        $application = $this->repo->saveApplicationInstruction($data);

        $jointHolders = (isset($input['jointHolders'])) ? request()->input('jointHolders') : [];

        if ($application->uuid) {
            $application->jointHolders()->createMany($jointHolders);
        }

        return response(['status' => 202, 'application' => $application->id]);
    }

    public function existingClientDetails($input)
    {
        $client = (new Client())->findOrFail($input['client_id']);

        $client = (new InvestmentApplicationTransformer())->allDetails($client);

        return $input = array_merge($input, $client);
    }

    public function saveContactPersons()
    {
        $input = request()->all();

        if ($input['processType'] == 'reserve_unit' || $input['processType'] == 'register_shareholder') {
            $transaction = ClientTransactionApproval::findorFail($input['id']);

            unset($input['id']);

            $test = array_merge($transaction->payload, $input);

            $transaction->payload = $test;

            $transaction->save();

            return response(['status' => 202, 'transactionID' => $transaction->id]);
        }

        $application = ClientFilledInvestmentApplication::findOrFail(request()->input('id'));

        $application->clientRepo->saveContactPersons($input);

        return response(['status' => 202, 'application' => $application->id]);
    }

    public function deleteContactPersons($id)
    {
        $contactPerson = ClientFilledInvestmentApplicationContactPersons::findOrFail($id);

        $contactPerson->delete();

        return response(['deleted' => true]);
    }

    public function saveJointHolders()
    {
        $input = request()->all();

        if ($input['processType'] == 'reserve_unit' || $input['processType'] == 'register_shareholder') {
            $transaction = ClientTransactionApproval::findorFail($input['id']);

            unset($input['id']);
            unset($input['processType']);

            $test = array_merge($transaction->payload, $input);

            $transaction->payload = $test;

            $transaction->save();

            return response(['status' => 202, 'transactionID' => $transaction->id]);
        }

        $application = ClientFilledInvestmentApplication::findOrFail(request()->input('id'));

        $application = $application->clientRepo->saveJointHolders($input);

        return response(['status' => 202, 'application' => $application->id]);
    }

    public function deleteJointHolder($id)
    {
        $holder = ClientFilledInvestmentApplicationJointHolders::findorFail($id);

        $holder->delete();

        return response(['deleted' => true]);
    }

    public function getJointHolder($id)
    {
        $data = Client::findOrFail($id);

        $client = ClientTransformer::transformHolder($data);

        return response()->json($client);
    }

    public function uploadDocument()
    {
        $input = request()->all();

        $slug = $input['slug'];

        $file = request()->file('file');

        unset($input['file']);

        $document = Document::make(file_get_contents($file), $file->getClientOriginalExtension(), $slug);

        if ($input['process_type'] === 'reserve_unit' || $input['process_type'] === 'register_shareholder') {
            $application = ClientTransactionApproval::findOrFail($input['application_id']);

            $this->repo->upload($input, $application, $document);
        }

        $application = ClientFilledInvestmentApplication::findOrFail($input['application_id']);

        $application->clientRepo->upload($input, $application, $document);

        if ($slug === 'payment') {
            (new PaymentProofUploadedMailer())->notify($application->id, 'application');
        }

        return response(['created' => true]);
    }

    public function deleteDocument($id)
    {
        $document = (new \App\Cytonn\Models\ClientFilledInvestmentApplicationDocument)->findOrFail($id);

        $document->delete();

        return response(['deleted' => true]);
    }

    public function updateSigningMandate()
    {
        $input = request()->all();

        $application = ClientFilledInvestmentApplication::findOrFail($input['application_id']);

        $application->signing_mandate = request('signing_mandate');

        $application->save();

        return response(['created' => true]);
    }

    public function uploadTaxExemption()
    {
        $input = request()->all();

        $client = Client::findOrFail($input['client_id']);

        $document = request()->hasFile('file') ? $this->createDoc($input) : null;

        unset($input['file']);

        $input['document_id'] = $document ? $document->id : null;

        if (isset($input['tax_id'])) {
            return $this->editTaxExemption($client, $input);
        }

        $instruction = ClientTransactionApproval::add([
            'client_id' => $client->id,
            'transaction_type' => 'add_tax_exemption',
            'payload' => $input
        ]);

        return response(['created' => true, 'instruction', $instruction->id]);
    }

    public function editTaxExemption(Client $client, $input)
    {
        $instruction = ClientTransactionApproval::add([
            'client_id' => $client->id,
            'transaction_type' => 'edit_tax_exemption',
            'payload' => $input
        ]);

        return response(['updated' => true, 'instruction', $instruction->id]);
    }

    public function createDoc($input)
    {
        $slug = $input['slug'];

        $file = request()->file('file');

        unset($input['file']);

        return Document::make(file_get_contents($file), $file->getClientOriginalExtension(), $slug);
    }
}
