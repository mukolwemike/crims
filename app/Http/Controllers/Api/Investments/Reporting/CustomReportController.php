<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/7/18
 * Time: 1:01 PM
 */

namespace App\Http\Controllers\Api\Investments\Reporting;

use App\Cytonn\Api\Transformers\Investments\CustomReportTransformer;
use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportArgument;
use App\Cytonn\Models\Reporting\ReportFilterType;
use App\Cytonn\Reporting\Custom\ReportRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CustomReportController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ReportRepository();
    }

    public function index()
    {
        $reports = Report::all();

        $reports = $this->repo->reports();

        return response()->json($reports);
    }

    public function store()
    {
        $input = request()->all();

        $report = $this->repo->saveReport($input);

        return response()->json($report);
    }

    public function show($id)
    {
        $report = Report::findOrFail($id);

        $report = app(CustomReportTransformer::class)->transform($report);

        return response()->json($report);
    }

    public function storeColumns($id)
    {
        $report = Report::findOrFail($id);

        $columns = request()->get('columns');

        $this->repo->saveReportColumns($report, $columns);

        return response()->json($report->columns);
    }

    public function storeArguments($id)
    {
        $report = Report::findOrFail($id);

        $arguments = request()->get('arguments');

        $arguments = $this->repo->saveArguments($arguments);

        return response()->json($arguments);
    }

    public function storeFilters($id)
    {
        $report = Report::findOrFail($id);

        $filters = request()->get('filters');

        $this->repo->saveReportFilters($report, $filters);

        return response()->json($report->filters);
    }

    public function modelRelations()
    {
        $model = app(request()->get('model_name'));

        $relations = $model->reportPresent()->relations;

        $model_relations = [];

        foreach ($relations as $key => $relation) {
            $result = [
                'value' => $relation,
                'label' => $relation
            ];

            array_push($model_relations, $result);
        }

        return $model_relations;
    }

    public function modelColumns()
    {
        $model = app(request()->get('model_name'));

        $columns = $model->reportPresent()->columns;

        $model_columns = [];

        foreach ($columns as $key => $column) {
            $result = [
                'value' => $key,
                'label' => ucwords(str_replace('_', ' ', $key))
            ];

            array_push($model_columns, $result);
        }

        return $model_columns;
    }

    public function filterTypes()
    {
        $filters = ReportFilterType::all();

        return response()->json($filters);
    }

    public function export($id)
    {
        $arguments = request()->get('form');

        $this->updateArguments($arguments);

        $report = Report::findOrFail($id);

        $report_id = $report->id;

        $user_id = \Auth::user()->id;

        $this->queue(
            function () use ($report_id, $user_id) {
                \Artisan::call(
                    'custom-reports:test',
                    [
                        'report_id' => $report_id ,
                        'user_id' => $user_id
                    ]
                );
            }
        );

        return response()->json([
            'report' => $report,
            'status' => 200
        ]);
    }

    public function updateArguments($args)
    {
        foreach ($args as $arg) {
            $argument = ReportArgument::find($arg['id']);

            $argument->update($arg);

            $argument->save();
        }

        return;
    }

    public function delete($id)
    {
        $report = Report::findOrFail($id);

        $report->delete();

        return response()->json([ 'status' => 200 ]);
    }
}
