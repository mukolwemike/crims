<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Http\Controllers\Api\Investments;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Investment\InvestmentPaymentSchedules\InvestmentPaymentScheduleTransformer;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class InvestmentPaymentScheduleController
{
    use AlternateSortFilterPaginateTrait;

    /**
     * @param $id
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index($id)
    {
        return $this->sortFilterPaginate(
            new InvestmentPaymentSchedule(),
            [],
            function ($paymentSchedule) {
                return app(InvestmentPaymentScheduleTransformer::class)->transform($paymentSchedule);
            },
            function ($model) use ($id) {
                $model = $model->where('parent_investment_id', $id);

                return $model;
            }
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSchedule($id)
    {
        $schedule = InvestmentPaymentSchedule::findOrFail($id);

        return Response::json(array_except($schedule->toArray(), ['created_at', 'updated_at', 'deleted_at']));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSchedule(Request $request, $id)
    {
        $schedule = InvestmentPaymentSchedule::findOrFail($request->get('schedule_id'));

        if ($schedule->investment) {
            return Response::json([
                'errors' => 'The schedule has already been processed',
                'status' => \Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
        }

        $investment = $schedule->parentInvestment;

        $input = $request->all();
        $input['date_before'] = $schedule->date;
        $input['amount_before'] = $schedule->amount;

        ClientTransactionApproval::make(
            $investment->client_id,
            'store_investment_payment_schedule',
            $input
        );

        return Response::json([
            'data' => ['message' => 'Transaction submitted successfully for approval'],
            'status' => \Symfony\Component\HttpFoundation\Response::HTTP_CREATED
        ], \Symfony\Component\HttpFoundation\Response::HTTP_CREATED);
    }

    /**
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listing()
    {
        return $this->sortFilterPaginate(
            new InvestmentPaymentSchedule(),
            [],
            function ($paymentSchedule) {
                return app(InvestmentPaymentScheduleTransformer::class)->transform($paymentSchedule);
            }
        );
    }
}
