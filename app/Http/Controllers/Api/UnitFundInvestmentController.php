<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 09/08/2018
 * Time: 11:21
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Http\Controllers\Controller;
use App\Http\CrimsClient\Requests\Request;

class UnitFundInvestmentController extends Controller
{
    public function investApplication(Request $request)
    {
        $input = $request->all();

        $app = (new UnitFundInvestmentInstruction)->findOrFail($input['instruction_id']);

        ClientTransactionApproval::add(
            [
                'client_id' => $app->client->id,
                'transaction_type' => 'unit_fund_investment',
                'payload' => $input
            ]
        );

        return response(['created' => true, 'status' => 202]);
    }

    public function editUnitFundInstruction(Request $request, $id)
    {
        $instruction = (new UnitFundInvestmentInstruction)->findOrFail($id);


        $instruction->unit_fund_id = $request->get('unit_fund_id');

        $instruction->amount = $request->get('amount');

        $instruction->number = $request->get('number');

        $instruction->save();

        return response([
            'status' => 200,
            'message' => 'Unit fund investment instruction saved succesfully'
        ]);
    }
}
