<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\RealEstateSalesAgreement;
use Cytonn\Api\Transformers\SalesAgreementTransformer;

/**
 * Class SalesAgreementsController
 *
 * @package Api
 */
class SalesAgreementsController extends ApiController
{
    /**
     * Generate the Sales Agreements grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new RealEstateSalesAgreement(),
            new SalesAgreementTransformer(),
            function ($model) {
                $model = $model->whereHas(
                    'holding',
                    function ($holding) {
                        return $holding->where('active', 1);
                    }
                );
                $model = $this->searchByClientCode($model);
                return $this->searchByProject($model);
            }
        );
    }

    /**
     * @param $sales_agreement
     * @return mixed
     */
    private function searchByClientCode($sales_agreement)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $sales_agreement->whereHas(
                'holding',
                function ($holding) use ($code) {
                    $holding->whereHas(
                        'client',
                        function ($client) use ($code) {
                            $client->where('client_code', 'like', '%' . $code . '%');
                        }
                    );
                }
            );
        }
        return $sales_agreement;
    }

    /**
     * @param $sales_agreement
     * @return mixed
     */
    private function searchByProject($sales_agreement)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['project'])) {
            $project = $state['search']['predicateObject']['project'];
            return $sales_agreement->whereHas(
                'holding',
                function ($holding) use ($project) {
                    $holding->whereHas(
                        'unit',
                        function ($unit) use ($project) {
                            $unit->where('project_id', $project);
                        }
                    );
                }
            );
        }
        return $sales_agreement;
    }
}
