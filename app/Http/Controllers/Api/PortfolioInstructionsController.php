<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\PortfolioInstruction;
use Cytonn\Api\Transformers\PortfolioInstructionTransformer;

/**
 * Class PortfolioInstructionsController
 *
 * @package Api
 */
class PortfolioInstructionsController extends ApiController
{
    /**
     * Generate the Client Payments grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new PortfolioInstruction(),
            new PortfolioInstructionTransformer(),
            function ($model) {
                $model = $this->searchByReceiver($model);
                return $this->searchBySender($model);
            }
        );
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function searchBySender($instruction)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['sender'])) {
            $account = $state['search']['predicateObject']['sender'];
            return $instruction->where('sender_id', $account);
        }
        return $instruction;
    }

    /**
     * @param $instruction
     * @return mixed
     */
    private function searchByReceiver($instruction)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['receiver'])) {
            $account = $state['search']['predicateObject']['receiver'];
            return $instruction->where('receiver_id', $account);
        }
        return $instruction;
    }
}
