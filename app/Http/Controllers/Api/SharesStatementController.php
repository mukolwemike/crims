<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesCampaignStatementItem;
use Cytonn\Api\Transformers\ShareholderTransformer;
use Cytonn\Api\Transformers\SharesCampaignStatementItemTransformer;

/**
 * Class SharesStatementController
 *
 * @package Api
 */
class SharesStatementController extends ApiController
{
    /**
     * Campaign items grid table
     *
     * @param  $campaignId
     * @return string
     */
    public function campaignItems($campaignId)
    {
        return $this->processTable(
            new SharesCampaignStatementItem(),
            new SharesCampaignStatementItemTransformer(),
            function ($items) use ($campaignId) {
                return $items->where('campaign_id', $campaignId);
            }
        );
    }

    /**
     * Clients missing from campaign
     *
     * @param  $campaignId
     * @return string
     */
    public function missingClients($campaignId)
    {
        $shareHolderIds = SharesCampaignStatementItem::where('campaign_id', $campaignId)
            ->get(['share_holder_id'])->lists('share_holder_id');

        return $this->processTable(
            new ShareHolder(),
            new ShareholderTransformer(),
            function ($holder) use ($shareHolderIds) {
                return $holder->has('shareHoldings')->whereNotIn('id', $shareHolderIds);
            }
        );
    }
}
