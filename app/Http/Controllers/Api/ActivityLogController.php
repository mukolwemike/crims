<?php
/**
 * Date: 05/03/2016
 * Time: 12:30 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ActivityLog;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Carbon\Carbon;
use Cytonn\Api\Transformers\ActivityLogTransformer;
use Cytonn\Api\Transformers\ClientTransactionApprovalsTransformer;
use Cytonn\Api\Transformers\PortfolioApprovalsTransformer;
use League\Fractal\Resource\Collection;

/**
 * Class ActivityLogController
 *
 * @package Api
 */
class ActivityLogController extends ApiController
{
    /**
     * @return string
     */
    public function index()
    {
        $log = new ActivityLog();

        $filtered = $this->filter($log)->latest();

        $sorted = $this->sort($filtered);

        $paginated = $this->paginate($sorted);

        $resource = new Collection($paginated['model'], new ActivityLogTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * List all client transactions
     *
     * @return string
     */
    public function clientTransactions()
    {
        $filter = null;
        $elasticSearchFilter = null;

        if ($this->hasSearchQuery()) {
            $elasticSearchFilter = function ($trans) {
                $this->elasticSearchByClientCode($trans);
                $this->searchByTransactionType($trans);

                return $trans->whereIn('approved', [null, 0])->where('approved_on', null);
            };
        } else {
            $filter = function ($approval) {
                $approval =  $approval->where('approved', 1)
                    ->latest('approved_on');

                $approval = $this->searchByClientCode($approval);
                $approval = $this->searchByTransactionType($approval);

                $args = \Request::get('args');

                if (isset($args['start']) && isset($args['end'])) {
                    $start = $args['start'];
                    $end = $args['end'];

                    $approval = $approval->where('approved_on', '>=', (new Carbon($start))->startOfDay())
                        ->where('approved_on', '<=', (new Carbon($end))->endOfDay());
                }

                return $approval;
            };
        }

        return $this->processTable(
            new ClientTransactionApproval(),
            new ClientTransactionApprovalsTransformer(),
            $filter,
            null,
            true,
            $elasticSearchFilter
        );
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByClientCode($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $trans->whereHas(
                'client',
                function ($client) use ($code) {
                    $client->where('client_code', 'like', '%' . $code . '%');
                }
            );
        }
        return $trans;
    }

    /**
     * List all portfolio transactions
     *
     * @return string
     */
    public function portfolioTransactions()
    {
        $filter = function ($approval) {
            $approval = $this->searchByInstitution($approval);
            $approval = $this->searchByTransactionType($approval);
            $args = \Request::get('args');

            $approval = $approval->latest('approved_on');

            if (isset($args['start']) && isset($args['end'])) {
                $start = $args['start'];
                $end = $args['end'];

                $approval = $approval->where('approved_on', '>=', (new Carbon($start))->startOfDay())
                    ->where('approved_on', '<=', (new Carbon($end))->endOfDay());
            }

            return $approval;
        };

        return $this->processTable(new PortfolioTransactionApproval(), new PortfolioApprovalsTransformer(), $filter);
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByInstitution($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['institution'])) {
            $name = $state['search']['predicateObject']['institution'];
            return $trans->whereHas(
                'institution',
                function ($institution) use ($name) {
                    $institution->where('name', $name);
                }
            );
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByTransactionType($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['transaction_type'])) {
            $type = $state['search']['predicateObject']['transaction_type'];
            return $trans->where('transaction_type', $type);
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function elasticSearchByClientCode($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            $trans = $trans->where('client_code', $code);
        }
        return $trans;
    }
}
