<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\CoopMembershipForm;
use Cytonn\Api\Transformers\CoopMembershipFormTransformer;

/**
 * Class CoopMembershipFormsController
 *
 * @package Api
 */
class CoopMembershipFormsController extends ApiController
{
    /**
     * Generate the membership forms grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(new CoopMembershipForm(), new CoopMembershipFormTransformer());
    }
}
