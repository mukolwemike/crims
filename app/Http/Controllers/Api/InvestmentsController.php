<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Dashboard\InvestmentsDashboardGenerator;
use App\Cytonn\Investment\InvestmentActionHandler;
use App\Cytonn\Investment\Transformers\InvestmentTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\InterestPaymentSchedule;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Rate;
use Carbon\Carbon;
use Cytonn\Api\Transformers\InterestPaymentTransformer;
use Cytonn\Api\Transformers\InvestmentsTransformer;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Dashboard\DashboardGenerator;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Rules\InvestmentInstructionRules;
use Cytonn\Presenters\AmountPresenter;

class InvestmentsController extends ApiController
{
    use InvestmentInstructionRules;

    public function getInvestments()
    {
        $investments = new ClientInvestment();

        $filter = function ($inv) {
            $filtered = $inv->forFundManager()->orderBy('withdrawn', 'ASC');

            return $this->searchByClientCode($filtered);
        };

        return $this->processTable($investments, new InvestmentsTransformer(), $filter);
    }

    public function getInvestmentById($id)
    {
        return ClientInvestment::findOrFail($id);
    }

    public function getBusinessConfirmations()
    {
        return $this->processTable(
            new ClientInvestment(),
            new InvestmentsTransformer(),
            function ($i) {
                $i = $this->filterBySentStatus($i);
                return $i->forFundManager()->latest('invested_date');
            }
        );
    }

    private function filterBySentStatus($i)
    {
        $confirm = \Request::get('tableState');

        if (isset($confirm['search']['predicateObject']['confirmation'])) {
            $confirmation = $confirm['search']['predicateObject']['confirmation'];

            if ($confirmation === 'sent') {
                return $i->has('confirmation');
            } elseif ($confirmation === 'not_sent') {
                return $i->whereHas(
                    'confirmation',
                    function ($confirmation) {
                    },
                    '<'
                );
            }
        }

        return $i;
    }

    public function interestPayments()
    {
        $dates = $this->getInterestDates();
        $start = $dates['start'];
        $end = $dates['end'];

        return $this->processTable(
            new InterestPaymentSchedule(),
            new InterestPaymentTransformer($end),
            function ($schedule) use ($start, $end) {
                $schedule = $schedule->betweenDates($start, $end)
                    ->where('description', 'not like', '%maturity%')
                    ->whereHas(
                        'investment',
                        function ($q) use ($start, $end) {
                            $q->where('interest_payment_interval', '>', 0)->activeBetweenDates($start, $end);
                        }
                    );

                return $this->searchByTenor($schedule);
            },
            function ($resource) use ($end) {
                $resource->setMetaValue('interest_date', $end->toDateString());
            }
        );
    }

    /**
     * Calculate interest payable on a certain date
     *
     * @param  int $id
     * @return float
     */
    public function calculate($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $date = new Carbon(\Request::get('date'));

        return $investment->repo->getTotalAvailableInterestAtNextDay($date);
    }

    public function penalty($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        $date = new Carbon(\Request::get('date'));
        $percentage = \Request::get('percentage');
        $withdrawnAmount = \Request::get('withdrawn_amount');

        $withdraw = new \Cytonn\Investment\Action\Withdraw();

        return $withdraw->calculatePenalty($investment, $date, $percentage, $withdrawnAmount)->amount;
    }

    /**
     * @return array
     */
    private function getInterestDates()
    {
        $state = \Request::get('tableState');

        $start = (new Carbon())->startOfMonth()->addDay();
        $end = (new Carbon())->endOfMonth();

        //filter
        if (isset($state['search']['predicateObject']['date'])) {
            $date_range = $state['search']['predicateObject']['date'];

            if (isset($date_range['before']) && isset($date_range['after'])) {
                $start = new Carbon($date_range['after']);
                $end = new Carbon($date_range['before']);
            } elseif (isset($date_range['before'])) {
                $end = (new Carbon($date_range['before']));
                $start = $end->copy()->startOfMonth();
            } elseif (isset($date_range['after'])) {
                $start = new Carbon($date_range['after']);
                $end = $start->copy()->endOfMonth();
            }
        }

        return ['start' => $start, 'end' => $end];
    }

    private function searchByTenor($schedule)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['tenor'])) {
            $tenor = $state['search']['predicateObject']['tenor'];

            $schedule = $schedule->whereHas(
                'investment',
                function ($i) use ($tenor) {
                    $i->where('interest_payment_interval', $tenor);
                }
            );
        }

        return $schedule;
    }

    private function searchByClientCode($inv)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];

            $inv = $inv->whereHas(
                'client',
                function ($q) use ($code) {
                    $q->where('client_code', $code);
                }
            );
        }

        return $inv;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getNetInterestAtDate($id)
    {
        $date = \Request::get('date');
        $investment = ClientInvestment::findOrFail($id);
        return $investment->repo->getNetInterestForInvestmentAtDate($date);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTotalValueOfInvestmentAtDate($id)
    {
        $date = \Request::get('date');
        $investment = ClientInvestment::findOrFail($id);
        return $investment->repo->getTotalValueOfInvestmentAtDate($date);
    }

    public function getWithdrawals()
    {
        $withdrawals = ClientInvestment::where('withdrawn', 1);

        $filter = function ($inv) {
            $filtered = $inv->forFundManager()->orderBy('withdrawn', 'ASC');

            return $this->searchByClientCode($filtered);
        };

        return $this->processTable($withdrawals, new InvestmentsTransformer(), $filter);
    }

    public function clientWithdrawals()
    {
        $withdrawals = ClientInvestment::where('withdrawn', 1);

        $filter = function ($inv) {
            $filtered = $inv->forFundManager()->orderBy('withdrawn', 'ASC');

            return $this->searchByClientCode($filtered);
        };

        return $this->processTable($withdrawals, new InvestmentsTransformer(), $filter);
    }

    /**
     * @param $investment_id
     * @return mixed
     */
    public function fetchInterestRates($investment_id)
    {
        $product = ClientInvestment::findOrFail($investment_id)->product;

        $interest_rate_update = InterestRateUpdate::where('product_id', $product->id)
            ->latest()
            ->first();

        $rates = is_null($interest_rate_update)
            ? [] : Rate::where('interest_rate_update_id', $interest_rate_update->id)
            ->orderBy('tenor', 'desc')
            ->get();

        return \Response::json(['rates' => $rates]);
    }

    public function topupInvestment()
    {
        $validator = $this->topupValidate(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        $product = Product::findOrFail($input['product_id']);

        $client = Client::find($input['client_id']);

        if (!$client) {
            return response(
                [
                    'error' => 'Error finding client',
                    'status' => 311
                ]
            );
        }

        (new InvestmentActionHandler())->topUpInvestment($client, $product, $input, null);

//        event(new TopupFormFilled($instruction));

        return response(['created' => true, 'status' => 202]);
    }

    public function editInvestmentTopup($invId)
    {
        $validator = $this->topupValidate(request());

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        $form = (new ClientTopupForm())->find($invId);

        if (!$form) {
            return response(['status' => 421, 'message' => 'Oops! did not find the topup instruction']);
        }

        $data = request()->except('client');
        (new InvestmentActionHandler())->editInvestmentTopupInstruction($data);

        return response(['created' => true, 'status' => 202]);
    }

    public function withdrawInvestment($invId)
    {
        $validator = $this->withdrawValidate(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $investment = (new ClientInvestment())->findOrFail($invId);

        if ($investment->isSeip()) {
            $withdrawalDate = request()->has('withdrawal_date') ?
                Carbon::parse(request()->get('withdrawal_date')) :
                Carbon::parse($investment->maturity_date);

            if ($withdrawalDate->diffInDays(Carbon::parse($investment->invested_date)) < 365) {
                return response(['status' => 422, 'message' => 'The investment is below 1 year and therefore not eligible for withdrawal']);
            }

            if ($investment->isSeip() && request()->get('amount_select') != 'principal_interest') {
                return response(['status' => 422, 'message' => 'Only full withdrawal of principal and interest is allowed for seip investments']);
            }
        }

        (new InvestmentActionHandler())->withdrawInvestment($investment, request()->all(), null);

//        event(new InvestmentActionPerformed($instruction));

        return response(['created' => true, 'status' => 202]);
    }

    public function editWithdrawal($id)
    {
        $validator = $this->editWithdrawValidate(request());

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        $form = (new ClientInvestmentInstruction())->find($id);

        if (!$form) {
            return response(['status' => 421, 'message' => 'Oops! did not find the withdrawal instruction']);
        }

        $data = request()->all();
        (new InvestmentActionHandler())->editWithdrawalInstruction($data);

        return response(['created' => true, 'status' => 202]);
    }

    public function amountAffected($invId)
    {
        $instruction = request()->all();
        $investment = ClientInvestment::findOrFail($invId);

        ($instruction['date'] = "mature")
            ? $date = Carbon::parse($investment->maturity_date)
            : $date = Carbon::parse($instruction['date']);

        if ($instruction['amount_select'] == 'principal') {
            return is_null($investment) ? null
                : [
                    'currency' => $investment->product->currency->code,
                    'amount' => $investment->amount,
                ];
        } elseif ($instruction['amount_select'] == 'principal_interest') {
            return [
                'currency' => $investment->product->currency->code,
                'amount' => $investment->repo->getTotalValueOfAnInvestment($date) + $investment->withdraw_amount,
            ];
        } elseif ($instruction['amount_select'] == 'interest') {
            return [
                'currency' => $investment->product->currency->code,
                'amount' => $investment->repo->getGrossInterestForInvestment($date) + $investment->withdraw_amount,
            ];
        }

        return null;
    }


    public function getCombineInvestments($id)
    {
        $investment = (new ClientInvestment())->findOrFail($id);

        $same_day_investments = (new ClientInvestment())->where('client_id', $investment->client_id)
            ->where('product_id', $investment->product_id)
            ->where('id', '!=', $investment->id)
            ->active()
            ->get();

        $same_day_investments = $same_day_investments->map(
            function ($investment) {
                return (new InvestmentTransformer())->transform($investment);
            }
        );

        return response($same_day_investments);
    }

    public function combineSelectedRollover($id)
    {
        $investment = (new ClientInvestment())->findOrFail($id);
        $input = request()->all();

        $investments = new Collection();

        $combineDate = Carbon::parse($input['combine_date']);
        $maturityDate = $investment->maturity_date;

        if ($combineDate > $maturityDate) {
            return response([
                'status' => 422,
                'message' => 'No investment should have its maturity date before the combine date'
            ]);
        }

        $investment->value = $investment->repo->getTotalValueOfInvestmentAtDate($combineDate);
        $investment->combine_date = $combineDate->toDateString();
        $investments->push($investment);

        foreach ($input['investments'] as $inv_id) {
            $inv = (new ClientInvestment())->findOrFailByUuid($inv_id);

            if ($combineDate->gt($inv->maturity_date)) {
                return response([
                    'status' => 422,
                    'message' => 'No investment should have its maturity date before the combine date, yeah'
                ]);
            }

            $inv->value = $inv->repo->getTotalValueOfInvestmentAtDate($combineDate);
            $inv->combine_date = $combineDate->toDateString();

            $investments->push($inv);
        }

        return response(['status' => 202, 'investments' => $investments]);
    }

    public function rolloverInvestment($id)
    {
        $validator = $this->rolloverValidate(request());

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        \DB::transaction(function () use ($id) {
            $investment = (new ClientInvestment())->findOrFail($id);

            $input = request()->all();

            $data = (isset($input['combinedInvestment']))
                ? request()->except('product_id', 'client_id', 'combinedInvestment')
                : request()->except('product_id', 'client_id');

            $instruction = (new InvestmentActionHandler())->investmentRollover($investment, $data);

//            event(new InvestmentActionPerformed($instruction));

            if (isset($input['combinedInvestment'])) {
                $this->saveCombinedInvestments($input['combinedInvestment'], $instruction);
            }
        });


        return response(['created' => true, 'status' => 202]);
    }

    public function saveCombinedInvestments($data, $instruction)
    {
        $data = collect($data)->map(function ($inv) use ($instruction) {
            $id = $inv['id'];

            $investment = ClientInvestment::find($id);

            if ($investment->maturity_date->lt($instruction->due_date)) {
                throw new ClientInvestmentException(
                    "The combine date ($instruction->due_date) is after maturity of investment of principal "
                    . AmountPresenter::currency($investment->amount)
                );
            }

            return $id;
        });

        $instruction->investments()->attach($data, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

        return response(['created' => true, 'status' => 202]);
    }

    public function editRollover($id)
    {
        $input = request()->all();
        $validator = $this->rolloverUpdateValidate(request());

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        $instruction = (new ClientInvestmentInstruction())->find($id);

        if (!$instruction) {
            return response(['status' => 421, 'message' => 'Oops! did not find the rollover instruction']);
        }

        \DB::transaction(function () use ($instruction, $input) {
            $data = request()
                ->except('combinedRollover', 'combine_date', 'combinedInvestment', 'rollover_instructions');
            (new InvestmentActionHandler())->editRolloverInstruction($data);

            if (isset($input['combinedInvestment'])) {
                $instruction->investments()->detach();
                $this->saveCombinedInvestments($input['combinedInvestment'], $instruction);
            }
        });

        return response(['created' => true, 'status' => 202]);
    }

    /*
    * api functions for investments dashboard statistics
    */

    public function spInvestments()
    {
        $invGenerator = new InvestmentsDashboardGenerator();
        return response()->json($invGenerator->activeSPInvestments($this->fundManager()), 200);
    }

    public function utfInvestments()
    {
        $invGenerator = new InvestmentsDashboardGenerator();
        return response()->json($invGenerator->activeUTFInvestments($this->fundManager()), 200);
    }

    public function investmentsApplication()
    {
        $invGenerator = new InvestmentsDashboardGenerator();
        return response()->json($invGenerator->applicationsCount($this->fundManager()), 200);
    }

    public function pendingInstructions()
    {
        $invGenerator = new InvestmentsDashboardGenerator();
        return response()->json($invGenerator->clientInstructions($this->fundManager()), 200);
    }

    public function pendingApprovals()
    {
        $gen = new DashboardGenerator();
        return response()->json($gen->getClientTransactionsAwaitingApprovalCount(), 200);
    }
}
