<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Api\Transformers\RealEstateScheduledPaymentTransformer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Realestate\Payments\ScheduleGenerator;

/**
 * Class RealEstatePaymentSchedulesController
 *
 * @package Api
 */
class RealEstatePaymentSchedulesController extends ApiController
{
    /**
     * @param $holding_id
     * @param $spacing
     * @return array
     */
    public function generatePaymentSchedules($holding_id, $spacing)
    {
        return (new ScheduleGenerator())->generate(UnitHolding::findOrFail($holding_id), $spacing);
    }

    public function overdue()
    {
        return $this->processTable(
            new RealEstatePaymentSchedule(),
            new RealEstateScheduledPaymentTransformer(),
            function ($schedule) {
                return $schedule->whereHas(
                    'holding',
                    function ($holding) {
                        $holding->where('active', true);
                    }
                )
                ->inPricing()
                ->isLast(false)
                ->where('date', '<', Carbon::today())
                ->where('paid', false)
                ->oldest('date');
            }
        );
    }
}
