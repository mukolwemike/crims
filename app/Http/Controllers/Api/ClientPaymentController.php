<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use Cytonn\Api\Transformers\ClientPaymentTransformer;
use Cytonn\Api\Transformers\SingleClientPaymentTransformer;

/**
 * Class ClientPaymentController
 *
 * @package Api
 */
class ClientPaymentController extends ApiController
{
    /**
     * Generate the Client Payments grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new ClientPayment(),
            new ClientPaymentTransformer(),
            function ($model) {
                if ($type = \Request::get('type')) {
                    $model = $model->ofType($type);
                }

                $model = $this->searchByClientCode($model)->where('amount', '!=', 0);
                $model = $this->searchByProduct($model);
                $model = $this->searchByFund($model);
                $model = $this->filterBySentStatus($model);

                return $this->searchByProject($model->latest('date')->latest());
            }
        );
    }

    /**
     * @param $id
     * @return string
     */
    public function clientPayments($id)
    {
        $client = Client::findOrFail($id);
        return $this->processTable(
            new ClientPayment(),
            new SingleClientPaymentTransformer(),
            function ($model) use ($client) {
                $model = $model->where('client_id', $client->id)->where('amount', '!=', 0);
                $model = $this->searchByClientCode($model);
                $model = $this->searchByProduct($model);
                $model = $this->searchByFund($model);
                $model = $this->filterByEntity($model);
                return $this->searchByProject($model)->latest('date')->latest()->orderBy('id', 'DESC');
            }
        );
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function searchByClientCode($payment)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $payment->whereHas(
                'client',
                function ($client) use ($code) {
                    $client->where('client_code', 'like', '%' . $code . '%');
                    ;
                }
            );
        }
        return $payment;
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function searchByProject($payment)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['project'])) {
            $project = $state['search']['predicateObject']['project'];
            return $payment->where('project_id', $project);
        }
        return $payment;
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function searchByProduct($payment)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['product'])) {
            $product = $state['search']['predicateObject']['product'];
            return $payment->where('product_id', $product);
        }
        return $payment;
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function searchByFund($payment)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['fund'])) {
            $fund = $state['search']['predicateObject']['fund'];
            return $payment->where('unit_fund_id', (int)$fund);
        }
        return $payment;
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function filterByEntity($payment)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['entity'])) {
            $entity = $state['search']['predicateObject']['entity'];
            return $payment->where('share_entity_id', $entity);
        }
        return $payment;
    }

    private function filterBySentStatus($payment)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['confirmation'])) {
            $confirmation = $state['search']['predicateObject']['confirmation'];
            if ($confirmation == 'sent') {
                return $payment->has('membershipConfirmation');
            }
            return $payment->whereHas(
                'membershipConfirmation',
                function ($membership) {
                },
                '<'
            );
        }
        return $payment;
    }
}
