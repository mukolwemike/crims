<?php
/**
 * Date: 18/05/2016
 * Time: 5:33 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Api\Transformers\RealEstateCancelledUnitTransformer;
use Cytonn\Api\Transformers\RealEstateClientsTransformer;
use Cytonn\Api\Transformers\RealEstateClientUnitTransformer;
use Cytonn\Api\Transformers\RealEstateProjectsTransformer;
use Cytonn\Api\Transformers\RealEstateUnitTransformer;

/**
 * Class RealEstateController
 *
 * @package Api
 */
class RealEstateController extends ApiController
{
    public function projects()
    {
        return $this->processTable(new Project(), new RealEstateProjectsTransformer());
    }

    public function clients()
    {
        return $this->processTable(
            new Client(),
            new RealEstateClientsTransformer(),
            function ($client) {
                return $client->has('unitHoldings');
            }
        );
    }

    public function clientUnits($clientId)
    {
        $filter = function ($unitHolding) use ($clientId) {
            return $unitHolding->has('unit')->where('client_id', $clientId);
        };

        return $this->processTable(new UnitHolding(), new RealEstateClientUnitTransformer(), $filter);
    }

    public function projectUnits($projectId)
    {
        $filter = function ($unit) use ($projectId) {
            return $unit->where('project_id', $projectId);
        };

        return $this->processTable(new RealestateUnit(), new RealEstateUnitTransformer(), $filter);
    }

    public function projectCancelledUnits($projectId)
    {
        $filter = function ($holding) use ($projectId) {
            return $holding->active(false)->whereHas(
                'unit',
                function ($unit) use ($projectId) {
                    $unit->where('project_id', $projectId);
                }
            );
        };

        return $this->processTable(new UnitHolding(), new RealEstateCancelledUnitTransformer(), $filter);
    }

    public function clientProjectUnits($cid, $pid)
    {
        $client = Client::findOrFail($cid);

        $project = Project::findOrFail($pid);

        return response($project->units()
            ->whereHas('activeHolding', function ($holding) use ($client) {
                return $holding->where('client_id', $client->id);
            })->get());
    }
}
