<?php
/**
 * Date: 05/04/2016
 * Time: 9:56 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ActiveStrategyDividend;
use App\Cytonn\Models\ActiveStrategyHolding;
use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Carbon\Carbon;
use Cytonn\Api\Transformers\ActiveStrategyHoldingTransformer;
use Cytonn\Api\Transformers\ActiveStrategySecurityTransformer;
use Cytonn\Api\Transformers\ActiveStrategyShareSaleTransformer;
use Cytonn\Api\Transformers\EquityDividendTransformer;
use Cytonn\Portfolio\ActiveStrategy\PortfolioValuation;
use League\Fractal\Resource\Collection;

/**
 * Class ActiveStrategyController
 *
 * @package Api
 */
class ActiveStrategyController extends ApiController
{
    /**
     * Show the securities grid page
     *
     * @return string
     */
    public function securities()
    {
        $securites = new PortfolioSecurity();

        $paginated = $this->sortFilterPaginate($securites);

        $resource = new Collection($paginated['model'], new ActiveStrategySecurityTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * Show the holding grid for a security
     *
     * @param  $security_id
     * @return string
     */
    public function holdings($security_id)
    {
        $holdings = new EquityHolding();

        $filtered = $this->filter($holdings)->where('security_id', $security_id);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new ActiveStrategyHoldingTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * @param $security_id
     * @param $type
     * @return string
     */
    public function dividends($security_id, $type)
    {
        $dividends = new ActiveStrategyDividend();

        $filtered = $this->filter($dividends)
            ->where('dividend_type', $type)
            ->where('security_id', $security_id);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new EquityDividendTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * Show sales table on securities page
     *
     * @param  $security_id
     * @return string
     */
    public function shareSales($security_id)
    {
        $sales = new ActiveStrategyShareSale();

        $filtered = $this->filter($sales)->where('security_id', $security_id);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new ActiveStrategyShareSaleTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * Data for asset allocation chart
     *
     * @return array
     */
    public function assetAllocation()
    {
        $securities = PortfolioSecurity::where('fund_manager_id', $this->fundManager()->id)->get();

        $names = [];

        $portfolio_costs = [];

        foreach ($securities as $security) {
            array_push($names, $security->name);

            try {
                $cost = (new PortfolioValuation($this->fundManager(), Carbon::today()))->portfolioCost();

                array_push($portfolio_costs, 100* $security->repo->totalCurrentPurchaseValue()/$cost);
            } catch (\Exception $e) {
                array_push($portfolio_costs, 0);
            }
        }
        
        return [
            'names'=>$names,
            'values'=>$portfolio_costs
        ];
    }
}
