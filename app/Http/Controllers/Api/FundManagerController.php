<?php
/**
 * Date: 19/08/2016
 * Time: 10:25 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\FundManager;
use Cytonn\Investment\FundManager\FundManagerScope;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FundManagerController extends ApiController
{
    public function change($id)
    {
        /*
         * @var FundManagerScope
         */
        $scope = app(FundManagerScope::class);

        $scope->setSelectedFundManager($id);

        return $this->respond('done');
    }

    public function getList()
    {
        $fundsList = FundManager::all()
            ->map(
                function ($manager) {
                    return ['label' => $manager->fullname, 'value' => $manager->id];
                }
            )
            ->toArray();

        return response(['fetched' => true, 'data' => $fundsList], 200);
    }

    public function funds($id)
    {
        $manager = FundManager::find($id);

        if (! $manager) {
            return response()->json(['message' => 'error', 'status' => 401 ]);
        }

        return response()->json([ 'funds' => $manager->unitFunds, 'status' => 200 ]);
    }
}
