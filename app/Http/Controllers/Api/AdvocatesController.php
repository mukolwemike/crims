<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Advocate;
use Cytonn\Api\Transformers\AdvocateTransformer;

/**
 * Class AdvocatesController
 *
 * @package Api
 */
class AdvocatesController extends ApiController
{
    /**
     * Generate the Advocates grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new Advocate(),
            new AdvocateTransformer(),
            function ($model) {
                return $this->searchByProject($model);
            }
        );
    }

    /**
     * @param $model
     * @return mixed
     */
    private function searchByProject($model)
    {
        $state = request('tableState');
        if (isset($state['search']['predicateObject']['project'])) {
            $projectId = $state['search']['predicateObject']['project'];
            return $model->whereHas(
                'projectAssignments',
                function ($assignment) use ($projectId) {
                    $assignment->where('project_id', $projectId);
                }
            );
        }
        return $model;
    }
}
