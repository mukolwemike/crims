<?php
/**
 * Date: 10/03/2016
 * Time: 12:20 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealestateCampaignStatementItem;
use Carbon\Carbon;
use Cytonn\Api\Transformers\RealEstateCampaignStatementItemTransformer;
use Cytonn\Api\Transformers\ClientTransformer;
use League\Fractal\Resource\Collection;

/**
 * Class StatementController
 *
 * @package Api
 */
class RealEstateStatementController extends ApiController
{
    /**
     * Campaign items grid table
     *
     * @param  $campaignId
     * @return string
     */
    public function campaignItems($campaignId)
    {
        $campaign = new RealestateCampaignStatementItem();

        $filtered = $this->filter($campaign)->where('campaign_id', $campaignId);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new RealEstateCampaignStatementItemTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * Clients missing from campaign
     *
     * @param  $campaignId
     * @return string
     */
    public function missingClients($campaignId)
    {
        $existing = RealestateCampaignStatementItem::where('campaign_id', $campaignId)->get()->lists('client_id');

        $clients = new Client();

        $remaining = Client::whereNotIn('id', $existing)
            ->whereHas(
                'unitHoldings',
                function ($holding) {
                    $holding->where('active', 1);
                }
            )->get()->lists('id');

        $filtered = $this->filter($clients)->whereIn('id', $remaining);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new ClientTransformer());
        $this->addPaginationToResource($paginated, $resource);
        $resource->setMetaValue('total_count', $filtered->count());

        return $this->fractal->createData($resource)->toJson();
    }
}
