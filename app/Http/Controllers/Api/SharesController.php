<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ShareHolding;
use Cytonn\Api\Transformers\ShareHoldingsTransformer;

/**
 * Class SharesController
 *
 * @package Api
 */
class SharesController extends ApiController
{
    /**
     * Show business confirmation grid
     *
     * @return string
     */
    public function getBusinessConfirmations()
    {
        return $this->processTable(
            new ShareHolding(),
            new ShareHoldingsTransformer(),
            function ($model) {
                $model = $this->filterBySentStatus($model);
                return $model
                    ->whereNotNull('client_payment_id')
                    ->forFundManager()
                    ->orderBy('date', 'DESC');
            }
        );
    }

    private function filterBySentStatus($model)
    {
        $confirm = \Request::get('tableState');

        if (isset($confirm['search']['predicateObject']['confirmation'])) {
            $confirmation = $confirm['search']['predicateObject']['confirmation'];

            if ($confirmation === 'sent') {
                return $model->has('confirmation');
            } elseif ($confirmation === 'not_sent') {
                return $model->whereHas(
                    'confirmation',
                    function ($confirmation) {
                    },
                    '<'
                );
            }
        }

        return $model;
    }

    public function getShareHoldingsForShareHolder($share_holder_id)
    {
        return $this->processTable(
            new ShareHolding(),
            new ShareHoldingsTransformer(),
            function ($model) use ($share_holder_id) {
                return $model->where('share_holder_id', $share_holder_id)->orderBy('date', 'DESC');
            }
        );
    }
}
