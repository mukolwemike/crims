<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Currency;

/**
 * Class CurrencyController
 *
 * @package Api
 */
class CurrencyController extends ApiController
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getList()
    {
        $currencyLists = Currency::all()->map(
            function ($currency) {
                return ['label' => $currency->name, 'value' => $currency->id];
            }
        )->toArray();

        return response(['fetched' => true, 'data' => $currencyLists], 200);
    }
}
