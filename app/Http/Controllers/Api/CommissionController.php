<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Api\Transformers\CommissionRecipientPositionsDetailsTransformer;
use App\Cytonn\Api\Transformers\FinancialAdvisorSearchTransformer;
use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecepientPosition;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Product;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Crims\Commission\Override;
use Cytonn\Api\Transformers\BulkCommissionPaymentsTransformer;
use Cytonn\Api\Transformers\CommissionRecipientDetailsTransformer;
use Cytonn\Api\Transformers\CommissionRecipientTransformer;
use Cytonn\Investment\Commission\Base;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use League\Fractal\Resource\Collection;

class CommissionController extends ApiController
{
    public function index($currency_id)
    {
        $currency = Currency::findOrFail($currency_id);

        $recipient = new CommissionRecepient();

        $filtered = function ($q) use ($currency) {
            return $q->whereHas(
                'commissions',
                function ($q) use ($currency) {
                    $q->whereHas(
                        'investment',
                        function ($q) use ($currency) {
                            $q->currency($currency);
                        }
                    );
                }
            );
        };

        $date = Carbon::today();

        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['month'])) {
            $date = new Carbon($state['search']['predicateObject']['month']);
        }

        $commissionDates = (new BulkCommissionPayment())->commissionDates($date);

        $transformer = new CommissionRecipientTransformer(
            $currency,
            $commissionDates['start'],
            $commissionDates['end']
        );

        $modifyResource = function ($resource) use ($commissionDates) {
            return $resource->setMetaValue('dates', [
                'start' => $commissionDates['start']->toDateString(),
                'end' => $commissionDates['end']->toDateString()
            ]);
        };

        return $this->processTable($recipient, $transformer, $filtered, $modifyResource);
    }

    public function recipients()
    {
        return $this->processTable(new CommissionRecepient(), new CommissionRecipientDetailsTransformer());
    }

    public function paymentDates()
    {
        return $this->processTable(
            new BulkCommissionPayment(),
            new BulkCommissionPaymentsTransformer(),
            function ($dates) {
                return $dates->latest('start');
            }
        );
    }

    public function hasZeroCommissionRate($recipient_id)
    {
        return (bool)CommissionRecepient::findOrFail($recipient_id)->zero_commission ? 1 : 0;
    }

    public function getCommissionRecipients()
    {
        return $this->processTable(new CommissionRecepient(), new CommissionRecipientDetailsTransformer());
    }

    public function commissionRecipients()
    {
        return CommissionRecepient::with(['type'])->orderBy('name', 'asc')->get();
    }

    /*
     * Get the overrides for a given recipient
     */
    public function getRecipientOverrides($recipientId, $currencyId, $start, $end)
    {
        $currency = Currency::findOrFail($currencyId);

        $recipient = CommissionRecepient::findOrFail($recipientId);

        $commissionDates = $this->getCommissionDates($start, $end);

        $recipientCalc = $recipient->calculator($currency, $commissionDates['start'], $commissionDates['end']);

        $reports = $recipientCalc->getReports();

        $recipientSummary = $recipientCalc->summary();

        $overrideClass =
            (new Override($recipient, $commissionDates['end'], 'investments', $commissionDates['start']));

        $overrideRate = $overrideClass->getOverrideRate();

        $totalSum = 0;

        $overrideArray = array();

        foreach ($reports as $fa) {
            $override = $recipientSummary->overrideOnRecipient($fa);

            $totalSum += $override;

            $overrideArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $recipientSummary->overrideCommission($fa),
                'override' => $override,
                'rate' => $overrideRate
            ];
        }

        return Response::json([
            'overrides' => $overrideArray,
            'total' => AmountPresenter::currency($totalSum),
            'commission' => [
                'compliance' => AmountPresenter::currency($recipientSummary->getCompliant()),
                'clawbacks' => AmountPresenter::currency($recipientSummary->getClawBacks()),
                'finalCommission' => AmountPresenter::currency($recipientSummary->finalCommission())
            ]
        ]);
    }

    /**
     * @param $recipientId
     * @param $currencyId
     * @param $start
     * @param $end
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecipientAdditionalCommission($recipientId, $currencyId, $start, $end)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $commissionDates = $this->getCommissionDates($start, $end);

        $recipientCalc = $recipient->additionalCommissionCalculator($commissionDates['start'], $commissionDates['end']);

        $recipientCalc = $recipientCalc->calculateOverallCommission();

        $recipientSummary = $recipientCalc->summary();

        return Response::json([
            'additional_commission' => AmountPresenter::currency($recipientSummary->additionalCommission()),
            'net_unpaid_commission' => AmountPresenter::currency($recipientSummary->netUnpaidCommission()),
            'net_repayments' => AmountPresenter::currency($recipientSummary->netRepayments()),
        ]);
    }

    public function oldgetRecipientOverrides($recipientId, $currencyId, $start, $end)
    {
        $currency = Currency::findOrFail($currencyId);

        $recipient = CommissionRecepient::findOrFail($recipientId);

        $commissionDates = $this->getCommissionDates($start, $end);

        $overrideClass =
            (new Override($recipient, $commissionDates['end'], 'investments', $commissionDates['start']));

        $overrideRate = $overrideClass->getOverrideRate();

        $reports = $overrideClass->reports($recipient);

        $reports = $reports->sortBy('name');

        $totalSum = 0;

        $overrideArray = array();

        foreach ($reports as $fa) {
            $calc = $fa->calculator($currency, $commissionDates['start'], $commissionDates['end']);

            $dates = $overrideClass->getOverrideDates($recipient, $fa);

            $calc = $calc->setOverrideDates($dates['start'], $dates['end']);

            $commission = $calc->summary()->total();

            $override = $commission * $overrideRate / 100;

            $totalSum += $override;

            $overrideArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $commission,
                'override_start' => ($dates['start']) ? $dates['start']->toDateString() : '',
                'override_end' => ($dates['end']) ? $dates['end']->toDateString() : '',
                'override' => $override,
                'rate' => $overrideRate
            ];
        }

        $calculator = $recipient->calculator($currency, $commissionDates['start'], $commissionDates['end']);

        $summary = $calculator->summary();

        return Response::json([
            'overrides' => $overrideArray,
            'total' => AmountPresenter::currency($totalSum),
            'commission' => [
                'compliance' => AmountPresenter::currency($summary->getCompliant()),
                'clawbacks' => AmountPresenter::currency($summary->getClawBacks()),
                'finalCommission' => AmountPresenter::currency($summary->total() + $totalSum)
            ]
        ]);
    }

    /*
     * Get the commission bulk dates
     */
    public function getCommissionDates($start, $end)
    {
        return [
            'start' => Carbon::parse($start)->startOfDay(),
            'end' => Carbon::parse($end)->endOfDay()
        ];
    }

    public function allRecipients()
    {
        $recipient = CommissionRecepient::search(request()->get('query'))->paginate(50);

        $response = new Collection($recipient, new FinancialAdvisorSearchTransformer());

        return $this->fractal->createData($response)->toJson();
    }

    /*
     * Get the commission rate for a given recipient
     */
    public function getRecipientProductCommissionRate(Request $request)
    {
        if ($request->has('client_id')) {
            $client = Client::find($request->client_id);

            if ($client && $client->franchise == 1) {
                return [
                    'rate' => 0,
                    'rate_name' => 'Franchise Client - 0%'
                ];
            }
        }

        $recipient = CommissionRecepient::find($request->get('recipientId'));

        if (is_null($recipient)) {
            return [
                'rate' => 0,
                'rate_name' => 'No Recipient - 0%'
            ];
        }

        if ($recipient->zero_commission == 1) {
            return [
                'rate' => 0,
                'rate_name' => 'Zero Commission - 0%'
            ];
        }

        $product = Product::findOrFail($request->get('product_id'));

        $date = Carbon::parse($request->get('invested_date'));

        $isParent = $request->has('is_parent') ? $request->get('is_parent') == 1 : false;

        $rate = $recipient->repo->getInvestmentCommissionRate($product, $date, null, $isParent);

        if (is_null($rate)) {
            return [
                'rate' => 0,
                'rate_name' => 'No Commission Set - 0%'
            ];
        }

        if ($request->has('investment_type_id')
            && $rate->rate) {
            $rate = $recipient->repo->checkRolloverInvestmentReward(
                $request->get('investment_type_id'),
                $date,
                $rate
            );
        }

        return [
            'rate' => $rate->rate,
            'rate_name' => $rate->name
        ];
    }

    /*
     *  get fa positions history
    */
    public function getCommissionRecipientPositions($commission_recipient_Id)
    {
        $filterFunc = function ($details) use ($commission_recipient_Id) {
            return $details->where('commission_recipient_Id', $commission_recipient_Id)->orderBy('start_date', 'DESC');
        };

        return $this->processTable(
            new CommissionRecepientPosition,
            new CommissionRecipientPositionsDetailsTransformer(),
            $filterFunc
        );
    }
}
