<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Cytonn\Api\Transformers\RealEstateScheduledPaymentTransformer;

/**
 * Class RealEstateScheduledPaymentController
 *
 * @package Api
 */
class RealEstateScheduledPaymentController extends ApiController
{
    /**
     * Show all payments made
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new RealEstatePaymentSchedule(),
            new RealEstateScheduledPaymentTransformer(),
            function ($model) {
                $model = $model->whereHas(
                    'holding',
                    function ($holding) {
                        $holding->active();
                    }
                );

                $model = $this->search($model);

                return $model->latest('date');
            }
        );
    }

    private function search($scheduledPayment)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['startDate']) &&
            isset($state['search']['predicateObject']['endDate'])) {
            $startDate = (new Carbon($state['search']['predicateObject']['startDate']))->startOfMonth();
            $endDate = (new Carbon($state['search']['predicateObject']['endDate']))->endOfMonth();

            $scheduledPayment = $scheduledPayment->whereDate('date', '>=', $startDate)
                ->whereDate('date', '<=', $endDate);
        } elseif (!isset($state['search']['predicateObject']['startDate']) &&
            isset($state['search']['predicateObject']['endDate'])) {
            $endDate = (new Carbon($state['search']['predicateObject']['endDate']))
                ->endOfMonth();

            $scheduledPayment = $scheduledPayment->whereDate('date', '<=', $endDate);
        } elseif (!isset($state['search']['predicateObject']['endDate']) &&
            isset($state['search']['predicateObject']['startDate'])) {
            $startDate = (new Carbon($state['search']['predicateObject']['startDate']))
                ->startOfMonth();

            $scheduledPayment = $scheduledPayment->whereDate('date', '>=', $startDate);
        }

        if (isset($state['search']['predicateObject']['project_id'])) {
            $p = $state['search']['predicateObject']['project_id'];

            if (!is_null($p)) {
                $scheduledPayment = $scheduledPayment->whereHas(
                    'holding',
                    function ($holding) use ($p) {
                        $holding->whereHas(
                            'unit',
                            function ($unit) use ($p) {
                                $unit->where('project_id', $p);
                            }
                        );
                    }
                );
            }
        }

        return $scheduledPayment;
    }
}
