<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientBulkMessage;
use Cytonn\Api\Transformers\ClientBulkMessageTransformer;

/**
 * Class ClientsBulkMessagesController
 *
 * @package Api
 */
class ClientsBulkMessagesController extends ApiController
{
    /**
     * Generate the Bulk Messages grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new ClientBulkMessage(),
            new ClientBulkMessageTransformer(),
            function ($model) {
                $model->latest();
                $model = $this->searchByApproval($model);
                return $this->searchBySent($model);
            }
        );
    }

    /**
     * @param $message
     * @return mixed
     */
    private function searchByApproval($message)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['approved'])) {
            $filter = $state['search']['predicateObject']['approved'];
            switch ($filter) {
                case '1':
                    return $message->whereNotNull('approval_id');
                    break;
                case '0':
                    return $message->whereNull('approval_id');
                    break;
                default:
                    break;
            }
        }
        return $message;
    }

    /**
     * @param $message
     * @return mixed
     */
    private function searchBySent($message)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['sent'])) {
            $filter = $state['search']['predicateObject']['sent'];
            switch ($filter) {
                case '1':
                    return $message->where('sent', true);
                    break;
                case '0':
                    return $message->where('sent', false);
                    break;
                default:
                    break;
            }
        }
        return $message;
    }
}
