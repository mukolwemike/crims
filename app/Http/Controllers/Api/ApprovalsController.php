<?php
/**
 * Date: 27/01/2016
 * Time: 8:29 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Api\Transformers\ClientTransactionApprovalsTransformer;
use League\Fractal\Resource\Collection;

class ApprovalsController extends ApiController
{
    protected $perPage = 10;

    public function listClientApprovals()
    {
        if ($this->hasSearchQuery()) {
            $elasticSearchFilter = function ($trans) {
                $this->elasticSearchByClientCode($trans);
                $this->searchByTransactionType($trans);
                $this->searchByStage($trans);

                return $trans->whereIn('approved', [null, 0])->where('approved_on', null);
            };
            $filter = null;
        } else {
            $filter = function ($trans) {
                $trans = $this->searchByClientCode($trans);
                $trans = $this->searchByTransactionType($trans);
                $trans = $this->searchByStage($trans);

                return $trans->where(
                    function ($q) {
                        $q->where('approved', null)->orWhere('approved', 0);
                    }
                )->latest();
            };
            $elasticSearchFilter = null;
        }

        return $this->processTable(
            new ClientTransactionApproval(),
            new ClientTransactionApprovalsTransformer(),
            $filter,
            null,
            true,
            $elasticSearchFilter
        );
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByClientCode($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            $trans = $trans->whereHas('client', function ($client) use ($code) {
                $client->where('client_code', $code);
            });
            $this->filterParams[] = [
                'column' => 'client_code',
                'value' => $code
            ];
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function elasticSearchByClientCode($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            $trans = $trans->where('client_code', $code);
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByTransactionType($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['transaction_type'])) {
            $type = $state['search']['predicateObject']['transaction_type'];

            return $trans->where('transaction_type', $type);
        }
        return $trans;
    }

    private function searchByStage($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['approval_stage'])) {
            $type = $state['search']['predicateObject']['approval_stage'];

            return $trans->where('awaiting_stage_id', $type);
        }

        return $trans;
    }
}
