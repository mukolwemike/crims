<?php
/**
 * Date: 06/04/2016
 * Time: 4:15 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Api\Transformers\Portfolio\PortfolioAllocationLimitTransformer;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Portfolio\PortfolioAllocationLimit;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Api\Transformers\PortfolioSummaryTransformer;
use Cytonn\Portfolio\Deposits\Analysis;
use Cytonn\Portfolio\MaturityRepository;
use Cytonn\Portfolio\Summary\Analytics;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Request;

/**
 * Class PortfolioController
 *
 * @package Api
 */
class PortfolioController extends ApiController
{
    /**
     * Portfolio summary
     *
     * @param  $currency_id
     * @return string
     */
    public function summary($currency_id)
    {
        $investor = new PortfolioSecurity();

        $state = \Request::get('tableState');

        $date = Carbon::today();

        if (isset($state['search']['predicateObject']['date'])) {
            $date = Carbon::parse($state['search']['predicateObject']['date']);
        }

        $fundId = (isset($state['search']['predicateObject']['unitFund']))
            ? $state['search']['predicateObject']['unitFund']
            : null;

        $unitFund = ($fundId) ? UnitFund::findOrFail($fundId) : null;

        $currency = Currency::findOrFail($currency_id);

        $filter = function ($investor) use ($currency_id, $state) {
            return $this->searchStatus($investor, $currency_id, $state);
        };

        return $this->processTable(
            $investor,
            new PortfolioSummaryTransformer($currency, $this->fundManager(), $date, $unitFund),
            $filter
        );
    }

    /*
     * Get the summary totals
     */
    public function summaryTotals($currencyId)
    {
        $currency = Currency::findOrFail($currencyId);

        $state = \Request::get('tableState');

        $date = Carbon::today();

        if (isset($state['search']['predicateObject']['date'])) {
            $date = Carbon::parse($state['search']['predicateObject']['date']);
        }

        $fundId = (isset($state['search']['predicateObject']['unitFund']))
            ? $state['search']['predicateObject']['unitFund']
            : null;

        $unitFund = ($fundId) ? UnitFund::findOrFail($fundId) : null;

        $analytics = (new Analytics($this->fundManager(), $date))->setCurrency($currency);

        if ($unitFund) {
            $analytics->setFund($unitFund);
        }

        return $this->respond([
            'cost_value' => $analytics->costValue(),
            'market_value' => $analytics->marketValue(),
            'adjusted_market_value' => $analytics->adjustedMarketValue(),
            'weighted_tenor' => $analytics->weightedTenor(),
            'weighted_rate' => $analytics->weightedRate()
        ]);
    }

    public function depositAnalysis($currency_id)
    {
        $currency = Currency::findOrFail($currency_id);

        $date = Carbon::today();

        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['date'])) {
            $date = Carbon::parse($state['search']['predicateObject']['date']);
        }

        $data = (new Analysis($currency, $this->fundManager(), $date))->generate();

        $total = [
            'Cost value' => $data->sum('Cost value'),
            'Gross Interest' => $data->sum('Gross Interest'),
            'Adjusted Gross Interest' => $data->sum('Adjusted Gross Interest'),
            'Adjusted Market Value' => $data->sum('Adjusted Market Value'),
            'Market Value' => $data->sum('Market Value'),
            'Exposure' => $data->sum('Exposure')
        ];

        return $this->respond(['data' => $data->values(), 'total' => $total]);
    }

    /**
     * @param $investor
     * @return mixed
     */
    private function searchStatus($investor, $currency_id, $state)
    {
        return $investor->whereHas('depositHoldings', function ($q) use ($state, $currency_id) {
            if (isset($state['search']['predicateObject']['active']) &&
                $state['search']['predicateObject']['active'] == 'active') {
                $q->where(function ($q) {
                    $q->whereNull('withdrawn')->orWhere('withdrawn', 0);
                });
            }
            $q->whereHas('investTransaction', function ($transaction) use ($currency_id) {
                $transaction->whereHas('custodialAccount', function ($account) use ($currency_id) {
                    $account->where('currency_id', $currency_id);
                });
            })->forFundManager();
        });
    }

    /**
     * @param $period
     * @param $currency_id
     */
    public function maturityProfile($period, $currency_id)
    {
        $currency = Currency::findOrFail($currency_id);

        $start = Request::get('start');

        $end = Request::get('end');

        $p_weeklies = [];

        if ($period == 'weekly') {
            $p_weeklies = (new MaturityRepository())->getWeeklyMaturityAnalysisFor($start, $end, $currency);
        } elseif ($period == 'monthly') {
            $p_weeklies = (new MaturityRepository())->getMonthlyMaturityAnalysisFor($start, $end, $currency);
        }

        $p_amounts = $p_weeklies->lists('amount');
        $p_dates = $p_weeklies->lists('start');

        $p_in_millions = new \Illuminate\Support\Collection();
        foreach ($p_amounts as $amount) {
            $p_in_millions->push(number_format($amount / 1000000, 2, '.', ''));
        }
        $p_amounts = $p_in_millions;


        $product = Product::where('currency_id', $currency->id)->first();

        if (!is_null($product)) {
            $c_weeklies = [];

            if ($period == 'weekly') {
                $c_weeklies = (new \Cytonn\Investment\MaturityRepository())
                    ->getWeeklyMaturityAnalysisFor($start, $end, $product);
            } elseif ($period == 'monthly') {
                $c_weeklies = (new \Cytonn\Investment\MaturityRepository())
                    ->getMonthlyMaturityAnalysisFor($start, $end, $product);
            }

            $c_amounts = $c_weeklies->lists('amount');

            $in_millions = new Collection();

            foreach ($c_amounts as $amount) {
                $in_millions
                    ->push(number_format($amount / 1000000, 2, '.', ''));
            }

            $c_amounts = $in_millions;
        } else {
            $c_amounts = 0;
            $c_dates = null;
        }

        //format dates
        $dates = new Collection();

        foreach ($p_dates as $date) {
            $dates->push($date->toFormattedDateString());
        }

        $var = [$p_amounts, $dates, $c_amounts];

        return $var;
    }

    public function investors()
    {
        return PortfolioInvestor::all();
    }

    public function assetAllocation()
    {
        return $this->processTable(
            new PortfolioAllocationLimit(),
            new PortfolioAllocationLimitTransformer(),
            function ($allocation) {
                $allocation = $this->searchByUnitFund($allocation);
                return $allocation->orderBy('updated_at', 'ASC');
            }
        );
    }

    public function searchByUnitFund($allocation)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['unit_fund_id'])) {
            $unit_fund_id = (bool)$state['search']['predicateObject']['unit_fund_id'];

            return $allocation->where('fund_id', $unit_fund_id);
        }

        return $allocation;
    }
}
