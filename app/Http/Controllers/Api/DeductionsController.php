<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientDeduction;
use Cytonn\Api\Transformers\DeductionsTransformer;
use Cytonn\Models\ClientInvestmentWithdrawal;

/**
 * Class InvestmentsController
 *
 * @package Api
 */
class DeductionsController extends ApiController
{
    /**
     * Show investments grid
     *
     * @return string
     */
    public function getDeductions()
    {
        return $this->processTable(
            new ClientInvestmentWithdrawal(),
            new DeductionsTransformer(),
            function ($model) {
                return $model->where('withdraw_type', 'deduction')->orderBy('date', 'DESC');
            }
        );
    }
}
