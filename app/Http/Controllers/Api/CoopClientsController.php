<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use Cytonn\Api\Transformers\ClientTransformer;

/**
 * Class CoopClientsController
 *
 * @package Api
 */
class CoopClientsController extends ApiController
{
    /**
     * Generate the clients grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new Client(),
            new ClientTransformer(),
            function ($model) {
                $model = $this->filterBySentStatus($model);
                return $model->whereNotNull('referee_id');
            }
        );
    }

    /**
     * @param $model
     * @return mixed
     */

    private function filterBySentStatus($model)
    {
        $confirm = \Request::get('tableState');

        if (isset($confirm['search']['predicateObject']['confirmation'])) {
            $confirmation = $confirm['search']['predicateObject']['confirmation'];

            if ($confirmation === 'sent') {
                return $model->has('client');
            } elseif ($confirmation === 'not_sent') {
                return $model->doesntHave('client');
            }
        }

        return $model;
    }
}
