<?php
/**
 * Date: 09/02/2016
 * Time: 3:12 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Notification;
use Cytonn\Api\Transformers\NotificationTransformer;
use Cytonn\Notifier\NotificationRepository;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Resource\Collection;

class NotificationsController extends ApiController
{
    public function notifications()
    {
        return $this->processTable(new Notification(), new NotificationTransformer(), function ($query) {
            return $query->whereHas('users', function ($user) {
                $user->where('users.id', \Auth::id());
            })->latest();
        });
    }

    public function getAllUnread()
    {
        $notifications = (new NotificationRepository())->getAllUnreadNotificationsForUser(\Auth::user());

        $resource = new Collection($notifications, new NotificationTransformer());

        return $this->fractal->createData($resource)->toJson();
    }

    public function getUnreadCount()
    {
        return (new NotificationRepository())->getUnreadNotificationCount(\Auth::user());
    }
}
