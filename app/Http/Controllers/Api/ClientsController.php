<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\Product;
use Cytonn\Api\Transformers\ClientSearchTransformer;
use Cytonn\Api\Transformers\ClientSummaryTransformer;
use Cytonn\Api\Transformers\ClientTransformer;
use Cytonn\Clients\ClientRules;
use League\Fractal\Resource\Collection;

class ClientsController extends ApiController
{
    use ClientRules;

    public function index()
    {
        return $this->processTable(
            new Client(),
            new ClientTransformer(),
            function ($model) {
                $state = \Request::get('tableState');

                if (isset($state['search']['predicateObject']['complete'])) {
                    $code = $state['search']['predicateObject']['complete'];

                    if (!$code) {
                        return $model->where(
                            function ($q) {
                                $q->where('complete', '!=', 1)->orWhereNull('complete');
                            }
                        )->forFundManager();
                    }

                    return $model->where('complete', 1)->forFundManager();
                }

                if (isset($state['search']['predicateObject']['investment_type'])) {
                    $type = $state['search']['predicateObject']['investment_type'];

                    if ($type == 0) {
                        return $model->whereHas('investments');
                    } elseif ($type == 1) {
                        return $model->whereHas('unitHoldings');
                    } elseif ($type == 2) {
                        return $model->whereHas('unitFundPurchases');
                    } elseif ($type == 3) {
                        return $model->whereHas('shareHoldings');
                    }
                }

                return $model->forFundManager();
            }
        );
    }

    public function summary($productId)
    {
        $product = Product::findOrFail($productId);

        return $this->processTable(
            new Client(),
            new ClientSummaryTransformer($product),
            function ($client) use ($product) {
                $filtered = $client->whereHas(
                    'investments',
                    function ($q) use ($product) {
                        $q->where('product_id', $product->id);
                    }
                )->orderBy('client_code', 'ASC');

                $filtered = $this->searchByClientCode($filtered);

                return $filtered;
            }
        );
    }

    private function searchByClientCode($model)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];

            return $model->where('client_code', $code);
        }

        return $model;
    }

    public function search()
    {
        $name = request()->get('name');

        $clients = Client::whereNull('clients.deleted_at')
            ->search($name)
            ->where('clients.deleted_at', null)
            ->paginate(20);

        $resource = new Collection($clients, new ClientTransformer());

        return $this->fractal->createData($resource)->toJson();
    }

    public function productPaymentsBalance($client_id, $product_id)
    {
        $client = Client::findOrFail($client_id);
        $product = Product::findOrFail($product_id);
        return (new ClientPayment())->balance($client, $product);
    }

    public function allClients()
    {
        $clients = Client::search(request()->get('query'))->paginate(50);

        $resource = new Collection($clients, new ClientSearchTransformer());

        return $this->fractal->createData($resource)->toJson();
    }

    public function clientBankAccounts($id)
    {
        $client = Client::findOrFail($id);

        $clientId = $client->id;


        $clientAccounts = (new ClientBankAccount())->where('client_id', $clientId)
            ->get()->map(
                function ($acc) {
                    return [
                        'id' => $acc->id,
                        'name' => $acc->account_name . ' ' . '-' . ' ' . @$acc->bank->name . ' ' . '-' . ' ' .
                            $acc->account_number,
                        'swift_code' => $acc->branch->swift_code
                    ];
                }
            );

        return response([$clientAccounts]);
    }

    public function bankDetails($id)
    {
        $details = (new ClientbankAccount())->find($id);
        $branch = ($details->branch_id) ? (new ClientBankBranch())->find($details->branch_id) : '';

        return [
            'bank' => ($branch) ? (new ClientBank())->find($branch->bank_id)->name : '',
            'branch' => ($branch) ? $branch->name : '',
            'account_name' => $details->account_name,
            'account_number' => $details->account_number
        ];
    }

    public function signature()
    {
        $input = request()->all();

        $action = 'add_client_signature';

        if (isset($input['id'])) {
            $action = 'update_client_signature';
        } else {
            $validation = $this->clientSignatureUpload(request());

            if ($validation) {
                return response()->json([
                    'errors' => $validation->messages()->getMessages(),
                    'status' => 442
                ]);
            }
        }

        $client = Client::findOrFail($input['client_id']);

        $clientSignature = ClientSignature::where('client_id', $client->id);

        $file = request()->file('file');

        if (isset($input['joint_client_holder_id'])) {
            $clientSignature =
                $clientSignature->where('joint_client_holder_id', $input['joint_client_holder_id']);
        }

        if (isset($input['client_contact_person_id'])) {
            $clientSignature =
                $clientSignature->where('client_contact_person_id', $input['client_contact_person_id']);
        }

        if (isset($input['commission_recepient_id'])) {
            $clientSignature =
                $clientSignature->where('commission_recepient_id', $input['commission_recepient_id']);
        }

        $clientSignature = $clientSignature->first();

        if ($clientSignature) {
            $document = $clientSignature->document;

            if ($file) {
                $document->updateDoc(
                    file_get_contents($file),
                    $file->getClientOriginalExtension(),
                    'client_signatures'
                );
            }
        } else {
            $document = Document::make(
                file_get_contents($file),
                $file->getClientOriginalExtension(),
                'client_signatures'
            );
        }

        $input['document_id'] = $document->id;

        unset($input['file']);

        ClientTransactionApproval::add([
            'client_id' => $client->id,
            'payload' => $input,
            'scheduled' => false,
            'transaction_type' => $action
        ]);

        \Flash::message('Client signature saved for approval');

        return response()->json([
            'status' => 200
        ]);
    }
}
