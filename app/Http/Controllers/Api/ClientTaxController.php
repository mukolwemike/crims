<?php
/**
 * Date: 22/03/2016
 * Time: 4:45 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTax;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Api\Transformers\ClientOngoingTaxTransformer;
use Cytonn\Api\Transformers\ClientTaxTransformer;
use League\Fractal\Resource\Collection;

/**
 * Class ClientTaxController
 *
 * @package Api
 */
class ClientTaxController extends ApiController
{
    /**
     * @param $product_id
     * @return string
     */
    public function taxDue($product_id)
    {
        $product = Product::findOrFail($product_id);

        $taxes = new ClientTax();

        $filtered = $this->filter($taxes)
            ->whereHas(
                'investment',
                function ($q) use ($product) {
                    $q->where('product_id', $product->id);
                }
            );

        $filtered = $this->filterTaxDueByClientCode($filtered);
        $filtered = $this->filterTaxDueByDate($filtered);



        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new ClientTaxTransformer());

        $this->addPaginationToResource($paginated, $resource);

        $resource->setMetaValue('totals', $this->calculateTaxDueTotals($filtered));

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * Filter by client code: strict
     *
     * @param  $model
     * @return mixed
     */
    private function filterTaxDueByClientCode($model)
    {
        $state = \Request::get('tableState');

        //filter
        if (isset($state['search']['predicateObject']['client_code'])) {
            $query = $state['search']['predicateObject']['client_code'];

            $model =  $model->whereHas(
                'client',
                function ($qu) use ($query) {
                    $qu->where('client_code', $query);
                }
            );
        }

        return $model;
    }

    /**
     * Filter between due date
     *
     * @param  $model
     * @return mixed
     */
    private function filterTaxDueByDate($model)
    {
        $state = \Request::get('tableState');

        //filter
        if (isset($state['search']['predicateObject']['date'])) {
            $date = $state['search']['predicateObject']['date'];

            if (isset($date['before']) && isset($date['after'])) {
                $model =  $model->where('date', '>=', $date['after'])->where('date', '<=', $date['before']);
            } elseif (isset($date['before'])) {
                $model =  $model->where('date', '<=', $date['before']);
            } elseif (isset($date['after'])) {
                $model =  $model->where('date', '>=', $date['after']);
            }
        }

        return $model;
    }

    /**
     * Calculate totals for filtered
     *
     * @param  $filteredModel
     * @return array
     */
    private function calculateTaxDueTotals($filteredModel)
    {
        $taxes = $filteredModel->get();

        $amount = 0;
        $gross = 0;

        foreach ($taxes as $tax) {
            $amount += $tax->investment->amount;
            $gross += $tax->investment->repo->getFinalGrossInterest();
        }
        return [
            'amount'=>$amount,
            'gross_interest'=>$gross,
            'tax'=>$filteredModel->sum('amount')
        ];
    }

    /**
     * Get the tax per client for period
     *
     * @param  $product_id
     * @return string
     */
    public function taxPerClientForPeriod($product_id)
    {
        $product = Product::findOrFail($product_id);

        $client = new Client();

        $state = \Request::get('tableState');

        $date = Carbon::now();

        if (isset($state['search']['predicateObject']['date'])) {
            $date = new Carbon($state['search']['predicateObject']['date']);
        }

        $filtered = $this->filter($client)->whereHas(
            'investments',
            function ($q) use ($product_id, $date) {
                $q->where('product_id', $product_id)
                                ->where('invested_date', '<=', $date->copy()->endOfMonth())
                                ->where('maturity_date', '>=', $date->copy()->startOfMonth());
            }
        );

        $filtered = $this->filterOngoingTaxByClientCode($filtered);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new ClientOngoingTaxTransformer(new Carbon($date), $product));

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * @param $model
     * @return mixed
     */
    private function filterOngoingTaxByClientCode($model)
    {
        $state = \Request::get('tableState');

        //filter
        if (isset($state['search']['predicateObject']['client_code'])) {
            $query = $state['search']['predicateObject']['client_code'];

            $model = $model->where('client_code', $query);
        }

        return $model;
    }
}
