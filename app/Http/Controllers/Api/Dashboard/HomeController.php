<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Http\Controllers\Api\Dashboard;

use App\Cytonn\Dashboard\DashboardChartsSummary;
use App\Cytonn\Dashboard\DashboardSummaryGenerator;
use App\Http\Controllers\Controller;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Dashboard\DashboardGenerator;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function withdrawalsStats()
    {
        $generator = new DashboardGenerator();

        return response()->json($generator->withdrawals($this->fundManager()));
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function inflowsStats()
    {
        $generator = new DashboardGenerator();

        $fundManager = $this->fundManager();

        return response()->json($generator->inflows($fundManager));
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function clientsStats()
    {
        $generator = new DashboardGenerator();

        $resp = $generator->clients($this->fundManager());

        return response()->json($resp);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function aumStats()
    {
        $fundManager = $this->fundManager();

        $generator = new DashboardGenerator();

        return response()->json($generator->aum($fundManager));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function matureWithdrawals()
    {
        $generator = new DashboardSummaryGenerator();

        return response()->json($generator->matureWithdrawals($this->fundManager()));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function prematureWithdrawals()
    {
        $generator = new DashboardSummaryGenerator();

        return response()->json($generator->prematureWithdrawals($this->fundManager()));
    }

    public function activeInvestments()
    {
        $generator = new DashboardSummaryGenerator();

        return response()->json($generator->activeInvestments($this->fundManager()));
    }

    public function pendingInvestmentTransactions()
    {
        $generator = new DashboardSummaryGenerator();

        return response()->json($generator->pendingInvestmentTransactions());
    }

    public function activePortfolioInvestments()
    {
        $generator = new DashboardSummaryGenerator();

        return response()->json($generator->activePortfolioInvestments());
    }

    public function pendingPortfolioTransactions()
    {
        $generator = new DashboardSummaryGenerator();

        return response()->json($generator->pendingPortfolioTransactions());
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function fundValuation()
    {
        $fundManager = $this->fundManager();

        $generator = new DashboardChartsSummary();

        return response()->json($generator->fundValuation($fundManager));
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function reValuation()
    {
        $generator = new DashboardChartsSummary();

        return response()->json($generator->reValuationChart());
    }
}
