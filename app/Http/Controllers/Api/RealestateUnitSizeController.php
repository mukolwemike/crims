<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\RealestateUnitSize;
use Cytonn\Realestate\RealestateUnitSizes\RealestateUnitSizeTransformer;

class RealestateUnitSizeController extends ApiController
{
    /*
     * Generate the real estate unit sizes table
     */
    public function getRealestateUnitSizes()
    {
        return $this->processTable(new RealestateUnitSize(), new RealestateUnitSizeTransformer());
    }
}
