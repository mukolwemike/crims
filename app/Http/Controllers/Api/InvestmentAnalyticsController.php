<?php
/**
 * Date: 13/08/2016
 * Time: 1:39 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Investment\Summary\Analytics;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Http\Request;

/**
 * Class InvestmentAnalyticsController
 *
 * @package Api
 */
class InvestmentAnalyticsController extends ApiController
{
    public function export(Request $request, $type, $entity_id)
    {
        $date = $request->get('date');
        $from_date = \Request::get('from_date');
        $email = \Auth::user()->email;

        $this->queue(function () use ($type, $entity_id, $date, $from_date, $email) {
            if ($type == 'products') {
                $entity = Product::find($entity_id);
            } else {
                $entity = Currency::find($entity_id);
            }

            $date = Carbon::parse($date);
            $from_date = Carbon::parse($from_date);

            $data = [
                'As At Date' => $date->toFormattedDateString(),
                'From Date' => $from_date->toFormattedDateString(),
                'Entity' => $entity->name
            ];

            $c = function ($amount, $positive = false, $decimals = 2) {
                return AmountPresenter::currency($amount, $positive, $decimals);
            };

            $output = [
                'Cost Value' => $c($this->costValue($type, $entity_id, $date)),
                'Market Value' => $c($this->marketValue($type, $entity_id, $date)),
                'Custody Fees' => $c($this->custodyFees($type, $entity_id, $date)),
                'Withholding Tax' => $this->withholdingTax($type, $entity_id, $date),
                'Residual Income' => $this->residualIncome($type, $entity_id, $date),
                'Weighted Rate' => $this->weightedRate($type, $entity_id, $date),
                'Weighted Tenor' => $this->weightedTenor($type, $entity_id, $date),
                'Persistence' =>
                    $c($this->persistence($type, $entity_id, $date, $from_date), false, 4),
            ];

            Mail::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject('Investment Analytics Report')
                ->view(
                    'emails.reports.generic',
                    ['blocks' => [
                        [
                            'intro' => 'See below the analytics for the following data',
                            'table' => $data
                        ],
                        [
                            'table' => $output
                        ]
                    ]]
                )->send();
        });

        return $this->setStatusCode(202)->respond([]);
    }

    public function costValue($type, $entity_id, $date)
    {
        if ($type == 'products') {
            $product = Product::findOrFail($entity_id);
            return (new Analytics($this->fundManager(), $date))
                ->setProduct($product)
                ->setBaseCurrency($product->currency)
                ->costValue();
        }

        return (new Analytics($this->fundManager(), $date))->setCurrency(Currency::findOrFail($entity_id))->costValue();
    }

    /**
     * @param $type
     * @param $entity_id
     * @return mixed
     */
    public function getCostValue(Request $request, $type, $entity_id)
    {
        $date = new Carbon($request->get('date'));

        return $this->costValue($type, $entity_id, $date);
    }

    /**
     * @param $type
     * @param $entity_id
     * @return int
     */
    public function getMarketValue(Request $request, $type, $entity_id)
    {
        $date = new Carbon($request->get('date'));

        return $this->marketValue($type, $entity_id, $date);
    }

    public function marketValue($type, $entity_id, $date)
    {
        if ($type == 'products') {
            $product = Product::findOrFail($entity_id);

            return (new Analytics($this->fundManager(), $date))
                ->setProduct($product)
                ->setBaseCurrency($product->currency)
                ->marketValue();
        }

        return (new Analytics($this->fundManager(), $date))
            ->setCurrency(Currency::findOrFail($entity_id))->marketValue();
    }

    /**
     * @param $type
     * @param $entity_id
     * @return int
     */
    public function getCustodyFees($type, $entity_id)
    {
        $date = new Carbon(\Request::get('date'));

        return $this->custodyFees($type, $entity_id, $date);
    }

    public function custodyFees($type, $entity_id, $date)
    {
        if ($type == 'products') {
            return Product::findOrFail($entity_id)->repo->custodyFeesAtDate($date);
        }

        return Product::where('currency_id', $entity_id)
            ->get()
            ->sum(
                function ($product) use ($date) {
                    return $product->repo->custodyFeesAtDate($date);
                }
            );
    }

    /**
     * @param $type
     * @param $entity_id
     * @return mixed
     */
    public function getWithholdingTax($type, $entity_id)
    {
        $date = new Carbon(\Request::get('date'));

        return $this->withholdingTax($type, $entity_id, $date);
    }

    public function withholdingTax($type, $entity_id, $date)
    {
        return $this->marketValue($type, $entity_id, $date) - $this->costValue($type, $entity_id, $date);
    }

    /**
     * @param $type
     * @param $entity_id
     * @return float
     */
    public function getResidualIncome($type, $entity_id)
    {
        $date = new Carbon(\Request::get('date'));

        return $this->residualIncome($type, $entity_id, $date);
    }

    public function residualIncome($type, $entity_id, $date)
    {
        if ($type == 'products') {
            return (new Analytics(FundManager::all(), $date))
                ->setProduct(Product::findOrFail($entity_id))
                ->setBaseCurrency(Currency::findOrFail($entity_id))
                ->residualIncome(null, $date);
        }

        return (new Analytics($this->fundManager(), $date))
            ->setCurrency(Currency::findOrFail($entity_id))->residualIncome(null, $date);
    }

    /**
     * @param $type
     * @param $entity_id
     * @return float|int
     */
    public function getWeightedRate($type, $entity_id)
    {
        $date = new Carbon(\Request::get('date'));

        return $this->weightedRate($type, $entity_id, $date);
    }

    public function weightedRate($type, $entity_id, $date)
    {
        $analytics = (new Analytics($this->fundManager(), $date));
        if ($type == 'products') {
            $product = Product::findOrFail($entity_id);

            return $analytics->setProduct($product)->setBaseCurrency($product->currency)->weightedRate();
        }

        return $analytics->setCurrency(Currency::findOrFail($entity_id))->weightedRate();
    }

    /**
     * @param $type
     * @param $entity_id
     * @return int|number
     */
    public function getWeightedTenor($type, $entity_id)
    {
        $date = new Carbon(\Request::get('date'));

        return $this->weightedTenor($type, $entity_id, $date);
    }

    public function weightedTenor($type, $entity_id, $date)
    {
        $analytics = (new Analytics($this->fundManager(), $date));
        if ($type == 'products') {
            $product = Product::findOrFail($entity_id);

            return $analytics->setBaseCurrency($product->currency)->setProduct($product)->weightedTenor();
        }

        return $analytics->setCurrency(Currency::findOrFail($entity_id))->weightedTenor();
    }

    /**
     * @param $type
     * @param $entity_id
     * @return float|int
     */
    public function getPersistence($type, $entity_id)
    {
        $date = new Carbon(\Request::get('date'));
        $from_date = new Carbon(\Request::get('from_date'));

        return $this->persistence($type, $entity_id, $date, $from_date);
    }

    public function persistence($type, $entity_id, $date, $from_date)
    {
        $analytics = (new Analytics($this->fundManager(), $date));
        if ($type == 'products') {
            $product = Product::findOrFail($entity_id);

            return $analytics->setBaseCurrency($product->currency)->setProduct($product)
                ->persistence($from_date, $date);
        }

        return $analytics->setCurrency(Currency::findOrFail($entity_id))->persistence($from_date, $date);
    }

    /*
     * Export the investment values
     */
}
