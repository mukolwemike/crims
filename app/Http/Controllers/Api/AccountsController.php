<?php
/**
 * Date: 15/03/2017
 * Time: 13:01
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;

class AccountsController extends ApiController
{
    public function compatibility($account_id)
    {
        $account = CustodialAccount::findOrFail($account_id);

        $product = Product::find(\Request::get('product'));
        $project = Project::find(\Request::get('project'));
        $entity = SharesEntity::find(\Request::get('entity'));

        if (($product && $project) || ($product && $entity) || ($project && $entity)) {
            throw new \InvalidArgumentException('Only one product/project/entity should be provided');
        }

        if ($product) {
            return [
                'compatible' => $product->currency->id == $account->currency->id,
                'from_currency'=>$product->currency,
                'to_currency' =>  $account->currency
            ];
        }

        if ($project) {
            return [
                'compatible'=>$project->currency->id == $account->currency->id,
                'from_currency'=>$project->currency,
                'to_currency' =>  $account->currency
            ];
        }

        if ($entity) {
            return [
                'compatible'=>$entity->currency->id == $account->currency->id,
                'from_currency'=>$entity->currency,
                'to_currency' =>  $account->currency
            ];
        }

        throw new \InvalidArgumentException('Please specify a product/project/entity');
    }

    public function summary($id)
    {
        $account = CustodialAccount::findOrFail($id);

        $start = new Carbon(\Request::get('start'));
        $end = new Carbon(\Request::get('end'));

        return $this->respond([
            'transactions' => $this->transactionTypeSummary($start, $end, $account),
            'products'=> $this->transactionProductSummary($start, $end, $account),
            'transfers' => $this->transferSummary($start, $end, $account)
        ]);
    }

    private function transferSummary($start, $end, $account)
    {
        $transferIn = function ($sender) use ($account, $start, $end) {
            return $account->transactions()->between($start, $end)
                ->whereHas('transferIn', function ($transfer) use ($sender) {
                    $transfer->whereHas('sender', function ($sending_trans) use ($sender) {
                        $sending_trans->where('custodial_account_id', $sender->id);
                    });
                })
                ->ofType('TI')
                ->sum('amount');
        };

        $transferOut = function ($receiver) use ($account, $start, $end) {
            return $account->transactions()->between($start, $end)
                ->whereHas('transferOut', function ($transfer) use ($receiver) {
                    $transfer->whereHas('receiver', function ($receiving_trans) use ($receiver) {
                        $receiving_trans->where('custodial_account_id', $receiver->id);
                    });
                })
                ->ofType('TO')
                ->sum('amount');
        };

        return CustodialAccount::all()
                ->map(function ($account) use ($transferIn, $transferOut) {
                    return [
                        'name' => $account->full_name,
                        'in' => $transferIn($account),
                        'out' => abs($transferOut($account))
                        ];
                })
                ->filter(function ($account) {
                    return ($account['out'] != 0) || ($account['in'] != 0);
                })->all();
    }

    private function transactionProductSummary($start, $end, $account)
    {
        $amountForProduct = function ($product, $comparator) use ($start, $end, $account) {
            return $account->transactions()->between($start, $end)
                ->where('amount', $comparator, 0)
                ->whereHas('clientPayment', function ($payment) use ($product) {
                    if ($product instanceof Product) {
                        return $payment->where('product_id', $product->id);
                    }
                    if ($product instanceof Project) {
                        return $payment->where('project_id', $product->id);
                    }
                    if ($product instanceof SharesEntity) {
                        return $payment->where('share_entity_id', $product->id);
                    }

                    throw new \InvalidArgumentException('An instance of product/project/entity must be provided');
                })->sum('amount');
        };

        return (new Collection())
            ->merge(Product::all()->all())
            ->merge(Project::all()->all())
            ->merge(SharesEntity::all()->all())
            ->each(function ($product) use ($amountForProduct) {
                $product->inflows = $amountForProduct($product, '>');
                $product->outflows = abs($amountForProduct($product, '<'));
            })
            ->filter(function ($product) {
                return ($product->inflows != 0) || ($product->outflows != 0);
            })
            ->map(function ($product) {
                return (object) [
                    'name' => $product->name,
                    'inflows' => $product->inflows,
                    'outflows' => $product->outflows
                ];
            });
    }

    private function transactionTypeSummary($start, $end, $account)
    {
        $amountForType = function ($type) use ($start, $end, $account) {
            return  $account->transactions()->between($start, $end)->ofType($type)->sum('amount');
        };

        $types = CustodialTransactionType::all()
            ->each(
                function ($type) use ($amountForType) {
                    $type->amount = $amountForType($type->name);
                }
            )
            ->filter(
                function ($type) {
                    return $type->amount != 0;
                }
            );

        $trans = [];
        foreach ($types as $t) {
            $trans[$t->description] = $t->amount;
        }

        return [
                'Opening Balance' => $account->balance($start->subDay()->endOfDay())
            ]
            + $trans +
            [
                'Closing Balance' => $account->balance($end->endOfDay())
            ];
    }
}
