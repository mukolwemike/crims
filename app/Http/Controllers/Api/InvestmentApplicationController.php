<?php
/**
 * Date: 08/02/2016
 * Time: 9:17 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Investment\Transformers\ClientFilledInvestmentApplicationTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\CompanyNatures;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Title;
use Cytonn\Api\Transformers\InvestmentApplicationTransformer;
use Cytonn\Investment\Rules\InvestmentApplicationRules;
use Cytonn\Unitization\ApplicationFormPartials;

class InvestmentApplicationController extends ApiController
{
    use InvestmentApplicationRules;

    public function applications()
    {
        return $this->processTable(
            new ClientInvestmentApplication(),
            new InvestmentApplicationTransformer(),
            function ($application) {
                $application = $this->searchByStatus($application);

                return $application->forFundManager()->latest();
            }
        );
    }

    private function searchByStatus($application)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['status'])) {
            $status = $state['search']['predicateObject']['status'];

            if ($status === 'invested') {
                return $application->has('investment');
            } elseif ($status === 'not_invested') {
                return $application->doesntHave('investment');
            }
        }

        return $application;
    }

    public function validateRisk()
    {
        $validator = $this->riskAssesment(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        return response(['status' => 202]);
    }

    public function validateInvestment()
    {
        $validator = $this->investmentDetails(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        return response(['status' => 202]);
    }

    public function saveEntryApplication()
    {
        $validator = $this->validateEntryLevelApplication(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        $data = request()->except('client_type', 'jointHolders', 'product_category');

        $data['individual'] = ($input['client_type'] == 'individual') ? 1 : 0;

        if (!isset($data['new_client'])) {
            $data['new_client'] = 1;
        }

        if (!isset($data['corporate_investor_type'])) {
            $data['corporate_investor_type'] = 1;
        }

        if (isset($data['client_id'])) {
            $data = $this->existingClientDetails($data);
        }

        if (isset($data['contact_persons'])) {
            $data['contact_persons'] = json_encode($data['contact_persons']);
        }

        $application = (new ClientFilledInvestmentApplication())->repo->saveApplicationInstruction($data);

        if ($application->uuid) {
            $application->jointHolders()->createMany(request()->input('jointHolders'));
        }

        return response(['created' => true, 'status' => 202]);
    }

    public function validateEntryLevelApplication($input)
    {
        $applicationType = $input['client_type'];

        if (isset($input['client_id']) && $input['client_id'] != null) {
            $validator = $this->shortExistingApplicationForm(request());
        } else {
            $validator = ($applicationType == 'individual')
                ? $this->shortIndividualApplicationForm(request())
                : $this->shortCorporateApplicationForm(request());
        }

        return $validator;
    }

    public function saveApplication()
    {
        $input = request()->all();

        $validator = $this->validateInvestmentForm(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $data = request()->except('client_type', 'jointHolders');
        $data['individual'] = ($input['client_type'] == 'individual') ? 1 : 0;

        if (!isset($data['corporate_investor_type'])) {
            $data['corporate_investor_type'] = 1;
        }

        if (isset($data['application_id'])) {
            $application = $this->saveExistingInstruction($data);
        } else {
            $application = $this->saveNewApplicationInstruction($data);

            if ($application->uuid) {
                $application->jointHolders()->createMany(request()->input('jointHolders'));
            }
        }

        $application_name = ($input['client_type'] == 'individual')
            ? Title::find($application->title)->name . ' ' . $application->firstname . ' ' .
            $application->middlename . ' ' . $application->lastname
            : $application->registered_name;

        return response([
            'created' => true,
            'status' => 202,
            'application_uuid' => $application->uuid,
            'application_name' => $application_name
        ]);
    }

    public function saveExistingInstruction($data)
    {
        $appId = $data['application_id'];
        if (isset($data['client_id'])) {
            $data = $this->existingClientDetails($data);
        }

        $application = (new ClientFilledInvestmentApplication())->findOrFailByUuid($appId);
        unset($data['application_id'], $data['client_id']);

        return $application->repo->saveApplicationInstruction($data);
    }

    public function saveNewApplicationInstruction($data)
    {
        if (isset($data['client_id'])) {
            $data = $this->existingClientDetails($data);
        }
        unset($data['application_type'], $data['client_id']);

        $application = new ClientFilledInvestmentApplication();
        return $application->repo->saveApplicationInstruction($data);
    }

    public function existingClientDetails($input)
    {
        $client = (new Client())->findOrFail($input['client_id']);
        $client = (new InvestmentApplicationTransformer())->allDetails($client);

        return $input = array_merge($input, $client);
    }

    public function validateInvestmentForm($input)
    {
        $applicationType = $input['client_type'];

        if (isset($input['client_id']) && $input['client_id'] != null) {
            $validator = $this->existingSubscriber(request());
        } else {
            $validator = ($applicationType == 'individual')
                ? $this->individualSubscriber(request())
                : $this->corporateSubscriber(request());
        }

        return $validator;
    }

    public function validateJointHolder()
    {
        $input = request()->all();

        if ($input['holder_type'] == 2) {
            $validator = $this->jointExistingHolder(request());
        } elseif ($input['holder_type'] == 3) {
            $validator = $this->jointDuplicateHolder(request());
        } elseif ($input['holder_type'] == 1) {
            $validator = $this->jointNewHolder(request());
        }

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        // check if not duplicate application has duplicate details and flag
        if ($input['holder_type'] == 1) {
            $validator = $this->checkDuplicateJointHolder();
        }
        
        if ($validator) {
            return response(
                [
                    'errors' => ['duplicate_client' =>
                        'The Joint-Holder details provided are duplicate. Apply as duplicate '],
                    'status' => 423
                ]
            );
        }

        return response(['status' => 202]);
    }

    public function checkDuplicateJointHolder()
    {
        $query = ClientJointDetail::where('id_or_passport', request('id_or_passport'))
            ->orWhere('pin_no', request('pin_no'))
            ->orWhere('email', request('email'));

        if (request('telephone_home')) {
            $query->orWhere('telephone_home', request('telephone_home'));
        }

        if (request('telephone_cell')) {
            $query->orWhere('telephone_cell', request('telephone_cell'));
        }

        return $query->exists();
    }

    public function updateApplication()
    {
        $input = request()->except('product_category', 'product');

        $application = ClientFilledInvestmentApplication::findOrFailByUuid($input['uuid']);

        $application->repo->saveApplicationInstruction($input);

        return response(['created' => true, 'status' => 202]);
    }

    public function uploadKyc()
    {
        $slug = request()->input('slug');
        $appId = request()->input('application_id');
        $file = request()->file('file');


        if ($appId == "undefined" || $appId == null) {
            return response(
                json_encode([
                    'application' =>
                        ['Please complete the investment and subscriber stages before uploading documents']
                ]),
                422
            )->header('Content-Type', 'application/json');
        }

        $application = ClientFilledInvestmentApplication::findOrFailByUuid($appId);

        if ($application->repo->aboveDocumentLimit($slug)) {
            return response(
                json_encode([
                    'file' => ['You cannot upload more than 5 files in a category']]),
                422
            )->header('Content-Type', 'application/json');
        }

        if (str_contains($file->getClientMimeType(), 'pdf') ||
            str_contains($file->getClientMimeType(), 'image')) {
            $application->repo->saveKycDocument($file, $slug, null);
        } else {
            return response(
                json_encode(['file' => ['The file should be an  or pdf']]),
                422
            )->header('Content-Type', 'application/json');
        }

        return response(['created' => true]);
    }

    public function kycComplete($appId)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($appId);

        $application->progress_kyc = 1;
        $application->save();

        return response(['created' => true, 'status' => 202]);
    }

    public function getKycDocuments($uuid)
    {
        $application = ClientFilledInvestmentApplication::findOrFailByUuid($uuid);
        $application = (new ClientFilledInvestmentApplicationTransformer())->transform($application);

        return response($application);
    }

    public function paymentComplete($appId)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($appId);

        $application->progress_payment = 1;
        $application->save();

        return response(['created' => true]);
    }

    public function mandateComplete($appId)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($appId);

        $application->progress_mandate = 1;
        $application->save();

        return response(['created' => true, 'status' => 202]);
    }

    public function uploadPayment()
    {
        $slug = request()->input('slug');

        $file = request()->file('file');

        $application = ClientFilledInvestmentApplication::findOrFailByUuid(request()->input('application_id'));

        if ($application->repo->aboveDocumentLimit($slug)) {
            return response(
                json_encode([
                    'file' => ['You cannot upload more than 5 files in a category']]),
                422
            )->header('Content-Type', 'application/json');
        }
        if (str_contains($file->getClientMimeType(), 'pdf') ||
            str_contains($file->getClientMimeType(), 'image')) {
            $application->repo->savePaymentDocument($file, $slug, null);
        } else {
            return response(
                json_encode(['file' => ['The file should be an image or pdf']]),
                422
            )->header('Content-Type', 'application/json');
        }

        return response(['created' => true]);
    }

    public function applicationComplete($uuid)
    {
        $app = ClientFilledInvestmentApplication::findOrFailByUuid($uuid);

        if ($app->progress_investment && $app->progress_subscriber && !$app->complete) {
            $app->complete = true;
            $app->save();

            return response(['created' => true, 'status' => 202]);
        } elseif ($app->complete) {
            return response(['created' => true, 'status' => 202]);
        } else {
            return response(['error' => $this->getIncompleteMessage($app)]);
        }
    }

    private function getIncompleteMessage($app)
    {
        if (!$app->progress_investment) {
            return 'Please complete the investment stage of the application';
        } elseif (!$app->progress_subscriber) {
            return 'Please complete the subscriber stage of the application';
        } elseif (!$app->progress_kyc) {
            return 'Please upload all the KYC documents in the kyc stage of the application';
        } else {
            return 'The application is not complete, check the previous steps';
        }
    }

    public function titles()
    {
        return Title::all();
    }

    public function gender()
    {
        return Gender::all();
    }

    public function contactMethods()
    {
        return ContactMethod::get(['id', 'description'])->toArray();
    }

    public function businessNatures()
    {
        return CompanyNatures::get(['id', 'name'])->toArray();
    }

    public function employmentTypes()
    {
        return (new Employment())->get(['id', 'name'])->toArray();
    }

    public function sourceOfFunds()
    {
        return FundSource::where('active', '=', 1)
            ->orderBy('weight')->get(['id', 'name'])->toArray();
    }

    public function countries()
    {
        return (new Country())->get(['id', 'name'])->toArray();
    }

    public function getProducts($type = null)
    {
        $products = is_null($type)
            ? Product::where('active', true)->get()
            : Product::where('active', true)->whereHas(
                'type',
                function ($t) use ($type) {
                    $t->where('slug', $type);
                }
            )->get();

        return response([$products]);
    }

    public function formPartials()
    {
        return (new ApplicationFormPartials())->formPartials();
    }
}
