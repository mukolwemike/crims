<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankBranch;
use Cytonn\Api\Transformers\BankBranchTransformer;
use Cytonn\Api\Transformers\BankTransformer;

/**
 * Class BanksController
 *
 * @package Api
 */
class BanksController extends ApiController
{
    /**
     * Generate the Banks grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new ClientBank(),
            new BankTransformer(),
            function ($model) {
                return $model->orderBy('name', 'asc');
            },
            null,
            true
        );
    }

    /**
     * Generate the Bank Branches grid page
     *
     * @param  ClientBank $bank
     * @return string
     */
    public function branches(ClientBank $bank)
    {
        return $this->processTable(
            new ClientBankBranch(),
            new BankBranchTransformer(),
            function ($model) use ($bank) {
                return $model->where('bank_id', $bank->id);
            }
        );
    }

    public function allBanks()
    {
        return ClientBank::all()->map(
            function ($bank) {
                return (new BankTransformer())->transform($bank);
            }
        );
    }

    public function bankBranches(ClientBank $bank)
    {
        $branches = $bank->branches()->orderBy('name', 'asc')->get();

        return $branches->map(function ($branch) {
            return (new BankBranchTransformer())->transform($branch);
        });
    }

    /**
     * @param ClientBank $bank
     * @return \Illuminate\Http\JsonResponse
     */
    public function branchesList(ClientBank $bank)
    {
        $branches = $bank->branches()->orderBy('name', 'asc')->get()->lists('name', 'id');
        return response()->json($branches);
    }
}
