<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Mailers\Shares\PurchasePaymentReminderMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesPurchaseOrder;
use Carbon\Carbon;
use Cytonn\Api\Transformers\SharesPurchaseOrderTransformer;
use Cytonn\Models\Shares\ShareCommissionRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class SharesPurchaseOrderController
 *
 * @package Api
 */
class SharesPurchaseOrderController extends ApiController
{
    /**
     * Generate the purchases grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new SharesPurchaseOrder(),
            new SharesPurchaseOrderTransformer(),
            function ($purchases) {
                $purchases = $this->filterByCancelled($purchases);
                return $this->filterByMatch($purchases);
            }
        );
    }

    /**
     * @param $share_holder_id
     * @return string
     */
    public function shareholderPurchases($share_holder_id)
    {
        return $this->processTable(
            new SharesPurchaseOrder(),
            new SharesPurchaseOrderTransformer(),
            function ($model) use ($share_holder_id) {
                $model = $this->filterByCancelled($model)->where('buyer_id', $share_holder_id);
                return $this->filterByMatch($model)->where('buyer_id', $share_holder_id);
            }
        );
    }

    /**
     * @param $purchases
     * @return mixed
     */
    private function filterByMatch($purchases)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['matched'])) {
            $state = $state['search']['predicateObject']['matched'];
            switch ($state) {
                case 'matched':
                    return $purchases->where('matched', 1);
                    break;
                case 'unmatched':
                    return $purchases->whereNull('matched')->orWhere('matched', 0);
                    break;
                default:
                    return $purchases;
                    break;
            }
        }
        return $purchases;
    }

    private function filterByCancelled($purchases)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['cancelled'])) {
            $state = $state['search']['predicateObject']['cancelled'];
            switch ($state) {
                case 'cancelled':
                    return $purchases->where('cancelled', 1);
                    break;
                case 'active':
                    return $purchases->whereNull('cancelled')->orWhere('cancelled', 0);
                    break;
                default:
                    return $purchases;
                    break;
            }
        }
        return $purchases;
    }

    /*
     * Get the share commission rates for the linked recipient
     */
    public function getShareCommissionRates($recipientId)
    {
        //Date
        //Client
        //Recipient


        $recipient = CommissionRecepient::findOrFail($recipientId);

        if ($recipient->zero_commission) {
            $rates[] = [
                'name' => 'Zero Commission 0%',
                'rate' => 0
            ];
        } else {
            $rates = ShareCommissionRate::where('commission_recipient_type_id', $recipient->recipient_type_id)
                ->orderBy('date', 'DESC')->get(['name', 'rate']);
        }

        return Response::json($rates);
    }

    /*
     * Get the share commission recipient rate
     */
    public function getShareCommissionRecipientRate(Request $request)
    {
        if ($request->has('client_id')) {
            $client = Client::find($request->client_id);

            if ($client && $client->franchise == 1) {
                return [
                    'rate' => 0,
                    'rate_name' => 'Franchise Client - 0%'
                ];
            }
        }

        $recipient = CommissionRecepient::find($request->get('recipient_id'));

        if (is_null($recipient)) {
            return [
                'rate' => 0,
                'rate_name' => 'No Recipient - 0%'
            ];
        }

        if ($recipient->zero_commission == 1) {
            return [
                'rate' => 0,
                'rate_name' => 'Zero Commission - 0%'
            ];
        }

        $date = Carbon::parse($request->get('date'));

        $rate = $recipient->repo->getSharesCommissionRate($date);

        if (is_null($rate)) {
            return [
                'rate' => 0,
                'rate_name' => 'No Commission Set - 0%'
            ];
        }

        return [
            'rate' => $rate->rate,
            'rate_name' => $rate->name
        ];
    }

    public function sendReminders()
    {
        $ids = request()->all();

        $purchases = SharePurchases::whereIn('id', $ids)->get();

        $purchases->each(function ($purchase) {

            $purchase_amount = $purchase->purchase_price * $purchase->number;

            $stamp_duty_amount = $purchase_amount/100;

            $amount_required = ($purchase_amount + $stamp_duty_amount);


            if ($purchase->shareHolder->sharePaymentsBalance() < $amount_required) {
                $mailer = new PurchasePaymentReminderMailer();

                $mailer->remind($purchase);
            }

            return;
        });
    }
}
