<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Project;
use Carbon\Carbon;
use Cytonn\Api\Transformers\BulkCommissionPaymentsTransformer;
use Cytonn\Api\Transformers\RealEstateCommissionRecipientTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use League\Fractal\Resource\Collection;

/**
 * Class RealEstateCommissionController
 *
 * @package Api
 */
class RealEstateCommissionController extends ApiController
{
    /**
     * Show the commission grid table
     *
     * @return string
     */
    public function index()
    {
        $state = \Request::get('tableState');

        $date = Carbon::today();

        if (isset($state['search']['predicateObject']['month'])) {
            $date = new Carbon($state['search']['predicateObject']['month']);
        }

        $bulk = BulkCommissionPayment::where('start', '<=', $date)->where('end', '>=', $date)->first();

        if ($bulk) {
            $start = $bulk->start;
            $end = $bulk->end;
        } else {
            $start = BulkCommissionPayment::latest()->first()->end->addDay();
            $end = Carbon::today();
        }


        $filter = function ($model) use ($start, $end) {
            return $model->whereHas(
                'realEstateCommissions',
                function ($commission) use ($start, $end) {
                    $commission->whereHas(
                        'schedules',
                        function ($schedule) use ($start, $end) {
                            $schedule->where('date', '>=', $start)->where('date', '<=', $end);
                        }
                    );
                }
            );
        };

        $meta = function (Collection $resource) use ($start, $end) {
            $resource->setMetaValue('start', $start->copy()->toDateString());
            $resource->setMetaValue('end', $end->copy()->toDateString());
        };

        return $this->processTable(
            new CommissionRecepient(),
            new RealEstateCommissionRecipientTransformer($start, $end),
            $filter,
            $meta
        );
    }

    /**
     * @return string
     */
    public function paymentDates()
    {
        return $this->processTable(
            new BulkCommissionPayment(),
            new BulkCommissionPaymentsTransformer(),
            function ($bcp) {
                return $bcp->whereNotNull('realestate_approval_id');
            }
        );
    }

    /*
     * Get the commission rate for the given recipient
     */
    public function getRecipientCommissionRate(Request $request)
    {
        if ($request->has('client_id')) {
            $client = Client::find($request->client_id);

            if ($client && $client->franchise == 1) {
                return collect([
                    'id' => '',
                    'amount' => 0,
                    'rate_name' => 'Franchise Commission - 0%'
                ]);
            }
        }

        $project = Project::findOrFail($request->get('projectId'));

        $recipient = CommissionRecepient::findOrFail($request->get('recipientId'));

        if ($recipient->zero_commission) {
            return collect([
                'id' => '',
                'amount' => 0,
                'rate_name' => 'Zero Commission - 0%'
            ]);
        }

        $date = $request->get('date') ? Carbon::parse($request->get('date')) : Carbon::today();

        $rate = $recipient->repo->getRealEstateCommissionRate($project, $date);

        if (is_null($rate)) {
            return collect([
                'id' => '',
                'amount' => 0,
                'rate_name' => 'No Commission Set - 0%'
            ]);
        }

        return collect($rate)->put('rate_name', $rate->recipient_type_and_amount);
    }
}
