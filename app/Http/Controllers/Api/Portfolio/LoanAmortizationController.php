<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/4/18
 * Time: 12:44 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Portfolio\Deposits\LoanAmortizationGenerator;
use App\Http\Controllers\Controller;

class LoanAmortizationController extends Controller
{
    public function schedule($id)
    {
        $holding = DepositHolding::findorFail($id);

        return (new LoanAmortizationGenerator($holding))->generate();
    }
}
