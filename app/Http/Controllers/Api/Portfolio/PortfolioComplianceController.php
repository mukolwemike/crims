<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/6/18
 * Time: 3:31 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\FundComplianceBenchmark;
use App\Cytonn\Models\Portfolio\FundComplianceType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\FundComplianceBenchmarkLimitsTransformer;
use Cytonn\Api\Transformers\Portfolio\FundComplianceBenchmarkTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundTransformer;

class PortfolioComplianceController extends controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
        return $this->sortFilterPaginate(
            new FundComplianceBenchmark(),
            [],
            function ($compliance) {
                return app(FundComplianceBenchmarkTransformer::class)->transform($compliance);
            },
            function ($model) {
                return $model;
            }
        );
    }

    public function store()
    {
        $input = request()->all();

        PortfolioTransactionApproval::add([
            'institution_id'    =>  null,
            'transaction_type'  =>  'compliance_benchmarks',
            'payload'           =>  $input
        ]);

        return response(['created' => true]);
    }

    public function show($id)
    {
        $benchmark = FundComplianceBenchmark::findOrFail($id);

        $limits = $benchmark->benchmarkLimits->map(function ($limit) {
            return (new FundComplianceBenchmarkLimitsTransformer())->transform($limit);
        });

        $unitFunds = $this->getUnitFunds($benchmark);

        $benchmark = (new FundComplianceBenchmarkTransformer())->transform($benchmark);

        return response([
            'benchmark' => $benchmark,
            'limits' => $limits,
            'unitFunds' => $unitFunds
        ]);
    }

    public function getUnitFunds(FundComplianceBenchmark $benchmark)
    {
        return UnitFund::whereHas('fundCompliance', function ($compliance) use ($benchmark) {
            return $benchmark->where('benchmark_id', $benchmark->id);
        })
        ->get()
        ->map(function ($unitFund) {
            return (new UnitFundTransformer())->transform($unitFund);
        });
    }
}
