<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/24/18
 * Time: 12:32 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Api\Transformers\Portfolio\WithholdingTaxTransformer;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialWithdrawType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\WithholdingTax;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Portfolio\Rules\WithholdingWithdrawal;

class WithholdingTaxController
{
    use WithholdingWithdrawal, AlternateSortFilterPaginateTrait;

    public function taxes($id)
    {
        $custodial = CustodialAccount::findOrFail($id);

        $filters = $this->filters(request()->all());

        return $this->sortFilterPaginate(

            new WithholdingTax(),
            [],
            function ($withholding) {
                return app(WithholdingTaxTransformer::class)->transform($withholding);
            },
            function ($model) use ($custodial, $filters) {

                $model = $model->forAccount($custodial->id)->whereNull('paid');

                if (!is_null($filters['start_date'])) {
                    $model = $model->where('date', '>=', $filters['start_date']->copy()->startOfDay());
                }

                if (!is_null($filters['end_date'])) {
                    $model =  $model = $model->where('date', '<=', $filters['end_date']->copy()->endOfDay());
                }

                if (!is_null($filters['client_code'])) {
                    $model = $model->forClient($filters['client_code']);
                }

                if (!is_null($filters['type'])) {
                    $model = $model->where('type', $filters['type']);
                }

                return $model;
            }
        );
    }

    private function filters($request)
    {
        $filters = [];

        $filters['start_date'] = isset($request['filters']['start'])
            ? Carbon::parse($request['filters']['start'])
            : null;

        $filters['end_date'] = isset($request['filters']['end'])
            ? Carbon::parse($request['filters']['end'])
            : null;

        $filters['client_code'] = isset($request['filters']['client_code'])
            ? $request['filters']['client_code']
            : null;

        $filters['type'] = isset($request['filters']['type'])
            ? $request['filters']['type']
            : null;

        return $filters;
    }

    public function store()
    {
        $validator = $this->createWithdrawal(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $type = CustodialWithdrawType::where('name', 'Withholding Tax')->first();

        $input = request()->all();

        $input['amount'] = WithholdingTax::whereIn('id', $input['selectedWHT'])
            ->sum('amount');

        $input['transaction'] = $type->id;

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'custodial_payment',
            'payload' => $input
        ]);

        return response([ 'created' => true, 'status' => 202 ]);
    }

    public function calculate()
    {
        $input = request()->all();

        $taxes = WithholdingTax::whereIn('id', $input)->sum('amount');

        return response()->json($taxes);
    }
}
