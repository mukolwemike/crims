<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\BondInterestPaymentSchedule;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\DepositType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\BondInterestPaymentScheduleTransformer;
use Cytonn\Api\Transformers\Portfolio\DepositHoldingTransfomer;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Portfolio\Rules\DepositSecuritiesRules;

class DepositsController extends Controller
{
    use AlternateSortFilterPaginateTrait, DepositSecuritiesRules;

    public function depositHoldings($id)
    {
        $security = (new PortfolioSecurity())->find($id);

        return $this->sortFilterPaginate(
            new DepositHolding(),
            [],
            function ($deposit) {
                return app(DepositHoldingTransfomer::class)->transform($deposit);
            },
            function ($model) use ($security) {
                return $model->where('portfolio_security_id', $security->id);
            }
        );
    }

    public function create()
    {
        $validator = $this->depositHoldingCreate(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input = request()->all();

        $portoflioSecurity = PortfolioSecurity::find($input['portfolio_security_id']);

        if (DepositType::find($input['deposit_type_id'])->slug == 'bond') {
            throw new CRIMSGeneralException("Please use order module to create bonds");
        }

        PortfolioTransactionApproval::add([
            'institution_id' => $portoflioSecurity->portfolio_investor_id,
            'transaction_type' => 'create_deposit_holding',
            'payload' => $input
        ]);

        return response([
            'created' => true,
            'status' => 202
        ]);
    }

    public function holdingDetails($id, $holdingId)
    {
        $holding = (new DepositHolding())->find($holdingId);

        return $this->sortFilterPaginate(
            new DepositHolding(),
            [],
            function ($deposit) {
                return app(DepositHoldingTransfomer::class)->transform($deposit);
            },
            function ($model) use ($holding) {
                return $model->where('id', $holding->id);
            }
        );
    }

    public function depositTypes()
    {
        $types = DepositType::all();

        return response(['types' => $types]);
    }

    public function portfolioDepositTypes()
    {
        return response(PortfolioInvestmentType::all());
    }

    public function taxRates()
    {
        $rates = Setting::where('type', 'WHT')
            ->orderBy('value', 'DESC')
            ->get()
            ->map(function ($setting) {
                $desc = $setting->description ? $setting->description : $setting->key;

                return [
                    'name' => $desc . ' - ' . $setting->value.'%',
                    'id' => $setting->id
                ];
            });

        return response(['rates' => $rates]);
    }

    /**
     * @param $id
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function interestSchedules($id)
    {
        return $this->sortFilterPaginate(
            new BondInterestPaymentSchedule(),
            [],
            function ($schedule) {
                return app(BondInterestPaymentScheduleTransformer::class)->transform($schedule);
            },
            function ($model) use ($id) {
                return $model->where('deposit_holding_id', $id);
            }
        );
    }
}
