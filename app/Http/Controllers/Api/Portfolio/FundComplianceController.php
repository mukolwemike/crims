<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/10/18
 * Time: 8:28 AM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\FundComplianceBenchmark;
use App\Cytonn\Models\Portfolio\FundComplianceType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Portfolio\Orders\Compliance;
use App\Http\Controllers\Controller;
use Cytonn\Api\Transformers\Portfolio\FundComplianceBenchmarkLimitsTransformer;

class FundComplianceController extends controller
{
    public function fundCompliance()
    {
        $data = (request()->all());

        $unitFund = UnitFund::findOrFail($data['unit_fund_id']);

        $account = isset($data['custodial_account_id']) ? CustodialAccount::find($data['custodial_account_id']) : null;

        $unitFund->value = $data['value'];

        $unitFund->custodial_account_id = $data['custodial_account_id'];

        $unitFund->account_name = @$account->account_name;

        $security = PortfolioSecurity::findOrFail($data['security_id']);

//        $fundCompliance = new Compliance($unitFund, $security, $data['value']);

        return response([
            'status' => [],
            'unitFund' => $unitFund,
        ]);
    }

    public function fundComplianceList()
    {
        $data = (request()->all());

        $unitFund = UnitFund::findOrFail($data['unit_fund_id']);

        $security = PortfolioSecurity::findOrFail($data['security_id']);

        $fundCompliance = new Compliance($unitFund, $security, $data['value']);

        $benchmarkLimits = ($fundCompliance->benchmarkLimit())
            ? (new FundComplianceBenchmarkLimitsTransformer())
                ->transform($fundCompliance->benchmarkLimit())
            : $fundCompliance->benchmarkLimit() ;

        return response([
            'status' => $fundCompliance->status(),
            'benchmarkLimits' => $benchmarkLimits,
            'liquidityLimits' => $fundCompliance->liquidityLimits(),
            'noGoZone' => $fundCompliance->noGoZone(),
            'unitFund' => $unitFund,
            'performance' => $unitFund->performances->last()
        ]);
    }

    public function complianceTypes()
    {
        return response(FundComplianceType::all());
    }

    public function complianceList()
    {
        $compliance = FundComplianceBenchmark::all()->map(function ($compliance) {
            return [
                'id' => $compliance->id,
                'name' => $compliance->name,
                'type' => $compliance->type->name,
                'abbr' => $compliance->type->abbr
            ];
        });

        return response($compliance);
    }
}
