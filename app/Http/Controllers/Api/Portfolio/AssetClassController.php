<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\AssetClassTransformer;
use Cytonn\Portfolio\Rules\AssetClassRules;

class AssetClassController extends Controller
{
    use AlternateSortFilterPaginateTrait, AssetClassRules;

    public function index()
    {
        return $this->sortFilterPaginate(
            new AssetClass(),
            [],
            function ($asset) {
                return app(AssetClassTransformer::class)->transform($asset);
            }
        );
    }

    public function store()
    {
        $validator = $this->assetClassCreate(request());
        $input = request()->all();

        $input['slug'] = str_slug($input['name']);

        if ($validator) {
            return response([ 'errors' => $validator->messages()->getMessages()], 422);
        }

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'create_asset_class',
            'payload'=>$input]);

        return response(['created' => true], 202);
    }

    public function show($id)
    {
        $assetClass = AssetClass::find($id);

        return response([ 'fetched' => true, 'data' => $assetClass ], 200);
    }

    public function update($id)
    {
        $asset = AssetClass::find($id);

        $input = request()->all();

        $validator = $this->assetClassUpdate(request(), $input);

        if ($validator) {
            return response([ 'errors' => $validator->messages()->getMessages()], 422);
        }

        $input['asset_class_id'] = $asset->id;
        $input['slug'] = str_slug($input['name']);

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'update_asset_class',
            'payload'=>$input]);

        return response(['updated' => true], 202);
    }
}
