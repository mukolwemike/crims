<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\PortfolioRepaymentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DepositsRepaymentController extends Controller
{
    public function index($id, $holder_id)
    {
        $investment = (new DepositHolding())->findOrFail($holder_id);

        $date_eff = new Carbon(\Request::get('date_eff'));

        $all_repayments = $investment->repayments()->where('date', '<=', $date_eff)->oldest('date')->get();

        $principal_repayments = $investment->repayments()->ofType('principal_repayment')->where(
            'date',
            '<=',
            $date_eff
        )->oldest('date')->get();

        $interest_repayments = $investment->repayments()->ofType('interest_repayment')->where(
            'date',
            '<=',
            $date_eff
        )->oldest('date')->get();

        $repaymenttypes = PortfolioRepaymentType::all()->mapWithKeys(
            function ($repaymenttype) {
                return [$repaymenttype['id'] => $repaymenttype['name']];
            }
        );

        return $repaymenttypes;


        //        return view('portfolio.actions.repayment.index',
        //            [
        //                'investment'=>$investment, 'principal_repayments'=>$principal_repayments,
        // 'interest_repayments'=>$interest_repayments,
        //                'all_repayments'=>$all_repayments,  'title'=>'Portfolio Loan Repayment',
        // 'date_eff'=>$date_eff,
        //                'repaymenttypes'=>$repaymenttypes
        //            ]);
    }

    public function store($id)
    {
        $input = \Request::except('_token');

        $this->investmentRepaymentForm->validate($input);

        $investment = \App\Cytonn\Models\Portfolio\DepositHolding::findOrFail($id);

        $input['investment_id'] = $id;
        $input['date'] = (new Carbon($input['date']))->toDateString();

        PortfolioTransactionApproval::add([
            'institution_id' => $investment->institution->id,
            'transaction_type' => 'portfolio_investment_repayment',
            'payload' => $input
        ]);

        \Flash::message('The repayment transaction has been saved for approval');

        return \Redirect::back();
    }

    public function destroy($id)
    {
        $this->deleteForm->validate(\Request::all());

        $input = \Request::except('_token', '_method');
        $input['repayment_id'] = $id;

        $repayment = PortfolioInvestmentRepayment::findOrFail($id);

        PortfolioTransactionApproval::add([
            'institution_id' => $repayment->investment->institution->id,
            'transaction_type' => 'reverse_portfolio_investment_repayment',
            'payload' => $input]);

        \Flash::message('The repayment transaction reversal has been saved for approval');

        return \Redirect::back();
    }
}
