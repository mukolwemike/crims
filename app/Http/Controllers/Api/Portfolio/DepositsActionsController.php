<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Portfolio\Rules\DepositActionRules;

class DepositsActionsController extends Controller
{
    use DepositActionRules;

    public function rollback()
    {
        $validator = $this->depositActionsValidate(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = \request()->all();

        $deposit = (new DepositHolding())->findOrFail($input['holding_id']);

        if ($deposit->withdrawn) {
            return response([
                    'error' => 'This investment is withdrawn, you first need to rollback the withdrawal',
                    'status' => 221]);
        }

        PortfolioTransactionApproval::add([
                'institution_id' => $deposit->portfolio_investor_id,
                'transaction_type' => 'reverse_deposit_investment',
                'payload' => $input]);

        return response(['status' => 202, 'message' => 'The investment reversal has been saved for approval']);
    }

    public function reverse()
    {
        $validator = $this->depositActionsValidate(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = \request()->all();

        $deposit = (new DepositHolding())->findOrFail($input['holding_id']);

        $child = (new DepositHolding())
            ->where('invest_transaction_approval_id', $deposit->withdraw_transaction_approval_id)
            ->latest()->exists();

        if ($child) {
            return response([
                'error' => 'The investment was rolled over to an existing investment, you cannot reverse the withdrawal'
                , 'status' => 221]);
        }

        PortfolioTransactionApproval::add(['institution_id' => $deposit->portfolio_investor_id, 'transaction_type' =>
            'rollback_deposit_withdrawal', 'payload' => $input]);

        return response([
            'status' => 202,
            'message' => 'The investment withdrawal rollback has been saved for approval']);
    }
}
