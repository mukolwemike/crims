<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/20/18
 * Time: 2:40 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderAllocation;
use App\Cytonn\Models\Portfolio\PortfolioOrderSettlementFee;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Portfolio\Orders\Compliance;
use App\Http\Controllers\Api\ApiController;
use Cytonn\Notifier\NotificationRepository;

class PortfolioOrderAllocationController extends ApiController
{
    public function index()
    {
    }

    public function store()
    {
        $input = request()->all();

        $portfolioOrder = PortfolioOrder::findOrFail($input['portfolio_order_id']);

        $security = $portfolioOrder->security;

        PortfolioTransactionApproval::add([
            'institution_id' => $security->investor->id,
            'transaction_type' => 'create_portfolio_order_allocation',
            'payload' => $input
        ]);

        return response(['created' => true]);
    }

    public function updateAllocation($id)
    {
        $allocation = PortfolioOrderAllocation::findOrFail($id);

        $allocation->status = request('type');

        $allocation->save();

        (new NotificationRepository())
            ->readNotificationWhenPortfolioConfirmationDone($allocation);

        if ($allocation->status == 1) {
            (new NotificationRepository())
                ->createNotificationForPortfolioSettlementDue($allocation);
        }

        return response([
            'created' => true,
            'status' => 200
        ]);
    }

    public function orderStatus(PortfolioOrderAllocation $allocation)
    {
        $order = $allocation->order;

        $fundOrders = $order->unitFundOrders;

        $security = $order->security;

        return $fundOrders->reduce(function ($verdict, $fundOrder) use ($order, $security) {
            $value = ($fundOrder->shares)
                ? $fundOrder->shares * $order->cut_off_price
                : $fundOrder->amount;

            $fundCompliance = new Compliance($fundOrder->unitFund, $security, $value);

            $fundOrder->status = $fundCompliance->status();

            if ($fundOrder->status) {
                return $verdict && false;
            }

            return $verdict && true;
        }, true);
    }

    public function settleEquityOrder()
    {
        $input = request()->all();

        $allocation = PortfolioOrderAllocation::findOrFail($input['id']);

        $allocation->status = 3;

        $allocation->save();

        (new NotificationRepository())
            ->readNotificationWhenPortfolioSettlementDone($allocation);

        PortfolioTransactionApproval::add([
            'institution_id' => $allocation->order->security->investor->id,
            'transaction_type' => 'settle_portfolio_order',
            'payload' => $input
        ]);

        return response(['created' => true, 'status' => 200]);
    }

    public function settleDepositOrder()
    {
        $input = request()->all();

        $allocation = PortfolioOrderAllocation::findOrFail($input['id']);

        $allocation->status = 3;

        $allocation->reject_reason = '';

        $allocation->save();

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'settle_portfolio_order',
            'payload' => $input
        ]);

        return response([
            'created' => true,
            'status' => 200
        ]);
    }

    public function settlementFees()
    {
        $fees = PortfolioOrderSettlementFee::all();

        return response($fees);
    }
}
