<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/20/18
 * Time: 2:40 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Api\Transformers\Portfolio\PortfolioOrderTransformer;
use App\Cytonn\Api\Transformers\Portfolio\UnitFundOrderTransformer;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderLifespanType;
use App\Cytonn\Models\Portfolio\PortfolioOrderType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\PortfolioSecurityTransformer;

class PortfolioOrderController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
        $request = request()->all();

        $fundManager = $this->fundManager();

        return $this->sortFilterPaginate(
            new PortfolioOrder(),
            [],
            function ($order) {
                return app(PortfolioOrderTransformer::class)->transform($order);
            },
            function ($model) use ($request, $fundManager) {
                $model = $this->searchByOrderType($model, $request);
                $model = $this->searchByAssetClass($model, $request);
                $model = $this->searchByFundManager($model, $fundManager);

                return $model;
            }
        );
    }

    public function store()
    {
        $security = PortfolioSecurity::findOrFail(request()->get('security_id'));

        $input = request()->except('security');

        PortfolioTransactionApproval::add([
            'institution_id' => $security->investor->id,
            'transaction_type' => 'create_portfolio_order',
            'payload' => $input
        ]);

        return response(['created' => true]);
    }

    public function show($id)
    {
        $order = PortfolioOrder::findOrFail($id);

        return response((new PortfolioOrderTransformer())->transform($order));
    }

    public function update($id)
    {
        $input = request()->all();

        $portfolioOrder = PortfolioOrder::findOrFail($input['id']);

        PortfolioTransactionApproval::add([
            'institution_id' => $portfolioOrder->security->investor->id,
            'transaction_type' => 'edit_portfolio_order',
            'payload' => $input
        ]);

        return response(['created' => true]);
    }

    public function delete()
    {
        $input = request()->all();

        $portfolioOrder = PortfolioOrder::findOrFail($input['id']);

        PortfolioTransactionApproval::add(
            [
                'institution_id'    =>  $portfolioOrder->security->investor->id,
                'transaction_type'  =>  'remove_portfolio_order',
                'payload'           =>  $input
            ]
        );

        return response(['created' => true]);
    }

    public function fundOrders($id)
    {
        $portfolioOrder = PortfolioOrder::findorFail($id);

        return $portfolioOrder->unitFundOrders
            ->map(function ($order) {
                return (new UnitFundOrderTransformer())->transform($order);
            });
    }

    public function orderTypes()
    {
        return response(PortfolioOrderType::all());
    }

    public function securities()
    {
        $fundManager = $this->fundManager();

        $securities = PortfolioSecurity::where('fund_manager_id', $fundManager->id)->get()->map(function ($security) {
            return (new PortfolioSecurityTransformer())->transform($security);
        });

        return response($securities);
    }


    private function searchBySecurity($form, $request)
    {
        $security_id = (isset($request['filters']['security_id']))
            ? $request['filters']['security_id']
            : null;

        if (!$security_id) {
            return $form;
        }

        return $form->where('security_id', $security_id);
    }
    
    private function searchByOrderType($form, $request)
    {
        $order_type_id = (isset($request['filters']['order_type_id']))
            ? $request['filters']['order_type_id']
            : null;

        if (!$order_type_id) {
            return $form;
        }

        return $form->where('order_type_id', $order_type_id);
    }

    private function searchByAssetClass($form, $request)
    {
        $asset_class_id = (isset($request['filters']['asset_class_id']))
            ? $request['filters']['asset_class_id']
            : null;

        if (!$asset_class_id) {
            return $form;
        }

        return $form->whereHas('security', function ($security) use ($asset_class_id) {
            return $security->whereHas('subAssetClass', function ($sub) use ($asset_class_id) {
                $sub->where('asset_class_id', $asset_class_id);
            });
        });
    }

    private function searchByFundManager($form, FundManager $fundManager)
    {
        return $form->whereHas('security', function ($security) use ($fundManager) {
            $security->where('fund_manager_id', $fundManager->id);
        });
    }

    public function rawOrders($id)
    {
        $portfolioOrder = PortfolioOrder::findOrFail($id);

        $unitFundOrders = $portfolioOrder->unitFundOrders->map(function ($order) {
            return (new UnitFundOrderTransformer())->transform($order);
        });

        return response([
            'portfolioOrder' => $portfolioOrder,
            'unitFundOrders' => $unitFundOrders
        ]);
    }

    public function orderLifespanTypes()
    {
        return response(PortfolioOrderLifespanType::all());
    }
}
