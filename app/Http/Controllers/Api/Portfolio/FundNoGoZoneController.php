<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/11/18
 * Time: 12:27 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Api\Transformers\Portfolio\FundNoGoZoneTransformer;
use App\Cytonn\Models\Portfolio\FundNoGoZone;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;

class FundNoGoZoneController extends controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
        return $this->sortFilterPaginate(

            new FundNoGoZone(),
            [],
            function ($compliance) {
                return app(FundNoGoZoneTransformer::class)->transform($compliance);
            },
            function ($model) {
                return $model;
            }
        );
    }

    public function store()
    {
        $input = request()->all();

        PortfolioTransactionApproval::add(
            [
                'institution_id'    =>  null,
                'transaction_type'  =>  'create_fund_no_go_zone',
                'payload'           =>  $input
            ]
        );

        return response(['created' => true]);
    }
}
