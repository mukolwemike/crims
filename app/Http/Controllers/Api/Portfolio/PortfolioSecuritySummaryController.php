<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundType;
use App\Http\Controllers\Controller;

class PortfolioSecuritySummaryController extends Controller
{
    public function depositSummary($type = '')
    {
        $currencies = (new Currency())->whereHas(
            'accounts',
            function ($accounts) {
                $accounts->has('portfolioInvestments');
            }
        )->get();

        $fundTypes = ['' => 'All'] + (new FundType())->pluck('name', 'id')->toArray();

        return response(
            [
            'title' => 'Deposits Summary',
            'currencies' => $currencies,
            'type' => $type ,
            'fundTypes' => $fundTypes
            ]
        );
    }
}
