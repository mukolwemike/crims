<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\BondHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\TBillTransformer;
use Cytonn\Api\Transformers\Portfolio\TBondTransfomer;
use Cytonn\Portfolio\Rules\BondSecuritiesRules;
use Illuminate\Http\Request;

class BondsController extends Controller
{
    use AlternateSortFilterPaginateTrait, BondSecuritiesRules;

    public function bondHoldings($id)
    {
        $security = (new PortfolioSecurity())->find($id);

        return $this->sortFilterPaginate(
            new BondHolding(),
            [],
            function ($bond) use ($security) {
                if ($security->subAssetClass->slug == 'treasury-bills') {
                    return app(TBillTransformer::class)->transform($bond);
                } elseif ($security->subAssetClass->slug == 'treasury-bonds') {
                    return app(TBondTransfomer::class)->transform($bond);
                }
            },
            function ($model) use ($security) {
                return $model->where('portfolio_security_id', $security->id);
            }
        );
    }

    public function create($securityId)
    {
    }

    public function store($securityId)
    {
        $validator = $this->bondCreate(request());
        $input = request()->all();

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'create_bond_security',
            'payload' => $input]);

        return response(['created' => true, 'status' => 202]);
    }

    public function show($securityId, $id)
    {
        $bond = (new BondHolding())->find($id);
        $security = (new PortfolioSecurity())->find($securityId);

        if ($security->subAssetClass->slug == 'treasury-bills') {
            return (new TBillTransformer())->transform($bond);
        }
        if ($security->subAssetClass->slug == 'treasury-bonds') {
            return (new TBondTransfomer())->transform($bond);
        }
    }

    public function edit($securityId, $id)
    {
    }

    public function update(Request $request, $securityId, $id)
    {
    }

    public function destroy($securityId, $id)
    {
    }
}
