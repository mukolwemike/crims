<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/3/18
 * Time: 9:38 AM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Sector;
use App\Http\Controllers\Controller;

class PortfolioSectorController extends Controller
{
    public function index()
    {
        return response(Sector::all());
    }
}
