<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/27/18
 * Time: 12:09 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Portfolio\Summary\Deposits;
use App\Cytonn\Portfolio\Summary\Equities;
use App\Cytonn\Portfolio\Summary\FundPricingSummary;
use App\Cytonn\Portfolio\Summary\FundSummary;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\DepositHoldingSummaryTransformer;
use Cytonn\Api\Transformers\Portfolio\EquityHoldingSummaryTransformer;
use Cytonn\Core\DataStructures\Carbon;

class PortfolioFundSummaryController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function assetClassSummary($id)
    {
        $date = request('date') ? Carbon::parse(request('date')) : Carbon::today();

        $unitFund = UnitFund::findOrFail($id);

        $summary = (new FundSummary($unitFund, $date->copy()))->summaryByAssetClass();

        return response($summary);
    }

    public function subAssetClassSummary($id)
    {
        $date = request('date') ? Carbon::parse(request('date')) : Carbon::today();

        $unitFund = UnitFund::findOrFail($id);

        $summary = (new FundSummary($unitFund, $date))->summaryByFundType();

        return response($summary);
    }

    public function securitySummary($id)
    {
        $date = request('date') ? Carbon::parse(request('date')) : Carbon::today();

        $unitFund = UnitFund::findOrFail($id);

        $summary = (new FundSummary($unitFund, $date))->summaryBySecurity();

        return response($summary);
    }

    public function fundPricingSummary($id)
    {
        $date = request('date') ? Carbon::parse(request('date')) : Carbon::today();

        $unitFund = UnitFund::findOrFail($id);

        $summary = (new FundPricingSummary($unitFund, $date->copy()))->summary();

        return response($summary);
    }


    public function depositAssets()
    {
        $request = request()->get('filters');

        $fundManager = $this->fundManager();

        $date = request('date') ? Carbon::parse(request('date')) : Carbon::today();

        $id = (!$request) ? UnitFund::forFundManager($fundManager->id)->first()->id: $request['unit_fund_id'];

        return $this->sortFilterPaginate(
            new DepositHolding(),
            [],
            function ($security) {
                return app(DepositHoldingSummaryTransformer::class)->transform($security);
            },
            function ($model) use ($id, $date) {
                return $model->where('unit_fund_id', $id)
                    ->activeOnDate($date);
            }
        );
    }

    public function equityAssets()
    {
        $request = request()->get('filters');

        $fundManager = $this->fundManager();

        $date = request('date') ? Carbon::parse(request('date')) : Carbon::today();

        $id = (!$request) ? UnitFund::forFundManager($fundManager->id)->first()->id: $request['unit_fund_id'];

        return $this->sortFilterPaginate(
            new EquityHolding(),
            [],
            function ($security) {
                return app(EquityHoldingSummaryTransformer::class)->transform($security);
            },
            function ($model) use ($id, $date) {
                return $model->where('unit_fund_id', $id)
                    ->activeAsAt($date);
            }
        );
    }
}
