<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Portfolio\SuspenseTransactions\SuspenseTransactionSearchTransformer;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Illuminate\Support\Facades\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SuspenseTransactionController
{
    use AlternateSortFilterPaginateTrait;

    /**
     * @param $id
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index($id)
    {
        return $this->sortFilterPaginate(
            new SuspenseTransaction(),
            [],
            function ($transaction) {
                return $this->getTransactionDetails($transaction);
            },
            function ($model) use ($id) {
                $model = $model->where('custodial_account_id', $id);

                $model = $this->filterByDate($model);

                return $model;
            }
        );
    }

    /**
     * @param SuspenseTransaction $transaction
     * @return array
     */
    private function getTransactionDetails(SuspenseTransaction $transaction)
    {
        return [
            'id' => $transaction->id,
            'amount' => AmountPresenter::accounting($transaction->amount),
            'date' => DatePresenter::formatDate($transaction->date),
            'bank_reference' => $transaction->bank_reference,
            'transaction_id' => $transaction->transaction_id,
            'outgoing_reference' => $transaction->outgoing_reference,
            'source_identity' => $transaction->source_identity,
            'matched' => $transaction->matched,
            'matched_status' => $transaction->matched == 1 ? 'Yes' : 'No',
            'custodial_account_id' => $transaction->custodial_account_id,
            'actual_amount' => $transaction->amount
        ];
    }

    /**
     * @param $model
     * @return mixed
     */
    private function filterByDate($model)
    {
        $request = \Request::get('filters');

        $start = isset($request['data']['start']) ? $request['data']['start'] : null;

        $end = isset($request['data']['end']) ? $request['data']['end'] : null;

        if ($start) {
            $model = $model->where('date', '>=', $start);
        }

        if ($end) {
            $model = $model->where('date', '<=', $end);
        }

        return $model;
    }

    /**
     * @param $id
     * @return array
     */
    public function details($id)
    {
        $transaction = SuspenseTransaction::findOrFail($id);

        return Response::json($this->getTransactionDetails($transaction));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaymentToDetails()
    {
        $products = Product::where('active', 1)->pluck('name', 'id')->toArray();

        $shareentities = SharesEntity::pluck('name', 'id')->toArray();

        $projects = Project::pluck('name', 'id')->toArray();

        $funds = UnitFund::pluck('name', 'id')->toArray();

        return Response::json([
            'product' => $products,
            'project' => $projects,
            'shares' => $shareentities,
            'funds' => $funds
        ]);
    }

    /**
     * @return string
     */
    public function searchSuspenseTransaction()
    {
        $input = \Request::all();

        $transactions = SuspenseTransaction::search($input['query']);

        if (array_key_exists('transaction_type', $input)) {
            if ($input['transaction_type'] == 'debit') {
                $transactions = $transactions->where('amount', '<', 0);
            } elseif ($input['transaction_type'] == 'credit') {
                $transactions = $transactions->where('amount', '>', 0);
            }
        }

        if (array_key_exists('account', $input)) {
            $transactions = $transactions->where('custodial_account_id', $input['account']);
        }

        if (array_key_exists('matched', $input)) {
            $transactions = $transactions->where('matched', $input['matched']);
        }

        $transactions = $transactions->paginate(50);

        $resource = new Collection($transactions, new SuspenseTransactionSearchTransformer());

        return (new Manager())->createData($resource)->toJson();
    }
}
