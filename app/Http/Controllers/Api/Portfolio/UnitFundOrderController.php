<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/20/18
 * Time: 2:40 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Api\Transformers\Portfolio\PortfolioOrderTransformer;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\PortfolioSecurityTransformer;

class UnitFundOrderController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
    }
}
