<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\ActiveStrategyPriceTrail;
use App\Cytonn\Models\ActiveStrategyTargetPrice;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\EquitySecurityTransformer;
use Cytonn\Api\Transformers\Portfolio\PortfolioSecurityTransformer;
use Cytonn\Portfolio\PortfolioSecurityRepository;
use Cytonn\Portfolio\Rules\PortfolioSecurityRules;

class PortfolioSecurityController extends Controller
{
    use AlternateSortFilterPaginateTrait, PortfolioSecurityRules;

    public function index()
    {
        $request = request()->all();

        $assetClassId = '';
        $subAssetClassId = '';

        if (isset($request['filters']['assetClassId'])) {
            $assetClassId = $request['filters']['assetClassId'];
        }

        if (isset($request['filters']['subAssetClassId'])) {
            $subAssetClassId = $request['filters']['subAssetClassId'];
        }

        $fundManager = $this->fundManager();

        return $this->sortFilterPaginate(
            new PortfolioSecurity(),
            [],
            function ($security) {
                return app(PortfolioSecurityTransformer::class)->transform($security);
            },
            function ($model) use ($fundManager, $assetClassId, $subAssetClassId) {
                return $model->where('fund_manager_id', $fundManager->id)
                    ->whereHas(
                        'subAssetClass',
                        function ($sub) use ($assetClassId, $subAssetClassId) {
                            if ($subAssetClassId) {
                                $sub->where('id', $subAssetClassId);
                            } elseif ($assetClassId) {
                                $sub->where('asset_class_id', $assetClassId);
                            }
                        }
                    );
            }
        );
    }

    public function store()
    {
        $validator = $this->portfolioSecurityCreate(request());

        $input = request()->all();

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(), 'status' => 422]
            );
        }

        $input['code'] = (new PortfolioSecurityRepository())->suggestCode();

        $input['contact_person'] = (isset($input['contact_person'])) ? $input['contact_person'] : null;

        $input['issuer'] = (isset($input['issuer'])) ? $input['issuer'] : null;

        $input['investor_account_name'] =
            (isset($input['investor_account_name'])) ? $input['investor_account_name'] : null;

        $input['investor_account_number'] =
            (isset($input['investor_account_number'])) ? $input['investor_account_number'] : null;

        $branch = (isset($input['investor_bank'])) ? ClientBankBranch::find($input['investor_bank_branch']) : null;

        $input = array_except($input, ['_token', 'files']);

        if (!isset($input['portfolio_investor_id'])) {
            $investor = PortfolioInvestor::create([
                'name' => $input['name'],
                'account_name' => $input['investor_account_name'],
                'account_number' => $input['investor_account_number'],
                'bank_name' => $branch ? $branch->bank->name : null,
                'branch_name' => $branch ? $branch->name : null
            ]);

            $input['portfolio_investor_id'] = $investor->id;
        }

        $security = new PortfolioSecurity();

        unset($input['investor_bank']);
        unset($input['investor_bank_branch']);
        unset($input['investor_account_name']);
        unset($input['investor_account_number']);

        $security->fill($input);

        $security->save();

        $security->update(['portfolio_investor_id' => $investor->id]);

        return response(['created' => true, 'status' => 202]);
    }


    public function show($id)
    {
        $portfolioSecurity = (new PortfolioSecurity)->find($id);
        $assetClass = $portfolioSecurity->subAssetClass->assetClass->slug;

        if ($assetClass == "equities") {
            $portfolioSecurity = (new EquitySecurityTransformer())->transform($portfolioSecurity);
            return response(['fetched' => true, 'data' => $portfolioSecurity], 200);
        }

        $portfolioSecurity = (new PortfolioSecurityTransformer())->transform($portfolioSecurity);

        return response(['fetched' => true, 'data' => $portfolioSecurity], 200);
    }

    public function update($id)
    {
        $validator = $this->portfolioSecurityEdit(request());

        $input = request()->all();

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'edit_portfolio_security',
            'payload' => $input
        ]);

        return response([
            'created' => true,
            'status' => 202
        ]);
    }

    public function securityTypes($security)
    {
        return AssetClass::where('slug', $security)->get()[0]->subAsset;
    }

    public function getSecurityHoldings($security_id, $action)
    {
        $security = PortfolioSecurity::findOrFail($security_id);

        $assetClass = $security->subAssetClass->assetClass->slug;

        if ($assetClass == "deposits") {
            return (new DepositsController())->depositHoldings($security->id);
        } elseif ($assetClass == "equities") {
            if ($action == "purchases") {
                return (new EquitiesController())->equitySharePurchases($security->id);
            } elseif ($action == 'market-price') {
                return $this->sortFilterPaginate(
                    new ActiveStrategyPriceTrail(),
                    [],
                    $this->universalTransformer(),
                    function ($model) use ($security) {
                        return $model->where('security_id', $security->id);
                    }
                );
            } elseif ($action == 'target-price') {
                return $this->sortFilterPaginate(
                    new ActiveStrategyTargetPrice(),
                    [],
                    $this->universalTransformer(),
                    function ($model) use ($security) {
                        return $model->where('security_id', $security->id);
                    }
                );
            } else {
                return (new EquitiesController())->equityShareSales($security->id);
            }
        } elseif ($assetClass == "bonds") {
            return (new BondsController())->bondHoldings($security->id);
        }

        return [];
    }

    public function investmentTypes()
    {
        return PortfolioInvestmentType::all();
    }

    public function getFundForFundManager($id)
    {
        $security = PortfolioSecurity::findOrFail($id);

        return response($security->fundManager->unitFunds);
    }

    public function managerFunds($id)
    {
        $fundManager = FundManager::findOrFail($id);

        $unitFunds = $fundManager->unitFunds()->get();

        return response($unitFunds);
    }

    public function rawSecurities($id)
    {
        $security = PortfolioSecurity::findOrFail($id);

        $security->asset_class = $security->subAssetClass->assetClass->id;
        $security->investor_account_name = $security->investor->account_name;
        $security->investor_account_number = $security->investor->account_number;
        $security->investor_bank = $security->investor->bank_name;
        $security->investor_bank_branch = $security->investor->branch_name;

        return response($security);
    }
}
