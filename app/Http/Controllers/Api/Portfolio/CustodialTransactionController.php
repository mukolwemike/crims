<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-20
 * Time: 11:39
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\CustodialTransactionTransformer;
use Cytonn\Portfolio\CustodialTransactions\CustodialTransactionSearchTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class CustodialTransactionController
{
    use AlternateSortFilterPaginateTrait;

    public function index($id)
    {
        $transactions = $this->sortFilterPaginate(
            new CustodialTransaction(),
            [],
            function ($transaction) {
                return (new CustodialTransactionTransformer())->transform($transaction);
            },
            function ($model) use ($id) {
                $model = $model->whereNull('transferred_transaction_id')->where('custodial_account_id', $id)->whereHas(
                    'transactionType',
                    function ($type) {
                        $type->where('name', 'FI');
                    }
                );

                $model = $this->searchByClientCode($model);
                $model = $this->searchByDates($model);

                return $model;
            }
        );

        return $transactions;
    }

    public function transactions($id)
    {
        return $this->sortFilterPaginate(
            new CustodialTransaction(),
            [],
            function ($transaction) {
                return (new CustodialTransactionTransformer())->transform($transaction);
            },
            function ($model) use ($id) {
                $model = $model->where('amount', '!=', 0)->where('custodial_account_id', $id)
                    ->latest('date')->orderBy('id', 'desc');

                $model = $this->searchByClientCode($model);
                $model = $this->searchByStartEndDates($model);
                $model = $this->searchByTransactionType($model);

                return $model;
            });
    }

    /*
     * Search by the client code and name
     */
    private function searchByClientCode($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $trans->whereHas(
                'client',
                function ($client) use ($code) {
                    $client->where('client_code', 'like', '%' . $code . '%');;
                }
            );
        }
        return $trans;
    }

    /*
     * Search by the start and end dates
     */
    public function searchByDates($trans)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['date']['after'])) {
            $trans = $trans->where('date', '>=', $state['search']['predicateObject']['date']['after']);
        }

        if (isset($state['search']['predicateObject']['date']['before'])) {
            $trans = $trans->where('date', '<=', $state['search']['predicateObject']['date']['before']);
        }

        return $trans;
    }

    public function searchByStartEndDates($trans)
    {
        $state = \Request::get('filters');

        if (isset($state['data']['start'])) {
            $trans = $trans->where('date', '>=', $state['data']['start']);
        }

        if (isset($state['data']['end'])) {
            $trans = $trans->where('date', '<=', $state['data']['end']);
        }

        return $trans;
    }

    public function searchByTransactionType($trans)
    {
        $state = \Request::get('filters');

        if (isset($state['data']['type'])) {
            $query = $state['data']['type'];

            $trans = $trans->where('type', $query);
        }

        return $trans;
    }


    public function calculate()
    {
        $input = request()->all();

        $transactions = CustodialTransaction::whereIn('id', $input)->sum('amount');

        return response()->json($transactions);
    }

    public function transfer($id)
    {
        $input = \Request::all();
        $input['reverse'] = \Request::get('reverse') == 'true';

        $transaction = PortfolioTransactionApproval::add(['institution_id' => null, 'transaction_type' => 'transfer',
            'payload' => $input]);

        \Flash::message('The transfer has been saved for approval');

        return response()->json(['created' => 200, 'transaction' => $transaction->id]);
    }

    /**
     * @return string
     */
    public function searchCustodialTransaction()
    {
        $input = \Request::all();

        $transactions = CustodialTransaction::search($input['query']);

        if (array_key_exists('account', $input)) {
            $transactions = $transactions->where('custodial_account_id', $input['account']);
        }

        if (array_key_exists('transaction_type', $input)) {
            if ($input['transaction_type'] == 'debit') {
                $transactions = $transactions->where('amount', '<', 0);
            } elseif ($input['transaction_type'] == 'credit') {
                $transactions = $transactions->where('amount', '>', 0);
            }
        }

        $transactions = $transactions->paginate(50);

        $resource = new Collection($transactions, new CustodialTransactionSearchTransformer());

        return (new Manager())->createData($resource)->toJson();
    }
}
