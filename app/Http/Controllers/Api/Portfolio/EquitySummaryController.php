<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\Controllers\Api\ApiController;
use Carbon\Carbon;
use Cytonn\Api\Transformers\EquityDividendTransformer;
use Cytonn\Api\Transformers\Portfolio\EquitySecurityTransformer;
use Cytonn\Portfolio\Equities\PortfolioValuation;
use Helper\Unit;
use League\Fractal\Resource\Collection;

class EquitySummaryController extends ApiController
{
    public function index()
    {
        $state = \Request::get('tableState');

        $fundId = (isset($state['search']['predicateObject']['unitFund']))
            ? $state['search']['predicateObject']['unitFund']
            : null;

        $unitFund = ($fundId) ? UnitFund::findOrFail($fundId) : null;

        $securities = $this->equitySecurities($unitFund);

        $paginated = $this->sortFilterPaginate($securities);

        $resource = new Collection($paginated['model'], new EquitySecurityTransformer($unitFund));

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    public function totals()
    {
        $state = \Request::get('tableState');

        $fundId = (isset($state['search']['predicateObject']['unitFund']))
            ? $state['search']['predicateObject']['unitFund']
            : null;

        $unitFund = ($fundId) ? UnitFund::findOrFail($fundId) : null;

        $valuation = new PortfolioValuation($this->fundManager(), Carbon::today());
        
        if ($unitFund) {
            $valuation = $valuation->setFund($unitFund);
        }

        return response()->json([
            'data' => [
                'number_of_shares' => $valuation->numberOfShares(),
                'portfolio_cost' => $valuation->portfolioCost(),
                'portfolio_value' => $valuation->portfolioValue(),
                'portfolio_return' => $valuation->portfolioReturn()
            ]
        ]);
    }

    public function assetAllocation()
    {
        $securities = $this->equitySecurities();

        $names = collect([]);

        $portfolio_costs = collect([]);

        $securities->each(
            function ($security) use ($names, $portfolio_costs) {
                $names->push($security->name);

                try {
                    $portfolio_costs->push(
                        100 *
                        (
                            $security->repo->totalCurrentPurchaseValue() /
                            (new PortfolioValuation($this->fundManager()))->portfolioCost()
                        )
                    );
                } catch (\Exception $e) {
                    $portfolio_costs->push(0);
                }
            }
        );

        return [
            'names' => $names->toArray(),
            'values' => $portfolio_costs->toArray()
        ];
    }

    public function equitySecurities(UnitFund $unitFund = null)
    {

        $eq =  PortfolioSecurity::forAssetClass('equities');

        if ($unitFund) {
            $eq = $eq->whereHas('equityHoldings', function ($holdings) use ($unitFund) {
                $holdings->where('unit_fund_id', $unitFund->id);
            });
        }

        return $eq;
    }

    public function dividends($security_id, $type)
    {
        $dividends = new EquityDividend();

        $filtered = $this->filter($dividends)
            ->where('dividend_type', $type)
            ->where('security_id', $security_id);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new EquityDividendTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }
}
