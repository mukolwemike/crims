<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityPriceTrail;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Portfolio\EquityTargetPrice;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Portfolio\EquityHoldingTransformer;
use Cytonn\Api\Transformers\Portfolio\EquitySecurityTransformer;
use Cytonn\Api\Transformers\Portfolio\EquityShareSaleTransformer;
use Cytonn\Portfolio\Rules\EquitySecuritiesRules;

class EquitiesController extends Controller
{
    use AlternateSortFilterPaginateTrait, EquitySecuritiesRules;

    public function index()
    {
        $security = new PortfolioSecurity();

        return $this->sortFilterPaginate(
            new PortfolioSecurity(),
            [],
            function ($equity) {
                return app(EquitySecurityTransformer::class)->transform($equity);
            },
            function ($model) use ($security) {
                return $model->whereHas(
                    'subAssetClass',
                    function ($sub) {
                        return $sub->whereHas(
                            'assetClass',
                            function ($asset) {
                                return $asset->where('slug', 'equities');
                            }
                        );
                    }
                );
            }
        );
    }

    public function equitySharePurchases($id)
    {
        $security = (new PortfolioSecurity())->find($id);

        return $this->sortFilterPaginate(
            new EquityHolding(),
            [],
            function ($equity) {
                return app(EquityHoldingTransformer::class)->transform($equity);
            },
            function ($model) use ($security) {
                return $model->where('portfolio_security_id', $security->id);
            }
        );
    }

    public function equityShareSales($id)
    {
        $security = (new PortfolioSecurity())->find($id);

        return $this->sortFilterPaginate(
            new EquityShareSale(),
            [],
            function ($sale) {
                return app(EquityShareSaleTransformer::class)->transform($sale);
            },
            function ($model) use ($security) {
                return $model->where('security_id', $security->id);
            }
        );
    }

    public function buyShares()
    {
        $validator = $this->equityShareBuy(request());
        $input = request()->all();

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'buy_equity_share',
            'payload' => $input
        ]);

        return response(['created' => true, 'status' => 202]);
    }

    public function sellShares()
    {
        $validator = $this->equityShareSell(request());
        $input = request()->all();

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'sell_equity_share',
            'payload' => $input
        ]);

        return response(['created' => true, 'status' => 202]);
    }

    public function shareDividends()
    {
        $validator = $this->equityShareDividends(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input = request()->all();

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'equity_shares_dividend',
            'payload' => $input
        ]);

        return response(['created' => true, 'status' => 202]);
    }

    public function store()
    {
        $validator = $this->equityCreate(request());
        $input = request()->all();

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' =>
                'create_equity_security',
            'payload' => $input
        ]);

        return response(['created' => true, 'status' => 202]);
    }

    public function marketPrice($securityId)
    {
        $security = (new PortfolioSecurity())->find($securityId);
        $priceTrail = $security->priceTrail()->latest()->paginate(10);

        return response(['priceTrail' => $priceTrail]);
    }

    public function targetPrice($securityId)
    {
        $security = (new PortfolioSecurity())->find($securityId);
        $targetTrail = $security->targetPrices()->latest()->paginate(10);

        return response(['priceTrail' => $targetTrail]);
    }

    public function saveMarketPriceTrail($security_id)
    {
        $validator = $this->priceTrail(request());
        $input = request()->all();

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        $input['security_id'] = $security_id;

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' =>
                'create_security_market_price_trail',
            'payload' => $input
        ]);

        return response(['created' => true, 'status' => 202]);
    }

    public function saveTargetPriceTrail($id)
    {
        $validator = $this->priceTrail(request());

        $input = request()->all();

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages(), 'status' => 422]);
        }

        $security_id = $id;

        $trail = (new EquityTargetPrice())
            ->where('date', $input['date'])
            ->where('security_id', $security_id)
            ->first();

        is_null($trail) ?
            $trail = new EquityTargetPrice()
            : $security = (new PortfolioSecurity())->findOrFail($security_id);
        ;

        $security = PortfolioSecurity::findOrFail($id);

        $input['security_id'] = $security->id;

        $trail->fill($input);

        $trail->save();

        return response(['created' => true, 'status' => 202]);
    }
}
