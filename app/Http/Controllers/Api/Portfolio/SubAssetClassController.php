<?php

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Portfolio\Rules\SubAssetClassRules;
use Illuminate\Http\Request;

class SubAssetClassController extends Controller
{
    use AlternateSortFilterPaginateTrait, SubAssetClassRules;

    public function index()
    {
    }


    public function store()
    {
    }


    public function show($id)
    {
    }


    public function update($id)
    {
    }

    public function subAssetClasses($id)
    {
        $subAssets = AssetClass::find($id)->subAsset;

        if ($subAssets) {
            return $subAssets->map(
                function ($subAsset) use ($id) {
                    return [
                        'id' => $subAsset->id,
                        'name' => $subAsset->name,
                        'assetClass' => AssetClass::find($id)->name
                    ];
                }
            );
        }

        return [];
    }


    public function saveSubAssetClasses(Request $request, $id)
    {
        $inputs = collect($request->all());

        $inputs->each(
            function ($input) {
                $validator = $this->subAssetClassCreate($input);

                if ($validator) {
                    return response(['errors' => $validator->messages()->getMessages()], 422);
                }
            }
        );

        $data['inputs'] = $inputs;
        $data['asset_class_id'] = $id;

        PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'create_sub_asset_class',
            'payload' => $data
        ]);

        return response(['created' => true], 202);
    }

    public function editSubAssetClass($id)
    {
        $validator = $this->subAssetClassUpdate(request());
        $input = request()->all();

        if ($validator) {
            return response(['errors' => $validator->messages()->getMessages()], 422);
        }

        PortfolioTransactionApproval::add([
                'institution_id' => null,
                'transaction_type' => 'update_sub_asset_class',
                'payload' => $input
        ]);

        return response(['created' => true], 202);
    }
}
