<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/6/18
 * Time: 3:31 PM
 */

namespace App\Http\Controllers\Api\Portfolio;

use App\Cytonn\Api\Transformers\Portfolio\FundLiquidityLimitsTransformer;
use App\Cytonn\Models\Portfolio\FundLiquidityLimit;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;

class PortfolioLiquidityLimitsController extends controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
        return $this->sortFilterPaginate(

            new FundLiquidityLimit(),
            [],
            function ($compliance) {
                return app(FundLiquidityLimitsTransformer::class)->transform($compliance);
            },
            function ($model) {
                return $model;
            }
        );
    }

    public function store()
    {
        $input = request()->all();

        PortfolioTransactionApproval::add(
            [
                'institution_id'    =>  null,
                'transaction_type'  =>  'liquidity_limit',
                'payload'           =>  $input
            ]
        );

        return response(['created' => true]);
    }
}
