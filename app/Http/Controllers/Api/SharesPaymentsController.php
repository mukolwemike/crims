<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ShareHolder;
use Cytonn\Api\Transformers\ClientPaymentTransformer;

/**
 * Class SharesPaymentsController
 *
 * @package Api
 */
class SharesPaymentsController extends ApiController
{
    /**
     * Generate the payments grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new ClientPayment(),
            new ClientPaymentTransformer(),
            function ($payments) {
                $state = \Request::get('tableState');

                if (isset($state['search']['predicateObject']['type'])) {
                    $query = $state['search']['predicateObject']['type'];

                    $payments = $payments->where('type', $query);
                }

                return $payments->whereNotNull('share_entity_id')->orderBy('date', 'DESC');
            }
        );
    }

    public function shareholderPayments($share_holder_id)
    {
        $share_holder = ShareHolder::findOrFail($share_holder_id);

        return $this->processTable(
            new ClientPayment(),
            new ClientPaymentTransformer(),
            function ($model) use ($share_holder) {
                return $model->where([
                    'share_entity_id' => $share_holder->entity_id,
                    'client_id' => $share_holder->client_id
                ]);
            }
        );
    }
}
