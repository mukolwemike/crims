<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 11/09/2018
 * Time: 07:35
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Api\Transformers\RealEstateForfeitureNoticeTransformer;
use App\Cytonn\Models\RealestateForfeitureNotice;

class RealestateForfeitureNoticeController extends ApiController
{
    public function index()
    {
        return $this->processTable(
            new RealestateForfeitureNotice(),
            new RealEstateForfeitureNoticeTransformer(),
            $this->filterQuery()
        );
    }

    /**
     * @param $notice
     * @return mixed
     */
    private function searchByClientCode($notice)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $notice->whereHas(
                'holding',
                function ($holding) use ($code) {
                    $holding->whereHas(
                        'client',
                        function ($client) use ($code) {
                            $client->where('client_code', 'like', '%' . $code . '%');
                        }
                    );
                }
            );
        }

        return $notice;
    }

    /**
     * @param $notice
     * @return mixed
     */
    private function searchByProject($notice)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['project'])) {
            $project = $state['search']['predicateObject']['project'];
            return $notice->whereHas(
                'holding',
                function ($holding) use ($project) {
                    $holding->inProjectIds([$project]);
                }
            );
        }
        return $notice;
    }


    /**
     * @param $notice
     * @return mixed
     */
    private function searchBySent($notice)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['sent_status'])) {
            $filter = $state['search']['predicateObject']['sent_status'];
            return $notice->where('status', $filter);
        }
        return $notice;
    }

    /**
     * @return mixed
     */
    private function filterQuery()
    {
        return function ($model) {
            $model = $model->latest()->whereHas(
                'holding',
                function ($holding) {
                    return $holding->active();
                }
            );
            $model = $this->searchByClientCode($model);
            $model = $this->searchBySent($model);
            return $this->searchByProject($model);
        };
    }
}
