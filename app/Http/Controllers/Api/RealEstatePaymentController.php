<?php
/**
 * Date: 04/07/2016
 * Time: 9:15 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\RealEstatePayment;
use Cytonn\Api\Transformers\RealEstatePaymentTransformer;

/**
 * Class RealEstatePaymentController
 *
 * @package Api
 */
class RealEstatePaymentController extends ApiController
{
    /**
     * Show all payments made
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new RealEstatePayment(),
            new RealEstatePaymentTransformer(),
            function ($model) {
                $model = $this->searchByClientCode($model);

                return $model->has('holding')->latest('date');
            }
        );
    }

    private function searchByClientCode($payment)
    {
        $state = \Request::get('tableState');

        //filter
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];

            return $payment->whereHas(
                'holding',
                function ($holding) use ($code) {
                    $holding->wherehas(
                        'client',
                        function ($client) use ($code) {
                            $client->where('client_code', $code);
                        }
                    );
                }
            );
        }

        return $payment;
    }
}
