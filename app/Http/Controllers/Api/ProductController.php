<?php
namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use Cytonn\Investment\Products\ProductGridTransformer;
use Illuminate\Http\Request;

class ProductController extends ApiController
{
    public function index()
    {
        return $this->processTable(new Product(), new ProductGridTransformer());
    }

    public function getInterestRates(Request $request)
    {
        $input = $request->all();
        $product = (new Product())->findOrFail($input['product_id']);

        if (!isset($input['amount'])) {
            $input['amount'] = 0 ;
        }

        if (isset($input['clientId'])) {
            $client = Client::find($input['clientId']);
            $input['amount'] = $input['amount'] +
                $client->repo->getActiveInvestmentTotalValueForClientForProduct($product);
        }

        $rate = (float) $product->repo->interestRate($input['tenor'], $input['amount']);

        return $this->respond($rate);
    }
}
