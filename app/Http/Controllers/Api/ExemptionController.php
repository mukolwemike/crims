<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\Exemption;
use Cytonn\System\Exemptions\ExemptionTransformer;
use Illuminate\Support\Facades\Request;

class ExemptionController extends ApiController
{
    /*
     * Get the exemptions
     */
    public function exemptions()
    {
        $filter = function ($model) {
            return $this->filterExemptions($model);
        };

        return $this->processTable(new Exemption(), new ExemptionTransformer(), $filter);
    }

    /*
     * Filter the exemptions
     */
    public function filterExemptions($model)
    {
        $state = Request::get('tableState');

        if (isset($state['search']['predicateObject']['date']['after'])) {
            $model = $model->where('start', '>=', $state['search']['predicateObject']['date']['after']);
        }

        if (isset($state['search']['predicateObject']['date']['before'])) {
            $model = $model->where('end', '<=', $state['search']['predicateObject']['date']['before']);
        }

        if (! Request::has('args')) {
            return $model;
        }

        foreach (Request::get('args') as $key => $value) {
            $model = $model->where($key, $value);
        }

        return $model;
    }
}
