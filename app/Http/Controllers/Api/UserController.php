<?php
/**
 * Date: 16/07/2016
 * Time: 9:29 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\User;

/**
 * Class UserController
 *
 * @package Api
 */
class UserController extends ApiController
{
    /**
     * @return string
     */
    public function clients()
    {
        return $this->processTable(new ClientUser());
    }

    /**
     * @return string
     */
    public function users()
    {
        return $this->processTable(new User());
    }
}
