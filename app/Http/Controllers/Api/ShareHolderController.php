<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use Cytonn\Api\Transformers\ShareholderTransformer;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Mailers\System\FlaggedRiskyClientMailer;
use Cytonn\Shares\ShareHolderRepository;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Response;

/**
 * Class ShareHolderController
 *
 * @package Api
 */
class ShareHolderController extends ApiController
{
    protected $mailer;

    public function __construct()
    {
        $this->mailer = new FlaggedRiskyClientMailer();
    }

    /**
     * Generate the Shareholders grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new ShareHolder(),
            new ShareholderTransformer(),
            function ($model) {
                $model = $this->searchByShareHolderNumber($model);
                return $this->searchByEntity($model);
            }
        );
    }

    /**
     * @param $shareholder
     * @return mixed
     */
    private function searchByShareHolderNumber($shareholder)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['shareholder_number'])) {
            $number = $state['search']['predicateObject']['shareholder_number'];
            return $shareholder->where('number', 'like', '%' . $number . '%');
            ;
        }
        return $shareholder;
    }

    /**
     * @param $shareholder
     * @return mixed
     */
    private function searchByEntity($shareholder)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['entity_id'])) {
            $entity = $state['search']['predicateObject']['entity_id'];
            return $shareholder->where('entity_id', $entity);
        }
        return $shareholder;
    }

    /*
     * Get the shares to be redeemed for the shareholder
     */
    public function getShareHolderRedemption($holderId, $date = null)
    {
        $holder = ShareHolder::findOrFail($holderId);

        $date = is_null($date) ? Carbon::now()->endOfDay() : Carbon::parse($date)->endOfDay();

        $shares = $holder->repo->getSharesForRedemption($date);

        $total = $shares->sum('total');

        $totalShares = $shares->sum('number');

        return Response::json([
            'shares' => $shares,
            'total' => $total,
            'total_shares' => $totalShares
        ]);
    }

    /**
     * @return mixed
     * @throws ClientInvestmentException
     */

    public function postRegister()
    {
        $validator = (new ShareHolderRepository())->validateShareHolderApplication(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        //check for risky clients
        if (!Arr::exists(request()->all(), 'risk_reason')) {
            $riskyClients = (new ClientApplicationRepository())->checkRiskyClients(request());

            if (!is_null($riskyClients)) {
                $this->mailer->sendFlaggedRiskyClientNotification($riskyClients);

                return response(
                    [
                        'warning' => [
                            'risk_client' => 'The client details entered belong to a client who has been deemed RISKY'
                        ],
                        'status' => 'risk'
                    ]
                );
            }
        }

        $input = (new ClientApplicationRepository())->cleanApplicationData(request()->all());

        $repo = new ClientRepository();

        $input['number'] = $repo->suggestShareHolderCode();

        $client = null;

        if (isset($input['client_id'])) {
            $client = Client::find($input['client_id']);
        }

        if ($client) {
            $repo = $client->repo;
        }

        $new_client = intval($input['new_client']);

        if ($new_client !== 2) {
            $allow_duplicate = ($new_client == 3) ? true : false;

            $duplicate = $repo->duplicate($input)->exists;

            if ($duplicate && !$allow_duplicate) {
                return response(
                    [
                        'warning' => [
                            'duplicate_client' => 'The details provided belong to another client! 
                            Please apply as a Duplicate'
                        ],
                        'status' => 'duplicate'
                    ]
                );
            }
        }

        $input['shareholder_type'] = ($input['individual'] == 1) ? 'individual' : 'corporate';

        $instruction = ClientTransactionApproval::add([
            'client_id' => (isset($input['client_id'])) ? $input['client_id'] : null,
            'transaction_type' => 'register_shareholder',
            'payload' => $input
        ]);

        return response(['status' => 202, 'transactionID' => $instruction->id]);
    }
}
