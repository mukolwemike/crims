<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstateUnitTranchePricing;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Api\Transformers\RealEstatePaymentPlanTransformer;
use Cytonn\Api\Transformers\RealEstateUnitTranchePricingTransformer;
use League\Fractal\Resource\Collection;

/**
 * Class RealEstateTrancheController
 *
 * @package Api
 */
class RealEstateTrancheController extends ApiController
{
    /**
     * @return string
     */
    public function getPaymentPlans()
    {
        $resource = new Collection(RealEstatePaymentPlan::all(), new RealEstatePaymentPlanTransformer());
        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * @param $holdingId
     * @return string
     */
    public function getAllTrancheUnitPricings($holdingId)
    {
        $unitHolding = UnitHolding::findOrFail($holdingId);

        $pricing = RealEstateUnitTranchePricing::all()->filter(
            function ($pricing) use ($unitHolding) {
                try {
                    return
                        $pricing->size->tranche->project_id == $unitHolding->project->id
                        and
                        $pricing->size->size_id == $unitHolding->unit->size_id
                        and
                        $pricing->size->project_floor_id == $unitHolding->unit->project_floor_id
                        and
                        $pricing->size->real_estate_type_id == $unitHolding->unit->real_estate_type_id;
                } catch (\Exception $e) {
                    return false;
                }
            }
        );

        $resource  = new Collection($pricing, new RealEstateUnitTranchePricingTransformer());

        return $this->fractal->createData($resource)->toJson();
    }
}
