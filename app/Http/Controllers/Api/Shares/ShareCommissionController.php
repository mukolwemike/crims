<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Http\Controllers\Api\Shares;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Http\Controllers\Api\ApiController;
use Carbon\Carbon;
use Crims\Commission\Override;
use Cytonn\Api\Transformers\Shares\ShareCommissionRecipientTransformer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Facades\Response;

/**
 * Class ShareCommissionController
 * @package App\Http\Controllers\Api\Shares
 */
class ShareCommissionController extends ApiController
{
    /**
     * @return string
     */
    public function index()
    {
        $recipient = new CommissionRecepient();

        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['month'])) {
            $date = new Carbon($state['search']['predicateObject']['month']);
        } else {
            $date = Carbon::today();
        }

        $commissionDates = (new BulkCommissionPayment())->commissionDates($date);

        $transformer = new ShareCommissionRecipientTransformer($commissionDates['start'], $commissionDates['end']);

        $modifyResource = function ($resource) use ($commissionDates) {
            return $resource->setMetaValue('dates', [
                'start' => $commissionDates['start']->toDateString(),
                'end' => $commissionDates['end']->toDateString()
            ]);
        };

        return $this->processTable($recipient, $transformer, null, $modifyResource);
    }

    public function getCommissionTotals($recipientId, $start, $end)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $start = Carbon::parse($start);

        $end = Carbon::parse($end);

        $shareSummary = $recipient->shareCommissionCalculator($start, $end)->summary();

        $earned = $shareSummary->total();

        $override = $shareSummary->override();

        $data = [
            'earned' => $earned,
            'override' => $override,
            'total' => $earned + $override
        ];

        return Response::json($data);
    }

    public function getCommissionSchedules($recipientId, $start, $end)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $start = Carbon::parse($start);

        $end = Carbon::parse($end);

        $schedules = $recipient->shareCommissionCalculator($start, $end)->schedules()->orderBy('date')->get();

        $schedulesArray = array();

        foreach ($schedules as $schedule) {
            $purchase = $schedule->sharePurchase;

            $purchaseOrder = $purchase->purchaseOrder;

            $client = $purchase->shareHolder->client;

            $data = [
                'id' => $schedule->id,
                'client_name' => ClientPresenter::presentFullNames($client->id),
                'shares_num' => $purchase->number,
                'purchase_price' => $purchase->purchase_price,
                'total' => $purchase->repo->getPurchasePrice(),
                'value_date' => $purchase->date,
                'rate' => $purchaseOrder->commission_rate,
                'date' => $schedule->date,
                'description' => $schedule->description,
                'amount' => $schedule->amount
            ];

            $data['combined_description'] = "Client : " . $data['client_name'] . ', Shares Num : ' . $data['shares_num']
                . ', Price : ' . $data['purchase_price'] . ', Date : ' . $data['value_date'];

            $schedulesArray[] = $data;
        }

        return Response::json($schedulesArray);
    }

    public function getCommissionOverrides($recipientId, $start, $end)
    {
        $recipient = CommissionRecepient::findOrFail($recipientId);

        $start = Carbon::parse($start);

        $end = Carbon::parse($end);

        $overrideClass = (new Override($recipient, $end, 'shares', $start));

        $overrideRate = $overrideClass->getOverrideRate();

        $recipientCalc = $recipient->shareCommissionCalculator($start, $end);

        $recipientSummary = $recipientCalc->summary();

        $reports = $recipientCalc->getReports();

        $totalSum = 0;

        $overrideArray = array();

        foreach ($reports as $fa) {
            $override = $recipientSummary->overrideOnRecipient($fa);

            $totalSum += $override;

            $overrideArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $recipientSummary->overrideCommission($fa),
                'override' => $override,
                'rate' => $overrideRate
            ];
        }

        return Response::json($overrideArray);
    }
}
