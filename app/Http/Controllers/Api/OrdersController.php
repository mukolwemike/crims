<?php
/**
 * Date: 22/02/2018
 * Time: 10:21
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;

class OrdersController extends ApiController
{
    public function index($stage)
    {
        switch ((int)$stage) {
            case 1:
                $orders = $this->entry();
                break;
            case 2:
                $orders = $this->confirmation();
                break;
            default:
                $stage = (int)$stage;
                $orders = $this->approval($stage - 3);
        }

        return collect($orders)->flatten()
            ->sortByDate('updated_at', 'DESC')
            ->map(
                function ($order) {
                    if ($order instanceof ClientFilledInvestmentApplication) {
                        return (object)$this->presentApplication($order);
                    }

                    if ($order instanceof ClientTopupForm) {
                        return (object)$this->presentTopup($order);
                    }

                    if ($order instanceof ClientInvestmentInstruction) {
                        return (object)$this->presentRollover($order);
                    }
                }
            )->values();
    }

    private function entry()
    {
        //only check for returned
        return [];
    }

    private function confirmation()
    {
        $instr = $this->instructions();

        $apps = $instr['applications']
            ->whereDoesntHave('application')
            ->orWhereHas(
                'application',
                function ($app) {
                    $app->whereDoesntHave('approval');
                }
            )->get();

        $topups = $instr['topups']->whereDoesntHave('approval')->get();

        $rollovers = $instr['rollovers']->whereDoesntHave('approval')->get();

        return [$topups, $rollovers, $apps];
    }

    private function approval($step)
    {
        $instr = $this->instructions();

        $apps = $instr['applications']
            ->whereHas(
                'application',
                function ($app) use ($step) {
                    $app->whereHas(
                        'approval',
                        function ($approval) use ($step) {
                            $approval->has('steps', '=', $step);
                        }
                    );
                }
            )->get();

        $topups = $instr['topups']->whereHas(
            'approval',
            function ($approval) use ($step) {
                $approval->has('steps', '=', $step);
            }
        )->get();

        $rollovers = $instr['rollovers']->whereHas(
            'approval',
            function ($approval) use ($step) {
                $approval->has('steps', '=', $step);
            }
        )->get();
        ;

        return [$topups, $rollovers, $apps];
    }

    private function instructions()
    {
        $applications = ClientFilledInvestmentApplication::where('created_at', '>=', Carbon::now()
            ->subMonthNoOverflow())
            ->whereDoesntHave(
                'application',
                function ($app) {
                    $app->whereHas(
                        'approval',
                        function ($approval) {
                            $approval->where('approved', 1);
                        }
                    );
                }
            )->latest();

        $topups = ClientTopupForm::where('created_at', '>=', Carbon::now()->subMonthNoOverflow())
            ->whereDoesntHave(
                'approval',
                function ($approval) {
                    $approval->where('approved', 1);
                }
            )->latest();

        $rollovers = ClientInvestmentInstruction::where('created_at', '>=', Carbon::now()
            ->subMonthNoOverflow())
            ->whereDoesntHave(
                'approval',
                function ($approval) {
                    $approval->where('approved', 1);
                }
            )->latest();

        return ['applications' => clone $applications, 'topups' => clone $topups, 'rollovers' => clone $rollovers];
    }

    private function presentApplication(ClientFilledInvestmentApplication $application)
    {
        $name = $application->individual
            ? $application->firstname . ' ' . $application->lastname : $application->registered_name;

        $link = route('view_client_filled_investment_application', [$application->id]);

        return ['name' => $name, 'link' => $link, 'type' => 'Application'];
    }

    private function presentRollover(ClientInvestmentInstruction $instruction)
    {
        $clientName = ($instruction->investment)
            ? ClientPresenter::presentJointFullNames($instruction->investment->client_id) : '';

        return [
            'name' => $clientName,
            'link' => $instruction->approval
                ? route('show_client_transaction_approval', [$instruction->approval->id])
                : route('view_client_rollover_instruction', [$instruction->id]),
            'type' => ucfirst($instruction->type->name)
        ];
    }

    private function presentTopup(ClientTopupForm $form)
    {
        return [
            'name' => ClientPresenter::presentJointFullNames($form->client_id),
            'link' => $form->approval
                ? route('show_client_transaction_approval', [$form->approval->id])
                : route('view_client_topup_instruction', [$form->id]),
            'type' => $form->latestInvestment() ? 'Top up' : 'New Investment'
        ];
    }
}
