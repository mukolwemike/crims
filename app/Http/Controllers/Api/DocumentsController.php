<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use Cytonn\Api\Transformers\DocumentTransformer;

/**
 * Class DocumentsController
 *
 * @package Api
 */
class DocumentsController extends ApiController
{
    /**
     * Generate the Documents grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new Document(),
            new DocumentTransformer(),
            function ($document) {
                $document = $document->whereNotNull('approval_id');
                $document = $this->searchByTitle($document);
                //            $document = $this->searchByComplianceDocument($document);
                $document = $this->searchByProject($document);
                $document = $this->searchByProduct($document);
                $document = $this->searchByPortfolioInvestor($document);
                return $this->searchByDocumentType($document)->orderBy('id', 'DESC');
            }
        );
    }

    /**
     * Generate the Documents grid page
     *
     * @param  $module
     * @param  $slug
     * @return string
     */
    public function documentsOfType($module, $slug)
    {
        return $this->processTable(
            new Document(),
            new DocumentTransformer(),
            function ($document) use ($slug, $module) {
                $document = $document->typeSlug($slug)->module($module);
                $document = $document->whereNotNull('approval_id');
                $document = $this->searchByTitle($document);
                //            $document = $this->searchByComplianceDocument($document);
                $document = $this->searchByProject($document);
                $document = $this->searchByProduct($document);
                return $this->searchByDocumentType($document)->orderBy('id', 'DESC');
            }
        );
    }

    private function searchByTitle($document)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['title'])) {
            $title = $state['search']['predicateObject']['title'];
            return $document->where('title', 'LIKE', '%'.$title.'%');
        }
        return $document;
    }

    private function searchByDocumentType($document)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['document_type'])) {
            $type_id = $state['search']['predicateObject']['document_type'];
            return $document->where('type_id', $type_id);
        }
        return $document;
    }

    private function searchByComplianceDocument($document)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['compliance_document'])) {
            return $document->complianceDocuments();
        }
        return $document;
    }

    private function searchByProject($document)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['project'])) {
            $project = $state['search']['predicateObject']['project'];
            if ($project == 0) {
                return $document;
            }
            return $document->where('project_id', $project);
        }
        return $document;
    }

    private function searchByProduct($document)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['product'])) {
            $product = $state['search']['predicateObject']['product'];
            if ($product == 0) {
                return $document;
            }
            return $document->where('product_id', $product);
        }
        return $document;
    }

    private function searchByPortfolioInvestor($document)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['portfolio_investor'])) {
            $portfolioInvestor = $state['search']['predicateObject']['portfolio_investor'];
            if ($portfolioInvestor == 0) {
                return $document;
            }
            return $document->where('portfolio_investor_id', $portfolioInvestor);
        }
        return $document;
    }

    public function kycDocumentTypes($type)
    {
        $clientType = ClientType::findOrFail($type);

        $fundManager = $this->fundManager();

        $documentTypes = ClientComplianceChecklist::where('client_type_id', $clientType->id)
            ->where('fund_manager_id', $fundManager->id)
            ->get();

        return response($documentTypes);
    }
}
