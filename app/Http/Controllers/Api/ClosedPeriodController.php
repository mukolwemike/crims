<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\ClosedPeriod;
use Cytonn\Investment\ClosedPeriods\ClosedPeriodTransformer;
use Illuminate\Support\Facades\Request;

class ClosedPeriodController extends ApiController
{
    /*
     * Get the closed periods
     */
    public function closedPeriods()
    {
        $filter = function ($model) {
            return $this->filterClosedPeriods($model)
                ->orderBy('start', 'desc');
        };

        return $this->processTable(new ClosedPeriod(), new ClosedPeriodTransformer(), $filter);
    }

    /*
     * Filter the closed periods
     */
    public function filterClosedPeriods($model)
    {
        $state = Request::get('tableState');

        if (isset($state['search']['predicateObject']['date']['after'])) {
            $model = $model->where('start', '>=', $state['search']['predicateObject']['date']['after']);
        }

        if (isset($state['search']['predicateObject']['date']['before'])) {
            $model = $model->where('end', '<=', $state['search']['predicateObject']['date']['before']);
        }

        if (! Request::has('args')) {
            return $model;
        }

        foreach (Request::get('args') as $key => $value) {
            $model = $model->where($key, $value);
        }

        return $model;
    }
}
