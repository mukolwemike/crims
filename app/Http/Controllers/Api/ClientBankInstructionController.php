<?php
/**
 * Date: 22/07/2016
 * Time: 8:38 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\BankInstruction;
use Cytonn\Api\Transformers\BankInstructionTransformer;

/**
 * Class ClientBankInstructionController
 *
 * @package Api
 */
class ClientBankInstructionController extends ApiController
{
    /**
     * ClientBankInstructionController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->authorizer->checkAuthority('addinvestment');
    }


    /**
     * Show the instructions in a table
     *
     * @param  null $client_id
     * @return string
     */
    public function index($client_id = null)
    {
        return $this->processTable(
            new BankInstruction(),
            new BankInstructionTransformer(),
            function ($model) use ($client_id) {
                if ($client_id) {
                    $model = $model->whereHas(
                        'investment',
                        function ($i) use ($client_id) {
                            $i->where('client_id', $client_id);
                        }
                    );
                }

                return $this->searchByClientCode($model->forFundManager());
            }
        );
    }

    public function show($client_id)
    {
        return $this->index($client_id);
    }

    /**
     * Search instructions by a certain client code
     *
     * @param  $model
     * @return mixed
     */
    protected function searchByClientCode($model)
    {
        $state = \Request::get('tableState');

        //filter
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
        } else {
            return $model;
        }

        return $model->whereHas(
            'investment',
            function ($investment) use ($code) {
                $investment->whereHas(
                    'client',
                    function ($client) use ($code) {
                        $client->where('client_code', $code);
                    }
                );
            }
        );
    }
}
