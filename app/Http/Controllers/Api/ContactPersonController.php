<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 15/03/2018
 * Time: 09:17
 */

namespace App\Http\Controllers\Api;

use Cytonn\Investment\Rules\InvestmentApplicationRules;

class ContactPersonController extends ApiController
{
    use InvestmentApplicationRules;

    public function validateContactPerson()
    {
        $validator = $this->contactPersons(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        return response(['status' => 202]);
    }
}
