<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\CoopPayment;
use Cytonn\Api\Transformers\CoopPaymentTransformer;

/**
 * Class CoopPaymentsController
 *
 * @package Api
 */
class CoopPaymentsController extends ApiController
{
    /**
     * Generate the payments grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new CoopPayment(),
            new CoopPaymentTransformer(),
            function ($payments) {
                $state = \Request::get('tableState');

                //filter
                if (isset($state['search']['predicateObject']['type'])) {
                    $query = $state['search']['predicateObject']['type'];

                    $payments = $payments->where('type', $query);
                }

                return $payments->forFundManager()->orderBy('date', 'DESC');
            }
        );
    }

    public function getClientPayments($client_id)
    {
        return $this->processTable(
            new CoopPayment(),
            new CoopPaymentTransformer(),
            function ($model) use ($client_id) {
                return $model->where('client_id', $client_id);
            }
        );
    }
}
