<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialAccountBalanceTrail;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Api\Transformers\CustodialAccountBalanceTrailTransformer;
use Cytonn\Api\Transformers\CustodialAccountTransformer;
use Cytonn\Api\Transformers\CustodialTransactionTransformer;
use Cytonn\Api\Transformers\ExchangeRateTransformer;
use Cytonn\Models\ExchangeRate;
use Illuminate\Support\Facades\Request;
use League\Fractal\Resource\Collection;

class CustodialAccountController extends ApiController
{
    public function getRemaining($id)
    {
        $remaining = CustodialAccount::where('id', '!=', $id)->get();

        $collection = new Collection($remaining, new CustodialAccountTransformer());

        return $this->fractal->createData($collection)->toJson();
    }

    public function transactions($id)
    {
        $filter = function ($model) use ($id) {
            return $this->searchTransactions($model)->where('amount', '!=', 0)->where('custodial_account_id', $id)
                ->latest('date')->orderBy('id', 'desc');
        };

        return $this->processTable(new CustodialTransaction(), new CustodialTransactionTransformer(), $filter);
    }

    public function accountBalanceTrail($id)
    {
        return $this->processTable(
            new CustodialAccountBalanceTrail(),
            new CustodialAccountBalanceTrailTransformer(),
            function ($model) use ($id) {
                $model = $this->filterTrailByDate($model);
                return $model->where('account_id', $id)->orderBy('date', 'ASC');
            }
        );
    }

    private function filterTrailByDate($trail)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['date'])) {
            $date = $state['search']['predicateObject']['date'];
            return $trail->where('date', $date);
        }
        return $trail;
    }

    protected function searchTransactions($model)
    {
        $state = \Request::get('tableState');

        //filter
        if (isset($state['search']['predicateObject']['transaction_type'])) {
            $query = $state['search']['predicateObject']['transaction_type'];

            $model = $model->where('type', $query);
        }

        if (isset($state['search']['predicateObject']['search_date_end'])) {
            $query = $state['search']['predicateObject']['search_date_end'];

            $model = $model->where('date', '<=', $query);
        }

        if (isset($state['search']['predicateObject']['search_date_start'])) {
            $query = $state['search']['predicateObject']['search_date_start'];

            $model = $model->where('date', '>=', $query);
        }

        if (isset($state['search']['predicateObject']['transaction_type'])) {
            $query = $state['search']['predicateObject']['transaction_type'];

            $model = $model->where('type', $query);
        }

        return $model;
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getCustodialAccountsForProduct($product_id)
    {
        return Product::findOrFail($product_id)->receivingAccounts;
    }

    /**
     * @param $project_id
     * @return mixed
     */
    public function getCustodialAccountsForProject($project_id)
    {
        return Project::findOrFail($project_id)->receivingAccounts;
    }

    /**
     * @param $entity_id
     * @return mixed
     */
    public function getCustodialAccountsForEntity($entity_id)
    {
        return SharesEntity::findOrFail($entity_id)->receivingAccounts;
    }

    /**
     * @param $fund_id
     * @return mixed
     */
    public function getCustodialAccountsForUnitFund($fund_id)
    {
        return UnitFund::findOrFail($fund_id)->receivingAccounts;
    }

    public function getCustodialAccountsForFundManager($fundManagerId)
    {
        $manager = (new FundManager())->findOrFail($fundManagerId);
        $accountsList = $manager->custodialAccounts()
            ->get()
            ->each(
                function ($account) {
                    $account->label = $account->full_name . ' - ' . '( ' . $account->account_no . ' )';
                    $account->value = $account->id;
                }
            )
            ->toArray();

        return response(['fetched' => true, 'data' => $accountsList], 200);
    }

    // get all custodial accounts
    public function getCustodialAccounts()
    {
        $accounts = CustodialAccount::all()
            ->map(
                function ($account) {
                    return [
                        'label' => $account->full_name,
                        'value' => $account->id
                    ];
                }
            )
            ->toArray();

        return response(['fetched' => true, 'accounts' => $accounts], 200);
    }

    public function getCustodialAccountsShort()
    {
        $accounts = CustodialAccount::all()
            ->map(
                function ($account) {
                    return [
                        'label' => $account->account_name,
                        'value' => $account->id
                    ];
                }
            )
            ->toArray();

        return response(['fetched' => true, 'accounts' => $accounts], 200);
    }

    /*
     * Get the custodial accounts exchange rates
     */
    public function custodialExchangeRates()
    {
        $filter = function ($model) {
            return $this->filterExchangeRates($model)
                ->orderBy('date', 'desc');
        };

        return $this->processTable(new ExchangeRate(), new ExchangeRateTransformer(), $filter);
    }

    /*
     * Filter the exchange rates base and to
     */
    public function filterExchangeRates($model)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['date']['after'])) {
            $model = $model->where('date', '>=', $state['search']['predicateObject']['date']['after']);
        }

        if (isset($state['search']['predicateObject']['date']['before'])) {
            $model = $model->where('date', '<=', $state['search']['predicateObject']['date']['before']);
        }

        if (!Request::has('args')) {
            return $model;
        }

        foreach (Request::get('args') as $key => $value) {
            $model = $model->where($key, $value);
        }

        return $model;
    }
}
