<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/26/18
 * Time: 10:58 AM
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Api\Transformers\ClientStandingOrderTransformer;
use App\Cytonn\Clients\Rules\StandingOrderRules;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientStandingOrderType;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;

class ClientStandingOrdersController extends ApiController
{
    use StandingOrderRules;

    public function store()
    {
        $validator = $this->standingOrderCreate(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $input = request()->all();

        if (!isset($input['date'])) {
            $input['date'] = Carbon::now();
        }

        $client = Client::findOrFail($input['client_id']);

        ClientTransactionApproval::make($client->id, 'create_client_standing_order', $input);

        return response([
            'updated' => true,
            'status' => 202
        ]);
    }

    public function show($id)
    {
        $client = Client::findOrFail($id);


        $orders = $client->standingOrders->map(function ($order) {
            return (new ClientStandingOrderTransformer())->transform($order);
        });

        return response($orders);
    }

    public function orderTypes()
    {
        return response(ClientStandingOrderType::all());
    }
}
