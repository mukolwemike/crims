<?php
/**
 * Date: 10/03/2016
 * Time: 12:20 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\CampaignStatementItem;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\StatementCampaign;
use Carbon\Carbon;
use Cytonn\Api\Transformers\CampaignStatementItemTransformer;
use Cytonn\Api\Transformers\ClientTransformer;
use League\Fractal\Resource\Collection;

/**
 * Class StatementController
 *
 * @package Api
 */
class StatementController extends ApiController
{
    /**
     * Campaign items grid table
     *
     * @param  $campaignId
     * @return string
     */
    public function campaignItems($campaignId)
    {
        $campaign = new CampaignStatementItem();

        $filtered = $this->filter($campaign)->where('campaign_id', $campaignId);

        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new CampaignStatementItemTransformer());

        $this->addPaginationToResource($paginated, $resource);

        return $this->fractal->createData($resource)->toJson();
    }

    /**
     * Clients missing from campaign
     *
     * @param  $campaignId
     * @return string
     */
    public function missingClients($campaignId)
    {
        $existing = CampaignStatementItem::where('campaign_id', $campaignId)->get()->lists('client_id');

        $campaign = StatementCampaign::findOrFail($campaignId);

        $clients = new Client();

        $cutOff = $campaign->send_date->copy()->subMonthNoOverflow()->toDateString();
        $statementDate = $campaign->send_date->copy();

        //TODO optimise query
        $remaining = Client::has('investments', '>', 0)
            ->whereNotIn('id', $existing)
            ->whereHas(
                'investments',
                function ($q) use ($cutOff, $statementDate) {
                    $q->where('invested_date', '<=', $statementDate->copy()->addDay())
                        ->where(
                            function ($query) use ($statementDate) {
                                //add all active or withdrawn within last month
                                $query->where('withdrawn', null)->orWhere('withdrawn', 0)
                                    ->orWhere('withdrawn', true)
                                    ->where('withdrawal_date', '>=', $statementDate->copy()->subMonthNoOverFlow());
                            }
                        );
                }
            )->get()->lists('id');

        $filtered = $this->filter($clients)->whereIn('id', $remaining);
        $paginated = $this->sortAndPaginate($filtered);

        $resource = new Collection($paginated['model'], new ClientTransformer());
        $this->addPaginationToResource($paginated, $resource);
        $resource->setMetaValue('total_count', $filtered->count());

        return $this->fractal->createData($resource)->toJson();
    }
}
