<?php

namespace App\Http\Controllers\Api\Ussd;

use App\Cytonn\Api\Transformers\USSDApplicationTransformer;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\System\Channel;
use App\Http\Controllers\Controller;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;

class USSDController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function index()
    {
        $fundManager = $this->fundManager();

        $channel = Channel::where('slug', 'ussd')->first();

        return $this->sortFilterPaginate(
            ClientFilledInvestmentApplication::where('channel_id', $channel->id),
            [],
            function ($application) {
                return app(USSDApplicationTransformer::class)->transform($application);
            },
            function ($model) {
                $model = $this->filterSentStatus($model);

                return $model;
            }
        );
    }

    private function filterSentStatus($model)
    {
        $request = \Request::get('filters');

        $option = isset($request['invested_option']) ? $request['invested_option'] : null;

        if ($option) {
            return $model->whereHas('application', function ($q) use ($option) {
                $q->whereHas('client', function ($q) use ($option) {
                    if ($option == 'invested') {
                        return $q->whereHas('unitFundPurchases');
                    } else {
                        return $q->whereDoesntHave('unitFundPurchases');
                    }
                });
            });
        }

        return $model;
    }

    public function show($id)
    {
        $app = app(USSDApplicationTransformer::class)->transform(ClientFilledInvestmentApplication::find($id));

        return response(['fetched' => true, 'data' => $app], 200);
    }

    public function sendReminder($id)
    {
        $client = ClientFilledInvestmentApplication::find($id)->application->client;

        $client->repo->sendNotification();
    }
}
