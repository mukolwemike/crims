<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\InterestRateUpdate;
use Cytonn\Api\Transformers\InterestRateUpdateTransformer;

/**
 * Class InterestRateUpdatesController
 *
 * @package Api
 */
class InterestRateUpdatesController extends ApiController
{
    /**
     * Generate the Interest Rate Updates grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new InterestRateUpdate(),
            new InterestRateUpdateTransformer(),
            function ($model) {
                return $this->searchByProduct($model->orderBy('date', 'DESC'));
            }
        );
    }

    public function getRates($update_id)
    {
        return InterestRateUpdate::findOrFail($update_id)->rates;
    }

    /**
     * @param $update
     * @return mixed
     */
    private function searchByProduct($update)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['product'])) {
            $product = $state['search']['predicateObject']['product'];
            return $update->where('product_id', $product);
        }
        return $update;
    }
}
