<?php
/**
 * Date: 01/02/2016
 * Time: 10:20 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Controllers\Api;

use App\Cytonn\Api\Transformers\UtilityBillingInstructionTransformer;
use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\TransactionApprovalDocument;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Cytonn\Api\Transformers\ClientFilledInvestmentApplicationTransformer;
use Cytonn\Api\Transformers\ClientInvestmentRolloverInstructionTransformer;
use Cytonn\Api\Transformers\ClientInvestmentTopupInstructionTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundInvestmentInstructionTransformer;
use Symfony\Component\HttpFoundation\Response;

class ClientInstructionController extends ApiController
{
    public function rollovers()
    {
        return $this->processTable(
            new ClientInvestmentInstruction(),
            new ClientInvestmentRolloverInstructionTransformer(),
            function ($form) {
                $form = $this->searchByInvested($form, 'rollover');
                $form = $this->searchByCancelled($form);
                $form = $this->searchByOrigin($form);
                return $form->has('investment')->latest();
            }
        );
    }

    public function topups()
    {
        return $this->processTable(
            new ClientTopupForm(),
            new ClientInvestmentTopupInstructionTransformer(),
            function ($form) {
                $form = $this->searchByInvested($form, 'topup');
                $form = $this->searchByCancelled($form);
                $form = $this->searchByOrigin($form);
                return $form->latest();
            }
        );
    }

    public function applications()
    {
        ini_set('max_execution_time', 300);

        return $this->processTable(
            new ClientFilledInvestmentApplication(),
            new ClientFilledInvestmentApplicationTransformer(),
            function ($application) {
                $application = $this->searchByUsed($application);
                return $application->latest();
            }
        );
    }

    public function unitFundInstructions()
    {
        return $this->processTable(
            new UnitFundInvestmentInstruction(),
            new UnitFundInvestmentInstructionTransformer(),
            function ($instruction) {
                $instruction = $this->searchByInvested($instruction, 'unitFund');

                $instruction = $this->searchByOrigin($instruction);

                $instruction = $this->searchByTypes($instruction);

                $instruction = $this->searchByFund($instruction);

                return $instruction->latest();
            }
        );
    }

    public function utilityBillingInstructions()
    {
        return $this->processTable(
            new UtilityBillingInstructions(),
            new UtilityBillingInstructionTransformer(),
            function ($instruction) {
                $instruction = $this->searchByTypes($instruction);

                $instruction = $this->searchByStatus($instruction);

                $instruction = $this->searchByFund($instruction);

                $instruction = $this->searchByOrigin($instruction);

                return $instruction->latest();
            }
        );
    }

    private function searchByUsed($app)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['used'])) {
            $used = $state['search']['predicateObject']['used'];
            switch ($used) {
                case 1:
                    return $app->has('application');
                    break;
                case 0:
                    return $app->doesntHave('application');
                    break;
                default:
            }
        }
        return $app;
    }

    private function searchByInvested($form, $type)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['invested'])) {
            $invested = $state['search']['predicateObject']['invested'];

            switch ($invested) {
                case 1:
                    return $form->whereNull('approval_id');
                    break;
                case 2:
                    return $form->whereNotNull('approval_id')->whereNull('cancelled');
                case 3:
                    return $form->whereNotNull('cancelled');
                    break;
                default:
            }
        }
        return $form;
    }

    private function searchByCancelled($form)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['cancelled'])) {
            $inactive = $state['search']['predicateObject']['cancelled'] == 1
                ? $state['search']['predicateObject']['cancelled'] : 0;
            return $form->where('inactive', $inactive);
        }
        return $form;
    }

    private function searchByOrigin($form)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['origin'])) {
            $origin = $state['search']['predicateObject']['origin'];

            switch ($origin) {
                case "admin":
                    return $form->where('channel_id', $this->getChannelId('admin'))->orWhereNull('user_id');
                case "ussd":
                    return $form->where('channel_id', $this->getChannelId('ussd'));
                case "mobile":
                    return $form->where('channel_id', $this->getChannelId('mobile'));
                case "web":
                    return $form->where('channel_id', $this->getChannelId('web'));
                case "mpesa":
                    return $form->where('channel_id', $this->getChannelId('mpesa'));
                case "client":
                    return $form->whereNotNull('user_id');
                default:
            }
        }
        return $form;
    }

    private function getChannelId($slug)
    {
        return Channel::where('slug', $slug)->first()->id;
    }

    private function searchByTypes($instruction)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['types'])) {
            $type = $state['search']['predicateObject']['types'];

            switch ($type) {
                case 1:
                    return $instruction->where('instruction_type_id', 1);

                    break;

                case 2:
                    return $instruction->where('instruction_type_id', 2);

                    break;
                case 3:
                    return $instruction->where('instruction_type_id', 3);

                    break;

                default:
            }
        }

        return $instruction;
    }

    private function searchByStatus($instruction)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['status'])) {
            $type = $state['search']['predicateObject']['status'];

            switch ($type) {
                case 1:
                    return $instruction->where('billing_status_id', 1);

                    break;

                case 2:
                    return $instruction->where('billing_status_id', 2);

                    break;
                case 3:
                    return $instruction->where('billing_status_id', 3);

                    break;
                case 4:
                    return $instruction->where('billing_status_id', 4);

                    break;

                default:
            }
        }

        return $instruction;
    }

    private function searchByFund($instruction)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['unit_fund'])) {
            $fund_id = $state['search']['predicateObject']['unit_fund'];

            return $instruction->where('unit_fund_id', $fund_id);
        }

        return $instruction;
    }

    public function getCommissionRate($type_id)
    {
        $commission_rate = (new \App\Cytonn\Models\CommissionRate)
            ->where('commission_recipient_type_id', $type_id)->get()->first();

        return $commission_rate->rate;
    }

    public function approvalTransactionDocument()
    {
        $input = request()->all();

        $slug = $input['slug'];

        $file = request()->file('file');

        unset($input['file']);

        $document = Document::make(file_get_contents($file), $file->getClientOriginalExtension(), $slug);

        $appDoc = new TransactionApprovalDocument();
        $appDoc->document_id = $document->id;
        $appDoc->approval_id = (int)$input['approval_id'];
        $appDoc->name = $input['description'];
        $appDoc->save();

        \Flash::success('Instruction Document Upload was successful');

        return response(['created' => true]);
    }

    public function fetchTransactionDocument($approvalId)
    {
        return response([
            'created' => true,
            'docs' => TransactionApprovalDocument::where('approval_id', $approvalId)->get(),
            'status' => Response::HTTP_CREATED
        ]);
    }

    public function deleteTransactionDocument($docId)
    {
        $document = Document::findOrFail($docId);

        $document->delete();

        $rec = TransactionApprovalDocument::where('document_id', $docId);

        $rec->delete();

        return response(['deleted' => true]);
    }
}
