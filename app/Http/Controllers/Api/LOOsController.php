<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\User;
use App\Jobs\RealEstate\ExportLOOs;
use Cytonn\Api\Transformers\LOOTransformer;

/**
 * Class LOOsController
 *
 * @package Api
 */
class LOOsController extends ApiController
{
    /**
     * Generate the LOOs grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(new RealestateLetterOfOffer(), new LOOTransformer(), $this->filterQuery());
    }

    public function export()
    {
        $loos = (new RealestateLetterOfOffer())->where($this->filterQuery())->get();

        dispatch(new ExportLOOs($loos, auth()->user()));

        return response([], 202);
    }

    /**
     * @param $loo
     * @return mixed
     */
    private function searchByClientCode($loo)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $loo->whereHas(
                'holding',
                function ($holding) use ($code) {
                    $holding->whereHas(
                        'client',
                        function ($client) use ($code) {
                            $client->where('client_code', 'like', '%' . $code . '%');
                        }
                    );
                }
            );
        }
        return $loo;
    }

    /**
     * @param $loo
     * @return mixed
     */
    private function searchByProject($loo)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['project'])) {
            $project = $state['search']['predicateObject']['project'];
            return $loo->whereHas(
                'holding',
                function ($holding) use ($project) {
                    $holding->whereHas(
                        'unit',
                        function ($unit) use ($project) {
                            $unit->where('project_id', $project);
                        }
                    );
                }
            );
        }
        return $loo;
    }

    /**
     * @param $loo
     * @return mixed
     */
    private function searchByApproval($loo)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['approval_status'])) {
            $filter = $state['search']['predicateObject']['approval_status'];
            switch ($filter) {
                case '1':
                    return $loo->whereNotNull('pm_approved_on');
                    break;
                case '2':
                    return $loo->whereNull('pm_approved_on');
                    break;
                default:
                    break;
            }
        }
        return $loo;
    }

    /**
     * @param $loo
     * @return mixed
     */
    private function searchBySent($loo)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['sent_status'])) {
            $filter = $state['search']['predicateObject']['sent_status'];
            switch ($filter) {
                case '1':
                    return $loo->whereNotNull('sent_on');
                    break;
                case '2':
                    return $loo->whereNull('sent_on');
                    break;
                default:
                    break;
            }
        }
        return $loo;
    }

    /**
     * @return mixed
     */
    private function filterQuery()
    {
        return function ($model) {
            $model = $model->whereHas(
                'holding',
                function ($holding) {
                    return $holding->where('active', 1);
                }
            );
            $model = $this->searchByClientCode($model);
            $model = $this->searchByApproval($model);
            $model = $this->searchBySent($model);
            return $this->searchByProject($model);
        };
    }
}
