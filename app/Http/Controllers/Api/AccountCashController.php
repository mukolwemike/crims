<?php

namespace App\Http\Controllers\Api;

use App\Cytonn\Models\CustodialTransaction;
use Cytonn\Api\Transformers\CustodialTransactionTransformer;

/**
 * Class AccountCashController
 *
 * @package Api
 */
class AccountCashController extends ApiController
{
    /**
     * Generate the Account Cash grid page
     *
     * @return string
     */
    public function index()
    {
        return $this->processTable(
            new CustodialTransaction(),
            new CustodialTransactionTransformer(),
            function ($model) {
                $model = $model->whereHas(
                    'transactionType',
                    function ($type) {
                        $type->whereIn('name', ['FI', 'TI']);
                    }
                );

                $model = $this->searchByClientCode($model);
                $model = $this->searchByCategory($model);
                $model = $this->searchByAccount($model);
                $model = $this->searchByWhetherHasClient($model);
                $model = $this->searchByProduct($model);
                $model = $this->searchByShareEntity($model);
                $model = $this->searchByTransferred($model);
                $model = $this->searchByDates($model);

                return $this->searchByProject($model)->latest('date');
            }
        );
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByClientCode($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['client_code'])) {
            $code = $state['search']['predicateObject']['client_code'];
            return $trans->whereHas(
                'client',
                function ($client) use ($code) {
                    $client->where('client_code', 'like', '%' . $code . '%');
                    ;
                }
            );
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByAccount($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['account'])) {
            $account = $state['search']['predicateObject']['account'];
            return $trans->where('custodial_account_id', $account);
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByProject($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['project'])) {
            $project = $state['search']['predicateObject']['project'];
            return $trans->whereHas(
                'clientPayment',
                function ($payment) use ($project) {
                    $payment->where('project_id', $project);
                }
            );
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByProduct($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['product'])) {
            $product = $state['search']['predicateObject']['product'];
            return $trans->whereHas(
                'clientPayment',
                function ($payment) use ($product) {
                    $payment->where('product_id', $product);
                }
            );
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByTransferred($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['transferred'])) {
            $transferred = $state['search']['predicateObject']['transferred'];
            return $trans->whereHas(
                'clientPayment',
                function ($payment) use ($transferred) {
                    $payment->whereNotNull('transferred_transaction_id', $transferred);
                }
            );
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByCategory($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['category'])) {
            $category = $state['search']['predicateObject']['category'];
            switch ($category) {
                case 'projects':
                    return $trans->whereNull('custodial_account_id');
                    break;
                case 'products':
                    return $trans->whereNotNull('custodial_account_id');
                    break;
                default:
            }
        }
        return $trans;
    }

    /**
     * @param $trans
     * @return mixed
     */
    private function searchByWhetherHasClient($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['has_client'])) {
            $state = $state['search']['predicateObject']['has_client'];
            switch ($state) {
                case '0':
                    return $trans->whereNull('client_id');
                    break;
                case '1':
                    return $trans->whereNotNull('client_id');
                    break;
                default:
            }
        }
        return $trans;
    }

    /*
     * Search by the start and end dates
     */
    public function searchByDates($trans)
    {
        $state = \Request::get('tableState');

        if (isset($state['search']['predicateObject']['date']['after'])) {
            $trans = $trans->where('date', '>=', $state['search']['predicateObject']['date']['after']);
        }

        if (isset($state['search']['predicateObject']['date']['before'])) {
            $trans = $trans->where('date', '<=', $state['search']['predicateObject']['date']['before']);
        }

        return $trans;
    }

    /**
     * Search by share entity
     */
    private function searchByShareEntity($trans)
    {
        $state = \Request::get('tableState');
        if (isset($state['search']['predicateObject']['shareEntity'])) {
            $shareEntity = $state['search']['predicateObject']['shareEntity'];
            return $trans->whereHas(
                'clientPayment',
                function ($payment) use ($shareEntity) {
                    $payment->where('share_entity_id', $shareEntity);
                }
            );
        }
        return $trans;
    }
}
