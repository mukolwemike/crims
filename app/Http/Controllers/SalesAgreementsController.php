<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Document;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstateSalesAgreement;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Realestate\Forms\UploadLooForm;

/**
 * Class SalesAgreementsController
 */
class SalesAgreementsController extends Controller
{
    /**
     * @var UploadLooForm
     */
    private $uploadLooForm;

    /**
     * SalesAgreementsController constructor.
     *
     * @param UploadLooForm $uploadLooForm
     */
    public function __construct(UploadLooForm $uploadLooForm)
    {
        $this->uploadLooForm = $uploadLooForm;

        parent::__construct();
    }


    /**
     * @return mixed
     */
    public function getIndex()
    {
        $projects = Project::all();
        return view(
            'realestate.sales_agreements.index',
            ['title' => 'Real Estate Sales Agreement', 'projects' => $projects]
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShow($id)
    {
        $sales_agreement = RealEstateSalesAgreement::findOrFail($id);
        $unitHolding = $sales_agreement->holding;
        $client = $unitHolding->client;
        $unit = $unitHolding->unit;
        $project = $unitHolding->project;

        !is_null($sales_agreement->history) ?: $sales_agreement->history = [];

        $previous_sales_agreements = Document::whereIn('id', $sales_agreement->history)
            ->latest()
            ->get()
            ->each(
                function ($doc) {
                    $doc->name = 'Uploaded on ' . DatePresenter::formatDateTime($doc->created_at);
                }
            );

        return view(
            'realestate.sales_agreements.show',
            [
                'unitHolding' => $unitHolding,
                'client' => $client,
                'unit' => $unit,
                'project' => $project,
                'sales_agreement' => $sales_agreement,
                'previous_sales_agreements' => $previous_sales_agreements
            ]
        );
    }
}
