<?php

namespace App\Http\Controllers\Unitization;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    public function sendBulkPayments($bcpId)
    {
        $bcp = BulkCommissionPayment::findOrFail($bcpId);
        $input['bcp_id'] = $bcp->id;

        $approval = ClientTransactionApproval::add(
            [
            'client_id'=>null,
            'transaction_type'=>'bulk_unitization_commission_payments',
            'payload'=>$input,
            'scheduled'=>false
            ]
        );

        $bcp->unitizationApproval()->associate($approval);
        $bcp->save();

        \Flash::message('Unitization commission payments have been saved for approval');

        return \Redirect::back();
    }

    public function awardCommission(Request $request, $bcpId)
    {
        $bcp = BulkCommissionPayment::findOrFail($request->get('bcp'));

        $date = Carbon::parse($request->get('date'))->toDateString();

        $this->queue(
            function () use ($bcp, $date) {
                \Artisan::call(
                    'unitization:award_unpaid_commissions',
                    ['bcp_id' => $bcp->id, 'date' => $date]
                );
            }
        );

        \Flash::message('The commission award report will be sent to your email shortly');

        return back();
    }

    public function recipientPayment($id)
    {
        $recipient = CommissionRecepient::findOrFail($id);

        $date = (new Carbon(\Request::get('date')));

        $commissionDates = (new BulkCommissionPayment())->commissionDates($date);

        return view('unitization.commissions.recipient_details', [
            'start' => $commissionDates['start'],
            'end' => $commissionDates['end'],
            'recipient' => $recipient,
            'date' => $date
        ]);
    }

    public function editSchedule(Request $request)
    {
        $schedule = UnitFundCommissionSchedule::findOrFail($request->get('id'));

        $input = $request->except(['_token']);

        $input['unit_fund_purchase_id'] = $schedule->unitFundCommission->unit_fund_purchase_id;
        $input['commission_recipient_id'] = $schedule->unitFundCommission->commission_recipient_id;
        $input['initial_date'] = $schedule->date;

        ClientTransactionApproval::add([
            'client_id' => $schedule->unitFundCommission->purchase->client_id,
            'transaction_type' => 'store_unit_fund_commission_payment_schedule',
            'payload' => $input
        ]);

        \Flash::message('Request to edit the unit fund commission payment schedule has been saved for approval');

        return back();
    }
}
