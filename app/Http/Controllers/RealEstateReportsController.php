<?php

namespace App\Http\Controllers;

use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Jobs\Clients\ClientContactsExportCommand;
use App\Jobs\RealEstate\ClientSummary;
use App\Jobs\RealEstate\ClientsWithOverduePaymentsSummary;
use App\Jobs\RealEstate\CombinedClientsWithOverduePaymentsReport;
use App\Jobs\RealEstate\DepositPaymentExpiryNotification;
use App\Jobs\RealEstate\ForfeitedUnits;
use App\Jobs\RealEstate\RePaymentsSummary;
use App\Jobs\RealEstate\SendClientsTrancheReport;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class RealEstateReportsController extends Controller
{
    protected $exportProjectCashFlowForm;

    /**
     * RealEstateReportsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display the  real estate reports
     *
     * @return Response
     */
    public function getIndex()
    {
        $clientProjects = Project::pluck('name', 'id')->toArray();
        $projects = [null => 'All projects'] + $clientProjects;
        $activeStatuses = ['all' => 'All Clients', 'active' => 'Active Clients', 'inactive' => 'Inactive Clients'];
        $products = Product::pluck('name', 'id');

        $categories = ['realestate' => 'Real Estate', 'investment' => 'Investment', 'combined' => 'Combined'];

        return view(
            'realestate.reports',
            [
                'title' => 'Real Estate Reports',
                'projects' => $projects,
                'categories' => $categories,
                'products' => $products,
                'activeStatuses' => $activeStatuses,
                'clientProjects' => $clientProjects
            ]
        );
    }

    /**
     * Export Project Cash Flow
     */
    public function postExportProjectCashFlows()
    {
        $project_id = Request::get('project_id');
        $date = Request::get('date');

        try {
            Project::findOrFail($project_id);
        } catch (ModelNotFoundException $e) {
            $project_id = null;
        }

        \Queue::push(
            static::class . '@export',
            ['project_id' => $project_id, 'user_id' => \Auth::user()->id, 'date' => $date]
        );

        \Flash::success('The report will be sent shortly via email.');

        return \Redirect::back();
    }

    public function export($job, $data)
    {
        Artisan::call(
            're:cashflows',
            [
                'project_id' => $data['project_id'],
                'user_id' => $data['user_id'],
                'date' => isset($data['date']) ? $data['date'] : ''
            ]
        );

        $job->delete();
    }

    /**
     * @return mixed
     */
    public function exportClientContacts()
    {
        $input = Request::all();

        $productIds = array_key_exists('product_ids', $input) ? $input['product_ids'] : [];

        $projectIds = array_key_exists('project_ids', $input) ? $input['project_ids'] : [];

        dispatch(new ClientContactsExportCommand(
            $input['category'],
            \Auth::user(),
            $projectIds,
            $productIds,
            $input['active']
        ));

        \Flash::success('The client contacts will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @param $job
     * @param $data
     */
    public function exportContacts($job, $data)
    {
        Artisan::call('cytonn:export-contacts', ['category' => $data['category'], '--user_id' => $data['user_id']]);

        $job->delete();
    }

    /**
     * @return mixed
     */
    public function getForfeitedUnits()
    {
        dispatch(new ForfeitedUnits(\Auth::user()));

        \Flash::success('The forfeited units report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function getClientsSummaries()
    {
        dispatch(new ClientSummary(\Auth::user()));

        \Flash::success('The clients summary will be sent shortly via email');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function getClientsOverdues()
    {
        dispatch(new CombinedClientsWithOverduePaymentsReport(\Auth::user()));

        \Flash::success('The combined clients overdue summary will be sent shortly via email');

        return \Redirect::back();
    }

    public function exportClientWithOverduePayments(\Illuminate\Http\Request $request)
    {
        $date = Carbon::parse($request->get('date'));

        $projectId = $request->get('project_id');

        $project = $projectId ? Project::find($projectId) : null;

        dispatch(new ClientsWithOverduePaymentsSummary($date, \Auth::user(), $project));

        \Flash::success('The clients overdue summary will be sent shortly via email');

        return \Redirect::back();
    }

    /**
     * get real estate payments summaries
     *
     * @return mixed
     */
    public function exportRealEstatePaymentsSummaries()
    {
        $input = Request::except('_token');

        $project_id = $input['project_id'];

        $user_id = Auth::user()->id;

        $this->queue(
            function () use ($user_id, $project_id) {
                Artisan::call('re:payments', ['user_id' => $user_id, 'project_id' => $project_id]);
            }
        );

        \Flash::success('The real estate payments summary will be sent shortly via email');

        return \Redirect::back();
    }

    public function exportProjectPaymentsSummaries(\Illuminate\Http\Request $request)
    {
        $id = \Auth::user() ? \Auth::user()->id : null;
        $start = Carbon::parse($request->get('start'));
        $end = Carbon::parse($request->get('end'));
        $pid = [$request->get('project_id')];

        $this->dispatch(new RePaymentsSummary($start, $end, [$id], $pid));


        \Flash::success('The real estate payments summary will be sent shortly via email');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportClientsTrancheReport()
    {
        $input = Request::all();
        $project = Project::findOrFail($input['project_id']);

        dispatch(new SendClientsTrancheReport($project, \Auth::user()));

        \Flash::success('The clients tranche report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportReservationSummaryReport()
    {
        $input = Request::all();

        $start = Carbon::parse($input['start'])->toDateString();

        $end = Carbon::parse($input['end'])->toDateString();

        $email = Auth::user()->email;

        $this->queue(
            function () use ($start, $end, $email) {
                \Artisan::call(
                    're:reservations-summary',
                    ['start' => $start, 'end' => $end, 'email' => $email]
                );
            }
        );

        \Flash::success('The reservation summary report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return mixed
     */
    public function exportDepositPaymentExpiryNotificationsReport()
    {
        dispatch(new DepositPaymentExpiryNotification(\Auth::user()));

        \Flash::success('The deposit payment expiry notifications report will be sent shortly via email.');

        return \Redirect::back();
    }

    /*
     * Export real estate inflows
     */
    public function exportRealEstateInflows()
    {
        $userId = \Auth::user()->id;
        $start = Carbon::parse(Request::get('start'))->toDateString();
        $end = Carbon::parse(Request::get('end'))->toDateString();

        $this->queue(
            function () use ($userId, $start, $end) {
                \Artisan::call(
                    're:inflows',
                    ['user_id' => $userId, 'start' => $start, 'end' => $end]
                );
            }
        );

        \Flash::success('The real estate inflows report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportRealEstateCommissions()
    {
        $userId = \Auth::user()->id;

        $start = Carbon::parse(Request::get('start'))->toDateString();

        $end = Carbon::parse(Request::get('end'))->toDateString();

        $project = Request::get('project_id');

        $this->queue(function () use ($userId, $start, $end, $project) {
            \Artisan::call('crims:re_commission_summary', [
                'user_id' => $userId,
                'start' => $start,
                'end' => $end,
                'project' => $project
            ]);
        });

        \Flash::success('The real estate commissions report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportRealEstateHoldingSummary()
    {
        $userId = \Auth::user()->id;

        $project = Request::get('project_id');

        $date = Request::get('date');

        $this->queue(function () use ($userId, $project, $date) {
            \Artisan::call('crims:re_holdings_summary', [
                'user_id' => $userId,
                'project_id' => $project,
                'date' => $date
            ]);
        });

        \Flash::success('The real estate holdings summary report will be sent shortly via email.');

        return \Redirect::back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportRealEstateMonthlyHoldingSummary()
    {
        $userId = \Auth::user()->id;

        $project = Request::get('project_id');

        $start = Request::get('start');

        $end = Request::get('end');

        $this->queue(function () use ($userId, $project, $start, $end) {
            \Artisan::call('crims:re_holdings_monthly_summary', [
                'user_id' => $userId,
                'start' => $start,
                'end' => $end,
                'project_id' => $project
            ]);
        });

        \Flash::success('The real estate monthly holdings summary report will be sent shortly via email.');

        return \Redirect::back();
    }
}
