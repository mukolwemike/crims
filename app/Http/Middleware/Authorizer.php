<?php

namespace App\Http\Middleware;

use App\Cytonn\Models\User;
use Closure;

class Authorizer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $authorizer = app('crims.authorizer');

        $authorizer->authorize($permission, [], true);

        return $next($request);
    }
}
