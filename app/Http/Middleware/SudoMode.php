<?php
/**
 * Date: 08/05/2017
 * Time: 17:00
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SudoMode
{
    public function handle($request, Closure $next)
    {
        if (!(new \Cytonn\Authentication\AuthRepository(Auth::user()))->isInSudoMode()) {
            return redirect()->guest(\Illuminate\Support\Facades\URL::route('set_sudo_mode'));
        }

        return $next($request);
    }
}
