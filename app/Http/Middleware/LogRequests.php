<?php

namespace App\Http\Middleware;

use Closure;
use Cytonn\Models\CrimsAdminLoginTrail;
use Cytonn\Models\CrimsAdminPageView;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class LogRequests
{
    protected $excludes = [
        '_debugbar'
    ];


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        if ($this->excluded($request)) {
            return;
        }

        $data = [
            'id' => Uuid::generate()->string,
            'user_id' => auth()->id(),
            'user_agent' => $request->server('HTTP_USER_AGENT'),
            'ip' => $request->ip(),
            'url' => $request->url()
        ];

        if (app()->environment() == 'production') {
            delaySave(CrimsAdminPageView::class, $data);
        }
    }

    protected function excluded(Request $request)
    {
        if (app()->environment() != 'production') {
            return true;
        }

        return collect($this->excludes)->reduce(
            function ($carry, $item) use ($request) {
                return $carry || str_contains($request->url(), $item);
            },
            false
        );
    }
}
