<?php

namespace App\Http\Middleware;

use Barryvdh\Debugbar\Middleware\Debugbar;
use Closure;

class DebugBarMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if (\Auth::user() && \Auth::user()->isAbleTo('user_management')) {
//
//            \Barryvdh\Debugbar\Facade::enable();
//        }

        return $next($request);
    }
}
