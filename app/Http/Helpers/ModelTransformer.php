<?php

namespace App\Http\Helpers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

class ModelTransformer extends TransformerAbstract
{
    public function transform(Model $model)
    {
        return $model->toArray();
    }
}
