<?php
namespace App\Http\Helpers;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 01/02/2017
 * Time: 09:28
 */
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class TableState
{
    public static function paginate(Request $request, $collection)
    {
        $page = $request->get('page');

        $perPage = $request->get('per_page');

        $offset = ($page * $perPage) - $perPage;
        $data=$collection->slice($offset, $perPage, true);
        return new LengthAwarePaginator($data, $collection->count(), $perPage, $perPage);
    }
}
