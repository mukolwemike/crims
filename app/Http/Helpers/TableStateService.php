<?php
namespace App\Http\Helpers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

trait TableStateService
{
    protected $tableQuery = null;

    protected function processTable(
        $modelOrBuilder,
        TransformerAbstract $transformer = null,
        \Closure $filterFunc = null,
        \Closure $modifyResource = null
    ) {
        !is_null($transformer) ?: $transformer = new ModelTransformer();

        if ($modelOrBuilder instanceof Model) {
            $modelOrBuilder = $modelOrBuilder->newQuery();
        }

        if ($modelOrBuilder instanceof Collection) {
            $coll = $modelOrBuilder;
            $resource = new \League\Fractal\Resource\Collection($coll, $transformer);
        } else {
            $state = \Input::get('tableState');

            !isset($state['pagination']['perPage']) ? : $this->perPage = $state['pagination']['perPage'];

            if (isset($state['search']['default'])) {
                $modelOrBuilder->search($state['search']['default']);
            }

            if (!is_null($filterFunc)) {
                $modelOrBuilder = $filterFunc($modelOrBuilder);
            }

            $page = isset($state['pagination']['page']) ? $state['pagination']['page'] : 1;

            $this->tableQuery = clone $modelOrBuilder;

            $coll = $modelOrBuilder->paginate($this->perPage, ['*'], 'page', $page);

            $resource = new \League\Fractal\Resource\Collection($coll, $transformer);
        }

        if (!is_null($modifyResource)) {
            $modifyResource($resource);
        }

        $resource->setMetaValue('pagination', $this->processPagination($coll));

        $manager = new Manager();

        if (isset($state['includes'])) {
            $manager = $manager->parseIncludes($state['includes']);
        }

        return $manager->createData($resource)->toJson();
    }

    private function processPagination($paginator)
    {
        if ($paginator instanceof LengthAwarePaginator) {
            return (object)[
                'total'=>  $paginator->total(),
                'current' => $paginator->currentPage(),
                'number_of_pages' => $paginator->lastPage(),
            ];
        } elseif ($paginator instanceof Collection) {
            return (object)[
                'total'=>  $paginator->count(),
                'current' => 1,
                'number_of_pages' => 1,
            ];
        } else {
            throw new \InvalidArgumentException("Paginator not supported");
        }
    }

    protected function getTableState($key = null)
    {
        $state = \Input::get('tableState');

        if (is_null($key)) {
            return $state;
        }

        $rec = function ($array, $keys) use (&$rec) {
            if (isset($keys[0]) && isset($array[$keys[0]])) {
                $array = $array[$keys[0]];
                array_shift($keys);

                return $rec($array, $keys);
            } else {
                return is_string($array) ? $array : null ;
            }
        };

        return $rec($state, explode('.', $key));
    }
}
