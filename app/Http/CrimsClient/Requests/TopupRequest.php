<?php

namespace App\Http\CrimsClient\Requests;

class TopupRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount'=>'required|numeric',
            'product' => 'required_if:product_category, "products"',
            'tenor' => 'required_if:product_category, "products"',
            'agreed_rate' => 'required_if:product_category, "products"',
            'unit_fund' => 'required_if:product_category, "funds"',
            'file' => 'mimes:jpeg,jpg,bmp,png,pdf'
        ];
    }
}
