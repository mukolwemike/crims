<?php
/**
 * Date: 13/05/2016
 * Time: 9:01 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Http\CrimsClient\Requests;

class ResetPasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'password'=>'required|confirmed|case_diff|numbers|letters'
        ];
    }
}
