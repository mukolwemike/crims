<?php

namespace App\Http\CrimsClient\Requests;

class WithdrawRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'withdrawal_stage' => 'required',
            'withdrawal_date' => 'required_if:withdrawal_stage,premature|date',
            'amount_select' => 'required',
            'amount' => 'required_if:amount_select,amount|numeric',
            'reason' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'amount.number'=>'The amount must be a number',
            'amount.required_if'=>'The amount is required',
            'reason.required'=>'Reason for withdrawal is required'
        ];
    }
}
