<?php

namespace App\Http\CrimsClient\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JointHolderForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_id' => 'required',
            'firstname'=>'required',
            'lastname'=>'required',
            'pin_no'=>'required',
            'id_or_passport'=>'required',
            'email'=>'required|email',
            'telephone_cell'=>'required',
        ];
    }
}
