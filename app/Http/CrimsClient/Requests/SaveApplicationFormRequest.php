<?php

namespace App\Http\CrimsClient\Requests;

use App\Cytonn\Models\Unitization\UnitFund;

class SaveApplicationFormRequest extends Request
{
    protected $rules = [
        'email' => 'required|email',
        'method_of_contact_id' => 'required',
        'pin_no' => 'required',
        'investor_account_name' => 'required',
        'investor_account_number' => 'required',
        'investor_bank' => 'required',
        'investor_bank_branch' => 'required'
    ];

    protected $messages = [
        'email.required' => 'Please provide an email',
        'telephone_home.required' => 'Please provide a phone number',
        'telephone_office.required' => 'Please provide a phone number',
        'product_id.exists' => 'Please select a valid product',
        'product_id.required' => 'Please select a product',
        'pin_no.required' => 'Please provide your pin number',
        'method_of_contact_id' => 'Please select a preferred method of contact',
        'investor_account_name.required' => 'Please enter your bank account name',
        'investor_account_number.required' => 'Please enter your account number',
        'investor_bank.required' => 'Please enter your bank name',
        'investor_bank_branch.required' => 'Please enter your bank branch'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = $this->segment(4);
        $request = $this->request->all();

        if ($type == 'individual') {
            if ($request['product_category'] == 'funds') {
                $fund_name = UnitFund::where('id', $request['unit_fund_id'])->first()->short_name;

                if ($fund_name == "CMMF") {
                    $this->validateCMMFIndividual();
                } else {
                    $this->validateIndividual();
                }
            } else {
                $this->validateIndividual();
            }
        } elseif ($type == 'corporate') {
            $this->validateCorporate();
        }

        return $this->rules;
    }

    public function messages()
    {
        return $this->messages;
    }

    public function validateIndividual()
    {
        $this->rules = $this->rules + [
                'telephone_home' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required'
            ];

        $this->messages = $this->messages + [
                'dob.required' => 'Please provide your date of birth'
            ];
    }

    public function validateCMMFIndividual()
    {
        $this->rules = [
            'firstname' => 'required|min:3',
            'lastname' => 'required|min:3',
            'telephone_home' => 'required|min:6',
            'email' => 'email',
            'id_or_passport' => 'required|min:5'
        ];

        $this->messages = [
            'firstname.required' => 'Please provide your firstname',
            'lastname.required' => 'Please provide your lastname',
            'email.email' => 'Please provide a valid email address',
            'telephone_home.required' => 'Please provide a phone number',
            'id_or_passport.required' => 'Please provide your ID/Passport number',
        ];
    }

    public function validateCorporate()
    {
        $this->rules = $this->rules + [
                'registered_name' => 'required',
                'street' => 'required',
                'telephone_office' => 'required'
            ];

        $this->messages = $this->messages + [
                'pin.required' => 'Please provide your tax pin number',
                'street.required' => 'Please provide your physical address'
            ];
    }
}
