<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 24/01/2019
 * Time: 09:19
 */

namespace App\Http\CrimsClient\Requests;

class MakePaymentRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount'=>'required|numeric',
            'slug' => 'required',
            'payment_date' => 'required|date',
            'mode_of_payment' => 'required',
            'unit_fund' => 'required_if:product_category, "funds"',
            'file' => 'required',
        ];
    }
}
