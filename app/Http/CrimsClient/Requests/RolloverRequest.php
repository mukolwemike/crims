<?php

namespace App\Http\CrimsClient\Requests;

class RolloverRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'agreed_rate'=>'numeric',
            'amount_select'=>'required',
            'amount'=>'required_if:amount_select,amount|numeric',
            'tenor'=>'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'tenor.required' => 'Please select the duration or enter a value'
        ];
    }
}
