<?php

namespace App\Http\CrimsClient\Requests;

class KycFilesUploadRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'slug'=>'required',
            'file'=>'required'
        ];
    }
}
