<?php

namespace App\Http\CrimsClient\Controllers\Investments;

use App\Cytonn\Authentication\Client\AuthRepository;
use App\Cytonn\Authentication\MailAuth\Auth;
use App\Cytonn\Authentication\TokenVerification;
use App\Cytonn\Investment\Calculator;
use App\Cytonn\Investment\ClientInvestments\ClientInvestmentRepository;
use App\Cytonn\Investment\InvestmentActionHandler;
use App\Cytonn\Investment\Reports\Statement;
use App\Cytonn\Mailers\System\PaymentProofUploadedMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientUser as User;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\Product;
use App\Events\ClientUserNotificationUpdated;
use App\Events\Investments\InvestmentActionPerformed;
use App\Events\Investments\TopupFormFilled;
use App\Exceptions\CrimsException;
use App\Exceptions\Handler;
use App\Http\CrimsClient\Controllers\Controller;
use App\Http\CrimsClient\Requests\RolloverRequest;
use App\Http\CrimsClient\Requests\TopupRequest;
use App\Http\CrimsClient\Requests\WithdrawRequest;
use App\Http\CrimsClient\Transformers\InvestmentMaturityTransfomer;
use App\Http\CrimsClient\Transformers\InvestmentTransformer;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\ProductTransformer;
use Cytonn\Api\Transformers\TopupFormTransformer;
use Cytonn\Authentication\Authy;
use Cytonn\FCM\FcmManager;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Investment\Rules\InvestmentInstructionRules;
use Cytonn\Investment\Statements\StatementProcessor;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class InvestmentsController extends Controller
{
    use InvestmentInstructionRules;

    use AlternateSortFilterPaginateTrait;

    use InvestmentInstructionRules;

    protected $totalInvestmentBalance;

    protected $totalInvestmentInterest;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function getActiveInvestments($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $access = $this->user()->isFaOrOwn($client, false);

        if (!$access) {
            return $this->response()
                ->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $investmentsRepository = new InvestmentsRepository();

        $investmentDetails = $investmentsRepository->getInvestmentDetails($client);

        return $this->response()->json([
            'data' => [
                'investments' => $investmentDetails,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }


    /**
     * @param $clientId
     * @param $productId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function getInvestmentDetails($clientId, $productId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $this->user()->isFaOrOwn($client);

        $processor = new StatementProcessor();

        $investments = $processor->process($client, Product::find($productId), Carbon::today());

        return $this->response()->json([
            'data' => [$investments],
            'status' => 200
        ]);
    }

    public function clientInvestments($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $access = $this->user()->accessClient($client);

        if (!$access) {
            return response()->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $inv = $client->repo->clientInvestments();

        return response()->json([
            'inv' => $inv,
        ]);
    }

    public function activeInvestments($clientId, $productId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $product = Product::find($productId);

        if (!$product) {
            return response()->json(['message' => 'No product with this id', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $access = $this->user()->accessClient($client);

        if (!$access) {
            return response()->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $repo = new ClientInvestmentRepository();

        $data = $repo->investments($client, $product)->toArray();

        $data['meta'] = $repo->totals($client, $product);

        return $this->response->json($data);
    }

    public function investments($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $access = $this->user()->accessClient($client);

        if (!$access) {
            return response()->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $investments = $client->repo->getInvestmentsForClient();

        return $this->response()->collection($investments, new InvestmentTransformer());
    }

    public function investment($investmentId)
    {
        $investment = ClientInvestment::findByUuid($investmentId);

        if (is_null($investment)) {
            return response()->json(['message' => 'Investment not found', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $access = $this->user()->accessClient($investment->client);

        if (!$access) {
            return response()->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $transformer = (new InvestmentTransformer())->append(['activities', 'instructions']);

        return $this->response()->item($investment, $transformer);
    }

    /**
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function topups($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $access = $this->user()->isFaOrOwn($client, false);

        if (!$access) {
            return $this->response()->json([
                'message' => 'No access to this client',
                'status' => Response::HTTP_UNAUTHORIZED
            ]);
        }

        $topups = $client->repo->getPendingTopupsForClient();

        $topups = $topups->map(function ($topup) {
            return (new TopupFormTransformer())->transform($topup);
        });

        return $this->response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => [
                'count' => $topups->count(),
                'topups' => $topups,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
        ], Response::HTTP_CREATED);
    }

    public function getTopup($topupId)
    {
        $topup = ClientTopupForm::findOrFailByUuid($topupId);

        return $this->response->item($topup, new TopupFormTransformer());
    }

    /**
     * @param TopupRequest $request
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function topup(TopupRequest $request, $clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $user = $this->user();

        $user->isFaOrOwn($client);

        $handler = new InvestmentActionHandler();

        $form = $handler->topUp($client, $request->all(), $user);

        if ($request->hasFile('file')) {
            $this->uploadBankTransfer($request, $form->uuid);

            (new PaymentProofUploadedMailer())->notify($form->id, 'topup');
        }

        event(new TopupFormFilled($form));

        $client->clientUsers->each(function ($user) {
            broadcast(new ClientUserNotificationUpdated($user));
        });

        $fcmManager = new FcmManager();

        $fcmManager->sendFcmMessageToMultipleDevices(
            $client,
            $form->uuid,
            $form->product->id,
            "Top up request",
            'Top up request submitted successfully',
            'topup'
        );

        return $this->response()->json([
            'data' => ['message' => 'Topup uploaded successfully'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED, [
            'location' => $form->uuid
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function sendStatementToClient(Request $request)
    {
        $product = Product::findOrFail($request->get('productId'));

        $client = Client::findOrFailByUuid($request->get('clientId'));

        $user = $this->user();

        $user->isFaOrOwn($client);

        $dateFrom = ($request->has('dateFrom')) ? new Carbon($request->get('dateFrom')) : null;

        $date = new Carbon($request->get('statementDate'));

        $file_name = 'Cytonn Investments ' . $product->name . ' Statement (' . $date->toDateString() . ')';

        $statement = (new Statement())->passwordProtect(true)->generate(
            $product,
            $client,
            $dateFrom,
            $date->copy()->startOfDay(),
            $user
        )->download($file_name . '.pdf');

        return $this->response()->json([
            'data' => [
                'message' => 'Sent statement to your email',
                'statement' => mb_convert_encoding($statement, 'base64', 'base64'),
                'name' => $file_name
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @param WithdrawRequest $request
     * @param $investmentId
     * @return mixed
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function withdraw(WithdrawRequest $request, $investmentId)
    {
        $investment = ClientInvestment::findOrFailByUuid($investmentId);

        $this->user()->shouldAccessClient($investment->client);

        $handler = new InvestmentActionHandler();

        $instruction = $handler->withdraw($investment, $request->input(), $this->user());

        event(new InvestmentActionPerformed($instruction));

        return $this->response()->created();
    }

    /**
     * @param Request $request
     * @param $invId
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function investmentWithdrawal(Request $request, $invId)
    {
        $investment = ClientInvestment::findOrFailByUuid($invId);

        $allow = $investment->repo->canWithdraw();

        if (!$allow['status']) {
            return $this->response()->json([
                'status' => Response::HTTP_NOT_ACCEPTABLE,
                'message' => $allow['reason']
            ]);
        }

        $user = $request->user();

        $user->isFaOrOwn($investment->client);

        $handler = new InvestmentActionHandler();

        $instruction = $handler->withdraw($investment, $request->all(), $user);

        event(new InvestmentActionPerformed($instruction));

        $instruction->investment->client->clientUsers->each(function ($user) {
            broadcast(new ClientUserNotificationUpdated($user));
        });

        $investmentTransformer = (new InvestmentTransformer())->append(['activities', 'instructions']);

        $fcmManager = new FcmManager();

        $fcmManager->sendFcmMessageToMultipleDevices(
            $investment->client,
            $investment->uuid,
            $investment->product->id,
            "Withdrawal Instruction",
            "Success, withdrawal request submitted",
            'withdrawal'
        );

        return $this->response()->json([
            'data' => [
                'message' => 'Withdrawal request submitted successfully',
                'can_withdraw' => [
                    'status' => false,
                    'reason' => 'Withdrawal scheduled',
                ],
                'instructions' => $investmentTransformer->instructions($investment)
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function rollover(RolloverRequest $request, $investmentId)
    {
        $validator = $this->rolloverValidate(request());

        if ($validator) {
            return response(
                [
                    'errors' => $validator->messages()->getMessages(),
                    'status' => 422
                ]
            );
        }

        $investment = ClientInvestment::findOrFailByUuid($investmentId);

        $user = $request->user();

        $access = $user->isFaOrOwn($investment->client, false);

        if (!$access) {
            return response()->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $data = $request->except('status', 'currency', 'form');

        $data['investment_id'] = $investment->id;

        $handler = new InvestmentActionHandler();

        $instruction = $handler->rollover($investment, $data, $user);

        $amount = $instruction->repo->calculateAmountAffected();

        $clientAmount = $investment->client->repo
            ->getActiveInvestmentTotalValueForClientForProduct($investment->product);

        $dueDate = $instruction->withdrawal_stage == 'mature' ? $investment->maturity_date : $instruction->due_date;

        $investmentValue = $investment->calculate(Carbon::parse($dueDate), false)->value();

        $totalAmount = $clientAmount - $investmentValue + $amount;

        $rate = $investment->product->repo->interestRate($instruction->tenor, $totalAmount);

        $instruction->update(['agreed_rate' => $rate]);

        event(new InvestmentActionPerformed($instruction));

        $instruction->investment->client->clientUsers->each(function ($user) {
            broadcast(new ClientUserNotificationUpdated($user));
        });

        $investmentTransformer = (new InvestmentTransformer())->append(['activities', 'instructions']);

        $fcmManager = new FcmManager();

        $fcmManager->sendFcmMessageToMultipleDevices(
            $investment->client,
            $investment->uuid,
            $investment->product->id,
            "Rollover Instruction",
            "Success, Rollover request submitted",
            'rollover'
        );

        return $this->response()->json([
            'data' => [
                'message' => 'Rollover request submitted successfully',
                'can_withdraw' => [
                    'status' => false,
                    'reason' => 'Rollover Scheduled'
                ],
                'instructions' => $investmentTransformer->instructions($investment)
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function getProducts($type = null)
    {
        $products = is_null($type)
            ?
            Product::where('active', true)->get()
            :
            Product::where('active', true)->whereHas('type', function ($t) use ($type) {
                $t->where('slug', $type);
            })->get();

        $products = $products->map(function ($product) {
            return (new ProductTransformer())->transform($product);
        });

        return $this->response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => $products
        ]);
    }

    public function myProducts($clientId)
    {
        try {
            $client = Client::findOrFailByUuid($clientId);

            $products = (new Product())->whereHas('investments', function ($investment) use ($client) {
                $investment->where('client_id', $client->id);
            })->get();

            $access = $this->user()->isFaOrOwn($client, false);

            if (!$access) {
                return $this->response()->json([
                    'message' => 'No access to this client',
                    'status' => Response::HTTP_UNAUTHORIZED
                ]);
            }
        } catch (\Exception $e) {
            $products = Product::where('active', 1)->get();

            app(Handler::class)->report($e);
        }

        return $this->response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => $products
        ]);
    }

    /**
     * @param $productId
     * @param $clientId
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function statementPdf($productId, $clientId, Request $request)
    {
        list($client, $product, $dateTo, $dateFrom) = $this->statementDetails($request, $productId, $clientId);

        $this->user()->isFaOrOwn($client);

        return (new Statement())
            ->passwordProtect(true)
            ->generate($product, $client, $dateFrom->startOfDay(), $dateTo->endOfDay(), $this->user())
            ->stream();
    }

    public function statementExcel($productId, $clientId, Request $request)
    {
        list($client, $product, $dateTo, $dateFrom) = $this->statementDetails($request, $productId, $clientId);

        $this->user()->isFaOrOwn($client);

        return (new Statement())
            ->passwordProtect(true)
            ->excel($product, $client, $dateFrom->startOfDay(), $dateTo->endOfDay(), $this->user())
            ->stream();
    }

    /**
     * @param Request $request
     * @param $productId
     * @param $clientId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function statement(Request $request, $productId, $clientId)
    {
        list($client, $product, $dateTo, $dateFrom) = $this->statementDetails($request, $productId, $clientId);

        $this->user()->isFaOrOwn($client);

        return (new Statement())->html($product, $client, $dateFrom->startOfDay(), $dateTo->endOfDay(), $this->user());
    }

    public function uploadBankTransfer(Request $request, $topupId)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if (str_contains($file->getClientMimeType(), 'pdf') ||
                str_contains($file->getClientMimeType(), 'image')) {
                $topup = ClientTopupForm::findOrFailByUuid($topupId);

                $contents = file_get_contents($file);
                $ext = $file->getClientOriginalExtension();

                $document = (new Document())
                    ->upload($contents, $ext, 'payment', 'Proof of payment', Carbon::today());
                $document->client_id = $topup->client_id;
                $document->save();

                //get the id and update to the topup form table
                $topup->document_id = $document->id;
                $topup->save();

                return $this->response()->json([
                    'data' => [
                        'message' => 'Transfer advice successfully uploaded',
                        'topup' => (new TopupFormTransformer())->transform($topup)
                    ],
                    'status' => Response::HTTP_CREATED
                ], Response::HTTP_CREATED, [
                    'location' => $document->uuid
                ]);
            } else {
                return response(json_encode(['file' => ['The file should be an image or pdf']]));
            }
        }
    }

    public function amountAffected(Request $request, $invId)
    {
        $instruction = $request->all();
        $investment = ClientInvestment::findOrFailByUuid($invId);

        ($instruction['date'] = "mature")
            ? $date = Carbon::parse($investment->maturity_date)
            : $date = Carbon::parse($instruction['date']);

        if ($instruction['amount_select'] == 'principal') {
            return is_null($investment) ? null
                : [
                    'currency' => $investment->product->currency->code,
                    'amount' => $investment->amount,
                ];
        } elseif ($instruction['amount_select'] == 'principal_interest') {
            return [
                'currency' => $investment->product->currency->code,
                'amount' => $investment->repo->getTotalValueOfAnInvestment($date) + $investment->withdraw_amount,
            ];
        } elseif ($instruction['amount_select'] == 'interest') {
            return [
                'currency' => $investment->product->currency->code,
                'amount' => $investment->repo->getGrossInterestForInvestment($date) + $investment->withdraw_amount,
            ];
        }

        return null;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function checkWithdrawalStatus($id)
    {
        $clientInvestment = ClientInvestment::findByUuid($id);

        $client = $clientInvestment->client;

        $this->user()->isFaOrOwn($client);

        $status = $clientInvestment->repo->canWithdraw();

        return $this->response()->json([
            'data' => $status,
            'status' => 200
        ]);
    }

    /**
     * @param $clientId
     * @return string
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function maturities($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $this->user()->shouldAccessClient($client);

        $maturities = (new InvestmentsRepository())->upcomingMaturities($client);

        return
            $this->processTable($maturities, new InvestmentMaturityTransfomer(), null, function ($maturity) {
                return $maturity;
            });
    }

    public function upcomingMaturities($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $access = $this->user()->isFaOrOwn($client, false);

        if (!$access) {
            return $this->response()->json(
                ['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]
            );
        }

        $maturities = (new InvestmentsRepository())->upcomingMaturities($client);

        $investmentMaturityTransformer = new InvestmentMaturityTransfomer();

        $maturities = $maturities->map(function ($maturity) use ($investmentMaturityTransformer) {
            return $investmentMaturityTransformer->transform($maturity);
        });

        return $this->response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => [
                'upcomingMaturities' => $maturities,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
        ], Response::HTTP_CREATED);
    }

    /**
     * @param $clientId
     * @return mixed
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function pendingTopups($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $this->user()->shouldAccessClient($client);

        $topups = $client->repo->getPendingTopupsForClient();

        return $this->response->collection($topups, new TopupFormTransformer(), ['count' => $topups->count()]);
    }

    public function calculateReturn(Request $request)
    {
        $investmentCalculator = new Calculator();

        $data = $investmentCalculator->calculateReturn($request);

        return $this->response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => $data
        ], Response::HTTP_CREATED);
    }

    public function investmentReturns()
    {
        $input = request()->all();

        $calculator = new Calculator();

        $result = $calculator->calculateInvestmentReturns($input);

        return response()->json([
            'data' => $result,
            'status' => 200
        ]);
    }

    public function calculateGoal(Request $request)
    {
        $investmentCalculator = new Calculator();

        $data = $investmentCalculator->calculateGoal($request);

        return $this->response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => $data
        ], Response::HTTP_CREATED);
    }

    public function investmentGoal()
    {
        $input = request()->all();

        $calculator = new Calculator();

        $result = $calculator->calculateInvestmentGoal($input);

        return response()->json([
            'data' => $result,
            'status' => 200
        ]);
    }

    /**
     * @param Request $request
     * @param $productId
     * @param $clientId
     * @return array
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function statementDetails(Request $request, $productId, $clientId): array
    {
        $client = Client::findOrFailByUuid($clientId);

        $product = Product::findOrFail($productId);

        $this->user()->isFaOrOwn($client);

        $dateTo = new Carbon($request->get('statement_date'));

        $dateFrom = new Carbon($request->get('date_from'));

        return array($client, $product, $dateTo, $dateFrom);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws CrimsException
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function sendVerifyToken()
    {
        $user = User::findByUuid(request()->get('userID'));

        $method = request()->get('option');

        $message = $method == 'authy' ? 'Verify Token' : '';

        $response = (new TokenVerification($user))->send($method, $message);

        return $this->response()->json([
            'verified' => $response->success ? true : false,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function validateToken()
    {
        $input = \request()->all();

        $user = User::findByUuid($input['userID']);

        $method = $input['option'];

        $validate = (new TokenVerification($user))->verify($method);

        return $this->response()->json([
            'validated' => $validate && isset($validate->success) ? $validate->success : false,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }
}
