<?php

namespace App\Http\CrimsClient\Controllers\Investments;

use App\Cytonn\Investment\Calculator;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Product;
use App\Http\CrimsClient\Controllers\Controller;
use Cytonn\Api\Transformers\ProductTransformer;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $products = Product::all();

        return $this->response()->collection($products, new ProductTransformer());
    }

    public function interestRate(Request $request)
    {
        $product = Product::findOrFail($request->input('id'));

        $rates = InterestRateUpdate::where('product_id', $product->id)
            ->orderBy('created_at', 'desc')
            ->first()->rates;

        return $rates;
    }

    public function getProductInterestRate(Request $request)
    {
        $product = $this->getInterestRates($request);

        return response()->json(['productInterestRate' => $product, 'status' => 200]);
    }

    public function getRates(Request $request)
    {
        $product = (new Product())->findOrFail($request->input('product_id'));

        return $product->repo->getPrevailingRate($request->input('tenor'));
    }

    public function getInterestRates()
    {
        $input = request()->all();

        if (!isset($input['amount'])) {
            $input['amount'] = 0;
        }

        $product = Product::findOrFail(request()->get('product_id'));

        if (isset($input['clientId'])) {
            $client = Client::findOrFailByUuid($input['clientId']);
            $input['amount'] = $input['amount'] +
                $client->repo->getActiveInvestmentTotalValueForClientForProduct($product);
        }

        return $product->repo->interestRate($input['tenor'], $input['amount']);
    }

    public function investmentRate()
    {
        $input = request()->all();

        $investment = ClientInvestment::findOrFailByUuid($input['investment_id']);

        $amount = $investment->client->repo->getActiveInvestmentTotalValueForClientForProduct($investment->product);

        return $investment->product->repo->interestRate($input['tenor'], $amount);
    }

    public function globalRates()
    {
        $input = request()->all();

        $product = Product::findOrFail($input['product']);

        return $product->repo->globalRate($input['tenor']);
    }

    public function getInvestmentDetails(Client $client)
    {
        $products = $client->repo->getActiveInvestmentsForClientQuery()
            ->orderBy('maturity_date')
            ->get()
            ->groupBy('product_id');

        return $products->map(function ($investments, $index) {
            $product = Product::find($index);

            $details = $this->investmentDetails($investments);

            $this->getTotalAmountInvestedInAProduct($details);

            return [
                'investments' => $details,
                'product' => [
                    'id' => $product->id,
                    'name' => $product->name,
                    'shortname' => $product->shortname,
                    'longname' => $product->longname,
                    'currency' => $product->currency->code],
                'totals' => [
                    'no_of_investments' => count($investments),
                    'total_value' => $this->totalInvestmentBalance,
                    'last_investment_date' =>
                        $investments->sortByDesc('invested_date')->pluck('invested_date')->first(),
                    'next_maturity_date' => $investments->pluck('maturity_date')->first()
                ]

            ];
        })->values();
    }

    public function currencies()
    {
        $currencyLists = Currency::all()->map(
            function ($currency) {
                return ['id'=> $currency->id,'label' => strtoupper($currency->name) . ' - ' . $currency->code, 'value' => $currency->code];
            }
        )->toArray();

        return response(['fetched' => true, 'data' => $currencyLists], 200);
    }

    public function prevailingRates($id)
    {
        $product = Product::findOrFail($id);

        $update = InterestRateUpdate::where('product_id', $product->id)
            ->orderBy('date', 'desc')
            ->first();

        if (!$update) {
            return [];
        }

        return $update->rates->groupBy('tenor');
    }
}
