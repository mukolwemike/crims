<?php

namespace App\Http\CrimsClient\Controllers\Documents;

use App\Cytonn\Api\Transformers\DocumentTypeTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use App\Http\CrimsClient\Controllers\Controller;
use App\Http\CrimsClient\Transformers\Documents\DocumentTransformer;
use Flash;
use Symfony\Component\HttpFoundation\Response;

class DocumentController extends Controller
{
    protected $docTransformer;

    public function __construct()
    {
        parent::__construct();
    }

    public function index($doc_type = null)
    {
        $clients = $this->user()->clients()->get()->pluck('id');

        $docQuery = Document::belongsToClient($clients)
            ->latest();

        return $this->processTable($docQuery, new DocumentTransformer());
    }

    public function show($uuid)
    {
        $clients = $this->user()->clients()->get()->pluck('id');

        $file = Document::where('uuid', $uuid)->belongsToClient($clients)->firstOrFail();

        if (!$file) {
            return abort(Response::HTTP_UNAUTHORIZED, "Seems like you do not have access to this file");
        }

        $file->checkAccess($this->user());

        return $this->streamFile($file->path());
    }

    /**
     * @param $id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function clientDocuments($id, $type = null)
    {
        $client = Client::findByUuid($id);

        $access = $this->user()->isFaOrOwn($client, false);

        if (!$access) {
            return $this->response()
                ->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $query = Document::belongsToClient($client)->latest('date')->latest();

        $documents = ($type === 'null')
            ? $query->get()
            : $query->where('type_id', $type)->get();
        
        $documents = $documents->map(function ($document) {
            return (new DocumentTransformer())->transform($document);
        });

        return $this->response()->json([
            'status' => 200,
            'data' => $documents
        ]);
    }

    public function documentTypes($client_id)
    {
        $client = Client::findOrFailByUuid($client_id);

        $docTypes = DocumentType::whereHas('documents', function ($document) use ($client) {
            $document->belongsToClient($client);
        })->get();

        return $docTypes->map(function ($type) {
            return ((new DocumentTypeTransformer())->transform($type));
        });
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|mixed
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function showDocument($id)
    {
        $file = Document::findByUuid($id);
        
        if (! $file) {
            Flash::error("We could not locate the selected document");

            return back();
        }

        $access = $this->user()->isFaOrOwn($this->user()->clients()->first(), false);
        
        if (!$access) {
            Flash::error("You do not have access to the selected document");

            return back();
        }

        return $this->streamFile($file->path());
    }
}
