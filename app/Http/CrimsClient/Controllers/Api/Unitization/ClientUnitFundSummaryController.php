<?php

namespace App\Http\CrimsClient\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\CrimsClient\Controllers\Controller;
use Cytonn\Api\Transformers\Unitization\UnitFundClientSummaryTransfomer;

class ClientUnitFundSummaryController extends Controller
{
    public function index($client_id)
    {
        $client = Client::findOrFail($client_id);

        return UnitFund::whereHas('purchases', function ($purchases) use ($client) {
            $purchases->where('client_id', $client->id);
        })
        ->get()
        ->map(function ($fund) use ($client) {
            return (new UnitFundClientSummaryTransfomer())->transform($client, $fund);
        });
    }
}
