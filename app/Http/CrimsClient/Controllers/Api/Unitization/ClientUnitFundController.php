<?php

namespace App\Http\CrimsClient\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Unitization\UnitFundClientRepository;
use App\Cytonn\Unitization\UnitFundClientSummary;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\Transformers\Unitization\UnitFundInvestmentInstructionTransformer;
use Symfony\Component\HttpFoundation\Response;

class ClientUnitFundController extends Controller
{
    public function index($client_id)
    {
        $client = Client::findOrFailByUuid($client_id);

        if ($notAllowed = $this->allow($client)) {
            return $notAllowed;
        }

        $unitFunds = UnitFund::whereHas('purchases', function ($purchases) use ($client) {
            $purchases->where('client_id', $client->id);
        })->where('active', 1)
        ->get()
        ->map(function ($fund) use ($client) {
            return (new UnitFundClientSummary($fund, $client))->index();
        });

        return response()->json([
            'data' => [
                'summaries' => $unitFunds,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function details($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);
        
        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $unitFunds = UnitFund::whereHas('purchases', function ($purchases) use ($client) {
            $purchases->where('client_id', $client->id);
        })
            ->get()
            ->map(function ($fund) use ($client) {
                return (new UnitFundClientRepository($client, $fund))->fundDetails();
            });

        return response()->json([
            'data' => [
                'summaries' => $unitFunds,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function previewStatement($fundId, $clientId, $date, $start = null)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $date = $date ? Carbon::parse($date) : Carbon::today();

        return (new UnitFundClientRepository(
            $client,
            $fund,
            $date->copy(),
            $start
        ))->previewStatement();
    }

    public function downloadStatement($fundId, $clientId, $start = null, $date = null)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $date = $date ? Carbon::parse($date) : Carbon::today();

        return (new UnitFundClientRepository(
            $client,
            $fund,
            $date->copy(),
            $start
        ))->downloadStatement();
    }

    public function downloadExcelStatement($fundId, $clientId, $start = null, $date = null)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $date = $date ? Carbon::parse($date) : Carbon::today();

        return (new UnitFundClientRepository(
            $client,
            $fund,
            $date->copy(),
            $start
        ))->excelStatement();
    }

    public function downloadMobileStatement($fundId, $clientId, $startDate, $endDate = null)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $startDate = Carbon::parse($startDate);

        $file_name = 'Cytonn Investments ' . $fund->name . ' Statement (' . $startDate->toDateString() . ')';

        $statement = (new UnitFundClientRepository(
            $client,
            $fund,
            Carbon::parse($endDate),
            $startDate
        ))->downloadStatement();

        return $this->response()->json([
            'data' => [
                'message' => 'Sent statement to your email',
                'statement' => mb_convert_encoding($statement, 'base64', 'base64'),
                'name' => $file_name
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function sendStatement($fundId, $clientId, $date, $start = null)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $date = Carbon::parse($date);

        return (new UnitFundClientRepository(
            $client,
            $fund,
            $date->copy(),
            $start
        ))->sendStatement();
    }

    public function loadStatement($fundId, $clientId, $start = null, $end = null)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $date = $end ? Carbon::parse($end) : Carbon::today();

        return (new UnitFundClientRepository(
            $client,
            $fund,
            $date->copy(),
            $start
        ))->loadStatement();
    }

    public function balance($fundId, $clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        return response((new UnitFundClientRepository($client, $fund))->balance());
    }

    public function unitsOwned($fundId, $clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $client = Client::findOrFail($clientId);

        $fund = UnitFund::findOrFail($fundId);

        return response((new UnitFundClientRepository($client, $fund))->ownedNumberOfUnits());
    }

    public function recentActivities($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $summary = new UnitFundClientSummary(null, $client);

        return response()->json($summary->recentActivities());
    }

    public function pendingInstructions($fundId, $clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $summary = new UnitFundClientSummary($fund, $client);

        return response()->json($summary->instruction());
    }

    public function allow(Client $client)
    {
        if (!$this->user()->isFaOrOwn($client, false)) {
            return response()->json([
                'message' => 'No access to this client',
                'status' => Response::HTTP_UNAUTHORIZED
            ]);
        }

        return false;
    }

    public function funds()
    {
        return response()->json([
            'data' => [
                'funds' => UnitFund::all(),
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function clientFunds($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $unitFunds = UnitFund::active()->ofType('cis')->whereHas('purchases', function ($purchases) use ($client) {
            $purchases->where('client_id', $client->id);
        })
            ->get()
            ->map(function ($fund) use ($client) {
                return [
                    'id' => $fund->id,
                    'name' => $fund->name,
                    'shortname' => $fund->short_name,
                    'type' => 'ut'
                ];
            });

        return response()->json([
            'data' => [
                'funds' => $unitFunds,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function getFundBankMpesaAccounts($fundId)
    {
        $fund = UnitFund::where('id', $fundId)->first();

        $bank = $fund->repo->collectionAccount('bank');

        $mpesa = $fund->repo->collectionAccount('mpesa');

        return response()->json([
            'data' => [
                'bank' => is_null($bank) ? null : [
                    'account_name' => $bank->account_name,
                    'bank_name' => $bank->bank_name,
                    'branch' => $bank->branch_name,
                    'number' => $bank->account_no,
                    'swift_code' => $bank->bank_swift
                ],
                'mpesa' => is_null($mpesa) ? null : [
                    'paybill' => $mpesa->account_no
                ],
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }
}
