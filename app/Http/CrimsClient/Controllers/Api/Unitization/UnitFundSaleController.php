<?php

namespace App\Http\CrimsClient\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\System\Locks\Facade\Lock;
use App\Cytonn\Unitization\UnitFundClientSummary;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use App\Events\Investments\Actions\UnitFundInstructionHasBeenMade;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\FCM\FcmManager;
use Cytonn\Unitization\Rules\UnitFundSaleRules;
use Cytonn\Unitization\Trading\Sell;
use Illuminate\Http\Response;

class UnitFundSaleController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundSaleRules;

    public function index($fundId, $uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $summary = new UnitFundClientSummary($fund, $client);

        return response()->json($summary->fundSales());
    }

    /**
     * @param $fundId
     * @param $uuid
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function store($fundId, $uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        $input = request()->all();

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return response([
                'message'=> 'We are processing one of your transaction, kindly wait a few seconds then try again.',
                'locked'=> $lock,
                'status' => 422
            ]);
        }

        $fund = UnitFund::find($input['unit_fund_id']);

        $validator = $this->saleCreate(request(), $client, $fund);

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $sellClass = (new Sell($fund, $client, Carbon::parse($input['date'])));

        $withdrawableUnits = $sellClass->withdrawableUnits();

        $clientAccount = array_key_exists('account_id', $input) ? ClientBankAccount::find($input['account_id']) :
            null;

        $transactionCharge = $sellClass->shouldChargeTransactionFee($clientAccount) ?
            $sellClass->calculateWithdrawalFee() : 0;

        if ((int)$input['number'] + $transactionCharge > $withdrawableUnits) {
            $chargeMessage = $transactionCharge > 0 ? " and transaction charge of $transactionCharge " : "";

            return response([
                'errors' => [
                    'number' => [
                        "The units to be withdrawn (" . $input['number'] . ")" . $chargeMessage ."are more than the withdrawable units
                         (" . (int)$withdrawableUnits . ")"
                    ]
                ],
                'status' => 422
            ]);
        }

        $input['user_id'] = isset($input['user_id']) ? ClientUser::findByUuid($input['user_id'])->id : null;

        $instruction = (new UnitFundInstructionsRepository())->sale($client, $fund, $input);

        event(new UnitFundInstructionHasBeenMade($instruction));

        $fcmManager = new FcmManager();

        $fcmManager->sendFcmMessageToMultipleDevices(
            $client,
            $instruction->id,
            $instruction->unitFund->id,
            "Sell units request",
            'Sell units request submitted successfully',
            'sellUnits'
        );

        return $this->response()->json([
            'data' => ['message' => 'Sell units instruction submitted successfully'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function allow(Client $client)
    {
        if (!$this->user()->isFaOrOwn($client, false)) {
            return response([
                'errors' => 'You have no access to client',
                'status' => 401
            ]);
        }

        return false;
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function calculateSaleCharge($fundId, $uuid)
    {
        $input = request()->all();

        $fund = UnitFund::findOrFail($fundId);

        $client = Client::findOrFailByUuid($uuid);

        $date = Carbon::parse($input['date']);

        $charge = (new Sell($fund, $client, $date))->calculateWithdrawalFee($input['units_sold']);

        return $this->response()->json([
            'data' => [
                'unit_sold' => $input['units_sold'],
                'fee' => $charge
            ],
            'status' => Response::HTTP_OK
        ], Response::HTTP_OK);
    }
}
