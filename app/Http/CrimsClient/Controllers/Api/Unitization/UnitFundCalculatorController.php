<?php

namespace App\Http\CrimsClient\Controllers\Api\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Unitization\Trading\Calculator;
use Symfony\Component\HttpFoundation\Response;

class UnitFundCalculatorController extends Controller
{
    public function calculate()
    {
        $input = request()->all();

        $unitfund = UnitFund::findOrFail($input['fund']);

        $date = (isset($input['date'])) ? Carbon::parse($input['date']) : Carbon::now();

        if (isset($input['amount'])) {
            return $this->calculateUnits($unitfund, $input, $date);
        } elseif (isset($input['units'])) {
            return $this->calculateAmount($unitfund, $input, $date);
        } else {
            return response([
                'status' => 403,
                'message' => 'Neither amount nor units is entered'
            ]);
        }
    }

    public function calculateUnits(UnitFund $fund, $input, Carbon $date)
    {
        $calculator = (new Calculator($fund, $input['amount'], null, $date));

        $data = [
            'fees' => $calculator->feesCharged(),
            'amount' => $input['amount'],
            'unitPrice' => $calculator->unitPrice(),
            'units' => $calculator->numberOfUnits(),
            'valueOfUnits' => $calculator->valueOfUnits(),
            'balance' => $calculator->balance(),
        ];

        return response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => $data
        ], Response::HTTP_CREATED);
    }

    public function calculateAmount(UnitFund $fund, $input, Carbon $date)
    {
        $calculator = (new Calculator($fund, null, $input['units'], $date));

        $data = [
            'fees' => $calculator->feesCharged(),
            'amount' => ($input['units'] * $calculator->unitPrice()) + $calculator->feesCharged(),
            'unitPrice' => $calculator->unitPrice(),
            'units' => $input['units'],
            'valueOfUnits' => $input['units'] * $calculator->unitPrice(),
            'balance' => 0,
        ];

        return response()->json([
            'status' => Response::HTTP_CREATED,
            'data' => $data
        ], Response::HTTP_CREATED);
    }
}
