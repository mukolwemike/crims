<?php

namespace App\Http\CrimsClient\Controllers\Api\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\System\Locks\Facade\Lock;
use App\Cytonn\Unitization\ProcessTransfer;
use App\Cytonn\Unitization\UnitFundClientSummary;
use App\Http\CrimsClient\Controllers\Controller;
use App\Jobs\Unitization\UnitFundProcessTransfer;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\ClientTransformer;
use Cytonn\FCM\FcmManager;
use Cytonn\Unitization\Rules\UnitFundTransferRules;
use Cytonn\Unitization\Trading\Sell;
use Symfony\Component\HttpFoundation\Response;

class UnitFundTransferController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundTransferRules;

    /**
     * @param $fundId
     * @param $uuid
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function given($fundId, $uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::find($fundId);

        $summary = new UnitFundClientSummary($fund, $client);

        return response()->json($summary->fundTransfersGiven());
    }

    /**
     * @param $fundId
     * @param $uuid
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function received($fundId, $uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::find($fundId);

        $summary = new UnitFundClientSummary($fund, $client);

        return response()->json($summary->fundTransfersReceived());
    }


    /**
     * @param Client $client
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function allow(Client $client)
    {
        if (!$this->user()->isFaOrOwn($client, false)) {
            return response([
                'errors' => 'You have no access to client',
                'status' => 401
            ]);
        }

        return false;
    }

    /**
     * @param $client_Code
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClient($client_Code)
    {
        $client = Client::where('client_code', $client_Code)->first();

        $data = (new ClientTransformer())->transform($client);

        $data['hasCMMF'] = $client->unitFundPurchases()
            ->whereHas('unitFund', function ($fund) {
                $fund->where('short_name', 'CMMF')->ofType('cis')->active();
            })->exists();

        return response()->json([
            'data' => $data,
            'status' => Response::HTTP_CREATED,
        ], Response::HTTP_CREATED);
    }

    /**
     * @param $fundId
     * @param $clientId
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     * @throws \Throwable
     */
    public function store($fundId, $clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return response([
                'message' => 'We are processing one of your transaction, kindly wait a few seconds then try again.',
                'locked' => $lock,
                'status' => 422
            ]);
        }

        $fund = UnitFund::find($fundId);

        $validator = $this->transferCreate(request(), $client, $fund);

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input = request()->all();

        if ((float)$input['number'] < ProcessTransfer::MIN_AMOUNT || $input['number'] > ProcessTransfer::MAX_AMOUNT) {
            return response([
                'errors' => [
                    'number' => [
                        "The units to be transferred (" . $input['number'] . ") are not between " .
                        ProcessTransfer::MIN_AMOUNT . " and " . ProcessTransfer::MAX_AMOUNT
                    ]
                ],
                'status' => 422
            ]);
        }

        $withdrawableUnits = (new Sell($fund, $client, Carbon::parse($input['date'])))->withdrawableUnits();

        if ((float)$input['number'] > $withdrawableUnits) {
            return response([
                'errors' => [
                    'number' => [
                        "The units to be transferred (" . $input['number'] . ") are more than the transferrable units
                         (" . (int)$withdrawableUnits . ")"
                    ]
                ],
                'status' => 422
            ]);
        }

        $instruction = $this->processTransfer($client, $fund, $input);

        if ($instruction) {
            $fcmManager = new FcmManager();

            $fcmManager->sendFcmMessageToMultipleDevices(
                $client,
                $instruction->id,
                $instruction->unitFund->id,
                "Transfer units request",
                'Transfer units request submitted successfully',
                'transferUnits'
            );
        }

        return $this->response()->json([
            'data' => ['message' => 'Transfer units instruction submitted successfully'],
            'status' => \Illuminate\Http\Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @param $client
     * @param $fund
     * @return mixed
     * @throws \Throwable
     */
    private function processTransfer($client, $fund, $input)
    {
        $input['user_id'] = isset($input['user_id']) ? ClientUser::findByUuid($input['user_id'])->id : getSystemUser()->id;
        $input['transferer_id'] = isset($input['transferer_id']) ? Client::findByUuid($input['transferer_id'])->id : $client->id;
        $input['unit_fund_id'] = isset($input['unit_fund_id']) ? UnitFund::findOrFail($input['unit_fund_id'])->id : $fund->id;
        $input['channel'] = 'web';

        //Queue voucher account transfers
        if ($client->client_code == 14132) {
            dispatch((new UnitFundProcessTransfer($input, $client, $fund))->onQueue(config('queue.priority.high')));

            return false;
        }

        return \DB::transaction(function () use ($input, $client, $fund) {
            $transferProcessor = new ProcessTransfer($input, $client, $fund);

            return $transferProcessor->process();
        });
    }
}
