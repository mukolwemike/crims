<?php

namespace App\Http\CrimsClient\Controllers\Api\Unitization;

use App\Cytonn\Mailers\System\PaymentProofUploadedMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\System\Locks\Facade\Lock;
use App\Cytonn\Unitization\UnitFundClientSummary;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use App\Cytonn\USSD\USSDRepository;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\FCM\FcmManager;
use Cytonn\Unitization\Rules\UnitFundPurchaseRules;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UnitFundPurchaseController extends Controller
{
    use AlternateSortFilterPaginateTrait, UnitFundPurchaseRules;

    public function index($fundId, $uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $fund = UnitFund::findOrFail($fundId);

        $summary = new UnitFundClientSummary($fund, $client);

        return response()->json($summary->fundPurchases());
    }

    /**
     * @param $fundId
     * @param $clientId
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function store($fundId, $clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $input = request()->all();

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return response([
                'message'=> 'We are processing one of your transaction, kindly wait a few seconds then try again.',
                'locked'=> $lock,
                'status' => 422
            ]);
        }

        $fund = UnitFund::findOrFail($input['unit_fund_id']);

        $validator = $this->purchaseCreate(request(), $client, $fund);

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input['user_id'] = isset($input['user_id']) ? ClientUser::findByUuid($input['user_id'])->id : null;

        $instruction = (new UnitFundInstructionsRepository())->purchase($input, $client, $fund);

        if (request()->hasFile('file')) {
            $this->uploadBankTransfer($instruction->id, request());

            (new PaymentProofUploadedMailer())->notify($instruction->id, $instruction->type->slug);
        }

        if ($input['mode_of_payment'] === 'mpesa') {
            (new USSDRepository())->sendStkPush($client, $fund, $input['amount']);
        }

//        event(new UnitFundInstructionHasBeenMade($instruction));

        $fcmManager = new FcmManager();

        $fcmManager->sendFcmMessageToMultipleDevices(
            $client,
            $instruction->id,
            $instruction->unitFund->id,
            "Buy units request",
            'Buy units request submitted successfully',
            'buy'
        );

        return $this->response()->json([
            'data' => ['message' => 'Buy units instruction submitted successfully'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function uploadBankTransfer($id, Request $request = null)
    {
        if (!$request) {
            $request = request()->all();
        }

        $instruction = UnitFundInvestmentInstruction::find($id);

        if ($request->hasFile('file')) {
            $file = $request->file('file');

            if (str_contains($file->getClientMimeType(), 'pdf') ||
                str_contains($file->getClientMimeType(), 'image')
            ) {
                $contents = file_get_contents($file);

                $ext = $file->getClientOriginalExtension();

                $document = (new Document())->upload(
                    $contents,
                    $ext,
                    'payment',
                    'Proof of payment',
                    Carbon::today()
                );

                $document->client_id = $instruction->client_id;

                $document->save();

                $instruction->document_id = $document->id;

                $instruction->save();

                return $this->response()->json([
                    'data' => [
                        'message' => 'Transfer advice successfully uploaded'
                    ],
                    'status' => Response::HTTP_CREATED
                ], Response::HTTP_CREATED, [
                    'location' => $document->uuid
                ]);
            } else {
                return response(json_encode(['file' => ['The file should be an image or pdf']]));
            }
        }
    }

    public function allow(Client $client)
    {
        if (!$this->user()->isFaOrOwn($client, false)) {
            return response([
                'errors' => 'You have no access to client',
                'status' => 401
            ]);
        }

        return false;
    }
}
