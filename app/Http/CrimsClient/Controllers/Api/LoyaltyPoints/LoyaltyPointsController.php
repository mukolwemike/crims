<?php

namespace App\Http\CrimsClient\Controllers\Api\LoyaltyPoints;

use App\Cytonn\Api\Transformers\Clients\LoyaltyRedeemInstructionsTransformer;
use App\Cytonn\Clients\ClientLoyaltyPoints\LoyaltyRedeemInstructionsRepository;
use App\Cytonn\Mailers\Client\LoyaltyPointsRedeemMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Models\RewardVoucher;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Clients\Approvals\Engine\Approval;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LoyaltyPointsController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    /**
     * @param $uuid
     * @return JsonResponse
     */
    public function loyaltyPointsSummaries($uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        $details = (new LoyaltyRedeemInstructionsRepository())->getCalculatedDetails($client);

        return response()->json([
            'details' => $details,
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function getVouchers()
    {
        $details = RewardVoucher::where('number', '>', 0)
            ->get()
            ->map(function ($voucher) {
                return $this->mapVoucher($voucher);
            });

        return response()->json([
            'details' => $details,
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function vouchersDetails()
    {
        return $this->sortFilterPaginate(
            new RewardVoucher(),
            [],
            function ($voucher) {
                return $this->mapVoucher($voucher);
            },
            function ($model) {
                return $model->where('number', '>', 0);
            }
        );
    }

    private function mapVoucher(RewardVoucher $voucher)
    {
        return [
            'id' => $voucher->id,
            'name' => $voucher->name,
            'value' => $voucher->value,
            'number' => $voucher->number,
            'document' => $voucher->document_id ? $voucher->document_id : null,
        ];
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|JsonResponse|Response
     * @throws \Throwable
     */
    public function redeemVoucher(Request $request)
    {
        $data = $request->all();

        $client = Client::findOrFailByUuid($data['client_id']);
        $user = ClientUser::findOrFailByUuid($data['user_id']);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $voucher = RewardVoucher::find($data['voucher_id']);

        if ($voucher->value > $client->calculateLoyalty()->getTotalPoints()) {
            return response([
                'errors' => 'Redeemable points are more than the client total points',
                'status' => 422
            ]);
        }

        $pendingInstruction = LoyaltyPointsRedeemInstructions::where('client_id', $client->id)
            ->whereNull('approval_id')
            ->exists();

        if ($pendingInstruction) {
            return response([
                'errors' => 'There is a pending request to redeem loyalty points',
                'status' => 422
            ]);
        }

        $data['client_id'] = $client->id;
        $data['user_id'] = $user->id;

        $instruction = app(LoyaltyPointsRedeemInstructions::class)->repo->saveInstruction($data, $voucher->value, 1);

        $data['instruction_id'] = $instruction->id;

        $approval = ClientTransactionApproval::add(
            [
                'client_id' => $client->id,
                'transaction_type' => 'redeem_loyalty_points',
                'payload' => $data
            ]
        );

        if (!$client->joint || $client->type->name != 'corporate') {
            $approve = new Approval($approval);

            $approve->systemExecute();

            $instruction->approval_id = $approval->id;

            $instruction->save();

            (new LoyaltyPointsRedeemMailer())->notify($client, $instruction);
        }

        return $this->response()->json([
            'data' => ['message' => 'Redeem Points process was successfully completed'],
            'status' => \Illuminate\Http\Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function allow(Client $client)
    {
        if (!$this->user()->isFaOrOwn($client, false)) {
            return response([
                'errors' => 'You have no access to client',
                'status' => 422
            ]);
        }

        return false;
    }

    public function loyaltyBrief($uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        $activities = LoyaltyPointsRedeemInstructions::where('client_id', $client->id)
            ->latest('date')
            ->take(5)
            ->get()
            ->map(function ($instruction) {
                return app(LoyaltyRedeemInstructionsTransformer::class)->transform($instruction);
            });

        return [
            'data' => $activities,
            'current_page' => 1,
            'from' => 1,
            'last_page' => 1,
            'next_page_url' => null,
            'path' => "/",
            'per_page' => "5",
            'prev_page_url' => null,
            'to' => count($activities),
            'total' => count($activities),
        ];
    }

    public function getRedeemInstructions($uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        return $this->sortFilterPaginate(
            new LoyaltyPointsRedeemInstructions(),
            [],
            function ($instruction) {
                return app(LoyaltyRedeemInstructionsTransformer::class)->transform($instruction);
            },
            function ($model) use ($client) {
                return $model->where('client_id', '=', $client->id);
            }
        );
    }
}
