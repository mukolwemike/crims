<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/9/18
 * Time: 12:57 PM
 */

namespace App\Http\CrimsClient\Controllers\Api\Client;

use App\Cytonn\Mailers\Client\ClientUpdateBankAccountMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Document;
use App\Jobs\USSD\ReferClient;
use Carbon\Carbon;
use Cytonn\Investment\ClientInstruction\ClientUploadedKycRepository;
use Illuminate\Http\Request;

class ClientController
{
    public function saveAccountDetails($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $input = request()->all();

        if ($input['formType'] == 'mpesa') {
            $input['phone_number'] = $this->cleanPhone($input['phone_number']);
        }

        if ($this->accountExists($client, $input) && !isset($input['id'])) {
            return response()->json(['status' => 422, 'errors' => 'Account Details already exists. Try Again']);
        }

        if (request()->hasFile('file')) {
            $input['document_id'] = $this->uploadAccountProof($client->id, request());

            unset($input['file']);
        }

        $input['client_id'] = $client->id;

        ClientTransactionApproval::add([
            'transaction_type' => 'add_client_account_details',
            'client_id' => $client->id,
            'payload' => $input
        ]);

        (new ClientUpdateBankAccountMailer())->notifyRequest($client);

        return response()->json(['status' => 202]);
    }

    private function uploadAccountProof($clientId, Request $request = null)
    {
        if (!$request) {
            $request = request()->all();
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');

            \Log::info($file);

            if (str_contains($file->getClientMimeType(), 'pdf') ||
                str_contains($file->getClientMimeType(), 'image')
            ) {
                $contents = file_get_contents($file);

                $ext = $file->getClientOriginalExtension();

                $document = (new Document())->upload(
                    $contents,
                    $ext,
                    'client_document',
                    'Bank Account Proof',
                    Carbon::today()
                );

                $document->client_id = $clientId;

                $document->save();

                return $document->id;
            } else {
                return null;
            }
        }
    }

    private function accountExists(Client $client, $input)
    {
        $accNum = ($input['formType'] == 'mpesa') ? $input['phone_number'] : $input['investor_account_number'];

        return ClientBankAccount::active()
            ->where('client_id', $client->id)
            ->where('account_number', $accNum)->exists();
    }

    private function cleanPhone($phone)
    {
        $phone = ltrim($phone, '+');
        $phone = ltrim($phone, '254');
        $phone = ltrim($phone, '0');

        $phone_country_code = '254' . $phone;

        return str_replace(' ', '', $phone_country_code);
    }

    public function getAccountDetails($accId)
    {
        $acc = ClientBankAccount::find($accId);

        $bank = ($acc->branch) ? $acc->branch->bank : null;

        $details = [
            'id' => $acc->id,
            'bank' => $bank ? $bank->id : $acc->bank_name,
            'branch' => $acc->branch ? $acc->branch->id : $acc->branch_name,
            'investor_account_name' => $acc->account_name,
            'investor_account_number' => $acc->account_number,
            'currency_id' => $acc->currency ? $acc->currency->id : null,
            'country_id' => $acc->country ? $acc->country->id : null,
            'type' => strtolower($acc->branch->swift_code),
            'document_id' => $acc->document ? $acc->document_id : null
        ];

        return response(['fetched' => true, 'details' => $details], 200);
    }

    public function removeAccountDetails($clientId, $accId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $input['id'] = $accId;

        ClientTransactionApproval::add([
            'transaction_type' => 'remove_client_account_details',
            'client_id' => $client->id,
            'payload' => $input
        ]);

        return response()->json(['status' => 202]);
    }

    public function editClient()
    {
        $input = request()->all();

        $client = Client::findOrFailByUuid($input['client_uuid']);

        $input['client_id'] = $client->id;

        ClientTransactionApproval::add([
            'transaction_type' => 'edit_client',
            'client_id' => $client->id,
            'payload' => $input
        ]);

        return response()->json(['status' => 202]);
    }

    public function getPendingKyc($uuid)
    {
        $pendingKyc = Client::findOrFailByUuid($uuid)->repo->requiredPendingKYCDocuments()
            ->map(function ($kyc) {
                return [
                    'id' => $kyc->id,
                    'name' => $kyc->name,
                    'slug' => $kyc->slug,
                ];
            });

        return response()->json(['status' => 202, 'data' => $pendingKyc]);
    }

    public function uploadClientKyc()
    {
        $input = request()->all();

        $document = (new ClientUploadedKycRepository())
            ->uploadKYC(file_get_contents($input['file']), $input['file']->getClientOriginalExtension());

        $input['document_id'] = $document->id;
        $input['filename'] = $input['file']->getClientOriginalName();
        unset($input['file']);

        return response()->json(['status' => 202, 'data' => $input]);
    }

    public function editClientKyc()
    {
        $input = request()->all();

        $client = Client::findOrFailByUuid($input['client_uuid']);

        if (!count($input['docsUploaded'])) {
            return response()->json(['status' => 404]);
        }

        ClientTransactionApproval::add([
            'transaction_type' => 'edit_client_kyc',
            'client_id' => $client->id,
            'payload' => $input
        ]);

        return response()->json(['status' => 202]);
    }

    public function countries()
    {
        return response()->json(['status' => 202, 'countries' => (new Country())->get(['id', 'name'])->toArray()]);
    }

    public function referClient()
    {
        $input = request()->all();

        $input['phone'] = array_key_exists('phone_number', $input) ? $input['phone_number'] : '';

        $input['email'] = array_key_exists('email', $input) ? $input['email'] : '';

        $input['client_name'] = auth()->user()->firstname . ' ' . auth()->user()->lastname;

        $input['source'] = 'WEB';

        $input['product'] = 'Cytonn Money Market Fund';

        dispatch(new ReferClient($input));
    }
}
