<?php

namespace App\Http\CrimsClient\Controllers\Api\Client;

use App\Cytonn\Api\Transformers\FinancialAdvisorSearchTransformer;
use App\Cytonn\Models\CommissionRecepient;
use App\Http\Controllers\Api\ApiController;
use League\Fractal\Resource\Collection;

class CommissionController extends ApiController
{
    public function allRecipients()
    {
        $recipient = CommissionRecepient::search(request()->get('query'))->paginate(50);

        $response = new Collection($recipient, new FinancialAdvisorSearchTransformer());

        return $this->fractal->createData($response)->toJson();
    }
}
