<?php

namespace App\Http\CrimsClient\Controllers\Api\Billing;

use App\Cytonn\Billing\BillingRepository;
use App\Cytonn\Billing\Rules\BillRules;
use App\Cytonn\Models\Billing\ClientUtilityBills;
use App\Cytonn\Models\Billing\UtilityBillingServices;
use App\Cytonn\Models\Billing\UtilityBillingType;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\System\Locks\Facade\Lock;
use App\Http\CrimsClient\Controllers\Controller;
use App\Jobs\USSD\SendMessages;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class BillingController extends Controller
{
    use BillRules;

    public function allBills()
    {
        $type = UtilityBillingType::where('slug', '=', 'bill')->first();

        $bills = UtilityBillingServices::active()
            ->where('type_id', $type->id)
            ->get()
            ->map(function ($bill) {
                return [
                    'id' => $bill->id,
                    'name' => $bill->name,
                    'slug' => $bill->slug,
                    'type' => $bill->type->slug,
                    'code' => $bill->code,
                    'logo' => $bill->logo,
                    'minimumAmount' => (float)$bill->minimum_pay_amount
                ];
            });

        return response()->json($bills);
    }

    public function clientBills($clientUuid)
    {
        $client = Client::findByUuid($clientUuid);

        $bills = ClientUtilityBills::where('client_id', $client->id)
            ->latest()
            ->get()
            ->map(function ($bill) {
                return [
                    'id' => $bill->id,
                    'bill_name' => $bill->utility->name,
                    'slug' => $bill->utility->slug,
                    'logo' => $bill->utility->logo,
                    'account_name' => $bill->account_name,
                    'account_number' => $bill->account_number,
                    'smart_Card_number' => $bill->smart_Card_number,
                    'meter_number' => $bill->meter_number,
                ];
            });

        return response()->json($bills);
    }

    public function deleteClientBill($billId, $clientUuid)
    {
        $client = Client::findByUuid($clientUuid);

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return response([
                'message' => 'We are processing one of your transaction, kindly wait a few seconds then try again.',
                'locked' => $lock,
                'status' => 422
            ]);
        }

        $bill = ClientUtilityBills::where('id', $billId)
            ->where('client_id', $client->id)
            ->first();

        $bill->delete();

        return response()->json([
            'data' => ['message' => 'Client Bill deleted successfully'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function updateClientBill($billId, $clientUuid)
    {
        $input = request()->all();

        $client = Client::findByUuid($clientUuid);

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return response([
                'message' => 'We are processing one of your transaction, kindly wait a few seconds then try again.',
                'locked' => $lock,
                'status' => 422
            ]);
        }

        $bill = ClientUtilityBills::where('id', $billId)
            ->where('client_id', $client->id)
            ->first();

        $bill->account_name = $input['account_name'];
        $bill->account_number = $input['account_number'];
        $bill->save();

        return response()->json([
            'data' => ['message' => 'Client Bill updated successfully'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function allAirtimeTypes()
    {
        $type = UtilityBillingType::where('slug', '=', 'airtime')->first();

        $bills = UtilityBillingServices::active()
            ->where('type_id', $type->id)
            ->get()
            ->map(function ($bill) {
                return [
                    'id' => $bill->id,
                    'name' => $bill->name,
                    'type' => $bill->type->slug,
                    'code' => $bill->code,
                    'logo' => $bill->logo,
                    'minimumAmount' => (float)$bill->minimum_pay_amount
                ];
            });

        return response()->json($bills);
    }

    public function sourceFunds()
    {
        $funds = UnitFund::whereNotNull('billing_fund')
            ->get()
            ->map(function ($fund) {
                return [
                    'id' => $fund->id,
                    'name' => $fund->name,
                ];
            });

        return response()->json($funds);
    }

    public function saveBillDetails()
    {
        $input = request()->all();
        $client = Client::findOrFailByUuid($input['client_id']);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return response([
                'message' => 'We are processing one of your transaction, kindly wait a few seconds then try again.',
                'locked' => $lock,
                'status' => 422
            ]);
        }

        $user = isset($input['user_id']) ? ClientUser::findOrFailByUuid($input['user_id']) : null;

        $validator = $this->validateBill(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $utility = UtilityBillingServices::find($input['bill_id']);

        if (isset($input['clientBill']) && $input['clientBill']) {
            $clientUtility = ClientUtilityBills::find($input['bill_id']);

            $utility = $clientUtility->utility;
        }

        $input['client_id'] = $client->id;

        $input['user_id'] = $user ? $user->id : null;

        $phone = Arr::first($client->getContactPhoneNumbersArray());

        if (!$phone) {
            return response([
                'errors' => 'The selected client has no phone number record. Please update it first',
                'status' => 422
            ]);
        }

        $input['msisdn'] = cleanPhoneNumber($phone);

        (new BillingRepository())->saveBillingInstruction($input, $client, $utility);

        $this->sendRequestNotification($client, $utility, $input, 'bill');

        return $this->response()->json([
            'data' => ['message' => 'Add Bill instruction submitted successfully'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function buyAirtime()
    {
        $input = request()->all();
        $client = Client::findOrFailByUuid($input['client_id']);

        if ($this->allow($client)) {
            return $this->allow($client);
        }

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return response([
                'message' => 'We are processing one of your transaction, kindly wait a few seconds then try again.',
                'locked' => $lock,
                'status' => 422
            ]);
        }

        $user = isset($input['user_id']) ? ClientUser::findOrFailByUuid($input['user_id']) : null;

        $validator = $this->validateAirtime(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $utility = UtilityBillingServices::find($input['bill_id']);

        $input['client_id'] = $client->id;

        $input['user_id'] = $user ? $user->id : null;
        $phone = Arr::first($client->getContactPhoneNumbersArray());

        if (!$phone) {
            return response([
                'errors' => 'The selected client has no phone number record. Please update it first',
                'status' => 422
            ]);
        }
        $input['msisdn'] = cleanPhoneNumber($phone);

        if (isset($input['phone_number'])) {
            $input['account_number'] = cleanPhoneNumber($input['phone_number']);
            unset($input['phone_number']);
        }

        (new BillingRepository())->saveBuyAirtimeRequest($input, $client, $utility);

        $this->sendRequestNotification($client, $utility, $input, 'airtime');

        return $this->response()->json([
            'data' => ['message' => 'Buy Airtime Request instruction submitted successfully'],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    protected function allow(Client $client)
    {
        if (!$this->user()->isFaOrOwn($client, false)) {
            return response([
                'errors' => 'You have no access to client',
                'status' => 401
            ]);
        }

        return false;
    }

    private function sendRequestNotification(Client $client, UtilityBillingServices $utility, $input, $type)
    {
        $phone = Arr::first($client->getContactPhoneNumbersArray());

        if (!$phone) {
            return;
        }

        $fund = isset($input['unit_fund_id']) ? UnitFund::findOrFail($input['unit_fund_id']) : null;

        $data = [
            'account_number' => $input['account_number'],
            'account_name' => isset($input['account_name']) ? $input['account_name'] : '',
            'amount' => $input['amount'],
            'bill_name' => $utility->name,
            'source_fund' => $fund ? $fund->name : '',
        ];

        if ($type == 'bill') {
            dispatch(new SendMessages($phone, 'bill-pay-request', $data));
        } else if ($type == 'airtime') {
            dispatch(new SendMessages($phone, 'airtime-pay-request', $data));
        }
    }
}
