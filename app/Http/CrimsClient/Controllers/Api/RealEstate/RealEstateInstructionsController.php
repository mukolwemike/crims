<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-06
 * Time: 14:13
 */

namespace App\Http\CrimsClient\Controllers\Api\RealEstate;

use App\Cytonn\Api\Transformers\RealEstate\RealEstateInstructionTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Cytonn\Models\RealEstateInstructionType;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Response;

class RealEstateInstructionsController extends Controller
{
    public function getPaymentsInstructions($uuid, $status = null)
    {
        $type_id = RealEstateInstructionType::where('slug', '=', 'holding_payment')->first()->id;
        $client = Client::findOrFailByUuid($uuid);

        $instructions = RealEstateInstruction::uploaded($status)->where('client_id', $client->id)
            ->where('instruction_type_id', $type_id)
            ->get()
            ->map(function ($instruction) {
                return (new RealEstateInstructionTransformer())->transform($instruction);
            });

        return response()->json([
            'payments' => $instructions,
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function getPaymentInstruction($paymentId)
    {
        $instruction = RealEstateInstruction::findOrFail($paymentId);

        $instruction = (new RealEstateInstructionTransformer())->transform($instruction);

        return response()->json([
            'payment' => $instruction,
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }
}
