<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/4/19
 * Time: 1:53 PM
 */

namespace App\Http\CrimsClient\Controllers\Api\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Http\CrimsClient\Controllers\Controller;
use App\ServiceBus\Queries\RealEstate\GetProjectsSummary;

class RealEstateController extends Controller
{
    public function index()
    {
        $projects = Project::all()
            ->map(function ($project) {
                return [
                    'id' => $project->id,
                    'name' => $project->name,
                    'url' => $project->url,
                    'booking_url' => $project->booking_url,
                    'reservation_url' => $project->reservation_url,
                    'description' => $project->description,
                    'logo' => $project->logo
                ];
            });

        return response()->json($projects);
    }

    public function paymentPlans()
    {
        return response()->json([
            'data' => RealEstatePaymentPlan::all()
        ]);
    }
}
