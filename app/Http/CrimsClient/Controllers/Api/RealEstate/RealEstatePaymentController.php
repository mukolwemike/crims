<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 2/6/19
 * Time: 9:48 AM
 */

namespace App\Http\CrimsClient\Controllers\Api\RealEstate;

use App\Cytonn\Api\Transformers\Clients\RealEstatePaymentTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Realestate\Payments\ClientPaymentRepository;
use App\Cytonn\Realestate\RealEstateClients\RealEstatePaymentRepository;
use App\Events\Realestate\ClientHoldingPaymentFilled;
use App\Http\CrimsClient\Controllers\Controller;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Http\Response;

class RealEstatePaymentController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new ClientPaymentRepository();
    }

    public function makePayment($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $instruction = $this->repository->makePayment($client->id, request(), $this->user());

//        event(new ClientHoldingPaymentFilled($instruction));

        return response()->json([
            'message' => 'Payment details sent for approval successfully',
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED, [
            'location' => $instruction->id
        ]);
    }

    public function uploadPaymentProof($paymentId)
    {
        $instruction = $this->repository->uploadPaymentProof($paymentId, request());

        return response()->json([
            'message' => 'Payment proof uploaded successfully',
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED, [
            'location' => $instruction->id
        ]);
    }

    public function showDocument($id)
    {
        $document = Document::findorFail($id);

        return $this->streamFile($document->path());
    }

    public function upcomingPayments($client_id)
    {
        $client = Client::findOrFailByUuid($client_id);

        $today = Carbon::today();
        
        $payments = (new RealEstatePaymentRepository())->upcomingPayments($client, $today);

        return response()->json($payments);
    }

    public function overduePayments($client_id)
    {
        $client = Client::findOrFailByUuid($client_id);

        $today = Carbon::today();
        
        $payments = (new RealEstatePaymentRepository())->overduePayments($client, $today);

        return response()->json($payments);
    }
}
