<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 14/01/2019
 * Time: 15:25
 */

namespace App\Http\CrimsClient\Controllers\Api\RealEstate;

use App\Cytonn\Api\Transformers\RealEstate\REPaymentSchedulesTransformer;
use App\Cytonn\Api\Transformers\RealEstate\REUnitHoldingTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Realestate\RealEstateClients\RealEstateClientsRepository;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Realestate\Statements\StatementGenerator;
use Illuminate\Http\Response;

class RealEstateClientsController extends Controller
{
    public function realEstateSummaries($uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        $investments = $client->repo->clientReInvestments();

        $arr = $investments['realEstateDetails']->toArray();
        $details = array();

        foreach ($arr as $key => $item) {
            $next_payment_date = array_column($item, 'next_payment_date');

            $sorted = array_multisort($next_payment_date, SORT_DESC, $item);

            array_push($details, [
                'project' => $key,
                'units' => $item,
                'totalPaid' => array_sum(array_column($item, 'paid')),
                'totalValue' => array_sum(array_column($item, 'unit_price')),
                'overdueAmount' => array_sum(array_column($item, 'overdue_amount')),
                'nextPayment' => ($sorted['next_payment_date']) ? $sorted['next_payment_date'] : false,
            ]);
        }

        return response()->json([
            'summaries' => $investments,
            'details' => $details,
            'totals' => $investments['realEstateTotals'],
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }


    public function unitDetails($id)
    {
        $details = (new REUnitHoldingTransformer())->transform($id);

        return response()->json([
            'data' => $details,
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function unitPaymentSchedules($id)
    {
        $holding = UnitHolding::where('unit_id', $id)->first();

        $schedules = $holding->paymentSchedules()
            ->where(function ($schedule) {
                return $schedule->ofNotType('reservation_fee');
            })
            ->get()
            ->map(function ($schedule) {
                return (new REPaymentSchedulesTransformer())->transform($schedule);
            });

        return response()->json([
            'data' => $schedules,
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function getREStatements($projectId, $clientId)
    {
        $request = request()->all();

        $date_to = ($request['date_from'] === 'null') ? Carbon::today() : Carbon::parse($request['date_to']);

        $date_from = ($request['date_from'] === 'null') ? null : Carbon::parse($request['date_from']);

        $client = Client::findOrFailByUuid($clientId);

        $project = Project::findOrFail($projectId);

        return (new RealEstateClientsRepository(
            $client,
            $project,
            $date_to,
            $date_from
        ))->loadStatement();
    }

    public function reProjects($uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        return response()->json([
            'projects' => $client->repo->clientReProjects($client),
            'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function downloadReStatement($projectId, $clientId, $date_from, $date_to)
    {
        $client = Client::findOrFailByUuid($clientId);

        $project_id = (int)$projectId;

        $project = Project::find($project_id);

        $date_to = ($date_to === 'null') ? Carbon::today() : Carbon::parse($date_to);

        $date_from = ($date_from === 'null') ? null : $date_from;

        return (new StatementGenerator())
            ->clientGenerate(
                $client,
                $project,
                $date_to,
                $date_from
            )->stream();
    }

    public function downloadExcelReStatement($projectId, $clientId, $date_from, $date_to)
    {
        $client = Client::findOrFailByUuid($clientId);

        $project_id = (int)$projectId;

        $project = Project::find($project_id);

        $date_to = ($date_to === 'null') ? Carbon::today() : Carbon::parse($date_to);

        $date_from = ($date_from === 'null') ? null : $date_from;

        return (new StatementGenerator())
            ->clientExcelGenerate(
                $client,
                $project,
                $date_to,
                $date_from
            )->stream();
    }

    public function allow(Client $client)
    {
        if (!$this->user()->isFaOrOwn($client, false)) {
            return response()->json([
                'message' => 'No access to this client',
                'status' => Response::HTTP_UNAUTHORIZED
            ]);
        }

        return false;
    }
}
