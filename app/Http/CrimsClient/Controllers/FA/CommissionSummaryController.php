<?php

namespace App\Http\CrimsClient\Controllers\FA;

use App\Cytonn\Investment\Transformers\RecipientClawbacksTransformer;
use App\Cytonn\Models\BankBranch;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\HrBank;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Http\CrimsClient\Controllers\Controller;
use App\Jobs\FAs\FaCommissionExport;
use Carbon\Carbon;
use Crims\Investments\Commission\RecipientCalculator as CMSRecipientCalculator;
use Crims\Realestate\Commission\RecipientCalculator;
use Crims\Unitization\Commission\RecipientCalculator as UTFRecipientCalculator;
use Crims\Shares\Commission\RecipientCalculator as OTCRecipientCalculator;
use Crims\AdditionalCommission\Commission\RecipientCalculator as AdditionalRecipientCalculator;
use Illuminate\Http\Request;

class CommissionSummaryController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');
    }

    public function index($id, $input = null)
    {
        ini_set('memory_limit', '512M');

        ini_set('max_execution_time', 300);

        $request = request()->all();

        if ($input) {
            $request = $input;
        }

        $currency = isset($request['currency'])? $request['currency'] : 'KES';

        $date = (isset($request['date'])) ? Carbon::parse($request['date']): Carbon::now();

        $dates = $this->commissionDates($date);

        $currency = Currency::where('code', $currency)->first();

        $fa = CommissionRecepient::findOrFailByUuid($id);

        $cms = function () use ($fa, $currency, $dates) {
            return (new CMSRecipientCalculator($fa, $currency, $dates->start, $dates->end))->summary();
        };

        $re = function () use ($fa, $dates) {
            return (new RecipientCalculator($fa, $dates->start, $dates->end))->summary();
        };

        $utf = function () use ($fa, $currency, $dates) {
            return (new UTFRecipientCalculator($fa, $dates->start, $dates->end, null, $currency))->summary();
        };

        $otc = function () use ($fa, $currency, $dates) {
            return (new OTCRecipientCalculator($fa, $dates->start, $dates->end, null, $currency))->summary();
        };

        $additional = function () use ($fa, $currency, $dates) {
            return (new AdditionalRecipientCalculator($fa, $dates->start, $dates->end, null))->summary();
        };

        $last_month = $date->copy()->subMonthNoOverflow();

        $cms_now = $cms();

        $re_now = $re();

        $utf_now = $utf();

        $otc_now = $otc();

        $previous = $cms($last_month)->total() + $re($last_month)->total();

        $cmsOverride = $cms_now->override();

        if ($currency->code == 'KES') {
            $reOverride = $re_now->override();

            $utfOverride = $utf_now->override();

            $otcOverride = $otc_now->override();

            $additionalNow = $additional();

            $advanceCommission = $additionalNow->additionalCommission();

            $unpaidAdvanceCommission = $additionalNow->netUnpaidCommission();
        } else {
            $reOverride = $utfOverride = $otcOverride = $advanceCommission = $unpaidAdvanceCommission = 0;
        }

        $products = [
            [
                'name' => 'Investments',
                'earned' => $cms_now->earned(),
                'total' => $cms_now->total(),
                'claw_backs' => $cms_now->getClawBacks(),
                'total_commission' => $cms_now->total() + $cmsOverride,
                'overrides' => $cmsOverride,
            ],
            [
                'name' => 'Real Estate',
                'earned' => $re_now->total(),
                'total' => $re_now->total(),
                'total_commission' => $re_now->total() + $reOverride,
                'claw_backs' => 0,
                'overrides' => $reOverride,
            ],
            [
                'name' => 'Unit Trust',
                'earned' => $utf_now->total(),
                'total' => $utf_now->total(),
                'total_commission' => $utf_now->total() + $utfOverride,
                'claw_backs' => 0,
                'overrides' => $utfOverride,
            ],
            [
                'name' => 'OTC Shares',
                'earned' => $otc_now->total(),
                'total' => $otc_now->total(),
                'total_commission' => $otc_now->total() + $otcOverride,
                'claw_backs' => 0,
                'overrides' => $otcOverride,
            ]
        ];

        $products = collect($products);

        $bank = $fa->bank_code ? HrBank::where('code', $fa->bank_code)->first() : null;

        if ($bank && $fa->branch_code) {
            $branch = BankBranch::where('code', $fa->branch_code)->where('bank_id', $bank->id)->first();
        } else {
            $branch = null;
        }

        $summary = [
            'fa' => [
                'id' => $fa->uuid,
                'name'=> $fa->name,
                'bank' => $bank ? $bank->name : null,
                'branch' => $branch ? $bank->name : null,
                'account_number' => $fa->account_number,
                'kra_pin' => $fa->kra_pin
            ],
            'start' => $dates->start->toDateString(),
            'end'  => $dates->end->toDateString(),
            'previous'=> $previous,
            'total'=>$products->sum('total'),
            'overrides' => $products->sum('overrides'),
            'total_commission' => $products->sum('total_commission'),
            'claw_backs' => $products->sum('claw_backs'),
            'unpaid_advance_commission' => $unpaidAdvanceCommission,
            'advance_commission' => $advanceCommission,
            'products'=> $products->all()
        ];

        if ($input) {
            return $summary;
        }

        return response()->json($summary);
    }

    public function faClawbacks(Request $request, $fa_id)
    {
        $fa = CommissionRecepient::findOrFailByUuid($fa_id);

        $dates = $this->commissionDates(Carbon::parse($request->get('date')));

        $currency = (new Currency())->where('code', 'KES')->first();

        $calc = new CMSRecipientCalculator($fa, $currency, $dates->start, $dates->end);

        return $this->response->collection($calc->clawBackQuery()->get(), (new RecipientClawbacksTransformer()));
    }

    /**
     * @param $fa_id
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function commissionSummary($fa_id, $input = null)
    {
        $fa_uuid = CommissionRecepient::findorFail($fa_id)->uuid;

        $commission_summary = $this->index($fa_uuid, $input);

        if ($input) {
            return $commission_summary;
        }

        return $this->response()->json(['data' => $commission_summary]);
    }

    public function export($id)
    {
        $request = request()->all();

        $currency = isset($request['currency'])? $request['currency'] : 'KES';

        $date = (isset($request['date'])) ? Carbon::parse($request['date']): Carbon::now();

        $dates = $this->commissionDates($date);

        $currency = Currency::where('code', $currency)->first();

        $fa = CommissionRecepient::findOrFailByUuid($id);

        dispatch(new FaCommissionExport($fa, $currency, $date->copy()));

        return response()->json(['sent' => true ], 200);
    }
}
