<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 8/8/17
 * Time: 7:28 AM
 */

namespace App\Http\CrimsClient\Controllers\FA;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Investment\Commission\ProjectionsCalculator;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Commissions\Calculator\ReProjectionsCalculator;

class ProjectionsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('jwt.auth');
    }

    public function getProjections()
    {
        $data = request()->all();
        $fa = CommissionRecepient::findOrFailByUuid($data['faId']);
        $retained = $data['retained'];

        $start = Carbon::parse($data['start'])->startOfMonth();
        $end = Carbon::parse($data['end'])->endOfMonth();

        $months = $this->getMonths($start, $end);
        $clients = $this->getClients($start, $end, $fa)->get();

        return $clients->map(function (Client $client) use ($months, $fa, $retained) {
            $out = [
                'Client Code' => $client->client_code,
                'Name' => ClientPresenter::presentJointFullNames($client->id),
            ];

            foreach ($months as $month) {
                $month = Carbon::parse($month);
                //                $out[$month->format('M, Y')] = $this->project($fa, $client, $month, $retained);
                $out[$month->format('M, Y')] =
                    AmountPresenter::currency($this->project($fa, $client, $month, $retained));
            }

            return $out;
        });
    }

    public function projectionTotals(CommissionRecepient $fa, Carbon $date = null)
    {
        $start = Carbon::parse($date)->startOfMonth();
        $end = Carbon::parse($date)->endOfMonth();

        $months = $this->getMonths($start, $end);
        $clients = $this->getClients($start, $end, $fa)->get();

        $retained = ($fa->type->name == 'IFA') ? true : false;

        return $clients->map(function (Client $client) use ($months, $fa, $retained) {
            foreach ($months as $month) {
                $month = Carbon::parse($month);
                $out['projection'] = $this->project($fa, $client, $month, $retained);
            }

            return $out;
        })->sum('projection');
    }

    public function project($fa, $client, $month, $retained)
    {
        $currency = (new Currency())->where('code', 'KES')->first();
        $dates = $this->commissionDates($month);

        $inv = (new ProjectionsCalculator($fa, $currency, $dates->start, $dates->end))
            ->setRetained($retained)
            ->calculate($client)
            ->sum('amount');

        $re = (new ReProjectionsCalculator($fa, $dates->start, $dates->end))
            ->setRetained($retained)
            ->calculate($client)
            ->sum('amount');

        return $re + $inv;
    }

    private function getClients($start, $end, CommissionRecepient $recipient)
    {
        $client = new Client();

        $query = $client->whereHas(
            'investments',
            function ($investments) use ($start, $end, $recipient) {
                $investments->whereHas(
                    'commission',
                    function ($commission) use ($start, $end, $recipient) {
                        $commission->where('recipient_id', $recipient->id);
                    }
                );
            }
        )->orWhereHas(
            'unitHoldings',
            function ($holding) use ($start, $end, $recipient) {
                $holding->whereHas(
                    'paymentSchedules',
                    function ($paymentSchedule) use ($start, $end, $recipient) {
                    }
                )->whereHas(
                    'commission',
                    function ($commission) use ($recipient) {
                        $commission->where('recipient_id', $recipient->id);
                    }
                );
            }
        );

        return clone $query;
    }

    protected function commissionDates(Carbon $date)
    {
        $bulk = $this->getBulk($date);

        if ($bulk) {
            return (object)[
                'start' => $bulk->start,
                'end' => $bulk->end
            ];
        }

        return (object)[
            'start' => $this->getStart($date),
            'end' => $this->getEnd($date)
        ];
    }

    private function getStart(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->subMonthNoOverflow())) {
            return $bulk->end->copy()->addDay();
        }

        return $date->copy()->subMonthNoOverflow()->startOfMonth()->addDays(20);
    }

    private function getEnd(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->addMonthNoOverflow())) {
            return $bulk->start->copy()->subDay();
        }

        return $date->copy()->startOfMonth()->addDays(19);
    }

    private function getBulk($date)
    {
        return (new BulkCommissionPayment())->where('start', '<=', $date)
            ->where('end', '>=', $date)->first();
    }

    private function getMonths(Carbon $startDate, Carbon $endDate)
    {
        $months = [];

        for ($start = $startDate->copy(); $start->lte($endDate->copy()); $start = $start->addMonthNoOverflow()) {
            $months[] = $start->copy()->startOfMonth();
        }

        return $months;
    }
}
