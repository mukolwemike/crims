<?php

namespace App\Http\CrimsClient\Controllers\FA;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Http\CrimsClient\Controllers\Controller;
use App\Http\CrimsClient\Transformers\ClientTransformer;
use Carbon\Carbon;
use Crims\Investments\Commission\Models\Recipient;
use Illuminate\Http\Request;
use \Crims\Commission\RecipientQueryObject;

class CommissionController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');
    }

    public function index(Request $request)
    {
        $date = (isset($request['date'])) ? Carbon::parse($request->get('date')): Carbon::now();

        $dates = $this->commissionDates($date);

        $currency = (isset($request['currency'])) ? $request['currency']: 'KES';

        $currency = Currency::where('code', $currency)->first();

        $fa = CommissionRecepient::findOrFailByUuid($request->get('id'));

        $clients = (new RecipientQueryObject($fa, $currency))
            ->clients();

        if ($request->get('filter')) {
            $clients = $clients->search($request->get('filter'));
        }

        $clients = $clients->get();

        return $this->response->collection($clients, new ClientTransformer($fa, $dates->start, $dates->end, $currency));
    }

    public function show(Request $request, $client_id)
    {
        $client = Client::findOrFailByUuid($client_id);

        $fa = CommissionRecepient::findOrFailByUuid($request->get('fa_id'));

        $dates = $this->commissionDates(Carbon::parse($request->get('date')));

        $transformer = new ClientTransformer($fa, $dates->start->copy(), $dates->end->copy());
        
        $transformer->schedules = true;

        return $this->response->item($client, $transformer);
    }
}
