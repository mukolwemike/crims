<?php

namespace App\Http\CrimsClient\Controllers\FA;

use App\Cytonn\Clients\Transformers\ClientTransformer;
use App\Cytonn\Investment\Transformers\FaCommissionsSchedulesTransformer;
use App\Cytonn\Investment\Transformers\FaOverridesTransformer;
use App\Cytonn\Investment\Transformers\RecipientTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Crims\Commission\Override;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Illuminate\Http\Request;

class FinancialAdvisorController extends Controller
{
    use AlternateSortFilterPaginateTrait;

    public function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');
    }

    public function myClients(Request $request)
    {
        $recipient = CommissionRecepient::findOrFailByUuid($request->get('id'));

        $clients = (new Client())->where(
            function ($q) use ($recipient) {
                $q->whereHas(
                    'investments',
                    function ($investment) use ($recipient) {
                        $investment->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                )->orWhereHas(
                    'unitHoldings',
                    function ($holding) use ($recipient) {
                        $holding->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                );
            }
        )
            ->search($request->get('filter'))
            ->get();

        return $this->response()->collection($clients, new ClientTransformer());
    }

    public function getClients($id)
    {
        $recipient = CommissionRecepient::findOrFailByUuid($id);

        $clients = (new Client())->where(
            function ($q) use ($recipient) {
                $q->whereHas(
                    'investments',
                    function ($investment) use ($recipient) {
                        $investment->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                )->orWhereHas(
                    'unitHoldings',
                    function ($holding) use ($recipient) {
                        $holding->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                );
            }
        )->get();

        return $this->response()->collection($clients, new ClientTransformer());
    }

    public function myFas($id, $date)
    {
        $fa = CommissionRecepient::findOrFailByUuid($id);

        $date = ($date) ? Carbon::parse($date) : Carbon::today();

        $dates = $this->commissionDates($date);

        $fas = (new Override($fa, Carbon::today(), 'investments', $dates->start))->reports($fa);

        return $this->processTable(
            $fas,
            new RecipientTransformer(),
            null,
            function ($fas) {
                return $fas;
            }
        );
    }

    public function getOverrides(Request $request, $fa_id)
    {
        $fa = CommissionRecepient::findOrFailByUuid($fa_id);

        $dates = $this->commissionDates(Carbon::parse($request->get('date')));

        $currency = (new Currency())->where('code', 'KES')->first();

        $override = (new Override($fa, $dates->end, 'investments', $dates->start));

        return $this->sortFilterPaginate(

            new CommissionRecepient(),
            [],
            function ($recipient) use ($fa, $currency, $dates) {

                return (new FaOverridesTransformer($fa, $currency, $dates->start, $dates->end))->transform($recipient);
            },
            function ($model) use ($dates, $fa) {
                return $model->whereHas('reportInvestmentOverrides', function ($q) use ($dates, $fa) {
                    $q->between($dates->start, $dates->end)->forRecipient($fa);
                })->orWhereHas('reportRealEstateOverrides', function ($q) use ($dates, $fa) {
                    $q->between($dates->start, $dates->end)->forRecipient($fa);
                })->orWhereHas('reportShareOverrides', function ($q) use ($dates, $fa) {
                    $q->between($dates->start, $dates->end)->forRecipient($fa);
                })->orWhereHas('reportUnitFundOverrides', function ($q) use ($dates, $fa) {
                    $q->between($dates->start, $dates->end)->forRecipient($fa);
                });
            }
        );

//        $reports = CommissionRecepient::where(function ($q) use ($dates, $fa) {
//            $q->whereHas('reportInvestmentOverrides', function ($q) use ($dates, $fa) {
//                $q->between($dates->start, $dates->end)->forRecipient($fa);
//            })->orWhereHas('reportRealEstateOverrides', function ($q) use ($dates, $fa) {
//                $q->between($dates->start, $dates->end)->forRecipient($fa);
//            })->orWhereHas('reportShareOverrides', function ($q) use ($dates, $fa) {
//                $q->between($dates->start, $dates->end)->forRecipient($fa);
//            })->orWhereHas('reportUnitFundOverrides', function ($q) use ($dates, $fa) {
//                $q->between($dates->start, $dates->end)->forRecipient($fa);
//            });
//        })->get();
//
//        $rate = function ($type) use ($dates, $override, $fa) {
//            return $override->getOverrideRate($dates->end, $type);
//        };
//
//        $meta = [
//            'name' => $fa->name,
//            'rank' => $fa->rank,
//            'rates' => [
//                'Investments' => $rate('investments'),
//                'Real Estate' => $rate('real_estate')
//            ]
//        ];
//
//        return $this->response
//            ->collection($reports, new FaOverridesTransformer($fa, $currency, $dates->start, $dates->end), $meta);
    }

    public function faProductCommissions(Request $request, $fa_id)
    {
        $fa = CommissionRecepient::findOrFailByUuid($fa_id);

        $dates = $this->commissionDates(Carbon::parse($request->get('date')));

        $currency = $request->get('currency');

        $currency = Currency::where('code', $currency)->first();

        return $this
            ->response->item($fa, new FaCommissionsSchedulesTransformer($fa, $dates->start, $dates->end, $currency));
    }

    public function myClientsPortfolio(Request $request)
    {
        $recipient = CommissionRecepient::findOrFailByUuid($request->get('id'));

        $clients = (new Client())->where(
            function ($q) use ($recipient) {
                $q->whereHas(
                    'investments',
                    function ($investment) use ($recipient) {
                        $investment->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                )->orWhereHas(
                    'unitHoldings',
                    function ($holding) use ($recipient) {
                        $holding->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                );
            }
        )
            ->search($request->get('filter'))
            ->get();

        return $this->response()->collection($clients, new ClientTransformer());
    }

    public function faClientPortfolio($id)
    {
        $recipient = CommissionRecepient::findOrFailByUuid($id);

        $clients = Client::where(
            function ($q) use ($recipient) {
                $q->whereHas(
                    'investments',
                    function ($investment) use ($recipient) {
                        $investment->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                )
                ->orWhereHas(
                    'unitHoldings',
                    function ($holding) use ($recipient) {
                        $holding->whereHas(
                            'commission',
                            function ($commission) use ($recipient) {
                                $commission->where('recipient_id', $recipient->id);
                            }
                        );
                    }
                )
                ->orWhereHas(
                    'unitFundPurchases',
                    function ($holding) use ($recipient) {
                        $holding->whereHas(
                            'unitFundCommission',
                            function ($commission) use ($recipient) {
                                $commission->where('commission_recipient_id', $recipient->id);
                            }
                        );
                    }
                );
            //                )->orWhereHas(
            //                    'unitFundPurchases',
            //                    function ($holding) use ($recipient) {
            //                        $holding->whereHas(
            //                            'unitFundCommission',
            //                            function ($commission) use ($recipient) {
            //                                $commission->where('commission_recipient_id', $recipient->id);
            //                            }
            //                        );
            //                    }
            //                );
            }
        )
            ->get();

        return $this->response()->collection($clients, new ClientTransformer());
    }
}
