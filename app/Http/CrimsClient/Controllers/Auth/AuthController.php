<?php

namespace App\Http\CrimsClient\Controllers\Auth;

use App\Cytonn\Auth\PasswordResetRequest;
use App\Cytonn\Authentication\Client\AuthRepository;
use App\Cytonn\Authentication\TokenVerification;
use App\Cytonn\Mailers\PasswordResetRequestMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\ClientUser as User;
use App\Events\AuthyConfirmPasswordReset;
use App\Events\AuthyStatusUpdated;
use App\Exceptions\CrimsException;
use App\Http\CrimsClient\Controllers\Controller;
use App\Http\CrimsClient\Requests\ResetPasswordRequest;
use App\Http\CrimsClient\Transformers\Auth\CurrentUserTransformer;
use Cytonn\Authentication\Authy;
use Cytonn\Models\CrimsClientLoginTrail;
use Cytonn\Users\ClientUserDeviceTokenRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as SympfonyResponse;
use Webpatser\Uuid\Uuid;

class AuthController extends Controller
{
    protected $authy;

    public function __construct(Authy $authy)
    {
        parent::__construct();

        $this->middleware('auth', ['only' => ['getAuthenticatedUser']]);

        $this->authy = $authy;
    }

    public function activate(Request $request, $username, $token)
    {
        $activation = (new AuthRepository)->checkUserAccountActivation($username, $token);


        if ($activation['success']) {
            if ($activation['reason'] == 'activated') {
                $request->session()->flash(
                    'message',
                    ['type' => 'success', 'message' => 'Your account is already active']
                );
            } else {
                return view('auth.activate');
            }
        } else {
            $request->session()->flash(
                'message',
                ['type' => 'error', 'message' => 'The link is incorrect, please request a new one']
            );
        }
        return redirect('/');
    }

    public function saveActivate(ResetPasswordRequest $request, $username, $token)
    {
        $password = $request->get('password');

        $activation = (new AuthRepository())->checkUserAccountActivation($username, $token);

        if ($activation['success'] && $activation['reason'] == 'valid') {
            \DB::transaction(
                function () use ($password, $username) {
                    (new AuthRepository())->activateUserFirstTime($username, $password);
                }
            );

            $request->session()->flash(
                'message',
                ['type' => 'success', 'message' => 'The account has been activated, you can now log in']
            );
        } else {
            $request->session()->flash(
                'message',
                ['type' => 'error', 'message' => 'The account could not be activated']
            );
        }

        return redirect('/');
    }

    /**
     * @param $username
     * @param bool $send
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    private function forgotPassword($username, $send = false)
    {
        $user = User::where('username', $username)->first();

        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $username)
                ->first();
        }

        if (is_null($user)) {
            return $this->response()->json(['success' => false, 'message' => 'User not found', 'status' => 404]);
        }

        \DB::transaction(function () use ($user, $send) {
            $reset = new PasswordResetRequest();
            $reset->user_id = $user->id;
            $reset->sent = $send;
            $reset->save();

            if ($send) {
                (new PasswordResetRequestMailer())->sendPasswordResetLink($user);
                return;
            }

            (new PasswordResetRequestMailer())->sendResetRequest($user);
        });
    }


    /**
     * @param Request $request
     * @return ClientUser|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null
     * @throws \Throwable
     */
    public function verifyUsername(Request $request)
    {
        $user = User::where('username', $request->only('username'))
            ->first();

        if (filter_var($request->get('username'), FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $request->only('username'))
                ->first();
        }

        if (is_null($user)) {
            return $this->response()->json([
                'success' => false,
                'message' => 'User not found',
                'status' => 404
            ]);
        }

        if ($user->hasNoClients()) {
            $this->forgotPassword($user->username, true);

            return response()->json([
                'success' => true,
                'status' => 200,
                'has_no_clients' => true,
                'message' => 'Password reset link is sent to your email. Use the link to reset your password.'
            ]);
        }

        return response()->json([
            'success' => true,
            'status' => 200,
            'user' => $user
        ]);
    }

    /**
     * @param Request $request
     * @return ClientUser|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function verifyCredentials(Request $request)
    {
        $credentials = $request->all();

        if (filter_var($credentials['username'], FILTER_VALIDATE_EMAIL)) {
            $credentials['email'] = $credentials['username'];
        }

        $user = $this->validateCredentials($credentials);

        if (!$user instanceof User) {
            return $user;
        }

        $this->loginUser($user, $credentials);

        return response()->json([
            'needs_two_factor' => false,
            'user' => $user
        ]);
    }


    public function authyCallback(Request $request)
    {
        $authy_id = $request->input('authy_id');

        $user = User::where('authy_id', $authy_id)->first();

        if ($user) {
            $user->authy_status = $request->input('status');
            $user->authy_uuid = $request->input('uuid');

            $user->save();

            if ($request->input('status') == 'approved') {
                event(new AuthyStatusUpdated($user));
                event(new AuthyConfirmPasswordReset($user));
            }

            return "ok";
        } else {
            return "invalid";
        }
    }

    /**
     * @param Request $request
     * @return ClientUser|array|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function verifyCredentialsFromMobile(Request $request)
    {
        $credentials = $request->all();

        if (filter_var($credentials['username'], FILTER_VALIDATE_EMAIL)) {
            $credentials['email'] = $credentials['username'];
        }

        $user = $this->validateCredentials($credentials);

        if ($user instanceof User) {
            $access = $user->access()->where('active', 1)->get();

            if (!count($access)) {
                return response()->json(['status' => SympfonyResponse::HTTP_FAILED_DEPENDENCY]);
            }

            if (!is_null($user->clients)) {
                $clients = $user->clients->map(function ($client) {
                    return [
                        'id' => $client->id,
                        'fullName' => $client->name() . ' - ' . $client->client_code,
                        'email' => $client->contact->email,
                        'firstname' => $client->contact->firstname,
                        'lastname' => $client->contact->lastname,
                        'phone' => $client->contact->phone,
                        'country_id' => $client->country_id,
                        'telephone_home' => $client->telephone_home,
                        'telephone_office' => $client->telephone_office,
                        'postal_address' => $client->postal_address,
                        'uuid' => $client->uuid,
                        'dob' => $client->dob,
                        'residence' => $client->residence,
                        'pin_no' => $client->pin_no,
                        'id_or_passport' => $client->id_or_passport,
                        'postal_code' => $client->postal_code,
                        'street' => $client->street,
                        'town' => $client->town,

                        'investor_bank' => $client->investor_bank,
                        'investor_bank_branch' => $client->investor_bank_branch,
                        'investor_account_name' => $client->investor_account_name,
                        'investor_account_number' => $client->investor_account_number,

                        'present_occupation' => $client->present_occupation,
                        'employer_name' => $client->employer_name,
                        'employer_address' => $client->employer_address,
                        'business_sector' => $client->business_sector,
                        'employment' => $client->employment ? $client->employment->name : null,
                        'employment_id' => $client->employment ? $client->employment->id : null,
                        'contactPersons' => $client->contactPersons,

                        'bank_details' => $this->accountDetails($client),

                        'hasSP' => $client->investments()->active()->count() ? true : false,
                        'hasUT' => $client->unitFundPurchases()->whereHas('unitFund', function ($fund) {
                            $fund->ofType('cis')->active();
                        })->exists(),
                        'hasRE' => count($client->repo->clientReInvestments()['realEstateDetails']) ? true : false,

                        'kycValidated' => $client->repo->checkClientKycValidated()
                    ];
                });

                $defaultAccountId = $user->clients->first()->uuid;

                if ((!$request->easyAccess) && $request->header('X-API-VERSION') > 0) {
                    $this->authy->sendOneTouch($user->authy_id);
                }

                $user = [
                    'id' => $user->id,
                    'uuid' => $user->uuid,
                    'clients' => $clients,
                    'name' => $user->present()->fullName,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'defaultAccountId' => $defaultAccountId,
                    'defautAccountHasSP' => count($user->clients->first()->investments) ? true : false,
                    'defautAccountHasUT' => $user->clients->first()->unitFundPurchases->count() ? true : false,
                    'defautAccountHasRE' =>
                        count($user->clients->first()->repo->clientReInvestments()['realEstateDetails']) ? true : false,
                    'cleanUsername' => $user->present()->cleanUsername,
                    'token' => $this->createAccessToken($user)
                ];

                return response()->json(['user' => $user, 'status' => SympfonyResponse::HTTP_CREATED]);
            }

            return response()->json(['status' => SympfonyResponse::HTTP_FAILED_DEPENDENCY]);
        }

        return $user;
    }

    public function accountDetails(Client $client)
    {
        return ClientBankAccount::where('client_id', $client->id)
            ->get()
            ->map(function ($acc) {

                $bank = ($acc->branch) ? $acc->branch->bank : null;

                return [
                    'bank' => $bank ? $bank->name : $acc->bank_name,
                    'branch' => $acc->branch ? $acc->branch->name : $acc->branch_name,
                    'account_name' => $acc->account_name,
                    'account_number' => $acc->account_number
                ];
            });
    }

    public function verifyTokenFromMobile(Request $request)
    {
        $user = User::find($request->get('user'));

        if (in_array(env('APP_ENV'), ['testing', 'local', 'staging'])) {
            $token = $this->createAccessToken($user);

            return response()->json(['token' => $token, 'status' => SympfonyResponse::HTTP_CREATED]);
        }

        $response = $this->authy->verifyToken($user, $request->get('token'));

        if ($response->success) {
            $token = $this->createAccessToken($user);

            return response()->json(['token' => $token, 'status' => SympfonyResponse::HTTP_CREATED]);
        }

        return response()->json(['status' => SympfonyResponse::HTTP_UNAUTHORIZED]);
    }

    public function createAccessToken(User $user)
    {
        return $user->createToken('CRIMS Mobile app')->accessToken;
    }

    public function sendToken(Request $request)
    {
        $user = User::find($request->get('user'));

        if ($user instanceof User) {
            $this->authy->requestSMS($user);

            return response()->json(['status' => 200]);
        }

        return response()->json(['status' => 401]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->except('_token', '_method');

        if (filter_var($credentials['username'], FILTER_VALIDATE_EMAIL)) {
            $credentials['email'] = $credentials['username'];
        }

        $user = $this->validateCredentials($credentials);

        if (!$user instanceof User) {
            return $user;
        }

        if ($request->get('option') === 'email') {
            return $this->checkOneTimeKey($user, $credentials);
        }

        if ($user->authRepo()->needsTwoFactor($request->input('key')) && !$this->canSkipTwoFactor()) {
            return $this->checkSecondFactor($user, $credentials);
        }

        return $this->loginUser($user, $credentials);
    }

    /**
     * @param Request $request
     * @return ClientUser|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null
     * @throws \Throwable
     */
    public function authenticatePasswordReset(Request $request)
    {
        $user = User::where('username', $request->only('username'))->first();

        if (filter_var($request->get('username'), FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $request->get('username'))->first();
        }

        if (is_null($user)) {
            return $this->response()->json(['success' => false, 'message' => 'User not found', 'status' => 404]);
        }

        $option = $request->has('option') ? $request->get('option') : 'sms';

        $verified = (new TokenVerification($user))->verify($option);

        if ($verified) {
            $this->forgotPassword($user->username, (bool)$verified);

            return response()->json([
                'success' => true,
                'message' => 'Password reset link sent to your email',
                'status' => 200
            ]);
        }

        return response()->json([
            'message' => 'Error, token is not verified, select an option to get a new token.',
            'status' => 400
        ]);
    }

    private function canSkipTwoFactor()
    {
        return (app()->environment() == 'local') && (env('SKIP_TWO_FACTOR') == true);
    }

    private function loginUser($user, $credentials = null)
    {
        $creds = array_only($credentials, ['username', 'password']);

        if (isset($credentials['email'])) {
            $creds = array_only($credentials, ['email', 'password']);
        }

        if ($credentials) {
            \Auth::attempt($creds);
        }

        $request = app(Request::class);

        if (app()->environment() == 'production') {
            delaySave(CrimsClientLoginTrail::class, [
                'id' => Uuid::generate()->string,
                'session_id' => Session::getId(),
                'user_id' => ($credentials) ? auth()->id() : $user->id,
                'user_agent' => $request->server('HTTP_USER_AGENT'),
                'ip' => $request->ip()
            ]);
        }

        $last_two_factor = (new AuthRepository())->setSuccessfulTwoFactor($user);

        return response()->json([
            'token' => '',
            'last_two_factor' => $last_two_factor,
            'status' => SympfonyResponse::HTTP_CREATED
        ]);
    }

    private function checkSecondFactor($user, $credentials)
    {
        $request = app(Request::class);

        $is_one_touch = isset($credentials['from_authy_one_touch']) && $credentials['from_authy_one_touch'];

        if (!$is_one_touch) {
            $response = $this->authy->verifyToken($user, $request->get('token'));

            if ($response->success) {
                return $this->loginUser($user, $credentials);
            }
        } else {
            $verified = $this->authy->verifyOneTouch($user, $credentials['one_touch_approval_uuid']);

            if ($verified) {
                return $this->loginUser($user, $credentials);
            }
        }

        return response()->json(['message' => 'error_token_not_verified', 'status' => 400]);
    }

    private function checkOneTimeKey($user, $credentials)
    {
        $validate = (new AuthRepository($user))->validateOneTimeKey($credentials['token']);

        if ($validate->success) {
            return $this->loginUser($user, $credentials);
        }

        return response()->json($validate);
    }

    public function requestSMS(Request $request)
    {
        $credentials = $request->only(['username', 'password']);

        if (filter_var($credentials['username'], FILTER_VALIDATE_EMAIL)) {
            $credentials['email'] = $credentials['username'];
        }

        $user = $this->validateCredentials($credentials);

        if (!$user instanceof User) {
            return $user;
        }

        if (is_null($user->authy_id)) {
            throw new CrimsException(
                'We could not send your token, please ask the administrator to confirm your phone number',
                400
            );
        }

        $method = $request->get('option');

        if ($method == 'email') {
            $response = (new AuthRepository($user))->requestTokenViaMail();
        } elseif ($method == 'authy') {
            $response = $this->authy->sendOneTouch($user->authy_id, 'Password Confirmation');
        } else {
            $response = $this->authy->requestSMS($user);
        }

        if ($response->success) {
            return $this->response()->json([
                'created' => true,
                'status' => Response::HTTP_CREATED
            ], Response::HTTP_CREATED);
        }

        $previous = new CrimsException($response->errors['message']);

        throw new CrimsException('An error occurred sending token', 500, $previous);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws CrimsException
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function passwordResetRequestSMS(Request $request)
    {
        $user = User::where('username', $request->get('username'))->first();

        if (filter_var($request->get('username'), FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $request->get('username'))->first();
        }

        if (!$user || !$user->authy_id) {
            throw new CrimsException(
                'We could not send your token, 
                please ask the administrator to confirm your phone number or email address',
                400
            );
        }

        $option = $request->has('option') ? $request->get('option') : 'sms';

        $message = $option == 'authy' ? 'Reset Password Confirmation' : '';

        $response = (new TokenVerification($user))->send($option, $message);

        if ($response) {
            return $this->response()->json([
                'created' => true,
                'status' => Response::HTTP_CREATED
            ], Response::HTTP_CREATED);
        }

        $previous = new CrimsException($response);

        throw new CrimsException('An error occurred sending token', 500, $previous);
    }

    public function getReset(Request $request, $username, $token)
    {
        if ((new AuthRepository)->checkResetLink($username, $token)) {
            return view('auth.reset');
        }

        $request->session()->flash(
            'message',
            ['type' => 'error', 'message' => 'The link is incorrect, please request a new one']
        );

        return redirect('/');
    }

    public function postReset(ResetPasswordRequest $request, $username, $token)
    {
        if ((new AuthRepository)->checkResetLink($username, $token)) {
            $user = User::where('username', $username)->first();

            $user->update(['password' => $request->input('password')]);

            (new AuthRepository())->removeResetLink($username);

            return response()->json([
                'message' => 'The password was reset, you can now log in',
                'status' => 200
            ]);
        }

        return response()->json([
            'message' => 'The link is incorrect, please request a new one',
            'status' => 401
        ]);
    }

    public function getAuthenticatedUser()
    {
        return $this->response->item(Auth::user(), new CurrentUserTransformer());
    }

    public function logout()
    {
        \Auth::logout();

        return $this->response()->created();
    }

    public function performLogout()
    {
        \Auth::logout();

        \request()->session()->flash('message', ['type' => 'success', 'message' => 'You are now logged out']);

        return redirect()->to('/');
    }

    private function validateCredentials($credentials)
    {
        $user = User::where('username', $credentials['username'])->first();

        $logged = Auth::validate(array_only($credentials, ['username', 'password']));

        if (isset($credentials['email'])) {
            $user = User::where('email', $credentials['email'])->first();

            $logged = Auth::validate(array_only($credentials, ['email', 'password']));
        }

        if (!$logged) {
            if ((new AuthRepository())->checkTooManyLoginAttempts($user) & !is_null($user)) {
                return response()->json([
                    'message' => 'too_many_login_attempts',
                    'status' => SympfonyResponse::HTTP_FORBIDDEN
                ]);
            }

            (new AuthRepository())->incrementLoginAttempts($user);

            return response()->json([
                'error' => 'invalid_credentials',
                'status' => SympfonyResponse::HTTP_UNAUTHORIZED
            ]);
        }

        if (!$user->active) {
            return response()->json([
                'message' => 'user_inactive',
                'status' => SympfonyResponse::HTTP_FORBIDDEN
            ]);
        }

        if ((new AuthRepository())->checkTooManyLoginAttempts($user)) {
            return response()->json([
                'message' => 'too_many_login_attempts',
                'status' => SympfonyResponse::HTTP_FORBIDDEN
            ]);
        }

        (new AuthRepository())->resetLoginAttempts($user);

        return $user;
    }

    public function updateDeviceFcm(Request $request)
    {
        $clientUser = ClientUser::findOrFail($request->client_id);

        $clientUserDeviceToken = $clientUser->deviceTokens()->where('device_fcm_token', $request->token)->first();

        if (!count($clientUserDeviceToken)) {
            $clientUserDeviceTokenRepository = new ClientUserDeviceTokenRepository();

            $input = [
                'client_user_id' => $request->client_id,
                'device_fcm_token' => $request->token,
                'platform' => is_null($request->platform) ? 'undefined' : $request->platform
            ];

            $clientUserDeviceTokenRepository->save($input);

            return $this->response()->json(['status' => SympfonyResponse::HTTP_CREATED, 'message' => 'Token created']);
        }

        return $this->response()->json(['status' => SympfonyResponse::HTTP_CREATED]);
    }
}
