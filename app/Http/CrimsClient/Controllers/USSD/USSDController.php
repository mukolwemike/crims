<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-27
 * Time: 09:00
 */

namespace App\Http\CrimsClient\Controllers\USSD;

use App\Cytonn\USSD\USSDHandler;
use App\Http\CrimsClient\Controllers\Controller;

class USSDController extends Controller
{
    /**
     * @param $key
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index($key)
    {
        if (!$this->auth($key)) {
            return $this->terminateSession("Could not process your request. Try again later");
        }

        $ussd = new USSDHandler(
            request("sessionId"),
            request("serviceCode"),
            request("phoneNumber"),
            request("text")
        );

        $response = $ussd->handle();

        if ($response['type'] == 'con' && !empty($response['text'])) {
            return $this->continueSession($response['text']);
        }

        return $this->terminateSession($response['text']);
    }

    private function terminateSession($response)
    {
        if (empty($response)) {
            reportException(new \InvalidArgumentException("Empty USSD response found"));

            $response = 'Could not process your request. Try again later';
        }

        return $this->respond('END '.$response);
    }

    private function continueSession($response)
    {
        return $this->respond('CON '.$response);
    }

    private function respond($response)
    {
        return response($response, 200, [
           'Content-type' => 'text/plain'
        ]);
    }

    private function auth($key)
    {
        $pass = config('services.ussd.key');

        return $pass === $key;
    }
}
