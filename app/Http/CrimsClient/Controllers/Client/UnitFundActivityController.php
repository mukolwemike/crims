<?php

namespace App\Http\CrimsClient\Controllers\Client;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Http\CrimsClient\Controllers\Controller;
use Cytonn\Api\Transformers\ClientUnitFundActivityTransformer;

class UnitFundActivityController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');
    }

    /**
     * @param $clientId
     * @param null $summary
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function activities($clientId, $summary = null)
    {
        $client = Client::findOrFail($clientId);

        $this->user()->shouldAccessClient($client);

        $activities = UnitFundInvestmentInstruction::where('client_id', $client->id)
            ->latest('date')
            ->get()
            ->map(function ($activity) {
                return (new ClientUnitFundActivityTransformer())->transform($activity);
            });

        $displayed_activities = $summary ? $activities->take($summary) : $activities;

        return $this->response()->json(['data' => $displayed_activities]);
    }
}
