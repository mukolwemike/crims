<?php

namespace App\Http\CrimsClient\Controllers\Client;

use App\Cytonn\Clients\Transformers\ClientTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\UnitHolding;
use App\Http\CrimsClient\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Api\Transformers\RealEstateClientUnitTransformer;
use Cytonn\Investment\BankDetails;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function show($id)
    {
        $client = Client::findByUuid($id);

        $this->user()->isFaOrOwn($client);

        return $this->response->item($client, new ClientTransformer());
    }

    /**
     * @param Request $request
     * @param Product $product
     * @param $client
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function invPortfolio(Request $request, $productId, $client)
    {
        $client = Client::findOrFailByUuid($client);

        $this->user()->isFaOrOwn($client);

        $date = Carbon::parse($request->get('date'));

        $product = Product::find($productId);

        $investments = $client->investments()->where('product_id', $product->id)
            ->active()
            ->get()
            ->map(function (ClientInvestment $investment) use ($product, $date) {

                $calculated = $investment->calculate($date, true);

                return (object)[
                    'product' => $product->name,
                    'principal' => $investment->amount,
                    'net_interest' => $calculated->netInterestBeforeDeductions(),
                    'withdrawal' => $calculated->withdrawals($date),
                    'total' => $calculated->total()
                ];
            });

        return response()->json(['investments' => $investments->all()]);

//        return Response((new ClientPortfolio())->clientInvestment($product, $client, $date->startOfDay()));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function rePortfolio(Request $request, $id)
    {
        $client = Client::findOrFailByUuid($id);

        $clientId = $client->id;

        $this->user()->isFaOrOwn($client);

        $filter = function ($unitHolding) use ($clientId) {
            return $unitHolding->has('unit')->where('client_id', $clientId);
        };

        return $this->processTable(new UnitHolding(), new RealEstateClientUnitTransformer(), $filter);
    }

    public function bankDetails($id)
    {
        $details = ClientbankAccount::find($id);

        $branch = ($details->branch_id) ? ClientBankBranch::find($details->branch_id) : '';

        return [
            'bank' => ($branch) ? ClientBank::find($branch->bank_id)->name : '',
            'branch' => ($branch) ? $branch->name : '',
            'account_name' => $details->account_name,
            'account_number' => $details->account_number
        ];
    }

    public function clientBankAccounts($id)
    {
        $client = Client::findOrFailByUuid($id);

        $access = $this->user()->isFaOrOwn($client, false);

        if (!$access) {
            return $this->response()
                ->json(['message' => 'No access to this client', 'status' => Response::HTTP_UNAUTHORIZED]);
        }

        $clientId = $client->id;

        $clientBankAccounts = ClientBankAccount::active()->where('client_id', $clientId)
            ->get()->map(function ($acc) {
                $bank = new BankDetails(null, null, $acc);

                return [
                    'id' => $acc->id,
                    'name' => $bank->accountName() . ' - ' . $bank->bankName() . ' - ' . $bank->accountNumber(),
                    'swift_code' => $bank->swiftCode(),
                    'bank_transfer_type' => $bank->paymentType()
                ];
            });

        return $this->response()->json([
            'data' => $clientBankAccounts,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }
}
