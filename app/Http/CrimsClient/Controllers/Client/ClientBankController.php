<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 28/04/2018
 * Time: 11:52
 */

namespace App\Http\CrimsClient\Controllers\Client;

use App\Cytonn\Models\ClientBank;
use Cytonn\Api\Transformers\BankBranchTransformer;
use Cytonn\Api\Transformers\BankTransformer;

class ClientBankController
{
    public function allBanks()
    {
        return ClientBank::all()->map(function ($bank) {
            return (new BankTransformer())->transform($bank);
        });
    }

    public function bankBranches(ClientBank $bank)
    {
        $branches = $bank->branches()->orderBy('name', 'asc')->get();

        return $branches->map(function ($branch) {
            return (new BankBranchTransformer())->transform($branch);
        });
    }
}
