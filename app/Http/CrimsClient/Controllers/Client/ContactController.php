<?php

namespace App\Http\CrimsClient\Controllers\Client;

use App\Cytonn\Contact\TitleTransformer;
use App\Cytonn\Models\CompanyNatures;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Title;
use App\Http\CrimsClient\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
    public function titles()
    {
        return Title::all();
    }

    public function getTitles()
    {
        $titles = Title::all()->map(function ($title) {
            return [
                'label' => $title->name,
                'value' => $title->id
            ];
        });
        return $this->response()->json([
            'titles' => $titles,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }
    
    public function gender()
    {
        return Gender::all();
    }

    public function getGender()
    {
        $genders = Gender::all()->map(function ($gender) {
            return [
                'label' => $gender->abbr,
                'value' => $gender->id
            ];
        });
        return $this->response()->json([
            'genders' => $genders,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function contactMethods()
    {
        return ContactMethod::get(['id', 'description'])->toArray();
    }

    public function businessNatures()
    {
        return CompanyNatures::get(['id', 'name'])->toArray();
    }

    public function employmentTypes()
    {
        return (new Employment())->get(['id', 'name'])->toArray();
    }

    public function sourceOfFunds()
    {
        return FundSource::where('active', '=', 1)
            ->orderBy('weight')->get(['id', 'name'])->toArray();
    }

    public function countries()
    {
        return (new Country())->get(['id', 'name'])->toArray();
    }
}
