<?php
/**
 * Date: 27/01/2016
 * Time: 11:20 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Http\CrimsClient\Controllers\Client;

use App\Cytonn\Clients\AccountOverviewRepository;
use App\Cytonn\Models\Client;
use App\Http\CrimsClient\Controllers\Controller;

class AccountOverviewController extends Controller
{
    /**
     * AccountOverviewController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');
        $this->repo = new AccountOverviewRepository();
    }

    /**
     * @param $clientId
     * @return array
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function brief($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $this->user()->shouldAccessClient($client);

        return $this->repo->getBriefOverview($client);
    }
}
