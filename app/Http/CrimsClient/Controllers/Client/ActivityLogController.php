<?php

namespace App\Http\CrimsClient\Controllers\Client;

use App\Cytonn\Clients\ActivityLogRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Http\CrimsClient\Controllers\Controller;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;

class ActivityLogController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->repo = new ActivityLogRepository();
        $this->middleware('jwt.auth');
    }

    /**
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function brief($clientId)
    {
        $input = \Input::all();

        $perPage = isset($input['per_page']) ? $input['per_page'] : null;

        $client = Client::findOrFailByUuid($clientId);

        $this->user()->shouldAccessClient($client);

        $activities = $this->repo->briefActivityLog($client, $perPage)
            ->map(function ($activity) {
                $product = ($activity instanceof ClientInvestment)
                    ? $activity->product : $activity->investment->product;

                return (object)[
                    'type' => $activity->transaction,
                    'type_name' => formatSlug($activity->transaction),
                    'product' => $product->name,
                    'date' => $activity->transaction_date->toDayDateTimeString(),
                    'amount' => $activity->amount,
                ];
            });

        $sorted = $activities->sortByDesc(function ($activity) {
            return strtotime($activity->date);
        })->values();

        if ($perPage) {
            $sorted = $sorted->take($input['per_page']);
        }

        return $this->response()->json(['data' => $sorted]);
    }

    /**
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function reBrief($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $this->user()->shouldAccessClient($client);

        $activities = $this->repo->briefReActivityLog($client);

        return $this->response()->json($activities);
    }

    /**
     * @param $clientId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\AuthorizationDeniedException
     */
    public function loyaltyBrief($clientId)
    {
        $client = Client::findOrFailByUuid($clientId);

        $this->user()->shouldAccessClient($client);

        $activities = $this->repo->briefLoyaltyActivityLog($client);

        return $this->response()->json($activities);
    }
}
