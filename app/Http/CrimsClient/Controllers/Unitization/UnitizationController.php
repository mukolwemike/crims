<?php

namespace App\Http\CrimsClient\Controllers\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPerformance;
use App\Http\CrimsClient\Controllers\Controller;
use Cytonn\Api\Transformers\FundCalculationTypesTransformer;
use Cytonn\Unitization\Rules\UnitFundRules;
use Cytonn\Api\Transformers\Unitization\UnitFundTransformer;
use Symfony\Component\HttpFoundation\Response;

class UnitizationController extends Controller
{
    use UnitFundRules;

    public function getAllFunds()
    {
        $fundsList = UnitFund::all()
            ->each(
                function ($fund) {
                    $fund->label = $fund->name;
                    $fund->value = $fund->id;
                }
            )
            ->toArray();

        return response(['fetched' => true, 'data' => $fundsList], 200);
    }

    public function getCISFunds()
    {
        $fundsList = UnitFund::active()->ofType('cis')
            ->get()
            ->map(function ($fund) {
                return (new UnitFundTransformer())->transform($fund);
            });

        return response([
            'fetched' => true,
            'data' => $fundsList,
            'status' => Response::HTTP_CREATED
            ], 200);
    }

    public function getFundAccounts($id)
    {
        $fundId = (int)$id;

        $fund = UnitFund::where('id', $fundId)->first();

        $data = [
            'bank' => $fund->repo->collectionAccount('bank'),
            'mpesa' => $fund->repo->collectionAccount('mpesa')
        ];

        return response(['fetched' => true, 'data' => $data], 200);
    }

    public function show($id)
    {
        $fund = (new UnitFundTransformer())->transform(UnitFund::find($id));

        return response(['fetched' => true, 'data' => $fund], 200);
    }

    public function showUnitFund($id)
    {
        $unit_fund = UnitFund::findOrFail($id);

        $calculation_slug = $unit_fund->category->calculation->slug;

        $perfomance = $unit_fund->latestPerformance();

        if ($calculation_slug == 'variable-unit-price') {
            $price = $perfomance->price;

            return [
                'name' => $unit_fund->name,
                'price' => $price
            ];
        }

        if ($calculation_slug == 'daily-yield') {
            $net_daily_yield = $perfomance->net_daily_yield;

            return [
                'name' => $unit_fund->name,
                'net_daily_yield' => $net_daily_yield
            ];
        }

        return [
            'name' => $unit_fund->name,
            'net_daily_yield' => null,
            'price' => null
        ];
    }
}
