<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/10/18
 * Time: 1:21 PM
 */

namespace App\Http\CrimsClient\Controllers\Notifications;

use App\Cytonn\Clients\ClientInstructionApprovalsRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\ClientNotification;
use App\Cytonn\Notifier\ClientNotificationRepository;
use App\Http\CrimsClient\Controllers\Controller;

class NotificationsController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ClientNotificationRepository();
    }

    public function index($uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        $notifications = $this->repo->notifications($client);

        $approvals = (new ClientInstructionApprovalsRepository())->getApprovals($client);

        if ($approvals) {
            $notifications->prepend($approvals);
        }

        return response()->json($notifications);
    }

    public function update($id)
    {
        $notification = ClientNotification::findOrFail($id);

        $this->repo->updateNotification($notification);

        return response()->json([
            'updated' => true
        ], 200);
    }
}
