<?php
/**
 * Date: 13/04/2016
 * Time: 10:17 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Http\CrimsClient\Controllers\Applications;

use App\Cytonn\Authentication\TokenVerification;
use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Data\Iprs;
use App\Cytonn\Mailers\System\PaymentProofUploadedMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Kyc\IprsRequest;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationDocument;
use App\Cytonn\Models\ClientFilledInvestmentApplicationJointHolders;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\USSD\USSDRepository;
use App\Events\InvestmentApplicationComplete;
use App\Exceptions\CrimsException;
use App\Http\CrimsClient\Controllers\Controller;
use App\Http\CrimsClient\Requests\JointHolderForm;
use App\Http\CrimsClient\Requests\KycFilesUploadRequest;
use App\Http\CrimsClient\Requests\SaveApplicationFormRequest;
use App\Http\CrimsClient\Transformers\ClientFilledInvestmentApplicationTransformer;
use App\Jobs\USSD\CreateStkPush;
use App\Jobs\USSD\SendMessages;
use Cytonn\Clients\ClientRepository;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Mailers\System\FlaggedRiskyClientMailer;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Rinvex\Authy\InvalidConfiguration;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class ApplicationController extends Controller
{
    protected $mailer;

    protected $repo;

    protected $repoClient;

    public function __construct()
    {
        $this->repo = new ClientApplicationRepository();
        $this->mailer = new FlaggedRiskyClientMailer();
        $this->repoClient = new ClientRepository();

        parent::__construct();
    }

    public function show($uuid)
    {
        $application = ClientFilledInvestmentApplication::findOrFailByUuid($uuid);

        return $this->response()->item($application, new ClientFilledInvestmentApplicationTransformer());
    }

    public function getJointHolders($applId)
    {
        $applications = ClientFilledInvestmentApplication::findOrFailByUuid($applId);
        $jointholders = (new ClientFilledInvestmentApplicationJointHolders())
            ->where('application_id', $applications->id)->get();

        return $jointholders;
    }

    public function validateInvestment(Request $request)
    {
        $this->validate(
            $request,
            [
                'amount' => 'required|numeric',
                'tenor' => 'required_if:product_category, "products"|numeric',
                'product_id' => 'required_if:product_category, "products"|exists:products,id',
                'unit_fund_id' => 'required_if:product_category, "funds"|exists:unit_funds,id'
            ],
            [
                'amount.required' => 'Please enter amount to invest',
                'amount.numeric' => 'Amount to invest must be number',
                'product_id.required' => 'Please select a product',
                'product_id.exists' => 'Please select a product'
            ]
        );

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ]);
    }

    public function validateRisk(Request $request)
    {
        $this->validate(
            $request,
            [
                'risk_duration' => 'required',
                'risk_inv_types' => 'required',
                'risk_inv_volatility' => 'required',
                'risk_inv_objective' => 'required',
                'risk_trade_frequency' => 'required',
                'risk_knowledge' => 'required',
                'risk_profile' => 'required'
            ],
            [
                'risk_duration.required' => 'Please select one of the options',
                'risk_inv_types.required' => 'Please select one of the options',
                'risk_inv_volatility.required' => 'Please select one of the options',
                'risk_inv_objective.required' => 'Please select one of the options',
                'risk_trade_frequency.required' => 'Please select one of the options',
                'risk_knowledge.required' => 'Please select one of the options',
                'risk_profile.required' => 'Please select one of the options'
            ]
        );

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ]);
    }

    /**
     * @param SaveApplicationFormRequest $request
     * @param $applicationType
     * @param null $app_uuid
     * @return Response
     * @throws Throwable
     */
    public function processClientApplication(SaveApplicationFormRequest $request, $applicationType, $app_uuid = null)
    {
        if ($app_uuid == 'null' || $app_uuid == 'NULL') {
            $app_uuid = null;
        }

        return DB::transaction(function () use ($request, $applicationType, $app_uuid) {
            $appF = $this->saveFilledApplication($request, $applicationType, $app_uuid);

            $this->checkRiskyClient($request, $applicationType);

            $client = null;

            if ((!$appF->application) && $appF->isShortApplication()) {
                $client = $this->processApplication($appF);
            }

            return $this->response()->json([
                'created' => true,
                'client' => $client,
                'application' => $appF->uuid,
                'status' => Response::HTTP_CREATED
            ], Response::HTTP_CREATED);
        });
    }

    /**
     * @param $request
     * @param $applicationType
     */
    private function checkRiskyClient($request, $applicationType)
    {
        $data = $request->all();

        $data['individual'] = $applicationType == 'corporate' ? 2 : 1;

        $data['new_client'] = 1;

        $channel = $request->get('channel');

        $channel = $channel == 'mobile' ? "CRIMS-MOBILE" : "CRIMS-WEB";

        (new ClientApplicationRepository())->checkAndNotifyRiskyClient($data, $channel);
    }

    /**
     * @param ClientFilledInvestmentApplication $application
     * @return mixed
     * @throws ClientInvestmentException
     * @throws Throwable
     */
    private function processApplication(ClientFilledInvestmentApplication $application)
    {
        $action = new USSDRepository();

        $app = $action->processApplication($application);

        $app = $action->approveApplication($app);

        $action->createClientUser($app, $app->client);

        $this->requestIprs($application, $app->client);

        $action->createMpesaAccount($app->client);

        return $app->client;
    }

    private function requestIprs(ClientFilledInvestmentApplication $application, Client $client)
    {
        if ((!$application->isShortApplication()) || IprsRequest::where('client_id', $client->id)->exists()) {
            return;
        }

        $iprs = new Iprs();
        $iprs->createRequest($client);

        foreach ($client->jointDetail as $joint) {
            $iprs->createRequest($client, $joint->id_or_passport, $joint);
        }
    }

    public function uploadApplicationForm(Request $request)
    {
        $input = request()->all();
        $slug = $input['slug'];
        $file = request()->file('file');
        unset($input['file']);
        $document = Document::make((array)file_get_contents($file), $file->getClientOriginalExtension(), $slug);
        $type = DocumentType::where('slug', $slug)->first();
        $input['phone'] = trimPhoneNumber($input['phone']);

        $filledDocument = new ClientFilledInvestmentApplicationDocument();
        $filledDocument->document_type_id = $type->id;
        $filledDocument->document()->associate($document);
        $filledDocument->description = $input['phone'];
        $filledDocument->payload = json_encode($input);
        $filledDocument->save();
    }

    public function saveFilledApplication(SaveApplicationFormRequest $request, $applicationType, $app_uuid)
    {
        $application = new ClientFilledInvestmentApplication();
        $fund_name = '';

        $channel = $request->get('channel');
        if (!$channel) {
            $channel = 'web';
        }

        if ($app_uuid == 'null' || $app_uuid == 'NULL') {
            $app_uuid = null;
        }

        if ($app_uuid != null) {
            $application = ClientFilledInvestmentApplication::findOrFailByUuid($app_uuid);
        }

        if ($request['product_category'] == 'funds') {
            $fund_name = UnitFund::where('id', $request['unit_fund_id'])->first()->short_name;
        }

        if ($applicationType == 'individual') {
            if ($fund_name == 'CMMF') {
                $request->validateCMMFIndividual();
            } else {
                $request->validateIndividual();
            }
        } elseif ($applicationType == 'corporate') {
            $request->validateCorporate();
        } else {
            throw new NotFoundHttpException('The application type was not found');
        }

        if (isset($request['financial_advisor'])) {
            $fa = CommissionRecepient::find($request['financial_advisor']);

            $request['financial_advisor'] = $fa ? $fa->id : null;
        }

        $application->repo->saveApplication(
            $request->except(['jointHolders', 'product_category', 'referral_code', 'channel', 'file', 'slug'])
        );

        if (isset($request['referral_code'])) {
            $fa = (new USSDRepository())->financialAdvisor($request['referral_code']);

            $request['financial_advisor'] = $fa ? $fa->id : null;

            unset($request['referral_code']);
        }

        if ($application->uuid && isset($request['jointHolders'])) {
            $application->jointHolders()->createMany($request->input('jointHolders'));
        }

        $this->updateUploadedForm($request, $applicationType, $application);

        $cid = Channel::where('slug', $channel)->first();

        $application->update(['channel_id' => !$cid ?: $cid->id]);

        return $application;
    }

    private function updateUploadedForm(Request $request, $applicationType, ClientFilledInvestmentApplication $application)
    {
        $phone = $applicationType == 'individual' ? $request->get('telephone_home') : $request->get('telephone_office');

        $uploadedForm = ClientFilledInvestmentApplicationDocument::where('description', trimPhoneNumber($phone))->latest()->first();

        if ($uploadedForm) {
            $uploadedForm->update([
                'application_id' => $application->id,
            ]);
        }
    }

    public function getReferredFA($referralCode, $mobile = null)
    {
        $fa = $mobile ? (new USSDRepository())->financialAdvisor($referralCode)
            : CommissionRecepient::find($referralCode);

        return $this->response()->json([
            'financialAdvisor' => $fa ? $fa->name : 'None',
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function commissionRecipients()
    {
        $fas = CommissionRecepient::all()
            ->map(function ($recipient) {
                return [
                    "label" => $recipient->name . " ($recipient->referral_code)",
                    "value" => $recipient->id,
                    "referral_code" => $recipient->referral_code
                ];
            });

        return $this->response()->json([
            'data' => $fas,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function uploadDocument()
    {
        $input = request()->all();

        $slug = $input['type'];

        $file = request()->file('file');

        unset($input['file']);

        $input['process_type'] = 'investment';

        $document = Document::make(file_get_contents($file), $file->getClientOriginalExtension(), $slug);

        if ($input['process_type'] === 'reserve_unit' || $input['process_type'] === 'register_shareholder') {
            $application = ClientTransactionApproval::findOrFail($input['application_id']);

            $this->repo->upload($input, $application, $document);
        }

        $application = ClientFilledInvestmentApplication::findOrFailByUuid($input['application_id']);

        $input['application_id'] = $application->id;

        $application->clientRepo->upload($input, $application, $document);

        if ($slug === 'payment') {
            (new PaymentProofUploadedMailer())->notify($application->id, 'application');
        }

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function mandate(KycFilesUploadRequest $request)
    {
        $slug = $request->input('slug');
        $appId = $request->input('application_id');
        $file = $request->file('file');
        $mandate = $request->input('signing_mandate');

        if ($appId == "undefined" || $appId == null) {
            return response(
                json_encode([
                    'application' => ['Please complete the investment and subscriber stages before uploading documents']
                ]),
                422
            )->header('Content-Type', 'application/json');
        }

        $application = ClientFilledInvestmentApplication::findOrFailByUuid($appId);

        if (str_contains($file->getClientMimeType(), 'pdf') ||
            str_contains($file->getClientMimeType(), 'image')) {
            $application->repo->saveMandateDocument(); // save document upload

            $this->updateMandate(); // update the mandate status
        } else {
            return response(
                json_encode(['file' => ['The file should be an  or pdf']]),
                422
            )->header('Content-Type', 'application/json');
        }

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function kycComplete($appId)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($appId);

        $application->progress_kyc = 1;
        $application->save();

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function paymentComplete($appId)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($appId);

        $application->progress_payment = 1;
        $application->save();

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function validateMpesaDeposit(Request $request)
    {
        $this->validate(
            $request,
            ['amount' => 'required'],
            ['amount.required' => 'Please specify the amount to invest']
        );

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }


    public function paymentMpesaComplete(Request $request, $appId)
    {
        $this->validateMpesaDeposit($request);

        if ($this->sendStkPush($request)) {
            $this->paymentComplete($appId);

            return $this->response()->json([
                'created' => true,
                'status' => Response::HTTP_CREATED
            ], Response::HTTP_CREATED);
        }

        return $this->response->json([
            'status' => 422,
            'message' => 'Unable to complete User Payment request via MPESA. Please use the paybill number given'
        ]);
    }

    protected function sendStkPush($data)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($data['applId']);

        $client = Client::findByUuid($data['clientUUID']);

        $reference = $client ? $client->client_code : 'app_' . $application->id;

        $payBill = (new USSDRepository())->getPayBill();

        $phone = $application->telephone_home;
        $amount = $data['amount'];

        $this->dispatch(new CreateStkPush($payBill, $phone, $amount, $reference));

        return true;
    }

    public function mandateComplete($appId)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($appId);

        $application->progress_mandate = 1;
        $application->save();

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function complete($uuid)
    {
        $app = ClientFilledInvestmentApplication::findOrFailByUuid($uuid);

        if ($app->progress_investment && $app->progress_subscriber && !$app->complete) {
            $app->complete = true;
            $app->save();

            event(new InvestmentApplicationComplete($app));

            return $this->response->created();
        } elseif ($app->complete) {
            event(new InvestmentApplicationComplete($app));
            return $this->response()->created();
        } else {
            return $this->response()->errorBadRequest($this->getIncompleteMessage($app));
        }
    }

    private function getIncompleteMessage($app)
    {
        if (!$app->progress_investment) {
            return 'Please complete the investment stage of the application';
        } elseif (!$app->progress_subscriber) {
            return 'Please complete the subscriber stage of the application';
        } elseif (!$app->progress_kyc) {
            return 'Please upload all the KYC documents in the kyc stage of the application';
        } else {
            return 'The application is not complete, check the previous steps';
        }
    }

    public function validateJointHolder(JointHolderForm $request)
    {
        return $this->response()->created();
    }

    public function updateMandate()
    {
        $input = request()->all();

        $application = ClientFilledInvestmentApplication::findByUuid($input['application_id']);

        $application->signing_mandate = $input['signing_mandate'];

        $application->save();

        return $this->response()->json([
            'created' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function applicationDocuments($uuid)
    {
        $application = ClientFilledInvestmentApplication::findOrFailByUuid($uuid);

        $documents = $application->clientRepo->getDocumentDetails('kyc');

        $uploaded = [];

        foreach ($documents as $document) {
            $uploaded[$document['slug']] = $document['slug'];
        }

        return response()->json([
            'uploaded' => $uploaded,
            'complete' => $this->requiredKyc($application)
        ]);
    }

    public function requiredKyc(ClientFilledInvestmentApplication $application)
    {
        $required = $this->requiredComplianceChecklist($application)->count();

        $checked = ClientFilledInvestmentApplicationDocument::where('application_id', $application->id)->count();

        if ($checked > 0 && $checked == $required || $checked > $required) {
            return true;
        }

        return false;
    }

    public function requiredComplianceChecklist(ClientFilledInvestmentApplication $application)
    {
        $fundManager = $application->product
            ? $application->product->fundManager
            : $application->unitFund->manager;

        $clientType = $application->individual ? 1 : 2;

        return $fundManager->complianceChecklist()
            ->where('client_type_id', $clientType)
            ->where('required', true)
            ->get();
    }

    public function requestBankSms(Request $request)
    {
        $application = ClientFilledInvestmentApplication::findByUuid($request->get('appId'));
        $repo = ($application->unitFund) ? $application->unitFund->repo : $application->product->repo;
        $client = ($application->application) ? $application->application->client : null;

        $ref = $client ? $client->client_code : 'app_' . $application->id;

        $data = ['client_code' => $ref, 'account' => $repo->collectionAccount('bank'), 'mpesa' => $repo->collectionAccount('mpesa')];

        dispatch(new SendMessages($client->telephone_home, 'bank-details', $data));

        return $this->response()->json([
            'sent' => true,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws InvalidConfiguration
     */
    public function sendVerifyToken(Request $request)
    {
        $input = $request->all();

        $input = $this->returnInput($input);

        if ($input['country_code'] != '+254') {
            return $this->response()->json([
                'verified' => false,
                'message' => 'We could not validate your phone number, kindly send your application form to operations@cytonn.com or call 0709101200 for assistance.',
                'status' => Response::HTTP_CREATED
            ], Response::HTTP_CREATED);
        }

        $response = (new TokenVerification(null, $input))->send('sms', '');

        return $this->response()->json([
            'verified' => $response->success ? true : false,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws InvalidConfiguration
     */
    public function validateToken(Request $request)
    {
        $input = $request->all();

        $input = $this->returnInput($input);

        $validate = (new TokenVerification(null, $input))->verify('sms');

        return $this->response()->json([
            'validated' => $validate->success,
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    /**
     * @param array $input
     * @return array
     */
    private function returnInput(array $input): array
    {
        list($code, $phone) = splitPhoneNumber($input['phone']);

        $input['country_code'] = $code;
        $input['phone'] = str_replace(' ', '', $phone);

        return $input;
    }
}
