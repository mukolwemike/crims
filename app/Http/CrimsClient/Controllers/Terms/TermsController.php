<?php

namespace App\Http\CrimsClient\Controllers\Terms;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Terms;
use App\Cytonn\Models\TermsAndCondition;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Http\CrimsClient\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;

class TermsController extends Controller
{
    public function general($product)
    {
        $product = Product::findOrFail($product);

        $terms = Terms::isActive()
            ->where('shortname', $product->shortname)
            ->ofType('new_investment')
            ->latest()
            ->first();

        return response()->json($terms);
    }

    public function fundGeneral($fund)
    {
        $fund = UnitFund::findOrFail($fund);

        $terms = Terms::isActive()
            ->where('shortname', $fund->short_name)
            ->ofType('new_investment')
            ->latest()
            ->first();

        return response()->json($terms);
    }

    public function topup($product)
    {
        $product = Product::findOrFail($product);

        $terms = Terms::isActive()
            ->where('shortname', $product->shortname)
            ->ofType('topup')
            ->latest()
            ->first();

        return response()->json($terms);
    }

    public function rollover($investment)
    {
        $investment = ClientInvestment::findOrFailByUuid($investment);

        $product = $investment->product;

        $terms = Terms::isActive()
            ->where('shortname', $product->shortname)
            ->ofType('rollover_withdraw')
            ->latest()
            ->first();

        return response()->json($terms);
    }

    public function getAllTerms()
    {
        $terms = TermsAndCondition::all();

        return response()->json([
            'data' => [
                'terms' => $terms,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function mobileGetAllFundGeneralTerms()
    {

        $terms = Terms::isActive()
            ->ofType('new_investment')
            ->latest()
            ->first();

        return response()->json([
            'data' => [
                'terms' => $terms,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function getAllTermsProduct($product)
    {
        $terms = TermsAndCondition::where('product_short_name', $product)->get();

        return response()->json([
            'data' => [
                'terms' => $terms,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public function getAllTermsForm($form, $product = null)
    {
        $query = TermsAndCondition::where('form', $form);

        if (!is_null($product)) {
            $query = $query->where('product_short_name', $product);
        }

        $terms = $query->get();

        return response()->json([
            'data' => [
                'terms' => $terms,
                'expiry_time' => (Carbon::now()->endOfDay()->diffInSeconds(Carbon::now())) * 1000
            ],
            'status' => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }
}
