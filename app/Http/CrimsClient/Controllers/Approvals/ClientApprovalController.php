<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/17/18
 * Time: 11:45 AM
 */

namespace App\Http\CrimsClient\Controllers\Approvals;

use App\Cytonn\Clients\ClientInstructionApprovalsRepository;
use App\Cytonn\Clients\Rules\ClientApprovalRules;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Http\CrimsClient\Controllers\Controller;
use Auth;
use Cytonn\Clients\Approvals\Engine\Approval;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ClientApprovalController extends Controller
{
    use ClientApprovalRules;

    public function index($uuid)
    {
        $client = Client::findOrFailByUuid($uuid);

        $approvals = (new ClientInstructionApprovalsRepository())->getApprovals($client);

        return response()->json($approvals);
    }

    /**
     * @return ResponseFactory|JsonResponse|Response
     * @throws Exception
     */
    public function approve()
    {
        $validator = $this->validateApproval(request());

        if ($validator) {
            return response([
                'errors' => $validator->messages()->getMessages(),
                'status' => 422
            ]);
        }

        $input = request()->all();

        $approval = ClientInstructionApproval::findOrFail($input['approval_id']);

        if (!(Auth::user()->id == $approval->user->id)) {
            return response()->json([
                'message' => 'User not allowed to approve this approval',
                'status' => 403
            ]);
        }

        $approval->update([
            'status' => $input['value'],
            'reason' => isset($input['reason']) ? $input['reason'] : ''
        ]);

        $approved = $this->allSignatoriesApprove($approval);

        if ($approval->fundInstruction && $approval->fundInstruction->type->slug == 'transfer' && $approved) {
            $this->unitFundInstructionApproval($approval->fundInstruction);
        }

        return response()->json([
            'created' => true,
            'status' => 200
        ]);
    }

    private function allSignatoriesApprove(ClientInstructionApproval $approval)
    {
        $approvals = ClientInstructionApproval::where('client_id', $approval->fundInstruction->client)
            ->where('fund_instruction_id', $approval->fundInstruction->id)
            ->get();

        foreach ($approvals as $approval) {
            if (!$approval->status) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @throws Exception
     */
    private function unitFundInstructionApproval(UnitFundInvestmentInstruction $instruction)
    {
        $client = $instruction->client;

        $data['instruction_id'] = $instruction->id;

        $approval = ClientTransactionApproval::create([
            'client_id' => $client->id,
            'transaction_type' => 'create_unit_fund_transfer',
            'payload' => $data,
            'sent_by' => getSystemUser() ? getSystemUser()->id : null,
            'awaiting_stage_id' => 1
        ]);

        $instruction->approval_id = $approval->id;

        $instruction->save();

        $approve = new Approval($approval);

        $approve->systemExecute();
    }
}
