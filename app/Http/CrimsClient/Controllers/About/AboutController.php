<?php

namespace App\Http\CrimsClient\Controllers\About;

use App\Http\CrimsClient\Controllers\Controller;

class AboutController extends Controller
{
    public function getOffices()
    {
        $url = "https://cytonn.com/api/get/offices";

        $client = new \GuzzleHttp\Client();

        $res = $client->get($url);

        $content = json_decode($res->getBody(), true);

        return response()->json($content['offices']);
    }
}
