<?php

namespace App\Http\CrimsClient\Controllers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Storage\DocumentStorageInterface;
use App\Http\Helpers\TableStateService;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use League\Flysystem\FileNotFoundException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 *
 * @package App\Http\Controllers
 */
abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, TableStateService;

    /**
     * @var int
     */
    protected $perPage = 10;

    /**
     * @var \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    protected $response;

    /**
     * @var
     */
    protected $user;

    /**
     * Controller constructor.
     *
     * @param $response
     */
    public function __construct()
    {
        $this->response = \response();

        $this->user = \Auth::user();
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    protected function response()
    {
        return $this->response;
    }

    /**
     * @return ClientUser
     */
    protected function user()
    {
        $this->user ?: $this->user = request()->user();

        return $this->user;
    }


    /**
     * @param Carbon $date
     * @return object
     */
    protected function commissionDates(Carbon $date)
    {
        $bulk = $this->getBulk($date);

        if ($bulk) {
            return (object)[
                'start' => $bulk->start,
                'end' => $bulk->end
            ];
        }

        return (object)[
            'start' => $this->getStart($date),
            'end' => $this->getEnd($date)
        ];
    }

    /**
     * @param Carbon $date
     * @return static
     */
    private function getStart(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->subMonthNoOverflow())) {
            return $bulk->end->copy()->addDay();
        }

        return $date->copy()->subMonthNoOverflow()->startOfMonth()->addDays(20);
    }

    /**
     * @param Carbon $date
     * @return static
     */
    private function getEnd(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->addMonthNoOverflow())) {
            return $bulk->start->copy()->subDay();
        }

        return $date->copy()->startOfMonth()->addDays(19);
    }

    /**
     * @param $date
     * @return mixed
     */
    private function getBulk($date)
    {
        return BulkCommissionPayment::where('start', '<=', $date)->where('end', '>=', $date)->first();
    }

    /**
     * Stream a file from the filesystem
     *
     * @param  $path
     * @return mixed
     */
    protected function streamFile($path)
    {
        $filesystem = \App::make(DocumentStorageInterface::class)->filesystem();

        try {
            $stream = $filesystem->readStream($path);
        } catch (FileNotFoundException $e) {
            return $this->response()->error('File not found', 404);
        }

        return response()->stream(
            function () use ($stream) {
                fpassthru($stream);
            },
            Response::HTTP_OK,
            [
                "Content-Type" => $filesystem->getMimetype($path),
                "Content-Length" => $filesystem->getSize($path),
                "Content-disposition" => "inline; filename=\"" . basename($path) . "\"",
            ]
        );
    }
}
