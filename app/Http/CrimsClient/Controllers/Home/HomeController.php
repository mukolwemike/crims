<?php
/**
 * Date: 05/01/2016
 * Time: 8:13 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Http\CrimsClient\Controllers\Home;

use App\Cytonn\Mailers\Client\ContactUsMailer;
use App\Cytonn\Models\ClientUser;
use App\Exceptions\CrimsException;
use App\Http\CrimsClient\Controllers\Controller;
use function config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Http\CrimsClient\Controllers\Documents\DocumentController;
use Illuminate\Http\Response;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers\Home
 */
class HomeController extends Controller
{
    
    /**
     * HomeController constructor.
     *
     * @param DocumentController $doc
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('home.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contactUs(Request $request)
    {
        $rules = [
            'message'=>'required|min:10',
            'subject'=>'required'
        ];

        $this->validate($request, $rules);

        $user = $request->user();

        (new ContactUsMailer())->sendContactUs($user, $request->input('subject'), $request->input('message'));

        return $this->response()->json([
            'data' => [
                'message' => 'Message submitted successfully',
            ],
            'status' => \Symfony\Component\HttpFoundation\Response::HTTP_CREATED
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws CrimsException
     */
    public function postFeedback(Request $request)
    {
        $this->validate(
            $request,
            [
            'subject'=>'required|min:5|max:255',
            'description'=>'required|min:5',
            'file.*' => 'mimes:jpeg,bmp,png,pdf|nullable|max:5000'
            ]
        );

        $http = new Client(
            [
            'base_uri' => config('services.pm.url')
            ]
        );

        $user = $this->user();

        $parameters = [
            [ 'name' => 'subject',  'contents' =>$request->input('subject')],
            [ 'name' => 'description', 'contents' => $request->input('description')],
            [ 'name' => 'project_id', 'contents' => config('services.pm.project_id') ],
            [ 'name' => 'client_id', 'contents' => $user ? $user->id : null],
            [ 'name' => 'client_email', 'contents' => $user ? $user->email : null],
            [ 'name' => 'source', 'contents' => 'crims-client']
        ];

        $data['multipart'] = $parameters;

        if ($file = $request->file('file')) {
            $data['multipart'] = array_merge(
                $data['multipart'],
                [
                [
                    'name' => 'file',
                    'filename' => $file->getClientOriginalName(),
                    'Mime-Type'=> $file->getmimeType(),
                    'contents' => fopen($file->getPathname(), 'r')
                ]
                ]
            );
        }
    
        try {
            $http->post("/api/post/feedback", $data);
        } catch (RequestException $e) {
            throw new CrimsException('Could not send feedback', 500, $e);
        }
        
        return $this->response->created();
    }
}
