<?php

namespace App\Http\CrimsClient\Middleware;

use App\Cytonn\System\CrimsClientPageView;
use Closure;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class LogRequests
{
    protected $excludes = [
        '_debugbar'
    ];


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        if ($this->excluded($request)) {
            return;
        }

        $data = [
            'id' => Uuid::generate()->string,
            'user_id' => auth()->id(),
            'user_agent' => $request->server('HTTP_USER_AGENT'),
            'ip' => $request->ip(),
            'url' => $request->url()
        ];

        if (app()->environment() == 'production') {
            delaySave(CrimsClientPageView::class, $data);
        }
    }

    protected function excluded(Request $request)
    {
        return collect($this->excludes)->reduce(
            function ($carry, $item) use ($request) {
                return $carry || str_contains($request->url(), $item);
            },
            false
        );
    }
}
