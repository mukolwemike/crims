<?php namespace App\Http\CrimsClient\Middleware;

use Closure;

use Illuminate\Http\Request;

class ValidateAuthyRequest
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected function checkBool($value)
    {
        if (is_bool($value)) {
            $value = ($value) ? 'true' : 'false';
        } else {
            $value = (is_null($value)) ? '' : $value;
        }
        return $value;
    }

    protected function sortParams($params)
    {
        $new_params = array();
        foreach ($params as $k => $v) {
            if (is_array($v)) {
                ksort($v);
                $new_params[$k] = $v;
                foreach ($v as $k2 => $v2) {
                    if (is_array($v2)) {
                        ksort($v2);
                        $new_params[$k][$k2] = $v2;
                        foreach ($v2 as $k3 => $v3) {
                            $v3 = $this->checkBool($v3);
                            $new_params[$k][$k2][$k3] = $v3;
                        }
                    } else {
                        $v2 = $this->checkBool($v2);
                        $new_params[$k][$k2] = $v2;
                    }
                }
            } else {
                $v = $this->checkBool($v);
                $new_params[$k] = $v;
            }
        }
        ksort($new_params);
        return $new_params;
    }

    public function handle(Request $request, Closure $next)
    {
        return $next($request);
        
        $key = config('services.authy.key');
        $url = $request->url();
        $params = $request->all();
        $nonce = $request->header("X-Authy-Signature-Nonce");
        $theirs = $request->header('X-Authy-Signature');

        $sorted_params = $this->sortParams($params);
        $query = http_build_query($sorted_params);
        $message = $nonce . '|' . $request->method() . '|' . $url . '|' . $query;

        $s = hash_hmac('sha256', $message, $key, true);
        $mine = base64_encode($s);


        if ($theirs != $mine) {
            return response()->json(['data' => "Not a valid Authy request."]);
        } else {
            return $next($request);
        }
    }
}
