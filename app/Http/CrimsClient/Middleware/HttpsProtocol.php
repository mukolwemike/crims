<?php
/**
 * Date: 03/02/2016
 * Time: 1:27 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Http\Middleware;

use Closure;

class HttpsProtocol
{
    public function handle($request, Closure $next)
    {
        if (!$request->secure()) {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}
