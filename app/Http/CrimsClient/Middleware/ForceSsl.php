<?php
/**
 * Date: 08/05/2017
 * Time: 16:58
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ForceSsl
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('X_FORWARDED_FOR')) {
            $this->setTrustedProxies($request);
        } else {
            if (!$request->secure()) {
                return redirect()->secure($request->getRequestUri());
            }
        }

        return $next($request);
    }

    private function setTrustedProxies(Request $request)
    {
        $trustedProxy = config('trustedproxy.proxies');

        if (in_array('*', $trustedProxy) || in_array('**', $trustedProxy)) {
            Request::setTrustedProxies([$request->getClientIp()]);

            return;
        }

        Request::setTrustedProxies($trustedProxy);
    }
}
