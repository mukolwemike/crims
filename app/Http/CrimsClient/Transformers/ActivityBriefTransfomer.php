<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 3/8/18
 * Time: 11:57 AM
 */

namespace App\Http\CrimsClient\Transformers;

use App\Cytonn\Models\ClientInvestment;
use League\Fractal\TransformerAbstract;

class ActivityBriefTransfomer extends TransformerAbstract
{
    public function transform($activity)
    {
        $product = ($activity instanceof ClientInvestment) ? $activity->product : $activity->investment->product;

        return [
            'type' => $activity->transaction,
            'product' => $product->name,
            'date' => $activity->transaction_date->toDayDateTimeString(),
            'amount' => $activity->amount,
        ];
    }
}
