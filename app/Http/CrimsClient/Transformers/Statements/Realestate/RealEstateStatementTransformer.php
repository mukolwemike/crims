<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 12/02/2019
 * Time: 11:57
 */

namespace App\Http\CrimsClient\Transformers\Statements\Realestate;

use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class RealEstateStatementTransformer extends TransformerAbstract
{
    public function transform($data)
    {
        $clusters = $data['clusters']->map(function ($cluster) {
            return [
                'unit_size' => $cluster->size->name,
                'holding_count' => $cluster->holdings->count(),
                'unit_number' => $cluster->unitNumbers,
                'total_price' => AmountPresenter::currency($cluster->total_price),
                'interest_accrued' => AmountPresenter::currency($cluster->interest_accrued),
                'total_payments_received' => AmountPresenter::currency($cluster->total_payments_received),
                'payment_pending' => AmountPresenter::currency($cluster->payment_pending),
                'payment_due' => AmountPresenter::currency($cluster->payment_due),
                'next_payment_date' => $cluster->next_payment_date,
                'next_payment_amount' => AmountPresenter::currency($cluster->next_payment_amount),
                'schedule' => $cluster->holdings->map(function ($holding) {
                    return $holding->overdue_schedules->map(function ($schedule) {
                        return [
                            'principal' => AmountPresenter::currency($schedule->principal),
                            'rate' => \Cytonn\Realestate\Payments\Interest::INTEREST_RATE,
                            'due_date' => DatePresenter::formatDate($schedule->due_date),
                            'tenor' => $schedule->tenor,
                            'interest_accrued' => AmountPresenter::currency($schedule->interest_accrued),
                            'paid' => AmountPresenter::currency($schedule->paid),
                            'balance' => AmountPresenter::currency($schedule->balance)
                        ];
                    });
                })
            ];
        });

        $totals = $data['totals'];

        $totals->total_price = AmountPresenter::currency($totals->total_price);
        $totals->interest_accrued = AmountPresenter::currency($totals->interest_accrued);
        $totals->reservation_fees = AmountPresenter::currency($totals->reservation_fees);
        $totals->deposit = AmountPresenter::currency($totals->deposit);
        $totals->total_installments = AmountPresenter::currency($totals->total_installments);
        $totals->total_payments_received = AmountPresenter::currency($totals->total_payments_received);
        $totals->payment_pending = AmountPresenter::currency($totals->payment_pending);

        return [
            "units" => $clusters,
            "totals" => $totals,
            "has_next_payments" => $data['has_next_payments'],
            "has_payments_due" => $data['has_payments_due']
        ];
    }
}
