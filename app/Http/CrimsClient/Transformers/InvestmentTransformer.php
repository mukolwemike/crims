<?php

namespace App\Http\CrimsClient\Transformers;

use App\Cytonn\Investment\Reports\InvestmentReporting;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use Carbon\Carbon;
use Cytonn\Api\Transformers\ProductTransformer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class InvestmentTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'product'
    ];

    protected $defaultIncludes = [
        'product'
    ];

    protected $appends = [];

    public function append($keys)
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }

        foreach ($keys as $key) {
            array_push($this->appends, $key);
        }

        return $this;
    }


    public function transform(ClientInvestment $investment)
    {
        $calculator = $investment->calculate(Carbon::today(), true);

        return [
            'id' => $investment->uuid,
            'maturity_date' => $investment->maturity_date->toDateString(),
            'invested_date' => $investment->invested_date->toDateString(),
            'amount' => $investment->amount,
            'interest_rate' => $investment->interest_rate,
            'gross_interest' => $calculator->grossInterest(),
            'net_interest' => $calculator->netInterest(),
            'total_value' => $calculator->total(),
            'total_on_maturity' => $investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date),
            'withdrawal' => $calculator->withdrawals(Carbon::today()),
            'currency_code' => $investment->product->currency->code,
            'active' => !((bool)$investment->withdrawn),
            'activities' => $calculator->getPrepared()->actions,
            'instructions' => $this->instructions($investment),
            'can_withdraw' => $investment->repo->canWithdraw()
        ];
    }

    public function details(ClientInvestment $investment)
    {
        $calculator = $investment->calculate(Carbon::today(), true);

        return [
            'id' => $investment->uuid,
            'product' => $investment->product->name,
            'maturity_date' => Carbon::parse($investment->maturity_date)->toFormattedDateString(),
            'invested_date' => Carbon::parse($investment->invested_date)->toFormattedDateString(),
            'amount' => AmountPresenter::currency($investment->amount, true, 0),
            'interest_rate' => AmountPresenter::currency($investment->interest_rate),
            'gross_interest' => AmountPresenter::currency($calculator->grossInterest(), true, 0),
            'net_interest' => AmountPresenter::currency($calculator->netInterest(), true, 0),
            'total_value' => AmountPresenter::currency($calculator->total(), true, 0),
            'total_on_maturity' => AmountPresenter::currency(
                $investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date),
                true,
                0
            ),
            'withdrawal' => AmountPresenter::currency($calculator->withdrawals(Carbon::today())),
            'currency_code' => $investment->product->currency->code,
            'active' => $investment->withdrawn ? 'Withdrawn' : 'Active',
            'activities' => $calculator->getPrepared()->actions,
            'instructions' => $this->instructions($investment),
            'can_withdraw' => $investment->repo->canWithdraw()
        ];
    }

    public function canWithdraw(ClientInvestment $investment)
    {
        if ($investment->withdrawn) {
            return ['status' => false, 'reason' => 'Investment Withdrawn'];
        }

        $rollover = $investment->instructions()->ofType('rollover')
            ->where('inactive', 0)->count();

        if ($rollover) {
            return
                [
                    'status' => false,
                    'reason' => 'Rollover Scheduled'
                ];
        }

        $interest = $investment->repo->netInterestAfterWithdrawals(Carbon::today(), true);

        $withdrawal = $investment->withdrawals()->latest('created_at')->first();

        $i = $investment->instructions()->ofType('withdraw')
            ->where(
                function ($q) use ($interest) {
                    $q->where('amount_select', '!=', 'interest')
                        ->orWhere(
                            function ($q) use ($interest) {
                                $q->whereNotNull('amount')->where('amount', '<=', $interest);
                            }
                        );
                }
            )->where('inactive', 0);

        if ($withdrawal) {
            $i = $i->where('created_at', '>', $withdrawal->created_at);
        }

        if ($i->count() > 0) {
            return [
                'status' => false,
                'reason' => 'Withdrawal scheduled',
            ];
        }

        $date = Carbon::today();

        $maturity_date = Carbon::parse($investment->maturity_date);

        if ($date->diffInMonths($maturity_date) > 1) {
            return [
                'status' => false,
                'reason' => 'Premature withdrawal is not allowed',
            ];
        }

        return [
            'status' => true,
            'reason' => 'No instruction given',
        ];
    }

    public function instructions(ClientInvestment $investment)
    {
        if (!in_array('instructions', $this->appends)) {
            return null;
        }

        return $investment->instructions()
            ->latest()
            ->take(10)
            ->get()
            ->map(
                function (ClientInvestmentInstruction $instr) use ($investment) {
                    $type = $instr->type->name;

                    return [
                        'type' => ucfirst($type),
                        'sent' => $instr->created_at->toIso8601String(),
                        'amount' => $instr->amount_select == 'amount' ? AmountPresenter::currency($instr->amount)
                            : ucfirst(str_replace('_', ' + ', $instr->amount_select)),
                        'date' => $type == 'rollover' || $instr->withdrawal_stage == 'mature' ? 'On Maturity'
                            : DatePresenter::formatDate($instr->due_date),
                        'status' => $instr->inactive ? 'Cancelled' : ($instr->executed() ? 'Completed' : 'Pending')
                    ];
                }
            );
    }

    public function includeProduct(ClientInvestment $investment)
    {
        $product = $investment->product;

        return $this->item($product, new ProductTransformer());
    }

    protected function activities(ClientInvestment $investment)
    {
        if (!in_array('activities', $this->appends)) {
            return null;
        }

        $data = (new InvestmentReporting())->processInvestment($investment, Carbon::today());

        return $data->statement;
    }
}
