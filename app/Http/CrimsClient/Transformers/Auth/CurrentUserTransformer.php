<?php

namespace App\Http\CrimsClient\Transformers\Auth;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientUser as User;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class CurrentUserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->uuid,
            'username' => $user->username,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'email' => $user->email,
            'phone' => ltrim($user->phone_country_code . $user->phone, "+"),
            'created_at_unix' => (new Carbon($user->created_at))->timestamp,
            'access' => $this->getAccessClients($user),
            'fas' => $this->getFas($user)
        ];
    }

    private function getAccessClients($user)
    {
        $access = $user->access()->where('active', 1)->pluck('client_id')->all();

        $clients = [];

        foreach ($access as $client_id) {
            $client = Client::findOrFail($client_id);

            $hasFund = $this->hasFund($client);
            $hasUnits = $this->hasMMFUnits($client);

            $c = new \stdClass();
            $c->id = $client->uuid;
            $c->clientType = $client->clientType->name;
            $c->email = $client->contact->email;
            $c->phone = $client->contact->phone;
            $c->id_or_passport = $client->id_or_passport;
            $c->residence = $client->residence;
            $c->tax_pin = $client->pin_no;
            $c->dob = $client->dob;
            $c->present_occupation = $client->present_occupation;
            $c->employer_name = $client->employer_name;
            $c->employer_address = $client->employer_address;
            $c->business_sector = $client->business_sector;
            $c->fullname = $client->name();
            $c->shortname = str_limit($client->jointShortName(), 40) . ' - ' . $client->client_code;
            $c->client_code = $client->client_code;
            $c->name = $this->accountName($client);
            $c->postal_address = $client->postal_address;
            $c->employment = $client->employment ? $client->employment->name : null;
            $c->employment_id = $client->employment ? $client->employment->id : null;
            $c->contactPersons = $client->contactPersons;
            $c->hasSP = $client->investments()->active()->count() ? true : false;
            $c->hasUT = $hasFund;
            $c->hasRE = count($client->repo->clientReInvestments()['realEstateDetails']) ? true : false;
            $c->bank_details = $this->accountDetails($client);
            $c->kycValidated = $client->repo->checkClientKycValidated();
            $c->joint = count($client->jointHolders) ? true : false;
            $c->jointHolders = $client->jointDetail;
            $c->hasMMFBalance = $hasUnits;
            $c->canRedeem = $this->canRedeemPoints($client);

            array_push($clients, $c);
        }

        return $clients;
    }

    private function hasFund(Client $client)
    {
        return $client->unitFundPurchases()
            ->whereHas('unitFund', function ($fund) {
                $fund->ofType('cis')->active();
            })->exists();
    }

    private function hasMMFUnits(Client $client)
    {
        if (!$this->hasFund($client)) {
            return false;
        }

        $fund = UnitFund::where('short_name', 'CMMF')->first();

        $first_purchase = $client->unitFundPurchases()
            ->forUnitFund($fund)
            ->orderBy('date', 'asc')
            ->first();

        $firstPurchaseDate = $first_purchase ? Carbon::parse($first_purchase->date) : Carbon::now();
        $today = Carbon::today();

        $calculator = $client->calculateFund($fund, $today, $firstPurchaseDate, false);
        $calculate = $calculator->getPrepared();

        return $calculate->total > 0;
    }


    private function canRedeemPoints(Client $client)
    {
//        $hasAbove2000Points = $client->calculateLoyalty()->getAvailablePoints() > 2000;

        return false;
    }

    public function getFas(User $user)
    {
        return $user->financialAdvisors
            ->map(
                function ($fa) {
                    return (object)['id' => $fa->uuid, 'name' => $fa->name];
                }
            )->all();
    }

    public function accountName(Client $client)
    {
        $name = $client->name() . ' - ' . $client->client_code;

        if ($acc = $client->account_name) {
            return $name . " ($acc)";
        }

        return $name;
    }

    public function accountDetails(Client $client)
    {
        return ClientBankAccount::active()->where('client_id', $client->id)
            ->get()
            ->map(function ($acc) {

                $bank = ($acc->branch) ? $acc->branch->bank : null;

                return [
                    'id' => $acc->id,
                    'bank' => $bank ? $bank->name : $acc->bank_name,
                    'branch' => $acc->branch ? $acc->branch->name : $acc->branch_name,
                    'account_name' => $acc->account_name,
                    'account_number' => $acc->account_number,
                    'currency' => $acc->currency ? $acc->currency->code : null,
                    'country' => $acc->country ? $acc->country->name : null
                ];
            });
    }
}
