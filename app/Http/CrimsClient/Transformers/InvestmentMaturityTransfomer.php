<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 3/8/18
 * Time: 11:57 AM
 */

namespace App\Http\CrimsClient\Transformers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class InvestmentMaturityTransfomer extends TransformerAbstract
{
    public function transform(ClientInvestment $investment)
    {
        $days_to_maturity = Carbon::now()->diffInDays(Carbon::parse($investment->maturity_date));

        return [
            'id' => $investment->uuid,
            'product' => $investment->product->name,
            'shortname' => $investment->product->shortname,
            'longname' => $investment->product->longname,
            'currency' => $investment->product->currency->code,
            'date' => DatePresenter::formatDate($investment->maturity_date),
            'amount' => $investment->amount,
            'days_to_maturity' => $days_to_maturity
        ];
    }
}
