<?php

/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 6/20/17
 * Time: 1:47 PM
 */

namespace App\Http\CrimsClient\Transformers\Documents;

use App\Cytonn\Models\Document;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class DocumentTransformer extends TransformerAbstract
{
    
    /**
     * @param Document $document
     * @return array
     */
    public function transform(Document $document)
    {
        return [
            'id' => $document->id,
            'title' => (!is_null($document->title)) ? $document->title : $document->type->name,
            'type' => $document->type->name,
            'uploaded_on' => DatePresenter::formatDate($document->created_at),
            'filename' => $document->filename
        ];
    }
}
