<?php

namespace App\Http\CrimsClient\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Payments;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Crims\Investments\Commission\RecipientCalculator as InvRecipientCalculator;
use Crims\Realestate\Commission\RecipientCalculator as CRERecipientCalculator;
use Crims\Shares\Commission\RecipientCalculator as OTCRecipientCalculator;
use Crims\Unitization\Commission\RecipientCalculator as UTFRecipientCalculator;
use Illuminate\Support\Arr;
use League\Fractal\TransformerAbstract;

class ClientTransformer extends TransformerAbstract
{
    protected $recipient;

    protected $start;

    protected $end;

    protected $currency;

    public $schedules = false;

    public function __construct(CommissionRecepient $recipient, Carbon $start, Carbon $end, Currency $currency = null)
    {
        $this->recipient = $recipient;

        $this->start = $start->copy();

        $this->end = $end->copy();

        $this->currency = $currency ? $currency : Currency::where('code', 'KES')->first();
    }

    public function transform(Client $client)
    {
        $products = collect([
            $this->investments($client),
            $this->realEstate($client),
            $this->unitTrust($client),
            $this->OTCShares($client)
        ]);

        $phone = Arr::first($client->getContactPhoneNumbersArray());

        return [
            'id' => $client->uuid,
            'code' => $client->client_code,
            'name' => $client->name(),
            'email' => $client->contact->email,
            'since' => $this->clientSince($client),
            'phone' => $phone,
            'total' => $products->sum('total'),
            'products' => $products->all(),
            'currency' => $this->currency->code,
            'active' => $this->active($client),
        ];
    }

    /**
     * @param $client
     * @return bool
     */
    protected function active(Client $client)
    {
        $inv = ClientInvestment::where('client_id', $client->id)->withdrawn(false)->count() > 0;

        $funds = $client->unitFundPurchases()->whereHas('unitFund', function ($fund) {
                $fund->ofType('cis')->active();
        })->count() > 0;

        $re = UnitHolding::where('client_id', $client->id)->where('active', 1)->count() > 0;

        return $inv || $re || $funds;
    }

    protected function investments($client)
    {
        $cms_calc = new InvRecipientCalculator(
            $this->recipient,
            $this->currency,
            $this->start->copy(),
            $this->end->copy(),
            $client
        );

        $out = [
            'name' => 'Investments',
            'total' => $cms_calc->summary()->earned() - $cms_calc->summary()->getClawBacks(),
            'short_name' => 'SP'
        ];

        if ($this->schedules) {
            $out['schedules'] = $cms_calc->compliantSchedulesQuery()
                ->get()
                ->map(
                    function ($sch) {
                        $investment = $sch->commission->investment;
                        return (object)[
                            'description' => $sch->description,
                            'principal' => $investment->amount,
                            'invested_date' => $investment->invested_date->toDateString(),
                            'maturity_date' => $investment->maturity_date->toDateString(),
                            'rate' => $sch->commission->rate,
                            'amount' => $sch->amount,
                        ];
                    }
                );

            $out['claw_backs'] = $cms_calc->clawBackQuery()
                ->get()
                ->map(
                    function ($claw) {
                        $investment = $claw->commission->investment;
                        return (object)[
                            'description' => $claw->narration,
                            'principal' => $investment->amount,
                            'invested_date' => $investment->invested_date->toDateString(),
                            'maturity_date' => $investment->maturity_date->toDateString(),
                            'amount' => $claw->amount
                        ];
                    }
                );
        }

        return (object)$out;
    }

    protected function realEstate($client)
    {
        $re_calc = (new CRERecipientCalculator($this->recipient, $this->start->copy(), $this->end->copy(), $client));

        $out = [
            'name' => 'Real Estate',
            'total' => $re_calc->summary()->total(),
            'short_name' => 'RE'
        ];

        if ($this->schedules) {
            $out['schedules'] = $re_calc->schedules()
                ->map(
                    function ($sch) {
                        $holding = $sch->commission->holding;

                        return (object)[
                            'project' => $holding->project->name,
                            'unit' => $holding->unit->number,
                            'description' => $sch->description,
                            'date' => $sch->date,
                            'amount' => $sch->amount
                        ];
                    }
                );
        }

        return (object)$out;
    }

    protected function unitTrust(Client $client)
    {
        $calculator = new UTFRecipientCalculator(
            $this->recipient,
            $this->start->copy(),
            $this->end->copy(),
            $client,
            $this->currency
        );

        $out = [
            'name' => 'Unit Trust',
            'total' => (float)$calculator->summary()->total(),
            'short_name' => 'UTF'
        ];

        if ($this->schedules) {
            $out['schedules'] = $calculator->schedules()
                ->get()
                ->map(
                    function ($sch) {

                        $purchase = $sch->purchase;

                        return (object)[
                            'fund' => $purchase->unitFund->name,
                            'description' => $sch->description,
                            'date' => $sch->date,
                            'amount' => $sch->amount
                        ];
                    }
                );
        }

        return (object)$out;
    }

    protected function OTCShares(Client $client)
    {
        $calculator = new OTCRecipientCalculator(
            $this->recipient,
            $this->start->copy(),
            $this->end->copy(),
            $client,
            $this->currency
        );

        $out = [
            'name' => 'OTC Shares',
            'total' => (float)$calculator->summary()->total(),
            'short_name' => 'OTC'
        ];

        if ($this->schedules) {
            $out['schedules'] = $calculator->schedules()
                ->get()
                ->map(
                    function ($sch) {

                        $purchase = $sch->sharePurchase;

                        return (object)[
                            'entity' => $purchase->shareHolder->entity->name,
                            'description' => $sch->description,
                            'date' => $sch->date,
                            'amount' => $sch->amount
                        ];
                    }
                );
        }

        return (object)$out;
    }

    private function clientSince(Client $client)
    {
        $inv = $client->investments()->oldest('invested_date')->first();

        $holdings = $client->unitHoldings;

        $re = (new Payments())->whereIn('holding_id', $holdings->lists('id'))->oldest('date')->first();

        $fund = $client->unitFundPurchases()->oldest('date')->first();

        if ($inv && $re) {
            if ($re->date->lt($inv->invested_date)) {
                return $re->date->toDateString();
            }

            return $inv->date ? $inv->date->toDateString() : '';
        }

        if ($inv) {
            return $inv->invested_date->toDateString();
        }

        if ($re) {
            return $re->date->toDateString();
        }

        if ($fund) {
            return $fund->date->toDateString();
        }

        return null;
    }

    public static function transformHolder($client)
    {
        return [
            'code' => $client->client_code,
            'firstname' => $client->contact->firstname,
            'lastname' => $client->contact->lastname,
            'email' => $client->contact->email,
            'pin_no' => ($client->client_type_id == 1) ? $client->pin_no : '',
            'telephone_cell' => $client->contact->phone,
            'id_or_passport' => $client->contact->id_or_passport,
            'client_id' => $client->id,
            'title_id' => $client->contact->title_id,
            'holder_type' => 2
        ];
    }
}
