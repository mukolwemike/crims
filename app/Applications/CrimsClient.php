<?php
/**
 * Date: 28/11/2017
 * Time: 12:12
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Applications;

use Illuminate\Foundation\Application;

class CrimsClient extends Application
{
    /**
     * Get the path to the application configuration files.
     *
     * @param  string $path Optionally, a path to append to the config path
     * @return string
     */
    public function configPath($path = '')
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR
            .'client'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Get the path to the public / web directory.
     *
     * @return string
     */
    public function publicPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'client';
    }

    /**
     * Get the path to the resources directory.
     *
     * @param  string $path
     * @return string
     */
    public function resourcePath($path = '')
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR
            .'client'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}
