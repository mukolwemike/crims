<?php
/**
 * Date: 05/02/2017
 * Time: 16:37
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Exceptions;

class GeneralException extends \Exception
{
}
