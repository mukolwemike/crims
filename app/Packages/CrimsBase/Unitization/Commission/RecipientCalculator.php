<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Unitization\Commission;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Crims\Base\ModelMapper;
use Crims\Investments\Commission\Models\Recipient;
use Crims\Unitization\Commission\Models\UnitFundCommissionSchedule;

class RecipientCalculator
{
    /**
     * @var $recipient
     */
    protected $recipient;

    /**
     * @var Carbon
     */
    private $end;

    /**
     * @var Carbon
     */
    private $start;

    /**
     * @var
     */
    public $schedulesFilter;

    /**
     * @var Client|null
     */
    private $client;

    /**
     * @var
     */
    private $currency;

    /**
     * RecipientCalculator constructor.
     * @param Recipient $recipient
     * @param Carbon $start
     * @param Carbon $end
     * @param Client|null $client
     * @param Currency|null $currency
     */
    public function __construct(
        Recipient $recipient,
        Carbon $start,
        Carbon $end,
        Client $client = null,
        Currency $currency = null
    ) {
        $this->recipient = $recipient;

        $this->start = $start;

        $this->end = $end;

        $this->client = $client;

        $this->currency = $currency;
    }

    /**
     * @return Summary
     */
    public function summary()
    {
        return new Summary(
            $this->recipient,
            $this->start,
            $this->end,
            $this->schedules()->sum('amount'),
            $this->client,
            $this->currency
        );
    }

    /**
     * @return mixed
     */
    public function schedules()
    {
        $schedules = ModelMapper::get(UnitFundCommissionSchedule::class);

        $q = $schedules::whereHas('unitFundCommission', function ($q) {

            $q->where('commission_recipient_id', $this->recipient->id);

            if ($this->schedulesFilter) {
                $q->where($this->schedulesFilter);
            }

            if ($this->client) {
                $q->whereHas('purchase', function ($purchase) {
                    $purchase->where('client_id', $this->client->id);
                });
            }

            if ($this->currency) {
                $q->whereHas('purchase', function ($purchase) {
                    $purchase->whereHas('unitFund', function ($fund) {
                        $fund->where('currency_id', $this->currency->id);
                    });
                });
            }
        })->where('date', '>=', $this->start)->where('date', '<=', $this->end);

        return $q;
    }

    /**
     * @param Carbon|null $purchasesFrom
     * @param Carbon|null $purchasesTo
     * @return $this
     */
    public function setOverrideDates(Carbon $purchasesFrom = null, Carbon $purchasesTo = null)
    {
        if ($purchasesFrom || $purchasesTo) {
            $q = function ($schedule) use ($purchasesFrom, $purchasesTo) {
                $schedule->whereHas('purchase', function ($purchase) use ($purchasesFrom, $purchasesTo) {
                    if ($purchasesFrom) {
                        $purchase->where('date', '>=', $purchasesFrom);
                    }

                    if ($purchasesTo) {
                        $purchase->where('date', '<=', $purchasesTo);
                    }

                    if ($this->client) {
                        $purchase->where('client_id', $this->client->id);
                    }
                });
            };

            $this->setSchedulesFilter($q);
        }

        return $this;
    }

    /**
     * @param \Closure $closure
     * @return $this
     */
    public function setSchedulesFilter(\Closure $closure)
    {
        $this->schedulesFilter = $closure;

        return $this;
    }

    public function getReports()
    {
        return CommissionRecepient::whereHas('unitFundCommissions', function ($q) {
            $q->whereHas('overrideSchedules', function ($q) {
                $q->forRecipient($this->recipient)->between($this->start, $this->end);
            });
        })->orderBy('name')->get();
    }
}
