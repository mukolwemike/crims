<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Unitization\Commission;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Unitization\UnitFundCommissionOverride;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use Carbon\Carbon;
use Crims\Commission\Override;
use Crims\Commission\SummaryContract;
use Crims\Investments\Commission\Models\Recipient;

class Summary implements SummaryContract
{
    /**
     * @var
     */
    protected $total;

    /**
     * @var Recipient
     */
    protected $recipient;

    /**
     * @var Carbon
     */
    protected $start;

    /**
     * @var Carbon
     */
    protected $end;

    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $currency;


    /**
     *
     */
    const COMMISSION_TYPE = 'unitization';

    /**
     * Summary constructor.
     * @param Recipient $recipient
     * @param Carbon $start
     * @param Carbon $end
     * @param $total
     * @param Client|null $client
     * @param Currency|null $currency
     */
    public function __construct(
        Recipient $recipient,
        Carbon $start,
        Carbon $end,
        $total,
        Client $client = null,
        Currency $currency = null
    ) {
        $this->recipient = $recipient;

        $this->start = $start;

        $this->end = $end;

        $this->total = $total;

        $this->client = $client;

        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function total()
    {
        return $this->total;
    }

    /**
     *
     */
    public function finalCommission()
    {
        return $this->total + $this->override();
    }

    public function override()
    {
        return UnitFundCommissionOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->sum('amount');
    }

    public function overrideOnRecipient(CommissionRecepient $recepient)
    {
        return UnitFundCommissionOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->sum('amount');
    }

    public function overrideCommission(CommissionRecepient $recepient)
    {
        return UnitFundCommissionOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->sum('schedule_amount');
    }
}
