<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Shares\Commission;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Crims\Base\ModelMapper;
use Crims\Investments\Commission\Models\Recipient;
use Crims\Shares\Commission\Models\ShareCommissionPaymentSchedule;

/**
 * Class RecipientCalculator
 * @package Crims\Shares\Commission
 */
class RecipientCalculator
{
    /**
     * @var $recipient
     */
    protected $recipient;

    /**
     * @var Carbon
     */
    private $end;

    /**
     * @var Carbon
     */
    private $start;

    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $currency;

    /**
     * @var
     */
    public $schedulesFilter;

    /**
     * RecipientCalculator constructor.
     * @param Recipient $recipient
     * @param Carbon $start
     * @param Carbon $end
     * @param Client|null $client
     * @param Currency $currency
     */
    public function __construct(
        Recipient $recipient,
        Carbon $start,
        Carbon $end,
        Client $client = null,
        Currency $currency = null
    ) {
        $this->recipient = $recipient;

        $this->start = $start;

        $this->end = $end;

        $this->client = $client;

        $this->currency = $currency;
    }

    /**
     * @return Summary
     */
    public function summary()
    {
        return new Summary(
            $this->recipient,
            $this->start,
            $this->end,
            $this->schedules()->sum('amount'),
            $this->client,
            $this->currency
        );
    }

    /**
     * @return mixed
     */
    public function schedules()
    {
        $schedules = ModelMapper::get(ShareCommissionPaymentSchedule::class);

        $q = $schedules::where('date', '>=', $this->start)
            ->where('date', '<=', $this->end)
            ->where('commission_recipient_id', $this->recipient->id);

        if ($this->client) {
            $q = $q->whereHas('sharePurchase', function ($purchase) {
                $purchase->whereHas('shareHolder', function ($holder) {
                    $holder->where('client_id', $this->client->id);
                });
            });
        }

        if ($this->currency) {
            $q = $q->forCurrency($this->currency);
        }

        if ($filter = $this->schedulesFilter) {
            $q = $q->where($filter);
        }

        return $q;
    }

    /**
     * @param Carbon|null $orderFrom
     * @param Carbon|null $orderTo
     * @return $this
     */
    public function setOverrideDates(Carbon $purchasesFrom = null, Carbon $purchasesTo = null)
    {
        if ($purchasesFrom || $purchasesTo) {
            $q = function ($schedule) use ($purchasesFrom, $purchasesTo) {
                $schedule->whereHas('sharePurchase', function ($purchase) use ($purchasesFrom, $purchasesTo) {
                    if ($purchasesFrom) {
                        $purchase->where('date', '>=', $purchasesFrom);
                    }

                    if ($purchasesTo) {
                        $purchase->where('date', '<=', $purchasesTo);
                    }
                });
            };

            $this->setSchedulesFilter($q);
        }

        return $this;
    }

    /**
     * @param \Closure $closure
     * @return $this
     */
    public function setSchedulesFilter(\Closure $closure)
    {
        $this->schedulesFilter = $closure;

        return $this;
    }

    public function getReports()
    {
        return CommissionRecepient::whereHas('shareCommissionPaymentSchedules', function ($q) {
            $q->whereHas('overrideSchedules', function ($q) {
                $q->forRecipient($this->recipient)->between($this->start, $this->end);
            });
        })->orderBy('name')->get();
    }
}
