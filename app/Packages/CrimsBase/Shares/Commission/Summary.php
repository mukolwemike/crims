<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Shares\Commission;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Shares\ShareCommissionPaymentOverride;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use Carbon\Carbon;
use Crims\Commission\Override;
use Crims\Commission\SummaryContract;
use Crims\Investments\Commission\Models\Recipient;

class Summary implements SummaryContract
{
    /**
     * @var
     */
    protected $total;

    /**
     * @var Recipient
     */
    protected $recipient;

    /**
     * @var Carbon
     */
    protected $start;

    /**
     * @var Carbon
     */
    protected $end;

    protected $client;

    protected $currency;

    /**
     * @var null
     */
    protected $override = null;

    /**
     *
     */
    const COMMISSION_TYPE = 'shares';

    /**
     * Summary constructor.
     * @param Recipient $recipient
     * @param Carbon $start
     * @param Carbon $end
     * @param $total
     * @param Client|null $client
     * @param Currency $currency
     */
    public function __construct(
        Recipient $recipient,
        Carbon $start,
        Carbon $end,
        $total,
        Client $client = null,
        Currency $currency = null
    ) {
        $this->total = $total;

        $this->recipient = $recipient;

        $this->start = $start;

        $this->end = $end;

        $this->client = $client;

        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function total()
    {
        return $this->total;
    }

    /**
     *
     */
    public function finalCommission()
    {
        return $this->total + $this->override();
    }

    public function override()
    {
        return ShareCommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->sum('amount');
    }

    public function overrideOnRecipient(CommissionRecepient $recepient)
    {
        return ShareCommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->sum('amount');
    }

    public function overrideCommission(CommissionRecepient $recepient)
    {
        return ShareCommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->sum('schedule_amount');
    }
}
