<?php
/**
 * Date: 05/02/2017
 * Time: 16:58
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Investments\Calculator;

use Crims\Investments\Structured\Models\Investment;

class Calculate
{
    protected $investment;

    /**
     * Calculate constructor.
     *
     * @param Investment $investment
     */
    public function __construct(Investment $investment)
    {
        $this->investment = $investment;
    }

    public function grossInterest($date = null)
    {
    }

    public function netInterest($date = null, $includeWithdrawals = true)
    {
    }

    public function withHoldingTax()
    {
    }

    public function withdrawals()
    {
        //withdrawal + deductions + payments
    }
}
