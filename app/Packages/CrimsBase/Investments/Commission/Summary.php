<?php
/**
 * Date: 27/10/2016
 * Time: 11:46
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Crims\Investments\Commission;

use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use Carbon\Carbon;
use Crims\Commission\Override;
use Crims\Commission\SummaryContract;
use Crims\Investments\Commission\Models\Recipient;
use Illuminate\Support\Collection;

/**
 * Class Summary
 *
 * @package Cytonn\Investment\Commission
 */
class Summary implements SummaryContract
{
    /**
     * @var
     */
    protected $clawBacks;

    /**
     * @var
     */
    protected $compliant;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var Carbon
     */
    protected $start;

    protected $end;

    /**
     * @var Recipient
     */
    private $recipient;

    /**
     * @var
     */
    protected $pay;

    /**
     * @var
     */
    protected $paid;

    /**
     * @var
     */
    protected $earned;

    /**
     * @var
     */
    protected $override = null;

    /**
     * @var
     */
    protected $total;

    const COMMISSION_TYPE = 'investments';


    /**
     * Summary constructor.
     *
     * @param Recipient      $recipient
     * @param Currency       $currency
     * @param Carbon         $start
     * @param Carbon         $end
     * @param $clawBacks
     * @param $compliant
     * @param $nonCompliant
     * @param $broughtForward
     */
    public function __construct(
        Recipient $recipient,
        Currency $currency,
        Carbon $start,
        Carbon $end,
        $clawBacks,
        $compliant
    ) {
        $this->clawBacks = $clawBacks;
        $this->compliant = $compliant;
        $this->recipient = $recipient;
        $this->start = $start;
        $this->end = $end;
        $this->currency = $currency;

        $this->calculate();
    }

    protected function calculate()
    {
        $this->earned = $this->compliant;

        $this->total = $this->earned - $this->clawBacks;
    }

    /**
     * @return Collection
     */
    public function getClawBacks()
    {
        return $this->clawBacks;
    }

    /**
     * @return Collection
     */
    public function getCompliant()
    {
        return $this->compliant;
    }

    public function earned()
    {
        return $this->earned;
    }

    public function total()
    {
        return $this->total;
    }

    /**
     * Final commission for period after doing adjustments
     *
     * @return int|mixed
     */
    public function finalCommission()
    {
        return $this->total() + $this->override();
    }

    /*
     * OVERRIDES
     */
    public function override()
    {
        if (is_null($this->override)) {
            return $this->override = CommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
                ->whereHas('commission', function ($q) {
                    $q->currency($this->currency);
                })->sum('amount');
        }

        return $this->override;
    }

    public function overrideOnRecipient(CommissionRecepient $recepient)
    {
        return CommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->whereHas('commission', function ($q) {
                $q->currency($this->currency);
            })->sum('amount');
    }

    public function overrideCommission(CommissionRecepient $recepient)
    {
        return CommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->whereHas('commission', function ($q) {
                $q->currency($this->currency);
            })->sum('schedule_amount');
    }
}
