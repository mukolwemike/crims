<?php
/**
 * Date: 27/10/2016
 * Time: 10:46
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Crims\Investments\Commission;

use Carbon\Carbon;
use Crims\Base\Models\Currency;

/**
 * Class Calculator
 *
 * @package Cytonn\Investment\Commission
 */
class Calculator extends Base
{
    /**
     * @var Carbon
     */
    protected $start;

    /**
     * @var Carbon
     */
    protected $end;

    /**
     * @var Currency
     */
    protected $currency;


    /**
     * Calculator constructor.
     *
     * @param Currency $currency
     * @param Carbon   $start
     * @param Carbon   $end
     */
    public function __construct(Currency $currency, Carbon $start, Carbon $end)
    {
        $this->start = $start;

        $this->end = $end;

        $this->currency = $currency;
    }

    /**
     * @return Carbon
     */
    protected function cutOffDate()
    {
        return $this->end;
    }

    /**
     * @return Carbon
     */
    protected function lastMonthCutOff()
    {
        return $this->start;
    }
}
