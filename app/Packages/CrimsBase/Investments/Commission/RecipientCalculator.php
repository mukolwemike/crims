<?php
/**
 * Date: 27/10/2016
 * Time: 10:36
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Crims\Investments\Commission;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Crims\Base\Clients\Client;
use Crims\Base\ModelMapper;
use Crims\Investments\Commission\Models\Recipient;
use Crims\Base\Models\Currency;
use Crims\Investments\Structured\Models\Investment;
use Crims\Investments\Commission\Models\CommissionPaymentSchedule;
use Crims\Investments\Commission\Models\CommissionClawback;
use Cytonn\Models\Investment\AdvanceInvestmentCommission;
use Cytonn\Models\Investment\AdvanceInvestmentCommissionRepayment;
use Cytonn\Models\RealEstate\AdvanceRealEstateCommissionRepayments;

/**
 * Class RecipientCalculator
 *
 * @package Cytonn\Investment\Commission
 */
class RecipientCalculator extends Calculator
{

    /**
     * @var Recipient
     */
    protected $recipient;

    /**
     * @var Currency
     */
    protected $currency;

    protected $client;

    protected $product;

    protected $investmentType;

    protected $investmentFilter;

    protected $compliantAmount = null;

    protected $clawbackAmount = null;

    protected $ignoreInvestmentFilter = false;

    protected $clawbacks = null;

    //cache schedules and clawbacks to avoid repeated queries
    private $schedules;
    private $clawbackRecords;
    private $advanceCommissions;
    private $unpaidAdvanceCommissions;
    private $summary;
    private $advanceRepayment = 0;

    /**
     * RecipientCalculator constructor.
     *
     * @param Recipient $recipient
     * @param Currency  $currency
     * @param Carbon    $start
     * @param Carbon    $end
     * @param Client    $client
     */
    public function __construct(
        Recipient $recipient,
        Currency $currency,
        Carbon $start,
        Carbon $end,
        \App\Cytonn\Models\Client $client = null
    ) {
        parent::__construct($currency, $start, $end);

        $this->recipient = $recipient;
        $this->currency = $currency;
        $this->client = $client;
    }

    public function setInvestmentFilter(\Closure $closure)
    {
        $this->investmentFilter = $closure;

        return $this;
    }

    public function setOverrideDates(Carbon $investmentsFrom = null, Carbon $investmentsEnd = null)
    {
        if ($investmentsFrom || $investmentsEnd) {
            $this->setInvestmentFilter(
                function ($query) use ($investmentsFrom, $investmentsEnd) {
                    if ($investmentsFrom) {
                        $query->where('invested_date', '>=', $investmentsFrom);
                    }

                    if ($investmentsEnd) {
                        $query->where('invested_date', '<=', $investmentsEnd);
                    }
                }
            );
        }

        return $this;
    }

    /**
     * @return Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @return Summary
     */
    public function summary($fresh = false)
    {
        if (!$this->summary || $fresh) {
            $claw_back = $this->clawbacks()->sum('amount');

            $compliant = $this->schedules()->sum('amount');

            return $this->summary = new Summary(
                $this->recipient,
                $this->currency,
                $this->start,
                $this->end,
                $claw_back,
                $compliant
            );
        }

        return $this->summary;
    }

    public function filterForProduct($product)
    {
        $this->product = $product;
    }

    public function filterForInvestmentType($type)
    {
        $this->investmentType = $type;
    }

    /**
     * @return mixed
     */
    public function compliantSchedulesQuery()
    {
        $schedule = ModelMapper::get(CommissionPaymentSchedule::class);

        return $schedule::whereHas(
            'commission',
            function ($commission) {
                $commission = $commission
                    ->currency($this->currency)
                    ->recipient($this->recipient)
                    ->whereHas(
                        'investment',
                        function ($investment) {

                            if ($this->investmentType) {
                                $investment->where('investment_type_id', $this->investmentType->id);
                            }

                            $investment->where('invested_date', '<=', $this->cutOffDate());

                            if ($this->investmentFilter && ! $this->ignoreInvestmentFilter) {
                                $investment->where($this->investmentFilter);
                            }
                        }
                    );

                if ($this->client) {
                    $commission = $commission->fromClient($this->client);
                }

                if ($this->product) {
                    $commission->forProduct($this->product);
                }
            }
        )->clawBack(false)->orderBy('date')->between($this->start, $this->end);
    }

    public function schedules()
    {
        if (!$this->schedules) {
            return $this->schedules = $this->compliantSchedulesQuery()->get();
        }

        return $this->schedules;
    }

    public function forInvestmentQuery(Investment $investment)
    {
        $schedule = ModelMapper::get(CommissionPaymentSchedule::class);

        return $schedule::whereHas(
            'commission',
            function ($commission) use ($investment) {
                $commission
                    ->currency($this->currency)
                    ->recipient($this->recipient)
                    ->writtenOff(false)
                    ->where('investment_id', $investment->id);
            }
        )->clawBack(false)->between($this->start, $this->end)->orderBy('date');
    }

    /**
     * Query that gets the clawbacks in a certain month
     *
     * @return mixed
     */
    public function clawBackQuery()
    {
        $model = ModelMapper::get(CommissionClawback::class);

        return $model::whereHas(
            'commission',
            function ($commission) {
                $commission
                    ->currency($this->currency)
                    ->recipient($this->recipient)
                    ->writtenOff(false)
                    ->has('investment');

                if ($this->investmentType) {
                    $commission->whereHas('investment', function ($investment) {
                        $investment->where('investment_type_id', $this->investmentType->id);
                    });
                }

                if ($this->investmentFilter) {
                    $commission->whereHas('investment', function ($investment) {
                        $investment->where($this->investmentFilter);
                    });
                }

                if ($this->client) {
                    $commission->fromClient($this->client);
                }

                if ($this->product) {
                    $commission->forProduct($this->product);
                }
            }
        )->betweenDates($this->lastMonthCutOff(), $this->cutOffDate())->orderBy('date');
    }

    public function clawbacks()
    {
        if (!$this->clawbackRecords) {
            return $this->clawbackRecords = $this->clawBackQuery()->get();
        }

        return $this->clawbackRecords;
    }

    /**
     * @return mixed
     */
    protected function emptyCollection()
    {
        $model = ModelMapper::get(CommissionPaymentSchedule::class);

        return $model::where('id', '<', 0)->where('id', '>', 0);
    }

    public function getReports()
    {
        return CommissionRecepient::whereHas('commissions', function ($q) {
            $q->currency($this->currency)->whereHas('overrideSchedules', function ($q) {
                $q->forRecipient($this->recipient)->between($this->start, $this->end);
            });
        })->orderBy('name')->get();
    }

//    /**
//     * @return AdvanceInvestmentCommission|\Illuminate\Database\Eloquent\Model|void
//     */
//    public function processAdvanceCommissions()
//    {
//        $netAmount = $this->summary()->netCommission();
//
//        if ($netAmount == 0) {
//            return;
//        }
//
//        if ($netAmount < 0 && $this->isIfa()) {
//            return $this->awardSystemAdvanceCommission(($netAmount));
//        }
//
//        if ($netAmount > 0) {
//            $this->advanceRepayment = $netAmount;
//
//            return $this->repayAdvanceCommission($netAmount);
//        }
//    }
//
//    /**
//     * @return bool
//     */
//    private function isIfa()
//    {
//        return $this->recipient->recipient_type_id == 3;
//    }
//
//    /**
//     * @param $amount
//     * @return AdvanceInvestmentCommission|\Illuminate\Database\Eloquent\Model
//     */
//    private function awardSystemAdvanceCommission($amount)
//    {
//        return AdvanceInvestmentCommission::create([
//            'amount' => abs($amount),
//            'recipient_id' => $this->recipient->id,
//            'date' => $this->end,
//            'paid' => 0,
//            'currency_id' => $this->currency->id,
//            'advance_commission_type_id' => 3,
//            'description' => 'System Clawback Commission for Negative Commission Value',
//            'repayable' => 1
//        ]);
//    }
//
//    /**
//     * @param $netAmount
//     */
//    private function repayAdvanceCommission($netAmount)
//    {
//        $unpaidAdvanceCommissions = $this->unpaidAdvanceCommissions();
//
//        foreach ($unpaidAdvanceCommissions as $unpaidAdvanceCommission) {
//            $unpaidAmount = $unpaidAdvanceCommission->unpaid_amount;
//
//            if ($netAmount > $unpaidAmount) {
//                $amount = $unpaidAmount;
//                $fullyPaid = true;
//            } else {
//                $amount = $netAmount;
//                $fullyPaid = false;
//            }
//
//            $this->recordAdvanceRepayment($unpaidAdvanceCommission, $amount);
//
//            unset($unpaidAdvanceCommission->unpaid_amount);
//
//            if ($fullyPaid) {
//                $unpaidAdvanceCommission->update([
//                    'paid_on' => $this->end
//                ]);
//            }
//
//            $unpaidAdvanceCommission->unpaid_amount = $unpaidAmount;
//
//            $netAmount = $netAmount - $amount;
//
//            if ($amount < 0) {
//                break;
//            }
//        }
//    }
//
//    /**
//     * @param AdvanceInvestmentCommission $advanceInvestmentCommission
//     * @param $amount
//     * @return AdvanceInvestmentCommissionRepayment|\Illuminate\Database\Eloquent\Model
//     */
//    private function recordAdvanceRepayment(AdvanceInvestmentCommission $advanceInvestmentCommission, $amount)
//    {
//        return AdvanceInvestmentCommissionRepayment::create([
//            'advance_investment_commission_id' => $advanceInvestmentCommission->id,
//            'date' => $this->end,
//            'amount' => $amount
//        ]);
//    }
//
//    /**
//     * @return int
//     */
//    public function getAdvanceRepayment()
//    {
//        return $this->advanceRepayment;
//    }
}
