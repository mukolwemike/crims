<?php
/**
 * Date: 03/02/2017
 * Time: 12:05
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Investments\Commission\Models;

interface Commission
{
    public function schedules();

    public function scopeCurrency($query, $currency);

    public function recipient($recipient);

    public function writtenOff($bool);
}
