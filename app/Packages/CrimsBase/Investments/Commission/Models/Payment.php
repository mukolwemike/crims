<?php
/**
 * Date: 03/02/2017
 * Time: 12:21
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Investments\Commission\Models;

interface Payment
{
    public function scopeInMonth();
}
