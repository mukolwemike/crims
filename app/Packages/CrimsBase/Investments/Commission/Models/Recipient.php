<?php
/**
 * Date: 03/02/2017
 * Time: 11:55
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Investments\Commission\Models;

interface Recipient
{
    public function type();

    public function rank();
}
