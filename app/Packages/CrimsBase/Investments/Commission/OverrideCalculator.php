<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Investments\Commission;

use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use App\Exceptions\CrimsException;
use App\Packages\CrimsBase\Commission\OverrideTrait;
use Carbon\Carbon;
use Cytonn\Models\CommissionOverrideRate;

class OverrideCalculator
{
    use OverrideTrait;

    protected $schedule;
    protected $clawBack;
    protected $date;
    protected $structure;
    const STRUCTURE_TYPE = 'investments';
    protected $liveMode = true;
    protected $overrideSchedules = [];

    public function __construct(
        Carbon $date,
        CommissionPaymentSchedule $schedule = null,
        CommissionClawback $clawback = null
    ) {
        $this->schedule = $schedule;
        $this->clawBack = $clawback;
        $this->date = $date;
        $this->structure = $this->getStructure(static::STRUCTURE_TYPE, $this->date);
    }

    /**
     * @param $mode
     * @return $this
     */
    public function setLiveMode($mode)
    {
        $this->liveMode = $mode;

        return $this;
    }

    /**
     * @throws CrimsException
     * @throws \Throwable
     */
    public function awardOverrides()
    {
        if (is_null($this->structure)) {
            return;
        }

        if (is_null($this->schedule)) {
            throw new CrimsException("Please provide a schedule");
        }

        \DB::transaction(function () {
            if ($this->schedule->amount != 0 && $this->schedule->claw_back != 1 && is_null($this->schedule->deleted_at)) {
                $this->awardSupervisorOverride($this->schedule->commission->recipient, $this->structure->inclusive);
            }

            if ($this->liveMode) {
                $this->schedule->overrideSchedules()->delete();

                $this->saveOverrideSchedules();
            }
        });
    }

    /**
     * @param CommissionRecepient $recepient
     */
    private function awardSupervisorOverride(CommissionRecepient $recepient, $inclusive)
    {
        $supervisorData = $this->getOverrideSupervisorAndRate($recepient, $this->date, $this->structure, $inclusive);

        if (is_null($supervisorData)) {
            return $inclusive == 1 ? $this->awardSupervisorOverride($recepient, 0) : null;
        }

        $inclusive = 0;

        $this->overrideSchedules[] = $this->saveOverrideForSchedule($supervisorData->supervisor, $supervisorData->rate);

        return $this->awardSupervisorOverride($supervisorData->supervisor, $inclusive);
    }

    /**
     * @param CommissionRecepient $recipient
     * @param CommissionOverrideRate $rate
     * @return array
     */
    private function saveOverrideForSchedule(CommissionRecepient $recipient, CommissionOverrideRate $rate)
    {
        return [
            'commission_id' => $this->schedule->commission_id,
            'schedule_id' => $this->schedule->id,
            'clawback_id' => null,
            'recipient_id' => $recipient->id,
            'date' => Carbon::parse($this->schedule->date)->toDateString(),
            'rate' => $rate->rate,
            'amount' => $this->calculateOverrideAmount($this->schedule->amount, $rate->rate),
            'report_id' => $this->schedule->commission->recipient_id,
            'schedule_amount' => $this->schedule->amount,
            'description' => $this->getDescription($this->schedule->id, $this->schedule->description, $rate->rate)
        ];
    }

    /**
     * @throws CrimsException
     * @throws \Throwable
     */
    public function clawbackOverrides()
    {
        if (is_null($this->clawBack)) {
            throw new CrimsException("Please provide a clawback");
        }

        \DB::transaction(function () {
            if ($this->clawBack->amount != 0 && is_null($this->clawBack->deleted_at)) {
                $this->clawBackSupervisorOverride($this->clawBack->commission->recipient, $this->structure->inclusive);
            }

            if ($this->liveMode) {
                $this->clawBack->overrideSchedules()->delete();

                $this->saveOverrideSchedules();
            }
        });
    }

    /**
     * @param CommissionRecepient $recepient
     */
    private function clawBackSupervisorOverride(CommissionRecepient $recepient, $inclusive)
    {
        $supervisorData = $this->getOverrideSupervisorAndRate($recepient, $this->date, $this->structure, $inclusive);

        if (is_null($supervisorData)) {
            return $inclusive == 1 ? $this->clawBackSupervisorOverride($recepient, 0) : null;
        }

        $inclusive = 0;

        $this->overrideSchedules[] = $this->saveOverrideForClawback($supervisorData->supervisor, $supervisorData->rate);

        return $this->clawBackSupervisorOverride($supervisorData->supervisor, $inclusive);
    }

    /**
     * @param CommissionRecepient $recipient
     * @param CommissionOverrideRate $rate
     * @return array
     */
    private function saveOverrideForClawback(CommissionRecepient $recipient, CommissionOverrideRate $rate)
    {
        return [
            'commission_id' => $this->clawBack->commission_id,
            'schedule_id' => null,
            'clawback_id' => $this->clawBack->id,
            'recipient_id' => $recipient->id,
            'date' => Carbon::parse($this->clawBack->date)->toDateString(),
            'rate' => $rate->rate,
            'amount' => -$this->calculateOverrideAmount($this->clawBack->amount, $rate->rate),
            'report_id' => $this->clawBack->commission->recipient_id,
            'schedule_amount' => -abs($this->clawBack->amount),
            'description' => $this->getDescription($this->clawBack->id, $this->clawBack->narration, $rate->rate)
        ];
    }

    /**
     *
     */
    public function saveOverrideSchedules()
    {
        foreach ($this->overrideSchedules as $schedule) {
            CommissionPaymentOverride::create($schedule);
        }
    }

    public function deleteOverrideSchedules()
    {
        if ($this->schedule) {
            $this->schedule->overrideSchedules()->delete();
        } else {
            $this->clawBack->overrideSchedules()->delete();
        }
    }

    /**
     * @return object
     */
    public function verifyScheduleOverrides()
    {
        $currentCount = count($this->overrideSchedules);

        $scheduleQuery = $this->schedule ? $this->schedule->overrideSchedules() : $this->clawBack->overrideSchedules();

        $savedSchedules = $scheduleQuery->get([
            'commission_id', 'recipient_id', 'report_id', 'clawback_id', 'schedule_id',
            'rate', 'date', 'schedule_amount', 'amount',
        ])->toArray();

        $savedSchedulesCount = count($savedSchedules);

        $missingSchedules = array();

        foreach ($this->overrideSchedules as $newSchedule) {
            $exceptSchedule = array_except($newSchedule, ['description']);

            $itemKey = array_search($exceptSchedule, $savedSchedules);

            if ($itemKey !== false) {
                unset($savedSchedules[$itemKey]);
            } else {
                $missingSchedules[] = $newSchedule;
            }
        }

        return (object) [
            'current_schedules_count' => $currentCount,
            'saved_schedules_count' => $savedSchedulesCount,
            'missing_schedules' => $missingSchedules,
            'invalid_schedules' => $savedSchedules
        ];
    }
}
