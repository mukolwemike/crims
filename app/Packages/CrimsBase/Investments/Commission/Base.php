<?php
/**
 * Date: 31/10/2016
 * Time: 10:45
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Crims\Investments\Commission;

use \Crims\Investments\Structured\Models\Investment;
use Crims\Investments\Commission\Models\Recipient;

/**
 * Class Base
 *
 * @package Cytonn\Investment\Commission
 */
class Base
{
    /**
     * The date when commission cuts off, and starts being paid at the end of month
     */
    const COMMISSION_CUT_OFF_DATE = 20;

    /**
     * Don't check compliance for FAs of these types
     */
    const DONT_CHECK_COMPLIANCE = ['ifa', 'fa', 'staff'];

    /**
     * @param Investment $investment
     *
     * @param $diffInDays
     * @return float
     */
    protected function commissionDifference(Investment $investment, $diffInDays)
    {
        return $investment->commission->rate * $investment->amount * $diffInDays / (100 * 365);
    }

    /**
     * @param Investment $investment
     * @return float
     */
    protected function calculateTotalCommission(Investment $investment)
    {
        return $investment->commission->rate * $investment->amount * $investment->repo->getTenorInDays() / (100 * 365);
    }

    /**
     * @param Investment $investment
     * @return mixed
     */
    protected function scheduledCommission(Investment $investment)
    {
        return $investment->commission->schedules()
            ->where('claw_back', false)
            ->sum('amount');
    }

    public function shouldCheckCompliance(Recipient $recipient)
    {
        return !in_array($recipient->type->slug, static::DONT_CHECK_COMPLIANCE);
    }

    protected function shouldNotCheckCompliance(Recipient $recipient)
    {
        return !$this->shouldCheckCompliance($recipient);
    }
}
