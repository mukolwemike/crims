<?php
/**
 * Date: 03/02/2017
 * Time: 12:00
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Investments\Structured\Models;

interface Investment
{
    public function commission();
}
