<?php
/**
 * Date: 03/02/2017
 * Time: 13:59
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Base;

class ModelMapper
{
    protected $maps = [];

    public static function get($key)
    {
        $instance = self::instance();

        if (!isset($instance->maps[$key])) {
            throw new \InvalidArgumentException("The model $key has not been mapped to its concrete model");
        }

        return \App::make($instance->maps[$key]);
    }

    private static function instance()
    {
        return \App::make('crims_models');
    }

    public function register($concrete, $abstract)
    {
        $this->maps[$abstract] = $concrete;
    }
}
