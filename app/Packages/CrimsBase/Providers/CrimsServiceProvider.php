<?php namespace Crims\Providers;

use Crims\Base\ModelMapper;
use Illuminate\Support\ServiceProvider;

class CrimsServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $model_maps = \Config::get('crims.model_maps');

        $this->app->singleton(
            'crims_models',
            function () use ($model_maps) {
                $mapper = new ModelMapper();

                foreach ($model_maps as $abs => $conc) {
                    $mapper->register($conc, $abs);
                }

                return $mapper;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
