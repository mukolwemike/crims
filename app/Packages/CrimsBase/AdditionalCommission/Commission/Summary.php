<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\AdditionalCommission\Commission;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;

class Summary
{
    protected $recipient;

    protected $start;

    protected $end;

    protected $additionalCommission;

    protected $unpaidAdditionalCommission;

    protected $unpaidAdditionalClawback;

    protected $overallCommission;

    protected $netRepayments;

    protected $finalCommission;

    protected $systemAdditionalCommission;

    protected $adjustmentCommission;

    public function __construct(
        CommissionRecepient $recipient,
        Carbon $start,
        Carbon $end,
        $overallCommission,
        $additionalCommission,
        $unpaidAdditionalCommission,
        $unpaidAdditionalClawback
    ) {
        $this->recipient = $recipient;
        $this->start = $start;
        $this->end = $end;
        $this->overallCommission = $overallCommission;
        $this->additionalCommission = $additionalCommission;
        $this->unpaidAdditionalCommission = $unpaidAdditionalCommission;
        $this->unpaidAdditionalClawback = $unpaidAdditionalClawback;

        $this->calculate();
    }

    private function calculate()
    {
        if ($this->isIfa()) {
            $this->calculateForIfa();
        } else {
            $this->netRepayments = $this->unpaidAdditionalCommission() + $this->unpaidAdditionalClawback();

            $this->finalCommission = $this->netCommission() - $this->netRepayments;

            $this->systemAdditionalCommission = 0;

            $this->adjustmentCommission = $this->systemAdditionalCommission + $this->additionalCommission() -
                $this->netRepayments;
        }
    }

    private function calculateForIfa()
    {
        $netCommission = $this->netCommission();

        $netCommission -= $this->unpaidAdditionalClawback();

        $repayment = $netCommission >= 0 ? min($netCommission, $this->unpaidAdditionalCommission()) : 0;

        $this->netRepayments = $repayment + $this->unpaidAdditionalClawback();

        $this->finalCommission = $netCommission - $repayment;

        $this->systemAdditionalCommission = $this->finalCommission >= 0 ? 0 : abs($this->finalCommission);

        $this->adjustmentCommission = $this->systemAdditionalCommission + $this->additionalCommission() -
            $this->netRepayments;
    }

    /**
     * @return mixed
     */
    public function additionalCommission()
    {
        return $this->additionalCommission;
    }

    /**
     * @return mixed
     */
    public function unpaidAdditionalCommission()
    {
        return $this->unpaidAdditionalCommission;
    }

    /**
     * @return mixed
     */
    public function unpaidAdditionalClawback()
    {
        return $this->unpaidAdditionalClawback;
    }

    /**
     * @return mixed
     */
    public function netUnpaidCommission()
    {
        return $this->unpaidAdditionalCommission() + $this->unpaidAdditionalClawback();
    }

    /**
     * @return mixed
     */
    public function total()
    {
        return $this->additionalCommission - $this->netUnpaidCommission();
    }

    /**
     * @return mixed
     */
    public function netCommission()
    {
        return $this->overallCommission + $this->additionalCommission;
    }

    /**
     * @return bool
     */
    public function isIfa()
    {
        return $this->recipient->recipient_type_id == 3;
    }

    /**
     * @return mixed
     */
    public function getOverallCommission()
    {
        return $this->overallCommission;
    }

    /**
     * @return mixed
     */
    public function netRepayments()
    {
        return $this->netRepayments;
    }

    /**
     * @return mixed
     */
    public function getFinalCommission()
    {
        return $this->finalCommission;
    }

    /**
     * @return mixed
     */
    public function getSystemAdditionalCommission()
    {
        return $this->systemAdditionalCommission;
    }

    /**
     * @return mixed
     */
    public function getAdjustmentCommission()
    {
        return $this->adjustmentCommission;
    }
}
