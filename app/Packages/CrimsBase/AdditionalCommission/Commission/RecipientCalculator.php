<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\AdditionalCommission\Commission;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Investment\AdditionalCommission;
use Carbon\Carbon;

class RecipientCalculator
{
    protected $recipient;

    protected $start;

    protected $end;

    protected $overallCommission;

    protected $additionalCommissions;

    protected $allUnpaidAdditionalCommissions;

    protected $unpaidAdditionalCommissions;

    protected $unpaidAdditionalClawbacks;

    protected $currencies;

    /**
     * RecipientCalculator constructor.
     * @param CommissionRecepient $recipient
     * @param Carbon $start
     * @param Carbon $end
     */
    public function __construct(CommissionRecepient $recipient, Carbon $start, Carbon $end, $overallCommission = null)
    {
        $this->recipient = $recipient;
        $this->start = $start;
        $this->end = $end;
        $this->overallCommission = $overallCommission;
    }

    /**
     * @return $this
     */
    public function calculateOverallCommission()
    {
        $investments = $this->currencies()->sum(function (Currency $currency) {
            $commission = $this->recipient->calculator($currency, $this->start, $this->end)->summary()
                ->finalCommission();

            return convert($commission, $currency, $this->end);
        });

        $re = $this->recipient->reCommissionCalculator($this->start, $this->end)->summary()->finalCommission();

        $shares = $this->recipient->shareCommissionCalculator($this->start, $this->end)->summary()->finalCommission();

        $units = $this->recipient->unitizationCommissionCalculator($this->start, $this->end)->summary()
            ->finalCommission();

        $this->overallCommission = $investments + $re + $shares + $units;

        return $this;
    }

    /**
     * @return Currency[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function currencies()
    {
        if (!$this->currencies) {
            $this->currencies = Currency::whereHas('products', function ($q) {
                $q->active()->has('investments');
            })->get();
        }

        return $this->currencies;
    }

    public function summary()
    {
        $additionalCommission = $this->additionalCommissions()->sum('amount');

        $unpaidAdditionalCommission = $this->unpaidAdditionalCommission()->sum('unpaid_amount');

        $unpaidAdditionalClawbacks = $this->unpaidAdditionalClawbacks()->sum('unpaid_amount');

        return new Summary(
            $this->recipient,
            $this->start,
            $this->end,
            $this->overallCommission,
            $additionalCommission,
            $unpaidAdditionalCommission,
            $unpaidAdditionalClawbacks
        );
    }

    /**
     * @return mixed
     */
    public function additionalCommissions()
    {
        if ($this->additionalCommissions) {
            return $this->additionalCommissions;
        }

        return $this->additionalCommissions = $this->additionalCommissionQuery()->get();
    }

    /**
     * @return mixed
     */
    private function additionalCommissionQuery()
    {
        return AdditionalCommission::between($this->start, $this->end)->forRecipient($this->recipient)->orderBy('date');
    }

    /**
     * @return mixed
     */
    public function allUnpaidAdditionalCommissions()
    {
        if ($this->allUnpaidAdditionalCommissions) {
            return $this->allUnpaidAdditionalCommissions;
        }

        $allUnpaidCommissions = $this->unpaidAdditionalCommissionQuery()->get();

        $unpaidClawbacks = $unpaidCommissions = [];

        foreach ($allUnpaidCommissions as $commission) {
            $commission->unpaid_amount = $commission->present()->unpaidAmount($this->start);

            if ($commission->type_id == 2) {
                $unpaidClawbacks[] = $commission;
            } else {
                $unpaidCommissions[] = $commission;
            }
        }

        $this->unpaidAdditionalCommissions = collect($unpaidCommissions);

        $this->unpaidAdditionalClawbacks = collect($unpaidClawbacks);

        return $this->allUnpaidAdditionalCommissions = $allUnpaidCommissions;
    }

    /**
     * @return mixed
     */
    private function unpaidAdditionalCommissionQuery()
    {
        return AdditionalCommission::forRecipient($this->recipient)->unpaidAsAt($this->start)->repayable()
            ->where('date', '<', $this->start)->orderBy('date');
    }

    /**
     * @return mixed
     */
    public function unpaidAdditionalCommission()
    {
        if ($this->unpaidAdditionalCommissions) {
            return $this->unpaidAdditionalCommissions;
        }

        $this->allUnpaidAdditionalCommissions();

        return $this->unpaidAdditionalCommissions;
    }

    /**
     * @return mixed
     */
    public function unpaidAdditionalClawbacks()
    {
        if ($this->unpaidAdditionalClawbacks) {
            return $this->unpaidAdditionalClawbacks;
        }

        $this->allUnpaidAdditionalCommissions();

        return $this->unpaidAdditionalClawbacks;
    }
}
