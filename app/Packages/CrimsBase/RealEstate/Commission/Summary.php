<?php
/**
 * Date: 09/02/2017
 * Time: 09:08
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Realestate\Commission;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstate\RealestateCommissionPaymentOverride;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use Carbon\Carbon;
use Crims\Base\ModelMapper;
use Crims\Commission\Override;
use Crims\Commission\SummaryContract;
use Crims\Investments\Commission\Models\OverrideStructure;
use Crims\Investments\Commission\Models\Recipient;
use Illuminate\Support\Collection;

/**
 * Class Summary
 *
 * @package Crims\Realestate\Commission
 */
class Summary implements SummaryContract
{
    /**
     * @var Collection
     */
    protected $schedules;

    /**
     * @var Recipient
     */
    protected $recipient;

    /**
     * @var Carbon
     */
    protected $start;

    /**
     * @var Carbon
     */
    protected $end;

    const COMMISSION_TYPE = 'real_estate';
    const OVERRIDE_COMMISSION_PERCENTAGE = 0.5;
    protected $overrideCommissionPercentage = null;

    private $total = null;
    private $earned = null;
    private $override = null;
    private $advanceCommission;
    private $unpaidAdvanceCommission;


    /**
     * Summary constructor.
     * @param Recipient $recipient
     * @param Carbon $start
     * @param Carbon $end
     * @param Collection $schedules
     * @param int $advanceCommission
     * @param int $unpaidAdvanceCommission
     */
    public function __construct(
        Recipient $recipient,
        Carbon $start,
        Carbon $end,
        Collection $schedules,
        $advanceCommission = 0,
        $unpaidAdvanceCommission = 0
    ) {
        $this->schedules = $schedules;
        $this->recipient = $recipient;
        $this->start = $start;
        $this->end = $end;
        $this->advanceCommission = $advanceCommission;
        $this->unpaidAdvanceCommission = $unpaidAdvanceCommission;
    }

    /**
     * @return mixed
     */
    public function total()
    {
        if (is_null($this->total)) {
            return $this->total = $this->earned() + $this->advanceCommission
                - $this->unpaidAdvanceCommission;
        }

        return $this->total;
    }

    /**
     * @return mixed|null
     */
    public function earned()
    {
        if (is_null($this->earned)) {
            return $this->earned = $this->schedules->sum('amount');
        }

        return $this->earned;
    }

    /*
     * Get the override commission total
     */
    public function overrideCommissionTotal()
    {
        $overrideCommissionPercentage = $this->getOverrideCommissionPercentage($this->recipient->type()->first());

        if ($overrideCommissionPercentage == 0) {
            return 0;
        }

        return $this->schedules->sum(
            function ($schedule) use ($overrideCommissionPercentage) {
                return $this->getOverrideCommissionOnSchedule($schedule, $overrideCommissionPercentage);
            }
        );
    }

    public function getStructure()
    {
        return ModelMapper::get(OverrideStructure::class)->where('slug', static::COMMISSION_TYPE)
            ->where('date', '<=', $this->start)
            ->latest('date')
            ->first();
    }

    /*
     * Get the override commission percentage
     */
    public function getOverrideCommissionPercentage($type)
    {
        if (!is_null($this->overrideCommissionPercentage)) {
            return $this->overrideCommissionPercentage;
        }

        $structure = $this->getStructure();

        if (is_null($structure)) {
            return $this->overrideCommissionPercentage = 0;
        }

        $overridePercentage = $type->realestateOverridePercentages()->where('structure_id', $structure->id)
            ->first();

        return $this->overrideCommissionPercentage = $overridePercentage ? $overridePercentage->rate : 0;
    }

    public function schedules()
    {
        return $this->schedules;
    }

    /**
     * @return int
     */
    public function getAdvanceCommission()
    {
        return $this->advanceCommission;
    }

    /**
     * @return int
     */
    public function getUnpaidAdvanceCommission()
    {
        return $this->unpaidAdvanceCommission;
    }

    /**
     * All the commission for the period before doing adjustments
     *
     * @return |null
     */
    public function actualFinalCommission()
    {
        return $this->total() + $this->override();
    }

    /**
     * All commission for the period that must be factored in
     *
     * @return int|null
     */
    public function netCommission()
    {
        return $this->earned() + $this->advanceCommission + $this->override();
    }

    /**
     * Amount that will go for repayment of advance commission
     *
     * @return int|mixed
     */
    public function advanceRepayment()
    {
        $netCommission = $this->netCommission();

        if ($this->unpaidAdvanceCommission <= 0 || $netCommission <= 0) {
            return 0;
        }

        return min($this->unpaidAdvanceCommission, $netCommission);
    }

    /**
     * Final commission for period after doing adjustments
     *
     * @return int|mixed
     */
    public function finalCommission()
    {
        $netCommission = $this->netCommission();

        $finalDeducted = $netCommission - $this->unpaidAdvanceCommission;

        return $this->isIfa() ? ($netCommission > 0 ? max(0, $finalDeducted) : 0) : $finalDeducted;
    }

    /**
     * @return bool
     */
    public function isIfa()
    {
        return $this->recipient->recipient_type_id == 3;
    }

    public function override()
    {
        if (is_null($this->override)) {
            return RealestateCommissionPaymentOverride::forRecipient($this->recipient)
                ->between($this->start, $this->end)->sum('amount');
        }

        return $this->override;
    }

    public function overrideOnRecipient(CommissionRecepient $recepient)
    {
        return RealestateCommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->sum('amount');
    }

    public function overrideCommission(CommissionRecepient $recepient)
    {
        return RealestateCommissionPaymentOverride::forRecipient($this->recipient)->between($this->start, $this->end)
            ->forReport($recepient)->sum('schedule_amount');
    }

    private function getOverrideCommissionOnSchedule($schedule, $overrideCommissionPercentage)
    {
        $commission = $schedule->commission;

        $holding = $commission->holding;

        $price = $holding->price();

        if ($price == 0) {
            return 0;
        }

        return ($overrideCommissionPercentage * $price * $schedule->amount) / ($commission->amount * 100);
    }
}
