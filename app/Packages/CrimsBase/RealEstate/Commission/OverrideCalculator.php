<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Realestate\Commission;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use App\Cytonn\Models\RealEstate\RealestateCommissionPaymentOverride;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Packages\CrimsBase\Commission\OverrideTrait;
use Carbon\Carbon;
use Cytonn\Models\CommissionOverrideRate;

class OverrideCalculator
{
    use OverrideTrait;

    protected $schedule;
    protected $date;
    protected $structure;
    const STRUCTURE_TYPE = 'real_estate';
    protected $liveMode = true;
    protected $overrideSchedules = [];

    public function __construct(Carbon $date, RealEstateCommissionPaymentSchedule $schedule)
    {
        $this->schedule = $schedule;
        $this->date = $date;
        $this->structure = $this->getStructure(static::STRUCTURE_TYPE, $this->date);
    }

    /**
     * @param $mode
     * @return $this
     */
    public function setLiveMode($mode)
    {
        $this->liveMode = $mode;

        return $this;
    }

    /**
     * @throws \Throwable
     */
    public function awardOverrides()
    {
        if (is_null($this->structure)) {
            return;
        }

        $this->updateScheduleAmount();

        \DB::transaction(function () {
            if ($this->schedule->amount != 0 && is_null($this->schedule->deleted_at)) {
                $this->awardSupervisorOverride($this->schedule->commission->recipient, $this->structure->inclusive);
            }

            if ($this->liveMode) {
                $this->schedule->overrideSchedules()->delete();

                $this->saveOverrideSchedules();
            }
        });
    }

    /**
     * @param CommissionRecepient $recepient
     */
    private function awardSupervisorOverride(CommissionRecepient $recepient, $inclusive)
    {
        $supervisorData = $this->getOverrideSupervisorAndRate($recepient, $this->date, $this->structure, $inclusive);

        if (is_null($supervisorData)) {
            return $inclusive == 1 ? $this->awardSupervisorOverride($recepient, 0) : null;
        }

        $inclusive = 0;

        $this->overrideSchedules[] = $this->saveOverrideForSchedule($supervisorData->supervisor, $supervisorData->rate);

        return $this->awardSupervisorOverride($supervisorData->supervisor, $inclusive);
    }

    /**
     * @param CommissionRecepient $recipient
     * @param CommissionOverrideRate $rate
     * @return array
     */
    private function saveOverrideForSchedule(CommissionRecepient $recipient, CommissionOverrideRate $rate)
    {
        return [
            'commission_id' => $this->schedule->commission_id,
            'schedule_id' => $this->schedule->id,
            'recipient_id' => $recipient->id,
            'date' => $this->schedule->date,
            'rate' => $rate->rate,
            'amount' => $this->calculateOverrideAmount($this->schedule->amount, $rate->rate),
            'report_id' => $this->schedule->commission->recipient_id,
            'schedule_amount' => $this->schedule->amount,
            'description' => $this->getDescription($this->schedule->id, $this->schedule->description, $rate->rate)
        ];
    }

    /**
     * @return int
     */
    private function updateScheduleAmount()
    {
        $percentage = $this->getOverrideCommissionPercentage();

        $commission = $this->schedule->commission;

        $holding = $commission->holding;

        $price = $holding->price();

        if ($price == 0) {
            return 0;
        }

        $amount = ($percentage * $price * $this->schedule->amount) / ($commission->amount * 100);

        $this->schedule->amount = round($amount, 2);
    }

    /**
     * @return int
     */
    private function getOverrideCommissionPercentage()
    {
        $recipient = $this->schedule->commission->recipient;

        $type = $recipient->repo->getRecipientType($this->date);

        $overridePercentage = $type->realestateOverridePercentages()
            ->where('start_date', '<=', $this->date)
            ->where('structure_id', $this->structure->id)->first();

        return $overridePercentage ? $overridePercentage->rate : 0;
    }

    /**
     * Create new override schedules
     */
    public function saveOverrideSchedules()
    {
        foreach ($this->overrideSchedules as $schedule) {
            RealestateCommissionPaymentOverride::create($schedule);
        }
    }

    /**
     * Delete override schedules for the schedule
     */
    public function deleteOverrideSchedules()
    {
        $this->schedule->overrideSchedules()->delete();
    }

    /**
     * @return object
     */
    public function verifyScheduleOverrides()
    {
        $currentCount = count($this->overrideSchedules);

        $savedSchedules = $this->schedule->overrideSchedules()->get([
            'commission_id', 'recipient_id', 'report_id', 'schedule_id',
            'rate', 'date', 'schedule_amount', 'amount',
        ])->toArray();

        $savedSchedulesCount = count($savedSchedules);

        $missingSchedules = array();

        foreach ($this->overrideSchedules as $newSchedule) {
            $exceptSchedule = array_except($newSchedule, ['description']);

            $itemKey = array_search($exceptSchedule, $savedSchedules);

            if ($itemKey !== false) {
                unset($savedSchedules[$itemKey]);
            } else {
                $missingSchedules[] = $newSchedule;
            }
        }

        return (object) [
            'current_schedules_count' => $currentCount,
            'saved_schedules_count' => $savedSchedulesCount,
            'missing_schedules' => $missingSchedules,
            'invalid_schedules' => $savedSchedules
        ];
    }
}
