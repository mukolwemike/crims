<?php

namespace Crims\Realestate\Commission;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;


use Crims\Base\ModelMapper;
use Crims\Commission\Override;
use Crims\Investments\Commission\Models\Recipient;
use Crims\Realestate\Commission\Models\RealEstateCommissionPaymentSchedule;
use Cytonn\Models\RealEstate\AdvanceRealEstateCommission;
use Cytonn\Models\RealEstate\AdvanceRealEstateCommissionRepayments;

/**
 * Class RecipientCalculator
 *
 * @package Cytonn\Realestate\Commissions\Calculator
 */
class RecipientCalculator
{
    /**
     * @var $recipient
     */
    protected $recipient;
    /**
     * @var Carbon
     */
    private $end;
    /**
     * @var Carbon
     */
    private $start;

    protected $client;

    public $schedulesFilter;

    private $summary;
    private $schedules;
    private $advanceCommissions;
    private $unpaidAdvanceCommissions;
    private $advanceRepayment = 0;

    /**
     * RecipientCalculator constructor.
     *
     * @param Recipient $recipient
     * @param Carbon    $start
     * @param Carbon    $end
     * @param Client    $client
     */
    public function __construct(Recipient $recipient, Carbon $start, Carbon $end, Client $client = null)
    {
        $this->recipient = $recipient;
        $this->end = $end;
        $this->start = $start;
        $this->client = $client;
    }

    /**
     * @param bool $fresh
     * @return Summary
     */
    public function summary($fresh = false)
    {
        if (!$this->summary || $fresh) {
            $advanceCommissions = $this->advanceCommissions()->sum('amount');

            $unpaidAdvanceCommissions = $this->unpaidAdvanceCommissions()->sum('unpaid_amount');

            return $this->summary = new Summary(
                $this->recipient,
                $this->start,
                $this->end,
                $this->schedules(),
                $advanceCommissions,
                $unpaidAdvanceCommissions
            );
        }

        return $this->summary;
    }

    public function setSchedulesFilter(\Closure $closure)
    {
        $this->schedulesFilter = $closure;

        return $this;
    }

    public function setOverrideDates(Carbon $holdingsFrom = null, Carbon $holdingsTo = null)
    {
        if ($holdingsFrom || $holdingsTo) {
            $q = function ($schedule) use ($holdingsFrom, $holdingsTo) {
                $schedule->whereHas(
                    'commission',
                    function ($commission) use ($holdingsFrom, $holdingsTo) {
                        $commission->whereHas(
                            'holding',
                            function ($holding) use ($holdingsFrom, $holdingsTo) {
                                if ($holdingsFrom) {
                                    $holding->where('reservation_date', '>=', $holdingsFrom);
                                }

                                if ($holdingsTo) {
                                    $holding->where('reservation_date', '<=', $holdingsTo);
                                }
                            }
                        );
                    }
                );
            };

            $this->setSchedulesFilter($q);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    public function schedules()
    {
        if (!$this->schedules) {
            $schedules = ModelMapper::get(RealEstateCommissionPaymentSchedule::class);

            $q = $schedules::where('date', '>=', $this->start)
                ->where('date', '<=', $this->end)
                ->orderBy('date')
                ->whereHas(
                    'commission',
                    function ($commission) {
                        $commission->where('recipient_id', $this->recipient->id);

                        if ($this->client) {
                            $commission->forClient($this->client);
                        }
                    }
                );

            if ($filter = $this->schedulesFilter) {
                $q = $q->where($filter);
            }

            return $this->schedules = $q->get();
        }

        return $this->schedules;
    }

    public function getReports()
    {
        return CommissionRecepient::whereHas('realEstateCommissions', function ($q) {
            $q->whereHas('schedules', function ($q) {
                $q->whereHas('overrideSchedules', function ($q) {
                    $q->forRecipient($this->recipient)->between($this->start, $this->end);
                });
            });
        })->orderBy('name')->get();
    }

    /*
     * ADVANCE COMMISISONS
     */
    /**
     * @return mixed
     */
    public function advanceCommissions($fresh = false)
    {
        if (!$this->advanceCommissions || $fresh) {
            return $this->advanceCommissions = $this->advanceCommissionQuery()->get();
        }

        return $this->advanceCommissions;
    }

    /**
     * @return mixed
     */
    public function advanceCommissionQuery()
    {
        return AdvanceRealEstateCommission::between($this->start, $this->end)->forRecipient($this->recipient)
            ->orderBy('date');
    }

    /*
     * UNPAID ADVANCE COMMISSION
     */
    /**
     * @return mixed
     */
    public function unpaidAdvanceCommissionQuery()
    {
        return AdvanceRealEstateCommission::forRecipient($this->recipient)->unpaidAsAt($this->start)->repayable()
            ->where('date', '<', $this->start)->orderBy('date');
    }

    /**
     * @return mixed
     */
    public function unpaidAdvanceCommissions()
    {
        if (!$this->unpaidAdvanceCommissions) {
            $unpaidCommissions = $this->unpaidAdvanceCommissionQuery()->get();

            return $this->unpaidAdvanceCommissions = $unpaidCommissions
                ->each(function (AdvanceRealEstateCommission $commission) {
                    $commission->unpaid_amount = $commission->present()->unpaidAmount($this->start);

                    return $commission;
                });
        }

        return $this->unpaidAdvanceCommissions;
    }

    /**
     * @return AdvanceRealEstateCommission|\Illuminate\Database\Eloquent\Model|void
     */
    public function processAdvanceCommissions()
    {
        $netAmount = $this->summary()->netCommission();

        if ($netAmount == 0) {
            return;
        }

        if ($netAmount < 0 && $this->isIfa()) {
            return $this->awardSystemAdvanceCommission(($netAmount));
        }

        if ($netAmount > 0) {
            $this->advanceRepayment = $netAmount;

            return $this->repayAdvanceCommission($netAmount);
        }
    }

    /**
     * @return bool
     */
    private function isIfa()
    {
        return $this->recipient->recipient_type_id == 3;
    }

    /**
     * @param $amount
     * @return AdvanceRealEstateCommission|\Illuminate\Database\Eloquent\Model
     */
    private function awardSystemAdvanceCommission($amount)
    {
        return AdvanceRealEstateCommission::create([
            'amount' => abs($amount),
            'recipient_id' => $this->recipient->id,
            'date' => $this->end,
            'paid' => 0,
            'advance_commission_type_id' => 3,
            'description' => 'System Clawback Commission for Negative Commission Value',
            'repayable' => 1
        ]);
    }

    /**
     * @param $netAmount
     */
    private function repayAdvanceCommission($netAmount)
    {
        $unpaidAdvanceCommissions = $this->unpaidAdvanceCommissions();

        foreach ($unpaidAdvanceCommissions as $unpaidAdvanceCommission) {
            $unpaidAmount = $unpaidAdvanceCommission->unpaid_amount;

            if ($netAmount > $unpaidAmount) {
                $amount = $unpaidAmount;
                $fullyPaid = true;
            } else {
                $amount = $netAmount;
                $fullyPaid = false;
            }

            $this->recordAdvanceRepayment($unpaidAdvanceCommission, $amount);

            unset($unpaidAdvanceCommission->unpaid_amount);

            if ($fullyPaid) {
                $unpaidAdvanceCommission->update([
                    'paid_on' => $this->end
                ]);
            }

            $unpaidAdvanceCommission->unpaid_amount = $unpaidAmount;

            $netAmount = $netAmount - $amount;

            if ($amount < 0) {
                break;
            }
        }
    }

    /**
     * @param AdvanceRealEstateCommission $advanceRealEstateCommission
     * @param $amount
     * @return AdvanceRealEstateCommissionRepayments|\Illuminate\Database\Eloquent\Model
     */
    private function recordAdvanceRepayment(AdvanceRealEstateCommission $advanceRealEstateCommission, $amount)
    {
        return AdvanceRealEstateCommissionRepayments::create([
            'advance_real_estate_commission_id' => $advanceRealEstateCommission->id,
            'date' => $this->end,
            'amount' => $amount
        ]);
    }

    /**
     * @return int
     */
    public function getAdvanceRepayment()
    {
        return $this->advanceRepayment;
    }
}
