<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Commission;

use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;

class OverrideManager
{
    protected $recipient;
    protected $startDate;
    protected $endDate;
    protected $liveMode = true;
    protected $scheduleArray = [];
    protected $updateSchedules = false;

    public function __construct(Carbon $startDate = null, Carbon $endDate = null, CommissionRecepient $recepient = null)
    {
        $this->recipient = $recepient;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param $mode
     * @return $this
     */
    public function setLiveMode($mode)
    {
        $this->liveMode = $mode;

        return $this;
    }

    /**
     * @param $mode
     * @return $this
     */
    public function setUpdateSchedules($mode)
    {
        $this->updateSchedules = $mode;

        return $this;
    }

    public function awardOverrides($scope = 0)
    {
        if ($scope == 0 || $scope == 1) {
            $this->awardForInvestmentClawbacks();
            $this->awardForInvestmentSchedules();
        }

        if ($scope == 0 || $scope == 2) {
            $this->awardForRealEstateSchedules();
        }

        if ($scope == 0 || $scope == 3) {
            $this->awardForShareSchedules();
        }

        if ($scope == 0 || $scope == 4) {
            $this->awardForUnitFundSchedules();
        }
    }

    public function awardForInvestmentSchedules()
    {
        $schedules = CommissionPaymentSchedule::clawBack(false)
            ->orderBy('id')
            ->whereHas('commission', function ($q) {
                $q->has('investment');
                return $this->filterByRecipient($q);
            });

        $schedules = $this->filterByStart($schedules);
        $schedules = $this->filterByEnd($schedules);

        foreach ($schedules->withTrashed()->get() as $key => $schedule) {
            $date = $schedule->getOverrideDate();

            $calc = $schedule->overrideCalculator($date);

            $calc->setLiveMode($this->liveMode);

            $calc->awardOverrides();

            $this->checkVerification($calc, 'Investment Schedules', $key);
        }
    }

    public function awardForInvestmentClawbacks()
    {
        \DB::transaction(function () {
            $clawbacks = CommissionClawback::whereHas('commission', function ($q) {
                $q->has('investment');
                return $this->filterByRecipient($q);
            });

            $clawbacks = $this->filterByStart($clawbacks);
            $clawbacks = $this->filterByEnd($clawbacks);

            foreach ($clawbacks->withTrashed()->get() as $key => $clawback) {
                $date = $clawback->getOverrideDate();

                $calc = $clawback->overrideCalculator($date);

                $calc->setLiveMode($this->liveMode);

                $calc->clawbackOverrides();

                $this->checkVerification($calc, 'Investment Clawbacks', $key);
            }
        });
    }

    public function awardForRealEstateSchedules()
    {
        \DB::transaction(function () {
            $schedules = RealEstateCommissionPaymentSchedule::whereHas('commission', function ($q) {
                $q->has('holding');
                return $this->filterByRecipient($q);
            });

            $schedules = $this->filterByStart($schedules);
            $schedules = $this->filterByEnd($schedules);

            foreach ($schedules->withTrashed()->get() as $key => $schedule) {
                $date = $schedule->getOverrideDate();

                $calc = $schedule->overrideCalculator($date);

                $calc->setLiveMode($this->liveMode);

                $calc->awardOverrides();

                $this->checkVerification($calc, 'Real Estate', $key);
            }
        });
    }

    public function awardForShareSchedules()
    {
        \DB::transaction(function () {
            $schedules = SharesCommissionPaymentSchedule::has('sharePurchase');

            $schedules = $this->filterByRecipient($schedules, 'commission_recipient_id');
            $schedules = $this->filterByStart($schedules);
            $schedules = $this->filterByEnd($schedules);

            foreach ($schedules->withTrashed()->get() as $key => $schedule) {
                $date = $schedule->getOverrideDate();

                $calc = $schedule->overrideCalculator($date);

                $calc->setLiveMode($this->liveMode);

                $calc->awardOverrides();

                $this->checkVerification($calc, 'Shares', $key);
            }
        });
    }

    public function awardForUnitFundSchedules()
    {
        \DB::transaction(function () {
            $schedules = UnitFundCommissionSchedule::whereHas('commission', function ($q) {
                $q->has('purchase');
                return $this->filterByRecipient($q, 'commission_recipient_id');
            });

            $schedules = $this->filterByStart($schedules);
            $schedules = $this->filterByEnd($schedules);

            foreach ($schedules->withTrashed()->get() as $key => $schedule) {
                $date = $schedule->getOverrideDate();

                $calc = $schedule->overrideCalculator($date);

                $calc->setLiveMode($this->liveMode);

                $calc->awardOverrides();

                $this->checkVerification($calc, 'Unit Funds', $key);
            }
        });
    }

    private function filterByRecipient($query, $collumn = 'recipient_id')
    {
        return $this->recipient ? $query->where($collumn, $this->recipient->id) : $query;
    }

    private function filterByStart($query)
    {
        return $this->startDate ? $query->where('date', '>=', $this->startDate) : $query;
    }

    private function filterByEnd($query)
    {
        return $this->endDate ? $query->where('date', '<=', $this->endDate) : $query;
    }

    private function saveVerificationResult($dataArray, $type, $key)
    {
        foreach ($dataArray->missing_schedules as $schedule) {
            $schedule['reason'] = 'Missing Schedule';
            $schedule['count'] = $key;

            $this->scheduleArray[$type][] = $this->presentSchedule($schedule);
        }

        foreach ($dataArray->invalid_schedules as $schedule) {
            $schedule['description'] = isset($schedule['description']) ? $schedule['description'] : '';
            $schedule['reason'] = 'Invalid Schedule';
            $schedule['count'] = $key;

            $this->scheduleArray[$type][] = $this->presentSchedule($schedule);
        }
    }

    private function presentSchedule($schedule)
    {
        return [
            'Count' => $schedule['count'],
            'Commission ID' => isset($schedule['commission_id']) ? $schedule['commission_id'] : '',
            'Recipient ID' => $schedule['recipient_id'],
            'Report ID' => $schedule['report_id'],
            'Schedule Id' => $schedule['schedule_id'],
            'Clawback Id' => isset($schedule['clawback_id']) ? $schedule['clawback_id'] : '',
            'Rate' => $schedule['rate'],
            'Date' => $schedule['date'],
            'Schedule Amount' => $schedule['schedule_amount'],
            'Amount' => $schedule['amount'],
            'Description' => isset($schedule['description']) ? $schedule['description'] : '',
            'Reason' => $schedule['reason']
        ];
    }

    public function sendVerificationReport()
    {
        $fileName = 'Override Verification - '. Carbon::now()->toDateString();

        ExcelWork::generateAndStoreMultiSheet($this->scheduleArray, $fileName);

        Mailer::compose()
            ->to([])
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find override verification report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function checkVerification($calc, $type, $key)
    {
        if (! $this->liveMode) {
            $verifiedOverrides = $calc->verifyScheduleOverrides();

            if ($this->updateSchedules &&
                (count($verifiedOverrides->missing_schedules) > 0 ||
                    count($verifiedOverrides->invalid_schedules) > 0)
            ) {
                $this->updateUnverifiedSchedules($calc);
            }
            $this->saveVerificationResult($verifiedOverrides, $type, $key);
        }
    }

    private function updateUnverifiedSchedules($calc)
    {
        $calc->deleteOverrideSchedules();

        $calc->saveOverrideSchedules();
    }
}
