<?php
/**
 * Date: 14/02/2017
 * Time: 14:37
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Commission;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Crims\Investments\Commission\Models\Recipient;
use Crims\Investments\Commission\RecipientCalculator as CMSRecipientCalculator;
use Crims\Shares\Commission\RecipientCalculator as ShareRecipientCalculator;
use Crims\Unitization\Commission\RecipientCalculator as UnitFundRecipientCalculator;
use Crims\Realestate\Commission\RecipientCalculator;
use Illuminate\Support\Collection;

class Summary implements SummaryContract
{
    protected $recipient;
    /**
     * @var Currency
     */
    private $currency;
    /**
     * @var Carbon
     */
    private $start;
    /**
     * @var Carbon
     */
    private $end;

    private $allSummaries = null;

    /**
     * Calculator constructor.
     *
     * @param Recipient $recipient
     * @param Currency  $currency
     * @param Carbon    $start
     * @param Carbon    $end
     */
    public function __construct(Recipient $recipient, Currency $currency, Carbon $start, Carbon $end)
    {
        $this->recipient = $recipient;
        $this->currency = $currency;
        $this->start = $start;
        $this->end = $end;
    }

    public function total()
    {
        return $this->all()->sum(function ($product) {
            return $product->total();
        });
    }

    public function overrideCommission(CommissionRecepient $recepient)
    {
        return $this->all()->sum(function ($product) use ($recepient) {
            return $product->overrideCommission($recepient);
        });
    }

    public function overrideOnRecipient(CommissionRecepient $recepient)
    {
        return $this->all()->sum(function ($product) use ($recepient) {
            return $product->overrideOnRecipient($recepient);
        });
    }

    public function override()
    {
        return $this->all()->sum(
            function ($product) {
                return $product->override();
            }
        );
    }

    public function getOverride(CommissionRecepient $supervisor)
    {
        return $this->allCalculators($supervisor)->sum(
            function ($calcArray) use ($supervisor) {
                $calc = $calcArray['calc'];
                $overrideClass = $calcArray['override'];

                $overrideRate = $overrideClass->getOverrideRate($overrideClass->getDate(), $overrideClass->getType());

                if ($overrideRate == 0) {
                    return 0;
                }

                $dates = $overrideClass->getOverrideDates($supervisor, $calc->getRecipient());

                $calc = $calc->setOverrideDates($dates['start'], $dates['end']);

                $commission = 0;

                $summary = $calc->summary();

                if ($summary::COMMISSION_TYPE == 'investments') {
                    $commission = $summary->total();
                } elseif ($summary::COMMISSION_TYPE == 'real_estate') {
                    $commission = $summary->overrideCommissionTotal();
                }

                return $commission * $overrideRate / 100;
            }
        );
    }

    public function finalCommission()
    {
        return $this->all()->sum(
            function ($product) {
                return $product->total() + $product->override();
            }
        );
    }

    private function allCalculators(CommissionRecepient $recepient)
    {
        return new Collection(
            [
            [
                'calc' => $this->investmentsCalculator(),
                'override' => $this->overrideClass('investments', $recepient)
            ],
            [
                'calc' => $this->realEstateCalculator(),
                'override' => $this->overrideClass('real_estate', $recepient)
            ]
            ]
        );
    }

    /*
     * Get an override class based on the type
     */
    private function overrideClass($type, CommissionRecepient $recipient)
    {
        return (new Override($recipient, $this->end, $type, $this->start));
    }

    private function all()
    {
        if (!is_null($this->allSummaries)) {
            return $this->allSummaries;
        }

        $summaryArray = $this->getInvestmentSummaries();

        $summaryArray[] = $this->realEstate();

        $summaryArray[] = $this->shares();

        $summaryArray[] = $this->unitFunds();

        return $this->allSummaries = new Collection($summaryArray);
    }

    /*
     * Get the investment calculator
     */
    private function investmentsCalculator($currency = null)
    {
        $currency = is_null($currency) ? $this->currency : $currency;

        return (new CMSRecipientCalculator($this->recipient, $currency, $this->start, $this->end));
    }

    /*
     * Get the real estate calculator
     */
    public function realEstateCalculator()
    {
        return (new RecipientCalculator($this->recipient, $this->start, $this->end));
    }

    public function shareCalculator()
    {
        return new ShareRecipientCalculator($this->recipient, $this->start, $this->end);
    }

    public function unitFundCalculator()
    {
        return new UnitFundRecipientCalculator($this->recipient, $this->start, $this->end);
    }

    private function investments($currency = null)
    {
        $currency = is_null($currency) ? $this->currency : $currency;

        return $this->investmentsCalculator($currency)->summary();
    }
    
    private function realEstate()
    {
        return $this->realEstateCalculator()->summary();
    }

    private function shares()
    {
        return $this->shareCalculator()->summary();
    }

    private function unitFunds()
    {
        return $this->unitFundCalculator()->summary();
    }

    private function getInvestmentSummaries()
    {
        $currencies = Currency::whereHas('products', function ($q) {
            $q->active()->has('investments');
        })->get();

        $calcArray = array();

        foreach ($currencies as $currency) {
            $calcArray[] = $this->investments($currency);
        }

        return $calcArray;
    }
}
