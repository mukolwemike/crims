<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Crims\Commission;

trait CommissionScheduleTrait
{
    protected static function bootCommissionScheduleTrait()
    {
        static::created(function ($model) {
            $model = $model->fresh();

            if (isLocal()) {
                return;
            }

            sendToQueue(function () use ($model) {
                $model->generateOverrides();
            }, null, false, config('queue.priority.low'), [], 30);
        });

        static::updated(function ($model) {
            $model = $model->fresh();

            sendToQueue(function () use ($model) {
                $model->generateOverrides();
            }, null, false, config('queue.priority.low'), [], 30);
        });

        static::deleted(function ($model) {
            sendToQueue(function () use ($model) {
                $model->overrideSchedules()->delete();
            }, null, false, config('queue.priority.low'), [], 30);
        });
    }
}
