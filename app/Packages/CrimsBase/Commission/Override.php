<?php
/**
 * Date: 08/02/2017
 * Time: 13:21
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Commission;

use App\Cytonn\Models\CommissionRecipientType;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Crims\Base\ModelMapper;
use Crims\FundManager;
use Crims\Investments\Commission\Models\OverrideStructure;
use Crims\Investments\Commission\Models\Recipient;
use Cytonn\Investment\Reports\Entities\CommissionRecipient;
use Illuminate\Support\Collection;

/**
 * Class Override
 *
 * @package Crims\Commission
 */
class Override
{
    /**
     * @var
     */
    protected $recipient;
    /**
     * @var Carbon
     */
    private $date;
    private $type;
    private $startDate;
    private $overrideStructure = null;
    private $real_estate_overrides_start_date = '2018-01-01';

    /**
     * @var FundManager
     */
    //    private $fundManager;

    /**
     * Override constructor.
     *
     * @param Recipient $recipient
     * @param Carbon $date
     * @param $type
     * @param Carbon $startDate
     */
    public function __construct(Recipient $recipient, Carbon $date, $type, Carbon $startDate)
    {
        $this->recipient = $recipient;
        //        $this->fundManager = $fundManager;
        $this->date = $date;
        $this->type = $type;
        $this->startDate = $startDate;
    }

    /**
     * @return Carbon
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @param \Closure $commissionCalculator
     * @param Carbon $date
     * @param $type
     * @return int
     */
    public function calculate(\Closure $commissionCalculator)
    {
        $rate = $this->getOverrideRate();

        if ($rate == 0) {
            return 0;
        }

        return $this->override($commissionCalculator, $rate);
    }

    private function getStructure($type, $date)
    {
        return ModelMapper::get(OverrideStructure::class)->where('slug', $type)
            ->where('date', '<=', $date)
            ->latest('date')
            ->first();
    }

    public function getOverrideRate(Carbon $date = null, $type = null, $rankId = null)
    {
        $recipient = $this->recipient;

        $type = $type ? $type : $this->type;

        $date = $date ? $date : $this->date;

        $rank = $rankId ? $rankId : $recipient->rank_id;

        $structure = $this->getStructure($type, $date);

        if (!$structure) {
            return 0;
        }

        $rate = $structure->rates()->where('rank_id', $rank)->first();

        if (!$rate) {
            return 0;
        }

        return $rate->rate;
    }

    /**
     * @param $calc
     * @return mixed
     */
    protected function override($calc, $rate = null)
    {
        $reports = $this->reports($this->recipient);

        return $reports->sum(
            function ($report) use ($rate, $calc) {
                $dates = $this->getOverrideDates($this->recipient, $report);

                $commission = $calc($report, $dates['start'], $dates['end']);

                return ($rate / 100) * $commission;
            }
        );
    }

    public function getOverrideRates(Carbon $start = null, Carbon $end = null)
    {
        $positions = $this->recipient->commissionRecipientPositions()->orderBy('start_date')
            ->get();

        $ratesArray = array();

        foreach ($positions as $index => $position) {
            $posStart = Carbon::parse($position->start_date);

            $posEnd = Carbon::parse($position->end_date);

            $startTrue = true;

            if ($start) {
                $startTrue = $posEnd >= $start;
            }

            if ($startTrue && $posStart <= $end) {
                $positionStart = Carbon::parse($position->start_date);

                if ($positionStart < $start) {
                    $positionStart = $start;
                }

                $positionEnd = is_null($position->end_date) ? $end : Carbon::parse($position->end_date);

                if ($positionEnd > $end) {
                    $positionEnd = $end;
                }

                $ratesArray[] = [
                    'start' => $positionStart,
                    'end' => $positionEnd,
                    'rate' => $this->getOverrideRate($positionEnd, null, $position->rank_id)
                ];
            }
        }

        return $ratesArray;
    }

    /**
     * Recursively obtain all FAs who report to the given fa
     *
     * @param  $recipient
     * @param  Carbon $date
     * @return mixed
     */
    public function reports($recipient)
    {
//        return  ModelMapper::get(Recipient::class)->where('id', 234)->get();
//        Possible improvement area; Remove anybody who is currently a staff or ifa; Relies on correctness of data
        if (!in_array($recipient->recipient_type_id, [2, 4, 5])
            || in_array($recipient->rank_id, [5, 6]) || $recipient->active != 1) {
            return new Collection();
        }

        $dates = null;

        $recursion = function ($recipient, $dates) use (&$recursion) {
            $accumulator = new Collection();

            $direct = $this->directReports($recipient, $dates);

            foreach ($direct as $one) {
                $accumulator->push($one);

                $dates = $this->getOverrideDates($recipient, $one);

                if ($one->rank_id < $recipient->rank_id) {
                    $next = $recursion($one, $dates);

                    $next->each(function ($n) use (&$accumulator) {
                        $accumulator->push($n);
                    });
                }
            }

            return $accumulator->unique('id');
        };

        $reports = $recursion($recipient, $dates);

        $structure = $this->getStructure($this->type, $this->date);

        if ($structure && $structure->inclusive) {
            $reports->push($recipient);
        }

        return $reports;
    }

    /**
     * @param $recipient
     * @return Collection
     */
    protected function directReports($recipient, $dates)
    {
        $faStart = $recipient->repo->getFaStartDate();

        $m = ModelMapper::get(Recipient::class);

        return $m->whereIn('recipient_type_id', [2, 4, 5])->where(function ($m) use ($recipient, $faStart, $dates) {
            return $m->where('reports_to', $recipient->id)->orWhereHas(
                'commissionRecipientPositions',
                function ($q) use ($recipient, $faStart, $dates) {
                    $q->where('reports_to', $recipient->id)->whereIn('recipient_type_id', [2, 4, 5]);

                    if ($faStart) {
                        $q->where('start_date', '>=', $faStart);
                    }

                    if ($dates) {
                        if (isset($dates['end']) && !is_null($dates['end'])) {
                            $q->where('end_date', '<=', $dates['end']);
                        }

                        if (isset($dates['start']) && !is_null($dates['start'])) {
                            $q->where('start_date', '>=', $dates['start']);
                        }
                    }
                }
            );
        })->with('commissionRecipientPositions')->get();
    }

    /*
     * Get the real estate override start date
     */
    public function getRealEstateOverrideStartDate(CommissionRecipientType $type)
    {
        $structure = $this->getStructure($this->type, $this->date);

        $overridePercentage = $type->realestateOverridePercentages()->where('structure_id', $structure->id)
            ->first();

        return Carbon::parse($overridePercentage->start_date);
    }

    /**
     * @param $supervisor
     * @param $fa
     * @return array
     * @throws CrimsException
     */
    public function getOverrideDates($supervisor, $fa)
    {
        try {
            if ($this->type == 'real_estate') {
                $recipientType = CommissionRecipientType::findOrFail($fa->recipient_type_id);

                $startDate = $this->getRealEstateOverrideStartDate($recipientType);
            } else {
                $startDate = null;
            }

            $lastDate = $this->getSupervisoryEndDate($supervisor);

            $endDate = $this->date;

            if ($lastDate && $lastDate < $endDate) {
                $endDate = $lastDate;
            }

            $recipientPosition = $fa->commissionRecipientPositions()->where('reports_to', $supervisor->id)
                ->orderBy('start_date', 'DESC')->first();

            if ($recipientPosition) {
                $endDate = $this->getEndDate($recipientPosition, $endDate, $supervisor);

                $supervisoryStartDate = $this->getSupervisoryStartDate($supervisor);

                $startDate = $this->getStartDate($recipientPosition, $startDate, $supervisor, $supervisoryStartDate);

                if ($this->type == 'real_estate'
                    && Carbon::parse($startDate)->lte(Carbon::parse($this->real_estate_overrides_start_date))
                ) {
                    $startDate = Carbon::parse($this->real_estate_overrides_start_date);
                }
            }

            return [
                'start' => $startDate,
                'end' => $endDate
            ];
        } catch (\Exception $e) {
            return [
                'start' => $startDate,
                'end' => $endDate
            ];
//            throw new CrimsException(
//                "Error in generation of override dates, Supervisor - "
//                . $supervisor->id . ' - Report : ' . $fa->id
//            );
        }
    }

    private function getSupervisoryEndDate($supervisor)
    {
        $lastSupervisoryPosition = $supervisor->commissionRecipientPositions()
            ->whereHas('rank', function ($q) {
                $q->whereHas('rates', function ($q) {
                    $q->where('structure_id', $this->getStructure($this->type, $this->date)->id);
                });
            })->orderBy('start_date', 'DESC')->first();

        if ($lastSupervisoryPosition && $lastSupervisoryPosition->end_date) {
            return Carbon::parse($lastSupervisoryPosition->end_date);
        }

        return null;
    }

    /**
     * @param $supervisor
     * @return static
     */
    private function getSupervisoryStartDate($supervisor)
    {
        $firstSupervisoryPosition = $supervisor->commissionRecipientPositions()
            ->whereHas('rank', function ($q) {
                $q->whereHas('rates', function ($q) {
                    $q->where('structure_id', $this->getStructure($this->type, $this->date)->id);
                });
            })->orderBy('start_date', 'ASC')->first();

        if (is_null($firstSupervisoryPosition)) {
            $firstSupervisoryPosition = $supervisor->commissionRecipientPositions()
                ->orderBy('start_date', 'ASC')->first();
        }

        return Carbon::parse($firstSupervisoryPosition->start_date);
    }

    /**
     * @param $position
     * @param $startDate
     * @param $supervisor
     * @param $supervisoryStartDate
     * @return static
     */
    private function getStartDate($position, $startDate, $supervisor, $supervisoryStartDate)
    {
        //Start Date
        //Get the latest record for the fa and sup combination
        //Recursively go back till you get the first fa and sup combination
        //Check if there are other sup of the same role before that
        //If there are use the end date of the last one
        //Else use the default


        //Recursively go back till 1) You get to a position that is before the supervisory start date
        //2) Position where we have a supevisor that was the same rank as the current supervisor

        $firstPosition = $this->getFirstRecipientPosition($position, $supervisor, $supervisoryStartDate);

        $firstFaPosition = $position->commissionRecipient->commissionRecipientPositions()
            ->orderBy('start_date', 'ASC')->first();

        if ($firstFaPosition->id != $firstPosition->id) {
            $positionStart = Carbon::parse($firstPosition->start_date);

            if ($positionStart >= $startDate) {
                $startDate = $positionStart;
            }
        }

        return $startDate;
    }

    /*
     * Recursively look for the first recipient position for the supervisor and fa combination
     */
    private function getFirstRecipientPosition($position, $supervisor, $supervisoryStartDate)
    {
        $recipientPosition = $position->commissionRecipient->commissionRecipientPositions()
            ->where('start_date', '<', $position->start_date)
            ->orderBy('start_date', 'DESC')->first();

        if (is_null($recipientPosition) || $recipientPosition && is_null($recipientPosition->reports_to)) {
            return $position;
        }

        if ($recipientPosition->reports_to == $supervisor->id
            && Carbon::parse($recipientPosition->start_date) >= $supervisoryStartDate
        ) {
            return $this->getFirstRecipientPosition($recipientPosition, $supervisor, $supervisoryStartDate);
        } else {
            $supervisorPosition = $recipientPosition->reportsTo->commissionRecipientPositions()
                ->where('start_date', '<=', $recipientPosition->start_date)
                ->orderBy('start_date', 'DESC')
                ->first();

            if (is_null($supervisorPosition)) {
                $supervisorPosition = $recipientPosition->reportsTo->commissionRecipientPositions()
                    ->where('start_date', '>=', $recipientPosition->start_date)
                    ->orderBy('start_date', 'ASC')
                    ->first();
            }

//            dd([
//               'recepient' =>$recipientPosition,
//               'super'  => $supervisorPosition,
//                'start' => $supervisoryStartDate,
//                'suppprant' => $supervisorPosition->rank_id,
//                'supr' => $supervisor->rank_id,
//                'recl' => $supervisorPosition->rank->level,
//                'supl' => $supervisor->rank->level
//            ]);


            if ($supervisor
                && $supervisorPosition
                && Carbon::parse($recipientPosition->start_date) >= $supervisoryStartDate &&
                $supervisorPosition->rank_id != $supervisor->rank_id &&
                $supervisorPosition->rank->level < $supervisor->rank->level
            ) {
                return $this->getFirstRecipientPosition($recipientPosition, $supervisor, $supervisoryStartDate);
            } else {
                return $position;
            }
        }
    }

    /**
     * Get an override end date
     * @param $position
     * @param $endDate
     * @param $supervisor
     * @return static
     */
    private function getEndDate($position, $endDate, $supervisor)
    {
        //End Date
        //Based on the passed position recursively check if there are other
        //positions for the same rank. If there are use their end date

        $lastPosition = $this->getComingRecipientPositions($position, $supervisor);

        if ($lastPosition->end_date) {
            $positionEnd = Carbon::parse($lastPosition->end_date);

            if ($positionEnd <= $endDate) {
                $endDate = $positionEnd;
            }
        }

        return $endDate;
    }

    /*
    * Recursively get recipient positions for coming supervisors
    */
    private function getComingRecipientPositions($position, $supervisor)
    {
        $recipientPosition = $position->commissionRecipient->commissionRecipientPositions()
            ->where('start_date', '>', $position->start_date)
            ->orderBy('start_date', 'ASC')->first();

        if (is_null($recipientPosition) || $recipientPosition && is_null($recipientPosition->reports_to)) {
            return $position;
        }

        if ($recipientPosition->reports_to == $supervisor->id) {
            $this->getComingRecipientPositions($recipientPosition, $supervisor);
        } else {
            $supervisorPosition = $recipientPosition->reportsTo->commissionRecipientPositions()
                ->where('start_date', '<=', $recipientPosition->start_date)
                ->orderBy('start_date', 'DESC')
                ->first();

            if (is_null($supervisorPosition)) {
                $supervisorPosition = $recipientPosition->reportsTo->commissionRecipientPositions()
                    ->where('start_date', '>=', $recipientPosition->start_date)
                    ->orderBy('start_date', 'ASC')
                    ->first();
            }

            return $supervisorPosition->rank_id != $supervisor->rank_id
            && $recipientPosition->rank->level < $supervisor->rank->level ?
                $this->getComingRecipientPositions($recipientPosition, $supervisor) :
                $position;
        }
    }
}
