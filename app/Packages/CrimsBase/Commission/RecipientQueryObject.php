<?php
/**
 * Date: 06/02/2017
 * Time: 12:55
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Commission;

use Crims\Base\Clients\Client;
use Crims\Base\ModelMapper;
use Crims\Base\Models\Currency;
use Crims\Investments\Commission\Models\Recipient;

class RecipientQueryObject
{
    protected $recipient;

    protected $currency;

    /**
     * RecipientQueryObject constructor.
     * @param Recipient $recipient
     * @param Currency|null $currency
     */
    public function __construct(Recipient $recipient, Currency $currency = null)
    {
        $this->recipient = $recipient;

        $this->currency = $currency;
    }

    public function clients()
    {
        $client = ModelMapper::get(Client::class);

        $query =  $client::whereHas(
            'investments',
            function ($investments) {
                $investments->whereHas(
                    'commission',
                    function ($commission) {
                        $commission->where('recipient_id', $this->recipient->id);
                    }
                )
                ->currency($this->currency);
            }
        )->orWhereHas(
            'unitHoldings',
            function ($holdings) {
                $holdings->whereHas(
                    'commission',
                    function ($commission) {
                        $commission->where('recipient_id', $this->recipient->id);
                    }
                );
            }
        )->orWhereHas(
            'unitFundPurchases',
            function ($fundPurchase) {
                $fundPurchase->whereHas(
                    'unitFundCommission',
                    function ($commission) {
                        $commission->where('commission_recipient_id', $this->recipient->id);
                    }
                );
            }
        )->orWhereHas(
            'shareHolders',
            function ($holder) {
                $holder->whereHas(
                    'sharePurchases',
                    function ($purchase) {
                        $purchase->whereHas(
                            'commissionSchedule',
                            function ($schedule) {
                                $schedule->where('commission_recipient_id', $this->recipient->id);
                            }
                        );
                    }
                );
            }
        );

        return clone $query;
    }
}
