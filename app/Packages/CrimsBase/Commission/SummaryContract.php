<?php
/**
 * Date: 09/02/2017
 * Time: 09:04
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-base
 * Cytonn Technologies
 */

namespace Crims\Commission;

interface SummaryContract
{
    public function total(); //total less any deductions

    public function override(); //overrides

    public function finalCommission(); //total + overrides
}
