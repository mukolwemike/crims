<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Packages\CrimsBase\Commission;

use App\Cytonn\Models\CommissionOverrideStructure;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientRank;
use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use Carbon\Carbon;

trait OverrideTrait
{
    public function getStructure($type, Carbon $date)
    {
        return CommissionOverrideStructure::where('slug', $type)
            ->with(['rates'])
            ->where('date', '<=', $date)->latest('date')->first();
    }

    public function getOverrideRate(CommissionOverrideStructure $structure, CommissionRecipientRank $rank)
    {
        return $structure->rates->where('rank_id', $rank->id)->first();
    }

    private function calculateOverrideAmount($amount, $rate)
    {
        return round($amount * $rate / 100, 2);
    }

    private function getDescription($scheduleId, $description, $rate)
    {
        return $description . ' Override' . ' ; Schedule Id : ' . $scheduleId . ' ; Rate : ' . $rate;
    }

    private function getOverrideSupervisorAndRate(
        CommissionRecepient $recepient,
        Carbon $date,
        CommissionOverrideStructure $structure,
        $inclusive
    ) {
        if ($inclusive == 1) {
            $supervisor = $recepient;
            $recipientPosition = null;
        } else {
            $recipientPosition = null;
            $recipientPosition = $recepient->repo->getRecipientPositionByDate($date);
            $supervisor = $recipientPosition ? $recipientPosition->reportsTo : null;
        }

        if (is_null($supervisor)) {
            return null;
        }

        $rank = $recipientPosition ? $recipientPosition->supervisorRank : null;

        $rank = $rank ? $rank : $supervisor->repo->getRecipientRankAsAt($date);

        if (is_null($rank)) {
            return null;
        }

        $rate = $this->getOverrideRate($structure, $rank);

        if (is_null($rate) || $rate->rate == 0) {
            return null;
        }

        return (object) [
            'supervisor' => $supervisor,
            'rate' => $rate
        ];
    }
}
