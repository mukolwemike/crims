<?php
/**
 * Date: 9/30/15
 * Time: 3:41 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Exceptions;

use Cytonn\Authorization\Permission;

/**
 * Exception raised when a user does not have access to an item
 * Class AuthorizationDeniedException
 *
 * @package Cytonn\Exceptions
 */
class AuthorizationDeniedException extends \Exception
{
    public $action;

    /**
     * AuthorizationDeniedException constructor.
     */
    public function __construct($action)
    {
        $this->action = $action;
        parent::__construct();
    }

    public function render()
    {
        $message = 'Access denied: '.$this->generateUserMessage();

        if (request()->wantsJson()) {
            return response()->json([
                'error' => "authorization_denied",
                'message' => $message
            ], 403);
        }

        \Flash::error($message);

        return take_back($this, false);
    }

    private function generateUserMessage()
    {
        $permission = Permission::where('name', $this->action)->first();

        if (is_null($permission)) {
            return "Unknown action: $this->action";
        } elseif ($permission->description) {
            return 'You cannot '.strtolower($permission->description);
        }

        return "You have no access to: $this->action";
    }
}
