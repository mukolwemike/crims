<?php
/**
 * Date: 06/12/2016
 * Time: 12:54
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Exceptions;

class CRIMSGeneralException extends \Exception
{
    public $message;

    public $dontReport = true;

    /**
     * ClientInvestmentException constructor.
     *
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    public function render()
    {
        if (request()->wantsJson()) {
            return response()->json(['status'=>'error', 'message'=>$this->getMessage()], 400);
        }

        return take_back($this);
    }
}
