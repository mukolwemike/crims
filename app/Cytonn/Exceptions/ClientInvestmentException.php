<?php
/**
 * Date: 8/17/15
 * Time: 12:57 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Exceptions;

class ClientInvestmentException extends CRIMSGeneralException
{
}
