<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\SearchOperations;

use Elasticquent\ElasticquentTrait;
use Illuminate\Support\Facades\Log;

trait ElasticOperationsTrait
{
    /*
     * Get the main ela
     */
    use ElasticquentTrait;

    /*
     * Override the default model boot call to allow for indexing
     * of model data on the different events
     */
    protected static function bootElasticOperationsTrait()
    {
        static::created(
            function ($model) {
                try {
                    $model = $model->fresh();

                    if ($model) {
                        $model->addToIndex();
                    }
                } catch (\Exception $e) {
                }
            }
        );

        static::updated(
            function ($model) {
                try {
                    $model = $model->fresh();

                    if ($model) {
                        $model->updateIndex();
                    }
                } catch (\Exception $e) {
                }
            }
        );

        static::deleted(
            function ($model) {
                try {
                    $model->removeFromIndex();
                } catch (\Exception $e) {
                }
            }
        );
    }

    /*
     * Basic Search in Elasticsearch
     */
    public static function search($term = null)
    {
        $instance = new static;

        $params = $instance->getBasicEsParams();

        $params['size'] = 1000;

        $params['body']['query']['match']['_all'] = $term;

        $result = $instance->getElasticSearchClient()->search($params);

        return static::hydrateElasticsearchResult($result);
    }

    /*
     * Search while sorting by id
     */
    public static function sortedSearchById($term = null)
    {
        $instance = new static;

        $params = $instance->getBasicEsParams();

        $params['size'] = 1000;

        $params['body']['sort'] = 'id';

        $params['body']['query']['match']['_all'] = $term;

        $result = $instance->getElasticSearchClient()->search($params);

        return static::hydrateElasticsearchResult($result);
    }

    /*
     * Search while sorting by another column
     */
    public static function sortedSearch($term = null, $sort = null, $direction = 'asc')
    {
        $instance = new static;

        $params = $instance->getBasicEsParams();

        $params['size'] = 1000;

        if (! is_null($sort)) {
            $sortArray = [
                $sort => [
                    'order' => $direction
                ]
            ];
        } else {
            $sortArray = 'id';
        }

        $params['body']['sort'] = $sortArray;

        $params['body']['query']['match']['_all'] = $term;

        $result = $instance->getElasticSearchClient()->search($params);

        return static::hydrateElasticsearchResult($result);
    }
}
