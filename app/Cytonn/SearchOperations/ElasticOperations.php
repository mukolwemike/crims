<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\SearchOperations;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientTransactionApproval;

class ElasticOperations
{
    /*
     * Get the tables available for indexing
     */
    protected $models = [
        ClientBank::class,
        ClientTransactionApproval::class
    ];

    /*
     * First we create the index; Should only be run once
     */
    public function createIndex()
    {
        try {
            $this->createCurlIndex();

            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }

    /*
     * Index all the tables
     */
    public function indexAllTables()
    {
        $this->createIndex();

        foreach ($this->models as $model) {
            $this->indexModel($model);
        }
    }

    /*
     * ReIndex all the tables
     */
    public function reIndexAllTables()
    {
        foreach ($this->models as $model) {
            $this->reIndexModel($model);
        }
    }

    /*
     * Index the passed model
     */
    public function indexModel($model)
    {
        $model::putMapping($ignoreConflicts = true);

        $model::addAllToIndex();
    }

    /*
     * Reindex the passed model
     */
    public function reIndexModel($model)
    {
        $model::reindex();
    }

    /**
     * Create an index using curl
     */
    public function createCurlIndex()
    {
        $this->deleteCurlIndex();

        $url = env('ELASTICSEARCH_SERVER') . '/' . config('elasticquent.default_index');

        $table = env('ELASTICSEARCH_TABLE');

        exec(
            "curl -XPUT \"" . $url . "\" -d'
        {
           \"settings\": {
              \"analysis\": {
                 \"filter\": {
                    \"nGram_filter\": {
                       \"type\": \"edge_ngram\",
                       \"min_gram\": 2,
                       \"max_gram\": 10,
                       \"token_chars\": [
                          \"letter\",
                          \"digit\",
                          \"punctuation\",
                          \"symbol\"
                       ]
                    }
                 },
                 \"analyzer\": {
                    \"nGram_analyzer\": {
                       \"type\": \"custom\",
                       \"tokenizer\": \"whitespace\",
                       \"filter\": [
                          \"lowercase\",
                          \"asciifolding\",
                          \"nGram_filter\"
                       ]
                    },
                    \"whitespace_analyzer\": {
                       \"type\": \"custom\",
                       \"tokenizer\": \"whitespace\",
                       \"filter\": [
                          \"lowercase\",
                          \"asciifolding\"
                       ]
                    }
                 }
              }
           },
           \"mappings\": {
              \"" . $table . "\": {
                 \"_all\": {
                    \"analyzer\": \"nGram_analyzer\",
                    \"search_analyzer\": \"whitespace_analyzer\"
                 }
              }
           }
        }'"
        );
    }

    /*
     * Delete an index using curl
     */
    public function deleteCurlIndex()
    {
        $url = env('ELASTICSEARCH_SERVER') . '/' . config('elasticquent.default_index');

        exec("curl -XDELETE '". $url ."?pretty'");
    }
}
