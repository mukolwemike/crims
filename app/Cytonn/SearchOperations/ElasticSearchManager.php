<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\SearchOperations;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Events\ElasticSearch\DailyElasticSearchIndex;

class ElasticSearchManager
{
    /**
     * Perform reindexing of the elastic search tables
     */
    public static function performReindex()
    {
        $models = [
            'ClientBanks' => ClientBank::class,
            'ClientTransactionApproval' => ClientTransactionApproval::class
        ];

        app(ElasticOperations::class)->createIndex();

        $data = array();

        foreach ($models as $key => $model) {
            $start = microtime(true);

            ini_set('max_execution_time', 300);

            ini_set('memory_limit', '1024M');

            try {
                $model::reindex();

                $data[] = [
                    'table' => $key.' table successfully indexed',
                    'execution_time' => round((microtime(true) - $start), 3) . ' seconds.'
                ];
            } catch (\Exception $e) {
                $data[] = [
                    'table' => $key.' table indexing failed',
                    'execution_time' => round((microtime(true) - $start), 3) . 'seconds.'
                ];
            }
        }

        event(new DailyElasticSearchIndex($data));
    }
}
