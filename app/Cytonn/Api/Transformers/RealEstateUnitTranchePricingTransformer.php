<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealEstateUnitTranchePricing;
use League\Fractal\TransformerAbstract;

/**
 * Class RealEstateUnitTranchePricingTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class RealEstateUnitTranchePricingTransformer extends TransformerAbstract
{
    /**
     * Transform unit tranche pricing to a way consumable in an API
     *
     * @param  RealEstateUnitTranchePricing $pricing
     * @return array
     */
    public function transform(RealEstateUnitTranchePricing $pricing)
    {
        return [
            'id'                =>  $pricing->id,
            'name_and_price'    =>  $pricing->tranche_and_price,
            'tranche_id'        =>  $pricing->size->tranche_id,
            'size_id'           =>  $pricing->size->size_id,
            'payment_plan_id'   => $pricing->payment_plan_id
        ];
    }
}
