<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/26/18
 * Time: 1:51 PM
 */

namespace App\Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientStandingOrder;
use App\Cytonn\Presenters\General\DatePresenter;
use League\Fractal\TransformerAbstract;

class ClientStandingOrderTransformer extends TransformerAbstract
{
    public function transform(ClientStandingOrder $order)
    {
        return [
            'date' => DatePresenter::formatDate($order->date),
            'end_date' => (isset($order->end_date)) ? DatePresenter::formatDate($order->end_date): null,
            'type' => $order->type->name,
            'type_slug' => $order->type->slug,
            'description' => $order->Description,
            'investment' => $order->investment,
            'project' => $order->project,
            'unit' => $order->unit
        ];
    }
}
