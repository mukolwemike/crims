<?php
/**
 * Date: 01/02/2016
 * Time: 12:25 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\System\Channel;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class ClientInvestmentRolloverInstructionTransformer extends TransformerAbstract
{
    public function transform(ClientInvestmentInstruction $instruction)
    {
        $investment = ClientInvestment::withTrashed()->findOrFail($instruction->investment_id);

        $amount_select = $instruction->repo->showAmountSelect();

        $amount = $instruction->repo->calculateAmountAffected();

        return [
            'id' => $instruction->id,
            'client_code' => $investment->client->client_code,
            'client_name' => ClientPresenter::presentFullNames($investment->client_id),
            'investment_amount' => $investment->amount,
            'currency' => $investment->product->currency->code,
            'maturity_date' => $investment->maturity_date->toDateString(),
            'due_date' => $instruction->due_date->toDateString(),
            'instruction_type' => $instruction->type->name,
            'amount_select' => $amount_select,
            'amount' => $amount,
            'origin' => ($instruction->channel_id) ? Channel::find($instruction->channel_id)->slug : (($instruction->user_id) ? 'client' : 'admin'),
            'approved' => $this->approved($instruction),
            'invested' => $instruction->executed(),
            'inactive' => $instruction->inactive == 1 ? true : false,
            'created_at' => $instruction->created_at->toIso8601String()
        ];
    }

    public function approved(ClientInvestmentInstruction $instruction)
    {
        $client = $instruction->investment->client;

        $approvals = $instruction->clientApprovals;

        $approved = ClientInstructionApproval::approved(true)->for(null, $instruction)->get();

        $mandate = $client->present()->getSigningMandate;

        switch ($mandate) {
            case 'All to sign':
                return $approvals->count() == $approved->count();

            case 'Either to sign':
                return $approved->count() ? true : false;

            case 'At Least Two to sign':
                return $approved->count() > 2 ? true : false ;

            case 'Singly':
            default:
                return true;
        }
    }
}
