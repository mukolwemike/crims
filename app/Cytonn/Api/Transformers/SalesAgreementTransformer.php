<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealEstateSalesAgreement;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class SalesAgreementTransformer extends TransformerAbstract
{
    public function transform(RealEstateSalesAgreement $sales_agreement)
    {
        return [
            'id' => $sales_agreement->id,
            'client_code' => $sales_agreement->holding->client->client_code,
            'fullName' => ClientPresenter::presentFullNames($sales_agreement->holding->client_id),
            'project' => $sales_agreement->holding->project ? $sales_agreement->holding->project->name : '',
            'vendor' => $sales_agreement->holding->project ? $sales_agreement->holding->project->vendor : '',
            'unit_number' => $sales_agreement->holding->unit->number,
            'size' => $sales_agreement
                    ->holding
                    ->unit
                    ->size->name . ' ' . $sales_agreement
                    ->holding->unit->type->name,
            'signed_on' => $sales_agreement->signed_on,
            'deleted_at' => $sales_agreement->deleted_at,
            'document_id' => $sales_agreement->document_id,
            'title' => is_null($sales_agreement->document)
                ? null
                : $sales_agreement->document->title
        ];
    }
}
