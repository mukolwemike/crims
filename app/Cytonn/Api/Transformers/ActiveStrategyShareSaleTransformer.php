<?php
/**
 * Date: 07/04/2016
 * Time: 9:09 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ActiveStrategyShareSale;
use League\Fractal\TransformerAbstract;

class ActiveStrategyShareSaleTransformer extends TransformerAbstract
{
    public function transform(ActiveStrategyShareSale $sale)
    {
        return [
            'id'=>$sale->id,
            'number'=>$sale->number,
            'sale_price'=>$sale->sale_price,
            'date'=>$sale->date,
            'target_price'=>$sale->repo->targetPrice(),
            'market_price'=>$sale->repo->marketPrice(),
            'market_value'=>$sale->repo->marketValue(),
            'sale_value'=>$sale->repo->saleValue(),
            'average_purchase_price'=>$sale->repo->averagePurchasePrice(),
            'total_purchase_value'=>$sale->repo->totalPurchaseValue(),
            'average_purchase_value'=>$sale->repo->averagePurchaseValuePerShare(),
            'gain_loss'=>$sale->repo->gainLoss()
        ];
    }
}
