<?php
/**
 * Date: 28/01/2016
 * Time: 8:58 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class ClientTransactionApprovalsTransformer extends TransformerAbstract
{
    public function transform(ClientTransactionApproval $transaction)
    {
        try {
            $client_code = $transaction->client->client_code;
            $client_name = ClientPresenter::presentJointFirstNameLastName($transaction->client_id);
        } catch (\Exception $e) {
            $client_code = '';
            $client_name = '';
        }

        $sent_by = UserPresenter::presentFullNamesNoTitle($transaction->sent_by);

        $approver = UserPresenter::presentFullNamesNoTitle($transaction->approved_by);

        $stage_name = null;

        $stage = $transaction->awaitingStage;
        if ($stage) {
            $stage_name = $stage->name;
        }

        return [
            'id'=>$transaction->id,
            'client_code'=>$client_code,
            'client_name'=>$client_name,
            'transaction_type'=>ucfirst(str_replace('_', ' ', $transaction->transaction_type)),
            'sent_by'=>$sent_by,
            'approved_by'=>$approver,
            'approved_on'=>!is_null($transaction->approved_on)? $transaction->approved_on->toCookieString() :'',
            'sent_on'=>$transaction->created_at->toDateTimeString(),
            'awaiting_stage' => $stage_name
        ];
    }
}
