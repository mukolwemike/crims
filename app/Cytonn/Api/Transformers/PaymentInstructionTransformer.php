<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\BankInstruction;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

/**
 * Class PaymentInstructionTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class PaymentInstructionTransformer extends TransformerAbstract
{
    /**
     * @param BankInstruction $instruction
     * @return array
     */
    public function transform(BankInstruction $instruction)
    {
        $transaction = $instruction->custodialTransaction;
        $payment = $transaction->clientPayment;
        $client = $payment->client;
        $account = $transaction->custodialAccount;
        $sig1 = $instruction->firstSignatory;
        $sig2 = $instruction->secondSignatory;


        return [
            'id'                    =>  $instruction->id,
            'payment_id'            =>  $payment->id,
            'transaction_id'        =>  $transaction->id,
            'account_name'          =>  $account->account_name.' '. $account->alias,
            'client_id'             =>  $client->id,
            'client_code'           =>  $client->client_code,
            'full_name'             =>  ClientPresenter::presentJointFullNames($client->id),
            'description'           =>  $instruction->description,
            'amount'                =>  $instruction->amount,
            'date'                  =>  $instruction->date,
            'first_signatory'       =>   $sig1 ?$sig1->full_name : '',
            'second_signatory'      =>  $sig2 ?$sig2->full_name : '',
            'project_product'       =>  $payment->present()->paymentFor
        ];
    }
}
