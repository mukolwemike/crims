<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientDeduction;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class DeductionsTransformer extends TransformerAbstract
{
    public function transform(ClientInvestmentWithdrawal $withdrawal)
    {
        return [
            'id'                    =>  $withdrawal->investment_id,
            'client_code'           =>  $withdrawal->investment->client->client_code,
            'fullName'              =>  ClientPresenter::presentJointFullNames($withdrawal->investment->client_id),
            'deduction_date'        =>  $withdrawal->date->toDateString(),
            'amount'                =>  $withdrawal->amount,
            'interest_rate'         =>  $withdrawal->investment->interest_rate,
            'currency'              =>  $withdrawal->investment->product->currency->code,
        ];
    }
}
