<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealEstatePaymentPlan;
use League\Fractal\TransformerAbstract;

/**
 * Class RealEstatePaymentPlanTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class RealEstatePaymentPlanTransformer extends TransformerAbstract
{
    /**
     * Transform the payment plans to a way consumable in an API
     *
     * @param  RealEstatePaymentPlan $plan
     * @return array
     */
    public function transform(RealEstatePaymentPlan $plan)
    {
        return [
            'id'   => $plan->id,
            'name' => $plan->name
        ];
    }
}
