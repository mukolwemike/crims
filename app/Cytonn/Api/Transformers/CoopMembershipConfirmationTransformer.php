<?php
/**
 * Date: 19/09/2016
 * Time: 9:00 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CoopPayment;
use App\Cytonn\Models\MembershipConfirmation;
use InvalidArgumentException;
use League\Fractal\TransformerAbstract;

class CoopMembershipConfirmationTransformer extends TransformerAbstract
{
    public function transform($payment)
    {
        if ($payment instanceof CoopPayment) {
            return $this->coop($payment);
        }

        if (!$payment instanceof ClientPayment) {
            throw new InvalidArgumentException('Only client or coop payment allowed');
        }

        $client = $payment->client;

        $total = $client->clientPayments()->ofType('FI')->sum('amount');
        $shares = $client->clientPayments()->ofType('I')->whereNotNull('share_entity_id')->sum('amount');
        $products = $client->clientPayments()->ofType('I')->whereNotNull('product_id')->sum('amount');
        $memberships = $client->clientPayments()->ofType('M')->sum('amount');

        return [
            'total_payment' => abs($total),
            'shares' => abs($shares),
            'products' => abs($products),
            'membership_fees'=> abs($memberships),
            'confirmation'=>MembershipConfirmation::where('client_payment_id', $payment->id)->count() > 0
        ];
    }

    public function coop($payment)
    {
        return [
            'total_payment'=>$payment->repo->totalForClient(),
            'shares'=>$payment->repo->totalForType('shares'),
            'products'=>$payment->repo->totalForType('product'),
            'membership_fees'=>$payment->repo->totalForType('membership'),
            'confirmation'=>MembershipConfirmation::where('payment_id', $payment->id)->count() > 0
        ];
    }
}
