<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use Carbon\Carbon;
use Cytonn\Portfolio\Summary\Analytics;
use League\Fractal\TransformerAbstract;

class PortfolioDepositAnalysisTransformer extends TransformerAbstract
{
    private $currency;

    protected $fundManager;

    protected $date;

    /**
     * @var
     */
    private $total;

    /**
     * PortfolioDepositAnalysisTransformer constructor.
     *
     * @param Currency    $currency
     * @param FundManager $fundManager
     * @param null        $date
     */
    public function __construct(Currency $currency, FundManager $fundManager, Carbon $date, $total)
    {
        $this->currency = $currency;
        $this->fundManager = $fundManager;
        $this->date = $date;
        $this->total = $total;
    }

    public function transform(SubAssetClass $subAssetClass)
    {
        $analytics = (new Analytics($this->fundManager, $this->date))->setCurrency($this->currency);
        $analytics->setFundType($subAssetClass);

        $total = $this->total;

        $amv = $analytics->adjustedMarketValue();

        return [
            'id'                        =>  $subAssetClass->id,
            'fund_type'                 =>  $subAssetClass->name,
            'principal'                 =>  $analytics->costValue(),
            'interest'                  =>  $analytics->grossInterest(),
            'adjusted_gross_interest'   =>  $analytics->adjustedGrossInterest(),
            'amount'                    =>  $amv,
            'market_value'              =>  $analytics->marketValue(),
            'percentage'                =>  $total != 0 ? 100 * $amv /$total : 0
        ];
    }
}
