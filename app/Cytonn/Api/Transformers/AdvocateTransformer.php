<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Advocate;
use League\Fractal\TransformerAbstract;

class AdvocateTransformer extends TransformerAbstract
{
    public function transform(Advocate $advocate)
    {
        return [
            'id'        =>  $advocate->id,
            'name'      =>  $advocate->name,
            'email'     =>  $advocate->email,
            'active'    =>  (bool) $advocate->active,
        ];
    }
}
