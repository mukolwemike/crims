<?php
/**
 * Date: 09/02/2016
 * Time: 3:14 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Notification;
use League\Fractal\TransformerAbstract;

class NotificationTransformer extends TransformerAbstract
{
    public function transform(Notification $notification)
    {
        return [
            'content'=>$notification->content,
            'read'=>$notification->permanent_read,
            'location'=>$notification->location,
            'time'=>$notification->created_at->toDateTimeString()
        ];
    }
}
