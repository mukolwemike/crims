<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientInvestment;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class InvestmentsTransformer extends TransformerAbstract
{
    public function transform(ClientInvestment $investment)
    {
        $gross = $investment->repo->getGrossInterestForInvestment();
        $net = $investment->repo->getNetInterestForInvestment();
        $tax = $gross - $net;
        $withdrawal_date = $investment->withdrawal_date;

        if (!is_null($withdrawal_date)) {
            $withdrawal_date = $withdrawal_date->toDateString();
        }


        //TODO refactor to use includes for client
        return [
            'id'=>$investment->id,
            'client_code'=>$investment->client->client_code,
            'description' => $investment->description,
            'fullName'=>ClientPresenter::presentJointFullNames($investment->client_id),
            'invested_date'=>$investment->invested_date->toDateString(),
            'maturity_date'=>$investment->maturity_date->toDateString(),
            'amount'=>$investment->amount,
            'interest_rate'=>$investment->interest_rate,
            'gross_interest'=>$gross,
            'withholding_tax'=>$tax,
            'net_interest'=>$net,
            'withdrawal'=>$investment->repo->getWithdrawnAmount(),
            'value'=>$investment->repo->getTotalValueOfAnInvestment(),
            'currency'=>$investment->product->currency->code,
            'product' => $investment->product->shortname,
            'withdrawal_date'=>$withdrawal_date,
            'withdrawn'=>$investment->withdrawn,
            'rolled'=>$investment->rolled,
            'type'=>ucfirst($investment->investmentType->name),
            'confirmation'=>$investment->repo->checkIfConfirmationHasBeenSent()
        ];
    }
}
