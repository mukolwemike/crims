<?php

namespace App\Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class USSDApplicationTransformer extends TransformerAbstract
{
    public function transform(ClientFilledInvestmentApplication $application)
    {
        $app = $application->application;

        $fund = $app ? UnitFund::find($app->unit_fund_id) : null;
        $number = count($app->client->unitFundPurchases)
            ? $app->client->unitFundPurchases->sum('number') : 0;

        return [
            'id' => $application->id,

            'name' => ($app->clientType->name == 'individual')
                ? $app->firstname . ' ' . $app->middlename . ' ' . $app->lastname
                : $app->registered_name,

            'client_code' => $app->client->client_code,

            'client_id' => $app->client->id,

            'fund' => $fund->name,

            'amount' => $number ? AmountPresenter::currency($number, false, 2) : null,

            'number' => $number ? AmountPresenter::currency($number, false, 0) . ' units' : null,

            'created_at' => DatePresenter::formatDate($app->created_at),

            'product_name' => $app->unit_fund_id ? $fund->name : '',

            'currency_name' => $app->unit_fund_id ? $fund->currency->code : '',

            'email_indemnity' => (bool)$app->email_indemnity,

            'invested' => count($app->client->unitFundPurchases) ? true : false,

            'approved' => ($app->approval && $app->approval->approved) ? true : false,

            'reminder' => ($app->client->incomplete_application_reminder) ? true : false,

            'show_url' => "/dashboard/unitization/ussd/ussd-applications/{$application->id}",
        ];
    }
}
