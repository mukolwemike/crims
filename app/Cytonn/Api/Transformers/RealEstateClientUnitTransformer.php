<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\UnitHolding;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class RealEstateClientUnitTransformer extends TransformerAbstract
{
    public function transform(UnitHolding $unitHolding)
    {
        return [
            'id' => $unitHolding->unit->id,
            'project' => $unitHolding->unit->project->name,
            'number' => $unitHolding->unit->number,
            'type_name' => $unitHolding->unit->type->name,
            'size_name' => $unitHolding->unit->size->name,
            'client_code' => $unitHolding->client->client_code,
            'client_name' => ClientPresenter::presentJointFullNames($unitHolding->client_id),
            'status' => $this->getStatus($unitHolding),
            'price' => $unitHolding->price(),
            'paid' => $unitHolding->totalPayments(),
            'balance' => $unitHolding->amountRemaining(),
            'unit' => $unitHolding->unit->number,
            'holding_id' => $unitHolding->id,
        ];
    }

    private function getStatus(UnitHolding $unitHolding)
    {
        return (bool)$unitHolding->unit->activeHolding;
    }
}
