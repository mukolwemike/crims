<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharesPurchaseOrder;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

/**
 * Class SharesPurchaseOrderTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class SharesPurchaseOrderTransformer extends TransformerAbstract
{

    /**
     * @param SharesPurchaseOrder $order
     * @return array
     */
    public function transform(SharesPurchaseOrder $order)
    {
        $bought = ShareHolding::where('shares_purchase_order_id', $order->id)->sum('number');

        return [
            'id'                =>  $order->id,
            'buyer_id'          =>  $order->buyer_id,
            'buyer_number'      =>  $order->buyer->number,
            'buyer'             =>  ClientPresenter::presentJointFullNames($order->buyer->client_id),
            'number'            =>  (int)$order->number,
            'bought'            =>  (int)$bought,
            'price'             =>  (double)$order->price,
            'request_date'      =>  DatePresenter::formatDate($order->request_date),
            'matched'           =>  (bool) $order->matched,
            'recipient'         =>  is_null($order->recipient) ? null : $order->recipient->name,
            'rate'              =>  $order->commission_rate,
            'good_till_filled_cancelled'=>  (bool) $order->good_till_filled_cancelled,
            'expiry_date'       =>  $order->expiry_date ? DatePresenter::formatDate($order->expiry_date) : null,
            'cancelled'         =>  (bool) $order->cancelled
        ];
    }
}
