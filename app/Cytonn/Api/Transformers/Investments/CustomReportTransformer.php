<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/8/18
 * Time: 9:01 PM
 */

namespace App\Cytonn\Api\Transformers\Investments;

use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportArgument;
use App\Cytonn\Reporting\Custom\ReportRepository;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Collection;
use League\Fractal\TransformerAbstract;

class CustomReportTransformer extends TransformerAbstract
{
    public function transform(Report $report)
    {
        $repo = new ReportRepository();

        return [
            'id' => $report->id,
            'name' => $report->name,
            'model' => $report->model_name,
            'date' => Carbon::parse($report->updated_at)->toFormattedDateString(),
            'show_url' => $report->path(),
            'edit_url' => $report->path() . "/edit",
            'export_url' => $report->export(),
            'columns' => $report->columns,
            'dynamic_arguments' => $repo->dynamicArguments($report),
            'filters' => $report->filters,
            'dynamic' => count($repo->dynamicArguments($report)) ? true : false
        ];
    }
}
