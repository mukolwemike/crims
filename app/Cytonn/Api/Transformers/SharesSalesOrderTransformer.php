<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

/**
 * Class SharesSalesOrderTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class SharesSalesOrderTransformer extends TransformerAbstract
{

    /**
     * @param SharesSalesOrder $order
     * @return array
     */
    public function transform(SharesSalesOrder $order)
    {
        $sold = ShareSale::where('shares_sales_order_id', $order->id)->sum('number');

        return [
            'id'                =>  $order->id,
            'seller_id'         =>  $order->seller_id,
            'seller_number'     =>  $order->seller->number,
            'seller'            =>  ClientPresenter::presentJointFullNames($order->seller->client_id),
            'number'            =>  (int)$order->number,
            'sold'              =>  (int)$sold,
            'price'             =>  (double)$order->price,
            'request_date'      =>  DatePresenter::formatDate($order->request_date),
            'matched'           =>  (bool) $order->matched,
            'good_till_filled_cancelled'=>(bool) $order->good_till_filled_cancelled,
            'expiry_date'       =>  $order->expiry_date ? DatePresenter::formatDate($order->expiry_date) : null,
            'cancelled'         =>  (bool) $order->cancelled
        ];
    }
}
