<?php
/**
 * Date: 04/07/2016
 * Time: 9:59 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealEstatePayment;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Payments\Payment;
use League\Fractal\TransformerAbstract;

/**
 * Class RealEstatePaymentTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class RealEstatePaymentTransformer extends TransformerAbstract
{


    /**
     * Transform the payment to a way consumable in an API
     *
     * @param  RealEstatePayment $payment
     * @return array
     */
    public function transform(RealEstatePayment $payment)
    {
        $client = $payment->holding->client;
        $unit = $payment->holding->unit;
        $project = $unit->project;
        return [
            'id' => $payment->id,
            'amount' => $payment->amount,
            'client_name' => ClientPresenter::presentJointFullNames($client->id),
            'client_code' => $client->client_code,
            'unit_id' => $unit->id,
            'unit_number' => $unit->number,
            'project_id' => $project->id,
            'date' => $payment->date->toDateString(),
            'confirmation_sent' => (new Payment($payment))->checkBCStatus(),
            'project_name' => $project->name,
        ];
    }
}
