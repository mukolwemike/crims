<?php

namespace Cytonn\Api\Transformers;

use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class DocumentTransformer extends TransformerAbstract
{
    public function transform($document)
    {
        $category_id = null;

        if (! is_null($document->product_id)) {
            $item = $document->product->name;
            $category = 'Investments';
            $category_id = '1';
        } elseif (! is_null($document->project_id)) {
            $item = $document->project->name;
            $category = 'Real Estate';
            $category_id = '2';
        } elseif (! is_null($document->portfolio_investor_id)) {
            $item = $document->portfolioInvestor->name;
            $category = 'Portfolio Investors';
            $category_id = '3';
        } else {
            $item = null;
            $category = null;
        }
        return [
            'id'                    => $document->id,
            'title'                 =>  $document->title,
            'compliance_document'   =>  BooleanPresenter::presentYesNo($document->compliance_document),
            'compliance'            =>  (bool) $document->compliance_document,
            'type'                  =>  $document->type->name,
            'type_id'               =>  $document->type_id . '',
            'uploaded_on'           =>  DatePresenter::formatDate($document->created_at),
            'date'                  =>  DatePresenter::formatDate($document->date),
            'date_raw'              =>  $document->date,
            'category'              =>  $category,
            'item'                  =>  $item,
            'product_id'            =>  $document->product_id,
            'project_id'            =>  $document->project_id,
            'portfolio_investor_id' =>  $document->portfolio_investor_id,
            'category_id'            =>  $category_id
        ];
    }
}
