<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharePurchases;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class ShareHoldingsTransformer extends TransformerAbstract
{
    public function transform(ShareHolding $shareHolding)
    {
        $stampDuty = $shareHolding->stampDuty->sum(function ($duty) {
            return abs($duty->payment->amount);
        });

        return [
            'id' => $shareHolding->id,
            'number' => $shareHolding->shareHolder->number,
            'fullName' => ClientPresenter::presentFullNames($shareHolding->shareHolder->client_id),
            'date' => (new Carbon($shareHolding->date))->toDateString(),
            'number' => $shareHolding->number,
            'share_price' => $shareHolding->purchase_price,
            'share_purchase' => $shareHolding->number * $shareHolding->purchase_price,
            'narrative' => @$shareHolding->payment->description,
            'confirmation' => $shareHolding->repo->checkIfConfirmationHasBeenSent($shareHolding),
            'category' => is_null($shareHolding->entity_id) ? null : $shareHolding->category->name,
            'entity' => is_null($shareHolding->entity_id) ? null : $shareHolding->entity->name,
            'customDuty' => $stampDuty,
            'sent' => $shareHolding->sent ? true : false
        ];
    }

    public function details(SharePurchases $purchases)
    {
        return [
            'id' => $purchases->id,
            'number' => $purchases->shareHolder->number,
            'fullName' => ClientPresenter::presentFullNames($purchases->shareHolder->client_id),
            'date' => Carbon::parse($purchases->date)->toFormattedDateString(),
            'share_number' => $purchases->number,
            'share_price' => $purchases->purchase_price,
            'share_purchase' => $purchases->number * $purchases->purchase_price,
            'narrative' => $purchases->payment ? $purchases->payment->description: null,
            'category' => $purchases->category ? $purchases->category->name : null,
            'entity' => $purchases->entity ? $purchases->entity->name : null,
            'recipient' => $purchases->purchaseOrder->recipient ? $purchases->purchaseOrder->recipient->name : null,
            'commission_rate' => $purchases->purchaseOrder->commission_rate,
            'balance' => AmountPresenter::currency($purchases->shareHolder->sharePaymentsBalance())
        ];
    }
}
