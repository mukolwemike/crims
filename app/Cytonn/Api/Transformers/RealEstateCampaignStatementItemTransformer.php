<?php


namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealestateCampaignStatementItem;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class RealEstateCampaignStatementItemTransformer extends TransformerAbstract
{
    public function transform(RealestateCampaignStatementItem $item)
    {
        return [
            'id'=>$item->id,
            'client_name'=>ClientPresenter::presentJointFirstNameLastName($item->client_id),
            'client_code'=>$item->client->client_code,
            'client_id'=>$item->client->id,
            'added_by'=>UserPresenter::presentFullNamesNoTitle($item->added_by),
            'added_on'=>(new Carbon($item->added_on))->toDateTimeString(),
            'sent'=>$item->sent
        ];
    }
}
