<?php
/**
 * Date: 18/05/2016
 * Time: 5:35 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\ClientPresenter;

class RealEstateClientsTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        return [
            'id'        =>  (int)$client->id,
            'code'      =>  $client->client_code,
            'fullName'  =>  ClientPresenter::presentJointFullNames($client->id),
            'units'     => $client->unitHoldings()->where('active', 1)->count()
        ];
    }
}
