<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Payments\Interest;
use League\Fractal\TransformerAbstract;

/**
 * Class RealEstateScheduledPaymentTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class RealEstateScheduledPaymentTransformer extends TransformerAbstract
{
    /**
     * Transform the payment schedule to a way consumable in an API
     *
     * @param  RealEstatePaymentSchedule $schedule
     * @return array
     */
    public function transform(RealEstatePaymentSchedule $schedule)
    {
        $client = $schedule->holding->client;
        $unit = $schedule->holding->unit;
        $project = $unit->project;

        return [
            'id' => $schedule->id,
            'amount' => $schedule->amount,
            'amount_pending' => $schedule->overduePayments(),
            'client_name' => ClientPresenter::presentJointFullNames($client->id),
            'client_code' => $client->client_code,
            'unit_id' => $unit->id,
            'unit_number' => $unit->number,
            'project_name' => $project->name,
            'date' => (new Carbon($schedule->date))->toDateString(),
            'description' => $schedule->description,
            'paymentType' => $schedule->type->name,
            'paid' => $schedule->paid(),
            'amount_overdue' => $schedule->repo->overdue(),
            'interest_overdue' => (new Interest($schedule->holding))->calculate($schedule, Carbon::today())
        ];
    }
}
