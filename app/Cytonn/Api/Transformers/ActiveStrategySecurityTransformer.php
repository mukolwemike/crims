<?php
/**
 * Date: 05/04/2016
 * Time: 9:57 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Cytonn\Portfolio\ActiveStrategy\PortfolioValuation;
use League\Fractal\TransformerAbstract;

/**
 * Class ActiveStrategySecurityTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class ActiveStrategySecurityTransformer extends TransformerAbstract
{
    /**
     * @param ActiveStrategySecurity $security
     * @return array
     */
    public function transform(PortfolioSecurity $security)
    {
        return [
            'name'=>$security->name,
            'id'=>$security->id,
            'total_number'=>$security->repo->numberOfSharesHeld(),
            'cost'=>$security->repo->averagePrice(),
            'total_cost'=>$this->calculateTotalCost($security),
            'market_value'=>$security->repo->totalCurrentMarketValue(),
            'target_price'=>$security->repo->targetPrice(),
            'market_price'=>$security->repo->marketPrice(),
            'alpha'=>$security->repo->alpha(),
            'portfolio_cost'=>$security->repo->totalCurrentPurchaseValue(),
            'portfolio_value'=>$security->repo->totalCurrentMarketValue(),
            'dividend'=>$security->repo->dividend(),
            'portfolio_return'=>$security->repo->currentGainLoss(),
            'exposure'=>$this->calculateExposure($security)
        ];
    }

    /**
     * @param $security
     * @return float|int
     */
    private function calculateExposure($security)
    {
        try {
            $value = (new PortfolioValuation($security->fundManager))->portfolioValue();
            return 100 * $security->repo->totalCurrentMarketValue() / $value;
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * @param $security
     * @return float|int
     */
    private function calculateTotalCost($security)
    {
        try {
            return $security->repo->totalPurchaseValueHeld() / $security->repo->numberOfSharesBought();
        } catch (\Exception $e) {
            return 0;
        }
    }
}
