<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\UnitHolding;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class RealEstateCancelledUnitTransformer extends TransformerAbstract
{
    public function transform(UnitHolding $holding)
    {
        $unit = $holding->unit;

        return [
            'id' => $holding->unit_id,
            'number' => $unit->number,
            'type_name' => $unit->type->name,
            'size_name' => $unit->size->name,
            'taken' => (bool) $unit->activeHolding,
            'paid' => $paid = $holding->totalPayments(),
            'refunded' => $refunded = $holding->refunds()->sum('amount'),
            'gains' =>  $paid - $refunded,
            'client_name' => ClientPresenter::presentJointFullNames($holding->client_id),
            'client_code' => $holding->client->client_code,
            'client_id' => $holding->client_id,
            'forfeit_reason' => $holding->forfeit_reason
        ];
    }
}
