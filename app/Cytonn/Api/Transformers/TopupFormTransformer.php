<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\Product;
use League\Fractal\TransformerAbstract;

class TopupFormTransformer extends TransformerAbstract
{
    public function transform(ClientTopupForm $form)
    {
        $doc = $form->document;

        $product = Product::findOrFail($form->product_id);

        return [
            'amount'=>$form->amount,
            'tenor'=>$form->tenor,
            'mode_of_payment'=>$form->mode_of_payment,
            'product'=> $product->name,
            'product_longname' => $product->longname,
            'currency' => $product->currency,
            'id'=>$form->uuid,
            'document_id'=>is_null($doc) ? null : $doc->uuid,
            'document_name' => is_null($doc) ? null : $doc->filename,
            'agreed_rate'=>$form->agreed_rate,
        ];
    }
}
