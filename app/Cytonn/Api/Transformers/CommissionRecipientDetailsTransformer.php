<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CommissionRecepient;
use League\Fractal\TransformerAbstract;

class CommissionRecipientDetailsTransformer extends TransformerAbstract
{
    /**
     * @param CommissionRecepient $recipient
     * @return array
     */
    public function transform(CommissionRecepient $recipient)
    {
        return [
            'id' => $recipient->id,
            'name' => $recipient->name,
            'fullname' => $recipient->name . ' - ' . $recipient->email . ' - ' . $recipient->type->name,
            'email' => $recipient->email,
            'supervisor' => $recipient->present()->getCurrentSupervisorName,
            'type' => $recipient->present()->getCurrentType,
            'rank' => $recipient->present()->getCurrentRank,
            'status' => $recipient->present()->getActive
        ];
    }
}
