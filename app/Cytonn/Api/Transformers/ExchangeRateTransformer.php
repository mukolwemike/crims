<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Api\Transformers;

use Cytonn\Models\ExchangeRate;
use League\Fractal\TransformerAbstract;

class ExchangeRateTransformer extends TransformerAbstract
{
    /**
     * @param ExchangeRate $rate
     * @return array
     */
    public function transform(ExchangeRate $rate)
    {
        return [
            'combination' => $rate->present()->currencyCombination,
            'rate' => $rate->rate,
            'date' => $rate->date
        ];
    }
}
