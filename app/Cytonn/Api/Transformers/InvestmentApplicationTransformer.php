<?php
/**
 * Date: 08/02/2016
 * Time: 9:20 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestmentApplication;
use League\Fractal\TransformerAbstract;

class InvestmentApplicationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'client', 'product'
    ];

    protected $defaultIncludes = [
        'client', 'product'
    ];


    public function transform(ClientInvestmentApplication $application)
    {
        return [
            'id' => $application->id,
            'amount' => $application->amount,
            'currency' => $application->product->currency->code,
            'status' => $application->repo->status(),
            'compliance' => $application->repo->compliance(),
            'email_indemnity' => (bool)$application->email_indemnity
        ];
    }

    public function includeClient(ClientInvestmentApplication $application)
    {
        $client = $application->client;

        return $this->item($client, new ClientTransformer());
    }

    public function includeProduct(ClientInvestmentApplication $application)
    {
        $product = $application->product;

        return $this->item($product, new ProductTransformer());
    }

    public function allDetails(Client $client)
    {
        $account = $client->bankAccounts()->latest()->first();

        return [
            'title' => $client->contact->title_id,
            'lastname' => $client->contact->lastname,
            'middlename' => $client->contact->middlename,
            'firstname' => $client->contact->firstname,
            'gender_id' => $client->contact->gender_id,
            'dob' => $client->dob,
            'pin_no' => $client->pin_no,
            'id_or_passport' => $client->id_or_passport,
            'postal_code' => $client->postal_code,
            'postal_address' => $client->postal_address,
            'country_id' => $client->country_id,
            'telephone_home' => $client->telephone_home ? $client->telephone_home : $client->contact->phone,
            'telephone_office' => $client->telephone_office,
            'street' => $client->street,
            'town' => $client->town,
            'method_of_contact_id' => $client->method_of_contact_id,
            'employment_id' => $client->employment_id,

            'employment_other' => $client->employment_other,
            'present_occupation' => $client->contact->title_id,
            'business_sector' => $client->business_sector,
            'employer_name' => $client->employer_name,
            'employer_address' => $client->employer_address,
            'corporate_investor_type' => $client->corporate_investor_type_id,
            'corporate_investor_type_other' => $client->corporate_investor_type_other,

            'registered_name' => $client->contact->corporate_registered_name,
            'trade_name' => $client->contact->corporate_trade_name,
            'registered_address' => $client->contact->registered_address,
            'registration_number' => $client->contact->registration_number,
            'investor_account_name' => ($account) ? $account->account_name : $client->investor_account_name,
            'investor_account_number' => ($account) ? $account->account_number : $client->investor_account_number,

            'contact_person_fname' => $client->contact_person_fname,
            'contact_person_lname' => $client->contact_person_lname,
            'email' => $client->contact->email,

            'investor_bank' => ($account) ? $account->clientBank->id : (int)$client->investor_bank,
            'contact_person_title' => $client->contact_person_title,
            'investor_bank_branch' => ($account) ? $account->clientBankBranch->id : (int)$client->investor_bank_branch,
            'individual' => ($client->type->name == 'individual') ? 1 : 0
        ];
    }
}
