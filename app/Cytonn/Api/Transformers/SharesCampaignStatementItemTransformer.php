<?php


namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\SharesCampaignStatementItem;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class SharesCampaignStatementItemTransformer extends TransformerAbstract
{
    public function transform(SharesCampaignStatementItem $item)
    {
        $holder = $item->shareHolder;
        $client = $holder->client;

        return [
            'id'                =>  $item->id,
            'share_holder_id'   =>  $holder->id,
            'name'              =>  ClientPresenter::presentJointFirstNameLastName($client->id),
            'number'            =>  $holder->number,
            'added_by'          =>  UserPresenter::presentFullNamesNoTitle($item->added_by),
            'added_on'          =>  (new Carbon($item->added_on))->toFormattedDateString(),
            'sent'              =>  $item->sent
        ];
    }
}
