<?php
/**
 * Date: 22/03/2016
 * Time: 4:52 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientTax;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class ClientTaxTransformer extends TransformerAbstract
{
    public function transform(ClientTax $tax)
    {
        return [
            'id'=>$tax->id,
            'client_code'=>$tax->investment->client->client_code,
            'client_name'=>ClientPresenter::presentJointFullNames($tax->investment->client_id),
            'date'=>$tax->investment->withdrawal_date->toDateString(),
            'invested_amount'=>$tax->investment->amount,
            'gross_interest'=> $tax->investment->repo->getFinalGrossInterest(),
            'amount'=>$tax->amount,
            'pin'=>$tax->investment->client->pin_no
        ];
    }
}
