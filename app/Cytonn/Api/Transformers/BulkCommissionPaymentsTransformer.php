<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\BulkCommissionPayment;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class BulkCommissionPaymentsTransformer extends TransformerAbstract
{
    public function transform(BulkCommissionPayment $bcp)
    {
        return [
            'id'                    =>  $bcp->id,
            'start_date'            =>  DatePresenter::formatDate($bcp->start),
            'end_date'              =>  DatePresenter::formatDate($bcp->end),
            'description'           =>  $bcp->description
        ];
    }
}
