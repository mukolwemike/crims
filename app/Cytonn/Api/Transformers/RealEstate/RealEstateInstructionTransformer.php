<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 04/02/2019
 * Time: 12:49
 */

namespace App\Cytonn\Api\Transformers\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class RealEstateInstructionTransformer extends TransformerAbstract
{
    public function transform(RealEstateInstruction $instruction)
    {
        return [
            'id' => $instruction->id,
            'client' => ClientPresenter::presentFullNames($instruction->client->id),
            'project' => $instruction->holding->unit->project->name,
            'unit' => $instruction->holding->unit->number,
            'amount' => $instruction->payload->amount,
            'created' => DatePresenter::formatDate($instruction->created_at),
            'updated' => DatePresenter::formatDate($instruction->updated_at),
            'status' => $instruction->approval ? true : false,
            'document_id' => $instruction->document_id,
            'mode_of_payment' =>$instruction->payload->mode_of_payment,
            'bank_details' => $this->bankDetails($instruction->holding->project)
        ];
    }

    public function bankDetails(Project $project)
    {
        return [
            'bank' => $project->bank_name,
            'branch' => $project->bank_branch_name,
            'account_name' => $project->bank_account_name,
            'account_number' => $project->bank_account_number,
            'swift_code' => $project->bank_swift_code,
        ];
    }
}
