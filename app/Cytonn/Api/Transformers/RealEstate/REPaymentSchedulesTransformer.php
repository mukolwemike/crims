<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 28/01/2019
 * Time: 10:13
 */

namespace App\Cytonn\Api\Transformers\RealEstate;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class REPaymentSchedulesTransformer extends TransformerAbstract
{
    public function transform(RealEstatePaymentSchedule $schedule)
    {
        return [
            'id' => $schedule->id,
            'date' => DatePresenter::formatDate(($schedule->date)),
            'amount' => $schedule->amount,
            'total_amount' => $schedule->amount,
            'description' => $schedule->description,
            'status' => $schedule->paid(),
            'type' => $schedule->type->name,
        ];
    }
}
