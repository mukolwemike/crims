<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Api\Transformers\RealEstate;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class CRMRealEstateScheduleTransformer extends TransformerAbstract
{
    /**
     * Transform the payment schedule to a way consumable in an API
     *
     * @param  RealEstatePaymentSchedule $schedule
     * @return array
     */
    public function transform(RealEstatePaymentSchedule $schedule)
    {
        $client = $schedule->holding->client;
        $unit = $schedule->holding->unit;
        $project = $unit->project;

        return [
            'id' => $schedule->id,
            'client_name' => ClientPresenter::presentJointFullNames($client->id),
            'unit_number' => $unit->number,
            'project_name' => $project->name,
            'date' => (new Carbon($schedule->date))->toDateString(),
            'client_id' => $client->id,
            'holding_id' => $schedule->unit_holding_id
        ];
    }
}
