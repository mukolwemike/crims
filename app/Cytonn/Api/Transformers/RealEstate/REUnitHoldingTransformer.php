<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 15/01/2019
 * Time: 15:31
 */

namespace App\Cytonn\Api\Transformers\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\AmountPresenter;

class REUnitHoldingTransformer extends TransformerAbstract
{
    public function transform($id)
    {
        $holding = UnitHolding::where('unit_id', $id)->first();

        $nextPayment = $holding->repo->unitNextPayment($holding);

        $totalPaid = (int)$holding->repo->unitTotalPaid($holding);

        $unit_price = $holding->price();

        $outstanding_balance = ($unit_price - $totalPaid);

        return [
            'holding_id' => $holding->id,
            'unit_id' => $holding->unit_id,
            'unit_name' => $holding->unit->number,
            'unit_size' => $holding->unit->size->name,
            'payment_plan' => ($holding->paymentPlan) ? $holding->paymentPlan->name : '',
            'unit_price' => AmountPresenter::currency($holding->price(), false, 0),
            'tranche' => ($holding->tranche) ? $holding->tranche->name : '',
            'reservation_date' => Carbon::parse($holding->reservation->date())->toFormattedDateString(),
            'deposit' => $holding->repo->unitDeposit($holding),
            'total_purchase_price' => AmountPresenter::currency($unit_price, false, 0),
            'installments' => $holding->repo->unitInstallments($holding),
            'booking_fees' => $holding->repo->unitBookingFees($holding),
            'next_payment_amount' => !is_null($p = $nextPayment) ?
                AmountPresenter::currency($p->amount, false, 0) : null,
            'next_payment_date' => !is_null($p = $nextPayment) ?
                Carbon::parse($p->date)->toFormattedDateString() : null,
            'total_paid' => AmountPresenter::currency($totalPaid, false, 0),
            'outstanding_balance' => AmountPresenter::currency($outstanding_balance, false, 0),
            'balance' => $outstanding_balance,
            'payments' => $holding->repo->getUnitPaymentActions($holding),
            'project' => ($holding->project) ? $holding->project->name : null,
            'interest' => AmountPresenter::currency($holding->interest(), false, 0),
            'bank_details' => $this->bankDetails($holding->project)
        ];
    }

    public function bankDetails(Project $project)
    {
        return [
            'bank' => $project->bank_name,
            'branch' => $project->bank_branch_name,
            'account_name' => $project->bank_account_name,
            'account_number' => $project->bank_account_number,
            'swift_code' => $project->bank_swift_code,
        ];
    }
}
