<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CoopMembershipForm;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class CoopMembershipFormTransformer extends TransformerAbstract
{
    public function transform(CoopMembershipForm $form)
    {
        return [
            'id'                    => $form->id,
            'client_code'           =>  $form->client_code,
            'fullName'              =>  $form->firstname . ' ' . $form->middlename . ' ' . $form->lastname,
            'email'                 =>  $form->email,
            'phone'                 =>  $form->phone,
            'dob'                   =>  DatePresenter::formatDate($form->dob),
            'id_or_passport'        =>  $form->id_or_passport,
            'approval'           =>  (bool) $form->approval_id,
            'deleted_at'            =>  $form->deleted_at
        ];
    }
}
