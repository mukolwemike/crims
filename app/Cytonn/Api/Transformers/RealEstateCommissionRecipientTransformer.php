<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class RealEstateCommissionRecipientTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class RealEstateCommissionRecipientTransformer extends TransformerAbstract
{
    protected $startDate;

    protected $endDate;

    /**
     * RealEstateCommissionRecipientTransformer constructor.
     *
     * @param $startDate
     * @param $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param CommissionRecepient $recipient
     * @return array
     */
    public function transform(CommissionRecepient $recipient)
    {
        return [
            'id'     => $recipient->id,
            'name'   => $recipient->name,
            'email'  => $recipient->email,
            'amount' => $recipient
                ->reCommissionCalculator($this->startDate, $this->endDate)
                ->summary()
                ->total(),
        ];
    }
}
