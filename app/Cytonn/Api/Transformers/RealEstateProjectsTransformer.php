<?php
/**
 * Date: 18/05/2016
 * Time: 5:35 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Project;
use League\Fractal\TransformerAbstract;

class RealEstateProjectsTransformer extends TransformerAbstract
{
    public function transform(Project $project)
    {
        $total_payments = $project->repo->sumOfPayments();
        $total_refunds = $project->repo->sumOfRefunds();

        return [
            'id' => $project->id,
            'name' => $project->name,
            'code' => $project->code,
            'reserved_count' => $project->repo->countReservedUnits(),
            'total_unit_count' => $project->repo->countNumberOfUnits(),
            'total_payments' => $total_payments,
            'total_refunds' => $total_refunds,
            'remaining' => $total_payments - $total_refunds,
        ];
    }
}
