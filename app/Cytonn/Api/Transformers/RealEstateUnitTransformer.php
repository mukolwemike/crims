<?php
/**
 * Date: 23/05/2016
 * Time: 6:11 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealestateUnit;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Arr;
use League\Fractal\TransformerAbstract;

class RealEstateUnitTransformer extends TransformerAbstract
{
    public function transform(RealestateUnit $unit)
    {
        $client_name = null;
        $client_code = null;
        $client_phone = null;
        $client_email = null;
        $penalty_exempt = null;
        $client_residence = null;


        if ($this->getStatus($unit)) {
            $holding  = $unit->activeHolding;

            $client_code = !is_null($holding->client) ? $holding->client->client_code : null;
            $client_name = $holding->client ? $holding->client->name() : '';
            $penalty_exempt = $holding->penalty_excempt == 0 || is_null($holding->penalty_excempt) ? false : true ;
            $client_phone = $holding->client ? Arr::first($holding->client->getContactPhoneNumbersArray()) : '';
            $client_email = $holding->client ? Arr::first($holding->client->getContactEmailsArray()) : '';
            $client_residence = $holding->client ? $holding->client->residence : '';
        }

        return [
            'id'=>$unit->id,
            'number'=>$unit->number,
            'type_name'=>$unit->type->name,
            'size_name'=>$unit->size->name,
            'client_code'=>$client_code,
            'client_name'=>$client_name,
            'client_phone' => $client_phone,
            'client_residence' => $client_residence,
            'client_email' => $client_email,
            'status'=>$this->getStatus($unit),
            'penalty_exempt'=>$penalty_exempt
        ];
    }

    private function getStatus(RealestateUnit $unit)
    {
        return (bool) $unit->activeHolding;
    }
}
