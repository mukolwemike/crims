<?php

namespace Cytonn\Api\Transformers;

use Carbon\Carbon;
use App\Cytonn\Models\CoopPayment;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

/**
 * Class CoopPaymentTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class CoopPaymentTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'membership_confirmation'
    ];

    /**
     * @param CoopPayment $payment
     * @return array
     */
    public function transform(CoopPayment $payment)
    {
        return [
            'id'                =>  $payment->id,
            'client_id'         =>  $payment->client_id,
            'client_code'       =>  $payment->client->client_code,
            'fullName'          =>  ClientPresenter::presentJointFullNames($payment->client->id),
            'amount'            =>  (double)$payment->amount,
            'date'              =>  (new Carbon($payment->date))->toDateString(),
            'narrative'         =>  $payment->narrative,
        ];
    }

    /**
     * @param CoopPayment $payment
     * @return \League\Fractal\Resource\Item
     */
    public function includeMembershipConfirmation(CoopPayment $payment)
    {
        return $this->item($payment, new CoopMembershipConfirmationTransformer());
    }
}
