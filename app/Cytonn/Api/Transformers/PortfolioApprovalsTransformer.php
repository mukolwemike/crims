<?php
/**
 * Date: 14/03/2016
 * Time: 9:55 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Presenters\InstitutionPresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class PortfolioApprovalsTransformer extends TransformerAbstract
{
    public function transform(PortfolioTransactionApproval $approval)
    {
        return [
            'id' => $approval->id,
            'institution_name' => InstitutionPresenter::presentName($approval->institution_id),
            'type' => ucfirst(str_replace('_', ' ', $approval->transaction_type)),
            'sent_by_name' => UserPresenter::presentFullNames($approval->sent_by),
            'approved_by' => !is_null($approval->approved_by)
                ? UserPresenter::presentFullNamesNoTitle($approval->approved_by) : '',
            'approved_on' => !is_null($approval->approved_on) ? $approval->approved_on->toCookieString() : '',
            'sent_on' => $approval->created_at->toDateTimeString()
        ];
    }
}
