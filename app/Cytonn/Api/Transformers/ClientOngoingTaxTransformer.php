<?php
/**
 * Date: 22/03/2016
 * Time: 7:15 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\ClientPresenter;

class ClientOngoingTaxTransformer extends TransformerAbstract
{
    private $date;
    private $product;

    /**
     * ClientOngoingTaxTransformer constructor.
     *
     * @param $date
     * @param $product
     */
    public function __construct(Carbon $date, Product $product)
    {
        $this->date = $date;
        $this->product = $product;
    }


    public function transform(Client $client)
    {
        $product = $this->product;
        $date = $this->date;

        return [
            'client_code'=>$client->client_code,
            'pin'=>$client->pin_no,
            'tax' => $client->taxrepo->getTotalTaxForProductForMonth($product, $date),
            'name' => ClientPresenter::presentFullNames($client->id),
            'amount' => $client->taxrepo->getTotalInvestedAmountForMonthForProduct($product, $date),
            'gross_interest' => $client->taxrepo->getTotalGrossInterestForProductForMonth($product, $date),
            'interest_rate' => $client->taxrepo->getWeightedRateForProductForMonth($product, $date),
        ];
    }
}
