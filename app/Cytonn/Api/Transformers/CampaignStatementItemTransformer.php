<?php
/**
 * Date: 10/03/2016
 * Time: 12:25 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CampaignStatementItem;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class CampaignStatementItemTransformer extends TransformerAbstract
{
    public function transform(CampaignStatementItem $item)
    {
        return [
            'id'=>$item->id,
            'client_name'=>ClientPresenter::presentJointFirstNameLastName($item->client_id),
            'client_code'=>$item->client->client_code,
            'client_id'=>$item->client->id,
            'added_by'=>UserPresenter::presentFullNamesNoTitle($item->added_by),
            'added_on'=>(new Carbon($item->added_on))->toDateTimeString(),
            'sent'=>$item->sent
        ];
    }
}
