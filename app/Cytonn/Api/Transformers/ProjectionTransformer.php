<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 11/1/17
 * Time: 9:15 AM
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Crims\Investments\Commission\Models\Recipient;
use Cytonn\Investment\Commission\ProjectionsCalculator;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Commissions\Calculator\ReProjectionsCalculator;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class ProjectionTransformer extends TransformerAbstract
{
    protected $recipient;
    protected $months;

    public function __construct(Recipient $recipient, array $months)
    {
        $this->recipient = $recipient;
        $this->months = $months;
    }

    public function transform(Client $client)
    {
        $currency = Currency::where('code', 'KES')->first();

        $commission = collect($this->months)->map(
            function ($month) use ($client, $currency) {
                $inv = (new ProjectionsCalculator($this->recipient, $currency, $month->start, $month->end))
                    ->calculate($client);
                $re = (new ReProjectionsCalculator($this->recipient, $client, $month->start, $month->end))
                    ->reProjections();

                return (object)[
                    'month' => $month->start->startOfMonth()->toDateString(),
                    'on_retainer' => [
                        'investments' => $inv->sum('on_retainer'),
                        'real_estate' => $re->sum('on_retainer'),
                        'total' => ($inv->sum('on_retainer') + $re->sum('on_retainer'))
                    ],
                    'off_retainer' => [
                        'investments' => $inv->sum('off_retainer'),
                        'real_estate' => $re->sum('off_retainer'),
                        'total' => ($inv->sum('off_retainer') + $re->sum('off_retainer'))
                    ]
                ];
            }
        )->all();

        return [
            'id' => $client->uuid,
            'code' => $client->client_code,
            'name' => ClientPresenter::presentFullNames($client->id),
            'projections' => $commission,
            'faName' => $this->recipient->name,
            'off_retainer_totals' => $this->offRetainerTotals($commission),
            'on_retainer_totals' => $this->onRetainerTotals($commission),
        ];
    }

    private function getForMonth(Collection $collection, Carbon $month)
    {
        return $collection->filter(
            function ($item) use ($month) {
                $m = Carbon::parse($item->month);
                return $m->gte($month->copy()->startOfMonth()) && $m->lte($month->copy()->endOfMonth());
            }
        )->first();
    }

    private function getMonths(Carbon $startDate, Carbon $endDate)
    {
        $months = [];

        for ($start = $startDate->copy(); $start->lte($endDate->copy()); $start = $start->addMonthNoOverflow()) {
            $months[] = $start->copy()->startOfMonth();
        }
        return $months;
    }

    public function getTotals($commission)
    {
        $clientTotals = 0;

        $clientTotals += $commission->map(
            function ($comm) {
                return ['total' => $comm->totals];
            }
        )->sum('total');

        return $clientTotals;
    }

    public function getInvTotals($commission)
    {
        $invTotal = 0;

        $invTotal += $commission->map(
            function ($inv) {
                return ['total' => $inv->inv_total];
            }
        )->sum('total');

        return $invTotal;
    }

    public function getReTotals($commission)
    {
        $reTotals = 0;
        $reTotals += $commission->map(
            function ($re) {
                return ['total' => $re->re_total];
            }
        )->sum('total');

        return $reTotals;
    }

    private function offRetainerTotals($commissions)
    {
        return collect($commissions)->map(
            function ($commission) {
                return [
                    'amount' => $commission->off_retainer['total']
                ];
            }
        )->sum('amount');
    }

    private function onRetainerTotals($commissions)
    {
        return collect($commissions)->map(
            function ($commission) {
                return [
                    'amount' => $commission->on_retainer['total']
                ];
            }
        )->sum('amount');
    }
}
