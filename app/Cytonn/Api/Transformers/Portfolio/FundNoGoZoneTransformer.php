<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/11/18
 * Time: 12:28 PM
 */

namespace App\Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\FundNoGoZone;
use League\Fractal\TransformerAbstract;

class FundNoGoZoneTransformer extends TransformerAbstract
{
    public function transform(FundNoGoZone $limit)
    {
        return [
            'id' => $limit->id,
            'unit_fund' => $limit->unitFund->name,
            'security' => $limit->security->name,
            'description' => $limit->description,
            'show_url' => '',
        ];
    }
}
