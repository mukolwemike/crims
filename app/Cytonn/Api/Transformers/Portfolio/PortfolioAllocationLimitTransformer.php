<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/18/18
 * Time: 4:12 PM
 */

namespace App\Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\PortfolioAllocationLimit;
use App\Cytonn\Presenters\General\AmountPresenter;
use League\Fractal\TransformerAbstract;

class PortfolioAllocationLimitTransformer extends TransformerAbstract
{
    public function transform(PortfolioAllocationLimit $allocation)
    {
        return [
            'fund' => $allocation->fund->name,
            'assetClass' => $allocation->subAssetClass->assetClass->name,
            'subAssetClass' => $allocation->subAssetClass->name,
            'limit' => AmountPresenter::currency($allocation->limit),
        ];
    }
}
