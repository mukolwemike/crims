<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 1:00 PM
 */

namespace App\Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Unitization\UnitFundOrder;
use App\Cytonn\Portfolio\Orders\Compliance;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Api\Transformers\Portfolio\FundComplianceBenchmarkLimitsTransformer;
use League\Fractal\TransformerAbstract;

class UnitFundOrderTransformer extends TransformerAbstract
{
    public function transform(UnitFundOrder $order)
    {
        $subAssetClass = $order->portfolioOrder->security->subAssetClass;

        $asset_class = $subAssetClass->assetClass->slug;

        $security = $order->portfolioOrder->Security;

        $value = $this->orderValue($order, $asset_class);

//        $fundCompliance = new Compliance($order->unitFund, $security, $value);

//        $benchmarkLimits = ($fundCompliance->benchmarkLimit())
//            ? (new FundComplianceBenchmarkLimitsTransformer())->transform($fundCompliance->benchmarkLimit())
//            : $fundCompliance->benchmarkLimit() ;

//        $perfomance = $order->unitFund->performances()->latest('date')->first();

        return [
            'id' => $order->id,
            'fund' => $order->unitFund->name,
            'unit_fund_id' => $order->unitFund->id,
            'amount' => $order->amount,
            'shares' => $order->shares,
            'shares_allocated' => ($asset_class == 'equities') ? $this->sharesAllocated($order) : 0,
            'amount_allocated' => ($asset_class !== 'equities') ? $this->amountAllocated($order) : 0,
            'balance' => $this->balance($order, $asset_class),
            'value' => $value,
            'status' => [],
            'warnStatus' => [],
            'aum' => 0,
            'benchmarkLimit' => [],
            'liquidityLimit' => [],
            'noGoZone' => [],
            'custodial_account_id' => $order->custodial_account_id,
            'account_name' => $order->custodialAccount ? $order->custodialAccount->account_name : null,
        ];
    }

    public function sharesAllocated(UnitFundOrder $order)
    {
        $type = $order->portfolioOrder->type->slug;

        if ($type == 'buy') {
            return $this->sharesAllocatedForPurchase($order);
        }

        return $this->sharesAllocatedForSale($order);
    }

    public function sharesAllocatedForPurchase(UnitFundOrder $order)
    {
        return EquityHolding::whereHas('allocation', function ($allocation) use ($order) {
            $allocation->where('portfolio_order_id', $order->portfolioOrder->id);
        })
            ->where('unit_fund_id', $order->unitFund->id)
            ->get()
            ->sum('number');
    }

    public function sharesAllocatedForSale(UnitFundOrder $order)
    {
        return EquityShareSale::whereHas('allocation', function ($allocation) use ($order) {
            $allocation->where('portfolio_order_id', $order->portfolioOrder->id);
        })
            ->where('unit_fund_id', $order->unitFund->id)
            ->get()
            ->sum('number');
    }

    public function amountAllocated(UnitFundOrder $order)
    {
        return DepositHolding::whereHas('allocation', function ($allocation) use ($order) {
            $allocation->where('portfolio_order_id', $order->portfolioOrder->id);
        })
            ->where('unit_fund_id', $order->unitFund->id)
            ->get()
            ->sum('amount');
    }

    public function balance(UnitFundOrder $order, $asset_class)
    {
        return ($asset_class == 'equities')
            ? $order->shares - $this->sharesAllocated($order)
            : $order->amount  - $this->amountAllocated($order);
    }

    public function orderValue(UnitFundOrder $order, $asset_class)
    {
        if ($asset_class == 'equities') {
            return $order->portfolioOrder->cut_off_price * $order->shares;
        }

        return abs($order->amount);
    }
}
