<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use League\Fractal\TransformerAbstract;

class SubAssetClassTransformer extends TransformerAbstract
{
    public function transform(SubAssetClass $subAsset)
    {
        return [
            'id' => $subAsset->id,
            'name' => $subAsset->name,
            'show_url' => $subAsset->path(),
            'edit_url' => $subAsset->path() . "/edit",
        ];
    }
}
