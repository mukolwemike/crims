<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/9/18
 * Time: 10:42 AM
 */

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\FundCompliance;
use App\Cytonn\Models\Portfolio\FundComplianceBenchmark;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class FundComplianceBenchmarkTransformer extends TransformerAbstract
{
    public function transform(FundComplianceBenchmark $benchmark)
    {
        return [
            'id' => $benchmark->id,
            'name' => $benchmark->name,
            'type' => $benchmark->type->name,
            'date' => DatePresenter::formatDate(Carbon::today()),
            'show_url' => $benchmark->path(),
        ];
    }
}
