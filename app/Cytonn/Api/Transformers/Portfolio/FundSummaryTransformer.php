<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class FundSummaryTransformer extends TransformerAbstract
{
    public function transform(DepositHolding $holding, Carbon $date = null)
    {
        $security = $holding->security;

        return [
            'name' => $security->name,
            'principal' => $this->currency($holding->amount),
            'nominal' => $this->currency($holding->nominal_value),
            'rate' => $holding->interest_rate,
            'invested_date' => $holding->invested_date,
            'date' => DatePresenter::formatDate($holding->invested_date),
            'maturity_date' => $holding->maturity_date->toDateString(),
            'purchase_price' => null,
            'current_price' => null,
            'market_value'=> $this->currency($holding->repo->getTotalValueOfAnInvestment()),
            'gross_interest' => $this->currency($holding->repo->getGrossInterestForInvestment()),
            'net_interest' => $this->currency($holding->repo->getNetInterestForInvestment()),
            'withholding_tax' => $this->currency($holding->repo->getWithholdingTaxForInvestment()),
        ];
    }

    public function currency($value)
    {
        return AmountPresenter::currency($value);
    }
}
