<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\BondInterestPaymentSchedule;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class BondInterestPaymentScheduleTransformer extends TransformerAbstract
{
    /**
     * @param BondInterestPaymentSchedule $schedule
     * @return array
     */
    public function transform(BondInterestPaymentSchedule $schedule)
    {
        return [
            'id' => $schedule->id,
            'description' => $schedule->description,
            'amount' => AmountPresenter::currency($schedule->amount),
            'date' => DatePresenter::formatDate($schedule->date)
        ];
    }

}