<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Equities\PortfolioValuation;
use League\Fractal\TransformerAbstract;

class EquitySecurityTransformer extends TransformerAbstract
{
    protected $unitFund;

    public function __construct(UnitFund $unitFund = null)
    {
        $this->unitFund = $unitFund;
    }

    public function transform(PortfolioSecurity $security)
    {
        $analytics = $security->repo->setFund($this->unitFund);

        $subAssetClass = $security->subAssetClass;
        
        return [
            'id' => $security->id,
            'name' => $security->name,
            'code' => $security->code,
            'investor' => ($security->investor)? $security->investor->name: '',
            'asset_class' => ($subAssetClass)? $subAssetClass->assetClass->name: '',
            'sub_asset_class' => ($subAssetClass)? $subAssetClass->name: '',
            'fund_manager' => $security->fundManager->name,
            'fund_manager_id' => $security->fund_manager_id,
            'show_url' => $security->path(),
            'edit_url' => $security->path() . "/edit",
            'total_number'=>$analytics->numberOfSharesHeld(),
            'cost'=>$analytics->averagePrice(),
            'total_cost'=> $this->calculateTotalCost($security),
            'market_value'=>$analytics->totalCurrentMarketValue(),
            'target_price'=>$analytics->targetPrice(),
            'market_price'=>$analytics->marketPrice(),
            'alpha'=>$analytics->alpha(),
            'portfolio_cost'=>$analytics->totalCurrentPurchaseValue(),
            'portfolio_value'=>$analytics->totalCurrentMarketValue(),
            'dividend'=>$analytics->dividend(),
            'portfolio_return'=>$analytics->currentGainLoss(),
            'exposure'=>$this->calculateExposure($security),
            'unit_fund' => ($security->unitFund) ? $security->unitFund->name : null,
        ];
    }
    
    private function calculateExposure($security)
    {
        try {
            return 100 * $security
                    ->repo->totalCurrentMarketValue() / (new PortfolioValuation($security->fundManager))
                    ->portfolioValue();
        } catch (\Exception $e) {
            return 0;
        }
    }

    private function calculateTotalCost($security)
    {
        try {
            return $security->repo->totalPurchaseValueHeld() / $security->repo->numberOfSharesBought();
        } catch (\Exception $e) {
            return 0;
        }
    }
}
