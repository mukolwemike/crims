<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/24/18
 * Time: 1:32 PM
 */

namespace App\Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\WithholdingTax;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class WithholdingTaxTransformer extends TransformerAbstract
{
    public function transform(WithholdingTax $withholdingTax)
    {
        if ($withholdingTax->withdrawal) {
            $investment = $withholdingTax->withdrawal->investment;
            $client = $investment->client;
            $productName = $investment->product->name;
            $principal = $investment->amount;
        } else {
            $sale = $withholdingTax->unitFundSale;
            $client = $sale->client;
            $principal = $sale->number * $sale->price;
            $productName = $sale->unitFund->short_name . ' Unit Sale';
        }

        return [
            'id' => $withholdingTax->id,
            'client' => $client->name(),
            'code' => $client->client_code,
            'amount' => AmountPresenter::currency($withholdingTax->amount),
            'value' => $withholdingTax->amount,
            'type' => ucfirst($withholdingTax->type),
            'date' => Carbon::parse($withholdingTax->date)->toFormattedDateString(),
            'product' => $productName,
            'principal' => AmountPresenter::currency($principal),
            'gross_interest' => AmountPresenter::currency($withholdingTax->gross_interest_amount),
            'net_interest' => AmountPresenter::currency($withholdingTax->net_interest_amount)
        ];
    }

    public function details(WithholdingTax $withholdingTax)
    {
        $principal = $unitSold = $fundName = $productName = '';

        if ($withholdingTax->withdrawal) {
            $investment = $withholdingTax->withdrawal->investment;
            $client = $investment->client;
            $productName = $investment->product->name;
            $principal = $investment->amount;
            $transactionType = $this->getWithdrawalType($withholdingTax->withdrawal);
        } else {
            $sale = $withholdingTax->unitFundSale;
            $client = $sale->client;
            $fundName = $sale->unitFund->name;
            $unitSold = $sale->number * $sale->price;
            $transactionType = $this->getUnitSaleType($withholdingTax->unitFundSale);
        }

        return [
            'Name' => ClientPresenter::presentJointFullNames($client->id),
            'Client Code' => $client->client_code,
            'Client Email' => $client->contact ? $client->contact->email : '',
            'Client Address' => $client->postal_address ? $client->postal_address.'-'.$client->postal_code.' '.$client->town: '',
            'Product' => $productName,
            'Unit Fund' => $fundName,
            'Principal' => (float) $principal,
            'Unit Value Sold' => (float) $unitSold,
            'Action Date' => Carbon::parse($withholdingTax->date)->toDateString(),
            'Type' => ucfirst($withholdingTax->type),
            'Gross Interest' => (float) $withholdingTax->gross_interest_amount,
            'Net Interest' => (float) $withholdingTax->net_interest_amount,
            'W/Tax' => (float) $withholdingTax->amount,
            'Description' => $withholdingTax->description,
            'Status' => $withholdingTax->paid ? 'Paid' : 'Not paid',
            'Nature of Transaction' => $transactionType,
            'Pin of Withholdee' => $client->pin_no,
            'Residential Status' => $client->present()->getResidency,
            'Date of Payment to Withholdee' => \Carbon\Carbon::parse($withholdingTax->date)->format('d/m/Y'),
            'Gross Amount' => (float) $withholdingTax->gross_interest_amount
        ];
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @return string
     */
    private function getWithdrawalType(ClientInvestmentWithdrawal $withdrawal)
    {
        return $withdrawal->withdraw_type == "interest" ? "Interest Payment" : "Withdrawal Payment";
    }

    /**
     * @param UnitFundSale $sale
     * @return string
     */
    private function getUnitSaleType(UnitFundSale $sale)
    {
        return $sale->sale_type == "interest" ? "Interest Payment" : "Units Sale";
    }
}
