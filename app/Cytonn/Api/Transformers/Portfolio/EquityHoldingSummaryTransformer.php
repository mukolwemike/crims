<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\EquityHolding;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class EquityHoldingSummaryTransformer extends TransformerAbstract
{
    public function transform(EquityHolding $holding)
    {
        $security = $holding->security;

        return [
            'name' => $security->name,
            'principal' => null,
            'nominal' => null,
            'rate' => null,
            'invested_date' => $holding->date,
            'date' => DatePresenter::formatDate($holding->date),
            'maturity_date' => null,
            'purchase_price' => $this->currency($holding->cost),
            'current_price' => $this->currency($holding->repo->marketPricePerShare()),
            'market_value'=> $this->currency($holding->repo->marketValue()),
            'value'=> $holding->repo->marketValue(),
        ];
    }
    
    public function currency($value)
    {
        return AmountPresenter::currency($value);
    }

    public function export(EquityHolding $holding)
    {
        $security = $holding->security;

        return [
            'Name' => $security->name,
            'Invested Date' => $holding->date,
            'Date' => DatePresenter::formatDate($holding->date),
            'Purchase Price' => $this->currency($holding->cost),
            'Current Price' => $this->currency($holding->repo->marketPricePerShare()),
            'Market Value'=> $this->currency($holding->repo->marketValue()),
        ];
    }
}
