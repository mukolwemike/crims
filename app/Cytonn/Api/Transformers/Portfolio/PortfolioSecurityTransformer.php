<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use League\Fractal\TransformerAbstract;

class PortfolioSecurityTransformer extends TransformerAbstract
{
    /**
     * @param PortfolioSecurity $security
     * @return array
     */
    public function transform(PortfolioSecurity $security)
    {
        $subAssetClass = $security->subAssetClass;

        $assetClass = $subAssetClass ? $subAssetClass->assetClass : null;

        return [
            'id' => $security->id,
            'fund_manager_id' => $security->fund_manager_id,
            'name' => $security->name,
            'code' => $security->code,
            'investor' => ($security->investor->name)? $security->investor->name: '',
            'investorId' => ($security->investor)? $security->investor->id: '',
            'asset_class' => ($assetClass)? $assetClass->name: '',
            'sub_asset_class' => ($subAssetClass)? $subAssetClass->name: '',
            'slug' => ($subAssetClass)? $subAssetClass->slug: '',
            'fund_manager' => $security->fundManager->name,
            'unit_fund' => ($security->unitFund) ? $security->unitFund->name : null,
            'show_url' => $security->path(),
            'edit_url' => $security->path() . "/edit",
        ];
    }
}
