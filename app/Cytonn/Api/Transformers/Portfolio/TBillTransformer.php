<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\BondHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Cytonn\Portfolio\Bonds\TBill;
use League\Fractal\TransformerAbstract;

class TBillTransformer extends TransformerAbstract
{
    public function transform(BondHolding $bond)
    {
        $tbill = new TBill($bond);
        $security = (new PortfolioSecurity())->find($bond->portfolio_security_id);
        
        return [
            'name' => $security->name,
            'code' => $security->code,
            'type' => $security->subAssetClass->assetClass->name,
            'investor' => $security->investor->name,
            'value_date'  => $bond->value_date,
            'maturity_date'  => $bond->maturity_date,
            'quoted_yield'  => ($bond->quoted_yield)? $bond->quoted_yield: '',
            'face_value'  => $bond->face_value,
            'offer_price' => ($bond->taxable)? $tbill->getOfferPrice() : '',
            'actual_purchase_price' => ($bond->taxable)? $tbill->getActualPrice() : '',
            'return_to_fund' => ($bond->taxable)? $tbill->getReturnToFund() : '',
            'show_url' => $bond->path(),
            'edit_url' => $bond->path() . "/edit",
            'slug' => $security->subAssetClass->slug
        ];
    }
}
