<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use League\Fractal\TransformerAbstract;

class DepositHoldingTransfomer extends TransformerAbstract
{
    public function transform(DepositHolding $deposit)
    {
        $date = new Carbon();
        $security = $deposit->security;

        return [
            'id' => $deposit->id,
            'portfolio_security_id' => $deposit->portfolio_security_id,
            'name' => $security->name,
            'assetClass' => $security->subAssetClass->assetClass->name,
            'subAssetClass' => $security->subAssetClass->name,
            'investor' => $security->investor->name,
            'fundManager' => $security->fundManager->name,
            'security' => $security,

            'invested_date' => $deposit->invested_date->toDateString(),
            'maturity_date' => $deposit->maturity_date->toDateString(),

            'interest_rate' => $deposit->interest_rate,
            'principal' => $this->currency($deposit->amount),

            'balance' => $this->currency($deposit->repo->principal()),
            'net_interest' => $this->currency($deposit->repo->getNetInterestForInvestment()),

            'principal_repayments' => $this->currency(
                $deposit->repo->repayments($date, 'principal_repayment')->sum('amount')
            ),
            'interest_repayments' => $this->currency(
                $deposit->repo->repayments($date, 'interest_repayment')->sum('amount')
            ),
            'interest_balance' => $this->currency($deposit->repo->getNetInterestAfterRepayments()),
            'withholding_tax' => $this->currency($deposit->repo->getWithholdingTaxForInvestment()),
            'total' => $this->currency($deposit->repo->getTotalValueOfAnInvestment()),
            'withdrawn' => $deposit->repo->withdrawn(),
            'rolled' => $this->currency($deposit->rolled),
            'type' => ($deposit->type) ? $deposit->type->name : '',

            'show_url' => $deposit->path(),
            'edit_url' => $deposit->path() . "/edit",
        ];
    }

    public function currency($value)
    {
        return AmountPresenter::currency($value);
    }
}
