<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\BondHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Cytonn\Portfolio\Bonds\TBond;
use League\Fractal\TransformerAbstract;

class TBondTransfomer extends TransformerAbstract
{
    public function transform(BondHolding $bond)
    {
        $tbond = new TBond($bond);
        $security = (new PortfolioSecurity())->find($bond->portfolio_security_id);
        
        return [
            'name'                              => $security->name,
            'code'                              => $security->code,
            'type'                              => $security->subAssetClass->assetClass->name,
            'investor'                          => $security->investor->name,
            'value_date'                        => $bond->value_date,
            'maturity_date'                     => $bond->maturity_date,
            'coupon_rate'                       => $bond->coupon_rate,
            'quoted_yield'                      => $bond->quoted_yield,
            'face_value'                        => $bond->face_value,
            'interest_earned_per_annum'         => $tbond->getInterestEarnedPerAnnum(),
            'interest_earned_per_day'           => $tbond->getInterestEarnedPerDay(),
            'accrued_interest'                  => $tbond->getAccruedInterest(),
            'number_of_semi_annual_interest_periods' => $tbond->getNumberOfSemiAnnualInterestPeriods(),
            'yield'                             => $tbond->getYield(),
            'show_url'                          => $bond->path(),
            'edit_url'                          => $bond->path() . "/edit",
            'slug'                              => $security->subAssetClass->slug
        ];
    }
}
