<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/9/18
 * Time: 10:42 AM
 */

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\FundComplianceBenchmarkLimit;
use League\Fractal\TransformerAbstract;

class FundComplianceBenchmarkLimitsTransformer extends TransformerAbstract
{
    public function transform(FundComplianceBenchmarkLimit $limit)
    {
        return [
            'id' => $limit->id,
            'name' => $limit->benchmark->name,
            'warn' => $limit->warn,
            'limit' => $limit->limit,
            'sub_asset_class' => $limit->subAssetClass->name,
            'asset_class' => $limit->subAssetClass->assetClass->name,
        ];
    }
}
