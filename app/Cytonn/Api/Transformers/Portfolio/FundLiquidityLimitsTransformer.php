<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/11/18
 * Time: 11:58 AM
 */

namespace App\Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\FundLiquidityLimit;
use League\Fractal\TransformerAbstract;

class FundLiquidityLimitsTransformer extends TransformerAbstract
{
    public function transform(FundLiquidityLimit $limit)
    {
        return [
            'id' => $limit->id,
            'unit_fund' => $limit->unitFund->name,
            'warn' => $limit->warn . '%',
            'limit' => $limit->limit . '%',
            'show_url' => ''
        ];
    }
}
