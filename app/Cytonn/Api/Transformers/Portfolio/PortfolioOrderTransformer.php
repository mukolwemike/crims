<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 1:00 PM
 */

namespace App\Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Presenters\General\AmountPresenter;
use League\Fractal\TransformerAbstract;

class PortfolioOrderTransformer extends TransformerAbstract
{
    public function transform(PortfolioOrder $order)
    {
        $custodialAccount = $order->custodialAccount;

        return [
            'id' => $order->id,
            'security' => $order->security->name,
            'order_type' => $order->type->name,
            'asset_class' => $order->security->subAssetClass->assetClass->name,
            'sub_asset_class' => $order->security->subAssetClass->name,
            'fund_manager' => $order->security->fundManager->fullname,
            'fund_manager_id' => $order->security->fundManager->id,
            'cut_off_price' => ($order->cut_off_price) ? AmountPresenter::currency($order->cut_off_price) : null,
            'minimum_rate' => ($order->minimum_rate) ? AmountPresenter::currency($order->minimum_rate) : null,
            'unit_fund_orders' => $order->unitFundOrders,
            'order_allocations' => $order->orderAllocations,
            'show_url' => $order->path(),
        ];
    }
}
