<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\EquityHolding;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class EquityHoldingTransformer extends TransformerAbstract
{
    public function transform(EquityHolding $holding)
    {
        return [
            'id'=>$holding->id,
            'security_name'=>$holding->security->name,
            'security_id'=>$holding->security->id,
            'number'=>$holding->number,
            'cost'=>$this->currency($holding->cost),
            'date'=>DatePresenter::formatDate($holding->date),
            'value'=>$this->currency($holding->repo->purchaseValuePerShare()),
            'target_price'=>$this->currency($holding->repo->latestTargetPrice()),
            'total_cost'=>$this->currency($holding->repo->totalCost()),
            'total_value'=>$this->currency($holding->repo->totalValue()),
            'market_price'=>$this->currency($holding->repo->marketPricePerShare()),
            'market_value'=>$this->currency($holding->repo->marketValue()),
            'gain_loss'=>$this->currency($holding->repo->gainLoss()),
            'purchase_date'=> DatePresenter::formatDate($holding->date),
            'settlement_date'=> ($holding->settlement_date)
                ? DatePresenter::formatDate($holding->settlement_date)
                : DatePresenter::formatDate($holding->date)
        ];
    }
    
    public function currency($value)
    {
        return AmountPresenter::currency($value);
    }
}
