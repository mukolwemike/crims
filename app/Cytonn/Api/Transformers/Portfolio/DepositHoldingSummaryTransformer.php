<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class DepositHoldingSummaryTransformer extends TransformerAbstract
{
    public function transform(DepositHolding $holding, Carbon $date = null)
    {
        $security = $holding->security;

        return [
            'name' => $security->name,
            'principal' => $this->currency($holding->bond_purchase_cost ?
                $holding->bond_purchase_cost : $holding->amount),
            'nominal' => $this->currency($holding->amount),
            'rate' => $holding->interest_rate,
            'invested_date' => $holding->invested_date,
            'date' => DatePresenter::formatDate(
                $holding->bond_purchase_date ? $holding->bond_purchase_date : $holding->invested_date
            ),
            'maturity_date' => $holding->maturity_date->toDateString(),
            'purchase_price' => null,
            'current_price' => null,
            'market_value'=> $this->currency($holding->repo->getTotalValueOfAnInvestment()),
            'gross_interest' => $this->currency($holding->repo->getGrossInterestForInvestment()),
            'net_interest' => $this->currency($holding->repo->getNetInterestForInvestment()),
            'withholding_tax' => $this->currency($holding->repo->getWithholdingTaxForInvestment()),
            'value' => $holding->repo->getTotalValueOfAnInvestment(),
        ];
    }

    public function currency($value)
    {
        return AmountPresenter::currency($value);
    }

    public function export(DepositHolding $holding, Carbon $date = null)
    {
        $security = $holding->security;

        return [
            'Security Name' => $security->name,
            'Principal' => $this->currency($holding->bond_purchase_cost ?
                $holding->bond_purchase_cost : $holding->amount),
            'Nominal' => $this->currency($holding->amount),
            'Rate' => $holding->interest_rate,
            'Invested Date' => $holding->invested_date,
            'Date' => DatePresenter::formatDate(
                $holding->bond_purchase_date ? $holding->bond_purchase_date : $holding->invested_date
            ),
            'Maturity Date' => $holding->maturity_date->toDateString(),
            'Market Value'=> $this->currency($holding->repo->getTotalValueOfAnInvestment()),
            'Gross Interest' => $this->currency($holding->repo->getGrossInterestForInvestment()),
            'Net Interest' => $this->currency($holding->repo->getNetInterestForInvestment()),
            'Withholding Tax' => $this->currency($holding->repo->getWithholdingTaxForInvestment()),
        ];
    }
}
