<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\EquityShareSale;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class EquityShareSaleTransformer extends TransformerAbstract
{
    public function transform(EquityShareSale $sale)
    {
        return [
            'id'=>$sale->id,
            'number'=>$sale->number,
            'sale_price'=>$this->currency($sale->sale_price),
            'date'=>DatePresenter::formatDate($sale->date),
            'target_price'=>$this->currency($sale->repo->targetPrice()),
            'market_price'=>$this->currency($sale->repo->marketPrice()),
            'market_value'=>$this->currency($sale->repo->marketValue()),
            'sale_value'=>$this->currency($sale->repo->saleValue()),
            'average_purchase_price'=>$this->currency($sale->repo->averagePurchasePrice()),
            'total_purchase_value'=>$this->currency($sale->repo->totalPurchaseValue()),
            'average_purchase_value'=>$this->currency($sale->repo->averagePurchaseValuePerShare()),
            'gain_loss'=>$this->currency($sale->repo->gainLoss()),
            'settlement_date'=> ($sale->settlement_date)
                ? DatePresenter::formatDate($sale->settlement_date)
                : DatePresenter::formatDate($sale->date)
        ];
    }
    
    public function currency($value)
    {
        return AmountPresenter::currency($value);
    }
}
