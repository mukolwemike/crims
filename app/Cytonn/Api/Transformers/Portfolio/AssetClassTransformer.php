<?php

namespace Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\AssetClass;
use League\Fractal\TransformerAbstract;

class AssetClassTransformer extends TransformerAbstract
{
    public function transform(AssetClass $asset)
    {
        return [
            'id' => $asset->id,
            'name' => $asset->name,
            'show_url' => $asset->path(),
            'edit_url' => $asset->path() . "/edit",
        ];
    }
}
