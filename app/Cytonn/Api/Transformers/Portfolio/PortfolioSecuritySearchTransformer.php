<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/20/18
 * Time: 5:24 PM
 */

namespace App\Cytonn\Api\Transformers\Portfolio;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use League\Fractal\TransformerAbstract;

class PortfolioSecuritySearchTransformer extends TransformerAbstract
{
    public function transform(PortfolioSecurity $security)
    {
        return [
            "label" => $security->name . ' ' . $security->code,
            "value" => $security->id,
        ];
    }
}
