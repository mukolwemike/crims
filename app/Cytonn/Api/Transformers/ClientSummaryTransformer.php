<?php
/**
 * Date: 15/02/2016
 * Time: 8:33 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\ClientPresenter;

class ClientSummaryTransformer extends TransformerAbstract
{
    private $product;

    /**
     * ClientSummaryTransformer constructor.
     *
     * @param $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }


    public function transform(Client $client)
    {
        $product = $this->product;

        return [
            'id'=>$client->id,
            'client_code'=>$client->client_code,
            'fullName'=>ClientPresenter::presentJointFullNames($client->id),
            'amount'=>$client->repo->getTodayInvestedAmountForProduct($product),
            'gross_interest'=>$client->repo->getTodayTotalGrossInterestForProduct($product),
            'net_interest'=>$client->repo->getTodayTotalInterestForProduct($product),
            'withholding_tax'=>$client->repo->getTodayTotalWithHoldingTaxForProduct($product),
            'withdrawal'=>$client->repo->getTodayTotalPaymentForProduct($product),
            'custody_fees'=>$client->repo->getCustodyFeesForProduct($product),
            'total'=>$client->repo->getTodayTotalInvestmentsValueForProduct($product),
        ];
    }
}
