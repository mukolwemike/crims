<?php
/**
 * Date: 05/04/2016
 * Time: 11:54 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ActiveStrategyHolding;
use League\Fractal\TransformerAbstract;

/**
 * Class ActiveStrategyHoldingTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class ActiveStrategyHoldingTransformer extends TransformerAbstract
{
    /**
     * @param ActiveStrategyHolding $holding
     * @return array
     */
    public function transform(ActiveStrategyHolding $holding)
    {
        return [
            'id'=>$holding->id,
            'security_name'=>$holding->security->name,
            'security_id'=>$holding->security->id,
            'number'=>$holding->number,
            'cost'=>$holding->cost,
            'date'=>$holding->date,
            'value'=>$holding->repo->purchaseValuePerShare(),
            'target_price'=>$holding->repo->latestTargetPrice(),
            'total_cost'=>$holding->repo->totalCost(),
            'total_value'=>$holding->repo->totalValue(),
            'market_price'=>$holding->repo->marketPricePerShare(),
            'market_value'=>$holding->repo->marketValue(),
            'gain_loss'=>$holding->repo->gainLoss(),
            'purchase_date'=>$holding->date
        ];
    }
}
