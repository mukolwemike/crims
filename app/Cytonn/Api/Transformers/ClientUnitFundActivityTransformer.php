<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInstructionType;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Cytonn\Presenters\AmountPresenter;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\ClientPresenter;

class ClientUnitFundActivityTransformer extends TransformerAbstract
{
    public function transform(UnitFundInvestmentInstruction $instruction)
    {
        $unit_fund = UnitFund::findOrFail($instruction->unit_fund_id);

        $client = Client::findOrFail($instruction->client_id);

        $transaciton_type = UnitFundInstructionType::findOrFail($instruction->instruction_type_id);

        $payload = json_decode($instruction->payload, true);

        $data = [
            'client' => ClientPresenter::presentFullNames($client->id),
            'unit_fund' => $unit_fund->name,
            'transaction_type' => $transaciton_type->name,
            'date' => $payload['date']
        ];

        $data = $this->transformTransactionType($data, $transaciton_type, $payload);

        return $data;
    }

    public function transformTransactionType($data, $transaciton_type, $payload)
    {
        if ($transaciton_type->slug == 'unit-fund-purchase') {
            $data['amount'] = AmountPresenter::currency($payload['amount']);
        } elseif ($transaciton_type->slug == 'unit-fund-sale') {
            $data['number'] = AmountPresenter::currency($payload['number']);
        } elseif ($transaciton_type->slug == 'unit-fund-transfer') {
            $transferer = Client::findOrFail($payload['transferer_id']);

            $transferee = Client::findOrFail($payload['transferee_id']);

            $data['number'] = AmountPresenter::currency($payload['number']);

            $data['transferee'] =  ClientPresenter::presentFullNames($transferer->id);

            $data['transferer'] =  ClientPresenter::presentFullNames($transferee->id);
        } elseif ($transaciton_type->slug == 'unit-fund-switch') {
            $data['previous_unit_fund_id'] = $payload['previous_unit_fund_id'];

            $data['current_unit_fund_id'] = $payload['current_unit_fund_id'];
        }

        return $data;
    }
}
