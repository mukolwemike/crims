<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class ClientTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        $hasFund = $client->unitFundPurchases()
            ->whereHas('unitFund', function ($fund) {
                $fund->ofType('cis')->active();
            })->exists();

        return [
            'id' => $client->id,
            'client_code' => $client->client_code,
            'fullName' => $client->name(),
            'combinedName' => $client->name() . ' - ' . $client->client_code,
            'email' => ($client->contact) ? $client->contact->email : '',
            'phone' => $client->present()->getAnyPhone,
            'type' => ClientPresenter::presentClientType($client->id),
            'compliant' => $client->repo->checkClientKycValidated(),
            'compliance_reason' => '',
            'investment_status' => $this->investmentActive($client),
            'account_status' => $this->accountActive($client),
            'deleted_at' => $client->deleted_at,
            'hasSP' => $client->investments()->active()->count() ? true : false,
            'hasUT' => $hasFund,
            'hasRE' => count($client->repo->clientReInvestments()['realEstateDetails']) ? true : false,
        ];
    }

    public function searchTransform(Client $client)
    {
        return [
            "label" => ClientPresenter::presentJointFullNames($client->id),
            "value" => $client->id,
            "code" => $client->client_code,
            "email" => $client->contact->email,
        ];
    }

    public function allDetails(Client $client)
    {
        $account = $client->bankAccounts()->latest()->first();

        return [
            'title_id' => $client->contact->title_id,
            'client_id' => $client->id,
            'lastname' => $client->contact->lastname,
            'middlename' => $client->contact->middlename,
            'firstname' => $client->contact->firstname,
            'preferredname' => $client->contact->preferredname,
            'gender_id' => $client->contact->gender_id,
            'dob' => $client->dob,
            'pin_no' => $client->pin_no,
            'id_or_passport' => $client->id_or_passport,
            'postal_code' => $client->postal_code,
            'postal_address' => $client->postal_address,

            'country_id' => $client->country_id,
            'telephone_office' => $client->telephone_office,

            'residential_address' => $client->residence,

            'street' => $client->street,
            'town' => $client->town,
            'method_of_contact_id' => $client->method_of_contact_id,
            'employment_id' => $client->employment_id,

            'employment_other' => $client->employment_other,
            'present_occupation' => $client->contact->title_id,
            'business_sector' => $client->business_sector,
            'employer_name' => $client->employer_name,
            'employer_address' => $client->employer_address,
            'corporate_investor_type_id' => $client->corporate_investor_type_id,
            'corporate_investor_type_other' => $client->corporate_investor_type_other,

            'registered_name' => $client->contact->corporate_registered_name,
            'trade_name' => $client->contact->corporate_trade_name,
            'registered_address' => $client->contact->registered_address,
            'registration_number' => $client->contact->registration_number,
            'investor_account_name' => $client->investor_account_name,

            'contact_person_fname' => $client->contact_person_fname,
            'contact_person_lname' => $client->contact_person_lname,
            'taxable' => $client->taxable,
            'email' => $client->contact->email,

            'client_bank_id' => ($account) ? $account->clientBank->id : null,
            'contact_person_title_id' => $client->contact_person_title,
            'client_bank_branch_id' => ($account) ? $account->clientBankBranch->id : null,

            'type_id' => $client->client_type_id
        ];
    }

    private function investmentActive(Client $client)
    {
        $investments = $client->investments()->active(true)->count();

        $unitHoldings = $client->unitHoldings()->active(true)->count();

        $unitFund = $client->unitFundPurchases()->count();

        return ($investments > 0) || ($unitHoldings > 0) || ($unitFund > 0);
    }

    private function accountActive(Client $client)
    {
        foreach ($client->clientUsers as $clientUser) {
            if (!$clientUser->active) {
                return false;
            }
        }

        return true;
    }
}
