<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CustodialAccountBalanceTrail;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class CustodialAccountBalanceTrailTransformer extends TransformerAbstract
{
    public function transform(CustodialAccountBalanceTrail $trail)
    {
        return [
            'id'                =>  $trail->id,
            'date'              =>  DatePresenter::formatDate($trail->date),
            'amount'            =>  $trail->amount
        ];
    }
}
