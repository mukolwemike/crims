<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Api\Transformers\Shares;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class ShareCommissionRecipientTransformer
 * @package Cytonn\Api\Transformers\Shares
 */
class ShareCommissionRecipientTransformer extends TransformerAbstract
{
    /**
     * @var Carbon
     */
    protected $start;
    /**
     * @var Carbon
     */
    protected $end;

    /**
     * ShareCommissionRecipientTransformer constructor.
     * @param Carbon $start
     * @param Carbon $end
     */
    public function __construct(Carbon $start, Carbon $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @param CommissionRecepient $recipient
     * @return array
     */
    public function transform(CommissionRecepient $recipient)
    {
        $summary = $recipient->shareCommissionCalculator($this->start, $this->end)->summary();

        $overrides = $summary->override();

        $total = $summary->total();

        return [
            'id' => $recipient->id,
            'name' => $recipient->name,
            'email' => $recipient->email,
            'earned' => $summary->total(),
            'total' => $total + $overrides,
            'override' => $overrides
        ];
    }
}
