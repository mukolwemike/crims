<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankBranch;
use League\Fractal\TransformerAbstract;

class BankBranchTransformer extends TransformerAbstract
{
    public function transform(ClientBankBranch $branch)
    {
        return [
            'id'            =>  $branch->id,
            'name'          =>  $branch->name,
            'swiftCode'     =>  $branch->swift_code,
            'branchCode'     =>  $branch->branch_code,
        ];
    }
}
