<?php
/**
 * Date: 31/03/2016
 * Time: 9:33 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\InterestPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

/**
 * Class InterestPaymentTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class InterestPaymentTransformer extends TransformerAbstract
{

    /**
     * @var Carbon
     */
    protected $date;


    /**
     * @param null $interestDate
     */
    public function __construct($interestDate = null)
    {
        is_null($interestDate) ? $this->date = Carbon::today() : $this->date = new Carbon($interestDate);
    }

    /**
     * @param InterestPaymentSchedule $schedule
     * @return array
     */
    public function transform(InterestPaymentSchedule $schedule)
    {
        $investment = $schedule->investment;

        $intervals = [null=>'On Maturity', 1=>'Monthly', 3=>'Quarterly', 6=>'Semi anually', 12=>'Annually'];

        $interval = $schedule->investment->interest_payment_interval;

        try {
            $name = $intervals[$interval];
        } catch (\Exception $e) {
            $name = 'On Maturity';
        }

        $last_payment = $investment->payments()->orderBy('date_paid', 'DESC')->first();

        if (is_null($last_payment)) {
            $last_payment_date = null;
        } else {
            $last_payment_date = $last_payment->date_paid->toDateString();
        }

        $gross_interest = $investment
            ->getCurrentGrossInterestAsAtEndOfDay(
                $investment->amount,
                $investment->interest_rate,
                $investment->invested_date,
                $investment->maturity_date,
                $this->date
            );

        $remaining = $investment
                ->getNetInterest($gross_interest, $investment->client->taxable) - $investment->repo->getTotalPayments();

        return [
            'id'=>$schedule->id,
            'amount'=>$schedule->amount,
            'description'=>$schedule->description,
            'type'=>$name,
            'date'=>$schedule->date->toDateString(),
            'value_date'=>$investment->invested_date->toDateString(),
            'last_payment_date'=>$last_payment_date,
            'client_code'=>$schedule->investment->client->client_code,
            'name'=>ClientPresenter::presentJointFullNames($schedule->investment->client_id),
            'principal'=>$schedule->investment->amount,
            'available'=> $remaining,
            'investment_id'=>$schedule->investment_id
        ];
    }
}
