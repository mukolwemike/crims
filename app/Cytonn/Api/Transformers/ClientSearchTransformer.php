<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class ClientSearchTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        return [
            "label" => ClientPresenter::presentJointFullNames($client->id) . ' ' .$client->client_code,
            "value" => $client->id,
            "code" => $client->client_code,
            "email" => $client->contact->email,
        ];
    }
}
