<?php

namespace App\Cytonn\Api\Transformers;

use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\System\Channel;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class UtilityBillingInstructionTransformer extends TransformerAbstract
{
    public function transform(UtilityBillingInstructions $instruction)
    {
        $instructionData = \GuzzleHttp\json_decode($instruction->payload);

        return [
            'id' => $instruction->id,
            'client_id' => $instruction->client_id,
            'client_code' => @$instruction->client->client_code,
            'client_name' => @ClientPresenter::presentFullNames($instruction->client->id),
            'amount' => $instruction->amount ? $instruction->amount : null,
            'number' => $instruction->number ? $instruction->number : null,
            'unit_fund' => $instruction->unitFund->name,
            'to' => 'Cypesa Wallet',
            'currency' => $instruction->unitFund->currency->code,
            'type' => ($instruction->type) ? $instruction->type->name : '',
            'date' => $instruction->date ? DatePresenter::formatDate($instruction->date) : null,
            'approval_id' => $instruction->approval ? $instruction->approval_id : null,
            'processed' => $instruction->billing_status_id,
            'origin' => ($instruction->channel_id) ? Channel::find($instruction->channel_id)->slug : (($instruction->user_id) ? 'client' : 'admin'),
            'created_at' => $instruction->created_at->toDateTimeString(),
            'account_name' => isset($instructionData->account_name) ? $instructionData->account_name : 'Airtime',
            'account_number' => $instruction->account_number ? $instruction->account_number : '',
            'bill_name' => $instruction->utility ? $instruction->utility->name : '',
            'bill_type' => $instruction->utility ? $instruction->utility->type->slug : '',
            'smart_card_number' => $instruction->smart_card_number ? $instruction->smart_card_number : '',
            'meter_number' => $instruction->meter_number ? $instruction->meter_number : '',
        ];
    }
}
