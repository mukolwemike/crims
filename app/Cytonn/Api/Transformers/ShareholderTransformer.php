<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesEntity;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;
use App\Cytonn\Models\ShareHolding;

class ShareholderTransformer extends TransformerAbstract
{
    public function transform(ShareHolder $holder)
    {
        $entity = $holder->entity;

        $shares = $holder->currentShares();

        $price = $entity->sharePrice();

        return [
            'id'                    =>  $holder->id,
            'number'                =>  $holder->number,
            'entity'                =>  $entity->name,
            'fullName'              =>  ClientPresenter::presentJointFullNames($holder->client_id),
            'email'                 =>  $holder->client->contact->email,
            'phone'                 =>  $holder->client->contact->phone,
            'active'                =>  (bool) $shares > 0,
            'number_of_shares'      =>  $shares,
            'value_of_shares'       =>  AmountPresenter::currency($shares * $price),
        ];
    }
}
