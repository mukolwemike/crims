<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 31/01/2019
 * Time: 10:23
 */

namespace App\Cytonn\Api\Transformers;

use App\Cytonn\Models\DocumentType;
use League\Fractal\TransformerAbstract;

class DocumentTypeTransformer extends TransformerAbstract
{
    public function transform(DocumentType $type)
    {
        return [
            'id' => $type->id,
            'value' => $type->name,
            'slug' => $type->slug,
        ];
    }
}
