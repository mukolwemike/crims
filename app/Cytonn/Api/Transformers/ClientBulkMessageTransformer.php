<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientBulkMessage;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class ClientBulkMessageTransformer extends TransformerAbstract
{
    public function transform(ClientBulkMessage $message)
    {
        return [
            'id'                    => $message->id,
            'subject'               =>  $message->subject,
            'approved'              =>  (bool) $message->approval_id,
            'sent'                  =>  (bool) $message->sent,
            'date'                  =>  $message->updated_at->toDateString(),
            'composer'              =>  UserPresenter::presentFullNames($message->composer)
        ];
    }
}
