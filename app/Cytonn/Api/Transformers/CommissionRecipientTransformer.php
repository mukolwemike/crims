<?php
/**
 * Date: 23/03/2016
 * Time: 9:31 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class CommissionRecipientTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class CommissionRecipientTransformer extends TransformerAbstract
{
    /**
     * @var null
     */
    public $currency = null;
    /**
     * @var bool
     */
    public $withTotals = false;

    public $date;
    /**
     * @var bool
     */
    public $withCommissionPeMonth = false;
    /**
     * @var array
     */
    public $dates = [];

    protected $start;

    protected $end;

    /**
     * CommissionRecipientTransformer constructor.
     *
     * @param null $currency
     */
    public function __construct(Currency $currency = null, Carbon $start = null, Carbon $end = null)
    {
        $this->currency = $currency;
        $this->start = $start;
        $this->end = $end;
    }


    /**
     * @param CommissionRecepient $recipient
     * @return array
     */
    public function transform(CommissionRecepient $recipient)
    {
        $summary = $recipient->calculator($this->currency, $this->start, $this->end)->summary();

        $overrides = $summary->override();

        $total = $summary->total();

        return [
            'id'=>$recipient->id,
            'name'=>$recipient->name,
            'email'=>$recipient->email,
            'earned' => $summary->earned(),
            'total'=> $total + $overrides,
            'claw_back'=>$summary->getClawBacks(),
            'override' => $overrides
        ];
    }

    public function recipient(CommissionRecepient $recepient)
    {
        return [
            'id' => $recepient->id,
            'name' => $recepient->name . ' - ' .$recepient->email,
            'email' => $recepient->email,
        ];
    }
}
