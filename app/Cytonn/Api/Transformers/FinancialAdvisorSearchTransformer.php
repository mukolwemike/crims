<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 08/08/2018
 * Time: 16:25
 */

namespace App\Cytonn\Api\Transformers;

use App\Cytonn\Models\CommissionRecepient;
use League\Fractal\TransformerAbstract;

class FinancialAdvisorSearchTransformer extends TransformerAbstract
{
    public function transform(CommissionRecepient $fa)
    {
        return [
            "label" => $fa->name ." ($fa->referral_code)",
            "value" => $fa->id,
        ];
    }
}
