<?php
/**
 * Date: 22/07/2016
 * Time: 8:52 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\BankInstruction;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

/**
 * Class BankInstructionTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class BankInstructionTransformer extends TransformerAbstract
{
    /**
     * @param BankInstruction $instruction
     * @return array
     */
    public function transform(BankInstruction $instruction)
    {
        $investment = $instruction->investment;

        $client = $investment->client;

        $bankDetails = $investment->bankDetails();

        return [
            'id'=>$instruction->id,
            'client_code'=>$client->client_code,
            'name'=> ClientPresenter::presentJointFullNames($client->id),
            'principal'=>$investment->amount,
            'invested_date'=>$investment->invested_date->toDateString(),
            'maturity_date'=>is_null($investment->maturity_date) ? '' :$investment->maturity_date->toDateString(),
            'type'=>$instruction->description,
            'bank'=>$bankDetails->bankName(),
            'branch'=>$bankDetails->branch(),
            'account_number'=>$bankDetails->accountNumber(),
            'account_name'=>$bankDetails->accountName(),
            'amount'=>$instruction->amount,
            'date'=>$instruction->date,
            'viewed'=>$instruction->viewed
        ];
    }
}
