<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\PortfolioInstruction;
use League\Fractal\TransformerAbstract;

/**
 * Class PortfolioInstructionTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class PortfolioInstructionTransformer extends TransformerAbstract
{
    /**
     * @param PortfolioInstruction $instruction
     * @return array
     */
    public function transform(PortfolioInstruction $instruction)
    {
        return [
            'id'                    =>  $instruction->id,
            'sender'                =>  $instruction->sender->account_name,
            'receiver'              =>  $instruction->receiver->account_name,
            'bank'                  =>  $instruction->sender->bank_name,
            'account_name'          =>  $instruction->sender->account_name,
            'account_number'        =>  $instruction->sender->account_no,
            'currency'              =>  $instruction->sender->currency->code,
            'amount'                =>  $instruction->amount,
            'date'                  =>  $instruction->date,
            'description'           =>  $instruction->description,
        ];
    }
}
