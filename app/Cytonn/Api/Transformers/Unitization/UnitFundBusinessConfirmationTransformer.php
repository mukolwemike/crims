<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundBusinessConfirmationTransformer extends TransformerAbstract
{
    public function transform(UnitFundPurchase $purchase)
    {
        return [
            'id'                        =>  $purchase->id,
            'client'                    =>  ClientPresenter::presentFullNames($purchase->client->id),
            'client_code'              =>   $purchase->client->client_code,
            'amountBeforeInvestment'    =>  $purchase->payment ? AmountPresenter::currency(abs($purchase->payment->amount)) : 0,
            'feesIncurred'              =>  AmountPresenter::currency($purchase->fees_incurred),
            'numberOfUnits'             =>  AmountPresenter::currency($purchase->number),
            'unitPrice'                 =>  AmountPresenter::currency($purchase->unit_price),
            'date'                      =>  $purchase->date->toFormattedDateString(),
            'confirmation'              =>  $purchase->is_confirmation_sent ? 'Sent' : 'Not sent',
            'fund'                      =>  $purchase->unitFund->name,
            'show_url'                  =>  "/dashboard/unitization/unit-fund-business-confirmations/{$purchase->id}",
        ];
    }
}
