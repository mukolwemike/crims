<?php

namespace Cytonn\Api\Transformers\Unitization;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Cytonn\Models\CommissionRecepient;

class UnitFundCommissionRecipientTransformer extends TransformerAbstract
{
    protected $start;
    protected $end;

    public function __construct(Carbon $start, Carbon $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function transform(CommissionRecepient $recipient)
    {
        $summary = $recipient
            ->unitizationCommissionCalculator($this->start, $this->end)
            ->summary();

        $override = $summary->override();

        $earned  = $summary->total();

        return [
            'id' => $recipient->id,
            'name' => $recipient->name,
            'email' => $recipient->email,
            'earned' => $earned,
            'override' => $override,
            'commission' => $override + $earned,
        ];
    }
}
