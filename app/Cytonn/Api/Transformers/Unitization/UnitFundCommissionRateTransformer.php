<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundCommissionRate;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class UnitFundCommissionRateTransformer extends TransformerAbstract
{
    public function transform(UnitFundCommissionRate $rate)
    {
        return [
            'id'                        =>  $rate->id,
            'rate'                      =>  AmountPresenter::currency($rate->rate),
            'date'                      =>  DatePresenter::formatDate($rate->date),
            'fundName'                  =>  $rate->fund->name,
            'recipientType'             =>  $rate->recipientType->name,
            'show_url'                  =>  $rate->path(),
        ];
    }
}
