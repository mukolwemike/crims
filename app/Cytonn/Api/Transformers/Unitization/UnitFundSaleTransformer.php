<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundSaleTransformer extends TransformerAbstract
{
    public function transform(UnitFundSale $sale)
    {
        $price = $sale->fund->unitPrice($sale->date);

        $amount = $sale->number * $price;

        return [
            'id' => $sale->id,
            'client_name' => ClientPresenter::presentFullNames($sale->client->id),
            'client_code' => $sale->number,
            'amount' => AmountPresenter::currency($amount, true, 0),
            'unit_price' => $price,
            'numberOfUnits' => AmountPresenter::currency($sale->number, true, 0),
            'date' =>  $sale->date->toFormattedDateString(),
            'currency' => $sale->fund->currency->name,
            'currency_code' => $sale->fund->currency->code,
            'description' => $sale->description
        ];
    }
}
