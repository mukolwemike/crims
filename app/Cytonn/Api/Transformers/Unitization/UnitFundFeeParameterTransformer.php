<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundFeeParameter;
use League\Fractal\TransformerAbstract;

class UnitFundFeeParameterTransformer extends TransformerAbstract
{
    public function transform(UnitFundFeeParameter $parameter)
    {
        return [
            'id'                        =>  $parameter->id,
            'name'                      =>  $parameter->name,
            'show_url'                  =>  $parameter->path(),
            'edit_url'                  =>  $parameter->path() . "/edit",
        ];
    }
}
