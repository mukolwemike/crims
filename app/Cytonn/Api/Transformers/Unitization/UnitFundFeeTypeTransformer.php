<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundFeeType;
use League\Fractal\TransformerAbstract;

class UnitFundFeeTypeTransformer extends TransformerAbstract
{
    public function transform(UnitFundFeeType $type)
    {
        return [
            'id'                        =>  $type->id,
            'name'                      =>  $type->name,
            'show_url'                  =>  $type->path(),
            'edit_url'                  =>  $type->path() . "/edit",
        ];
    }
}
