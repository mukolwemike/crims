<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundSwitch;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundSwitchTransformer extends TransformerAbstract
{
    public function transform(UnitFundSwitch $switch)
    {
        return [
            'id'                        =>  $switch->id,
            'holderName'                =>  ClientPresenter::presentFullNames($switch->holder->client_id),
            'holderNumber'              =>  $switch->holder->number,
            'date'                      =>  $switch->date->toDayDateTimeString(),
            'from'                      =>  $switch->previousFund->name,
            'to'                        =>  $switch->currentFund->name,
            'show_url'                  =>  $switch->path(),
        ];
    }
}
