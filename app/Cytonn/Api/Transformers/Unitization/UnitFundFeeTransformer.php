<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundFee;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\BooleanPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundFeeTransformer extends TransformerAbstract
{
    public function transform(UnitFundFee $fee)
    {
        $percentages = $this->feePercentages($fee);

        return [
            'fund' => $fee->fund->name,
            'id' => $fee->id,
            'type' => $fee->type->name,
            'date' => $fee->date->toDayDateTimeString(),
            'dependent' => BooleanPresenter::presentYesNo($fee->dependent),
            'charged_at' => $fee->charged_at,
            'show_url' => $fee->path(),
            'edit_url' => $fee->path() . "/edit",
            'percentages' => $percentages,
            'dep' => $fee->dependent,
            'amount' => $fee->amounts->sum('amount')
        ];
    }

    public function feePercentages(UnitFundFee $fee)
    {
        return $fee->percentages->map(function ($percentage) {
            return [
                'percentage' => $percentage->percentage,
                'is_amount' => $percentage->is_amount,
                'parameter' => $percentage->parameter->name,
                'date' => Carbon::parse($percentage->date)->toFormattedDateString()
            ];
        });
    }
}
