<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/7/18
 * Time: 10:33 AM
 */

namespace App\Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundFeePayment;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Presenters\General\DatePresenter;
use League\Fractal\TransformerAbstract;

class UnitFundFeePaymentTransformer extends TransformerAbstract
{
    public function transform(UnitFundFeePayment $payment)
    {
        return [
            'id' => $payment->id,
            'type' => $payment->fee->type->name,
            'amount' => AmountPresenter::currency($payment->amount),
            'date' => DatePresenter::formatDate($payment->date),
            'recipient' => $payment->recipient->name,
            'description' => $payment->custodialTransaction->description,
            'fund' => $payment->unitFund->name,
            'show_url' =>  $payment->path(),
            'edit_url' =>  $payment->path() . "/edit",
        ];
    }
}
