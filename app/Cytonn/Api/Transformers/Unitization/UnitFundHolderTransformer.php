<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundHolderTransformer extends TransformerAbstract
{
    public function transform(Client $client, UnitFund $unitFund)
    {
        return [
            'id'                    =>  $client->id,
            'holderNumber'          =>  $client->client_code,
            'holderName'            =>  ClientPresenter::presentJointFullNames($client->id),
            'clientType'            =>  $client->type->name,
            'unitFund'              =>  $unitFund->name,
            'email'                 =>  $client->contact->email,
            'phone'                 =>  $client->contact->phone,
            'active'                =>  (bool) $client->unitFundPurchases()->count() > 0,
            'number_of_shares'      =>  $client->unitFundPurchases()->sum('number'),
            'show_url'              =>  '',
        ];
    }
}
