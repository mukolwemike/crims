<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundTransfer;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundTransferTransformer extends TransformerAbstract
{
    public function transform(UnitFundTransfer $transfer)
    {
        return [
            'id'                        =>  $transfer->id,
            'senderName'                =>  ClientPresenter::presentFullNames($transfer->sender->id),
            'senderNumber'              =>  $transfer->sender->client_code,
            'recipientName'             =>  ClientPresenter::presentFullNames($transfer->recipient->id),
            'recipientNumber'           =>  $transfer->recipient->client_code,
            'numberOfUnits'             =>  AmountPresenter::currency($transfer->number, true, 0),
            'date'                      =>  $transfer->date->toFormattedDateString(),
//            'show_url'                  =>  $transfer->sender->path(),
        ];
    }
}
