<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 08/06/2018
 * Time: 20:45
 */

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class UnitFundInvestmentInstructionTransformer extends TransformerAbstract
{
    public function transform(UnitFundInvestmentInstruction $instruction)
    {
        $processed = $instruction->approval || $instruction->purchase || $instruction->unit_fund_sale_id
            || $instruction->unit_fund_transfer_id || $instruction->unit_fund_switch_id || $instruction->inactive
            || $instruction->cancelled;

        $instructionData = \GuzzleHttp\json_decode($instruction->payload);

        $transferer = isset($instructionData->transferer_id)
            ? Client::find($instructionData->transferer_id) : null;

        $transferee = isset($instructionData->transferee_id)
            ? Client::find($instructionData->transferee_id) : null;

        $payload = \GuzzleHttp\json_decode($instruction->payload, true);

        return [
            'id' => $instruction->id,
            'client_id' => $instruction->client_id,
            'client_code' => @$instruction->client->client_code,
            'client_name' => @ClientPresenter::presentFullNames($instruction->client->id),
            'amount' => $instruction->amount ? $instruction->amount : null,
            'number' => $instruction->number ? $instruction->number : null,
            'bank' => $this->getBank($instruction),
            'unit_type' => $instruction->instruction_type_id,
            'unit_fund' => $instruction->unitFund->name,
            'created_at' => $instruction->created_at->toDateTimeString(),
            'currency' => $instruction->unitFund->currency->code,
            'processesed' => $instruction->cancelled ? 'cancelled' : $processed,
            'processed' => $instruction->cancelled ? 'cancelled' : $processed,
            'origin' => ($instruction->channel_id) ? Channel::find($instruction->channel_id)->slug : (($instruction->user_id) ? 'client' : 'admin'),
            'client' => $instruction->client,
            'unitFund' => $instruction->unitFund,
            'type' => ($instruction->type) ? $instruction->type->name : 'Purchase',
            'instructionType' => $instruction->type ? $instruction->type->slug : 'purchase',
            'date' => $instruction->date ? $instruction->date : null,
            'document_id' => $instruction->document ? $instruction->document->uuid : null,
            'kycValidated' => $instruction->client ? $instruction->client->repo->checkClientKycValidated() : false,
            'transfer_from' => isset($instructionData->transferer_id)
                ? @ClientPresenter::presentFullNames($instructionData->transferer_id) . ' - ' . $transferer->client_code
                : null,
            'transfer_to' => isset($instructionData->transferee_id)
                ? @ClientPresenter::presentFullNames($instructionData->transferee_id) . ' - ' . $transferee->client_code
                : null,
            'transfer_date' => isset($instructionData->transferee_id)
                ? @DatePresenter::formatDate($instructionData->date) : null,
            'channel' => isset($instructionData->channel) ? $instructionData->channel : null,
            'doc_id' => $instruction->document ? $instruction->document->id : null,
            'approval_id' => $instruction->approval ? $instruction->approval_id : null,
            'mode_of_payment' => isset($payload['mode_of_payment']) ? $payload['mode_of_payment'] : null,
        ];
    }

    public function brief(UnitFundInvestmentInstruction $instruction)
    {
        return [
            'amount' => $instruction->amount ? $instruction->amount : null,
            'number' => $instruction->number ? $instruction->number : null,
            'currency' => $instruction->unitFund->currency->name,
            'unitFund' => $instruction->unitFund->short_name,
            'type' => ($instruction->type) ? $instruction->type->name : 'Purchase',
            'date' => Carbon::parse($instruction->date)->toFormattedDateString(),
        ];
    }

    protected function getBank(UnitFundInvestmentInstruction $instruction)
    {
        $bank = null;

        if (isset(\GuzzleHttp\json_decode($instruction->payload)->bank)) {
            $bank = \GuzzleHttp\json_decode($instruction->payload)->bank;
        }

        if (isset(\GuzzleHttp\json_decode($instruction->payload)->account_id)) {
            $clientBank = ClientBankAccount::find(\GuzzleHttp\json_decode($instruction->payload)->account_id);

            $bank = $clientBank->branch->bank->name . ' - ' . $clientBank->account_number;
        }

        return $bank;
    }
}
