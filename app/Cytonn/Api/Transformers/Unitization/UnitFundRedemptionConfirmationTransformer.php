<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-12
 * Time: 10:57
 */

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundRedemptionConfirmationTransformer extends TransformerAbstract
{
    public function transform(UnitFundSale $sale)
    {
        return [
            'id' => $sale->id,
            'client' => ClientPresenter::presentFullNames($sale->client->id),
            'client_code' => $sale->client->client_code,
            'amountBeforeInvestment' => ($sale->payment)
                ? AmountPresenter::currency(abs($sale->payment->amount)) : '',
            'feesIncurred' => AmountPresenter::currency($sale->fees_incurred),
            'numberOfUnits' => AmountPresenter::currency($sale->number),
            'numberInWords' => AmountPresenter::convertToWords($sale->number),
            'unitPrice' => AmountPresenter::currency($sale->unit_price),
            'date' => $sale->date->toFormattedDateString(),
            'confirmation' => $sale->is_confirmation_sent ? true : false,
            'fund' => $sale->fund->name,
            'show_url' => "/dashboard/unitization/unit-fund-redemption-confirmations/{$sale->id}",
        ];
    }
}
