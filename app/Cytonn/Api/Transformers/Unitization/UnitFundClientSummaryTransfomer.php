<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/5/18
 * Time: 12:22 PM
 */

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Unitization\UnitFundClientRepository;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Unitization\Trading\Sell;
use Jazz\Helper\Unit;
use League\Fractal\TransformerAbstract;

class UnitFundClientSummaryTransfomer extends TransformerAbstract
{
    /**
     * @param Client $client
     * @param UnitFund $fund
     * @return array
     * @throws ClientInvestmentException
     * @throws \Exception
     */
    public function transform(Client $client, UnitFund $fund)
    {
        $first_purchase = $client->unitFundPurchases()
            ->forUnitFund($fund)
            ->orderBy('date', 'asc')
//            ->remember(1000)
            ->first();

        $firstPurchaseDate = $first_purchase ? Carbon::parse($first_purchase->date) : Carbon::now();
        $today = Carbon::today();

        $calculator = $client->calculateFund($fund, $today, $firstPurchaseDate, false);
        $calculate = $calculator->getPrepared();
        unset($calculate->all_actions);

        $sold = $calculator->totalSales();

        $sellClass = (new Sell($fund, $client, $today));

        $withdrawableUnits = $sellClass->withdrawableUnits();

//        $dividends = $client->repo->getUnitsFromDividendsAsAt($fund, $today->copy());

        $fundRepo = new UnitFundClientRepository($client, $fund);

        $last_performance_date = $fund->latestPerformance()
            ? Carbon::parse($fund->latestPerformance()->date)
            : null;

        $currency = $fund->currency;

        $key = 'net_daily_yield'.$fund->id.Carbon::today()->toDateString();

        $yield = cache()->get($key);

        if (!cache()->has($key)) {
            $yield = $fund->latestPerformance()->net_daily_yield;

            cache([$key => $yield], Carbon::now()->addHours(2));
        }

        return [
            'id' => $client->id,
            'code' => $client->client_code,
            'name' => ClientPresenter::presentJointFullNames($client->id),
            'email' => $client->contact->email,
            'fund' => $fund->name,
            'fundId' => $fund->id,
            'ownedUnits' => AmountPresenter::currency(floor($calculator->totalUnits()), false, 0),
            'accruedInterest' => AmountPresenter::currency(floor($calculator->totalInterest()), false, 0),
            'ownedUnitsUnedited' => floor($calculator->totalUnits()),
            'unitsPurchased' => AmountPresenter::currency($calculator->totalPurchases(), false, 2),
            'unitsSold' => AmountPresenter::currency($sold, false, 0),
//            'dividends' => AmountPresenter::currency($dividends, false, 2),
            'feesIncurred' => AmountPresenter::currency($client->repo->getFeesIncurredAsAt($fund, $today->copy())),
            'show_url' =>  $client->clientFundDetails($fund),
            'price' => $calculator->price(),
            'previous_price' => $fund->unitPrice($last_performance_date),
            'value' => $calculator->totalAmount(),
            'shortname' => @$fund->short_name,
            'type' => $fund->category->slug,
            'yield' => $yield,
            'currency' => $currency->name,
            'currency_code' => $currency->code,
            'balance' => $fundRepo->balance(),
            'total_invested_amount' => $fundRepo->totalAmountInvested(),
            'net_invested_amount' => $fundRepo->netInvestmentAmount(),
            'un_invested_amount' => $fundRepo->uninvestedAmount(),
            'calculate' => $calculate,
            'category'=>'ut',
            'withdrawable_units' => $withdrawableUnits,
            'withdrawal_fee' => $sellClass->calculateWithdrawalFee(),
            'lock_in_days' => AmountPresenter::currency($fund->lockInDays(), false, 0)
        ];
    }
}
