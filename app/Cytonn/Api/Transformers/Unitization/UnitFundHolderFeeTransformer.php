<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundHolderFee;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundHolderFeeTransformer extends TransformerAbstract
{
    public function transform(UnitFundHolderFee $holderFee)
    {
        return [
            'id'            =>  $holderFee->id,
            'holderNumber'  =>  $holderFee->holder->number,
            'holderName'    =>  ClientPresenter::presentFullNames($holderFee->holder->client_id),
            'type'          =>  $holderFee->fee->type->name,
            'amount'        =>  AmountPresenter::currency($holderFee->amount),
            'incurredOn'    =>  $holderFee->created_at->toDayDateTimeString(),
        ];
    }
}
