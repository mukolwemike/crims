<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundFeeChargedTransformer extends TransformerAbstract
{
    public function transform(UnitFundFeesCharged $feeCharged)
    {
        $client = $feeCharged->getClient();

        return [
            'id'            =>  $feeCharged->id,
            'client_code'   =>  ($client)? $client->client_code: '',
            'client_name'   =>  ($client)? ClientPresenter::presentFullNames($client->id): '',
            'type'          =>  $feeCharged->fee->type->name,
            'amount'        =>  AmountPresenter::currency($feeCharged->amount),
            'incurredOn'    =>  $feeCharged->created_at->toDayDateTimeString(),
            'percentage'    =>  0,
            'value'        =>  $feeCharged->amount,
        ];
    }
}
