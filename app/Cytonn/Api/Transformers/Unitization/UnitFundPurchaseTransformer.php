<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundPurchaseTransformer extends TransformerAbstract
{
    public function transform(UnitFundPurchase $purchase)
    {
        $today = Carbon::today();

//        $recipient = $purchase->getRecipient();

        return [
            'id' => $purchase->id,
            'holderName' => ClientPresenter::presentFullNames($purchase->client->id),
            'holderNumber' => '',
            'amountBeforeInvestment' => abs($purchase->getAmount()),
            'amount_before_investment' => AmountPresenter::currency(abs($purchase->getAmount()), true, 0),
            'feesIncurred' => AmountPresenter::currency($purchase->fees->sum('amount'), true, 0),
            'fees_incurred' => abs($purchase->fees->sum('amount')),
            'numberOfUnits' => AmountPresenter::currency($purchase->number, true, 0),
            'unitPrice' => AmountPresenter::currency($this->price($purchase)),
            'date' => $purchase->date->toFormattedDateString(),
            'show_url' => $purchase->path(),
            'amount' => $purchase->amount,
            'description' => $purchase->description,
            'number' => $purchase->number,
            'client_payment_id' => $purchase->client_payment_id,
            'client_id' => $purchase->client_id,
            'unit_fund_id' => $purchase->unit_fund_id,
            'price' => $this->price($purchase),
            'recipient' => $purchase->getRecipient(),
            'fund' => $purchase->unitFund->name,
            'value' => AmountPresenter::currency(($this->price($purchase) * $purchase->number), true, 0),
            'currency' => $purchase->unitFund->currency->name,
            'current_price' => $this->price($purchase, $today),
            'currency_code' => $purchase->unitFund->currency->code,
            'fund_id' => $purchase->unitFund->id,
            'rate' => $purchase->unitFundCommission ? $purchase->unitFundCommission->rate : null,
        ];
    }

    public function price(UnitFundPurchase $purchase, $date = null)
    {
        $date = $date ? Carbon::parse($date) : Carbon::parse($purchase->date);

        return $purchase->unitFund->unitPrice($date->copy());
    }
}
