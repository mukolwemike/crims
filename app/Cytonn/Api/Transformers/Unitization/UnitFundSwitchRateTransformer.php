<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundSwitchRate;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class UnitFundSwitchRateTransformer extends TransformerAbstract
{
    public function transform(UnitFundSwitchRate $rate)
    {
        return [
            'id'                        =>  $rate->id,
            'amount'                    =>  AmountPresenter::currency($rate->amount),
            'date'                      =>  DatePresenter::formatDate($rate->date),
            'show_url'                  =>  $rate->path(),
            'edit_url'                  =>  "{$rate->path()}/edit",
        ];
    }
}
