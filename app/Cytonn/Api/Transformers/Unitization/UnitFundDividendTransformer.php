<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundDividend;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class UnitFundDividendTransformer extends TransformerAbstract
{
    public function transform(UnitFundDividend $dividend)
    {
        return [
            'id'                        =>  $dividend->id,
            'amount'                    =>  AmountPresenter::currency($dividend->amount),
            'date'                      =>  DatePresenter::formatDate($dividend->date),
            'show_url'                  =>  $dividend->path(),
            'edit_url'                  =>  "{$dividend->path()}/edit",
        ];
    }
}
