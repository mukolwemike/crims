<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/13/17
 * Time: 4:46 PM
 */

namespace Cytonn\Api\Transformers\Unitization;

use Cytonn\Unitization\Trading\Calculator;
use League\Fractal\TransformerAbstract;

class UnitFundCalculatorTransfomer extends TransformerAbstract
{
    public function transform(Calculator $calculator)
    {
        return [
            'amount' => $calculator->getAmount(),
            'units' => $calculator->getNumberOfUnits(),
            'fees' => $calculator->getFeesIncurred(),
            'balance' => $calculator->getBalance(),
            'amount_less_fee' => $calculator->getAmountLessFees(),
            'total_unit_price' => $calculator->getTotalUnitsPrice(),
            'unit_price' => $calculator->getUnitPrice(),
        ];
    }
}
