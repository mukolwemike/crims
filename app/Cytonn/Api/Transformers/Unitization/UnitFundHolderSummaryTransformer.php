<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Client;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\ClientPresenter;

class UnitFundHolderSummaryTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        $today = Carbon::today();

        return [
            'id'              =>  $client->id,
            'code'            =>  $client->client_code,
            'name'            =>  ClientPresenter::presentJointFullNames($client->id),
            'purchases'       =>  $client->unitFundPurchases(),
            'sales'           =>  $client->unitFundSales->sum('number'),
            'transfers'       =>  $client->unitFundTransfers->sum('number'),
//            'unitFund'        =>  $holder->fund->name,
//            'ownedUnits'      =>  $holder->repo->getOwnedUnitsAsAt($today->copy()),
//            'feesIncurred'    =>  AmountPresenter::currency($holder->repo->getFeesIncurredAsAt($today->copy())),
//            'show_url'        =>  $holder->path(),
        ];
    }


    public function transfer()
    {
    }
}
