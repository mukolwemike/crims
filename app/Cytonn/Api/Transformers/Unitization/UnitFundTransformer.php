<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\AmountPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundTransformer extends TransformerAbstract
{
    /**
     * @param UnitFund $fund
     * @return array
     */
    public function transform(UnitFund $fund)
    {
        $date = Carbon::now();

        $bank = $fund->repo->collectionAccount('bank');

        $mpesa = $fund->repo->collectionAccount('mpesa');

        $fundArray = $fund->toArray();

        return array_merge($fundArray, [
            'code' => $fund->id,
            'initialUnitPrice' => AmountPresenter::currency($fund->initial_unit_price),
            'currentUnitPrice' => $fund->unitPrice($date->copy()),
            'minimumInvestmentAmount' => (int)$fund->minimum_investment_amount,
            'minimumTopupAmount' => AmountPresenter::currency($fund->minimum_topup_amount),
            'objectives' => $fund->fund_objectives,
            'minimumInvestmentHorizon' => $fund->minimum_investment_horizon,
            'currency' => $fund->currency->code,
            'fundManager' => $fund->manager->fullname,
            'show_url' => $fund->path(),
            'edit_url' => $fund->path() . "/edit",
            'collection_accounts' => [
                'bank' => is_null($bank) ? null : [
                    'account_name' => $bank->account_name,
                    'bank_name' => $bank->bank_name,
                    'branch' => $bank->branch_name,
                    'number' => $bank->account_no,
                    'swift_code' => $bank->bank_swift
                ],
                'mpesa' => is_null($mpesa) ? null : [
                    'paybill' => $mpesa->account_no
                ]
            ]
        ]);
    }
}
