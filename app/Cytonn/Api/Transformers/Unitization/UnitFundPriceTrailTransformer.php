<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundPerformance;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class UnitFundPriceTrailTransformer extends TransformerAbstract
{
    public function transform(UnitFundPerformance $trail)
    {
        $date = Carbon::today();

        return [
            'id'                        =>  $trail->id,
            'aum'                       => $trail->aum,
            'nav'                       => $trail->nav,
            'assets'                    => $trail->assets,
            'cash'                      => $trail->cash,
            'yield'                     => $trail->net_daily_yield,
            'price'                     =>  $trail->price,
            'date'                      =>  $trail->date,
            'units'                      =>  $trail->fund->repo->getUnits($date),
            'show_url'                  =>  $trail->path(),
            'edit_url'                  =>  $trail->path() . "/edit",
        ];
    }
}
