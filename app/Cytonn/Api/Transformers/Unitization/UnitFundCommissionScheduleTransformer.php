<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 14/02/2019
 * Time: 15:29
 */

namespace App\Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class UnitFundCommissionScheduleTransformer extends TransformerAbstract
{
    public function transform(UnitFundCommissionSchedule $schedule)
    {
        return [
            'id' => $schedule->id,
            'date' => DatePresenter::formatDate($schedule->date),
            'amount' => AmountPresenter::currency($schedule->amount),
            'description' => $schedule->description,
        ];
    }
}
