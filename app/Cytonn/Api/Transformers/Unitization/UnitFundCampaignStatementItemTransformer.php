<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundCampaignStatementItem;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\ClientPresenter;

class UnitFundCampaignStatementItemTransformer extends TransformerAbstract
{
    public function transform(UnitFundCampaignStatementItem $item)
    {
        return [
            'id'                =>  $item->id,
            'holderNumber'      =>  $item->client->client_code,
            'holderName'        =>  ClientPresenter::presentJointFullNames($item->client_id),
            'unitFund'          =>  $item->campaign->fund->name,
            'addedBy'           =>  UserPresenter::presentFullNames($item->added_by),
            'addedOn'           =>  DatePresenter::formatDate($item->added_on),
            'sent'              =>  (boolean) $item->sent,
            'show_url'          =>  '',
        ];
    }
}
