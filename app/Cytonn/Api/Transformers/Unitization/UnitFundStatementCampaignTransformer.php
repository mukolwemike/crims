<?php

namespace Cytonn\Api\Transformers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class UnitFundStatementCampaignTransformer extends TransformerAbstract
{
    public function transform(UnitFundStatementCampaign $campaign)
    {
        return [
            'id'                        =>  $campaign->id,
            'name'                      =>  $campaign->name,
            'type'                      =>  $campaign->type->name,
            'sender'                    =>  UserPresenter::presentFullNames($campaign->sender_id),
            'date'                      =>  DatePresenter::formatDate($campaign->send_date),
            'closed'                    =>  BooleanPresenter::presentYesNo($campaign->closed),
            'show_url'                  =>  $campaign->path(),
            'edit_url'                  =>  $campaign->path() . "/edit",
        ];
    }
}
