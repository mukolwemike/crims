<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\InterestRateUpdate;
use League\Fractal\TransformerAbstract;

class InterestRateUpdateTransformer extends TransformerAbstract
{
    public function transform(InterestRateUpdate $update)
    {
        return [
            'id'            => $update->id,
            'product'       => $update->product->name,
            'date'          => $update->date,
            'description'   => $update->description,
        ];
    }
}
