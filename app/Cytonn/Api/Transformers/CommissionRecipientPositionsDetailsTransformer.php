<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 15/08/2018
 * Time: 16:49
 */

namespace App\Cytonn\Api\Transformers;

use App\Cytonn\Models\CommissionRecepientPosition;
use League\Fractal\TransformerAbstract;

class CommissionRecipientPositionsDetailsTransformer extends TransformerAbstract
{
    public function transform(CommissionRecepientPosition $position)
    {
        return [
          'id' => $position->id,
          'fa_type' => $position->type->name,
          'fa_rank' => $position->rank->name,
          'supervisor' => $position->present()->getSupervisor,
          'start_date' => $position->start_date,
          'end_date' => $position->end_date,
        ];
    }
}
