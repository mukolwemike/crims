<?php

namespace App\Cytonn\Api\Transformers\Clients;

use App\Cytonn\Models\RewardVoucher;
use Cytonn\Presenters\AmountPresenter;
use League\Fractal\TransformerAbstract;

class VoucherTransformer extends TransformerAbstract
{
    public function transform(RewardVoucher $voucher)
    {
        return [
            'id' => $voucher->id,
            'name' => $voucher->name,
            'value' => AmountPresenter::currency($voucher->value, false, 2),
            'number' => $voucher->number,
            'document' => $voucher->document_id ? $voucher->document_id : null,
        ];
    }
}
