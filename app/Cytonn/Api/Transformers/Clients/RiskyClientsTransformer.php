<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 06/11/2018
 * Time: 11:25
 */

namespace Cytonn\Api\Transformers\Clients;

use App\Cytonn\Models\Client\RiskyClient;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\CountryPresenter;
use Cytonn\Presenters\UserPresenter;
use League\Fractal\TransformerAbstract;

class RiskyClientsTransformer extends TransformerAbstract
{
    public function transform(RiskyClient $riskyClient)
    {
        return [
            'id'=>$riskyClient->id,
            'name_or_organization'=> ($riskyClient->type != 1)
                ? $riskyClient->organization : ($riskyClient->firstname .' '. $riskyClient->lastname),
            'email' => $riskyClient->email,
            'nationality' => CountryPresenter::present($riskyClient->country_id),
            'flagged_date'=>$riskyClient->date_flagged,
            'flagged_by'=>UserPresenter::presentFullNames($riskyClient->flagged_by),
            'risky_status_id'=> $riskyClient->risky_status_id,
        ];
    }
}
