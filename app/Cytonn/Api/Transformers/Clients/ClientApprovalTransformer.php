<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/17/18
 * Time: 12:10 PM
 */

namespace App\Cytonn\Api\Transformers\Clients;

use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Core\DataStructures\Carbon;

class ClientApprovalTransformer
{
    public function transformInstruction(ClientInvestmentInstruction $instruction)
    {
        $mandate = $instruction->investment->client->present()->getSigningMandate;

        $approved = $instruction->repo->approvedByClient();

        $date = $instruction->type->name == 'rollover'
            ? Carbon::parse($instruction->updated_at)->toFormattedDateString()
            : Carbon::parse($instruction->withdrawal_date)->toDayDateTimeString();

        return [
            'type' => ucwords($instruction->type->name),
            'product' => $instruction->investment->product->name,
            'amount' => AmountPresenter::currency($instruction->repo->calculateAmountAffected()),
            'date' => $date,
            'signatories' => $this->signatories($instruction->clientApprovals),
            'mandate' => $mandate,
            'status' => $approved
        ];
    }

    public function transformTopup(ClientTopupForm $form)
    {
        $mandate = $form->client->present()->getSigningMandate;

        $approved = $form->repo->approvedByClient();

        return [
            'type' => 'Investment Top Up',
            'product' => $form->product->name,
            'amount' => AmountPresenter::currency($form->amount),
            'date' => Carbon::parse($form->updated_at)->toDayDateTimeString(),
            'signatories' => $this->signatories($form->clientApprovals),
            'mandate' => $mandate,
            'status' => $approved
        ];
    }

    public function transformFundInstruction(UnitFundInvestmentInstruction $instruction)
    {
        $mandate = $instruction->client->present()->getSigningMandate;

        $approved = $instruction->repo->approvedByClient($instruction);

        return [
            'type' => ucwords($instruction->type->name),
            'product' => $instruction->unitFund->name,
            'amount' => AmountPresenter::currency($instruction->repo->calculateAmountAffected($instruction)),
            'date' => Carbon::parse($instruction->updated_at)->toDayDateTimeString(),
            'signatories' => $this->signatories($instruction->clientApprovals),
            'mandate' => $mandate,
            'status' => $approved
        ];
    }

    public function signatories($approvals)
    {
        return $approvals->map(function (ClientInstructionApproval $approval) {

            $name = $approval->user ? $approval->user->firstname . ' ' . $approval->user->lastname : '';

            $status = $approval->approvalStatus ? $approval->approvalStatus->slug : 'pending approval';
            
            $requested = $approval->requestedByUser();

            $showApprove = $approval->client_user_id == \Auth::user()->id;

            return [
                'id' => $approval->id,
                'user' => $name,
                'requested' => $requested,
                'status' => ucwords($status),
                'show_approve' => $showApprove,
                'approval_status' => $approval->status,
                'reason' => $approval->reason ? $approval->reason : 'N/A'
            ];
        });
    }
}
