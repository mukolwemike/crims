<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/12/19
 * Time: 3:40 PM
 */

namespace App\Cytonn\Api\Transformers\Clients;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use League\Fractal\TransformerAbstract;

class RealEstatePaymentTransformer extends TransformerAbstract
{
    public function transform(RealEstatePaymentSchedule $schedule)
    {
        return [
            'id' => $schedule->id,
            'date' => Carbon::parse($schedule->date)->toFormattedDateString(),
            'amount' => AmountPresenter::currency($schedule->amount, false, 0),
            'amount_pending' => AmountPresenter::currency(abs($schedule->overduePayments()), false, 0),
            'unit_no' => $schedule->holding->unit->number,
            'desc' => $schedule->description,
            'type' => $schedule->type->name,
            'overdue_amount' => $schedule->repo->overdue(),
            'project' => $schedule->holding->project->name,
            'unit_id' => $schedule->holding->unit->id
        ];
    }

    public function transformPayment(RealEstatePayment $payment)
    {
        return [
            'id' => $payment->id,
            'desc' => $payment->description,
            'type' => $payment->type->name,
            'project' => $payment->holding->unit->project->name,
            'unit_no' => $payment->holding->unit->number,
            'amount' => AmountPresenter::currency($payment->amount, false, 0),
            'date' => Carbon::parse($payment->date)->toDateString()
        ];
    }
}
