<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/10/18
 * Time: 1:31 PM
 */

namespace App\Cytonn\Api\Transformers\Clients;

use App\Cytonn\Models\Client\ClientNotification;
use App\Cytonn\Models\Client\ClientNotificationRead;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class ClientNotificationTransformer extends TransformerAbstract
{
    public function transform(ClientNotification $notification)
    {
        $user = \Auth::user();

        return [
            'id' => $notification->id,
            'short_description' => $notification->short_description,
            'description' => $notification->description,
            'created_by' => $notification->user ? $notification->user->firstname : '',
            'status' => $notification->read($user) ? 'read' : 'unread',
            'url' => $notification->target_route,
            'date' => Carbon::parse($notification->created_at)->toDayDateTimeString()
        ];
    }
}
