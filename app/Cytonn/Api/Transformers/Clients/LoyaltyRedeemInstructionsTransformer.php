<?php

namespace App\Cytonn\Api\Transformers\Clients;

use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Models\RewardVoucher;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class LoyaltyRedeemInstructionsTransformer extends TransformerAbstract
{
    public function transform(LoyaltyPointsRedeemInstructions $instruction)
    {
        $payload = \GuzzleHttp\json_decode($instruction->payload);

        return [
            'id' => $instruction->id,
            'amount' => AmountPresenter::currency($instruction->amount, false, 2),
            'vouchers' => $this->getVoucherIds($payload),
            'date' => DatePresenter::formatDate($instruction->date),
            'origin' => $instruction->origin,
            'status' => $instruction->approval_id ? true : false,
        ];
    }

    private function getVoucherIds($payload)
    {
        $vNames = '';
        if (isset($payload->voucher_id)) {
            $vNames .= RewardVoucher::find($payload->voucher_id)->name;
        } elseif (isset($payload->voucher_ids)) {
            $v = 0;
            foreach ($payload->voucher_ids as $id) {
                $v++;
                $vNames .= $v . '. ' . RewardVoucher::find($id)->name . '. <br/>';
            }
        }

        return $vNames;
    }
}
