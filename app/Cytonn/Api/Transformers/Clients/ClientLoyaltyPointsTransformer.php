<?php

namespace App\Cytonn\Api\Transformers\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class ClientLoyaltyPointsTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        $loyaltyCalc = $client->calculateLoyalty();

        return [
            'id' => $client->id,
            'clientCode' => $client->client_code,
            'client' => ClientPresenter::presentFullNames($client->id),
            'invPoints' => AmountPresenter::currency($loyaltyCalc->getTotalInvestmentsPoints(), false, 2),
            'fundPoints' => AmountPresenter::currency($loyaltyCalc->getTotalFundsPoints(), false, 2),
            'rePoints' => AmountPresenter::currency($loyaltyCalc->getTotalHoldingsPoints(), false, 2),
            'totalPoints' => AmountPresenter::currency($loyaltyCalc->getTotalPoints(), false, 2),
            'availablePoints' => AmountPresenter::currency($loyaltyCalc->getAvailablePoints(), false, 2),
            'currency' => 'KES',
            'value' => AmountPresenter::currency($loyaltyCalc->getAvailablePoints(), false, 2)
        ];
    }
}
