<?php
/**
 * Date: 14/12/2016
 * Time: 12:00
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CustodialTransaction;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class CustodialTransactionTransformer extends TransformerAbstract
{
    public function transform(CustodialTransaction $transaction)
    {
        $amount = $transaction->amount;
        if (is_null($transaction->client)) {
            $full_name = $transaction->received_from;
            $client_id = null;
            $client_code = null;
        } else {
            $full_name = ClientPresenter::presentFullNames($transaction->client_id);
            $client_code = $transaction->client->client_code;
            $client_id = $transaction->client_id;
        }

        $fa = is_null($transaction->recipient) ? null : $transaction->recipient->name;

        $description = $transaction->description;
        if ($full_name) {
            $description = $full_name.' : '.$description;
        }

        return [
            'id'            =>  $transaction->id,
            'description'   =>  $description,
            'date'          =>  $transaction->date,
            'amount'        =>  $amount,
            'credit'        =>  $amount > 0 ? null : AmountPresenter::currency($amount, true, 2),
            'debit'         =>  $amount <= 0 ? null : AmountPresenter::currency($amount, true, 2),
            'type_name'     =>  $transaction->typeName(),
            'type'          =>  $transaction->type,
            'balance'       =>  is_null($transaction->custodial_account_id) ? null : AmountPresenter::currency($transaction->balance(), false, 2),
            'editable'      =>  (bool) $transaction->transactionType->editable,
            'full_name'     =>  $full_name,
            'fa'            =>  $fa,
            'client_code'   =>  $client_code,
            'client_id'     =>  $client_id,
            'transferred'   =>  ClientPresenter::presentTransferred($transaction->transferred),
            'account'       =>  is_null($transaction->custodial_account_id)
                ? null
                : $transaction->custodialAccount->account_name,
            'ref_no'        =>  $transaction->bank_reference_no,
        ];
    }
}
