<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Product;
use League\Fractal\TransformerAbstract;
use Cytonn\Presenters\AmountPresenter;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        $acc = $product->repo->collectionAccount('bank');
        $mpesa = $product->repo->collectionAccount('mpesa');

        return [
            'id'=>$product->id,
            'name'=>$product->name,
            'longname'=>$product->longname,
            'shortname'=>$product->shortname,
            'description'=>$product->description,
            'minimumInvestmentAmount' => (int)$product->minimum_investment_amount,
            'minimumTopupAmount' => AmountPresenter::currency($product->minimum_topup_amount),
            'currency'=>[
                'id'=>$product->currency->id,
                'code'=>$product->currency->code
            ],
            'collection_accounts' => [
                'bank' => is_null($acc) ? null : [
                    'account_name' => $acc->account_name,
                    'bank_name' => $acc->bank_name,
                    'branch' => $acc->branch_name,
                    'number' => $acc->account_no,
                    'swift_code' => $acc->bank_swift
                ],
                'mpesa' => is_null($mpesa) ? null : [
                    'paybill' => $mpesa->account_no
                ]
            ]
        ];
    }
}
