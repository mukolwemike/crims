<?php
/**
 * Date: 17/03/2016
 * Time: 12:02 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\CustodialAccount;
use League\Fractal\TransformerAbstract;

class CustodialAccountTransformer extends TransformerAbstract
{
    public function transform(CustodialAccount $account)
    {
        return [
            'id'=>$account->id,
            'name'=>$account->account_name,
            'currency_code'=>$account->currency->code,
            'full_name'=>$account->full_name
        ];
    }
}
