<?php
/**
 * Date: 16/07/2016
 * Time: 9:31 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

/**
 * Class ModelTransformer
 *
 * @package Cytonn\Api\Transformers
 */
class ModelTransformer extends TransformerAbstract
{
    /**
     * @param Model $model
     * @return array
     */
    public function transform(Model $model)
    {
        return $model->toArray();
    }
}
