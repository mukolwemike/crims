<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Portfolio\EquityDividend;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class EquityDividendTransformer extends TransformerAbstract
{
    public function transform(EquityDividend $dividend)
    {
        return [
            'id'                =>  $dividend->id,
            'security'          =>  $dividend->security->name,
            'dividends'         =>  AmountPresenter::currency($dividend->dividend),
            'shares'            =>  AmountPresenter::currency($dividend->shares, true, 0),
            'number'            =>  AmountPresenter::currency($dividend->number, true, 0),
            'total_amount_paid' =>  AmountPresenter::currency($dividend->number * $dividend->dividend),
            'date'              =>  DatePresenter::formatDate($dividend->date),
            'prevailing_tax_rate'=>  $dividend->prevailingTaxRate(),
            'book_closure_date' =>  DatePresenter::formatDate($dividend->book_closure_date),
            'conversion_price'  =>  AmountPresenter::currency($dividend->conversion_price),
            'received'          =>  ($dividend->received)? 'Received' : 'Not Received'
        ];
    }
}
