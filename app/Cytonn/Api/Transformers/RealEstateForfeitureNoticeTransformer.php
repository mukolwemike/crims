<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 11/09/2018
 * Time: 07:41
 */

namespace App\Cytonn\Api\Transformers;

use App\Cytonn\Models\RealestateForfeitureNotice;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class RealEstateForfeitureNoticeTransformer extends TransformerAbstract
{
    public function transform(RealestateForfeitureNotice $notices)
    {
        return [
            'project_name' => $notices->holding->project->name,
            'project_id' => $notices->holding->project->id,
            'unit_id' => $notices->holding->unit->number,
            'client_code' => $notices->holding->client->client_code,
            'client_name' => ClientPresenter::presentFullNames($notices->holding->client_id),
            'status' => $notices->status,
            'id' => $notices->id
        ];
    }
}
