<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientBank;
use League\Fractal\TransformerAbstract;

class BankTransformer extends TransformerAbstract
{
    public function transform(ClientBank $bank)
    {
        return [
            'id'            =>  $bank->id,
            'name'          =>  $bank->name,
            'swiftCode'     =>  $bank->swift_code,
            'clearingCode'  =>  $bank->clearing_code,
        ];
    }
}
