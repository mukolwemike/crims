<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class ClientFilledInvestmentApplicationTransformer extends TransformerAbstract
{
    public function transform(ClientFilledInvestmentApplication $application)
    {
        if (!is_null($application->filled)) {
            return $this->processedApplication($application->application);
        }

        return $this->clientFilledApplication($application);
    }

    public function clientFilledApplication(ClientFilledInvestmentApplication $application)
    {
        $product = $this->appProduct($application);

        $app = $application->toArray();

        $app['name'] = $this->clientName($application);

        $app['amount'] = $application->amount;

        $app['product_name'] = $product ? $product->name: null;

        $app['currency_name'] = $product? $product->currency->code : null;

        $app['updated_at'] = Carbon::parse($application->updated_at)->toFormattedDateString();

        $app['created_at'] = Carbon::parse($application->created_at)->toFormattedDateString();

        $app['used'] = !is_null($application->filled);

        $app['email_indemnity'] = $application->filledDocuments()->documentType('email_indemnity')->exists();

        return $app;
    }

    public function clientName(ClientFilledInvestmentApplication $app)
    {
        if ($app->client_id) {
            $client = Client::findOrFail($app->client_id);

            return $client->name();
        }

        if ($app->individual == 2) {
            return $app->registered_name;
        }

        $title = ($app->clientTitle) ? $app->clientTitle->name : null;

        return $title .' '. $app->firstname . ' ' . $app->lastname ;
    }

    public function appProduct(ClientFilledInvestmentApplication $application)
    {
        if (!is_null($application->product_id)) {
            return $application->product;
        } elseif ($application->unit_fund_id) {
            return UnitFund::findOrFail($application->unit_fund_id);
        }
    }


    public function processedApplication(ClientInvestmentApplication $application)
    {
        $app = $application->toArray();

        $app['name'] = ($application->clientType->name == 'individual')
                ? $application->firstname.' '.$application->middlename.' '.$application->lastname
                : $application->registered_name;

        $app['updated_at'] = $application->updated_at->toIso8601String();

        $app['created_at'] = $application->created_at->toIso8601String();

        if (!is_null($application->product)) {
            $app['product_name'] = $application->product->name;

            $app['currency_name'] = $application->product->currency->code;
        } elseif ($application->unit_fund_id) {
            $fund = UnitFund::findOrFail($application->unit_fund_id);

            $app['product_name'] = $fund->name;

            $app['currency_name'] = $fund->currency->code;
        } else {
            $app['product_name'] = '';

            $app['currency_name'] = '';
        }

        $app['email_indemnity'] = (bool) $application->email_indemnity;

        $app['used'] = true;

        $app['approved'] = ($application->approval && $application->approval->approved) ? true : false;

        return $app;
    }
}
