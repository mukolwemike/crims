<?php
/**
 * Date: 08/03/2016
 * Time: 8:19 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\System\Channel;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class ClientInvestmentTopupInstructionTransformer extends TransformerAbstract
{
    public function transform(ClientTopupForm $form)
    {
        return [
            'id' => $form->id,
            'client_code' => $form->client ? $form->client->client_code : '',
            'client_name' => ClientPresenter::presentFullNames($form->client_id),
            'amount' => $form->amount,
            'currency' => $form->product->currency->code,
            'product_name' => $form->product->shortname,
            'agreed_rate' => $form->agreed_rate,
            'tenor' => $form->tenor,
            'origin' => ($form->channel_id) ? Channel::find($form->channel_id)->slug : (($form->user_id) ? 'client' : 'admin'),
            'approved' => $form->repo->approvedByClient(),
            'invested' => $form->invested(),
            'inactive' => $form->inactive == 1 ? true : false,
            'created_at' => $form->created_at->toDateTimeString()
        ];
    }
}
