<?php

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\RealestateLetterOfOffer;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class LOOTransformer extends TransformerAbstract
{
    public function transform(RealestateLetterOfOffer $loo)
    {
        return [
            'id'                    => $loo->id,
            'client_code'           =>  $loo->holding->client->client_code,
            'fullName'              =>  ClientPresenter::presentFullNames($loo->holding->client_id),
            'project'               =>  $loo->holding->project->name,
            'advocate'              =>  is_null($loo->advocate) ? 'Not Set': $loo->advocate->name,
            'vendor'                =>  $loo->holding->project->vendor,
            'unit_number'           =>  $loo->holding->unit->number,
            'size'                  =>  $loo->holding->unit->size->name . ' ' . $loo->holding->unit->type->name,
            'status'                =>  (bool) $loo->sent_on,
            'rejected'              =>  (bool) ! $loo->pm_approved_on and $loo->rejects->count() > 0,
            'pm_approved_on'        => $loo->pm_approved_on,
            'deleted_at'            =>  $loo->deleted_at,
            'document_id'           => $loo->document_id
        ];
    }
}
