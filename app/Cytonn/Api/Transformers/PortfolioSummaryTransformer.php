<?php
/**
 * Date: 06/04/2016
 * Time: 4:20 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Summary\Analytics;
use League\Fractal\TransformerAbstract;

class PortfolioSummaryTransformer extends TransformerAbstract
{
    private $currency;

    protected $fundManager;

    protected $date;

    protected $unitFund;

    public function __construct(
        Currency $currency,
        FundManager $fundManager,
        $date = null,
        UnitFund $unitFund = null
    ) {
        $this->currency = $currency;
        $this->fundManager = $fundManager;
        $this->date = $date;
        $this->unitFund = $unitFund;
    }

    public function transform(PortfolioSecurity $security)
    {
        $analytics = new Analytics($this->fundManager, $this->date);

        $analytics->setPortfolioInstitution($security);

        if ($this->unitFund) {
            $analytics->setFund($this->unitFund);
        }

        $gross = $analytics->grossInterest();

        $net = $analytics->netInterest();

        return [
            'id' => $security->id,
            'code' => $security->code,
            'name' => $security->name,
            'amount' => $analytics->costValue(),
            'value' => $analytics->marketValue(),
            'gross_interest' => $gross,
            'adjusted_gross_interest' => $analytics->adjustedGrossInterest(),
            'net_interest'=> $net,
            'interest_repaid' => $analytics->interestRepaid(),
            'withholding_tax'=> $gross - $net
        ];
    }
}
