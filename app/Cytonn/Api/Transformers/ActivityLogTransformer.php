<?php
/**
 * Date: 05/03/2016
 * Time: 12:34 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ActivityLog;
use League\Fractal\TransformerAbstract;

class ActivityLogTransformer extends TransformerAbstract
{
    public function transform(ActivityLog $log)
    {
        return [
            'id'=>$log->id,
            'target'=>$log->target,
            'text'=>$log->text,
            'time'=>$log->created_at->toDateTimeString()
        ];
    }
}
