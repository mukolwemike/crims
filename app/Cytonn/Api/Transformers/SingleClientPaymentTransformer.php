<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Api\Transformers;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ShareHolder;
use Cytonn\Presenters\ClientPresenter;
use League\Fractal\TransformerAbstract;

class SingleClientPaymentTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'membership_confirmation'
    ];

    /**
     * @param ClientPayment $payment
     * @return array
     */
    public function transform(ClientPayment $payment)
    {
        if (is_null($payment->client)) {
            $full_name = null;

            $client_id = null;

            $client_code = null;
        } else {
            $full_name = ClientPresenter::presentJointFullNames($payment->client_id);

            $client_code = $payment->client->client_code;

            $client_id = $payment->client_id;
        }

        $entity = is_null($entity = $payment->shareEntity) ? null : $entity->name;

        $project = is_null($payment->project) ? null : $payment->project->name;

        $product = is_null($payment->product) ? null : $payment->product->name;

        $payment_to = $entity.''.$product.$project;

        $share_holder_id = null;

        if ($client_id and $payment->shareEntity) {
            $share_holder = ShareHolder::where('client_id', $client_id)
                ->where('entity_id', $payment->shareEntity->id)
                ->first();

            is_null($share_holder) ? : $share_holder_id = $share_holder->id;
        }

        return [
            'id'            =>  $payment->id,
            'description'   =>  $payment->description,
            'date'          =>  $payment->date->toDateString(),
            'amount'        =>  (double)$payment->amount,
            'full_name'     =>  $full_name,
            'fa'            => is_null($payment->recipient)
                ? null
                : $payment->recipient->name,
            'client_code'   =>  $client_code,
            'client_id'     =>  $client_id,
            'share_holder_id'=>  $share_holder_id,
            'type'          =>  $payment->type->name,
            'type_slug'     =>  $payment->type->slug,
            'share_entity'  =>  $entity,
            'account'       =>  is_null($trans = $payment->custodialTransaction)
                ? null
                : $trans->custodialAccount->account_name,
            'project'       =>  $project,
            'product'       =>  $product,
            'entity'        =>  $entity,
            'payment_for'   =>  $payment_to,
            'balance'       =>  $payment->paymentBalance()
        ];
    }

    /**
     * @param ClientPayment $payment
     * @return \League\Fractal\Resource\Item
     */
    public function includeMembershipConfirmation(ClientPayment $payment)
    {
        return $this->item($payment, new CoopMembershipConfirmationTransformer());
    }
}
