<?php

namespace Cytonn\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class AuthorizeAPI
{
    public function apiRequest($url)
    {
        $client=new Client(['base_uri'=>'https://hr.cytonn.com/']);
        $token=$this->getAccessToken();
        $response=$client->get(
            $url,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]
        );

        try {
            $data = $response;
        } catch (\Exception $e) {
            throw new RequestException($e->getMessage(), $response);
        }
        return $data;
    }

    private function getAccessToken()
    {
        $client=new Client(['base_uri'=>'https://hr.cytonn.com/']);
        $accessToken = null;
        try {
            $response = $client->post(
                env('HR_AUTH_URL'),
                [
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                        'client_id' => env('HR_CLIENT_ID'),
                        'client_secret' => env('HR_CLIENT_SECRET')
                    ]
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        } catch (\Exception $e) {
            $response = null;
        }

        if ($response && $response->getStatusCode() == 200) {
            // $accessTokenObject = (object) $response->json(['object'=>true]);
            $accessTokenObject = json_decode($response->getBody());
            $accessToken = $accessTokenObject->access_token;
        }
        return $accessToken;
    }
}
