<?php
/**
 * Date: 28/01/2016
 * Time: 5:14 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Api\DataTables;

use Cytonn\Api\Transformers\ModelTransformer;
use Illuminate\Database\QueryException;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class SortFilterPaginateTrait
 *
 * @package Cytonn\Api\DataTables
 */
trait SortFilterPaginateTrait
{
    /**
     * @var int
     */
    protected $perPage = 10;

    /*
     * Define some of the required variables
     */
    protected $elastic = false;
    protected $stateArray = [];
    protected $direction = 'asc';
    protected $column = 'id';
    protected $elasticSearchFilterFunc = null;

    /*
     * Get the filter, sort and paginate data from the
     */
    public function getTableStateData()
    {
        $state = \Request::get('tableState');

        $stateArray = array();

        //Get the Sort Query
        $dir = $this->direction;
        $term = $this->column;
        if (isset($state['sort'])) {
            if (isset($state['sort']['reverse'])) {
                $state['sort']['reverse'] == 'true' ? $dir = 'desc' : $dir = 'asc';
            }

            if (isset($state['sort']['predicate'])) {
                $term = $state['sort']['predicate'];
            }
        }
        $stateArray['sortDirection'] = $dir;
        $stateArray['sortItem'] = $term;


        //Get the Pagination Details
        if ($state['pagination']) {
            if (isset($state['pagination']['start'])) {
                $stateArray['offset'] = (int)$state['pagination']['start'];
            } else {
                $stateArray['offset'] = 0;
            }

            if (isset($state['pagination']['number'])) {
                $stateArray['itemsPerPage'] = $state['pagination']['number'];
            } else {
                $stateArray['itemsPerPage'] = $this->perPage;
            }
        } else {
            $stateArray['offset'] = 0;

            $stateArray['itemsPerPage'] = $this->perPage;
        }

        return $stateArray;
    }

    /*
     * Check if the request has a search query
     */
    public function hasSearchQuery()
    {
        $state = \Request::get('tableState');

        return isset($state['search']['predicateObject']['$']);
    }

    /**
     * filter, sort and paginate the model
     *
     * @param  $model
     * @return array
     */
    public function sortFilterPaginate($model)
    {
        $filtered = $this->filter($model);

        $sorted = $this->sort($filtered);

        return $this->paginate($sorted);
    }

    /**
     * filter the model
     *
     * @param  $model
     * @return mixed
     */
    public function filter($model)
    {
        $state = \Request::get('tableState');

        //filter
        if (isset($state['search']['predicateObject']['$'])) {
            $query = $state['search']['predicateObject']['$'];

            if ($this->elastic) {
                $model = $this->elasticSearchFilter($model, $query, $this->elasticSearchFilterFunc);
            } else {
                $model = $model::search($query, null, true)->distinct();
            }
        }

        return $model;
    }

    private function filterUsingParams($model, $filterParams)
    {
        foreach ($filterParams as $filterParam) {
            if (isset($filterParam['sign'])) {
                $model = $model->where($filterParam['column'], $filterParam['sign'], $filterParam['value']);
            } else {
                $model = $model->where($filterParam['column'], $filterParam['value']);
            }
        }

        return $model;
    }

    public function elasticSearchFilter($model, $query, \Closure $filterFunc = null)
    {
        $this->stateArray = $this->getTableStateData();

        $model = $model->sortedSearch($query, $this->stateArray['sortItem'], $this->stateArray['sortDirection']);

        if (! is_null($filterFunc)) {
            $model = $filterFunc($model);
        }

        $paginatedModel = $model->slice($this->stateArray['offset'], $this->stateArray['itemsPerPage']);

        return [
            'model' => $paginatedModel,
            'elastic_search_complete' => true,
            'offset' => $this->stateArray['offset'],
            'total_pages' => ceil(count($model)/$this->stateArray['itemsPerPage']),
            'total_results' => count($model)
        ];
    }

    private function elasticFilterUsingParams($model)
    {
        foreach ($this->filterParams as $filterParam) {
            if (isset($filterParam['sign'])) {
                if ($filterParam['sign'] == '=') {
                    $model = $model->where($filterParam['column'], $filterParam['value']);
                }
            } else {
                $model = $model->where($filterParam['column'], $filterParam['value']);
            }
        }

        return $model;
    }

    /**
     * Sort the model
     *
     * @param  $model
     * @return mixed
     */
    public function sort($model)
    {
        $state = \Request::get('tableState');

        //sort
        if (isset($state['sort'])) {
            try {
                $state['sort']['reverse'] == 'true' ? $dir = 'DESC' : $dir = 'ASC' ;

                $model = $model->orderBy($state['sort']['predicate'], $dir);
            } catch (QueryException $e) {
            }
        }

        return $model;
    }

    /**
     * Paginate the model
     *
     * @param  $model
     * @return array
     */
    public function paginate($model)
    {
        $state = \Request::get('tableState');

        $model_count = $model->count();

        if (!isset($state['pagination'])) {
            $state['pagination']['start'] = 0;
        }
        if (!isset($state['pagination']['number'])) {
            $state['pagination']['number'] = $this->perPage;
        }

        //pagination
        if ($state['pagination']) {
            $offset = (int)$state['pagination']['start'];
            $this->perPage = ($state['pagination']['number'] != 0)
                ? $state['pagination']['number']
                : $this->perPage;
            $model = $model->offset($offset)->take($this->perPage)->get();
        } else {
            $offset = 0;
            $model = $model->offset($offset)->take($this->perPage)->get();
        }

        $totalPages = ceil($model_count/$this->perPage);

        /*
         * Sometimes the search return duplicates, the duplicates should be filtered out
         */
        return [
            'model'=>$model,
            'offset'=>$offset,
            'total_pages'=>$totalPages
        ];
    }

    /**
     * @param $model
     * @return array
     */
    public function sortAndPaginate($model)
    {
        return $this->paginate($this->sort($model));
    }

    /**
     * Add the pagination information to the resource
     *
     * @param  $paginatedModel
     * @param  \League\Fractal\Resource\Collection $resource
     * @return mixed
     */
    public function addPaginationToResource($paginatedModel, $resource)
    {
        return $resource->setMetaValue('pagination', [
            'offset'=>$paginatedModel['offset'],
            'total_pages'=>$paginatedModel['total_pages']
        ]);
    }


    /**
     * Combine all functions into one simple API
     *
     * @param  $model
     * @param  TransformerAbstract|null $transformer
     * @param  \Closure|null $filterFunc
     * @param  \Closure|null $modifyResource
     * @param bool $elastic
     * @param \Closure|null $elasticSearchFilterFunc
     * @return string
     */
    public function processTable(
        $model,
        TransformerAbstract $transformer = null,
        \Closure $filterFunc = null,
        \Closure $modifyResource = null,
        $elastic = false,
        \Closure $elasticSearchFilterFunc = null
    ) {
        $this->elastic = $elastic;
        $this->elasticSearchFilterFunc = $elasticSearchFilterFunc;

        $filtered = $this->filter($model);

        if (is_null($transformer)) {
            $transformer = new ModelTransformer();
        }

        if (array_key_exists('elastic_search_complete', $filtered)) {
            $paginated = $filtered;
        } else {
            if (!is_null($filterFunc)) {
                $filtered = $filterFunc($filtered);
            }

            $paginated = $this->sortAndPaginate($filtered);
        }

        $resource = new Collection($paginated['model'], $transformer);

        $this->addPaginationToResource($paginated, $resource);

        if (!is_null($modifyResource)) {
            $modifyResource($resource);
        }

        $manager = new Manager();

        $state = \Request::get('tableState');

        if (isset($state['includes'])) {
            $manager = $manager->parseIncludes($state['includes']);
        }

        return $manager->createData($resource)->toJson();
    }
}
