<?php
/**
 * Date: 08/09/2016
 * Time: 8:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\Validation;

/**
 * Class ValidatorTrait
 * Used to Validate or Clean Data, Calls method on Cytonn\Core\Validation\Validator
 *
 * @method  bool validate(array $data, array $rules, array $messages = [])
 * @method  bool isValid(array $data, array $rules, array $messages = [])
 * @method  setup(array $rules, array $messages = [])
 * @method  array filter(array $data, array $allowedKeys)
 * @method  array reject(array $data, array $removeKeys)
 * @method  array getValidationErrors()
 * @method  array fillMissingKeys(array $data, array $keys)
 * @package Cytonn\Core\Validation
 */
trait ValidatorTrait
{

    /**
     * Call methods from the validator class
     *
     * @param  $name
     * @param  $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        $class_name = Validator::class;

        $validator = app($class_name);

        if (method_exists($validator, $name)) {
            return call_user_func_array([$validator, $name], $args);
        } else {
            throw new \BadMethodCallException("The method $name does not exist in $class_name");
        }
    }
}
