<?php
/**
 * Date: 08/09/2016
 * Time: 8:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\Validation;

use Illuminate\Support\Arr;
use Laracasts\Validation\FormValidationException;
use Laracasts\Validation\FormValidator;

/**
 * Class Validator
 *
 * @package Cytonn\Core\Validation
 */
class Validator extends FormValidator
{
    /**
     * @var
     */
    protected $validationErrors;

    /**
     * @var array
     */
    public $rules;

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @return mixed
     */
    public function validate($data, $rules = [], $messages = [])
    {
        $this->setup($rules, $messages);

        return parent::validate($data);
    }

    /**
     * @param array $rules
     * @param array $messages
     */
    public function setup(array $rules, array $messages = [])
    {
        $this->rules = $rules;
        $this->messages = $messages;
    }

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @return bool
     */
    public function isValid(array $data, array $rules, array $messages = [])
    {
        try {
            $this->validate($data, $rules, $messages);
        } catch (FormValidationException $e) {
            $this->validationErrors = $e->getErrors();

            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * @param array $data
     * @param array $allowedKeys
     * @return array
     */
    public function filter(array $data, array $allowedKeys)
    {
        return Arr::only($data, $allowedKeys);
    }

    /**
     * @param array $data
     * @param array $removeKeys
     * @return array
     */
    public function reject(array $data, array $removeKeys)
    {
        return Arr::except($data, $removeKeys);
    }

    /**
     * @param array $data
     * @param array $keys
     * @return array
     */
    public function fillMissingKeys(array $data, array $keys)
    {
        foreach ($keys as $item) {
            if (!isset($data[$item])) {
                $data[$item] = null;
            }
        }

        return $data;
    }
}
