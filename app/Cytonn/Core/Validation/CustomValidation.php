<?php
/**
 * Date: 14/09/2016
 * Time: 8:51 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\Validation;

use Illuminate\Support\Facades\App;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Validator;

class CustomValidation extends Validator
{


    /**
     * CustomValidation constructor.
     */
    public function __construct(Translator $translator)
    {
        parent::__construct($translator, [], []);
    }

    public function validateSameOrAfter($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'same_or_after');

        $this->customMessages['same_or_after'] = "The :attribute must be on or after $parameters[0].";

        if ($format = $this->getDateFormat($attribute)) {
            return $this->validateAfterWithFormat($format, $value, $parameters);
        }

        if (! ($date = strtotime($parameters[0]))) {
            return strtotime($value) >= strtotime($this->getValue($parameters[0]));
        }

        return strtotime($value) >= $date;
    }

    public function validateSameOrBefore($attribute, $value, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'same_or_before');

        $this->customMessages['same_or_before'] = "The :attribute must be on or before $parameters[0].";

        if ($format = $this->getDateFormat($attribute)) {
            return $this->validateBeforeWithFormat($format, $value, $parameters);
        }

        if (! ($date = strtotime($parameters[0]))) {
            return strtotime($value) <= strtotime($this->getValue($parameters[0]));
        }

        return strtotime($value) <= $date;
    }
}
