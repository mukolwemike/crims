<?php
/**
 * Date: 11/10/2016
 * Time: 11:25
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\DataStructures;

use Cytonn\Lib\Helper;

/**
 * Class Number
 *
 * @package Cytonn\Core\DataStructures
 */
class Number
{
    /**
     * @param $number
     * @param bool   $cast_int
     * @return bool|null|string
     */
    public static function toWords($number, $cast_int = false)
    {
        return (new Helper())->convertNumberToWords($number, $cast_int);
    }

    /**
     * Convert number to 1st, 2nd, 3rd
     *
     * @param  $num
     * @return string
     */
    public static function ordinalize($num)
    {
        if (!is_numeric($num)) {
            return $num;
        }

        if ($num % 100 >= 11 and $num % 100 <= 13) {
            return $num."th";
        } elseif ($num % 10 == 1) {
            return $num."st";
        } elseif ($num % 10 == 2) {
            return $num."nd";
        } elseif ($num % 10 == 3) {
            return $num."rd";
        } else {
            return $num."th";
        }
    }

    public static function ordinaliseWords($number)
    {
        if (!is_numeric($number)) {
            return $number;
        }

        $in_words = self::toWords($number);

        switch ($number) {
            case 1:
                return 'first';
                break;
            case 2:
                return 'second';
                break;
            case 3:
                return 'third';
                break;
            case 5:
                return 'fifth';
                break;
            case 8:
                return 'eighth';
                break;
            case 20:
                return 'twentieth';
                break;
            case 30:
                return 'thirtieth';
                break;
            default:
                return $in_words.'th';
        }
    }
}
