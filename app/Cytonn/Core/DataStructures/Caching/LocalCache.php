<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 30/10/2018
 * Time: 10:11
 */

namespace App\Cytonn\Core\DataStructures\Caching;

trait LocalCache
{
    public static $localCacheRepo = [];

    private static $localCacheLimit = 1000;

    /**
     * @param int $localCacheLimit
     */
    public static function setLocalCacheLimit(int $localCacheLimit)
    {
        self::$localCacheLimit = $localCacheLimit;
    }

    public function cache($key, $value = null)
    {
        return self::setToCache($key, $value);
    }

    public static function setToCache($key, $value = null)
    {
        if ($value) {
            static::$localCacheRepo[$key] = $value;

            if (count(static::$localCacheRepo) > static::$localCacheLimit) {
                $fk = array_first(array_keys(static::$localCacheRepo));

                unset(static::$localCacheRepo[$fk]);
            }

            return $value;
        }

        if (isset(static::$localCacheRepo[$key])) {
            return static::$localCacheRepo[$key];
        }
    }
}
