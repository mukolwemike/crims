<?php
/**
 * Date: 09/07/2016
 * Time: 11:23 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\DataStructures;

use Carbon\Carbon as ParentCarbon;

/**
 * Class Carbon
 *
 * @package Cytonn\Core\DataStructures
 */
class Carbon extends ParentCarbon
{
    /**
     * Constructor
     *
     * @param null $time
     * @param null $tz
     */
    public function __construct($time = null, $tz = null)
    {
        parent::__construct($time, $tz);
    }

    /**
     * Create a new instance
     *
     * @param  null $time
     * @param  null $tz
     * @return static
     */
    public static function make($time = null, $tz = null)
    {
        return new static($time, $tz);
    }

    /**
     * Get the number of days into the month
     *
     * @return int
     */
    public function daysIntoMonth()
    {
        return 1 + $this->copy()->diffInDays($this->copy()->startOfMonth());
    }

    /**
     * @return int
     */
    public function daysIntoYear()
    {
        return 1 + $this->copy()->diffInDays($this->copy()->startOfYear());
    }

    /**
     * Get the number of days in current month
     *
     * @return int
     */
    public function daysInMonth()
    {
        return 1 + $this->copy()->endOfMonth()->diffInDays($this->copy()->startOfMonth());
    }

    /**
     * @param ParentCarbon $first
     * @param ParentCarbon $second
     * @return ParentCarbon
     */
    public static function latest(ParentCarbon $first, ParentCarbon $second)
    {
        if ($first->gt($second)) {
            return $first;
        }

        return $second;
    }

    /**
     * @param ParentCarbon $first
     * @param ParentCarbon $second
     * @return ParentCarbon
     */
    public static function earliest(ParentCarbon $first, ParentCarbon $second)
    {
        if ($first->lt($second)) {
            return $first;
        }

        return $second;
    }

    /**
     * @param ParentCarbon $startDate
     * @param ParentCarbon $endDate
     * @return array
     */
    public static function monthsBetweenArray(ParentCarbon $startDate, ParentCarbon $endDate)
    {
        $start = $startDate->copy();
        $end = $endDate->copy();

        $out = [];

        for ($d = $start; $d->lte($end); $d = $d->addMonthsNoOverflow(1)) {
            $out[] = $d->copy();
        }

        return $out;
    }

    /**
     * @param ParentCarbon $startDate
     * @param ParentCarbon $endDate
     * @return array
     */
    public static function daysBetweenArray(ParentCarbon $startDate, ParentCarbon $endDate)
    {
        $start = $startDate->copy();
        $end = $endDate->copy();

        $out = [];

        for ($d = $start; $d->lte($end); $d = $d->addDay()) {
            $out[] = $d->copy();
        }

        return $out;
    }

    public static function nowIsBetween(\Carbon\Carbon $start, \Carbon\Carbon $end)
    {
        $now = \Carbon\Carbon::now();

        return $start->lte($now) && $end->gte($now);
    }
}
