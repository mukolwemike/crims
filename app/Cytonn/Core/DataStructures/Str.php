<?php
/**
 * Date: 15/10/2016
 * Time: 14:15
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\DataStructures;

use Illuminate\Support\Str as SupportStringStr;

/**
 * Class Str
 *
 * @package Cytonn\Core\DataStructures
 */
class Str extends SupportStringStr
{
    /**
     * @param $str
     * @return string
     */
    public static function snakeToWords($str)
    {
        return ucwords(str_replace('_', ' ', $str));
    }

    public static function removeLineBreaks($str)
    {
        return preg_replace('~[\r\n]+~', '', $str);
    }
}
