<?php
/**
 * Date: 02/11/2016
 * Time: 20:10
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\DataStructures\Files;

use Illuminate\Support\Collection;

/**
 * Class Excel
 *
 * @package Cytonn\Core\DataStructures\Files
 */
class Excel
{

    /**
     * @param $filename
     * @param $data
     * @return mixed
     */
    public static function fromModel($filename, $data)
    {
        if (is_array($data)) {
            $data = Collection::make($data);
        }

        if (!($data->first() instanceof Collection || is_array($data->first()))) {
            $data = ['Sheet 1'=>$data];
        }

        return \Excel::create($filename, function ($excel) use ($data) {
            $index = 0;
            foreach ($data as $name => $items) {
                $index++;

                if (strlen(trim($name)) <= 2) {
                    $name = 'Sheet '.$index;
                }

                $name = self::cleanSheetName($name);

                $excel->sheet($name, function ($sheet) use ($items) {
                    if (is_array($items)) {
                        $items = Collection::make($items);
                    }

                    $sheet->fromModel($items);
                });
            }
        });
    }

    public static function cleanSheetName($name)
    {
        $forbidden = ['*', ':', '/', '\\', '?', '[', ']', '.', '\''];

        foreach ($forbidden as $item) {
            $name = str_replace($item, '', $name);
        }

        return substr($name, 0, 30);
    }
}
