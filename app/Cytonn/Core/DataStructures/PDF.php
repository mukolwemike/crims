<?php
/**
 * Date: 27/02/2018
 * Time: 19:36
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\DataStructures;

class PDF extends \Barryvdh\DomPDF\PDF
{

    /**
     * Set a password to open the PDF
     *
     * @param string $password
     * @throws \Exception
     */
    public function setEncryption($password)
    {
        $this->render();
        $this->dompdf->getCanvas()->get_cpdf()->setEncryption($password, $password);
    }
}
