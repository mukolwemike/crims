<?php
/**
 * Date: 17/06/2016
 * Time: 11:06 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\DataStructures;

use \Illuminate\Support\Collection as IlluminateCollection;

/**
 * Class Collection
 *
 * @package Cytonn\Core\DataStructures
 */
class Collection extends IlluminateCollection
{
    /**
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        parent::__construct($items);
    }

    /**
     * Make a new instance from an instance of Illuminate\Support\Collection
     *
     * @param  IlluminateCollection $collection
     * @return static
     */
    public static function makeFromCollection(IlluminateCollection $collection)
    {
        return new static($collection->items);
    }


    /**
     * Checks if the collection is made of an associative array
     *
     * @return bool
     */
    public function isAssociative()
    {
        return array_keys($this->items) !== range(0, count($this->items) - 1);
    }

    /**
     * Run a map over each of the items, maintain keys if associative
     *
     * @param  \Closure $callback
     * @return static
     */
    public function map(callable $callback)
    {
        if (!$this->isAssociative()) {
            return static::makeFromCollection(parent::map($callback));
        }

        $result = [];

        foreach ($this->items as $key => $val) {
            $result[$key] = $callback($val, $key);
        }

        return new static($result);
    }

    /**
     * Zip the collection together with one or more arrays.
     *
     * e.g. new Collection([1, 2, 3])->zip([4, 5, 6]);
     *      => [[1, 4], [2, 5], [3, 6]]
     *
     * @param  mixed ...$items
     * @return static
     */
    public function zip($items)
    {
        $arrayableItems = array_map(
            function ($items) {
                return $this->getArrayableItems($items);
            },
            func_get_args()
        );

        $params = array_merge(
            [function () {
                return new static(func_get_args());
            }, $this->items],
            $arrayableItems
        );

        return new static(call_user_func_array('array_map', $params));
    }

    /**
     * Transpose an array, maintaining keys
     *
     * @return static
     */
    public function transpose()
    {
        //TODO refactor loops
        $out = [];

        foreach ($this->items as $key => $subarr) {
            foreach ($subarr as $subkey => $subvalue) {
                $out[$subkey][$key] = $subvalue;
            }
        }

        return new static($out);
    }



    /**
     * Filter items by the given key value pair using loose comparison.
     *
     * @param  string $key
     * @param  mixed  $value
     * @return static
     */
    public function whereLoose($key, $value)
    {
        return $this->where($key, $value, false);
    }
}
