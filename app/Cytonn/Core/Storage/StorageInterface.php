<?php
/**
 * Date: 30/05/2016
 * Time: 10:57 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\Storage;

/**
 * Interface StorageInterface
 *
 * @package Cytonn\Core\Storage
 */
/**
 * Interface StorageInterface
 *
 * @package Cytonn\Core\Storage
 */
interface StorageInterface
{
    /**
     * @param $url
     * @return mixed
     */
    public function get($url);

    /**
     * @param $url
     * @param $file_contents
     * @return mixed
     */
    public function put($url, $file_contents);

    /**
     * @return mixed
     */
    public function getStorageLocation();

    /**
     * @return mixed
     */
    public function filesystem();
}
