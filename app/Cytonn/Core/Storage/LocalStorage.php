<?php
/**
 * Date: 30/05/2016
 * Time: 10:57 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\Storage;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

/**
 * Class LocalStorage
 *
 * @package Cytonn\Core\Storage
 */
class LocalStorage implements StorageInterface
{

    /**
     * @var
     */
    protected $client;

    /*
     * @var AwsS3Adapter $client;
     */
    /**
     * @var
     */
    protected $adapter;

    /**
     * @var
     */
    protected $filesystem;


    /**
     *
     */
    public function __construct()
    {
        $this->connect();
    }


    /**
     * @param $url
     * @return mixed
     */
    public function get($url)
    {
        return $this->filesystem->read($url);
    }

    /**
     * @param $url
     * @param $file_contents
     * @return mixed
     */
    public function put($url, $file_contents)
    {
        return $this->filesystem->put($url, $file_contents);
    }

    /**
     * @return mixed
     */
    public function filesystem()
    {
        return $this->filesystem;
    }

    /**
     * Set up the storage
     */
    public function connect()
    {
        $this->adapter = new Local(storage_path(''));

        $this->filesystem = new Filesystem($this->adapter);
    }

    /**
     * Get the location where files are stored
     *
     * @return string
     */
    public function getStorageLocation()
    {
        return 'private';
    }
}
