<?php
/**
 * Date: 30/05/2016
 * Time: 10:58 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Core\Storage;

use Aws\AwsClient;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

/**
 * Class S3Storage
 *
 * @package Cytonn\Core\Storage
 */
class S3Storage implements StorageInterface
{

    /**
     * @var AwsClient $client
     */
    protected $client;

    /*
     * @var AwsS3Adapter $client;
     */
    /**
     * @var
     */
    protected $adapter;

    /**
     * @var
     */
    protected $filesystem;


    /**
     * S3Storage constructor.
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * Get the file
     *
     * @param  $url
     * @return mixed
     */
    public function get($url)
    {
        return $this->filesystem ->read($url);
    }

    /**
     * Save file
     *
     * @param  $url
     * @param  $file_contents
     * @return mixed
     */
    public function put($url, $file_contents)
    {
        return $this->filesystem->put($url, $file_contents);
    }

    /**
     * Get the adapter
     *
     * @return mixed
     */
    public function adapter()
    {
        return $this->adapter;
    }

    /**
     * grab the filesystem object
     *
     * @return mixed
     */
    public function filesystem()
    {
        return $this->filesystem;
    }


    /**
     * Connect to s3 storage
     */
    private function connect()
    {
        $default = config('filesystems.default');

        $config = \config('filesystems.disks.'.$default);

        $this->client = new S3Client(
            [
            'credentials' => [
                'key'    => $config['key'],
                'secret' => $config['secret'],
            ],
            'bucket'=>$config['bucket'],
            'region' => $config['region'],
            'version' => $config['version'],
            ]
        );

        $this->adapter = new AwsS3Adapter($this->client, $config['bucket'], '');

        $this->filesystem = new Filesystem($this->adapter);
    }

    /**
     * Get location of files in s3
     *
     * @return string
     */
    public function getStorageLocation()
    {
        return 'private';
    }

    /**
     * Configure the s3 storage externally
     *
     * @param $key
     * @param $secret
     * @param $bucket
     * @param $region
     * @param null   $endpoint
     * @param string $version
     */
    public function configure($key, $secret, $bucket, $region, $endpoint = null, $version = 'latest')
    {
        $this->client = new S3Client(
            [
            'credentials' => [
                'key'    => $key,
                'secret' => $secret,
            ],
            'bucket'=>$bucket,
            'endpoint'=>$endpoint,
            'region' => $region,
            'version' => $version,
            ]
        );

        $this->adapter = new AwsS3Adapter($this->client, $bucket, '');

        $this->filesystem = new Filesystem($this->adapter);
    }
}
