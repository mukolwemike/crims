<?php

namespace App\Cytonn\Data;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Kyc\IprsRequest;
use App\Cytonn\Models\Client\Kyc\IprsValidation;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Title;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Core\Storage\StorageInterface;
use Cytonn\Presenters\ClientPresenter;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\URL;
use Webpatser\Uuid\Uuid;

class Iprs
{
    protected $client;

    protected $wsdl;

    protected $username;

    protected $password;

    /**
     * Iprs constructor.
     * @param null $wsdl
     */
    public function __construct()
    {
        $this->wsdl = config('services.iprs.wsdl');
        $this->username = config('services.iprs.username');
        $this->password = config('services.iprs.password');
    }

    public function createRequest(Client $client, $id_number = null, ClientJointDetail $joint = null)
    {
        return IprsRequest::create([
            'client_id' => $client->id,
            'id_number' => $id_number ? $id_number : $client->id_or_passport,
            'status' => null,
            'validation_id' => null,
            'joint_id' => $joint ? $joint->id : null
        ]);
    }

    public function makeRequest(IprsRequest $request)
    {
        $local = $this->localRequest($request->id_number);

        if ($local) {
            $this->saveResponse($local, $request);

            return true;
        }

        if (app()->environment('production')) {
            $resp = $this->requestFromIprs($request->id_number);

            if (!$resp instanceof IprsValidation) {
                $this->handleIprsFailure($request, $resp);
                return false;
            }

            $this->saveResponse($resp, $request);

            return true;
        }

        $fake = $this->fakeIprs($request->id_number);

        $this->saveResponse($fake, $request);

        return true;
    }

    protected function saveResponse(IprsValidation $validation, IprsRequest $request)
    {
        $request->status = 1;
        $request->validation()->associate($validation);
        $request->save();

        if (!$request->joint) {
            return $this->saveClientResponse($validation, $request);
        }

        return $this->saveJointResponse($validation, $request);
    }

    /**
     * @param IprsValidation $validation
     * @param IprsRequest $request
     * @return bool
     */
    private function saveClientResponse(IprsValidation $validation, IprsRequest $request)
    {
        $client = $request->client;

        $validClient = $this->validateNames($client, $validation);

        if ($validClient) {
            $validation->clients()->attach($client, ['joint_id' => $request->joint_id]);
            $this->saveAdditionalClientInformation($client, $validation);

            return true;
        }

        $id_names = $validation->firstname.' '.$validation->othername.' '.$validation->surname;

        $this->communicateFailure($request, $validation, "Names on ID ($id_names) do not match given names");

        return false;
    }

    /**
     * @param IprsValidation $validation
     * @param IprsRequest $request
     * @return bool
     */
    private function saveJointResponse(IprsValidation $validation, IprsRequest $request)
    {
        $jointDetail = $request->joint;

        $validJoint = $this->validateNames($jointDetail->client, $validation, $jointDetail);

        if ($validJoint) {
            $this->saveAdditionalJointInformation($request->joint, $validation);

            return true;
        }

        $id_names = $validation->firstname.' '.$validation->othername.' '.$validation->surname;

        $this->communicateFailure($request, $validation, "Names on ID ($id_names) do not match given names");

        return false;
    }

    /**
     * @param IprsRequest $request
     * @param $reason
     */
    private function handleIprsFailure(IprsRequest $request, $reason)
    {
        $request->update([
           'status' => 0,
           'tries' => $request->tries + 1,
           'comment' => $reason
        ]);

        $this->communicateFailure($request, null, "ID Details not found - ".$reason);
    }

    /**
     * @param Client $client
     * @param IprsValidation $validation
     */
    private function saveAdditionalClientInformation(Client $client, IprsValidation $validation)
    {
        if ($client->id_or_passport !== $validation->id_number) {
            return;
        }

        $client->dob ? : $client->update(['dob' => $validation->dob]);


        $title = $validation->gender->abbr == 'M' ? 'Mr.' : 'Ms.';
        $title = Title::where('name', $title)->remember(1000)->first();
        $title_id = $title ? $title->id : null;

        $updateData = [];
        $contact = $client->contact;
        $contact->gender_id ?: $updateData['gender_id'] = $validation->gender_id;
        $contact->title_id ?: $updateData['title_id'] = $title_id;

        if ($updateData) {
            $contact->update($updateData);
        }
    }


    private function saveAdditionalJointInformation(ClientJointDetail $jointDetail, IprsValidation $validation)
    {
        if ($jointDetail->id_or_passport !== $validation->id_number) {
            return;
        }

        $jointDetail->dob ? : $jointDetail->update(['dob' => $validation->dob]);

        $title = $validation->gender->abbr == 'M' ? 'Mr.' : 'Ms.';
        $title = Title::where('name', $title)->remember(1000)->first();
        $title_id = $title ? $title->id : null;

        $jointDetail->gender_id ?: $updateData['gender_id'] = $validation->gender_id;
        $jointDetail->title_id ?: $updateData['title_id'] = $title_id;
        $jointDetail->save();
    }

    private function validateNames(Client $client, IprsValidation $validation, ClientJointDetail $joint = null)
    {
        $fullnames = ClientPresenter::presentFullNameNoTitle($client->id);

        if ($joint) {
            $fullnames = $joint->firstname. ' '. $joint->middlename.' '.$joint->lastname;
        }

        $iprs_names = $validation->firstname. " ".$validation->othername. " ".$validation->surname;

        $names = $this->getNonMatchingNames($iprs_names, $fullnames);

        if ($names->count() >= 2) {
            return false;
        }

        $this->createKYCRecord($validation, $client);

        return true;
    }

    public function getNonMatchingNames($iprs_names, $given_names)
    {
        return collect(explode(" ", $iprs_names))->filter(function ($name) {
            return !empty($name);
        })->filter(function ($name) use ($given_names) {
            return !str_contains(strtolower($given_names), strtolower($name));
        });
    }

    private function createKYCRecord(IprsValidation $validation, Client $client)
    {
        $kyc = null;
        if ($client->fund_manager_id) {
            $kyc = ClientComplianceChecklist::where('fund_manager_id', $client->fund_manager_id)
                ->where('slug', 'id_or_passport')
                ->remember(1000)
                ->first();
        }

        if (!$kyc) {
            $kyc = ClientComplianceChecklist::where('slug', 'id_or_passport')
                ->remember(1000)
                ->first();
        }

        $names = $validation->firstname. " ".$validation->othername. " ".$validation->surname;

        ClientUploadedKyc::create([
            'kyc_id' => $kyc->id,
            'client_id' => $client->id,
            'user_id' => getSystemUser()->id,
            'iprs_validation_id' => $validation->id,
            'number' => $validation->id_number,
            'comment' => "ID $validation->id_number for $names validated by IPRS"
        ]);
    }

    private function communicateFailure(IprsRequest $request, IprsValidation $validation = null, $reason)
    {
        $client = $request->client;

        $fullnames = ClientPresenter::presentFullNameNoTitle($request->client_id);

        $url = URL::route('view_client', ['id' => $client->id]);

        //TODO email client to update details or provide proof
        Mail::compose()
            ->to(systemEmailGroups(['fintech']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : IPRS Validation Failed')
            ->text("
                    <p>Name: {$fullnames} </p>
                    <p>ID: {$request->id_number} </p>
                    <p>Reason: {$reason}</p>
                    <p>Check client code
                    <a href=\"{$url}\">{$client->client_code}</a>
                     for more details</p>
                ")->send();
    }

    protected function testIprsLogin()
    {
        $result = $this->client()->Login(['log' => $this->username, 'pass' => $this->password]);

        return $result->LoginResult;
    }

    protected function localRequest($id)
    {
        return IprsValidation::where('id_number', $id)->first();
    }


    protected function requestFromIprs($id)
    {
        $result = $this->checkProvidedNumber($id);

        if ($result->ErrorOcurred) {
            return $result->ErrorMessage;
        }

        $gender = Gender::where('abbr', $result->Gender)->remember(1000)->first();

        return IprsValidation::create([
            'id_number' => $id,
            'firstname' => ucfirst(strtolower($result->First_Name)),
            'othername' => ucfirst(strtolower($result->Other_Name)),
            'surname' => ucfirst(strtolower($result->Surname)),
            'dob' => is_null($result->Date_of_Birth) ? null: Carbon::parse($result->Date_of_Birth),
            'citizenship' => is_null($result->Citizenship) ? null : $result->Citizenship,
            'occupation' => is_null($result->Occupation) ? null: $result->Occupation,
            'gender_id' => $gender ? $gender->id : null,
            'photo_filename' => $this->processIprsPhoto($result->Photo),
            'location' => $result->Place_of_Live
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function checkProvidedNumber($id)
    {
        $id = trim($id);

        if (is_numeric($id)) {
            return $this->requestIprsById($id);
        }

        return $this->requestIprsByPassport($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function requestIprsById($id)
    {
        return $this->client()->GetDataByIdCard([
            'log' => $this->username,
            'pass' => $this->password,
            'id_number' => $id
        ])->GetDataByIdCardResult;
    }

    /**
     * @param $passport
     * @return mixed
     */
    private function requestIprsByPassport($passport)
    {
        return $this->client()->GetDataByPassport([
            'log' => $this->username,
            'pass' => $this->password,
            'passport_number' => $passport
        ])->GetDataByPassportResult;
    }

    /**
     * @param $photoString
     * @return null
     * @throws \Exception
     */
    private function processIprsPhoto($photoString)
    {
        $filename = null;

        if (strlen($photoString)) {
            $filename = Uuid::generate()->string.'.jpg';

            $type = DocumentType::where('slug', 'iprs_files')->remember(1000)->first();

            $path = $type->path().'/'.$filename;

            \App::make(StorageInterface::class)->put($path, $photoString);
        }

        return $filename;
    }

    private function fakeIprs($id)
    {
        if (app()->environment('production')) {
            throw new \LogicException("Fake IPRS validation should not be used in production");
        }

        $faker = Faker::create();

        return IprsValidation::create([
            'id_number' => $id,
            'firstname' => $faker->firstName,
            'othername' => $faker->lastName,
            'surname' => '',
            'dob' => $faker->date(),
            'citizenship' => 'Kenyan',
            'occupation' => 'SELF EMPLOYED PERSON ',
            'gender_id' => Gender::first()->id,
            'photo_filename' => null
        ]);
    }

    private function client()
    {
        if ($this->client) {
            return $this->client;
        }

        $this->client = new \SoapClient($this->wsdl, ['exceptions' => true, 'trace' => true]);

        return $this->client;
    }

    public function testConnection()
    {
        $this->client();
    }
}
