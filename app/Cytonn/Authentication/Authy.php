<?php
/**
 * Date: 15/07/2016
 * Time: 6:51 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Authentication;

use App\Cytonn\Authentication\Authy\Client;
use App\Cytonn\Models\ClientUser;
use Carbon\Carbon;
use GuzzleHttp\Client as GuzzleClient;
use Rinvex\Authy\App;
use Rinvex\Authy\Token;
use Hash;

/**
 * Class Authy
 *
 * @package Cytonn\Authentication
 */
class Authy
{
    protected $key;

    /**
     * Authy constructor.
     *
     * @param $key
     */
    public function __construct()
    {
        $this->key = config('services.authy.key');
    }

    protected function http()
    {
        return new GuzzleClient();
    }

    /**
     * Get the authy API instance
     */
    protected function instance()
    {
        $httpClient = $this->http();

        return new App($httpClient, $this->key);
    }

    protected function user()
    {
        return new \Rinvex\Authy\User($this->http(), $this->key);
    }

    public function registerUser(ClientUser $user)
    {
        $authyUser = $this->user()->register($user->email, $user->phone, $user->phone_country_code);

        if ($authyUser->get('success')) {
            $body = $authyUser->get('user');

            $user->authy_id = $body['id'];
            $user->save();
        }
    }

    public function status($authyId)
    {
        $authy = $this->user();

        $status = $authy->status($authyId);

        return (object)[
            'success' => $status->get('success'),
            'body' => $status->get('status')
        ];
    }

    /**
     * @return Token
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    protected function token()
    {
        return new Token($this->http(), $this->key);
    }

    public function verifyToken(ClientUser $user, $token)
    {
        $authyToken = $this->token();

        $response = $authyToken->verify($token, $user->authy_id);

        return (object)[
            'success' => $response->succeed(),
            'body' => $response->get('body'),
            'errors' => $response->get('errors')
        ];
    }

    public function verifyOneTouch(ClientUser $user, $key)
    {
        $key = trim($key);

        if (strlen($key) <= 1) {
            return false;
        }

        $r = trim($user->authy_uuid) == $key;

        if ($r) {
            $user->update(['authy_uuid' => null]);
        }

        return $r;
    }

    /**
     * @param $authyId
     * @param null $message
     * @return object
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function sendOneTouch($authyId, $message = null)
    {
        $client = new Client($this->http(), $this->key);

        $response = $client->sendOneTouch($authyId, $message, 300);

        return (object)[
           'success' => $response->succeed(),
           'body' => $response->get('body'),
           'errors' => $response->errors()
        ];
    }

    public function requestSMS(ClientUser $user, $method = 'sms', $force = true)
    {
        $token = $this->token();

        $response = $token->send($user->authy_id, $method, $force);

        return (object)[
            'success' => $response->succeed(),
            'body' => $response->get('body'),
            'errors' => $response->errors()
        ];
    }
}
