<?php

namespace App\Cytonn\Authentication;

use App\Cytonn\Authentication\Authy\Client;
use App\Cytonn\Authentication\Client\AuthRepository;
use App\Cytonn\Clients\ApplicationTokenVerification;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\CommissionRecepient;
use App\Jobs\USSD\SendMessages;
use Carbon\Carbon;
use Cytonn\Authentication\Authy;
use Cytonn\Lib\HOTPass;
use GuzzleHttp\Client as GuzzleClient;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Rinvex\Authy\Token;

class TokenVerification
{
    protected $user;

    protected $key;

    protected $data;

    public function __construct(ClientUser $user = null, $input = null)
    {
        $this->user = $user;

        $this->key = config('services.authy.key');

        $this->data = $input;
    }

    private function http()
    {
        return new GuzzleClient();
    }

    /**
     * @param $option
     * @return object
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function send($option, $message = '')
    {
        switch ($option) {
            case 'authy':
                return $this->sendAuthyOneTouch($message);
                break;
            case 'email':
                return $this->sendEmail();
                break;
            case 'sms':
            default:
                return $this->sendSms();
                break;
        }
    }

    /**
     * @param $option
     * @return object
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    public function verify($option)
    {
        switch ($option) {
            case 'authy':
                return $this->verifyAuthy();
                break;
            case 'email':
                return $this->verifyEmail();
                break;
            case 'sms':
            default:
                return $this->verifySms();
                break;
        }
    }

    /**
     * @return object
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    protected function sendSms()
    {
        if ($this->checkCountryCode() && is_null($this->user)) {
            $token = $this->saveVerificationToken();

            $phone = $this->data['phone'];
            $fa = $this->data['faId'] ? CommissionRecepient::find($this->data['faId']) : null;
            $faPhone = '';

            if ($fa) {
                $faPhone = splitPhoneNumber($fa->phone);
                $faPhone = $faPhone ? '0' . $faPhone[1] : '';
            }

            $data = [
                'phone' => $this->data['country_code'] . $this->data['phone'],
                'token' => $token,
                'fa_phone' => $faPhone ? $faPhone : ''
            ];

            $phone = '0' . $phone;

            dispatch((new SendMessages($phone, 'send-app-token', $data))->onConnection('sync'));

            return (object)[
                'success' => true,
            ];
        }

        if ($this->checkCountryCode()) {
            $token = $this->saveToken($this->user);

            $phone = $this->user->phone_country_code . $this->user->phone;

            $data = ['user' => $this->user, 'token' => $token];

            dispatch((new SendMessages($phone, 'send-token', $data))->onConnection('sync'));

            return (object)[
                'success' => true,
            ];
        }

        return (new Authy())->requestSMS($this->user);
    }

    /**
     * @param Request $request
     * @return object
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    protected function verifySms()
    {
        if ($this->checkCountryCode() && is_null($this->user)) {
            return $this->verifyApplicationToken();
        }

        $token = \request()->get('token');

        return $this->checkCountryCode()
            ? (new AuthRepository($this->user))->validateOneTimeKey($token)
            : (new Authy())->verifyToken($this->user, $token);
    }

    private function verifyApplicationToken()
    {
        $phone = $this->data['phone'];

        $token = $this->data['token'];

        $ver = ApplicationTokenVerification::where('phone', $phone)
            ->where('token', $token);

        if ($ver->exists()) {
            $ver->update(['deleted_at' => new Carbon('now')]);

            return (object)[
                'success' => true,
            ];
        }

        return (object)[
            'success' => false,
        ];
    }

    /**
     * @param ClientUser $user
     * @return mixed
     */
    private static function saveToken(ClientUser $user)
    {
        $token = static::generateToken($user);

        $user->one_time_key = Hash::make($token);

        $user->one_time_key_created_at = new Carbon('now');

        $user->save();

        return $token;
    }

    private function saveVerificationToken()
    {
        $key = Hash::make($this->data['phone']);

        $token = (new HOTPass())->generate($key, 1);

        $appVer = new ApplicationTokenVerification();

        $appVer->phone_country_code = $this->data['country_code'];
        $appVer->phone = $this->data['phone'];
        $appVer->token = $token;

        $appVer->save();

        return $token;
    }

    private static function generateToken(ClientUser $user)
    {
        $key = Hash::make($user->username);

        return (new HOTPass())->generate($key, 1);
    }

    /**
     * @param null $message
     * @return object
     * @throws \Rinvex\Authy\InvalidConfiguration
     */
    protected function sendAuthyOneTouch($message = null)
    {
        $client = new Client($this->http(), $this->key);

        $response = $client->sendOneTouch($this->user->authy_id, $message, 300);

        return (object)[
            'success' => $response->succeed(),
            'body' => $response->get('body'),
            'errors' => $response->errors()
        ];
    }

    private function verifyAuthy()
    {
        $request = \request();

        $is_one_touch = isset($request['from_authy_one_touch']) && $request['from_authy_one_touch'];

        if (!$is_one_touch) {
            $response = (new Authy())->verifyToken($this->user, $request->get('token'));

            return $response;
        }

        return (new Authy())->verifyOneTouch($this->user, $request['one_touch_approval_uuid']);
    }

    protected function sendEmail()
    {
        return (new AuthRepository($this->user))->requestTokenViaMail();
    }

    protected function verifyEmail()
    {
        $token = \request()->get('token');

        return (new AuthRepository($this->user))->validateOneTimeKey($token);
    }

    /**
     * @return bool
     */
    protected function checkCountryCode(): bool
    {
        $code = $this->user ? $this->user->phone_country_code : $this->data['country_code'];

        return in_array($code, ['254', '+254']);
    }
}
