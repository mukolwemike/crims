<?php
/**
 * Date: 8/23/15
 * Time: 11:08 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Authentication\Events;

use App\Cytonn\Models\User;

/**
 * Raised when a user is logged in: i.e. when username and password are confirmed
 * Class UserHasLoggedIn
 *
 * @package Cytonn\Authentication\Events
 */
class UserHasLoggedIn
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var
     */
    public $otp;

    /**
     * UserHasLoggedIn constructor.
     *
     * @param $user
     */
    public function __construct(User $user, $otp)
    {
        $this->user = $user;

        $this->otp = $otp;
    }
}
