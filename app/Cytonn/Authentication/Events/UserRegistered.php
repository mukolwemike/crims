<?php
/**
 * Date: 8/21/15
 * Time: 9:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Authentication\Events;

class UserRegistered
{
    public $user;

    /**
     * UserRegistered constructor.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
}
