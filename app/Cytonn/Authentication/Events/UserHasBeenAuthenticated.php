<?php
/**
 * Date: 05/12/2015
 * Time: 11:59 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Authentication\Events;

use App\Cytonn\Models\User;

class UserHasBeenAuthenticated
{
    public $user;

    /**
     * UserHasBeenAuthenticated constructor.
     *
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
