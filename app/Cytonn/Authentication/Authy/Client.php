<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 25/06/2018
 * Time: 12:50
 */

namespace App\Cytonn\Authentication\Authy;

use GuzzleHttp\ClientInterface;
use Rinvex\Authy\InvalidConfiguration;
use Rinvex\Authy\Response;

class Client extends \Rinvex\Authy\Client
{
    protected $api_base = 'https://api.authy.com/';

    protected $format = 'json';

    public function __construct(ClientInterface $client, $key, $format = 'json')
    {
        $this->format = $format === 'xml' ? 'xml' : 'json';

        parent::__construct($client, $key, $format);
    }

    /**
     * Send One touch approval request
     * @param $authyId
     * @param null $message
     * @return Response
     */
    public function sendOneTouch($authyId, $message = null, $seconds = 86400)
    {
        $url = $this->api_base  ."onetouch/$this->format/users/$authyId/approval_requests";

        $params = $this->params + [
               "form_params" => [
                   "message" => $message ? $message :  "Approve login to CRIMS",
                   "seconds_to_expire" => $seconds
               ]
            ];

        return new Response($this->http->post($url, $params));
    }
}
