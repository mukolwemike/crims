<?php

namespace Cytonn\Authentication;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\UserMailer;

class AuthRepository
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function addLoginAttempt()
    {
        $this->user->last_login_attempt = Carbon::now();

        $this->save();
    }

    public function addLastLogin()
    {
        $this->user->last_login = new Carbon('now');

        $this->save();
    }

    public function save()
    {
        return $this->user->save();
    }

    public function resetLoginAttempts()
    {
        $this->user->login_attempt = 0;

        $this->save();
    }

    public function addInvalidLoginAttempt()
    {
        $login_attempts = $this->user->login_attempt;
        $login_attempts++;
        $this->user->login_attempt = $login_attempts;

        $this->save();
    }

    public function resendActivationLink()
    {
        $mailer = new UserMailer();
        $mailer->sendActivationEmail($this->user);
    }

    public function createActivationKey()
    {
        $activation_key = sha1(md5(time()));

        $this->user->activation_key = $activation_key;

        $this->user->activation_key_created_at = Carbon::now();

        $this->user->save();

        return $activation_key;
    }

    public function checkActivationKeyExpiry()
    {
        $time_sent = new Carbon($this->user->activation_key_created_at);

        $time_elapsed = $time_sent->copy()->diffInMinutes(Carbon::now());

        if ($time_elapsed > config('auth.activation.expire')) {
            return false;
        }
        return true;
    }

    public function isInSudoMode()
    {
        if (\Session::has('sudo_mode')) {
            if ((new \Carbon\Carbon(\Session::get('sudo_mode')))->isFuture()) {
                return true;
            }
        }

        return false;
    }
}
