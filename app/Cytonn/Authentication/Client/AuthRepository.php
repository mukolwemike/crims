<?php

namespace App\Cytonn\Authentication\Client;

use App\Cytonn\Authentication\MailAuth\Auth;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPasswordReminder;
use App\Cytonn\Models\ClientUser as User;
use App\Cytonn\Models\ClientUserAccess;
use Carbon\Carbon;
use Response;

class AuthRepository
{
    protected $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function checkUserAccountActivation($username, $token)
    {
        $user = User::where('username', $username)->first();

        $expiry = (new Carbon($user->activation_key_created_at))->addHours(24);

        if (!$user) {
            return ['success'=>false, 'reason'=>'link_invalid'];
        }
        if ($user->active) {
            return ['success'=>true, 'reason'=>'activated'];
        } elseif ($user->activation_key == $token && $expiry->isFuture()) {
            return ['success'=>true, 'reason'=>'valid'];
        } else {
            return ['success'=>false, 'reason'=>'link_invalid'];
        }
    }

    public function activateUserFirstTime($username, $password)
    {
        $user = User::where('username', $username)->first(); //set password

        $user->password = $password;
        $user->active = true;
        $user->save();

        return $user;
    }

    public function incrementLoginAttempts($user)
    {
        if (is_null($user)) {
            return false;
        }

        $user->login_attempts = $user->login_attempts + 1;
        $user->last_login_attempt = Carbon::now();

        return $user->save();
    }

    public function checkTooManyLoginAttempts($user)
    {
        if (is_null($user)) {
            return true;
        }

        if ($user->login_attempts >= 10 && (
            new Carbon($user->last_login_attempt))
                ->diffInMinutes(Carbon::now()) <= 30) {
            return true;
        }

        return false;
    }

    public function resetLoginAttempts($user)
    {
        $user->login_attempts = 0;
        $user->save();
    }

    public function needsTwoFactor($key)
    {
        $enabled = config('auth.two_factor_always_on');

        if ($enabled) {
            return true;
        }

        $user = $this->user;

        if ($user->hasNoClients()) {
            return $this->deviceIdentified($user, $key);
        }

        if (!$user->last_two_factor || !$user->last_two_factor_key) {
            return true;
        }

        return ! (Carbon::parse($user->last_two_factor)
                ->diffInDays(Carbon::now()) <= 7 && $user->last_two_factor_key == $key);
    }

    private function deviceIdentified($user, $key)
    {
        return !is_null($user->last_two_factor_key) && $user->last_two_factor_key == $key;
    }

    public function setSuccessfulTwoFactor(User $user)
    {
        $user->last_two_factor_key = $key = sha1(md5(time()));
        $user->last_two_factor =  Carbon::now();
        $user->save();

        return $key;
    }

    public function checkClientAccess(User $user, Client $client)
    {
        $access = ClientUserAccess::where('user_id', $user->id)
            ->where('client_id', $client->id)
            ->where('active', 1)
            ->first();

        return (bool) $access;
    }

    public function checkIsFaOrOwn(User $user, Client $client)
    {
        $own = $this->checkClientAccess($user, $client);

        $fas = $user->financialAdvisors->reduce(function ($carry, $fa) use ($client) {
                return $carry || $fa->repo->hasClient($client);
        },
            false);

        return $own || $fas;
    }

    public function checkResetLink($username, $token)
    {
        $user = User::where('username', $username)->first();

        try {
            $link = ClientPasswordReminder::where('user_id', $user->id)->latest()->first();

            return (
                new Carbon($link->created_at))
                    ->diffInHours(Carbon::now()) <= 24 && \Hash::check($token, $link->token);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function removeResetLink($username)
    {
        $user = User::where('username', $username)->first();

        $link = ClientPasswordReminder::where('user_id', $user->id)
            ->latest()
            ->first();

        $link->update(['token' => null]);
    }

    public function requestTokenViaMail()
    {
        (new Auth($this->user))->send();

        return (object)[
            'success' => 200,
        ];
    }

    public function validateOneTimeKey($token)
    {
        return (new Auth($this->user))->validateToken($token);
    }
}
