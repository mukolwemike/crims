<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 1/15/19
 * Time: 1:32 PM
 */

namespace App\Cytonn\Authentication\MailAuth;

use App\Cytonn\Mailers\Client\AuthTokenMailer;
use App\Cytonn\Models\ClientUser;
use Carbon\Carbon;
use Cytonn\Lib\HOTPass;
use Hash;

class Auth
{
    protected $user;

    public function __construct(ClientUser $user)
    {
        $this->user = $user;
    }

    public function send()
    {
        $token = $this->saveToken($this->user);

        if ($token) {
            (new AuthTokenMailer())->sendMail($this->user, $token);
        }

        return $token;
    }

    private static function saveToken(ClientUser $user)
    {
        $token = static::generateToken($user);

        $user->one_time_key = Hash::make($token);

        $user->one_time_key_created_at = new Carbon('now');

        $user->save();

        return $token;
    }

    private static function generateToken(ClientUser $user)
    {
        $key = Hash::make($user->username);

        return (new HOTPass())->generate($key, 1);
    }

    public function validateToken($token)
    {
        $created = Carbon::parse($this->user->one_time_key_created_at);

        if (Carbon::now()->diffInMinutes($created) > 12) {
            return (object)[
                'message' => 'Token already expired',
                'success' => false,
                'status' => 401
            ];
        }

        $allow = Hash::check($token, $this->user->one_time_key);

        if ($allow) {
            return (object)[
                'message' => 'Token matches',
                'success' => true,
                'status' => 200
            ];
        }

        return (object)[
            'message' => 'Token do not match token sent',
            'success' => false,
            'status' => 400
        ];
    }
}
