<?php
/**
 * Date: 07/10/2015
 * Time: 2:43 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Authentication\Forms;

use Laracasts\Validation\FormValidator;

class PasswordReminderForm extends FormValidator
{
    public $rules = [
        'email'=>'required|email'
    ];
}
