<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 4:01 PM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait DepositSecuritiesRules
{
    use Rules;

    public function depositHoldingCreate($request)
    {
        $rules = [
            'deposit_type_id' =>  'required|numeric',
            'invested_date' =>  'required|date',
            'maturity_date' =>  'required|date',
            'tax_rate_id' =>  'required|numeric',
            'amount' =>  'required|numeric',
            'nominal_value' => 'numeric',
            'interest_rate' => 'required|numeric',
            'coupon_payment_duration' => 'numeric',
            'unit_fund_id' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    public function depositHoldingUpdate($request)
    {
        $rules = [
        
        ];

        return $this->verdict($request, $rules);
    }
}
