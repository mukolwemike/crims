<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/27/18
 * Time: 9:43 AM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait PortfolioOrderAllocation
{
    use Rules;

    public function allocateForm($request)
    {
        $rules = [
            'portfolio_order_id' =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
