<?php
namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait DepositActionRules
{
    use Rules;

    public function depositActionsValidate($request)
    {
        $rules = [
            'reason' =>  'required|min:10',
        ];

        return $this->verdict($request, $rules);
    }
}
