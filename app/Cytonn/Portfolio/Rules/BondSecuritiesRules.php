<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 4:01 PM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait BondSecuritiesRules
{
    use Rules;

    public function bondCreate($request)
    {
        $rules = [
            'face_value' =>  'required|numeric',
            'tenor' =>  'required',
            'value_date' =>  'required|date',
            'maturity_date' =>  'required|date',
            'interest_payment_dates' =>  'required',
            'tax_rate' =>  'required_with:taxable',
        ];

        return $this->verdict($request, $rules);
    }

    public function bondUpdate($request)
    {
        $rules = [
        
        ];

        return $this->verdict($request, $rules);
    }
}
