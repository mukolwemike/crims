<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 4:01 PM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait PortfolioSecurityRules
{
    use Rules;

    public function portfolioSecurityCreate($request)
    {
        $rules = [
            'name' =>  'required|string|min:3',
            'sub_asset_class_id' =>  'required',
            'fund_manager_id' =>  'required',
        ];

        return $this->verdict($request, $rules);
    }

    public function portfolioSecurityEdit($request)
    {
        $rules = [
            'name' =>  'required|string|min:3',
            'sub_asset_class_id' =>  'required',
            'fund_manager_id' =>  'required',
        ];

        return $this->verdict($request, $rules);
    }

    public function portfolioSecurityUpdate($request)
    {
        $rules = [
            'name' =>  'required|string|min:3',
            'sub_asset_class_id' =>  'required',
            'portfolio_investor_id' =>  'required',
            'fund_manager_id' =>  'required',
        ];

        return $this->verdict($request, $rules);
    }

    public function portfolioAllocation($request)
    {
        $rules = [
            'fund_id' => 'required|numeric',
            'sub_asset_class_id' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
