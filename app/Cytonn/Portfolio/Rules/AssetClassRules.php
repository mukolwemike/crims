<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 4:01 PM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait AssetClassRules
{
    /**
     * Rule trait
     */
    use Rules;

    /**
     * @param $request
     * @return mixed
     */
    public function assetClassCreate($request)
    {
        $rules = [
            'name' =>  'required|string|min:3|unique:asset_classes',
        ];

        return $this->verdict($request, $rules);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function assetClassUpdate($request)
    {
        $rules = [
            'name' =>  'required|string|min:3',
        ];

        return $this->verdict($request, $rules);
    }
}
