<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 4:01 PM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait WithholdingWithdrawal
{
    use Rules;

    public function createWithdrawal($request)
    {
        $rules = [
//            'amount'=>'required| numeric',
            'date' => 'required | date',
            'narrative' => 'required'
        ];

        return $this->verdict($request, $rules);
    }
}
