<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 4:01 PM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait SubAssetClassRules
{
    /**
     * Rule trait
     */
    use Rules;

    /**
     * @param $request
     * @return mixed
     */
    public function subAssetClassCreate($request)
    {
        $rules = [
            'name' =>  'required|string|min:3|unique:asset_classes',
            'asset_class_id'=> 'required'
        ];

        return $this->check($request, $rules);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function subAssetClassUpdate($request)
    {
        $rules = [
            'name' =>  'required|string|min:3',
            'id'=> 'required'
        ];

        return $this->verdict($request, $rules);
    }
}
