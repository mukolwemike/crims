<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 4:01 PM
 */

namespace Cytonn\Portfolio\Rules;

use Cytonn\Rules\Rules;

trait EquitySecuritiesRules
{
    use Rules;

    public function equityCreate($request)
    {
        $rules = [
            'investment_date' => 'required|date',
            'number' => 'required|numeric',
            'cost' => 'required|numeric',
            'market_price' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
    
    public function equityShareBuy($request)
    {
        $rules = [
            'date' => 'required|date',
            'number' => 'required|numeric',
            'cost' => 'required|numeric',
            'settlement_date' => 'required|date',
        ];
        
        return $this->verdict($request, $rules);
    }
    
    public function equityShareSell($request)
    {
        $rules = [
            'date' => 'required|date',
            'number' => 'required|numeric',
            'sale_price' => 'required|numeric',
            'settlement_date' => 'required|date',
        ];
        
        return $this->verdict($request, $rules);
    }
    
    public function equityShareDividends($request)
    {
        $rules = [
            'date' => 'required|date',
            'book_closure_date' => 'required|date',
            'dividend_type' => 'required',
            'dividend' => 'required_if:dividend_type, "cash"|required_if:method_of_calculation, "conversion"',
            'conversion_price' => 'required_if:method_of_calculation, "conversion"',
            'old_shares' => 'required_if:method_of_calculation, "ratio"',
            'new_shares' => 'required_if:method_of_calculation, "ratio"',
        ];
        
        return $this->verdict($request, $rules);
    }
    
    public function priceTrail($request)
    {
        $rules = [
            'date' => 'required|date',
            'price' => 'required|numeric',
        ];
        
        return $this->verdict($request, $rules);
    }

    public function equityUpdate($request)
    {
        $rules = [
        
        ];

        return $this->verdict($request, $rules);
    }
}
