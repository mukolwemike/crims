<?php
/**
 * Date: 19/10/2015
 * Time: 4:39 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class PortfolioInvestorRepository
 *
 * @package Cytonn\Portfolio
 */
class PortfolioInvestorRepository
{
    /**
     * @var PortfolioInvestor
     */
    public $investor;

    /**
     * InvestorRepository constructor.
     *
     * @param $investor
     */
    public function __construct(PortfolioInvestor $investor = null)
    {
        is_null($investor) ? $this->investor = new PortfolioInvestor() : $this->investor = $investor;
    }


    /**
     * @param Currency $currency
     * @param FundManager $fundManager
     * @param null $date
     * @return Builder
     */
    protected function investmentsQuery(
        Currency $currency,
        FundManager $fundManager,
        $date = null,
        $subAssetClass = null
    ) {
        $query = DepositHolding::whereHas(
            'security',
            function ($security) {
                $security->where('portfolio_investor_id', $this->investor->id);
            }
        );

        if ($subAssetClass) {
            $query->whereHas(
                'security',
                function ($security) use ($subAssetClass) {
                    $security->where('sub_asset_class_id', $subAssetClass);
                }
            );
        }

        return $query->currency($currency)->fundManager($fundManager)->activeOnDate(Carbon::parse($date));
    }

    /**
     * Get the total amount invested into investor
     *
     * @param  Currency $currency
     * @param  FundManager $fundManager
     * @param  null $date
     * @param  null $subAssetClass
     * @return number
     */
    public function totalDepositsPrincipal(
        Currency $currency,
        FundManager $fundManager,
        $date = null,
        $subAssetClass = null
    ) {
        $date = Carbon::parse($date);

        $investments = $this->investmentsQuery($currency, $fundManager, $date, $subAssetClass)->get();

        return $investments->sum(
            function ($investment) use ($date) {
                return $investment->repo->principal($date);
            }
        );
    }

    /**
     * Get the total value of the investments
     *
     * @param  Currency $currency
     * @param  FundManager $fundManager
     * @param  null $date
     * @param  bool $as_at_next_day
     * @param  null $subAssetClass
     * @return number
     */
    public function totalDepositValue(
        Currency $currency,
        FundManager $fundManager,
        $date = null,
        $as_at_next_day = false,
        $subAssetClass = null
    ) {
        $date = Carbon::parse($date);

        $investments = $this->investmentsQuery($currency, $fundManager, $date, $subAssetClass)->get();

        return $investments->sum(
            function ($investment) use ($date, $as_at_next_day) {
                return $investment->repo->getTotalValueOfAnInvestmentAsAtDate($date, $as_at_next_day);
            }
        );
    }

    /*
     * Get the total value of all the portfolio investments linked to a portfolio investor
     */
    public function totalDepositValueForAllProducts($date = null, $as_at_next_day = false)
    {
        $date = Carbon::parse($date);

        $investments = $query = DepositHolding::whereHas(
            'security',
            function ($security) {
                $security->where('portfolio_investor_id', $this->investor->id);
            }
        )->activeOnDate(Carbon::parse($date))->get();

        return $investments->sum(
            function ($investment) use ($date, $as_at_next_day) {
                return $investment->repo->getKesConvertedValue(
                    $investment->repo->getTotalValueOfAnInvestmentAsAtDate($date, $as_at_next_day)
                );
            }
        );
    }

    /**
     * @param Currency $currency
     * @param FundManager $fundManager
     * @param null $date
     * @return number
     */
    public function totalGrossInterest(
        Currency $currency,
        FundManager $fundManager,
        $date = null,
        $subAssetClass = null
    ) {
        $investments = $this->investmentsQuery($currency, $fundManager, $date, $subAssetClass)->get();

        return $investments->sum(
            function (DepositHolding $investment) use ($date) {
                return $investment->repo->getGrossInterestForInvestment($date);
            }
        );
    }

    /**
     * @param Currency $currency
     * @param FundManager $fundManager
     * @param $date
     * @return number
     */
    public function totalNetInterest(Currency $currency, FundManager $fundManager, $date = null, $subAssetClass = null)
    {
        $investments = $this->investmentsQuery($currency, $fundManager, $date, $subAssetClass)->get();

        return $investments->sum(
            function (DepositHolding $investment) use ($date) {
                return $investment->repo->getNetInterestForInvestment($date);
            }
        );
    }

    public function totalRepayments(
        Currency $currency,
        FundManager $fundManager,
        $date = null,
        $type = null,
        $subAssetClass = null
    ) {
        $date = Carbon::parse($date);

        $investments = $this->investmentsQuery($currency, $fundManager, $date, $subAssetClass)->get();

        return $investments->sum(
            function (DepositHolding $investment) use ($type, $date) {
                return $investment->repayments()->ofType($type)->before($date)->sum('amount');
            }
        );
    }

    /**
     * @param Currency $currency
     * @param FundManager $fundManager
     * @param $date
     * @return number
     */
    public function totalWithholdingTax(
        Currency $currency,
        FundManager $fundManager,
        $date = null,
        $subAssetClass = null
    ) {
        $investments = $this->investmentsQuery($currency, $fundManager, $date, $subAssetClass)->get();

        return $investments->sum(
            function (DepositHolding $investment) use ($date) {
                return $investment->repo->getWithholdingTaxForInvestment($date);
            }
        );
    }

    /*
     * Get the currencies that a given portfolio investor has investments in
     */
    public function getCurrencies($fundManager = null)
    {
        return Currency::all()->filter(
            function ($currency) use ($fundManager) {
                return DepositHolding::currency($currency, true)
                    ->fundManager($fundManager)
                    ->institution($this->investor)->exists();
            }
        );
    }

    /*c
     * Get the fund managers that a given portfolio investor has investments in
     */
    public function getFundManagers()
    {
        return FundManager::whereHas(
            'securities',
            function ($q) {
                $q->where('portfolio_investor_id', $this->investor->id);
            }
        )->get();
    }

    /*
     * Get the latest product for a given protfolio investor
     */
    public function getProductName()
    {
        $security = $this->investor->securities()->latest()->first();

        if ($security) {
            return $security->fundManager->name;
        }

        return '';
    }
}
