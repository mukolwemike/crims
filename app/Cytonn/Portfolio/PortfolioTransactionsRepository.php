<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/28/16
 * Time: 9:07 AM
 */

namespace Cytonn\Portfolio;

use Laracasts\Commander\CommanderTrait;

class PortfolioTransactionsRepository
{
    use CommanderTrait;
}
