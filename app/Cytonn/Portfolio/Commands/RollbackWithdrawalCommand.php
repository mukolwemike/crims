<?php

namespace Cytonn\Portfolio\Commands;

class RollbackWithdrawalCommand
{
    public $approval;

    /**
     * RollbackWithdrawalCommand constructor.
     *
     * @param $approval
     */
    public function __construct($approval)
    {
        $this->approval = $approval;
    }
}
