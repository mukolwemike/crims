<?php

namespace Cytonn\Portfolio\Commands;

class ReverseInvestmentCommand
{
    public $approval;

    /**
     * ReverseInvestmentCommand constructor.
     *
     * @param $approval
     */
    public function __construct($approval)
    {
        $this->approval = $approval;
    }
}
