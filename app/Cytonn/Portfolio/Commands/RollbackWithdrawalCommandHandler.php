<?php

namespace Cytonn\Portfolio\Commands;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use \App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestor;
use Cytonn\Portfolio\Portfolio;
use Laracasts\Commander\CommandHandler;
use DB;

/**
 * Class RollbackWithdrawalCommandHandler
 *
 * @package Cytonn\Portfolio\Commands
 */
class RollbackWithdrawalCommandHandler implements CommandHandler
{

    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        DB::transaction(
            function () use ($command) {
                $this->rollback($command->approval);
            }
        );
    }

    /**
     * Perform the rollback
     *
     * @param $approval
     */
    private function rollback($approval)
    {
        $investment = \App\Cytonn\Models\Portfolio\DepositHolding::findOrFail($approval->payload['investment_id']);

        $this->reverseCustodial($investment);

        //reverse the investment withdrawal now
        $investment->withdrawn = null;
        $investment->custodial_withdraw_transaction_id = null;
        $investment->withdraw_transaction_approval_id = null;
        $investment->withdraw_amount = null;
        $investment->withdrawal_date = null;
        $investment->withdrawn_by  = null;
        $investment->rolled = null;
        $investment->save();
    }

    /**
     * Reverse the custodial transaction
     *
     * @param \App\Cytonn\Models\Portfolio\DepositHolding $investment
     */
    private function reverseCustodial(\App\Cytonn\Models\Portfolio\DepositHolding $investment)
    {
        $transaction = CustodialTransaction::withTrashed()->find($investment->custodial_withdraw_transaction_id);

        if (!is_null($transaction)) {
            $transaction->delete();

            return true;
        }

        $investor = PortfolioInvestor::find($investment->portfolio_investor_id);

        is_null($investor) ? $investor_name = '' : $investor_name = $investor->name;

        //update custodial
        $custodial = new CustodialTransaction();
        $custodial->custodial_account_id = $investment->custodial_account_id;

        $custodial->amount = - abs($investment->withdraw_amount);
        $custodial->transaction_owners_id = (new Portfolio())->getPortfolioTransactionOwner($investment)->id;
        $custodial->description = 'Portfolio investment withdrawal was reversed: ' . $investor_name;
        $custodial->type = CustodialTransactionType::where('name', 'I')->first()->id;
        $custodial->date = $investment->withdrawal_date;
        $custodial->save();

        return $custodial;
    }
}
