<?php

namespace Cytonn\Portfolio\Commands;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentType;
use Cytonn\Portfolio\Events\PortFolioInvestmentHasBeenEdited;
use Cytonn\Presenters\CustodialPresenter;
use DB;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

/**
 * Class EditInvestmentCommandHandler
 *
 * @package Cytonn\Portfolio\Commands
 */
class EditInvestmentCommandHandler implements CommandHandler
{
    use DispatchableTrait;

    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        $approval = $command->approval;

        DB::transaction(
            function () use ($approval) {
                $data = $approval->payload;

                $investment = DepositHolding::findOrFail($data['investment_id']);

                $oldInvestment = $investment;

                if (isset($data['amount'])) {
                    $difference = $investment->amount - $data['amount'];

                    if (($difference != 0) || ($investment->custodial_account_id != $data['custodial_account_id'])) {
                        $this->editCustodial($investment, $data, $approval);
                    }
                }

                $newInvestment = $this->editInvestment($investment, $data, $approval);

                $investment->raise(new PortFolioInvestmentHasBeenEdited($newInvestment, $oldInvestment));

                $this->dispatchEventsFor($investment);
            }
        );
    }

    /**
     * Edit the investment
     *
     * @param  DepositHolding $investment
     * @param  $data
     * @return DepositHolding
     */
    private function editInvestment(DepositHolding $investment, $data, $approval)
    {
        $description = isset($data['description']) ? $data['description'] : null;

        $contact_person = isset($data['contact_person']) ? $data['contact_person'] : null;

        $portfolio_investment_type = isset($data['type_id']) ? PortfolioInvestmentType::find($data['type_id']) : null;

//        $investment->amount = $data['amount'];

//        $investment->invested_date = $data['invested_date'];

        $investment->maturity_date = $data['maturity_date'];

        $investment->interest_rate = $data['interest_rate'];

        $investment->on_call = $data['on_call'];

        $investment->tax_rate_id = $data['tax_rate_id'];

//        $investment->taxable = $data['taxable'];

        $investment->description = $description;

        $investment->contact_person = $contact_person;

//        $investment->type_id = is_null($portfolio_investment_type) ? null : $portfolio_investment_type->id;

        $investment->save();

        return $investment;
    }

    /**
     * Make any adjustments to affected custodial accounts
     *
     * @param $investment
     * @param $data
     * @return CustodialTransaction|mixed
     */
    private function editCustodial(DepositHolding $investment, $data, $approval)
    {
        $transaction = $investment->investTransaction;

        if ($transaction) {
            $transaction->amount = -abs($data['amount']);

            $transaction->custodial_account_id = $data['custodial_account_id'];

            $transaction->save();

            return $transaction;
        } else {
            $createNew = $this->createCustodialTransaction($investment, $data, $approval);

            $createNew->amount = -abs($data['amount']);

            $createNew->description = 'A portfolio investment for ' .
                $investment->security->name . ' was edited and affected custodial account ' .
                CustodialPresenter::presentName($data['custodial_account_id']);

            $createNew->type = CustodialTransactionType::where('name', 'I')->first()->id;

            $createNew->date = $investment->invested_date;

            $createNew->custodial_account_id = $data['custodial_account_id'];

            $createNew->save();

            //Link transaction to investment
            $investment->custodial_invest_transaction_id = $createNew->id;

            $investment->save();

            return $createNew;
        }

        //         Wont be applied since without a transaction we cant get a custodial account
        //        //create transaction if not found
        //        $custodial = $this->createCustodialTransaction($investment, $data, $approval);
        //
        //        //if the custodial account did not change
        //        if ($investment->repo->getCustodialAccountId() == $data['custodial_account_id'])
        //        {
        //            $difference = $investment->amount - $data['amount'];
        //
        //            $custodial->amount = $difference;
        //
        //            $custodial->date = $investment->invested_date;
        //
        //            $custodial->description = 'A portfolio investment for '. $investment->security->name .'
        //  was edited, the amount was affected';
        //
        //            if ($difference >= 0)
        //            {
        //                $custodial->type = CustodialTransactionType::where('name', 'R')->first()->id;
        //            }
        //            else
        //            {
        //                $custodial->type = CustodialTransactionType::where('name', 'I')->first()->id;
        //            }
        //
        //            $custodial->save();
        //        }
        //        else
        //        {
        //            $reversingOld = $this->createCustodialTransaction($investment, $data, $approval);
        //
        //            $reversingOld->amount = $investment->amount;
        //
        //            $reversingOld->description = 'A portfolio investment for '.$investment->institution->name.'
        // was edited and affected custodial accounts '.
        //CustodialPresenter::presentName($investment->custodial_account_id).' and '.
        //CustodialPresenter::presentName($data['custodial_account_id']);;
        //
        //            $reversingOld->type =  CustodialTransactionType::where('name', 'R')->first()->id;
        //
        //            $reversingOld->date = $investment->invested_date;
        //
        //            $reversingOld->save();
        //
        //            $createNew = $this->createCustodialTransaction($investment, $data, $approval);
        //
        //            $createNew->amount = -abs($data['amount']);
        //
        //            $createNew->description = 'A portfolio investment for '.$investment->institution->name.'
        // was edited and affected custodial accounts '.
        //CustodialPresenter::presentName($investment->custodial_account_id).' and '.
        //CustodialPresenter::presentName($data['custodial_account_id']);
        //
        //            $createNew->type = CustodialTransactionType::where('name', 'I')->first()->id;
        //
        //            $createNew->date = $investment->invested_date;
        //
        //            $createNew->custodial_account_id = $data['custodial_account_id'];
        //
        //            $createNew->save();
        //        }
    }

    /**
     * Create and return a custodial transaction object with the defaults set
     *
     * @param  $investment
     * @return CustodialTransaction
     */
    private function createCustodialTransaction($deposit, $data, $approval)
    {
        $custodial = new CustodialTransaction();

        $custodial->approval_id = $approval->id;

        $custodial->portfolio_institution_id = $data['portfolio_institution_id'];

        return $custodial;
    }
}
