<?php

namespace Cytonn\Portfolio\Commands;

use App\Cytonn\Models\PortfolioTransactionApproval;

/**
 * Class CustodialTransferCommand
 *
 * @package Cytonn\Portfolio\Commands
 */
class CustodialTransferCommand
{
    /**
     * @var PortfolioTransactionApproval
     */
    public $approval;


    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function __construct(PortfolioTransactionApproval $approval)
    {
        $this->approval = $approval;
    }
}
