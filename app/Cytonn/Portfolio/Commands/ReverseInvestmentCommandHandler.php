<?php

namespace Cytonn\Portfolio\Commands;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestor;
use Cytonn\Portfolio\Portfolio;
use DB;
use Laracasts\Commander\CommandHandler;

class ReverseInvestmentCommandHandler implements CommandHandler
{

    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        $approval = $command->approval;

        DB::transaction(
            function () use ($approval) {
                $investment =
                    \App\Cytonn\Models\Portfolio\DepositHolding::findOrFail($approval->payload['investment_id']);

                $this->performReversal($investment);
            }
        );
    }

    private function performReversal(DepositHolding $investment)
    {
        $this->reverseCustodial($investment);

        $investment->delete();
    }

    private function reverseCustodial(DepositHolding $investment)
    {
        $investor = PortfolioInvestor::find($investment->portfolio_investor_id);

        is_null($investor) ? $investor_name = '' : $investor_name = $investor->name;

        $transaction = CustodialTransaction::withTrashed()->find($investment->custodial_invest_transaction_id);

        if (!is_null($transaction)) {
            $transaction->delete();
            return true;
        }

        //update custodial if trans was not found
        $custodial = new CustodialTransaction();
        $custodial->custodial_account_id = $investment->custodial_account_id;

        $custodial->amount = $investment->amount;
        $custodial->transaction_owners_id = (new Portfolio())->getPortfolioTransactionOwner($investment)->id;
        $custodial->description = 'Portfolio investment was reversed: ' . $investor_name;
        $custodial->type = CustodialTransactionType::where('name', 'R')->first()->id;
        $custodial->date = $investment->invested_date;

        return $custodial->save();
    }
}
