<?php

namespace Cytonn\Portfolio\Commands;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\CustodialTransfer;
use Cytonn\Portfolio\Events\TransferHasBeenMade;
use Cytonn\Presenters\AmountPresenter;
use DB;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CustodialTransferCommandHandler implements CommandHandler
{
    use DispatchableTrait, EventGenerator;

    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        DB::transaction(
            function () use ($command) {
                $data = $command->approval->payload;

                $data['approval_id'] = $command->approval->id;

                $sender = CustodialAccount::findOrFail($data['source_account']);
                $recipient = CustodialAccount::findOrFail($data['destination_id']);

                $this->performTransfer($sender, $recipient, $data);
            }
        );
    }

    public function performTransfer(CustodialAccount $sender, CustodialAccount $recipient, array $data)
    {
        $amount = isset($data['credit_amount']) ? abs($data['credit_amount']) :
            abs($data['amount'] * (float)$data['effective_rate']);
        //if the custodial accounts are different
        $sending = new CustodialTransaction();
        $sending->custodial_account_id = $sender->id;
        $sending->transaction_owners_id = null;
        $sending->amount = -abs($data['amount']);
        $sending->date = $data['transfer_date'];
        $sending->description = 'Transfer to ' . $recipient->account_name . ' : ' . $data['narrative'];
        $sending->type = CustodialTransactionType::where('name', 'TO')->first()->id;
        $sending->approval_id = $data['approval_id'];

        $sending->save();

        $receiving = new CustodialTransaction();
        $receiving->custodial_account_id = $recipient->id;
        $receiving->transaction_owners_id = null;
        $receiving->amount = $amount;
        $receiving->description = 'Transfer from ' . $sender->account_name . ' : ' . $data['narrative'];
        $receiving->type = CustodialTransactionType::where('name', 'TI')->first()->id;
        $receiving->date = $data['transfer_date'];
        $receiving->approval_id = $data['approval_id'];

        $receiving->save();

        // Record Custodial Transfer
        $transfer = new CustodialTransfer();
        $transfer->description = 'Transfer ' . AmountPresenter::currency($amount) . ' from ' . $sender->account_name .
            ' to ' . $recipient->account_name;
        $transfer->out_transaction_id = $sending->id;
        $transfer->in_transaction_id = $receiving->id;
        $transfer->exchange = $amount;
        $transfer->save();

        // update transfer status
        if (isset($data['sender_ids'])) {
            $custodialTransactionIds = $data['sender_ids'];

            foreach ($custodialTransactionIds as $id) {
                $trans = CustodialTransaction::find($id);
                $trans->transferred_transaction_id = $sending->id;
                $trans->save();
            }
        }

        unset($data['sender_ids']);

        // Generate instructions
        $this->raise(new TransferHasBeenMade($sending, $receiving));
        $this->dispatchEventsFor($this);
    }
}
