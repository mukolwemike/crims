<?php

namespace Cytonn\Portfolio\Commands;

use App\Cytonn\Models\PortfolioTransactionApproval;

class EditInvestmentCommand
{
    public $approval;


    public function __construct(PortfolioTransactionApproval $approval)
    {
        $this->approval = $approval;
    }
}
