<?php
/**
 * Date: 05/04/2016
 * Time: 10:46 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class BuyActiveStrategySharesForm extends FormValidator
{
    public $rules = [
        'number'=>'required|numeric',
        'date'=>'required|date',
        'cost'=>'required|numeric'
    ];
}
