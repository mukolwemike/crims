<?php
/**
 * Date: 18/04/2016
 * Time: 4:57 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class CombinedRolloverForm extends FormValidator
{
    public $rules = [
        'invested_date'=>'required|date|after:old_maturity_date',
        'maturity_date'=>'required|date|after:new_invested_date',
        'amount'=>'required|numeric',
        'interest_rate'=>'required|numeric'
    ];
}
