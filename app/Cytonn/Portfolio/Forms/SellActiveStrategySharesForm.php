<?php
/**
 * Date: 06/04/2016
 * Time: 9:31 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class SellActiveStrategySharesForm extends FormValidator
{
    public $rules = [
        'date'=>'required|date',
        'number'=>'required|numeric',
        'price'=>'required|numeric'
    ];
}
