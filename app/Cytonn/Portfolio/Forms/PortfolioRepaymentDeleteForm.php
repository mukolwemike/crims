<?php
/**
 * Date: 13/01/2017
 * Time: 17:43
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class PortfolioRepaymentDeleteForm extends FormValidator
{
    public $rules = [
        'reason'=>'required|min:10'
    ];
}
