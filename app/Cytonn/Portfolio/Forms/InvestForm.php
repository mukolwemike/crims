<?php
/**
 * Date: 02/10/2015
 * Time: 4:46 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class InvestForm extends FormValidator
{
    public $rules = [
        'portfolio_investor_id'=>'required',
        'fund_type_id'=>'required',
        'custodial_account_id'=>'required',
        'invested_date'=>'required|date',
        'maturity_date'=>'required|date|after:invested_date',
        'interest_rate'=>'required',
        'amount'=>'required',
    //        'contact_person'=>'required'
    ];
}
