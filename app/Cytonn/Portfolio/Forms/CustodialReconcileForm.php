<?php

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class CustodialReconcileForm extends FormValidator
{
    public $rules = [
        'amount'=>'required| numeric',
        'date' => 'required | date',
        'type' => 'required',
        'narrative' => 'required'
    ];
}
