<?php
/**
 * Date: 17/03/2016
 * Time: 10:05 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class CustodialWithdrawForm extends FormValidator
{
    public $rules = [
        'amount'=>'required| numeric',
        'date' => 'required | date',
        'transaction' => 'required',
        'narrative' => 'required'
    ];
}
