<?php
/**
 * Date: 21/03/2016
 * Time: 8:00 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class RollbackWithdrawalForm extends FormValidator
{
    public $rules = [
        'reason'=>'required|min:10'
    ];
}
