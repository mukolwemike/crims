<?php
/**
 * Date: 16/03/2016
 * Time: 4:24 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class EditInvestmentform extends FormValidator
{
    public $rules = [
//        'custodial_account_id'=>'required',
//        'invested_date'=>'required|date',
        'maturity_date'=>'required|date|after:invested_date',
        'interest_rate'=>'required|numeric',
        'contact_person'=>'required',
//        'amount'=>'required|numeric'
    ];
}
