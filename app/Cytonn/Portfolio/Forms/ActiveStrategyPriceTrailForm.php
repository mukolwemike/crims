<?php
/**
 * Date: 05/04/2016
 * Time: 1:58 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class ActiveStrategyPriceTrailForm extends FormValidator
{
    public $rules = [
        'date'=>'required|date',
        'price'=>'required|numeric'
    ];
}
