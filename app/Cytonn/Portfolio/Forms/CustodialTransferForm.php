<?php
/**
 * Date: 17/03/2016
 * Time: 12:51 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class CustodialTransferForm extends FormValidator
{
    public $rules = [
        'amount'=>'required | numeric',
        'transfer_date' => 'required | date',
        'effective_rate' => 'required | numeric',
        'narrative'=>'required',
        'destination_id'=>'required'
    ];

    public $messages = [
        'destination_id.required'=>'Please select the destination account',
        'effective_rate.required'=>'Please enter a valid exchange rate',
        'effective_rate.numeric'=>'Please enter a valid exchange rate'
    ];
}
