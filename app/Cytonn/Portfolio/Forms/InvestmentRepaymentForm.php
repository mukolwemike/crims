<?php
/**
 * Date: 22/06/2016
 * Time: 5:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class InvestmentRepaymentForm extends FormValidator
{
    public $rules = [
        'amount' => 'required_if:payment_amount_type_id,0|numeric',
        'date' => 'required|date|same_or_after:invested_date',
        'narrative' => 'required|min:10',
        'payment_type' => 'required'
    ];
}
