<?php
/**
 * Date: 05/04/2016
 * Time: 3:26 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class ActiveStrategyTargetPriceForm extends FormValidator
{
    public $rules = [
        'price'=>'required'
    ];
}
