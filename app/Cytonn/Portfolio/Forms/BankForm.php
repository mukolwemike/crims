<?php
    /**
     * Date: 17/11/2015
     * Time: 12:14 PM
     *
     * @author Edwin Mukiri <emukiri@cytonn.com>
     * Project: crm
     * Cytonn Technology
     */

    namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class BankForm extends FormValidator
{
    public $rules = [
    'name'=>'required|unique:banks'
    ];
}
