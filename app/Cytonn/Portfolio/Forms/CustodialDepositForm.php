<?php

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class CustodialDepositForm extends FormValidator
{
    public $rules = [
        'amount'=>'required| numeric',
        'date' => 'required | date',
        'narrative' => 'required'
    ];
}
