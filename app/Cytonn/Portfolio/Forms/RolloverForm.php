<?php
/**
 * Date: 02/10/2015
 * Time: 10:19 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class RolloverForm extends FormValidator
{
    public $rules = [
        'invested_date'=>'required|date|same_or_after:old_investment_maturity_date',
        'maturity_date'=>'required|date|after:invested_date',
        'amount'=>'required|numeric',
        'interest_rate'=>'required|numeric'
    ];
}
