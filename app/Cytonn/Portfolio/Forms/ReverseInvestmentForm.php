<?php
/**
 * Date: 21/03/2016
 * Time: 7:36 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class ReverseInvestmentForm extends FormValidator
{
    public $rules = [
        'reason'=>'required|min:10'
    ];
}
