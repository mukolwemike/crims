<?php
/**
 * Date: 02/10/2015
 * Time: 9:20 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class WithdrawForm extends FormValidator
{
    public $rules = [
    ];

    public function premature()
    {
        $this->rules = array_add($this->rules, 'premature', 'required');
        $this->rules = array_add(
            $this->rules,
            'end_date',
            'required|date|before:maturity_date|same_or_after:invested_date'
        );
    }

    public function partial()
    {
        $this->rules = array_add($this->rules, 'amount', 'required | numeric');
    }
}
