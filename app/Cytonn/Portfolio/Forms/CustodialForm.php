<?php
/**
 * Date: 02/10/2015
 * Time: 5:40 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class CustodialForm extends FormValidator
{
    public $rules = [
        'account_name'=>'required',
        'bank_id'=>'required',
        'account_no'=>'required',
        'currency_id'=>'required'
    ];
}
