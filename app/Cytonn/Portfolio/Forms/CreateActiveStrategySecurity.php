<?php
/**
 * Date: 04/04/2016
 * Time: 8:13 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class CreateActiveStrategySecurity extends FormValidator
{
    public $rules = [
        'name'=>'required|min:3'
    ];
}
