<?php
/**
 * Date: 02/10/2015
 * Time: 5:50 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio\Forms;

use Laracasts\Validation\FormValidator;

class InstitutionForm extends FormValidator
{
    public $rules = [
      'name'=>'required'
    ];
}
