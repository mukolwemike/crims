<?php

namespace Cytonn\Portfolio\Handlers;

use App\Cytonn\Models\PortfolioInstruction;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Handlers\EventListener;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Approvals\Engine\Approval;
use Cytonn\Portfolio\Events\TransferHasBeenMade;
use Cytonn\Portfolio\Events\ApprovalIsReady;
use ReflectionException;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 8:47 AM
 */
class EventHandler extends EventListener
{
    /**
     * @param ApprovalIsReady $event
     */
    public function whenApprovalIsReady(ApprovalIsReady $event)
    {
        $handler = new Approval($event->approval);

        \DB::transaction(
            function () use ($handler, $event) {
                $handler->approve();
            }
        );
    }

    /**
     * @param TransferHasBeenMade $event
     */
    public function whenTransferHasBeenMade(TransferHasBeenMade $event)
    {
        // Instructions for sending
        $instruction = new PortfolioInstruction();
        $instruction->sender_id = $event->sending->custodial_account_id;
        $instruction->receiver_id = $event->receiving->custodial_account_id;
        $instruction->amount = abs($event->sending->amount);
        $instruction->description = $event->sending->description;
        $instruction->date = $event->sending->date;
        $instruction->approval_id = $event->sending->approval_id;
        $instruction->save();
    }
}
