<?php

namespace Cytonn\Portfolio\Statement;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use Carbon\Carbon;

class Processor
{
    protected $currency;

    protected $fundManager;

    protected $institution;

    protected $statementDate;

    public function __construct(Currency $currency, FundManager $fundManager, PortfolioSecurity $institution)
    {
        $this->currency = $currency;
        $this->fundManager = $fundManager;
        $this->institution = $institution;
    }

    public function process(Carbon $date, Carbon $from = null)
    {
        $investments = $this->activeInvestments($date, $from);

        $this->statementDate = $date;

        $investments = $investments->map(
            function (DepositHolding $investment) use ($date) {
                return $investment->calculate($date, true)->getPrepared();
            }
        );

        return $this->appendTotals($investments);
    }

    protected function appendTotals($investments)
    {
        return [
            'investments' => $investments,
            'totals' => [
                'principal' => $investments->sum(
                    function ($investment) {
                        return $investment->total->principal;
                    }
                ),
                'gross_interest' => $investments->sum(
                    function ($investment) {
                        return $investment->total->gross_interest_cumulative;
                    }
                ),
                'net_interest' => $investments->sum(
                    function ($investment) {
                        return $investment->total->net_interest_cumulative;
                    }
                ),
                'value' => $investments->sum(
                    function ($investment) {
                        return $investment->total->total;
                    }
                )
            ]
        ];
    }

    protected function activeInvestments($date, $from)
    {
        return DepositHolding::statement($date, $from)
            ->currency($this->currency, true)
            ->fundManager($this->fundManager)
            ->forSecurity($this->institution)
            ->get();
    }
}
