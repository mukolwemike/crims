<?php
/**
 * Date: 11/01/2017
 * Time: 18:59
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Statement;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\User;
use Carbon\Carbon;

class Generator
{
    public function generate(
        FundManager $fundManager,
        Currency $currency,
        PortfolioSecurity $security,
        Carbon $date,
        Carbon $from = null,
        User $sender,
        $campaignId = null
    ) {
        $processor = new Processor($currency, $fundManager, $security);

        $data = $processor->process($date, $from);

        $campaign = $campaignId ? StatementCampaign::findOrFail($campaignId) : null;

        $logo = $fundManager->logo;

        return \PDF::loadView(
            'portfolio.reports.statement',
            ['fundManager' => $fundManager, 'institution' => $security, 'logo' => $logo,
                'statementDate' => $date, 'currency' => $currency,
                'sender' => $sender, 'data' => $data, 'campaign' => $campaign,
                'companyAddress' => str_contains($fundManager->fullname, 'Cytonn Asset Manager') ? 'asset_manager' : null,
            ]
        );
    }
}
