<?php
/**
 * Date: 03/11/2016
 * Time: 12:46
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Summary;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class Analytics extends \Cytonn\Structured\Analytics\Analytics
{
    protected $currency;

    protected $fund;

    protected $fundManager;

    protected $fundType;

    protected $institution;

    protected $costValue;

    protected $offshore;


    public function __construct($fundManager, Carbon $date)
    {
        if ($fundManager instanceof FundManager) {
            $fundManager = [$fundManager];
        }

        if ($fundManager instanceof Collection) {
            $fundManager = $fundManager->all();
        }

        $this->fundManager = $fundManager;

        $this->date = $date;

        parent::__construct();
    }

    public function setFundType(SubAssetClass $subAssetClass)
    {
        $this->fundType = $subAssetClass;

        return $this;
    }

    public function setPortfolioInstitution(PortfolioSecurity $institution)
    {
        $this->institution = $institution;

        return $this;
    }

    public function setFund(UnitFund $fund)
    {
        $this->fund = $fund;

        return $this;
    }

    public function setOffshore($offshore)
    {
        $this->offshore = $offshore;

        return $this;
    }

    /**
     * @param Currency $currency
     * @return Analytics
     */
    public function setCurrency(Currency $currency): Analytics
    {
        $this->currency = $currency;

        return $this;
    }

    protected function activeInvestments(): Builder
    {
        $q = DepositHolding::activeOnDate($this->date);

        if ($this->currency) {
            if (!$this->baseCurrency) {
                $this->baseCurrency = $this->currency;
            }

            $q = $q->currency($this->currency);
        }

        if ($this->fund) {
            $q = $q->where('unit_fund_id', $this->fund->id);
        }

        if ($this->fundManager) {
            $q = $q->fundManager($this->fundManager);
        }

        if ($this->fundType) {
            $q = $q->whereHas('security', function ($security) {
                $security->where('sub_asset_class_id', $this->fundType->id);
            });
        }

        if ($this->institution) {
            $q = $q->where('portfolio_security_id', $this->institution->id);
        }

        if ($this->offshore) {
            $q = $q->offshore($this->offshore);
        }

        return $q;
    }

    public function costValue(): float
    {
        if ($this->costValue) {
            return $this->costValue;
        }

        return (float)$this->costValue = $this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                return $this->convertAmount(
                    $investment->calculated->principal(),
                    $investment->repo->currency(),
                    $this->date
                );
            });
    }

    public function marketValue(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->value();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function adjustedMarketValue(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amv = $investment->calculated->adjustedMarketValue();

                return $this->convertAmount($amv, $investment->repo->currency(), $this->date);
            });
    }

    public function grossInterest(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->grossInterest();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function netInterest(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->netInterest();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function grossInterestForDay()
    {
        return (float) $this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->dailyGrossInterest();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function netInterestForDay(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->dailyGrossInterest();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function dailyAmortization(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->dailyAmortization();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function adjustedGrossInterest(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->adjustedGrossInterest();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function interestRepaid()
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                $amount = $investment->calculated->netInterestRepaid();

                return $this->convertAmount($amount, $investment->custodialAccount()->currency, $this->date);
            });
    }

    public function repayments()
    {
        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) {
                return $investment->repayments()->ofType('interest_repayment')->count();
            });
    }

    public function weightedRate(): float
    {
        $total_deposits_plus_bal = $this->costValue() + $this->custodialBalance();

        if ($total_deposits_plus_bal == 0) {
            return 0;
        }

        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) use ($total_deposits_plus_bal) {
                $rate = $this->convertRate(
                    $investment->repo->currentRate($this->date),
                    $investment->custodialAccount()->currency,
                    $this->date
                );

                $principal = $this->convertAmount(
                    $investment->calculated->principal(),
                    $investment->custodialAccount()->currency,
                    $this->date
                );

                return ($principal / $total_deposits_plus_bal) * $rate;
            });
    }

    public function netAnnualWeightedRate()
    {
        $total_deposits_plus_bal = $this->costValue() + $this->custodialBalance();

        if ($total_deposits_plus_bal == 0) {
            return 0;
        }

        return (float)$this->getInvestments()
            ->sum(function (DepositHolding $investment) use ($total_deposits_plus_bal) {
                $rate = $this->convertRate(
                    $investment->repo->currentRate($this->date),
                    $investment->custodialAccount()->currency,
                    $this->date
                );

                $netRate = $rate * (1 - ($investment->withHoldingTaxRate() / 100));

                $principal = $this->convertAmount(
                    $investment->calculated->principal(),
                    $investment->custodialAccount()->currency,
                    $this->date
                );

                return ($principal / $total_deposits_plus_bal) * $netRate;
            });
    }

    public function grossAnnualWeightedRate()
    {
        return $this->weightedRate();
    }

    public function weightedTenor(): float
    {
        $balance = $this->custodialBalance();

        $total_deposits_plus_bal = $this->costValue() + $balance;

        if ($total_deposits_plus_bal == 0) {
            return 0;
        }

        return (float)$this->getInvestments()->sum(
            function (DepositHolding $investment) use ($total_deposits_plus_bal) {
                $tenor = $this->date->copy()->diffInDays($investment->maturity_date);

                $principal = $this->convertAmount(
                    $investment->calculated->principal(),
                    $investment->custodialAccount()->currency,
                    $this->date
                );

                return ($principal / $total_deposits_plus_bal) * round($tenor / 30, 0);
            }
        );
    }

    public function custodialBalance(): float
    {
        return (float)array_sum($this->custodialAccountsBalances());
    }

    public function custodialAccountsBalances(): array
    {
        $accounts = CustodialAccount::whereHas('products', function ($product) {
            $product->whereIn('fund_manager_id', collect($this->fundManager)->lists('id'));
        });

        if ($this->currency) {
            $accounts = $accounts->where('currency_id', $this->currency->id);
        }

        if ($this->fund) {
            $accounts = $this->fund->custodialAccount();
        }

        $accounts = $accounts->get();

        $out = [];

        foreach ($accounts as $account) {
            $out[$account->account_name] = $this->balanceForAccount($account, $this->date);
        }

        return $out;
    }

    protected function balanceForAccount(CustodialAccount $account, Carbon $date): float
    {
        $bal = $account->latestBankBalance($date);

        if ($bal) {
            return (float)$this->convertAmount($bal->amount, $account->currency, $date);
        }

        return 0.00;
    }
}
