<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/27/18
 * Time: 3:38 PM
 */

namespace App\Cytonn\Portfolio\Summary;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Portfolio\Orders\Compliance;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Unitization\Trading\Client\Performance;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Equities\EquitySecurityRepository;
use Cytonn\Portfolio\Summary\Analytics;

class FundSummary
{
    protected $fundManager;

    protected $unitFund;

    protected $date;

    public function __construct(UnitFund $unitFund, Carbon $date)
    {
        $this->fundManager = $unitFund->manager;

        $this->unitFund = $unitFund;

        $this->date = $date;
    }

    public function summaryByAssetClass()
    {
        $totals = $this->equitySummary()->market + $this->depositSummary()->market;

        $equity_exposure = $totals != 0 ? ($this->equitySummary()->market / $totals) * 100 : 0;

        $equities = (object)[
            'name' => 'Equities',
            'cost_value' => $this->equitySummary()->cost,
            'market_value' => $this->equitySummary()->market,
            'exposure' => AmountPresenter::currency($equity_exposure, true, 2)
        ];

        $deposit_exposure = $totals != 0 ? ($this->depositSummary()->market / $totals) * 100 : 0;

        $deposits = (object)[
            'name' => 'Deposits',
            'cost_value' => $this->depositSummary()->cost,
            'market_value' => $this->depositSummary()->market,
            'exposure' => AmountPresenter::currency($deposit_exposure, true, 2)
        ];

        return [$equities, $deposits];
    }

    public function summaryByFundType()
    {
        $subAssetClasses = SubAssetClass::all();

        $analytics = (new Analytics($this->fundManager, $this->date));

        $balance = $analytics->custodialBalance();

        $class = $subAssetClasses->map(function ($class) {

            $slug = $class->assetClass ? $class->assetClass->slug : null;

            $analytics = ($slug == 'equities') ? $this->equitySummary($class) : $this->depositSummary($class);

            $compliance = collect($this->fundCompliance($class))->first();

            return [
                'name' => $class->name,
                'cost_value' => $analytics->cost,
                'market_value' => $analytics->market,
                'compliant' => $compliance ? false : true,
                'reason' => $compliance ? $compliance->reason : null
            ];
        });

        return $class->push([
            'name' => 'Cash',
            'cost_value' => $balance,
            'market_value' => $balance,
        ]);
    }

    public function summaryBySecurity()
    {
        return $this->securities()->map(function ($security) {
            $subAssetClass = $security->subAssetClass;

            $slug = $subAssetClass->assetClass->slug;

            switch ($slug) {
                case 'equities':
                    return $this->summaryBySecurityOfTypeEquity($security);
                    break;

                case 'deposits':
                    return $this->summaryBySecurityOfTypeDeposit($security);
                    break;

                case 'bonds':
                    return $this->summaryBySecurityOfTypeDeposit($security);
                    break;

                default:
                    throw new ClientInvestmentException("Not Supported");
                    break;
            }
        });
    }

    public function summaryBySecurityOfTypeEquity(PortfolioSecurity $security)
    {
        $analytics = $this->security($security);

        return [
            'name' => $security->name,
            'cost_value' => $analytics->cost,
            'market_value' => $analytics->market,
        ];
    }

    public function summaryBySecurityOfTypeDeposit(PortfolioSecurity $security)
    {
        $analytics = $this->depositSummary(null, $security);

        return [
            'name' => $security->name,
            'cost_value' => $analytics->costValue(),
            'market_value' => $analytics->marketValue(),
        ];
    }

    public function equitySummary(SubAssetClass $class = null)
    {
        $securities = $this->securities($class)
            ->map(function ($security) {
                return $this->security($security);
            });

        $securities->cost = $securities->sum('cost');

        $securities->market = $securities->sum('market');

        return $securities;
    }

    public function security(PortfolioSecurity $security)
    {
        $analytics = (new EquitySecurityRepository($security))->setFund($this->unitFund);

        $security->cost = $analytics->totalPurchaseCostHeld($this->date);

        $security->market = $analytics->totalCurrentMarketValue($this->date);

        return $security;
    }

    public function depositSummary(SubAssetClass $class = null, PortfolioSecurity $security = null)
    {
        $analytics = (new Analytics($this->fundManager, $this->date->copy()));

        $analytics->setFund($this->unitFund);

        if ($security) {
            $analytics->setPortfolioInstitution($security);
        }

        if ($class) {
            $analytics->setFundType($class);
        }

        $analytics->cost = $analytics->costValue();

        $analytics->market = $analytics->marketValue();

        return $analytics;
    }

    public function securities(SubAssetClass $class = null)
    {
        if ($class) {
            return $this->unitFund->securities()
                ->whereHas('subAssetClass', function ($subClass) use ($class) {
                    $subClass->where('id', $class->id);
                })->get();
        }

        return $this->unitFund->securities()->get();
    }

    public function fundCompliance(SubAssetClass $subAssetClass)
    {
        return (new Compliance($this->unitFund, null, null, $subAssetClass))->status();
    }
}
