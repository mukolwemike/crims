<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/31/18
 * Time: 10:05 AM
 */

namespace App\Cytonn\Portfolio\Summary;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Unitization\Trading\Performance;
use Cytonn\Unitization\Trading\Trade;
use Rollbar\Rollbar;

class FundPricingSummary extends FundSummary
{
    use AlternateSortFilterPaginateTrait;

    protected $unitFund;

    protected $date;

    private $performance;

    private $aum;

    public function __construct(UnitFund $unitFund, $date = null)
    {
        $this->unitFund = $unitFund;

        $this->date = $date ? Carbon::parse($date) : Carbon::today();

        parent::__construct($this->unitFund, $this->date);

        $this->performance = new Performance($this->unitFund, $date->copy());
    }

    public function totals()
    {
        $category = $this->unitFund->category->calculation->slug;

        switch ($category) {
            case 'daily-yield':
                return $this->moneyMarketFundTotals();
                break;

            case 'variable-unit-price':
                return $this->navFundTotals();
                break;

            default:
                return $this->navFundTotals();
                break;
        }
    }

    public function summary()
    {
        $category = $this->unitFund->category->calculation->slug;

        switch ($category) {
            case 'daily-yield':
                return $this->moneyMarketFundSummary();
                break;

            case 'variable-unit-price':
                return $this->navFundSummary();
                break;

            default:
                return $this->navFundSummary();
                break;
        }
    }

    public function moneyMarketFundSummary()
    {
        return [
            'currency' => $this->unitFund->currency->code,
            'calculation' => $this->unitFund->category->calculation->slug,
            'liabilities' => $this->liabilities(),
            'totals' => $this->moneyMarketFundTotals(),
        ];
    }

    public function navFundSummary()
    {
        return [
            'currency' => $this->unitFund->currency->code,
            'calculation' => $this->unitFund->category->calculation->slug,
            'liabilities' => $this->liabilities(),
            'totals' => $this->navFundTotals(),
        ];
    }

    public function liabilities()
    {
        $performance = $this->performance;

        $aum = $this->aum();

        $fees = $performance->feesToBeChargedToday();

        return $fees->map(function ($fee) use ($performance, $aum) {


            $previous = $performance->previousFeesCharged($fee);
            $charged = $performance->todayFeesCharged($fee);
            $charging = $performance->todayFeesChargingAmount($aum, $fee);

            $amount = $previous + $charged + $charging;

            return [
                'type' => $fee->type->name,
                'amount' => $amount,
                'today' => $charged + $charging
            ];
        });
    }


    public function todayFeeAmount(UnitFundFee $fee)
    {
        if (!$fee->feeChargeFrequency) {
            return 0;
        }

        $performance = $this->performance;

        return $fee->calculateRecurrentAmount($performance->aumAtCost());
    }

    public function navFundTotals()
    {
        $this->date->addDay(1);

        $performance = $this->performance;

        return [
            'aum' => $performance->aum(),
            'liabilities' => $performance->liabilities(),
            'nav' => $performance->nav(),
            'cash' => $this->accounts(),
            'units' => $performance->totalUnits(),
            'price' => $performance->unitPrice(),
            'return' => $performance->portfolioReturn()
        ];
    }

    private function moneyMarketFundTotals()
    {
        $performance = $this->performance;

        return [
            'assets' => $performance->assetCostValue(),
            'cash' => $this->accounts(),
            'aum' => $performance->aumAtCost(),
            'gross_daily_income' => $performance->grossIncomeForDay(),
            'today_liabilities' => $performance->todayLiabilities(true),
            'net_income_per_unit' => $performance->netIncomePerUnit(),
            'gross_daily_yield' => $performance->grossDailyYield(),
            'outperformance_fee_percent' => $performance->todayOutPerformancePercent(),
            'outperformance_fee_amount' => $performance->todayOutPerformanceAmount(),
            'net_daily_yield' => $performance->netDailyYield(),
            'gross_annual_yield' => $performance->grossAnnualYield(),
            'net_annual_yield' => $performance->netAnnualYield(),
        ];
    }

    private function accounts()
    {
        return $this->performance->accounts()
            ->map(function ($account) {
                return (object) [
                    'name' => $account->account_name,
                    'currency' => $account->currency->code,
                    'balance' => $account->balance($this->date)
                ];
            })->values();
    }

    private function aum()
    {
        if ($this->aum) {
            return $this->aum;
        }

        $calculation = $this->unitFund->category->calculation->slug;

        switch ($calculation) {
            case 'variable-unit-price':
                return $this->aum = $this->performance->aum();
                break;
            case 'daily-yield':
                return $this->aum = $this->performance->aumAtCost();
                break;
            case 'simple-interest':
                return $this->aum = $this->performance->aumAtCost();
                break;
            default:
                return $this->aum = $this->performance->aumAtCost();
                break;
        }
    }
}
