<?php
/**
 * Date: 06/04/2018
 * Time: 09:21
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\AssetClasses;

use Carbon\Carbon;

class BondPerformance implements AssetClassPerformance
{
    protected $fund;

    protected $fundManager;

    protected $date;

    /**
     * BondPerformance constructor.
     * @param $fundManager
     * @param $date
     */
    public function __construct($fundManager, $date)
    {
        $this->fundManager = $fundManager;
        $this->date = Carbon::parse($date);
    }


    public function costValue()
    {
        return 0;
    }

    public function marketValue()
    {
        return 0;
    }

    public function aum()
    {
        return 0;
    }

    public function dailyYield()
    {
        return 0;
    }

    /**
     * @param mixed $fund
     * @return BondPerformance
     */
    public function setFund($fund)
    {
        $this->fund = $fund;
        return $this;
    }

    public function grossInterestForDay()
    {
        // TODO: Implement grossInterestForDay() method.
    }

    public function netInterestForDay()
    {
        // TODO: Implement netInterestForDay() method.
    }
}
