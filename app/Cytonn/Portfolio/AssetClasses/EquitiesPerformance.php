<?php
/**
 * Date: 06/04/2018
 * Time: 09:21
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\AssetClasses;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Portfolio\Equities\PortfolioValuation;

class EquitiesPerformance implements AssetClassPerformance
{
    protected $date;

    protected $fundManager;

    protected $fund;

    protected $offshore;

    /**
     * EquitiesPerformance constructor.
     * @param $date
     * @param $fundManager
     */
    public function __construct($fundManager, Carbon $date)
    {
        $this->date = $date;
        $this->fundManager = $fundManager;
    }

    public function costValue()
    {
        return $this->prepare()->portfolioCost($this->date);
    }

    public function marketValue()
    {
        return $this->prepare()->portfolioValue($this->date);
    }

    public function aum()
    {
        return $this->marketValue();
    }

    public function percentageReturn()
    {
        return $this->prepare()->percentageReturn($this->date);
    }

    protected function prepare()
    {
        $valuation = new PortfolioValuation($this->fundManager, $this->date);

        if ($this->fund) {
            $valuation->setFund($this->fund);
        }

        if ($this->offshore) {
            $valuation->setOffshore($this->offshore);
        }

        return $valuation;
    }

    public function setFund(UnitFund $fund)
    {
        $this->fund = $fund;

        return $this;
    }

    public function setOffshore($offshore)
    {
        $this->offshore = $offshore;

        return $this;
    }

    public function netAnnualWeightedRate()
    {
        return 0;
    }

    public function grossAnnualWeightedRate()
    {
        return 0;
    }

    public function grossInterestForDay()
    {
        return 0;
    }

    public function netInterestForDay()
    {
        return 0;
    }
}
