<?php
/**
 * Date: 06/04/2018
 * Time: 09:20
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\AssetClasses;

interface AssetClassPerformance
{
    public function costValue();
    public function marketValue();
    public function aum();
    public function grossInterestForDay();
    public function netInterestForDay();
    public function percentageReturn();
}
