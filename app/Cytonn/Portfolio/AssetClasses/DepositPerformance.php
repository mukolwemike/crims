<?php
/**
 * Date: 06/04/2018
 * Time: 09:20
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\AssetClasses;

use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Portfolio\Summary\Analytics;

class DepositPerformance implements AssetClassPerformance
{
    protected $analytics;

    protected $fundManager;

    protected $date;

    protected $fund;

    protected $offshore;

    /**
     * DepositPerformance constructor.
     * @param $fundManager
     * @param $date
     */
    public function __construct($fundManager, Carbon $date)
    {
        $this->fundManager = $fundManager;
        $this->date = $date;
    }

    public function costValue()
    {
        return $this->prepare()->costValue();
    }

    public function marketValue()
    {
        return $this->prepare()->marketValue();
    }

    public function aum()
    {
        return $this->marketValue();
    }

    public function percentageReturn()
    {
        return $this->netAnnualWeightedRate();
    }

    public function setFund(UnitFund $fund)
    {
        $this->fund = $fund;

        return $this;
    }

    public function setOffshore($offshore)
    {
        $this->offshore = $offshore;
        return $this;
    }

    protected function prepare()
    {
        if ($this->analytics) {
            return $this->analytics;
        }

        $analytics = new Analytics($this->fundManager, $this->date);

        $this->fund ? $analytics->setFund($this->fund) : null;

        $this->offshore ? $analytics->setOffshore($this->offshore) : null;

        $this->analytics = $analytics;

        return $this->analytics;
    }

    public function grossInterestForDay()
    {
        //TODO check how to handle armotization
        return $this->prepare()->grossInterestForDay();
//        + $this->prepare()->dailyAmortization();
    }

    public function netInterestForDay()
    {
        return $this->prepare()->netInterestForDay();
//            + $this->prepare()->dailyAmortization();
    }

    public function netAnnualWeightedRate()
    {
        return $this->prepare()->netAnnualWeightedRate();
    }

    public function grossAnnualWeightedRate()
    {
        return $this->prepare()->grossAnnualWeightedRate();
    }
}
