<?php
/**
 * Date: 13/10/2015
 * Time: 12:28 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialBalance;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\Setting;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Portfolio\Deposits\Calculator;

class DepositHoldingRepository
{
    private static $calculated;

    public $depositHolding;

    public function __construct(DepositHolding $depositHolding = null)
    {
        is_null($depositHolding)
            ? $this->depositHolding = new DepositHolding() : $this->depositHolding = $depositHolding;
    }

    /**
     * @param null $date
     * @param bool $as_at_next_day
     * @return Calculator $calc
     */
    protected function calculated($date = null, $as_at_next_day = false)
    {
        $hash = sha1(((string)$date) . '' . (string)$as_at_next_day . 'investment' . $this->depositHolding->id);

        if (isset(static::$calculated[$hash])) {
            return static::$calculated[$hash];
        }

        $date = Carbon::parse($date);
        $calculated = $this->depositHolding->calculate($date, $as_at_next_day);

        $this->setCalculated($calculated, $hash);

        return $calculated;
    }

    private function setCalculated($calculated, $hash)
    {
        static::$calculated[$hash] = $calculated;

        if (count(static::$calculated) > InvestmentsRepository::MAX_STORED) {
            $first_key = array_first(array_keys(static::$calculated));

            unset(static::$calculated[$first_key]);
        }
    }

    public function principal($date = null)
    {
        return $this->calculated($date)->principal();
    }

    public function currency()
    {
        return $this->depositHolding->custodialAccount()->currency;
    }

    public function currentRate(Carbon $date)
    {
        if ($date->lt($this->depositHolding->maturity_date)) {
            return $this->depositHolding->interest_rate;
        }

        return 0;
    }

    public function grossInterest($amount, $interestRate, $tenor)
    {
        return $amount * $interestRate * $tenor / (100 * 365);
    }

    protected function endDate(Carbon $date)
    {
        $maturity_date = Carbon::parse($this->maturityDate());

        if ($date->gt($maturity_date)) {
            return $maturity_date;
        }

        return $date;
    }

    private function maturityDate()
    {
        $m = $this->depositHolding->maturity_date;

        if (!$w = $this->depositHolding->withdrawal_date) {
            return $m;
        }

        if ($w->lte($m)) {
            return $w;
        }

        return $m;
    }


    protected function calculateTenor(Carbon $from, Carbon $to)
    {
        if ($from->gt($to)) {
            return 0;
        }

        return $from->copy()->diffInDays($to);
    }

    public function getInvestmentTotal($date = null, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->total();
    }

    public function repayments($date, $type)
    {
        $date = Carbon::parse($date);

        return $this->depositHolding->repayments()->ofType($type)->before($date)->get();
    }

    /**
     * @param $currency_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @deprecated should not be in this class
     */
    public function getActiveInvestmentsForCurrency($currency_id)
    {
        return (new DepositHolding())->whereHas(
            'CustodialAccount',
            function ($q) use ($currency_id) {
                $q->where('currency_id', $currency_id);
            }
        )->where('withdrawn', null)
            ->orWhere('withdrawn', 0)
            ->orderBy('invested_date', 'ASC')->get();
    }

    /**
     * @param $currency_id
     * @return mixed
     * @deprecated should not be in this class
     */
    public function getTotalportfolioInvestmentsForCurrency($currency_id)
    {
        $investments = (new DepositHolding())->whereHas(
            'CustodialAccount',
            function ($q) use ($currency_id) {
                $q->where('currency_id', $currency_id);
            }
        )->where('withdrawn', null)
            ->orWhere('withdrawn', 0)
            ->orderBy('invested_date', 'ASC')->get();

        return $investments->sum(
            function ($investment) {
                return $investment->repo->principal();
            }
        );
    }

    /**
     * @param $currency_id
     * @param $date
     * @return mixed
     * @deprecated should not be in this class
     */
    public function getTotalPortfolioInvestmentsForCurrencyAtDate($currency_id, $date)
    {
        return (new DepositHolding())->whereHas(
            'CustodialAccount',
            function ($q) use ($currency_id) {
                $q->where('currency_id', $currency_id);
            }
        )->activeOnDate($date)->sum('amount');
    }

    /**
     * @param $currency_id
     * @param null $fund_manager_id
     * @return int
     * @deprecated should not be in this class
     */
    public function getPortfolioCashBalanceForCurrency($currency_id, $fund_manager_id = null)
    {
        //for fund manager
        $accounts = $fund_manager_id
            ? (new CustodialAccount())->where(['currency_id' => $currency_id, 'fund_manager_id' => $fund_manager_id])
                ->get()
            : (new CustodialAccount())->where('currency_id', $currency_id)->get();

        $balance = 0;

        foreach ($accounts as $account) {
            //replace with trail balance
            $balance += $account->balanceTrail->count() ? $account->latestBankBalance()->amount : 0;
        }

        return $balance;
    }

    /**
     * @param $currency_id
     * @param null $fund_manager_id
     * @return int|mixed
     * @deprecated should not be in this class
     */
    public function getTotalPortfolioAmountForCurrency($currency_id, $fund_manager_id = null)
    {
        return
            $this->getTotalportfolioInvestmentsForCurrency($currency_id) +
            $this->getPortfolioCashBalanceForCurrency($currency_id, $fund_manager_id);
    }

    /**
     * @param $currency_id
     * @return float|int
     * @deprecated should not be in this class
     */
    public function getTotalportfolioInvestmentsValueForCurrency($currency_id)
    {
        $investments = $this->getActiveInvestmentsForCurrency($currency_id);

        $total_investments_array = [];

        foreach ($investments as $investment) {
            array_push($total_investments_array, $investment->repo->getTotalValueOfAnInvestment());
        }

        return array_sum($total_investments_array);
    }

    public function adjustedMarketValue($date = null, $as_at_next_day = false)
    {
        $date = Carbon::parse($date);

        return $this->calculated($date, $as_at_next_day)->adjustedMarketValue();
    }

    public function adjustedGrossInterest($date = null, $as_at_next_day = false)
    {
        $date = Carbon::parse($date);

        return $this->calculated($date, $as_at_next_day)->adjustedGrossInterest();
    }

    public function getTotalportfolioInvestmentsGrossInterestForCurrency($currency_id)
    {
        $investments = $this->getActiveInvestmentsForCurrency($currency_id);

        $total_gross_interest_array = [];

        foreach ($investments as $investment) {
            array_push($total_gross_interest_array, $investment->repo->getGrossInterestForInvestment());
        }

        return array_sum($total_gross_interest_array);
    }

    public function getTotalWithholdingTaxForCurrency($currency_id)
    {
        $investments = $this->getActiveInvestmentsForCurrency($currency_id);

        $total_withholding_tax_array = [];

        foreach ($investments as $investment) {
            $tax = $investment->repo->getWithholdingTaxForInvestment();

            array_push($total_withholding_tax_array, $tax);
        }
        return array_sum($total_withholding_tax_array);
    }

    public function getTotalNetInterestForCurrency($currency_id)
    {
        $investments = $this->getActiveInvestmentsForCurrency($currency_id);

        $total_net_interest_array = [];

        foreach ($investments as $investment) {
            $net_interest = $investment->repo->getNetInterestForInvestment();

            array_push($total_net_interest_array, $net_interest);
        }
        return array_sum($total_net_interest_array);
    }

    public function getDepositWeightedRateForCurrency($currency_id)
    {
        $deposits = (new DepositHolding())->whereHas(
            'CustodialAccount',
            function ($q) use ($currency_id) {
                $q->where('currency_id', $currency_id);
            }
        )->where('withdrawn', null)->orWhere('withdrawn', 0)->get();


        $balance = (new CustodialRepository())->getSumCustodialBalanceForCurrency($currency_id);

        if ($balance < 0) {
            $balance = 0;
        }

        $total_deposits_plus_bal = $deposits->sum('amount') + $balance;

        $weighted_rate_array = [];

        foreach ($deposits as $deposit) {
            if ($total_deposits_plus_bal != 0) {
                $weighted_rate = ($deposit->amount / $total_deposits_plus_bal) * $deposit->interest_rate;
            } else {
                $weighted_rate = 0;
            }

            array_push($weighted_rate_array, $weighted_rate);
        }

        return array_sum($weighted_rate_array);
    }

    public function getDepositWeightedRateForCurrencyAtDate($currency_id, $date)
    {
        $deposits = (new DepositHolding())->whereHas(
            'CustodialAccount',
            function ($q) use ($currency_id) {
                $q->where('currency_id', $currency_id);
            }
        )->activeOnDate($date)->where('withdrawn', null)->orWhere('withdrawn', 0)->get();

        $bal = CustodialBalance::balanceForCurrencyAtDate($currency_id, $date);

        !is_null($bal)
            ? $balance = $bal
            : $balance = (new CustodialRepository())->getSumCustodialBalanceForCurrency($currency_id);

        if ($balance < 0) {
            $balance = 0;
        }

        $total_deposits_plus_bal = $deposits->sum('amount') + $balance;

        $weighted_rate_array = [];

        foreach ($deposits as $deposit) {
            if ($total_deposits_plus_bal != 0) {
                $weighted_rate = ($deposit->amount / $total_deposits_plus_bal) * $deposit->interest_rate;
            } else {
                $weighted_rate = 0;
            }

            array_push($weighted_rate_array, $weighted_rate);
        }

        return array_sum($weighted_rate_array);
    }

    /**
     * @param $currency_id
     * @param null $date
     * @return float|int
     */
    public function getDepositWeightedTenorForCurrency($currency_id, $date = null)
    {
        $date_set = (new Carbon($date))->toDateString();

        $deposits = DepositHolding::where('invested_date', '<=', $date_set)
            ->where('maturity_date', '>=', $date_set)->whereHas(
                'CustodialAccount',
                function ($q) use ($currency_id) {
                    $q->where('currency_id', $currency_id);
                }
            )->get();


        $balance = (new CustodialRepository())->getSumCustodialBalanceForCurrencyAtDate($currency_id, $date);

        if ($balance < 0) {
            $balance = 0;
        }

        $total_deposits_plus_bal = $deposits->sum('amount') + $balance;


        $weighted_tenor_array = [];

        foreach ($deposits as $deposit) {
            if ($total_deposits_plus_bal != 0) {
                $tenor = (new Carbon($date))->copy()->diffInDays(new Carbon($deposit->invested_date));
                $weighted_tenor = ($deposit->amount / $total_deposits_plus_bal) * ($tenor / 30);

                array_push($weighted_tenor_array, $weighted_tenor);
            }
        }

        return array_sum($weighted_tenor_array);
    }

    public function getTenorInDays()
    {
        $invested_date = new Carbon($this->depositHolding->invested_date);

        $maturity_date = $this->getCurrentEffectiveInterestDate($this->depositHolding->maturity_date);

        return $invested_date->copy()->diffInDays($maturity_date);
    }

    public function getTenorInDaysAsAtDate(Carbon $date = null)
    {
        $invested_date = new Carbon($this->depositHolding->invested_date);

        $maturity_date = new Carbon($this->depositHolding->maturity_date);

        $date = $date ? $date : Carbon::now();

        $maturity_date = $maturity_date <= $date ? $maturity_date : $date;

        return $invested_date->copy()->diffInDays($maturity_date);
    }

    public function getNetInterest($grossInterest)
    {
        if ($this->depositHolding->taxable) {
            $tax_rate = Setting::where('key', 'withholding_tax_rate')->latest()->first()->value;
        } else {
            $tax_rate = 0;
        }

        return (1 - ($tax_rate / 100)) * $grossInterest;
    }

    public function getWithholdingTaxForInvestment($date = null)
    {
        return $this->calculated($date)->withholdingTax();

//        if ($this->depositHolding->taxable) {
//
//            $tax_rate = ($this->depositHolding->tax_rate_id)
//                ? Setting::find($this->depositHolding->tax_rate_id)->value
//                : Setting::where('key', 'withholding_tax_rate')->latest()->first()->value;
//
//        } else {
//            $tax_rate = 0;
//        }
//
//        return ($tax_rate / 100) * $this->getGrossInterestForInvestment($date);
    }

    public function getNetInterestForInvestment($date = null, $as_at_next_date = false)
    {
        return $this->getNetInterest($this->getGrossInterestForInvestment($date, $as_at_next_date));
    }

    public function withdrawn($date = null)
    {
        $date = Carbon::parse($date);

        if ($this->depositHolding->withdrawn && $date >= Carbon::parse($this->depositHolding->withdrawal_date)) {
            return true;
        }

        return $this->getTotalValueOfAnInvestmentAsAtDate($date) <= 0;
    }

    public function getNetInterestAfterRepayments($date = null)
    {
        $date = new Carbon($date);

        $repayments = $this->repayments($date, 'interest_repayment')->sum('amount');

        return $this->getNetInterestForInvestment($date) - $repayments;
    }

    public function getGrossInterestForInvestment($date = null, $as_at_next_date = false)
    {
        return $this->calculated($date, $as_at_next_date)->grossInterest();
    }

    public function getCurrentTenorInDays()
    {
        if ($this->depositHolding->withdrawn) {
            return (new Carbon($this->depositHolding->withdrawal_date))
                ->diffInDays(new Carbon($this->depositHolding->invested_date));
        } elseif ((new Carbon($this->depositHolding->maturity_date))->isFuture()) {
            return (new Carbon())->diffInDays(new Carbon($this->depositHolding->invested_date));
        } else {
            return (new Carbon($this->depositHolding->maturity_date))
                ->diffInDays(new Carbon($this->depositHolding->invested_date));
        }
    }

    public function getCurrentGrossInterest()
    {
        return $this->getGrossInterestForInvestment();
    }

    public function getCurrentNetInterest()
    {
        return $this->getNetInterest($this->getCurrentGrossInterest());
    }

    public function getCurrentEffectiveInterestDate($maturity_date)
    {
        $maturity_date = new Carbon($maturity_date);

        if ($maturity_date->isPast()) {
            return $maturity_date;
        }

        return Carbon::today();
    }

    public function getTotalValueOfAnInvestment(Carbon $date = null, $as_at_next_day = false)
    {
        return $this->getInvestmentTotal($date, $as_at_next_day);
    }

    public function getTotalValueOfAnInvestmentAsAtDate($date, $as_at_next_day = false)
    {
        return $this->getInvestmentTotal($date, $as_at_next_day);
    }

    public function getKesConvertedValue($amount)
    {
        $currency = $this->depositHolding->custodialAccount()->currency;

        if ($currency->id == 1) {
            return $amount;
        } else {
            $exchangeRate = $currency->toExchangeRates()->latest()->first();

            if (!$exchangeRate) {
                throw new ClientInvestmentException("Exchange rate for currency $currency->code was not found");
            }

            return $amount * $exchangeRate->rate;
        }
    }

    /**
     * @param $date
     * @param bool $as_at_next_day
     * @return float|int
     */
    public function getGrossInterestForInvestmentAsAtDate($date, $as_at_next_day = false)
    {
        return $this->getGrossInterestForInvestment($date, $as_at_next_day);
    }

    public function grossInterestBeforeDeductions($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->grossInterestBeforeDeductions();
    }

    public function getNetInterestForInvestmentAsAtDate($date, $as_at_next_day = false)
    {
        $gross = $this->getGrossInterestForInvestmentAsAtDate($date, $as_at_next_day);

        return $this->getNetInterest($gross);
    }

    public function grossInterestRepaid($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->grossInterestRepaid();
    }

    public function netInterestRepaid($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->netInterestRepaid();
    }

    public function grossInterestAccrued($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->grossInterestBeforeDeductions();
    }

    public function netInterestAccrued($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->netInterestBeforeDeductions();
    }

    public function getPreviousTransaction($transaction)
    {
        return (new CustodialTransaction())->where(
            'custodial_account_id',
            $transaction->custodial_account_id
        )->where('created_at', '<', $transaction->created_at)->latest()->first();
    }


    public function checkIfDuplicate(array $data)
    {
        $data = array_only(
            $data,
            ['portfolio_investor_id', 'invested_date', 'maturity_date', 'amount', 'interest_rate']
        );

        return (bool)(new DepositHolding())->where($data)->first();
    }

    public function suggestInstitutionCode()
    {
        $codes = Collection::make((new PortfolioInvestor())->whereNotNull('code')->pluck('code')->all())->map(
            function ($code) {
                return intval($code);
            }
        )->toArray();

        if (count($codes) == 0) {
            return 1000;
        } elseif (is_array($codes) && count($codes) > 1) {
            return 1 + max($codes);
        } elseif ($codes instanceof Collection) {
            return 1 + max($codes->all());
        }

        return 1 + max($codes);
    }

    /*
     * Get the custodial account id for the deposit holding
     */
    public function getCustodialAccountId()
    {
        $transaction = $this->depositHolding->investTransaction;

        if ($transaction) {
            return $transaction->custodial_account_id;
        }

        return null;
    }

    /*
     * Get the custodial account id for the deposit holding
     */
    public function getCustodialAccountName()
    {
        $transaction = $this->depositHolding->investTransaction;

        if ($transaction) {
            return $transaction->custodialAccount->account_name;
        }

        return null;
    }

    public function interestValue(Carbon $date = null)
    {
        $principal = abs($this->depositHolding->nominal_value);

        $rate = $this->depositHolding->interest_rate;

        $tenor = $this->depositHolding->maturity_date->diffInMonths($this->depositHolding->invested_date);

        return $principal * $rate / 100;
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getTotalAvailableInterestAtDate($date, $asAtNextDay = false, $interestRate = null)
    {
        return $this->calculated($date, $asAtNextDay, $interestRate)->netInterest();
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getTotalAvailableInterestAtNextDay($date)
    {
        return $this->getTotalAvailableInterestAtDate($date, true);
    }
}
