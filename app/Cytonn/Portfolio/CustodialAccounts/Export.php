<?php

namespace Cytonn\Portfolio\CustodialAccounts;

use App\Cytonn\Models\CustodialAccount;
use Carbon\Carbon;

class Export
{
    public function transactions(CustodialAccount $account, Carbon $start, Carbon $end, $name = null)
    {
        $transactions = $account->transactions()
            ->where('date', '<=', $end)
            ->where('date', '>=', $start)
            ->where('amount', '!=', 0)
            ->latest('date')
            ->orderBy('id', 'desc')
            ->get();

        if (!$name) {
            $name = 'Custodial Account Details';
        }

        return \Excel::create(
            $name,
            function ($excel) use ($transactions, $account) {
                $excel->sheet(
                    'Transactions',
                    function ($sheet) use ($transactions, $account) {
                        $sheet->loadView(
                            'portfolio/partials/custodialdetailstable',
                            [ 'transactions' => $transactions, 'account_id' => $account->id, 'custodial'=>$account ]
                        );
                    }
                );
            }
        )->store('xlsx');
    }
}
