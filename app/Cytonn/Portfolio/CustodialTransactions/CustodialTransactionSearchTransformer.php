<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Portfolio\CustodialTransactions;

use App\Cytonn\Models\CustodialTransaction;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class CustodialTransactionSearchTransformer extends TransformerAbstract
{
    /**
     * @param CustodialTransaction $transaction
     * @return array
     */
    public function transform(CustodialTransaction $transaction)
    {
        $date = Carbon::parse($transaction->date)->toDateString();

        $amount = $transaction->amount;

        $description = $transaction->description;

        $recievedFrom = $transaction->received_from;

        $currency = $transaction->custodialAccount->currency->code;

        return [
            'value' => $transaction->id,
            'date' => $date,
            'amount' => $amount,
            'currency' => $currency,
            'description' => $description,
            'received_from' => $recievedFrom,
            'label' => $transaction->id . ' : ' . $date . ' : ' . $currency . ' ' . $amount . ' : ' . $description
        ];
    }
}
