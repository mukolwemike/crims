<?php
/**
 * Date: 19/08/2016
 * Time: 3:55 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\FundManager\Scopes;

use Cytonn\Investment\FundManager\Scope;
use Illuminate\Database\Eloquent\Builder;

class HasCustodialAccountScope extends Scope
{
    public function whenApplied(Builder $builder)
    {
        $builder->whereHas(
            'custodialAccount',
            function ($account) {
                $account->where('fund_manager_id', $this->fundManagerScope->getSelectedFundManagerId());
            }
        );
    }
}
