<?php
/**
 * Date: 19/08/2016
 * Time: 3:54 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\FundManager\Traits;

use Cytonn\Portfolio\FundManager\Scopes\HasCustodialAccountScope;

trait HasCustodialAccountFundManagerTrait
{
    public static function bootHasCustodialAccountFundManagerTrait()
    {
        static::addGlobalScope(new HasCustodialAccountScope());
    }
}
