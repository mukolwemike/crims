<?php
/**
 * Date: 8/17/15
 * Time: 4:50 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio;

/**
 * Class Portfolio
 *
 * @package Cytonn\Portfolio
 */
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Setting;
use App\Cytonn\Models\TransactionOwner;

/**
 * Class Portfolio
 *
 * @package Cytonn\Portfolio
 */
class Portfolio
{
    /**
     * @var
     */
    public $investment;

    /**
     * @var
     */
    public $custodial;

    /**
     * Returns the bank/cp owner, if not available saves a new bank/cp owner to transaction owners table
     *
     * @return TransactionOwner
     */
    public function getPortfolioTransactionOwner(DepositHolding $investment)
    {
        //get transaction owner from client id
        $owner = TransactionOwner::where('fund_investor_id', $investment->fund_investor_id)->first();

        if ($owner == null) { //client not in transaction owners table, add
            $owner = new TransactionOwner();
            $owner->fund_investor_id = $investment->fund_investor_id;
            $owner->client_id = null;
            $owner->fund_manager_id = null;
            $owner->save();
        }

        return $owner;
    }

    /**
     * Returns the maturity date given the invested date and tenor in months
     *
     * @param  $investedDate
     * @param  $tenor
     * @return mixed
     */
    protected function getMaturityDate($investedDate, $tenor)
    {
        return $investedDate->copy()->addMonths($tenor);
    }

    /**
     * Returns the value of investment less withholding tax given the principal, tenor in days and rate %
     *
     * @param  $principal
     * @param  $tenorInDays
     * @param  $ratePercent
     * @return mixed
     */
    public function getFinalValue($principal, $tenorInDays, $ratePercent, $taxable)
    {
        $interest = $principal * $tenorInDays * $ratePercent / (365 * 100);

        if ($taxable) {
            $withHoldingTax = Setting::where('key', 'withholding_tax_rate')->latest()->first()->value * $interest / 100;
        } else {
            $withHoldingTax = 0;
        }

        return $principal + $interest - $withHoldingTax;
    }

    /**
     * Returns the tenor in days given the invested date and maturity date
     *
     * @param  $investedDate
     * @param  $maturityDate
     * @return mixed
     */
    protected function getTenorInDays($investedDate, $maturityDate)
    {
        return $investedDate->copy()->diffInDays($maturityDate);
    }

    /**
     * Returns the custody fees charged for that investment
     *
     * @param  Int $amount
     * @return float
     */
    protected function getCustodyFees($amount)
    {
        return Setting::where('key', 'custody_fee_rate')->latest()->first()->value * $amount;
    }
}
