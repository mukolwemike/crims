<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Portfolio\SuspenseTransactions;

use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class SuspenseTransactionSearchTransformer extends TransformerAbstract
{
    /**
     * @param SuspenseTransaction $transaction
     * @return array
     */
    public function transform(SuspenseTransaction $transaction)
    {
        $date = Carbon::parse($transaction->date)->toDateString();

        $account = $transaction->custodialAccount->account_name;

        $currency = $transaction->custodialAccount->currency->code;

        $transactionId = $transaction->transaction_id;

        return [
            'value' => $transaction->id,
            'label' => $transaction->id . ' : ' . $date . ' : ' . $currency . ' ' .$transaction->amount .
                ' : ' . $account . ' : ' . $transactionId,
            'amount' => $transaction->amount,
            'currency' => $currency,
            'date' => $date,
            'account' => $account,
            'transaction_id' => $transactionId
        ];
    }
}
