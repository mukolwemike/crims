<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Portfolio\SuspenseTransactions;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;

class SuspenseTransactionRepository
{
    public $transaction;

    /**
     * SuspenseTransactionRepository constructor.
     * @param SuspenseTransaction|null $transaction
     */
    public function __construct(SuspenseTransaction $transaction = null)
    {
        $this->transaction = is_null($transaction) ? new SuspenseTransaction() : $transaction;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function getSuspenseTransactionById($id)
    {
        return SuspenseTransaction::findOrFail($id);
    }

    /**
     * @return bool
     */
    public function hasApproval()
    {
        return $this->transaction->clientApproval()->exists() || $this->transaction->approval()->exists();
    }

    /**
     * @return SuspenseTransaction|null
     */
    public function setMatched()
    {
        $this->transaction->matched = 1;

        $this->transaction->save();

        return $this->transaction->fresh();
    }
}
