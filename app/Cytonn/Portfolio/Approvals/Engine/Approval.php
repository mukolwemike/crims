<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Portfolio\Approvals\Engine;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use App\Events\Approvals\ClientTransactionApproved;
use App\Exceptions\CrimsException;
use Cytonn\Exceptions\AuthorizationDeniedException;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStage;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStep;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Exception;
use Illuminate\Support\Facades\Auth;

/**
 * Class Approval
 * @package Cytonn\Portfolio\Approvals\Engine
 */
class Approval
{
    use LocksTransactions;

    /**
     * @var PortfolioTransactionApproval
     */
    protected $approval;

    /**
     * Approval constructor.
     * @param PortfolioTransactionApproval $approval
     */
    public function __construct(PortfolioTransactionApproval $approval)
    {
        $this->approval = $approval;
    }

    /**
     * @return mixed
     */
    public function nextStage()
    {
        return $this->getRemainingStages()->first();
    }

    /**
     * @return mixed
     * @throws ClientInvestmentException
     */
    private function getRemainingStages()
    {
        $type = $this->transactionType();

        if (!$type) {
            throw new ClientInvestmentException("Transaction type could not be found");
        }

        $approved = $this->approval->steps->pluck('stage_id')->all();

        return $type->allStages()
            ->filter(
                function (PortfolioTransactionApprovalStage $stage) use ($approved) {
                    return !in_array($stage->id, $approved);
                }
            );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    private function transactionType()
    {
        return $this->approval->type();
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function approve()
    {
        $client_id = $this->approval->institution ? $this->approval->institution->id : null;

        $key = get_class($this->approval->handler()).'_portfolio_inst_'.$client_id;

        try {
            $lock = $this->getAtomicLock($key);

            $client_lock = null;
            if ($client_id) {
                $client_lock = $this->getAtomicLock('portfolio_institution'.$client_id);
            }

            $result = $this->executeApproval();

            $this->releaseAtomicLock($lock);
            $this->releaseAtomicLock($client_lock);
        } catch (Exception $exception) {
            $this->releaseAtomicLock($lock);
            $this->releaseAtomicLock($client_lock);

            throw $exception;
        }

        return $result;
    }

    /**
     * @return mixed
     * @throws AuthorizationDeniedException
     * @throws CRIMSGeneralException
     */
    public function executeApproval()
    {
//        $this->lockForUpdate();

        $stage = $this->nextStage();

        $this->checkAccess($stage);

        $handler = $this->approval->handler();

        //run any pre-approval validations
        if (method_exists($handler, 'preApproval')) {
            $handler->preApproval();
        }

        $this->validateApproval($stage);

        $step = $this->createApprovalStep($stage);

        if ($this->checkFullyApproved($step)) {
            $this->approval->approve();

            return $this->handle();
        }

        \Flash::success('The transaction has been sent to the next stage of approval');

        $next = redirect()->back();

        $this->approval->raise(new ApprovalSuccessful($this->approval, $next));

        return $this->approval->dispatchEventsFor($this->approval);
    }

    /**
     * @param PortfolioTransactionApprovalStage $stage
     * @return bool
     * @throws AuthorizationDeniedException
     */
    public function checkAccess(PortfolioTransactionApprovalStage $stage)
    {
        if (\App::environment('testing') || \App::environment('local') || $stage->applies_to_all) {
            return true;
        }

        if ($stage->approvers()->where('user_id', \Auth::user()->id)->exists()) {
            return true;
        }

        throw new AuthorizationDeniedException('You cannot approve this transaction');
    }

    /**
     * @param PortfolioTransactionApprovalStage $stage
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    private function createApprovalStep(PortfolioTransactionApprovalStage $stage)
    {
        return PortfolioTransactionApprovalStep::create([
            'approval_id' => $this->approval->id,
            'stage_id' => $stage->id,
            'user_id' => Auth::user()->id
        ]);
    }

    /**
     * @param PortfolioTransactionApprovalStep $step
     * @return bool
     */
    private function checkFullyApproved(PortfolioTransactionApprovalStep $step)
    {
        $stages = $this->getRemainingStages()->reject(
            function ($stage) use ($step) {
                return $step->stage_id == $stage->id;
            }
        );

        if ($nxt = $stages->first()) {
            $this->approval->update(['awaiting_stage_id' => $nxt->id]);
        }

        return $stages->count() == 0;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function handle()
    {
        $handler = $this->approval->handler();

        if (!($handler instanceof ApprovalHandlerInterface)) {
            throw new Exception('The handler should be an instance of ApprovalHandlerInterface');
        }

        if (!$this->approval->approved) {
            $this->approval->approve();
        }

        $output = $this->approval->handler()->handle($this->approval);

        \Flash::success('Transaction is now fully approved, it has now been executed');

        event(new ClientTransactionApproved($this->approval));

        return $output;
    }

    private function lockForUpdate()
    {
        $this->lockModelForUpdate($this->approval);
    }

    /**
     * @param $stage
     * @throws CRIMSGeneralException
     */
    private function validateApproval($stage)
    {
        if (is_null($stage)) {
            throw new CRIMSGeneralException("Request does not seem to have next step, try again");
        }

        $step = $this->approval->steps()->where('stage_id', $stage->id);

        if ($step->exists() || $this->approval->approved) {
            throw new CRIMSGeneralException("Transaction is already approved");
        }
    }

    /**
     * @param User $user
     * @throws CrimsException
     */
    public function forceApprove(User $user)
    {
        if ($user->username != 'system') {
            throw new CrimsException("Only system allowed to force approve transactions");
        }

        $this->approval->type()->allStages()->each(function ($stage) use ($user) {
            PortfolioTransactionApprovalStep::create([
                'approval_id' => $this->approval->id,
                'stage_id' => $stage->id,
                'user_id' => $user->id
            ]);
        });

        $this->approval->approve($user);
    }
}
