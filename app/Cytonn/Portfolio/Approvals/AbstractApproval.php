<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Portfolio\Approvals;

use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Presenters\InstitutionPresenter;

abstract class AbstractApproval implements ApprovalHandlerInterface
{
    protected $approval;

    protected $formats = [];

    public function __construct(PortfolioTransactionApproval $approval = null)
    {
        $this->approval = $approval;
    }

    public function get($fieldName)
    {
        if (method_exists($this, 'pluck')) {
            $val = $this->pluck($fieldName);

            if ($val) {
                return $this->format($fieldName, $val);
            }
        }

        $payload = $this->approval->payload;

        if ($f = $this->getters($fieldName)) {
            return $this->format($fieldName, $f);
        }

        if (isset($payload[$fieldName])) {
            return $this->format($fieldName, $payload[$fieldName]);
        }

        return null;
    }

    protected function getters($name)
    {
        switch ($name) {
            case 'institution_name':
                if ($c = $this->approval->institution) {
                    return InstitutionPresenter::presentName($c->id);
                }
                // no break
            default:
                return null;
        }
    }

    protected function format($name, $value)
    {
        if (!isset($this->formats[$name])) {
            return $value;
        }

        $format = $this->formats[$name];

        switch ($format) {
            case 'date':
                return Carbon::parse($value)->toFormattedDateString();
            case 'time':
                return Carbon::parse($value)->toDayDateTimeString();
            case 'amount':
                return AmountPresenter::currency($value);
        }

        if (starts_with($format, 'append')) {
            $to_append = last(explode(':', $this->formats[$name]));
            return $value.$to_append;
        }

        return $value;
    }

    public static function create(PortfolioInvestor $investor = null, User $user, array $data)
    {
        $name = strtolower(snake_case(class_basename(static::class)));

        return PortfolioTransactionApproval::create(
            [
                'institution_id' => $investor ? $investor->id : null,
                'transaction_type' => $name,
                'sent_by' => $user->id,
                'payload' => $data
            ]
        );
    }
}
