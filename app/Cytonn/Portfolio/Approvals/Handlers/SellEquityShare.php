<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 11:54 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Authorization\Authorizer;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class SellEquityShare implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $sale = new EquityShareSale();
        $sale->fill($data);
        $sale->save();

        $this->debit($sale, $approval);
        $approval->approve();

        \Flash::success('Equity share sale has been saved');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $security = PortfolioSecurity::find($data['security_id']);

        $fund = null;
        if (isset($data['unit_fund_id'])) {
            $fund = UnitFund::findOrFail($data['unit_fund_id']);
        }

        return [
            'security'=>$security,
            'fund' => $fund
        ];
    }

    /**
     * @param EquityShareSale $sale
     * @param $approval
     * @return \App\Cytonn\Models\CustodialTransaction
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    private function debit(EquityShareSale $sale, $approval)
    {
        $amount = $sale->number * $sale->sale_price;

        $fund = $sale->unitFund;

        $trans = PortfolioTransaction::build(
            $fund->custodialAccount,
            'I',
            $sale->date,
            'Sale of shares: '.$sale->security->name. ' Fund: '.$fund->name,
            $amount,
            $approval,
            $sale->security->investor,
            null
        )->debit();

        return $trans;
    }
}
