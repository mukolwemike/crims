<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class EditPortfolioSecurity implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files', 'sub_asset_class', 'asset_class']);

        $investorInput = [
            'name' => $data['name'],
            'account_name' => $data['investor_account_name'],
            'account_number' => $data['investor_account_number'],
            'bank_name' => $data['investor_bank'],
            'branch_name' => $data['investor_bank_branch']
        ];

        if (!isset($data['portfolio_investor_id'])) {
            $investor = PortfolioInvestor::create($investorInput);

            $input['portfolio_investor_id'] = $investor->id;
        } else {
            $investor = PortfolioInvestor::findOrFail($data['portfolio_investor_id']);

            $investor->update($investorInput);
        }

        $security = PortfolioSecurity::findOrFail($data['id']);

        $security->update($input);

        $approval->approve();

        \Flash::success('Portfolio Security updated succesfully');

        $next = \Redirect::to('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $security = PortfolioSecurity::findOrFail($data['id']);

        $subAssetClass = SubAssetClass::findOrFail($data['sub_asset_class_id']);

        $depositType =
            (isset($data['deposit_type_id'])) ? PortfolioInvestmentType::findOrFail($data['deposit_type_id']): null;

        $fundManager = FundManager::findOrFail($data['fund_manager_id']);

        return [
            'data'=> $data,
            'depositType' => $depositType,
            'subAssetClass' => $subAssetClass,
            'fundManager' => $fundManager,
            'security' => $security
        ];
    }
}
