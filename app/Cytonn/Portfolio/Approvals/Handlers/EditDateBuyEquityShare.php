<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 12/10/2018
 * Time: 09:24
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Portfolio\Actions\EquityEdit;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;

class EditDateBuyEquityShare implements ApprovalHandlerInterface
{
    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equityHolding = EquityHolding::findOrFail($data['id']);

        (new EquityEdit())->purchase($equityHolding, $data);

        $approval->approve();

        \Flash::success('The purchased shares date edit have been reversed');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equityHolding = EquityHolding::find($data['id']);

        $security = $equityHolding->security;

        return [
            'equityHolding' => $equityHolding,
            'security' => $security,
            'data' => $data
        ];
    }
}
