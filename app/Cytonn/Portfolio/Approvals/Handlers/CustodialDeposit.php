<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CustodialDeposit implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    /**
     * @var Authorizer
     */
    protected $authorizer;

    /**
     * CustodialReconciliation constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $trans = new CustodialTransaction();
        $trans->custodial_account_id = $data['custodial_id'];

        if (isset($data['deposit_type'])) {
            $transactionType = CustodialTransactionType::where('name', $data['deposit_type'])->first();
        } else {
            $transactionType = CustodialTransactionType::where('name', 'FMI')->first();
        }

        $trans->type = $transactionType->id;
        $amount = abs($data['amount']);
        $trans->amount = $amount;
        $trans->date = $data['date'];
        $trans->description = $transactionType->description . ' : ' . $data['narrative'];
        $trans->approval_id = $approval->id;
        $trans->save();

        $approval->approve();

        $suspenseTransaction = $approval->suspenseTransaction;

        if ($suspenseTransaction) {
            $suspenseTransaction->repo->setMatched();
        }

        \Flash::success('Deposit transaction has been completed successfully');
        $next = \redirect('/dashboard/portfolio/custodials/details/' . $data['custodial_id']);
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $account = CustodialAccount::find($data['custodial_id']);

        if (isset($data['deposit_type'])) {
            $transactionType = CustodialTransactionType::where('name', $data['deposit_type'])->first();
        } else {
            $transactionType = null;
        }

        return [
            'account' => $account,
            'transactionType' => $transactionType
        ];
    }
}
