<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 10:24 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundType;
use \App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use Cytonn\Authorization\Authorizer;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Portfolio\Commands\EditInvestmentCommand;

class EditInvestment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * EditInvestment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $investment = DepositHolding::findOrFail($approval->payload['investment_id']);

        if ($investment->withdrawn) {
            throw new ClientInvestmentException('The investment has already been withdrawn, tou cannot edit it');
        }

        $this->execute(new EditInvestmentCommand($approval), ['approval' => $approval]);

        $approval->approve();

        \Flash::success('The Investment has been saved');

        $next = \redirect('/dashboard/portfolio/securities/' . $investment->portfolio_security_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        if (!isset($data['investment_id'])) {
            $investment = null;
        } else {
            $investment = DepositHolding::findOrFail($data['investment_id']);
        }

        $custodialaccounts = function ($acc) {
            return CustodialAccount::findOrFail($acc);
        };

        $portfolioinvestmenttypes = function ($id) {
            return PortfolioInvestmentType::findOrFail($id);
        };

        $tax = isset($data['tax_rate_id']) ? Setting::find($data['tax_rate_id']) : null;

        return [
            'title'=>'Porfolio Transaction Approval',
            'investment' => $investment,
            'data' => $data,
            'custodialaccounts' => $custodialaccounts,
            'portfolioinvestmenttypes' => $portfolioinvestmenttypes,
            'tax' => $tax
        ];
    }
}
