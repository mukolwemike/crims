<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class LinkOperatingAccount implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $account = CustodialAccount::findOrFail($data['custodial_id']);

        $fund = UnitFund::findOrFail($data['unit_fund_id']);
        $fund->operatingAccounts()->attach($account);

        \Flash::success("Unit Fund is linked to operating account successfully");

        $next = \Redirect::to("dashboard/portfolio/custodials");

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $account = CustodialAccount::findOrFail($data['custodial_id']);

        $product = (isset($data['product_id'])) ? Product::findOrFail($data['product_id']) : null;
        $shares = (isset($data['entity_id'])) ? SharesEntity::findOrFail($data['entity_id']) : null;
        $fund = (isset($data['unit_fund_id'])) ? UnitFund::findOrFail($data['unit_fund_id']) : null;
        $project = (isset($data['project_id'])) ? Project::findOrFail($data['project_id']) : null;

        return [
            'data' => $data,
            'account' => $account,
            'product' => $product,
            'shares' => $shares,
            'fund' => $fund,
            'project' => $project,
        ];
    }
}
