<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccountBalanceTrail;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class DeleteCustodialAccountBalanceTrail implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param PortfolioTransactionApproval $approval
     * @throws \Exception
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $trail = CustodialAccountBalanceTrail::findOrFail($data['trail_id']);

        $trail->delete();

        \Flash::success('Account balance trail has been deleted');

        $next = \redirect('/dashboard/portfolio/custodials/account-balance-trail/' . $trail->account_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  PortfolioTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval, array $vars = null)
    {
        return ['trail' => CustodialAccountBalanceTrail::findOrFail($approval->payload['trail_id'])];
    }
}
