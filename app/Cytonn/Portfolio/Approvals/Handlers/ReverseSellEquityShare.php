<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Actions\EquityReverse;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class ReverseSellEquityShare implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equitySale = EquityShareSale::findOrFail($data['id']);

        app(EquityReverse::class)->sale($equitySale, $data);

        $approval->approve();

        \Flash::success('Reversal of Equity share sale has been executed');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equitySale = EquityShareSale::find($data['id']);

        if (is_null($equitySale)) {
            $equitySale = EquityShareSale::where('id', $data['id'])->withTrashed()->first();
        }

        return [
            'security'=>$equitySale->security,
            'equitySale' => $equitySale
        ];
    }
}
