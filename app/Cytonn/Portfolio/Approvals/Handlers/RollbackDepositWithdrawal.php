<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Actions\Reverse;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class RollbackDepositWithdrawal implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $deposit = (new DepositHolding())->findOrFail($approval->payload['holding_id']);

        (new Reverse())->withdrawal($deposit);

        $approval->approve();

        \Flash::success('The investment withdrawal rollback has been done');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $deposit = (new DepositHolding())->withTrashed()->findOrFail($data['holding_id']);
        $portfolioInvestor = $deposit->security->investor;

        return['deposit' => $deposit, 'data' => $data, 'portfolioInvestor' => $portfolioInvestor];
    }
}
