<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 12:10 PM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategyHoldingType;
use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class ActiveStrategyDividend implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * SaveActiveStrategyDividend constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }


    public function handle(PortfolioTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $data = $approval->payload;

        switch ($data['dividend_type']) {
            case 'cash':
                $this->cash($data);
                break;
            case 'shares':
                $this->scrip($data);
                break;
            default:
                new ClientInvestmentException('Dividend type is not supported!');
        }

        $approval->approve();

        \Flash::success('The new dividend has been saved');

        $next = \redirect('/dashboard/portfolio/activestrategy');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    private function cash(array $data)
    {
        $security = ActiveStrategySecurity::findOrFail($data['security_id']);

        $rate = Setting::where('key', 'dividend_tax_rate')->latest()->first();

        $after_tax = (1 - ($rate->value/100)) * $data['dividend'];

        $data['dividend_type'] = isset($data['dividend_type']) ? $data['dividend_type'] : 'cash';

        //TODO add custodial transaction as second stage
        return \App\Cytonn\Models\ActiveStrategyDividend::create(
            [
            'security_id' => $data['security_id'],
            'dividend' => $after_tax,
            'shares' => 0,
            'number' => $this->sharesAtClosure($security, Carbon::parse($data['book_closure_date'])),
            'prevailing_tax_rate_id' => $rate->id,
            'conversion_price' => $this->ifIsset($data, 'conversion_price'),
            'date' => $data['date'],
            'book_closure_date' => $data['book_closure_date'],
            'dividend_type' => $data['dividend_type']
            ]
        );
    }

    private function scrip(array $data)
    {
        $security = ActiveStrategySecurity::findOrFail($data['security_id']);

        $rate = $this->taxRate();

        $shares_at_closure = $this->sharesAtClosure($security, Carbon::parse($data['book_closure_date']));

        if (isset($data['method_of_calculation']) and $data['method_of_calculation'] == 'ratio') {
            $shares = $this->calculateFromRatio($shares_at_closure, $data);
        } else {
            $shares = $this->calculateFromDividendAndConversionPrice($rate, $data, $shares_at_closure);
        }

        $activeStrategyDividend = new \App\Cytonn\Models\ActiveStrategyDividend();

        $activeStrategyDividend->fill(
            [
            'security_id' => $data['security_id'],
            'dividend' => 0,
            'shares' => $shares,
            'number' => $shares_at_closure,
            'prevailing_tax_rate_id' => $rate->id,
            'conversion_price' => isset($data['conversion_price']) ? $data['conversion_price'] : null,
            'date' => $data['date'],
            'book_closure_date' => $data['book_closure_date'],
            'dividend_type' => $data['dividend_type']
            ]
        );

        $activeStrategyDividend->save();

        $type = ActiveStrategyHoldingType::where('slug', 'dividend')->first();

        //TODO move to second stage
        $security->repo->purchase(0, Carbon::parse($data['date']), $shares, $type);
    }

    private function sharesAtClosure(ActiveStrategySecurity $security, Carbon $date)
    {
        return $security->repo->numberOfSharesHeld($date);
    }

    private function taxRate()
    {
        return Setting::where('key', 'dividend_tax_rate')->latest()->first();
    }

    private function ifIsset($array, $key)
    {
        if (isset($array[$key])) {
            return $array[$key];
        }

        return null;
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $dividend_types = ['cash'=>'Cash', 'shares'=>'Shares'];
        $methods_of_calculation = ['ratio'=>'Ration', 'conversion'=>'Conversion Price'];

        $data = $approval->payload;

        $security = ActiveStrategySecurity::findOrFail($data['security_id']);

        $shares_at_closure = $this->sharesAtClosure($security, Carbon::parse($data['book_closure_date']));

        $ctiveStrategySecurity = function ($id) {
            return ActiveStrategySecurity::findOrFail($id);
        };

        return [
                    'dividend_types'=>$dividend_types, 'shares_at_closure'=> $shares_at_closure,
                    'ctiveStrategySecurity'=>$ctiveStrategySecurity, 'methods_of_calculation'=> $methods_of_calculation
        ];
    }

    /**
     * @param $rate
     * @param $data
     * @param $shares_at_closure
     * @return float|int
     */
    private function calculateFromDividendAndConversionPrice($rate, $data, $shares_at_closure)
    {
        $after_tax = (1 - ($rate->value/100)) * $data['dividend'];

        return  $shares_at_closure * $after_tax / $data['conversion_price'];
    }

    /**
     * @param $shares_at_closure
     * @param $data
     * @return float|int
     */
    private function calculateFromRatio($shares_at_closure, $data)
    {
        return ($shares_at_closure / $data['old_shares']) * $data['new_shares'];
    }
}
