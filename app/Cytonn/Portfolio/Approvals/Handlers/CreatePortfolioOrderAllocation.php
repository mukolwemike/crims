<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 11:02 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderAllocation;
use App\Cytonn\Models\Portfolio\PortfolioOrderType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Portfolio\Orders\Allocate;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Notifier\NotificationRepository;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class CreatePortfolioOrderAllocation implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['allocations']);

        $allocations = $data['allocations'];

        $portfolioOrder = PortfolioOrder::findOrFail($data['portfolio_order_id']);

        if ($allocations) {
            $allocations = $portfolioOrder->orderAllocations()->createMany($allocations);
        } else {
            throw new ClientInvestmentException("Cannot save allocation, there are no allocations to save");
        }

        $allocations->each(function ($allocation) {
            (new NotificationRepository())->createNotificationForPortfolioConfirmationDue($allocation);
        });

        $approval->approve();

        \Flash::success('Portfolio order allocation has been saved');

        $next = \redirect('/dashboard/portfolio/orders/'.$portfolioOrder->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $portfolioOrder = PortfolioOrder::findOrFail($data['portfolio_order_id']);

        $allocations = (isset($data['allocations'])) ? collect($data['allocations']) : null;

        return [
            'data' => $data,
            'allocations' => $allocations,
            'portfolioOrder' => $portfolioOrder,
        ];
    }
}
