<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 11:02 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Actions\Reverse;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Portfolio\Commands\ReverseInvestmentCommand;

class ReverseInvestment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * ReverseInvestment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $investment = DepositHolding::findOrFail($approval->payload['investment_id']);

        (new Reverse())->investment($investment);

        $approval->approve();

        \Flash::success('The investment reversal has been successful');

        $next = \redirect('/dashboard/portfolio');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $investment = DepositHolding::findOrFail($data['investment_id']);
        $portfolioInvestor = PortfolioInvestor::find($investment->portfolio_investor_id);

        return['investment'=>$investment, 'data'=>$data, 'portfolioInvestor'=>$portfolioInvestor];
    }
}
