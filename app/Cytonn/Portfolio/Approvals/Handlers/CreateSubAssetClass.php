<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 5:27 PM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class CreateSubAssetClass implements ApprovalHandlerInterface
{
    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $data['inputs']->each(
            function ($asset) {
                $asse_class_id = $asset['asset_class_id'];
                $input = array_except($asset, ['_token', 'files']);
                $asset = new SubAssetClass();

                $asset->fill($input);
                $asset->save();
            }
        );

        $approval->approve();

        \Flash::success('Sub Assets class have been succesfully saved');

        //        $next = \Redirect::to('/dashboard/portfolio/asset-classes/'.$data['asset_class_id']);
        $next = \Redirect::to('/dashboard/portfolio/asset-classes');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload['inputs'];

        $data = $data->map(function ($asset) {
            return [
                'sub_asset_class' =>   $asset['name'],
                'asset_class' =>   AssetClass::find($asset['asset_class_id'])->name,
            ];
        });

        return ['data'=>$data];
    }
}
