<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 12:03 PM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateActiveStrategySecurity implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * CreateActiveStrategySecurity constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $data = $approval->payload;

        isset($data['security_id'])
            ? $sec = ActiveStrategySecurity::findOrFail($data['security_id'])
            : $sec = new ActiveStrategySecurity();

        $sec->name = $approval->payload['name'];

        $sec->save();

        $approval->approve();

        \Flash::success('The security has been created');

        $next = \redirect('/dashboard/portfolio/activestrategy');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        return[];
    }
}
