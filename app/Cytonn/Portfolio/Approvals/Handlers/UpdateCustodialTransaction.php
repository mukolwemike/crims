<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateCustodialTransaction implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, ValidatorTrait;

    /**
     * @var Authorizer
     */
    protected $authorizer;

    /**
     * UpdateCustodialTransaction constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $trans = CustodialTransaction::findOrFail($data['transaction_id']);

        if ($data['type'] == 'credit') {
            $amount = -abs($data['amount']);
        } elseif ($data['type'] == 'debit') {
            $amount = abs($data['amount']);
        } else {
            throw new ClientInvestmentException('Type not supported');
        }

        $trans->amount = $amount;
        $trans->date = $data['date'];
        $trans->description = CustodialTransactionType::where('name', $data['type_name'])
                ->first()->description . ' : ' . $data['narrative'];
        $trans->approval_id = $approval->id;
        $trans->save();

        $approval->approve();

        \Flash::success('Reconciliation update has been made');
        $next = \redirect('/dashboard/portfolio/custodials/details/' . $data['custodial_id']);
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $old = CustodialTransaction::findOrFail($data['transaction_id']);
        $trans_data = $this->reject($data, ['created_at', 'updated_at']);
        $new = (new CustodialTransaction())->fill($trans_data);

        // Set the old data if it's not approved
        if (! $approval->approved) {
            $data['edited'] = $old->toArray();
            $approval->payload = $data;
            $approval->save();
        }

        $old = (new CustodialTransaction())->fill($data['edited']);

        return ['old'=>$old, 'new'=>$new];
    }
}
