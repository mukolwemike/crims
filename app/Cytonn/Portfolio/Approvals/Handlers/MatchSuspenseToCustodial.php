<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class MatchSuspenseToCustodial implements ApprovalHandlerInterface
{
    /**
     * @param PortfolioTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $suspenseTransaction = SuspenseTransaction::findOrFail($data['suspense_transaction_id']);

        if ($suspenseTransaction->matched == 1) {
            throw new ClientInvestmentException('The suspense transaction is already matched');
        }

        $custodialTransaction = CustodialTransaction::findOrFail($data['custodial_transaction_id']);

        $custodialTransaction->bank_amount = $suspenseTransaction->amount;

        $custodialTransaction->bank_transaction_id = $suspenseTransaction->transaction_id;

        $custodialTransaction->save();

        $approval->approve();

        $suspenseTransaction->repo->setMatched();

        \Flash::success('The transactions have been matched successfully');

        $next = \redirect('/dashboard/portfolio/suspense_transactions/' . $suspenseTransaction->custodial_account_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $suspenseTransaction = SuspenseTransaction::findOrFail($data['suspense_transaction_id']);

        $custodialTransaction = CustodialTransaction::findOrFail($data['custodial_transaction_id']);

        return [
            'suspenseTransaction' => $suspenseTransaction,
            'custodialTransaction' => $custodialTransaction
        ];
    }
}
