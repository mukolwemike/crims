<?php
namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityHoldingType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class BuyEquityShare implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $security = PortfolioSecurity::findOrFail($data['portfolio_security_id']);

        $fund = null;
        if (isset($data['unit_fund_id'])) {
            $fund = UnitFund::findOrFail($data['unit_fund_id']);
        }

        $type = (new EquityHoldingType())->where('slug', 'purchase')->first();
        
        $holding = $security->repo->purchase($data, $type, $fund);

        $trans = $security->repo->credit($holding, $approval);

        $holding->custodialTransaction()->associate($trans);
        $holding->approval()->associate($approval);
        $holding->save();

        $approval->approve();

        \Flash::success('The new shares have been purchased');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $security = PortfolioSecurity::findOrFail($data['portfolio_security_id']);
        $fund = null;
        if (isset($data['unit_fund_id'])) {
            $fund = UnitFund::findOrFail($data['unit_fund_id']);
        }

        return [
            'security'=>$security,
            'fund' => $fund
        ];
    }
}
