<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 11:02 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundOrder;
use App\Cytonn\Portfolio\FundComplianceRepository;
use App\Cytonn\Portfolio\Orders\Compliance;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class CreatePortfolioOrder implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $orderType = PortfolioOrderType::findOrFail($data['order_type_id']);

        if (in_array($orderType->slug, ['buy', 'sell'])) {
            $this->process($data, $approval);
        } else {
            throw new \InvalidArgumentException("Portfolio order type not supported");
        }

        $approval->approve();

        \Flash::success('Portfolio order has been saved');

        $next = \redirect('/dashboard/portfolio/orders');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function process($data, PortfolioTransactionApproval $approval)
    {
        $portfolioOrder = $this->savePortfolioOrder($data, $approval);

        if (count($data['orders']) == 0) {
            throw new CRIMSGeneralException("Please add the portfolios to allocate to");
        }

        $this->addFundOrders($portfolioOrder, $data['orders'], $data)->toArray();

        return $portfolioOrder;
    }

    public function savePortfolioOrder($data, PortfolioTransactionApproval $approval)
    {
        $portfolioOrder = new PortfolioOrder();

        return $portfolioOrder->create([
            'security_id' => $data['security_id'],
            'order_type_id' => $data['order_type_id'],
            'cut_off_price' => (isset($data['cut_off_price'])) ? $data['cut_off_price']: null,
            'minimum_rate' => (isset($data['minimum_rate'])) ? $data['minimum_rate']: null,
            'approval_id' => $approval->id,
            'good_till_type_id' => $data['good_till'],
            'expiry_date' => isset($data['expiry_date']) ? $data['expiry_date'] : null,
        ]);
    }

    public function addFundOrders(PortfolioOrder $portfolioOrder, $fund_orders, $data)
    {
        return collect($fund_orders)->map(function ($order) use ($data, $portfolioOrder) {
            $fundOrder = new UnitFundOrder();

            return $fundOrder->create([
                'order_id' => $portfolioOrder->id,
                'unit_fund_id' => $order['id'],
                'shares' => (isset($data['cut_off_price'])) ? $order['value'] / $data['cut_off_price'] : null,
                'amount' => (!isset($data['cut_off_price'])) ? $order['value'] : null,
                'custodial_account_id' => isset($order['custodial_account_id']) ? $order['custodial_account_id'] : null
            ]);
        });
    }

    public function getFundOrders($data, PortfolioSecurity $security)
    {
        return collect($data['orders'])->map(function ($order) use ($security, $data) {
            $unitFund = UnitFund::findOrFail($order['id']);

            $number = (isset($data['cut_off_price'])) ? $order['value'] / $data['cut_off_price'] : $order['value'];

            $fundCompliance = new Compliance($unitFund, $security, $order['value']);

            return [
                'shares' => (isset($data['cut_off_price'])) ? $order['value'] / $data['cut_off_price'] : null,
                'amount' => (!isset($data['cut_off_price'])) ? $order['value'] : null,
                'status' => $fundCompliance->status(),
                'unitFund' => $unitFund,
                'account' => isset($order['custodial_account_id']) ?
                    CustodialAccount::find($order['custodial_account_id']) : null
            ];
        });
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $security = PortfolioSecurity::findOrFail($data['security_id']);

        $orderType = PortfolioOrderType::findOrFail($data['order_type_id']);

        $fund_orders = $this->getFundOrders($data, $security);

        return [
            'data' => $data,
            'security' => $security,
            'fund_orders' => $fund_orders,
            'orderType' => $orderType,
        ];
    }
}
