<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 10:55 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use \App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Actions\Reverse;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Portfolio\Commands\RollbackWithdrawalCommand;

class RollbackWithdrawal implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * RollbackWithdrawal constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $investment = \App\Cytonn\Models\Portfolio\DepositHolding::findOrFail($approval->payload['investment_id']);

        (new Reverse())->withdrawal($investment);

        $approval->approve();

        \Flash::success('The investment withdrawal rollback has been done');

        $next = \redirect('/dashboard/portfolio');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = \App\Cytonn\Models\Portfolio\DepositHolding::findOrFail($data['investment_id']);

        return['investment'=>$investment, 'data'=>$data];
    }
}
