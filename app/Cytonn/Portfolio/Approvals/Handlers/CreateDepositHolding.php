<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundType;
use App\Cytonn\Models\Portfolio\BondHolding;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\DepositType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SecurityInterestPaymentDate;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Authorization\Authorizer;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Portfolio\Actions\DepositInvestment;
use Cytonn\Portfolio\Actions\Investment;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateDepositHolding implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $input = $approval->payload;

        $portfolioSecurity = PortfolioSecurity::findOrFail($input['portfolio_security_id']);
        
        $input = array_add($input, 'fund_manager_id', $portfolioSecurity->fundManager->id);

        $input = array_add($input, 'approval_id', $approval->id);

        if (DepositType::find($input['deposit_type_id'])->slug == 'bond') {
            throw new CRIMSGeneralException("Please use orders to create bonds.");
        }

        (new DepositInvestment())->invest($input);

        $approval->approve();
    
        \Flash::success('Portfolio Deposits has been approved and invested');
    
        $fundType = $portfolioSecurity->subAssetClass;

        $investmentType = PortfolioInvestmentType::find($input['deposit_type_id']);
    
        $next = \view('portfolio.investmentsuccess', [
            'title' => 'Deposit Holding',
            'investment' => $approval->payload,
            'fundType' => $fundType,
            'investmentType' => $investmentType
        ]);
    
        $approval->raise(new ApprovalSuccessful($approval, $next));
    
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $custodialAccount = CustodialAccount::find($data['custodial_account_id']);
        
        $portfolioSecurity = PortfolioSecurity::find($data['portfolio_security_id']);

        $fund = null;

        if (isset($data['unit_fund_id'])) {
            $fund = UnitFund::findOrFail($data['unit_fund_id']);
        }

        $rate = Setting::where('key', 'withholding_tax_rate')->first();

        $tax = isset($data['tax_rate_id']) ? Setting::findOrFail($data['tax_rate_id']) : $rate;

        $def_type = DepositType::where('slug', 'deposit')->first();

        $investmentType = isset($data['deposit_type_id'])
            ? DepositType::findOrFail($data['deposit_type_id']) : $def_type;

        return [
            'data' => $data,
            'custodialAccount' => $custodialAccount,
            'fund' => $fund,
            'portfolioSecurity' => $portfolioSecurity,
            'investmentType' => $investmentType,
            'tax' => $tax
        ];
    }
}
