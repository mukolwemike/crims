<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\EquityPriceTrail;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateSecurityMarketPriceTrail implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $trail = (new EquityPriceTrail())->where('date', $data['date'])
            ->where('security_id', $data['security_id'])->first();

        is_null($trail) ?
            $trail = new EquityPriceTrail()
            : $security = (new PortfolioSecurity())->findOrFail($data['security_id']);
        ;

        $trail->fill($data);
        $trail->security_id = $data['security_id'];
        $trail->save();

        $approval->approve();

        \Flash::success('The security market price trail has been created successfully');

        $next = \redirect('/dashboard/portfolio/securities/' . $data['security_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $security = PortfolioSecurity::findOrFail($data['security_id']);

        return ['data' => $data, 'security' => $security];
    }
}
