<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 11:02 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundOrder;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class EditPortfolioOrder implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $portfolioOrder = PortfolioOrder::findOrFail($data['id']);

        $edited_fund_orders = $data['fund_orders'];

        unset($data['unit_fund_orders'], $data['fund_orders']);

        $portfolioOrder->update($data);

        $portfolioOrder->unitFundOrders->each(function (UnitFundOrder $order) {
            return $order->delete();
        });

        $this->editOrders($edited_fund_orders, $portfolioOrder);

        $approval->approve();

        \Flash::success('Portfolio order has been saved');

        $next = \redirect('/dashboard/portfolio/orders/' . $portfolioOrder->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function editOrders($orders, PortfolioOrder $portfolioOrder)
    {
        $edited_fund_orders = collect($orders)->map(function ($fund_order) {
            return array_intersect_key($fund_order, array_flip(['unit_fund_id', 'custodial_account_id', 'amount', 'shares']));
        });

        return $portfolioOrder->unitFundOrders()->createMany($edited_fund_orders->toArray());
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $portfolioOrder = PortfolioOrder::findOrFail($data['id']);

        $security = PortfolioSecurity::findOrFail($data['security_id']);

        $orderType = PortfolioOrderType::findOrFail($data['order_type_id']);

        $prev_fund_orders = $portfolioOrder->unitFundOrders;

        $fund_orders_now = $data['fund_orders'];

        $getCustodialAccountName = function ($id) {
            $account = CustodialAccount::find($id);

            return $account ? $account->account_name : '';
        };

        return [
            'data' => $data,
            'security' => $security,
            'fund_orders_now' => $fund_orders_now,
            'prev_fund_orders' => $prev_fund_orders,
            'orderType' => $orderType,
            'portfolioOrder' => $portfolioOrder,
            'getCustodialAccountName' => $getCustodialAccountName
        ];
    }
}
