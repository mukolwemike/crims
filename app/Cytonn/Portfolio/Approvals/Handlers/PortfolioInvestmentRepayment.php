<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 11:10 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioRepaymentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Portfolio\Actions\Repayment;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class PortfolioInvestmentRepayment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * InvestmentRepayment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return mixed
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        (new Repayment())->make($approval);

        $approval->approve();

        \Flash::success('The deposit repayment has been saved');

        $next = \redirect('/dashboard/portfolio/'.$data['deposit_id'].'/repay');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $deposit = DepositHolding::findOrFail($data['deposit_id']);

        $repaymentType = PortfolioRepaymentType::find($data['payment_type']);

        if ($data['payment_amount_type_id'] == 1) {
            $data['amount'] = $deposit->repo->getTotalValueOfAnInvestmentAsAtDate(Carbon::parse($data['date']));
        }

        return [
            'data'=>$data,
            'deposit'=>$deposit,
            'repaymentType'=>$repaymentType
        ];
    }
}
