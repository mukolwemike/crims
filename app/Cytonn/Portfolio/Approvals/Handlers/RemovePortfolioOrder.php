<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/22/18
 * Time: 8:25 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundOrder;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class RemovePortfolioOrder implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $portfolioOrder = PortfolioOrder::findOrFail($data['id']);

        $unitFundOrders = $portfolioOrder->unitFundOrders;

        $unitFundOrders->each(function (UnitFundOrder $unitFundOrder) {
            $unitFundOrder->delete();
        });

        $portfolioOrder->delete();

        $approval->approve();

        \Flash::success('Portfolio order has been removed succesfully');

        $next = \redirect('/dashboard/portfolio/orders');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $portfolioOrder = PortfolioOrder::findOrFail($data['id']);

        $security = $portfolioOrder->security;

        $fund_orders = $portfolioOrder->unitFundOrders;

        return [
            'data' => $data,
            'portfolioOrder' => $portfolioOrder,
            'security' => $security,
            'fund_orders' => $fund_orders,
        ];
    }
}
