<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategyHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateBoughtActiveStrategyShares implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * UpdateBoughtActiveStrategyShares constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $data = $approval->payload;

        $holding = ActiveStrategyHolding::findOrFail($data['holding_id']);
        $holding->update(array_only($data, ['date', 'number', 'cost']));

        $approval->approve();

        \Flash::success('The active strategy share holding has been updated');

        $next = \redirect('/dashboard/portfolio/activestrategy/'.$holding->security_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = ActiveStrategyHolding::findOrFail($data['holding_id']);
        return['holding'=>$holding];
    }
}
