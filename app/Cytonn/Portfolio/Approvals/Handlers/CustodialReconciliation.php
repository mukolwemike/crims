<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CustodialReconciliation implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    /**
     * @var Authorizer
     */
    protected $authorizer;

    /**
     * CustodialReconciliation constructor
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $trans = new CustodialTransaction();
        $trans->custodial_account_id = $data['custodial_id'];
        $trans->type = CustodialTransactionType::where('name', 'RR')->first()->id;
        if ($data['type'] == 'credit') {
            $amount = -abs($data['amount']);
        } elseif ($data['type'] == 'debit') {
            $amount = abs($data['amount']);
        }
        $trans->amount = $amount;
        $trans->date = $data['date'];
        $trans->description = CustodialTransactionType::where('name', 'RR')
                ->first()->description . ' : ' . $data['narrative'];
        $trans->approval_id = $approval->id;
        $trans->save();

        $approval->approve();

        \Flash::success('Reconciliation has been made');
        $next = \redirect('/dashboard/portfolio/custodials/details/' . $data['custodial_id']);
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $account = CustodialAccount::find($data['custodial_id']);

        return ['account' => $account];
    }
}
