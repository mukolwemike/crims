<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 9:41 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundType;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Investment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * Investment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $input = $approval->payload;
        $input = array_add($input, 'fund_manager_id', null);
        $input = array_add($input, 'approval_id', $approval->id);

        (new \Cytonn\Portfolio\Actions\Investment())->invest($input);

        $approval->approve();

        \Flash::success('Portfolio Investment has been approved and invested');

        $fundType = FundType::find($input['fund_type_id']);
        $investmentType = PortfolioInvestmentType::find($input['type_id']);

        $next = \view(
            'portfolio.investmentsuccess',
            ['title' => 'Investment Invested', 'investment' => $approval->payload, 'fundType' => $fundType,
                'investmentType' => $investmentType
            ]
        );

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fundType = FundType::find($data['fund_type_id']);
        $investmentType = function ($id) {
            return PortfolioInvestmentType::find($id);
        };
        $custodialAccount = CustodialAccount::find($data['custodial_account_id']);
        $investor = PortfolioInvestor::find($data['portfolio_investor_id']);

        $investorsLists = PortfolioInvestor::all()->lists('name', 'id');
        $fundTypeLists = FundType::all()->lists('name', 'id');
        $custodialAccountsLists = CustodialAccount::all()->lists('account_name', 'id');

        $fundManager = $approval->fundManager;

        return [
            'fundType' => $fundType, 'investmentType' => $investmentType, 'custodialAccount' => $custodialAccount,
            'investor' => $investor, 'investorsLists' => $investorsLists, 'fundTypeLists' => $fundTypeLists,
            'custodialAccountsLists' => $custodialAccountsLists, 'fundManager' => $fundManager
        ];
    }
}
