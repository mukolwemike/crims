<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 12:10 PM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Models\Portfolio\EquityHoldingType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Custodial\Transact\ClientTransaction;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Cytonn\Unitization\Trading\Buy;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class ReceiveEquityShareDividend implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }


    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $date = Carbon::parse($data['date']);

        $dividend = EquityDividend::findOrFail($data['dividend_id']);

        $security = PortfolioSecurity::findOrFail($data['security_id']);

        $dividend->date = $date;

        $dividend->received = true;

        $dividend->save();

        switch ($dividend->dividend_type) {
            case 'cash':
                $this->debit($dividend, $approval);
                break;

            case 'shares':
                $holding = $security->repo->purchaseFromDividend($dividend, $approval);

                $dividend->equityHolding()->associate($holding);
                $dividend->save();
                break;

            default:
                new ClientInvestmentException('Dividend type is not supported!');
        }

        \Queue::push(static::class.'@disburseShares', [ 'dividend' => $dividend ]);

        $approval->approve();

        \Flash::success('The equity share dividend has been received');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $dividend_types = ['cash'=>'Cash', 'shares'=>'Shares'];

        $methods_of_calculation = ['ratio'=>'Ratio', 'conversion'=>'Conversion Price'];

        $data = $approval->payload;

        $dividend = EquityDividend::findOrFail($data['dividend_id']);

        $security = PortfolioSecurity::findOrFail($data['security_id']);

        $shares_at_closure = $this->sharesAtClosure($security, Carbon::parse($dividend->book_closure_date));

        $unitFund = $dividend->unitFund;

        return [
            'dividend_types'=>$dividend_types,
            'shares_at_closure'=> $shares_at_closure,
            'security'=>$security,
            'methods_of_calculation'=> $methods_of_calculation,
            'fund' => $unitFund,
            'dividend' => $dividend
        ];
    }

    private function sharesAtClosure(PortfolioSecurity $security, Carbon $date)
    {
        return $security->repo->numberOfSharesHeld($date);
    }

    public function debit(EquityDividend $dividend, $approval)
    {
        $amount = $this->calculateDividend($dividend);

        $fund = $dividend->unitFund;

        $trans = PortfolioTransaction::build(
            $fund->custodialAccount,
            'I',
            $dividend->date,
            'Received cash dividend: '.$dividend->security->name. ' Fund: '.$fund->name,
            $amount,
            $approval,
            $dividend->security->investor,
            null
        )->debit();

        $dividend->custodialTransaction()->associate($trans);

        $dividend->save();

        return $dividend;
    }

    public function disburseShares($job, $data)
    {
        $dividendArr = $data['dividend'];

        $dividend = EquityDividend::findOrFail($dividendArr['id']);

        $valueOfDividend = $this->calculateDividend($dividend);

        $clients = $dividend->unitFund->repo->fundClients($dividend->book_closure_date);

        $date = Carbon::parse($dividend->book_closure_date);

        $fundUnits = $dividend->unitFund->repo->getUnits($date->copy());

        $clients->each(function ($client) use ($dividend, $fundUnits, $valueOfDividend, $date) {
            $clientUnits = $client->repo->ownedNumberOfUnits($dividend->unitFund, $date);

            $valueOfInvestment = ($clientUnits / $fundUnits) * $valueOfDividend;

            $this->purchaseUnits($client, $dividend, $valueOfInvestment);
        });

        $job->delete();
    }

    public function purchaseUnits(Client $client, EquityDividend $dividend, $valueOfInvestment)
    {
        $recipient = $client->getLatestFA();

        $date = Carbon::parse($dividend->book_closure_date);

        (new Buy(
            $client,
            $recipient,
            $valueOfInvestment,
            $date,
            null,
            true,
            $dividend->unitFund,
            true,
            $dividend
        ))->purchase();
    }

    public function calculateDividend(EquityDividend $dividend)
    {
        $date = Carbon::parse($dividend->date);

        if ($dividend->dividend_type == 'cash') {
            return ($dividend->number * $dividend->dividend);
        }

        $price = $dividend->security->latestPrice($date)->price;

        return $dividend->shares * $price;
    }
}
