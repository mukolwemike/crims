<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Actions\EquityReverse;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class ReverseBuyEquityShare implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equityHolding = EquityHolding::findOrFail($data['id']);

        (new EquityReverse())->purchase($equityHolding, $data);

        $approval->approve();

        \Flash::success('The purchased shares have been reversed');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equityHolding = EquityHolding::find($data['id']);

        if (is_null($equityHolding)) {
            $equityHolding = EquityHolding::where('id', $data['id'])->whereNotNull('deleted_at')->first();
        }

        $security = $equityHolding->security;

        return [
            'equityHolding' => $equityHolding,
            'security' => $security
        ];
    }
}
