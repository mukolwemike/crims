<?php
/**
 * Date: 03/11/2016
 * Time: 19:23
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class CustodialAccountBankBalance implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $account = CustodialAccount::findOrFail($data['account_id']);

        $account->balanceTrail()->create(
            [
                'approval_id' => $approval->id,
                'amount' => $data['amount'],
                'date' => $data['date']
            ]
        );

        $approval->approve();

        \Flash::success('Account balance has been updated');

        $next = \redirect('dashboard/portfolio/custodials/details/' . $data['account_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param PortfolioTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval, array $vars = null)
    {
        return ['account' => CustodialAccount::findOrFail($approval->payload['account_id'])];
    }
}
