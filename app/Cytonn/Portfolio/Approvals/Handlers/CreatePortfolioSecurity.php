<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\Sector;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Cytonn\Portfolio\PortfolioSecurityRepository;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreatePortfolioSecurity implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $data['code'] = (new PortfolioSecurityRepository())->suggestCode();

        $data['contact_person'] = (isset($data['contact_person'])) ? $data['contact_person'] : null;

        $data['issuer'] = (isset($data['issuer'])) ? $data['issuer'] : null;

        $data['investor_account_name'] =
            (isset($data['investor_account_name'])) ? $data['investor_account_name'] : null;

        $data['investor_account_number'] =
            (isset($data['investor_account_number'])) ? $data['investor_account_number'] : null;

        $branch = (isset($data['investor_bank'])) ? ClientBankBranch::find($data['investor_bank_branch']) : null;

        $input = array_except($data, ['_token', 'files']);

        if (!isset($data['portfolio_investor_id'])) {
            $investor = PortfolioInvestor::create([
                    'name' => $data['name'],
                    'account_name' => $data['investor_account_name'],
                    'account_number' => $data['investor_account_number'],
                    'bank_name' => $branch ? $branch->bank->name : null,
                    'branch_name' => $branch ? $branch->name : null
                ]);

            $input['portfolio_investor_id'] = $investor->id;
        }

        $security = new PortfolioSecurity();

        unset($input['investor_bank']);
        unset($input['investor_bank_branch']);
        unset($input['investor_account_name']);
        unset($input['investor_account_number']);

        $security->fill($input);

        $security->save();

        $security->update(['portfolio_investor_id' => $investor->id]);

        $approval->approve();

        \Flash::success('Portfolio Security saved succesfully');

        $next = \Redirect::to('/dashboard/portfolio/securities/');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $subAssetClass = SubAssetClass::findOrFail($data['sub_asset_class_id']);

        $depositType =
            (isset($data['deposit_type_id'])) ? PortfolioInvestmentType::findOrFail($data['deposit_type_id']) : null;

        $fundManager = FundManager::findOrFail($data['fund_manager_id']);

        $sector = isset($data['sector_id']) ? Sector::findOrFail($data['sector_id']) : null;

        $country = isset($data['country_id']) ? Country::findOrFail($data['country_id']) : null;

        $currency = isset($data['currency_id']) ? Currency::findOrFail($data['currency_id']) : null;

        $data['code'] = (new PortfolioSecurityRepository())->suggestCode();

        $data['contact_person'] = (isset($data['contact_person'])) ? $data['contact_person'] : null;

        $data['issuer'] = (isset($data['issuer'])) ? $data['issuer'] : null;

        $data['investor_account_name'] =
            (isset($data['investor_account_name'])) ? $data['investor_account_name'] : null;

        $data['investor_account_number'] =
            (isset($data['investor_account_number'])) ? $data['investor_account_number'] : null;

        $branch = (isset($data['investor_bank'])) ? ClientBankBranch::find($data['investor_bank_branch']) : null;

        return [
            'data' => $data,
            'depositType' => $depositType,
            'subAssetClass' => $subAssetClass,
            'fundManager' => $fundManager,
            'sector' => $sector,
            'country' => $country,
            'currency' => $currency,
            'branch' => $branch
        ];
    }
}
