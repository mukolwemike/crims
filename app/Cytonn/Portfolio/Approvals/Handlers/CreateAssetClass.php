<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 5:27 PM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class CreateAssetClass implements ApprovalHandlerInterface
{
    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files']);

        $asset = new AssetClass();
        $asset->fill($input);
        $asset->save();

        $approval->approve();

        \Flash::success('Asset class has been succesfully saved');

        $next = \Redirect::to('/dashboard/portfolio/asset-classes/');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        return ['data'=>$data];
    }
}
