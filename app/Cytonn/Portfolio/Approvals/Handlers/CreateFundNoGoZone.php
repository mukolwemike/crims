<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 09/06/2018
 * Time: 15:18
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\FundLiquidityLimit;
use App\Cytonn\Models\Portfolio\FundNoGoZone;
use App\Cytonn\Models\Portfolio\PortfolioAllocationLimit;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class CreateFundNoGoZone implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $limit = new FundNoGoZone();

        $limit->fill($data);

        $limit->save();

        $approval->approve();

        \Flash::success('portfolio liquid limit added successfully');

        $next = \redirect('/dashboard/portfolio/compliance');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $unitFund = UnitFund::findorFail($data['fund_id']);

        $security = PortfolioSecurity::findOrFail($data['security_id']);

        return [
            'data' =>$data,
            'unitFund' => $unitFund,
            'security' => $security
        ];
    }
}
