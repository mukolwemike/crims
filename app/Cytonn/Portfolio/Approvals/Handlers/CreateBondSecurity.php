<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\BondHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SecurityInterestPaymentDate;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateBondSecurity implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $input = array_except($data, ['interest_payment_dates','_token', 'files']);

        $holding = new BondHolding();

        $holding->fill($input);
        $holding->save();

        collect($data['interest_payment_dates'])->each(
            function ($date) use ($holding) {
                $date['bond_holding_id'] = $holding->id;

                $paymentDates = new SecurityInterestPaymentDate();
                $paymentDates->fill($date);
                $paymentDates->save();
            }
        );

        $approval->approve();

        \Flash::success('The security has been created');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        
        $portoflioSecurity = PortfolioSecurity::find($data['portfolio_security_id']);
        
        $data['asset_class'] = ($portoflioSecurity->subAssetClass)->assetClass->name;
        $data['investor'] =PortfolioInvestor::find($portoflioSecurity->portfolio_investor_id)->name;
        $data['portfolio_security'] = $portoflioSecurity->name;

        return ['data'=> $data];
    }
}
