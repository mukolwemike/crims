<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 10:12 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Investment\Tax\GenerateWithholdingTax;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\CustodialWithdrawType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\WithholdingTax;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CustodialPayment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $trans = null;

        if (!isset($data['exempted'])) {
            $trans = $this->recordTransaction($data);
        }

        $whts = isset($data['selectedWHT']) ? $data['selectedWHT'] : null;

        if ($whts) {
            \Queue::push(static::class . '@updateWHT', [
                'approval_id' => $approval->id,
                'custodial_transaction_id' => $trans ? $trans->id : null
            ]);
        }

        $trans->update([
            'approval_id' => $approval->id
        ]);

        $approval->approve();

        $suspenseTransaction = $approval->suspenseTransaction;

        if ($suspenseTransaction) {
            $suspenseTransaction->repo->setMatched();
        }

        $route = 'dashboard/portfolio/custodials';

        if ($trans) {
            $route = $route . '/details/' . $trans->custodial_account_id;
        }

        $next = \redirect($route);

        \Flash::success('Payment has been made');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function recordTransaction($data)
    {
        $trans = new CustodialTransaction();

        $trans->custodial_account_id = $data['custodial_id'];

        $trans->type = CustodialTransactionType::where(
            'name',
            CustodialWithdrawType::find($data['transaction'])->code
        )->first()->id;

        $trans->amount = -abs($data['amount']);

        $trans->date = $data['date'];

        $trans->description = CustodialWithdrawType::find($data['transaction'])->name . ': ' . $data['narrative'];

        $trans->bank_transaction_id = isset($data['bank_transaction_id']) ? $data['bank_transaction_id'] : null;

        $trans->bank_amount = isset($data['bank_amount']) ? $data['bank_amount'] : null;

        $trans->save();

        return $trans;
    }

    public function updateWHT($job, $data)
    {
        $approval = PortfolioTransactionApproval::find($data['approval_id']);

        $transaction = !is_null($data['custodial_transaction_id'])
            ? CustodialTransaction::find($data['custodial_transaction_id'])
            : null;

        $data = $approval->payload;

        $taxes = WithholdingTax::whereIn('id', $data['selectedWHT'])->get();

        $taxes->each(function (WithholdingTax $wht) use ($transaction) {

            $wht->paid = true;

            $wht->custodial_transaction_id = $transaction ? $transaction->id : null;

            $wht->save();
        });

        $job->delete();
    }

    public function action($approval, $action, $param)
    {
        switch ($action) {
            case 'export':
                return $this->export($approval);
                break;

            default:
                throw new \InvalidArgumentException("Action $action not supported in " . static::class);
        }
    }

    public function export($approval)
    {
        \Queue::push(
            static::class . '@send',
            ['approval_id' => $approval->id,]
        );

        \Flash::message('Report will be sent to your email shortly');

        return \redirect()->back();
    }

    public function send($job, $data)
    {
        $approval = PortfolioTransactionApproval::find($data['approval_id']);

        $data = $approval->payload;

        $taxIds = $data['selectedWHT'];

        $user = \Auth::user();

        (new GenerateWithholdingTax($user, null, null, null, $taxIds))->send();

        $job->delete();
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $whts = isset($data['selectedWHT']) ? $data['selectedWHT'] : null;

        $withdrawType = CustodialWithdrawType::find($data['transaction']);

        $account = CustodialAccount::find($data['custodial_id']);

        return [
            'account' => $account,
            'withdrawType' => $withdrawType,
            'whts' => $whts,
            'approval' => $approval
        ];
    }
}
