<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 12:10 PM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategyHoldingType;
use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Models\Portfolio\EquityHoldingType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class EquitySharesDividend implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }


    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        switch ($data['dividend_type']) {
            case 'cash':
                $this->cash($data, $approval);

                break;

            case 'shares':
                $this->scrip($data);

                break;

            default:
                new ClientInvestmentException('Dividend type is not supported!');
        }

        $approval->approve();

        \Flash::success('The new dividend has been saved');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    private function cash(array $data, $approval)
    {
        $security =(new  PortfolioSecurity())->findOrFail($data['security_id']);

        $rate = $this->taxRate();

        $after_tax = (1 - ($rate->value/100)) * $data['dividend'];

        $dividend =  (new EquityDividend())->create([
                'security_id' => $data['security_id'],
                'dividend' => $after_tax,
                'shares' => 0,
                'number' => $this->sharesAtClosure($security, Carbon::parse($data['book_closure_date'])),
                'prevailing_tax_rate_id' => $rate->id,
                'conversion_price' => 0,
                'date' => $data['date'],
                'book_closure_date' => $data['book_closure_date'],
                'dividend_type' => $data['dividend_type'],
                'unit_fund_id' => (isset($data['unit_fund_id']))? $data['unit_fund_id'] : null
        ]);

        return $dividend;
    }

    private function scrip(array $data)
    {
        $security = (new PortfolioSecurity())->findOrFail($data['security_id']);

        $rate = $this->taxRate();

        $shares_at_closure = $this->sharesAtClosure($security, Carbon::parse($data['book_closure_date']));

        if (isset($data['method_of_calculation']) and $data['method_of_calculation'] == 'ratio') {
            $shares = $this->calculateFromRatio($shares_at_closure, $data);
        } elseif (isset($data['method_of_calculation']) and $data['method_of_calculation'] == 'conversion') {
            $shares = $this->calculateFromDividendAndConversionPrice($rate, $data, $shares_at_closure);
        } else {
            new ClientInvestmentException('Method not supported');
        }

        $equityDividend = new EquityDividend();
    
        $equityDividend->fill(
            [
                'security_id' => $data['security_id'],
                'dividend' => 0,
                'shares' => $shares,
                'number' => $shares_at_closure,
                'prevailing_tax_rate_id' => $rate->id,
                'conversion_price' => isset($data['conversion_price']) ? $data['conversion_price'] : null,
                'date' => $data['date'],
                'book_closure_date' => $data['book_closure_date'],
                'dividend_type' => $data['dividend_type'],
                'unit_fund_id' => (isset($data['unit_fund_id']))? $data['unit_fund_id'] : null
            ]
        );

        return $equityDividend->save();
    }

    private function sharesAtClosure(PortfolioSecurity $security, Carbon $date)
    {
        return $security->repo->numberOfSharesHeld($date);
    }

    private function taxRate()
    {
        return Setting::where('key', 'dividend_tax_rate')->latest()->first();
    }

    private function ifIsset($array, $key)
    {
        if (isset($array[$key])) {
            return $array[$key];
        }

        return null;
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $dividend_types = ['cash'=>'Cash', 'shares'=>'Shares'];

        $methods_of_calculation = ['ratio'=>'Ration', 'conversion'=>'Conversion Price'];

        $data = $approval->payload;

        $security = (new PortfolioSecurity())->findOrFail($data['security_id']);

        $shares_at_closure = $this->sharesAtClosure($security, Carbon::parse($data['book_closure_date']));

        $security = function ($id) {
            return (new PortfolioSecurity())->findOrFail($id);
        };

        $unitFund = (isset($data['unit_fund_id']))
            ? UnitFund::findOrFail($data['unit_fund_id'])
            : null;

        return [
            'dividend_types'=>$dividend_types,
            'shares_at_closure'=> $shares_at_closure,
            'security'=>$security,
            'methods_of_calculation'=> $methods_of_calculation,
            'fund' => $unitFund
        ];
    }

    private function calculateFromDividendAndConversionPrice($rate, $data, $shares_at_closure)
    {
        $amount = $shares_at_closure * $data['dividend'];

        $tax = (1 - ($rate->value/100)) * $amount;

        $net_amount = $amount - $tax;

        return  floor($net_amount / $data['conversion_price']);
    }

    private function calculateFromRatio($shares_at_closure, $data)
    {
        return ($shares_at_closure / $data['old_shares']) * $data['new_shares'];
    }
}
