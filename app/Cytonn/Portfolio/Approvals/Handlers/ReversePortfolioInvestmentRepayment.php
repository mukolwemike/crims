<?php
/**
 * Date: 13/01/2017
 * Time: 17:55
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Models\Portfolio\DepositHoldingRepayments;
use Cytonn\Portfolio\Actions\Reverse;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use App\Cytonn\Models\PortfolioInvestmentRepayment;

class ReversePortfolioInvestmentRepayment implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $actor = (new Reverse());

        $repayment_id = $approval->payload['repayment_id'];

        $repayment = PortfolioInvestmentRepayment::find($repayment_id);

        $actor->repayment($repayment);

        $next = \redirect("/dashboard/portfolio/$repayment->investment_id/repay");

        \Flash::message('The repayment has been reversed successfully');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $repayment = PortfolioInvestmentRepayment::withTrashed()->find($approval->payload['repayment_id']);

        $portfolioInvestor = $repayment->investment->security;

        return ['repayment'=>$repayment, 'portfolioInvestor'=>$portfolioInvestor];
    }
}
