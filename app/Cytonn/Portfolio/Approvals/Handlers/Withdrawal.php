<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 11:30 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Actions\Withdraw;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Cytonn\Portfolio\Forms\WithdrawForm;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Withdrawal implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * Withdrawal constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @throws ClientInvestmentException
     * @throws \Laracasts\Validation\FormValidationException
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $form = \App::make(WithdrawForm::class);

        $deposit = DepositHolding::findOrFail($approval->payload['deposit_id']);

        $input = $approval->payload;
        $input = array_add($input, 'approval_id', $approval->id);

        if ($deposit->withdrawn == 1) {
            throw new ClientInvestmentException('This deposit has already been withdrawn');
        }

        $withdrawal = new Withdraw();


        if ($input['premature'] == 'true' || $input['premature'] === true) {
            $form->premature();

            $validation_data = $approval->payload;
            $validation_data = array_add($validation_data, 'invested_date', $deposit->invested_date);
            $validation_data = array_add($validation_data, 'maturity_date', $deposit->maturity_date);

            if ($input['partial'] == 'true' || $input['partial'] === true) {
                $form->partial();

                if ((float)$input['amount'] > $deposit->repo->getTotalValueOfAnInvestment()) {
                    throw new ClientInvestmentException(
                        'The amount redeemed is more than the deposit current value'
                    );
                }

                $form->validate($validation_data);

                $withdrawal->partial($deposit, \Auth::user(), $input);
            } else {
                $form->validate($validation_data);

                $withdrawal->premature($deposit, \Auth::user(), $input);
            }
        } else {
            unset($input['withdraw_date']);

            $form->validate($approval->payload);

            $withdrawal->mature($deposit, \Auth::user(), $input);
        }

        \Flash::success('The withdrawal has been successful');

        $approval->approve();

        $next = \view(
            'portfolio.withdrawsuccess',
            ['title' => 'Investment Withdrawn', 'deposit' => $deposit, 'withdrawal' => $withdrawal]
        );

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        if (!isset($data['deposit_id']) && isset($data['investment_id'])) {
            $data['deposit_id'] = $data['investment_id'];

            $approval->payload = $data;

            $approval->save();
        }

        $deposit = DepositHolding::findOrFail($data['deposit_id']);

        return ['deposit' => $deposit, 'data' => $data];
    }
}
