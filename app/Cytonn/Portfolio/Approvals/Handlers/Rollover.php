<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/19/16
 * Time: 2:46 PM
 */

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Forms\RolloverForm;
use Cytonn\Core\DataStructures\Carbon;

class Rollover implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * Rollover constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }


    /**
     * @param PortfolioTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $form = \App::make(RolloverForm::class);

        $input = $approval->payload;
        $input = array_add($input, 'approval_id', $approval->id);

        $oldInvestment = DepositHolding::findOrFail($approval->payload['deposit']);

        if ($oldInvestment->withdrawn == 1) {
            throw new ClientInvestmentException(
                'This deposit has already been withdrawn, you can only rollover active deposits'
            );
        }

        $validation_data = $approval->payload;
        $validation_data = array_add(
            $validation_data,
            'old_deposit_maturity_date',
            (new Carbon($oldInvestment->maturity_date))->subDay()
        );

        if ($oldInvestment->repo->getTotalValueOfAnInvestment() + 0.1 < (float) + $approval->payload['amount']) {
            throw new ClientInvestmentException(
                'You can only rollover an amount less than the value of an deposit today'
            );
        }

        $form->validate($validation_data);

        $rollover = new \Cytonn\Portfolio\Actions\Rollover();

        $rollover->single($oldInvestment, \Auth::user(), $input);

        \Flash::success('The rollover has been approved');

        $approval->approve();

        $next = \view(
            'portfolio.rolloversuccess',
            ['title'=>'Investment Rollover', 'deposit'=>$oldInvestment,
                'rollover'=>$rollover, 'depositRollover' => $approval->payload]
        );

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $deposit = DepositHolding::findOrFail($data['deposit']);
        $total = $deposit->repo->getTotalValueOfAnInvestment();
        $remaining = $total - $data['amount'];
        return['deposit'=>$deposit, 'total'=>$total, 'remaining'=>$remaining, 'data'=>$data];
    }
}
