<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 11:54 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class SellActiveStrategyShares implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * SellActiveStrategyShares constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $data = $approval->payload;

        //TODO check if there's transaction cost
        $sale = new ActiveStrategyShareSale();
        $sale->number = $data['number'];
        $sale->sale_price = $data['price'];
        $sale->date = $data['date'];
        $sale->security_id = $data['security_id'];
        $sale->save();

        $approval->approve();

        \Flash::success('The new dividend has been saved');

        $next = \redirect('/dashboard/portfolio/activestrategy');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }


    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $activeStrategySecurity = function ($id) {
            return ActiveStrategySecurity::findOrFail($id);
        };

        return [
            'activeStrategySecurity'=>$activeStrategySecurity
        ];
    }
}
