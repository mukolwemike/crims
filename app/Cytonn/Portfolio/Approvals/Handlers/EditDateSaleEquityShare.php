<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 12/10/2018
 * Time: 10:09
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Portfolio\Actions\EquityEdit;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;

class EditDateSaleEquityShare implements ApprovalHandlerInterface
{
    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equitySale = EquityShareSale::findOrFail($data['id']);

        (new EquityEdit())->sale($equitySale, $data);

        $approval->approve();

        \Flash::success('The purchased shares date edit have been reversed');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $equitySale = EquityShareSale::find($data['id']);

        $security = $equitySale->security;

        return [
            'equitySale' => $equitySale,
            'security' => $security,
            'data' => $data
        ];
    }
}
