<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 11:46 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategyHoldingType;
use App\Cytonn\Models\ActiveStrategySecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class BuyActiveStrategyShares implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * BuyActiveStrategyShares constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $data = $approval->payload;

        $security = ActiveStrategySecurity::findOrFail($data['security_id']);

        $type = ActiveStrategyHoldingType::where('slug', 'purchase')->first();

        $security->repo->purchase($data['cost'], Carbon::parse($data['date']), $data['number'], $type);

        $approval->approve();

        \Flash::success('The new shares have been purchased');

        $next = \redirect('/dashboard/portfolio/activestrategy');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $activeStrategySecurity = ActiveStrategySecurity::findOrFail($data['security_id']);
        return [
            'activeStrategySecurity'=>$activeStrategySecurity
        ];
    }
}
