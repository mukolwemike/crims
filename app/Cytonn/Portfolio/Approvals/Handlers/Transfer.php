<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 10:31 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Commands\CustodialTransferCommand;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Transfer implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * Transfer constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $this->execute(new CustodialTransferCommand($approval), ['approval' => $approval]);

        $approval->approve();

        \Flash::success('The transfer has been successfully executed');

        $approval->matchedSuspenseTransactions->each(function ($suspenseTransaction) {
            $suspenseTransaction->repo->setMatched();
        });

//        $next = \redirect('/dashboard/portfolio/custodials/details/' . $data['source_account']);
        $next = \redirect('/dashboard/portfolio/approve');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $sender = CustodialAccount::find($data['source_account']);

        $recipient = CustodialAccount::find($data['destination_id']);

        return [
            'sender' => $sender,
            'recipient' => $recipient,
            'data' => $data
        ];
    }
}
