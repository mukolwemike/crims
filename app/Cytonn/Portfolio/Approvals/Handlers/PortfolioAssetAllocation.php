<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 09/06/2018
 * Time: 15:18
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\PortfolioAllocationLimit;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class PortfolioAssetAllocation implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $unitFund = UnitFund::findorFail($data['fund_id']);

        $subAssetClass =SubAssetClass::findOrFail($data['sub_asset_class_id']);

        $allocation = new PortfolioAllocationLimit();

        $allocation->fill($data);
        
        $allocation->save();

        $approval->approve();

        \Flash::success('portfolio asset allocation added successfully');

        $next = \redirect('/dashboard/portfolio/asset');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $unitFund = UnitFund::findorFail($data['fund_id']);

        $subAssetClass = SubAssetClass::findOrFail($data['sub_asset_class_id']);

        return [
            'data' =>$data,
            'unitFund' => $unitFund,
            'subAssetClass' => $subAssetClass
        ];
    }
}
