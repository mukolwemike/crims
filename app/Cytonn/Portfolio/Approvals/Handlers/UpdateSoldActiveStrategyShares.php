<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateSoldActiveStrategyShares implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * SellActiveStrategyShares constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addportfolioinvestment');

        $data = $approval->payload;

        $share_sale = ActiveStrategyShareSale::findOrFail($data['share_sale_id']);
        $share_sale->update(array_only($data, ['number', 'sale_price', 'date']));

        $approval->approve();

        \Flash::success('The active strategy share sale has been updated');

        $next = \redirect('/dashboard/portfolio/activestrategy/'.$share_sale->security_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }


    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $share_sale = ActiveStrategyShareSale::findOrFail($data['share_sale_id']);

        return['share_sale'=>$share_sale];
    }
}
