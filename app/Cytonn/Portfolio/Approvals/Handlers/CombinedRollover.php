<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 10:00 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Actions\Rollover;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Portfolio\Forms\CombinedRolloverForm;

class CombinedRollover implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * CombinedRollover constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $form = \App::make(CombinedRolloverForm::class);

        $data = $approval->payload;

        $oldInvestment  = DepositHolding::findOrFail($data['investment']);

        $validation_data = $data;

        //allow same day
        $validation_data = array_add(
            $validation_data,
            'old_investment_maturity_date',
            (new Carbon($oldInvestment->maturity_date))->subDay()->toDateString()
        );
        $validation_data = array_add(
            $validation_data,
            'old_investment_amount',
            (float) $oldInvestment->repo->getTotalValueOfAnInvestmentAsAtDate($oldInvestment->maturity_date)
        );

        $form->validate($validation_data);

        $rollover = new Rollover();

        $data['approval_id'] = $approval->id;

        $rollover->combined($oldInvestment, \Auth::user(), $data);

        $approval->approve();

        \Flash::success('The investments have been combined and rolled over');

        $next = \redirect('/dashboard/portfolio');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $investment = DepositHolding::findOrFail($data['investment']);

        $investments = new \Illuminate\Support\Collection();

        foreach (json_decode($data['investments']) as $inv_id) {
            $investmt = DepositHolding::find($inv_id);
            $investmt->value = $investmt->repo
                    ->getTotalValueOfAnInvestmentAsAtDate($investmt->maturity_date) + $investmt->withdraw_amount;
            $investments->push($investmt);
        }
        return ['investment'=>$investment, 'investments'=>$investments, 'data'=>$data];
    }
}
