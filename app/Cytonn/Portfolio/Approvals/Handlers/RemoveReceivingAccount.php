<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class RemoveReceivingAccount implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $account = CustodialAccount::findOrFail($data['custodial_id']);

        $fund = UnitFund::findOrFail($data['fund_id']);
        $fund->receivingAccounts()->detach($account);

        \Flash::success("Unit Fund is linked to receiving account successfully");

        $next = \Redirect::to("/dashboard/portfolio/unit-fund/details/" . $data['fund_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $account = CustodialAccount::findOrFail($data['custodial_id']);

        $fund = UnitFund::findOrFail($data['fund_id']);

        return [
            'data' => $data,
            'account' => $account,
            'fund' => $fund,
        ];
    }
}
