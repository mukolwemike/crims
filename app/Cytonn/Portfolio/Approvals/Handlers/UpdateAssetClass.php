<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/31/17
 * Time: 5:27 PM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\AssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class UpdateAssetClass implements ApprovalHandlerInterface
{
    /**
     * @param PortfolioTransactionApproval $approval
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $asset = AssetClass::find($data['asset_class_id']);

        $asset->name = $data['name'];
        $asset->slug = $data['slug'];
        $asset->save();

        $approval->approve();

        \Flash::success('Asset class has been succesfully updated');

        $next = \Redirect::to('/dashboard/portfolio/asset-classes/');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param PortfolioTransactionApproval $approval
     * @return array
     */
    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        return ['data' => $data];
    }
}
