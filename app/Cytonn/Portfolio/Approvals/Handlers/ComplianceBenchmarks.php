<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/9/18
 * Time: 9:00 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\FundCompliance;
use App\Cytonn\Models\Portfolio\FundComplianceBenchmark;
use App\Cytonn\Models\Portfolio\FundComplianceType;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class ComplianceBenchmarks implements ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $benchmark_data = array_except($data, ['limits']);

        $benchmark = $this->saveBenchmark($benchmark_data);

        $benchmark->benchmarkLimits()->createMany($data['limits']);

        $approval->approve();

        \Flash::success('The compliance benchmark has been added successfully');

        $next = \redirect('/dashboard/portfolio/compliance');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function saveBenchmark($benchmark_data)
    {
        $benchmark = new FundComplianceBenchmark();

        $benchmark->fill($benchmark_data);

        $benchmark->save();

        return $benchmark;
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $limits = collect($data['limits'])->map(function ($limit) {
            if ($limit) {
                return [
                    'lower' => $limit['warn'],
                    'upper' => $limit['limit'],
                    'sub_class' => SubAssetClass::find($limit['sub_asset_class_id'])
                ];
            }
        });

        $complianceType = isset($data['compliance_type_id'])
            ? FundComplianceType::findOrFail($data['compliance_type_id'])
            : null;

        return [
            'data' => $data,
            'limits' => $limits,
            'complianceType' => $complianceType
        ];
    }
}
