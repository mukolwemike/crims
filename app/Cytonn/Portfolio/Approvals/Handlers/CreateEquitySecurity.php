<?php

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateEquitySecurity implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $input = array_except($data, ['interest_payment_dates','_token', 'files']);

        //TODO Equity holding calculations
        dd($input);
        
        $holding = new EquityHolding();

        $holding->fill($input);
        $holding->save();

        $approval->approve();

        \Flash::success('The security has been created succesfully');

        $next = \redirect('/dashboard/portfolio/securities');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;
        $portfolioSecurity = PortfolioSecurity::find($data['portfolio_security_id']);
        
        $data['portfolio_security'] = $portfolioSecurity->name;
        $data['asset_class'] = $portfolioSecurity->subAssetClass->assetClass->name;
        $data['sub_asset_class'] = $portfolioSecurity->subAssetClass->name;
        $data['investor'] = $portfolioSecurity->investor->name;
        
        return ['data'=> $data];
    }
}
