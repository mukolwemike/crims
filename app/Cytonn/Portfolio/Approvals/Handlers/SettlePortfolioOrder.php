<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 11:02 AM
 */

namespace Cytonn\Portfolio\Approvals\Handlers;

use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioOrderAllocation;
use App\Cytonn\Models\Portfolio\PortfolioOrderType;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Portfolio\Orders\Allocate;
use App\Cytonn\Portfolio\Orders\Compliance;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Approvals\ApprovalHandlerInterface;
use Cytonn\Portfolio\Events\ApprovalSuccessful;

class SettlePortfolioOrder implements ApprovalHandlerInterface
{
    /**
     * @param PortfolioTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['allocations']);

        $portfolioOrder = PortfolioOrder::findOrFail($data['order_id']);

        $allocation = PortfolioOrderAllocation::findOrFail($input['id']);

        (new Allocate($allocation, $approval))->allocate();

        $allocation->status = 4;

        $allocation->save();

        $approval->approve();

        \Flash::success('Portfolio order allocation has been settled');

        $next = \redirect('/dashboard/portfolio/orders/'.$portfolioOrder->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function validateAllocation($data, PortfolioTransactionApproval $approval)
    {
        $security = PortfolioOrderAllocation::findOrFail($data['id'])->order->security;

        return $this->getFundOrders($data, $security, $approval)
            ->reduce(function ($verdict, $order) use ($security, $data) {
                if ($order['status']) {
                    return $verdict && false;
                }

                return $verdict && true;
            }, true);
    }

    public function getFundOrders($data, PortfolioSecurity $security, PortfolioTransactionApproval $approval)
    {
        $allocation = PortfolioOrderAllocation::findOrFail($data['id']);

        $order = $allocation->order;

        $fundOrders = $order->unitFundOrders;

        return $fundOrders->map(function ($fundOrder) use ($security, $allocation, $order, $approval) {
            $unitFund = UnitFund::findOrFail($fundOrder->unit_fund_id);

            $value = ($fundOrder->shares) ? $order->cut_off_price * $fundOrder->shares : $fundOrder->amount;

            $fundCompliance = new Compliance($unitFund, $security, $value);

            $allocation = (isset($fundOrder->shares))
                ? (new Allocate($allocation, $approval))->setPurchaseData($fundOrder)
                : (new Allocate($allocation, $approval))->setDepositData($fundOrder);

            return [
                'shares' => (isset($fundOrder->shares)) ? $fundOrder->shares : null,
                'amount' => (isset($fundOrder->amount)) ? $fundOrder->amount : null,
                'status' => $fundCompliance->status(),
                'unitFund' => $unitFund,
                'allocated' => (isset($fundOrder->shares)) ? $allocation['number']: $allocation['amount'],
                'value' => isset($fundOrder->shares) ? $allocation['cost'] * $allocation['number'] : null,
                'cost_price' => (isset($fundOrder->shares)) ? $allocation['cost'] : null,
                'fee_per_share' => (isset($fundOrder->shares)) ? $allocation['fee_per_share'] : null,
            ];
        });
    }

    public function setFees($fees, PortfolioOrderAllocation $allocation)
    {
        return collect($fees)->map(function ($fee) use ($allocation) {
            return [
                'amount' => $this->calculateAmount($fee, $allocation),
                'description' => $fee['description'],
                'percentage' => $fee['percentage']
            ];
        });
    }

    public function calculateAmount($fee, PortfolioOrderAllocation $allocation)
    {
        if (isset($fee['percentage'])) {
            return (floatval($fee['percentage']) / 100) * ($allocation->shares * $allocation->price);
        }

        return $fee['amount'];
    }

    public function prepareView(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $portfolioOrder = PortfolioOrder::findOrFail($data['order_id']);

        $security = $portfolioOrder->security;

        $allocation = PortfolioOrderAllocation::findOrFail($data['id']);

        $fundOrders = $this->getFundOrders($data, $security, $approval);

        $fees = (isset($data['fees'])) ? $this->setFees($data['fees'], $allocation) : null;

        $limits = $this->getFundOrders($data, $security, $approval)->pluck('status')->flatten();

        $asset_class = $portfolioOrder->security->subAssetClass->assetClass->name;

        return [
            'data' => $data,
            'allocation' => $allocation,
            'portfolioOrder' => $portfolioOrder,
            'fees' => $fees,
            'limits' => $limits,
            'fundOrders' => $fundOrders,
            'asset_class' => $asset_class
        ];
    }

    public function onReject(PortfolioTransactionApproval $approval, $reason)
    {
        $data = $approval->payload;

        $allocation = PortfolioOrderAllocation::findOrFail($data['id']);

        $allocation->status = 1;

        $allocation->reject_reason = $reason;

        $allocation->save();
    }
}
