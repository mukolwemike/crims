<?php

namespace Cytonn\Portfolio\Approvals;

use App\Cytonn\Models\PortfolioTransactionApproval;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 8:54 AM
 */
interface ApprovalHandlerInterface
{
    public function handle(PortfolioTransactionApproval $approval);

    public function prepareView(PortfolioTransactionApproval $approval);
}
