<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/11/18
 * Time: 8:38 AM
 */

namespace App\Cytonn\Portfolio;

use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\Unitization\UnitFund;

class FundComplianceRepository
{
    private $subAssetClass;

    private $unitFund;

    public function __construct(SubAssetClass $subAssetClass, UnitFund $unitFund)
    {
        $this->unitFund = $unitFund;

        $this->subAssetClass = $subAssetClass;
    }

    public function benchmark()
    {
        $this->limit();
    }

    public function limit()
    {
        return $this->unitFund->benchmarkLimit($this->subAssetClass);
    }

    public function aum()
    {
        $performance = $this->unitFund->performances->last();

        return $performance->aum;
    }

    public function limitAmount()
    {
        if ($this->limit()) {
            return abs($this->limit()->limit / 100 * $this->aum());
        }

        return abs($this->aum());
    }

    public function warnAmount()
    {
        if ($this->limit()) {
            return abs($this->limit()->warn / 100 * $this->aum());
        }

        return abs($this->aum());
    }
}
