<?php
namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\DepositType;
use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Custodial\Transact\DepositTransaction;
use Cytonn\Portfolio\Bonds\InterestPayments\ScheduleGenerator;
use Illuminate\Support\Arr;

class DepositInvestment extends Deposits
{
    /**
     * @param array $data
     * @return $this
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function invest(array $data)
    {
        $data = $this->fillMissingArrayIndex($data, ['description', 'contact_person', 'deposit_type_id']);

        $input = Arr::only($data, DepositHolding::getTableColumnsAsArray());

        $input = $this->fillDepositHoldingDefaults($input, $data);

        $depositHolding = $this->createInvestment($input);

        $custodialAccount = CustodialAccount::findOrFail($data['custodial_account_id']);

        $trans = $this->credit($depositHolding, $custodialAccount);

        $this->custodial = $trans;

        $this->investment = $depositHolding;

        if ($depositHolding->depositType->slug == 'bond' && $depositHolding->coupon_payment_duration > 0) {
            \Queue::push(static::class.'@generateStatement', ['holding_id' => $depositHolding->id]);
        }

        return $this;
    }

    private function fillDepositHoldingDefaults(array $input, array $data)
    {
        $input['on_call'] = isset($data['on_call']) ? boolval($data['on_call']) : null;
        $input['coupon_payment_duration'] = isset($data['coupon_payment_duration'])
            ? $data['coupon_payment_duration'] : 0;

        $input['bond_purchase_cost'] = isset($data['purchase_cost']) ? $data['purchase_cost'] : $data['amount'];
        $input['bond_purchase_date'] = isset($data['purchase_date']) ? $data['purchase_date'] : $data['invested_date'];
        $input['coupon_rate'] = isset($data['coupon_rate']) ? $data['coupon_rate'] : $data['interest_rate'];

        $input['type_id'] = isset($data['type_id']) ? $data['type_id'] : PortfolioInvestmentType::first()->id;
        $input['invest_transaction_approval_id'] = $data['approval_id'];


        return $input;
    }

    /**
     * @param $job
     * @param $holding
     */
    public function generateStatement($job, $holding)
    {
        $holding = DepositHolding::findOrFail($holding['holding_id']);

        (new ScheduleGenerator())->generate($holding);

        $job->delete();
    }

    private function credit(DepositHolding $depositHolding, CustodialAccount $account)
    {

        $cost = $depositHolding->bond_purchase_cost > 0 ? $depositHolding->bond_purchase_cost : $depositHolding->amount;
        $date = $depositHolding->bond_purchase_date
            ? $depositHolding->bond_purchase_date : $depositHolding->invested_date;

        $trans = DepositTransaction::build(
            $account,
            'I',
            $date,
            'New deposit holding to: '.$depositHolding->security->investor->name,
            $cost,
            $depositHolding->investApproval,
            $depositHolding->security->investor,
            null
        )->credit();
    
        $depositHolding->investTransaction()->associate($trans);
        $depositHolding->save();

        return $trans;
    }
}
