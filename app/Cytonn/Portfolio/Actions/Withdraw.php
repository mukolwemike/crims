<?php
/**
 * Date: 8/17/15
 * Time: 4:50 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Custodial\Transact\PortfolioTransaction;

class Withdraw extends Base
{
    public $newInvestment;

    public function mature(DepositHolding $deposit, User $user, $data)
    {
        $amount = $deposit->repo->getInvestmentTotal($deposit->maturity_date);

        $deposit = $this->withdraw(
            $deposit,
            $amount,
            $deposit->maturity_date,
            $user,
            PortfolioTransactionApproval::find($data['approval_id'])
        );

        $trans = $this->debit(
            $deposit,
            $amount,
            $deposit->maturity_date,
            'Portfolio deposit withdrawn on maturity: '.$deposit->security->investor->name,
            PortfolioTransactionApproval::find($data['approval_id'])
        );

        $this->deposit = $deposit;
        $this->custodial = $trans;

        return $this;
    }

    public function premature(DepositHolding $deposit, User $user, $data)
    {
        $amount = $deposit->repo->getInvestmentTotal($data['end_date']);
        $date = Carbon::parse($data['end_date']);
        $approval = PortfolioTransactionApproval::find($data['approval_id']);

        $deposit = $this->withdraw(
            $deposit,
            $amount,
            $date,
            $user,
            $approval
        );

        $trans = $this->debit(
            $deposit,
            $amount,
            $date,
            'Portfolio deposit withdrawn prematurely: '.$deposit->security->investor->name,
            $approval
        );

        $this->deposit = $deposit;
        $this->custodial = $trans;

        return $this;
    }

    public function partial(DepositHolding $deposit, User $user, $data)
    {
        dd("Disabled, please make a full repayment");
        
        $date = Carbon::parse($data['end_date']);
        $approval = PortfolioTransactionApproval::find($data['approval_id']);

        $deposit = $this->withdraw(
            $deposit,
            $data['amount'],
            $date,
            $user,
            $approval,
            true
        );

        $trans = $this->debit(
            $deposit,
            $data['amount'],
            $date,
            'Portfolio deposit withdrawn partially & prematurely: '.$deposit->security->investor->name,
            $approval
        );

        $final_amount =  $deposit->repo->getInvestmentTotal($data['end_date']);

        $newInvestment = $this->createInvestment(
            Carbon::parse($data['end_date'])->addDay(),
            $deposit->maturity_date,
            $deposit->interest_rate,
            $final_amount - abs($data['amount']),
            $approval,
            $deposit->taxable,
            $deposit->on_call,
            null,
            null,
            null,
            $deposit->security
        );

        $newInvestment->investTransaction()->associate($trans);
        $newInvestment->parent()->associate($deposit);
        $newInvestment->save();

        $deposit->child()->associate($newInvestment);
        $deposit->save();

        $this->deposit = $deposit;
        $this->custodial = $trans;
        $this->newInvestment = $newInvestment;

        return $this;
    }

    private function debit(
        DepositHolding $deposit,
        $amount,
        \DateTime $date,
        $description,
        PortfolioTransactionApproval $approval = null
    ) {
        $trans = PortfolioTransaction::build(
            $deposit->investTransaction->custodialAccount,
            'R',
            $date,
            $description,
            $amount,
            $approval,
            $deposit->security->investor,
            null
        )->debit();

        $deposit->withdrawTransaction()->associate($trans);
        $deposit->save();

        return $trans;
    }
}
