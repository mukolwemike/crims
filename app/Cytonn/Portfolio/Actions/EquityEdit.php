<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 12/10/2018
 * Time: 09:45
 */

namespace App\Cytonn\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\EquityHolding;
use Carbon\Carbon;
use App\Cytonn\Models\Portfolio\EquityShareSale;

class EquityEdit
{
    /*
     * Perform the edit of a share purchase
     */
    public function purchase(EquityHolding $equityHolding, $data)
    {
        $equityHolding->update([
            'date' => Carbon::parse($data['trade_date']),
            'settlement_date' => Carbon::parse($data['settlement_date']),
        ]);
    }

    /*
     * Perform the edit of a shares sale
     */
    public function sale(EquityShareSale $equityShareSale, $data)
    {
        $equityShareSale->update([
            'date' => Carbon::parse($data['trade_date'])
        ]);
    }
}
