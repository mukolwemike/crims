<?php
/**
 * Date: 07/12/2016
 * Time: 10:01
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioRepaymentAction;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\PortfolioRepaymentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Portfolio\Portfolio;
use Cytonn\Workflow\Hooks\ClosingPeriods;

/**
 * Class Base
 *
 * @package Cytonn\Portfolio\Actions
 */
abstract class Base extends Portfolio
{
    //Actions
    const TYPE_ROLLOVER = 2;
    const TYPE_WITHDRAWAL = 1;

    //Types
    const TYPE_FULL_REPAYMENT = 'full_repayment';

    protected function checkClosingPeriod($date, $approval)
    {
        $is_closed = (new ClosingPeriods())->periodIsClosed($date, $approval);

        if ($is_closed) {
            throw new CRIMSGeneralException("Could not complete transaction, period is closed");
        }
    }

    /**
     * @param \DateTime $invested_date
     * @param \DateTime $maturity_date
     * @param $interest_rate
     * @param $amount
     * @param PortfolioTransactionApproval $approval
     * @param bool $taxable
     * @param bool $on_call
     * @param null $contact_person
     * @param null $description
     * @param null $portfolioInvestmentType
     * @param PortfolioSecurity|null $security
     * @param UnitFund|null $fund
     * @return DepositHolding
     * @throws CRIMSGeneralException
     */
    protected function createInvestment(
        \DateTime $invested_date,
        \DateTime $maturity_date,
        $interest_rate,
        $amount,
        PortfolioTransactionApproval $approval,
        $taxable = true,
        $on_call = false,
        $contact_person = null,
        $description = null,
        $portfolioInvestmentType = null,
        PortfolioSecurity $security = null,
        UnitFund $fund = null
    ) {
        $this->checkClosingPeriod($invested_date, $approval);

        //record the new deposit
        $deposit = new DepositHolding();
        $deposit->portfolio_security_id = $security->id;
        $deposit->invested_date = $invested_date;
        $deposit->maturity_date = $maturity_date;
        $deposit->interest_rate = $interest_rate;
        $deposit->amount = $amount;
        $deposit->invest_transaction_approval_id = $approval->id;
        $deposit->taxable = $taxable;
        $deposit->on_call = $on_call;
        $deposit->contact_person = $contact_person;
        $deposit->description = $description;
        $deposit->type_id = is_null($portfolioInvestmentType) ? null : $portfolioInvestmentType->id;
        $deposit->unit_fund_id = $fund ? $fund->id : null;
        $deposit->save();


        return $deposit;
    }

    /**
     * @param DepositHolding $deposit
     * @param $amount
     * @param \DateTime $date
     * @param User $user
     * @param PortfolioTransactionApproval $approval
     * @param bool $partial
     * @return DepositHolding
     * @throws CRIMSGeneralException
     */
    protected function withdraw(
        DepositHolding $deposit,
        $amount,
        \DateTime $date,
        User $user,
        PortfolioTransactionApproval $approval,
        $partial = false
    ) {
        $this->checkClosingPeriod($date, $approval);

        $deposit->update(
            [
                'withdraw_amount' => $amount,
                'withdrawal_date' => $date,
                'withdrawn' => true,
                'withdrawn_by' => $user->id,
                'withdraw_transaction_approval_id' => $approval->id,
                'partial_withdraw' => $partial
            ]
        );

        return $deposit;
    }

    protected function rollover(
        DepositHolding $deposit,
        $withdrawAmount,
        \DateTime $date,
        User $user,
        PortfolioTransactionApproval $approval
    ) {
        $this->checkClosingPeriod($date, $approval);

        $deposit = $this->withdraw($deposit, $withdrawAmount, $date, $user, $approval);

        $deposit->update([
            'rolled' => true
        ]);

        return $deposit;
    }

    public function reverseWithdrawal(DepositHolding $deposit)
    {
        $deposit->update([
            'withdraw_amount' => null,
            'withdrawal_date' => null,
            'withdrawn' => null,
            'withdrawn_by' => null,
            'withdraw_transaction_approval_id' => null,
            'custodial_withdraw_transaction_id' => null,
            'partial_withdraw' => null,
            'rolled' => null
        ]);

        return $deposit;
    }

    protected function fillMissingArrayIndex(array $data, array $keys)
    {
        foreach ($keys as $key) {
            if (!isset($data[$key])) {
                $data[$key] = null;
            }
        }

        return $data;
    }

    protected function recordRollover(
        DepositHolding $depositHolding,
        Carbon $date,
        $rolloverAmount,
        $withdrawAmount,
        PortfolioTransactionApproval $approval
    ) {
        $rolloverWithdrawal = $this->createRepayment(
            $depositHolding,
            $date,
            $rolloverAmount,
            $approval,
            static::TYPE_FULL_REPAYMENT,
            static::TYPE_ROLLOVER,
            "Rollover"
        );

        $withdrawnWithdrawal = $this->createRepayment(
            $depositHolding,
            $date,
            $withdrawAmount,
            $approval,
            static::TYPE_FULL_REPAYMENT,
            static::TYPE_WITHDRAWAL,
            "Repayment"
        );

        $depositHolding->update([
            'rolled' => true
        ]);

        return (object) [
            'withdrawal' => $withdrawnWithdrawal,
            'rollover' => $rolloverWithdrawal
        ];
    }

    protected function createRepayment(
        DepositHolding $depositHolding,
        Carbon $date,
        $amount,
        PortfolioTransactionApproval $approval,
        $type,
        $actionType,
        $narration
    ) {
        $this->checkClosingPeriod($date, $approval);

        $type = PortfolioRepaymentType::where('slug', $type)->first();

        $action = PortfolioRepaymentAction::findOrFail($actionType);

        return PortfolioInvestmentRepayment::create([
            'investment_id' => $depositHolding->id,
            'date' => $date,
            'narrative' => $narration,
            'amount' => $amount,
            'type_id' => $type->id,
            'approval_id' => $approval->id,
            'portfolio_repayment_action_id' => $action->id
        ]);
    }
}
