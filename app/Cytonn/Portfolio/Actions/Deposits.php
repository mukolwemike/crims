<?php

namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\DepositType;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Portfolio\Portfolio;
use Cytonn\Workflow\Hooks\ClosingPeriods;
use DateTime;

abstract class Deposits extends Portfolio
{
    protected function checkClosingPeriod($date, $approval)
    {
        $is_closed = (new ClosingPeriods())->periodIsClosed($date, $approval);

        if ($is_closed) {
            throw new CRIMSGeneralException("Could not complete transaction, period is closed");
        }
    }

    /**
     * @param array $data
     * @return DepositHolding|\Illuminate\Database\Eloquent\Model
     * @throws CRIMSGeneralException
     */
    protected function createInvestment(array $data)
    {
        $value_date = Carbon::parse($data['invested_date']);
        $approval = PortfolioTransactionApproval::find($data['invest_transaction_approval_id']);

        $this->checkClosingPeriod($value_date, $approval);

        $investment = DepositHolding::create($data);

        $investment->save();
        
        return $investment;
    }

    protected function withdraw(
        DepositHolding $investment,
        $amount,
        DateTime $date,
        User $user,
        PortfolioTransactionApproval $approval,
        $partial = false
    ) {
        $this->checkClosingPeriod($date, $approval);

        $investment->update(
            [
            'withdraw_amount' => $amount,
            'withdrawal_date' => $date,
            'withdrawn' => true,
            'withdrawn_by' => $user->id,
            'withdraw_transaction_approval_id' => $approval->id,
            'partial_withdraw' => $partial
            ]
        );

        return $investment;
    }

    protected function rollover(
        DepositHolding $investment,
        $withdrawAmount,
        DateTime $date,
        User $user,
        PortfolioTransactionApproval $approval
    ) {
        $this->checkClosingPeriod($date, $approval);

        $investment = $this->withdraw($investment, $withdrawAmount, $date, $user, $approval);

        $investment->update(
            [
            'rolled' => true
            ]
        );

        return $investment;
    }

    public function reverseWithdrawal(DepositHolding $investment)
    {
        $investment->update(
            [
            'withdraw_amount' => null,
            'withdrawal_date' => null,
            'withdrawn' => null,
            'withdrawn_by' => null,
            'withdraw_transaction_approval_id' => null,
            'custodial_withdraw_transaction_id' => null,
            'partial_withdraw' => null,
            'rolled'=>null
            ]
        );

        return $investment;
    }
    
    protected function fillMissingArrayIndex(array $data, array $keys)
    {
        foreach ($keys as $key) {
            if (!isset($data[$key])) {
                $data[$key] = null;
            }
        }

        return $data;
    }
}
