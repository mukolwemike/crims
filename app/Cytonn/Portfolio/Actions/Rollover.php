<?php
/**
 * Date: 8/18/15
 * Time: 6:04 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioInvestmentTopup;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenMade;
use App\Exceptions\CrimsException;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Presenters\AmountPresenter;

/**
 * Class PortfolioRollover
 *
 * @package Cytonn\Portfolio
 */
class Rollover extends Base
{
    public $oldInvestment;

    public $combined = false;

    public $rolledInvestments;

    public $amountWithdrawn;
    
    public $deposit;


    /**
     * @param DepositHolding $deposit
     * @param User           $user
     * @param $data
     * @return $this
     * @throws \Exception
     */
    public function single(DepositHolding $deposit, User $user, $data)
    {
        $approval = PortfolioTransactionApproval::find($data['approval_id']);

        $maturityDate = Carbon::parse($deposit->maturity_date);

        $finalValueOfOldInvestment = $deposit->repo->getTotalValueOfAnInvestmentAsAtDate($maturityDate);

        $amountWithdrawn = $finalValueOfOldInvestment - $data['amount'];

        $diff = $data['amount'] > $finalValueOfOldInvestment;

        if ($data['amount'] > $finalValueOfOldInvestment && abs($diff) > 1) {
            throw new ClientInvestmentException("The rollover amount indicated exceeds amount available 
            for rollover " . $finalValueOfOldInvestment . ' vs ' . $data['amount']);
        }

        $depositsCol = collect([$deposit]);

        $this->updateMaturityDates($depositsCol, $maturityDate);

        $data = $this->fillMissingArrayIndex($data, ['description', 'contact_person']);

        $newInvestment = $this->createInvestment(
            Carbon::parse($data['invested_date']),
            Carbon::parse($data['maturity_date']),
            $data['interest_rate'],
            $data['amount'],
            $approval,
            $data['taxable'],
            $deposit->on_call,
            null,
            $data['description'],
            null,
            $deposit->security,
            $deposit->unitFund
        );

        $repayments = $this->recordRepayments($depositsCol, $amountWithdrawn, $approval);

        $this->recordReinvestments($newInvestment, $repayments);

        $this->validateRolloverSuccessful($depositsCol, $repayments, $newInvestment);

        $newInvestment->parent()->associate($deposit);
        $custodialTransaction = $repayments[0]->withdrawal->custodialTransaction;
        $newInvestment->custodial_invest_transaction_id = $custodialTransaction->id;
        $newInvestment->save();

        $deposit->child()->associate($newInvestment);
        $deposit->save();

        $this->deposit = $newInvestment;
        $this->custodial = $custodialTransaction;

        return $this;
    }

    /**
     * @param DepositHolding $oldInvestment
     * @param User $user
     * @param array $data
     * @return $this
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function combined(DepositHolding $oldInvestment, User $user, array $data)
    {
        $deposits = DepositHolding::whereIn('id', json_decode($data['investments']))->get();

        $date = Carbon::parse($oldInvestment->maturity_date);

        $this->updateMaturityDates($deposits, $date);

        $deposits->map(function (DepositHolding $inv) {
            $inv->value = $inv->repo->getTotalValueOfAnInvestmentAsAtDate($inv->maturity_date);
        });

        switch ($data['reinvest']) {
            case 'reinvest':
                $amountToWithdraw = $deposits->sum('value') - $data['amount'];
                $amountToReinvest = $data['amount'];
                $custodial_amt = - abs($amountToWithdraw);
                break;
            case 'withdraw':
                $amountToWithdraw = $data['amount'];
                $amountToReinvest = $deposits->sum('value') - $data['amount'];
                $custodial_amt = - abs($amountToWithdraw);
                break;
            case 'topup':
                $amountToWithdraw = 0;
                $amountToReinvest = $deposits->sum('value') + $data['amount'];
                $custodial_amt =  abs($data['amount']);
                break;
            default:
                throw new \InvalidArgumentException('Type not found');
        }

        $approval = PortfolioTransactionApproval::find($data['approval_id']);

        $data = $this->fillMissingArrayIndex($data, ['description', 'contact_person']);

        $newInvestment = $this->createInvestment(
            Carbon::parse($data['invested_date']),
            Carbon::parse($data['maturity_date']),
            $data['interest_rate'],
            $amountToReinvest,
            $approval,
            $data['taxable'],
            null,
            null,
            $data['description'],
            null,
            $oldInvestment->security,
            $oldInvestment->unitFund
        );

        $investTransaction = null;

        if ($custodial_amt > 0) {
            $topup = $this->recordPortfolioTopup($custodial_amt, $date, $newInvestment, $oldInvestment, $approval);

            $investTransaction = $topup->custodialTransaction;
        }

        $repayments = $this->recordRepayments($deposits, $amountToWithdraw, $approval);

        $investTransaction = $investTransaction ? $investTransaction : $repayments[0]->withdrawal->custodialTransaction;

        $this->recordReinvestments($newInvestment, $repayments);

        $this->validateRolloverSuccessful($deposits, $repayments, $newInvestment);

        $newInvestment->parent()->associate($oldInvestment);

        $newInvestment->custodial_invest_transaction_id = $investTransaction->id;

        $newInvestment->save();

        $this->oldInvestment = $oldInvestment;
        $this->client = $newInvestment;
        $this->custodial = $investTransaction;
        $this->combined = true;
        $this->rolledInvestments = $deposits;
        $this->amountWithdrawn = $amountToWithdraw;

        return $this;
    }

    private function debit(
        DepositHolding $deposit,
        $amount,
        \DateTime $date,
        $description,
        PortfolioTransactionApproval $approval
    ) {
        $trans = PortfolioTransaction::build(
            $deposit->custodialAccount(),
            'R',
            $date,
            $description,
            $amount,
            $approval,
            $deposit->security->investor
        )->debit();

        $deposit->withdrawTransaction()->associate($trans);
        $deposit->save();

        return $trans;
    }

    private function credit(
        DepositHolding $deposit,
        $amount,
        \DateTime $date,
        $description,
        PortfolioTransactionApproval $approval
    ) {
        $trans = PortfolioTransaction::build(
            $deposit->custodialAccount(),
            'I',
            $date,
            $description,
            $amount,
            $approval,
            $deposit->security->investor,
            null
        )->credit();

        $deposit->withdrawTransaction()->associate($trans);
        $deposit->save();

        return $trans;
    }

    private function updateMaturityDates($depositHoldings, Carbon $date)
    {
        $depositHoldings->each(function (DepositHolding $depositHolding) use ($date) {
            if ($depositHolding->withdrawn) {
                throw new ClientInvestmentException("Investment of principal "
                    . AmountPresenter::currency($depositHolding->amount) . " is withdrawn");
            }

            if ($depositHolding->maturity_date->lt($date)) {
                throw new ClientInvestmentException(
                    "The combine date must be before or on maturity of all investments"
                );
            }

            if ($depositHolding->maturity_date->startOfDay()->ne($date->copy()->startOfDay())) {
                $depositHolding->update(['maturity_date' => $date]);
            }
        });
    }

    private function recordRepayments(
        $depositHoldings,
        $withdrawAmount,
        PortfolioTransactionApproval $approval
    ) {
        $remainingAmount = $withdrawAmount;

        return $depositHoldings->map(function (DepositHolding $depositHolding) use (
            &$remainingAmount,
            $approval
        ) {
            $depositHolding = $depositHolding->fresh();

            $value = $depositHolding->calculate($depositHolding->maturity_date)->total();

            $amount = $value < $remainingAmount ? $value : $remainingAmount;

            $r = $this->recordRollover(
                $depositHolding,
                Carbon::parse($depositHolding->maturity_date),
                $value - $amount,
                $amount,
                $approval
            );

            event(new PortfolioRepaymentHasBeenMade($depositHolding, $r->rollover));

            event(new PortfolioRepaymentHasBeenMade($depositHolding, $r->withdrawal));

            $trans = $this->debit(
                $depositHolding,
                $amount,
                $depositHolding->maturity_date,
                'Portfolio deposit rolled over: ' . $depositHolding->security->investor->name,
                $approval
            );

            $withdrawalRepayment = $r->withdrawal;
            $withdrawalRepayment->custodial_transaction_id = $trans->id;
            $withdrawalRepayment->save();

            $remainingAmount = $remainingAmount - $amount;

            return $r;
        });
    }

    private function recordReinvestments(DepositHolding $depositHolding, $repayments)
    {
        $repayments->each(function ($repayment) use ($depositHolding) {
            $rolloverRepayment = $repayment->rollover;

            $rolloverRepayment->update(['reinvested_to' => $depositHolding->id]);
        });
    }

    private function validateRolloverSuccessful($depositHoldings, $repayments, DepositHolding $newDepositHolding)
    {
        $depositHoldings->each(function (DepositHolding $depositHolding) use ($newDepositHolding) {
            $depositHolding = $depositHolding->fresh();

            $laterRepayments = $depositHolding->repayments()
                ->where('date', '>', $depositHolding->withdrawal_date)
                ->exists();

            if ($laterRepayments) {
                throw new ClientInvestmentException("The investment has repayments in future, cannot rollover");
            }

            $value = $depositHolding->calculate($depositHolding->withdrawal_date, true)->total();

            if (abs($value) > 1) {
                throw new \LogicException(
                    "An investment should have value 0 after rollover ".
                    "(Inv ID: $depositHolding->id Bal: " . AmountPresenter::currency($value) . " )"
                );
            }

            if (Carbon::parse($depositHolding->maturity_date) > Carbon::parse($newDepositHolding->invested_date)) {
                throw new ClientInvestmentException("The old investment should not have its maturity date "
                    . "after the new investment date");
            }
        });

        $rolledAmount = $repayments->sum(function ($repayment) {
            return $repayment->rollover->amount;
        });

        if (abs($rolledAmount - $newDepositHolding->amount) > 1) {
            throw new \LogicException("The amount rolled over does not match what was deducted from " .
                "investments - $rolledAmount & $newDepositHolding->amount");
        }
    }

    private function recordPortfolioTopup(
        $amount,
        Carbon $date,
        DepositHolding $newDepositHolding,
        DepositHolding $oldDepositHolding,
        PortfolioTransactionApproval $approval
    ) {
        $this->validateTopup($oldDepositHolding, $date);

        $transaction = $this->credit(
            $oldDepositHolding,
            $amount,
            $oldDepositHolding->maturity_date,
            'An existing portfolio deposit topped up: ' . $oldDepositHolding->security->investor->name,
            $approval
        );

        return PortfolioInvestmentTopup::create([
            'amount' => $amount,
            'date' => $date,
            'topup_to' => $oldDepositHolding->id,
            'topup_from' => $newDepositHolding->id,
            'custodial_transaction_id' => $transaction->id,
            'description' => 'Topup',
            'approval_id' => $approval->id
        ]);
    }

    private function validateTopup(DepositHolding $depositHolding, Carbon $topupDate)
    {
        if ($topupDate > Carbon::parse($depositHolding->maturity_date)) {
            throw new ClientInvestmentException(
                "Topup failed, the topup date should not be after the investment maturity date"
            );
        }
    }
}
