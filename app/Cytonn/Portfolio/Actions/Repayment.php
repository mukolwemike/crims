<?php
/**
 * Date: 08/12/2016
 * Time: 10:33
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenMade;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Cytonn\Exceptions\ClientInvestmentException;

/**
 * Class Repayment
 * @package Cytonn\Portfolio\Actions
 */
class Repayment extends Base
{
    /**
     * @param PortfolioTransactionApproval $approval
     * @return PortfolioInvestmentRepayment|\Illuminate\Database\Eloquent\Model
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function make(PortfolioTransactionApproval $approval)
    {
        $data = $approval->payload;

        $date = Carbon::parse($data['date']);

        $this->checkClosingPeriod($date, $approval);

        $deposit = DepositHolding::findOrFail($data['deposit_id']);

        if ($data['payment_amount_type_id'] == 1) {
            $data['amount'] = $deposit->repo->getTotalValueOfAnInvestmentAsAtDate($date);
        }

        $data['approval_id'] = $approval->id;

        $repayment = $this->createNewRepayment($data);

        $this->debit($repayment, $approval);

        event(new PortfolioRepaymentHasBeenMade($deposit, $repayment));

        return $repayment;
    }

    /**
     * @param $data
     * @return PortfolioInvestmentRepayment|\Illuminate\Database\Eloquent\Model
     */
    private function createNewRepayment($data)
    {
        return PortfolioInvestmentRepayment::create([
            'amount' => $data['amount'],
            'date' => $data['date'],
            'narrative' => $data['narrative'],
            'investment_id' => $data['deposit_id'],
            'type_id' => $data['payment_type'],
            'approval_id' => $data['approval_id'],
            'portfolio_repayment_action_id' => 1
        ]);
    }

    /**
     * @param PortfolioInvestmentRepayment $repayment
     * @param PortfolioTransactionApproval|null $approval
     * @return \App\Cytonn\Models\CustodialTransaction
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    private function debit(PortfolioInvestmentRepayment $repayment, PortfolioTransactionApproval $approval = null)
    {
        $trans = PortfolioTransaction::build(
            $this->getCustodialAccount($repayment),
            'R',
            $repayment->date,
            'Repayment of portfolio investment from '.$repayment->investment->security->investor->name,
            $repayment->amount,
            $approval,
            $repayment->investment->security->investor,
            null
        )->debit();

        $repayment->custodialTransaction()->associate($trans);
        $repayment->save();

        return $trans;
    }

    /*
     * Get the custodial account account
     */
    /**
     * @param $repayment
     * @return mixed
     */
    public function getCustodialAccount($repayment)
    {
        $transaction = $repayment->investment->investTransaction;

        if ($transaction) {
            return $transaction->custodialAccount;
        }

        $transaction = $repayment->investment->investTransaction()->withTrashed()->first();

        if ($transaction) {
            return $transaction->custodialAccount;
        }

        return $repayment->investment->security->fundManager->custodialAccounts()->latest()->first();
    }
}
