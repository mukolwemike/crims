<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityShareSale;

class EquityReverse
{
    /*
     * Perform the reversal of a share purchase
     */
    public function purchase(EquityHolding $equityHolding, $data)
    {
        $equityHolding->update([
            'reverse_reason' => $data['reason']
        ]);

        $equityHolding->delete();
    }

    /*
     * Perform reversal of a shares sale
     */
    public function sale(EquityShareSale $equityShareSale, $data)
    {
        $equityShareSale->update([
            'reverse_reason' => $data['reason']
        ]);

        $equityShareSale->delete();
    }
}
