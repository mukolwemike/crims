<?php

namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundType;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Carbon\Carbon;
use Cytonn\Custodial\Transact\PortfolioTransaction;

class Investment extends Base
{
    public function invest(array $data)
    {
        $data = $this->fillMissingArrayIndex($data, ['description', 'contact_person', 'type_id']);

        $investor = PortfolioInvestor::find($data['portfolio_investor_id']);

        $investment = $this->createInvestment(
            $investor,
            CustodialAccount::find($data['custodial_account_id']),
            FundType::find($data['fund_type_id']),
            new Carbon($data['invested_date']),
            new Carbon($data['maturity_date']),
            $data['interest_rate'],
            $data['amount'],
            PortfolioTransactionApproval::find($data['approval_id']),
            $data['taxable'],
            isset($data['on_call']) ? boolval($data['on_call']) : null,
            $data['contact_person'],
            $data['description'],
            PortfolioInvestmentType::find($data['type_id']),
            $investor->security
        );

        $trans = $this->credit($investment);

        $this->custodial = $trans;
        $this->investment = $investment;

        return $this;
    }

    private function credit(DepositHolding $investment)
    {
        $trans = PortfolioTransaction::build(
            $investment->custodialAccount,
            'I',
            $investment->invested_date,
            'New portfolio investment to: '.$investment->institution->name,
            $investment->amount,
            $investment->investApproval,
            $investment->institution(),
            null
        )->credit();

        $investment->investTransaction()->associate($trans);
        $investment->save();

        return $trans;
    }
}
