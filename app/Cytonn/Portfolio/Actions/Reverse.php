<?php
namespace Cytonn\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenRemoved;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Models\Portfolio\DepositHoldingRepayments;

class Reverse extends Base
{
    public function investment(DepositHolding $investment)
    {
        if ($investment->withdrawn) {
            throw new ClientInvestmentException('Cannot reverse withdrawn investment');
        }

        $reInvestedFromRepayments = PortfolioInvestmentRepayment::where('reinvested_to', $investment->id)->get();

        $reInvestedFromRepayments->each(function (PortfolioInvestmentRepayment $repayment) {
            $this->repayment($repayment);
        });

        $investment->investTransaction()->delete();

        $investment->repayments()->delete();

        $investment->delete();
    }

    protected function parents(DepositHolding $investment)
    {
        $parents =  (new DepositHolding())->where(
            'withdraw_transaction_approval_id',
            $investment->invest_transaction_approval_id
        )->get();

        if ($investment->parent) {
            $parents->push($investment->parent);
        }

        return $parents->unique('id');
    }

    protected function children(DepositHolding $investment)
    {
        $children =  (new DepositHolding())->where(
            'invest_transaction_approval_id',
            $investment->withdraw_transaction_approval_id
        )->get();

        if ($investment->child) {
            $children->push($investment->child);
        }

        return $children->unique('id');
    }

    public function withdrawal(DepositHolding $investment)
    {
        if ($this->children($investment)->count() > 0) {
            throw new ClientInvestmentException("Cannot reverse rollovers and partial withdrawals");
        }

        $investment->withdrawTransaction()->delete();

        return $this->reverseWithdrawal($investment);
    }

    public function repayment(PortfolioInvestmentRepayment $repayment)
    {
        $repayment->custodialTransaction()->delete();

        event(new PortfolioRepaymentHasBeenRemoved($repayment));

        $repayment->delete();
    }
}
