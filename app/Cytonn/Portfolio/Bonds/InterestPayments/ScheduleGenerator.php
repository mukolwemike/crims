<?php

/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 05/06/2018
 * Time: 10:50
 */

namespace Cytonn\Portfolio\Bonds\InterestPayments;

use App\Cytonn\Models\Portfolio\DepositHolding;
use Cytonn\Core\DataStructures\Carbon;

class ScheduleGenerator
{
    /**
     * @param DepositHolding $holding
     * @return bool
     */
    public function generate(DepositHolding $holding)
    {
        if ($this->validate($holding)) {
            return $this->generateSchedules($holding);
        }

        return false;
    }

    /**
     * @param DepositHolding $holding
     * @return bool
     */
    public function regenarateSchedules(DepositHolding $holding)
    {
        $holding->interestSchedules()->forceDelete();

        return $this->generate($holding);
    }

    /**
     * @param DepositHolding $holding
     * @return bool
     */
    private function validate(DepositHolding $holding)
    {
        $valueDate = Carbon::parse($holding->invested_date);

        $maturityDate = Carbon::parse($holding->maturity_date);

        $tenorInMonths = $valueDate->copy()->diffInMonths($maturityDate);

        if ($tenorInMonths <= 0 || $holding->coupon_payment_duration > $tenorInMonths) {
            return false;
        }

        return true;
    }

    /**
     * @param DepositHolding $holding
     * @return bool
     */
    private function generateSchedules(DepositHolding $holding)
    {
        $start = Carbon::parse($holding->invested_date);

        $end = Carbon::parse($holding->maturity_date);

        $schedules = $this->schedule(
            $start,
            $end,
            $holding->coupon_payment_duration
        );

        $amountPaid = 0;

        foreach ($schedules as $key => $schedule) {
            $date = Carbon::parse($schedule['date']);

            $schedules[$key]['amount'] = $holding->repo->getTotalAvailableInterestAtNextDay($date->copy()) - $amountPaid;

            $amountPaid += $schedules[$key]['amount'];
        }

        $holding->interestSchedules()->createMany($schedules);

        return true;
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param $interval
     * @return array
     */
    private function schedule(
        Carbon $start,
        Carbon $end,
        $interval
    ) {
        $schedules = [];

        $dailyInterval = ($interval / 3) * 91;

        $run_date = $start->addDays($dailyInterval);

        $counter = 1;

        while ($end->gt($run_date)) {
            $schedules[] = [
                'date' => $run_date->copy(),
                'amount' => 0,
                'description' => 'Interest payment after ' . $interval * $counter . ' months'
            ];

            $run_date = $run_date->addDays($dailyInterval);

            $counter++;
        }

        return $schedules;
    }
}
