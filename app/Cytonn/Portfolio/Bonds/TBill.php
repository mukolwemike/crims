<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 11/16/17
 * Time: 5:28 PM
 */
namespace Cytonn\Portfolio\Bonds;

use App\Cytonn\Models\Portfolio\BondHolding;
use Carbon\Carbon;

class TBill extends Bond
{
    private $offerPrice;

    private $actualPrice;

    private $returnToFund;

    public function __construct(BondHolding $holding, Carbon $date = null)
    {
        parent::__construct($holding, $date);

        $this->setOfferPrice($this->calculateOfferPrice());
        $this->setActualPrice($this->calculateActualPrice());
        $this->setReturnToFund($this->calculateReturnToFund());
    }

    private function calculateOfferPrice()
    {
        $denominator = 1 + (($this->getHolding()->quoted_yield/100) * ($this->getHolding()->tenor)/365);
        return (double) 100 * (1 / $denominator);
    }

    private function calculateActualPrice()
    {
        return (double) ($this->getHolding()->face_value / 100) * $this->getOfferPrice();
    }

    private function calculateReturnToFund()
    {
        return (double) ($this->getHolding()->face_value - $this->getActualPrice());
    }

    public function getOfferPrice()
    {
        return $this->offerPrice;
    }

    private function setOfferPrice($offerPrice)
    {
        $this->offerPrice = $offerPrice;
    }

    public function getActualPrice()
    {
        return $this->actualPrice;
    }

    private function setActualPrice($actualPrice)
    {
        $this->actualPrice = $actualPrice;
    }

    public function getReturnToFund()
    {
        return $this->returnToFund;
    }

    private function setReturnToFund($returnToFund)
    {
        $this->returnToFund = $returnToFund;
    }
}
