<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 11/16/17
 * Time: 5:28 PM
 */
namespace Cytonn\Portfolio\Bonds;

use App\Cytonn\Models\Portfolio\BondHolding;
use Carbon\Carbon;

class TBond extends Bond
{
    private $interestEarnedPerAnnum;

    private $numberOfSemiAnnualInterestPeriods;

    private $accruedInterest;

    private $yield;

    public function __construct(BondHolding $holding, Carbon $date = null)
    {
        parent::__construct($holding, $date);
        
        $this->setInterestEarnedPerAnnum($this->calculateInterestEarnedPerAnnum());
        $this->setAccruedInterest($this->calculateAccruedInterest());
        $this->setNumberOfSemiAnnualInterestPeriods($this->calculateNumberOfSemiAnnualInterestPeriods());
        $this->setYield($this->calculateYield());
    }

    private function calculateInterestEarnedPerAnnum()
    {
        return (double) $this->getHolding()->face_value * $this->getHolding()->coupon_rate / 100;
    }

    private function calculateAccruedInterest()
    {
        $numberOfDays = Carbon::parse($this->getHolding()->value_date)->diffInDays($this->getDate());

        return (double) $this->getInterestEarnedPerDay() * $numberOfDays;
    }

    private function calculateYield()
    {
        $yield = 0;
        $quotient = 1 + $this->getHolding()->quoted_yield / 100;

        for ($index = 1; $index <= $this->getNumberOfSemiAnnualInterestPeriods(); $index++) {
            if ($index == $this->getNumberOfSemiAnnualInterestPeriods()) {
                $yield +=
                    ($this->getHolding()->face_value + $this->getInterestRatePerAnnum() / 2)/(pow($quotient, $index));
            } else {
                $yield += ($this->getInterestRatePerAnnum() / 2)/(pow($quotient, $index));
            }
        }

        return $yield;
    }

    public function getInterestEarnedPerAnnum()
    {
        return (float) $this->interestEarnedPerAnnum;
    }

    public function getInterestEarnedPerDay()
    {
        return (float) $this->getInterestEarnedPerAnnum() / 365;
    }

    private function setInterestEarnedPerAnnum($interestEarnedPerAnnum)
    {
        $this->interestEarnedPerAnnum = $interestEarnedPerAnnum;
    }

    public function getAccruedInterest()
    {
        return (double) $this->accruedInterest;
    }

    public function getNumberOfSemiAnnualInterestPeriods()
    {
        return $this->numberOfSemiAnnualInterestPeriods;
    }

    private function setNumberOfSemiAnnualInterestPeriods($numberOfSemiAnnualInterestPeriods)
    {
        $this->numberOfSemiAnnualInterestPeriods = $numberOfSemiAnnualInterestPeriods;
    }

    private function setAccruedInterest($accruedInterest)
    {
        // Sum of Interest amount per day since value date, as at a said date
        $this->accruedInterest = $accruedInterest;
    }

    public function getYield()
    {
        return (double) $this->yield;
    }

    private function setYield($yield)
    {
        $this->yield = $yield;
    }

    private function calculateNumberOfSemiAnnualInterestPeriods()
    {
        $maturityDate = Carbon::parse($this->getHolding()->maturity_date);
        $valueDate = Carbon::parse($this->getHolding()->value_date);
        return (int) ceil($maturityDate->diffInDays($valueDate) * 2 / 365);
    }
    
    public function getInterestRatePerAnnum()
    {
        return $this->getHolding()->coupon_rate;
    }
}
