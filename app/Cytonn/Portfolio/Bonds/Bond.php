<?php

namespace Cytonn\Portfolio\Bonds;

use App\Cytonn\Models\Portfolio\BondHolding;
use Carbon\Carbon;

class Bond
{
    private $holding;
    private $date;

    public function __construct(BondHolding $holding, Carbon $date = null)
    {
        $this->setHolding($holding);
        $this->setDate($date);
    }

    public function getHolding()
    {
        return $this->holding;
    }

    private function setHolding($holding)
    {
        $this->holding = $holding;
    }

    public function getDate()
    {
        return $this->date;
    }

    private function setDate($date)
    {
        $this->date = Carbon::parse($date);
    }
}
