<?php
/**
 * Date: 05/04/2016
 * Time: 3:57 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\ActiveStrategy;

use App\Cytonn\Models\ActiveStrategyHolding;

/**
 * Class ActiveStrategyHoldingRepository
 *
 * @package Cytonn\Portfolio\ActiveStrategy
 */
class ActiveStrategyHoldingRepository
{
    /**
     * @var ActiveStrategyHolding
     */
    public $holding;

    /**
     * ActiveStrategyHoldingRepository constructor.
     *
     * @param ActiveStrategyHolding $holding
     */
    public function __construct(ActiveStrategyHolding $holding = null)
    {
        is_null($holding) ? $this->holding = new ActiveStrategyHolding() : $this->holding = $holding;
    }

    /**
     * Calculate the purchase value per share with transaction cost
     *
     * @return mixed
     */
    public function purchaseValuePerShare()
    {
        return $this->holding->cost + $this->transactionCostPerShare();
    }

    /**
     * Calculate market price per share at current trading price
     *
     * @return int
     */
    public function marketPricePerShare()
    {
        $price = $this->holding->security->priceTrail()->latest('date')->first();
        
        if ($price) {
            return $price->price;
        }

        return 0;
    }

    /**
     * Calculate the transaction cost per share
     *
     * @return mixed
     */
    public function transactionCostPerShare()
    {
        $rate = $this->holding->transactionCostRate->rate;

        return $this->holding->cost * $rate/100;
    }

    /**
     * Return the latest target price
     *
     * @return null
     */
    public function latestTargetPrice()
    {
        $target = $this->holding->security->targetPrices()->latest('date')->first();

        if ($target) {
            return $target->price;
        }

        return 0;
    }

    /**
     * Calculate the total cost of shares, without transaction costs
     *
     * @return mixed
     */
    public function totalCost()
    {
        return $this->holding->cost * $this->holding->number;
    }

    /**
     * Calculate the total value of shares, with transaction costs
     *
     * @return mixed
     */
    public function totalValue()
    {
        return $this->holding->number * $this->purchaseValuePerShare();
    }

    /**
     * Calculate the total value of shares at current trading price
     *
     * @return int
     */
    public function marketValue()
    {
        return $this->marketPricePerShare() * $this->holding->number;
    }

    /**
     * Calculate the gain or loss for this holding
     *
     * @return int
     */
    public function gainLoss()
    {
        return $this->marketValue() - $this->totalValue();
    }
}
