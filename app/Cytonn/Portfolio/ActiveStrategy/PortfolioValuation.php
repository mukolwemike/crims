<?php
/**
 * Date: 06/04/2016
 * Time: 7:57 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\ActiveStrategy;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;

/**
 * Class PortfolioValuation
 *
 * @package Cytonn\Portfolio\ActiveStrategy
 */
class PortfolioValuation
{
    protected $fund;

    protected $date;

    protected $fundManager;

    protected $offshore;

    protected $baseCurrency;

    private $securities;

    /**
     * PortfolioValuation constructor.
     * @param $date
     * @param array|FundManager $fundManager
     */
    public function __construct($fundManager, Carbon $date = null)
    {
        $this->date = Carbon::parse($date);

        $this->fundManager = $fundManager;

        $this->baseCurrency = Currency::where('code', config('system.base_currency'))->first();
    }

    public function securities()
    {
        $query = PortfolioSecurity::forAssetClass('equities');

        if (!is_array($this->fundManager)) {
            $this->fundManager = [$this->fundManager];
        }

        if ($this->offshore) {
            $query = $query->offshore($this->offshore);
        }

        $fms = collect($this->fundManager)->lists('id');

        return $query->whereIn('fund_manager_id', $fms);
    }

    private function getSecurities()
    {
        if ($this->securities) {
            return $this->securities;
        }

        return $this->securities = $this->securities()->get();
    }

    /**
     * Calculate total return for portfolio
     *
     * @param Carbon|null $date
     * @return int
     */
    public function portfolioReturn(Carbon $date = null)
    {
        $securities = $this->getSecurities();

        return $securities->sum(function (PortfolioSecurity $security) use ($date) {
            $ret = $security->repo
                ->setFund($this->fund)
                ->currentGainLoss($date);

            return $this->convert($ret, $security->currency);
        });
    }

    /**
     * Calculate total cost for portfolio
     *
     * @param Carbon|null $date
     * @return int
     */
    public function portfolioCost(Carbon $date = null)
    {
        $securities = $this->getSecurities();

        return $securities->sum(function (PortfolioSecurity $security) use ($date) {
            $cost =  $security->repo
                ->setFund($this->fund)
                ->totalPurchaseValueHeld($date);

            return $this->convert($cost, $security->currency);
        });
    }

    /**
     * Calculate total value for portfolio
     *
     * @param Carbon|null $date
     * @return int
     */
    public function portfolioValue(Carbon $date = null)
    {
        $securities = $this->getSecurities();

        return $securities->sum(function (PortfolioSecurity $security) use ($date) {
            $value = $security->repo
                ->setFund($this->fund)
                ->totalCurrentMarketValue($date);

            $currency = $security->cur;

            if (is_null($currency)) {
                $currency = $security->currency;
            }

            return $this->convert($value, $currency);
        });
    }

    public function dividends(Carbon $date = null)
    {
        $securities = $this->getSecurities();

        return $securities->sum(function (PortfolioSecurity $security) use ($date) {
            $div =  $security->repo
                ->setFund($this->fund)
                ->dividend($date);

            return $this->convert($div, $security->cur);
        });
    }

    /**
     * Calculate total number of shares in portfolio
     *
     * @param Carbon|null $date
     * @return mixed
     */
    public function numberOfShares(Carbon $date = null)
    {
        $securities = $this->getSecurities();

        return $securities->sum(function (PortfolioSecurity $security) use ($date) {
            return $security->repo
                ->setFund($this->fund)
                ->numberOfSharesHeld($date);
        });
    }

    public function percentageReturn(Carbon $date = null)
    {
        $aum = $this->portfolioCost();

        if ($aum == 0) {
            return 0;
        }

        return $this->getSecurities()->sum(function (PortfolioSecurity $security) use ($date, $aum) {
            $repo = $security->repo;

            $repo->setFund($this->fund);

            $calc = $repo->yieldCalculation($date);

            return $calc->yield * $calc->cost / $aum;
        });
    }

    protected function convert($amount, Currency $currency = null)
    {
        return convert($amount, $currency, $this->date, $this->baseCurrency);
    }

    /**
     * @param mixed $fund
     * @return PortfolioValuation
     */
    public function setFund(UnitFund $fund = null)
    {
        $this->fund = $fund;
        return $this;
    }

    public function setOffshore($offshore)
    {
        $this->offshore = $offshore;

        return $this;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model|null|static $baseCurrency
     * @return PortfolioValuation
     */
    public function setBaseCurrency($baseCurrency)
    {
        $this->baseCurrency = $baseCurrency;
        return $this;
    }
}
