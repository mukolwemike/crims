<?php
/**
 * Date: 05/04/2016
 * Time: 5:18 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\ActiveStrategy;

/**
 * Class ActiveStrategySecurityRepository
 *
 * @package Cytonn\Portfolio\ActiveStrategy
 */
use App\Cytonn\Models\ActiveStrategyDividend;
use App\Cytonn\Models\ActiveStrategyHolding;
use App\Cytonn\Models\ActiveStrategyHoldingType;
use App\Cytonn\Models\ActiveStrategySecurity;
use Carbon\Carbon;

/**
 * Class ActiveStrategySecurityRepository
 *
 * @package Cytonn\Portfolio\ActiveStrategy
 */
class ActiveStrategySecurityRepository
{
    /**
     * @var ActiveStrategySecurity|null
     */
    protected $security;


    /**
     * @param ActiveStrategySecurity|null $security
     */
    public function __construct(ActiveStrategySecurity $security = null)
    {
        is_null($security) ? $this->security = new ActiveStrategySecurity()  : $this->security = $security;
    }

    /**
     * The average price a security was bought at
     *
     * @return float|int
     */
    public function averagePrice()
    {
        try {
            return $this->totalPurchaseCostHeld() / $this->numberOfSharesBought();
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * The target price set for a security
     *
     * @return int
     */
    public function targetPrice()
    {
        try {
            return $this->security->targetPrices()->latest('date')->first()->price;
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * The market price a security is trading at
     *
     * @return int
     */
    public function marketPrice()
    {
        try {
            return $this->security->priceTrail()->latest('date')->first()->price;
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * The difference between target and market price
     *
     * @return int
     */
    public function alpha()
    {
        return $this->targetPrice() - $this->marketPrice();
    }

    /**
     * The number of shares held in a security
     *
     * @param  Carbon $date
     * @return mixed
     */
    public function numberOfSharesHeld(Carbon $date = null)
    {
        return $this->numberOfSharesBought($date) - $this->numberOfSharesSold($date);
    }

    /**
     * @return mixed
     */
    public function numberOfSharesBought(Carbon $date = null)
    {
        $date = new Carbon($date);

        return $this->security->holdings()->before($date)->sum('number');
    }

    /**
     * @param Carbon $date
     * @return int
     */
    public function numberOfSharesSold(Carbon $date = null)
    {
        $date = new Carbon($date);

        return $this->security->sales()->before($date)->sum('number');
    }

    public function currentTotalCost()
    {
        try {
            return $this->totalPurchaseValueHeld() / $this->numberOfSharesBought();
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * The total cost of shares held in a security (without factoring transaction costs)
     *
     * @return int
     */
    public function totalPurchaseCostHeld()
    {
        $holdings = $this->security->holdings;

        $total = 0;
        foreach ($holdings as $holding) {
            $total += $holding->cost * $holding->number;
        }

        return $total;
    }

    /**
     * The total purchase value of shares held, including transaction costs
     *
     * @return int
     */
    public function totalPurchaseValueHeld()
    {
        $holdings = $this->security->holdings;

        $total = 0;
        foreach ($holdings as $holding) {
            $total += $holding->repo->totalValue();
        }

        return $total;
    }

    public function totalCurrentPurchaseValue()
    {
        try {
            return $this->numberOfSharesHeld() *  $this->totalPurchaseValueHeld() / $this->numberOfSharesBought();
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function totalSalesValue()
    {
        $sales = $this->security->sales;

        $total = 0;

        foreach ($sales as $sale) {
            $total+=$sale->repo->saleValue();
        }

        return $total;
    }

    /**
     * The total market value of shares held at the current market price
     *
     * @return int
     */
    public function totalMarketValueHeld()
    {
        $holdings = $this->security->holdings;

        $total = 0;
        foreach ($holdings as $holding) {
            $total += $holding->repo->marketValue();
        }

        return $total;
    }

    public function totalCurrentMarketValue()
    {
        return $this->marketPrice() * $this->numberOfSharesHeld();
    }

    public function currentPortfolioValue()
    {
        return $this->numberOfSharesHeld() * $this->marketPrice();
    }

    public function totalMarketValueOfSoldShares()
    {
        $sales = $this->security->sales;

        $total = 0;
        foreach ($sales as $sale) {
            $total += $sale->repo->marketValue();
        }

        return $total;
    }

    /**
     * The gain or loss for a security
     *
     * @return int
     */
    public function totalGainLoss()
    {
        $holdings = $this->security->holdings;

        $total = 0;
        foreach ($holdings as $holding) {
            $total += $holding->repo->gainLoss();
        }

        return $total;
    }

    public function totalGailLossInSales()
    {
        $sales = $this->security->sales;

        $total = 0;

        foreach ($sales as $sale) {
            $total+= $sale->repo->gainLoss();
        }

        return $total;
    }

    /**
     * The return for a security
     *
     * @return int
     */
    public function portfolioReturn()
    {
        return $this->totalGainLoss() + $this->dividend();
    }

    public function currentGainLoss()
    {
        return $this->currentPortfolioValue() + $this->dividend() - $this->totalCurrentPurchaseValue();
    }

    /**
     * Total dividends earned in a security
     *
     * @return int
     */
    public function dividend()
    {
        $dividends = ActiveStrategyDividend::where('security_id', $this->security->id)->get();

        $amount = 0;
        foreach ($dividends as $div) {
            $amount+= $div->dividend * $div->number;
        }

        return $amount;
    }

    public function purchase($cost, Carbon $date, $numberOfShares, ActiveStrategyHoldingType $type)
    {
        return ActiveStrategyHolding::create(
            [
            'cost' => $cost,
            'date' => $date,
            'number' => $numberOfShares,
            'security_id' => $this->security->id,
            'type_id' => $type->id
            ]
        );
    }
}
