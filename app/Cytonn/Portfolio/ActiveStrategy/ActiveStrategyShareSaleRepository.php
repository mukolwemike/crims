<?php
/**
 * Date: 07/04/2016
 * Time: 9:31 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\ActiveStrategy;

use App\Cytonn\Models\ActiveStrategyShareSale;

class ActiveStrategyShareSaleRepository
{
    public $sale;

    /**
     * ActiveStrategyShareSaleRepository constructor.
     *
     * @param $sale
     */
    public function __construct(ActiveStrategyShareSale $sale = null)
    {
        is_null($sale) ? $this->sale = new ActiveStrategyShareSale() : $this->sale = $sale;
    }

    public function targetPrice()
    {
        try {
            return $this->sale->security->targetPrices()->where('date', '<=', $this->sale->date)
                ->latest()->first()->price;
        } catch (\Exception $e) {
            $pr = $this->sale->security->targetPrices()->where('date', '>=', $this->sale->date)
                ->oldest()->first();

            if (is_null($pr)) {
                return 0;
            }

            return $pr->price;
        }
    }

    public function marketPrice()
    {
        return $this->sale->security->repo->marketPrice();
    }

    public function numberOfShares()
    {
        return $this->sale->number;
    }

    public function marketValue()
    {
        return $this->numberOfShares() * $this->marketPrice();
    }

    public function saleValue()
    {
        return $this->numberOfShares() * $this->sale->sale_price;
    }

    public function averagePurchasePrice()
    {
        $holdings_before_date = $this->sale->security->holdings;

        $total = 0;
        $total_number = 0;

        if ($holdings_before_date) {
            $holdings_before_date = $this->sale->security->holdings()->where('date', '<=', $this->sale->date)->get();

            foreach ($holdings_before_date as $holding) {
                $total += $holding->cost * $holding->number;
                $total_number += $holding->number;
            }
        }

        try {
            return $total / $total_number;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function averagePurchaseValuePerShare()
    {
        $holdings_before_date = $this->sale->security->holdings;

        $total = 0;
        $total_number = 0;

        if ($holdings_before_date) {
            $holdings_before_date = $this->sale->security->holdings()->where('date', '<=', $this->sale->date)->get();

            foreach ($holdings_before_date as $holding) {
                $total += $holding->repo->purchaseValuePerShare() * $holding->number;
                $total_number += $holding->number;
            }
        }

        try {
            return $total / $total_number;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function totalPurchaseValue()
    {
        return $this->averagePurchaseValuePerShare() * $this->numberOfShares();
    }

    public function gainLoss()
    {
        return $this->saleValue() - $this->totalPurchaseValue();
    }
}
