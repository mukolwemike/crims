<?php
/**
 * Date: 06/04/2016
 * Time: 11:51 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\ActiveStrategy;

use Laracasts\Validation\FormValidator;

class SaveDividendsForm extends FormValidator
{
    public $rules = [
        'date'=>'required|date',
//        'dividend'=>'required|numeric'
    ];
}
