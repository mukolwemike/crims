<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 11/17/17
 * Time: 4:46 PM
 */

namespace Cytonn\Portfolio;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Illuminate\Support\Collection;

class PortfolioSecurityRepository
{
    public function suggestCode()
    {
        $codes = Collection::make(PortfolioSecurity::withTrashed()->pluck('code'))->map(
            function ($code) {
                return intval($code);
            }
        )->toArray();

        if (count($codes) == 0) {
            return 1000;
        }

        if (is_array($codes) && count($codes) > 1) {
            return 1 + max($codes);
        } elseif ($codes instanceof Collection) {
            return 1 + max($codes->all());
        }

        return 1 + max($codes);
    }
}
