<?php
/**
 * Date: 18/11/2015
 * Time: 2:21 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Portfolio;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use Carbon\Carbon;

class CustodialRepository
{
    public $account;

    /**
     * CustodialRepository constructor.
     *
     * @param $account
     */
    public function __construct(CustodialAccount $account = null)
    {
        is_null($account) ? $this->account = new CustodialAccount() : $this->account = $account;
    }


    public function getSumCustodialBalanceForCurrency($currency_id)
    {
        $accounts = CustodialAccount::where('currency_id', $currency_id)->get();

        $balance_arr = [];

        foreach ($accounts as $account) {
            array_push($balance_arr, $account->balance());
        }

        return array_sum($balance_arr);
    }

    public function getSumCustodialBalanceForCurrencyAtDate($currency_id, $date)
    {
        $accounts = CustodialAccount::where('currency_id', $currency_id)->get();

        $balance_arr = [];

        foreach ($accounts as $account) {
            $latest_transaction = CustodialTransaction::where('custodial_account_id', $account->id)
                ->where('date', '<=', (new Carbon($date))->toDateString())
                ->latest()
                ->first();

            array_push($balance_arr, $account->transactionBalance($latest_transaction));
        }

        return array_sum($balance_arr);
    }
}
