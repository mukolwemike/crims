<?php

namespace Cytonn\Portfolio\Equities;

use App\Cytonn\Models\ActiveStrategyTransactionCost;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityHoldingType;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Portfolio\PortfolioOrder;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Custodial\Transact\PortfolioTransaction;
use Cytonn\Portfolio\Actions\EquityReverse;
use Cytonn\Portfolio\Events\PortfolioTransactionApproved;
use Illuminate\Database\Eloquent\Builder;

class EquitySecurityRepository
{
    protected $security;

    protected $fund;

    protected $baseCurrency;

    protected $date;

    protected $currency;

    public function __construct(PortfolioSecurity $security = null, Carbon $date = null)
    {
        is_null($security) ?
            $this->security = new PortfolioSecurity()
            : $this->security = $security;

        $this->date = $date ? $date : Carbon::today();

        $this->baseCurrency = $this->currency = $this->security->cur;
    }

    /**
     * @param Carbon|null $date
     * @return Builder
     */
    public function holdingsQuery(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $this->setDate($date);

        $query = $this->security->equityHoldings()->before($date);

        if ($this->fund) {
            $query = $query->forUnitFund($this->fund);
        }

        return $query;
    }

    public function holdingsForFund()
    {
        return $this->security->whereHas('equityHoldings', function ($holding) {
            $holding->where('unit_fund_id', $this->fund->id);
        });
    }

    public function salesQuery(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $this->setDate($date);

        $query = $this->security->sales()->before($date);

        if ($this->fund) {
            return $query->forUnitFund($this->fund);
        }

        return $query;
    }

    public function averagePrice(Carbon $date = null)
    {
        $bought = $this->numberOfSharesBought($date);

        $price =  $bought == 0 ? 0 : $this->totalPurchaseCostHeld()/$bought;

        return $this->convert($price);
    }

    public function targetPrice()
    {
        $target = $this->security->targetPrices()
            ->latest('date')
            ->first();

        return $this->convert(is_null($target) ? 0 : $target->price);
    }

    public function marketPrice(Carbon $date = null)
    {
        $trail = $this->security->priceTrail()
            ->before($date)
            ->latest('date')
            ->first();

        return $this->convert(is_null($trail) ? 0 : $trail->price);
    }

    public function alpha()
    {
        return $this->convert($this->targetPrice() - $this->marketPrice());
    }

    public function numberOfSharesHeld(Carbon $date = null)
    {
        return $this->numberOfSharesBought($date) - $this->numberOfSharesSold($date);
    }

    public function numberOfSharesBought(Carbon $date = null)
    {
        return $this->holdingsQuery($date)->sum('number');
    }

    public function numberOfSharesSold(Carbon $date = null)
    {
        return $this->salesQuery($date)->sum('number');
    }

    public function currentTotalCost(Carbon $date = null)
    {
        $bought = $this->numberOfSharesBought($date);

        $cost = $bought == 0 ? 0 : $this->totalPurchaseValueHeld($date) / $bought;

        $this->convert($cost);
    }

    public function totalPurchaseCostHeld(Carbon $date = null)
    {
        $cost = $this->holdingsQuery($date)->get()
            ->sum(function (EquityHolding $holding) {
                return $holding->cost * $holding->number;
            });

        return $this->convert($cost);
    }

    public function holdingCost(Carbon $date = null)
    {
        return $this->convert($this->holdingsQuery($date)->sum('cost'));
    }

    public function holdingNumber(Carbon $date = null)
    {
        return $this->holdingsQuery($date)->sum('number');
    }

    public function totalPurchaseValueHeld(Carbon $date = null)
    {
        $this->setDate($date);

        $value =  $this->holdingsQuery($date)->get()
            ->sum(function (EquityHolding $holding) {
                return $holding->repo->totalValue();
            });

        return $this->convert($value);
    }

    public function totalCurrentPurchaseValue(Carbon $date = null)
    {
        $purchaseValue = $this->numberOfSharesHeld($date) *  $this->totalPurchaseValueHeld($date);

        $numberOfShares = $this->numberOfSharesBought($date);

        $value =  $numberOfShares == 0 ? 0 : $purchaseValue/$numberOfShares;

        return $this->convert($value);
    }

    public function totalSalesValue(Carbon $date = null)
    {
        $this->setDate($date);

        $sales = $this->salesQuery($date)->get()
            ->sum(function (EquityShareSale $sale) {
                return $sale->repo->saleValue();
            });

        return $this->convert($sales);
    }

    public function totalMarketValueHeld(Carbon $date = null)
    {
        $this->setDate($date);

        $mv = $this->holdingsQuery($date)->get()
            ->sum(function (EquityHolding $holding) use ($date) {
                return $holding->repo->marketValue($date);
            });

        return $this->convert($mv);
    }

    public function totalCurrentMarketValue(Carbon $date = null)
    {
        $this->setDate($date);

        $mv =  $this->marketPrice($date) * $this->numberOfSharesHeld($date);

        return $this->convert($mv);
    }

    public function currentPortfolioValue(Carbon $date = null)
    {
        $this->setDate($date);

        $pv = $this->numberOfSharesHeld($date) * $this->marketPrice($date);

        return $this->convert($pv);
    }

    public function totalMarketValueOfSoldShares(Carbon $date = null)
    {
        $this->setDate($date);

        $mv =  $this->salesQuery($date)->get()
            ->sum(function (EquityShareSale $sale) {
                return $sale->repo->marketValue();
            });

        return $this->convert($mv);
    }

    public function totalGainLoss(Carbon $date = null)
    {
        $this->setDate($date);

        $gl = $this->holdingsQuery($date)->get()
            ->sum(function (EquityHolding $holding) use ($date) {
                return $holding->repo->gainLoss($date);
            });

        return $this->convert($gl);
    }

    public function totalGailLossInSales(Carbon $date = null)
    {
        $this->setDate($date);

        $gl = $this->salesQuery($date)->get()
            ->sum(function (EquityShareSale $sale) {
                return $sale->repo->gainLoss();
            });

        return $this->convert($gl);
    }

    public function portfolioReturn(Carbon $date = null)
    {
        $this->setDate($date);

        $ret = $this->totalGainLoss($date) + $this->dividend($date);

        return $this->convert($ret);
    }



    public function currentGainLoss(Carbon $date = null)
    {
        $this->setDate($date);

        $cgl = $this->currentPortfolioValue($date) + $this->dividend($date) - $this->totalCurrentPurchaseValue($date);

        return $this->convert($cgl);
    }

    public function dividend(Carbon $date = null)
    {
        $this->setDate($date);

        $dividends = EquityDividend::where('security_id', $this->security->id)
            ->where('dividend_type', 'cash')
            ->before($date)
            ->get();

        $total = $dividends->sum(function (EquityDividend $dividend) {
            return $dividend->dividend * $dividend->number;
        });

        if ($this->fund) {
            $forSecurity = $this->security->repo->numberOfSharesHeld($date);

            if ($forSecurity == 0) {
                return 0;
            }

            $forFund = $this->numberOfSharesHeld($date);

            return $forFund/$forSecurity * $total;
        }

        return $this->convert($total);
    }

    public function purchaseFromDividend(EquityDividend $dividend, PortfolioTransactionApproval $approval)
    {
        $approval = $approval->payload;

        $date = Carbon::parse($approval['date']);

        $data = [
            'cost' => abs($this->security->latestPrice($date)->price),
            'date' => $dividend->date,
            'settlement_date' => Carbon::parse($approval['date']),
            'number' => $dividend->shares,
        ];

        $type = (new EquityHoldingType())->where('slug', 'dividend')->first();

        return $this->purchase($data, $type, $dividend->unitFund);
    }

    public function purchase($data, EquityHoldingType $type, UnitFund $fund = null)
    {
        $transactionCost = (new ActiveStrategyTransactionCost())->latest()->first();

        if (!isset($data['fee_per_share'])) {
            $data['fee_per_share'] = $this->feePerShare($data, $transactionCost);
        }

        return EquityHolding::create([
            'cost' => $data['cost'],
            'date' => Carbon::parse($data['date']),
            'settlement_date' => (isset($data['settlement_date'])) ? Carbon::parse($data['settlement_date']): null,
            'number' => $data['number'],
            'unit_fund_id' => $fund ? $fund->id: null,
            'portfolio_security_id' => $this->security->id,
            'active_strategy_holding_type_id' => $type->id,
            'active_strategy_transaction_cost_id' => $transactionCost->id,
            'fee_per_share' => (isset($data['fee_per_share'])) ? $data['fee_per_share']: null
        ]);
    }

    public function feePerShare($data, ActiveStrategyTransactionCost $cost)
    {
        $value = $data['cost'] * $data['number'];

        $cost = $cost->rate * $value;

        return ($cost/ $data['number']);
    }

    public function credit(EquityHolding $holding, $approval)
    {
        $cost = $holding->number * $holding->cost;

        $charges = (isset($approval->payload['fees']))
            ? $this->settlementCharges($approval, $cost)
            : 0;

        $additional = $cost * $holding->getTransactionCostRate()/100;

        $amount = $cost + $additional + $charges;

        $fund = $holding->unitFund;

        $trans = PortfolioTransaction::build(
            $fund->custodialAccount,
            'I',
            $holding->date,
            'Purchase of shares: '.$holding->security->name. ' Fund: '.$fund->name,
            $amount,
            $approval,
            $holding->security->investor
        )->credit();

        return $trans;
    }

    public function yieldCalculation(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        //get current share balance;
        $numberOfSharesHeld = $this->numberOfSharesHeld($date->copy());

        $summation = 0;

        $purchases = collect([]);

        $this->holdingsQuery($date)->latest('date')
            ->chunk(10, function ($holdings) use (&$purchases, &$summation, $numberOfSharesHeld) {
                /** @var EquityHolding $holding */
                foreach ($holdings as $holding) {
                    $purchases->push($holding);
                    $summation += $holding->number;

                    if ($summation >= $numberOfSharesHeld) {
                        return false;
                    }
                }
            });

        $alreadyProcessed = 0;
        $price = $this->marketPrice();

        $processed = $purchases->map(function (EquityHolding $purchase)
 use (&$alreadyProcessed, $numberOfSharesHeld, $price, $date) {
            $number = $purchase->number;

            $currentLimit = $number + $alreadyProcessed;

            if ($currentLimit > $numberOfSharesHeld) {
                $number = $numberOfSharesHeld - $alreadyProcessed;
            }

            $alreadyProcessed += $number;
            $cost = $purchase->cost * $number;
            $market = $price * $number;
            $days = $date->diffInDays($purchase->date);

            return (object) [
                'number' => $number,
                'cost' => $cost,
                'market' => $market,
                'yield' => ($days == 0 || $cost == 0) ? 0 : (365/$days) * 100 * ($market - $cost)/$cost,
                'days' => $days,
                'cost_price' => $purchase->cost,
                'market_price' => $price
            ];
        });

        $yield = $processed->sum(function ($processed) use ($numberOfSharesHeld) {
            return $numberOfSharesHeld == 0 ? 0 : $processed->yield * ($processed->number/$numberOfSharesHeld);
        });

        $cost_price = $processed->sum(function ($processed) use ($numberOfSharesHeld) {
            return $numberOfSharesHeld == 0 ? 0 :$processed->cost_price * ($processed->number/$numberOfSharesHeld);
        });

        return (object) [
            'cost' => $processed->sum('cost'),
            'market' => $processed->sum('market'),
            'yield' => $yield,
            'cost_price' => $cost_price,
            'market_price' => (float) $price
        ];




        //get purchases for each
                // get cost of purchases
                //weighted return of each
                //sum to get yield
    }

    public function yield(Carbon $date = null)
    {
        return $this->yieldCalculation($date)->yield;
    }

    public function settlementCharges($approval, $cost)
    {
        return collect($approval->payload['fees'])->map(function ($fee) use ($cost) {
            return [
                'amount' => (isset($fee['percentage']))
                    ? (floatval($fee['percentage']) / 100) * $cost
                    : $fee['amount']
            ];
        })->sum('amount');
    }

    public function debit(EquityShareSale $sale, $approval)
    {
        $cost = ($sale->number * $sale->sale_price);

        $charges = (isset($approval->payload['fees']))
            ? $this->settlementCharges($approval, $cost)
            : 0;

        $amount = $cost - $charges;

        $fund = $sale->unitFund;

        $trans = PortfolioTransaction::build(
            $fund->custodialAccount,
            'I',
            $sale->date,
            'Sale of shares: '.$sale->security->name. ' Fund: '.$fund->name,
            $amount,
            $approval,
            $sale->security->investor,
            null
        )->debit();

        return $trans;
    }

    public function getCurrencies($fundManager = null)
    {
        return Currency::all()->filter(function ($currency) use ($fundManager) {
            return DepositHolding::currency($currency, true)
                    ->fundManager($fundManager)
                    ->forSecurity($this->security)->exists();
        });
    }

    /**
     * @param Carbon|static $date
     * @return EquitySecurityRepository
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    protected function convert($amount)
    {
        if (is_null($this->currency) && is_null($this->baseCurrency)) {
            return $amount;
        }

        return convert($amount, $this->currency, $this->date, $this->baseCurrency);
    }

    public function getFundManagers()
    {
        return $this->security->fundManager()->get();
    }

    public function getProductName()
    {
        return $this->security->fundManager->name;
    }

    public function setFund(UnitFund $fund = null)
    {
        $this->fund = $fund;
        return $this;
    }

    /**
     * @param mixed $baseCurrency
     * @return EquitySecurityRepository
     */
    public function setBaseCurrency($baseCurrency)
    {
        $this->baseCurrency = $baseCurrency;
        return $this;
    }
}
