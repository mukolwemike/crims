<?php

namespace Cytonn\Portfolio\Equities;

use App\Cytonn\Models\Portfolio\EquityShareSale;

class EquityShareSaleRepository
{
    public $sale;

    public function __construct(EquityShareSale $sale = null)
    {
        is_null($sale)? $this->sale = new EquityShareSale(): $this->sale = $sale;
    }

    public function targetPrice()
    {
        return $this->sale->security->repo->targetPrice();
    }

    public function marketPrice()
    {
        return $this->sale->security->repo->marketPrice();
    }

    public function numberOfShares()
    {
        return $this->sale->number;
    }

    public function marketValue()
    {
        return $this->numberOfShares() * $this->marketPrice();
    }

    public function saleValue()
    {
        return $this->numberOfShares() * $this->sale->sale_price;
    }

    public function averagePurchasePrice()
    {
        $holdings = $this->sale->security->equityHoldings()->where('date', '<=', $this->sale->date)->get();

        $cost = $holdings->sum(function ($holding) {
            return $holding->cost * $holding->number;
        });

        $total_number = $holdings->sum('number');

        return $total_number == 0 ? 0 : $cost/$total_number;
    }

    public function averagePurchaseValuePerShare()
    {
        $holdings = $this->sale->security->equityHoldings()->where('date', '<=', $this->sale->date)->get();

        $cost = $holdings->sum(function ($holding) {
            return $holding->repo->purchaseValuePerShare() * $holding->number;
        });

        $total_number = $holdings->sum('number');

        return $total_number == 0 ? 0 : $cost/$total_number;
    }

    public function totalPurchaseValue()
    {
        return $this->averagePurchaseValuePerShare() * $this->numberOfShares();
    }

    public function gainLoss()
    {
        return $this->saleValue() - $this->totalPurchaseValue();
    }
}
