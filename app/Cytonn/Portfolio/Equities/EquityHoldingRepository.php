<?php
namespace Cytonn\Portfolio\Equities;

use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;

class EquityHoldingRepository
{
    public $holding;
 
    public function __construct(EquityHolding $holding = null)
    {
        is_null($holding) ? $this->holding = new EquityHolding() : $this->holding = $holding;
    }

    public function purchaseValuePerShare()
    {
        return $this->holding->cost + $this->transactionCostPerShare();
    }

    public function marketPricePerShare(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $price = $this->holding->security
            ->priceTrail()
            ->before($date)
            ->latest('date')
            ->first();
        
        return $price ? $price->price : 0;
    }

    public function transactionCostPerShare()
    {
        $rate = $this->holding->transactionCostRate->rate;
        
        return $this->holding->cost * $rate/100;
    }

    public function latestTargetPrice()
    {
        $target = $this->holding->security->targetPrices()->latest('date')->first();
        
        return $target ? $target->price : 0;
    }
 
    public function totalCost()
    {
        return $this->holding->cost * $this->holding->number;
    }

    public function totalValue()
    {
        return $this->holding->number * $this->purchaseValuePerShare();
    }

    public function marketValue(Carbon $date = null)
    {
        return $this->marketPricePerShare($date) * $this->holding->number;
    }

    public function gainLoss(Carbon $date = null)
    {
        return $this->marketValue($date) - $this->totalValue();
    }

    public function netNumberOfShares(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        $security = $this->holding->security;

        $holdings = $security->equityHoldings()->before($date)
//            ->where('created_at', '<', $this->holding->created_at)
        ;

        if ($this->fund) {
            $holdings = $holdings->forUnitFund($this->fund);
        }

        $holdings = $holdings->sum('number');

        $sales = $security->sales()->before($date)
//            ->where('created_at', '<', $this->holding->created_at)
        ;

        if ($this->fund) {
            $sales = $sales->forUnitFund($this->fund);
        }

        $sales = $sales->sum('number');

        if ($holdings >= $sales) {
            return $this->holding->number;
        } else {
            $diff = $sales - $holdings;

            return $diff >= $this->holding->number ? 0 : $this->holding->number - $diff;
        }
    }

    public function setFund(UnitFund $fund = null)
    {
        $this->fund = $fund;

        return $this;
    }
}
