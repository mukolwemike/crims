<?php

namespace Cytonn\Portfolio\Equities;

use Cytonn\Portfolio\ActiveStrategy\PortfolioValuation as ActiveStrategyValuation;

class PortfolioValuation extends ActiveStrategyValuation
{
    //Use parent class to make changes
}
