<?php

namespace Cytonn\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class MaturityRepository
 *
 * @package Cytonn\Portfolio
 */
class MaturityRepository
{
    /**
     * Calculate the weekly data
     *
     * @param  $start
     * @param  $end
     * @param  $currency
     * @return Collection
     */
    public function getWeeklyMaturityAnalysisFor($start, $end, $currency)
    {
        $start = (new Carbon($start))->startOfDay();
        $end = (new Carbon($end))->endOfDay();

        $date = new Carbon($start->copy());

        $weekly_coll = new Collection();

        while ($date->lte($end->copy())) {
            $investments = DepositHolding::where('maturity_date', '>=', $date->copy()
                ->startOfWeek()->toDateString())
                ->where('maturity_date', '<=', $date->copy()->endOfWeek()->toDateString())
                ->currency($currency)
                ->where(function ($q) {
                    $q->where('withdrawn', null)
                        ->orWhere('withdrawn', 0);
                })
                ->get();

            $weekly_coll->push([
                'amount' => $investments->sum('amount'),
                'start' => $date->copy()->startOfWeek(),
                'end' => $date->copy()->endOfWeek()
            ]);

            $date->addWeek();
        }

        return $weekly_coll;
    }

    /**
     * Calculate the monthly data
     *
     * @param  $start
     * @param  $end
     * @param  $currency
     * @return Collection
     */
    public function getMonthlyMaturityAnalysisFor($start, $end, $currency)
    {
        $start = new Carbon($start);
        $end = new Carbon($end);

        $monthly_coll = new Collection();

        $date = new Carbon($start->copy()); //reset

        while ($date->lte($end->copy())) {
            $investments = DepositHolding::where('maturity_date', '>=', $date->copy()
                ->startOfMonth()->toDateString())
                ->where('maturity_date', '<=', $date->copy()->endOfMonth()->toDateString())
                ->currency($currency)
                ->where(function ($q) {
                    $q->where('withdrawn', null)
                        ->orWhere('withdrawn', 0);
                })->get();

            $monthly_coll->push([
                'amount' => $investments->sum('amount'),
                'start' => $date->copy()->startOfMonth(),
                'end' => $date->copy()->endOfMonth()
            ]);

            $date->addMonth();
        }

        return $monthly_coll;
    }
}
