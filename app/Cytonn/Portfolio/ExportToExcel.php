<?php
/**
 * Date: 06/08/2016
 * Time: 11:58 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestor;

/**
 * Class ExportToExcel
 *
 * @package Cytonn\Portfolio
 */
class ExportToExcel
{
    /**
     * @param PortfolioInvestor $institution
     * @param FundManager $fundManager
     * @param null $as_at_date
     * @return mixed
     */
    public function investmentsForInstitution(
        PortfolioSecurity $institution,
        FundManager $fundManager,
        $as_at_date = null
    ) {
        return \Excel::create(
            'Investments for ' . $institution->name,
            function ($excel) use ($institution, $fundManager, $as_at_date) {
                $currencies = Currency::whereHas(
                    'accounts',
                    function ($account) {
                        $account->whereHas(
                            'fundManager',
                            function ($manager) {
                                $manager->whereHas(
                                    'securities',
                                    function ($security) {
                                        $security->has('depositHoldings');
                                    }
                                );
                            }
                        );
                    }
                )->get();

                $currencies->each(
                    function ($currency) use ($excel, $institution, $fundManager, $as_at_date) {
                        $investments = DepositHolding::forSecurity($institution)
                            ->currency($currency)
                            ->where('invested_date', '<=', $as_at_date)
                            ->get();

                        $excel->sheet(
                            'Investments ' . $currency->code,
                            function ($sheet) use ($institution, $currency, $investments, $fundManager, $as_at_date) {
                                $sheet->mergeCells('A1:I1');
                                $sheet->mergeCells('A3:I3');
                                $sheet->loadView(
                                    'portfolio.exports.institution_summary',
                                    [
                                        'institution' => $institution,
                                        'currency' => $currency,
                                        'investments' => $investments,
                                        'fund_manager' => $fundManager,
                                        'as_at_date' => $as_at_date
                                    ]
                                );
                            }
                        );
                    }
                );
            }
        );
    }
}
