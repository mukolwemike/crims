<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Portfolio\Orders;

use App\Cytonn\Models\Portfolio\PortfolioOrder;

class PortfolioOrderGenerator
{
    public function generate(PortfolioOrder $order)
    {
        return \PDF::loadView('portfolio.reports.orders.order_placement', [
            'order' => $order,
        ]);
    }
}
