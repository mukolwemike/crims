<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/28/18
 * Time: 9:17 AM
 */

namespace App\Cytonn\Portfolio\Orders;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Portfolio\Summary\FundPricingSummary;
use Carbon\Carbon;

class Compliance
{
    private $unitFund;

    private $subAssetClass;

    private $value;

    private $security;

    private $cache = [];


    public function __construct(
        UnitFund $unitFund,
        PortfolioSecurity $security = null,
        $value = null,
        SubAssetClass $subAssetClass = null,
        $date = null
    ) {
        $this->unitFund = $unitFund;

        $this->value = (int)$value;

        $this->date = $date ? $date : Carbon::today();
        
        $this->security = $security? $security : null;

        $this->subAssetClass = $subAssetClass ? $subAssetClass : $security->subAssetClass;
    }

    public function status()
    {
        $status = [];

        if (!$this->orderWithinLimit()) {
            $reason = (object)[
                'reason' => $this->benchmark()->name . ' Compliance limits violated',
                'description' => 'This order is not within compliance limits specified by ' .
                    $this->benchmark()->name .
                    ' of '
                    . $this->benchmarkLimit()->warn .
                    '% lower limit and ' .
                    $this->benchmarkLimit()->limit .
                    '% upper limit',
            ];

            array_push($status, $reason);
        }

        if (!is_null($this->noGoZone())) {
            $reason = (object)[
                'reason' => 'No go zone violated',
                'description' => 'No go zone for ' . $this->unitFund->name . ' on ' . $this->security->name,
            ];

            array_push($status, $reason);
        }

        return $status;
    }

    public function orderWithinLimit()
    {
        if (!$this->benchmarkLimit()) {
            return true;
        }

        if ($this->aboveLowerLimit() && $this->belowUpperLimit()) {
            return true;
        }

        return false;
    }

    public function aboveLowerLimit()
    {
        if ($this->value > $this->lowerLimit() || $this->value == $this->lowerLimit()) {
            return true;
        }

        return false;
    }

    public function belowUpperLimit()
    {
        if ($this->value < $this->upperLimit() || $this->value == $this->upperLimit()) {
            return true;
        }

        return false;
    }

    public function lowerLimit()
    {
        return ($this->benchmarkLimit()->warn / 100) * $this->aum();
    }

    public function upperLimit()
    {
        return ($this->benchmarkLimit()->limit / 100) * $this->aum();
    }

    public function benchmarkLimit()
    {
        return $this->unitFund->benchmarkLimit($this->subAssetClass);
    }


    public function warnStatus()
    {
        $warnStatus = [];

        if (!is_null($this->benchmarkLimit()) &&
            $this->value == $this->benchmarkWarnAmount() ||
            $this->value > $this->benchmarkWarnAmount() &&
            $this->value < $this->benchmarkLimitAmount()
        ) {
            $reason = (object)[
                'reason' => $this->benchmark()->name . ' limit almost violated',
                'description' => $this->benchmark()->name . ' limit almost violated by ' . $this->unitFund->name,
            ];

            array_push($warnStatus, $reason);
        }

        if (!is_null($this->liquidityLimits()) &&
            $this->value == $this->liquidityWarnAmount() ||
            $this->value > $this->liquidityWarnAmount() &&
            $this->value < $this->liquidityLimitAmount()
        ) {
            $reason = (object)[
                'reason' => 'Liquidity limit on ' . $this->subAssetClass->name . ' is almost violated',
                'description' => 'Liquidity limit on ' . $this->subAssetClass->name . ' by ' . $this->unitFund->name .
                    ' is almost violated'
            ];

            array_push($warnStatus, $reason);
        }

        return $warnStatus;
    }

    public function benchmark()
    {
        return $this->benchmarkLimit()->benchmark;
    }

    public function benchmarkLimitAmount()
    {
        if ($this->benchmarkLimit()) {
            return ($this->benchmarkLimit()->limit / 100) * $this->aum();
        }

        return $this->aum();
    }

    public function benchmarkWarnAmount()
    {
        if ($this->benchmarkLimit()) {
            return ($this->benchmarkLimit()->warn / 100) * $this->aum();
        }

        return $this->aum();
    }

    public function liquidityLimits()
    {
        if ($this->subAssetClass->liquid) {
            return $this->unitFund->liquidityLimits->last();
        }
    }

    public function liquidityLimitAmount()
    {
        if (!is_null($this->liquidityLimits())) {
            return ($this->liquidityLimits()->limit / 100) * $this->aum();
        }

        return $this->aum();
    }

    public function liquidityWarnAmount()
    {
        if ($this->liquidityLimits()) {
            return ($this->liquidityLimits()->warn / 100) * $this->aum();
        }

        return $this->aum();
    }

    public function noGoZone()
    {
        if (!$this->security) {
            return;
        }

        return $this->unitFund->noGoZone($this->security);
    }

    public function aum()
    {
        if ($this->cache('aum')) {
            return $this->cache('aum');
        }

        $summary = (new FundPricingSummary($this->unitFund, Carbon::today()))->totals();

        return $this->cache('aum', $summary['aum']);
    }

    private function cache($key, $value = null)
    {
        if ($value) {
            $this->cache[$key] = $value;

            return $value;
        }

        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        return null;
    }
}
