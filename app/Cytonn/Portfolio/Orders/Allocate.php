<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/28/18
 * Time: 9:17 AM
 */

namespace App\Cytonn\Portfolio\Orders;

use App\Cytonn\Models\ActiveStrategyTransactionCost;
use App\Cytonn\Models\Portfolio\EquityHoldingType;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Portfolio\PortfolioOrderAllocation;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundOrder;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Portfolio\Actions\DepositInvestment;

class Allocate
{
    private $allocation;

    private $order;

    private $security;

    protected $approval;

    public function __construct(PortfolioOrderAllocation $allocation, PortfolioTransactionApproval $approval)
    {
        $this->allocation = $allocation;

        $this->order = $allocation->order;

        $this->security = $allocation->order->security;

        $this->approval = $approval;
    }

    /**
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function allocate()
    {
        $type = $this->security->subAssetClass->assetClass->slug;

        switch ($type) {
            case 'equities':
                return $this->equityHolding();
                break;

            case 'deposits':
                return $this->depositHolding();
                break;

            case 'bonds':
                return $this->depositHolding();
                break;

            default:
                throw new ClientInvestmentException("Portfolio asset class not supported");
                break;
        }
    }

    /**
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function equityHolding()
    {
        $orderType = $this->order->type->slug;

        switch ($orderType) {
            case 'buy':
                return $this->buyEquityShares();
                break;

            case 'sell':
                return $this->sellEquityShares();
                break;

            default:
                throw new ClientInvestmentException("Order type is not supported");
                break;
        }
    }

    public function buyEquityShares()
    {
        return $this->order->unitFundOrders->each(function ($fundOrder) {
            $data = $this->setPurchaseData($fundOrder);

            $type = EquityHoldingType::where('slug', 'purchase')->first();

            $holding = $this->security->repo->purchase($data, $type, $fundOrder->unitFund, $this->allocation);

            $holding->allocation()->associate($this->allocation);

            $trans = $this->security->repo->credit($holding, $this->approval);

            $holding->custodialTransaction()->associate($trans);

            $holding->approval()->associate($this->approval);

            $holding->save();
        });
    }

    public function setPurchaseData(UnitFundOrder $fundOrder)
    {
        return $data = [
            'cost' => $this->allocation->price,
            'date' => $this->allocation->date,
            'number' => $this->sharesToAllocate($fundOrder),
            'fee_per_share' => $this->feePerShare($fundOrder)
        ];
    }

    public function settlementCharges($approval, $cost)
    {
        return collect($approval->payload['fees'])->map(function ($fee) use ($cost) {
            return [
                'amount' => (isset($fee['percentage']))
                    ? (floatval($fee['percentage']) / 100) * $cost
                    : $fee['amount']
            ];
        })->sum('amount');
    }

    public function sharesToAllocate(UnitFundOrder $fundOrder)
    {
        $shares_ordered = $this->order->unitFundOrders->sum('shares');

        $shared_ordered_for_fund = abs($fundOrder->shares);

        $shares = ($shared_ordered_for_fund / $shares_ordered) * $this->allocation->shares;

        return round($shares / 100) * 100;
    }

    public function sellEquityShares()
    {
        return $this->order->unitFundOrders->each(function ($fundOrder) {
            $data = $this->setSaleData($fundOrder);

            $sale = new EquityShareSale();

            $sale->fill($data);

            $sale->unitFund()->associate($fundOrder->unitFund);

            $sale->security()->associate($this->security);

            $sale->approval()->associate($this->approval);

            $sale->allocation()->associate($this->allocation);

            $sale->save();

            $this->security->repo->debit($sale, $this->approval);
        });
    }

    public function setSaleData(UnitFundOrder $fundOrder)
    {
        return $data = [
            'sale_price' => $this->allocation->price,
            'date' => $this->allocation->date,
            'number' => $this->sharesToAllocate($fundOrder),
            'fee_per_share' => $this->feePerShare($fundOrder)
        ];
    }

    public function feePerShare(UnitFundOrder $fundOrder)
    {
//        $value = $this->sharesToAllocate($fundOrder) * $this->allocation->price;

        $value = $this->allocation->shares * $this->allocation->price;

        $charges = $this->settlementCharges($this->approval, $this->allocation->price);

        $transaction_cost = (new ActiveStrategyTransactionCost())->latest()->first()->rate;

        $cost = ($transaction_cost / 100) * $value;

        $total_charges = $charges + $cost;

//        return $total_charges /$this->sharesToAllocate($fundOrder);
        return $total_charges / $this->allocation->shares;
    }

    public function depositHolding()
    {
        return $this->order->unitFundOrders->each(function ($order) {
            $data = $this->setDepositData($order);

            $deposit = (new DepositInvestment())->invest($data);

            $deposit->investment->allocation()->associate($this->allocation);

            $deposit->investment->save();
        });
    }

    public function setDepositData(UnitFundOrder $order)
    {
        $data = $this->approval->payload;

        $amount = $this->amountToAllocate($order);

        return [
            'amount' => $amount,
            'portfolio_security_id' => $this->security->id,
            'invested_date' => $data['invested_date'],
            'maturity_date' => isset($data['maturity_date']) ? $data['maturity_date'] : null,
            'interest_rate' => isset($data['ytm']) ? $data['ytm'] : $this->allocation->rate,
            'coupon_rate' => isset($data['coupon_rate']) ? $data['coupon_rate'] : $this->allocation->rate,
            'coupon_payment_duration' => (isset($data['coupon_payment_duration']))
                ? $data['coupon_payment_duration'] : null,
            'approval_id' => $this->approval->id,
            'unit_fund_id' => $order->unitFund->id,
            'deposit_type_id' => $data['deposit_type_id'],
            'tax_rate_id' => $data['tax_rate_id'],
            'custodial_account_id' => $data['custodial_account_id'],
            'purchase_cost' => isset($data['purchase_cost']) ? $data['purchase_cost'] : $amount,
            'purchase_date' => isset($data['purchase_date']) ? $data['purchase_date'] : $data['invested_date'],
            'bond_clean_price' => isset($data['clean_price']) ? $data['clean_price'] : 100,
            'bond_dirty_price' => isset($data['dirty_price']) ? $data['dirty_price'] : 100,
            'bond_par_price' => isset($data['par_price']) ? $data['par_price'] : 100,
            'bond_type_id' => isset($data['bond_type_id']) ? $data['bond_type_id'] : null
        ];
    }

    public function amountToAllocate(UnitFundOrder $fundOrder)
    {
        $amount_ordered = $this->order->unitFundOrders->sum('amount');

        $amount_ordered_for_fund = abs($fundOrder->amount);

        $amount = ($amount_ordered_for_fund / $amount_ordered) * $this->allocation->amount;

        return round($amount);
    }

    public function bondHolding()
    {
        dd($this->allocation);
    }
}
