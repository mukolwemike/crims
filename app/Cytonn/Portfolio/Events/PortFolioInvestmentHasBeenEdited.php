<?php
/**
 * Date: 16/03/2016
 * Time: 6:26 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Events;

use App\Cytonn\Models\Portfolio\DepositHolding;

class PortFolioInvestmentHasBeenEdited
{
    public $newInvestment;

    public $oldInvestment;

    /**
     * PortForlioInvestmentHasBeenEdited constructor.
     *
     * @param $newInvestment
     * @param $oldInvestment
     */
    public function __construct(DepositHolding $newInvestment, DepositHolding $oldInvestment)
    {
        $this->newInvestment = $newInvestment;
        $this->oldInvestment = $oldInvestment;
    }
}
