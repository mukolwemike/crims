<?php
/**
 * Date: 14/12/2015
 * Time: 2:41 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Events;

use App\Cytonn\Models\PortfolioTransactionApproval;

class PortfolioTransactionApproved
{
    public $transaction;

    /**
     * TransactionApproved constructor.
     *
     * @param $transaction
     */
    public function __construct(PortfolioTransactionApproval $transaction)
    {
        $this->transaction = $transaction;
    }
}
