<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 8:44 AM
 */

namespace Cytonn\Portfolio\Events;

use App\Cytonn\Models\PortfolioTransactionApproval;

class ApprovalSuccessful
{
    public $next;

    public $approval;

    /**
     * ApprovalSuccessful constructor.
     *
     * @param $next
     * @param $approval
     */
    public function __construct(PortfolioTransactionApproval $approval, $next)
    {
        $this->next = $next;

        $this->approval = $approval;
    }
}
