<?php

namespace Cytonn\Portfolio\Events;

use App\Cytonn\Models\CustodialTransaction;

class TransferHasBeenMade
{
    public $sending;

    public $receiving;

    /**
     * TransferHasBeenMade constructor.
     *
     * @param CustodialTransaction $sending
     * @param CustodialTransaction $receiving
     */
    public function __construct(CustodialTransaction $sending, CustodialTransaction $receiving)
    {
        $this->sending = $sending;
        $this->receiving = $receiving;
    }
}
