<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/20/16
 * Time: 8:43 AM
 */

namespace Cytonn\Portfolio\Events;

use App\Cytonn\Models\PortfolioTransactionApproval;

class ApprovalIsReady
{
    public $approval;

    /**
     * ApprovalIsReady constructor.
     *
     * @param $approval
     */
    public function __construct(PortfolioTransactionApproval $approval)
    {
        $this->approval = $approval;
    }
}
