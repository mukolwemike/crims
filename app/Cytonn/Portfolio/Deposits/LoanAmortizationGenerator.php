<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/4/18
 * Time: 12:54 PM
 */

namespace App\Cytonn\Portfolio\Deposits;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Core\DataStructures\Carbon;

class LoanAmortizationGenerator
{
    protected $holding;

    public function __construct(DepositHolding $holding)
    {
        $this->holding = $holding;
    }

    public function generate()
    {
        $start = Carbon::parse($this->holding->invested_date);

        $end = Carbon::parse($this->holding->maturity_date)->addMonth(1);

        return $this->schedule($start, $end, 1);
    }

    public function schedule(Carbon $start, Carbon $end, $interval)
    {
        $schedules = [];

        $run_date = $start->addMonthsNoOverflow($interval)->copy();

        $counter = 1;

        $amount = (float) $this->holding->amount;

        $prev_interest = 0;

        while ($end->gt($run_date) && $amount > 0) {
            $interest = $this->interest($amount);

            $cumulative_interest = $interest + $prev_interest;

            $payment = $this->scheduledPayment();

            $balance = round(($amount + $interest) - $payment, 2);

            $principal = $payment - $interest;

            $schedules[] = [
                'date' => $run_date->copy()->toFormattedDateString(),
                'beginning_balance' => AmountPresenter::currency($amount),
                'scheduled_payment' => AmountPresenter::currency($payment),
                'extra_payment' => '',
                'total_payment' => AmountPresenter::currency($payment),
                'principal' => AmountPresenter::currency($principal),
                'interest' => AmountPresenter::currency($interest),
                'ending_balance' => AmountPresenter::currency($balance),
                'cumulative_interest' => AmountPresenter::currency($cumulative_interest)
            ];

            $run_date = $run_date->addMonthsNoOverflow($interval);

            $amount = $balance;

            $prev_interest += $interest;

            $counter++;
        }

        return $schedules;
    }

    public function scheduledPayment()
    {
        $invested = Carbon::parse($this->holding->invested_date);

        $maturity = Carbon::today();

        $period = $invested->diffInMonths($maturity);

        $interestFactor = 1 - pow(1 + ($this->holding->interest_rate/1200), -($period));

        return $this->averageInterest() / $interestFactor;
    }

    public function interest($amount)
    {
        return $amount * ($this->holding->interest_rate/1200);
    }

    public function averageInterest()
    {
        return ((int)  $this->holding->amount * ($this->holding->interest_rate/100))/12;
    }
}
