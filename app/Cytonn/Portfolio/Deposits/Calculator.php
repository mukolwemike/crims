<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Portfolio\Deposits;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioInvestmentTopup;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;

class Calculator extends \Cytonn\Structured\Calculations\Calculator
{
    protected $investment;

    public static $count = 0;
    protected $date;
    protected $prepared;
    protected $as_at_next_day;
    protected $interestRate = null;


    /**
     * Calculator constructor.
     *
     * @param DepositHolding $investment
     * @param Carbon         $date
     * @param bool           $as_at_next_day
     */
    public function __construct(DepositHolding $investment, Carbon $date, $as_at_next_day = false)
    {
        $this->investment = $investment;
        $this->date = $date;
        $this->as_at_next_day = $as_at_next_day;

        $this->prepared = $this->prepare();
    }

    protected function prepare()
    {
        $actions = $this->getActions();
        $principal = $this->investment->bond_purchase_cost ?
            $this->investment->bond_purchase_cost : $this->investment->amount;

        $netInterestCF = 0;
        $startDate = $this->investment->invested_date->copy();

        if($this->investment->bond_purchase_date) {
            $startDate = Carbon::parse($this->investment->bond_purchase_date);
        }

        $withdrawal = null;

        $actions = $actions->map(
            function ($w) use (
                &$principal,
                &$netInterestCF,
                &$withdrawal,
                &$startDate,
                &$balance
            ) {

                if ($w instanceof PortfolioInvestmentTopup) {
                    $type = 'topup';
                    $amount = abs($w->amount);
                    $description = $w->description ? $w->description : 'Topup';
                } elseif ($w instanceof PortfolioInvestmentRepayment) {
                    $type = $w->type->slug;
                    $amount = -abs($w->amount);
                    $description = $w->portfolio_repayment_action_id == 2 ? 'Rollover' : $w->type->name;
                } elseif ($w instanceof EmptyModel) {
                    $type = 'empty';
                    $amount = 0;
                    $description = 'Empty';
                } else {
                    throw new ClientInvestmentException("Only topup/repayments actions accepted");
                }

                $prepared = $this->calculate(
                    $principal,
                    $startDate,
                    $w->date,
                    $netInterestCF,
                    $amount,
                    $type
                );

                $prepared->type = $type;
                $prepared->date = $w->date;
                $prepared->amount = $amount;
                $prepared->action = $w;
                $prepared->description = $description;

                $principal = $prepared->principal;
                $netInterestCF = $prepared->net_interest_after;
                $startDate = $w->date;
                $withdrawal = $w;

                return $prepared;
            }
        );

        $start = $this->investment->invested_date;
        if($this->investment->bond_purchase_date) {
            $start = $this->investment->bond_purchase_date;
        }

        if ($withdrawal) {
            $start = $withdrawal->date;
        }

        $last = $this->calculate($principal, $start, $this->date, $netInterestCF, 0, null);
        $last->type = 'end';
        $last->date = $this->date;
        $last->investment = null;
        $last->description = 'Total';

        $amortized = $this->amortizedPrincipal();
        $last->armotized_principal = $amortized;
        $last->total = $last->total + $amortized;

        return (object)[
            'investment' => $this->investment,
            'actions' => $actions->all(),
            'total'   => $last
        ];
    }

    protected function calculate(
        $principal,
        Carbon $start,
        Carbon $end = null,
        $netInterestCF,
        $changeAmount,
        $type
    ) {
        $is_first = $this->investment->invested_date->copy()->startOfDay()->eq($start->copy()->startOfDay());

        if($this->investment->bond_purchase_date) {
            $is_first = $this->investment->bond_purchase_date->copy()->startOfDay()->eq($start->copy()->startOfDay());
        }

        if (!$is_first) {
            $start = $start->copy()->addDay();
        }

        $start = $this->limitStart($start->copy());

        $is_before_end_day = $end->copy()->startOfDay()->lt($this->date->copy()->startOfDay());

        if ($is_before_end_day || $this->as_at_next_day) {
            $end = $end->copy()->addDay();
        }

        $end = $this->limitEnd($end->copy());

        $rate = (is_null($this->interestRate)) ? $this->investment->interest_rate : $this->interestRate;

        $gross_i = $this->getGrossInterest($principal, $rate, $start, $end);
        $net_i = $this->getNetInterest($gross_i, $this->investment);
        $net_i_cumulative = $netInterestCF + $net_i;
        $gross_i_cumulative = $this->calculateGrossFromNet($net_i_cumulative, $this->investment);
        $net_i_after_withdr = $net_i_cumulative;
        if (! in_array($type, ['topup', 'principal_repayment'])) {
            $net_i_after_withdr = max([$net_i_cumulative + $changeAmount, 0]);
        }
        $gross_i_after_withdr = $this->calculateGrossFromNet($net_i_after_withdr, $this->investment);
        $wtax_after_withdr = $gross_i_after_withdr - $net_i_after_withdr;

        $total = $principal + $net_i_cumulative + $changeAmount;

        $new_principal = min([$total, $principal]);

        if (in_array($type, ['principal_repayment', 'topup'])) {
            $new_principal = $principal + $changeAmount;

            $total = $new_principal + $net_i_cumulative;
        }

        return (object)[
            'original_principal' => (float) $principal,
            'principal' => (float) $new_principal,
            'gross_interest' => $gross_i,
            'net_interest' => $net_i,
            'wtax' => $gross_i - $net_i,
            'gross_interest_cumulative' => $gross_i_cumulative,
            'net_interest_cumulative' => $net_i_cumulative,
            'wtax_cumulative' => $gross_i_cumulative - $net_i_cumulative,
            'gross_interest_after' => $gross_i_after_withdr,
            'net_interest_after' => $net_i_after_withdr,
            'wtax_after' => $wtax_after_withdr,
            'total' => $total
        ];
    }

    private function getActions()
    {
        $repayments = $this->investment->repayments()->before($this->date)->get();

        $topups = $this->investment->topupsTo()->before($this->date)->get();

        $actions = $topups->merge($repayments->all())
            ->filter(function ($action) {
                return $action->date->lte($this->date) && ($action->amount != 0);
            });

        return $actions->sortByDate('date', 'ASC')->values();
    }

    protected function limitStart(Carbon $start)
    {

        if ($this->investment->bond_purchase_date && $this->investment->bond_purchase_date->gte($start)) {
            return $this->investment->bond_purchase_date;
        }

        if ($this->investment->invested_date->gte($start)) {
            return $this->investment->invested_date;
        }

        return $start;
    }

    protected function limitEnd(Carbon $end)
    {
        $investmentEnd = ($this->investment->withdrawal_date &&
        $this->investment->withdrawal_date->lte($this->investment->maturity_date)) ?
            $this->investment->withdrawal_date :
            $this->investment->maturity_date;

        return $investmentEnd->lte($end) ? $investmentEnd : $end;
    }

    public function getPrepared()
    {
        return $this->prepared;
    }

    public function netInterestRepaid()
    {
        return $this->netInterestBeforeDeductions() - $this->netInterest();
    }

    public function grossInterestRepaid()
    {
        return $this->calculateGrossFromNet($this->netInterestRepaid(), $this->investment->taxable);
    }

    public function adjustedGrossInterest()
    {
        return $this->grossInterest();
    }

    protected function nominalValue()
    {
        $nominal = $this->investment->purchase_cost;

        return $nominal > 0 ? $nominal : $this->investment->amount;
    }

    public function consideration()
    {
        $consideration = $this->investment->amount;

        if($this->investment->bond_dirty_price && $this->investment->bond_par_price) {
            $consideration = $this->investment->amount
                * $this->investment->bond_dirty_price/$this->investment->bond_par_price;
        }

        return $consideration;
    }

    public function amortizedPrincipal(Carbon $date = null)
    {
        $date = $date ? $date : $this->date;
        $days = $date->diffInDays($this->investment->invested_date);

        if ($days == 0) {
            return 0;
        }

        return $days * $this->dailyAmortization();
    }

    public function dailyAmortization()
    {
        $cost = $this->investment->amount;
        $nominal = $this->nominalValue();

        $tenor = $this->investment->maturity_date->diffInDays($this->investment->invested_date);

        if ($tenor == 0 || $cost == $nominal) {
            return 0;
        }

        return (1/$tenor) * ($nominal - $cost);
    }

    public function dailyGrossInterest()
    {
        $principal = $this->principal();

        return (1/365) * $this->investment->interest_rate * (1/100) * $principal;
    }

    public function dailyNetInterest()
    {
        $gross = $this->dailyGrossInterest();

        return $this->getNetInterest($gross, $this->investment);
    }
}
