<?php
/**
 * Date: 24/02/2018
 * Time: 23:02
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Deposits;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use Carbon\Carbon;
use Cytonn\Portfolio\Summary\Analytics;

class Analysis
{
    /**
     * @var Currency
     */
    private $currency;

    private $fundManager;

    private $date;

    /**
     * Create a new job instance.
     *
     * @param Currency $currency
     * @param FundManager $fundManager
     * @param Carbon $date
     */
    public function __construct(Currency $currency, FundManager $fundManager, Carbon $date)
    {
        $this->fundManager = $fundManager;
        $this->date = $date;
        $this->currency = $currency;
    }

    public function generate()
    {
        $analytics = (new Analytics($this->fundManager, $this->date))->setCurrency($this->currency);

        $balance = $analytics->custodialBalance();

        $total = $analytics->adjustedMarketValue() + $balance;

        $classes = SubAssetClass::all()->map(function ($class) use ($total) {
            $analytics = (new Analytics($this->fundManager, $this->date))->setCurrency($this->currency);

            $analytics->setFundType($class);

            $amount = $analytics->adjustedMarketValue();

            return [
                'Name'                       => $class->name,
                'Cost value'                 =>  $analytics->costValue(),
                'Gross Interest'             =>  $analytics->grossInterest(),
                'Interest repaid'            =>  $analytics->interestRepaid(),
                'Adjusted Gross Interest'    =>  $analytics->adjustedGrossInterest(),
                'Adjusted Market Value'      =>  $amount,
                'Market Value'               =>  $analytics->marketValue(),
                'Exposure'                   =>  $total != 0 ? 100 * $amount/$total : 0
            ];
        })->filter(function ($class) {
            return $class['Market Value'] != 0;
        });

        return $classes->push([
            'Name'                       =>  'Cash',
            'Cost value'                 =>  $balance,
            'Gross Interest'             =>  0,
            'Interest repaid'            =>  0,
            'Adjusted Gross Interest'    =>  0,
            'Adjusted Market Value'      =>  $balance,
            'Market Value'               =>  $balance,
            'Exposure'                   =>  $total != 0 ? 100 * $balance/$total : 0
        ]);
    }
}
