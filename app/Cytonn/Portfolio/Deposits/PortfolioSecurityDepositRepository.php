<?php
/**
 * Date: 30/01/2018
 * Time: 20:48
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Deposits;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Cytonn\Investment\InvestmentsRepository;

class PortfolioSecurityDepositRepository
{
    protected $security;
    /**
     * @var Currency
     */
    private $currency;

    private static $calculated = [];

    /**
     * DepositRepository constructor.
     *
     * @param PortfolioSecurity|null $security
     * @param Currency $currency
     */
    public function __construct(PortfolioSecurity $security, Currency $currency)
    {
        $this->security = $security;
        $this->currency = $currency;
    }

    protected function prepared($date, $as_at_date)
    {
        $hash = sha1('deposit_security_id_' . $this->security->id . '_date_' . $date->toDateString() .
            '_currency_' . $this->currency->code . '_next_' . (string)$as_at_date);

        if (isset(static::$calculated[$hash])) {
            return static::$calculated[$hash];
        }

        $prepared = $this->security->depositHoldings()->activeOnDate($date)
            ->currency($this->currency)
            ->get()
            ->map(
                function (DepositHolding $deposit) use ($date, $as_at_date) {
                    return $deposit->calculate($date, $as_at_date);
                }
            );

        $this->setCalculated($prepared, $hash);

        return $prepared;
    }

    private function setCalculated($calculated, $hash)
    {
        static::$calculated[$hash] = $calculated;

        if (count(static::$calculated) > InvestmentsRepository::MAX_STORED) {
            $first_key = array_first(array_keys(static::$calculated));

            unset(static::$calculated[$first_key]);
        }
    }

    public function principal($date, $as_at_next_day = false)
    {
        return $this->prepared($date, $as_at_next_day)->sum(
            function (Calculator $calculator) {
                return $calculator->principal();
            }
        );
    }

    public function grossInterest($date, $as_at_next_day = false)
    {
        return $this->prepared($date, $as_at_next_day)->sum(
            function (Calculator $calculator) {
                return $calculator->grossInterest();
            }
        );
    }

    public function netInterest($date, $as_at_next_day = false)
    {
        return $this->prepared($date, $as_at_next_day)->sum(
            function (Calculator $calculator) {
                return $calculator->netInterest();
            }
        );
    }

    public function total($date, $as_at_next_day = false)
    {
        return $this->prepared($date, $as_at_next_day)->sum(
            function (Calculator $calculator) {
                return $calculator->total();
            }
        );
    }

    public function withholdingTax($date, $as_at_next_day = false)
    {
        return $this->prepared($date, $as_at_next_day)->sum(
            function (Calculator $calculator) {
                return $calculator->withholdingTax();
            }
        );
    }

    public function totalRepayments($date, $type = null)
    {
        return $this->security->depositHoldings()
            ->get()
            ->sum(
                function (DepositHolding $holding) use ($type, $date) {
                    $rep = $holding->repayments()->before($date);

                    if ($type) {
                        $rep = $rep->ofType($type);
                    }

                    return $rep->sum('amount');
                }
            );
    }
}
