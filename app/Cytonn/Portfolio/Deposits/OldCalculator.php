<?php
/**
 * Date: 30/01/2018
 * Time: 11:54
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Portfolio\Deposits;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use Carbon\Carbon;

class OldCalculator extends \Cytonn\Structured\Calculations\Calculator
{
    protected $investment;

    public static $count = 0;
    protected $date;
    protected $prepared;
    protected $as_at_next_day;
    protected $interestRate = null;


    /**
     * Calculator constructor.
     *
     * @param DepositHolding $investment
     * @param Carbon $date
     * @param bool $as_at_next_day
     */
    public function __construct(DepositHolding $investment, Carbon $date, $as_at_next_day = false)
    {
        $this->investment = $investment;
        $this->date = $date;
        $this->as_at_next_day = $as_at_next_day;

        $this->prepared = $this->prepare();
    }

    protected function prepare()
    {
        $withdrawals = $this->investment->repayments()->before($this->date)->oldest('date')->get();

        $principal = $this->investment->amount;
        $nominal = $this->nominalValue();

        $netInterestCF = 0;
        $startDate = $this->investment->invested_date->copy();

        $withdrawal = null;

        $actions = $withdrawals->map(
            function ($w) use (
                &$nominal,
                &$principal,
                &$netInterestCF,
                $withdrawals,
                &$withdrawal,
                &$startDate,
                &$balance
            ) {
                $amount = -abs($w->amount);

                $prepared = $this
                    ->calculate($nominal, $principal, $startDate, $w->date, $netInterestCF, $amount, $w->type->slug);

                $prepared->type = $w->type->slug;
                $prepared->date = $w->date;
                $prepared->amount = $amount;
                $prepared->action = $w;
                $prepared->description = $w->type->name;

                $principal = $prepared->principal;
                $nominal = $prepared->nominal;
                $netInterestCF = $prepared->net_interest_after;
                $startDate = $w->date;
                $withdrawal = $w;

                return $prepared;
            }
        );

        $start = $this->investment->invested_date;
        if ($withdrawal) {
            $start = $withdrawal->date;
        }

        $investment = $this->investment;
        if ($this->investment->withdrawn && $investment->withdrawal_date->lte($this->date)) {
            $total = $this->calculate(
                $nominal,
                $principal,
                $start,
                $this->date,
                $netInterestCF,
                0,
                null
            )->total;

            if ($this->investment->withdraw_amount > 0) {
                $amt = -$this->investment->withdraw_amount;
                $p_withdrawal = $this->calculate(
                    $nominal,
                    $principal,
                    $start,
                    $investment->withdrawal_date,
                    $netInterestCF,
                    $amt,
                    null
                );

                $p_withdrawal->type = null;
                $p_withdrawal->date = $investment->withdrawal_date;
                $p_withdrawal->amount = $amt;
                $p_withdrawal->action = null;
                $p_withdrawal->description = 'Withdrawal';

                $ex = $this->investment->repayments()->where('date', $investment->withdrawal_date)
                    ->where('amount', abs($amt))->where('portfolio_repayment_action_id', 1)
                    ->where('type_id', 3)->first();

                if (is_null($ex)) {
                    $actions->push($p_withdrawal);

                    $principal = $p_withdrawal->principal;
                    $nominal = $p_withdrawal->nominal;
                    $netInterestCF = $p_withdrawal->net_interest_after;
                    $start = $investment->withdrawal_date;
                }
            }

            if ($this->investment->rolled || $this->investment->partial_withdraw) {
                $amt = -($total - $this->investment->withdraw_amount);
                $r_withdrawal = $this->calculate(
                    $nominal,
                    $principal,
                    $start,
                    $this->date,
                    $netInterestCF,
                    $amt,
                    null
                );
                $r_withdrawal->type = null;
                $r_withdrawal->date = $investment->withdrawal_date;
                $r_withdrawal->amount = $amt;
                $r_withdrawal->action = null;
                $r_withdrawal->description = 'Rollover';

                $ex = $this->investment->repayments()->where('date', $investment->withdrawal_date)
                    ->where('amount', abs($amt))->where('portfolio_repayment_action_id', 2)
                    ->where('type_id', 3)->first();

                if (is_null($ex)) {
                    $actions->push($r_withdrawal);

                    $principal = $r_withdrawal->principal;
                    $nominal = $r_withdrawal->nominal;
                    $netInterestCF = $r_withdrawal->net_interest_after;
                    $start = $investment->withdrawal_date;
                }
            }

            $withdrawal = new PortfolioInvestmentRepayment(['date' => $this->investment->withdrawal_date]);
        }

        if ($withdrawal) {
            $start = $withdrawal->date;
        }

        $last = $this->calculate($nominal, $principal, $start, $this->date, $netInterestCF, 0, null);
        $last->type = 'end';
        $last->date = $this->date;
        $last->investment = null;
        $last->description = 'Total';

        $amortized = $this->amortizedPrincipal();
        $last->armotized_principal = $amortized;
        $last->total = $last->total + $amortized;

        return (object)[
            'investment' => $this->investment,
            'actions' => $actions->all(),
            'total' => $last
        ];
    }

    protected function calculate(
        $nominal,
        $principal,
        Carbon $start,
        Carbon $end = null,
        $netInterestCF,
        $changeAmount,
        $type
    ) {
        $is_first = $this->investment->invested_date->copy()->startOfDay()->eq($start->copy()->startOfDay());

        if (!$is_first) {
            $start = $start->copy()->addDay();
        }

        $start = $this->limitStart($start->copy());

        $is_before_end_day = $end->copy()->startOfDay()->lt($this->date->copy()->startOfDay());

        if ($is_before_end_day || $this->as_at_next_day) {
            $end = $end->copy()->addDay();
        }

        $end = $this->limitEnd($end->copy());


        $taxable = $this->investment->taxable;
        $rate = (is_null($this->interestRate)) ? $this->investment->interest_rate : $this->interestRate;

        $gross_i = $this->getGrossInterest($nominal, $rate, $start, $end);
        $net_i = $this->getNetInterest($gross_i, $this->investment);
        $net_i_cumulative = $netInterestCF + $net_i;
        $gross_i_cumulative = $this->calculateGrossFromNet($net_i_cumulative, $this->investment);
        $net_i_after_withdr = $net_i_cumulative;
        if ($type != 'principal_repayment') {
            $net_i_after_withdr = max([$net_i_cumulative + $changeAmount, 0]);
        }
        $gross_i_after_withdr = $this->calculateGrossFromNet($net_i_after_withdr, $this->investment);
        $wtax_after_withdr = $gross_i_after_withdr - $net_i_after_withdr;

        $total = $principal + $net_i_cumulative + $changeAmount;
        $total_nominal = $nominal + $net_i_cumulative + $changeAmount;

        $new_principal = min([$total, $principal]);
        $new_nominal = min([$total_nominal, $nominal]);

        if ($type == 'principal_repayment') {
            $new_principal = $principal + $changeAmount;
            $new_nominal = $nominal + $changeAmount;

            $total = $new_principal + $net_i_cumulative;
            $nominal = $new_nominal + $net_i_cumulative;
        }

        return (object)[
            'original_principal' => (float)$principal,
            'principal' => (float)$new_principal,
            'original_nominal' => (float)$nominal,
            'nominal' => (float)$new_nominal,
            'gross_interest' => $gross_i,
            'net_interest' => $net_i,
            'wtax' => $gross_i - $net_i,
            'gross_interest_cumulative' => $gross_i_cumulative,
            'net_interest_cumulative' => $net_i_cumulative,
            'wtax_cumulative' => $gross_i_cumulative - $net_i_cumulative,
            'gross_interest_after' => $gross_i_after_withdr,
            'net_interest_after' => $net_i_after_withdr,
            'wtax_after' => $wtax_after_withdr,
            'total' => $total
        ];
    }

    protected function limitStart(Carbon $start)
    {
        if ($this->investment->invested_date->gte($start)) {
            return $this->investment->invested_date;
        }

        return $start;
    }

    protected function limitEnd(Carbon $end)
    {
        if ($this->investment->withdrawal_date) {
            if ($this->investment->withdrawal_date->lte($end)) {
                return $this->investment->withdrawal_date;
            }
        }

        if ($this->investment->maturity_date->lte($end)) {
            return $this->investment->maturity_date;
        }

        return $end;
    }

    public function getPrepared()
    {
        return $this->prepared;
    }

    public function netInterestRepaid()
    {
        return $this->netInterestBeforeDeductions() - $this->netInterest();
    }

    public function grossInterestRepaid()
    {
        return $this->calculateGrossFromNet($this->netInterestRepaid(), $this->investment->taxable);
    }

    public function adjustedGrossInterest()
    {
        return $this->grossInterest();
    }

    protected function nominalValue()
    {
        $nominal = $this->investment->nominal_value;

        return $nominal > 0 ? $nominal : $this->investment->amount;
    }

    public function amortizedPrincipal(Carbon $date = null)
    {
        $cost = $this->investment->amount;
        $nominal = $this->nominalValue();

        $date = $date ? $date : $this->date;

        $tenor = $this->investment->maturity_date->diffInDays($this->investment->invested_date);
        $days = $date->diffInDays($this->investment->invested_date);

        if ($days == 0 || $tenor == 0 || $cost == $nominal) {
            return 0;
        }

        return ($days / $tenor) * ($nominal - $cost);
    }
}
