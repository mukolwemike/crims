<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Hr;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Oauth\ApiCredentialManager;
use GuzzleHttp\Client as GuzzleClient;

class HrManager
{
    use ApiCredentialManager;

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCommissionRecipientData($all = null)
    {
        if ($all) {
            CommissionRecepient::query()->update([
                'employee_id' => null,
                'employee_number' => null
            ]);

            $recipients = CommissionRecepient::where('recipient_type_id', '!=', 3)->get();
        } else {
            $recipients = CommissionRecepient::whereNull('employee_id')
                ->orWhereNull('employee_number')->get();
        }

        $recipientArray = array();

        foreach ($recipients as $recipient) {
            if (str_contains($recipient->email, 'cytonn')) {
                $recipientArray[] = $recipient->email;
            }
        }

        $accessToken = $this->seekHrAuthorization();

        if (is_null($accessToken)) {
            return;
        }

        $client = new GuzzleClient();

        $response = $client->request('GET', getenv('HR_COMMISSION_EMPLOYEE'), [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => [
                'emails' => $recipientArray,
            ]
        ]);

        try {
            $data = \GuzzleHttp\json_decode($response->getBody());

            $this->updateCommissionRecipientData($data);
        } catch (\Exception $e) {
        }
    }

    /**
     * @param $recipients
     */
    private function updateCommissionRecipientData($recipients)
    {
        foreach ($recipients as $recipientData) {
            CommissionRecepient::where('email', $recipientData->email)->update([
                'employee_id' => $recipientData->id,
                'employee_number' => $recipientData->employee_number
            ]);
        }
    }

    /**
     * Update the clients with data from HR
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStaffClients()
    {
        $page = 0;

        $perPage = 500;

        do {
            $clients = $this->getClients($page, $perPage);

            if (count($clients) > 0) {
                $this->sendClientDataToHr($clients);
            }

            $page++;
        } while (count($clients) > 0);
    }

    /**
     * @param $page
     * @param $perPage
     * @return array
     */
    private function getClients($page, $perPage)
    {
        $clients = Client::whereNull('employee_number')->skip($page * $perPage)->take($perPage)->get();

        $clientArray = array();

        foreach ($clients as $client) {
            $email = $phone = $kra = null;

            if (isNotEmptyOrNull($client->contact->email)) {
                $email = $client->contact->email;
            }

            if (isNotEmptyOrNull($client->contact->phone)) {
                $phone = $client->contact->phone;
            }

            if (isNotEmptyOrNull($client->contact->pin_no)) {
                $kra = $client->contact->pin_no;
            }

            if ($email || $phone || $kra) {
                $clientArray[] = [
                    'id' => $client->id,
                    'email' => $client->contact->email,
                    'phone' => $client->contact->phone,
                    'pin_number' => $client->pin_no
                ];
            }
        }

        return $clientArray;
    }

    /**
     * @param $clients
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendClientDataToHr($clients)
    {
        $accessToken = $this->seekHrAuthorization();

        if (is_null($accessToken)) {
            return;
        }

        $client = new GuzzleClient();

        $response = $client->request('GET', getenv('HR_CLIENT_EMPLOYEE'), [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => [
                'clients' => $clients,
            ]
        ]);

        try {
            $data = \GuzzleHttp\json_decode($response->getBody());

            $this->updateClientData($data);
        } catch (\Exception $e) {
        }
    }

    /**
     * @param $clients
     */
    private function updateClientData($clients)
    {
        foreach ($clients as $client) {
            Client::where('id', $client->id)->update([
                'employee_number' => $client->employee_number
            ]);
        }
    }
}
