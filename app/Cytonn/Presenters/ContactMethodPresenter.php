<?php
/**
 * Date: 9/8/15
 * Time: 12:49 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\ContactMethod;

class ContactMethodPresenter
{
    public static function present($id)
    {
        $method = ContactMethod::find($id);

        if (is_null($method)) {
            return null;
        }

        return $method->description;
    }
}
