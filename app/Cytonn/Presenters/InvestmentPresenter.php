<?php
/**
 * Date: 09/10/2015
 * Time: 9:29 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\Setting;
use Carbon\Carbon;

/**
 * Class InvestmentPresenter
 *
 * @package Cytonn\Presenters
 */
class InvestmentPresenter
{
    /**
     * @param $investmentId
     * @return int
     */
    public static function tenorInMonths($investmentId)
    {
        $investment = ClientInvestment::find($investmentId);

        if (is_null($investment)) {
            return 0;
        }

        $invested_date = new Carbon($investment->invested_date);

        $maturity_date = new Carbon($investment->maturity_date);

        return $invested_date->copy()->diffInMonths($maturity_date);
    }

    /**
     * @param $commission
     * @param bool|false $formatCurrency
     * @return float|string
     */
    public static function getTotalCommission($commission, $formatCurrency = false)
    {
        $rate = $commission->rate;

        $principal = $commission->investment->amount;

        $tenor = self::tenorInMonths($commission->investment->id);

        $value = $principal * $rate * $tenor / (12 * 100);

        if ($formatCurrency) {
            return AmountPresenter::currency($value);
        }

        return $value;
    }

    /**
     * @param $commission
     * @param bool|false $formatCurrency
     * @return float|string
     */
    public static function getCommissionFirstInstallment($commission, $formatCurrency = false)
    {
        if (self::tenorInMonths($commission->investment->id) == 1) {
            return self::getTotalCommission($commission, $formatCurrency);
        }

        $firstInstallmentRate =
            Setting::where('key', 'commission_first_installment_rate')
                ->latest()
                ->first()
                ->value;

        $installment = self::getTotalCommission($commission) * $firstInstallmentRate;

        if ($formatCurrency) {
            return AmountPresenter::currency($installment);
        }

        return $installment;
    }

    /**
     * @param $commission
     * @param bool|false $formatCurrency
     * @return float|string
     */
    public static function getCommissionMonthlyInstallment($commission, $formatCurrency = false)
    {
        $tenorInMonths = self::tenorInMonths($commission->investment->id);
        if ($tenorInMonths == 1) {
            return 0.00; //commission is only paid in the monthly installment
        }


        $remaining = self::getTotalCommission($commission) - self::getCommissionFirstInstallment($commission);

        $installments = $remaining / ($tenorInMonths - 1);

        if ($formatCurrency) {
            return AmountPresenter::currency($installments);
        }
        return $installments;
    }

    /**
     * @param $commission
     * @param bool|false $formatCurrency
     * @return number|string
     */
    public static function getTotalCommissionPaid($commission, $formatCurrency = false)
    {
        $payments =
            CommissionPaymentSchedule::where('commission_id', $commission->id)
                ->where('paid', 1)
                ->get();

        $summer = [];
        foreach ($payments as $payment) {
            $amount = $payment->amount;

            array_push($summer, $amount);
        }

        $total = array_sum($summer);

        if ($formatCurrency) {
            return AmountPresenter::currency($total);
        }

        return $total;
    }

    /**
     * @param $commission
     * @param bool|false $formatCurrency
     * @return float|string
     */
    public static function getCommissionPaymentBalance($commission, $formatCurrency = false)
    {
        $paid = self::getTotalCommissionPaid($commission);

        $total = self::getTotalCommission($commission);

        $balance = $total - $paid;

        if ($formatCurrency) {
            return AmountPresenter::currency($balance);
        }
        return $balance;
    }

    public static function getCommissionTotals($commissions)
    {
        $principals = [];
        $c_totals = [];
        $c_payments = [];
        $c_balances = [];
        foreach ($commissions as $c) {
            $principal = $c->investment->amount;

            array_push($principals, $principal);

            $total_commission = self::getTotalCommission($c);

            array_push($c_totals, $total_commission);

            $commission_paid = self::getTotalCommissionPaid($c);

            array_push($c_payments, $commission_paid);

            $commission_balance = self::getCommissionPaymentBalance($c);

            array_push($c_balances, $commission_balance);
        }

        $total_principal = array_sum($principals);
        $total_commissions = array_sum($c_totals);
        $total_payments = array_sum($c_payments);
        $total_balances = array_sum($c_balances);

        return [
            'principal' => $total_principal,
            'commissions' => $total_commissions,
            'payments' => $total_payments,
            'balances' => $total_balances
        ];
    }

    public static function presentInvestmentStatus($investment)
    {
        if ($investment->rolled == 1) {
            return '<span class="label label-warning">rolled over</span>';
        } elseif ($investment->withdrawn == 1) {
            return '<span class="label label-danger">withdrawn</span>';
        }
        return '<span class="label label-success">active</span>';
    }

    public static function presentMethodOfContact($id)
    {
        return ($id == 1) ? 'email' : 'post';
    }
}
