<?php
/**
 * Date: 9/8/15
 * Time: 10:29 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

use Carbon\Carbon;

class DatePresenter
{
    public static function formatDate($date_string)
    {
        if (strlen(trim($date_string)) < 2 || is_null($date_string)) {
            return null;
        }

        try {
            $date = new Carbon($date_string);

            return $date->toFormattedDateString();
        } catch (\Exception $e) {
            return null;
        }
    }
    public static function formatShortDate($date_string)
    {
        $date = new Carbon($date_string);

        return $date->format('d-M-y');
    }

    public static function formatDateTime($time_string)
    {
        $time = new Carbon($time_string);

        return $time->format('H:i:s jS F Y');
    }

    public static function formatDateFirst($time_string)
    {
        $time = new Carbon($time_string);

        return $time->format('d F Y H:i:s');
    }

    public static function formatFullDate($time_string)
    {
        $time = new Carbon($time_string);

        return $time->format('l, d M Y');
    }

    /*
     * Get the date difference between two dates
     */
    public static function getDateDifference($startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate);

        $endDate = Carbon::parse($endDate);

        return $startDate->diffInDays($endDate);
    }
}
