<?php

namespace Cytonn\Presenters;

use App\Cytonn\Models\PortfolioInvestor;

class InstitutionPresenter
{
    public static function presentName($id)
    {
        $institution = PortfolioInvestor::find($id);

        if (is_null($institution)) {
            return '';
        }

        return $institution->name;
    }
}
