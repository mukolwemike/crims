<?php
/**
 * Date: 9/8/15
 * Time: 10:55 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\Gender;

class GenderPresenter
{
    public static function presentAbbr($id)
    {
        $g = Gender::find($id);

        if (is_null($g)) {
            return null;
        }

        return $g->abbr;
    }
}
