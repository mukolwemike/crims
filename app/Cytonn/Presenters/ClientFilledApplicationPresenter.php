<?php
/**
 * Date: 05/05/2016
 * Time: 12:00 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Presenters;

use Laracasts\Presenter\Presenter;

/**
 * Class ClientFilledApplicationPresenter
 *
 * @package App\Cytonn\Presenters
 */
class ClientFilledApplicationPresenter extends Presenter
{

    /**
     * @return mixed|string
     */
    public function name()
    {
        if ($this->individual) {
            return $this->firstname.' '.$this->middlename.' '.$this->lastname;
        }

        return $this->registered_name;
    }

    public function firstName()
    {
        if ($this->individual) {
            return $this->entity->firstname;
        }

        return $this->registered_name;
    }

    /**
     * @return string
     */
    public function amount()
    {
        return number_format($this->entity->amount);
    }
}
