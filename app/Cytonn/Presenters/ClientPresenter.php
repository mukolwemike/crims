<?php
/**
 * Date: 9/9/15
 * Time: 7:31 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Title;
use Cytonn\Core\DataStructures\Carbon;

class ClientPresenter
{
    public static function presentFullNames($id)
    {
        return static::presentJointFullNames($id);
    }

    public static function presentFullNameNoTitle($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return ' ';
        }

        return ContactPresenter::presentFullNameNoTitle($client->contact_id);
    }

    public static function presentTitle($id)
    {
        $title = Title::find($id);

        if (is_null($title)) {
            return ' ';
        }

        return $title->name;
    }

    public static function presentFullNameWithPartials($firstName, $lastName, $middleName = null)
    {
        return $firstName . ' ' . $middleName . ' ' . $lastName;
    }

    public static function presentShortName($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return null;
        }

        return ContactPresenter::shortName($client->contact_id);
    }

    public static function presentSalutation($client_id)
    {
        $client = Client::find($client_id);

        if (is_null($client)) {
            return '';
        }

        if (!$client->joint) {
            return self::presentShortName($client_id);
        } else {
            $salutation = [];
            array_push($salutation, self::presentShortName($client_id));

            $joint = $client->jointHolders;
            foreach ($joint as $j) {
                array_push($salutation, $j->details->present()->getShortName);
            }

            $result = '';
            $count = count($salutation);
            for ($i = 0; $i < $count; $i++) {
                if ($i == 0) {
                    $result = $salutation[$i];
                } elseif ($i == $count - 1) {
                    $result = $result . ' and ' . $salutation[$i];
                } else {
                    $result = $result . ', ' . $salutation[$i];
                }
            }

            return $result;
        }
    }

    public static function presentFirstNameLastName($client_id)
    {
        $client = Client::find($client_id);

        if (is_null($client)) {
            return ' ';
        }

        return ContactPresenter::presentFirstNameLastName($client->contact_id);
    }

    public static function presentFirstName($client_id)
    {
        $client = Client::find($client_id);

        if (is_null($client)) {
            return ' ';
        }

        return ContactPresenter::presentFirstName($client->contact_id);
    }

    public static function presentUsernameClientUser($client_id)
    {
        $user = ClientUser::find($client_id);

        if (is_null($user)) {
            return ' ';
        }

        return $user->username;
    }


    public static function presentJointFirstNameLastName($client_id)
    {
        $client = Client::find($client_id);

        if (is_null($client)) {
            return '';
        }

        if (!$client->joint) {
            return self::presentFirstNameLastName($client_id);
        } else {
            $salutation = [];
            array_push($salutation, self::presentFirstNameLastName($client_id));

            $joint = $client->jointHolders;
            foreach ($joint as $j) {
                array_push($salutation, $j->details->present()->fullname);
            }

            $result = '';
            $count = count($salutation);
            for ($i = 0; $i < $count; $i++) {
                if ($i == 0) {
                    $result = $salutation[$i];
                } elseif ($i == $count - 1) {
                    $result = $result . ' and ' . $salutation[$i];
                } else {
                    $result = $result . ', ' . $salutation[$i];
                }
            }

            return $result;
        }
    }

    public static function presentJointFullNames($client_id)
    {
        $client = Client::find($client_id);

        if (is_null($client)) {
            return '';
        }

        if (!$client->joint) {
            return ContactPresenter::fullName($client->contact_id);
        } else {
            $salutation = [];
            array_push($salutation, ContactPresenter::fullName($client->contact_id));

            $joint = $client->jointHolders;
            foreach ($joint as $j) {
                array_push($salutation, $j->details->present()->fullname);
            }

            $result = '';
            $count = count($salutation);
            for ($i = 0; $i < $count; $i++) {
                if ($i == 0) {
                    $result = $salutation[$i];
                } elseif ($i == $count - 1) {
                    $result = $result . ' and ' . $salutation[$i];
                } else {
                    $result = $result . ', ' . $salutation[$i];
                }
            }

            return $result;
        }
    }

    public static function presentJointShortNames($client_id)
    {
        $client = Client::find($client_id);

        if (is_null($client)) {
            return '';
        }

        if (!$client->joint && $client->jointHolders->count() == 0) {
            return self::presentShortName($client_id);
        } else {
            $salutation = [];
            array_push($salutation, self::presentShortName($client_id));

            $joint = $client->jointHolders;
            foreach ($joint as $j) {
                array_push($salutation, $j->details->present()->getShortName);
            }

            $result = '';
            $count = count($salutation);
            for ($i = 0; $i < $count; $i++) {
                if ($i == 0) {
                    $result = $salutation[$i];
                } elseif ($i == $count - 1) {
                    $result = $result . ' and ' . $salutation[$i];
                } else {
                    $result = $result . ', ' . $salutation[$i];
                }
            }

            return $result;
        }
    }

    public static function presentJointFirstNames($client_id)
    {
        $client = Client::find($client_id);

        if (is_null($client)) {
            return '';
        }

        if (!$client->joint) {
            return self::presentShortName($client_id);
        } else {
            $salutation = [];
            array_push($salutation, ContactPresenter::firstName($client->contact_id));

            $joint = $client->jointHolders;
            foreach ($joint as $j) {
                array_push($salutation, $j->details->firstname);
            }

            $result = '';
            $count = count($salutation);
            for ($i = 0; $i < $count; $i++) {
                if ($i == 0) {
                    $result = $salutation[$i];
                } elseif ($i == $count - 1) {
                    $result = $result . ' and ' . $salutation[$i];
                } else {
                    $result = $result . ', ' . $salutation[$i];
                }
            }

            return $result;
        }
    }

    public static function presentContactPerson($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return null;
        }

        return $client->contact_person_fname . ' ' . $client->contact_person_lname;
    }

    public static function presentContactPersonShortName($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return null;
        }

        $persons = $client->contactPersons;

        $result = $persons->implode('name', ' and ');

        if (!is_null($client->contact_person_fname) && !is_null($client->contact_person_lname)) {
            if ($result != '') {
                $result = $client->contact_person_fname . ' ' . $client->contact_person_lname . ' and ' . $result;
            } else {
                $result = $client->contact_person_fname . ' ' . $client->contact_person_lname;
            }
        }

        return $result;
    }

    public static function presentAddress($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return null;
        }

        if (strlen(trim($client->postal_address)) > 0) {
            return 'P.O Box ' . $client->postal_address . ' - ' . $client->postal_code . '
                    <div class="bold-underline">' . $client->town . '</div><br/>';
        } else {
            return $client->street . '<br/>';
        }
    }

    public static function presentAddressInline($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return null;
        }

        if (strlen(trim($client->postal_address)) > 0) {
            return 'P.O Box ' . $client->postal_address . ' - ' . $client->postal_code . '<br/>
                    <span>' . $client->town . '</span><br/>';
        } else {
            return $client->street . '<br/>';
        }
    }

    public static function getEffectiveTotalDate($date)
    {
        $date = new Carbon($date);
        if ($date->isFuture()) {
            return Carbon::today()->toDateString();
        }
        return $date;
    }

    public static function presentClientType($id)
    {
        $client = Client::find($id);

        $type = ucfirst($client->clientType->name);
        if ($client->joint) {
            return $type . ' Joint';
        }
        return $type;
    }

    /**
     * Return preferredName (if it exists)
     *
     * @param  $id
     * @return mixed|string
     */
    public static function presentPreferredName($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return ' ';
        }

        return ContactPresenter::preferredName($client->contact_id);
    }

    public static function presentAge($id)
    {
        $client = Client::find($id);

        if (is_null($client->dob)) {
            return '';
        } elseif (Carbon::parse($client->dob)->daysIntoYear() == 1) {
            return '';
        } else {
            return Carbon::parse($client->dob)->diffInYears(Carbon::now());
        }
    }

    public static function presentJointShortName($id)
    {
        $client = Client::find($id);

        if (is_null($client)) {
            return '';
        }

        if (!$client->joint) {
            return ContactPresenter::fullName($client->contact_id);
        } else {
            $salutation = [];

            array_push($salutation, ContactPresenter::fullName($client->contact_id));

            $joint = $client->jointHolders;

            foreach ($joint as $j) {
                array_push($salutation, $j->details->present()->fullname);
            }

            $result = '';

            $count = count($salutation);

            for ($i = 0; $i < $count; $i++) {
                if ($i == 0) {
                    $result = $salutation[$i];
                } elseif ($i == $count - 1) {
                    $result = $result . ' and Others';
                }
            }
        }

        return $result;
    }

    public static function presentJointHolderFullName($id)
    {
        $jointDetail = ClientJointDetail::find($id);

        return $jointDetail ? $jointDetail->title->name . ' ' . $jointDetail->firstname . ' ' . $jointDetail->lastname : '';
    }

    public static function presentRiskyStatus($id)
    {
        $status = Client\RiskyStatus::find($id);

        if (is_null($status)) {
            return '';
        }

        return strtoupper($status->name);
    }

    public static function signingMandate($id)
    {
        switch ($id) {
            case 0:
                return 'Singly';
                break;

            case 1:
                return 'All to sign';
                break;

            case 2:
                return 'Either to sign';
                break;

            case 3:
                return 'At Least Two to sign';
                break;

            default:
                return 'No mandate specified!';
        }
    }

    public static function presentTransferred($status)
    {
        if ($status) {
            return 'Transferred';
        } else {
            return '';
        }
    }

    public static function presentUploadedKyc($slug)
    {
        return $slug ? ClientComplianceChecklist::where('slug', $slug)->first()->name : '';
    }
}
