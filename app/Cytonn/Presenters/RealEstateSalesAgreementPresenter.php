<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Presenters;

use Laracasts\Presenter\Presenter;

class RealEstateSalesAgreementPresenter extends Presenter
{
    /*
     * Get the signed date for the real estate sales agreement
     */
    public function dateSigned()
    {
        if ($this->date_signed) {
            return $this->date_signed->toDateString();
        }
    }
}
