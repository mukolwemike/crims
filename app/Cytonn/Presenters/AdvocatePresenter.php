<?php

namespace Cytonn\Presenters;

use App\Cytonn\Models\Advocate;

class AdvocatePresenter
{
    public static function presentAddress($id)
    {
        $advocate = Advocate::find($id);

        if (is_null($advocate)) {
            return null;
        }

        if (strlen(trim($advocate->postal_address)) > 0) {
            return $advocate->name . '<br/>' .
                $advocate->building . '<br/>' .
                $advocate->road . '<br/>' .
                'P.O Box ' . $advocate->postal_address . ' - ' .
                $advocate->postal_code . ' ' . $advocate->town . ', ' . $advocate->country->name . '.<br/>' .
                'Tel : ' . $advocate->telephone_office . '<br/>' .
                'Mobile : ' . $advocate->telephone_cell .  '<br/>' .
                '<span style="border-bottom: 1px solid;">E-mail : ' . $advocate->email .'</span>';
        } else {
            return $advocate->street.'<br/>';
        }
    }
}
