<?php
/**
 * Date: 16/03/2016
 * Time: 6:16 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\CustodialAccount;

class CustodialPresenter
{
    public static function presentName($account_id)
    {
        $account = CustodialAccount::find($account_id);

        if (is_null($account)) {
            return '';
        }

        return $account->name;
    }
}
