<?php

namespace App\Cytonn\Presenters\General;

use Carbon\Carbon;

class DatePresenter
{
    public static function formatDate($date_string)
    {
        $date = new Carbon($date_string);

        return $date->toFormattedDateString();
    }
}
