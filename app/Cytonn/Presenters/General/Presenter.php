<?php

namespace App\Cytonn\Presenters\General;

class Presenter
{
    public static function currency($amount, $absolute = false, $decimals = 2)
    {
        return AmountPresenter::currency($amount, $absolute, $decimals);
    }

    public static function date($date)
    {
        return DatePresenter::formatDate($date);
    }

    public static function accounting($amount, $absolute = false, $decimals = 2)
    {
        return \Cytonn\Presenters\AmountPresenter::accounting($amount, $absolute, $decimals);
    }
}
