<?php
/**
 * Date: 9/8/15
 * Time: 10:23 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

class BooleanPresenter
{
    public static function present($status)
    {
        return $status;
    }

    public static function presentIcon($status)
    {
        if ($status) {
            return '<i class="fa fa-check-square-o fa-2 text-success"></i>';
        } else {
            return '<i class="fa fa-times fa-2 text-danger"></i>';
        }
    }

    public static function presentYesNo($status)
    {
        return $status ? 'Yes' : 'No';
    }
}
