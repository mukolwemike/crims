<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Presenters;

use Laracasts\Presenter\Presenter;

class RealEstateSizePresenter extends Presenter
{
    /*
     * Generate the name for the real estate name
     */
    public function getName()
    {
        if ($this->realestate_land_size_id) {
            return $this->name . ' on ' . $this->landSize->name;
        }

        return $this->name;
    }
}
