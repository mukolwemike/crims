<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\RealestateLetterOfOffer;
use Laracasts\Presenter\Presenter;

class RealEstateLetterOfOfferPresenter extends Presenter
{
    /*
     * Get the signed date for the real estate loo
     */
    public function dateSigned()
    {
        if ($this->date_signed) {
            return $this->date_signed->toDateString();
        }
    }

    /*
     * Get the next LOO
     */
    public function nextUnapprovedLOO()
    {
        $loo = RealestateLetterOfOffer::where('id', '>', $this->id)->approved(false)
            ->orderBy('id', 'ASC')->first();

        return $loo ? $loo->id : null;
    }

    /*
     * Get the previous LOO
     */
    public function prevUnapprovedLOO()
    {
        $loo = RealestateLetterOfOffer::where('id', '<', $this->id)->approved(false)
            ->orderBy('id', 'DESC')->first();

        return $loo ? $loo->id : null;
    }
}
