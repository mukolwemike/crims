<?php

namespace Cytonn\Presenters;

use Cytonn\Lib\Helper;

class AmountPresenter
{
    public static function currency($amount, $absolute = false, $decimals = 2)
    {
        if (round($amount, 2) == 0.00 || round($amount, 2) == -0.00) {
            $amount = 0;
        }

        if ($decimals == 0) {
            round($amount);
        }

        if ($absolute) {
            $amount = abs($amount);
        }

        $amount = round($amount, $decimals);

        return number_format($amount, $decimals);
    }

    public static function accounting($amount, $absolute = false, $decimals = 2)
    {
        if ($absolute || $amount > 0) {
            return static::currency($amount, $absolute, $decimals);
        }

        return '('.static::currency($amount, true, $decimals).')';
    }

    public static function convertToWords($amount)
    {
        return (new Helper())->convertNumberToWords($amount);
    }
}
