<?php
/**
 * Date: 29/11/2016
 * Time: 12:47
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Presenters;

use Laracasts\Presenter\Presenter;

class ClientTransactionApprovalPresenter extends Presenter
{
    public function type()
    {
        $type = $this->entity->transaction_type;

        return ucfirst(str_replace('_', ' ', $type));
    }
}
