<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 19/07/2018
 * Time: 12:33
 */

namespace App\Cytonn\Presenters;

use Laracasts\Presenter\Presenter;

class PortfolioTransactionApprovalPresenter extends Presenter
{
    public function type()
    {
        $type = $this->entity->transaction_type;

        return ucfirst(str_replace('_', ' ', $type));
    }
}
