<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 26/10/2018
 * Time: 11:23
 */

namespace App\Cytonn\Presenters;

use App\Cytonn\Models\CommissionRecepient;

class CommissionPresenter
{
    public static function presentRecipient($id)
    {
        $recipient = CommissionRecepient::find($id);

        if (is_null($recipient)) {
            return ' ';
        }

        return $recipient->name;
    }

    public static function presentRate($id)
    {
        $recipient = CommissionRecepient::find($id);

        if (is_null($recipient)) {
            return ' ';
        }

        return $recipient->type->rate->rate;
    }
}
