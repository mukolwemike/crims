<?php
/**
 * Date: 9/8/15
 * Time: 11:09 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\Country;

class CountryPresenter
{
    public static function present($id)
    {
        $country = Country::find($id);

        if (is_null($country)) {
            return null;
        }

        return $country->name;
    }
}
