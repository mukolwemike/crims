<?php
/**
 * Date: 04/07/2016
 * Time: 6:36 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\RealEstatePayment;
use Laracasts\Presenter\Presenter;

/**
 * Class RealEstatePaymentPresenter
 *
 * @package Cytonn\Presenters
 */
class RealEstatePaymentPresenter extends Presenter
{

    /**
     * Retrieve the schedule paid
     *
     * @return string
     */
    public function scheduleName()
    {
        return $this->schedule->description;
    }

    public function bcTag()
    {
        $count = RealEstatePayment::where('schedule_id', $this->schedule->id)->count();

        if ($count > 1) {
            $rank = 1 + RealEstatePayment::where('schedule_id', $this->schedule->id)
                    ->where('date', '<', $this->date)->count();

            return $this->schedule->description . ' #' . $rank;
        }

        return $this->schedule->description;
    }

    public function getCommissionRecipient()
    {
        $recipient = null;

        $holding = $this->holding;

        if ($holding->commission) {
            $recipient = $holding->commission->recipient;
        }

        if (is_null($recipient) && $holding->client) {
            $recipient = $holding->client->getLatestFa();
        }

        return $recipient;
    }
}
