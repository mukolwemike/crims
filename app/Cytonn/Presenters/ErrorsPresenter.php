<?php
/**
 * Date: 9/7/15
 * Time: 11:48 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

/**
 * Class ErrorsPresenter
 *
 * @package Cytonn\Presenters
 */
class ErrorsPresenter
{
    /**
     * Outputs the form error message when given the messages variable and fieldname
     *
     * @param $messages
     * @param $fieldName
     * @return string
     */
    public static function formErrors($errors, $fieldName)
    {
        return '<span class="group"><p class="form-errors group">'.$errors->first($fieldName).'</p></span>';
    }
}
