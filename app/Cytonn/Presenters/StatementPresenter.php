<?php
/**
 * Date: 09/12/2015
 * Time: 12:09 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

class StatementPresenter
{
    public $interest = [];
    public $balance = 0;
    public $last_inv;
    public $gross_interests = [];
    public $net_interests = [];
    public $children = [];

    public $parents = [];

    public function presentDescendants($inv)
    {
        array_push($this->children, $inv);
        array_push(
            $this->net_interests,
            $inv->parent()->repo->getNetInterestForInvestmentAtDate($inv->withdrawal_date)
        );

        array_push($this->interest, $inv->repo->getTotalPayments());
        $this->balance = $inv->amount;
        $this->last_inv = $inv;

        if ($inv->descendant()) {
            $this->presentDescendants($inv->descendant());
            $this->balance = $inv->descendant()->amount;
            $this->last_inv = $inv->descendant();
        }


        return [
            'interest' => $this->interest,
            'balance' => $this->balance,
            'last_inv' => $this->last_inv,
            'children' => $this->children
        ];
    }

    public function getTotalNetInterestForParents($child)
    {
        $parents = $this->parent($child);

        $interests = [];

        foreach ($parents as $parent) {
            $interest = $parent->repo->getFinalNetInterest();

            array_push($interests, $interest);
        }

        return array_sum($interests);
    }

    public function getTotalGrossInterestForParents($child)
    {
        $parents = $this->parent($child);

        $interests = [];

        foreach ($parents as $parent) {
            $interest = $parent->repo->getFinalGrossInterest();

            array_push($interests, $interest);
        }

        return array_sum($interests);
    }

    protected function parent($child)
    {
        if ($child->parent()) {
            array_push($this->parents, $child->parent());

            $this->parent($child->parent());
        }
        return $this->parents;
    }
}
