<?php
/**
 * Date: 04/11/2015
 * Time: 5:37 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

class CustodialAmountPresenter
{
    public static function presentReceiptAmount($value, $format = true)
    {
        if ($value >= 0) {
            if (!$format) {
                return $value;
            }

            return AmountPresenter::currency($value, true);
        }
        return null;
    }

    public static function presentPaymentAmount($value, $format = true)
    {
        if ($value < 0) {
            if (!$format) {
                return abs($value);
            }

            return AmountPresenter::currency($value, true);
        }
        return null;
    }
}
