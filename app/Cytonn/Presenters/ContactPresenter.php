<?php

namespace Cytonn\Presenters;

use App\Cytonn\Models\Contact;

/**
 * Class ContactPresenter
 *
 * @package Cytonn\Presenters
 */
class ContactPresenter
{
    /**
     * @param $contactId
     * @return mixed|string
     */
    public static function fullName($contactId)
    {
        $contact = Contact::find($contactId);

        if (is_null($contact)) {
            return '';
        }

        if ($contact->entityType->name == 'individual') {
            return self::getTitle($contact) . ' ' . $contact->firstname .
                ' ' . $contact->middlename . ' ' . $contact->lastname;
        }

        if ($contact->entityType->name == 'corporate') {
            return $contact->corporate_registered_name;
        }

        return '';
    }

    /**
     * @param $contactId
     * @return mixed|string
     */
    public static function shortName($contactId)
    {
        $contact = Contact::find($contactId);

        if (is_null($contact)) {
            return '';
        }

        if ($contact->entityType->name == 'individual') {
            return self::getTitle($contact) . ' ' . $contact->lastname;
        }
        if ($contact->entityType->name == 'corporate') {
            return $contact->corporate_registered_name;
        }

        return '';
    }

    /**
     * @param $contactId
     * @return mixed|string
     */
    public static function firstNameWithTitle($contactId)
    {
        $contact = Contact::find($contactId);

        if (is_null($contact)) {
            return '';
        }

        if ($contact->entityType->name == 'individual') {
            return self::getTitle($contact) . ' ' . $contact->lastname;
        }
        if ($contact->entityType->name == 'corporate') {
            return $contact->corporate_registered_name;
        }

        return '';
    }

    /**
     * @param $contactId
     * @return mixed|string
     */
    public static function firstName($contactId)
    {
        $contact = Contact::find($contactId);

        if ($contact->entityType->name == 'individual') {
            return $contact->firstname;
        }
        if ($contact->entityType->name == 'corporate') {
            return $contact->corporate_registered_name;
        }

        return '';
    }

    /**
     * @param $contactId
     * @return mixed|string
     */
    public static function presentFullNameNoTitle($contactId)
    {
        try {
            $contact = Contact::find($contactId);

            if (is_null($contact)) {
                return '';
            }

            if ($contact->entityType->name == 'individual') {
                return $contact->firstname . ' ' . $contact->middlename . ' ' . $contact->lastname;
            }
            if ($contact->entityType->name == 'corporate') {
                return $contact->corporate_registered_name;
            }

            return '';
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @param $contactId
     * @return mixed|string
     */
    public static function presentFirstNameLastName($contactId)
    {
        $contact = Contact::find($contactId);

        if (is_null($contact)) {
            return '';
        }

        if ($contact->entityType->name == 'individual') {
            $title = self::getTitle($contact);

            return $title . ' ' . $contact->firstname . ' ' . $contact->lastname;
        }
        if ($contact->entityType->name == 'corporate') {
            return $contact->corporate_registered_name;
        }

        return '';
    }

    public static function presentFirstName($contactId)
    {
        $contact = Contact::find($contactId);

        if (is_null($contact)) {
            return '';
        }

        if ($contact->entityType->name == 'individual') {
            return  $contact->firstname;
        }
        if ($contact->entityType->name == 'corporate') {
            return $contact->corporate_registered_name;
        }

        return '';
    }

    /**
     * @param $contact
     * @return string
     */
    private static function getTitle($contact)
    {
        try {
            return $contact->title->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @param $contact
     * @return string
     */
    public static function getGender($contact)
    {
        try {
            return $contact->gender->abbr;
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * Return preferredName (if it exists) in the contacts table
     *
     * @param  $contactId
     * @return mixed|string
     */
    public static function preferredName($contactId)
    {
        $contact = Contact::find($contactId);

        if (is_null($contact)) {
            return '';
        }

        if (!$contact->preferredname || strlen(trim($contact->preferredname)) == 0) {
            return $contact->firstname;
        }

        return $contact->preferredname;
    }
}
