<?php
/**
 * Date: 9/8/15
 * Time: 5:48 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Presenters;

use App\Cytonn\Models\User;
use Illuminate\Support\Collection;

class UserPresenter
{
    /**
     * Get User title
     *
     * @param  $user
     * @return string
     */
    private static function getTitle(User $user = null)
    {
        try {
            return $user->title->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * Present user short names
     *
     * @param  $id
     * @return null|string
     */
    public static function presentNames($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return null;
        }

        return self::getTitle($user) . ' ' . $user->lastname;
    }

    /**
     * Present User full names
     *
     * @param  $id
     * @return null|string
     */
    public static function presentFullNames($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return null;
        }

        if ($user instanceof Collection) {
            $user = $user->first();
        }

        return self::getTitle($user) . ' ' . $user->firstname . ' ' . $user->middlename . ' ' . $user->lastname;
    }

    /**
     * Present user full names without title
     *
     * @param  $id
     * @return null|string
     */
    public static function presentFullNamesNoTitle($id)
    {
        try {
            $user = User::find($id);

            if (is_null($user)) {
                return null;
            }

            $name = '';

            if ($user->firstname != '') {
                $name .= trim($user->firstname);
            }

            if ($user->middlename != '') {
                $name .= ' ' . trim($user->middlename);
            }

            if ($user->lastname != '') {
                $name .= ' ' . trim($user->lastname);
            }

            return $name;
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * Present user letter closing
     *
     * @param  $user_id
     * @return string
     */
    public static function presentLetterClosing($user_id)
    {
        $user = User::find($user_id);

        if (is_null($user)) {
            return '';
        }

        !is_null($user->signature) ?: $user->signature = 'empty.png';

        return '<img src="' . storage_path() . '/resources/signatures/' . $user->signature . '" height="60px"/><br/>'
            . '<strong>' . self::presentFullNameWithoutTitle($user->id) . '</strong><br/>'
            . '<strong style="text-decoration: underline;">' . $user->jobtitle . '</strong>';
    }

    /**
     * Present user letter closing with no signature
     *
     * @param  $user_id
     * @return string
     */
    public static function presentLetterClosingNoSignature($user_id)
    {
        $user = User::find($user_id);

        if (is_null($user)) {
            return '';
        }

        return '<p>' . self::presentFullNameWithoutTitle($user->id) . '</p>'
            . '<strong style="text-decoration: underline;">' . $user->jobtitle . '</strong>';
    }

    /**
     * Present User Sign Off
     *
     * @param  $user_id
     * @return string
     */
    public static function presentSignOff($user_id)
    {
        $user = User::find($user_id);

        if (is_null($user)) {
            return '';
        }

        return '<strong>' . self::presentFullNameWithoutTitle($user->id) . '</strong><br/>'
            . '<strong style="text-decoration: underline;">' . $user->jobtitle . '</strong>';
    }

    /**
     * @param $user_id
     * @return string
     */
    public static function signatureBase64($user_id)
    {
        $user = User::find($user_id);

        if (is_null($user)) {
            return '';
        }

        try {
            $path = storage_path() . '/resources/signatures/' . $user->signature;

            $imagedata = file_get_contents($path);
        } catch (\Exception $e) {
            $path = storage_path() . '/resources/signatures/empty.png';

            $imagedata = file_get_contents($path);
        }

        return 'data:' . mime_content_type($path) . ';base64,' . base64_encode($imagedata);
    }

    /**
     * Present user full names with no Title
     *
     * @param  $user_id
     * @return string
     */
    public static function presentFullNameWithoutTitle($user_id)
    {
        try {
            $user = User::findOrFail($user_id);

            is_null($user->qualification) ? $q = '' : $q = ', ' . $user->qualification;

            $middle = str_limit($user->middlename, 1, '.');

            return $user->firstname . ' ' . $middle . ' ' . $user->lastname . $q;
        } catch (\Exception $e) {
            return '';
        }
    }

    public static function presentUsername($user_id)
    {
        try {
            $user = User::findOrFail($user_id);
            return preg_replace('/[^A-Za-z0-9\-]/', '', $user->username);
        } catch (\Exception $e) {
            return '';
        }
    }
}
