<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 17/10/2018
 * Time: 15:21
 */

namespace App\Cytonn\Presenters;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankBranch;

class BankPresenter
{
    public static function presentBankName($id)
    {
        $bank = ClientBank::find($id);

        if (is_null($bank)) {
            return ' ';
        }

        return $bank->name;
    }

    public static function presentBranchName($id)
    {
        $branch = ClientBankBranch::find($id);

        if (is_null($branch)) {
            return ' ';
        }

        return $branch->name;
    }
}
