<?php

namespace App\Cytonn\Presenters;

use Laracasts\Presenter\Presenter;

class JointHolderPresenter extends Presenter
{
    public function firstNameLastName()
    {
        return $this->title->name.' '.$this->firstname.' '.$this->lastname;
    }

    public function shortName()
    {
        return $this->title->name.' '.$this->lastname;
    }
}
