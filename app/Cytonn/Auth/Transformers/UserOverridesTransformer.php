<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 14/02/2017
 * Time: 14:18
 */

namespace App\Cytonn\Auth\Transformers;

use App\Cytonn\Investments\Commission\Recipient;
use App\Cytonn\Investments\Currency;
use Carbon\Carbon;
use Crims\Commission\Summary;
use League\Fractal\TransformerAbstract;

class UserOverridesTransformer extends TransformerAbstract
{
    protected $recipient;

    protected $currency;

    protected $start;

    protected $end;

    /**
     * UserOverridesTransformer constructor.
     *
     * @param $recipient
     * @param $currency
     * @param $start
     * @param $end
     */
    public function __construct(Recipient $recipient, Currency $currency, Carbon $start, Carbon $end)
    {
        $this->recipient = $recipient;
        $this->currency = $currency;
        $this->start = $start;
        $this->end = $end;
    }


    public function transform(Recipient $recipient)
    {
        $summary = new Summary($recipient, $this->currency, $this->start, $this->end);

        $commission = $summary->total();

        return [
            'name'=>$recipient->name,
            'amount'=>$commission,
            'rank'=>$recipient->rank,
            'rate'=>$recipient->rate,
            'override'=>$this->recipient->rank->override_rate * $commission/100
        ];
    }
}
