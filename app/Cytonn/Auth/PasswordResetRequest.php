<?php
/**
 * Date: 13/05/2016
 * Time: 4:37 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Cytonn\Auth;

use App\Cytonn\BaseModel;

class PasswordResetRequest extends BaseModel
{
    public $table = 'password_reset_requests';
}
