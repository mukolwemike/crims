<?php
/**
 * Date: 26/02/2016
 * Time: 9:09 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Cytonn\Auth;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class CurrentUserTransformer
 *
 * @package App\Cytonn\Auth
 */
class CurrentUserTransformer extends TransformerAbstract
{
    /**
     * @param \App\Cytonn\Auth\User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'=>$user->uuid,
            'username'=>$user->username,
            'firstname'=>$user->firstname,
            'lastname'=>$user->lastname,
            'email'=>$user->email,
            'created_at_unix'=>(new Carbon($user->created_at))->timestamp,
            'access'=>$this->getAccessClients($user),
            'fas' =>$this->getFas($user)
        ];
    }

    /**
     * @param $user
     * @return array
     */
    private function getAccessClients($user)
    {
        $access = $user->access()->where('active', 1)->pluck('client_id')->all();

        $clients = [];

        foreach ($access as $client_id) {
            $client = Client::findOrFail($client_id);
            $c = new \stdClass();
            $c->id = $client->uuid;
            $c->fullname = $client->present()->fullName;
            $c->name = $this->accountName($client);
            array_push($clients, $c);
        }

        return $clients;
    }

    public function getFas(User $user)
    {
        return   $user->financialAdvisors
            ->map(
                function ($fa) {
                    return (object) ['id'=>$fa->uuid, 'name'=>$fa->name];
                }
            )->all();
    }
    public function accountName(Client $client)
    {
        $name = $client->present()->fullName. ' - '.$client->client_code;

        if ($acc = $client->account_name) {
            return $name ." ($acc)";
        }

        return $name;
    }
}
