<?php
/**
 * Date: 13/05/2016
 * Time: 6:45 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Cytonn\Auth;

use App\Cytonn\BaseModel;

class ClientPasswordReminder extends BaseModel
{
    protected $table = 'client_password_reminders';

    protected $guarded = ['id'];
}
