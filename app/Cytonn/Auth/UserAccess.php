<?php
/**
 * Date: 26/02/2016
 * Time: 9:23 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Cytonn\Auth;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\Client;

class UserAccess extends BaseModel
{
    public $table = 'client_user_access';

    protected $guarded = ['id'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
