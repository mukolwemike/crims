<?php
/**
 * Date: 02/03/2016
 * Time: 11:30 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Cytonn\Auth;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\Client;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientUserAccess extends BaseModel
{
    use SoftDeletes;

    public $table = 'client_user_access';

    protected $guarded = ['id'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
