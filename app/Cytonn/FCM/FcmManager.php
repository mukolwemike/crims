<?php

namespace Cytonn\FCM;

/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 4/18/18
 * Time: 10:51 AM
 */

use App\Cytonn\Models\ClientUser;
use Cytonn\Users\ClientUserDeviceTokenRepository;
use FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class FcmManager
{
    /**
     * @param $client
     * @param $investment
     * @param $title
     * @param $body
     * @param $type
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function sendFcmMessageToMultipleDevices(
        $client,
        $investmentId,
        $productId,
        $title,
        $body,
        $type
    ) {

        return;

        if (in_array(env('APP_ENV'), ['testing', 'local', 'staging'])) {
            return;
        }

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $clientUserDeviceTokenRepository = new ClientUserDeviceTokenRepository();

        $androidTokens = array();

        $iOSTokens = array();

        $undefinedTokens = array();

        foreach ($client->clientUsers as $clientUser) {
            $clientUserDeviceTokens = $clientUserDeviceTokenRepository->find($clientUser);

            $androidTokens = array_merge(
                $androidTokens,
                $clientUserDeviceTokens
                    ->where('platform', 'android')
                    ->pluck('device_fcm_token')
                    ->toArray()
            );

            $iOSTokens = array_merge(
                $iOSTokens,
                $clientUserDeviceTokens
                    ->where('platform', 'ios')
                    ->pluck('device_fcm_token')
                    ->toArray()
            );

            $undefinedTokens = array_merge(
                $undefinedTokens,
                $clientUserDeviceTokens
                    ->where('platform', 'undefined')
                    ->pluck('device_fcm_token')
                    ->toArray()
            );
        }


        if (count($undefinedTokens)) {
            $androidTokens = array_merge($androidTokens, $undefinedTokens);

            $iOSTokens = array_merge($iOSTokens, $undefinedTokens);
        }

        $androidTokens = array_unique($androidTokens);

        $iOSTokens = array_unique($iOSTokens);

        $iOSOptionBuilder = new OptionsBuilder();
        $androidOptionBuilder = new OptionsBuilder();

        $iOSOptionBuilder->setTimeToLive(60 * 20)
            ->setContentAvailable(true)
            ->setPriority('high');

        $androidOptionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setSound('default');

        $androidDataBuilder = new PayloadDataBuilder();
        $iOSDataBuilder = new PayloadDataBuilder();

        $message_data = [
            'title' => $title,
            'body' => $body,
            'type' => $type,
            'icon' => 'ic_status_bar',
            'large_icon' => "ic_notification",
            'show_in_foreground' => true,
            'clientId' => $client->uuid,
            'investmentId' => $investmentId,
            'productId' => $productId,
            'clearClientAccount' => true,
        ];

        $iOSDataBuilder->addData(['notification' => $message_data]);
        $androidDataBuilder->addData(['custom_notification' => $message_data]);

        $iOSOption = $iOSOptionBuilder->build();
        $androidOption = $androidOptionBuilder->build();
        $notification = $notificationBuilder->build();

        $androidData = $androidDataBuilder->build();
        $iOSdata = $iOSDataBuilder->build();

        $tokensToDelete = array();

        if (!is_null($androidTokens) && count($androidTokens) > 0) {
            $downstreamAndroidResponse = FCM::sendTo($androidTokens, $iOSOption, null, $androidData);

            $tokensToDelete = array_merge($tokensToDelete, $downstreamAndroidResponse->tokensToDelete());
        }

        if (!is_null($iOSTokens) && count($iOSTokens) > 0) {
            $downstreamiOSResponse = FCM::sendTo($iOSTokens, $androidOption, $notification, $iOSdata);

            $tokensToDelete = array_merge($tokensToDelete, $downstreamiOSResponse->tokensToDelete());
        }

        if (count($tokensToDelete)) {
            $this->deleteClientDeviceTokens($tokensToDelete);
        }

//        $downstreamResponse->tokensToModify();
//
//        $downstreamResponse->tokensToRetry();
//
//        $downstreamResponse->tokensWithError();
    }

    public function deleteClientDeviceTokens($tokens)
    {
        $clientUserDeviceTokenRepository = new ClientUserDeviceTokenRepository();

        foreach ($tokens as $token) {
            $clientUserDeviceTokenRepository->delete($token);
        }
    }

    public function getClientDeviceTokens($client)
    {
        $users = $client->clientUsers;

        return $users->filter(function ($user) {
            return $user->device_fcm_token != null;
        })->pluck('device_fcm_token')->toArray();
    }

    public function moveClientDeviceTokens()
    {
        $deviceTokens = ClientUser::where('device_fcm_token', '!=', null)->get();

        $clientUserDeviceTokenRepository = new ClientUserDeviceTokenRepository();

        $deviceTokens->each(function ($deviceToken) use ($clientUserDeviceTokenRepository) {
            $input = [
                'client_user_id' => $deviceToken->id,
                'platform' => 'undefined',
                'device_fcm_token' => $deviceToken->device_fcm_token
            ];

            $clientUserDeviceTokenRepository->save($input);
        });
    }
}
