<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 13/06/2019
 * Time: 09:36
 */

namespace Cytonn\Users;

use Illuminate\Support\Arr;

class UsernameGenerator
{
    public static function generate($firstname, $middlename = '', $lastname, $email = null, $try = 0)
    {
        if ($email && ($try == 0)) {
            $code = Arr::first(explode("@", $email));

            return strtolower(trim($code));
        }

        if (strlen($firstname) == 0 && strlen($lastname) == 0) {
            throw new \InvalidArgumentException("Could not generate username, provide first and last names");
        }

        $username = trim($firstname);

        if (strlen(trim($lastname)) > 1 && $try == 1) {
            $username = trim(substr($firstname, 0, 1).''.$lastname);
        }

        if ($try == 2) {
            $username = trim(substr($firstname, 0, 1).substr($middlename, 0, 1).$lastname);
        }

        if ($try == 3) {
            $username = trim($firstname.substr($middlename, 0, 1).$lastname);
        }

        if ($try == 4) {
            $username = trim($firstname.substr($middlename, 0, 1).substr($lastname, 0, 1));
        }

        if ($try >= 5) {
            if ($email) {
                $username = trim(Arr::first(explode("@", $email)));
            }

            $username = $username.$try;
        }

        return strtolower(trim($username));
    }
}
