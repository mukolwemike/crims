<?php
/**
 * Date: 05/12/2015
 * Time: 11:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Users;

use App\Cytonn\Models\User;
use Illuminate\Support\Facades\Session;

/**
 * Class SessionRepository
 *
 * @package Cytonn\Users
 */
class SessionRepository
{
    /**
     * When user logs in, the old session is destroyed so that only one session exists at a time
     *
     * @param User $user
     */
    public function swapSession(User $user)
    {
        $new_sessid   = Session::getId(); //get new session_id after user sign in

        try {
            $last_session = Session::getHandler()->read($user->last_sessid); // retrive last session
        } catch (\Exception $e) {
            $last_session = false;
        }

        if ($last_session) {
            Session::getHandler()->destroy($user->last_sessid);
        }

        $user->last_sessid = $new_sessid;
        $user->save();
    }
}
