<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 7/16/18
 * Time: 1:09 PM
 */

namespace Cytonn\Users;

use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\ClientUserDeviceToken;

class ClientUserDeviceTokenRepository
{
    /**
     * @var
     */
    public $deviceToken;

    /**
     * ClientUserDeviceTokenRepository constructor.
     * @param ClientUserDeviceToken|null $deviceToken
     */
    public function __construct(ClientUserDeviceToken $deviceToken = null)
    {
        is_null($deviceToken) ? $this->deviceToken = new ClientUserDeviceToken() : $this->deviceToken = $deviceToken;
    }

    public function find(ClientUser $clientUser)
    {
        return $this->deviceToken->where('client_user_id', $clientUser->id)->get();
    }

    public function save($input)
    {
        return ClientUserDeviceToken::create($input);
    }

    public function update()
    {
    }

    public function delete($token)
    {
        return $this->deviceToken->where('device_fcm_token', $token)->delete();
    }
}
