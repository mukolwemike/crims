<?php
/**
 * Date: 15/01/2016
 * Time: 3:58 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Users;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientPasswordReminder;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\ClientUserAccess;
use App\Exceptions\CrimsException;
use App\Jobs\USSD\SendMessages;
use Carbon\Carbon;
use Cytonn\Authentication\Authy;
use Cytonn\Mailers\UserMailer;
use Illuminate\Support\Arr;

/**
 * Class UserClientRepository
 *
 * @package Cytonn\Users
 */
class UserClientRepository
{
    /**
     * @var ClientUser
     */
    public $user;

    /**
     * UserClientRepository constructor.
     *
     * @param $user
     */
    public function __construct(ClientUser $user = null)
    {
        is_null($user) ? $this->user = new ClientUser() : $this->user = $user;
    }

    /**
     * @param Client $client
     * @param null $phone
     */
    public function provisionForClient(Client $client, $phone = null)
    {
        $phone  = $phone ? $phone : $client->phone;

        if (!$phone) {
            $phone = $client->telephone_home ? $client->telephone_home : $client->telephone_office;
        }

        if (!$phone) {
            $phone = Arr::first($client->getContactPhoneNumbersArray());
        }

        if ($client->type->name == 'individual') {
            $this->provisionForIndividual(
                $client,
                $client->contact->firstname,
                $client->contact->middlename,
                $client->contact->lastname,
                $phone,
                $client->contact->email
            );

            $this->provisionForJoint($client);

            return;
        }

        $this->provisionForCompanies($client);
    }

    /**
     * @param Client $client
     */
    private function provisionForJoint(Client $client)
    {
        $client->jointDetail->each(function (ClientJointDetail $jointDetail) use ($client) {
            $phone = $jointDetail->telephone_cell;

            if (!$phone) {
                $phone = $jointDetail->telephone_home ? $jointDetail->telephone_home : $jointDetail->telephone_office;
            }

            $this->provisionForIndividual(
                $client,
                $jointDetail->firstname,
                $jointDetail->middlename,
                $jointDetail->lastname,
                $phone,
                $jointDetail->email
            );
        });
    }

    /**
     * @param Client $client
     */
    private function provisionForCompanies(Client $client)
    {
        $phone = $client->phone;

        if (!$phone) {
            $phone = $client->telephone_home ? $client->telephone_home : $client->telephone_office;
        }

        $fname = $client->contact->corporate_registered_name;

        $this->provisionForIndividual(
            $client,
            $fname,
            '',
            '',
            $phone,
            $client->contact->email
        );
    }

    /**
     * @param Client $client
     * @param $firstname
     * @param $middlname
     * @param $lastname
     * @param $phone
     * @param $email
     */
    private function provisionForIndividual(Client $client, $firstname, $middlname, $lastname, $phone, $email)
    {
        list($code, $phone_s) = splitPhoneNumber($phone);

        $exists = ClientUser::where(['phone_country_code' => $code, 'phone' =>$phone_s])
            ->whereNotNull('phone')
            ->orWhere(function ($user) use ($code, $phone_s) {
                $user->where(['phone_country_code' => $code, 'phone' => '0'.$phone_s]);
            })->orWhere(function ($user) use ($email) {
                $user->whereNotNull('email')->where('email', $email);
            })->first();

        if ($exists) {
            $this->user = $exists;
            $this->assignAccess($client);

            $this->provisionPin($client);

            return;
        }

        $username = $this->generateUsername($firstname, $middlname, $lastname, $email);

        $user = (new ClientUser())->create([
            'phone' => str_replace(' ', '', trim($phone_s)),
            'phone_country_code' => $code,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email'    => $email,
            'username'=> str_replace(" ", "", trim($username))
        ]);

        $this->user = $user;

        $this->provisionUser();

        $this->assignAccess($client);

        $this->provisionPin($client);

        $this->activateClientUser($client);
    }

    private function activateClientUser(Client $client)
    {
        if($client->applications()->exists()){
            $application = $client->applications()->latest()->first();

            $clientFilled = ClientFilledInvestmentApplication::find($application->form_id);

            if($clientFilled->channel && $clientFilled->channel->slug == 'ussd'){
                $this->user->update(['active' => 1]);
            }
        }
    }

    private function provisionPin(Client $client)
    {
        if ($this->user->pin) {
            return;
        }

        $pin = rand(1001, 9998);

        $phone = $this->user->phone_country_code.ltrim($this->user->phone, '0');

        $this->user->update(['pin' => $pin]);

        $hasApplication = $client->applications()->has('unitFund')->exists();
        $hasPurchase = $client->unitFundPurchases()->exists();

        if ($hasPurchase || $hasApplication) {
            dispatch(new SendMessages($phone, 'onboarding', ['pin' => $pin, 'client_code'=>$client->client_code]));
        }
    }


    /**
     * Provision the new user and setup all necessities
     */
    public function provisionUser()
    {
        $this->registerAuthy();
        return $this->user;
    }

    public function updateUser()
    {
        $this->registerAuthy();
    }

    public function registerAuthy()
    {
        $authy = new Authy();

        $authy->registerUser($this->user);
    }

    /**
     * @return string
     */
    public function generateActivationKey()
    {
        $activation_key = sha1(md5(time()));

        $this->user->activation_key = $activation_key;

        $this->user->activation_key_created_at = Carbon::now();

        $this->user->save();

        return $activation_key;
    }

    /**
     * @return ClientUser
     */
    public function resendActivationKey()
    {
        $activationKey = $this->generateActivationKey();

        (new UserMailer())->sendClientActivationLink($this->user, $activationKey);

        return $this->user;
    }


    /**
     * Deactivate a client account
     *
     * @return bool
     */
    public function deactivate()
    {
        $this->user->active = false;
        return $this->user->save();
    }

    /**
     * Reactivate a client account
     *
     * @return bool
     */
    public function reactivate()
    {
        $this->user->active = true;
        return $this->user->save();
    }

    /**
     * Activate a user account after creation
     *
     * @param  $password
     * @return ClientUser
     */
    public function activateFirstTime($password)
    {
        //set password
        $this->user->password = $password;
        $this->user->active = true;
        $this->user->save();
        return $this->user;
    }

    /**
     * Connect a client to a user account
     *
     * @param  Client                    $client
     * @param  ClientTransactionApproval $approval
     * @return ClientUserAccess
     */
    public function assignAccess(Client $client, ClientTransactionApproval $approval = null)
    {
        if (ClientUserAccess::where(['user_id' => $this->user->id, 'client_id' => $client->id, 'active' => true])
            ->exists()) {
            return true;
        }

        $access = new ClientUserAccess();
        $access->user_id = $this->user->id;
        $access->client_id = $client->id;
        $access->approval_id = $approval ? $approval->id : null;
        $access->active = true;
        $access->save();

        $signature = $this->signature($client);

        if ($signature) {
            $this->user->clientSignatures()->save($signature, [ 'active' => true ]);
        }

        return $access;
    }

    public function signature(Client $client)
    {
        return $client->clientSignatures()
            ->whereDoesntHave('userClientSignatures')
            ->active(true)
            ->get()
            ->filter(function ($signature) {
                return $signature->present()->getEmail() == $this->user->email;
            })
            ->first();
    }

    public function hasAccess(Client $client)
    {
        return (bool) ClientUserAccess::where('client_id', $client->id)
            ->where('user_id', $this->user->id)
            ->where('active', 1)
            ->first();
    }

    public function revokeAccess($accessId)
    {
        $access = ClientUserAccess::findOrFail($accessId);
        $access->active = false;
        $access->save();

        return $access;
    }

    public function resetPassword()
    {
        $code = sha1(md5(time()));

        $reminder = new ClientPasswordReminder();
        $reminder->token = \Hash::make($code);
        $reminder->user_id = $this->user->id;
        $reminder->save();

        if ($this->user->email){
            $mailer = new UserMailer();
            $mailer->sendClientPasswordResetLink($this->user, $code);
        }else{
            dispatch(new SendMessages($this->user->phone, 'password-reset-link', ['user' => $this->user, 'code' => $code]));
        }
    }

    public static function generateUsername($firstname, $middlename, $lastname, $email)
    {
        $try = 0;

        while (true) {
            $username = UsernameGenerator::generate($firstname, $middlename, $lastname, $email, $try);

            $isAvailable = !ClientUser::where('username', $username)->exists();
            $isLong = strlen($username) >= 4;

            if ($isAvailable && $isLong) {
                break;
            }

            $try++;
        }

        return $username;
    }
}
