<?php
/**
 * Date: 09/12/2017
 * Time: 19:21
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Users;

use Laracasts\Presenter\Presenter;

class ClientUserPresenter extends Presenter
{
    public function fullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function cleanUsername()
    {
        return preg_replace('/[^A-Za-z0-9\-]/', '', $this->username);
    }
}
