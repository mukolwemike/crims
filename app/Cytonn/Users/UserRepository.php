<?php
/**
 * Date: 8/10/15
 * Time: 10:05 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Users;

use App\Cytonn\Models\PasswordReminder;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\UserMailer;
use Illuminate\Support\Collection;

/**
 * Class UserRepository
 *
 * @package Cytonn\Users
 */
/**
 * Class UserRepository
 *
 * @package Cytonn\Users
 */
class UserRepository
{
    /**
     * @var
     */
    public $user;

    /**
     * UserRepository constructor.
     *
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        if (is_null($user)) {
            $this->user = new User();
        } else {
            $this->user = $user;
        }
    }

    /**
     * @param array $userdata
     * @return mixed
     */
    public function save($userdata)
    {
        return $this->user->add($userdata);
    }

    /**
     * Send a password Reset link to the user
     */
    public function sendPasswordResetLink()
    {
        $code = sha1(md5(time()));

        $reminder = new PasswordReminder();
        $reminder->token = \Hash::make($code);
        $reminder->email = $this->user->email;
        $reminder->save();

        $mailer = new UserMailer();
        $mailer->sendPasswordResetlink($this->user, $code);
    }

    /**
     * Deactivate a user account
     */
    public function deactivate()
    {
        $this->user->active = 0;
        $this->user->save();
    }

    /**
     * Reactivate a deactivated user account
     */
    public function reactivate()
    {
        $this->user->active = 1;
        $this->user->save();
    }

    /*
     * Get a user by their id
     */
    public function getUserById($id)
    {
        return User::findOrFail($id);
    }

    /*
     * Check if a user exists by their email
     */
    public function getUserByEmail($email)
    {
        $user = User::where('email', $email)->first();

        return $user;
    }

    /*
     * Store a new user record or update an existing one
     */
    public function store($input, $id)
    {
        if ($id) {
            $user = $this->getUserById($id);

            $user->update($input);

            return $user;
        }

        return User::create($input);
    }

    public function removeInactive($users)
    {
        return collect($users)->reject(function ($user) {
            return User::where('email', $user)->where('active', 0)->first();
        })->all();
    }
}
