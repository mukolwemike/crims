<?php
/**
 * Date: 15/01/2016
 * Time: 3:54 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Users\Forms;

use Laracasts\Validation\FormValidator;

class UserClientForm extends FormValidator
{
    public $rules = [
        'email'=>'required|email',
        'phone'=>'required',
        'phone_country_code'=>'required',
        'firstname'=>'required',
        'lastname'=>'required'
    ];

    public function newUser()
    {
        $this->rules['username'] = 'required|unique:client_users';
        $this->rules['phone'] = 'required|unique:client_users,phone';
    }

    public $messages = [
        'phone.unique'=>'There is an account with this number in the system'
    ];
}
