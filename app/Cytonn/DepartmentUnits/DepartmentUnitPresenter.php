<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\DepartmentUnits;

use Laracasts\Presenter\Presenter;

class DepartmentUnitPresenter extends Presenter
{
    /*
     * Get the branch name
     */
    public function getBranch()
    {
        return $this->departmentBranch ? $this->departmentBranch->name : $this->name;
    }

    /*
     * Get the head
     */
    public function getHead()
    {
        return $this->head ? $this->head->name : '';
    }
}
