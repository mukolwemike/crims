<?php

namespace App\Cytonn\Billing;

use App\Cytonn\Models\Billing\ClientUtilityBills;
use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\Billing\UtilityBillingServices;
use App\Cytonn\Models\Billing\UtilityBillingType;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;

class BillingRepository
{
    public function saveBillingInstruction($input, Client $client, UtilityBillingServices $utility)
    {
        if (isset($input['checked']) && $input['checked']) {
            $this->saveClientBill($input, $client, $utility);
        }

        $this->saveBill($input, $client, $utility, 'bill');
    }

    public function saveBuyAirtimeRequest($input, Client $client, UtilityBillingServices $utility)
    {
        $this->saveBill($input, $client, $utility, 'airtime');
    }

    private function saveClientBill($input, Client $client, UtilityBillingServices $services)
    {
        $utility = new ClientUtilityBills();

        $utility->client()->associate($client);

        $utility->utility()->associate($services);

        $utility->account_name = $input['account_name'];

        $utility->account_number = $input['account_number'];

        $utility->smart_card_number = isset($input['smart_card_number']) ? $input['smart_card_number'] : '';

        $utility->meter_number = isset($input['meter_number']) ? $input['meter_number'] : '';

        $utility->save();
    }

    private function saveBill($input, Client $client, UtilityBillingServices $utility, $slug)
    {
        $instruction = new UtilityBillingInstructions();

        $type = UtilityBillingType::where('slug', $slug)->first();

        $instruction->type()->associate($type);

        $instruction->client()->associate($client);

        $instruction->utility()->associate($utility);

        $instruction->user_id = isset($input['user_id']) ? $input['user_id'] : null;

        $instruction->account_number = $input['account_number'];

        $instruction->smart_card_number = isset($input['smart_card_number']) ? $input['smart_card_number'] : '';

        $instruction->meter_number = isset($input['meter_number']) ? $input['meter_number'] : '';

        $instruction->amount = $input['amount'];

        $instruction->number = $input['number'] = $this->getNumber($input);

        $instruction->msisdn = $input['msisdn'];

        $instruction->date = isset($input['date']) ? Carbon::parse($input['date'])->toDateString() : Carbon::now()->toDateString();

        $instruction->unit_fund_id = isset($input['unit_fund_id']) ? $input['unit_fund_id'] : null;

        $instruction->investment_id = isset($input['investment_id']) ? $input['investment_id'] : null;

        $instruction->billing_status_id = 1; // pending

        $instruction->channel_id = $this->getChannelId($input);

        $instruction->payload = \GuzzleHttp\json_encode($input);

        $instruction->save();
    }

    private function getChannelId($data)
    {
        return isset($data['channel']) ? Channel::where('slug', $data['channel'])->first()->id : null;
    }

    /**
     * @param $data
     * @return float|int|mixed
     */
    private function getNumber($data)
    {
        if (!isset($data['unit_fund_id'])) {
            return $data['amount'];
        }

        $fund = UnitFund::findOrFail($data['unit_fund_id']);

        $price = $fund->unitPrice();

        return $price != 0 ? (float)$data['amount'] / $price : $data['amount'];
    }
}
