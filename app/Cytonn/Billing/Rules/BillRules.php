<?php

namespace App\Cytonn\Billing\Rules;

use Cytonn\Rules\Rules;

trait BillRules
{
    use Rules;

    private function globalRules()
    {
        return [
            'unit_fund_id' => 'required',
            'date' => 'required|date',
            'amount' => 'required|numeric:'
        ];
    }

    private function globalMessages()
    {
        return [
            'date.required' => 'Date Field is required.',
            'date.date' => 'Provide correct date format.',
            'unit_fund_id.required' => 'Specify source of funds before proceeding',
        ];
    }

    public function validateBill($request)
    {
        $rules = $this->globalRules() + [
                'account_name' => 'required',
                'account_number' => 'required',
            ];

        $messages = $this->globalMessages() + [
                'account_number.required' => 'Specify Account Number before proceeding.',
                'account_name.required' => 'Specify Account Name before proceeding',
            ];

        return $this->verdict($request, $rules, $messages);
    }

    public function validateAirtime($request)
    {
        $rules = $this->globalRules() + [
                'phone_number' => 'required',
            ];

        $messages = $this->globalMessages() + [
                'phone_number.required' => 'Phone number Field is required.',
            ];

        return $this->verdict($request, $rules, $messages);
    }
}
