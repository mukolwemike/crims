<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/17/18
 * Time: 11:49 AM
 */

namespace App\Cytonn\SMS\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Support\Notifiers\SMS\SMS;

class UnitFundActionSMSNotify
{
    public function phoneNumbers(Client $client)
    {
        $numbers = $client->getContactPhoneNumbersArray();

        return collect($numbers);
    }

    public function notifyOnPurchase(UnitFundPurchase $purchase)
    {
        $phoneNumbers = $this->phoneNumbers($purchase->client);

        $units = AmountPresenter::currency($purchase->number, true, 0);

        $price = AmountPresenter::currency($purchase->price);

        $fund = $purchase->unitFund;

        $more_info = $this->moreInformation($fund);

        $message = 'Purchase of '. $units . ' units at ' . $fund->currency->code . ' ' . $price .
            ' in our unit trust product offering specifically ' . $fund->name .
            ', has been processed. Business confirmation will be sent via email. ' . $more_info;

        SMS::queue($phoneNumbers->all(), $message);
    }

    public function notifyOnSale(UnitFundSale $sale)
    {
        $phoneNumbers = $this->phoneNumbers($sale->client);

        $units = AmountPresenter::currency($sale->number, true, 0);

        $price = AmountPresenter::currency($sale->price);

        $fund = $sale->fund;

        $more_info = $this->moreInformation($fund);

        $message = 'Sale of '. $units . ' units at ' . $fund->currency->code . ' ' . $price .
            ' in our unit trust product offering specifically ' . $fund->name .
            ', has been processed. Business confirmation will be sent via email. ' . $more_info;

        SMS::queue($phoneNumbers->all(), $message);
    }

    public function moreInformation(UnitFund $fund)
    {
        $company = str_contains($fund->manager->name, 'Seriani') ? 'seriani' : null;

        $cytonn = 'Please do not hesitate to contact us on +254 709 101 000 or email us at operations@cytonn.com';

        $seriani = 'Please do not hesitate to contact us on 0708 758 969 or email us at operations@serianiasset.com';

        return $company ? $seriani : $cytonn;
    }
}
