<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/17/18
 * Time: 12:48 PM
 */

namespace App\Cytonn\SMS\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Notifiers\SMS\SMS;

class NonComplianceSMSNotify
{
    public function notify(Client $client, UnitFund $fund)
    {
        $phoneNumbers = $client->getContactPhoneNumbersArray();

        $shortname = ClientPresenter::presentShortName($client->id);

        $message = 'Dear ' .$shortname .', Please submit your pending KYC documents for the investment in ' .
            $fund->name . '. Kindly note that your investment will not earn any interests if they remain 
            non compliant.' .
            'We thank you for your continued support and for choosing ' .$fund->manager->name .
            ' as your preferred Investment Manager';

        SMS::queue($phoneNumbers, $message);
    }
}
