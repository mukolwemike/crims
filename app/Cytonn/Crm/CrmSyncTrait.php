<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Crm;

use App\Jobs\Clients\SendDataToCrm;

trait CrmSyncTrait
{
    protected static function bootCrmSyncTrait()
    {
        static::created(function ($model) {
                static::createJob($model);
        });

        static::updated(function ($model) {
                static::createJob($model);
        });
    }

    /*
     * Get the initial input array
     */
    protected static function createJob($model)
    {
        if (env('APP_ENV') == 'production') {
            dispatch((new SendDataToCrm($model->table, $model->id))->onQueue(config('queue.priority.low')));
        }
    }
}
