<?php
/**
 * Date: 30/10/2015
 * Time: 3:57 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Reporting;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;

class BusinessConfirmationGenerator
{
    protected $protectDocument = false;

    public function generateBusinessConfirmation($client, $data)
    {
        $investment = ClientInvestment::find($data['investment_id']);

        is_null($investment) ? $product = Product::latest()->first() : $product = $investment->product;

        $interval = $investment->interest_payment_interval;

        if (empty($interval)) {
            $interval = null;
        }

        $interest_payment_date = $investment->interest_payment_date;

        $fm = $product->fundManager;

        $nextPaymentSchedule = $investmentPaymentScheduleDetail = null;
        if ($investment->product->present()->isSeip) {
            $investmentPaymentScheduleDetail = $investment->repo->getInvestmentPaymentScheduleDetail();

            $nextPaymentSchedule = $investmentPaymentScheduleDetail->investment->repo
                ->nextInvestmentPaymentSchedule($investment->invested_date);
        }

        $pdf = \PDF::loadView('reports.businessconfirmation', [
            'investment' => $investment,
            'client' => $client,
            'product' => $product,
            'interval' => $interval,
            'interest_payment_date' => $interest_payment_date,
            'data' => $data,
            'logo' => $product->fundManager->logo,
            'signature' => $fm->signature,
            'emailSender' => $fm->principal_partner ? $fm->principal_partner : $fm->fullname,
            'nextPaymentSchedule' => $nextPaymentSchedule,
            'investmentPaymentScheduleDetail' => $investmentPaymentScheduleDetail
        ]);

        if ($this->protectDocument) {
            $pdf->setEncryption($client->repo->documentPassword());
        }
        return $pdf;
    }

    /**
     * @param bool $protectDocument
     * @return BusinessConfirmationGenerator
     */
    public function setProtectDocument(bool $protectDocument): BusinessConfirmationGenerator
    {
        $this->protectDocument = $protectDocument;

        return $this;
    }
}
