<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;
use Maatwebsite\Excel\Facades\Excel;

class AllLOOsSentExcelGenerator
{
    public function excel($file_name, $loos_grouped_by_project)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($loos_grouped_by_project) {
                foreach ($loos_grouped_by_project as $project => $loos_arr) {
                    $project = Project::findOrFail($project);
                    $excel->sheet(
                        $project->name,
                        function ($sheet) use ($loos_arr) {
                            $loos = $loos_arr->map(
                                function ($loo) {
                                    $l = new EmptyModel();
                                    $l->Client_Code = is_null($loo->holding->client)
                                        ? null
                                        : $loo->holding->client->client_code;
                                    $l->Client_Name = ClientPresenter::presentFullNames($loo->holding->client_id);
                                    $l->Advocate = @$loo->advocate->name;
                                    $l->PM_Approver = UserPresenter::presentFullNames($loo->pm_approval_id);
                                    $l->Approved_On = DatePresenter::formatDate($loo->pm_approved_on);
                                    $l->Sent_On = DatePresenter::formatDate($loo->sent_on);
                                    $l->Sent_By = UserPresenter::presentFullNames($loo->sent_by);
                                    return $l;
                                }
                            );
                            $sheet->fromModel($loos);
                            $sheet->prependRow(['Letters of Offer Report - ' .
                                DatePresenter::formatDate(Carbon::today()->toDateString())]);
                            $sheet->mergeCells('A1:G1');
                        }
                    );
                }
            }
        );
    }
}
