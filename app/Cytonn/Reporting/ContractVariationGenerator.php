<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 4/1/19
 * Time: 8:49 AM
 */

namespace App\Cytonn\Reporting;

use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\RealEstate\RealEstateContractVariation;
use App\Cytonn\Models\RealestateLetterOfOffer;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Number;
use Cytonn\Core\Storage\StorageInterface;
use NumberFormatter;
use Webpatser\Uuid\Uuid;

class ContractVariationGenerator
{
    public function generate(RealEstateContractVariation $variation)
    {
        $holding = $variation->prevLoo ? $variation->prevLoo->holding :  $variation->loo->holding;

        $newHolding = $variation->loo->holding;

        $client = $holding->client;

        $schedules = $holding->paymentSchedules()->oldest('date')->get();

        $deposits  = $schedules->filter(
            function ($schedule) {
                return $schedule->type->slug == 'deposit';
            }
        );

        $total_deposit =  (int)$deposits->sum('amount');

        $reservation_fee = $holding->payments()->whereHas(
            'type',
            function ($type) {
                $type->where('slug', 'reservation_fee');
            }
        )->sum('amount');

        $net_deposit = (float) $total_deposit - (float) $reservation_fee;

        $deposit_in_words = ucwords(Number::toWords($net_deposit));

        $prev_price = (float) $variation->unit_price;

        $prev_price_in_words = ucwords(Number::toWords((int)$prev_price));

        $deposit_percentage = ($net_deposit/ $prev_price)*100;

        $date = Carbon::parse($variation->generated_on);

        $day_of = (new NumberFormatter('en_US', NumberFormatter::ORDINAL))->format($date->copy()->day);

        $month_of = $date->copy()->englishMonth;

        $year_of = $date->copy()->year;

        $vars = [
            'variation' => $variation,
            'client' => $client,
            'holding' => $holding,
            'newHolding' => $newHolding,
            'deposit_in_words' => $deposit_in_words,
            'net_deposit' => $net_deposit,
            'deposit_percentage' => $deposit_percentage,
            'day_of' => $day_of,
            'month_of' => $month_of,
            'year_of' => $year_of,
            'prev_price' => $prev_price,
            'prev_price_in_words' => $prev_price_in_words
        ];

        $view = 'realestate.contract_variation.variation_document';

        return \PDF::loadView($view, $vars);
    }

    public function upload($file_contents, $file_ext, RealestateLetterOfOffer $loo = null)
    {
        $filename = Uuid::generate()->string . '.' . $file_ext;

        $document = new Document();

        $url = (new RealEstateContractVariation())->getLocation() . '/' . $filename;

        \App::make(StorageInterface::class)->filesystem()->put($url, $file_contents);

        $document->type_id = DocumentType::where('slug', 'contract_variation')->first()->id;
        $document->filename = $filename;
        $document->save();

        return $document;
    }
}
