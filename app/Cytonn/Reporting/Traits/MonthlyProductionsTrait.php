<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  10/12/2018
 *
 */

namespace Cytonn\Reporting\Traits;

use Carbon\Carbon;

trait MonthlyProductionsTrait
{
    /*
     * Get the month to month production
    */
    public function getRecipientMonthsProduction($months, $recipientId)
    {
        $monthlyProduction = array();

        foreach ($months as $month) {
            $startOfMonth = Carbon::parse($month)->startOfMonth();

            $endOfMonth = Carbon::parse($month)->endOfMonth();

            $monthlyProduction[$month->format('F-Y')] = [
                'new_money' => $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 1),
                'topups' => $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 2),
                'rollovers' => $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 3),
                'rollover_inflow' => $this->getRolloverInflows($startOfMonth, $endOfMonth, $recipientId),
                'topup_inflow' => $this->getRolloverTopups($startOfMonth, $endOfMonth, $recipientId),
                'reinvested_interest' =>
                    $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 5),
                'partial_withdrawals' =>
                    $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 4),
                'reinvested_withdrawals' => $this->getCompetitionWithdrawals(
                    $startOfMonth,
                    $endOfMonth,
                    $recipientId,
                    null,
                    [2]
                ),
                'sent_to_client_withdrawals' => $this->getCompetitionWithdrawals(
                    $startOfMonth,
                    $endOfMonth,
                    $recipientId,
                    null,
                    [1]
                ),
                'withdrawals' => $this->getCompetitionWithdrawals(
                    $startOfMonth,
                    $endOfMonth,
                    $recipientId,
                    null,
                    [1, 2]
                ),
                'real_estate_actuals' => $this->realEstateActualPayments($startOfMonth, $endOfMonth, $recipientId),
            ];
        }

        $startOfMonth = Carbon::parse($months[0])->startOfMonth();

        $endOfMonth = Carbon::parse($months[count($months) - 1])->endOfMonth();

        $monthlyProduction['Total'] = [
            'new_money' => $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 1),
            'topups' => $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 2),
            'rollovers' => $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 3),
            'reinvested_interest' =>
                $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 5),
            'partial_withdrawals' =>
                $this->getInflows($startOfMonth, $endOfMonth, $recipientId, null, 4),
            'rollover_inflow' => $this->getRolloverInflows($startOfMonth, $endOfMonth, $recipientId),
            'topup_inflow' => $this->getRolloverTopups($startOfMonth, $endOfMonth, $recipientId),
            'reinvested_withdrawals' => $this->getCompetitionWithdrawals(
                $startOfMonth,
                $endOfMonth,
                $recipientId,
                null,
                [2]
            ),
            'sent_to_client_withdrawals' => $this->getCompetitionWithdrawals(
                $startOfMonth,
                $endOfMonth,
                $recipientId,
                null,
                [1]
            ),
            'withdrawals' => $this->getCompetitionWithdrawals(
                $startOfMonth,
                $endOfMonth,
                $recipientId,
                null,
                [1, 2]
            ),
            'real_estate_actuals' => $this->realEstateActualPayments($startOfMonth, $endOfMonth, $recipientId)
        ];

        return $monthlyProduction;
    }
}
