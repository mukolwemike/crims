<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Maatwebsite\Excel\Facades\Excel;

class DailyWithdrawalReportExcelGenerator
{
    public function excel(
        $file_name,
        $withdrawals,
        $interest_payments,
        $rollovers,
        Carbon $start,
        Carbon $end,
        $monthly = false
    ) {

        return Excel::create(
            $file_name,
            function ($excel) use ($withdrawals, $interest_payments, $rollovers, $start, $end, $monthly) {
                $excel->sheet(
                    'Withdrawals',
                    function ($sheet) use ($withdrawals, $start, $end, $monthly) {
                        $withdrawals = $withdrawals->map(
                            function ($withdrawal) {
                                $investment = $withdrawal->investment;

                                $w = new EmptyModel();
                                $w->{'Client Code'} = $investment->client->client_code;
                                $w->{'Client Name'} = ClientPresenter::presentFullNames($investment->client_id);
                                $w->{'Amount Invested'} = $investment->amount;
                                $w->{'Value Date'} = DatePresenter::formatDate($investment->invested_date);
                                $w->{'Maturity Date'} = DatePresenter::formatDate($investment->maturity_date);
                                $w->{'Amount Withdrawn'} = (float)$withdrawal->amount;
                                $w->{'FA Name'} = (is_null($investment->client->relationshipPerson))
                                    ? $investment->commission->recipient->name
                                    : $investment->client->relationshipPerson->name;
                                $w->{'FA Position'} = (is_null($investment->client->relationshipPerson))
                                    ? $investment->commission->recipient->type->name
                                    : $investment->client->relationshipPerson->type->name;
                                $w->{'Date'} = DatePresenter::formatDate($withdrawal->date);
                                return $w;
                            }
                        );

                        $sheet->fromModel($withdrawals);

                        if ($monthly) {
                            $sheet->prependRow(['Monthly withdrawals - ' .
                                DatePresenter::formatDate($start->copy()->toDateString()) . ' to ' .
                                DatePresenter::formatDate($end->copy()->toDateString())]);
                        } else {
                            $sheet->prependRow(['Today\'s withdrawals - ' .
                                DatePresenter::formatDate($start->copy()->toDateString())]);
                        }

                        $sheet->mergeCells('A1:E1');
                    }
                );

                // Rollovers
                $excel->sheet(
                    'Rollovers',
                    function ($sheet) use ($rollovers, $start, $end, $monthly) {
                        $rollover = $rollovers->map(
                            function ($withdrawal) use ($start, $end) {
                                $investment = $withdrawal->investment;

                                $reinvested = (float)$withdrawal->amount;

                                $total = (float)$investment->repo
                                    ->getTotalValueOfInvestmentAtDate(Carbon::parse($withdrawal->date)->subDay(), true);

                                $descendant = ($withdrawal->reinvestedTo)
                                    ? $withdrawal->reinvestedTo
                                    : ClientInvestment::where('approval_id', $withdrawal->approval_id)->first();

                                $withdrawn = ClientInvestmentWithdrawal::where('approval_id', $withdrawal
                                    ->approval_id)->where('type_id', 1)
                                    ->where('withdraw_type', 'withdrawal')->sum('amount');

                                $i = new EmptyModel();
                                $i->{'Client Code'} = $investment->client->client_code;
                                $i->{'Client Name'} = ClientPresenter::presentFullNames($investment->client_id);
                                $i->Principal = (float)$investment->amount;
                                $i->{'Value Date'} = $investment->invested_date->toDateString();
                                $i->{'Maturity Date'} = $investment->maturity_date->toDateString();
                                $i->{'Withdrawn On'} = Carbon::parse($withdrawal->date)->toFormattedDateString();
                                $i->{'Value at Maturity'} = $total;
                                $i->{'Amount Withdrawn'} = $withdrawn;
                                $i->{'Amount Reinvested'} = (float)$reinvested;
                                $i->{'Tenor'} = ($descendant)
                                    ? $descendant->maturity_date->diffInMonths($descendant->invested_date) : '';
                                $i->{'New Maturity Date'} = ($descendant)
                                    ? $descendant->maturity_date->toDateString() : '';
                                $i->Product = $investment->product->name;
                                $i->Type = $withdrawal->type;
                                $i->{'FA Name'} = (is_null($investment->client->relationshipPerson))
                                        ? $investment->commission->recipient->name
                                        : $investment->client->relationshipPerson->name;
                                $i->{'FA Position'} = (is_null($investment->client->relationshipPerson))
                                    ? $investment->commission->recipient->type->name
                                    : $investment->client->relationshipPerson->type->name;

                                return $i;
                            }
                        );

                        $sheet->fromModel($rollover);

                        if ($monthly) {
                            $sheet->prependRow(['Monthly Rollovers - ' .
                                DatePresenter::formatDate($start->copy()->toDateString()) . ' to ' .
                                DatePresenter::formatDate($end->copy()->toDateString())]);
                        } else {
                            $sheet->prependRow(['Today\'s Rollovers - ' .
                                DatePresenter::formatDate($start->copy()->toDateString())]);
                        }

                        $sheet->mergeCells('A1:E1');
                    }
                );

                // Interest Payments
                $excel->sheet(
                    'Interest Payments',
                    function ($sheet) use ($interest_payments, $start, $end, $monthly) {
                        $interest_payments = $interest_payments->map(
                            function ($withdrawal) {
                                $investment = $withdrawal->investment;

                                $i = new EmptyModel();
                                $i->{'Client Code'} = $investment->client->client_code;
                                $i->{'Client Name'} = ClientPresenter::presentFullNames($investment->client_id);
                                $i->{'Product'} = $investment->product->name;
                                $i->{'Amount Invested'} = $investment->amount;
                                $i->{'Value Date'} = DatePresenter::formatDate($investment->invested_date);
                                $i->{'Maturity Date'} = DatePresenter::formatDate($investment->maturity_date);
                                $i->{'Interest Paid'} = (float)$withdrawal->amount;
                                $i->{'Action'} = $withdrawal->type->name;
                                $i->FA = $investment->commission->recipient->name;
                                $i->{'Date'} = DatePresenter::formatDate($withdrawal->date);
                                return $i;
                            }
                        );
                        $sheet->fromModel($interest_payments);

                        if ($monthly) {
                            $sheet->prependRow(['Monthly Interest Payments - ' .
                                DatePresenter::formatDate($start->copy()->toDateString()) . ' to ' .
                                DatePresenter::formatDate($end->copy()->toDateString())]);
                        } else {
                            $sheet->prependRow(['Today\'s Interest Payments - ' .
                                DatePresenter::formatDate($start->copy()->toDateString())]);
                        }

                        $sheet->mergeCells('A1:E1');
                    }
                );
            }
        );
    }
}
