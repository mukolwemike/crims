<?php
/**
 * Date: 11/11/2015
 * Time: 5:56 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class ClientStatementExcelGenerator
{
    public function excel()
    {
        $date = Carbon::today();
        return Excel::create(
            'CHYS Statements ' . $date,
            function ($excel) {
                $excel->sheet(
                    'Summary',
                    function ($sheet) {
                        $client_coll = new Collection();
                        $clients = Client::all();

                        foreach ($clients as $client) {
                            foreach (Product::all() as $product) {
                                if ($client->repo->hasProduct($product)) {
                                    $c = new EmptyModel();
                                    $c->Name = ClientPresenter::presentFullNames($client->id);
                                    $c->ClientCode = $client->client_code;
                                    $c->Product = $product->name;
                                    $c->AmountInvested = AmountPresenter::currency($client
                                        ->repo->getTodayInvestedAmountForProduct($product));
                                    $c->NetInterest = AmountPresenter::currency($client
                                        ->repo->getTodayTotalInterestForProduct($product));
                                    $c->Withdrawal = AmountPresenter::currency($client
                                        ->repo->getTodayTotalPaymentForProduct($product));
                                    $c->TotalValue = AmountPresenter::currency($client
                                        ->repo->getTodayTotalInvestmentsValueForProduct($product));


                                    $client_coll->push($c);
                                }
                            }
                        }

                        $sheet->fromModel($client_coll);
                    }
                );

                foreach (Client::all() as $client) {
                    $excel->sheet(
                        $client->name(),
                        function ($sheet) use ($client) {
                            $sheet->loadView('reports.excelstmt', ['client' => $client]);
                        }
                    );
                }
            }
        );
    }
}
