<?php
/**
 * Date: 9/15/15
 * Time: 12:08 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Reporting;

use App\Cytonn\Models\CampaignStatementItem;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\StatementBatch;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\StatementJob;
use App\Cytonn\Models\StatementLog;
use App\Cytonn\Models\StatementSendRequest;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Investment\Statements\StatementProcessor;
use Cytonn\Mailers\StatementMailer;
use Cytonn\Portfolio\Statement\Generator;

/**
 * Class ClientStatementGenerator
 *
 * @package Cytonn\Reporting
 */
class ClientStatementGenerator
{
    private $view = 'investment.statement.doc.statement';

    private $view_table = 'investment.statement.doc.table';

    protected $protectOutput = false;

    /**
     * @param $productId
     * @param $clientId
     * @param null $campaignId
     * @param null $userId
     * @param null $from
     * @param null $statementDate
     * @param string $template
     * @param string $format
     * @return mixed
     * @throws \Exception
     */
    public function generateStatementForClient(
        $productId,
        $clientId,
        $campaignId = null,
        $userId = null,
        $from = null,
        $statementDate = null,
        $template = 'compact',
        $format = 'pdf'
    ) {
        if (!is_null($from)) {
            $from = Carbon::parse($from);
        }
        if (!is_null($statementDate)) {
            $statementDate = Carbon::parse($statementDate);
        }
        if (is_null($format)) {
            $format = 'pdf';
        }
        if (is_null($template)) {
            $template = 'compact';
        }

        $data = $this->viewData($productId, $clientId, $campaignId, $userId, $statementDate, $from, $template);

        if ($format == 'pdf') {
            $pdf = \PDF::loadView($this->view, $data);

            if ($this->protectOutput) {
                $client = Client::findOrFail($clientId);

                $pdf->setEncryption($client->repo->documentPassword());
            }
            return $pdf;
        }

        if ($format == 'excel') {
            return $this->excelWorkbook($clientId, $productId, $from, $statementDate, $template);
        }

        throw new \InvalidArgumentException("Unknown format $format");
    }

    public function excelWorkbook($clientId, $productId, $start, $end, $template)
    {
        $client = Client::find($clientId);
        $product = Product::find($productId);

        return \Excel::create('Statement', function ($excel) use ($client, $product, $start, $end, $template) {
            $excel->sheet('Statement', function ($sheet) use ($client, $product, $start, $end, $template) {
                $this->excelSheet($sheet, $product, $client, $end, $start, $template);

                $sheet->setColumnFormat(array(
                    'B' => '#,##0.00',
                    'C' => '0%',
                    'D' => 'dd/mm/yy',
                    'E' => '#,##0.00',
                    'F' => '#,##0.00',
                    'G' => '#,##0.00',
                    'H' => '#,##0.00',
                    'I' => 'dd/mm/yy',
                ));
            });
        });
    }

    public function excelSheet(
        $sheet,
        Product $product,
        Client $client,
        Carbon $statementDate,
        Carbon $from = null,
        $template = 'compact'
    ) {
        return $sheet->loadView($this->view_table, $this->viewData(
            $product->id,
            $client->id,
            null,
            null,
            $statementDate,
            $from,
            $template
        ));
    }

    private function viewData(
        $productId,
        $clientId,
        $campaignId = null,
        $userId = null,
        $statementDate = null,
        $from = null,
        $template = 'compact'
    ) {
        is_null($statementDate) ? $statementDate = Carbon::now() : $statementDate = new Carbon($statementDate);

        $client = Client::with('investments')->findOrfail($clientId);

        $product = Product::findOrFail($productId);

        $campaign = ($campaignId) ? StatementCampaign::findOrFail($campaignId) : null;

        $expanded = $template != 'compact';

        $stmt_data = (new StatementProcessor())->process($client, $product, $statementDate, $from, $expanded);

        return [
            'client' => $client,
            'statementDate' => $statementDate,
            'data' => $stmt_data,
            'product' => $product,
            'sender' => $userId,
            'logo' => $product->fundManager->logo,
            'template' => $template,
            'campaign' => $campaign
        ];
    }

    /**
     * Send all the client statements
     *
     * @param $batch
     * @return string
     * @throws \Throwable
     */
    public function sendAllStatements($batch)
    {
        return \Queue::push(
            'Cytonn\Reporting\ClientStatementGenerator@fireSendAllStatements',
            ['batch_id' => $batch->id]
        );
    }

    /**
     * Generate all statements
     *
     * @param $job
     * @param $data
     * @throws \Throwable
     */
    public function fireSendAllStatements($job, $data)
    {
        $clients = Client::has('investments')->get();

        $batch = StatementBatch::findOrNew($data['batch_id']);
        $batch->total_jobs = $clients->count();
        $batch->save();

        foreach ($clients as $client) {
            \Queue::push(
                'Cytonn\Reporting\ClientStatementGenerator@fireIndividualStatement',
                ['clientId' => $client->id, 'batch_id' => $data['batch_id']]
            );
        }

        $job->delete();
    }

    /**
     * Send the individual statement
     *
     * @param $job
     * @param $data
     * @throws \Exception
     */
    public function fireIndividualStatement($job, $data)
    {
        $campaign = CampaignStatementItem::findOrFail($data['item_id'])->campaign;
        $data['statement_date'] = $campaign->send_date;

        isset($data['statement_date']) ? $statementDate = new Carbon($data['statement_date']) :
            $statementDate = Carbon::now();

        if (isset($data['batch_id'])) {
            $request_id = StatementBatch::find($data['batch_id'])->request_id;
            $userId = StatementSendRequest::find($request_id)->approved_by;
            $client = Client::find($data['clientId']);

            $latest_investment = ClientInvestment::where('client_id', $client->id)
                ->orderBy('maturity_date', 'DESC')->first();

            if ($latest_investment->withdrawn) {
                $end_date = new Carbon($latest_investment->withdrawal_date);
            } else {
                $end_date = new Carbon($latest_investment->maturity_date);
            }

            $withdrawnAfterLastMonth = $end_date->copy()->gte($statementDate->copy()->subMonth());

            if ($client->repo->isActive() || $withdrawnAfterLastMonth) {
                $batch_stmt = new StatementJob();
                $batch_stmt->batch_id = $data['batch_id'];
                $batch_stmt->client_id = $client->id;
                $batch_stmt->save();

                $stmt = new ClientStatementGenerator();
                $stmt->setProtectOutput(true);

                $stmt->sendStatementToClient($client, $statementDate, $campaign->id, $userId);
                $batch_stmt->complete = true;
                $batch_stmt->save();

                $log = new StatementLog();
                $log->fill(['client_id' => $client->id, 'sent_by' => $userId]);
                $log->save();
            }
        } elseif (isset($data['campaign_id'])) {
            $campaign = StatementCampaign::findOrFail($data['campaign_id']);

            //require client id, sender id,
            $client = Client::findOrFail($data['client_id']);
            $stmt = new ClientStatementGenerator();

            $stmt->sendStatementToClient($client, $campaign->send_date, $campaign->id, $campaign->sender_id);

            $log = new StatementLog();
            $log->fill(['client_id' => $data['client_id'], 'sent_by' => $data['user_id']]);
            $log->save();
        } else {
            //require client id, sender id,
            $client = Client::findOrFail($data['client_id']);
            $stmt = new ClientStatementGenerator();

            $stmt->sendStatementToClient($client, $statementDate, $campaign->id, $data['user_id']);

            $log = new StatementLog();
            $log->fill(['client_id' => $client->id, 'sent_by' => $data['user_id']]);
            $log->save();
        }

        $job->delete();
    }

    /**
     * Send a campaign statement item
     *
     * @param CampaignStatementItem $item
     * @throws \Exception
     */
    public function sendCampaignStatementItem(CampaignStatementItem $item)
    {
        $campaign = $item->campaign;

        //require client id, sender id,
        $client = Client::findOrFail($item->client_id);

        if (is_null($client->getContactEmailsArray())) {
            return;
        }

        $item = CampaignStatementItem::find($item->id);

        if (!$item->sent) {
            $this->sendStatementToClient($client, $campaign->send_date, $campaign->id, $campaign->sender_id);

            $log = new StatementLog();
            $log->fill(['client_id' => $item->client_id, 'sent_by' => $campaign->sender_id]);
            $log->save();

            $item->sent = true;
//            $item->sent_on = Carbon::now();
            $item->sent_on = $campaign->send_date;//Changed to date send date of the campaign
            $item->save();
        }
    }

    /**
     * Send one client statement
     * @param $client
     * @param $userId
     * @param $statementDate
     * @param $campaignId
     * @throws \Exception
     */
    public function sendStatementToClient($client, $statementDate, $campaignId, $userId = null)
    {
        if (is_null($client->getContactEmailsArray())) {
            \Flash::message('Client has no emails to send statement to');

            return;
        }

        $statements = [];

        $sender = ($userId) ? User::find($userId) : null;

        $products = Product::whereHas('investments', function ($investment) use ($client, $statementDate) {
            $investment->statement($statementDate)->where('client_id', $client->id);
        })->whereHas('fundManager', function ($fm) {
            $fm->where('active', 1);
        })->get();

        foreach ($products as $product) {
            $this->setProtectOutput(true);

            $statements[$product->name . ' Statement.pdf']
                = $this->generateStatementForClient(
                    $product->id,
                    $client->id,
                    $campaignId,
                    $userId,
                    null,
                    $statementDate
                )->output();
        }

        $portfolioSecurities = $client->portfolioSecurities;

        foreach ($portfolioSecurities as $portfolioSecurity) {
            $fundManager = $portfolioSecurity->fundManager;

            foreach ($portfolioSecurity->repo->getCurrencies($fundManager) as $currency) {
                $statements['Portfolio Statement - ' . $portfolioSecurity->name . ' - ' .
                $fundManager->name . ' - ' . $currency->name . '.pdf'] =
                    (new Generator())->generate(
                        $fundManager,
                        $currency,
                        $portfolioSecurity,
                        $statementDate,
                        null,
                        $sender,
                        $campaignId
                    )->output();
            }
        }

        $campaign = ($campaignId) ? StatementCampaign::findOrFail($campaignId) : null;

        (new StatementMailer())->sendStatementToClient($client, $statements, $statementDate, $sender, $campaign);
    }


    /**
     * @param $job
     * @param $data
     */
    public function fireCheckStatementSendStatus($job, $data)
    {
        $batch = StatementBatch::find($data['batch_id']);

        $progress = $this->checkSendProgress($batch);

        if ($progress == 1) {
            $batch->sent = true;
            $batch->save();
        } else {
            $job->release();
        }

        $job->delete();
    }

    /**
     * @param $batch
     * @return float
     */
    protected function checkSendProgress($batch)
    {
        $complete_jobs = StatementJob::where('complete', 1)->where(
            'batch_id',
            $batch->id
        )->count();

        return $complete_jobs / $batch->total_jobs;
    }

    /**
     * @param bool $protectOutput
     * @return ClientStatementGenerator
     */
    public function setProtectOutput(bool $protectOutput): ClientStatementGenerator
    {
        $this->protectOutput = $protectOutput;
        return $this;
    }
}
