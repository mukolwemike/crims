<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Number;
use Cytonn\Lib\Helper;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use RealestateLetterOfOffer;

class LooGenerator
{
    public function generate(UnitHolding $unitHolding, $format = 'pdf')
    {
        $sender_id = Auth::user()->id;

        $purchasersAdvocate = $unitHolding->purchasers_advocate;

        $client = $unitHolding->client;
        $fa = $client->getLatestFa('realestate');
        $unit = $unitHolding->unit;
        $schedules = $unitHolding->paymentSchedules()->oldest('date')->get();
        $project = $unit->project;

        $installments = $schedules->filter(
            function ($schedule) {
                return $schedule->type->slug == 'installment';
            }
        );

        $deposits  = $schedules->filter(
            function ($schedule) {
                return $schedule->type->slug == 'deposit';
            }
        );

        $price_in_words = ucwords(Number::toWords((int)$unitHolding->price()));

        $total_deposit =  (int)$deposits->sum('amount');

        $reservation_fee = $unitHolding->payments()->whereHas(
            'type',
            function ($type) {
                $type->where('slug', 'reservation_fee');
            }
        )->sum('amount');

        $net_deposit = $total_deposit - $reservation_fee;
        $deposit_in_words = ucwords(Number::toWords($net_deposit));

        $balance_after_deposit = $unitHolding->price() - $total_deposit;
        //        $balance_after_deposit_in_words = ucwords(Number::toWords($balance_after_deposit));
        $balance_after_deposit_days = 90;

        foreach ($schedules as $schedule) {
            if ($schedule->days) {
                $balance_after_deposit_days = $schedule->days;
            }

            $schedule->amount_in_words = ucwords(Number::toWords($schedule->amount));
        }

        $email_sender = UserPresenter::presentLetterClosingNoSignature($sender_id);

        $ordinaliseWords = function ($number) {
            return Number::ordinaliseWords($number);
        };

        $paid = $unitHolding->payments()->sum('amount');

        $overDeposit = 0;
        if ($net_deposit < 0) {
            $balance_after_deposit += $net_deposit;
            $overDeposit = abs($net_deposit);
        }
        $balance_after_deposit_in_words = ucwords(Number::toWords($balance_after_deposit));

        $toWords = function ($amount) {
            return ucwords(Number::toWords($amount));
        };

        $vars = [
            'unitHolding'       =>  $unitHolding,
            'holding'           => $unitHolding,
            'purchasers_advocate' => $purchasersAdvocate,
            'loo'               => $unitHolding->loo,
            'client'            =>  $client,
            'fa'                =>  $fa,
            'unit'              =>  $unit,
            'schedules'         =>  $schedules,
            'installments'      =>  $installments,
            'reservation_fee'   =>  $reservation_fee,
            'paid'              =>  $paid,
            'cash_payment_days' =>  $balance_after_deposit_days,
            'project'           =>  $project,
            'price_in_words'    =>  $price_in_words,
            'deposits'          =>  $deposits,
            'total_deposit'     =>  $total_deposit,
            'net_deposit'       =>  $net_deposit,
            'overDeposit'      =>  $overDeposit,
            'deposit_in_words'  =>  $deposit_in_words,
            'balance_after_deposit'             =>  $balance_after_deposit,
            'balance_after_deposit_in_words'    =>  $balance_after_deposit_in_words,
            'email_sender'      =>  $email_sender,
            'logo'              =>$unitHolding->project->fundManager->re_logo,
            'ordinaliseWords'   => $ordinaliseWords,
            'toWords'           => $toWords,
            'pm_id'             => $unitHolding->loo->pm_approval_id
        ];

        $view = 'realestate.loos.projects.default';

        if ($t = $project->loo_template) {
            $view = 'realestate.loos.projects.'.$t;
        }

        return \PDF::loadView($view, $vars);
    }
}
