<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Product;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class InterestPaymentsReportExcelGenerator
{
    public function excel($interest_payments_grouped_by_product, $file_name)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($interest_payments_grouped_by_product, $file_name) {
                foreach ($interest_payments_grouped_by_product as $product => $interest_payments_arr) {
                    $product = Product::findOrFail($product);

                    $excel->sheet(
                        $product->name,
                        function ($sheet) use ($interest_payments_arr, $file_name) {
                            $interest_payments = Collection::make($interest_payments_arr)->map(
                                function ($interest_payment) {
                                    $ip = new EmptyModel();

                                    $ip->Client_Code = $interest_payment->investment->client->client_code;
                                    $ip->Name =
                                        ClientPresenter::presentFullNames($interest_payment->investment->client_id);
                                    $ip->Principal = $interest_payment->investment->amount;
                                    $ip->Interest_Rate =
                                        AmountPresenter::currency($interest_payment->investment->interest_rate) . '%';
                                    $ip->Value_Date =
                                        DatePresenter::formatDate($interest_payment->investment->invested_date);
                                    $ip->Maturity_Date =
                                        DatePresenter::formatDate($interest_payment->investment->maturity_date);
                                    $ip->Amount_Paid = AmountPresenter::currency($interest_payment->amount);
                                    $ip->Date_Paid = DatePresenter::formatDate($interest_payment->date_paid);


                                    return $ip;
                                }
                            );

                            $sheet->fromModel($interest_payments);
                            $sheet->prependRow([$file_name]);

                            $sheet->mergeCells('A1:H1');
                            $sheet->cells(
                                'A1:H1',
                                function ($cells) {
                                    $cells->setFontWeight('bold');
                                }
                            );

                            $sheet->cells(
                                'A2:H2',
                                function ($cells) {
                                    $cells->setFontWeight('bold');
                                }
                            );
                        }
                    );
                }
            }
        );
    }
}
