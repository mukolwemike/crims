<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\FundManager;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Maatwebsite\Excel\Facades\Excel;

class ComplianceReportExcelGenerator
{
    public function excel($file_name, $clients_grouped_by_fund_manager)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($clients_grouped_by_fund_manager) {
                foreach ($clients_grouped_by_fund_manager as $fund_manager => $clients_arr) {
                    $excel->sheet(
                        FundManager::findOrFail($fund_manager)->name,
                        function ($sheet) use ($clients_arr) {
                            $clients = Collection::make($clients_arr)->map(
                                function ($client) {
                                    $c = new EmptyModel();
                                    $c->Client_Code = $client->client_code;
                                    $c->Client_Name = ClientPresenter::presentFullNames($client->id);
                                    $c->Email = $client->contact->email;
                                    $c->Phone = $client->phone;
                                    $c->Telephone_Officice = $client->telephone_office;
                                    $c->Telephone_Home = $client->telephone_home;
                                    $c->Compliant = $client->compliant;
                                    return $c;
                                }
                            );
                            $sheet->fromModel($clients);
                            $sheet->prependRow(['Compliance Report - ' . DatePresenter::formatDate(Carbon::today()
                                    ->toDateString())]);
                            $sheet->mergeCells('A1:F1');
                        }
                    );
                }
            }
        );
    }
}
