<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Maatwebsite\Excel\Facades\Excel;

class ClientDeductionsExcelGenerator
{
    public function excel(Carbon $startDate, Carbon $endDate)
    {
        return Excel::create(
            'Client Deductions ' . $startDate->toDateString() . ' - ' . $endDate->toDateString(),
            function ($excel) use ($startDate, $endDate) {
                $excel->sheet(
                    'Deductions',
                    function ($sheet) use ($startDate, $endDate) {
                        $deductions = ClientInvestmentWithdrawal::where('date', '>=', $startDate)
                            ->where('date', '<=', $endDate)
                            ->where('withdraw_type', 'deduction')
                            ->get()
                            ->map(
                                function ($deduction) use ($endDate) {
                                    $invRepo = (new InvestmentsRepository($deduction->investment));
                                    $d = new EmptyModel();

                                    $d->ClientName =
                                        ClientPresenter::presentFullNames($deduction->investment->client_id);
                                    $d->ClientCode = $deduction->investment->client->client_code;
                                    $d->Principal = $deduction->investment->amount;
                                    $d->InterestRate = AmountPresenter::currency($deduction->investment->interest_rate);
                                    $d->ValueDate = DatePresenter::formatDate($deduction->investment->invested_date);
                                    $d->MaturityDate = DatePresenter::formatDate($deduction->investment->maturity_date);
                                    $d->Tenor = $invRepo->getTenorInMonths();

                                    $d->DeductionAmount = $deduction->amount;
                                    $d->Date = DatePresenter::formatDate($deduction->date);
                                    $d->WithdrawalDate = $deduction->investment->withdrawal_date;
                                    $d->DateOfDeposit = $deduction->date->toDateString();
                                    $d->InterestEarned =
                                        AmountPresenter::currency($invRepo->getNetInterestForInvestment());

                                    //                        $d->InterestCharged = '';
                                    //                        $d->Penalty = '';
                                    $d->Scheduled_Commission =
                                        AmountPresenter::currency($invRepo->getScheduledCommissions());
                                    $d->Clawbacks = AmountPresenter::currency($invRepo->getCommissionClawbacks());
                                    $d->Net_Commission =
                                        AmountPresenter::currency($invRepo->getCommissionsPaidFromInvestment());

                                    return $d;
                                }
                            );

                        $sheet->fromModel($deductions);

                        $sheet->prependRow([$startDate->formatLocalized('%A %d %B %Y') . ' - ' .
                            $endDate->formatLocalized('%A %d %B %Y')]);
                        $sheet->prependRow(['Client Investment Deductions']);
                        $sheet->mergeCells('A1:G1');
                        $sheet->mergeCells('A2:G2');
                    }
                );
            }
        );
    }
}
