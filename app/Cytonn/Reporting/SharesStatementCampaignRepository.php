<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesCampaignStatementItem;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesStatementCampaign;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\User;
use Carbon\Carbon;

/**
 * Class SharesStatementCampaignRepository
 *
 * @package Cytonn\Reporting
 */
class SharesStatementCampaignRepository
{
    /**
     * @var null|StatementCampaign
     */
    public $campaign;

    /**
     * SharesStatementCampaignRepository constructor.
     *
     * @param SharesStatementCampaign|null $campaign
     */
    public function __construct(SharesStatementCampaign $campaign = null)
    {
        is_null($campaign) ? $this->campaign = new SharesStatementCampaign() : $this->campaign = $campaign;
    }

    /**
     * Add a client to a campaign
     *
     * @param  ShareHolder $holder
     * @param  User $user
     * @return bool
     */
    public function addShareHolderToCampaign(ShareHolder $holder, User $user)
    {
        $has_holdings = $holder->has('shareHoldings')
            ->get()
            ->filter(
                function ($holding) {
                    return $holding->sum('number') > 0;
                }
            )
            ->count();

        if ($has_holdings and !$this->checkIfShareHolderIsInCampaign($holder)) {
            return (new SharesCampaignStatementItem())
                ->fill(
                    [
                        'share_holder_id' => $holder->id,
                        'campaign_id' => $this->campaign->id,
                        'added_by' => $user->id,
                        'added_on' => Carbon::now()
                    ]
                )->save();
        }
        return false;
    }

    /**
     * Check whether a share holder is already in a campaign
     *
     * @param  ShareHolder $holder
     * @return bool
     */
    public function checkIfShareHolderIsInCampaign(ShareHolder $holder)
    {
        $item = SharesCampaignStatementItem::where('campaign_id', $this->campaign->id)
            ->where('share_holder_id', $holder->id)->first();

        return !is_null($item);
    }

    /**
     * Queue a job to Send all unsent statements in a campaign
     */
    public function sendCampaign(User $user = null)
    {
        $this->campaign->sent_by = $user ? $user->id : \Auth::user()->id;
        $this->campaign->save();

        SharesCampaignStatementItem::where('campaign_id', $this->campaign->id)
            ->where('sent', null)
            ->update(['ready' => true]);

//        \Queue::push(static::class . '@fireSendCampaign', ['campaign_id' => $this->campaign->id]);
    }

    /**
     * Send the campaign
     *
     * @param $job
     * @param $data
     */
    public function fireSendCampaign($job, $data)
    {
        SharesCampaignStatementItem::where('campaign_id', $data['campaign_id'])
            ->where('sent', null)
            ->get()
            ->each(
                function ($item) {
                    $this->sendCampaignItem($item);
                }
            );

        $job->delete();
    }

    /**
     * Send a statement in a campaign to a client
     *
     * @param $item
     */
    public function sendCampaignItem($item)
    {
        $item->ready = true;
        $item->save();
    }

    /**
     * @return mixed
     */
    public function countSentItems()
    {
        return $this->campaign->items()->where('sent', 1)->count();
    }

    /**
     * @return mixed
     */
    public function countAllItems()
    {
        return $this->campaign->items()->count();
    }

    /**
     * Automatically add the clients to campaign
     */
    public function automaticallyAddClients()
    {
        //disabled for coop
        throw new \InvalidArgumentException("Coop share statements should not be sent");

        return;
        ShareHolder::where('entity_id', 1)->has('shareHoldings')
            ->get()
            ->each(
                function ($holder) {
                    (new SharesCampaignStatementItem())->fill(
                        [
                            'share_holder_id' => $holder->id,
                            'campaign_id' => $this->campaign->id,
                            'added_by' => $this->campaign->sender_id,
                            'added_on' => Carbon::now()
                        ]
                    )->save();
                }
            );
    }

    public function fireAutomaticallyAddClients($job, $data)
    {
        $campaign = SharesStatementCampaign::findOrFail($data['campaign_id']);

        $this->campaign = $campaign;

        $this->automaticallyAddClients();

        $job->delete();
    }

    public function productSummary()
    {
        return SharesEntity::all()
            ->map(
                function ($entity) {
                    $count = function ($in) use ($entity) {
                        return ShareHolder::whereHas(
                            'shareHoldings',
                            function ($holding) use ($entity) {
                                $holding->where('entity_id', $entity->id);
                            }
                        )
                            ->whereIn('id', $in)
                            ->count();
                    };

                    return [
                        'name' => $entity->name,
                        'total' => $count($this->campaign->items()->remember(1)->pluck('share_holder_id')->all()),
                        'sent' => $count($this->campaign->items()->whereNotNull('sent_on')->remember(1)
                            ->pluck('share_holder_id')->all()),
                        'failed' => $count($this->campaign->items()->whereNotNull('failed_on')->remember(1)
                            ->pluck('share_holder_id')->all())
                    ];
                }
            )->filter(
                function ($entity) {
                    return $entity['total'] > 0;
                }
            );
    }
}
