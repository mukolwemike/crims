<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstateUnitTranche;
use Cytonn\Core\DataStructures\Files\Excel as CrimsExcel;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class ClientsTrancheReportExcelGenerator
{
    public function excel(RealEstateUnitTranche $tranche, $holdings_arr, $filename)
    {
        return Excel::create(
            $filename,
            function ($excel) use ($tranche, $holdings_arr) {
                if ($tranche->sizes) {
                    $tranche->sizes->map(
                        function ($sizing) use ($excel, $holdings_arr) {
                            $excel->sheet(
                                CrimsExcel::cleanSheetName($sizing->size->name),
                                function ($sheet) use ($holdings_arr, $sizing) {
                                    $holdings = \Cytonn\Core\DataStructures\Collection::make($holdings_arr->all())
                                        ->filter(
                                            function ($holding) use ($sizing) {
                                                return $holding->unit->size_id === $sizing->size_id;
                                            }
                                        );
                                    $holding_coll = new Collection();
                                    foreach ($holdings as $holding) {
                                        $h = new EmptyModel();
                                        $h->Client_Code = $holding->client->client_code;
                                        $h->Client_Name = ClientPresenter::presentFullNames($holding->client_id);
                                        $h->Email = $holding->client->contact->email;
                                        $h->Project = $holding->project->name;
                                        $h->Unit = $holding->unit->number;
                                        $h->Unit_Type = $holding->unit->type->name;

                                        $holding_coll->push($h);
                                    }
                                    $sheet->fromModel($holding_coll);
                                }
                            );
                        }
                    );
                }
            }
        );
    }
}
