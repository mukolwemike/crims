<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class OverduePaymentsReportExcelGenerator
{
    public function excel($file_name, $overdue_payments_grouped_by_project, Carbon $date)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($overdue_payments_grouped_by_project, $date) {
                foreach ($overdue_payments_grouped_by_project as $project => $schedules) {
                    $excel->sheet(
                        str_limit($project, 31),
                        function ($sheet) use ($schedules, $date) {
                            $sheet->fromModel(collect($schedules));

                            $sheet->prependRow([DatePresenter::formatDate($date->toDateString())]);

                            $sheet->mergeCells('A1:K1');
                            $sheet->cells(
                                'A1:K1',
                                function ($cells) {
                                    $cells->setFontWeight('bold');
                                }
                            );

                            $sheet->cells(
                                'A2:K2',
                                function ($cells) {
                                    $cells->setFontWeight('bold');
                                }
                            );
                        }
                    );
                }
            }
        );
    }
}
