<?php
/**
 * Date: 01/12/2015
 * Time: 1:04 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Reporting;

use App\Cytonn\Models\ClientBankAccount;

class BankInstructionGenerator
{
    public function generateInstructionForClientInvestment($data, $investment, $instruction)
    {
        if (!$instruction->clientBankAccount) {
            $default_account = ClientBankAccount::where(
                [
                'client_id' =>  $instruction->custodialTransaction->client_id,
                'default'   =>  true
                ]
            )->first();
            $instruction->clientBankAccount = $default_account;
        }
        return \PDF::loadView(
            'reports.clientbankinstruction',
            ['investment'=>$investment, 'data'=>$data, 'instruction'=>$instruction]
        );
    }
}
