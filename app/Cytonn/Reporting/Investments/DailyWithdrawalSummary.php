<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Reporting\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\InterestPayment;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;

class DailyWithdrawalSummary
{
    /*
     * Generate a daily withdrawal summary
     */
    public static function generateSummary($startDate = null, $endDate = null)
    {
        $startDate = Carbon::parse('2015-01-01');

        $endDate = Carbon::now();

        $withdrawalArray = array();

        $currencies = Currency::all();

        foreach ($currencies as $currency) {
            $withdrawals = ClientInvestment::where('withdrawn', 1)
                ->whereNotNull('withdrawn_on')
                ->where('withdrawn_on', '>=', $startDate)
                ->where('withdrawn_on', '<=', $endDate)
                ->where('rolled', '!=', 1)
                ->whereHas(
                    'product',
                    function ($q) use ($currency) {
                        $q->where('currency_id', $currency->id);
                    }
                )
                ->orderBy('withdrawn_on')
                ->get()
                ->groupBy(
                    function ($item) {
                        return Carbon::parse($item->withdrawn_on)->toDateString();
                    }
                );

            $interestPayments = InterestPayment::orderBy('date_paid')
                ->where('date_paid', '>=', $startDate)
                ->where('date_paid', '<=', $endDate)
                ->whereHas(
                    'investment',
                    function ($q) use ($currency) {
                        $q->whereHas(
                            'product',
                            function ($q) use ($currency) {
                                $q->where('currency_id', $currency->id);
                            }
                        );
                    }
                )
                ->get()
                ->groupBy(
                    function ($item) {
                        return Carbon::parse($item->date_paid)->toDateString();
                    }
                );

            foreach ($withdrawals as $key => $withdrawal) {
                $interestPaymentAmount = 0;

                $date = Carbon::parse($withdrawal[0]->withdrawn_on);

                $year = $date->format('Y');

                $formattedDate = $date->format('d F Y');

                $withdrawnAmount = $withdrawal->sum('withdraw_amount');

                $interestPayment = $interestPayments->pull($key);

                if ($interestPayment) {
                    $interestPaymentAmount = $interestPayment->sum('amount');
                }

                $withdrawalArray[$year . ' - ' . $currency->code][$date->toDateString()] = [
                    'Date' => $formattedDate,
                    'Withdrawals' => number_format($withdrawnAmount, 2),
                    'Interest Payments' => number_format($interestPaymentAmount, 2)
                ];
            }

            foreach ($interestPayments as $key => $interestPayment) {
                $date = Carbon::parse($interestPayment[0]->date_paid);

                $year = $date->format('Y');

                $formattedDate = $date->format('d F Y');

                $interestPaymentAmount = $interestPayment->sum('amount');

                $withdrawalArray[$year . ' - ' . $currency->code][$date->toDateString()] = [
                    'Date' => $formattedDate,
                    'Withdrawals' => number_format(0, 2),
                    'Interest Payments' => number_format($interestPaymentAmount, 2)
                ];
            }

            foreach ($withdrawalArray as $key => $withdrawal) {
                ksort($withdrawalArray[$key]);
            }

            ksort($withdrawalArray);
        }

        ExcelWork::generateAndExportMultiSheet($withdrawalArray, 'Daily_Withdrawal_Summary');
    }
}
