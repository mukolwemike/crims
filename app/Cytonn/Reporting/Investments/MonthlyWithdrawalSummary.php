<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Reporting\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\InterestPayment;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;

class MonthlyWithdrawalSummary
{
    /*
     * Generate monthly withdrawal summary
     */
    public static function generateSummary()
    {
        $currencies = Currency::all();

        $currencyWithdrawalArray = array();

        foreach ($currencies as $currency) {
            $firstWithdrawal = ClientInvestment::where('withdrawn', 1)
                ->whereNotNull('withdrawn_on')
                ->where('rolled', '!=', 1)
                ->whereHas(
                    'product',
                    function ($q) use ($currency) {
                        $q->where('currency_id', $currency->id);
                    }
                )
                ->orderBy('withdrawn_on')->first();

            $firstInterestPayment = InterestPayment::whereHas(
                'investment',
                function ($q) use ($currency) {
                    $q->whereHas(
                        'product',
                        function ($q) use ($currency) {
                            $q->where('currency_id', $currency->id);
                        }
                    );
                }
            )->orderBy('date_paid')->first();

            if ($firstInterestPayment && $firstWithdrawal) {
                if (Carbon::parse($firstInterestPayment->date_paid) <= Carbon::parse($firstWithdrawal->withdrawn_on)) {
                    $startDate = Carbon::parse($firstInterestPayment->date_paid)->startOfMonth();
                } else {
                    $startDate = Carbon::parse($firstWithdrawal->withdrawn_on)->startOfMonth();
                }
            } else {
                if ($firstInterestPayment) {
                    $startDate = Carbon::parse($firstInterestPayment->date_paid)->startOfMonth();
                } elseif ($firstWithdrawal) {
                    $startDate = Carbon::parse($firstWithdrawal->withdrawn_on)->startOfMonth();
                } else {
                    $startDate = Carbon::now();
                }
            }

            $withdrawalArray = array();

            while (Carbon::parse($startDate) <= Carbon::now()->startOfMonth()) {
                $endDate = $startDate->copy()->endOfMonth();

                $withdrawals = ClientInvestment::where('withdrawn', 1)
                    ->where('withdrawn_on', '>=', $startDate)
                    ->where('withdrawn_on', '<=', $endDate)
                    ->where('rolled', '!=', 1)
                    ->whereHas(
                        'product',
                        function ($q) use ($currency) {
                            $q->where('currency_id', $currency->id);
                        }
                    )
                    ->sum('withdraw_amount');

                $interestPayments = InterestPayment::where('date_paid', '>=', $startDate)
                    ->where('date_paid', '<=', $endDate)
                    ->whereHas(
                        'investment',
                        function ($q) use ($currency) {
                            $q->whereHas(
                                'product',
                                function ($q) use ($currency) {
                                    $q->where('currency_id', $currency->id);
                                }
                            );
                        }
                    )
                    ->sum('amount');

                if ($withdrawals != 0 || $interestPayments != 0) {
                    $withdrawalArray[] = [
                        'Month' => $startDate->format('F Y'),
                        'Withdrawals' => number_format($withdrawals, 2),
                        'Interest Payments' => number_format($interestPayments, 2)
                    ];
                }

                $startDate = $startDate->addMonth();
            }

            $currencyWithdrawalArray[$currency->code] = $withdrawalArray;
        }

        ExcelWork::generateAndExportMultiSheet($currencyWithdrawalArray, 'Monthly_Withdrawal_Summary');
    }
}
