<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Illuminate\Support\Collection;

class ProjectCashFlowsExcelGenerator
{
    public function excel($project, Carbon $startDate, Carbon $endDate)
    {
        $numberOfMonths = $startDate->diffInMonths($endDate);

        $anotherStart = (new Carbon($startDate));

        $date = DatePresenter::formatDate(Carbon::today());

        $file = \Excel::create(
            $project->name . ' Cash Flows Worksheet ' . $date,
            function ($excel) use ($project, $startDate, $endDate, $anotherStart, $numberOfMonths) {
                $excel->sheet(
                    'Cash Flows',
                    function ($sheet) use ($project, $numberOfMonths, $startDate, $endDate, $anotherStart) {
                        $column_headers = [
                            'FA',
                            'Client Name',
                            'Client Code',
                            'House Unit Typology',
                            'House Number',
                            'Amount Payable',
                            'Plan',
                        ];
                        for ($i = 0; $i < $numberOfMonths; $i++) {
                            $column_headers[] = $anotherStart->format('M');
                            $anotherStart->addMonth();
                        }

                        $units = $project->units()->has('activeHolding')->get()->filter(
                            function ($unit) {
                                return $unit->activeHolding->with('client', 'unit', 'activeHolding');
                            }
                        );

                        $holdings_column = new Collection();
                        foreach ($units as $unit) {
                            $holding = UnitHolding::findOrFail($unit->activeHolding->id);

                            $u = new EmptyModel();
                            $u->FA = 'TODO';//TODO
                            $u->ClientName = ClientPresenter::presentFullNames($unit->activeHolding->client_id);
                            $u->ClientCode = $unit->activeHolding->client->client_code;
                            $u->HouseUnitTypology = $unit->size->name;
                            $u->HouseNumber = $unit->number;
                            $u->AmountPayable = $holding->price();
                            $u->Plan = '';
                            if (!is_null($unit->activeHolding->paymentPlan)) {
                                $u->Plan = $unit->activeHolding->paymentPlan->name;
                            }

                            $schedules = $holding->paymentSchedules->filter(
                                function ($schedule) use ($startDate, $endDate) {
                                    if ($schedule->date >= $startDate && $schedule->date < $endDate) {
                                        return $schedule;
                                    }
                                }
                            );

                            $payments = $holding->payments->filter(
                                function ($payment) use ($startDate, $endDate) {
                                    if ($payment->date >= $startDate && $payment->date < $endDate) {
                                        return $payment;
                                    }
                                }
                            );

                            $paymentIds = $payments->map(
                                function ($payment) {
                                    return $payment->id;
                                }
                            );

                            $monthlyPayments = $schedules->map(
                                function ($schedule) use ($paymentIds) {
                                    return in_array($schedule->id, $paymentIds->toArray()) ?
                                        $schedule->amount :
                                        RealEstatePayment::where([
                                            'schedule_id' => $schedule->id,
                                            'holding_id' => $schedule->unit_holding_id
                                        ])->first();
                                }
                            );

                            foreach ($monthlyPayments as $payment) {
                                try {
                                    $u->$payment = $payment->amount;
                                } catch (\Exception $e) {
                                    $e->getMessage();
                                }
                            }

                            $holdings_column->push($u);
                        }
                        $sheet->fromModel($holdings_column);
                        $sheet->prependRow(1, $column_headers)->cells(
                            'A1:P1',
                            function ($cell) {
                                $cell->setBackground('#006666')->setFontColor('#FFFFFF')->setFontSize(14);
                                ;
                            }
                        )->setHeight(1, 50);
                        $sheet->removeRow(2);
                    }
                );
            }
        );

        return $file;
    }
}
