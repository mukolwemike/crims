<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  07/11/2018
 *
 */

namespace Cytonn\Reporting\SummaryReports\FaEvaluationSummaries;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Unitization\UnitFundPurchaseSale;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;

trait WithdrawalsTrait
{
    /*
     * Get the cumulative outflows
     */
    public function getWithdrawals($start = null, $end = null, $recipientId = null, $productIds = null, $type = [1])
    {
        return $this->getInvestmentWithdrawals($start, $end, $recipientId, $productIds, $type) +
            $this->getUnitFundOutflows($start, $end, $recipientId);
    }

    /*
     * Setup withdrawals query
     */
    public function getWithdrawalQuery(
        $start = null,
        $end = null,
        $recipientId = null,
        $productIds = null,
        $type = [1]
    ) {
        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->where('amount', '>', 0)
            ->where('date', '<=', $end)
            ->whereIn('type_id', $type)
            ->whereHas(
                'investment',
                function ($q) use ($recipientId, $start, $productIds, $end) {
                    $q->where('invested_date', '<=', $end);

                    if ($start) {
                        $q->where('invested_date', '>=', $start);
                    }

                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($recipientId) {
                            if ($recipientId) {
                                $q->where('recipient_id', $recipientId);
                            }
                        }
                    );
                }
            );

        return $q;
    }

    /**
     * @param null $start
     * @param null $end
     * @param null $recipientId
     * @return mixed
     */
    public function getNormalInvestmentWithdrawals($start = null, $end = null, $recipientId = null)
    {
        $withdrawals = ClientInvestmentWithdrawal::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('type_id', '=', 1)
            ->whereHas(
                'investment',
                function ($q) use ($recipientId) {
                    $q->whereHas(
                        'commission',
                        function ($q) use ($recipientId) {
                            if ($recipientId) {
                                $q->where('recipient_id', $recipientId);
                            }
                        }
                    );
                }
            )->get();

        return $withdrawals->sum(function (ClientInvestmentWithdrawal $withdrawal) {
            return convert(
                $this->getCompanyWithdrawnValue($withdrawal),
                $withdrawal->investment->product->currency,
                Carbon::parse($withdrawal->date)
            );
        });
    }

    /**
     * @param $start
     * @param $end
     * @return bool
     */
    public function checkCumulative($recipient, $start, $end)
    {
        if ($recipient) {
            return ! (ClientInvestment::whereHas('commission', function ($q) use ($recipient) {
                $q->where('recipient_id', $recipient->id);
            })->where('invested_date', '<=', Carbon::parse($start))->exists());
        }

        return Carbon::parse($start) < Carbon::parse($end)->startOfYear();
    }

    /*
     * Get the monthly product withdrawal either for an FA or the company in total
     */
    public function getInvestmentWithdrawals(
        $start = null,
        $end = null,
        $recipientId = null,
        $productIds = null,
        $type = [1]
    ) {
        $q = $this->getWithdrawalQuery($start, $end, $recipientId, $productIds, $type);

        $q->where('date', '<=', $end);

        return $q->get()->sum(
            function ($clientWithdrawal) use ($end, $start) {
                return convert(
                    $this->getWithdrawnValue($clientWithdrawal, $start, $end),
                    $clientWithdrawal->investment->product->currency,
                    Carbon::parse($clientWithdrawal->date)
                );
            }
        );
    }

    /*
     * Get the monthly product withdrawal either for an FA or the company in total
     */
    public function getWithdrawalsWithoutEndDate(
        $start = null,
        $end = null,
        $recipientId = null,
        $productIds = null,
        $type = [1]
    ) {
        $q = $this->getWithdrawalQuery($start, $end, $recipientId, $productIds, $type);

        return $q->get()->sum(
            function ($clientWithdrawal) use ($end, $start) {
                return convert(
                    $this->getWithdrawnValue($clientWithdrawal, $start, $end),
                    $clientWithdrawal->investment->product->currency,
                    Carbon::parse($clientWithdrawal->date)
                );
            }
        );
    }

    /**
     * Get the monthly product withdrawal either for an FA or the company in total
     *
     * @param  $start
     * @param  $end
     * @param  $recipientId
     * @param  $productIds
     * @return mixed
     */
    public function getCompetitionWithdrawals(
        $start = null,
        $end = null,
        $recipientId = null,
        $productIds = null,
        $type = [1]
    ) {
        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->whereIn('type_id', $type)
            ->where('date', '<=', $end)
            ->whereHas(
                'investment',
                function ($q) use ($recipientId, $start, $productIds, $end) {
                    $q->where('invested_date', '<=', Carbon::parse($end)->endOfYear());

                    if ($start) {
                        $q->where('invested_date', '>=', Carbon::parse($start)->startOfYear());
                    }

                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($recipientId) {
                            if ($recipientId) {
                                $q->where('recipient_id', $recipientId);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        return $q->get()->sum(
            function ($clientWithdrawal) use ($end, $start) {
                return convert(
                    $this->getWithdrawnValue($clientWithdrawal, $start, $end),
                    $clientWithdrawal->investment->product->currency,
                    Carbon::parse($end)
                );
            }
        );
    }

    /**
     * Get the monthly product withdrawal either for an FA or the company in total
     *
     * @param  $start
     * @param  $end
     * @param  $recipientId
     * @param  $productIds
     * @return mixed
     */
    public function getCompanyWithdrawals(
        $start = null,
        $end = null,
        $recipientId = null,
        $productIds = null,
        $type = [1]
    ) {
        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->whereIn('type_id', $type)
            ->where('amount', '>', 0)
            ->where('date', '<=', $end)
            ->whereHas(
                'investment',
                function ($q) use ($recipientId, $start, $productIds, $end) {
                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($recipientId) {
                            if ($recipientId) {
                                $q->where('recipient_id', $recipientId);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        return $q->get()->sum(function ($clientWithdrawal) use ($end) {
            $withdrawal = $clientWithdrawal->amount;

            if ($clientWithdrawal->type_id == 2) {
                $investment = $clientWithdrawal->investment;

                $interest = $investment
                    ->calculate(Carbon::parse($clientWithdrawal->date)->subDay(), true)->netInterest();

                $withdrawal -= $interest;
            }

            return convert($withdrawal, $clientWithdrawal->investment->product->currency, Carbon::parse($end));
        });
    }

    /*
     * Get the company withdrawals value
     */
    public function getCompanyWithdrawnValue(ClientInvestmentWithdrawal $clientWithdrawal)
    {
        $withdrawal = $clientWithdrawal->amount;

        $investment = $clientWithdrawal->investment;

        if ($clientWithdrawal->type_id == 2) {
            $interest = $investment->calculate(Carbon::parse($clientWithdrawal->date)->subDay(), true)->netInterest();

            $withdrawal -= $interest;
        } else {
            $withdrawal = $clientWithdrawal->principalWithdrawn();
        }

        return $withdrawal;
    }

    /*
    * Get the withdrawan value based on the passed withdrawal
    */
    public function getWithdrawnValue(
        ClientInvestmentWithdrawal $clientWithdrawal,
        $start = null,
        $end = null,
        $cumulative = null
    ) {
        $investment = $clientWithdrawal->investment;

        $recipient = $investment->commission->recipient;

        $cumulative = is_null($cumulative) ? $this->checkCumulative($recipient, $start, $end) : $cumulative;

        $withdrawal = $clientWithdrawal->amount;

        if ($clientWithdrawal->type_id == 2) {
            $withdrawal -=
                $investment->calculate(Carbon::parse($clientWithdrawal->date)->subDay(), true)->netInterest();
        } else {
            if ($investment->investment_type_id == 3) {
                $reinvestWithdrawal = $investment->reinvestedFrom()->latest('date')->where('type_id', 2)->first();

                if (is_null($reinvestWithdrawal)) {
                    $reinvestWithdrawal = $investment->reinvestedFrom()->latest('date')->first();
                }

                if ($reinvestWithdrawal) {
                    if ($cumulative) {
                        $principal = $clientWithdrawal->principalWithdrawn();
                    } else {
                        $rolloverInvestment = $reinvestWithdrawal->investment;

                        $principalWithdrawn = $clientWithdrawal->principalWithdrawn();

                        $rolloverPrincipal = $rolloverInvestment
                            ->calculate(Carbon::parse($reinvestWithdrawal->date)->subDay(), true)->netInterest();

                        $topupPrincipal = $rolloverInvestment->topupsTo()->sum('amount');

                        $principal = ($rolloverPrincipal + $topupPrincipal) * $principalWithdrawn / $investment->amount;
                    }
                } else {
                    $principal = $clientWithdrawal->principalWithdrawn();
                }
            } else {
                $principal = $clientWithdrawal->principalWithdrawn();
            }

            $withdrawal = ($withdrawal > $principal) ? $principal : $withdrawal;
        }

        return $withdrawal;
    }

    /**
     * @param null $start
     * @param null $end
     * @param null $recipientId
     * @return mixed
     */
    public function getUnitFundOutflowQuery($start = null, $end = null, $recipientId = null)
    {
        return UnitFundSale::nonFees()->whereHas('purchaseSales', function ($q) use ($recipientId, $end, $start) {
            $q->where('date', '<=', $end)->whereHas('purchase', function ($q) use ($recipientId, $end, $start) {
                $q->where('date', '<=', $end);

                if ($start) {
                    $q->where('date', '>=', $start);
                }
                $q->whereHas('unitFundCommission', function ($q) use ($recipientId) {
                    if ($recipientId) {
                        $q->where('commission_recipient_id', $recipientId);
                    }
                });
            });
        });
    }

    /**
     * Get the outflows of a specific product on a monthly basis
     *
     * @param  $start
     * @param  $end
     * @param  $recipientId
     * @return number
     */
    public function getUnitFundOutflows($start = null, $end = null, $recipientId = null)
    {
        $q = $this->getUnitFundOutflowQuery($start, $end, $recipientId);

        return $q->get()
            ->sum(
                function ($sale) use ($end, $recipientId) {
                    $purchaseAmount = $sale->repository()->getPurchaseValueForFa($recipientId);

                    return convert(
                        $purchaseAmount,
                        $sale->fund->currency,
                        Carbon::parse($sale->date)
                    );
                }
            );
    }

    /**
     * @param null $start
     * @param null $end
     * @param null $recipientId
     * @return mixed
     */
    public function getNormalUnitFundOutflows($start = null, $end = null, $recipientId = null)
    {
        $sales = UnitFundSale::nonFees()->where('date', '<=', $end)->where('date', '>=', $start)
            ->whereHas('purchaseSales', function ($q) use ($recipientId, $end, $start) {
                $q->whereHas('purchase', function ($q) use ($recipientId, $end, $start) {
                    $q->whereHas('unitFundCommission', function ($q) use ($recipientId) {
                        if ($recipientId) {
                            $q->where('commission_recipient_id', $recipientId);
                        }
                    });
                });
            });

        return $sales->get()->sum(
            function ($sale) use ($end, $recipientId) {
                $purchaseAmount = $sale->repository()->getPurchaseValueForFa($recipientId);

                return convert(
                    $purchaseAmount,
                    $sale->fund->currency,
                    Carbon::parse($sale->date)
                );
            }
        );
    }

    /**
     * @param null $start
     * @param null $end
     * @param null $recipientId
     * @return mixed
     */
    public function getCompetitionUnitFundOutflows($start = null, $end = null, $recipientId = null)
    {
        $q = UnitFundSale::nonFees()->where('date', '<=', $end)
            ->whereHas('purchaseSales', function ($q) use ($recipientId) {
                $q->whereHas('purchase', function ($q) use ($recipientId) {
                    $q->whereHas('unitFundCommission', function ($q) use ($recipientId) {
                        if ($recipientId) {
                            $q->where('commission_recipient_id', $recipientId);
                        }
                    });
                });
            });

        if ($start) {
            $q->where('date', '>=', $start);
        }

        return $q->get()->sum(
            function ($sale) use ($end, $recipientId) {
                $purchaseAmount = $sale->repository()->getPurchaseValueForFa($recipientId);

                return convert(
                    $purchaseAmount,
                    $sale->fund->currency,
                    Carbon::parse($sale->date)
                );
            }
        );
    }

    /**
     * @param $start
     * @param $end
     * @param null $rec_id
     * @param null $productIds
     * @param array $type
     * @return array
     */
    public function getOutFlowClients($start, $end, $rec_id = null, $productIds = null, $type = [1, 2])
    {
        return array_merge(
            $this->getWithdrawalClients($start, $end, $rec_id, $productIds, $type),
            $this->getUnitFundOutflowClients($start, $end, $rec_id)
        );
    }

    /**
     * @param $start
     * @param $end
     * @param null $rec_id
     * @param null $productIds
     * @param array $type
     * @return array
     */
    public function getWithdrawalClients($start, $end, $rec_id = null, $productIds = null, $type = [1, 2])
    {
        $clientArray = array();

        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->whereIn('type_id', [1])
            ->where('amount', '>', 0)
            ->where('date', '<=', $end)
            ->whereHas(
                'investment',
                function ($q) use ($rec_id, $start, $productIds, $end) {
                    $q->where('invested_date', '<=', $end)->whereHas(
                        'commission',
                        function ($q) use ($rec_id) {
                            if ($rec_id) {
                                $q->where('recipient_id', $rec_id);
                            }
                        }
                    );

                    if ($start) {
                        $q->where('invested_date', '>=', $start);
                    }

                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }
                }
            );

        foreach ($q->get() as $clientWithdrawal) {
            $withdrawal = $this->getWithdrawnValue($clientWithdrawal, $start, $end);

            $clientArray[] = [
                'Date' => Carbon::parse($clientWithdrawal->date)->toDateString(),
                'Type' => $clientWithdrawal->type_id == 1 ? 'Send to Client' : 'Rollover',
                'Name' => $clientWithdrawal->investment->client->name(),
                'Invested_Date' => Carbon::parse($clientWithdrawal->investment->invested_date)->toDateString(),
                'Principal' => round(
                    convert(
                        $clientWithdrawal->investment->amount,
                        $clientWithdrawal->investment->product->currency,
                        Carbon::parse($end)
                    ),
                    0
                ),
                'Amount' => round($withdrawal, 0),
                'Converted_Amount_Ksh' => round(
                    convert(
                        $withdrawal,
                        $clientWithdrawal->investment->product->currency,
                        Carbon::parse($end)
                    ),
                    0
                ),
                'Product' => $clientWithdrawal->investment->product->name,
            ];
        }

        return $clientArray;
    }

    /**
     * @param $start
     * @param $end
     * @param null $rec_id
     * @param null $productIds
     * @param array $type
     * @return \Illuminate\Support\Collection|static
     */
    public function getCompetitionWithdrawalClients($start, $end, $rec_id = null, $productIds = null, $type = [1, 2])
    {
        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->whereIn('type_id', $type)
            ->where('date', '<=', $end)
            ->whereHas(
                'investment',
                function ($q) use ($rec_id, $start, $productIds, $end) {
                    $q->where('invested_date', '<=', Carbon::parse($end)->endOfYear());

                    if ($start) {
                        $q->where('invested_date', '>=', Carbon::parse($start)->startOfYear());
                    }

                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($rec_id) {
                            if ($rec_id) {
                                $q->where('recipient_id', $rec_id);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        return $q->get()->map(function ($clientWithdrawal) use ($end) {
            $withdrawal = $clientWithdrawal->amount;

            if ($clientWithdrawal->type_id == 2) {
                $withdrawal = $clientWithdrawal->principalWithdrawn();
            }

            return [
                'Date' => Carbon::parse($clientWithdrawal->date)->toDateString(),
                'Type' => $clientWithdrawal->type_id == 1 ? 'Send to Client' : 'Rollover',
                'Name' => $clientWithdrawal->investment->client->name(),
                'Invested Date' => $clientWithdrawal->investment->invested_date,
                'Amount' => round($withdrawal, 0),
                'Converted Amount (Ksh)' => round(
                    convert($withdrawal, $clientWithdrawal->investment->product->currency, Carbon::parse($end)),
                    0
                ),
                'Product' => $clientWithdrawal->investment->product->name,
            ];
        });
    }

    /**
     * @param $start
     * @param $end
     * @param null $rec_id
     * @param null $productIds
     * @return \Illuminate\Support\Collection|static
     */
    public function getCompanyWithdrawalClients($start, $end, $rec_id = null, $productIds = null, $type = [1, 2])
    {
        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->whereIn('type_id', $type)
            ->where('amount', '>', 0)
            ->where('date', '<=', $end)
            ->whereHas(
                'investment',
                function ($q) use ($rec_id, $start, $productIds, $end) {
                    //                    $q->where('invested_date', '<=', $end);
                    //
                    //                    if ($start)
                    //                    {
                    //                        $q->where('invested_date', '>=', $start);
                    //                    }

                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($rec_id) {
                            if ($rec_id) {
                                $q->where('recipient_id', $rec_id);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        $clientArray = $q->get()->map(function ($clientWithdrawal) use ($end) {
            $withdrawal = $clientWithdrawal->amount;

            if ($clientWithdrawal->type_id == 2) {
                $withdrawal = $clientWithdrawal->principalWithdrawn();
            }

            return [
                'Date' => Carbon::parse($clientWithdrawal->date)->toDateString(),
                'Type' => $clientWithdrawal->type_id == 1 ? 'Send to Client' : 'Rollover',
                'Name' => $clientWithdrawal->investment->client->name(),
                'Invested Date' => $clientWithdrawal->investment->invested_date,
                'Amount' => round($withdrawal, 0),
                'Converted Amount (Ksh)' => round(
                    convert($withdrawal, $clientWithdrawal->investment->product->currency, Carbon::parse($end)),
                    0
                ),
                'Product' => $clientWithdrawal->investment->product->name,
            ];
        });

        return $clientArray;
    }

    /**
     * @param null $start
     * @param null $end
     * @param null $recipientId
     * @return array
     */
    public function getUnitFundOutflowClients($start = null, $end = null, $recipientId = null)
    {
        $clientArray = array();

        $q = $this->getUnitFundOutflowQuery($start, $end, $recipientId);

        foreach ($q->get() as $sale) {
            $purchaseAmount = $sale->repository()->getPurchaseValueForFa($recipientId);

            $clientArray[] = [
                'Date' => Carbon::parse($sale->date)->toDateString(),
                'Type' => 'Unit Fund Sale',
                'Name' => ClientPresenter::presentFullNames($sale->client_id),
                'Invested_Date' => Carbon::parse($sale->date)->toDateString(),
                'Principal' => $purchaseAmount,
                'Amount' => round($purchaseAmount, 0),
                'Converted_Amount_Ksh' => round(
                    convert(
                        $purchaseAmount,
                        $sale->fund->currency,
                        Carbon::parse($sale->date)
                    ),
                    0
                ),
                'Product' => $sale->fund->short_name,
            ];
        }

        return $clientArray;
    }
}
