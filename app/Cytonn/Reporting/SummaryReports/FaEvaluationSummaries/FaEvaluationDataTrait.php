<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  2019-05-09
 *
 */

namespace Cytonn\Reporting\SummaryReports\FaEvaluationSummaries;

use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;

trait FaEvaluationDataTrait
{
    public function userEvaluationData($userData, $endDate)
    {
        $endDate = Carbon::parse($endDate)->endOfDay();

        $recipientId = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($userData->email);

        $ytdStartDate = Carbon::parse($userData->ytd_start_date);

        $cumulativeStartDate = Carbon::parse($userData->cumulative_start_date);

        $employmentDate = Carbon::parse($userData->production_employment_date);

        if ($recipientId) {
            $YTDInflows = $this->getYTDInflows($ytdStartDate, $endDate, $recipientId);

            $cumulativeInflows = $this->getCumulativeInflows($cumulativeStartDate, $endDate, $recipientId);

            $YTDWithdrawals = $this->getWithdrawals($ytdStartDate, $endDate, $recipientId);

            $cumulativeWithdrawals = $this->getWithdrawals($cumulativeStartDate, $endDate, $recipientId);

            $YTDNetFlow = $YTDInflows - $YTDWithdrawals;

            $cumulativeNetFlow = $cumulativeInflows - $cumulativeWithdrawals;

            $YTDRealEstateSales = $this->realEstateSales($ytdStartDate, $endDate, $recipientId);

            $YTDRealEstateCashPlanSales = $this->realEstateCashPlanSales($ytdStartDate, $endDate, $recipientId);

            $cumulativeRealEstateSales = $this->realEstateSales($cumulativeStartDate, $endDate, $recipientId);

            $cumulativeRealEstateCashPlanSales = $this->realEstateCashPlanSales($cumulativeStartDate, $endDate, $recipientId);

            $YTDActualPayments = $this->realEstateActualPayments($ytdStartDate, $endDate, $recipientId);

            $cumulativeCashFlows = $this->realEstateActualPayments($cumulativeStartDate, $endDate, $recipientId);

            $YTDRetention = $YTDNetFlow + $YTDRealEstateCashPlanSales;

            $cumulativeRetention = $cumulativeNetFlow + $cumulativeRealEstateCashPlanSales;

            $userYtdTargets = $userData->YTDTargets;

            $userCumTargets = $userData->cumulativeTargets;

            $ytdPercentage = percentage($YTDRetention, $userYtdTargets, 1);

            $cumPercentage = percentage($cumulativeRetention, $userData->cumulativeTargets, 1);

            $ytdPercentageRetention = percentage($YTDNetFlow, $YTDInflows, 1);

            $cumulativePercentageRetention = percentage($cumulativeNetFlow, $cumulativeInflows, 1);

            $cumulativeRetentionBeforeDemotion = 0;

            if ($employmentDate != $cumulativeStartDate) {
                $cumulativeInflowsBeforeDemotion = $this->getCumulativeInflows(
                    $employmentDate,
                    $cumulativeStartDate,
                    $recipientId
                );

                $cumulativeWithdrawalsBeforeDemotion = $this->getWithdrawals(
                    $employmentDate,
                    $cumulativeStartDate,
                    $recipientId
                );

                $cumulativeNetFlowBeforeDemotion =
                    $cumulativeInflowsBeforeDemotion - $cumulativeWithdrawalsBeforeDemotion;

                $cumulativeRealEstateSalesBeforeDemotion = $this->realEstateCashPlanSales(
                    $employmentDate,
                    $cumulativeStartDate,
                    $recipientId
                );

                $cumulativeRetentionBeforeDemotion =
                    $cumulativeNetFlowBeforeDemotion + $cumulativeRealEstateSalesBeforeDemotion;
            }
        } else {
            $YTDInflows = $cumulativeInflows = $YTDWithdrawals = $cumulativeWithdrawals =
            $YTDNetFlow = $cumulativeNetFlow = $YTDRealEstateSales = $YTDRealEstateCashPlanSales = 0;

            $YTDActualPayments = $cumulativeRealEstateSales = $cumulativeRealEstateCashPlanSales =
            $cumulativeCashFlows = $YTDRetention = $cumulativeRetention = $userYtdTargets = 0;

            $ytdPercentage = $cumPercentage = $userCumTargets =
            $ytdPercentageRetention = $cumulativePercentageRetention = $cumulativeRetentionBeforeDemotion = 0;
        }

        return [
            'id' => $userData->id,
            'name' => $userData->name,
            'unit' => $userData->unit,
            'branch' => $userData->branch,
            'days' => $userData->days,
            'value_days' => $userData->value_days,
            'title' => $userData->title,
            'fa_title' => $userData->fa_title,
            'suspended_days' => $userData->suspended_days,
            'currentFaPositionDays' => $userData->currentFaPositionDays,
            'employmentStatus' => $userData->employmentStatus,
            'position_status' => $userData->position_status,
            'currentPositionDays' => $userData->currentPositionDays,
            'ytd_start_date' => $userData->ytd_start_date,
            'cumulative_start_date' => $userData->cumulative_start_date,
            'targets_start_date' => $userData->targets_start_date,
            'investments' => [
                'inflow' => $YTDInflows,
                'cumulativeInflows' => $cumulativeInflows,
                'outflow' => $YTDWithdrawals,
                'cumulativeOutflows' => $cumulativeWithdrawals,
                'netflow' => $YTDNetFlow,
                'cumulativeNetflow' => $cumulativeNetFlow
            ],
            'real_estate' => [
                'YTDRealEstateSales' => $YTDRealEstateSales,
                'YTDRealEstateCashPlanSales' => $YTDRealEstateCashPlanSales,
                'YTDActualPayments' => $YTDActualPayments,
                'cumulativeRealEstateSales' => $cumulativeRealEstateSales,
                'cumulativeRealEstateCashPlanSales' => $cumulativeRealEstateCashPlanSales,
                'cumulativeCashFlows' => $cumulativeCashFlows
            ],
            'retention' => [
                'YTDRetention' => $YTDRetention,
                'cumulativeRetention' => $cumulativeRetention,
                'YTDTargets' => $userYtdTargets,
                'cumulativeTargets' => $userCumTargets,
                'YTDPercentageRetention' => $ytdPercentageRetention,
                'cumulativeRetentionBeforeDemotion' => $cumulativeRetentionBeforeDemotion,
                'cumulativePercentageRetention' => $cumulativePercentageRetention,
                'YTDPercentage' => $ytdPercentage,
                'cumulativePercentage' => $cumPercentage
            ]
        ];
    }
}
