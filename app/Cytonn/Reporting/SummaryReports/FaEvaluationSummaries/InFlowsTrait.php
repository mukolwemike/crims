<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  07/11/2018
 *
 */

namespace Cytonn\Reporting\SummaryReports\FaEvaluationSummaries;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;

trait InFlowsTrait
{
    /*
     * Get total ytd inflows
     */
    public function getYTDInflows($ytdStartDate, Carbon $endDate, $recipientId)
    {
        return $this->getInflows($ytdStartDate, $endDate, $recipientId) +
            $this->getRolloverInflows($ytdStartDate, $endDate, $recipientId) +
            $this->getRolloverTopups($ytdStartDate, $endDate, $recipientId) +
            $this->getUnitFundInflows($ytdStartDate, $endDate, $recipientId);
    }

    /*
     * Get cumulative inflows
     */
    public function getCumulativeInflows($startDate, $endDate, $recipientId)
    {
        return $this->getInflows($startDate, $endDate, $recipientId) +
            $this->getRolloverInflows($startDate, $endDate, $recipientId) +
            $this->getRolloverTopups($startDate, $endDate, $recipientId) +
            $this->getUnitFundInflows($startDate, $endDate, $recipientId);
    }

    /**
     * Get the inflows of a specific product on a monthly basis
     *
     * @param  $start
     * @param  $end
     * @param  $recipientId
     * @param  $productIds
     * @return number
     */
    public function getInflows($start = null, $end = null, $recipientId = null, $productIds = null, $type = null)
    {
        $q = ClientInvestment::where('invested_date', '<=', $end)
            ->whereHas(
                'commission',
                function ($q) use ($recipientId) {
                    if ($recipientId) {
                        $q->where('recipient_id', $recipientId);
                    }
                }
            );

        if ($start) {
            $q->where('invested_date', '>=', $start);
        }

        if ($type) {
            $q->where('investment_type_id', $type);
        } else {
            $q->where('investment_type_id', '!=', 3);
        }

        if ($productIds) {
            $q->whereIn('product_id', $productIds);
        }

        return $q->get()
            ->sum(
                function ($clientInvestment) use ($end) {
                    return convert(
                        $clientInvestment->amount,
                        $clientInvestment->product->currency,
                        Carbon::parse($clientInvestment->invested_date)
                    );
                }
            );
    }

    /*
    * Get the rollover inflows
    */
    public function getRolloverInflows(
        $start = null,
        $end = null,
        $recipientId = null,
        $productIds = null,
        $type = [2]
    ) {
        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->whereIn('type_id', $type)
            ->where('date', '<=', $end)
            ->whereHas(
                'reinvestedTo',
                function ($q) use ($recipientId, $start, $productIds, $end) {
                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($recipientId) {
                            if ($recipientId) {
                                $q->where('recipient_id', $recipientId);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        return $q->get()->sum(
            function ($clientWithdrawal) use ($end, $recipientId) {
                $investment = $clientWithdrawal->investment;

                $investmentCalc = $investment->calculate(Carbon::parse($clientWithdrawal->date)->subDay(), true);

                if ($investment->commission->recipient_id == $recipientId) {
                    $interest = $investmentCalc->netInterest();
                } else {
                    $interest = $investmentCalc->total();
                }

                return convert(
                    $interest,
                    $clientWithdrawal->investment->product->currency,
                    Carbon::parse($clientWithdrawal->date)
                );
            }
        );
    }

    /*
     * Get rollover topups
     */
    public function getRolloverTopups($start = null, $end = null, $recipientId = null, $productIds = null)
    {
        $q = ClientInvestmentTopup::where('date', '<=', $end)
            ->whereHas(
                'toInvestment',
                function ($q) use ($recipientId, $start, $productIds, $end) {
                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($recipientId) {
                            if ($recipientId) {
                                $q->where('recipient_id', $recipientId);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        return $q->get()->sum(
            function ($clientTopup) use ($end) {
                return convert(
                    $clientTopup->amount,
                    $clientTopup->toInvestment->product->currency,
                    Carbon::parse($clientTopup->date)
                );
            }
        );
    }

    /**
     * Get the inflows of a specific product on a monthly basis
     *
     * @param  $start
     * @param  $end
     * @param  $recipientId
     * @param  $productIds
     * @return number
     */
    public function getUnitFundInflows($start = null, $end = null, $recipientId = null)
    {
        $q = UnitFundPurchase::where('date', '<=', $end)
            ->whereHas('unitFundCommission', function ($q) use ($recipientId) {
                if ($recipientId) {
                    $q->where('commission_recipient_id', $recipientId);
                }
            });

        if ($start) {
            $q->where('date', '>=', $start);
        }

        return $q->get()
            ->sum(
                function ($purchase) use ($end) {
                    return convert(
                        $purchase->number * $purchase->price,
                        $purchase->unitFund->currency,
                        Carbon::parse($purchase->date)
                    );
                }
            );
    }

    /**
     * @param $start
     * @param $end
     * @param null $recipientId
     * @param null $productIds
     * @return array
     */
    public function getInflowClients($start, $end, $recipientId = null, $productIds = null)
    {
        $clientArray = array_merge(
            $this->getInvestmentInflowClients($start, $end, $recipientId, $productIds),
            $this->getRolloverInflowsClients($start, $end, $recipientId, $productIds, [2])
        );

        $clientArray = array_merge(
            $clientArray,
            $this->getRolloverTopupClients($start, $end, $recipientId, $productIds)
        );

        $clientArray = array_merge($clientArray, $this->getUnitFundInflowClients($start, $end, $recipientId));

        return $clientArray;
    }

    /**
     * @param $start
     * @param $end
     * @param null $recipientId
     * @param null $productIds
     * @return array
     */
    public function getInvestmentInflowClients($start, $end, $recipientId = null, $productIds = null)
    {
        $clientArray = array();

        $q = ClientInvestment::where('invested_date', '<=', $end)
            ->where('investment_type_id', '!=', 3)
            ->whereHas(
                'commission',
                function ($q) use ($recipientId) {
                    if ($recipientId) {
                        $q->where('recipient_id', $recipientId);
                    }
                }
            );

        if ($start) {
            $q->where('invested_date', '>=', $start);
        }

        if ($productIds) {
            $q->whereIn('product_id', $productIds);
        }

        $inflows = $q->get();

        foreach ($inflows as $clientInvestment) {
            $amount = $clientInvestment->amount;

            $clientArray[] = [
                'Date' => Carbon::parse($clientInvestment->invested_date)->toDateString(),
                'Invested_Date' => Carbon::parse($clientInvestment->invested_date)->toDateString(),
                'Name' => $clientInvestment->client->name(),
                'Type' => ucwords($clientInvestment->type->name),
                'Amount' => round($amount, 0),
                'Converted_Amount_Ksh' =>
                    convert(
                        $amount,
                        $clientInvestment->product->currency,
                        Carbon::parse($clientInvestment->invested_date)
                    ),
                'Product' => $clientInvestment->product->name,
            ];
        }

        return $clientArray;
    }

    /*
     * Get rollover inflows clients
     */
    public function getRolloverInflowsClients(
        $start = null,
        $end = null,
        $recipientId = null,
        $productIds = null,
        $type = [2]
    ) {
        $clientArray = array();

        $q = ClientInvestmentWithdrawal::where('withdraw_type', 'withdrawal')
            ->whereIn('type_id', $type)
            ->where('date', '<=', $end)
            ->whereHas(
                'reinvestedTo',
                function ($q) use ($recipientId, $start, $productIds, $end) {
                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($recipientId) {
                            if ($recipientId) {
                                $q->where('recipient_id', $recipientId);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        $withdrawals = $q->get();

        foreach ($withdrawals as $withdrawal) {
            $clientInvestment = $withdrawal->investment;

            $investmentCalc = $clientInvestment->calculate(Carbon::parse($withdrawal->date)->subDay(), true);

            if ($clientInvestment->commission->recipient_id == $recipientId) {
                $interest = $investmentCalc->netInterest();
            } else {
                $interest = $investmentCalc->total();
            }

            $clientArray[] = [
                'Date' => Carbon::parse($withdrawal->date)->toDateString(),
                'Invested_Date' => Carbon::parse($withdrawal->date)->toDateString(),
                'Name' => $clientInvestment->client->name(),
                'Type' => ucwords('Rollover Interest'),
                'Amount' => round($interest, 0),
                'Converted_Amount_Ksh' =>
                    convert($interest, $clientInvestment->product->currency, Carbon::parse($withdrawal->date)),
                'Product' => $clientInvestment->product->name,
            ];
        }

        return $clientArray;
    }

    /**
     * Get the inflows of a specific product on a monthly basis
     *
     * @param $clientArray
     * @param  $start
     * @param  $end
     * @param  $rec_id
     * @return array
     */
    public function getUnitFundInflowClients($start = null, $end = null, $rec_id = null)
    {
        $clientArray = array();

        $q = UnitFundPurchase::where('date', '<=', $end)
            ->whereHas('unitFundCommission', function ($q) use ($rec_id) {
                if ($rec_id) {
                    $q->where('commission_recipient_id', $rec_id);
                }
            });

        if ($start) {
            $q->where('date', '>=', $start);
        }

        $purchases = $q->get();

        foreach ($purchases as $purchase) {
            $amount = $purchase->number * $purchase->price;

            $clientArray[] = [
                'Date' => Carbon::parse($purchase->date)->toDateString(),
                'Invested_Date' => Carbon::parse($purchase->date)->toDateString(),
                'Name' => ClientPresenter::presentFullNames($purchase->client_id),
                'Type' => $purchase->unitFund->short_name . ' Purchase',
                'Amount' => round($amount, 0),
                'Converted_Amount_Ksh' =>
                    convert(
                        $amount,
                        $purchase->unitFund->currency,
                        Carbon::parse($purchase->date)
                    ),
                'Product' => $purchase->unitFund->short_name,
            ];
        }

        return $clientArray;
    }

    /*
     * Get rollover topup clients
     */
    public function getRolloverTopupClients(
        $start = null,
        $end = null,
        $rec_id = null,
        $productIds = null
    ) {
        $clientArray = [];

        $q = ClientInvestmentTopup::where('date', '<=', $end)
            ->whereHas(
                'toInvestment',
                function ($q) use ($rec_id, $start, $productIds, $end) {
                    if ($productIds) {
                        $q->whereIn('product_id', $productIds);
                    }

                    $q->whereHas(
                        'commission',
                        function ($q) use ($rec_id) {
                            if ($rec_id) {
                                $q->where('recipient_id', $rec_id);
                            }
                        }
                    );
                }
            );

        if ($start) {
            $q->where('date', '>=', $start);
        }

        $topups = $q->get();

        foreach ($topups as $topup) {
            $clientInvestment = $topup->toInvestment;

            $toInvestment = $topup->toInvestment;

            $clientArray[] = [
                'Date' => Carbon::parse($toInvestment->invested_date)->toDateString(),
                'Invested_Date' => Carbon::parse($toInvestment->invested_date)->toDateString(),
                'Name' => $clientInvestment->client->name(),
                'Type' => ucwords('Topup'),
                'Amount' => round($topup->amount, 0),
                'Converted_Amount_Ksh' =>
                    convert(
                        $topup->amount,
                        $toInvestment->product->currency,
                        Carbon::parse($toInvestment->invested_date)
                    ),
                'Product' => $toInvestment->product->name,
            ];
        }

        return $clientArray;
    }

    /**
     * @param $start
     * @param $end
     * @param $rec_id
     *
     * @return \Closure
     */
    public function getInflowClientsQuery($start, $end, $rec_id)
    {
        return function ($model) use ($start, $end, $rec_id) {
            $q = $model->where('invested_date', '<=', $end)
                ->whereHas(
                    'commission',
                    function ($q) use ($rec_id) {
                        $q->where('recipient_id', $rec_id);
                    }
                );

            if ($start) {
                $q->where('invested_date', '>=', $start);
            }

            return $q;
        };
    }
}
