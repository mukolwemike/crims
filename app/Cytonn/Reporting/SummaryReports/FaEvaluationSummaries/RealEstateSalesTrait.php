<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  07/11/2018
 *
 */

namespace Cytonn\Reporting\SummaryReports\FaEvaluationSummaries;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;

trait RealEstateSalesTrait
{
    /*
     * Fetch real estate payments between a set of dates
     */
    private function fetchUnitHoldings($startDate, $endDate, $recipientId)
    {
        $unitHoldings = UnitHolding::whereHas(
            'commission',
            function ($commission) use ($recipientId) {
                $commission->where('recipient_id', $recipientId);
            }
        )->reservedBefore($endDate)->activeAtDate($endDate);

        if ($startDate) {
            $unitHoldings->reservedAfter($startDate);
        }

        return $unitHoldings;
    }
    /**
     * Get the overall real estate sales
     *
     * @param  $startDate
     * @param  $endDate
     * @param  $recipientId
     * @return mixed
     */
    public function realEstateSales($startDate, $endDate, $recipientId)
    {
        $unitHoldings = $this->fetchUnitHoldings($startDate, $endDate, $recipientId);

        return $unitHoldings->get()->sum(function ($holding) {
            return $holding->price();
        });
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $recipientId
     * @return mixed
     */
    public function realEstateCashPlanSales($startDate, $endDate, $recipientId)
    {
        $unitHoldings = $this->fetchUnitHoldings($startDate, $endDate, $recipientId);

        return $unitHoldings->get()->sum(function ($holding) {
            return $holding->repo->getCashPrice();
        });
    }

    /*
     * Get the count of the units sold by a commission recipient
     */
    public function realEstateUnitsSold($startDate, $endDate, $recipientId)
    {
         return $this->fetchUnitHoldings($startDate, $endDate, $recipientId)->count();
    }

    /**
     * Get actual payments for real estate
     *
     * @param $startDate
     * @param $endDate
     * @param $recipientId
     * @return mixed
     */
    public function realEstateActualPayments($startDate = null, $endDate = null, $recipientId = null)
    {
        $payment = RealEstatePayment::whereHas(
            'holding',
            function ($holding) use ($recipientId, $endDate) {
                $holding->activeAtDate($endDate)->whereHas(
                    'commission',
                    function ($commission) use ($recipientId) {
                        $commission->where('recipient_id', $recipientId);
                    }
                );
            }
        )->where('date', '<=', $endDate);

        if ($startDate) {
            $payment->where('date', '>=', $startDate);
        }

        return $payment->sum('amount');
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $rec_id
     * @return \Illuminate\Support\Collection|static
     */
    public function getRealEstateSalesClients($startDate, $endDate, $rec_id)
    {
        $unitHoldings = UnitHolding::whereHas(
            'commission',
            function ($commission) use ($rec_id) {
                $commission->where('recipient_id', $rec_id);
            }
        )->reservedBefore($endDate)->activeAtDate($endDate);

        if ($startDate) {
            $unitHoldings->reservedAfter($startDate);
        }

        return $unitHoldings->get()->map(function ($holding) {
            return [
                'Date' => Carbon::parse($holding->created_at)->toDateString(),
                'Name' => ($holding->client) ? $holding->client->name() : '',
                'Amount' => round($holding->price(), 0),
                'Cash_Price' => round($holding->repo->getCashPrice()),
                'Project' => $holding->unit->project->name . ' - ' . $holding->unit->number
            ];
        });
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $rec_id
     * @return \Illuminate\Support\Collection|static
     */
    public function getRealEstateActualPayments($startDate, $endDate, $rec_id)
    {
        $payments = RealEstatePayment::whereHas(
            'holding',
            function ($holding) use ($rec_id, $endDate) {
                $holding->activeAtDate($endDate)->whereHas(
                    'commission',
                    function ($commission) use ($rec_id) {
                        $commission->where('recipient_id', $rec_id);
                    }
                );
            }
        )->where('date', '<=', $endDate);

        if ($startDate) {
            $payments->where('date', '>=', $startDate);
        }

        return $payments->get()->map(
            function ($payment) {
                $holding = $payment->holding;

                return [
                    'Date' => Carbon::parse($payment->date)->toDateString(),
                    'Name' => ($holding->client) ? $holding->client->name() : '',
                    'Amount' => round($payment->amount, 0),
                    'Project' => $holding->unit->project->name . ' - ' . $holding->unit->number
                ];
            }
        );
    }
}
