<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 8/2/17
 * Time: 2:26 PM
 */

namespace Cytonn\Reporting\SummaryReports;

use App\Cytonn\Models\ClientInvestment;

trait Inflows
{
    public $currency;


    /**
     * Get the inflows of a specific product on a monthly basis
     *
     * @param  $start
     * @param  $end
     * @param  $rec_id
     * @param  $productIds
     * @return number
     */
    public function getInflows($start, $end, $rec_id = null, $productIds = null)
    {
        $q = ClientInvestment::where('invested_date', '>=', $start)
            ->where('invested_date', '<=', $end)
            ->whereHas(
                'commission',
                function ($q) use ($rec_id) {
                    if ($rec_id) {
                        $q->where('recipient_id', $rec_id);
                    }
                }
            );

        if ($productIds) {
            $q->whereIn('product_id', $productIds);
        }

        return $q->get()
            ->sum(function ($clientInvestment) use ($end) {
                return convert($clientInvestment->repo->inflow(), $clientInvestment->product->currency, $end);
            });
    }
}
