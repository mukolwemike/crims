<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Reporting\SummaryReports\Datatables;

use App\Cytonn\Models\BaseModel;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

trait SummaryPaginator
{
    /*
     * Get the state array variable
     */
    public $stateArray;
    protected $filterArray = [];

    /*
     * Process the table
     */
    public function processTable(
        BaseModel $model,
        $stateArray,
        TransformerAbstract $transformer,
        \Closure $filterFunction = null,
        $filterParams = []
    ) {
        $this->stateArray = $stateArray;

        $this->filterArray = $filterParams;

        $paginatedModel = $this->sortFilterPaginate($model, $filterFunction);

        $resource = new Collection($paginatedModel['model'], $transformer);

        $this->addPaginationToResource($paginatedModel, $resource);

        return (new Manager())->createData($resource)->toJson();
    }

    /*
     * Filter, Sort and Paginate the Model
     */
    public function sortFilterPaginate($model, $filterFunction)
    {
        $filtered = $this->filter($model, $filterFunction);

        return $this->paginate($this->sort($filtered));
    }

    /*
     * Do the filter for the model
     */
    public function filter($model, $filterFunction)
    {
        if (!is_null($this->stateArray['query'])) {
            $model = $model->search($this->stateArray['query']);
        }

        if ($filterFunction) {
            $model = $filterFunction($model);
        }

        foreach ($this->filterArray as $filter) {
            if (!isset($filter['sign'])) {
                $filter['sign'] = '=';
            }

            $model = $model->where($filter['column'], $filter['sign'], $filter['value']);
        }

        return $model;
    }

    /*
     * Do the sort on the model
     */
    public function sort($model)
    {
        return $model->orderBy($this->stateArray['sortItem'], $this->stateArray['sortDirection']);
    }

    /*
     * Paginate the model
     */
    public function paginate($model)
    {
        $page = $model->count();

        $model = $model->offset($this->stateArray['offset'])->take($this->stateArray['itemsPerPage'])->get();

        return [
            'model' => $model,
            'offset' => $this->stateArray['offset'],
            'total_pages' => ceil($page / $this->stateArray['itemsPerPage']),
            'total_results' => $page
        ];
    }

    /*
     * Add the pagination information to the resource
     */
    public function addPaginationToResource($paginatedModel, $resource)
    {
        return $resource->setMetaValue(
            'pagination',
            [
                'offset' => $paginatedModel['offset'],
                'total_pages' => $paginatedModel['total_pages'],
                'total_results' => $paginatedModel['total_results']
            ]
        );
    }
}
