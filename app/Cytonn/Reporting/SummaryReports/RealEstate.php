<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 8/2/17
 * Time: 2:26 PM
 */

namespace Cytonn\Reporting\SummaryReports;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;

trait RealEstate
{

    /**
     * Get real estate sales
     *
     * @param $rec_id
     */
    public function realEstateSales($start, $end, $rec_id)
    {
        return UnitHolding::whereHas(
            'commission',
            function ($commission) use ($rec_id) {
                $commission->where('recipient_id', $rec_id);
            }
        )
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->get()
            ->sum(
                function ($holding) {
                    return $holding->price();
                }
            );
    }

    /**
     * Get actual payments for real estate
     *
     * @param $start
     * @param $end
     * @param $rec_id
     */
    public function realEstateActualPayments($start, $end, $rec_id)
    {
        return RealEstatePayment::whereHas(
            'holding',
            function ($holding) use ($rec_id) {
                $holding->whereHas(
                    'commission',
                    function ($commission) use ($rec_id) {
                        $commission->where('recipient_id', $rec_id);
                    }
                );
            }
        )->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->sum('amount');
    }
}
