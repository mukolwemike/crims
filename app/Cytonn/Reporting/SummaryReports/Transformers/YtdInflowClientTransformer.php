<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Reporting\SummaryReports\Transformers;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use League\Fractal\TransformerAbstract;

class YtdInflowClientTransformer extends TransformerAbstract
{
    /*
     * Get the required variables
     */
    private $end;

    /*
     * Setup the constructor for the controller
     */
    public function __construct($end)
    {
        $this->end = $end;
    }

    /**
     * @param ClientInvestment $investment
     */
    public function transform(ClientInvestment $investment)
    {
        $amount = convert($investment->repo->inflow(), $investment->product->currency, Carbon::parse($this->end));

        return [
            'date' => Carbon::parse($investment->invested_date)->toDateString(),
            'name' => $investment->client->name(),
            'amount' => AmountPresenter::currency($amount, false, 0),
            'product' => $investment->product->name
        ];
    }
}
