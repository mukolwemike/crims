<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 8/2/17
 * Time: 2:27 PM
 */

namespace Cytonn\Reporting\SummaryReports;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;

trait Withdrawals
{
    public $currency;

    /**
     * Get the monthly product withdrawal either for an FA or the company in total
     *
     * @param  $start
     * @param  $end
     * @param  $rec_id
     * @param  $productIds
     * @return mixed
     */
    public function getWithdrawals($start, $end, $rec_id = null, $productIds = null)
    {
        $q = ClientInvestment::where('withdrawn', 1)->where('withdrawal_date', '>=', $start)
            ->where('withdrawal_date', '<=', $end)
            ->whereHas(
                'commission',
                function ($q) use ($rec_id) {
                    if ($rec_id) {
                        $q->where('recipient_id', $rec_id);
                    }
                }
            );

        if ($productIds) {
            $q->whereIn('product_id', $productIds);
        }

        return $q->get()->sum(
            function ($clientWithdrawal) use ($end) {
                $withdrawal = $clientWithdrawal->withdraw_amount;

                if ($withdrawal > $clientWithdrawal->amount) {
                    return $clientWithdrawal->amount;
                }

                return $this->convert($withdrawal, $clientWithdrawal->product->currency, $end);
            }
        );
    }

    public function convert($amount, Currency $currency, Carbon $date)
    {
        $rate = (new Convert($currency))->enableCaching(10)->read($this->currency, $date);

        return $amount * $rate;
    }
}
