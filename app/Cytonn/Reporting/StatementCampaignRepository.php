<?php
/**
 * Date: 10/03/2016
 * Time: 11:49 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Reporting;

use App\Cytonn\Models\CampaignStatementItem;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\User;
use Carbon\Carbon;

/**
 * Class StatementCampaignRepository
 *
 * @package Cytonn\Reporting
 */

/**
 * Class StatementCampaignRepository
 *
 * @package Cytonn\Reporting
 */
class StatementCampaignRepository
{
    /**
     * @var null|StatementCampaign
     */
    public $campaign;

    /**
     * StatementCampaignRepository constructor.
     *
     * @param StatementCampaign|null $campaign
     */
    public function __construct(StatementCampaign $campaign = null)
    {
        is_null($campaign) ? $this->campaign = new StatementCampaign() : $this->campaign = $campaign;
    }

    /**
     * Add a client to a campaign
     *
     * @param  Client $client
     * @param  User $user
     * @return bool
     */
    public function addClientToCampaign(Client $client, User $user)
    {
        $statementDate = $this->campaign->send_date->copy();

        $has_investments = $client->whereHas(
            'investments',
            function ($investment) use ($statementDate) {
                $investment->statement($statementDate);
            }
        )->count();

        if ($has_investments and !$this->checkIfClientIsInCampaign($client)) {
            return (new CampaignStatementItem())
                ->fill(
                    [
                        'client_id' => $client->id,
                        'campaign_id' => $this->campaign->id,
                        'added_by' => $user->id,
                        'added_on' => Carbon::now()
                    ]
                )->save();
        }
        return false;
    }

    /**
     * Check whether a client is already in a campaign
     *
     * @param  Client $client
     * @return bool
     */
    public function checkIfClientIsInCampaign(Client $client)
    {
        $item = CampaignStatementItem::where('campaign_id', $this->campaign->id)
            ->where('client_id', $client->id)->first();

        return !is_null($item);
    }

    /**
     * Queue a job to Send all unsent statements in a campaign
     */
    public function sendCampaign(User $user = null)
    {
        $this->campaign->sent_by = $user ? $user->id : \Auth::user()->id;
        $this->campaign->save();

        CampaignStatementItem::where('campaign_id', $this->campaign->id)
            ->where('sent', null)
            ->update(['ready' => true]);

//        \Queue::push(
//            'Cytonn\Reporting\StatementCampaignRepository@fireSendCampaign',
//            ['campaign_id' => $this->campaign->id]
//        );
    }

    /**
     * Send the campaign
     *
     * @param $job
     * @param $data
     */
    public function fireSendCampaign($job, $data)
    {
        $this->campaign = CampaignStatementItem::where('campaign_id', $this->campaign->id)->first();

        CampaignStatementItem::where('campaign_id', $data['campaign_id'])
            ->where('sent', null)
            ->get()
            ->each(
                function ($item) {
                    $this->sendCampaignItem($item);
                }
            );

        $job->delete();
    }

    /**
     * Send a statement in a campaign to a client
     *
     * @param $item
     */
    public function sendCampaignItem($item)
    {
        $item->ready = true;
        $item->save();
    }

    /**
     * @return mixed
     */
    public function countSentItems()
    {
        return $this->campaign->items()->where('sent', 1)->count();
    }

    /**
     * @return mixed
     */
    public function countAllItems()
    {
        return $this->campaign->items()->count();
    }

    /**
     * Automatically add the clients to campaign
     */
    public function automaticallyAddClients()
    {
        $statementDate = $this->campaign->send_date->copy();

        $clients = Client::whereHas('investments', function ($investment) use ($statementDate) {
            $investment->statement($statementDate)->whereHas('product', function ($product) {
                $product->whereHas('fundManager', function ($fm) {
                    $fm->where('active', 1);
                });
            });
        })->orWhereHas(
            'portfolioInvestments',
            function ($investment) use ($statementDate) {
                $investment->statement($statementDate);
            }
        )->whereDoesntHave('investmentCampaignItems', function ($q) {
            $q->where('campaign_id', $this->campaign->id);
        })->get();

        $clients->each(function ($client) {
            $has_emails = count($client->getContactEmailsArray()) > 0;

            if ($has_emails) {
                (new CampaignStatementItem())->fill([
                    'client_id' => $client->id,
                    'campaign_id' => $this->campaign->id,
                    'added_by' => $this->campaign->sender_id,
                    'added_on' => Carbon::now()
                ])->save();
            }
        });
    }

    public function fillMissing()
    {
        $statementDate = $this->campaign->send_date->copy();

        $clients = Client:: whereHas(
            'investments',
            function ($investment) use ($statementDate) {
                $investment->where('invested_date', '<=', $statementDate->copy()->addDay())
                    ->where(
                        function ($query) use ($statementDate) {
                            $query->active()
                                ->orWhere(
                                    function ($i) use ($statementDate) {
                                        $cutOff = $statementDate->copy()->subMonthNoOverFlow();

                                        $i->where('withdrawn', true)
                                            ->where('withdrawal_date', '>=', $cutOff);
                                    }
                                );
                        }
                    );
            }
        )->whereNotIn('id', $this->campaign->items()->lists('client_id'))->get();

        var_dump($clients->count());

        $clients->each(
            function ($client) {
                (new CampaignStatementItem())->fill(
                    [
                        'client_id' => $client->id,
                        'campaign_id' => $this->campaign->id,
                        'added_by' => $this->campaign->sender_id,
                        'added_on' => Carbon::now()
                    ]
                )->save();
            }
        );
    }

    public function fireAutomaticallyAddClients($job, $data)
    {
        $campaign = StatementCampaign::findOrFail($data['campaign_id']);

        $this->campaign = $campaign;

        $this->automaticallyAddClients();

        $job->delete();
    }

    public function productSummary()
    {
        return Product::active()->get()
            ->map(
                function ($product) {
                    $count = function ($in) use ($product) {
                        return Client::whereHas(
                            'investments',
                            function ($investment) use ($product) {
                                $investment->statement($this->campaign->send_date)->where('product_id', $product->id);
                            }
                        )
                            ->whereIn('id', $in)
                            ->count();
                    };

                    return [
                        'name' => $product->name,
                        'total' => $count($this->campaign->items()->remember(1)->pluck('client_id')->all()),
                        'sent' => $count($this->campaign->items()->whereNotNull('sent_on')->remember(1)
                            ->pluck('client_id')->all()),
                        'failed' => $count($this->campaign->items()->whereNotNull('failed_on')->remember(1)
                            ->pluck('client_id')->all())
                    ];
                }
            )->filter(
                function ($product) {
                    return $product['total'] > 0;
                }
            )->all();
    }
}
