<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class FAsCommissionSummaryBetweenDatesExcelGenerator
{
    public function excel(Carbon $startDate, Carbon $endDate, $title)
    {
        return Excel::create(
            $title,
            function ($excel) use ($startDate, $endDate) {
                // Loop through the months between start and end dates
                while ($startDate->lt($endDate)) {
                    $dates = $this->commissionDates($startDate->copy());

                    // Generate sheets for this month for each currency
                    $currencies = Currency::all();

                    $excel->sheet(
                        $startDate->copy()->format('M Y'),
                        function ($sheet) use ($startDate, $endDate, $currencies, $dates) {
                            $recipients = CommissionRecepient::all()
                                ->map(
                                    function (CommissionRecepient $recipient) use (
                                        $startDate,
                                        $endDate,
                                        $currencies,
                                        $dates
                                    ) {
                                        $e = new EmptyModel();

                                        $e->{'FA'} = $recipient->name;

                                        foreach ($currencies as $currency) {
                                            $calculator = $recipient->calculator(
                                                $currency,
                                                $dates->start->copy(),
                                                $dates->end->copy()
                                            );

                                            $e->{'CHYS ' . $currency->code} = $calculator->summary()->finalCommission();
                                        }

                                        $re_calculator = $recipient->reCommissionCalculator(
                                            $dates->start->copy(),
                                            $dates->end->copy()
                                        );

                                        $e->{'Real Estate'} = $re_calculator->summary()->finalCommission();

                                        $shareCalculator = $recipient->shareCommissionCalculator(
                                            $dates->start->copy(),
                                            $dates->end->copy()
                                        );

                                        $e->{'Shares'} = $shareCalculator->summary()->finalCommission();

                                        $unitFundCalculator = $recipient->unitizationCommissionCalculator(
                                            $dates->start->copy(),
                                            $dates->end->copy()
                                        );

                                        $e->{'Unit Funds'} = $unitFundCalculator->summary()->finalCommission();

                                        return $e;
                                    }
                                );

                            $sheet->fromModel($recipients);
                            $sheet->prependRow([$dates->end->copy()->format('M Y')]);
                            $sheet->mergeCells('A1:C1');
                        }
                    );

                    $startDate->addMonthNoOverflow();
                }
            }
        );
    }


    /**
     * @param Carbon $date
     * @return object
     */
    protected function commissionDates(Carbon $date)
    {
        $bulk = $this->getBulk($date);

        if ($bulk) {
            return (object)[
                'start' => $bulk->start,
                'end' => $bulk->end
            ];
        }

        return (object)[
            'start' => $this->getStart($date),
            'end' => $this->getEnd($date)
        ];
    }

    /**
     * @param Carbon $date
     * @return Carbon
     */
    private function getStart(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->subMonthNoOverflow())) {
            return $bulk->end->copy()->addDay();
        }

        return $date->copy()->subMonthNoOverflow()->startOfMonth()->addDays(20);
    }

    /**
     * @param Carbon $date
     * @return Carbon
     */
    private function getEnd(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->addMonthNoOverflow())) {
            return $bulk->start->copy()->subDay();
        }

        return $date->copy()->startOfMonth()->addDays(19);
    }

    /**
     * @param $date
     * @return mixed
     */
    private function getBulk($date)
    {
        return BulkCommissionPayment::where('start', '<=', $date)
            ->where('end', '>=', $date)->first();
    }
}
