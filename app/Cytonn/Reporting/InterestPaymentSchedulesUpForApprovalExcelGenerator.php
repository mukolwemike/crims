<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\InterestAction;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;

class InterestPaymentSchedulesUpForApprovalExcelGenerator
{
    public function excel($interest_payment_schedules_grouped_by_interest_action, Carbon $date, $approval_id)
    {
        $filtered_schedules = $interest_payment_schedules_grouped_by_interest_action;
        $file = \Excel::create(
            'Interest_Payment_Schedules',
            function ($excel) use ($filtered_schedules, $date, $approval_id) {
                if (count($filtered_schedules) == 0) {
                    $excel->sheet(
                        "No Data",
                        function ($sheet) {
                            $sheet->fromArray([]);
                        }
                    );
                } else {
                    foreach ($filtered_schedules as $interest_action_id => $interest_payment_schedules_arr) {
                        $sheet_name = InterestAction::findOrFail($interest_action_id)->name;
                        $excel->sheet(
                            $sheet_name,
                            function ($sheet) use ($interest_payment_schedules_arr, $date, $approval_id) {
                                $sheet->prependRow(1, ['Interest Payment Schedules Up For Approval Worksheet']);

                                $interest_payment_schedules_coll = $interest_payment_schedules_arr->map(
                                    function ($payment) use ($date, $approval_id) {
                                        $p = new EmptyModel();
                                        $p->ClientCode = $payment->investment->client->client_code;
                                        $p->Name = ClientPresenter::presentFullNames($payment->investment->client->id);
                                        $p->{'E-Mail'} = $payment->investment->client->contact->email;
                                        $p->Product = $payment->investment->product->name;
                                        $p->Principal = AmountPresenter::currency($payment->investment->amount);
                                        $p->InterestRate = $payment->investment->interest_rate . '%';
                                        $p->ValueDate = $payment->investment->invested_date;
                                        $p->MaturityDate = $payment->investment->maturity_date;
                                        $p->PaymentDescription = $payment->description;

                                        $bank = $payment->investment->bankDetails();

                                        try {
                                            $p->{'Account Name'} = $bank->accountName();
                                            $p->{'Account Number'} = $bank->accountNumber();
                                            ;
                                            $p->{'Branch'} = $bank->branch();
                                            $p->{'Bank'} = $bank->bankName();
                                        } catch (\Exception $e) {
                                        }

                                        // If exists approval, show amount in interest_payments table, else calculate
                                        $interest_payment = ClientInvestmentWithdrawal::where([
                                            'approval_id' => $approval_id,
                                            'investment_id' => $payment->investment_id])->get();
                                        //
                                        $p->PaymentAmount = $payment->calculatePaymentAmount($date);

                                        if ($interest_payment->count()) {
                                            $p->PaymentAmount = $interest_payment->sum('amount');
                                        }

                                        $p->PaymentAmount = (float)$p->PaymentAmount;

                                        return $p;
                                    }
                                );

                                $sheet->fromModel($interest_payment_schedules_coll);
                                $sheet->prependRow(['Interest payment schedule - Interest Calculated as at ' .
                                    $date->toFormattedDateString()]);
                                $sheet->mergeCells('A1:E1');
                            }
                        );
                    }
                }
            }
        );

        return $file;
    }
}
