<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;
use Maatwebsite\Excel\Facades\Excel;

class DailyBusinessConfirmationsReportExcelGenerator
{
    public function excel($file_name, $inv_bcs, $re_bcs, $shares_bcs, Carbon $today)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($inv_bcs, $re_bcs, $shares_bcs, $today) {
                // Investment BCs
                $excel->sheet(
                    'Investment BCs',
                    function ($sheet) use ($inv_bcs, $today) {
                        $inv_bcs = $inv_bcs->map(
                            function ($bc) {
                                $payload = $bc->payload;

                                $e = new EmptyModel();

                                $e->{'Client'} = ClientPresenter::presentFullNames($bc->investment->client_id);

                                $e->{'Client Code'} = $bc->investment->client->client_code;

                                $e->{'Principal'} = $payload['principal'];
                                $e->{'Interest Rate'} = $payload['interest_rate'];
                                $e->{'Invested Date'} = $payload['invested_date'];
                                $e->{'Maturity Date'} = $payload['maturity_date'];
                                $e->{'Date BC Sent'} = Carbon::parse($bc->created_at)->toDateTimeString();
                                $e->{'Sender'} = UserPresenter::presentFullNames($payload['sender']);

                                return $e;
                            }
                        );
                        $sheet->fromModel($inv_bcs);
                        $sheet->prependRow(['Today\'s Investment Business Confirmations - ' .
                            DatePresenter::formatDate($today->copy()->toDateString())]);
                        $sheet->mergeCells('A1:E1');
                    }
                );

                // Real Estate BCs
                $excel->sheet(
                    'RE BCs',
                    function ($sheet) use ($re_bcs, $today) {
                        $re_bcs = $re_bcs->map(
                            function ($bc) use ($today) {
                                $payload = $bc->payload;

                                $e = new EmptyModel();

                                $e->{'Client'} = ClientPresenter::presentFullNames($bc->payment->holding->client_id);
                                $e->{'Client Code'} = $bc->payment->holding->client->client_code;
                                $e->{'Project'} = $payload->project;
                                $e->{'Price'} = $payload->price;
                                $e->{'Unit No.'} = $bc->payment->holding->unit->size->name . ' ' . $bc->payment->holding
                                        ->unit->type->name;
                                $e->{'Unit Size/Type'} = $payload->unit_number;
                                $e->{'Amount Paid'} = $payload->total;
                                $e->{'Amount Remaining'} = $payload->amount_remaining;
                                $e->{'Date BC Sent'} = Carbon::parse($bc->created_at)->toDateTimeString();
                                $e->{'Sender'} = UserPresenter::presentFullNames($bc->sent_by);
                                $e->{'Payment Type'} = $bc->payment->type->name;

                                return $e;
                            }
                        );
                        $sheet->fromModel($re_bcs);
                        $sheet->prependRow(['Today\'s Real Estate Business Confirmations - ' .
                            DatePresenter::formatDate($today->copy()->toDateString())]);
                        $sheet->mergeCells('A1:E1');
                    }
                );

                // TODO Comment out once OTC is in production / or merged to master
                //            // Shares BCs
                //            $excel->sheet('Shares BCs', function($sheet) use($shares_bcs, $today)
                //            {
                //                $shares_bcs = $shares_bcs->map(function($bc)
                //                {
                //                    $payload = $bc->payload;
                //
                //                    $e = new \EmptyModel();
                //
                //                    $e->{'Shareholder'} = ClientPresenter::presentFullNames($bc->holding->client_id);
                //                    $e->{'Shareholder No.'} = is_null($bc->holding->shareHolder)
                //                          ? null : $bc->holding->shareHolder->number;
                //                    $e->{'Entity'} = is_null($bc->holding->shareHolder)
                //                                      ? null : $bc->holding->shareHolder->entity->name;
                //                    $e->{'No. of Shares'} = $bc->holding->number;
                //                    $e->{'Purchase Price'} = $bc->holding->purchase_price;
                //                    $e->{'Date'} = $payload['date'];
                //                    $e->{'Sender'} = UserPresenter::presentFullNames($payload['sender']);
                //
                //                    return $e;
                //                });
                //                $sheet->fromModel($shares_bcs);
                //                $sheet->prependRow(['Today\'s Shares Business Confirmations - ' .
                //                        DatePresenter::formatDate($today->copy()->toDateString())]);
                //                $sheet->mergeCells('A1:E1');
                //            });
            }
        );
    }
}
