<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class TrancheReportsExcelGenerator
{
    public function excel($tranches_grouped_by_project, $file_name)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($tranches_grouped_by_project, $file_name) {
                foreach ($tranches_grouped_by_project as $project => $tranches_arr) {
                    $project = Project::findOrFail($project);

                    $excel->sheet(
                        str_limit($project->name, 31),
                        function ($sheet) use ($tranches_arr, $file_name) {
                            $tranches = Collection::make($tranches_arr)->map(
                                function ($tranche) {
                                    $sizes = $tranche->sizes;

                                    $tr = new EmptyModel();
                                    $tr->Name = $tranche->name;

                                    foreach ($sizes as $sizing) {
                                        $tr->{$sizing->size->name} = $tranche->holdings()
                                            ->whereHas(
                                                'unit',
                                                function ($unit) use ($sizing) {
                                                    $unit->where('size_id', $sizing->size_id);
                                                }
                                            )
                                            ->active()
                                            ->count();
                                    }

                                    return $tr;
                                }
                            );

                            $sheet->fromModel($tranches);
                            $sheet->prependRow([$file_name]);

                            $sheet->mergeCells('A1:E1');
                            $sheet->cells(
                                'A1:E1',
                                function ($cells) {
                                    $cells->setFontWeight('bold');
                                }
                            );

                            $sheet->cells(
                                'A2:E2',
                                function ($cells) {
                                    $cells->setFontWeight('bold');
                                }
                            );
                        }
                    );
                }
            }
        );
    }
}
