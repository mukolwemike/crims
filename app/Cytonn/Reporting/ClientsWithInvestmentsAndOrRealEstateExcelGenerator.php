<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstatePayment;
use Carbon\Carbon;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Maatwebsite\Excel\Facades\Excel;

class ClientsWithInvestmentsAndOrRealEstateExcelGenerator
{
    public function excel($file_name)
    {
        return Excel::create(
            $file_name,
            function ($excel) {
                $excel->sheet(
                    'Clients',
                    function ($sheet) {
                        $clients = Client::all()->map(
                            function ($client) {
                                $c = new EmptyModel();
                                $c->ClientCode = $client->client_code;
                                $c->ClientName = ClientPresenter::presentFullNames($client->id);
                                $c->FA = is_null($client->getLatestFA()) ? null : $client->getLatestFA()->name;
                                $c->Investment =
                                    BooleanPresenter::presentYesNo($client->investments->count() > 0);
                                $c->RealEstate =
                                    BooleanPresenter::presentYesNo($client->unitHoldings->count() > 0);
                                $c->{'Client since'} = $this->dateOnboarded($client);
                                return $c;
                            }
                        );
                        $sheet->fromModel($clients);
                        $sheet->prependRow(
                            ['Clients Report - ' . DatePresenter::formatDate(Carbon::today()->toDateString())]
                        );
                        $sheet->mergeCells('A1:F1');
                    }
                );
            }
        );
    }

    private function dateOnboarded(Client $client)
    {
        $investment = ClientInvestment::where('client_id', $client->id)
            ->oldest('invested_date')->first();
        $re = RealEstatePayment::whereHas(
            'holding',
            function ($holding) use ($client) {
                $holding->where('client_id', $client->id);
            }
        )->oldest('date')
            ->first();

        $date = null;
        $re_date = null;

        if ($investment) {
            $date = $investment->invested_date;
        }
        if ($re) {
            $re_date = $re->date;
        }

        if ($date && $re_date) {
            return \Cytonn\Core\DataStructures\Carbon::earliest($date, $re_date);
        }

        if ($date) {
            return $date;
        }

        if ($re_date) {
            return $re_date;
        }

        return null;
    }
}
