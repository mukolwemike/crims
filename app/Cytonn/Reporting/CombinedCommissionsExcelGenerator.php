<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\CommissionOverrideStructure;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Crims\Commission\Override;
use Crims\Investments\Commission\RecipientCalculator;
use Crims\Realestate\Commission\RecipientCalculator as RecipientCalculator_RE;
use Crims\Shares\Commission\RecipientCalculator as RecipientCalculator_Shares;
use Crims\Unitization\Commission\RecipientCalculator as RecipientCalculator_Unitization;
use Crims\AdditionalCommission\Commission\RecipientCalculator as RecipientCalculator_Additional;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Presenters\BooleanPresenter;

/**
 * Class CombinedCommissionsExcelGenerator
 * @package Cytonn\Reporting
 */
class CombinedCommissionsExcelGenerator
{
    private $currencies;

    private $investmentCalculators;

    public function excel(BulkCommissionPayment $bcp)
    {
        $recipients = CommissionRecepient::with('type')->active()->get();

        $comms = $recipients->map(function (CommissionRecepient $recepient) use ($bcp) {
            return $this->presentFACommission($bcp, $recepient);
        })->reject(function ($comm) {
            return $comm->totals->realestate == 0
                && $comm->totals->cms == 0
                && $comm->totals->shares == 0
                && $comm->totals->units == 0
                && $comm->totals->additional == 0;
        });

        $groups = $comms->groupBy(function ($rec) {
            return $rec->recipient->type->name;
        })->map(function ($group) {
            return $group->map(function ($rec) {
                $recipient = $rec->recipient;

                return (new EmptyModel())->fill([
                    'Employee ID' => $recipient->employee_id,
                    'Employee Number' => $recipient->employee_number,
                    'Name' => $recipient->name,
                    'Active' => BooleanPresenter::presentYesNo($recipient->active),
                    'Account Number' => $recipient->account_number,
                    'KRA Pin' => $recipient->kra_pin,
                    'Bank Code' => $recipient->bank_code,
                    'Branch Code' => $recipient->branch_code,
                    'SP - KES' => $rec->totals->cms,
                    'RE - KES' => $rec->totals->realestate,
                    'UTF - KES' => $rec->totals->units,
                    'OTC - KES' => $rec->totals->shares,
                    'Advance - KES' => $rec->totals->additional,
                    'Total' => $rec->total
                ]);
            });
        });

        $file = \Excel::create('Combined_commissions', function ($excel) use ($bcp, $groups, $comms) {
            foreach ($groups as $key => $group) {
                $excel->sheet($key, function ($sheet) use ($group) {
                    $sheet->fromModel($group);
                });
            }

            foreach ($comms as $comm) {
                $excel->sheet(Excel::cleanSheetName($comm->recipient->name), function ($sheet) use ($comm) {
                    $sheet->loadView('exports.commission.combined_schedules', [
                        'investments' => $comm->schedules->cms,
                        'shares' => $comm->schedules->shares,
                        'real_estate' => $comm->schedules->realestate,
                        'units' => $comm->schedules->units,
                        'additional' => $comm->schedules->additional
                    ]);
                });
            }
        });

        return $file;
    }

    /**
     * @param BulkCommissionPayment $bcp
     * @param CommissionRecepient $recepient
     * @return object
     * @throws \App\Exceptions\CrimsException
     */

    private function presentFACommission(BulkCommissionPayment $bcp, CommissionRecepient $recepient)
    {
        $shares = new RecipientCalculator_Shares($recepient, $bcp->start, $bcp->end);
        $re = new RecipientCalculator_RE($recepient, $bcp->start, $bcp->end);
        $units = new RecipientCalculator_Unitization($recepient, $bcp->start, $bcp->end);

        $cms = $this->currencies()->map(function ($curr) use ($recepient, $bcp) {
            return (object)[
                'currency' => $curr,
                'calculator' => $this->getInvestmentCalculator($recepient, $curr, $bcp->start, $bcp->end)
            ];
        });

        $cms_schedules = $cms->map(function ($cm) use ($recepient, $bcp) {
            return (object)[
                'currency' => $cm->currency,
                'schedules' => $cm->calculator->schedules(),
                'claw_backs' => $cm->calculator->clawbacks(),
                'overrides' => $this->getInvestmentsReports($recepient, $bcp, $cm->currency, $cm->calculator),
            ];
        })->reject(function ($cm) {
            return (count($cm->schedules) == 0) && (count($cm->claw_backs) == 0) && (count($cm->overrides) == 0);
        });

        $re_total = $re->summary()->finalCommission();
        $cms_total = $cms->sum(function ($cm) use ($bcp) {
            return convert($cm->calculator->summary()->finalCommission(), $cm->currency, $bcp->end);
        });
        $share_total = $shares->summary()->finalCommission();
        $units_total = $units->summary()->finalCommission();

        $totalCommission = $re_total + $cms_total + $share_total + $units_total;
        $additional = new RecipientCalculator_Additional($recepient, $bcp->start, $bcp->end, $totalCommission);

        $schedules = (object)[
            'realestate' => (object)[
                'schedules' => $re->schedules(),
                'overrides' => $this->getRealEstateReports($recepient, $bcp, $re),
            ],
            'shares' => (object)[
                'schedules' => $shares->schedules()->get(),
                'overrides' => $this->getShareReports($recepient, $bcp, $shares)
            ],
            'units' => (object)[
                'schedules' => $units->schedules()->get(),
                'overrides' => $this->getUnitizationReports($recepient, $bcp, $units)
            ],
            'cms' => $cms_schedules,
            'additional' => (object) [
                'additional' => $additional->additionalCommissions(),
                'unpaid' => $additional->allUnpaidAdditionalCommissions()
            ]
        ];

        $additional_total = $additional->summary()->getAdjustmentCommission();

        return (object)[
            'recipient' => $recepient,
            'schedules' => $schedules,
            'totals' => (object)[
                'realestate' => $re_total,
                'cms' => $cms_total,
                'shares' => $share_total,
                'units' => $units_total,
                'additional' => $additional_total
            ],
            'total' => $share_total + $cms_total + $units_total + $re_total + $additional_total
        ];
    }

    /**
     * @param BulkCommissionPayment $bulk
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    private function getOverrideStructure(BulkCommissionPayment $bulk, $slug)
    {
        return CommissionOverrideStructure::where('slug', $slug)
            ->where('date', '<=', $bulk->end)
            ->latest('date')
            ->first();
    }

    /*
     * Get the override rate to use
     */
    /**
     * @param $recipient
     * @param $structure
     * @return int
     */
    public function getOverrideRate($recipient, $structure)
    {
        $rate = $structure->rates()->where('rank_id', $recipient->rank_id)->first();

        if (!$rate) {
            return 0;
        }

        return $rate->rate;
    }

    /*
     * INVESTMENTS
     */
    /**
     * @param CommissionRecepient $recipient
     * @param BulkCommissionPayment $bcp
     * @param Currency $currency
     * @return mixed
     * @throws \App\Exceptions\CrimsException
     */
    private function getInvestmentsReports(
        CommissionRecepient $recipient,
        BulkCommissionPayment $bcp,
        Currency $currency,
        RecipientCalculator $calculator
    ) {

        $overrideClass = (new Override($recipient, $bcp->end, 'investments', $bcp->start));

        $reportsArray = array();

        $overrideRate = $overrideClass->getOverrideRate();

        if ($overrideRate == 0) {
            return $reportsArray;
        }

        $reports = $calculator->getReports();

        $recipientSummary = $calculator->summary();

        foreach ($reports as $fa) {
            $reportsArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $recipientSummary->overrideCommission($fa),
                'rate' => $overrideRate,
                'override' => $recipientSummary->overrideOnRecipient($fa)
            ];
        }

        return $reportsArray;
    }

    /*
     * SHARES
     */
    /**
     * @param CommissionRecepient $recipient
     * @param BulkCommissionPayment $bulk
     * @return mixed
     * @throws \App\Exceptions\CrimsException
     */
    private function getShareReports(
        CommissionRecepient $recipient,
        BulkCommissionPayment $bulk,
        RecipientCalculator_Shares $calculator
    ) {
        $reportsArray = array();

        $overrideStructure = $this->getOverrideStructure($bulk, 'shares');

        if (is_null($overrideStructure)) {
            return $reportsArray;
        }

        $overrideRate = $this->getOverrideRate($recipient, $overrideStructure);

        if ($overrideRate == 0) {
            return $reportsArray;
        }

        $reports = $calculator->getReports();

        $recipientSummary = $calculator->summary();

        foreach ($reports as $fa) {
            $reportsArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $recipientSummary->overrideCommission($fa),
                'rate' => $overrideRate,
                'override' => $recipientSummary->overrideOnRecipient($fa)
            ];
        }

        return $reportsArray;
    }

    /*
     * UNITIZATION
     */
    /**
     * @param CommissionRecepient $recipient
     * @param BulkCommissionPayment $bulk
     * @return mixed
     * @throws \App\Exceptions\CrimsException
     */
    private function getUnitizationReports(
        CommissionRecepient $recipient,
        BulkCommissionPayment $bulk,
        RecipientCalculator_Unitization $calculator
    ) {
        $reportsArray = array();

        $overrideStructure = $this->getOverrideStructure($bulk, 'unitization');

        if (is_null($overrideStructure)) {
            return $reportsArray;
        }

        $overrideRate = $this->getOverrideRate($recipient, $overrideStructure);

        if ($overrideRate == 0) {
            return $reportsArray;
        }

        $reports = $calculator->getReports();

        $recipientSummary = $calculator->summary();

        foreach ($reports as $fa) {
            $reportsArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $recipientSummary->overrideCommission($fa),
                'rate' => $overrideRate,
                'override' => $recipientSummary->overrideOnRecipient($fa)
            ];
        }

        return $reportsArray;
    }

    /*
     * REAL ESTATE
     */
    /**
     * @param CommissionRecepient $recipient
     * @param BulkCommissionPayment $bulk
     * @return mixed
     * @throws \App\Exceptions\CrimsException
     */
    private function getRealEstateReports(
        CommissionRecepient $recipient,
        BulkCommissionPayment $bulk,
        RecipientCalculator_RE $calculator
    ) {
        $overrideClass = new Override($recipient, $bulk->end, 'real_estate', $bulk->start);

        $reportsArray = array();

        $overrideRate = $overrideClass->getOverrideRate();

        if ($overrideRate == 0) {
            return $reportsArray;
        }

        $reports = $calculator->getReports();

        $recipientSummary = $calculator->summary();

        foreach ($reports as $fa) {
            $reportsArray[] = [
                'name' => $fa->name,
                'rank' => $fa->rank->name,
                'commission' => $recipientSummary->overrideCommission($fa),
                'rate' => $overrideRate,
                'override' => $recipientSummary->overrideOnRecipient($fa)
            ];
        }

        return $reportsArray;
    }

    private function getInvestmentCalculator($recipient, $currency, $start, $end, $rateStart = null, $rateEnd = null)
    {
        $key = $this->investmentCalculatorKey($recipient, $currency, $start, $end, $rateStart, $rateEnd);

        if (isset($this->investmentCalculators[$key])) {
            return $this->investmentCalculators[$key];
        }

        $calc = $recipient->calculator($currency, $start, $end);

        if ($rateEnd && $rateStart) {
            $calc = $calc->setOverrideDates($rateStart, $rateEnd);
        }

        $this->saveInvestmentCalculator($calc, $recipient, $currency, $start, $end, $rateStart, $rateEnd);

        return $calc;
    }

    private function saveInvestmentCalculator(
        RecipientCalculator $calculator,
        CommissionRecepient $recipient,
        Currency $currency,
        Carbon $start,
        Carbon $end,
        Carbon $rateStart = null,
        Carbon $rateEnd = null
    ) {
        $key = $this->investmentCalculatorKey($recipient, $currency, $start, $end, $rateStart, $rateEnd);

        $this->investmentCalculators[$key] = $calculator;

        return $calculator;
    }

    private function investmentCalculatorKey(
        CommissionRecepient $recipient,
        Currency $currency,
        Carbon $start,
        Carbon $end,
        Carbon $rateStart = null,
        Carbon $rateEnd = null
    ) {
        $st = $rateStart ? $rateStart->toDateString() : 'none';
        $en = $rateEnd ? $rateEnd->toDateString() : 'none';

        return 'investment_calculator_for' . $recipient->id . 'currency' . $currency->id . 'from' .
            $start->toDateString() . 'to' . $end->toDateString() . "_limit_$st" . '_and_' . $en;
    }

    private function currencies()
    {
        if (!$this->currencies) {
            $this->currencies = Currency::whereHas('products', function ($q) {
                $q->active()->has('investments');
            })->get();
        }

        return $this->currencies;
    }
}
