<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Maatwebsite\Excel\Facades\Excel;

class ClientWithdrawalsExcelGenerator
{
    public function excel(Carbon $startDate, Carbon $endDate)
    {
        return Excel::create(
            'Client Withdrawals ' . $startDate->toDateString() . ' - ' . $endDate->toDateString(),
            function ($excel) use ($startDate, $endDate) {
                $excel->sheet(
                    'Withdrawals',
                    function ($sheet) use ($startDate, $endDate) {
                        $withdrawals = ClientInvestmentWithdrawal::where('date', '>=', $startDate)
                            ->where('date', '<=', $endDate)
                            ->where('type_id', 1)
                            ->where('withdraw_type', 'withdrawal')
                            ->where('amount', '>', 0)
                            ->get()
                            ->map(
                                function ($withdrawal) {
                                    $investment = $withdrawal->investment;

                                    $d = new EmptyModel();
                                    $d->ClientName = ClientPresenter::presentFullNames($investment->client_id);
                                    $d->ClientCode = $investment->client->client_code;
                                    $d->Currency = $investment->product->currency->code;
                                    $d->Principal = $investment->amount;
                                    $d->InterestRate = $investment->interest_rate;
                                    $d->ValueDate = DatePresenter::formatDate($investment->invested_date);
                                    $d->WithdrawnAmount = $withdrawal->amount;
                                    $d->WithdrawnOn = DatePresenter::formatDate($withdrawal->date);

                                    return $d;
                                }
                            );

                        $sheet->fromModel($withdrawals);

                        $sheet->prependRow([$startDate->formatLocalized('%A %d %B %Y') . ' - ' .
                            $endDate->formatLocalized('%A %d %B %Y')]);
                        $sheet->prependRow(['Client Investment Withdrawals']);
                        $sheet->mergeCells('A1:G1');
                        $sheet->mergeCells('A2:G2');
                    }
                );

                $excel->sheet(
                    'Interest Payments',
                    function ($sheet) use ($startDate, $endDate) {
                        $interest_payments = ClientInvestmentWithdrawal::where('withdraw_type', 'interest')
                            ->where('date', '>=', $startDate)
                            ->where('date', '<=', $endDate)
                            ->where('amount', '>', 0)
                            ->get()
                            ->map(
                                function ($ip) {
                                    $investment = $ip->investment;

                                    $d = new EmptyModel();
                                    $d->ClientName = ClientPresenter::presentFullNames($investment->client_id);
                                    $d->ClientCode = $investment->client->client_code;
                                    $d->Currency = $investment->product->currency->code;
                                    $d->Principal = $investment->amount;
                                    $d->InterestRate = $investment->interest_rate;
                                    $d->ValueDate = DatePresenter::formatDate($investment->invested_date);
                                    $d->Amount = $ip->amount;
                                    $d->DatePaid = DatePresenter::formatDate($ip->date);
                                    $d->Action = $ip->type->name;

                                    return $d;
                                }
                            );

                        $sheet->fromModel($interest_payments);

                        $sheet->prependRow([$startDate->formatLocalized('%A %d %B %Y') . ' - ' .
                            $endDate->formatLocalized('%A %d %B %Y')]);
                        $sheet->prependRow(['Client Interest Payments']);
                        $sheet->mergeCells('A1:G1');
                        $sheet->mergeCells('A2:G2');
                    }
                );
            }
        );
    }
}
