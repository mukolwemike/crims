<?php


namespace Cytonn\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateCampaignStatementItem;
use App\Cytonn\Models\RealestateStatementCampaign;
use App\Cytonn\Models\StatementCampaign;
use App\Cytonn\Models\User;
use Carbon\Carbon;

/**
 * Class RealestateStatementCampaignRepository
 *
 * @package Cytonn\Reporting
 */
class RealestateStatementCampaignRepository
{
    /**
     * @var null|StatementCampaign
     */
    public $campaign;

    /**
     * RealestateStatementCampaignRepository constructor.
     *
     * @param RealestateStatementCampaign|null $campaign
     */
    public function __construct(RealestateStatementCampaign $campaign = null)
    {
        is_null($campaign) ? $this->campaign = new RealestateStatementCampaign() : $this->campaign = $campaign;
    }

    /**
     * Add a client to a campaign
     *
     * @param  Client $client
     * @param  User $user
     * @return bool
     */
    public function addClientToCampaign(Client $client, User $user)
    {
        $has_holdings = $client->whereHas(
            'unitHoldings',
            function ($holding) {
                $holding->active()->whereHas('unit', function ($q) {
                    $q->has('project');
                });
            }
        )->count();

        if ($has_holdings and !$this->checkIfClientIsInCampaign($client)) {
            return (new RealestateCampaignStatementItem())
                ->fill(
                    [
                        'client_id' => $client->id,
                        'campaign_id' => $this->campaign->id,
                        'added_by' => $user->id,
                        'added_on' => Carbon::now()
                    ]
                )->save();
        }
        return false;
    }

    /**
     * Check whether a client is already in a campaign
     *
     * @param  Client $client
     * @return bool
     */
    public function checkIfClientIsInCampaign(Client $client)
    {
        $item = RealestateCampaignStatementItem::where('campaign_id', $this->campaign->id)
            ->where('client_id', $client->id)->first();

        return !is_null($item);
    }

    /**
     * Queue a job to Send all unsent statements in a campaign
     */
    public function sendCampaign(User $user = null)
    {
        $this->campaign->sent_by = $user ? $user->id : \Auth::user()->id;
        $this->campaign->save();

        RealestateCampaignStatementItem::where('campaign_id', $this->campaign->id)
            ->where('sent', null)
            ->update(['ready' => true]);

//        \Queue::push(
//            'Cytonn\Reporting\RealestateStatementCampaignRepository@fireSendCampaign',
//            ['campaign_id' => $this->campaign->id]
//        );
    }

    /**
     * Send the campaign
     *
     * @param $job
     * @param $data
     */
    public function fireSendCampaign($job, $data)
    {
        RealestateCampaignStatementItem::where('campaign_id', $data['campaign_id'])
            ->where('sent', null)
            ->get()
            ->each(
                function ($item) {
                    $this->sendCampaignItem($item);
                }
            );

        $job->delete();
    }

    /**
     * Send a statement in a campaign to a client
     *
     * @param $item
     */
    public function sendCampaignItem($item)
    {
        $item->ready = true;
        $item->save();
    }

    /**
     * @return mixed
     */
    public function countSentItems()
    {
        return $this->campaign->items()->where('sent', 1)->count();
    }

    /**
     * @return mixed
     */
    public function countAllItems()
    {
        return $this->campaign->items()->count();
    }

    /**
     * Automatically add the clients to campaign
     */
    public function automaticallyAddClients()
    {
        Client::whereHas('unitHoldings', function ($q) {
            $q->where('active', 1)->where('receive_campaign_statement', 1)->whereHas('unit', function ($q) {
                $q->has('project');
            });
        })->whereDoesntHave('realEstateCampaignItems', function ($q) {
            $q->where('campaign_id', $this->campaign->id);
        })->get()->each(function (Client $client) {
            (new RealestateCampaignStatementItem())->fill([
                'client_id' => $client->id,
                'campaign_id' => $this->campaign->id,
                'added_by' => $this->campaign->sender_id,
                'added_on' => Carbon::now()
            ])->save();
        });
    }

    public function fillMissing()
    {
        $clients = Client::whereHas(
            'unitHoldings',
            function ($q) {
                $q->where('active', 1)->whereHas('unit', function ($q) {
                    $q->has('project');
                });
            }
        )->whereNotIn('id', $this->campaign->items()->lists('client_id'))->get();

        $clients->each(
            function ($client) {
                (new RealestateCampaignStatementItem())->fill(
                    [
                        'client_id' => $client->id,
                        'campaign_id' => $this->campaign->id,
                        'added_by' => $this->campaign->sender_id,
                        'added_on' => Carbon::now()
                    ]
                )->save();
            }
        );
    }

    public function fireAutomaticallyAddClients($job, $data)
    {
        $campaign = RealestateStatementCampaign::findOrFail($data['campaign_id']);

        $this->campaign = $campaign;

        $this->automaticallyAddClients();

        $job->delete();
    }

    public function productSummary()
    {
        return Project::all()
            ->map(
                function ($project) {
                    $count = function ($in) use ($project) {
                        return Client::whereHas(
                            'unitHoldings',
                            function ($holding) use ($project) {
                                $holding->whereHas(
                                    'unit',
                                    function ($unit) use ($project) {
                                        $unit->where('project_id', $project->id);
                                    }
                                );
                            }
                        )
                            ->whereIn('id', $in)
                            ->count();
                    };

                    return [
                        'name' => $project->name,
                        'total' => $count($this->campaign->items()->remember(1)->pluck('client_id')->all()),
                        'sent' => $count($this->campaign->items()->whereNotNull('sent_on')->remember(1)
                            ->pluck('client_id')->all()),
                        'failed' => $count($this->campaign->items()->whereNotNull('failed_on')->remember(1)
                            ->pluck('client_id')->all())
                    ];
                }
            )->filter(
                function ($product) {
                    return $product['total'] > 0;
                }
            )->all();
    }
}
