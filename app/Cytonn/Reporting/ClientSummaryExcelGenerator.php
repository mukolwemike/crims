<?php

namespace Cytonn\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Portfolio\DepositHolding;
use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;
use Cytonn\Presenters\ClientPresenter;

class ClientSummaryExcelGenerator
{
    use CalculatorTrait;

    public function excel($product, $fileName = null, Carbon $date = null)
    {
        $date = $date ? $date : Carbon::today();

        $dateString = \Cytonn\Presenters\DatePresenter::formatDate(Carbon::parse($date));

        if (is_null($fileName)) {
            $fileName = $product->name . '_Worksheet_' . $dateString;
        }

        $file = \Excel::create(
            $fileName,
            function ($excel) use ($product, $date) {
                $excel->sheet(
                    'Client_Summary',
                    function ($sheet) use ($product, $date) {
                        $sheet->prependRow(1, [$product->name]);

                        $clients = Client::whereHas(
                            'investments',
                            function ($q) use ($product, $date) {
                                $q->where('product_id', $product->id)->activeOnDate($date);
                            }
                        )->get();

                        $clients_coll = new \Illuminate\Support\Collection();

                        foreach ($clients as $client) {
                            $c = new Client();
                            $c->Name = ClientPresenter::presentFullNames($client->id);
                            $c->ClientCode = $client->client_code;
                            $c->TodayInvestedAmount = $client->repo->getTodayInvestedAmountForProduct($product, $date);
                            $c->CustodyFees = $client->repo->getCustodyFeesForProductAtDate($product, $date);

                            $clients_coll->push($c);
                        }

                        $sheet->fromModel($clients_coll);
                    }
                );

                $excel->sheet(
                    'Deposits',
                    function ($sheet) use ($date) {
                        $deposits = DepositHolding::activeOnDate($date)->get();
                        $dep_coll = new \Illuminate\Support\Collection();
                        foreach ($deposits as $deposit) {
                            $dep = new DepositHolding();
                            $dep->Security = $deposit->security->name;
                            $dep->Amount = $deposit->amount;
                            $dep->ValueDate = $deposit->invested_date;
                            $dep->MaturityDate = $deposit->maturity_date;
                            $dep->DaysElapsed = $deposit->repo->getTenorInDaysAsAtDate($date);
                            $dep->GrossInterest = $deposit->repo->getGrossInterestForInvestment($date);
                            $dep->WithHoldingTax = $deposit->repo->getWithholdingTaxForInvestment($date);
                            $dep->NetInterest = $deposit->repo->getNetInterestForInvestment($date);
                            $dep->TotalValue = $deposit->repo->getTotalValueOfAnInvestmentAsAtDate($date);

                            $dep_coll->push($dep);
                        }
                        $sheet->fromModel($dep_coll);
                    }
                );

                $excel->sheet(
                    'Commission',
                    function ($sheet) use ($product, $date) {
                        $recipients = CommissionRecepient::whereHas(
                            'commissions',
                            function ($q) use ($product, $date) {
                                $q->whereHas(
                                    'investment',
                                    function ($q) use ($product, $date) {
                                        $q->where('product_id', $product->id)->activeOnDate($date);
                                    }
                                );
                            }
                        )->with('commissions')->get();

                        $comm_coll = new \Illuminate\Support\Collection();

                        foreach ($recipients as $recipient) {
                            $commissions = $recipient->commissions()->whereHas(
                                'investment',
                                function ($q) use ($product, $date) {
                                    $q->activeOnDate($date)->where('product_id', $product->id);
                                }
                            )->get();

                            foreach ($commissions as $commission) {
                                $c = new Commission();
                                $c->FAName = $recipient->name;
                                $c->Client = ClientPresenter::presentFullNames($commission->investment->client_id);
                                $c->Principal = $commission->investment->amount;
                                $c->invested_date = $commission->investment->invested_date;
                                $c->maturity_date = $commission->investment->maturity_date;
                                $c->rate = $commission->rate . '%';
                                $c->TotalCommission = $commission->rate * $commission->investment->amount;
                                $c->FirstInstallment = $c->TotalCommission / 2;

                                $comm_coll->push($c);
                            }
                        }

                        $sheet->fromModel($comm_coll);
                    }
                );

                $clients = Client::whereHas(
                    'investments',
                    function ($q) use ($product, $date) {
                        $q->where('product_id', $product->id)->activeOnDate($date);
                    }
                )->with('investments')->get();

                foreach ($clients as $client) {
                    $excel->sheet(
                        substr(ClientPresenter::presentFullNames($client->id), 0, 30),
                        function ($sheet) use ($client, $product, $date) {
                            $investments =
                                $client->investments()->where('product_id', $product->id)->activeOnDate($date)->get();

                            $inv_coll = new \Illuminate\Support\Collection();

                            foreach ($investments as $investment) {
                                $inv = new ClientInvestment();
                                $inv->ClientName = ClientPresenter::presentFullNames($investment->client_id);
                                $inv->ClientCode = $investment->client->client_code;
                                $inv->InvestedAmount = $investment->amount;
                                $inv->ClientRate = $investment->interest_rate;
                                $inv->ValueDate = $investment->invested_date;
                                $inv->MaturityDate = $investment->maturity_date;
                                $inv->Tenor = $investment->repo->getTenorInDaysAsAtDate($date);
                                $inv->GrossInterest = $investment->repo->getGrossInterestForInvestmentAtDate($date);
                                $inv->WithHoldingTax = $investment->repo->getWithholdingTaxAtDate($date);
                                $inv->NetInterest = $investment->repo->getNetInterestForInvestmentAtDate($date);

                                $inv_coll->push($inv);
                            }
                            $sheet->fromModel($inv_coll);
                        }
                    );
                }
            }
        );

        return $file;
    }
}
