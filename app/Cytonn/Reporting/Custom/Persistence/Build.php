<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 09/10/2018
 * Time: 10:33
 */

namespace App\Cytonn\Reporting\Custom\Persistence;

use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportFilter;
use App\Cytonn\Reporting\Custom\Query\Filter;

class Build
{
    public function build(Report $report)
    {
        $builtReport = new \Cytonn\Reporting\Custom\Report($report->model_name);

        $builtReport->with(unserialize($report->with_relations));

        $builtReport->columns($this->getColumns($report));
        $builtReport->labels($this->getLabels($report));
        $builtReport->format($this->getFormats($report));
        $builtReport->arguments($this->getArguments($report));

        $filters = $this->getFilters($report);

        foreach ($filters as $filter) {
            $builtReport->filter($filter);
        }

        return $builtReport;
    }

    private function getColumns(Report $report)
    {
        $columns = $report->columns;

        return $columns->lists('name');
    }

    private function getLabels(Report $report)
    {
        $columns = $report->columns()->whereNotNull('label')->get();

        return $columns->lists('label', 'name');
    }

    private function getFormats(Report $report)
    {
        $columns = $report->columns()->whereNotNull('format')->get();

        return $columns->lists('format', 'name');
    }

    private function getArguments(Report $report)
    {
        $columns = $report->columns;

        return $columns->mapWithKeys(function ($column) {
            return [$column->name => $column->arguments()->orderBy('index', 'ASC')->get()->lists('value')];
        })->all();
    }

    private function getFilters(Report $report)
    {
        $filters = $report->filters()->whereNull('parent_id')->get();

        return $filters->map(function (ReportFilter $filter) {
            return Filter::build($filter);
        });
    }
}
