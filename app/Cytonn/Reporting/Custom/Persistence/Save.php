<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 09/10/2018
 * Time: 10:33
 */

namespace App\Cytonn\Reporting\Custom\Persistence;

use App\Cytonn\Reporting\Custom\Query\Filter;
use Cytonn\Reporting\Custom\Report;

class Save
{

    public function persist(Report $builtReport)
    {
        $report = \App\Cytonn\Models\Reporting\Report::create([
            'name' => $builtReport->getModel().' Report',
            'model_name' => $builtReport->getModel(),
            'with_relations' => serialize($builtReport->getRelations())
        ]);

        $this->saveColumns($builtReport, $report);

        $this->saveLabels($builtReport, $report);

        $this->saveFormats($builtReport, $report);

        $this->saveArguments($builtReport, $report);

        $this->saveFilters($builtReport, $report);

        return $report->fresh();
    }

    private function saveColumns(Report $builtReport, \App\Cytonn\Models\Reporting\Report $report)
    {
        $cols = collect($builtReport->getColumns())->map(function ($col) {
            return ['name' => $col];
        })->all();

        $report->columns()->createMany($cols);

        return $report;
    }

    private function saveLabels(Report $builtReport, \App\Cytonn\Models\Reporting\Report $report)
    {
        $columns = $report->columns;

        collect($builtReport->getLabels())->map(function ($val, $key) use ($columns) {
            $col = $columns->where('name', $key)->first();

            $col->update(['label' => $val]);
        });

        return $report;
    }

    private function saveFormats(Report $builtReport, \App\Cytonn\Models\Reporting\Report $report)
    {
        $columns = $report->columns;

        collect($builtReport->getFormats())->map(function ($val, $key) use ($columns) {
            $col = $columns->where('name', $key)->first();

            $col->update(['format' => $val]);
        });

        return $report;
    }

    private function saveArguments(Report $builtReport, \App\Cytonn\Models\Reporting\Report $report)
    {
        $columns = $report->columns;

        collect($builtReport->getArguments())->map(function ($val, $key) use ($columns) {
            $col = $columns->where('name', $key)->first();

            if (!is_array($val)) {
                $val = [$val];
            }

            $i = 0;

            foreach ($val as $arg) {
                $col->arguments()->create([
                    'index' => $i,
                    'value' => $arg
                ]);

                $i++;
            }
        });

        return $report;
    }

    private function saveFilters(Report $builtReport, \App\Cytonn\Models\Reporting\Report $report)
    {
        collect($builtReport->getFilters())->map(function (Filter $filter) use ($report) {
            $filter->save($report);
        });

        return $report;
    }
}
