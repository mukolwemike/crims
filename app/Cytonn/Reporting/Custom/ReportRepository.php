<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/8/18
 * Time: 8:34 AM
 */

namespace App\Cytonn\Reporting\Custom;

use App\Cytonn\Api\Transformers\Investments\CustomReportTransformer;
use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportArgument;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;

class ReportRepository
{
    use AlternateSortFilterPaginateTrait;

    public function reports()
    {
        return $this->sortFilterPaginate(
            new Report(),
            [],
            function ($report) {
                return app(CustomReportTransformer::class)->transform($report);
            },
            function ($model) {
                return $model;
            }
        );
    }

    public function saveReport($input)
    {
        $input['with_relations'] = serialize($input['with_relations']);

        $report = new Report();

        $report->fill($input);

        $report->save();

        return $report;
    }

    public function saveReportColumns(Report $report, $columns)
    {
        foreach ($columns as $column) {
            $column['label'] = !isset($column['label'])
                ? ucwords(str_replace('_', ' ', $column['name']))
                : $column['label'];

            $report->columns()->create($column);
        }

        return $report;
    }

    public function saveArguments($arguments)
    {
        dd($arguments);
        foreach ($arguments as $argument) {
            $arg = new ReportArgument();

            $arg->fill($argument);

            $arg->save();
        }

        return;
    }

    public function saveReportFilters(Report $report, $filters)
    {
        $report->filters()->createMany($filters);

        return $report;
    }

    public function dynamicArguments(Report $report)
    {
        return ReportArgument::whereHas('column', function ($column) use ($report) {
            $column->whereHas('report', function ($r) use ($report) {
                $r->where('id', $report->id);
            });
        })
            ->whereNotNull('dynamic')
            ->get();
    }
}
