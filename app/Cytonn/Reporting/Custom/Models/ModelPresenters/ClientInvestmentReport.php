<?php
/**
 * Date: 27/03/2018
 * Time: 10:03
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Reporting\Custom\Models\ModelPresenters;

use Carbon\Carbon;
use Cytonn\Reporting\Custom\Models\Presenter;

class ClientInvestmentReport extends Presenter
{
    public $columns = [
        'amount' => '',
        'principal' => '@principal',
        'maturity_date' => '',
        'invested_date' => '',
        'withdrawal_date' => '',
        'gross_interest'  => '',
        'net_interest'    =>  '',
        'withdrawn_amount' => '',
        'total'           =>  '',
        'client_code' => 'client.client_code'
    ];

    public $formats = [
        'amount' => 'currency',
        'principal' => 'currency',
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'withdrawal_date' => 'date',
        'gross_interest'  => 'currency',
        'net_interest'    =>  'currency',
        'withdrawn_amount' => 'currency',
        'total'           =>  'currency'
    ];

    public $arguments = [
        'principal' => 'date|date',
        'net_interest' => 'date|date,next_day|bool',
        'gross_interest' => 'date|date,next_day|bool',
        'total' => 'date|date,next_day|bool'
    ];

    public $relations= [
        'client',
        'client.contact',
        'withdrawals',
        'topupsTo',
        'topupsFrom'
    ];

    protected function principal($date)
    {
        $date = Carbon::parse($date);

        return  $this->entity->repo->principal($date);
    }

    protected function netInterest($date)
    {
        return $this->entity->repo->netInterest($date);
    }

    protected function grossInterest($date)
    {
        return $this->entity->repo->grossInterest($date);
    }

    public function get($column, $arguments = [])
    {
        if (!in_array($column, array_keys($this->columns))) {
            return $this->tryMissing($column);
        }

        $getter = $this->columns[$column];

        if (empty($getter)) {
            return $this->entity{$column};
        }

        if (starts_with($getter, '@')) {
            return $this->getFromMethod($column, $arguments);
        }

        if (str_contains($getter, '.')) {
            return $this->getFromRelation($column);
        }

        return $this->entity->{$column};
    }

    protected function tryMissing($column)
    {
        if (str_contains($column, '.')) {
            return $this->accessRelation($column);
        }

        return $this->entity->{$column};
    }

    protected function getFromMethod($column, $arguments)
    {
        $method = str_replace('@', '', $this->columns[$column]);

        return call_user_func_array(array($this, $method), $arguments);
    }

    protected function getFromRelation($column)
    {
        $getter = $this->columns[$column];

        return $this->accessRelation($getter);
    }

    protected function accessRelation($getter)
    {
        $accessors = explode('.', $getter);

        return collect($accessors)->reduce(function ($current, $carry) {
            return $current->{$carry};
        }, $this->entity);
    }
}
