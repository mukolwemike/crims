<?php
/**
 * Date: 14/03/2018
 * Time: 11:31
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Reporting\Custom\Models;

use App\Cytonn\Reporting\Custom\Models\Formatters\ReportFormatter;

trait ReportableModel
{

    protected $reportFormatterInstance;

    protected function formatReportColumn($column, $format = null)
    {
        if (is_null($format)) {
            return $column;
        }

        return $this->getFormatter()->{'format'.ucfirst(camel_case($format))}($column);
    }

    protected function getFormatter()
    {
        if ($this->reportFormatterInstance) {
            return $this->reportFormatterInstance;
        }

        $formatter = app(ReportFormatter::class);

        if (property_exists($this, 'reportFormatter')) {
            $formatter = app($this->reportFormatter);
        }

        return $this->reportFormatterInstance = $formatter;
    }

    /**
     * @param array $columns
     * @return \Illuminate\Support\Collection
     */
    public function getReportRows($columns = [], $arguments = [], $formats = [], $labels = [])
    {
        if (empty($this->reportPresenter)) {
            throw new \LogicException("Please specify a report presenter for the model ".get_class($this));
        }

        /**
         * @var Presenter
         */
        $presenter = new $this->reportPresenter($this);

        if (!$columns) {
            $columns = array_keys($presenter->columns);
        }

        return collect($columns)->mapWithKeys(function ($column) use ($presenter, $arguments, $labels, $formats) {
            $args = $this->getReportColumnArgs($column, $arguments);

            if (!is_array($args)) {
                $args = [$args];
            }

            $label = $this->getReportColumnArgs($column, $labels);
            $label = $label ? $label : $column;

            $format = $this->getReportColumnArgs($column, $formats);

            $value = $presenter->get($column, $args);

            return [$label => $this->formatReportColumn($value, $format)];
        });
    }

    protected function getReportColumnArgs($column, array $arguments)
    {
        if (!array_key_exists($column, $arguments)) {
            return null;
        }

        return $arguments[$column];
    }

    public function reportPresent()
    {
        return new $this->reportPresenter($this);
    }
}
