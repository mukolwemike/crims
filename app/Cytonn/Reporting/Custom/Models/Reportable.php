<?php
/**
 * Date: 14/03/2018
 * Time: 11:31
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Reporting\Custom\Models;

interface Reportable
{
    public function getReportRows($columns, $arguments, $formats, $labels);
}
