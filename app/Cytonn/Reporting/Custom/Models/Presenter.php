<?php
/**
 * Date: 29/03/2018
 * Time: 11:37
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Reporting\Custom\Models;

use Illuminate\Database\Eloquent\Model;

abstract class Presenter
{
    protected $entity;

    protected $columns = [];

    protected $formats = [];

    protected $arguments = [];

    public function __construct(Model $entity)
    {
        $this->entity = $entity;
    }

    abstract public function get($column, $arguments);
}
