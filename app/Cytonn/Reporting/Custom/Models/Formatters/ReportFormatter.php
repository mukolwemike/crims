<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 13/07/2018
 * Time: 08:44
 */

namespace App\Cytonn\Reporting\Custom\Models\Formatters;

use Carbon\Carbon;

class ReportFormatter
{
    public function formatDateString($date)
    {
        return Carbon::parse($date)->toDateString();
    }

    public function formatDate($date)
    {
        return Carbon::parse($date)->toFormattedDateString();
    }

    public function formatCurrency($amount)
    {
        return number_format($amount, 2);
    }
}
