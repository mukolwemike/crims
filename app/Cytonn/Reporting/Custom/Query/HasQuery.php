<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 16/08/2018
 * Time: 11:06
 */

namespace App\Cytonn\Reporting\Custom\Query;

use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportFilter;
use Illuminate\Database\Eloquent\Builder;

class HasQuery extends Filter
{
    const TYPE = 'has';

    protected $relation;

    protected $operator = '>=';

    protected $count = 1;

    protected $nested;

    /**
     * @return mixed
     */
    public function getRelation()
    {
        return $this->relation;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return Filter
     */
    public function getNested(): Filter
    {
        return $this->nested;
    }

    /**
     * HasQuery constructor.
     * @param $relation
     * @param string $operator
     * @param int $count
     * @param string $boolean
     * @param $nested
     */
    public function __construct(
        $relation,
        string $operator = '>=',
        int $count = 1,
        string $boolean = 'and',
        Filter $nested = null
    ) {
        $this->relation = $relation;
        $this->operator = $operator;
        $this->count = $count;
        $this->boolean = $boolean;
        $this->nested = $nested;
    }

    public function apply(Builder $query)
    {
        $closure = null;

        if ($this->nested) {
            $closure = function ($query) {
                return $this->nested->apply($query);
            };
        }

        return $query->has($this->relation, $this->operator, $this->count, $this->boolean, $closure);
    }

    public function save(Report $report, ReportFilter $parent = null)
    {
        $filter = ReportFilter::create([
            'relation' => $this->relation,
            'operator' => $this->operator,
            'count' => $this->count,
            'boolean' => $this->boolean,
            'type_id' => $this->type()->id
        ]);

        if ($parent) {
            $filter->update(['parent_id' => $parent->id]);
        }

        if ($this->nested) {
            $this->nested->save($report, $filter);
        }
    }
}
