<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 14/08/2018
 * Time: 11:50
 */

namespace App\Cytonn\Reporting\Custom\Query;

use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportFilter;
use App\Cytonn\Models\Reporting\ReportFilterType;
use Illuminate\Database\Eloquent\Builder;

abstract class Filter
{
    const TYPES = ['QUERY', 'SCOPE', 'HAS'];

    protected $boolean = 'and';

    public static function where($field, $operator = '=', $value = null, $boolean = 'and')
    {
        if ($field instanceof Filter || is_array($field)) {
            return new WhereQuery($field);
        }

        if (is_null($value)) {
            return new WhereQuery($field, $operator);
        }

        return new WhereQuery($field, $operator, $value, $boolean);
    }

    public static function has($relation, $operator = '>=', $count = 1, $boolean = 'and', $nested = null)
    {
        return new HasQuery($relation, $operator, $count, $boolean, $nested);
    }

    public static function whereHas($relation, Filter $nested, $boolean = 'and', $operater = '>=', $count = 1)
    {
        return static::has($relation, $operater, $count, $boolean, $nested);
    }

    public static function scope($name, $arguments = null)
    {
        if (is_null($arguments)) {
            $arguments = [];
        }

        if (!is_array($arguments)) {
            $arguments = [$arguments];
        }

        return new ScopeQuery($name, $arguments);
    }

    public static function build(ReportFilter $filter)
    {
        switch ($filter->type->slug) {
            case 'where':
                return static::buildWhere($filter);
                break;
            case 'has':
                return static::buildHas($filter);
                break;
            case 'scope':
                return static::scope($filter->scope_name, $filter->value);
                break;
            default:
                throw new \InvalidArgumentException("Filter type not supported");
        }
    }

    private static function buildWhere(ReportFilter $filter)
    {
        $children = $filter->children;

        if ($children->count() === 0) {
            return static::where($filter->field, $filter->operator, $filter->value, $filter->boolean);
        }

        $nested = $children->map(function (ReportFilter $filter) {
            return static::build($filter);
        });

        return static::where($nested->all());
    }

    private static function buildHas(ReportFilter $filter)
    {
        $children = $filter->children;

        if ($children->count() === 0) {
            return static::has($filter->relation, $filter->operator, $filter->boolean);
        }

        $nested = $children->map(function (ReportFilter $filter) {
            return static::build($filter);
        });

        return static::whereHas($filter->relation, $nested->all(), $filter->boolean, $filter->operator, $filter->count);
    }

    /**
     * @return string
     */
    public function getBoolean(): string
    {
        return $this->boolean;
    }

    protected function type()
    {
        return ReportFilterType::where('slug', static::TYPE)->first();
    }

    abstract public function apply(Builder $query);

    abstract public function save(Report $report, ReportFilter $parent = null);
}
