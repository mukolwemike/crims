<?php
/**
 * Date: 14/03/2018
 * Time: 14:44
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Reporting\Custom\Query;

use App\Cytonn\Reporting\Custom\Query\Filter;
use Cytonn\Reporting\Custom\System\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Filtering
{
    /**
     * @var Builder
     */
    protected $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function apply($filters)
    {
        if ($this->query instanceof Model) {
            $this->query = $this->query->newQuery();
        }

        foreach ($filters as $filter) {
            $this->applyFilter($filter);
        }

        return $this->query;
    }

    protected function applyFilter(Filter $filter)
    {
        $this->query =  $filter->apply($this->query);
    }
}
