<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 16/08/2018
 * Time: 11:01
 */

namespace App\Cytonn\Reporting\Custom\Query;

use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportFilter;
use Illuminate\Database\Eloquent\Builder;

class WhereQuery extends Filter
{
    const TYPE = 'where';

    protected $field;

    protected $operator = '=';

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    protected $value = null;

    /**
     * WhereQuery constructor.
     * @param $field
     * @param string $boolean
     * @param string $operator
     * @param null $value
     */
    public function __construct($field, string $operator = '=', $value = null, string $boolean = 'and')
    {
        $this->field = $field;
        $this->boolean = $boolean;
        $this->operator = $operator;
        $this->value = $value;
    }

    public function apply(Builder $query)
    {
        if ($this->field instanceof Filter || is_array($this->field)) {
            return $this->applyNested($query, $this->field);
        }

        return $query->where($this->field, $this->operator, $this->value, $this->boolean);
    }

    protected function applyNested(Builder $query, $filters)
    {
        if (!is_array($filters)) {
            $filters = [$filters];
        }

        return $query->where(function ($q) use ($filters) {
            foreach ($filters as $filter) {
                $q = $filter->apply($q);
            }

            return $q;
        }, $this->operator, $this->value, $this->boolean);
    }

    public function save(Report $report, ReportFilter $parent = null)
    {
        $field = $this->field;

        if ($this->field instanceof Filter || is_array($this->field)) {
            $field = null;
        }

        $filter =  ReportFilter::create([
            'field' => $field,
            'boolean' => $this->boolean,
            'operator' => $this->operator,
            'value' => $this->value,
            'report_id' => $report->id,
            'type_id' => $this->type()->id
        ]);

        if ($parent) {
            $filter->update(['parent_id' => $parent->id]);
        }

        $this->saveNested($report, $filter);
    }

    protected function saveNested(Report $report, ReportFilter $parent)
    {
        $nestedFilters = [];

        if ($this->field instanceof Filter) {
            $nestedFilters = [$this->field];
        }

        if (is_array($this->field)) {
            $nestedFilters = $this->field;
        }

        foreach ($nestedFilters as $nestedFilter) {
            $nestedFilter->save($report, $parent);
        }
    }
}
