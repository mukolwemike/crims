<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 16/08/2018
 * Time: 11:02
 */

namespace App\Cytonn\Reporting\Custom\Query;

use App\Cytonn\Models\Reporting\Report;
use App\Cytonn\Models\Reporting\ReportFilter;
use Illuminate\Database\Eloquent\Builder;

class ScopeQuery extends Filter
{
    const TYPE = 'scope';

    protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    protected $arguments = [];

    /**
     * ScopeQuery constructor.
     * @param $name
     * @param array $arguments
     */
    public function __construct($name, array $arguments = [])
    {
        $this->name = $name;

        $this->arguments = $arguments;
    }

    public function apply(Builder $query)
    {
        return $query->{$this->name}(...$this->arguments);
    }

    public function save(Report $report, ReportFilter $parent = null)
    {
        return ReportFilter::create([
            'scope_name' => $this->name,
            'value' => serialize($this->arguments),
            'type_id' => $this->type()->id
        ]);
    }
}
