<?php
/**
 * Date: 14/03/2018
 * Time: 11:28
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Reporting\Custom;

use App\Cytonn\Reporting\Custom\Persistence\Save;
use App\Cytonn\Reporting\Custom\Query\Filter;
use Cytonn\Reporting\Custom\Models\Reportable;
use Cytonn\Reporting\Custom\Query\Filtering;
use Illuminate\Database\Eloquent\Builder;

class Report
{
    protected $model;

    /**
     * @var array
     */
    protected $relations;

    protected $filters = [];


    protected $arguments = [];

    protected $labels = [];

    protected $columns = [];

    protected $formats = [];

    /**
     * @var Builder
     */
    protected $query;

    /**
     * Report constructor.
     * @param $model
     * @param array $relations
     * @param array $columns
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    public function filter(Filter $filter)
    {
        array_push($this->filters, $filter);

        return $this;
    }

    public function with($relations)
    {
        if (!is_array($relations)) {
            $relations = [$relations];
        }

        $this->relations = $relations;

        return $this;
    }

    public function run()
    {
        $model =  app($this->model);

        if (!$model instanceof Reportable) {
            throw new \InvalidArgumentException("The model must be an instance of reportable");
        }

        if (count($this->relations)) {
            $relations =array_filter($this->relations, function ($arr) {
                return $arr;
            });

            $model = $model->with($relations);
        }

        $filter = new Filtering($model);
        $this->query = $filter->apply($this->filters);

        return $this;
    }

    public function get()
    {
        if (!$this->query) {
            $this->run();
        }

        return $this->query->get();
    }

    public function export()
    {
        $rows = $this->get();

        $data = $rows->map(function (Reportable $row) {
            return (object) $row->getReportRows(
                $this->columns,
                $this->arguments,
                $this->formats,
                $this->labels
            )->all();
        });

        return $data;
    }

    public function count()
    {
        return $this->query->count();
    }

    public function save()
    {
        $save = new Save();

        return $save->persist($this);
    }

    public function arguments(array $args = [])
    {
        $this->arguments = $args;
        return $this;
    }

    public function labels(array $labels = [])
    {
        $this->labels = $labels;
        return $this;
    }

    public function columns(array $columns = [])
    {
        $this->columns = $columns;
        return $this;
    }

    public function format(array $formats = [])
    {
        $this->formats = $formats;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return array
     */
    public function getRelations(): array
    {
        return $this->relations;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @return array
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getFormats(): array
    {
        return $this->formats;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }
}
