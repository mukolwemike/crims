<?php


namespace Cytonn\Reporting;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\InterestAction;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;

class InterestPaymentExcelGenerator
{
    /**
     * @var Carbon
     */
    protected $date;


    /**
     * @param null $interestDate
     */
    public function __construct($interestDate = null)
    {
        is_null($interestDate) ? $this->date = Carbon::today() : $this->date = new Carbon($interestDate);
    }


    public function excel(Carbon $startDate, Carbon $endDate, $filtered_grouped_by_interest_action)
    {
        $filtered_group = $filtered_grouped_by_interest_action;

        $file = \Excel::create(
            'Interest Payments Worksheet - ' . $startDate->toFormattedDateString() . ' to ' .
            $endDate->toFormattedDateString(),
            function ($excel) use ($startDate, $endDate, $filtered_group) {
                foreach ($filtered_group as $interest_action_id => $interest_payment_schedules_arr) {
                    $sheet_name = InterestAction::findOrFail($interest_action_id)->name;
                    $excel->sheet(
                        $sheet_name,
                        function ($sheet) use ($startDate, $endDate, $interest_payment_schedules_arr) {
                            $interest_payment_schedules_coll = $interest_payment_schedules_arr->map(
                                function ($payment) {
                                    $p = new EmptyModel();
                                    $p->ClientCode = $payment->investment->client->client_code;
                                    $p->Name = ClientPresenter::presentFullNames($payment->investment->client->id);
                                    $p->Principal = $payment->investment->amount;
                                    $p->InterestRate = $payment->investment->interest_rate . '%';
                                    $p->ValueDate = $payment->investment->maturity_date;
                                    $p->MaturityDate = $payment->investment->invested_date;
                                    $p->PaymentDescription = $payment->description;

                                    $intervals = [
                                        null => 'On Maturity',
                                        1 => 'Monthly',
                                        3 => 'Quarterly',
                                        6 => 'Semi anually',
                                        12 => 'Annually'
                                    ];
                                    $interval = $payment->investment->interest_payment_interval;
                                    try {
                                        $name = $intervals[$interval];
                                    } catch (\Exception $e) {
                                        $name = 'On Maturity';
                                    }
                                    $p->PaymentFrequency = $name;

                                    $last_payment =
                                        $payment->investment->payments()->orderBy('date_paid', 'DESC')->first();

                                    if (is_null($last_payment)) {
                                        $last_payment_date = null;
                                    } else {
                                        $last_payment_date = $last_payment->date_paid;
                                    }
                                    $p->LastPaymentDate = $last_payment_date;
                                    $p->PaymentDate = $payment->date;
                                    $gross_interest = $payment->investment->getCurrentGrossInterestAsAtEndOfDay(
                                        $payment->investment->amount,
                                        $payment->investment->interest_rate,
                                        $payment->investment->invested_date,
                                        $payment->investment->maturity_date,
                                        $this->date
                                    );
                                    $p->PaymentAmount = $payment->investment->getNetInterest(
                                        $gross_interest,
                                        $payment->investment->client->taxable
                                    ) -
                                        $payment->investment->repo->getTotalPayments();
                                    $p->Bank = $payment->investment->bankDetails()->bankName();
                                    $p->Branch = $payment->investment->bankDetails()->branch();
                                    $p->AccountName = $payment->investment->bankDetails()->accountName();
                                    $p->AccountNumber = $payment->investment->bankDetails()->accountNumber();
                                    $p->ClearingCode = $payment->investment->bankDetails()->clearingCode();
                                    $p->SwiftCode = $payment->investment->bankDetails()->swiftCode();

                                    return $p;
                                }
                            );

                            $sheet->fromModel($interest_payment_schedules_coll);
                            $sheet->prependRow([$startDate->formatLocalized('%A %d %B %Y') . ' - ' .
                                $endDate->formatLocalized('%A %d %B %Y')]);
                            $sheet->prependRow(['Interest payment schedule - Interest Calculated as at ' .
                                $this->date->toFormattedDateString()]);
                            $sheet->mergeCells('A1:E1');
                            $sheet->mergeCells('A2:E2');
                        }
                    );
                }
            }
        );

        return $file;
    }
}
