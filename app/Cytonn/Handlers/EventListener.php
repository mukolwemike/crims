<?php
/**
 * Date: 03/05/2017
 * Time: 18:50
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Handlers;

use ReflectionClass;

abstract class EventListener
{
    /**
     * Figure out what the name of the class is.
     *
     * @param  $event
     * @return string
     */
    protected function getEventName($event)
    {
        $event = str_replace('.', '\\', $event);

        return (new ReflectionClass($event))->getShortName();
    }

    /**
     * Handle the event
     *
     * @param  $event
     * @return mixed
     */
    public function handle($event, $args)
    {
        $eventName = $this->getEventName($event);

        if ($this->listenerIsRegistered($eventName)) {
            return call_user_func_array([$this, 'when'.$eventName], $args);
        }
    }

    /**
     * Determine if a method in the subclass is registered
     * for this particular event.
     *
     * @param  $eventName
     * @return bool
     */
    protected function listenerIsRegistered($eventName)
    {
        $method = "when{$eventName}";

        return method_exists($this, $method);
    }
}
