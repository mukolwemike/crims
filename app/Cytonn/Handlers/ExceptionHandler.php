<?php
/**
 * Date: 01/10/2016
 * Time: 00:20
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Handlers;

use App\Cytonn\Models\Behaviours\LockException;
use App\Exceptions\Handler;
use Exception;
use \Flash;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Laracasts\Validation\FormValidationException;
use Illuminate\Support\Facades\Input;
use SSOManager\Exceptions\InvalidStateException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExceptionHandler
 *
 * @package Cytonn\Handlers
 */
class ExceptionHandler extends Handler
{
    public function reportException(Exception $exception)
    {
        return parent::report($exception);
    }

    public function renderKnownClasses($request, $e)
    {
        if ($e instanceof FormValidationException) {
            Flash::error('There are errors in the form');

            return redirect()->back()->withInput()->withErrors($e->getErrors());
        }

        if ($e instanceof TokenMismatchException) {
            Flash::error('Couldn\'t execute that, please try again');

            if ($request->wantsJson()) {
                return \response()->json(['error' => 'Unknown general error, try again', 'title'=>'token_mismatch']);
            }

            return redirect()->back()->withInput($request->except('_token'));
        }

        if ($e instanceof InvalidStateException) {
            Flash::error('Couldn\'t execute that, please try again');

            if ($request->wantsJson()) {
                return \response()->json(['error' => 'Unknown general error, try again', 'title'=>'token_mismatch']);
            }

            return redirect('/sso/authorize');
        }

        if ($e instanceof ModelNotFoundException) {
            $model = $e->getModel();

            $msg = "The item ($model) was not found";

            if (request()->wantsJson()) {
                return response()->json(['error' => $msg, 'title' => 'model not found'], Response::HTTP_NOT_FOUND);
            }

            Flash::error($msg);

            return take_back($e);
        }

        if ($e instanceof AuthenticationException) {
            Flash::error('Please login to proceed.');

            if ($request->expectsJson()) {
                return response()->json(['error' => 'Unauthenticated.'], 401);
            } else {
                return redirect('/sso/authorize');
            }
        }

        if ($e instanceof LockException) {
            $msg = 'Couldn\'t obtain lock for action, similar transaction may be running. Try again in a minute';

            Flash::warning($msg);

            if ($request->wantsJson()) {
                return \response()->json(['error' => $msg, 'title'=>'lock_failed']);
            }

            return redirect()->back()->withInput($request->all());
        }

        return false;
    }
}
