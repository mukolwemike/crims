<?php
/**
 * Date: 05/12/2015
 * Time: 11:35 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Handlers;

use Cytonn\Notifier\NotificationRepository;
use Cytonn\Investment\Events\PortfolioTransactionApprovalRequested;
use Cytonn\Investment\Events\ClientTransactionApprovalRequested;

/**
 * Class NotificationHandler
 *
 * @package Cytonn\Handlers
 */
class NotificationHandler extends EventListener
{
    /**
     * Create a notification when approval is requested
     *
     * @param ClientTransactionApprovalRequested $event
     */
    public function whenClientTransactionApprovalRequested(ClientTransactionApprovalRequested $event)
    {
        (new NotificationRepository())->createNotificationForNewClientTransactionApproval($event->request);
    }

    /**
     * Create a notification when approval is requested
     *
     * @param PortfolioTransactionApprovalRequested $event
     */
    public function whenPortfolioTransactionApprovalRequested(PortfolioTransactionApprovalRequested $event)
    {
        (new NotificationRepository())->createNotificationForNewPortfolioTransactionApproval($event->request);
    }
}
