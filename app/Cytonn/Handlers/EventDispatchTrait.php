<?php
/**
 * Date: 03/05/2017
 * Time: 23:30
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Handlers;

use Laracasts\Commander\Events\Contracts\Dispatcher;

trait EventDispatchTrait
{
    /**
     * The Dispatcher instance.
     *
     * @var Dispatcher
     */
    protected $eventDispatcher;

    /**
     * Dispatch all events for an entity.
     *
     * @param object $entity
     */
    public function dispatchEventsFor($entity)
    {
        return $this->getLaracastsEventDispatcher()->dispatch($entity->releaseEvents());
    }

    /**
     * Get the event dispatcher.
     *
     * @return Dispatcher
     */
    public function getLaracastsEventDispatcher()
    {
        return $this->eventDispatcher ?: app('Laracasts\Commander\Events\EventDispatcher');
    }
}
