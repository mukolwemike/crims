<?php
/**
 * Date: 05/12/2015
 * Time: 11:38 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Handlers;

use App\Cytonn\Models\ClientInvestment;
use Cytonn\Authentication\Events\UserHasBeenAuthenticated;
use Cytonn\Authentication\Events\UserHasLoggedIn;
use Cytonn\Authentication\Events\UserRegistered;
use Cytonn\Investment\Events\ClientInvestmentHasBeenAdded;
use Cytonn\Investment\Events\CombinedInvestmentsRolledOver;
use Cytonn\Investment\Events\InterestHasBeenReinvested;
use Cytonn\Investment\Events\InvestmentHasBeenConfirmed;
use Cytonn\Investment\Events\InvestmentHasBeenEdited;
use Cytonn\Investment\Events\InvestmentHasBeenRolledOver;
use Cytonn\Investment\Events\InvestmentHasBeenToppedUp;
use Cytonn\Investment\Events\InvestmentHasBeenTransferred;
use Cytonn\Investment\Events\InvestmentHasBeenWithdrawn;
use Cytonn\Investment\Events\NewInvestmentAdded;
use Cytonn\Logger\Logger;
use Cytonn\Presenters\ClientPresenter;

/**
 * Class LogHandler
 *
 * @package Cytonn\Handlers
 */
class LogHandler extends EventListener
{
    /**
     * @var Logger
     */
    private $logger;

    protected $user;

    /**
     * LogHandler constructor.
     *
     * @param $logger
     */
    public function __construct()
    {
        $this->logger = new Logger();

        if (\Auth::user()) {
            $this->user = \Auth::user()->id;
        } else {
            $this->user = null;
        }
    }

    /**
     * @param UserRegistered $event
     */
    public function whenUserRegistered(UserRegistered $event)
    {
    }

    /**
     * @param UserHasLoggedIn $event
     */
    public function whenUserHasLoggedIn(UserHasLoggedIn $event)
    {
    }

    /**
     * @param ClientInvestmentHasBeenAdded $event
     */
    public function whenClientInvestmentHasBeenAdded(ClientInvestmentHasBeenAdded $event)
    {
    }

    /**
     * @param InvestmentHasBeenConfirmed $event
     */
    public function whenInvestmentHasBeenConfirmed(InvestmentHasBeenConfirmed $event)
    {
        $investment = $event->investment;

        $this->logger
            ->addActivityLog(
                'Business confirmation for ' . ClientPresenter::presentFullNames($investment->client_id) .
                "'s investment was sent",
                '/dashboard/investments/clientinvestments/' . $investment->id,
                $this->user,
                $investment->client_id,
                get_class($investment),
                $investment->product->fund_manager_id
            );
    }

    /**
     * @param NewInvestmentAdded $event
     */
    public function whenNewInvestmentAdded(NewInvestmentAdded $event)
    {
        $investment = $event->investment;

        $this->logger->addActivityLog(
            'New Investment for ' . ClientPresenter::presentFullNames($investment->client_id) . ' was added',
            '/dashboard/investments/clientinvestments/' . $investment->id,
            $this->user,
            $investment->client_id,
            get_class($investment),
            $investment->product->fund_manager_id
        );
    }

    /**
     * @param InvestmentHasBeenRolledOver $event
     */
    public function whenInvestmentHasBeenRolledOver(InvestmentHasBeenRolledOver $event)
    {
        $investment = $event->rollover->client;

        $this->logger->addActivityLog(
            'Rollover for ' . ClientPresenter::presentFullNames($investment->client_id) . ' was added',
            '/dashboard/investments/clientinvestments/' . $investment->id,
            $this->user,
            $investment->client_id,
            get_class($investment),
            $investment->product->fund_manager_id
        );
    }

    /**
     * @param CombinedInvestmentsRolledOver $event
     */
    public function whenCombinedInvestmentsRolledOver(CombinedInvestmentsRolledOver $event)
    {
        $investment = $event->rollover->client;

        $this->logger->addActivityLog(
            'Combined Investment Rollover for ' . ClientPresenter::presentFullNames($investment->client_id) .
            ' was added',
            '/dashboard/investments/clientinvestments/' . $investment->id,
            $this->user,
            $investment->client_id,
            get_class($investment),
            $investment->product->fund_manager_id
        );
    }

    /**
     * @param InvestmentHasBeenWithdrawn $event
     */
    public function whenInvestmentHasBeenWithdrawn(InvestmentHasBeenWithdrawn $event)
    {
        $investment = $event->withdrawal->client;

        $this->logger->addActivityLog(
            ClientPresenter::presentFullNames($investment->client_id) . "'s investment withdrawn",
            '/dashboard/investments/clientinvestments/' . $investment->id,
            $this->user,
            $investment->client_id,
            get_class($investment),
            $investment->product->fund_manager_id
        );
    }

    /**
     * @param InvestmentHasBeenEdited $event
     */
    public function whenInvestmentHasBeenEdited(InvestmentHasBeenEdited $event)
    {
        $investment = $event->investment;

        $this->logger->addActivityLog(
            ClientPresenter::presentFullNames($investment->client_id) . "'s investment was edited",
            '/dashboard/investments/clientinvestments/' . $investment->id,
            $this->user,
            $investment->client_id,
            get_class($investment),
            $investment->product->fund_manager_id
        );
    }

    /**
     * @param InvestmentHasBeenToppedUp $event
     */
    public function whenInvestmentHasBeenToppedUp(InvestmentHasBeenToppedUp $event)
    {
        $investment = $event->topup->client;

        $this->logger->addActivityLog(
            'A topup for ' . ClientPresenter::presentFullNames($investment->client_id) . ' was added',
            '/dashboard/investments/clientinvestments/' . $investment->id,
            $this->user,
            $investment->client_id,
            get_class($investment),
            $investment->product->fund_manager_id
        );
    }

    public function whenInterestHasBeenReinvested(InterestHasBeenReinvested $event)
    {
        $reinvestment = $event->reinvestment;

        $this->logger->addActivityLog(
            'A reinvestment for ' . ClientPresenter::presentFullNames($reinvestment->client_id) .
            ' was added',
            '/dashboard/investments/clientinvestments/' . $reinvestment->id,
            $this->user,
            $reinvestment->client_id,
            get_class($reinvestment),
            $reinvestment->product->fund_manager_id
        );
    }

    /**
     * @param UserHasBeenAuthenticated $event
     */
    public function whenUserHasBeenAuthenticated(UserHasBeenAuthenticated $event)
    {
    }

    /**
     * @param InvestmentHasBeenTransferred $event
     */
    public function whenInvestmentHasBeenTransferred(InvestmentHasBeenTransferred $event)
    {
        $transfer = $event->transfer;

        $text = 'Investment was transferred from ' .
            ClientPresenter::presentJointFirstNameLastName($transfer->source_id) . ' to ' .
            ClientPresenter::presentJointFirstNameLastName($transfer->destination_id);

        $this->logger->addActivityLog(
            $text,
            '/dashboard/investments/approve/' . $transfer->approval_id,
            $this->user,
            $transfer->investment_id,
            ClientInvestment::class,
            ClientInvestment::find($transfer->investment_id)->product->fund_manager_id
        );
    }
}
