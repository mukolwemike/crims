<?php
/**
 * Date: 8/21/15
 * Time: 2:28 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Handlers;

use Cytonn\Authentication\Events\UserHasBeenAuthenticated;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Logger\Logger;
use Cytonn\Notifier\NotificationRepository;
use Cytonn\Notifier\Transactions\ClientTransactionNotifier;
use Cytonn\Users\SessionRepository;
use Laracasts\Commander\CommanderTrait;
use Cytonn\Handlers\EventListener;
use Cytonn\Mailers\UserMailer;
use Cytonn\Authentication\Events\UserRegistered;
use Cytonn\Authentication\Events\UserHasLoggedIn;
use Cytonn\Investment\Events\TransactionApproved;
use Cytonn\Portfolio\Events\PortfolioTransactionApproved;

/**
 * Class EventHandler
 *
 * @package Cytonn\Handlers
 */
class EventHandler extends EventListener
{
    use CommanderTrait;

    /**
     * @param UserRegistered $event
     */
    public function whenUserRegistered(UserRegistered $event)
    {
        $mailer = new UserMailer();

        $mailer->sendActivationEmail($event->user);
    }

    /**
     * @param UserHasLoggedIn $event
     */
    public function whenUserHasLoggedIn(UserHasLoggedIn $event)
    {
        $mailer = new UserMailer();

        $mailer->sendOTPToUser($event->user, $event->otp);
    }

    /**
     * @param UserHasBeenAuthenticated $event
     */
    public function whenUserHasBeenAuthenticated(UserHasBeenAuthenticated $event)
    {
        (new SessionRepository())->swapSession($event->user);

        (new Logger())->logAuthenticated();
    }

    /**
     *  Set notification as read when transaction is approved
     *
     * @param TransactionApproved $event
     */
    public function whenTransactionApproved(TransactionApproved $event)
    {
        (new NotificationRepository())->setClientApprovalNotificationPermanentlyRead($event->transaction);
    }

    public function whenApprovalSuccessful($approvalSuccessful)
    {
        if ($approvalSuccessful instanceof ApprovalSuccessful) {
            $notifier = new ClientTransactionNotifier();

            $notifier->build($approvalSuccessful->approval);
        }
    }

    /**
     * Set notification as read when transaction is approved
     *
     * @param PortfolioTransactionApproved $event
     */
    public function whenPortfolioTransactionApproved(PortfolioTransactionApproved $event)
    {
        (new NotificationRepository())->setPortfolioApprovalNotificationPermanentlyRead($event->transaction);
    }
}
