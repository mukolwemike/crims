<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\PaymentSystem;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\PaymentSystem\PaymentTransactionTrait;
use App\Cytonn\PaymentSystem\ProcessTransfer;
use App\Cytonn\PaymentSystem\ProcessUnitFundInvestments;
use Cytonn\Exceptions\CRIMSGeneralException;
use SMS;

class CreditTransactionProcessor
{
    use PaymentTransactionTrait;

    protected $transactionData;

    protected $custodialAccount;

    protected $client = null;

    protected $matchType = null;

    protected $fundDepositCode = null;

    /**
     * CreditTransactionProcessor constructor.
     * @param $transactionData
     * @param CustodialAccount $custodialAccount
     */
    public function __construct($transactionData, CustodialAccount $custodialAccount)
    {
        $this->transactionData = $transactionData;

        $this->custodialAccount = $custodialAccount;
    }

    /**
     * @throws CRIMSGeneralException
     * @throws \Throwable
     */
    public function process()
    {
        if ($this->checkPaymentToDestination($this->custodialAccount)) {
            $this->processPortfolio();

            return;
        }

        $this->processClient();

        return;
        //TODO handle deposit recall/portfolio withdrawal
    }

    /**
     * @throws \Throwable
     */
    protected function processClient()
    {
        //Get Client
        $clientProcessor = new ProcessClient($this->transactionData);

        $clientProcessor->process();

        $this->client = $clientProcessor->getClient();

        $this->fundDepositCode = $clientProcessor->getFundDepositCode();

        $suspenseTransaction = is_null($this->client) ? $this->checkSuspenseTransaction() : null;

        if ($suspenseTransaction) {
            $this->transfer($suspenseTransaction);

            return;
        }

        $this->matchType = $clientProcessor->getMatchType();

        //Add Inflow
        $inflowProcessor = new ProcessInflow(
            $this->transactionData,
            $this->custodialAccount,
            $this->matchType,
            $this->client
        );

        $inflowProcessor->process();


        //Exempt BANK PHONE, match type phone
        if (is_null($this->client) || $this->matchType == 'BANK_PHONE') {
            return;
        }

        //Invest
        $this->processInvestments();

        $this->sendAcknowledgement();
    }

    protected function processPortfolio()
    {
        $suspenseTransaction = is_null($this->client) ? $this->checkSuspenseTransaction() : null;

        if ($suspenseTransaction) {
            $this->transfer($suspenseTransaction);

            return;
        }

        $this->saveSuspenseTransaction();
    }

    /**
     * @param $suspenseTransaction
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    protected function transfer($suspenseTransaction)
    {
        $transferProcessor = new ProcessTransfer(
            $this->transactionData,
            $this->custodialAccount,
            $suspenseTransaction
        );

        $transferProcessor->process();
    }

    /**
     * @return SuspenseTransaction|\Illuminate\Database\Eloquent\Model|null
     */
    private function checkSuspenseTransaction()
    {
        $bankReference = $this->getBankTransactionReference($this->transactionData->transaction_id);

        return SuspenseTransaction::where('bank_reference', $bankReference)->where('matched', 0)
            ->where('amount', -1 * $this->transactionData->amount)->first();
    }

    private function sendAcknowledgement()
    {
        $phoneNumber = $this->transactionData->source_number;

        if (isEmptyOrNull($phoneNumber)) {
            return;
        }

        $account =
            $this->client ?
                $account = $this->client->client_code . ' (' . str_limit($this->client->present()->fullName, 100) . ')'
                : null;

        $accName = $account ? 'account ' . $account : 'an unknown account';

        $message = 'This is to confirm receipt of ' . $this->custodialAccount->currency->code . ' '
            . $this->transactionData->amount . ' in ' . $this->getPaymentToName() .
            ' for ' . $accName .
            '. We shall send a business confirmation on the investment. Call us on 0709101200. ' .
            'CMMF, a wallet that you stand to earn upto 11% p.a.';

        SMS::send($phoneNumber, $message);
    }

    /**
     * @return mixed|string
     */
    private function getPaymentToName()
    {
        //TODO change to presenter
        if (!is_null($this->custodialAccount->product_id)) {
            return Product::findOrFail($this->custodialAccount->product_id)->name;
        } elseif (!is_null($this->custodialAccount->project_id)) {
            return Project::find($this->custodialAccount->project_id)->name;
        } elseif (!is_null($this->custodialAccount->entity_id)) {
            return SharesEntity::find($this->custodialAccount->entity_id)->name;
        } elseif (!is_null($this->custodialAccount->unit_fund_id)) {
            if ($this->fundDepositCode) {
                return UnitFund::where('deposit_code', $this->fundDepositCode)->firstOrFail()->name;
            } else {
                return UnitFund::find($this->custodialAccount->unit_fund_id)->name;
            }
        } else {
            return '';
        }
    }

    /**
     * @throws CRIMSGeneralException
     */
    private function processInvestments()
    {
        if (!is_null($this->custodialAccount->product_id)) {
            //TODO Add processor for CHYS
        } elseif (!is_null($this->custodialAccount->project_id)) {
            //TODO Add processor for Real Estate
        } elseif (!is_null($this->custodialAccount->entity_id)) {
            //TODO add processor for shares
        } elseif (!is_null($this->custodialAccount->unit_fund_id)) {

            if ($this->fundDepositCode) {
                $fund = UnitFund::where('deposit_code', $this->fundDepositCode)->firstOrFail();
            } else {
                $fund = UnitFund::findOrFail($this->custodialAccount->unit_fund_id);
            }

            $unitFundProcessor = new ProcessUnitFundInvestments($this->transactionData, $this->client, $fund);

            $unitFundProcessor->process();
        } else {
            return;
        }
    }
}
