<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\PaymentSystem;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Mailers\Unitization\BusinessConfirmationMailer;

class ProcessUnitFundInvestments
{
    use PaymentTransactionTrait;

    protected $transactionData;

    protected $unitFund;

    protected $client;

    protected $instruction;

    protected $approval;

    protected $unitFundPurchase;

    /**
     * ProcessUnitFundInvestments constructor.
     * @param $transactionData
     * @param Client $client
     * @param UnitFund $unitFund
     */
    public function __construct($transactionData, Client $client, UnitFund $unitFund)
    {
        $this->transactionData = $transactionData;

        $this->unitFund = $unitFund;

        $this->client = $client;
    }

    /**
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     * @throws \Exception
     */
    public function process()
    {
        $this->createUnitFundInstruction();

        $this->createUnitFundInvestmentApproval();

        $this->approveClientTransaction();
    }

    /**
     * @return \App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction
     */
    private function createUnitFundInstruction()
    {
        $input = [
            'date' => $this->getDateToInvest($this->transactionData->date),
            'amount' => $this->transactionData->amount
        ];

        $input['channel'] = 'mpesa';

        $this->instruction = (new UnitFundInstructionsRepository())->purchase($input, $this->client, $this->unitFund);

        return $this->instruction;
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    private function createUnitFundInvestmentApproval()
    {
        $recipient = $this->getUnitFundCommissionRecipient($this->client);

        $input = [
            'amount' => $this->instruction->amount,
            'client_id' => $this->client->id,
            'date' => $this->instruction->date,
            'description' => 'Purchase of Units for Ksh. ' . $this->instruction->amount,
            'fees' => 1,
            'instruction_id' => $this->instruction->id,
            'units' => 0,
            'recipient' => $recipient ? $recipient->id : null
        ];

        $this->approval = ClientTransactionApproval::create([
            'client_id' => $this->client->id,
            'transaction_type' => 'unit_fund_investment',
            'payload' => $input,
            'sent_by' => getSystemUser()->id,
            'approved' => false,
            'approved_by' => null,
            'approved_on' => null,
            'awaiting_stage_id' => 6
        ]);

        return $this->approval;
    }

    private function setUnitFundPurchase()
    {
        $this->unitFundPurchase = UnitFundPurchase::where('approval_id', $this->approval->id)
            ->first();
    }

    /**
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    private function sendBusinessConfirmation()
    {
        (new BusinessConfirmationMailer())->sendBC($this->unitFundPurchase);
    }

    /**
     * @param $date
     * @return Carbon
     */
    private function getDateToInvest($date)
    {
        $date = Carbon::parse($date)->addDay();

        return $date->isWeekend() ? $date->addWeek()->startOfWeek() : $date;
    }

    /**
     * @param Client $client
     * @return |null
     */
    private function getUnitFundCommissionRecipient(Client $client)
    {
        $latestPurchase = $client->unitFundPurchases()
            ->whereHas('unitFundCommission', function ($c) {
                $c->whereNotNull('commission_recipient_id');
            })->latest('date')
            ->first();

        $recipient =  $latestPurchase ? $latestPurchase->unitFundCommission->recipient : null;

        if ($recipient) {
            return $recipient;
        }

        $recipient = $client->getLatestFa();

        if ($recipient) {
            return $recipient;
        }

        return CommissionRecepient::where('email', 'operations@cytonn.com')
            ->where('active', 1)
            ->first();
    }
}
