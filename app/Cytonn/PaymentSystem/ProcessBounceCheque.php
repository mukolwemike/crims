<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\PaymentSystem;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\PaymentSystem\PaymentTransactionTrait;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;

class ProcessBounceCheque
{
    use PaymentTransactionTrait;

    protected $transactionData;

    protected $transaction;

    protected $approval;

    /**
     * ProcessFees constructor.
     * @param $transactionData
     * @param CustodialAccount $custodialAccount
     */
    public function __construct($transactionData, CustodialTransaction $transaction)
    {
        $this->transactionData = $transactionData;

        $this->transaction = $transaction;
    }

    /**
     * @throws \Exception
     */
    public function process()
    {
        $this->saveBounceChequeApproval();

        $this->approveClientTransaction();
    }

    /**
     * @return PortfolioTransactionApproval|\Illuminate\Database\Eloquent\Model
     */
    private function saveBounceChequeApproval()
    {
        $data = [
            'date' => Carbon::parse($this->transactionData->date)->toDateString(),
            'transaction_id' => $this->transaction->id
        ];

        $this->approval = ClientTransactionApproval::create([
            'client_id' => $this->transaction->client_id,
            'transaction_type' => 'add_cheque_as_bounced',
            'payload' => $data,
            'sent_by' => getSystemUser()->id,
            'approved' => false,
            'approved_by' => null,
            'approved_on' => null,
            'awaiting_stage_id' => 6
        ]);

        return $this->approval;
    }
}
