<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\PaymentSystem;

use App\Cytonn\PaymentSystem\PaymentTransactionTrait;
use Cytonn\Exceptions\CRIMSGeneralException;

class PaymentTransactionProcessor
{
    use PaymentTransactionTrait;

    protected $transactionData;

    protected $transactionType; // DEBIT or CREDIT

    protected $custodialAccount = null;

    public function __construct($data)
    {
        $this->transactionData = $data;

        $this->transactionType = $this->transactionData->amount > 0 ? 'CREDIT' : 'DEBIT';
    }

    /**
     * @throws CRIMSGeneralException
     * @throws \Throwable
     */
    public function process()
    {
        $this->custodialAccount = $this->getCustodialAccount(
            $this->transactionData->account_no,
            $this->transactionData->bank_swift
        );

        if (is_null($this->custodialAccount)) {
            throw new CRIMSGeneralException("Could not locate account number :  "
                . $this->transactionData->account_no);
        }

        if ($this->transactionType == 'DEBIT') {
            (new DebitTransactionProcessor($this->transactionData, $this->custodialAccount))->process();

            return;
        }

        (new CreditTransactionProcessor($this->transactionData, $this->custodialAccount))->process();
    }
}
