<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\PaymentSystem;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Handlers\AddInflowFromTransaction;
use Cytonn\Portfolio\Approvals\Engine\Approval;

class ProcessTransfer
{
    protected $transactionData;

    protected $custodialAccount;

    protected $suspenseTransaction;

    protected $approval;

    /**
     * ProcessTransfer constructor.
     * @param $transactionData
     * @param CustodialAccount $custodialAccount
     * @param SuspenseTransaction $suspenseTransaction
     */
    public function __construct(
        $transactionData,
        CustodialAccount $custodialAccount,
        SuspenseTransaction $suspenseTransaction
    ) {
        $this->transactionData = $transactionData;

        $this->custodialAccount = $custodialAccount;

        $this->suspenseTransaction = $suspenseTransaction;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function process()
    {
        $this->saveCustodialTransferApproval();

        $this->approveTransaction();

        $transactions = $this->updateCustodialTransactions();

        $this->updateSuspenseTransaction();

        if ($transactions['in']) {
            $this->createClientInflow($transactions['in']);
        }
    }

    /**
     * @param CustodialTransaction $transaction
     * @throws \App\Exceptions\CrimsException
     */
    private function createClientInflow(CustodialTransaction $transaction)
    {
        $account = $transaction->custodialAccount;

        $hasP =
            $account->product_id ||
            $account->project_id ||
            $account->unit_fund_id ||
            $account->entity_id;

        if (!$hasP) {
            return;
        }

        $approval = AddInflowFromTransaction::create(
            null,
            null,
            getSystemUser(),
            ['transaction_id' => $transaction->id]
        );

        $approval->systemExecute();
    }

    private function approveTransaction()
    {
        $handler = $this->approval->handler();

        $handler->handle($this->approval);

        $this->approval->approve(User::find(1));
    }

    /**
     * @return PortfolioTransactionApproval
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    private function saveCustodialTransferApproval()
    {
        $data = [
            'amount' => abs($this->suspenseTransaction->amount),
            'credit_amount' => $this->transactionData->amount,
            'transfer_date' => Carbon::parse($this->transactionData->date)->toDateString(),
            'effective_rate' => 1 / $this->transactionData->exchange_rate,
            'narrative' => $this->transactionData->narration,
            'destination_id' => $this->custodialAccount->id,
            'exchange' => $this->transactionData->exchange_rate,
            'reverse' => false,
            'source_account' => $this->suspenseTransaction->custodial_account_id
        ];

        $this->approval = PortfolioTransactionApproval::add([
            'institution_id' => null,
            'transaction_type' => 'transfer',
            'payload' => $data,
            'sent_by' => 1,
            'approved' => 1,
            'approved_by' => 1,
            'approved_on' => Carbon::now(),
            'fund_manager_id' => $this->custodialAccount->fund_manager_id,
            'awaiting_stage_id' => 1
        ]);

        (new Approval($this->approval))->forceApprove(getSystemUser());

        return $this->approval;
    }

    private function updateCustodialTransactions()
    {
        $inTransaction = CustodialTransaction::where('approval_id', $this->approval->id)
            ->where('type', 11)
            ->first();

        if ($inTransaction) {
            $inTransaction->update([
                'bank_amount' => $this->transactionData->amount,
                'bank_transaction_id' => $this->transactionData->transaction_id
            ]);
        }

        $outTransaction = CustodialTransaction::where('approval_id', $this->approval->id)
            ->where('type', 12)
            ->first();

        if ($outTransaction) {
            $outTransaction->update([
                'bank_amount' => $this->suspenseTransaction->amount,
                'bank_transaction_id' => $this->suspenseTransaction->transaction_id
            ]);
        }

        return [
            'in' => $inTransaction,
            'out' => $outTransaction
        ];
    }

    private function updateSuspenseTransaction()
    {
        $this->suspenseTransaction->update([
            'matched' => 1,
            'approval_id' => $this->approval->id
        ]);
    }
}
