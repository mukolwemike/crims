<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\PaymentSystem;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\PaymentSystem\PaymentTransactionTrait;
use Carbon\Carbon;

class DebitTransactionProcessor
{
    use PaymentTransactionTrait;

    protected $transactionData;

    protected $custodialAccount;

    public function __construct($transactionData, CustodialAccount $custodialAccount)
    {
        $this->transactionData = $transactionData;

        $this->custodialAccount = $custodialAccount;
    }

    /**
     * @throws \Throwable
     */
    public function process()
    {
        if ($this->transactionData->source_identity == 'BANK_FEES') {
            (new ProcessFees($this->transactionData, $this->custodialAccount))->process();

            return;
        }

        if ($this->transactionData->source_identity == 'CHEQUE') {
            $transaction = $this->getChequeCustodialTransaction();

            if ($transaction) {
                (new ProcessBounceCheque($this->transactionData, $transaction))->process();

                return;
            }
        }

        //TODO handle deposit placement

        $transaction = CustodialTransaction::where(
            'outgoing_reference',
            $this->transactionData->outgoing_reference
        )->where('custodial_account_id', $this->custodialAccount->id)
            ->first();

        if ($transaction) {
            $transaction->bank_amount = $this->transactionData->amount;
            $transaction->bank_transaction_id = $this->transactionData->transaction_id;

            $transaction->save();

            return;
        }

        $this->saveSuspenseTransaction();
    }

    /**
     * @return CustodialTransaction|\Illuminate\Database\Eloquent\Model|null
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    private function getChequeCustodialTransaction()
    {
        $chequeNumber = $this->getChequeNumber($this->transactionData->transaction_id);

        return CustodialTransaction::where('cheque_number', $chequeNumber)
            ->where('type', 1)->first();
    }
}
