<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\PaymentSystem;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Exceptions\ClientInvestmentException;

trait PaymentTransactionTrait
{
    /**
     * @param $accountNumber
     * @return CustodialAccount|\Illuminate\Database\Eloquent\Model|null
     */
    public function getCustodialAccount($accountNumber, $swift)
    {
        $query = function ($accountNumber, $swift) {
            return CustodialAccount::where('account_no', $accountNumber)
                ->where('bank_swift', $swift);
        };

        $acc = $query($accountNumber, $swift)
            ->where('preferred_account', 1)
            ->latest()
            ->first();

        if ($acc) {
            return $acc;
        }

        return $query($accountNumber, $swift)
            ->latest()
            ->first();
    }

    /**
     * @param CustodialAccount $custodialAccount
     * @return bool
     */
    public function checkPaymentToDestination(CustodialAccount $custodialAccount)
    {
        return is_null($custodialAccount->product_id) && is_null($custodialAccount->project_id)
            && is_null($custodialAccount->entity_id) && is_null($custodialAccount->unit_fund_id);
    }

    /**
     * @param $transactionId
     * @return mixed
     */
    public function getChequeNumber($transactionId)
    {
        $codeArray = explode('#', $transactionId);

        return $codeArray[count($codeArray) - 1];
    }

    /**
     * @param $transactionId
     * @return mixed
     */
    public function getBankTransactionReference($transactionId)
    {
        $codeArray = explode('#', $transactionId);

        return $codeArray[0];
    }

    /**
     * @param $client
     * @param $matchType
     * @return string
     */
    public function getInflowMatchType($client, $matchType)
    {
        if (is_null($client)) {
            return 'UNKNOWN';
        }

        return in_array($matchType, ['BANK_PHONE']) ? "PARTIAL" : "COMPLETE";
    }

    /**
     * @return SuspenseTransaction|\Illuminate\Database\Eloquent\Model
     */
    protected function saveSuspenseTransaction()
    {
        $input = (array) $this->transactionData;

        $input = array_except($input, ['id']);

        $input['payload'] = $input;

        $input['custodial_account_id'] = $this->custodialAccount->id;

        $input['bank_reference'] = $this->getBankTransactionReference($this->transactionData->transaction_id);

        $suspense = SuspenseTransaction::where('transaction_id', $input['transaction_id'])
            ->exists();

        if ($suspense) {
            return;
        }

        return SuspenseTransaction::create($input);
    }

    /**
     * @throws \Exception
     */
    protected function approveClientTransaction()
    {
        $approve = new Approval($this->approval);

        try {
            $approve->systemExecute();
        }
        catch (\Throwable $e)
        {
            if(!str_contains($e->getMessage(), 'Closure')) {
                throw $e;
            }

            logRollbar('Error approving transaction '
                .PHP_EOL. $this->approval->transaction_type
                    .PHP_EOL. get_class($e)
                . PHP_EOL . json_encode($this->approval->payload));

            throw $e;
        }
    }
}
