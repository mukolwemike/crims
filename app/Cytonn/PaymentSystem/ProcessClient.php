<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\PaymentSystem;

use App\Cytonn\Data\Iprs;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\USSD\USSDRepository;
use Cytonn\Presenters\ContactPresenter;

class ProcessClient
{
    protected $transactionData;

    protected $client = null;

    protected $matchType = null;

    protected $fundDepositCode = null;

    /**
     * ProcessClient constructor.
     * @param $transactionData
     */
    public function __construct($transactionData)
    {
        $this->transactionData = $transactionData;
    }

    /**
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function process()
    {
        $this->client = $this->transactionData->source_identity == 'MPESA'
            ? $this->validateMpesaClient()
            : $this->validateBankClient();
    }

    /**
     * @return null
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return null
     */
    public function getMatchType()
    {
        return $this->matchType;
    }

    /**
     * @return null
     */
    public function getFundDepositCode()
    {
        return $this->fundDepositCode;
    }

    /**
     * @return Client|\Illuminate\Database\Eloquent\Model|null
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function validateMpesaClient()
    {
        $reference = $this->processReference($this->transactionData->reference);

        if (!isNotEmptyOrNull($reference)) {
            return null;
        }

        $client = $this->checkClientCodeWithReference($reference);

        if ($client) {
            $this->matchType = 'CLIENT_CODE';

            return $client;
        }

        $application = $this->getApplicationFromReference($reference);

        if ($application) {
            $this->matchType = 'APPLICATION_ID';

            return $this->processApplication($application);
        }

        if (isEmptyOrNull($this->transactionData->source_number)) {
            return null;
        }

        $client = $this->checkMpesaPhoneAndReference(
            $this->transactionData->source_number,
            $this->transactionData->reference
        );

        if ($client) {
            $this->matchType = 'MPESA_PHONE';

            return $client;
        }

        return null;
    }

    private function processReference($reference)
    {
        $unitFunds = UnitFund::whereNotNull('deposit_code')->get();

        $reference = trim($reference);

        foreach ($unitFunds as $unitFund) {
            $lastPartOfReference = strtoupper(substr($reference, -strlen($unitFund->deposit_code)));
            $extractedClientCode = substr($reference, 0, -strlen($unitFund->deposit_code));

            if ($lastPartOfReference == strtoupper($unitFund->deposit_code)) {
                $this->fundDepositCode = $unitFund->deposit_code;

                return trim($extractedClientCode);
            }
        }

        return $reference;
    }

    /**
     * @param $reference
     * @return Client|\Illuminate\Database\Eloquent\Model|null
     */
    private function checkClientCodeWithReference($reference)
    {
        return Client::where('client_code', $reference)->first();
    }

    /**
     * @param $reference
     * @return ClientFilledInvestmentApplication|null|void
     */
    private function getApplicationFromReference($reference)
    {
        if (!starts_with($reference, 'app_')) {
            return null;
        }

        return ClientFilledInvestmentApplication::find(substr($reference, 4));
    }

    /**
     * @param $phone
     * @param $reference
     * @return null |null
     */
    private function checkMpesaPhoneAndReference($phone, $reference)
    {
        if (isEmptyOrNull($reference)) {
            return null;
        }

        $contacts = Contact::where('phone', $phone)->get();

        foreach ($contacts as $contact) {
            $client = $contact->client;

            if ($reference == $client->id_or_passport) {
                return $client;
            }

            $fullname = ContactPresenter::presentFullNameNoTitle($contact->id);

            if ($reference == $fullname) {
                return $client;
            }

            if ($this->matchNames($reference, $fullname)) {
                return $client;
            }
        }

        return null;
    }

    private function matchNames($systemNames, $mpesaNames)
    {
        $sys = explode(' ', $systemNames);
        $mp = explode(' ', $mpesaNames);

        $matched = [];

        foreach ($sys as $segment) {
            foreach ($mp as $mpesaSegment) {
                if ($segment == $mpesaSegment) {
                    $matched[] = $segment;
                }
            }
        }

        $percentage1 = count($matched)/count($sys);
        $percentage2 = count($matched)/count($mp);

        return min($percentage1, $percentage2) > 0.65;
    }

    /**
     * @return Client|\Illuminate\Database\Eloquent\Model|null
     */
    private function validateBankClient()
    {
        if (isNotEmptyOrNull($this->transactionData->reference)) {
            $client = $this->checkClientCodeWithReference($this->transactionData->reference);

            if ($client) {
                $this->matchType = 'CLIENT_CODE';

                return $client;
            }

            if (strlen($this->transactionData->reference) > 5) {
                $client = $this->checkBankClientIdNumberWithReference($this->transactionData->reference);

                if ($client) {
                    $this->matchType = 'ID_NUMBER';

                    return $client;
                }
            }
        }

        if (isNotEmptyOrNull($this->transactionData->payer_name) &&
            isNotEmptyOrNull($this->transactionData->payer_email)) {
            $client = $this->checkBankClientNameAndEmail(
                $this->transactionData->payer_name,
                $this->transactionData->payer_email
            );

            if ($client) {
                $this->matchType = 'EMAIL';

                return $client;
            }
        }

        if (isNotEmptyOrNull($this->transactionData->payer_name) &&
            isNotEmptyOrNull($this->transactionData->source_number)) {
            $client = $this->checkBankClientNameAndPhone(
                $this->transactionData->payer_name,
                $this->transactionData->source_number
            );

            if ($client) {
                $this->matchType = 'BANK_PHONE';

                return $client;
            }
        }

        return null;
    }

    /**
     * @param $reference
     * @return Client|\Illuminate\Database\Eloquent\Model|null
     */
    private function checkBankClientIdNumberWithReference($reference)
    {
        return Client::where('id_or_passport', $reference)->where('client_type_id', 1)->first();
    }

    /**
     * @param $clientName
     * @param $email
     * @return |null
     */
    private function checkBankClientNameAndEmail($clientName, $email)
    {
        $contacts = Contact::where('email', $email)->get();

        foreach ($contacts as $contact) {
            $fullname = ContactPresenter::presentFullNameNoTitle($contact->id);

            $client = $contact->client;

            $nameMatched = $fullname == $clientName || $this->matchNames($fullname, $clientName);

            if ($nameMatched && $client) {
                return $client;
            }
        }

        return null;
    }

    /**
     * @param $clientName
     * @param $phone
     * @return |null
     */
    private function checkBankClientNameAndPhone($clientName, $phone)
    {
        $contacts = Contact::where('phone', $phone)->get();

        foreach ($contacts as $contact) {
            $fullname = ContactPresenter::presentFullNameNoTitle($contact->id);

            $client = $contact->client;

            $nameMatched = $fullname == $clientName || $this->matchNames($fullname, $clientName);

            if ($nameMatched && $client) {
                return $client;
            }
        }

        return null;
    }

    /**
     * @param ClientFilledInvestmentApplication $application
     * @return Client
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws \Throwable
     */
    protected function processApplication(ClientFilledInvestmentApplication $application)
    {
        $appl = $application->application;

        $client = $appl ? $appl->client : null;

        if ($client) {
            return $client;
        }

        $action = new USSDRepository();

        $app = $action->processApplication($application);

        $app = $action->approveApplication($app);

        $action->createClientUser($app, $app->client);

        $this->requestIprs($application, $app->client);

        $action->createMpesaAccount($app->client);

        return $app->client;
    }

    private function requestIprs(ClientFilledInvestmentApplication $application, Client $client)
    {
        if (!$application->individual || !$application->unitFund) {
            return;
        }

        if ($application->unitFund->short_name != 'CMMF') {
            return;
        }

        $iprs = new Iprs();
        $iprs->createRequest($client);

        foreach ($client->jointDetail as $joint) {
            $iprs->createRequest($client, $joint->id_or_passport, $joint);
        }
    }
}
