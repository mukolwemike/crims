<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\PaymentSystem;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialWithdrawType;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Portfolio\Approvals\Engine\Approval;

class ProcessFees
{
    protected $transactionData;

    protected $custodialAccount;

    protected $approval;

    /**
     * ProcessFees constructor.
     * @param $transactionData
     * @param CustodialAccount $custodialAccount
     */
    public function __construct($transactionData, CustodialAccount $custodialAccount)
    {
        $this->transactionData = $transactionData;

        $this->custodialAccount = $custodialAccount;
    }

    /**
     * @throws \Throwable
     */
    public function process()
    {
        if ($this->checkExistingTransaction()) {
            return;
        }

        $this->saveCustodialPaymentApproval();

        $this->approveTransaction();
    }

    private function approveTransaction()
    {
        $handler = $this->approval->handler();

        $handler->handle($this->approval);

        $this->approval->approve(User::find(1));
    }

    /**
     * @return CustodialTransaction|\Illuminate\Database\Eloquent\Model|null
     */
    private function checkExistingTransaction()
    {
        return CustodialTransaction::where('type', 5)
            ->where('bank_transaction_id', $this->transactionData->transaction_id)
            ->first();
    }

    /**
     * @return PortfolioTransactionApproval|\Illuminate\Database\Eloquent\Model
     */
    private function saveCustodialPaymentApproval()
    {
        $data = [
            'amount' => abs($this->transactionData->amount),
            'date' => Carbon::parse($this->transactionData->date)->toDateString(),
            'transaction' => $this->getFeeType(),
            'narrative' => $this->transactionData->narration,
            'custodial_id' => $this->custodialAccount->id,
            'bank_transaction_id' => $this->transactionData->transaction_id,
            'bank_amount' => $this->transactionData->amount
        ];

        $this->approval = PortfolioTransactionApproval::create([
            'payload' => $data,
            'transaction_type' => 'custodial_payment',
            'sent_by' => 1,
            'approved' => 1,
            'approved_by' => 1,
            'approved_on' => Carbon::now(),
            'fund_manager_id' => $this->custodialAccount->fund_manager_id,
            'awaiting_stage_id' => 1
        ]);

        (new Approval($this->approval))->forceApprove(getSystemUser());

        return $this->approval;
    }

    private function getFeeType()
    {
        $fee = 'EOE';

        if (str_contains($this->transactionData->narration, 'CUSTODY')) {
            $fee = 'ECF';
        }

        return CustodialWithdrawType::where('code', $fee)->first()->id;
    }
}
