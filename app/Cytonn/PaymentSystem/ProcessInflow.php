<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\PaymentSystem;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\PaymentSystem\PaymentTransactionTrait;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Presenters\ClientPresenter;

class ProcessInflow
{
    use PaymentTransactionTrait;

    protected $transactionData;
    protected $custodialAccount;
    protected $client;
    protected $matchType;
    protected $clientPayment = null;
    protected $custodialTransaction = null;
    protected $approval = null;

    public function __construct($transactionData, CustodialAccount $custodialAccount, $matchType, Client $client = null)
    {
        $this->client = $client;

        $this->custodialAccount = $custodialAccount;

        $this->transactionData = $transactionData;

        $this->matchType = $matchType;
    }

    /**
     * @throws \Throwable
     */
    public function process()
    {
        $this->saveInflowApproval();

        $this->approveClientTransaction();

        $this->updateCustodialTransactionAndPayment();
    }

    /**
     * @return null
     */
    public function getClientPayment()
    {
        return $this->clientPayment;
    }

    /**
     * @return null
     */
    public function getCustodialTransaction()
    {
        return $this->custodialTransaction;
    }

    /**
     * @return null
     */
    public function getApproval()
    {
        return $this->approval;
    }

    private function updateCustodialTransactionAndPayment()
    {
        $this->clientPayment = ClientPayment::where('approval_id', $this->approval->id)->first();

        $this->clientPayment->update([
            'inflow_match_type' => $this->getInflowMatchType($this->client, $this->matchType),
            'description' => 'Client funds deposited - ' . $this->transactionData->narration
        ]);

        $this->custodialTransaction = $this->clientPayment->custodialTransaction;

        if ($this->custodialTransaction) {
            $this->custodialTransaction->update([
                'bank_transaction_id' => $this->transactionData->transaction_id,
                'bank_amount' => $this->transactionData->amount,
            ]);
        }
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function saveInflowApproval()
    {
        $data = $this->prepareInflowApprovalData();

        $approval = ClientTransactionApproval::create([
            'client_id' => $data['client_id'],
            'transaction_type' => 'add_cash_to_account',
            'payload' => $data,
            'sent_by' => getSystemUser()->id,
            'approved' => false,
            'approved_by' => null,
            'approved_on' => null,
            'awaiting_stage_id' => 6
        ]);

        return $this->approval = $approval;
    }

    /**
     * @return array
     */
    private function prepareInflowApprovalData()
    {
        $input = array();

        $input['id'] = null;

        $input['type'] = CustodialTransactionType::where('name', 'FI')->first()->id;

        $input['custodial_account_id'] = $this->custodialAccount->id;

        $paymentToData = $this->getPaymentToData();

        $input = array_merge($input, $paymentToData);

        $input['exchange_rate'] = 1; //cash is always in destination currency

        $source = $this->getPaymentSource();

        $input['source'] = $source;

        if ($source == 'cheque') {
            $input['cheque_number'] = $this->getChequeNumber($this->transactionData->transaction_id);
        }

        $input['amount'] = $this->transactionData->amount;

        $input['date'] = Carbon::parse($this->transactionData->date)->toDateString();

        $input = array_merge($input, $this->getClientInflowDetails());

        $input['description'] = 'Client funds deposited - ' . $this->transactionData->narration;

        $input = array_merge($input, $this->getSourceIdentityCodes());

        return $input;
    }


    /**
     * @return array|null
     */
    private function getPaymentToData()
    {
        if (!is_null($this->custodialAccount->product_id)) {
            return [
                'product_id' => $this->custodialAccount->product_id
            ];
        } elseif (!is_null($this->custodialAccount->project_id)) {
            return [
                'project_id' => $this->custodialAccount->project_id
            ];
        } elseif (!is_null($this->custodialAccount->entity_id)) {
            return [
                'entity_id' => $this->custodialAccount->entity_id
            ];
        } elseif (!is_null($this->custodialAccount->unit_fund_id)) {
            return [
                'unit_fund_id' => $this->custodialAccount->unit_fund_id
            ];
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    private function getPaymentSource()
    {
        if ($this->transactionData->source_identity == 'MPESA') {
            return 'mpesa';
        } elseif ($this->transactionData->source_identity == 'CHEQUE') {
            return 'cheque';
        } else {
            return 'deposit';
        }
    }

    /**
     * @return array
     */
    private function getClientInflowDetails()
    {
        $input = array();

        if ($this->client) {
            $input['client_id'] = $this->client->id;

            $input['received_from'] = ClientPresenter::presentFullNames($this->client->id);

            $fa = $this->client->getLatestFa();

            $input['commission_recipient_id'] = $fa ? $fa->id : null;
        } else {
            $input['client_id'] = null;

            $input['received_from'] = $this->transactionData->payer_name;

            $input['commission_recipient_id'] = null;
        }

        return $input;
    }

    /**
     * @return array
     */
    private function getSourceIdentityCodes()
    {
        $codeArray = array();

        if ($this->transactionData->source_identity == 'MPESA') {
            $codeArray['mpesa_confirmation_code'] = $this->transactionData->transaction_id;

            $codeArray['bank_reference_no'] = $this->transactionData->transaction_id;
        } else {
            $codeArray['bank_reference_no'] = $this->transactionData->transaction_id;

            $codeArray['mpesa_confirmation_code'] = null;
        }

        return $codeArray;
    }
}
