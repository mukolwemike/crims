<?php
/**
 * Date: 8/11/15
 * Time: 3:09 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Staff;

use App\Cytonn\Models\Staff;

class StaffRepository
{
    public $staff;

    /**
     * StaffRepository constructor.
     *
     * @param $staff
     */
    public function __construct(Staff $staff = null)
    {
        is_null($staff) ? $this->staff = new Staff(): $this->staff = $staff;
    }

    public function save($staffdata)
    {
        return $this->staff->add($staffdata);
    }
}
