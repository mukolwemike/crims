<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Coop\Clients;

use Cytonn\Rules\Rules;

trait CoopClientRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate an individual coop client
     */
    public function individualCompleteClientCreate($request)
    {
        $rules = [
            'title_id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required | email',
            'phone' => 'required',
            'dob' => 'required',
            'payment_date' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate an individual incomplete coop client
     */
    public function individualInCompleteClientCreate($request)
    {
        $rules = [
            'title_id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
        ];

        return $this->verdict($request, $rules);
    }

    /*
    * Validate an corporate coop client
    */
    public function corporateCompleteClientCreate($request)
    {
        $rules = [
            'registered_name' => 'required',
            'registered_address' => 'required',
            'method_of_contact_id' => 'required',
            'contact_person_title' => 'required',
            'contact_person_lname' => 'required',
            'contact_person_fname' => 'required',
            'email' => 'required | email',
            'phone' => 'required',
            'payment_date' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate an corporate incomplete coop client
     */
    public function corporateInCompleteClientCreate($request)
    {
        $rules = [
            'registered_name' => 'required'
        ];

        return $this->verdict($request, $rules);
    }
}
