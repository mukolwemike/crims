<?php
/**
 * Date: 19/09/2016
 * Time: 9:52 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Coop\Memberships;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CoopPayment;
use App\Cytonn\Models\MembershipConfirmation;
use Cytonn\Mailers\Coop\BusinessConfirmationMailer;
use Cytonn\Presenters\UserPresenter;
use App\Cytonn\Models\User;

/**
 * Class BusinessConfirmationGenerator
 *
 * @package Cytonn\Coop\Memberships
 */
class BusinessConfirmationGenerator
{
    /**
     * @var BusinessConfirmationMailer
     */
    protected $mailer;

    /**
     * BusinessConfirmationGenerator constructor.
     */
    public function __construct()
    {
        $this->mailer = new BusinessConfirmationMailer();
    }

    /**
     * @param CoopPayment  $payment
     * @param array        $payments
     * @param $email_sender
     * @return mixed
     */
    public function generate(ClientPayment $payment, array $payments, $email_sender)
    {
        $payments = ClientPayment::whereIn('id', $payments)->get();

        $payments->each(
            function ($payment) {
                $payment->description = $payment->repo->describe();
            }
        );

        $total = $payments->sum(
            function ($payment) {
                return abs($payment->amount);
            }
        );

        $client = $payment->client;

        $email_sender = UserPresenter::presentLetterClosing($email_sender);

        return \PDF::loadView(
            'coop.reports.membership_confirmation',
            ['email_sender'=>$email_sender,
            'client'=>$client, 'logo'=>$client->fundManager->logo, 'payments'=>$payments, 'total'=>$total
            ]
        );
    }

    /**
     * @param CoopPayment $payment
     * @param array       $payments
     */
    public function send(ClientPayment $payment, array $payments)
    {
        $this->mailer->sendMembershipConfirmation($payment, $payments);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getBusinessConfirmation($id)
    {
        $conf = MembershipConfirmation::findOrFail($id);

        $payment = $conf->clientPayment;

        $payments = $conf->payload->payments;

        return $this->generate($payment, $payments, $conf->sent_by);
    }

    /**
     * @param CoopPayment $payment
     * @param array       $payments
     * @param User|null   $sender
     * @return MembershipConfirmation
     */
    public function save(ClientPayment $payment, array $payments, User $sender = null)
    {
        return MembershipConfirmation::create([
                'client_payment_id'=>$payment->id,
                'payload'=>(object)[ 'payments'=>$payments],
                'sent_by'=> is_null($sender) ? null :$sender->id
            ]);
    }
}
