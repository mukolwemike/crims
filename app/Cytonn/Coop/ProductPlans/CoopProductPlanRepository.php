<?php

namespace Cytonn\Coop\ProductPlans;

use App\Cytonn\Models\ProductPlan;

/**
 * Class CoopProductPlanRepository
 *
 * @package Cytonn\Coop\Payments
 */
class CoopProductPlanRepository
{

    /**
     * @return mixed
     */
    public function getAllCoopProductPlans()
    {
        return ProductPlan::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCoopProductPlanById($id)
    {
        return ProductPlan::findOrFail($id)->get();
    }

    public function getClientCoopProductPlans($clientId)
    {
        return ProductPlan::where('client_id', $clientId)->get();
    }

    /**
     * @param array $data
     * @return ProductPlan
     */
    public function createCoopProductPlan(array $data)
    {
        $data['active'] = true;
        if ($data['category'] == 'shares') {
            unset($data['product_id']);
            unset($data['amount']);
        }
        unset($data['category']);

        return  ProductPlan::create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @return bool|int
     */
    public function updateCoopProductPlan(array $data, $id)
    {
        $product_plan = ProductPlan::findOrFail($id);
        if ($data['category'] == 'shares') {
            //            unset($data['product_id']);
            //            unset($data['amount']);
            $data['product_id'] = null;
            $data['amount'] = null;
        }
        unset($data['category']);
        return $product_plan->update($data);
    }

    /**
     * @param $id
     * @return bool|int
     */
    public function activateProductPlan($id)
    {
        $product_plan = ProductPlan::findOrFail($id);
        return $product_plan->update(
            [
            'active'    =>      1
            ]
        );
    }

    /**
     * @param $id
     * @return bool|int
     */
    public function deactivateProductPlan($id)
    {
        $product_plan = ProductPlan::findOrFail($id);
        return $product_plan->update(
            [
            'active'    =>      0
            ]
        );
    }

    /**
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function deleteProductPlan($id)
    {
        return ProductPlan::findOrFail($id)->delete();
    }
}
