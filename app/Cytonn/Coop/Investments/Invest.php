<?php
/**
 * Date: 07/09/2016
 * Time: 4:07 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Coop\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ProductPlan;
use Cytonn\Investment\Action\Base;
use Cytonn\Investment\Events\ClientInvestmentHasBeenAdded;
use Cytonn\Investment\Events\NewInvestmentAdded;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Invest extends Base
{
    use DispatchableTrait, EventGenerator;

    public function invest(ProductPlan $productPlan, ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $client = $approval->client;

        /*
         * create investment
         */
        $investment = ClientInvestment::create(
            [
            'approval_id'=>$approval->id,
            'client_id'=>$client->id,
            'amount'=>$data['amount'],
            'invested_date'=>$data['invested_date'],
            'maturity_date'=>$data['maturity_date'],
            'investment_type_id'=>$data['investment_type_id'],
            'interest_rate'=>$data['interest_rate'],
            'product_id'=>$productPlan->product_id,
            'product_plan_id'=>$productPlan->id,
            'interest_payment_interval'=>0
            ]
        );

        $investment->save();

        //create commission
        $investment->commissionRepo->saveCommission(
            $investment,
            [
            'commission_rate'=>$data['commission_rate'],
            'commission_recepient'=>$data['commission_recipient']
            ]
        );

        // Deduct from payments
        $payment = $this->paymentIn($investment);
        $investment->paymentIn()->associate($payment);
        $payment->save();

        //raise events for investment
        $this->raise(new ClientInvestmentHasBeenAdded($investment));
        $this->raise(new NewInvestmentAdded($investment));
        $this->dispatchEventsFor($this);

        return $investment;
    }
}
