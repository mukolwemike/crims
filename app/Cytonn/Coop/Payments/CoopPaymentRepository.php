<?php

namespace Cytonn\Coop\Payments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CoopPayment;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\ShareHolding;
use Carbon\Carbon;
use App\Cytonn\Models\CustodialTransaction;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\ClientPresenter;

/**
 * Class CoopPaymentRepository
 *
 * @package Cytonn\Coop\Payments
 */
class CoopPaymentRepository
{
    use ValidatorTrait;

    /**
     * @var CoopPayment
     */
    protected $payment;


    /**
     * CoopPaymentRepository constructor.
     *
     * @param CoopPayment|null $payment
     */
    public function __construct(CoopPayment $payment = null)
    {
        is_null($payment) ? $this->payment = new CoopPayment() : $this->payment = $payment;
    }


    /**
     * Create a payment together with its custodial transaction
     *
     * @param  array            $data
     * @param  CustodialAccount $account
     * @return CoopPayment
     */
    public function createCoopPayment(array $data, CustodialAccount $account)
    {
        $payment = $this->createCoopPaymentWithoutCustodialTransaction($data);

        $payment->custodial_transaction_id = $this->createCustodialTransactionForPayment($payment, $account)->id;
        $payment->save();

        return $payment;
    }

    /**
     * Create a custodial transaction for a payment
     *
     * @param  CoopPayment      $payment
     * @param  CustodialAccount $account
     * @return CustodialTransaction
     */
    protected function createCustodialTransactionForPayment(CoopPayment $payment, CustodialAccount $account)
    {
        $client = $payment->client;

        $owner = $client->repo->getClientTransactionOwner();

        // Record payment in custodial transactions table
        $transaction = CustodialTransaction::create(
            [
            'type'                      =>  CustodialTransactionType::where('name', 'FI')->first()->id,
            'custodial_account_id'      =>  $account->id,
            'amount'                    =>  $payment->amount,
            'description'               =>  'New Payment from'. ClientPresenter::presentFullNames($client->id),
            'date'                      =>  $payment->date,
            'transaction_owners_id'     => $owner->id
            ]
        );

        return $transaction;
    }

    /**
     * Create a payment
     *
     * @param  array $data
     * @throws ClientInvestmentException
     * @return CoopPayment
     */
    public function createCoopPaymentWithoutCustodialTransaction(array $data)
    {
        $rules = [
            'client_id'=>'required',
            'approval_id'=>'required',
            'type'=>'required',
            'amount'=>'required|numeric',
            'date'=>'required|date'
        ];

        $this->payment->client_id = $data['client_id']; //set the client if class is not called from an existing payment

        if ((float)abs($data['amount']) > $this->balance($data['date']) && (float)$data['amount'] < 0) {
            throw new ClientInvestmentException(
                'The client does not have enough funds for this transaction. Please add a payment first'
            );
        }

        $data = $this->filter($data, CoopPayment::getTableColumnsAsArray());


        if (!$this->isValid($data, $rules)) {
            throw new ClientInvestmentException('The coop payment details are incorrectly formed');
        }

        return CoopPayment::create($data);
    }

    /**
     * Get a clients balance as at some date
     *
     * @param  null $date
     * @return mixed
     */
    public function balance($date = null)
    {
        $date = new Carbon($date);

        return (float)CoopPayment::where('date', '<=', $date)
            ->where('client_id', $this->payment->client_id)
            ->sum('amount');
    }

    /**
     * @param $slug
     * @return float
     */
    public function totalForType($slug)
    {
        return abs(CoopPayment::where('client_id', $this->payment->client_id)
            ->where('type', $slug)->sum('amount'));
    }

    /**
     * @return float
     */
    public function totalForClient()
    {
        return (float)CoopPayment::where('client_id', $this->payment->client_id)
            ->where('amount', '>', 0)
            ->sum('amount');
    }

    /**
     * @return mixed|string
     */
    public function describe()
    {
        $description = $this->payment->narrative;

        switch ($this->payment->type) {
            case 'product':
                $investment = ClientInvestment::where('payment_id', $this->payment->id)
                    ->first();

                if (!is_null($investment)) {
                    $product = $investment->product->name;
                    $duration = $investment->productPlan->duration;
                    $description = $product." ".(int)$duration.' '.str_plural('Year', $duration);
                }
                break;
            case 'shares':
                $holding = ShareHolding::where('payment_id', $this->payment->id)
                    ->first();

                if (!is_null($holding)) {
                    $description = "Share purchase ($holding->number shares @ $holding->purchase_price)";
                }
                break;
            case 'membership':
                $description = 'Membership fees';
                break;
            default:
                $description = $this->payment->narrative;
        }

        return $description;
    }
}
