<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Roles;

use Cytonn\Rules\Rules;

trait RoleRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * ROLES
     */
    /*
     * Validate roles creation
     */
    public function rolesCreate($request)
    {
        $rules = [
            'name' => 'required | unique:authoritaire_roles',
            'description' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate roles update
     */
    public function rolesUpdate($request, $id)
    {
        $rules = [
            'name' => 'required | unique:authoritaire_roles,name,' . $id,
            'description' => 'required'
        ];

        return $this->verdict($request, $rules);
    }
}
