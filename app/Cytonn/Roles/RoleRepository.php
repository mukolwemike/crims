<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Roles;

use Cytonn\Authorization\Role;

class RoleRepository
{
    /*
     * Get a roles by its id
     */
    public function getRoleById($id)
    {
        return Role::findOrFail($id);
    }

    /*
     * Store or update a roles
     */
    public function save($input, $id)
    {
        if ($id) {
            $role = $this->getRoleById($id);

            $role->update($input);

            return $role;
        }

        return Role::create($input);
    }
}
