<?php

namespace Cytonn\Banks\Forms;

use Laracasts\Validation\FormValidator;

class BranchForm extends FormValidator
{
    public $rules = [
        'name'              =>  'required | string | min:3 | max:100',
        'swift_code'        =>  'nullable',
        'branch_code'        =>  'nullable',
    ];
}
