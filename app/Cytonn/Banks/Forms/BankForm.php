<?php

namespace Cytonn\Banks\Forms;

use Laracasts\Validation\FormValidator;

class BankForm extends FormValidator
{
    public $rules = [
        'name'              =>  'required | string | min:3 | max:100',
        'swift_code'        =>  'required',
        'clearing_code'     =>  'required',
    ];
}
