<?php
/**
 * Date: 06/06/2016
 * Time: 11:09 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Handlers;

use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Mailers\LOOMailer;
use Cytonn\Realestate\Commissions\AwardHandler;
use Cytonn\Realestate\Commissions\Calculator;
use Cytonn\Realestate\Commissions\RealEstateCommissionRepository;
use Cytonn\Realestate\Events\LOOHasBeenRejected;
use Cytonn\Realestate\Events\LOOHasBeenSigned;
use Cytonn\Realestate\Events\LOOHasBeenUploaded;
use Cytonn\Realestate\Events\PaymentHasBeenMade;
use Cytonn\Realestate\Events\ReservationFeesPaid;
use Cytonn\Realestate\Events\SalesAgreementHasBeenSigned;
use Cytonn\Realestate\Events\SalesAgreementHasBeenUploaded;
use Cytonn\Realestate\Events\UnitHasBeenReserved;
use Cytonn\Realestate\Payments\Interest;
use Cytonn\Realestate\Payments\Payment;
use Cytonn\Realestate\Payments\Scheduling;
use Cytonn\Realestate\Sale\LetterOfOffer;
use Cytonn\Handlers\EventListener;
use Cytonn\Mailers\SalesAgreementsMailer;

/**
 * Class EventHandler
 *
 * @package Cytonn\Realestate\Handlers
 */
class EventHandler extends EventListener
{
    /**
     * When a unit is reserved
     *
     * @param UnitHasBeenReserved $event
     */
    public function whenUnitHasBeenReserved(UnitHasBeenReserved $event)
    {
        if ($event->creating) {
            //Create a schedule for the booking fee
            (new Scheduling())->scheduleReservationFee($event->reservationForm->holding);
        }
    }


    /**
     * When reservation fees have been paid
     *
     * @param ReservationFeesPaid $event
     */
    public function whenReservationFeesPaid(ReservationFeesPaid $event)
    {
        //schedule an LOO to be prepared
        (new LetterOfOffer())->schedule($event->payment);
    }

    /**
     * When sales agreement is uploaded and signed
     *
     * @param SalesAgreementHasBeenUploaded $event
     */
    public function whenSalesAgreementHasBeenUploaded(SalesAgreementHasBeenUploaded $event)
    {
        (new SalesAgreementsMailer())->sendEmailWhenSalesAgreementHasBeenUploaded($event->salesAgreement);
    }

    /**
     * When a payment is made
     *
     * @param PaymentHasBeenMade $event
     */
    public function whenPaymentHasBeenMade(PaymentHasBeenMade $event)
    {
        $payment = $event->payment;

        (new Payment($payment))->sendBusinessConfirmation();

        (new AwardHandler())->onPaymentMade($payment);

        (new Interest($payment->holding))->saveInterestAfterPayment($payment);
    }

    /**
     * When LOO is rejected
     *
     * @param LOOHasBeenRejected $event
     */
    public function whenLOOHasBeenRejected(LOOHasBeenRejected $event)
    {
        (new LOOMailer())->sendEmailToOpsWhenLOOHasBeenRejected($event->realestateLetterOfOfferReject);
    }

    /**
     * When LOO is signed
     *
     * @param LOOHasBeenSigned $event
     */
    public function whenLOOHasBeenSigned(LOOHasBeenSigned $event)
    {
        $realestateLetterOfOffer = $event->realestateLetterOfOffer;

        (new AwardHandler())->onLooSigned($realestateLetterOfOffer);
    }

    /**
     * When Sales Agreement is signed
     *
     * @param SalesAgreementHasBeenSigned $event
     */
    public function whenSalesAgreementHasBeenSigned(SalesAgreementHasBeenSigned $event)
    {
        $salesAgreement = $event->salesAgreement;

        //calculate and award commission
        (new AwardHandler())->onSalesAgreementSigned($salesAgreement);
    }
}
