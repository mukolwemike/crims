<?php
/**
 * Date: 14/01/2017
 * Time: 14:15
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;

class BaseCalculator
{
    /**
     *
     * @param UnitHolding $holding
     * @param Carbon $date
     * @return float
     */
    protected function getPercentagePaid(UnitHolding $holding, Carbon $date = null)
    {
        if ($holding->price() <= 0) {
            return 0;
        }

        $total = $holding->totalPayments($date);
        $refunds = $holding->refundedAmount($date);

        $total -= $refunds;

        return 100 * $total / $holding->price();
    }

    /**
     * @param UnitHolding $holding
     * @return number
     */
    public function calculateTotalCommission(UnitHolding $holding)
    {
        $commission = $holding->commission;

        if (!$commission) {
            return;
        }

        if (!$commission->rate && !$commission->commission_rate) {
            return;
        }

        if ($commission->commission_rate) {
            $rate = $commission->commission_rate;
            $type = 'percentage';
        } else {
            $rate = $commission->rate->amount;
            $type = $commission->rate->type;
        }

        switch ($type) {
            case 'percentage':
                $amount = ($rate / 100) * $holding->price();
                break;
            case 'amount':
                $amount = $rate;
                break;
            default:
                throw new \InvalidArgumentException("The commission type $type is not supported");
        }

        return $amount;
    }

    public function saveTotalCommission(UnitHolding $holding)
    {
        $commission = $holding->commission;

        $total = $this->calculateTotalCommission($holding);

        if ($total) {
            $commission->amount = $total;
            $commission->save();
        }
    }
}
