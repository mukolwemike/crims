<?php

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;

/**
 * Class RealEstateCommissionRepository
 *
 * @package Cytonn\Realestate\Commissions
 */
class RealEstateCommissionPaymentScheduleRepository
{
    /**
     * @return mixed
     */
    public function getAllCommissionPaymentSchedules()
    {
        return RealEstateCommissionPaymentSchedule::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCommissionPaymentScheduleById($id)
    {
        return RealEstateCommissionPaymentSchedule::findOrFail($id);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function createCommissionPaymentSchedule(array $data)
    {
        $rate = new RealEstateCommissionPaymentSchedule();
        $rate->fill($data);

        return $rate->save();
    }

    /**
     * @param array $data
     * @param $id
     * @return bool|int
     */
    public function updateCommissionPaymentSchedule(array $data, $id)
    {
        $commissionPaymentSchedule = RealEstateCommissionPaymentSchedule::findOrFail($id);

        return $commissionPaymentSchedule->update($data);
    }

    /**
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function deleteCommissionPaymentSchedule($id)
    {
        return RealEstateCommissionPaymentSchedule::findOrFail($id)->delete();
    }
}
