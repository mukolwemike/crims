<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Realestate\Commissions;

use Laracasts\Presenter\Presenter;

class RealEstateCommissionPresenter extends Presenter
{
    /*
     * Get the rate for the commission
     */
    public function getRate()
    {
        if ($this->commission_rate) {
            return $this->commission_rate;
        } elseif ($this->rate) {
            return $this->rate->amount;
        } else {
            return '';
        }
    }

    /*
     * Get the rate type
     */
    public function getType()
    {
        if ($this->commission_rate) {
            return 'percentage';
        } elseif ($this->rate) {
            return $this->rate->type;
        } else {
            return '';
        }
    }
}
