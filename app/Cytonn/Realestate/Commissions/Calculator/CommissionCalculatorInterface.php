<?php
/**
 * Date: 03/08/2016
 * Time: 10:35 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Commissions\Calculator;

use App\Cytonn\Models\RealEstatePayment;

/**
 * Interface CommissionCalculatorInterface
 *
 * @package Cytonn\Realestate\Commissions\Calculator
 */
interface CommissionCalculatorInterface
{

    /**
     * Calculate the payment
     *
     * @param  RealEstatePayment $payment
     * @return mixed
     */
    public function calculate(RealEstatePayment $payment);
}
