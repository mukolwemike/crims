<?php
/**
 * Date: 12/01/2017
 * Time: 11:20
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Commissions\Calculator;

use App\Cytonn\Models\RealestateCommission;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\RealEstateCommissionPaymentScheduleType;
use App\Cytonn\Models\RealEstateCommissionStructure;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstateSalesAgreement;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Realestate\Commissions\BaseCalculator;

/**
 * Class Award
 *
 * @package Cytonn\Realestate\Commissions\Calculator
 */
class Award extends BaseCalculator
{
    /**
     * @var UnitHolding
     */
    protected $unitHolding;

    /**
     * The commission constants
     *
     * @var object
     */
    private $constants;

    /**
     * @return object
     */
    public function getConstants(): object
    {
        return $this->constants;
    }

    /**
     * Award constructor.
     *
     * @param $unitHolding
     */
    public function __construct(UnitHolding $unitHolding)
    {
        $this->unitHolding = $unitHolding;

        $this->saveTotalCommission($unitHolding);

        $this->constants = $this->getCommissionConstants(Carbon::parse($this->unitHolding->reservation_date));
    }


    /**
     * Pay commission when deposit is paid
     */
    public function onDepositComplete()
    {
        $conditionMet = $this->initialConditionMet();

        if (!$conditionMet) {
            return;
        }

        $payment = $this->unitHolding->repo->paymentPercentageMet(10);

        $this->award(
            'deposit',
            $this->constants->deposit_award,
            Carbon::latest($payment->date, $conditionMet),
            '10% deposit paid: ' . AmountPresenter::currency($payment->amount)
            . ' on ' . DatePresenter::formatDate($payment->date)
        );
    }

    /**
     * @param RealEstateSalesAgreement $salesAgreement
     */
    public function onSalesAgreementSigned(RealEstateSalesAgreement $salesAgreement)
    {
        $conditionMet = $this->initialConditionMet();

        if (!$conditionMet) {
            return;
        }

        if (!$this->saleAgreementSigned()) {
            return;
        }

        $this->award(
            'sales_agreement',
            $this->constants->sales_agreement_award,
            Carbon::latest($salesAgreement->date_received, $conditionMet),
            'Sales agreement signed by client: received - ' .
            DatePresenter::formatDate($salesAgreement->date_received)
        );
    }

    /**
     * @param RealEstatePayment $payment
     */
    public function onInstallmentPaid(RealEstatePayment $payment)
    {
        $initialCondition = $this->initialConditionMet($payment->date);

        if (!$initialCondition) {
            return;
        }

        if (!$this->saleAgreementSigned()) {
            return;
        }

        $latest = Carbon::latest($initialCondition, $this->unitHolding->salesAgreement->date_received);

        $percentage_due = $this->getInstallmentPercentageDue($payment->date);

        $this->award(
            'installment',
            $percentage_due,
            Carbon::latest($payment->date, $latest),
            "Installment paid: " . AmountPresenter::currency($payment->amount) . ' on ' .
            DatePresenter::formatDate($payment->date)
        );
    }

    /*
     * Determine new award based on the paid installments
     */
    public function onInstallmentPaidNew(RealEstatePayment $payment)
    {
        $initialCondition = $this->initialConditionMet($payment->date);

        if (!$initialCondition) {
            return;
        }

        if (!$this->saleAgreementSigned()) {
            return;
        }

        $latest = Carbon::latest($initialCondition, $this->unitHolding->salesAgreement->date_received);

        $percentage_due = $this->getInstallmentPercentageDueNew($payment->date);

        $this->award(
            'installment',
            $percentage_due,
            Carbon::latest($payment->date, $latest),
            "Installment paid: " . AmountPresenter::currency($payment->amount) . ' on ' .
            DatePresenter::formatDate($payment->date)
        );
    }

    /*
     * Get the installment percentage due to be paid
     */
    public function getInstallmentPercentageDueNew(\Carbon\Carbon $date = null)
    {
        $totalInstallmentPercentage = array_sum($this->constants->installment_stages);

        $percentageDue = $this->getPercentagePaid($this->unitHolding, $date) * $totalInstallmentPercentage  / 100;

        return min($percentageDue, $totalInstallmentPercentage);
    }

    /**
     *
     * @param \Carbon\Carbon $date
     * @return int
     */
    protected function getInstallmentPercentageDue(\Carbon\Carbon $date = null)
    {
        $percentage_paid = $this->getPercentagePaid($this->unitHolding, $date);

        $percentage_due = 0;

        foreach ($this->constants->installment_stages as $paid => $award) {
            if ($percentage_paid >= (float)$paid) {
                $percentage_due += $award;
            }
        }

        return $percentage_due;

        //TODO use collections
    }

    /**
     * @param $type
     * @param $limit
     * @param $date
     * @param $description
     */
    protected function award($type, $limit, $date, $description)
    {
        $schedules = $this->getSchedules($type);

        $amount_scheduled = $schedules->sum('amount');

        $commission = $this->unitHolding->commission;

        if ($commission->amount == 0) {
            return;
        }

        $percentage_scheduled = ($amount_scheduled / $commission->amount) * 100;

        if ($percentage_scheduled < $limit) {
            $total_for_type = ($limit / 100) * $commission->amount;
            $diff = $total_for_type - $amount_scheduled;

            if ($diff <= 0.49) {
                return;//don't record zeros or negatives, or too small values
            }

            $this->createSchedule(
                $commission,
                $date,
                $diff,
                RealEstateCommissionPaymentScheduleType::where('slug', $type)->first(),
                $description
            );
        }
    }

    /**
     * @param RealestateCommission $commission
     * @param $date
     * @param $amount
     * @param RealEstateCommissionPaymentScheduleType $type
     * @param $description
     * @return RealEstateCommissionPaymentSchedule
     */
    protected function createSchedule(
        RealestateCommission $commission,
        $date,
        $amount,
        RealEstateCommissionPaymentScheduleType $type,
        $description
    ) {
        return RealEstateCommissionPaymentSchedule::create([
            'commission_id' => $commission->id,
            'date' => $date,
            'amount' => $amount,
            'type_id' => $type->id,
            'description' => $description
        ]);
    }

    /**
     * @param \Carbon\Carbon $date
     * @return bool|Carbon
     */
    protected function initialConditionMet(\Carbon\Carbon $date = null)
    {
        $loo = $this->unitHolding->loo;

        if (!$loo) {
            return false;
        }

        if (is_null($loo->date_signed)) {
            return false;
        }

        $percentage = $this->getPercentagePaid($this->unitHolding, $date);

        $payment = $this->unitHolding->repo->paymentPercentageMet(10);

        if (!$payment) {
            return false;
        }

        $date_met = Carbon::latest($payment->date, $loo->date_received);

        return $percentage >= $this->constants->minimum_payment_percentage ? $date_met : false;
    }

    /**
     * @return bool|Carbon
     */
    protected function saleAgreementSigned()
    {
        $not_signed = is_null($this->unitHolding->salesAgreement->date_signed);

        return $not_signed ? false : $this->unitHolding->salesAgreement->date_received;
    }

    /**
     * @param $type
     * @return mixed
     */
    protected function getSchedules($type)
    {
        return $this->unitHolding->commission->schedules()
            ->whereHas(
                'type',
                function ($t) use ($type) {
                    $t->where('slug', $type);
                }
            )->get();
    }

    /*
     * Get the constanst to be applied in the computation of the commission
     */
    public function getCommissionConstants(Carbon $reservationDate)
    {
        $structure = RealEstateCommissionStructure::where('effective_date', '<=', $reservationDate)
            ->latest('effective_date')
            ->first();

        $minimumPaymentPercentage = $structure->percentages()
            ->whereHas(
                'parameter',
                function ($q) {
                    $q->where('slug', 'minimum_payment_percentage');
                }
            )->first();

        $depositAward = $structure->percentages()
            ->whereHas(
                'parameter',
                function ($q) {
                    $q->where('slug', 'deposit_award');
                }
            )->first();

        $salesAgreementAward = $structure->percentages()
            ->whereHas(
                'parameter',
                function ($q) {
                    $q->where('slug', 'sales_agreement_award');
                }
            )->first();

        $installmentStages = $structure->percentages()
            ->whereHas(
                'parameter',
                function ($q) {
                    $q->where('slug', 'installment_stages');
                }
            )->pluck('value', 'stage')->toArray();

        return (object)[
            'minimum_payment_percentage' => $minimumPaymentPercentage->value,
            'deposit_award' => $depositAward->value,
            'sales_agreement_award' => $salesAgreementAward->value,
            'installment_stages' => $installmentStages
        ];
    }
}
