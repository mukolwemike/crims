<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 11/1/17
 * Time: 11:19 AM
 */

namespace Cytonn\Realestate\Commissions\Calculator;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Crims\Investments\Commission\Models\Recipient;
use Cytonn\Investment\Commission\Projections\Projector;

class ReProjectionsCalculator extends Projector
{
    protected $recipient;
    protected $start;
    protected $end;

    public function __construct(Recipient $recipient, Carbon $start, Carbon $end)
    {
        $this->recipient = $recipient;
        $this->start = $start;
        $this->end = $end;
    }

    public function calculate(Client $client = null)
    {
        return $this->commissionProjection($client)
            ->groupBy(
                function ($schedule) {
                    return Carbon::parse($schedule->date)->startOfMonth()->toDateString();
                }
            )
            ->map(
                function ($group) {
                    return (object)[
                        'name' => 'Real Estate',
                        'short_name' => 'RE',
                        'amount' => $group->sum('amount'),
                        'month' => $group->first()->date,
                    ];
                }
            )
            ->filter(
                function ($monthsummary) {
                    $d = Carbon::parse($monthsummary->month);

                    return $d->copy()->gte($this->start->copy()->startOfMonth()) &&
                        $d->copy()->lte($this->end->copy()->endOfMonth());
                }
            );
    }

    private function commissionProjection($client)
    {
        $start = $this->start;
        $end = $this->end;
        $recipient = $this->recipient;

        $units = UnitHolding::whereHas(
            'commission',
            function ($commission) use ($recipient, $start, $end) {
                $commission->where('recipient_id', $recipient->id);
            }
        )->whereHas(
            'paymentSchedules',
            function ($schedules) use ($start, $end) {
                $schedules->where('date', '>=', $start)->where('date', '<=', $end);
            }
        );

        if ($client) {
            $units = $units->where('client_id', $client->id);
        }

        $units = $units->get();

        $commissionSchedules = $units->map(
            function (UnitHolding $unit) {
                $price = $unit->price();

                $payments = ['10' => 0.25, '20' => 0.1, '40' => 0.1, '60' => 0.1, '80' => 0.1, '100' => 0.1];

                $schedules = $unit->paymentSchedules()
                    ->inPricing()
                    ->get();

                $project = $unit->project;

                $conversion_rate = $this->convert($project, 1, $unit->commission->commission_rate);

                $totalCommission = $unit->commission->amount;

                $commissions = $schedules->each(
                    function ($schedule) use ($schedules, $price) {
                        $before = $schedules->where('date', '<=', $schedule->date)->sum('amount');

                        $schedule->cumulative = (int)(10 * floor(10 * $before / $price));
                    }
                )
                    ->filter(
                        function ($schedule) use ($payments) {
                            return in_array($schedule->cumulative, array_keys($payments));
                        }
                    )
                    ->groupBy('cumulative')
                    ->map(
                        function ($group) {
                            return $group->sortByDate('date', 'ASC')->first();
                        }
                    )
                    ->flatten()
                    ->sortByDate('date', 'ASC')
                    ->map(
                        function ($schedule) use ($payments, $totalCommission) {
                            $rate = $payments[$schedule->cumulative];

                            $schedule->commission_amt = $rate * $totalCommission;

                            return $schedule;
                        }
                    );

                $first = $commissions->filter(
                    function ($c) {
                        try {
                            return !is_null($c->date);
                        } catch (\Exception $exception) {
                            dd($c);
                        }
                    }
                )->first();

                if ($first) {
                    $loo = (object)[
                        'date' => Carbon::parse($first->date)->addDays(30),
                        'description' => 'Sales Ag Signed',
                        'commission_amt' => 0.25 * $totalCommission
                    ];

                    $commissions->push($loo);
                }

                return $commissions->map(
                    function ($commission) use ($conversion_rate) {
                        return (object)[
                            'date' => $commission->date,
                            'description' => $commission->description,
                            'amount' => $commission->commission_amt * $conversion_rate
                        ];
                    }
                )->filter(
                    function ($comm) {
                        return ($comm->date->lte($this->end)) && ($comm->date->gte($this->start));
                    }
                );
            }
        );

        return $commissionSchedules->flatten();
    }

    private function convert(Project $project, $amount, $rate)
    {
        if (is_null($this->retained)) {
            return $amount;
        }

        $converted_rate = $this->calculateRate($project, $this->getRequiredType(), $rate);

        return ($converted_rate / $rate) * $amount;
    }

    private function calculateRate($project, $type, $default)
    {
        $key = 'fa_project_rate_for_' . $project->id . '_type_' . $type;
        if (cache()->has($key)) {
            return cache()->get($key);
        }

        $rate = $project->commissionRates()->whereHas(
            'recipientType',
            function ($r) use ($type) {
                $r->where('slug', $type);
            }
        )->first();

        if ($rate) {
            cache()->put([$key => $rate->amount], 10);
            return $rate->amount;
        }

        return $default;
    }
}
