<?php

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\RealestateCommissionRate;

/**
 * Class RealEstateCommissionRateRepository
 *
 * @package Cytonn\Realestate\Commissions
 */
class RealEstateCommissionRateRepository
{

    /**
     * @return mixed
     */
    public function getAllRates()
    {
        return RealestateCommissionRate::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRateById($id)
    {
        return RealestateCommissionRate::findOrFail($id)->get();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function createRate(array $data)
    {
        $rate = new RealestateCommissionRate();
        $rate->fill($data);

        return $rate->save();
    }

    /**
     * @param array $data
     * @param $id
     * @return bool|int
     */
    public function updateRate(array $data, $id)
    {
        $commissionRate = RealestateCommissionRate::findOrFail($id);
        return $commissionRate->update($data);
    }

    /**
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function deleteRate($id)
    {
        return RealestateCommissionRate::findOrFail($id)->delete();
    }
}
