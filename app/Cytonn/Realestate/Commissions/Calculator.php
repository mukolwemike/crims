<?php
/**
 * Date: 03/08/2016
 * Time: 11:31 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\RealEstatePayment;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Realestate\Commissions\Calculator\Award;

/**
 * Class Calculator
 *
 * @package Cytonn\Realestate\Commissions
 */
class Calculator
{
    /**
     * @var array
     */
    protected $calculators = [
        'payment_plan_based' => Award::class
    ];


    /**
     * @param RealEstatePayment $payment
     * @throws ClientInvestmentException
     * @return bool
     */
    public function calculate(RealEstatePayment $payment)
    {
        $project = $payment->holding->project;

        try {
            /*
             * @var CommissionCalculatorInterface
             */
            $calculator = \App::make($this->calculators[$project->commission_calculator]);
        } catch (\Exception $e) {
            throw new ClientInvestmentException('The commission payment scheme has not been implemented');
        }

        return $calculator->calculate($payment);
    }


    /**
     * Get an array of the available calculators
     *
     * @return array
     */
    public function getCalculatorList()
    {
        $values = Collection::make(array_keys($this->calculators))->map(
            function ($item) {
                return ucwords(str_replace('_', ' ', $item));
            }
        )->all();

        return array_combine(array_keys($this->calculators), $values);
    }
}
