<?php
/**
 * Date: 16/01/2017
 * Time: 11:52
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;

class Recipients
{
    public function forDates(Carbon $start, Carbon $end)
    {
        return CommissionRecepient::active()->whereHas(
            'realEstateCommissions',
            function ($commission) use ($start, $end) {
                $commission->whereHas(
                    'schedules',
                    function ($schedule) use ($start, $end) {
                        $schedule->where('date', '>=', $start)->where('date', '<=', $end);
                    }
                );
            }
        );
    }
}
