<?php
/**
 * Date: 14/01/2017
 * Time: 14:10
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstateSalesAgreement;
use Cytonn\Realestate\Commissions\Calculator\Award;

/**
 * Class AwardHandler
 *
 * @package Cytonn\Realestate\Commissions
 */
class AwardHandler
{
    /**
     * @param RealEstatePayment $payment
     */
    public function onPaymentMade(RealEstatePayment $payment)
    {
        (new Award($payment->holding))->onDepositComplete();
        (new Award($payment->holding))->onInstallmentPaidNew($payment);
    }

    /**
     * @param RealestateLetterOfOffer $loo
     */
    public function onLooSigned(RealestateLetterOfOffer $loo)
    {
        (new Award($loo->holding))->onDepositComplete();
    }

    /**
     * @param RealEstateSalesAgreement $agreement
     */
    public function onSalesAgreementSigned(RealEstateSalesAgreement $agreement)
    {
        (new Award($agreement->holding))->onSalesAgreementSigned($agreement);
    }
}
