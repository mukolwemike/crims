<?php

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstateCommissionPayment;
use Carbon\Carbon;
use Crims\Realestate\Commission\RecipientCalculator;

class Payment
{
    protected $recipient;

    public function __construct(CommissionRecepient $recipient)
    {
        $this->recipient = $recipient;
    }

    public function make(BulkCommissionPayment $payment, ClientTransactionApproval $approval)
    {
        $calculator = (new RecipientCalculator($this->recipient, $payment->start, $payment->end));

        $summary = $calculator->summary();

        $amount = $summary->finalCommission();

        $earned = $summary->earned();

        $overrides = $summary->override();

        if ($earned == 0 && $overrides == 0) {
            return;
        }

        return (new RealEstateCommissionPayment())->create([
            'amount' => $amount,
            'earned' => $earned,
            'overrides' => $overrides,
            'description' => 'Commission Payment ' . $payment->start->toFormattedDateString() . ' - ' .
                $payment->end->toFormattedDateString(),
            'recipient_id' => $this->recipient->id,
            'date' => Carbon::parse($payment->end),
            'approval_id' => $approval->id
        ]);
    }
}
