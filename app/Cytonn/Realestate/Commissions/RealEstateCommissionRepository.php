<?php

namespace Cytonn\Realestate\Commissions;

use App\Cytonn\Models\RealestateCommission;
use App\Cytonn\Models\RealestateCommissionRate;
use App\Cytonn\Models\RealEstateSalesAgreement;
use Cytonn\Exceptions\ClientInvestmentException;

/**
 * Class RealEstateCommissionRepository
 *
 * @package Cytonn\Realestate\Commissions
 */
class RealEstateCommissionRepository
{

    /**
     * @return mixed
     */
    public function getAllCommissions()
    {
        return RealestateCommission::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCommissionById($id)
    {
        return RealestateCommission::findOrFail($id)->get();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function createCommission(array $data)
    {
        $rate = new RealestateCommission();
        $rate->fill($data);

        return $rate->save();
    }

    /**
     * @param array $data
     * @param $id
     * @return bool|int
     */
    public function updateCommission(array $data, $id)
    {
        $commissionCommission = RealestateCommission::findOrFail($id);
        return $commissionCommission->update($data);
    }

    /**
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function deleteCommission($id)
    {
        return RealestateCommission::findOrFail($id)->delete();
    }

    /**
     * Save commission amount
     *
     * @param  RealEstateSalesAgreement $salesAgreement
     * @throws ClientInvestmentException
     */
    public function initCommissionOnSalesAgreement(RealEstateSalesAgreement $salesAgreement)
    {
        $holding = $salesAgreement->holding;
        $price = $holding->price();
        $recipient_type_id = $holding->commission->recipient->recipient_type_id;

        if (!$holding->commission->commission_rate_id) {
            $rate = $holding->project->commissionRates()->where('recipient_type_id', $recipient_type_id)->first();
        } else {
            $rate = RealestateCommissionRate::findOrFail($holding->commission->commission_rate_id);
        }

        if (!$holding->commission->awarded) {
            $amount = 0;
        } elseif ($rate->type == 'percentage') {
            $amount = $price * $rate->amount / 100;
        } elseif ($rate->type == 'amount') {
            $amount = $rate->amount;
        } else {
            throw new ClientInvestmentException('The commission payment rate type is not supported');
        }

        $holding->commission->amount = $amount;
        $holding->commission->save();

        //check if enough payments have been made to award commission
        $latestPayment = $holding->payments()->latest('date')->first();

        if (!is_null($latestPayment)) {
            (new Calculator())->calculate($latestPayment);
        }
    }
}
