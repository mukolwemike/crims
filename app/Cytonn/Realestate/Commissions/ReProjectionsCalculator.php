<?php

namespace App\Cytonn\RealEstate\Commissions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use function array_keys;
use function cache;
use function floor;
use function in_array;
use function strtotime;

class ReProjectionsCalculator
{
    protected $recipient;

    protected $client;
    protected $start;

    protected $end;

    public function __construct(CommissionRecepient $recipient, Client $client, Carbon $start, Carbon $end)
    {
        $this->recipient = $recipient;
        $this->client = $client;
        $this->start = $start;
        $this->end = $end;
    }

    public function reProjections()
    {
        return $this->commissionProjection()->groupBy(
            function ($schedule) {
                return (string)Carbon::parse($schedule->date)->month;
            }
        )
            ->map(
                function ($group) {
                    return (object)[
                        'name' => 'Real Estate',
                        'short_name' => 'RE',
                        'on_retainer' => $group->sum('on_retainer'),
                        'off_retainer' => $group->sum('off_retainer'),
                        'month' => $group->first()->date,
                    ];
                }
            )
            ->filter(
                function ($monthsummary) {
                    $d = Carbon::parse($monthsummary->month);
                    return $d->copy()
                            ->gte($this->start->copy()->startOfMonth()) &&
                        $d->copy()->lte($this->end->copy()->endOfMonth());
                }
            );
    }

    public function commissionProjection()
    {
        $start = $this->start;
        $end = $this->end;
        $recipient = $this->recipient;

        $units = (new UnitHolding())->whereHas(
            'commission',
            function ($commission) use ($recipient, $start, $end) {
                $commission->where('recipient_id', $recipient->id);
            }
        )->whereHas(
            'paymentSchedules',
            function ($schedules) use ($start, $end) {
                $schedules->where('date', '>=', $start)->where('date', '<=', $end);
            }
        )->get();

        $commissionSchedules = $units->map(
            function (UnitHolding $unit) {
                $price = $unit->price();

                $rate = function ($slug) use ($unit) {
                    $key = 'fa_project_rate_for_' . $unit->project->id . '_type_' . $slug;
                    if (cache()->has($key)) {
                        return cache()->get($key);
                    }
                    $rate = $unit->project->commissionRates()->whereHas(
                        'recipientType',
                        function ($r) use ($slug) {
                            $r->where('slug', $slug);
                        }
                    )->first();

                    if ($rate) {
                        cache()->put([$key => $rate->amount], 10);
                        return $rate->amount;
                    }
                    return 0;
                };

                $off_retainer_commission = $rate('ifa') * $price / 100;
                $on_retainer_commission = $rate('fa') * $price / 100;

                $payments = ['10' => 0.25, '20' => 0.1, '40' => 0.1, '60' => 0.1, '80' => 0.1, '100' => 0.1];

                $schedules = $unit->paymentSchedules()->inPricing()->get();
                $commissions = $schedules->each(
                    function ($schedule) use ($schedules, $price) {
                        $before = $schedules->where('date', '<=', $schedule->date)->sum('amount');
                        $schedule->cumulative = (int)(10 * floor(10 * $before / $price));
                    }
                )
                    ->filter(
                        function ($schedule) use ($payments) {
                            return in_array($schedule->cumulative, array_keys($payments));
                        }
                    )
                    ->groupBy('cumulative')
                    ->map(
                        function ($group) {
                            return $group->sortBy(
                                function ($sort) {
                                    return strtotime($sort->date);
                                }
                            )->first();
                        }
                    )
                    ->sortBy(
                        function ($sort) {
                            return strtotime($sort->date);
                        }
                    )
                    ->each(
                        function ($schedule) use ($payments, $on_retainer_commission, $off_retainer_commission) {
                            $rate = $payments[$schedule->cumulative];
                            $schedule->on_retainer = $rate * $on_retainer_commission;
                            $schedule->off_retainer = $rate * $off_retainer_commission;
                        }
                    );

                $first = $commissions->first();
                if ($first) {
                    $sa = (object)[
                        'date' => Carbon::parse($first->date)->addDays(30)->toDateString(),
                        'description' => 'Sales Ag Signed',
                        'on_retainer' => 0.25 * $on_retainer_commission,
                        'off_retainer' => 0.25 * $off_retainer_commission];
                    $commissions->push($sa);
                }
                return $commissions->map(
                    function ($commission) {
                        return (object)[
                            'date' => $commission->date,
                            'description' => $commission->description,
                            'on_retainer' => $commission->on_retainer,
                            'off_retainer' => $commission->off_retainer
                        ];
                    }
                );
            }
        );

        return $commissionSchedules->flatten();
    }
}
