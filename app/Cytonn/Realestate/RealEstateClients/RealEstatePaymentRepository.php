<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/12/19
 * Time: 3:45 PM
 */

namespace App\Cytonn\Realestate\RealEstateClients;

use App\Cytonn\Api\Transformers\Clients\RealEstatePaymentTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Core\DataStructures\Carbon;

class RealEstatePaymentRepository
{
    use AlternateSortFilterPaginateTrait;

    public function overduePayments(Client $client, Carbon $date)
    {
        return $this->sortFilterPaginate(

            new RealEstatePaymentSchedule(),
            [],
            function ($schedule) {
                return app(RealEstatePaymentTransformer::class)->transform($schedule);
            },
            function ($model) use ($client, $date) {
                return $model->whereHas('holding', function ($holding) use ($client) {
                    $holding->where('client_id', $client->id);
                })
                    ->where('date', '<', $date->copy())
                    ->where('paid', '!=', 1);
            }
        );
    }

    public function upcomingPayments(Client $client, Carbon $date)
    {
        return $this->sortFilterPaginate(

            new RealEstatePaymentSchedule(),
            [],
            function ($schedule) {
                return app(RealEstatePaymentTransformer::class)->transform($schedule);
            },
            function ($model) use ($client, $date) {
                return $model->whereHas('holding', function ($holding) use ($client) {
                    $holding->where('client_id', $client->id);
                })
                    ->where('date', '>=', $date->copy())
                    ->orderBy('date', 'asc');
            }
        );
    }
}
