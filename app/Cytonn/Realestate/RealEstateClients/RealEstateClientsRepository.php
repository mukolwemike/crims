<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-02-20
 * Time: 09:39
 */

namespace App\Cytonn\Realestate\RealEstateClients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Realestate\Statements\StatementGenerator;

class RealEstateClientsRepository
{
    use AlternateSortFilterPaginateTrait;

    protected $client;

    protected $project;

    protected $date_from;

    protected $date_to;

    protected $user;

    public function __construct(Client $client, Project $project, Carbon $date_to, $date_from = null)
    {
        $this->client = $client;

        $this->project = $project;

        $this->date_from = $date_from ? Carbon::parse($date_from) : null;

        $this->date_to = $date_to;
    }

    public function loadStatement()
    {
        return (new StatementGenerator())->html(
            $this->client,
            $this->project,
            $this->date_to,
            $this->date_from
        );
    }

    public function downloadStatement()
    {
        return (new StatementGenerator())->clientGenerate(
            $this->client,
            $this->project,
            $this->date_to,
            $this->date_from
        )->stream();
    }
}
