<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Realestate\RealEstateUnitGroups;

use Cytonn\Rules\Rules;

trait RealEstateUnitGroupRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate the creation of a unit group
     */
    public function unitGroupCreate($request)
    {
        $rules = [
            'name' => 'required | unique:real_estate_unit_groups',
            'project_id' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate the edit of a unit group
     */
    public function unitGroupUpdate($request, $id)
    {
        $rules = [
            'name' => 'required | unique:real_estate_unit_groups,name,' . $id,
            'project_id' => 'required'
        ];

        return $this->verdict($request, $rules);
    }
}
