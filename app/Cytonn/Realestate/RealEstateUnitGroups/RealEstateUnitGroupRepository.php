<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Realestate\RealEstateUnitGroups;

use Cytonn\Models\RealEstateUnitGroup;

class RealEstateUnitGroupRepository
{
    /*
     * Get a given real estate unit group by id
     */
    public function getRealEstateUnitGroupById($id)
    {
        return RealEstateUnitGroup::findOrFail($id);
    }

    /*
     * Store or update a real estate unit group
     */
    public function save($input, $id = null)
    {
        if ($id) {
            $realEstateUnitGroup = $this->getRealEstateUnitGroupById($id);

            $realEstateUnitGroup->update($input);

            return $realEstateUnitGroup;
        }

        return RealEstateUnitGroup::create($input);
    }
}
