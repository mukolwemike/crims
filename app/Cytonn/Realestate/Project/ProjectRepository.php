<?php

namespace Cytonn\Realestate\Project;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstateRefund;
use App\Cytonn\Models\RealEstateSalesAgreement;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Class ProjectRepository
 *
 * @package Cytonn\Realestate\Project
 */
class ProjectRepository
{
    /**
     * @var Project
     */
    protected $project;

    /**
     * ProjectRepository constructor.
     *
     * @param $project
     */
    public function __construct(Project $project = null)
    {
        is_null($project)?
            $this->project = new Project() :
            $this->project = $project;
    }


    /**
     * Count the number of units that have been reserved
     *
     * @return mixed
     */
    public function countReservedUnits($date = null)
    {
        return UnitHolding::whereHas(
            'unit',
            function ($unit) {
                $unit->where('project_id', $this->project->id);
            }
        )->where('active', 1)->count();
    }

    /*
     * Count the reserved units as at a specific date
     */
    public function countReservedUnitsAsAt(Carbon $date)
    {
        $holdings = UnitHolding::whereHas(
            'unit',
            function ($unit) {
                $unit->where('project_id', $this->project->id);
            }
        );

        if ($date) {
            $holdings->where('reservation_date', '<=', $date)->activeAtDate($date);
        }

        return $holdings->count();
    }

    /**
     * Count the total number of units in the project
     *
     * @return mixed
     */
    public function countNumberOfUnits()
    {
        return $this->project->sizeNumbers()->sum('number');
    }

    /**
     * Get the total amount paid for this project
     *
     * @return mixed
     */
    public function sumOfPayments()
    {
        return RealEstatePayment::whereHas(
            'holding',
            function ($holding) {
                $holding->whereHas(
                    'unit',
                    function ($unit) {
                        $unit->where('project_id', $this->project->id);
                    }
                );
            }
        )->sum('amount');
    }

    public function sumOfRefunds()
    {
        return RealEstateRefund::whereHas(
            'holding',
            function ($holding) {
                $holding->whereHas(
                    'unit',
                    function ($unit) {
                        $unit->where('project_id', $this->project->id);
                    }
                );
            }
        )->sum('amount');
    }

    /*
     * Sum of all the loos signed for the project
     */
    public function sumOfLoosSignedAsAt(Carbon $date, $active = true)
    {
        return RealestateLetterOfOffer::forProject($this->project, $active, $date)
            ->where('date_signed', '<=', $date)->count();
    }

    /*
     * Sum of all the loos total for the project
     */
    public function sumOfLoosTotalAsAt(Carbon $date, $active = true)
    {
        return RealestateLetterOfOffer::forProject($this->project, $active, $date)->count();
    }

    /*
     * Sum of all the loos signed for the project
     */
    public function sumOfLoosSentAsAt(Carbon $date, $active = true)
    {
        return RealestateLetterOfOffer::forProject($this->project, $active, $date)
            ->where('sent_on', '<=', $date->copy()->endOfDay())->count();
    }

    /*
     * Sum of all the sas signed for the project
     */
    public function sumOfSasSignedAsAt(Carbon $date, $active = true)
    {
        return RealEstateSalesAgreement::forProject($this->project, $active, $date)
            ->where('date_signed', '<=', $date->copy()->endOfDay())->count();
    }

    /*
     * Sum of all the sas uploaded for the project
     */
    public function sumOfSasUploadedAsAt(Carbon $date, $active = true)
    {
        return RealEstateSalesAgreement::forProject($this->project, $active, $date)
            ->where('uploaded_on', '<=', $date->copy()->endOfDay())->count();
    }

    /*
     * Sum of all the loos total for the project
     */
    public function sumOfSasTotalAsAt(Carbon $date, $active = true)
    {
        return RealEstateSalesAgreement::forProject($this->project, $active, $date)->count();
    }

    /*
     * Total Sales for a project as at a given date
     */
    public function totalSalesAsAt(Carbon $date)
    {
        return UnitHolding::activeAtDate($date)->inProjectIds([$this->project->id])
            ->get()
            ->sum(function ($holding) {
                return $holding->price();
            });
    }

    /*
     * Get total payments for a project as at a given date
     */
    public function totalPaymentsAsAt(Carbon $date, $typeId = null)
    {
        $payments = RealEstatePayment::forProjects([$this->project->id])
            ->before($date)
            ->whereHas('holding', function ($q) use ($date) {
                $q->activeAtDate($date);
            });

        if ($typeId) {
            $payments->where('payment_type_id', $typeId);
        }

        return $payments->sum('amount');
    }

    /*
     * Get the total overdue payments for a given project
     */
    public function totalOverduePaymentsAsAt(Carbon $date)
    {
        return UnitHolding::activeAtDate($date)->inProjectIds([$this->project->id])
            ->get()
            ->sum(function ($holding) {
                return $holding->repo->amountOverdue();
            });
    }
    
    /*
     * Get the total overdue holdings for a given project
     */
    public function totalOverdueHoldings(Carbon $date)
    {
        return UnitHolding::activeAtDate($date)->inProjectIds([$this->project->id])
            ->get()
            ->filter(function (UnitHolding $holding) {
                return $holding->repo->isOverdue();
            });
    }
}
