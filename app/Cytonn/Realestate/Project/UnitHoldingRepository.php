<?php
/**
 * Date: 21/12/2016
 * Time: 12:29
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Project;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Realestate\Payments\Interest;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UnitHoldingRepository
 *
 * @package Cytonn\Realestate\Project
 */
class UnitHoldingRepository
{
    /**
     * @var UnitHolding
     */
    protected $holding;

    protected $totalPaid;

    protected $nextPayments = array();

    /**
     * UnitHoldingRepository constructor.
     *
     * @param $holding
     */
    public function __construct(UnitHolding $holding = null)
    {
        is_null($holding) ? $this->holding = new UnitHolding() : $this->holding = $holding;
    }

    /**
     * @param UnitHolding|null $holding
     * @return Collection
     */
    public function overDueSchedules()
    {
        return $this->holding->paymentSchedules()->inPricing()
            ->isLast(false)
            ->get()->filter(
                function (RealEstatePaymentSchedule $schedule) {
                    return !$schedule->paid();
                }
            );
    }

    public function currentLastSchedule()
    {
        return $this->holding->paymentSchedules()
            ->isLast()
            ->first();
    }

    public function lastSchedule()
    {
        return $this->holding->paymentSchedules()
            ->inPricing()
            ->accruesInterest()
            ->isServiceCharge(false)
            ->orderBy('date', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->first();
    }

    public function setLastSchedule()
    {
        $this->holding->paymentSchedules()->where('last_schedule', 1)->update(['last_schedule' => 0]);

        $lastSchedule = $this->lastSchedule();

        if ($lastSchedule) {
            $lastSchedule->last_schedule = 1;
            $lastSchedule->save();
        }

        return $lastSchedule;
    }

    /*
     * Check if the unit holding is overdue or not
     */
    public function isOverdue(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        $schedules = $this->holding->paymentSchedules()->inPricing()
            ->isLast(false)
            ->before($date)->orderBy('date')->get();

        foreach ($schedules as $schedule) {
            if (!$schedule->paid()) {
                return true;
            }
        }

        return false;
    }

    /*
     * Get the overdue date for a holding
     */
    public function getDateOverdue(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        $schedules = $this->holding->paymentSchedules()->inPricing()
            ->isLast(false)
            ->before($date)->orderBy('date')->get();

        foreach ($schedules as $schedule) {
            if (!$schedule->paid()) {
                return Carbon::parse($schedule->date);
            }
        }

        return null;
    }

    /**
     * @return number
     */
    public function interestAccrued()
    {
        return $this->overDueSchedules()->sum(
            function (RealEstatePaymentSchedule $schedule) {
                return $schedule->repo->interestAccrued();
            }
        );
    }

    /**
     * @param UnitHolding|null $holding
     * @return number
     */
    public function amountOverdue()
    {
        return $this->overDueSchedules()->sum(
            function (RealEstatePaymentSchedule $schedule) {
                return $schedule->amountNotPaid();
            }
        );
    }

    /**
     * @param $percentage
     * @return null
     */
    public function paymentPercentageMet($percentage)
    {
        $amount = ($percentage / 100) * $this->holding->price();

        $payments = $this->holding->payments()->oldest('date')->get();

        foreach ($payments as $payment) {
            $amount_paid = $this->holding->payments()->before($payment->date)->sum('amount');
            $refund = $this->holding->refunds()->before($payment->date)->sum('amount');

            if ($amount_paid - $refund >= $amount) {
                return $payment;
            }
        }

        return null;
    }

    public function getClientUnitHoldings(Client $client)
    {
        $holdings = $client->unitHoldings()
            ->whereHas('unit')
            ->whereNull('forfeit_date')
            ->where('active', 1)
            ->get();

        $units = $holdings->map(function ($holding) {

            $date = Carbon::today();

            $nextPayment = $this->unitNextPayment($holding);

            $totalPaid = $this->unitTotalPaid($holding);

            $this->totalPaid += $totalPaid;

            $unit_price = $holding->price();

            $outstanding_balance = ($unit_price - (int)$this->totalPaid);

            $balance = ($unit_price - (int)$totalPaid);

            $overdue_amount = $this->amountOverdue($holding);

            return [
                'unit_id' => $holding->unit->id,
                'unit_name' => $holding->unit->number,
                'unit_size' => $holding->unit->size->name,
                'payment_plan' => ($holding->paymentPlan) ? $holding->paymentPlan->name : '',
                'unit_price' => (int)$holding->price(),
                'tranche' => ($holding->tranche) ? $holding->tranche->name : '',
                'reservation_date' => Carbon::parse($holding->reservation->date())->toFormattedDateString(),
                'deposit' => $this->unitDeposit($holding),
                'total_purchase_price' => $unit_price,
                'installments' => $this->unitInstallments($holding),
                'booking_fees' => AmountPresenter::currency((int)$this->unitBookingFees($holding), false, 0),
                'next_payment_amount' => !is_null($p = $nextPayment) ? $p->amount : null,
                'next_payment_date' => !is_null($p = $nextPayment)
                    ? Carbon::parse($p->date)->toFormattedDateString() : null,
                'total_paid' => AmountPresenter::currency($this->totalPaid, false, 0),
                'paid' => $this->totalPaid,
                'outstanding_balance' => AmountPresenter::currency($outstanding_balance, false, 0),
                'balance' => AmountPresenter::currency($balance, false, 0),
                'payments' => $this->getUnitPaymentActions($holding),
                'project' => $holding->project ? $holding->project->name : null,
                'overdue_amount' => $this->overDue($holding)
            ];
        })->groupBy('project');

        $this->nextPayments = collect($this->nextPayments)->first();

        $totals = [
            'investments' => $holdings->count(),
            'total_paid' => $this->totalPaid,
            'next_payment_amount' => (!is_null($this->nextPayments)) ? $this->nextPayments->amount : null,
            'next_payment_date' => (!is_null($this->nextPayments)) ?
                Carbon::parse($this->nextPayments->date)->toFormattedDateString() : null
        ];

        return ['realEstateDetails' => $units, 'realEstateTotals' => $totals];
    }

    private function overDue(UnitHolding $holding)
    {
        $date = Carbon::today();

        return $holding->paymentSchedules()
            ->inPricing()
            ->accruesInterest()
            ->before($date)
            ->remember(Interest::CACHE)
            ->get()
            ->sum(
                function (RealEstatePaymentSchedule $schedule) use ($date) {
                    return $schedule->last_schedule == 1 ? 0 : $schedule->repo->overdue($date);
                }
            );
    }

    public function unitDeposit(UnitHolding $holding)
    {
        return $holding->payments()->whereHas('type', function ($type) {
            $type->where('slug', 'deposit');
        })->where('date', '<=', Carbon::today())->sum('amount');
    }

    public function unitDepositDate(UnitHolding $holding)
    {
        return $holding->payments()->whereHas('type', function ($type) {
            $type->where('slug', 'deposit');
        })->where('date', '<=', Carbon::today())->sum('amount');
    }

    public function unitInstallments(UnitHolding $holding)
    {
        return $holding->payments()->whereHas('type', function ($type) {
            $type->where('slug', 'installment');
        })->where('date', '<=', Carbon::today())->sum('amount');
    }

    public function unitBookingFees(UnitHolding $holding)
    {
        return $holding->payments()->whereHas('type', function ($type) {
            $type->where('slug', 'reservation_fee');
        })->where('date', '<=', Carbon::today())->sum('amount');
    }

    public function unitOtherFees(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::today();

        return $this->holding->payments()->whereHas(
            'type',
            function ($type) {
                $type->whereNotIn('slug', ['deposit', 'installment', 'reservation_fee']);
            }
        )->where('date', '<=', $date)->sum('amount');
    }

    public function unitTotalPaid($holding, $date = null, $date_from = null)
    {
        $date = $date ? Carbon::parse($date) : Carbon::today();

        $payments = $holding->payments()->before($date);

        if ($date_from) {
            $payments = $payments->where('date', '>=', $date_from);
        }

        return $payments->sum('amount');
    }

    public function netUnitTotalPaid($date = null, $date_from = null)
    {
        $date = is_null($date) ? Carbon::today() : Carbon::parse($date);

        $totalRefund = $this->holding->refundedAmount($date, $date_from);

        return $this->unitTotalPaid($this->holding, $date, $date_from) - $totalRefund;
    }

    public function unitNextPayment($holding)
    {
        $paid = $holding->payments()->before(Carbon::today())->sum('amount');

        $nextUnpaidSchedule = function ($date) use (&$nextUnpaidSchedule, $holding, $paid) {
            $nextSchedule = $holding->paymentSchedules()
                ->where('date', '>', $date)
                ->oldest('date')
                ->first();

            if (!$nextSchedule) {
                return null;
            }

            $sumOfPast = $holding->paymentSchedules()
                ->inPricing()
                ->before($nextSchedule->date)
                ->remember(Interest::CACHE)
                ->get()
                ->sum(function (RealEstatePaymentSchedule $schedule) {
                    return $schedule->repo->total(Carbon::today());
                });

            return $sumOfPast > $paid ? $nextSchedule : $nextUnpaidSchedule($nextSchedule->date);
        };

        $schedule = $nextUnpaidSchedule(Carbon::today());

        if ($schedule) {
            $schedule->amount = $schedule->repo->overdue($schedule->date, true);
        }

        array_push($this->nextPayments, $schedule);

        return $schedule;
    }

    public function getUnitPaymentActions($holding)
    {
        return $holding->payments->map(function ($payment) {
            return [
                'id' => $payment->id,
                'date' => $payment->date,
                'description' => $payment->description,
                'amount' => (int)$payment->amount,
                'status' => $payment->schedule->paid(),
                'payment' => $payment->type->name
            ];
        });
    }

    public function currentActiveFeitureNotice()
    {
        return $this->holding->realestateForfeitureNotices()->reviewed(false)->latest()->first();
    }

    public function getFirstUnitPaymentDate()
    {
        $percentage = 0.1;
        $sum = 0;

        $tenPercent = $percentage * $this->holding->price();
        $payments = $this->holding->payments;

        if ($payments->sum('amount') < $tenPercent) {
            return;
        }

        while ($sum <= $tenPercent) {
            foreach ($payments as $payment) {
                $sum += $payment->amount;

                if ($sum >= $tenPercent) {
                    return $payment->date;
                }
            }
        }

        return;
    }

    public function commissionPaid()
    {
        return RealEstateCommissionPaymentSchedule::whereHas('commission', function ($q) {
            $q->where('holding_id', $this->holding->id);
        })->sum('amount');
    }

    /**
     * @return Carbon|null
     */
    public function getReservationDate()
    {
        if (isNotEmptyOrNull($this->holding->reservation_date)) {
            return Carbon::parse($this->holding->reservation_date);
        }

        $firstPayment = $this->holding->payments()->oldest('date')->first();

        return $firstPayment ? Carbon::parse($firstPayment->date) : null;
    }

    /**
     * @return mixed
     */
    public function getTotalUnpaidSchedulesAmount()
    {
        return $this->holding->paymentSchedules()->inPricing()->get()
            ->sum(function (RealEstatePaymentSchedule $schedule) {
                return $schedule->paid() ? 0 : $schedule->amountNotPaid();
            });
    }

    /**
     * @return |null
     * @throws \Exception
     */
    public function getCashPrice()
    {
        if (is_null($this->holding->unitTranche)) {
            return null;
        }

        $cashPlan = getRealEstateCashPlan();

        try {
            return $this->holding->unitTranche->repo->getUnitPrice($this->holding->unit, $cashPlan);
        } catch (\Exception $e) {
            return null;
        }
    }
}
