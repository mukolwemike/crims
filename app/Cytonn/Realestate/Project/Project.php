<?php

namespace App\Cytonn\RealEstate\Project;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\RealestateCommissionRate;

class Project extends BaseModel
{
    protected $table = 'projects';
    
    public function commissionRates()
    {
        return $this->hasMany(RealestateCommissionRate::class, 'project_id');
    }
}
