<?php

namespace Cytonn\Realestate\Commands;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\ReservationForm;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Investment\JointInvestorAddCommand;
use Cytonn\Realestate\Events\UnitHasBeenReserved;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;
use function array_key_exists;

/**
 * Class ReserveRealEstateUnitCommandHandler
 *
 * @package Cytonn\Realestate\Commands
 */
class ReserveRealEstateUnitCommandHandler implements CommandHandler
{
    use DispatchableTrait, ValidatorTrait, CommanderTrait;

    /**
     * @var bool
     */
    protected $creatingReservation = false;

    /**
     * Handle the command
     *
     * @param $command
     *
     * @return bool
     * @throws \Throwable
     */
    public function handle($command)
    {
        \DB::transaction(
            function () use ($command) {
                $form = $this->reserve($command->data);

                //set false when updating
                $this->creatingReservation = true;

                $form->raise(new UnitHasBeenReserved($form, $this->creatingReservation));

                $this->dispatchEventsFor($form);
            }
        );

        return true;
    }


    /**
     * Add a new reservation form
     *
     * @param  $data
     * @return ReservationForm $form
     * @throws ClientInvestmentException
     */
    protected function reserve($data)
    {
        //Create client, contact and bank account
        $client = $this->getClientFromData(array_except($data, ['penalty_exempt']));

        $this->checkUnitReserved($data['unit_id']);

        if (!array_key_exists('penalty_exempt', $data)) {
            $data['penalty_exempt'] = 0;
        }

        //Create the unit holding
        $holding = $this->createHolding($client->id, $data['unit_id'], $data['penalty_exempt']);

        //create holders for client
        (new ClientApplicationRepository())->saveJointHolders($data);

//        // Check where to save data...
//        unset($data['new_client']);
//        unset($data['existing_client']);
//        unset($data['unit_id']);
//        unset($data['individual']);
//        unset($data['complete']);
//        unset($data['franchise']);
//        unset($data['funds_source_id']);
//        unset($data['funds_source_other']);
//        unset($data['type']);
//        unset($data['holders']);
//        unset($data['processType']);
//        unset($data['employment_id']);
//        unset($data['dob']);
//        unset($data['gender_id']);
//        unset($data['method_of_contact_id']);
//        unset($data['kin_name']);
//        unset($data['kin_postal']);
//        unset($data['kin_email']);
//        unset($data['kin_phone']);
//        unset($data['risk_reason']);
//        unset($data['registered_name']);
//        unset($data['corporate_investor_type']);
//        unset($data['trade_name']);
//        unset($data['contacts']);
//        unset($data['holders']);

        //Create the reservation form
        $form_data = $this->filter($data, ReservationForm::getTableColumnsAsArray());
        $form = new ReservationForm();
        $form->fill($form_data);
        $form->holding_id = $holding->id;
        $form->save();

        return $form;
    }

    /**
     * @param $clientId
     * @param $unitId
     * @param $penalty_excempt
     * @return UnitHolding
     */
    protected function createHolding($clientId, $unitId, $penalty_excempt)
    {
        $holding = new UnitHolding();

        $holding->client_id = $clientId;

        $holding->unit_id = $unitId;

        $holding->penalty_excempt = $penalty_excempt;

        $holding->active = true;

        $holding->save();

        return $holding;
    }

    /**
     * @param $unitId
     * @throws ClientInvestmentException
     */
    protected function checkUnitReserved($unitId)
    {
        $holding = UnitHolding::where('unit_id', $unitId)->where('active', 1)->first();

        if ($holding) {
            throw new ClientInvestmentException('The unit has an existing reservation');
        }
    }

    /**
     * Create or get the client from the reservation form
     *
     * @param  array $data
     * @throws ClientInvestmentException
     * @return Client
     */
    private function getClientFromData(array $data)
    {
        //Handle existing clients
        if ($data['existing_client']) {
            $client = $this->getExistingClient($data);

            if (is_null($client)) {
                throw new ClientInvestmentException("The client does not exist");
            }

            return $client;
        }

        //Check if another client exists with these details
        $repo = new ClientRepository();

        $client = null;

        if (isset($data['client_id'])) {
            $client = Client::find($data['client_id']);
        }

        if ($client) {
            $repo = $client->repo;
        }

        //Handle new clients and ignore duplicate clients
        if ($data['new_client'] != 3) {
            $duplicate = $repo->duplicate($data)->exists;

            if ($duplicate) {
                throw new ClientInvestmentException('Another client already exists with these details.');
            }
        }

        //Save client and contact record
        unset($data['client_id']);

        $client_data = $this->filter($data, Client::getTableColumnsAsArray());

        $client = new Client();

        $client->fill($client_data);

        $client->fund_manager_id = RealestateUnit::find($data['unit_id'])->project->fund_manager_id;

        $client->contact()->associate($this->createContactInformation($data));

        $client->save();

        if (isset($data['contacts'])) {
            $client->contactPersons()->createMany($data['contacts']);
        }

        if (isset($data['holders'])) {
            $this->saveJointHolders($client, $data['holders']);
        }

        if (array_key_exists('branch_id', $data) && $data['branch_id'] && $data['investor_account_name'] &&
            $data['investor_account_number']) {
            // Save bank branch
            $bankAccount = new ClientBankAccount(
                [
                    'branch_id' => $data['branch_id'],
                    'account_name' => $data['investor_account_name'],
                    'account_number' => $data['investor_account_number'],
                    'default' => 1
                ]
            );
            $client->bankAccounts()->save($bankAccount);
        }

        if (!$client->client_code) {
            (new ApplicationsRepository())->setClientCode($client);
        }

        return $client;
    }

    public function saveJointHolders(Client $client, $holders)
    {
        collect($holders)->each(
            function ($holder) use ($client) {

                $holder = array_add($holder, 'joint_id', null);
                $holder = array_add($holder, 'client_id', $client->id);

                $holder['existing_client_id'] = $client->id;

                return $this->execute(JointInvestorAddCommand::class, ['data' => $holder]);
            }
        );
    }

    /**
     * @param array $data
     * @return Client|null
     */
    private function getExistingClient(array $data)
    {
        if (isset($data['client_id'])) {
            return Client::find($data['client_id']);
        }

        if (isset($data['client_code'])) {
            return Client::where('client_code', $data['client_code'])->first();
        }

        return null;
    }

    /**
     * @param $data
     * @return Contact
     */
    private function createContactInformation($data)
    {
        $data['corporate_registered_name'] = (isset($data['registered_name'])) ? $data['registered_name'] : '';
        $data['corporate_trade_name'] = (isset($data['trade_name'])) ? $data['trade_name'] : '';

        $contact_data = $this->filter($data, Contact::getTableColumnsAsArray());

        $contact_data['entity_type_id'] = $data['client_type_id'];

        unset($contact_data['client_type_id']);

        return Contact::create($contact_data);
    }
}
