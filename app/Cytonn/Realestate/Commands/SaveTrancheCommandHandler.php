<?php

namespace Cytonn\Realestate\Commands;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstateType;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\RealEstateUnitTranchePricing;
use App\Cytonn\Models\RealEstateUnitTrancheSizing;
use Laracasts\Commander\CommandHandler;

class SaveTrancheCommandHandler implements CommandHandler
{

    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        $project = Project::findOrFail($command->project_id);

        $tranche = RealEstateUnitTranche::findOrNew($command->tranche_id);
        $tranche->name = $command->name;
        $tranche->date = $command->date;
        $tranche->project_id = $command->project_id;
        $tranche->save();

        foreach ($command->tranches as $tranche_sizing) {
            $size = new RealEstateUnitTrancheSizing();

            if (isset($tranche_sizing['sizing_id'])) {
                $size = RealEstateUnitTrancheSizing::findOrFail($tranche_sizing['sizing_id']);
            }

            if (isset($tranche_sizing['type_id'])) {
                $tranche_sizing['type_id'] = $project->projectTypes()
                    ->where('type_id', $tranche_sizing['type_id'])->first()->id;
            }

            if (isset($tranche_sizing['floor_id'])) {
                $tranche_sizing['floor_id'] = $project->projectFloors()
                    ->where('floor_id', $tranche_sizing['floor_id'])->first()->id;
            }

            $size->fill([
                'tranche_id' => $tranche->id,
                'number' => $tranche_sizing['number'],
                'size_id' => $tranche_sizing['size_id'],
                'project_floor_id' => isset($tranche_sizing['floor_id']) ? $tranche_sizing['floor_id'] : null,
                'real_estate_type_id' => isset($tranche_sizing['type_id']) ? $tranche_sizing['type_id'] : null
            ]);

            $size->save();

            foreach ($tranche_sizing['prices'] as $tranche_pricing) {
                $pricing = new RealEstateUnitTranchePricing();

                if (isset($tranche_pricing['price_id'])) {
                    $pricing = RealEstateUnitTranchePricing::findOrFail($tranche_pricing['price_id']);
                }

                $pricing->payment_plan_id = $tranche_pricing['payment_plan_id'];
                $pricing->sizing_id = $size->id;
                $pricing->price = $tranche_pricing['price'];
                $pricing->value = $tranche_pricing['value'];

                $pricing->save();
            }
        }
    }
}
