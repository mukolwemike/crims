<?php

namespace Cytonn\Realestate\Commands;

class ReserveRealEstateUnitCommand
{
    public $data;

    /**
     * ReserveRealEstateUnitCommand constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
}
