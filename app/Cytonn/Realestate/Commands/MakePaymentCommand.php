<?php

namespace Cytonn\Realestate\Commands;

class MakePaymentCommand
{
    public $amount;

    public $description;

    public $date;

    public $payment_type_id;

    /**
     * MakePaymentCommand constructor.
     *
     * @param $amount
     * @param $date
     * @param $description
     * @param $payment_type_id
     */
    public function __construct($amount, $date, $description, $payment_type_id)
    {
        $this->amount = $amount;
        $this->date = $date;
        $this->description = $description;
        $this->payment_type_id = $payment_type_id;
    }
}
