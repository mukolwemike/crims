<?php

namespace Cytonn\Realestate\Commands;

class SaveTrancheCommand
{
    public $name;

    public $date;

    public $project_id;

    public $tranches;

    public $tranche_id = null;

    /**
     * SaveTrancheCommand constructor.
     *
     * @param $date
     * @param $name
     * @param $project_id
     * @param $tranches
     */
    public function __construct($date, $name, $project_id, $tranches, $tranche_id = null)
    {
        $this->date = $date;
        $this->name = $name;
        $this->project_id = $project_id;
        $this->tranches = $tranches;
        $this->tranche_id = $tranche_id;
    }
}
