<?php
/**
 * Date: 06/10/2016
 * Time: 12:18
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Reports;

use App\Cytonn\Models\Client;
use Cytonn\Mailers\RealEstate\PaymentPlanMailer;

/**
 * Class PaymentPlan
 *
 * @package Cytonn\Realestate\Reports
 */
class PaymentPlan
{
    /**
     * @param Client $client
     * @return mixed
     */
    public function generate(Client $client)
    {
        return \PDF::loadView('realestate.reports.payment_plan', [
            'client' => $client,
            'sender' => \Auth::user(),
            'logo' => $client->fundManager->re_logo
        ]);
    }

    /**
     * @param Client $client
     */
    public function send(Client $client)
    {
        $document = $this->generate($client);

        (new PaymentPlanMailer())->sendPlan($client, $document);
    }
}
