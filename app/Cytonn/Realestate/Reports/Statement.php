<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 11/02/2019
 * Time: 11:00
 */

namespace App\Cytonn\Realestate\Reports;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Realestate\Statements\StatementProcessor;

class Statement
{
    protected $protect = false;

    protected $protectOutput;


    public function setProtectOutput(bool $protectOutput): Statement
    {
        $this->protectOutput = $protectOutput;
        return $this;
    }

    public function generate(
        Client $client,
        Project $project,
        $date = null
    ) {
        $date = is_null($date)
            ? $date = Carbon::now()
            : $date = Carbon::parse($date);

        $stmt_data = (new StatementProcessor())->process($client, $project, $date);

        $pdf = \PDF::loadView('reports.investments.statement', [
            'clusters' => $stmt_data['clusters'],
            'totals' => $stmt_data['totals'],
            'statementDate' => $date,
            'project' => $project,
            'client' => $client,
            'logo' => $project->fundManager->re_logo,
            'has_next_payments' => $stmt_data['has_next_payments'],
            'has_payments_due' => $stmt_data['has_payments_due'],
        ]);

        if ($this->protectOutput) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    public function html(Project $project, Client $client, $date = null)
    {
        $date = is_null($date)
            ? $date = Carbon::now()
            : $date = Carbon::parse($date);

        $stmt_data = (new StatementProcessor())->process($client, $project, $date);

        return view('reports.realestate.statement_table', [
            'clusters' => $stmt_data['clusters'],
            'totals' => $stmt_data['totals'],
            'client' => $client,
            'date' => $date,
            'data' => $stmt_data,
            'project' => $project,
            'statementDate' => $date,
        ]);
    }

    public function passwordProtect($protect = true)
    {
        $this->protect = $protect;

        return $this;
    }
}
