<?php

namespace Cytonn\Realestate\Units;

use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Project;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Maatwebsite\Excel\Facades\Excel;

class UnitsWithLoosWithoutSAExcelGenerator
{
    public function excel($file_name, $unitHoldings)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($unitHoldings) {
                foreach ($unitHoldings as $project => $holding_arr) {
                    $project = Project::findOrFail($project);
                    $excel->sheet(
                        $project->name,
                        function ($sheet) use ($holding_arr) {
                            $holdings = $holding_arr->map(
                                function ($holding) {
                                    $l = new EmptyModel();
                                    $l->Unit_Number = $holding->unit->number;
                                    $l->Client_Code = is_null($holding->client) ? null : $holding->client->client_code;
                                    $l->Client_Name = ClientPresenter::presentFullNames($holding->client_id);
                                    $l->Type = $holding->unit->type->name;
                                    $l->Size = $holding->unit->size->name;
                                    $l->Unit_Price = AmountPresenter::currency($holding->price());
                                    $l->Payment_Plan = $holding->paymentPlan->name;
                                    $l->Loo_Sent_On = DatePresenter::formatDate($holding->loo->sent_on);
                                    $l->Loo_Signed_On = DatePresenter::formatDate($holding->loo->date_signed);
                                    $l->Loo_Ten_Percent_Deposit_On =
                                        DatePresenter::formatDate($holding->repo->getFirstUnitPaymentDate($holding));
                                    $l->FA = ($holding->commission->recipient)
                                        ? $holding->commission->recipient->name : null;
                                    $l->Advocate = ($holding->loo->advocate_id) ? $holding->loo->advocate->name : null;
                                    return $l;
                                }
                            );
                            $sheet->fromModel($holdings);
                            $sheet->prependRow(['Units with sent LOOS but with pending Sales Agreements - ' .
                                DatePresenter::formatDate(Carbon::today()->toDateString())]);
                            $sheet->mergeCells('A1:G1');
                        }
                    );
                }
            }
        );
    }
}
