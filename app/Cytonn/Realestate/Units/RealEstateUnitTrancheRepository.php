<?php

namespace App\Cytonn\RealEstate\Units;

use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\RealEstateUnitTranche;

class RealEstateUnitTrancheRepository
{
    protected $tranche;

    public function __construct(RealEstateUnitTranche $tranche = null)
    {
        is_null($tranche) ? $this->tranche = new RealEstateUnitTranche() : $this->tranche = $tranche;
    }

    public function getUnitPrice(RealestateUnitSize $size, RealEstatePaymentPlan $paymentPlan)
    {
        $sizing = $this->tranche->sizes()->where('size_id', $size->id)->first();
        
        $pricing = $sizing->prices()->where('payment_plan_id', $paymentPlan->id)->first();
        
        return $pricing->price;
    }
}
