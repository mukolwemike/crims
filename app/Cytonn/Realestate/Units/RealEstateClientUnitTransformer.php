<?php

namespace App\Cytonn\RealEstate\Units;

use App\Cytonn\Models\UnitHolding;
use League\Fractal\TransformerAbstract;

class RealEstateClientUnitTransformer extends TransformerAbstract
{
    public function transform(UnitHolding $unitHolding)
    {
        return [
            'id'        =>  $unitHolding->unit->id,
            'project'   =>  $unitHolding->unit->project->name,
            'number'    =>  $unitHolding->unit->number,
            'size_name' =>  $unitHolding->unit->size->name,
            'client_code'=>$unitHolding->client->client_code,
            'status'    => $this->getStatus($unitHolding),
            'price'=>$unitHolding->price(),
            'paid'=>$unitHolding->totalPayments(),
            'balance'=>$unitHolding->amountRemaining(),
            'unit'=>$unitHolding->unit->number,
        ];
    }
    
    private function getStatus(UnitHolding $unitHolding)
    {
        return (bool) $unitHolding->unit->activeHolding;
    }
}
