<?php
/**
 * Date: 03/08/2016
 * Time: 4:17 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Tranches;

use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\RealEstateUnitTrancheSizing;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Number;
use Cytonn\Mailers\RealEstate\TrancheCheckMailer;
use Illuminate\Support\Facades\Log;

/**
 * Class RealEstateUnitTrancheRepository
 *
 * @package Cytonn\Realestate\Tranches
 */
class RealEstateUnitTrancheRepository
{

    /**
     * @var RealEstateUnitTranche
     */
    protected $tranche;

    /**
     * RealEstateUnitTrancheRepository constructor.
     */
    public function __construct(RealEstateUnitTranche $tranche = null)
    {
        is_null($tranche) ? $this->tranche = new RealEstateUnitTranche() : $this->tranche = $tranche;
    }

    /**
     * Get the price of a unit for a given tranche
     *
     * @param  RealestateUnitSize $size
     * @param  RealEstatePaymentPlan $paymentPlan
     * @return mixed
     */
    public function getUnitPrice(RealestateUnit $unit, RealEstatePaymentPlan $paymentPlan)
    {
        $size = $unit->size;

        $projectFloor = $unit->projectFloor;

        $unitType = $unit->realEstateType;

        $sizing = $this->tranche->sizes()->where('size_id', $size->id);

        if ($projectFloor) {
            $sizing = $sizing->where('project_floor_id', $projectFloor->id);
        }

        if ($unitType) {
            $sizing = $sizing->where('real_estate_type_id', $unitType->id);
        }

        $sizing = $sizing->first();

        if (is_null($sizing)) {
            Log::info($unit->id);
        }

        $pricing = $sizing->prices()->where('payment_plan_id', $paymentPlan->id)->first();

        return $pricing->price;
    }

    /**
     * Check whether all units for a given size in a tranche have been reserved
     *
     * @param  UnitHolding $holding
     * @return bool
     */
    public function trancheCheck(UnitHolding $holding)
    {
        $unit = RealestateUnit::findOrFail($holding->unit_id);
        $size = $unit->size;

        $allUnits = RealEstateUnitTrancheSizing::where('tranche_id', $this->tranche->id)
            ->where('size_id', $size->id);

        if ($unit->projectFloor) {
            $allUnits->where('project_floor_id', $unit->project_floor_id);
        }

        if ($unit->realEstateType) {
            $allUnits->where('real_estate_type_id', $unit->real_estate_type_id);
        }

        $allUnits = $allUnits->first()->number;
        $reserved = $this->tranche
            ->holdings()
            ->active()
            ->whereHas(
                'unit',
                function ($unit) use ($size) {
                    return $unit->where('size_id', $size->id);
                }
            )->count();


        // Send alert when 3 units are remaining in tranche
        $remaining = $allUnits - $reserved;

        if ($remaining <= 3 and $remaining != 0) {
            $content = 'There are now only ' . strtoupper(Number::toWords((int)$remaining)) . ' ' .
                $size->name . 's in the ' . $this->tranche->name . ' tranche in ' . $this->tranche->project->name . '!';
            if ($remaining == 1) {
                $content = 'There is now only ' . strtoupper(Number::toWords((int)$remaining)) . ' ' . $size->name .
                    ' in the ' . $this->tranche->name . ' tranche in ' . $this->tranche->project->name . '!';
            }
            (new TrancheCheckMailer())->sendEmail($this->tranche, $unit, $content);
            \Flash::message($content);
            return false;
        }

        // Send alert when there are no more units remaining in tranche
        //        if($reserved >= $allUnits)
        if ($remaining == 0) {
            $content = "All the " . $unit->size->name . "'s in the " . $this->tranche->name . " tranche, in " .
                $this->tranche->project->name . ", have been booked!";
            (new TrancheCheckMailer())->sendEmail($this->tranche, $unit, $content);
            \Flash::error('All ' . $size->name . 's in ' . $this->tranche->name . ' tranche have been booked!');
            return true;
        }


        return false;
    }
}
