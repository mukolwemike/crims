<?php
/**
 * Date: 06/06/2016
 * Time: 7:35 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class RealEstatePaymentForm extends FormValidator
{
    public $rules = [
        'date'=>'required|date',
        'description'=>'required|min:10',
        'amount'=>'required|numeric',
        'schedule_id'=>'required',
        'negotiated_price'=>'numeric'
    ];

    public $messages = [
        'schedule'=>'Please select the payment, or add one if it doesn\'t exist'
    ];

    public function firstPayment()
    {
        $this->rules['tranche_id'] = 'required';
        $this->rules['rate'] = 'required';
    }
}
