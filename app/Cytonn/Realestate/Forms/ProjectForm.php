<?php
/**
 * Date: 03/08/2016
 * Time: 12:19 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class ProjectForm extends FormValidator
{
    public $rules = [
        'code'=>'required',
        'name'=>'required|min:3',
        'reservation_fee'=>'required',
        'type_id'=>'required',
        'commission_calculator'=>'required'
    ];
}
