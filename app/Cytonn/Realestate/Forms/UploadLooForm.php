<?php
/**
 * Date: 08/06/2016
 * Time: 12:54 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class UploadLooForm extends FormValidator
{
    public $rules = [
        'file'=>'max:8000|mimes:pdf,jpg,png,bmp',
        'date_signed'=>'nullable|date',
        'sent_on'=>'nullable|date',
    ];
}
