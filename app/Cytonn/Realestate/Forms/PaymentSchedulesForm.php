<?php
/**
 * Date: 09/06/2016
 * Time: 6:56 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class PaymentSchedulesForm extends FormValidator
{
    public $rules = [
        'description'=>'required|min:10',
        'payment_type_id'=>'required',
        'date'=>'required|date',
        'amount'=>'required|numeric'
    ];
}
