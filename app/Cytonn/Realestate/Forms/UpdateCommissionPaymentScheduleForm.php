<?php
/**
 * Date: 20/01/2017
 * Time: 08:48
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class UpdateCommissionPaymentScheduleForm extends FormValidator
{
    public $rules = [
        'date'=>'required|date',
        'reason'=>'required|min:10'
    ];
}
