<?php

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class AdvocatesForm extends FormValidator
{
    public $rules = [
        'name'              =>  'required | string | min:3 | max:100',
    //        'postal_code'       =>  'nullable',
    //        'postal_address'    =>  'nullable',
    //        'street'            =>  'nullable | string',
    //        'town'              =>  'nullable | string',
    //        'country_id'        =>  'nullable | numeric',
    //        'building'          =>  'nullable | string',
    //        'telephone_office'  =>  'nullable',
    //        'telephone_cell'    =>  'nullable',
        'email'             =>  'required',
//        'address'           =>  'required',
        'active'            =>  'boolean'
    ];
}
