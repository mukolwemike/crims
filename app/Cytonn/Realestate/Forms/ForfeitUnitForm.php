<?php
/**
 * Date: 29/07/2016
 * Time: 12:27 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class ForfeitUnitForm extends FormValidator
{
    public $rules = [
        'forfeiture_date'=>'required|date',
        'amount'=>'required|numeric',
        'narration'=>'required|min:10'
    ];
}
