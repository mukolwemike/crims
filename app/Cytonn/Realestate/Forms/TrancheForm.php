<?php
/**
 * Date: 26/07/2016
 * Time: 5:30 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class TrancheForm extends FormValidator
{
    public $rules = [
        'price'=>'required|numeric',
        'number'=>'required|numeric',
        'payment_plan_id'=>'required',
        'size_id'=>'required'
    ];
}
