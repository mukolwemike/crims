<?php
/**
 * Date: 08/06/2016
 * Time: 12:54 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class UploadSalesAgreementForm extends FormValidator
{
    public $rules = [
        'file'=>'max:10000|mimes:pdf,jpg,png,bmp',
        'title'=>'required',
    ];
}
