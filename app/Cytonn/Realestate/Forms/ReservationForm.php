<?php
/**
 * Date: 01/06/2016
 * Time: 2:03 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class ReservationForm extends FormValidator
{
    public $rules = [
        'firstname'=>'required_without_all:client_id,corporate_registered_name',
        'lastname'=>'required_without_all:client_id,corporate_registered_name',
        'corporate_registered_name'=>'required_without_all:client_id,firstname',
        'email'=>'required_without:client_id|email|nullable',
        'postal_address'=>'required_without_all:client_id,street',
        'postal_code'=>'required_with:postal_address|required_without:client_id',
        'town'=>'required_with:postal_address|required_without:client_id',
        'street'=>'required_without_all:client_id,postal_address',
        'country_id'=>'required_without:client_id',
        'client_id'=>'required_if:new_client,0',
        'id_or_passport'=>'required_without_all:client_id|required_with:firstname',
        'contact_person_lname'=>'required_with:corporate_registered_name',
        'contact_person_fname'=>'required_with:corporate_registered_name',
        'investor_bank_branch'=>'required_with:investor_account_name|numeric',
    ];

    protected $messages = [
    //        'investor_account_name.required'=>'The account name is required',
    //        'investor_account_number.required'=>'The account number is required',
    //        'investor_bank.required'=>'The bank name is required',
    //        'investor_bank_branch'=>'The branch is required',
        'client_id.required_with'=>'The client code is required for existing clients'
    ];
}
