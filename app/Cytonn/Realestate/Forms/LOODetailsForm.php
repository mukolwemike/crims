<?php

namespace Cytonn\Realestate\Forms;

use Laracasts\Validation\FormValidator;

class LOODetailsForm extends FormValidator
{
    public $rules = [
        'long_name'                 =>  'required',
        'vendor'                    =>  'required',
        'vendor_address'            =>  'required',
        'completion_date'           =>  'required',
        'county_government'         =>  'required',
        'bank_name'                 =>  'required',
        'bank_branch_name'          =>  'required',
        'bank_account_name'         =>  'required',
        'bank_account_number'       =>  'required',
        'bank_swift_code'           =>  'required',
        'bank_clearing_code'        =>  'required',
        'development_description'   =>  'required'
    ];

    public $messages = [
        'long_name.required'                 =>  'The long name is required',
        'vendor.required'                    =>  'The vendor is required',
        'vendor_address.required'            =>  'The vendor address is required',
        'completion_date.required'           =>  'The completion date is required',
        'county_government.required'         =>  'The county government is required',
        'bank_name.required'                 =>  'The bank name is required',
        'bank_branch_name.required'          =>  'The branch name is required',
        'bank_account_name.required'         =>  'The account name is required',
        'bank_account_number.required'       =>  'The account number is required',
        'bank_swift_code.required'           =>  'The swift code is required',
        'bank_clearing_code.required'        =>  'The clearing code is required',
        'development_description.required'   =>  'The development description is required'
    ];
}
