<?php
/**
 * Date: 07/06/2016
 * Time: 11:29 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\RealEstatePayment;

class ReservationFeesPaid
{
    public $payment;

    /**
     * ReservationFeesPaid constructor.
     *
     * @param $payment
     */
    public function __construct(RealEstatePayment $payment)
    {
        $this->payment = $payment;
    }
}
