<?php
/**
 * Date: 07/06/2016
 * Time: 10:57 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\RealEstatePayment;

class PaymentHasBeenMade
{
    public $payment;

    /**
     * PaymentHasBeenMade constructor.
     *
     * @param $payment
     */
    public function __construct(RealEstatePayment $payment)
    {
        $this->payment = $payment;
    }
}
