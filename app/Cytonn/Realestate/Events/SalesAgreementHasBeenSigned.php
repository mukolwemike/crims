<?php
/**
 * Created by PhpStorm.
 * User: internone
 * Date: 7/21/16
 * Time: 4:44 PM
 */

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\RealEstateSalesAgreement;

class SalesAgreementHasBeenSigned
{
    public $salesAgreement;

    public function __construct(RealEstateSalesAgreement $salesAgreement)
    {
        $this->salesAgreement = $salesAgreement;
    }
}
