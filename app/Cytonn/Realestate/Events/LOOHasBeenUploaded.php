<?php

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\RealestateLetterOfOffer;

class LOOHasBeenUploaded
{
    public $realestateLetterOfOffer;

    /**
     * LOOHasBeenUploaded constructor.
     *
     * @param RealestateLetterOfOffer $realestateLetterOfOffer
     */
    public function __construct(RealestateLetterOfOffer $realestateLetterOfOffer)
    {
        $this->realestateLetterOfOffer = $realestateLetterOfOffer;
    }
}
