<?php

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\RealestateLetterOfOffer;

class LOOHasBeenSigned
{
    public $realestateLetterOfOffer;

    /**
     * LOOHasBeenSigned constructor.
     *
     * @param RealestateLetterOfOffer $realestateLetterOfOffer
     */
    public function __construct(RealestateLetterOfOffer $realestateLetterOfOffer)
    {
        $this->realestateLetterOfOffer = $realestateLetterOfOffer;
    }
}
