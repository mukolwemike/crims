<?php

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\RealEstateSalesAgreement;

class SalesAgreementHasBeenUploaded
{
    public $salesAgreement;

    public function __construct(RealEstateSalesAgreement $salesAgreement)
    {
        $this->salesAgreement = $salesAgreement;
    }
}
