<?php

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\RealestateLetterOfOfferReject;

class LOOHasBeenRejected
{
    public $realestateLetterOfOfferReject;

    /**
     * LOOHasBeenRejected constructor.
     *
     * @param RealestateLetterOfOfferReject $realestateLetterOfOfferReject
     */
    public function __construct(RealestateLetterOfOfferReject $realestateLetterOfOfferReject)
    {
        $this->realestateLetterOfOfferReject = $realestateLetterOfOfferReject;
    }
}
