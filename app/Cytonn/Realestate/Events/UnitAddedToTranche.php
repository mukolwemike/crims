<?php
/**
 * Date: 21/06/2016
 * Time: 11:40 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\UnitHolding;

class UnitAddedToTranche
{
    public $holding;

    /**
     * UnitAddedToTranche constructor.
     *
     * @param $holding
     */
    public function __construct(UnitHolding $holding)
    {
        $this->holding = $holding;
    }
}
