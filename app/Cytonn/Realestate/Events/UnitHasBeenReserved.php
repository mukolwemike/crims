<?php
/**
 * Date: 04/06/2016
 * Time: 11:22 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Events;

use App\Cytonn\Models\ReservationForm;

class UnitHasBeenReserved
{
    public $reservationForm;

    public $creating;

    /**
     * UnitHasBeenReserved constructor.
     *
     * @param $reservationForm
     * @param $creating
     */
    public function __construct(ReservationForm $reservationForm, $creating = false)
    {
        $this->reservationForm = $reservationForm;

        $this->creating = $creating;
    }
}
