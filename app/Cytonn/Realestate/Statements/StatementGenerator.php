<?php
/**
 * Created by PhpStorm.
 * User: internone
 * Date: 7/13/16
 * Time: 2:31 PM
 */

namespace Cytonn\Realestate\Statements;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateCampaignStatementItem;
use App\Cytonn\Models\RealestateStatementCampaign;
use App\Cytonn\Models\StatementLog;
use App\Cytonn\Models\User;
use Carbon\Carbon;
//use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Mailers\RealEstate\StatementMailer;
use Cytonn\Presenters\ClientPresenter;
use Maatwebsite\Excel\Facades\Excel;

class StatementGenerator
{
    protected $protectOutput;

    protected $protect = true;

    public function setProtectOutput(bool $protectOutput): StatementGenerator
    {
        $this->protectOutput = $protectOutput;
        return $this;
    }

    // generate

    public function generate(
        Client $client,
        Project $project,
        Carbon $date,
        $sender,
        RealestateStatementCampaign $campaign = null
    ) {
        $statement_data = (new StatementProcessor())->process($client, $project, $date);

        $pdf = \PDF::loadView('realestate.reports.statement', [
            'clusters' => $statement_data['clusters'],
            'campaign' => $campaign,
            'totals' => $statement_data['totals'],
            'statementDate' => $date,
            'project' => $project,
            'client' => $client,
            'sender' => $sender,
            'logo' => $project->fundManager->re_logo,
            'has_next_payments' => $statement_data['has_next_payments'],
            'has_payments_due' => $statement_data['has_payments_due'],
        ]);

        if ($this->protectOutput) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    /**
     * Send a realestate campaign statement item
     *
     * @param  RealestateCampaignStatementItem $item
     * @return bool
     */
    public function sendRealEstateCampaignStatementItem(RealestateCampaignStatementItem $item)
    {
        $campaign = $item->campaign;


        //require client id, sender id,
        $client = Client::findOrFail($item->client_id);

        if (is_null($client->getContactEmailsArray())) {
            return;
        }

        $projects = $this->getProjects($client->id);

        if (count($projects) == 0) {
            $item->sent = true;
            $item->sent_on = $campaign->send_date;
            return $item->save();
        }

        $sender = User::findOrFail($campaign->sender_id);

        $this->setProtectOutput(true);

        $statements = [];
        foreach ($projects as $project) {
            $statements[$project->name . ' Statement - ' . ClientPresenter::presentJointFirstNameLastName($client->id) .
            '.pdf'] = $this->generate($client, $project, $campaign->send_date, $sender, $campaign)->output();
        }

        $mailer = new StatementMailer();
        $mailer->sendStatementToClient(
            $client,
            $projects,
            $statements,
            $campaign->send_date,
            $campaign->sender_id,
            $campaign
        );

        $log = new StatementLog();
        $log->fill(['client_id' => $item->client_id, 'sent_by' => $campaign->sender_id]);
        $log->save();

        $item->sent = true;
        //        $item->sent_on = Carbon::now();
        $item->sent_on = $campaign->send_date;//Changed to date send date of the campaign

        return $item->save();
    }

    /**
     * @param $clientId
     * @return mixed
     */
    protected function getProjects($clientId)
    {
        return Project::whereHas('units', function ($q) use ($clientId) {
            $q->whereHas('holdings', function ($q) use ($clientId) {
                $q->where('client_id', $clientId)->active()->where('receive_campaign_statement', 1);
            });
        })->get();
    }

    public function html(Client $client, Project $project, $date, $date_from = null)
    {
        $processed = (new StatementProcessor())->process($client, $project, $date, $date_from);

        return view('reports.realestate.statement', [
            'date' => $date,
            'clusters' => $processed['clusters'],
            'totals' => $processed['totals'],
            'has_next_payments' => $processed['has_next_payments'],
            'has_payments_due' => $processed['has_payments_due'],
            'client' => $client,
            'project' => $project,
            'data' => $processed
        ]);
    }

    public function clientGenerate(
        Client $client,
        Project $project,
        Carbon $date,
        $date_from = null
    ) {

        $statement_data = (new StatementProcessor())->process($client, $project, $date, $date_from);

        $pdf = \PDF::loadView('reports.realestate.statement_table', [
            'clusters' => $statement_data['clusters'],
            'campaign' => null,
            'totals' => $statement_data['totals'],
            'statementDate' => $date,
            'project' => $project,
            'client' => $client,
            'sender' => null,
            'logo' => $project->fundManager->re_logo,
            'has_next_payments' => $statement_data['has_next_payments'],
            'has_payments_due' => $statement_data['has_payments_due'],
        ]);

        if ($this->protect) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    public function clientExcelGenerate(
        Client $client,
        Project $project,
        Carbon $date,
        $date_from = null
    ) {
        $statement_data = (new StatementProcessor())->process($client, $project, $date, $date_from);

        return \Excel::create(
            'Statement',
            function ($excel) use ($statement_data, $client, $project, $date, $date_from) {
                $excel->sheet(
                    'Statement',
                    function ($sheet) use ($statement_data, $client, $project, $date, $date_from) {

                        $sheet->loadView(
                            'reports.realestate.statement_excel',
                            [
                            'clusters' => $statement_data['clusters'],
                            'campaign' => null,
                            'totals' => $statement_data['totals'],
                            'statementDate' => $date,
                            'project' => $project,
                            'client' => $client,
                            'sender' => null,
                            'logo' => $project->fundManager->re_logo,
                            'has_next_payments' => $statement_data['has_next_payments'],
                            'has_payments_due' => $statement_data['has_payments_due'],
                            ]
                        );
                    }
                );
            }
        )->download('xlsx');
    }

    public function passwordProtect($protect = true)
    {
        $this->protect = $protect;

        return $this;
    }
}
