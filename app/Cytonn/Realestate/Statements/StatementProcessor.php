<?php

namespace Cytonn\Realestate\Statements;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Realestate\Payments\Interest;

/**
 * Created by PhpStorm.
 * User: internone
 * Date: 7/13/16
 * Time: 2:10 PM
 */
class StatementProcessor
{
    /**
     * @var
     */
    protected $date;

    protected $date_from;

    /**
     * @var
     */
    protected $project;

    /**
     * @var
     */
    protected $client;

    /**
     * @param Client $client
     * @param Project $project
     * @param Carbon $date
     * @param null $date_from
     * @return array
     */
    public function process(Client $client, Project $project, Carbon $date, $date_from = null)
    {
        $this->date = $date;

        $this->client = $client;

        $this->project = $project;

        $this->date_from = $date_from ? Carbon::parse($date_from) : null;

        $holdings = $client->unitHoldings()
            ->whereHas('unit', function ($unit) use ($project) {
                $unit->where('project_id', $project->id);
            })
            ->where('active', 1)
            ->where('receive_campaign_statement', 1)
            ->get();

        $unitHoldings = Collection::makeFromCollection($holdings);

        $unitHoldings->each(
            function ($holding) use ($date, $date_from) {

                $size_id = $holding->unit->size_id;
                $price = (int)$holding->price();
                $tranche_id = $holding->tranche_id;

                $payments = $holding->repo->netUnitTotalPaid($date, $date_from); // ok

                $plan = $holding->payment_plan_id;
                $holding->total_payments = $payments;
                $holding->other_fees = $holding->repo->unitOtherFees($date);
                $holding->interest_accrued = $holding->interest($date, $date_from); // ok

                $holding->reservation_fees = $holding->payments()
                    ->whereHas('type', function ($type) {
                            $type->where('slug', 'reservation_fee');
                    })
                    ->where('date', '<=', $date)
                    ->where(function ($payment) use ($date_from) {
                        if ($date_from) {
                            return $payment->where('date', '>=', $date_from);
                        }
                    })
                    ->sum('amount'); // ok

                $holding->deposit = $holding->payments()
                    ->whereHas('type', function ($type) {
                            $type->where('slug', 'deposit');
                    })->where('date', '<=', $date)
                    ->where(function ($payment) use ($date_from) {
                        if ($date_from) {
                            return $payment->where('date', '>=', $date_from);
                        }
                    })
                    ->sum('amount'); // ok

                $holding->total_installments = $holding->payments()
                    ->whereHas('type', function ($type) {
                        $type->where('slug', 'installment');
                    })
                    ->where('date', '<=', $date)
                    ->where(function ($payment) use ($date_from) {
                        if ($date_from) {
                            return $payment->where('date', '>=', $date_from);
                        }
                    })
                    ->sum('amount'); // ok

                $holding->overdue_schedules = $holding->paymentSchedules()
                    ->inPricing()
                    ->accruesInterest()
                    ->before($date)
                    ->remember(Interest::CACHE)
                    ->get()
                    ->each(
                        function (RealEstatePaymentSchedule $schedule) {
                            $interest = new Interest($schedule->holding);

                            $schedule->interest_accrued = $interest->interest($schedule, $this->date);
                        }
                    )
                    ->filter(
                        function (RealEstatePaymentSchedule $schedule) {
                            return $schedule->interest_accrued > 0;
                        }
                    )
                    ->each(
                        function (RealEstatePaymentSchedule $schedule) {
                            $end_date = $this->date;

                            if ($schedule->repo->overdue($this->date) == 0) {
                                $end_date = $schedule->repo->endDate($this->date);
                            }

                            $schedule->due_date = $schedule->repo->interestAccrualDate();
                            $schedule->tenor = $end_date->copy()->diffInDays($schedule->due_date);
                            $schedule->principal = $schedule->repo->overdue($schedule->date, false);
                            $schedule->paid = $schedule->repo->amountPaid($this->date);
                            $total = $schedule->principal + $schedule->interest_accrued - $schedule->paid;

                            $schedule->balance = $total > 0 ? $total : 0;
                        }
                    );

                $holding->unique = "size $size_id price $price tranche $tranche_id paid $payments plan $plan";
            }
        );


        $clusterKeys = $unitHoldings->unique('unique')->pluck('unique');

        $clusters = $clusterKeys->map(
            function ($key) use ($unitHoldings) {
                $cluster = new \stdClass();
                $cluster->holdings = $unitHoldings->where('unique', $key);
                $holding = $cluster->holdings->first();
                $cluster->project = $holding->project;
                $cluster->size = $holding->unit->size;
                $cluster->no_of_units = $cluster->holdings->count();
                $cluster->unitNumbers = $this->getUnitNumberString($cluster->holdings);

                $cluster->type = $holding->unit->type;
                $cluster->price = $holding->price();
                $cluster->total_price = $cluster->price * $cluster->no_of_units;
                $cluster->total_payments_received = $cluster->holdings->sum('total_payments');
                $cluster->interest_accrued = $cluster->holdings->sum('interest_accrued');
                $cluster->other_fees = $cluster->holdings->sum('other_fees');
                $cluster->payment_pending =
                    $cluster->total_price + $cluster->interest_accrued - ($cluster->total_payments_received - $cluster->other_fees);
                $cluster->reservation_fees = $cluster->holdings->sum('reservation_fees');
                $cluster->deposit = $cluster->holdings->sum('deposit');
                $cluster->total_installments = $cluster->holdings->sum('total_installments');

                $next_payments = $this->getNextPayments($cluster);
                $cluster->next_payment_amount = $next_payments->sum('amount');

                $p = $next_payments->first();
                $scheduleDate = null;

                if ($p) {
                    $scheduleDate = $p->last_schedule == 1
                        ? 'On Project Completion'
                        : \Cytonn\Presenters\DatePresenter::formatDate($p->date);
                }

                $cluster->next_payment_date = $scheduleDate;
                $cluster->payment_due = $this->getPaymentsDue($cluster);

                return $cluster;
            }
        );

        $totals = new \stdClass();
        $totals->no_of_units = $clusters->sum('no_of_units');
        $totals->total_price = $clusters->sum('total_price');
        $totals->interest_accrued = $clusters->sum('interest_accrued');
        $totals->reservation_fees = $clusters->sum('reservation_fees');
        $totals->deposit = $clusters->sum('deposit');
        $totals->total_installments = $clusters->sum('total_installments');
        $totals->total_payments_received = $clusters->sum('total_payments_received');
        $totals->payment_pending = $clusters->sum('payment_pending');

        return [
            'clusters' => $clusters,
            'totals' => $totals,
            'has_next_payments' => $clusters->sum('next_payment_amount') > 0,
            'has_payments_due' => $clusters->sum('payment_due') > 0
        ];
    }

    /**
     * @param \stdClass $cluster
     * @return mixed
     */
    private function getNextPayments(\stdClass $cluster)
    {
        $holdings = $cluster->holdings;

        return $holdings->map(
            function (UnitHolding $holding) {
                $paid = $holding->repo->netUnitTotalPaid($this->date);

                $nextUnpaidSchedule = function ($date) use (&$nextUnpaidSchedule, $holding, $paid) {
                    $nextSchedule = $holding->paymentSchedules()
                        ->where('date', '>', $date)
                        ->oldest('date')
                        ->first();

                    if (!$nextSchedule) {
                        return null;
                    }

                    $sumOfPast = $holding->paymentSchedules()
                        ->inPricing()
                        ->before($nextSchedule->date)
                        ->remember(Interest::CACHE)
                        ->get()
                        ->sum(
                            function (RealEstatePaymentSchedule $schedule) {
                                return $schedule->repo->total($this->date);
                            }
                        );

                    return $sumOfPast > $paid ? $nextSchedule : $nextUnpaidSchedule($nextSchedule->date);
                };

                $schedule = $nextUnpaidSchedule($this->date);

                if ($schedule) {
                    $schedule->amount = $schedule->repo->overdue($schedule->date, true);
                }

                return $schedule;
            }
        )->filter(
            function ($schedule) {
                return !is_null($schedule);
            }
        );
    }

    /**
     * @param \stdClass $cluster
     * @return mixed
     */
    private function getPaymentsDue(\stdClass $cluster)
    {
        $holdings = $cluster->holdings;

        return $holdings->sum(
            function (UnitHolding $holding) {
                return $this->overDue($holding);
            }
        );
    }

    private function overDue(UnitHolding $holding)
    {
        return $holding->paymentSchedules()
            ->inPricing()
            ->before($this->date)
            ->remember(Interest::CACHE)
            ->get()
            ->sum(
                function (RealEstatePaymentSchedule $schedule) {
                    return $schedule->last_schedule == 1 ? 0 : $schedule->repo->overdue($this->date);
                }
            );
    }

    /*
     * Get the unit numbers for the units in the cluster holdings
     */
    private function getUnitNumberString($clusterHoldings)
    {
        return rtrim(
            $clusterHoldings->reduce(
                function ($carry, $holding) {
                    return $carry . $holding->unit->number . ',';
                },
                ''
            ),
            ','
        );
    }
}
