<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Realestate\RealestateUnits;

use Laracasts\Presenter\Presenter;

class RealEstateUnitPresenter extends Presenter
{
    /*
     * Get the unit size
     */
    public function unitSizeName()
    {
        if ($this->size) {
            return $this->size->name;
        }

        return '';
    }

    /*
     * Get the unit size
     */
    public function unitTypeName()
    {
        if ($this->type) {
            return $this->type->name;
        }

        return '';
    }
}
