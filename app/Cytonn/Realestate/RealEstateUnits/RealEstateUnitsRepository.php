<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 14/11/2018
 * Time: 20:42
 */

namespace Cytonn\Realestate\RealEstateUnits;

use Illuminate\Support\Arr;

class RealEstateUnitsRepository
{
    use ReserveUnitApplicationRules;

    public function validateReserveUnitApplication($input)
    {
        $clientType = $input['individual'] == 1 ? 'individual' : 'corporate';

        if (Arr::exists(request()->all(), 'risk_reason')) {
            $validator = ($clientType == 'individual')
                ? $this->validateIndividualRiskyForm(request())
                : $this->validateCorporateRiskyForm(request());
        } elseif ($input['new_client'] == 2) {
            $validator = $this->validateExistingForm(request());
        } elseif ($input['new_client'] == 3) {
            $validator = ($clientType == 'individual')
                ? $this->validateIndividualDuplicateForm(request())
                : $this->validateCorporateDuplicateForm(request());
        } else {
            $validator = ($clientType == 'individual')
                ? $this->validateIndividualForm(request())
                : $this->validateCorporateForm(request());
        }


        return $validator;
    }
}
