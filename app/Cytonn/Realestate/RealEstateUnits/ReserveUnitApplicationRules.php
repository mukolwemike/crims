<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 14/11/2018
 * Time: 20:45
 */

namespace Cytonn\Realestate\RealEstateUnits;

use Cytonn\Rules\Rules;

trait ReserveUnitApplicationRules
{
    use Rules;

    protected $commonRules = [
        'email' => 'required|email',
        'method_of_contact_id' => 'required',
        'investor_account_name' => 'required',
        'investor_account_number' => 'required',
        'investor_bank' => 'required',
        'investor_bank_branch' => 'required',
        'town' => 'required',
        'street' => 'required',
        'postal_code' => 'required',
        'postal_address' => 'required',
    ];

    protected $individualRules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'title' => 'required',
        'telephone_home' => 'required',
        'id_or_passport' => 'required',
        'risk_reason' => 'required_if:risky,"true"'
    ];

    protected $corporateRules = [
        'registered_name' => 'required',
        'telephone_office' => 'required',
        'registered_address' => 'required',
        'registration_number' => 'required'
    ];

    public function validateExistingForm($request)
    {
        $rules = [
            'client_id' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    public function validateIndividualForm($request)
    {
        $rules = $this->commonRules + $this->individualRules;

        return $this->verdict($request, $rules);
    }

    public function validateCorporateForm($request)
    {
        $rules = $this->commonRules + $this->corporateRules;

        return $this->verdict($request, $rules);
    }

    public function validateIndividualDuplicateForm($request)
    {
        $rules = $this->individualRules + $this->commonRules + [
                'duplicate_reason' => 'required',
            ];

        return $this->verdict($request, $rules);
    }

    public function validateCorporateDuplicateForm($request)
    {
        $rules = $this->corporateRules + $this->commonRules + [
                'duplicate_reason' => 'required'
            ];

        return $this->verdict($request, $rules);
    }

    public function validateIndividualRiskyForm($request)
    {
        $rules = $this->commonRules + $this->individualRules + [
                'risk_reason' => 'required'
            ];

        return $this->verdict($request, $rules);
    }

    public function validateCorporateRiskyForm($request)
    {
        $rules = $this->commonRules + $this->corporateRules + [
                'risk_reason' => 'required'
            ];

        return $this->verdict($request, $rules);
    }
}
