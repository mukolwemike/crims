<?php
/**
 * Date: 23/01/2017
 * Time: 12:46
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Payments;

use App\Cytonn\Core\DataStructures\Caching\LocalCache;
use App\Cytonn\Models\RealestateInterestAccrued;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use \Cache;

/**
 * Class Interest
 *
 * @package Cytonn\Realestate\Payments
 */
class Interest
{
    use LocalCache;
    /**
     * @var UnitHolding
     */
    protected $holding;
    protected $project;
    protected $allowExemptions = true;

    /**
     *
     */
    const GRACE_PERIOD = 30; //grace period in days

    /**
     *
     */
    const INTEREST_RATE = 18; //% interest rate

    /**
     *
     */
    const CACHE = 1; //cache interest related items for this number of minutes

    /**
     * Interest constructor.
     *
     * @param $holding
     */
    public function __construct(UnitHolding $holding)
    {
        $this->holding = $holding;
        $this->project = $holding->unit->project;
    }

    /**
     * @param mixed $allowExemptions
     */
    public function setAllowExemptions($allowExemptions)
    {
        $this->allowExemptions = $allowExemptions;
    }

    /**
     * @param RealEstatePayment $payment
     */
    public function saveInterestAfterPayment(RealEstatePayment $payment)
    {
        $schedules = $this->holding->paymentSchedules()
            ->inPricing()
            ->accruesInterest()
            ->oldest('date')
            ->remember(static::CACHE)
            ->before($payment->date)
            ->get();

        $schedules->each(function (RealEstatePaymentSchedule $schedule) use ($payment) {
                $interestAccrued = $this->calculate($schedule, $payment->date);

                $overdue = $schedule->repo->overdue($payment->date);

                $saved = $this->savedInterest($schedule);

            if ($overdue <= 0 && $interestAccrued > $saved) {
                $this->save($schedule, $payment, $interestAccrued - $saved);
            }
        });
    }

    /**
     * @param RealEstatePaymentSchedule $schedule
     * @return mixed
     */
    private function getSaved(RealEstatePaymentSchedule $schedule)
    {
        return RealestateInterestAccrued::where('schedule_id', $schedule->id)->get();
    }

    /**
     * @param RealEstatePaymentSchedule $schedule
     * @return int
     */
    public function savedInterest(RealEstatePaymentSchedule $schedule)
    {
        if (! $schedule->penalty || $schedule->last_schedule == 1) {
            return 0;
        }

        return $this->getSaved($schedule)->sum('amount');
    }

    /**
     * @param RealEstatePaymentSchedule $schedule
     * @param Carbon                    $date
     * @return int|mixed|number
     */
    public function calculate(RealEstatePaymentSchedule $schedule, Carbon $date)
    {
        $key = 'schedule_interest_'.$schedule->id.'date'.$date->toDateTimeString();

        if ($v = $this->cache($key)) {
            return $v;
        }

        if ($this->exempted($schedule, $date)) {
            return 0;
        }

        $start_date = $this->getFirstPaymentDate($schedule, $date);

        $payments = $this->holding
            ->payments
            ->filter(function ($payment) use ($schedule, $date, $start_date) {
                $p_date = $payment->date;

                return $p_date->copy()->gte($start_date) &&
                    $p_date->copy()->lte($date) &&
                    $p_date->copy()->gt($schedule->date);
            })
            ->sortByDate('date', 'ASC')
            ->values();

        $end_date = $date->copy();

        if ($payments->count() > 0) {
            $end_date = $payments[0]->date;
        }

        $principal = $schedule->repo->overdue($schedule->date, false);

        $interest = 0;

        $interestStart = $this->getInterestStart($schedule);

        $days = $this->checkExemptedDate($interestStart, $end_date);

        $i = $this->compoundInterest($principal, $interestStart, $end_date, $days);

        $interest += $i;

        foreach ($payments as $index => $payment) {
            $principal = $schedule->repo->overdue($payment->date, false);

            if ($principal <= 0) {
                if ($payment->date->diffInDays($schedule->date) <= static::GRACE_PERIOD) {
                    return 0;
                }
            }

            if ($principal > 0) {
                $end_date = $date->copy();

                if (isset($payments[$index + 1])) {
                    $end_date = $payments[$index + 1]->date;
                }

                $date = \Cytonn\Core\DataStructures\Carbon::latest($interestStart, $payment->date);

                $days = $this->checkExemptedDate($date, $end_date);

                $i = $this->compoundInterest($principal, $date, $end_date, $days);

                $interest += $i;
            }
        }

        $this->cache($key, $interest);

        return $interest;
    }

    private function checkExemptedDate($startDate, $endDate)
    {
        if ($this->project && $this->allowExemptions) {
            if ($this->project->id == 5) {
                $exemptionStart = Carbon::parse('2019-01-01');
                $exemptionEnd = Carbon::parse('2019-05-31');

                return $this->getDaysExcludingExemptionPeriod($startDate, $endDate, $exemptionStart, $exemptionEnd);
            }
        }

        return $endDate->copy()->diffInDays($startDate->copy());
    }

    private function getDaysExcludingExemptionPeriod(
        Carbon $startDate,
        Carbon $endDate,
        Carbon $exemptionStart,
        Carbon $exemptionEnd
    ) {
        $daysBefore = $daysAfter = 0;

        if ($startDate < $exemptionStart) {
            $beforeEnd = $endDate <= $exemptionStart ? $endDate : $exemptionStart;

            $daysBefore = $beforeEnd->diffInDays($startDate);
        }

        if ($endDate > $exemptionEnd) {
            $afterStart = $startDate >= $exemptionEnd ? $startDate : $exemptionEnd;

            $daysAfter = $endDate->diffInDays($afterStart);
        }

        return $daysBefore + $daysAfter;
    }

    private function getInterestStart($schedule)
    {
        if ($schedule->interest_date) {
            return $schedule->interest_date;
        }

        return $schedule->date;
    }

    private function compareStartDateWithLoo(Carbon $dueDate)
    {
        $loo_date = $this->holding->loo->date_signed;

        if ($dueDate->lt($loo_date)) {
            return $loo_date;
        }

        return $dueDate;
    }

    /**
     * @param $principal
     * @param Carbon    $startDate
     * @param Carbon    $endDate
     * @return int|mixed
     */
    private function compoundInterest($principal, Carbon $startDate, Carbon $endDate, $days = null)
    {
        $startDate = $this->compareStartDateWithLoo($startDate);

        if ($principal <= 0) {
            return 0;
        }

        if ($startDate->copy()->gte($endDate->copy())) {
            return 0;
        }

        $rate = 1 + static::INTEREST_RATE/(12* 100);

        $days = (! is_null($days)) ? $days : $endDate->copy()->diffInDays($startDate->copy());

        $days = $days / 30;

        $amount = $principal * pow($rate, $days);

        return $amount - $principal;
    }

    /**
     * @param $schedule
     * @param $date
     * @return bool|Carbon
     */
    private function getFirstPaymentDate($schedule, $date)
    {
        $first_payment = $this->holding
            ->payments
            ->sortByDate('date', 'ASC')
            ->values()
            ->filter(function ($payment) use ($schedule) {
                return $schedule->repo->overdue($payment->date, false) > 0;
            })->first();

        if (!$first_payment) {
            return $date;
        }

        return $first_payment->date;
    }

    /**
     * @param RealEstatePaymentSchedule $schedule
     * @param Carbon|null               $date
     * @return int|mixed|number
     */
    public function interest(RealEstatePaymentSchedule $schedule, Carbon $date = null)
    {
        $date = $date ? Carbon::parse($date) : Carbon::today();

        if ($schedule->interestAccrued()->count() > 0) {
            $paid_on = $schedule->interestAccrued()->first()->payment->date;

            if ($paid_on->lte($date)) {
                return $this->savedInterest($schedule);
            }
        }

        return $this->calculate($schedule, $date);
    }


    /**
     * @param RealEstatePaymentSchedule $schedule
     * @param RealEstatePayment         $payment
     * @param $amount
     * @return RealestateInterestAccrued
     */
    protected function save(RealEstatePaymentSchedule $schedule, RealEstatePayment $payment, $amount)
    {
        return RealestateInterestAccrued::create([
            'schedule_id' => $schedule->id,
            'payment_id' => $payment->id,
            'amount' => $amount
        ]);
    }

    /**
     * Check if client should be levied interest: Has signed Loo and grace period of schedule has passed
     *
     * @param  RealEstatePaymentSchedule $schedule
     * @param  Carbon                    $date
     * @return bool
     */
    protected function notSignedLooOrGracePeriodHasNotReached(RealEstatePaymentSchedule $schedule, Carbon $date)
    {
        $loo = $this->holding->letterOfOffer;

        if (!$loo) {
            return true;
        }

        if (!$loo->date_signed) {
            return true;
        }

        $grace_day = $schedule->repo->interestGracePeriodDate();

        if ($grace_day->copy()->gt($date->copy())) {
            return true;
        }

        return $schedule->repo->overdue($grace_day, false) <= 0;
    }

    protected function exempted(RealEstatePaymentSchedule $schedule, $date)
    {
        return in_array($schedule->type->slug, ['deposit', 'reservation_fee'])

            || $schedule->type->accrues_interest != 1

            || $this->notSignedLooOrGracePeriodHasNotReached($schedule, $date)

            || !$schedule->penalty

            || $schedule->holding->penalty_excempt

            || !$schedule->holding->project->leviesInterest()

            || $schedule->last_schedule == 1;
    }
}
