<?php
/**
 * Date: 06/06/2016
 * Time: 11:25 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Payments;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstateBusinessConfirmation;
use App\Cytonn\Models\RealestateCommission;
use App\Cytonn\Models\RealestateCommissionRate;
use App\Cytonn\Models\RealEstatePayment;
use Cytonn\Mailers\RealEstate\BusinessConfirmationMailer;
use Cytonn\Realestate\Events\UnitAddedToTranche;
use Illuminate\Support\Collection;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class Payment
 *
 * @package Cytonn\Realestate\Payments
 */
class Payment
{
    use EventGenerator, DispatchableTrait;

    /**
     * @var
     */
    protected $payment;

    protected $protectOutput;

    /**
     * Payment constructor.
     *
     * @param $payment
     */
    public function __construct(RealEstatePayment $payment = null)
    {
        $this->payment = $payment;
    }




    public function setProtectOutput(bool $protectOutput)
    {
        $this->protectOutput = $protectOutput;
        return $this;
    }


    /**
     * Add a unit to a tranche and/or set a negotiated price
     *
     * @param  $holding
     * @param  $tranche
     * @param  null    $negotiatedPrice
     * @return mixed
     */
    public function addPricingInformation($holding, $tranche, $negotiatedPrice = null)
    {
        $negotiatedPrice = $this->cleanNegotiatedPrice($negotiatedPrice);

        if ($negotiatedPrice != null) {
            $holding->negotiated_price = $negotiatedPrice;
        }

        $holding->tranche_id = $tranche->id;

        $this->raise(new UnitAddedToTranche($holding));
        $this->dispatchEventsFor($this);

        return $holding->save();
    }

    /**
     * @param $negotiatedPrice
     * @return null
     */
    private function cleanNegotiatedPrice($negotiatedPrice)
    {
        if (is_null($negotiatedPrice)) {
            return null;
        }

        if ((int) $negotiatedPrice <= 0) {
            return null;
        }

        return $negotiatedPrice;
    }

    /**
     * Generate the Business confirmation, from either payment or a saved BC
     *
     * @param null $bcId
     */
    public function generateBusinessConfirmation($bcId = null)
    {
        if (is_null($bcId)) {
            $bc = $this->createBCFromPayment();
            $sender_id = $this->payment->approval->approved_by;
        } else {
            $businessConf = RealEstateBusinessConfirmation::findOrFail($bcId);
            $this->payment = $businessConf->payment;
            $bc = $businessConf->payload;
            $sender_id = $this->payment->approval->approved_by;
        }

        $bc->payments = new Collection($bc->payments);

        $project = $this->payment->holding->project;

        $client = $this->payment->holding->client;

        $pdf = \PDF::loadView('reports.realestate_businessconfirmation', [
                'payment'=>$this->payment,
                'amountRemaining'=> $this->payment->holding->amountRemaining(),
                'business_confirmation'=>$bc,
                'holding'=>$this->payment->holding,
                'client'=> $client,
                'project'=>$project,
                'unit'=>$this->payment->holding->unit,
                'sender_id'=>$sender_id,
                'logo'=>$project->fundManager->re_logo,
                'signature' => $project->fundManager->signature,
                'emailSender' => $project->fundManager->fullname
            ]);

        if ($this->protectOutput) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    /**
     * @return \stdClass
     */
    private function createBCFromPayment()
    {
        $bc = new \stdClass();
        $bc->project = $this->payment->holding->project->name;
        $bc->price = $this->payment->holding->price(false);
        $bc->discounted_price = $this->payment->holding->price();
        $bc->discount = $this->payment->holding->discount;
        $bc->unit_number = $this->payment->holding->unit->number;
        $bc->payments = $this->payment->holding->payments()->latest('date')->get()
            ->map(
                function ($payment) {
                    $p = new \stdClass();
                    $p->amount = $payment->amount;
                    $p->name = $payment->present()->bcTag;
                    $p->id = $payment->id;

                    return $p;
                }
            )->toArray();
        $bc->total = $this->payment->holding->totalPayments();
        $bc->amount_remaining = $this->payment->holding->amountRemaining();

        return $bc;
    }

    /**
     * Create a new business confirmation using the payment
     */
    public function sendBusinessConfirmation()
    {
        $mailer = new BusinessConfirmationMailer();

        $this->setProtectOutput(true);
        $bc = $this->generateBusinessConfirmation();

        $client = $this->payment->holding->client;

        $mailer->sendBC($client, $bc, $this->payment);

        $this->saveBusinessConfirmation();
    }

    /**
     * Check if a business confirmation has been sent
     *
     * @return bool
     */
    public function checkBCStatus()
    {
        return RealEstateBusinessConfirmation::where('payment_id', $this->payment->id)->count() > 0;
    }

    /**
     * Save a business confirmation
     */
    private function saveBusinessConfirmation()
    {
        $businessConf = new RealEstateBusinessConfirmation();
        $businessConf->payment_id = $this->payment->id;
        $businessConf->payload = $this->createBCFromPayment();
        $businessConf->sent_by = \Auth::user() ? \Auth::user()->id : $this->payment->approval->approved_by;
        $businessConf->save();
    }

    /**
     * @param $recipient_id
     * @param $awarded
     * @param $commission_rate
     * @return RealestateCommission
     */
    public function initCommission($recipient_id, $awarded, $commission_rate = null, $reason = null)
    {
        CommissionRecepient::findOrFail($recipient_id);

        $holding = $this->payment->holding;

        $commission = new RealestateCommission();

        if ($holding->commission) {
            $commission = $holding->commission;
        }

        $commission->recipient_id = $recipient_id;
        $commission->holding_id = $this->payment->holding_id;
        $commission->awarded = $awarded;
        $commission->commission_rate = $commission_rate;
        $commission->reason = $reason;
        $commission->save();

        return $commission;
    }
}
