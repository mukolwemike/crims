<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 07/02/2019
 * Time: 12:52
 */

namespace App\Cytonn\Realestate\Payments;

use App\Cytonn\Models\ClientUser as User;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Cytonn\Models\RealEstateInstructionType;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClientPaymentRepository
{
    public function makePayment($clientId, Request $request, User $user = null)
    {
        $input = $request->all();

        $instruction_type = RealEstateInstructionType::where('slug', '=', $input['type'])->first();

        $instruction = new RealEstateInstruction();
        $instruction->client_id = $clientId;
        $instruction->holding_id = $input['holding_id'];
        $instruction->payload = json_encode($input);
        $instruction->user_id = $user ? $user->id : null;
        $instruction->instruction_type_id = $instruction_type->id;

        $instruction->save();

        if ($request->hasFile('file')) {
            $this->uploadProof($request, $clientId, $instruction->id);
        }

        return $instruction;
    }

    public function uploadPaymentProof($paymentId, Request $request)
    {
        $instruction = RealEstateInstruction::findOrFail($paymentId);

        $this->uploadProof($request, $instruction->client_id, $paymentId);

        return $instruction;
    }

    protected function uploadProof(Request $request, $clientId, $instructionId)
    {
        $file = $request->file('file');
        $slug = ($request['slug']) ? $request['slug'] : 'payment';

        if (str_contains($file->getClientMimeType(), 'pdf') ||
            str_contains($file->getClientMimeType(), 'image')) {
            $contents = file_get_contents($file);
            $ext = $file->getClientOriginalExtension();

            $document = (new Document())
                ->upload($contents, $ext, $slug, 'Proof of payment', Carbon::today());
            $document->client_id = $clientId;
            $document->save();

            $instruction = RealEstateInstruction::findOrFail($instructionId);

            $instruction->document_id = $document->id;
            $instruction->save();
        } else {
            return response(json_encode(['file' => ['The file should be an image or pdf']]));
        }
    }
}
