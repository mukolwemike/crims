<?php
/**
 * Date: 21/12/2016
 * Time: 12:34
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Payments;

use App\Cytonn\Core\DataStructures\Caching\LocalCache;
use App\Cytonn\Models\RealestateInterestAccrued;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class PaymentScheduleRepository
 *
 * @package Cytonn\Realestate\Payments
 */
class PaymentScheduleRepository
{
    use LocalCache;

    /**
     * @var RealEstatePaymentSchedule
     */
    protected $schedule;

    /**
     * PaymentScheduleRepository constructor.
     *
     * @param $schedule
     */
    public function __construct(RealEstatePaymentSchedule $schedule = null)
    {
        is_null($schedule) ? $this->schedule = new RealEstatePaymentSchedule() : $this->schedule = $schedule;
    }

    /**
     * @param Carbon|null $date
     * @return mixed
     */
    public function total(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        if ($this->schedule->last_schedule == 1) {
            return $this->schedule->amount;
        }

        $interest = new Interest($this->schedule->holding);

        return $this->schedule->amount + $interest->interest($this->schedule, $date);
    }

    /**
     * @param Carbon|null $date
     * @param bool $includeInterest
     * @return int|mixed
     */
    public function overdue(Carbon $date = null, $includeInterest = true)
    {
        $date = Carbon::parse($date);

        ini_set('xdebug.max_nesting_level', 10000);

        $intr = $includeInterest ? 'true' : 'false';

        $key = 'overdue_on__' . $date . '_interest_set_to_' . $intr . '_schedule_id_' . $this->schedule->id;

        if ($v = $this->cache($key)) {
            return $v;
        }

        $out = $this->calculateOverdue($date, $includeInterest);

        $this->cache($key, $out);

        return $out;
    }

    /**
     * @return mixed
     */
    public function interestAccrualDate()
    {
        $scheduled = $this->schedule->date;

        if (!$loo = $this->schedule->holding->loo) {
            return $scheduled;
        }

        if (!$date = $loo->date_signed) {
            return $scheduled;
        }

        if ($date->gt($scheduled)) {
            return $date;
        }

        return $scheduled;
    }

    /**
     * @param Carbon $asAtDate
     * @return mixed
     */
    public function endDate(Carbon $asAtDate)
    {
        $key = 'schedule_end_date' . $this->schedule->id . $asAtDate->toDateTimeString();

        if ($v = $this->cache($key)) {
            return $v;
        }

        $interest = RealestateInterestAccrued::where('schedule_id', $this->schedule->id)->first();

        if ($interest) {
            return $this->cache($key, $interest->payment->date);
        }

        return $this->cache($key, $asAtDate);

        if ($date_paid = $this->datePaid()) {
            return $this->cache($key, $date_paid);
        }

        return $this->cache($key, $asAtDate);
    }

    /**
     * @return bool
     */
    public function datePaid()
    {
        $holding = $this->schedule->holding;

        $payments = $holding->payments()->oldest()->get();

        $amountDue = $this->amountDue(Carbon::today());

        $payment = $payments->filter(
            function ($payment) use ($amountDue) {
                $cumulative = $this->schedule->holding
                    ->payments()->before($payment->date)
                    ->sum('amount');

                return $cumulative >= $amountDue;
            }
        )->first();

        if ($payment) {
            return $payment->date;
        }

        return false;
    }

    /**
     * @param Carbon|null $date
     * @param bool $includeInterest
     * @return int|mixed
     */
    private function calculateOverdue(Carbon $date = null, $includeInterest = true)
    {
        $date = Carbon::parse($date);

        $key = 'raw_overdue__' . $this->schedule->id . '' . $date . 'int' . $includeInterest;

        if ($v = $this->cache($key)) {
            return $v;
        }

        if ($date->copy()->lt($this->schedule->date)) {
            return 0;
        }

        $debt = $this->netOverdue($date, $includeInterest);

        $interest = new Interest($this->schedule->holding);

        $accrued = $includeInterest ? $interest->interest($this->schedule, $date) : 0;

        $total = $this->schedule->amount + $accrued;

        if ($debt < 0) {
            return $this->cache($key, 0);
        }

        if (abs($debt) < $total) {
            return $this->cache($key, abs($debt));
        }

        return $this->cache($key, $total);
    }

    /**
     * @param Carbon $date
     * @return mixed
     */
    public function amountDue(Carbon $date)
    {
        return $this->schedule->holding->paymentSchedules()
            ->inPricing()
            ->before($date)
            ->sum('amount');
    }

    /**
     * @param Carbon|null $date
     * @param $includeInterest
     * @return mixed
     */
    private function netOverdue(Carbon $date = null, $includeInterest)
    {
        $key = 'net_overdue__' . $date . 'int' . $includeInterest . 'schedule' . $this->schedule->id;

        if ($v = $this->cache($key)) {
            return $v;
        }

        $date = Carbon::parse($date);

        $holding = $this->schedule->holding;

        $scheduled = $holding->paymentSchedules()
            ->inPricing()
            ->accruesInterest()
            ->before($this->schedule->date)
            ->oldest('date')
            ->remember(Interest::CACHE)
            ->get()
            ->sum(
                function (RealEstatePaymentSchedule $schedule) use ($holding, $date, $includeInterest) {
                    if ($schedule->date == $this->schedule->date && $schedule->id > $this->schedule->id) {
                        return 0;
                    }

                    $interest = new Interest($holding);

                    $accrued = 0;

                    $same_schedule = $schedule->id == $this->schedule->id;

                    if (!$same_schedule || $includeInterest) {
                        $accrued = $interest->interest($schedule, $date);
                    }

                    return $schedule->amount + $accrued;
                }
            );

        $paid = $holding->payments()
            ->before($date)
            ->sum('amount');


        $out = $scheduled - $paid;

        $this->cache($key, $out);

        return $out;
    }

    public function amountPaid(Carbon $date = null)
    {
        //overdue as at grace period, if zero return principal
        $date = Carbon::parse($date);

        $overdue_as_at_grace_period = $this->overdue($this->interestGracePeriodDate());
        if ($overdue_as_at_grace_period == 0) {
            return $this->schedule->amount;
        }

        //return principal + interest - overdue
        $overdue_today = $this->overdue($date, true);
        $principal = $this->overdue($this->schedule->date);
        $interest = (new Interest($this->schedule->holding))->interest($this->schedule, $date);

        return $principal + $interest - $overdue_today;
    }

    public function interestGracePeriodDate()
    {
        /**
         * @var Carbon
         */
        $date = Carbon::parse($this->schedule->date);

        $schedule_grace = $date->copy()->addDays(Interest::GRACE_PERIOD);

        $project = $this->schedule->holding->project;

        if ($project_grace = $project->penalty_start_date) {
            return \Cytonn\Core\DataStructures\Carbon::latest($schedule_grace, $project_grace);
        }

        return $schedule_grace;
    }
}
