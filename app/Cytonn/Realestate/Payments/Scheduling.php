<?php
/**
 * Date: 06/06/2016
 * Time: 11:29 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Payments;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;

class Scheduling
{
    public function scheduleReservationFee(UnitHolding $holding)
    {
        $amount = $holding->project->reservationFee();

        return RealEstatePaymentSchedule::make('reservation_fee', $holding, $amount, 'Reservation fee');
    }
}
