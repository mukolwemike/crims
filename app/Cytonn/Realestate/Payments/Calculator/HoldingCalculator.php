<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Realestate\Payments\Calculator;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstateRefund;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\Unitization\BenchmarkValue;
use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;

class HoldingCalculator
{
    use CalculatorTrait;

    const MINIMUM_PAYMENT_PERCENTAGE = 60;

    const DEFAULT_BOND_RATE = 10.7;

    private $interestRate = null;

    private $holding;

    private $startDate;

    private $endDate;

    private $unitPrice;

    private $minimumAmount;

    private $prepared;

    /**
     * HoldingCalculator constructor.
     * @param UnitHolding $holding
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(UnitHolding $holding, Carbon $startDate, Carbon $endDate)
    {
        $this->holding = $holding;

        $this->startDate = $startDate;

        $this->endDate = $endDate;

        $this->unitPrice = $this->holding->price();

        $this->minimumAmount = (static::MINIMUM_PAYMENT_PERCENTAGE * $this->unitPrice) / 100;

        $this->interestRate = $this->getBondRate();

        $this->prepared = $this->prepare();
    }

    /**
     * @return object
     */
    private function prepare()
    {
        $netPaidAmount = $this->getNetAmountPaid($this->startDate);

        $actions = $this->getPaymentsActions();

        $startDate = $this->startDate->copy();

        $cumulativeNetInterest = 0;

        $actions = $actions->map(function ($action) use (&$startDate, &$netPaidAmount, &$cumulativeNetInterest) {

            $endDate = $action->date;

            if ($action instanceof RealEstatePayment) {
                $changeAmount = $action->amount;
            } elseif ($action instanceof RealEstateRefund) {
                $changeAmount = -$action->amount;
            }

            $prepared = $this->calculate($startDate, $endDate, $netPaidAmount, $changeAmount, $cumulativeNetInterest);
            $prepared->action = $action;

            $startDate = $prepared->end->copy();
            $netPaidAmount = $prepared->net_paid_amount;
            $cumulativeNetInterest = $prepared->cumulative_net_interest;

            return $prepared;
        });

        $last = $this->calculate($startDate, $this->endDate->copy(), $netPaidAmount, 0, $cumulativeNetInterest);

        return (object) [
            'actions' => $actions->all(),
            'total' => $last
        ];
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param $netPaidAmount
     * @param $changeAmount
     * @param $cumulativeNetInterest
     * @return object
     */
    private function calculate(Carbon $startDate, Carbon $endDate, $netPaidAmount, $changeAmount, $cumulativeNetInterest)
    {
        $principal = $netPaidAmount -  $this->minimumAmount;

        $grossInterest = $netInterest = 0;

        if ($netPaidAmount > $this->minimumAmount) {
            $grossInterest = $this->getGrossInterest($principal, $this->interestRate, $startDate, $endDate);
            $taxable = $this->checkTaxable($this->holding->client, $endDate);
            $netInterest = $this->getNetInterest($grossInterest, $taxable);
        }

        $newCumulativeNetInterest = $cumulativeNetInterest + $netInterest;

        $newPaidAmount = $netPaidAmount + $changeAmount;

        return (object) [
            'net_paid_amount_before' => $netPaidAmount,
            'minimum_amount' =>  $this->minimumAmount,
            'net_paid_amount' => $newPaidAmount,
            'principal' => $principal,
            'change_amount' => $changeAmount,
            'start' => $startDate,
            'end' => $endDate,
            'gross_interest' => $grossInterest,
            'net_interest' => $netInterest,
            'cumulative_net_interest' => $newCumulativeNetInterest
        ];
    }

    /**
     * @param Carbon $date
     * @return mixed
     */
    private function getNetAmountPaid(Carbon $date)
    {
        $total = $this->holding->totalPayments($date);

        $refunds = $this->holding->refundedAmount($date);

        return $total - $refunds;
    }

    /**
     * @return mixed
     */
    private function getPaymentsActions()
    {
        $payments = $this->holding->payments()->between($this->startDate, $this->endDate)->get();

        $refunds = $this->holding->refunds()->between($this->startDate, $this->endDate)->get();

        $actions = $payments->merge($refunds->all());

        $actions = $actions->sortByDate('date', 'ASC');

        return $actions->values();
    }

    /**
     * @param Client $client
     * @param Carbon $date
     * @return bool
     */
    private function checkTaxable(Client $client, Carbon $date)
    {
        if (!$client->taxable) {
            return false;
        }

        if ($client->relationLoaded('taxExemptions') && !$client->taxExemptions->count()
        ) {
            return true;
        }

        if (! $client->taxExemptions()->exists()) {
            return true;
        }

        $exempt = !$client->taxExemptions()
            ->where(function ($q) use ($date) {
                $q->where(function ($q) use ($date) {
                    $q->where('start', '<=', $date)->where('end', '>=', $date);
                })
                    ->orWhere(function ($q) use ($date) {
                        $q->where('start', '<=', $date)->whereNull('end');
                    })
                    ->orWhere(function ($q) use ($date) {
                        $q->whereNull('start')->where('end', '>=', $date);
                    })
                    ->orWhere(function ($q) {
                        $q->whereNull('start')->whereNull('end');
                    });
            })->exists();

        return (bool) $exempt;
    }

    /**
     * @return object
     */
    public function getPrepared()
    {
        return $this->prepared;
    }

    /**
     * @return mixed
     */
    public function netInterest()
    {
        return $this->prepared->total->cumulative_net_interest;
    }

    /**
     * @return mixed
     */
    public function netPaidAmount()
    {
        return $this->prepared->total->net_paid_amount;
    }

    /**
     * @return mixed
     */
    public function principal()
    {
        return $this->prepared->total->principal;
    }

    /**
     * @return float
     */
    private function getBondRate()
    {
        $value = BenchmarkValue::forBenchmarkName('T-bond-5')->latest('date')->first();

        return $value ? $value->value : static::DEFAULT_BOND_RATE;
    }

    /**
     * @return float|null
     */
    public function getInterestRate()
    {
        return $this->interestRate;
    }

    /**
     * @return UnitHolding
     */
    public function getHolding()
    {
        return $this->holding;
    }

    /**
     * @return int
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @return float|int
     */
    public function getMinimumAmount()
    {
        return $this->minimumAmount;
    }

    /**
     * @return Carbon
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return Carbon
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
}
