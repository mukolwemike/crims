<?php

namespace Cytonn\Realestate\Payments;

use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Maatwebsite\Excel\Facades\Excel;

class PaymentRemindersForUpcomingRealEstatePaymentsExcelGenerator
{
    public function excel($file_name, $reminders)
    {
        return Excel::create(
            $file_name,
            function ($excel) use ($reminders) {
                $excel->sheet(
                    'Payment Reminders',
                    function ($sheet) use ($reminders) {
                        $reminders = $reminders->map(
                            function ($reminder) {
                                $r = new EmptyModel();
                                $r->ClientCode = $reminder->holding->client->client_code;
                                $r->ClientName = ClientPresenter::presentFullNames($reminder->holding->client_id);
                                $r->UnitNumber = $reminder->holding->unit->number;
                                $r->UnitType =
                                    $reminder->holding->unit->size->name . ' ' . $reminder->holding->unit->type->name;
                                $r->Amount = AmountPresenter::currency($reminder->amount);
                                $r->FA = is_null($reminder->holding->commission->recipient->name)
                                    ? null
                                    : $reminder->holding->commission->recipient->name;
                                return $r;
                            }
                        );
                        $sheet->fromModel($reminders);
                        $sheet->prependRow(
                            ['Payment Reminders - ' .
                                DatePresenter::formatDate(Carbon::today()->addDays(14)->toDateString())]
                        );
                        $sheet->mergeCells('A1:F1');
                    }
                );
            }
        );
    }
}
