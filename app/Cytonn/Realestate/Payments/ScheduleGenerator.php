<?php
/**
 * Date: 11/10/2016
 * Time: 14:38
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Payments;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;

/**
 * Class ScheduleGenerator
 *
 * @package Cytonn\Realestate\Payments
 */
class ScheduleGenerator
{
    protected $spacing;

    /**
     * @param UnitHolding $holding
     * @param null $spacing
     * @return array
     */
    public function generate(UnitHolding $holding, $spacing = null)
    {
        $this->spacing = $spacing;

        $paymentPlanSlug = $holding->paymentPlan->slug;
        $price = $holding->price();
        $payment_schedules = [];
        $dateLooSigned = (new Carbon($holding->payments->first()->date))->addDays(30);
        $project = $holding->project;

        switch ($paymentPlanSlug) {
            case 'cash':
                $payment_schedules = [
                    [
                        'description' => '10% Deposit',
                        'payment_type_id' =>
                            RealEstatePaymentType::where('slug', 'deposit')->first()->id . '',
                        'date' => $dateLooSigned->toDateString(),
                        'amount' => round($price * 0.1, 2)
                    ],
                    [
                        'description' => 'Final Deposit',
                        'payment_type_id' =>
                            RealEstatePaymentType::where('slug', 'installment')->first()->id . '',
                        'date' => $dateLooSigned->copy()->addDays(90)->toDateString(),
                        'amount' => round($price * 0.9, 2)
                    ]
                ];
                break;

            case 'installment':
                $deposit = round($price * 0.1, 2);

                $lastInstallment = round($price * 0.1, 2);

                $totalInstallmentAmount = 0;

                $payment_schedules = [
                    [
                        'description' => '10% Deposit',
                        'payment_type_id' =>
                            RealEstatePaymentType::where('slug', 'deposit')->first()->id . '',
                        'date' => $dateLooSigned->toDateString(),
                        'amount' => $deposit
                    ]
                ];

                $numberOfInstallments = $this->getNumberOfInstallments($project);

                for ($i = 0; $i < $numberOfInstallments; $i++) {
                    $paymentSchedule = $this->getInstallment(
                        $i,
                        $project,
                        $dateLooSigned,
                        $price,
                        $deposit,
                        $lastInstallment,
                        $numberOfInstallments,
                        $totalInstallmentAmount
                    );

                    $payment_schedules[] = $paymentSchedule;

                    $totalInstallmentAmount += $paymentSchedule['amount'];
                }

                $payment_schedules[] = [
                    'description' => 'Final Installment',
                    'payment_type_id' =>
                        RealEstatePaymentType::where('slug', 'installment')->first()->id . '',
                    'date' => is_null($project->completion_date) ? null : $project->completion_date->toDateString(),
                    'amount' => $lastInstallment
                ];

                break;

            case 'mortgage':
                $payment_schedules = [
                    [
                        'description' => '10% Deposit',
                        'payment_type_id' =>
                            RealEstatePaymentType::where('slug', 'deposit')->first()->id . '',
                        'date' => $dateLooSigned->toDateString(),
                        'amount' => round($price * 0.1, 2)
                    ],
                    [
                        'description' => 'Final Deposit',
                        'payment_type_id' =>
                            RealEstatePaymentType::where('slug', 'installment')->first()->id . '',
                        'date' => (new Carbon($holding->unit->project->completion_date))->toDateString(),
                        'amount' => round($price * 0.9, 2)
                    ]
                ];
                break;
            case 'zero_deposit_mortgage':
                // Override $dateLooSigned due to how months are signed
                $dateLooSigned = (new Carbon($holding->loo->date_signed))->addMonths(-1);

                $ten_percent = round($price * 0.1, 2);

                $totalDepositInstallment = 0;

                for ($i = 0; $i < 6; $i++) {
                    $date =
                        $this->getInstallmentDate(
                            $dateLooSigned,
                            (new Carbon($dateLooSigned))->addMonthsNoOverflow(6),
                            $i + 1,
                            6,
                            1
                        );

                    $amount = $i + 1 == 6 ? $ten_percent - $totalDepositInstallment :
                        roundToHundred(round((double)$ten_percent / 6, 2));

                    $payment_schedules[] = [
                        'description' => 'Installment ' . ($i + 1) . ' - ' . (new Carbon($date))->format('M Y'),
                        'payment_type_id' =>
                            RealEstatePaymentType::where('slug', 'deposit')->first()->id . '',
                        'date' => $date,
                        'amount' => $amount
                    ];

                    $totalDepositInstallment += $amount;
                }

                $last_installment = [
                    'description' =>
                        'Final Installment - ' .
                        (new Carbon($holding->unit->project->completion_date))->format('M Y'),
                    'payment_type_id' =>
                        RealEstatePaymentType::where('slug', 'installment')->first()->id . '',
                    'date' => (new Carbon($holding->unit->project->completion_date))->toDateString(),
                    'amount' => round($price * 0.9, 2)
                ];

                $payment_schedules[] = $last_installment;
                break;

            case 'zero_deposit_installment':
                // Override $dateLooSigned due to how months are signed
                $dateLooSigned = (new Carbon($holding->loo->date_signed))->addMonths(-1);

                $deposit = round($price * 0.1, 2);

                $lastInstallment = round($price * 0.1, 2);

                $totalZeroDepositInstallment = 0;

                $date = $dateLooSigned;

                for ($i = 0; $i < 6; $i++) {
                    $date =
                        $this->getInstallmentDate(
                            $dateLooSigned,
                            (new Carbon($dateLooSigned))->addMonthsNoOverflow(6),
                            $i + 1,
                            6,
                            1
                        );

                    $amount = $i + 1 == 6 ? (double)$deposit - $totalZeroDepositInstallment :
                        roundToHundred(round((double)$deposit / 6, 2));

                    $payment_schedules[] = [
                        'description' =>
                            'Deposit Installment ' . ($i + 1) . ' - ' . (new Carbon($date))->format('M Y'),
                        'payment_type_id' =>
                            RealEstatePaymentType::where('slug', 'deposit')->first()->id . '',
                        'date' => $date,
                        'amount' => $amount
                    ];

                    $totalZeroDepositInstallment += $amount;
                }

                $numberOfInstallments = $this->getNumberOfInstallments($project);

                $totalZeroInstallment = 0;

                for ($i = 0; $i < $numberOfInstallments; $i++) {
                    $schedule = $this->getInstallment(
                        $i,
                        $project,
                        $date,
                        $price,
                        $deposit,
                        $lastInstallment,
                        $numberOfInstallments,
                        $totalZeroInstallment
                    );

                    $payment_schedules[] = $schedule;

                    $totalZeroInstallment += $schedule['amount'];
                }

                $payment_schedules[] = [
                    'description' => 'Final Installment',
                    'payment_type_id' => RealEstatePaymentType::where('slug', 'installment')
                            ->first()->id . '',
                    'date' => $project->completion_date->toDateString(),
                    'amount' => $lastInstallment
                ];

                break;
            case 'default':
                throw new \InvalidArgumentException("The payment plan $paymentPlanSlug is not supported");
                break;
        }

        return $payment_schedules;
    }

    /**
     * @param $index
     * @param $project
     * @param $dateLooSigned
     * @param $price
     * @param $deposit
     * @param $lastInstallment
     * @param $numberOfInstallments
     * @return array
     */
    private function getInstallment(
        $index,
        $project,
        $dateLooSigned,
        $price,
        $deposit,
        $lastInstallment,
        $numberOfInstallments,
        $totalInstallmentAmount
    ) {
        if ($numberOfInstallments == $index + 1) {
            $amount = $price - ($deposit + $lastInstallment + $totalInstallmentAmount);
        } else {
            $amount = roundToHundred(($price - ($deposit + $lastInstallment)) / $numberOfInstallments);
        }

        $paymentType = RealEstatePaymentType::where('slug', 'installment')->first();

        return [
            'description' => 'Installment ' . ($index + 1),
            'payment_type_id' => $paymentType->id . '',
            'date' => $this->getInstallmentDate(
                new Carbon($dateLooSigned),
                $project->completion_date,
                $index + 1,
                9,
                $this->spacing
            ),
            'amount' => round($amount, 2)
        ];
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param $rank
     * @param $total_installments
     * @param null $max_interval
     * @return string
     */
    private function getInstallmentDate(
        Carbon $start,
        Carbon $end,
        $rank,
        $total_installments,
        $max_interval = null
    ) {
        $diff = $end->copy()->diffInMonths($start->copy());
        $installment_diff = $diff / $total_installments;

        if ($max_interval) {
            return $start->addMonthsNoOverflow($max_interval * $rank)->endOfMonth()->toDateString();
        }
        return $start->addMonthsNoOverflow($installment_diff * $rank)->endOfMonth()->toDateString();
    }

    /**
     * @param Project $project
     * @return float|int
     */
    private function getNumberOfInstallments(Project $project)
    {
        $numberOfInstallments = $this->calculateNumberOfInstallments($project, $this->spacing);

        if ($numberOfInstallments > 8) {
            $this->spacing++;
            $numberOfInstallments = $this->calculateNumberOfInstallments($project, $this->spacing);
        }

        if ($numberOfInstallments <= 0) {
            $this->spacing--;
            $this->spacing = $this->spacing == 0 ? 1 : $this->spacing; // Do not allow spacing = 0
            $numberOfInstallments = $this->calculateNumberOfInstallments($project, $this->spacing);
        }

        return $numberOfInstallments;
    }

    /**
     * @param Project $project
     * @return float|int
     */
    private function calculateNumberOfInstallments(Project $project)
    {
        return round(Carbon::parse($project->completion_date)->diffInMonths(Carbon::today()) / $this->spacing, 0);
    }
}
