<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Realestate\Payments\InterestAccrual;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Support\Facades\DB;

class ProjectAccrualManager
{
    private $project;

    private $startDate;

    private $endDate;

    private $reportMode = false;

    private $holdingArray = [];

    /**
     * ProjectAccrualManager constructor.
     * @param Project $project
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Project $project, Carbon $startDate, Carbon $endDate)
    {
        $this->project = $project;

        $this->startDate = $startDate;

        $this->endDate = $endDate;
    }

    /**
     * @param bool $reportMode
     */
    public function setReportMode(bool $reportMode)
    {
        $this->reportMode = $reportMode;

        return $this;
    }

    /**
     * @throws \Throwable
     */
    public function process()
    {
        try {
            $currentHolding = null;

            DB::transaction(function () use (&$currentHolding) {
                $holdings = $this->getHoldings();

                foreach ($holdings as $holding) {
                    $currentHolding = $holding;

                    $calc = $this->processHolding($holding);

                    if ($this->reportMode) {
                        $this->saveHoldingReportData($holding, $calc);
                    }
                }
            });
        } catch (\Exception $e) {
            $this->notifyOnFailure($e, $currentHolding);
        }

        if ($this->reportMode) {
            $this->sendReport();
        }
    }

    /**
     * @return mixed
     */
    private function getHoldings()
    {
        return UnitHolding::inProjectIds([$this->project->id])->active()->has('payments')->get();
    }

    /**
     * @param UnitHolding $holding
     * @return \App\Cytonn\Realestate\Payments\Calculator\HoldingCalculator|void
     * @throws \App\Exceptions\CrimsException
     */
    public function processHolding(UnitHolding $holding)
    {
        return (new HoldingAccrualManager($holding, $this->startDate, $this->endDate))
            ->setReportMode($this->reportMode)
            ->process();
    }

    /**
     * @param \Exception $exception
     */
    private function notifyOnFailure(\Exception $exception, $holding)
    {
        Mailer::compose()
            ->to(config('system.administrators'))
            ->subject('Project Interest Accrual ' . $this->endDate->toDateString())
            ->text("Holding : " . @$holding->id . "<br>Project : " . $this->project->name . "<br>Content : " . $exception->getMessage())
            ->send();
    }

    /**
     * @param UnitHolding $holding
     * @param $calculator
     */
    private function saveHoldingReportData(UnitHolding $holding, $calculator)
    {
        $principal = $calculator->principal();

        $unpaidAmount = $holding->repo->getTotalUnpaidSchedulesAmount();

        $paymentAmount = $calculator->netInterest() >= $unpaidAmount ? $unpaidAmount : $calculator->netInterest();

        $this->holdingArray[] = [
            'Client Code' => $holding->client->client_code,
            'Client Name' => ClientPresenter::presentFullNames($holding->client_id),
            'Project' => $holding->project->name,
            'Unit Price' => $calculator->getUnitPrice(),
            'Interest Rate' => $calculator->getInterestRate(),
            'Start Date' => $calculator->getStartDate()->toDateString(),
            'End Date' => $calculator->getEndDate()->toDateString(),
            'Minimum Amount' => $calculator->getMinimumAmount(),
            'Net Payments' => $calculator->netPaidAmount(),
            'Eligible Amount' => $principal > 0 ? $principal : 0,
            'Net Interest' => $calculator->netInterest(),
            'Unpaid Amount' => $unpaidAmount,
            'Amount To Be Paid' => $paymentAmount
        ];
    }

    /**
     * @return bool
     */
    private function sendReport()
    {
        $fileName = 'Real Estate Interest - '. $this->endDate->toDateString();

        ExcelWork::generateAndStoreSingleSheet($this->holdingArray, $fileName);

        Mailer::compose()
            ->to([])
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the real estate interest report for '
             . $this->project->name . " between " . $this->startDate->toDateString() . " and "
            . $this->endDate->toDateString())
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }
}
