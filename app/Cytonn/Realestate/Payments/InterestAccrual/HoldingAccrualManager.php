<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Realestate\Payments\InterestAccrual;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\RealEstate\RealestateInterestPayment;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Realestate\Payments\Calculator\HoldingCalculator;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Presenters\ClientPresenter;

class HoldingAccrualManager
{
    private $holding;

    private $startDate;

    private $endDate;

    private $reportMode = false;

    public function __construct(UnitHolding $holding, Carbon $startDate, Carbon $endDate)
    {
        $this->holding = $holding;

        $this->startDate = $startDate;

        $this->endDate = $endDate;
    }

    /**
     * @param bool $reportMode
     */
    public function setReportMode(bool $reportMode)
    {
        $this->reportMode = $reportMode;

        return $this;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     */
    public function process()
    {
        if ($this->validateInterestPayment()) {
            throw new CrimsException("There is an existing interest payment for the holding : "
                . $this->holding->id . " and period " . $this->startDate->toDateString() . " to " .
                $this->endDate->toDateString());
        }

        $calculator = new HoldingCalculator($this->holding, $this->startDate->copy(), $this->endDate->copy());

        $netInterest = $calculator->netInterest();

        if ($this->reportMode) {
            return $calculator;
        }

        if ($netInterest <= 0) {
            return;
        }

        $inflowApproval = $this->processInflow($netInterest);

        $unpaidAmount = $this->holding->repo->getTotalUnpaidSchedulesAmount();

        $paymentAmount = $unpaidAmount >= $netInterest ? $netInterest : $unpaidAmount;

        $rePaymentApproval = $this->processMakeRealEstatePayment($paymentAmount);

        $this->recordPaymentInterests($rePaymentApproval, $inflowApproval);
    }

    /**
     * @param $amount
     */
    private function processInflow($amount)
    {
        $approval = $this->saveInflowApproval($amount);

        $this->approveClientTransaction($approval);

        return $approval;
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     */
    private function saveInflowApproval($amount)
    {
        $data = $this->prepareInflowApprovalData();

        $data['amount'] = $amount;

        $approval = ClientTransactionApproval::create([
            'client_id' => $data['client_id'],
            'transaction_type' => 'add_cash_to_account',
            'payload' => $data,
            'sent_by' => 1,
            'approved' => 1,
            'approved_by' => 1,
            'approved_on' => Carbon::now(),
            'awaiting_stage_id' => 6
        ]);

        (new Approval($approval))->forceApprove(getSystemUser());

        return $approval;
    }

    /**
     * @return array
     */
    private function prepareInflowApprovalData()
    {
        $input = array();

        $input['id'] = null;

        $input['type'] = CustodialTransactionType::where('name', 'FI')->first()->id;

        $input['custodial_account_id'] = $this->getProjectAccount()[$this->holding->unit->project_id];

        $input['project_id'] = $this->holding->unit->project_id;

        $input['exchange_rate'] = 1; //cash is always in destination currency

        $input['source'] = 'deposit';

        $input['date'] = $this->endDate;

        $input['client_id'] = $this->holding->client_id;

        $input['received_from'] = ClientPresenter::presentFullNames($this->holding->client_id);

        $fa = $this->holding->client->getLatestFa();

        $input['commission_recipient_id'] = $fa ? $fa->id : null;

        $input['description'] = 'Real Estate Payments Interest Deposited';

        return $input;
    }

    /**
     * @param $approval
     */
    protected function approveClientTransaction($approval)
    {
        $handler = $approval->handler();

        $approve = new Approval($approval);

//        $approve->lockForUpdate();

        $handler->handle($approval);
    }

    /**
     * @param $netInterest
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function processMakeRealEstatePayment($netInterest)
    {
        $approval = $this->saveRePaymentApproval($netInterest);

        $this->approveClientTransaction($approval);

        return $approval;
    }

    /**
     * @param $amount
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function saveRePaymentApproval($amount)
    {
        $input = [
            'date' => $this->endDate,
            'amount' => $amount,
            'description' => "Real Estate Accrued Interest Payment",
            'holding_id' => $this->holding->id
        ];

        $approval = ClientTransactionApproval::create([
            'client_id' => $this->holding->client_id,
            'transaction_type' => 'make_real_estate_payment',
            'payload' => $input,
            'sent_by' => 1,
            'approved' => 1,
            'approved_by' => 1,
            'approved_on' => Carbon::now(),
            'awaiting_stage_id' => 6
        ]);

        (new Approval($approval))->forceApprove(getSystemUser());

        return $approval;
    }

    /**
     * @param $paymentApproval
     * @param $inflowApproval
     */
    private function recordPaymentInterests($paymentApproval, $inflowApproval)
    {
        $payments = RealEstatePayment::where('approval_id', $paymentApproval->id)
            ->where('holding_id', $this->holding->id)
            ->get();

        $clientPayment = ClientPayment::where('approval_id', $inflowApproval->id)->first();

        $payments->each(function (RealEstatePayment $payment) use ($clientPayment) {
            RealestateInterestPayment::create([
                'holding_id' => $this->holding->id,
                'payment_id' => $payment->id,
                'client_payment_id' => $clientPayment->id
            ]);
        });
    }

    /**
     * @return bool
     */
    private function validateInterestPayment()
    {
        return ClientPayment::where('type_id', 1)
            ->where('date', '>=', $this->startDate)
            ->where('date', '<=', $this->endDate)
            ->whereHas('realEstateInterests', function ($q) {
                $q->where('holding_id', $this->holding->id);
            })
            ->exists();
    }

    /**
     * @return array
     */
    private function getProjectAccount()
    {
        $accounts = array();

        $accounts[5] = 81;

        return $accounts;
    }
}
