<?php
/**
 * Date: 08/06/2016
 * Time: 4:31 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Sale;

use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\RealEstateSalesAgreement;
use Cytonn\Core\Storage\StorageInterface;
use Webpatser\Uuid\Uuid;

class SalesAgreement
{
    protected $salesAgreement;

    /**
     * SalesAgreement constructor.
     *
     * @param $salesAgreement
     */
    public function __construct(RealEstateSalesAgreement $salesAgreement = null)
    {
        is_null($salesAgreement) ?
            $this->salesAgreement = new RealEstateSalesAgreement()
            : $this->salesAgreement = $salesAgreement;
    }


    /**
     * @param $file_contents
     * @param $file_ext
     * @param $title
     * @return Document
     * @throws \Exception
     */
    public function upload($file_contents, $file_ext, $title = null)
    {
        $filename = Uuid::generate()->string . '.' . $file_ext;

        $url = $this->salesAgreement->getLocation() . '/' . $filename;

        //save the file in the filesystem
        \App::make(StorageInterface::class)->put($url, $file_contents);

        //save document to db - describing the file
        $document = new Document();
        $document->type_id = DocumentType::where('slug', 'sales_agreement')->first()->id;
        $document->filename = $filename;
        $document->title = $title;
        $document->save();

        return $document;
    }
}
