<?php
/**
 * Date: 07/06/2016
 * Time: 12:58 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Realestate\Sale;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Core\Storage\StorageInterface;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Reporting\LooGenerator;
use Webpatser\Uuid\Uuid;

/**
 * Class LetterOfOffer
 *
 * @package Cytonn\Realestate\Sale
 */
class LetterOfOffer
{
    /**
     * @var null|RealestateLetterOfOffer
     */
    protected $letterOfOffer;


    /**
     * @param RealestateLetterOfOffer|null $loo
     */
    public function __construct(RealestateLetterOfOffer $loo = null)
    {
        is_null($this->letterOfOffer = $loo) ?
            $this->letterOfOffer = new RealestateLetterOfOffer() :
            $this->letterOfOffer = $loo;
    }

    /**
     * Schedule a LOO after reservation payment
     *
     * @param  RealEstatePayment $reservationFee
     * @return bool|RealestateLetterOfOffer
     */
    public function schedule(RealEstatePayment $reservationFee)
    {
        $holding = $reservationFee->holding;
        $loo_date = $reservationFee->date->addDays($this->getLooDueDate());

        if (isset($holding->loo->document_id)) {
            return null;
        }

        return $this->createLoo($holding, $loo_date);
    }

    /**
     * Create the LOO
     *
     * @param  $holding
     * @param  $date
     * @return RealestateLetterOfOffer
     */
    public function createLoo(UnitHolding $holding, $date = null)
    {
        $loo = $holding->letterOfOffer;

        if (is_null($loo)) {
            $loo = new RealestateLetterOfOffer();
            $loo->holding_id = $holding->id;
        }

        $this->letterOfOffer = $loo;

        if (is_null($date) && is_null($loo->due_date)) {
            throw new ClientInvestmentException('Please add a reservation so that the LOO due date is set');
        }

        if (!$loo->advocate_id) {
            $loo->advocate_id = $this->getAdvocateToAssign($holding);
            $loo->save();
        }

        if ($this->validatePaymentSchedule($holding) && $holding->loo) {
            $loo->document_id = $this->createDocument($holding)->id;
        } else {
            \Flash::warning('The Loo was not generated, since the payment schedules do not match the price');
        }

        if (!is_null($date)) {
            $loo->due_date = $date;
        }

        $loo->save();

        return $loo;
    }

    /**
     * @param RealestateLetterOfOffer $loo
     * @return RealestateLetterOfOffer
     */
    public function updateLooDocument(RealestateLetterOfOffer $loo)
    {
        //        if($loo->sent_by)
        //        {
        //            throw new ClientInvestmentException('The LOO has already been sent, you cannot regenerate');
        //        }

        $file = (new LooGenerator())->generate($loo->holding)->output();

        $loo->update(['document_id' => $this->uploadLoo($file, 'pdf', $loo)->id]);
        return $loo;
    }

    /**
     * Generate LOO document and add a record of it to Documents table
     *
     * @param  UnitHolding $unitHolding
     * @return Document
     */
    protected function createDocument(UnitHolding $unitHolding)
    {
        $file = (new LooGenerator())->generate($unitHolding)->output();

        return $this->uploadLoo($file, 'pdf');
    }

    /**
     * Dynamically determine the advocate with the least work log and return his/her id
     *
     * @param    UnitHolding $holding
     * @return   mixed
     * @internal param Client $client
     */
    protected function getAdvocateToAssign(UnitHolding $holding)
    {
        $client = $holding->client;

        if (!$client) {
            return $this->getRandomAdvocate($holding);
        }

        $loo = RealestateLetterOfOffer::whereNotNull('advocate_id')->whereHas(
            'holding',
            function ($holding) use ($client) {
                $holding->where('client_id', $client->id);
            }
        )->wherehas(
            'advocate',
            function ($advocate) {
                $advocate->where('active', 1);
            }
        )->latest()->first();

        if (!$loo) {
            return $this->getRandomAdvocate($holding);
        }

        if (!$loo->advocate_id) {
            return $this->getRandomAdvocate($holding);
        }

        return $loo->advocate_id;
    }

    /**
     * @param UnitHolding $holding
     * @return mixed
     */
    protected function getRandomAdvocate(UnitHolding $holding)
    {
        return last(
            Advocate::where('active', 1)
                ->whereHas(
                    'projectAssignments',
                    function ($assignment) use ($holding) {
                        return $assignment->where('project_id', $holding->project->id);
                    }
                )
                ->get()->map(
                    function ($advocate) {
                        return [
                            'id' => $advocate->id,
                            'log' => $advocate->loos()->where('created_at', '>=', Carbon::now()->subMonth())->count()
                        ];
                    }
                )->sortByDesc('log')->toArray()
        )['id'];
    }


    /**
     * Get the the date between when reservation is made until LOO is issued
     *
     * @return int
     */
    protected function getLooDueDate()
    {
        return 10;
    }

    /**
     * @param UnitHolding $holding
     * @param string $format
     * @return mixed
     */
    public function generateLoo(UnitHolding $holding, $format = 'pdf')
    {
        $loo = $holding->loo;

        if (!$loo->advocate_id) {
            $loo->advocate_id = $this->getAdvocateToAssign($holding);
            $loo->save();
        }

        return (new LooGenerator())->generate($holding, $format);
    }

    /**
     * @param $file_contents
     * @param $file_ext
     * @param RealestateLetterOfOffer $loo
     * @return Document
     * @throws \Exception
     */
    public function uploadLoo($file_contents, $file_ext, RealestateLetterOfOffer $loo = null)
    {
        if ($loo) {
            $filename = $loo->document->filename;
            $document = Document::findOrFail($loo->document->id);
        } else {
            $filename = Uuid::generate()->string . '.' . $file_ext;
            $document = new Document();
        }

        $url = $this->letterOfOffer->getLocation() . '/' . $filename;

        //save the file in the filesystem
        \App::make(StorageInterface::class)->filesystem()->put($url, $file_contents);

        // save/update document to db - describing the file
        $document->type_id = DocumentType::where('slug', 'loo')->first()->id;
        $document->filename = $filename;
        $document->save();

        return $document;
    }

    /**
     * @param UnitHolding $holding
     * @return bool
     */
    protected function validatePaymentSchedule(UnitHolding $holding)
    {
        $price = round($holding->price());

        $schedules = round($holding->paymentSchedules()->inPricing()->sum('amount'));

        return ($price != 0) && ($price == $schedules);
    }

    /**
     * @param UnitHolding $holding
     * @return mixed
     */
    public function generateLooForm(UnitHolding $holding)
    {
        $client = $holding->client;

        return \PDF::loadView(
            'realestate.reports.loo_authorization_form',
            [
                'client' => $client, 'sender' => \Auth::user(),
                'holding' => $holding, 'logo' => $holding->project->fundManager->re_logo]
        );
    }

    public function generateContractVariation(UnitHolding $holding)
    {
        $client = $holding->client;

        return \PDF::loadView('realestate.reports.contract_variation', [
            'client' => $client, 'sender' => \Auth::user(),
            'holding' => $holding, 'logo' => $holding->project->fundManager->re_logo
        ]);
    }
}
