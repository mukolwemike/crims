<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Realestate\RealestateUnitSizes;

use Laracasts\Validation\FormValidator;

class RealestateUnitSizeForm extends FormValidator
{
    public $rules = [
        'name'=>'required',
    ];
}
