<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Realestate\RealestateUnitSizes;

use App\Cytonn\Models\RealestateUnitSize;

class RealestateUnitSizeRepository
{
    /*
     * Get a unit size by its id
     */
    public function getRealestateUnitSizeById($id)
    {
        return RealestateUnitSize::findOrFail($id);
    }

    /*
     * Store or update real estate unit size record
     */
    public function store($input, $id)
    {
        if ($id) {
            $unitSize = $this->getRealestateUnitSizeById($id);

            $unitSize->update($input);

            return $unitSize;
        }

        return RealestateUnitSize::create($input);
    }

    /*
     * Get unit sizes with their complete names
     */
    public function getUnitSizesNames()
    {
        $unitSizesArray = array();

        foreach (RealestateUnitSize::all() as $unitSize) {
            $unitSizesArray[$unitSize->id] = $unitSize->present()->getName;
        }

        return $unitSizesArray;
    }
}
