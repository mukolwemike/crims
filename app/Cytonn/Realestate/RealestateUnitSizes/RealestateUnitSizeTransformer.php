<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Realestate\RealestateUnitSizes;

use App\Cytonn\Models\RealestateUnitSize;
use League\Fractal\TransformerAbstract;

class RealestateUnitSizeTransformer extends TransformerAbstract
{
    /**
     * @param RealestateUnitSize $unitSize
     * @return array
     */
    public function transform(RealestateUnitSize $unitSize)
    {
        return [
            'id' => $unitSize->id,
            'completename' => $unitSize->present()->getName,
            'name' => $unitSize->name,
            'realestate_land_size_id' => $unitSize->realestate_land_size_id
        ];
    }
}
