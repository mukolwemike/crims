<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Realestate\RealestateLandSizes;

use App\Cytonn\Models\RealestateLandSize;

class RealestateLandSizeRepository
{
    /*
     * Get a real estate landsize by id
     */
    public function getRealestateLandSizeById($id)
    {
        return RealestateLandSize::findOrFail($id);
    }

    /*
     * Get a real estate land size name
     */
    public function getRealestateLandSizeName($id)
    {
        $landSize = RealestateLandSize::find($id);

        if ($landSize) {
            return $landSize->name;
        }

        return '';
    }
}
