<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Uuids;

use Webpatser\Uuid\Uuid;

trait UuidTrait
{
    /**
     * Boot function from laravel.
     */
    protected static function bootUuidTrait()
    {
        static::creating(
            function ($model) {
                $model->uuid = Uuid::generate()->string;
            }
        );

        static::updating(
            function ($model) {
                if (is_null($model->uuid)) {
                    $model->uuid = Uuid::generate()->string;
                }
            }
        );
    }
}
