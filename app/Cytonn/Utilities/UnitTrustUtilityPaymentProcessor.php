<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Utilities;

use App\Cytonn\Custodial\PaymentIntegration\CytonnPaymentSystem;
use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\Billing\UtilityBillingStatus;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Events\Utilities\UtilityPaymentHasFailed;
use App\Exceptions\CrimsException;
use App\Jobs\USSD\SendMessages;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Sell;
use Exception;

class UnitTrustUtilityPaymentProcessor
{
    protected $utilitiesFund;

    protected $data;

    protected $instruction;

    protected $utilityPayment;

    protected $disbursementAccount;

    /**
     * UnitTrustUtilityPaymentProcessor constructor.
     * @param UtilityBillingInstructions $instruction
     * @param $data
     */
    public function __construct(UtilityBillingInstructions $instruction, $data)
    {
        $this->instruction = $instruction;

        $this->data = $data;
    }

    /**
     * @throws CrimsException
     */
    public function process()
    {
        if ($this->data->status == "success") {
            $this->processSuccessfulTransaction();
        } else {
            $this->processFailedTransaction();
        }
    }

    /*
     * PROCESSING SUCCESSFUL TRANSACTION
     */
    /**
     * @throws CrimsException
     */
    private function processSuccessfulTransaction()
    {
        /*
         * TRANSFER OPTION
         *
         * 1. Update the utility instruction
         * 2. Send relevant notification
         *
         *
         * WITHDRAW OPTION
         *
         * 1. Generate Bank Instruction
         * 2. Submit Instruction to Payment System
         * 3. Update Inflow with instruction outgoing reference
         * 4. Update the utility instruction
         * 5. Send relevant notification
         *
         */
//        $this->processInWithdrawOption();

        $this->updateUtilityBillingInstruction();

        $this->sendNotification();
    }

    private function updateUtilityBillingInstruction()
    {
        $this->instruction->update([
            'billing_status_id' => UtilityBillingStatus::where('slug', 'completed')->first()->id,
            'response_payload' => \GuzzleHttp\json_encode($this->data)
        ]);
    }

    private function sendNotification()
    {
        $this->sendEmailNotification();

        if ($this->instruction->utility->notify == 0) {
            return;
        }

        $phone = $this->instruction->msisdn;

        $data = (array)$this->data;

        if ($phone) {
            dispatch(new SendMessages($phone, 'utility-payment-successful', $data));
        }
    }

    private function sendEmailNotification()
    {
        $client = $this->instruction->client;

        $unitFund = $this->instruction->unitFund;

        $toEmails = $client->getContactEmailsArray();

        if (count($toEmails) == 0) {
            return;
        }

        $sender = config('system.emails.operations_group');

        $ccEmails = [$sender];

        Mailer::compose()
            ->from([$sender => ucfirst($unitFund->name)])
            ->subject("Bill Payment Request Processed Successfully - " . ClientPresenter::presentJointFirstNames($client->id))
            ->to($toEmails)
            ->cc($ccEmails)
            ->bcc(config('system.administrators'))
            ->view('emails.utilities.payment_confirmation', [
                'client' => $client,
                'data' => $this->data,
                'instruction' => $this->instruction,
                'sourceAccount' => $unitFund->name,
                'currency' => $unitFund->currency->code,
                'payment' => $this->instruction->utilityPayment
            ])
            ->queue();
    }

    /*
     * WITHDRAWAL OPTION
     */
    /**
     * @throws CrimsException
     */
    private function processInWithdrawOption()
    {
        $bankInstruction = $this->generateBankInstruction();

        $this->submitWithdrawalPayment($bankInstruction, $this->instruction);

        $this->updateUtilitiesInflow($bankInstruction, $this->instruction->sale);
    }

    /**
     * @return BankInstruction
     */
    private function generateBankInstruction()
    {
        $account = $this->getUtilityWithdrawalClientAccount($this->instruction->client);

        $bankInstruction =  (new Sell($this->instruction->unitFund, $this->instruction->client, Carbon::today()))
            ->generateInstruction($this->instruction->sale, $account, $this->getPayingAccount($this->instruction))
            ->getInstruction();

        $bankInstruction->update(['identifier' => 'utility_billing_instruction_id_' . $this->instruction->id]);

        return $bankInstruction;
    }

    /**
     * @param Client $client
     * @return ClientBankAccount|\Illuminate\Database\Eloquent\Model|null
     */
    private function getUtilityWithdrawalClientAccount(Client $client)
    {
        $bankDetails = $this->getWithdrawalBankAccountDetails();

        $bankAccount = $this->fetchClientBankAccount($client, $bankDetails['investor_account_number']);

        if ($bankAccount) {
            return $bankAccount;
        }

        $bankDetails = array_merge($bankDetails, [
            'active' => '0',
            'client_id' => $client->id
        ]);

        ClientTransactionApproval::add([
            'client_id' => $client->id,
            'payload' => $bankDetails,
            'scheduled' => false,
            'transaction_type' => 'add_bank_details'
        ]);

        return $this->fetchClientBankAccount($client, $bankDetails['investor_account_number']);
    }

    /**
     * @return array
     */
    protected function getWithdrawalBankAccountDetails()
    {
        return [
            'investor_account_name' => 'Billing Collection Account',
            'investor_account_number' => '0105087624404',
            'currency' => '1',
            'bank_id' => '21',
            'branch_id' => '786',
        ];
    }

    /**
     * @param Client $client
     * @param $accountNumber
     * @return ClientBankAccount|\Illuminate\Database\Eloquent\Model|null
     */
    private function fetchClientBankAccount(Client $client, $accountNumber)
    {
        return ClientBankAccount::where('client_id', $client->id)
            ->where('account_number', $accountNumber)
            ->first();
    }

    /**
     * @param UtilityBillingInstructions $instruction
     * @return |null
     */
    private function getPayingAccount(UtilityBillingInstructions $instruction)
    {
        if ($this->disbursementAccount) {
            return $this->disbursementAccount;
        }

        return $this->disbursementAccount = $instruction->unitFund->repo->getDisbursementAccount();
    }

    /**
     * @param BankInstruction $instruction
     * @param UtilityBillingInstructions $billingInstruction
     * @throws CrimsException|Exception
     */
    private function submitWithdrawalPayment(
        BankInstruction $instruction,
        UtilityBillingInstructions $billingInstruction
    ) {
        $response = $this->submit($instruction, $this->getPayingAccount($billingInstruction));

        if (count($response->failed) > 0) {
            $ex = "Payment submission failed... || " . @json_encode($response->failed);

            logRollbar($ex);

            throw new CrimsException($ex);
        }
    }

    /**
     * @param BankInstruction $instruction
     * @param CustodialAccount|null $account
     * @return mixed
     * @throws Exception
     */
    protected function submit(BankInstruction $instruction, CustodialAccount $account = null)
    {
        $trans = $instruction->custodialTransaction;

        $account = $account ? $account : $trans->custodialAccount;

        //where separate account is defined for automated transactions, use that account
        if ($account->id !== $trans->custodial_account_id) {
            $trans->update(['custodial_account_id' => $account->id]);
        }

        $integration = new CytonnPaymentSystem();

        return $integration->submit(collect([$instruction]), $account);
    }

    /**
     * @param BankInstruction $bankInstruction
     * @param UnitFundSale $unitSale
     */
    private function updateUtilitiesInflow(BankInstruction $bankInstruction, UnitFundSale $unitSale)
    {
        $transaction = $this->getUtilityInflowTransaction($unitSale);

        $transaction->update([
            'outgoing_reference' => $bankInstruction->custodialTransaction->outgoing_reference
        ]);

        return $transaction;
    }

    /**
     * @param UnitFundSale $unitSale
     * @return CustodialTransaction|\Illuminate\Database\Eloquent\Model|null
     */
    private function getUtilityInflowTransaction(UnitFundSale $unitSale)
    {
        $reference = $unitSale->client->client_code . '-' . $unitSale->payment->id;

        return CustodialTransaction::where('outgoing_reference', $reference)->first();
    }

    /*
     * PROCESSING FAILED TRANSACTION
     */
    /**
     * @throws CrimsException
     */
    private function processFailedTransaction()
    {
        /*
         * TRANSFER OPTION
         * 1. Reverse utility payment
         * 2. Transfer Cash from Utilities
         * 3. Reverse unit fund sale
         * 4. Update the utility instruction
         * 5. Send relevant notification
         * 6. Report Failure
         *
         * WITHDRAW OPTION
         * 1. Reverse utility payment
         * 2. Reverse utility inflow
         * 3. Reverse unit fund sale
         * 4. Update the utility instruction
         * 5. Send relevant notification
         * 6. Report Failure
         */
        $this->reverseUtilityPayment();

        $this->reverseInTransferOption();

//        $this->reverseInWithdrawOption();

        $approval = $this->reverseUnitFundSale();

        $this->updateFailedInstruction($approval);

        event(new UtilityPaymentHasFailed($this->instruction));

        $this->reportFailure();
    }

    private function reverseUtilityPayment()
    {
        $this->utilityPayment = $this->instruction->utilityPayment;

        $this->instruction->utilityPayment->delete();
    }

    /**
     * @throws CrimsException
     */
    private function reverseInTransferOption()
    {
        $this->transferCashFromUtilities();
    }

    /**
     * @throws CrimsException
     */
    private function reverseInWithdrawOption()
    {
        $this->reverseUtilityInflow();
    }

    /**
     * @throws CrimsException
     */
    private function transferCashFromUtilities()
    {
        $payment = $this->utilityPayment;

        $fa = $payment->client ? $payment->client->getLatestFA('units') : null;

        $utilitiesFund = $this->getUtilitesFund();

        $approvalData = [
            'previous_fund_id' => $utilitiesFund->id,
            'new_fund_id' => $this->instruction->unit_fund_id,
            'amount' => abs($payment->amount),
            'date' => $payment->date,
            'exchange_rate' => 1,
            'custodial_account_id' => $this->instruction->unitFund->getDisbursementAccount('auto')->id,
            'sending_client_id' => $payment->client_id,
            'receiving_client_id' => $payment->client_id,
            'commission_recipient_id' => $fa ? $fa->id : null
        ];

        $approval = ClientTransactionApproval::make(
            $payment->client_id,
            'transfer_cash_to_another_client',
            $approvalData
        );

        $approval->systemExecute();
    }

    /**
     * @throws CrimsException
     */
    private function reverseUtilityInflow()
    {
        $transaction = $this->getUtilityInflowTransaction($this->instruction->sale);

        $input = [
            'reason' => 'Reversal due to failed utility payment transaction',
            'transaction_id' => $transaction->id,
        ];

        $approval = ClientTransactionApproval::make(
            $this->instruction->client_id,
            'reverse_cash_inflow',
            $input
        );

        $approval->systemExecute();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    protected function getUtilitesFund()
    {
        if ($this->utilitiesFund) {
            return $this->utilitiesFund;
        }

        return UnitFund::where('short_name', '=', 'CUF')->first();
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws CrimsException
     */
    private function reverseUnitFundSale()
    {
        $approvalData = [
            'reason' => "Reversal due to failed utility payment for instruction " . $this->instruction->id,
            'sale_id' => $this->instruction->unit_fund_sale_id,
            'fundId' => $this->instruction->sale->unit_fund_id,
            'unit_fund_id' => $this->instruction->sale->unit_fund_id,
            'clientId' => $this->instruction->client_id,
            'client_id' => $this->instruction->client_id,
            'type' => 'unit_fund_sale'
        ];

        $approval = ClientTransactionApproval::make(
            $this->instruction->client_id,
            'unit_fund_reverse_transaction',
            $approvalData
        );

        $approval->systemExecute();

        return $approval;
    }

    /**
     * @param ClientTransactionApproval $approval
     */
    private function updateFailedInstruction(ClientTransactionApproval $approval)
    {
        $this->instruction->update([
            'reversal_approval_id' => $approval->id,
            'response_payload' => \GuzzleHttp\json_encode($this->data),
            'billing_status_id' => UtilityBillingStatus::where('slug', 'cancelled')->first()->id
        ]);
    }

    private function reportFailure()
    {
        Mailer::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : Reversed Automatic Utility Payment Error')
            ->text("<p>Data: <br>
                    <pre>" . \GuzzleHttp\json_encode($this->data) . " </pre>
                </p>")
            ->queue();
    }
}
