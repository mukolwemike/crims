<?php

namespace Cytonn\Unitization;

use App\Cytonn\Models\Unitization\UnitFundApplication;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class UnitFundHolderRepository
{
    protected $holder;

    /**
     * UnitFundHolderRepository constructor.
     *
     * @param    UnitFundHolder|null $holder
     * @internal param $holder
     */
    public function __construct(UnitFundHolder $holder = null)
    {
        $this->holder = is_null($holder) ? new UnitFundHolder() : $holder;
    }

    public function getOwnedUnitsAsAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);
        return $this->holder->ownedNumberOfUnits($date->copy());
    }

    public function getFeesIncurredAsAt(Carbon $date)
    {
        $date = Carbon::parse($date);

        return $this->holder->feesIncurred()
                ->where('date', '<=', $date->toDateString())
                ->sum('amount');
    }

    public function save(UnitFundApplication $application)
    {
        $client = $application->client;
        $fund = $application->fund;
        
        $holder = new UnitFundHolder(
            [
                'number'    =>  $this->suggestHolderNumber()
            ]
        );
        
        $holder->client()->associate($client);
        $holder->fund()->associate($fund);

        $holder->save();
    }

    public function suggestHolderNumber()
    {
        $codes = Collection::make(
            UnitFundHolder::withTrashed()
            ->pluck('number')
        )
            ->map(
                function ($code) {
                    return intval($code);
                }
            )->toArray();
        
        if (count($codes) == 0) {
            return 1000;
        }
        
        if (is_array($codes) && count($codes) > 1) {
            return 1 + max($codes);
        } elseif ($codes instanceof Collection) {
            return 1 + max($codes->all());
        }
        
        return 1 + max($codes);
    }

    public function getValueOfOwnedUnitsAsAt(Carbon $date)
    {
        return (double) $this->getOwnedUnitsAsAt($date) * $this->holder->fund->getPriceTrail($date)->price;
    }
}
