<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/10/17
 * Time: 9:11 AM
 */

namespace Cytonn\Unitization;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\ContactMethod;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\Unitization\UnitFund;

class ApplicationFormPartials
{
    /**
     * @return array
     */
    public function formPartials()
    {
        return [
            'titles' => Title::all(),
            'gender' => Gender::all(),
            'countries' => Country::all(),
            'unitFunds' => UnitFund::all(),
            'clientType' => ClientType::all(),
            'products' => Product::active()->get(),
            'employment_types' => Employment::all(),
            'sources_of_funds' => FundSource::all(),
            'contact_methods' => ContactMethod::all(),
            'investor_types' => CorporateInvestorType::all(),
            'banks' => ClientBank::orderBy('name', 'asc')->get(),
            'unitFunds' => $this->unitFunds()
        ];
    }
    
    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function unitFunds()
    {
        return UnitFund::all()->map(function ($fund) {
            return [
                    'id' => $fund->id,
                    'name' => $fund->name
                ];
        });
    }
}
