<?php

namespace Cytonn\Unitization;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Unitization\UnitFundCommission;
use App\Cytonn\Models\Unitization\UnitFundCommissionRate;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;

class UnitFundCommissionRepository
{
    private $purchase;

    private $recipient;

    private $unitPrice;

    private $rate;

    private $commission;

    private $amount;

    public function __construct(UnitFundPurchase $purchase, $unitPrice, CommissionRecepient $recipient, $amount = null)
    {
        $this->purchase = $purchase;
        $this->recipient = $recipient;
        $this->unitPrice = $unitPrice;
        $this->amount = $amount ? $amount : (float) $purchase->number * $purchase->price;

        $rate = $this->determineRateForRecipient($this->recipient, $this->purchase->date);
        $this->setRate($rate);

        $commission = $this->calculateCommission($rate->rate);
        $this->setCommission($commission);
    }

    /**
     * @return UnitFundCommission|\Illuminate\Database\Eloquent\Model
     * @throws ClientInvestmentException
     */
    public function saveCommission()
    {
        $rate = $this->calculateRate();

        $description = "Purchase of {$this->purchase->unitFund->name}'s units : {$this->purchase->number} @" .
            " {$this->unitPrice}";

        $this->commission = UnitFundCommission::create([
            'commission_recipient_id' => $this->recipient->id,
            'unit_fund_purchase_id' => $this->purchase->id,
            'date' => $this->purchase->date,
            'rate' => $rate,
            'description' => $description
        ]);

        return $this->commission;
    }

    /**
     * @return int
     * @throws ClientInvestmentException
     */
    private function calculateRate()
    {
        if ($this->recipient->zero_commission) {
            return $this->rate = 0;
        }

        $rate = $this->getRate();

        if (is_null($rate)) {
            throw new ClientInvestmentException("Missing commission rate for recipient type " .
                $this->recipient->type->name . " unit fund " . $this->purchase->unitFund->short_name .
                " for date " . $this->purchase->date->toDateString());
        }

        return $this->rate = $rate->rate;
    }

    /**
     * @throws ClientInvestmentException
     */
    public function updateCommission()
    {
        $commission = $this->purchase->unitFundCommission;

        $commission->update([
            'commission_recipient_id' => $this->recipient->id,
            'rate' => $this->calculateRate()
        ]);
    }

    private function calculateCommission($rate)
    {
        return (double)abs($this->purchase->number * $this->unitPrice * $rate / 100);
    }

    private function determineRateForRecipient(CommissionRecepient $recepient, Carbon $date)
    {
        if ($recepient->zero_commission == 1) {
            return new UnitFundCommissionRate();
        }

        $portfolio = $recepient->repo->getUnitFundPortfolio($this->purchase->unitFund, $date);

        $rate = UnitFundCommissionRate::where('commission_recipient_type_id', $recepient->recipient_type_id)
            ->where('unit_fund_id', $this->purchase->unit_fund_id)
            ->where('min_amount', '<=', $portfolio)
            ->where('date', '<=', $date)
            ->orderBy('date', 'DESC')
            ->orderBy('min_amount', 'DESC')
            ->first();

        return is_null($rate) ? new UnitFundCommissionRate() : $rate;
    }

    public function getRate()
    {
        return $this->rate;
    }

    private function setRate(UnitFundCommissionRate $rate)
    {
        $this->rate = $rate;
    }

    public function getCommission()
    {
        return $this->commission;
    }

    private function setCommission($commission)
    {
        $this->commission = $commission;
    }
}
