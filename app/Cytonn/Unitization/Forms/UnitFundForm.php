<?php

namespace Cytonn\Unitization\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

/**
 * Class UnitFundForm
 *
 * @package Cytonn\Unitization\Forms
 */
class UnitFundForm extends FormValidator
{
    public $rules = [
        'name'                      =>  'required|string|min:3',
        'initial_unit_price'        =>  'required|numeric',
        'minimum_investment_amount' =>  'required|numeric',
        'fund_objectives'           =>  'required|string',
        'benefits'                  =>  'required|string',
        'minimum_investment_horizon'=>  'required|numeric',
    ];
}
