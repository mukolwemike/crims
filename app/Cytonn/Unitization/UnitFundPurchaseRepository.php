<?php

namespace Cytonn\Unitization;

use App\Cytonn\Models\Unitization\UnitFundBusinessConfirmation;
use App\Cytonn\Models\Unitization\UnitFundPurchase;

class UnitFundPurchaseRepository
{
    protected $purchase;

    public function __construct(UnitFundPurchase $purchase)
    {
        $this->purchase = $purchase;
    }

    public function checkIfConfirmationHasBeenSent()
    {
        return (bool)UnitFundBusinessConfirmation::where('unit_fund_purchase_id', $this->purchase->id)
                ->count() > 0;
    }
}
