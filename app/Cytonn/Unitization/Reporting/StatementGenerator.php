<?php

namespace Cytonn\Unitization\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\StatementLog;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCampaignStatementItem;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Mailers\Unitization\StatementMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class StatementGenerator
{
    protected $protect = true;

    /**
     * @param Client $client
     * @param UnitFund $fund
     * @param $date
     * @param User|null $sender
     * @return \PDF
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function generate(
        Client $client,
        UnitFund $fund,
        $date,
        User $sender = null,
        $start_date = null,
        $campaign = null
    ) {
        $start = $start_date ? Carbon::parse($start_date) : null;

        $processor = new StatementProcessor($fund, $client, Carbon::parse($date), $start);

        $processed = $processor->process();

        $actions = $processed->filteredActions();

        $email_sender = $sender ? UserPresenter::presentLetterClosing($sender->id) : null;

        $opening = $processed->getPrepared()->opening;

        $start_date = $opening->date;

        $pdf = \PDF::loadView('unitization.reports.statement', [
            'email_sender' => $email_sender,
            'date' => $date,
            'client' => $client,
            'fund' => $fund,
            'unitFund' => $fund,
            'logo' => $fund->logo ? $fund->logo : $fund->fundManager->logo,
            'actions' => $actions,
            'opening_balance' => $processed->getPrepared()->opening_balance,
            'start_date' => $start_date,
            'calculation' => $fund->category->calculation->slug,
            'price' => $processed->getPrepared()->price,
            'closing_balance' => $processed->getPrepared()->closing_balance,
            'opening' => $opening,
            'companyAddress' => str_contains($fund->fundManager->fullname, 'Cytonn Asset Manager')
                ? 'asset_manager' : null,
            'view' => false,
            'user' => $sender,
            'signature' => $fund->fundManager->signature,
            'emailSender' => $fund->fundManager->fullname,
            'campaign' => $campaign
        ]);

        if ($fund->category->slug == 'pension-fund') {
            $pdf->setPaper('a4', 'landscape');
        }

        if ($this->protect) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    /**
     * @param Client $client
     * @param UnitFund $fund
     * @param $date
     * @param User|null $sender
     * @param null $start_date
     * @return \PDF
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function view(Client $client, UnitFund $fund, $date, User $sender = null, $start_date = null)
    {
        $start = $start_date ? Carbon::parse($start_date) : null;

        $processor = new StatementProcessor($fund, $client, $date, $start);

        $processed = $processor->process();

        $actions = $processed->filteredActions();

        $opening = $processed->getPrepared()->opening;

        $start_date = $opening->date;

        return \PDF::loadView('unitization.reports.statement', [
            'date' => $date,
            'fund' => $fund,
            'actions' => $actions,
            'opening_balance' => $processed->getPrepared()->opening_balance,
            'start_date' => $start_date,
            'calculation' => $fund->category->calculation->slug,
            'price' => $processed->getPrepared()->price,
            'closing_balance' => $processed->getPrepared()->closing_balance,
            'opening' => $opening,
            'view' => true
        ]);
    }

    /**
     * @param Client $client
     * @param UnitFund $fund
     * @param $date
     * @param User|null $sender
     * @param null $start_date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function html(Client $client, UnitFund $fund, $date, User $sender = null, $start_date = null)
    {
        $start = $start_date ? Carbon::parse($start_date) : null;

        $processor = new StatementProcessor($fund, $client, $date, $start);

        $processed = $processor->process();

        $actions = $processed->filteredActions();

        $opening = $processed->getPrepared()->opening;

        $start_date = $opening->date;

        return view('unitization.reports.view', [
            'date' => $date,
            'fund' => $fund,
            'actions' => $actions,
            'opening_balance' => $processed->getPrepared()->opening_balance,
            'start_date' => $start_date,
            'calculation' => $fund->category->calculation->slug,
            'price' => $processed->getPrepared()->price,
            'closing_balance' => $processed->getPrepared()->closing_balance,
            'opening' => $opening,
            'view' => true
        ]);
    }

    public function excel(Client $client, UnitFund $fund, $date, User $sender = null, $start_date = null)
    {
        $start = $start_date ? Carbon::parse($start_date) : null;

        $processor = new StatementProcessor($fund, $client, $date, $start);

        $processed = $processor->process();

        $actions = $processed->filteredActions();

        $opening = $processed->getPrepared()->opening;

        $start_date = $opening->date;

        return \Excel::create(
            'Statement',
            function ($excel) use ($processed, $client, $fund, $date, $actions, $start_date, $opening) {
                $excel->sheet(
                    'Statement',
                    function ($sheet) use ($processed, $client, $fund, $date, $actions, $start_date, $opening) {

                        $calculation = $fund->category->calculation->slug;

                        $sheet->loadView(
                            'unitization.reports.statements.'.$calculation.'-view',
                            [
                                'date' => $date,
                                'fund' => $fund,
                                'actions' => $actions,
                                'opening_balance' => $processed->getPrepared()->opening_balance,
                                'start_date' => $start_date,
                                'calculation' => $calculation,
                                'price' => $processed->getPrepared()->price,
                                'closing_balance' => $processed->getPrepared()->closing_balance,
                                'opening' => $opening,
                                'view' => true
                            ]
                        );
                    }
                );
            }
        )->download('xlsx');
    }

    /**
     * @param UnitFundCampaignStatementItem $item
     * @return bool|void
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function sendCampaignStatementItem(UnitFundCampaignStatementItem $item)
    {
        $campaign = $item->campaign;

        $client = Client::findOrFail($item->client_id);

        $fund = UnitFund::findOrFail($campaign->unit_fund_id);

        $sender = User::findOrFail($campaign->sender_id);

        if (is_null($client->getContactEmailsArray())) {
            return;
        }

        $statements = [];

        $statements['Unit Trust Statement - ' .
        ClientPresenter::presentJointFirstNameLastName($client->id) . '.pdf'] =
            $this->generate($client, $fund, $campaign->send_date, $sender, null, $campaign)->output();

        $mailer = new StatementMailer();

        $mailer->sendStatementToClient(
            $client,
            $statements,
            $campaign->send_date,
            $fund,
            $campaign->sender_id,
            $campaign
        );

        $log = new StatementLog();

        $log->fill(['client_id' => $item->client_id, 'sent_by' => $campaign->sender_id]);

        $log->save();

        $item->sent = true;

        $item->sent_on = $campaign->send_date;

        return $item->save();
    }

    public function passwordProtect($protect = true)
    {
        $this->protect = $protect;

        return $this;
    }
}
