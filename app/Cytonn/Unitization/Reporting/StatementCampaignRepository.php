<?php

namespace Cytonn\Unitization\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCampaignStatementItem;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use App\Cytonn\Models\User;
use Carbon\Carbon;

class StatementCampaignRepository
{
    public $campaign;

    protected $date;

    protected $fund;

    public function __construct(UnitFundStatementCampaign $campaign = null)
    {
        $this->campaign = $campaign ? $campaign : new UnitFundStatementCampaign();

        $this->date = Carbon::parse($this->campaign->send_date);

        $this->fund = $this->campaign->unitFund;
    }

    public function addClientToCampaign(Client $client, User $user)
    {
        $hasPurchases = $client->has('unitFundPurchases')
            ->get()
            ->filter(
                function ($client) {
                    return $client->repo->ownedNumberOfUnits($this->campaign->fund) > 0;
                }
            )
            ->count();

        if ($hasPurchases and !$this->checkIfHolderIsInCampaign($client)) {
            return (new UnitFundCampaignStatementItem())
                ->fill(
                    [
                        'client_id' => $client->id,
                        'unit_fund_statement_campaign_id' => $this->campaign->id,
                        'added_by' => $user->id,
                        'added_on' => Carbon::now()
                    ]
                )->save();
        }
        return false;
    }

    public function checkIfHolderIsInCampaign(Client $client)
    {
        $item = UnitFundCampaignStatementItem::where('unit_fund_statement_campaign_id', $this->campaign->id)
            ->where('client_id', $client->id)
            ->first();

        return !is_null($item);
    }

    public function sendCampaign(User $user = null)
    {
        $this->campaign->sent_by = $user ? $user->id : auth()->id();

        $this->campaign->save();

        UnitFundCampaignStatementItem::where('unit_fund_statement_campaign_id', $this->campaign->id)
            ->where('sent', null)->update(['ready' => true]);

//        \Queue::push(static::class . '@fireSendCampaign', [
//            'campaign_id' => $this->campaign->id
//        ], config('queue.priority.high'));
    }

    public function fireSendCampaign($job, $data)
    {
        UnitFundCampaignStatementItem::where('unit_fund_statement_campaign_id', $data['campaign_id'])
            ->where('sent', null)
            ->get()
            ->each(
                function (UnitFundCampaignStatementItem $item) {
                    $this->sendCampaignItem($item);
                }
            );

        $job->delete();
    }

    public function sendCampaignItem(UnitFundCampaignStatementItem $item)
    {
        $item->ready = true;

        $item->save();
    }

    public function countSentItems()
    {
        return $this->campaign->items()->where('sent', 1)->count();
    }

    public function countAllItems()
    {
        return $this->campaign->items()->count();
    }

    public function automaticallyAddClients($setReady = false)
    {
        $clients = Client::whereHas('unitFundPurchases', function ($purchase) {
            $purchase->where('unit_fund_id', $this->campaign->unit_fund_id);
        })->whereDoesntHave('unitFundCampaignItems', function ($q) {
            $q->where('unit_fund_statement_campaign_id', $this->campaign->id);
        })->get();

        foreach ($clients as $key => $client) {
            if (!$this->eligibleForAdditionToCampaign($client)) {
                continue;
            }

            (new UnitFundCampaignStatementItem())->fill([
                'client_id' => $client->id,
                'unit_fund_statement_campaign_id' => $this->campaign->id,
                'added_by' => $this->campaign->sender_id,
                'added_on' => Carbon::now(),
                'ready' => $setReady ? $setReady : null
            ])->save();
        }
    }

    /**
     * @param Client $client
     * @return bool
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function eligibleForAdditionToCampaign(Client $client)
    {
        $fund = $this->campaign->fund;

        $sales = $client->unitFundSales()->where('unit_fund_id', $fund->id);

        $sale = $sales->latest()->first();

        if (is_null($sale)) {
            return true;
        }

        if ($sale && $this->date->copy()->lte(Carbon::parse($sale->date)->copy()->addMonthNoOverflow())) {
            return true;
        }

        $purchaseAmount = $client->unitFundPurchases()->where('unit_fund_id', $fund->id)
            ->sum('number');

        if ($purchaseAmount >  $sales->sum('number')) {
            return true;
        }

        $bal = $client->repo->ownedNumberOfUnits($fund, $this->date);

        if ($bal >= 1) {
            return true;
        }

        return false;
    }

    public function fireAutomaticallyAddClients($job, $data)
    {
        $campaign = UnitFundStatementCampaign::findOrFail($data['campaign_id']);

        $this->campaign = $campaign;

        $this->date = Carbon::parse($campaign->send_date);

        $this->fund = $campaign->unitFund()->first();

        $this->automaticallyAddClients();

        $job->delete();
    }

    public function fundSummary()
    {
        return collect([
            [
                'name' => $this->campaign->fund->name,
                'total' => $this->campaign->items()->count(),
                'sent' => $this->campaign->items()->whereNotNull('sent_on')->count(),
                'failed' => $this->campaign->items()->whereNotNull('failed_on')->count()
            ]
        ]);
    }

    public function productSummary()
    {
        return $this->fundSummary();
    }
}
