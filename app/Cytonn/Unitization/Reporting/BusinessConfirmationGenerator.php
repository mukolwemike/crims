<?php

namespace Cytonn\Unitization\Reporting;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\User;
use Cytonn\Presenters\UserPresenter;

class BusinessConfirmationGenerator
{
    protected $protectDocument = false;

    public function generate(UnitFundPurchase $purchase, User $sender = null)
    {
        $client = $purchase->client;

        $fm = $purchase->unitFund->fundManager;

        $emailSender = $fm->fullname;
//        if ($sender) {
//            $emailSender = UserPresenter::presentLetterClosing($sender->id);
//        }

        $unitFund = $purchase->unitFund;

        $pdf = \PDF::loadView('unitization.reports.business_confirmation', [
            'emailSender'   =>  $emailSender,
            'client'        =>  $client,
            'unitFund'      =>  $unitFund,
            'purchase'      =>  $purchase,
            'logo'          =>  $unitFund->manager->logo,
            'companyAddress' => str_contains($fm->fullname, 'Cytonn Asset Manager') ? 'asset_manager' : null,
            'signature' => $fm->signature
        ]);


        if ($this->protectDocument) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    public function setProtectDocument(bool $protectDocument): BusinessConfirmationGenerator
    {
        $this->protectDocument = $protectDocument;
        return $this;
    }
}
