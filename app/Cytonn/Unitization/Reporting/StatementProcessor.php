<?php
/**
 * Date: 07/06/2018
 * Time: 09:56
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Unitization\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;

class StatementProcessor
{
    protected $fund;

    protected $client;

    protected $start;

    protected $date;

    /**
     * StatementProcessor constructor.
     * @param UnitFund $fund
     * @param Client $client
     * @param Carbon $date
     * @param Carbon $start
     */
    public function __construct(UnitFund $fund, Client $client, Carbon $date, Carbon $start = null)
    {
        $this->fund = $fund;
        $this->start = $start;
        $this->date = $date;
        $this->client = $client;

        if (is_null($start)) {
            $this->start = $this->getStartDate();
        }
    }

    /**
     * @return \Cytonn\Unitization\Calculate\MoneyMarketCalculator|\Cytonn\Unitization\Calculate\NavFundCalculator
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function process()
    {
        $calc = $this->client->calculateFund($this->fund, $this->date, $this->start);

        return $calc;
    }

    private function getStartDate()
    {
        $calc = $this->fund->category->calculation->slug;

        if ($calc === 'daily-yield') {
            return $this->getMMFStart();
        }

        if ($calc === 'income-distribution') {
            return $this->getPensionStart();
        }

        if ($this->date->copy()->isLastOfMonth()) {
            return $this->date->copy()->startOfMonth();
        }

        return $this->date->copy()->subMonthNoOverflow()->addDay();
    }

    private function getMMFStart()
    {
        $d = $this->date->copy()->subMonthsNoOverflow(12);

        if ($this->date->copy()->isLastOfMonth()) {
            return $d->copy()->startOfMonth();
        }

        return $this->date->copy()->subYearNoOverflow()->addDay();
    }

    private function getPensionStart()
    {
        return $this->date->copy()->startOfYear();
    }
}
