<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-12
 * Time: 13:01
 */

namespace Cytonn\Unitization\Reporting;

use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Unitization\Trading\Performance;

class RedemptionConfirmationGenerator
{
    protected $protectDocument = false;

    public function generate(UnitFundSale $sale, User $sender)
    {
        $client = $sale->client;

        $fm = $sale->fund->fundManager;

        $emailSender = UserPresenter::presentLetterClosing($sender->id);
        
        $pdf = \PDF::loadView('unitization.reports.redemption_confirmation', [
            'emailSender' => $emailSender,
            'client' => $client,
            'unitFund' => $sale->fund,
            'bank' => $client->bankAccounts[0],
            'sale' => $sale,
            'logo' => $sale->fund->manager->logo,
            'companyAddress' => str_contains($fm->name, 'Seriani') ? 'seriani' : null
        ]);


        if ($this->protectDocument) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }


    public function setProtectDocument(bool $protectDocument): RedemptionConfirmationGenerator
    {
        $this->protectDocument = $protectDocument;
        return $this;
    }
}
