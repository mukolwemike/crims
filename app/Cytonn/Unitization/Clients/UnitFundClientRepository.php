<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInterestSchedule;
use Carbon\Carbon;

class UnitFundClientRepository
{
    protected $client;

    /**
     * UnitFundClientRepository constructor.
     * @param Client|null $client
     */
    public function __construct(Client $client = null)
    {
        is_null($client) ? $this->client = new Client() : $this->client = $client;
    }

    /**
     * @param UnitFund $fund
     * @param Carbon $date
     * @return mixed
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function getUnitFundInterestAsAt(UnitFund $fund, Carbon $date, $as_at_next_day = false)
    {
        $calc = $this->client->calculateFund($fund, $date, null, $as_at_next_day);

        return $calc->getPrepared()->net_interest_after;
    }

    /**
     * @param UnitFund $unitFund
     * @return UnitFundInterestSchedule|\Illuminate\Database\Eloquent\Model|null
     */
    public function getInterestScheduleForFund(UnitFund $unitFund)
    {
        return UnitFundInterestSchedule::where('unit_fund_id', $unitFund->id)
            ->where('client_id', $this->client->id)->first();
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon|null $date
     * @return UnitFundInterestSchedule|\Illuminate\Database\Eloquent\Model|null
     */
    public function updateNextInterestPaymentDate(UnitFund $unitFund, Carbon $date = null)
    {
        $schedule = $this->getInterestScheduleForFund($unitFund);

        if (is_null($schedule)) {
            return null;
        }

        $date = $date ? $date : Carbon::parse($schedule->next_payment_date);

        $nextPaymentDate = $schedule->getNextPayment($date);

        $schedule->next_payment_date = $nextPaymentDate;

        $schedule->save();

        return $schedule;
    }
}
