<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/22/18
 * Time: 5:18 PM
 */

namespace App\Cytonn\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundPurchaseSale;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Exceptions\CRIMSGeneralException;

class UnitFundSaleRepository
{
    protected $fund;

    protected $sale;

    public function __construct(UnitFund $fund = null, UnitFundSale $sale)
    {
        $this->fund = is_null($fund) ? new UnitFund() : $fund;

        $this->sale = $sale;
    }

    public function linkToPurchase()
    {
        $units_sold = $this->sale->purchaseSales->sum('number');

        $units_to_sale = ((float)$this->sale->number) - $units_sold;

        $activePurchases = $this->sale->client->unitFundPurchases()->active()->count();

        $count = 0;

        while ($units_to_sale > 1 && $count <= $activePurchases) {
            $purchase = $this->oldestActiveClientPurchase($this->sale->fund, $this->sale->client);

            if (is_null($purchase)) {
                $calc = $this->sale->client->calculateFund(
                    $this->sale->fund,
                    $this->sale->date,
                    null,
                    true
                );

                if ($calc->totalPurchases() <= $calc->totalSales()) {
                    return;
                }

                throw new CRIMSGeneralException("The sold units exceeds available Sale : "
                    . $this->sale->id . ' Amount : ' . $units_to_sale);
            }

            $client = $purchase->client;

            $calc = $client->calculateFund(
                $purchase->unitFund,
                $this->sale->date,
                null,
                true,
                $purchase
            );

            $totalUnits = $calc->totalUnits() - $calc->getPrepared()->net_interest_after;

            $units_to_give = ((float)$totalUnits);

            $number = $units_to_give > $units_to_sale ? $units_to_sale : $units_to_give;

            $this->sale->unitFundPurchasesSales()->attach($purchase->id, [
                'number' => $number,
                'date' => $this->sale->date
            ]);

            $units_sold = $number;

            $units_to_sale = $units_to_sale - $units_sold;

            $this->updatePurchase($purchase, $this->sale);

            $count ++;
        }
    }

    public function oldestActiveClientPurchase(UnitFund $fund, Client $client)
    {
        return $fund->purchases()
            ->where('client_id', $client->id)
            ->oldest('date')
            ->whereNull('sold')
            ->first();
    }

    public function updatePurchase(UnitFundPurchase $purchase, UnitFundSale $sale)
    {
        $purchase = $purchase->fresh();

        if (abs($purchase->purchaseSales->sum('number') -(float)$purchase->number) > 1) {
            return $purchase;
        }

        $purchase->update([
            'sold' => true,
            'sale_date' => $sale->date
        ]);

        return $purchase;
    }

    public function getPurchaseValue()
    {
        $purchaseSales = $this->sale->purchaseSales;

        return $purchaseSales->sum(function (UnitFundPurchaseSale $purchaseSale) {
//            $purchaseAmount = $this->sale->client->calculateFund(
//                $purchaseSale->purchase->unitFund,
//                $this->sale->date,
//                null,
//                true,
//                $purchaseSale->purchase
//            )->totalPurchases();

            $purchaseNum = $purchaseSale->purchase ? $purchaseSale->purchase->number :  $purchaseSale->number;

            $number = min($purchaseSale->number, $purchaseNum);

            return $number * ($purchaseSale->purchase ? $purchaseSale->purchase->price : 0);
        });
    }

    public function getCommissionRecipient()
    {
        $purchase = UnitFundPurchase::whereHas('purchaseSales', function ($q) {
            $q->where('unit_fund_sale_id', $this->sale->id);
        })->first();

        return $purchase ? $purchase->getRecipient() : null;
    }

    public function getPurchaseValueForFa($recipientId = null)
    {
        if ($recipientId) {
            $purchaseSales = $this->sale->purchaseSales()->whereHas('purchase', function ($q) use ($recipientId) {
                $q->whereHas('unitFundCommission', function ($q) use ($recipientId) {
                    $q->where('commission_recipient_id', $recipientId);
                });
            })->get();
        } else {
            $purchaseSales = $this->sale->purchaseSales;
        }

        return $purchaseSales->sum(function (UnitFundPurchaseSale $purchaseSale) use ($recipientId) {
            $number = min($purchaseSale->number, $purchaseSale->purchase->number);

            return $number * ($purchaseSale->purchase ? $purchaseSale->purchase->price : 0);
        });
    }
}
