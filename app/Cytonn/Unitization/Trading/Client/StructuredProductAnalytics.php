<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/18/18
 * Time: 2:41 PM
 */

namespace App\Cytonn\Unitization\Trading\Client;

use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Investment\Summary\Analytics;

class StructuredProductAnalytics implements UnitFundPerformance
{
    protected $fund;

    protected $date;

    protected $analytics;

    /**
     * StructuredProductAnalytics constructor.
     * @param $fund
     * @param $date
     */
    public function __construct(UnitFund $fund, Carbon $date)
    {
        $this->fund = $fund;
        $this->date = $date;
    }


    public function aum()
    {
        return $this->prepare()->aum();
    }

    public function marketValue()
    {
        return $this->prepare()->marketValue();
    }

    public function costValue()
    {
        return $this->prepare()->costValue();
    }

    /**
     * @return Analytics
     */
    protected function prepare()
    {
        if ($this->analytics) {
            return $this->analytics;
        }

        $this->analytics = new Analytics($this->fund->fundManager, $this->date->copy());
        $this->analytics->setBaseCurrency($this->fund->currency);
        $this->analytics->setFund($this->fund);

        return $this->analytics;
    }
}
