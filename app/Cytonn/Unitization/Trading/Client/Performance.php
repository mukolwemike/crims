<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/18/18
 * Time: 3:25 PM
 */

namespace App\Cytonn\Unitization\Trading\Client;

use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;

class Performance implements UnitFundPerformance
{
    protected $fund;

    protected $date;

    /**
     * Performance constructor.
     * @param $fund
     * @param $date
     */
    public function __construct(UnitFund $fund, Carbon $date)
    {
        $this->fund = $fund;
        $this->date = $date;
    }


    protected function liabilities()
    {
        return collect([
            new Analytics($this->fund, $this->date),
            new StructuredProductAnalytics($this->fund, $this->date)
        ]);
    }


    public function aum()
    {
        return $this->liabilities()->sum(function (UnitFundPerformance $liab) {
            return $liab->aum();
        });
    }

    public function marketValue()
    {
        return $this->liabilities()->sum(function (UnitFundPerformance $liab) {
            return $liab->marketValue();
        });
    }

    public function costValue()
    {
        return $this->liabilities()->sum(function (UnitFundPerformance $liab) {
            return $liab->costValue();
        });
    }
}
