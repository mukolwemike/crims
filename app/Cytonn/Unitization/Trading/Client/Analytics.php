<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/18/18
 * Time: 1:12 PM
 */

namespace App\Cytonn\Unitization\Trading\Client;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Unitization\Trading\Trade;

class Analytics extends Trade implements UnitFundPerformance
{
    private $clients;

    /**
     * Analytics constructor.
     * @param $fund
     * @param $date
     */
    public function __construct(UnitFund $fund, Carbon $date)
    {
        parent::__construct($fund, $date);
    }


    public function aum()
    {
        return $this->marketValue();
    }

    public function marketValue()
    {
        return $this->clients()->sum(function (Client $client) {
            return $client->calculated->marketValue();
        });
    }

    public function costValue()
    {
        return $this->clients()->sum(function (Client $client) {
            return $client->calculated->costValue();
        });
    }

    public function clients()
    {
        if ($this->clients) {
            return $this->clients;
        }

        return $this->clients =  Client::with(
            'unitFundPurchases',
            'unitFundPurchases.unitFund',
            'unitFundSales',
            'unitFundSales.unitFund'
        )
            ->whereHas('unitFundPurchases', function ($purchases) {
                $purchases->where('unit_fund_id', $this->fund->id);
            })->get()
            ->each(function (Client $client) {
                $client->calculated = $client->calculateFund($this->fund, $this->date);
            });
    }
}
