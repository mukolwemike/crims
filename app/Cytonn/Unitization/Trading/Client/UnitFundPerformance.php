<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/18/18
 * Time: 2:40 PM
 */

namespace App\Cytonn\Unitization\Trading\Client;

interface UnitFundPerformance
{
    public function aum();

    public function marketValue();

    public function costValue();
}
