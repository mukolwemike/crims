<?php

namespace Cytonn\Unitization\Trading;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundHolderFee;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Unitization\UnitFundSaleRepository;
use App\Events\Investments\Actions\UnitFundSaleHasBeenMade;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\BankDetails;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Custodial\Withdraw as CustodyWithdraw;

/**
 * Class Sell
 *
 * @package Cytonn\Unitization\Trading
 */
class Sell extends Trade
{
    protected $client;

    public function __construct(UnitFund $fund, Client $client, Carbon $date)
    {
        parent::__construct($fund, $date);

        $this->client = $client;
    }

    /**
     * @param $units
     * @param array $saleData
     * @return UnitFundSale
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    public function sell($units, $saleData = [])
    {
        $this->validate($units);

        $balance = $this->client->repo->ownedNumberOfUnits($this->fund, $this->date);

        $feesCharged = $this->chargeFees($units);

        $charged = 0;

        if ($feesCharged) {
            $charged = $feesCharged->sum(function (UnitFundFeesCharged $charge) {
                return $charge->deduction()->sum('number');
            });
        }

        $total = $units + $charged;

        $withdraw = $units;

        if ($total > $balance) {
            $withdraw = $units - $charged;
        }

        $sale = $this->deductUnits($withdraw, $saleData, true);

        if ($sale->fees->count()) {
            $sale->fees()->saveMany($feesCharged);
        }

        return $sale;
    }

    /**
     * @param $units
     * @return bool
     * @throws ClientInvestmentException
     * @throws CRIMSGeneralException
     */
    protected function validate($units)
    {
        if ($this->fund->category->slug == 'pension-fund') {
            return true;
        }

        if (!$this->fund->previousPerformance($this->date->copy())) {
            throw new ClientInvestmentException(
                "Cannot sell, unit fund price/rate for previous day is not specified"
            );
        }

        $withdrawableUnits = $this->withdrawableUnits();

        if ($units > (0.01 + $withdrawableUnits)) {
            throw new ClientInvestmentException(
                "The units to be withdrawn (" . $units . ") are more than the available units
                         (" . number_format($withdrawableUnits, 2) . ")"
            );
        }

        $held = $this->client->repo->ownedNumberOfUnits($this->fund, $this->date);

        if ($units < 1) {
            throw new CRIMSGeneralException("Cannot sell less than one unit");
        }

        if ($units > (0.01 + $held)) {
            $u = AmountPresenter::currency($units);
            $h = AmountPresenter::currency($held);

            throw new ClientInvestmentException(
                "Client does not have enough units. Requested: $u, Available: $h"
            );
        }

        return true;
    }

    /**
     * @param $units
     * @return bool
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    public function canSell($units)
    {
        return $this->validate($units);
    }

    protected function chargeFees($units)
    {
        $fees = $this->calculateFees($units);

        $exceeded_lock_in = $this->client->repo->exceededLockInPeriod($this->date->copy(), $this->fund);

        if (!$fees || !$fees->count() || $exceeded_lock_in) {
            return 0;
        }

        return $fees->map(function ($fee) use ($units) {
            $feeUnits = $fee->fee_units;

            $deduction = $this->deductUnits(
                $feeUnits,
                [
                    'description' => "Fee: {$fee->type->name} for withdrawal of $units units on {$this->date->toFormattedDateString()}",
                    'narration' => 'Fees: ' . $fee->type->name
                ],
                false
            );

            $feeCharged = $this->recordFee($fee, $feeUnits * $this->price());

            $feeCharged->deduction()->associate($deduction);
            $feeCharged->save();

            return $feeCharged;
        });
    }

    public function calculateFees($units)
    {
        $fees = $this->feeToBeCharged();

        $exceeded_lock_in = $this->client->repo->exceededLockInPeriod($this->date->copy(), $this->fund);

        if (!$fees->count() || $exceeded_lock_in) {
            return 0;
        }

        return $fees->map(function ($fee) use ($units) {
            $feeUnits = $this->feeValue($fee, $units);

            $fee->fee_units = $feeUnits;

            return $fee;
        });
    }

    protected function feeToBeCharged()
    {
        return $this->fund
            ->terminalFees()
            ->get();
    }

    /**
     * @param $number
     * @param null $desc
     * @param null $narration
     * @param bool $cashOut
     * @return UnitFundSale
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function deductUnits($number, $saleData = [], $cashOut = true)
    {
        $price = $this->fund->unitPrice($this->date->copy());

        $input = [
            'description' => isset($saleData['narration']) ? $saleData['narration'] : 'Sale',
            'number' => $number,
            'date' => $this->date->copy(),
            'price' => $price
        ];

        if (array_key_exists('sale_type', $saleData)) {
            $input['sale_type'] = $saleData['sale_type'];
        }

        $sale = new UnitFundSale($input);

        $sale->client()->associate($this->client);

        $sale->fund()->associate($this->fund);

        if ($cashOut) {
            $description = isset($saleData['description'])
                ? $saleData['description']
                : "Sale of $number units on {$this->date->toFormattedDateString()}";

            $payment = Payment::make(
                $this->client,
                null,
                null,
                null,
                null,
                null,
                $this->date->copy(),
                $number * $this->price(),
                'W',
                $description,
                $this->fund
            )->debit();

            $sale->payment()->associate($payment);
        }

        $sale->save();

        (new UnitFundSaleRepository($this->fund, $sale))->linkToPurchase();

        return $sale;
    }

    /**
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function payInterest(ClientTransactionApproval $approval)
    {
        $interestUnits = $this->client->unitFundClientRepo->getUnitFundInterestAsAt($this->fund, $this->date);

        $this->validate($interestUnits);

        $price = $this->fund->unitPrice($this->date->copy());

        $sale = new UnitFundSale([
            'description' => 'Interest Payment',
            'number' => $interestUnits,
            'date' => $this->date->copy(),
            'price' => $price,
            'client_id' => $this->client->id,
            'unit_fund_id' => $this->fund->id,
            'approval_id' => $approval->id,
            'sale_type' => 'interest'
        ]);

        $this->client->unitFundClientRepo->updateNextInterestPaymentDate($this->fund);

        $payment = Payment::make(
            $this->client,
            null,
            null,
            null,
            null,
            null,
            $this->date->copy(),
            $interestUnits * $price,
            'W',
            'Interest Payment',
            $this->fund
        )->debit();

        $sale->payment()->associate($payment);

        $sale->save();

        $this->generateInstruction($sale);

        event(new UnitFundSaleHasBeenMade($sale));

        return $sale;
    }

    /**
     * @param UnitFundSale $sale
     * @param ClientBankAccount|null $bankAccount
     * @param CustodialAccount|null $account
     * @return \Cytonn\Custodial\Instruction
     */
    public function generateInstruction(
        UnitFundSale $sale,
        ClientBankAccount $bankAccount = null,
        CustodialAccount $account = null
    ) {
        $account = $account ? $account : $sale->fund->getDisbursementAccount();

        if (is_null($bankAccount)) {
            $bankAccount = $sale->instruction ? $sale->instruction->bankAccount : null;
        }

        return CustodyWithdraw::make(
            $this->date,
            $account,
            $sale->description ? $sale->description : "Withdrawal",
            $sale->client,
            $sale->payment->amount,
            null,
            null,
            null,
            $sale->approval,
            $sale->payment,
            null,
            null,
            $bankAccount,
            null,
            $sale->payment->fund
        );
    }

    /**
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function withdrawableUnits()
    {
        $calculator = $this->client->calculateFund($this->fund, $this->date, null, false);

        $purchases = $this->client->repo->totalUnitPurchasesWithinLockPeriod($this->fund, $this->date);

        $withdrawableUnits = $calculator->totalUnits() - $purchases;

       if($withdrawableUnits < 0){
           return 0;
       }

        return $withdrawableUnits;
    }

    /**
     * @param $number
     * @return bool
     * @throws ClientInvestmentException
     */
    public function aboveWithdrawalLimit($number)
    {
        return $number > (0.01 + $this->withdrawableUnits());
    }

    /**
     * @param UnitFundInvestmentInstruction $instruction
     * @return UnitFundSale|null
     * @throws CRIMSGeneralException
     */
    public function chargeTransactionFee(UnitFundInvestmentInstruction $instruction)
    {
        $clientAccount = $instruction->bankAccount;

        if (!$this->shouldChargeTransactionFee($clientAccount)) {
            return null;
        }

        $transactionFee = $this->getTransactionFee();

        $feeAmount = $transactionFee->getAmount();

        $sale = $this->chargeUnitSaleFees($feeAmount, $instruction->number);

        $feeCharged = $this->recordFee($transactionFee, $feeAmount * $this->price());

        $feeCharged->deduction()->associate($sale);

        $feeCharged->save();

        return $sale;
    }

    /**
     * @param ClientBankAccount|null $clientAccount
     * @return bool
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     */
    public function shouldChargeTransactionFee(ClientBankAccount $clientAccount = null)
    {
        $transactionFee = $this->getTransactionFee();

        if (! $transactionFee) {
            return false;
        }

        if (is_null($clientAccount)) {
            $clientAccount = $this->client->present()->getDefaultAccount;
        }

        $bank = new BankDetails(null, $this->client, $clientAccount);

        return $bank->paymentType() == 'mpesa' ? true : false;
    }

    /**
     * @param null $feeAmount
     * @param int $soldUnits
     * @return UnitFundSale
     * @throws CRIMSGeneralException
     */
    public function chargeUnitSaleFees($feeAmount = null, $soldUnits = 0)
    {
        $feeAmount = $feeAmount ? $feeAmount : $this->calculateWithdrawalFee($soldUnits);

        $this->validate($feeAmount);

        return $this->deductUnits(
            $feeAmount,
            [
                'sale_type' => 'unit_sale_transaction_fee',
                'narration' => 'Unit Sale Transaction Fee'
            ],
            false
        );
    }

    public function getTransactionFee()
    {
        return $this->fund->transactionFees()->latest()->first();
    }

    /**
     * @param $unitsSold
     * @return int
     */
    public function calculateWithdrawalFee($unitsSold = null)
    {
        $transactionFee = $this->getTransactionFee();

        return $transactionFee ? $transactionFee->getAmount() : 25;
    }
}
