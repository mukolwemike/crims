<?php

namespace Cytonn\Unitization\Trading;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;

class Calculator
{
    protected $fund;

    protected $amount;

    protected $units;

    protected $date;

    protected $type;

    public function __construct(UnitFund $fund, $amount = null, $numberOfUnits = null, Carbon $date = null)
    {
        $this->fund = $fund;

        $this->amount = $amount;

        $this->units = $numberOfUnits;

        $this->date = Carbon::parse($date);
    }

    public function feesCharged()
    {
        return $this->initialFees()->sum('amount');
    }

    public function unitPrice()
    {
        return $this->fund->unitPrice($this->date->copy());
    }

    public function netAmount()
    {
        return $this->amount - $this->feesCharged();
    }

    public function numberOfUnits()
    {
        return $this->netAmount() / $this->unitPrice();
    }

    public function balance()
    {
        return $this->netAmount() % $this->unitPrice();
    }

    public function initialFees()
    {
        return $this->fund->initialFees->map(function (UnitFundFee $fee) {

            $dependent_on = $fee->percentages()->latest()->first()->parameter->slug;

            $is_upfront = $fee->dependent and ($dependent_on == 'investment-amount');

            $percentage = $fee->getPercentageAsAtDate($this->date)
                ? $fee->getPercentageAsAtDate($this->date)->percentage
                : 0;

            ($this->amount)
                    ? $amount = !$is_upfront ? 0 : $percentage * $this->amount/100
                    : $amount = !$is_upfront
                        ? 0
                        : ($percentage * ($this->units * $this->unitPrice()))/(100 - $percentage)
                    ;

            return (object) ['amount' => $amount, 'fee' => $fee];
        });
    }

    public function valueOfUnits()
    {
        return $this->unitPrice() * $this->numberOfUnits();
    }
}
