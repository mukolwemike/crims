<?php

namespace Cytonn\Unitization\Trading;

use function Amp\Parallel\Worker\create;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundApplication;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundHolderFee;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Unitization\Commissions\Generators\Generator;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Unitization\UnitFundCommissionRepository;

class Buy extends Trade
{
    protected $date;

    private $amount;

    private $client;

    private $fundPurchase;

    private $recipient;

    private $skipFees;

    protected $fund;

    private $skipPayment;

    private $dividend;

    public function __construct(
        Client $client,
        CommissionRecepient $recipient = null,
        $amountBeforeInvestment,
        Carbon $date = null,
        UnitFundApplication $application = null,
        $skipFees = false,
        UnitFund $fund = null,
        $skipPayment = null,
        $dividend = null
    ) {
        $this->fund = (!is_null($fund)) ? $fund : $application->fund;

        $this->date = Carbon::parse($date);

        parent::__construct($this->fund, $this->date);

        $this->client = $client;

        $this->recipient = $recipient;

        $this->amount = $amountBeforeInvestment;

        $this->skipFees = $skipFees;

        $this->skipPayment = $skipPayment;

        $this->dividend = $dividend;
    }

    /**
     * @return UnitFundPurchase
     * @throws ClientInvestmentException
     */
    public function purchase()
    {
        $this->validate();

        $fees = $this->skipFees ? collect([]) : $this->calculateUpfrontFees();

        $total_fees = $fees->sum('amount');

        $amount = $this->amount - $total_fees;

        if ($amount <= 0) {
            throw new ClientInvestmentException("Cannot invest, amount after fee deduction is zero");
        }

        $purchase = $this->transferUnits($this->amount);

        $this->saveFeesCharged($fees, $purchase);

        return $purchase;
    }

    /**
     * @throws ClientInvestmentException
     */
    protected function validate()
    {
        $calc = $this->fund->category->calculation->slug;

        switch ($calc) {
            case 'variable-unit-price':
                if (!$this->fund->previousPerformance($this->date->copy())) {
                    throw new ClientInvestmentException("Cannot invest, unit fund price is not specified");
                }
                break;
            case 'daily-yield':
                break;
            case 'income-distribution':
                break;
            default:
                throw new ClientInvestmentException("Fund calculation not supported");
        }
    }

    protected function makePayment(UnitFundPurchase $purchase)
    {
        $description = "Investment: purchased {$purchase->number} units at {$purchase->price}";

        $amount = $purchase->number * $purchase->price;

        $payment = Payment::make(
            $this->client,
            $this->recipient,
            null,
            null,
            null,
            null,
            $this->date,
            $amount,
            'I',
            $description,
            $this->fund
        )->credit();

        return $payment;
    }

    protected function getUpfrontFees()
    {
        return $this->fund
            ->initialFees;
    }

    public function calculateUpfrontFees()
    {
        $deducted = $this->getUpfrontFees()->map(function (UnitFundFee $fee) {

            if ($this->skipFees) {
                return (object)['amount' => 0, 'fee' => $fee];
            }

            $dependent_on = $fee->percentages()->latest()->first()->parameter->slug;

            $is_upfront = $fee->dependent and ($dependent_on == 'investment-amount');

            $amount = 0;

            if ($is_upfront) {
                $percentageRecord = $fee->getPercentageAsAtDate($this->date);

                $amount = $percentageRecord->is_amount ? $percentageRecord->percentage : $percentageRecord->percentage * $this->amount / 100;
            }

           // $amount = !$is_upfront ? 0 : $fee->getPercentageAsAtDate($this->date)->percentage * $this->amount / 100;

            return (object)['amount' => $amount, 'fee' => $fee];
        });

        return $deducted;
    }

    public function transferUnits($amount, $description = null)
    {
        $price = $this->fund->unitPrice($this->date->copy());

        $number = \roundOffUnits($amount / $price);

        if (!$description) {
            $description = ($this->dividend)
                ? 'Shares from' . $this->dividend->security->name . 'dividend'
                : 'Purchase';
        }

        $purchase = new UnitFundPurchase([
            'description' => $description,
            'number' => $number,
            'date' => $this->date,
            'price' => $price
        ]);

        if (!$this->skipPayment) {
            $payment = $this->makePayment($purchase);

            $purchase->payment()->associate($payment);
        }

        if ($this->dividend) {
            $purchase->dividend()->associate($this->dividend);
        }

        $purchase->client()->associate($this->client);

        $purchase->unitFund()->associate($this->fund);

        $purchase->save();

        if ($this->recipient) {
            (new UnitFundCommissionRepository($purchase, $price, $this->recipient, $amount))->saveCommission();

            (new Generator())->createSchedules($purchase);
        }

        return $purchase;
    }

    public function saveFeesCharged($fees, $purchase)
    {
        $fees->each(function ($feeObj) use ($purchase) {
            $fee = $feeObj->fee;
            $amount = $feeObj->amount;

            $units = roundOffUnits($amount / $this->fund->unitPrice($this->date));

            //create corresponding sale
            $sale = (new Sell($this->fund, $this->client, $this->date))->deductUnits(
                $units,
                [
                    'narration' => 'Fee: ' . $fee->type->name
                ],
                false
            );

            $chargedFee = UnitFundFeesCharged::create([
                'amount' => $amount,
                'date' => $this->date
            ]);

            $chargedFee->fund()->associate($this->fund);

            $chargedFee->deduction()->associate($sale);

            $chargedFee->purchase()->associate($purchase);

            $chargedFee->fee()->associate($fee);

            $chargedFee->save();
        });
    }
}
