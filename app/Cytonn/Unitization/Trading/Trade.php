<?php

namespace Cytonn\Unitization\Trading;

use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundBenchmark;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeeParameter;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Support\Collection;

class Trade
{
    /**
     * @var Carbon
     */
    protected $date;

    /**
     * @var UnitFund
     */
    protected $fund;

    /**
     * Trade constructor.
     *
     * @param UnitFund $fund
     * @param Carbon   $date
     */
    public function __construct(UnitFund $fund, Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $this->fund = $fund;
        $this->date = $date;
    }

    public function previousFeesCharged(UnitFundFee $fee = null)
    {
        $charges =  $this->fund->feesCharged()
            ->before($this->date, true);

        if ($fee) {
            $charges = $charges->where('fee_id', $fee->id);
        }

        return $charges->sum('amount');
    }

    public function todayFeesCharged(UnitFundFee $fee = null)
    {
        $charges =  $this->fund->feesCharged()->where('date', $this->date);

        if ($fee) {
            $charges = $charges->where('fee_id', $fee->id);
        }

        return $charges->sum('amount');
    }

    public function todayFeesChargingAmount($aum, UnitFundFee $fee = null)
    {
        return $this->todayFeesCharging($aum, $fee)->sum('amount');
    }

    /**
     * @param $aum
     * @param UnitFundFee|null $fee
     * @param null $grossYield
     * @return Collection
     */
    public function todayFeesCharging($aum, UnitFundFee $fee = null, $grossYield = null)
    {
        $fees = $this->feesToBeChargedToday($fee);

        return $fees->map(function (UnitFundFee $fee) use ($aum, $grossYield) {

            $parameter = $fee->getPercentageAsAtDate($this->date);

            if (!$parameter) {
                return (object) [
                    'fee' => $fee,
                    'dependent_on' => null,
                    'amount' => $fee->calculateFixedFee()
                ];
            }

            $type = UnitFundFeeParameter::find($parameter->unit_fund_fee_parameter_id)->slug;

            switch ($type) {
                case 'total-asset-under-management':
                    $amount = $fee->calculateRecurrentAmount($aum, $this->date);

                    return (object)[
                        'fee' => $fee,
                        'dependent_on' => 'total-asset-under-management',
                        'amount' => $amount
                    ];
                    break;
                case 'investment-amount':
                    //These are charged on investment and saved, cannot be calculated here.
                    break;
                case 'outperformance':
                    if (!$grossYield) {
                        return null;
                    }

                    return (object) [
                        'fee' => $fee,
                        'dependent_on' => $type,
                        'amount' => $this->calculateTodayOutperformance($aum, $grossYield)
                    ];

                    break;
                default:
                    throw new \InvalidArgumentException("Fee type $type not supported");
            }

            return null;
        })->filter(function ($fee) {
            return !is_null($fee);
        });
    }

    protected function calculateTodayOutPerformance($aum, $grossYield)
    {
        $benchmarks = $this->fund->benchmarks()->where('type', 'outperformance')->get();

        return $benchmarks->sum(function (UnitFundBenchmark $benchmark) use ($aum, $grossYield) {
            $limit = $benchmark->benchmark->benchmarkValues()->latest('date')->first();

            if (!$limit) {
                return 0;
            }

            $value = $limit->value + $benchmark->modifier;

            return $this->calculateOutPerformance($benchmark, $grossYield, $value, $aum);
        });
    }

    private function calculateOutPerformance(UnitFundBenchmark $benchmark, $grossYield, $limit, $aumAtCost)
    {
        $fee = $benchmark->fee;

        if (!$fee) {
            return 0;
        }

        $perc = $fee->getPercentageAsAtDate($this->date);

        $managerShare = $perc ? $perc->percentage/100 : 0;

        if ($grossYield <= $limit || $managerShare == 0) {
            return 0;
        }

        $outPerformance = ($grossYield - $limit)/100;

        return $outPerformance * $managerShare * $aumAtCost/365;
    }

    public function feesToBeChargedToday(UnitFundFee $fee = null)
    {
        if ($fee) {
            return collect([$fee]);
        }

        return UnitFundFee::whereHas('feeChargeType', function ($type) {
            $type->where('slug', 'recurrent');
        })
        ->where('date', '<=', $this->date->copy())
        ->where('unit_fund_id', $this->fund->id)
        ->whereDoesntHave('charges', function ($charges) {
            $charges->where('date', $this->date);
        })->get();
    }

    protected function price(Carbon $date = null)
    {
        $d = $date ? $date->copy() : $this->date->copy();

        return $this->fund->unitPrice($d);
    }

    public function feeValue(UnitFundFee $fee, $units)
    {
        if ($fee->dependent) {
            return ($fee->getPercentageAsAtDate($this->date)->percentage * $units) / 100;
        }

        return $fee->getAmount();
    }

    /**
     * @param UnitFundFee $fee
     * @param $amount
     * @return UnitFundFeesCharged
     */
    protected function recordFee(UnitFundFee $fee, $amount)
    {
        if ($amount <= 0) {
            return;
        }

        return UnitFundFeesCharged::create([
            'amount' => $amount,
            'date' => $this->date,
            'fee_id' => $fee->id,
            'unit_fund_id' => $this->fund->id
        ]);
    }
}
