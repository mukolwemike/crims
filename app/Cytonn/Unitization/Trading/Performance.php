<?php

namespace Cytonn\Unitization\Trading;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Portfolio\AssetClasses\AssetClassPerformance;
use Cytonn\Portfolio\AssetClasses\DepositPerformance;
use Cytonn\Portfolio\AssetClasses\EquitiesPerformance;

class Performance extends Trade
{
    protected $offshore;

    private $cache = [];

    public function __construct(UnitFund $fund, Carbon $date = null)
    {
        parent::__construct($fund, $date);
    }

    protected function assets()
    {
        $date = $this->date;

        $fm = $this->fund->fundManager;

        return collect([
            $this->getDepositPerformance($fm, $date),
            $this->getEquitiesPerformance($fm, $date)
        ]);
    }

    private function getDepositPerformance($fm, $date)
    {
        $deposit = (new DepositPerformance($fm, $date))->setFund($this->fund);

        return $this->offshore ? $deposit->setOffshore($this->offshore) : $deposit;
    }

    private function getEquitiesPerformance($fm, $date)
    {
        $equities = (new EquitiesPerformance($fm, $date))->setFund($this->fund);

        return $this->offshore ? $equities->setOffshore($this->offshore) : $equities;
    }

    public function setOffshore($offshore)
    {
        $this->offshore = $offshore;
        return $this;
    }

    public function assetCostValue()
    {
        $key = $this->key('asset_cost_value');

        if ($cv = $this->get($key)) {
            return $cv;
        }

        $cv = $this->assets()->sum(function (AssetClassPerformance $performance) {
            return $performance->costValue();
        });

        return $this->cache($key, $cv);
    }

    public function assetMarketValue()
    {
        $key = $this->key('asset_market_value');

        if ($val = $this->get($key)) {
            return $val;
        }

        $cv = $this->assets()->sum(function (AssetClassPerformance $performance) {
            return $performance->marketValue();
        });

        return $this->cache($key, $cv);
    }

    public function portfolioReturn()
    {
        $aum = $this->aumAtCost();

        return $this->assets()->sum(function (AssetClassPerformance $assetClass) use ($aum) {

            return $assetClass->percentageReturn() * $assetClass->costValue()/$aum ;

//            dump([
//                'class' => basename(get_class($assetClass)),
//                'return' => $assetClass->percentageReturn(),
//                'cv' => $assetClass->costValue(),
//                'aum' => $aum,
//                'weighted' => $ret
//            ]);
        });
    }

    public function accounts()
    {
        return $this->fund->repo->portfolioAccounts();
    }

    public function cashBalance(CustodialAccount $account = null)
    {
        if ($account) {
            return convert($account->balance($this->date), $account->currency, $this->date, $this->fund->currency);
        }

        return $this->accounts()
            ->sum(function ($account) {
                $amount = $account->balance($this->date);

                return convert($amount, $account->currency, $this->date, $this->fund->currency);
            });
    }

    public function cashInflows(Carbon $dateFrom)
    {
        $accounts = $this->accounts();

        return $this->accounts()->sum(function (CustodialAccount $account) use ($dateFrom, $accounts) {
            return $this->transactionsQuery($account, $dateFrom)
                    ->where('amount', '>', 0)
                    ->where(function ($q) use ($accounts) {
                        $q->ofType('FI')
                            ->orWhere(function ($q) use ($accounts) {
                                $q->isTransferFrom($accounts);
                            })->orWhere(function ($q) {
                                $q->ofType('RR');
                            });
                    })->sum('amount');
        });
    }

    public function cashOutflows(Carbon $dateFrom)
    {
        $accounts = $this->accounts();

        return $accounts->sum(function (CustodialAccount $account) use ($dateFrom, $accounts) {
            $bal = $this->transactionsQuery($account, $dateFrom)
                    ->where('amount', '<', 0)
                    ->where(function ($q) use ($accounts) {
                        $q->ofType('FO')
                            ->orWhere(function ($q) use ($accounts) {
                                $q->isTransferTo($accounts);
                            })->orWhere(function ($q) {
                                $q->isExpense();
                            });
                    })->sum('amount');

            return abs($bal);
        });
    }

    private function transactionsQuery($account, $dateFrom)
    {
        return $account->transactions()
            ->between($dateFrom, $this->date);
    }


    public function grossIncomeForDay()
    {
        $key = $this->key('gross_income_for_day');

        if ($i = $this->get($key)) {
            return $i;
        }

        $i = $this->assets()->sum(function (AssetClassPerformance $performance) {
            return $performance->grossInterestForDay();
        });

        return $this->cache($key, $i);
    }

    public function netIncomePerUnit()
    {
        $key = $this->key('net_income_per_unit');

        if ($i = $this->get($key)) {
            return $i;
        }

        $grossIncome = $this->grossIncomeForDay();

        $fees = $this->todayLiabilities(true);

        $aum = $this->aumAtCost();

        if ($aum == 0) {
            return 0;
        }

        $i =  100 * ($grossIncome - $fees)/$aum;

        return $this->cache($key, $i);
    }

    public function grossDailyYield()
    {
        $key = 'gross_daily_yield';

        if ($amt = $this->get($key)) {
            return $amt;
        }

        $yield = $this->netIncomePerUnit() * 365;

        return $this->cache($key, $yield);
    }

    public function todayOutPerformanceAmount()
    {
        $gross = $this->grossDailyYield();
        $aum = $this->aumAtCost();

        return $this->calculateTodayOutPerformance($aum, $gross);
    }

    public function todayOutPerformancePercent()
    {
        $aumAtCost = $this->aumAtCost();

        return $aumAtCost != 0 ? 365 * 100 * $this->todayOutPerformanceAmount() / $aumAtCost : 0;
    }

    public function netDailyYield()
    {
        return $this->grossDailyYield() - $this->todayOutPerformancePercent();
    }

    public function grossAnnualYield()
    {
        return $this->convertToAnnual($this->grossDailyYield());
    }

    public function netAnnualYield()
    {
        return $this->convertToAnnual($this->netDailyYield());
    }

    public function aumAtCost()
    {
        $key = $this->key('aum_cost');

        if ($aum = $this->get($key)) {
            return $aum;
        }

        $aum =  $this->assetCostValue() + $this->cashBalance();

        return $this->cache($key, $aum);
    }

    public function aum()
    {
        $key = $this->key('aum');

        if ($aum = $this->get($key)) {
            return $aum;
        }

        $aum =  $this->assetMarketValue() + $this->cashBalance();

        return $this->cache($key, $aum);
    }

    public function liabilities()
    {
        $previous = $this->previousFeesCharged();

        return $previous + $this->todayLiabilities();
    }

    public function todayLiabilities($atCost = false)
    {
        $aum = $atCost ? $this->aumAtCost() : $this->aum();

        return $this->todayFeesCharged() + $this->todayFeesChargingAmount($aum);
    }

    public function nav()
    {
        return $this->aum() - $this->liabilities();
    }

    public function totalUnits()
    {
        return $this->fund->repo->getUnits($this->date);
    }

    public function unitPrice()
    {
        $newUnits = $this->totalUnits();

        return $newUnits > 0 ? $this->nav() / $newUnits : 0;
    }

    private function convertToDaily($annual)
    {
        $r = 1 + $annual/100;

        return 100 * 365 * ($r ** (1/365) - 1);
    }

    private function convertToAnnual($daily)
    {
        $r = $daily/(365 * 100);

        return 100 * (((1 + $r) ** 365) -1);
    }

    private function key($addition = null)
    {
        return '_fund_performance_fund'.$this->fund->id.'_date'.$this->date->toDateString().
            '_offshore_'.$this->offshore."_plus_".$addition;
    }

    private function cache($key, $value)
    {
        $this->cache[$key] = $value;

        return $value;
    }

    private function get($key)
    {
        if (isset($this->cache[$key])) {
            return $this->cache[$key];
        }

        return false;
    }
}
