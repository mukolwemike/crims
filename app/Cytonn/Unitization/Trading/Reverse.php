<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 21/05/2018
 * Time: 11:43
 */

namespace Cytonn\Unitization\Trading;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\Unitization\UnitFundTransfer;

class Reverse
{
    public function purchase(UnitFundPurchase $purchase)
    {
        $purchase->fees->each(function (UnitFundFeesCharged $fee) {
            $this->payment($fee->payment);

            if ($ded = $fee->deduction) {
                $this->sale($ded);
            }

            $fee->delete();
        });

        if ($purchase->unitFundCommission) {
            $purchase->unitFundCommission->schedules->each(function ($schedule) {
                $schedule->delete();
            });
        }
        
        $purchase->unitFundCommission()->delete();

        $purchase->commissionPaymentSchedule()->delete();

        $purchase->businessConfirmations()->delete();

        $this->payment($purchase->payment);

        $purchase->delete();
    }

    public function sale(UnitFundSale $sale)
    {
        $sale->fees->each(function (UnitFundFeesCharged $fee) {

            $this->payment($fee->payment);

            if ($ded = $fee->deduction) {
                $this->sale($ded);
            }

            $fee->delete();
        });

        $this->payment($sale->payment);

        $sale->unitFundPurchasesSales->each(function (UnitFundPurchase $purchase) {
            $purchase->sold = null;
            $purchase->sale_date = null;

            $purchase->save();
        });

        $sale->unitFundPurchasesSales()->detach();

        $sale->delete();
    }

    public function transfer(UnitFundTransfer $transfer)
    {
        if($transfer->fees){
            $transfer->fees->each(function (UnitFundFeesCharged $fee) {
                $this->payment($fee->payment);

                if ($ded = $fee->deduction) {
                    $this->sale($ded);
                }

                $fee->delete();
            });
        }

        $this->purchase($transfer->purchase);
        $this->sale($transfer->sale);

        $transfer->delete();
    }

    public function payment(ClientPayment $payment = null)
    {
        if (is_null($payment)) {
            return;
        }

        $payment->custodialTransaction()->delete();

        $payment->delete();
    }
}
