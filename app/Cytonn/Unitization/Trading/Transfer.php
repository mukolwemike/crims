<?php

namespace Cytonn\Unitization\Trading;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use Carbon\Carbon;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Presenters\ClientPresenter;

class Transfer extends Trade
{
    protected $date;

    /**
     * @var Client
     */
    protected $sender;

    /**
     * @var Client
     */
    protected $recipient;

    protected $number;


    public function __construct(UnitFund $fund, Carbon $date, $number)
    {
        $this->number = $number;
        parent::__construct($fund, $date);
    }

    /**
     * @param Client $sender
     * @return $this
     */
    public function from(Client $sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @param Client $recipient
     * @return $this
     */
    public function to(Client $recipient)
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     * @return UnitFundTransfer
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function send()
    {
        $this->validate();

        $transfer = new UnitFundTransfer([
            'number' => $this->number,
            'date' => $this->date,
        ]);

        $transfer->sender()->associate($this->sender);
        $transfer->recipient()->associate($this->recipient);
        $transfer->fund()->associate($this->fund);

        $sale = $this->sell();
        $purchase = $this->purchase();
        $transfer->sale()->associate($sale);
        $transfer->purchase()->associate($purchase);

        $transfer->save();

        return $transfer;
    }

    private function description()
    {
        return "Transfer $this->number units from " .
            ClientPresenter::presentJointFullNames($this->sender->id) . ' to ' .
            ClientPresenter::presentJointFullNames($this->recipient->id);
    }

    /**
     * @return \App\Cytonn\Models\Unitization\UnitFundSale
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    protected function sell()
    {
        $sale = new Sell($this->fund, $this->sender, $this->date->copy());

        return $sale->deductUnits(
            $this->number,
            [
                'description' => $this->description(),
                'narration' => 'Transfer out'
            ],
            false
        );
    }


    protected function purchase()
    {
        $amount = $this->number * $this->price();

        $recipient = null;  //$client->getLatestFa('units');

        $buy = new Buy(
            $this->recipient,
            $recipient,
            $amount,
            $this->date,
            null,
            true,
            $this->fund,
            true,
            false
        );

        return $buy->transferUnits($amount, 'Transfer in');
    }

    /**
     * @throws CRIMSGeneralException
     */
    protected function validate()
    {
        if ($this->sender->id === $this->recipient->id) {
            throw new CRIMSGeneralException("Sender and recipient cannot be the same");
        }

        $canSell = (new Sell($this->fund, $this->sender, $this->date))->canSell($this->number);

        if (!$canSell) {
            throw new CRIMSGeneralException("Cannot transfer units, transaction failed");
        }

        $this->ensureRecipientIsClient();
    }

    /**
     * @throws CRIMSGeneralException
     */
    private function ensureRecipientIsClient()
    {
        return true; // allows transfer to even non-cmmf clients

//        $exists = $this->fund->purchases()->where('client_id', $this->recipient->id)->exists();
//
//        if (!$exists) {
//            throw new CRIMSGeneralException("Recipient {$this->recipient->client_code} is not a client in".
//            " {$this->fund->name}");
//        }
    }
}
