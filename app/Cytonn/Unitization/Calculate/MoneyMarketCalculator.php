<?php
/**
 * Date: 06/06/2018
 * Time: 10:56
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Unitization\Calculate;

use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Carbon as MyCarbon;
use Cytonn\Exceptions\CRIMSGeneralException;
use Illuminate\Support\Collection;
use LogicException;

class MoneyMarketCalculator extends Calculator
{
    protected $dailies;

    protected $actions;

    private $savedFilters;

    private static $fundRates = [];


    /**
     * @return object
     */
    protected function prepare()
    {
        $purchases = $this->purchases();

        $sales = $this->sales();

        $dailies = $this->calculateDailyInterest($purchases, $sales);

        $this->dailies = $dailies;

        $inPeriod = $this->inPeriodDailies($dailies);

        $opening = $this->openingBalance($dailies);

        $last = $inPeriod->last();

        $total = ($dailies->last()) ? $dailies->last()->total : 0;

        $price = $this->price();

        return (object)[
            'opening_balance' => $opening,
            'opening' => (object) (object) [
                'date' => $this->start,
                'price' => $price,
                'balance' => $opening
            ],
            'purchases' => $inPeriod->sum('purchase_amount'),
            'sales'  => $inPeriod->sum('sale_amount'),
            'gross_interest' => $inPeriod->sum('gross_interest'),
            'withholding_tax' => $inPeriod->sum('withholding_tax'),
            'net_interest'    => $inPeriod->sum('net_interest'),
            'net_interest_after' => $last ? $last->net_interest_after : 0,
            'total' => $total,
//            'dailies' => $dailies,
            'all_actions' => $inPeriod,
            'price'  => $price,
            'closing_balance' => $total
        ];
    }

    private function calculateDailyInterest($purchases, $sales)
    {
        $start = ($purchases->first()) ? $purchases->first()->date : Carbon::now();

        $principal = 0;

        $days = MyCarbon::daysBetweenArray($start, $this->date);

        $netInterestCF = 0;

        $interestData = (object) [
            'cumulative_gross_interest' => 0,
            'cumulative_wht' => 0,
            'cumulative_net_interest' => 0,
            'withdrawn_gross_interest' => 0,
            'withdrawn_wht' => 0,
            'withdrawn_net_interest' => 0
        ];

        return collect($days)->map(function (Carbon $day) use ($purchases, $sales, &$principal, &$netInterestCF, &$interestData) {
            $rate = $this->interestRate($day);

            $dayP = $purchases->filter(function ($purchase) use ($day) {
                return $purchase->date->copy()->startOfDay()->eq($day->copy()->startOfDay());
            });

            $p = $dayP->sum('number');

            $dayS = $sales->filter(function ($sale) use ($day) {
                return $sale->date->copy()->startOfDay()->eq($day->copy()->startOfDay());
            });

            $s = $dayS->sum('number');

            $original = $principal;
            $principal = $principal + $p - $s;

            $gi = $this->grossInterestForDay($principal, $rate);

            if ($day->copy()->startOfDay()->eq($this->date->startOfDay()) && !($this->as_at_next_day)) {
                $gi = 0;
            }

            //WHT is currently disabled
            $taxable = false;
            if ($day->gte(Carbon::parse(static::WHT_START_DATE))) {
                $taxable = $this->checkTaxable($this->client, $day);
                $ni = $this->getNetInterest($gi, $taxable);
            } else {
                $ni = $gi;
            }

            $tax = $gi - $ni;

            $principal = $total =  $principal + $ni;

            if (abs($principal) < 1) {
                $principal = 0;
                $total = 0;
            }

            $cumulativeNetInterest = $netInterestCF + $ni;
            $cumulativeGrossInterest = $interestData->cumulative_gross_interest + $gi - $interestData->withdrawn_gross_interest;
            $finalNetInterestCf = max($cumulativeNetInterest - $s, 0);
            $withdrawnNetInterest = $cumulativeNetInterest - $finalNetInterestCf;

            if ($cumulativeNetInterest != 0) {
                $withdrawnGrossInterest = $withdrawnNetInterest == $cumulativeNetInterest ? $cumulativeGrossInterest :
                    $withdrawnNetInterest * $cumulativeGrossInterest / $cumulativeNetInterest;
            } else {
                $withdrawnGrossInterest = 0;
            }

            $interestData->cumulative_gross_interest = $cumulativeGrossInterest;
            $interestData->cumulative_wht += $tax - $interestData->withdrawn_wht;
            $interestData->cumulative_net_interest += $ni - $interestData->withdrawn_net_interest;

            $interestData->withdrawn_gross_interest = $withdrawnGrossInterest;
            $whtTax = ($withdrawnGrossInterest - $withdrawnNetInterest);
            $interestData->withdrawn_wht = $whtTax;
            $interestData->withdrawn_adjusted_gross_interest = round($whtTax, 2) != 0 ?
                $this->calculateGrossFromWht(($withdrawnGrossInterest - $withdrawnNetInterest)) : $withdrawnGrossInterest;
            $interestData->withdrawn_net_interest = $withdrawnNetInterest;

            $netInterestCF = $finalNetInterestCf;

            return (object)[
                'date' => $day,
                'principal' => $original,
                'purchases' => $dayP,
                'sales' => $dayS,
                'purchase_amount' => $p,
                'sale_amount' => $s,
                'interest_rate' => $rate,
                'gross_interest' => $gi,
                'withholding_tax' => $tax,
                'net_interest' => $ni,
                'net_interest_after' => $netInterestCF,
                'interest_data' => clone $interestData,
                'total' => $total
            ];
        });
    }

    private function checkTaxable(Client $client, Carbon $date)
    {
        //return true if client is taxable
        if (!$client->taxable) {
            return false;
        }

        if ($client->relationLoaded('taxExemptions')) {
            return $this->checkForTaxExemptions($client, $date);
        }

        $hasExemption = $client->taxExemptions()->where(function ($exemption) use ($date) {
            $exemption->where('start', '<=', $date)->where('end', '>=', $date);
        })->orWhere(function ($exemption) use ($date) {
            $exemption->whereNull('start')->where('end', '>=', $date);
        })->orWhere(function ($exemption) use ($date) {
            $exemption->where('start', '<=', $date)->whereNull('end');
        })->exists();

        return !$hasExemption;
    }

    private function checkForTaxExemptions(Client $client, Carbon $date)
    {
        $matched =  $client->taxExemptions->filter(function (Client\TaxExemption $exemption) use ($date) {
            if (is_null($exemption->start) && is_null($exemption->end)) {
                return true;
            }

            if (is_null($exemption->end)) {
                return Carbon::parse($exemption->start)->lte($date);
            }

            if (is_null($exemption->start)) {
                return Carbon::parse($exemption->end)->gte($date);
            }

            return Carbon::parse($exemption->end)->gte($date) && Carbon::parse($exemption->start)->lte($date);
        });

        return count($matched) <= 0;
    }

    private function openingBalance(Collection $dailies)
    {
        $before = $dailies->filter(function ($day) {
            return $day->date->lt($this->start);
        });

        $last = $before->last();

        return $last ? $last->total : 0;
    }

    private function inPeriodDailies(Collection $dailies)
    {
        return $dailies->filter(function ($day) {
            return $day->date->gte($this->start);
        });
    }

    private function dateEquals(Carbon $start, Carbon $end, Carbon $third = null)
    {
        $first =  $start->copy()->startOfDay()->eq($end->copy()->startOfDay());

        if (!$third) {
            return $first;
        }

        return $first && $third->copy()->startOfDay()->eq($end->copy()->startOfDay());
    }

    protected function filterActions($openingBal, $actions)
    {
        $filtered = $actions->filter(function ($day) {
            $endMonth = $this->dateEquals($day->date, $day->date->copy()->endOfMonth());
            $end = $this->dateEquals($day->date, $this->date->copy());
            $start = $this->dateEquals($day->date, $this->start->copy());
            $has_action = ($day->purchase_amount != 0) || ($day->sale_amount != 0);

            return $endMonth || $end || $start || $has_action;
        });


        if ($actions->count() == 0) {
            return $actions;
        }

        $previous = clone $actions->first();
        $previous->date = $previous->date->copy();
        $previous->total = $openingBal;

        $startDate = $filtered->first()->date;
        $endDate = $filtered->last()->date;

        $used = [];

        $filteredActions = $filtered->map(function ($day) use (&$previous, $actions, $startDate, $endDate, &$used) {
            $in_period = $actions->filter(function ($action) use ($previous, $day, $startDate, $endDate, &$used) {
                if (array_has($used, $action->date->toDateString())) {
                    return false;
                }

                $isOnlyDay = $this->dateEquals($startDate, $endDate, $action->date);

                if ($isOnlyDay && $this->dateEquals($action->date, $this->date, $day->date)) {
                    $used[$action->date->toDateString()] = $action;
                    return true;
                }

                if ($this->dateEquals($startDate, $action->date, $day->date)) {
                    return false;
                }

                $after = $action->date->copy()->startOfDay()->gte($previous->date->copy()->startOfDay());
                $before = $action->date->copy()->startOfDay()->lte($day->date->copy()->startOfDay());

                $result = $before && $after;

                if ($result) {
                    $used[$action->date->toDateString()] = $action;
                }

                return $result;
            });

            $this->saveFiltered($in_period);

            $result = clone $day;


            $result->principal = $previous->total;
            $result->net_interest = $in_period->sum('net_interest');
            $result->gross_interest = $in_period->sum('gross_interest');
            $result->withholding_tax = $in_period->sum('withholding_tax');
            $result->from = $previous->date->copy()->addDay()->toDateString();
            $result->to = $day->date->copy()->toDateString();
            $result->total = $previous->total + $result->net_interest + $result->purchase_amount - $result->sale_amount;
            $result->opening_balance = $previous->total;

            $previous = $result;

            return $result;
        });

        $this->validateResult($this->getPrepared()->all_actions, $filteredActions);

        return $filteredActions;
    }

    private function saveFiltered($actions)
    {
        if (!$this->savedFilters) {
            $this->savedFilters = new Collection();
        }

        $this->savedFilters = $this->savedFilters->merge($actions);
    }

    /**
     * @param $actions
     * @param $filtered
     */
    private function validateResult($actions, $filtered)
    {
        $saved = $this->savedFilters->count();
        $org = $actions->count();

        if ($saved != $org) {
            throw new LogicException(
                "Some actions were lost/added during filtering - $saved saved, original $org"
            );
        }

        $saved_ni = (int) $filtered->sum('net_interest');
        $org_ni = (int) $actions->sum('net_interest');

        if ($org_ni != $saved_ni) {
            throw new LogicException(
                "The interest calculated does not match, original $org_ni, saved $saved_ni"
            );
        }

        $saved_total = (int) $filtered->last()->total;
        $org_total = (int) $actions->last()->total;

        if (abs($org_total - $saved_total) > 1) {
            throw new LogicException(
                "The total does not match, original $org_total, saved $saved_total"
            );
        }
    }



    private function interestRate(Carbon $date)
    {

        if (isset(static::$fundRates[$this->fund->id][$date->toDateString()])) {
            return static::$fundRates[$this->fund->id][$date->toDateString()];
        }

        $performance = $this->fund->performances()
            ->where('date', '<=', $date)
            ->latest('date')
            ->first();

        if (!$performance) {
            return 0;
        }

        $rate =  $performance->net_daily_yield;

        static::$fundRates[$this->fund->id][$date->toDateString()] = $rate;

        return $rate;
    }

    public function totalUnits()
    {
        return $this->prepared->total;
    }

    public function totalPurchases()
    {
        return $this->prepared->purchases;
    }

    public function totalSales()
    {
        return $this->prepared->sales;
    }

    public function totalInterest()
    {
        return $this->prepared->net_interest;
    }

    /**
     * @return int|mixed
     * @throws \Exception
     */
    public function costValue()
    {
        $net = $this->actions()->sum('net_interest');

        $price = $this->price();

        $principal = $this->purchases($this->date)->sum(function ($purchase) use ($price) {
            return $purchase->number * $price;
        });

        $sales = $this->sales()->sum(function ($sale) use ($price) {
            return $sale->number * $price;
        });

        $netSale = $sales - $net;

        if ($netSale <= 0) {
            $netSale  = 0;
        }

        return $principal - $netSale;
    }

    /**
     * @param null $date
     * @return float|int|mixed
     * @throws \Exception
     */
    public function price($date = null)
    {
        return $this->fixedPrice($date);
    }

    /**
     * @return mixed
     */
    public function getDailies()
    {
        return $this->dailies;
    }
}
