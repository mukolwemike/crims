<?php
/**
 * Date: 06/06/2018
 * Time: 10:56
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Unitization\Calculate;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundPurchaseSale;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;

class NavFundCalculator extends Calculator
{
    public function prepare()
    {
        //sales before end date
        $sales = $this->sales();

        //purchases before end date
        $purchases = $this->purchases();

        $opening = $this->balance($this->start);

        $actions = $this->getActions(0, $purchases, $sales);

        $in_period = $actions->filter(function ($action) {
            return $action->date->gte($this->start) && $action->date->lte($this->date);
        });

        return (object)[
            'opening_balance' => $opening,
            'opening' => (object) [
                'date' => $this->start,
                'price' => $this->price($this->start),
                'balance' => $opening
            ],
            'purchases' => $purchases->sum('number'),
            'sales'  => $sales->sum('number'),
            'price'  => $this->price(),
            'actions' => $in_period,
            'all_actions' => $in_period,
            'net_interest_after' => 0,
            'closing_balance' => $closing_balance = $this->balance($this->date),
            'total' => $closing_balance
        ];
    }

    protected function filterActions($openingBalance, $actions)
    {
        return $this->getPrepared()->actions;
    }

    protected function getActions($openingBalance, $purchases, $sales)
    {
        $actions = collect([])->merge($purchases)->merge($sales)->sortByDate('date', "ASC")->values();

        $balance = $openingBalance;

        return $actions->map(function ($action) use (&$balance) {
            $purchase = $sale = collect([]);

            if ($action instanceof UnitFundPurchase) {
                $number = $action->number;
                $purchase = collect([$action]);
            } elseif ($action instanceof UnitFundSale) {
                $number = -$action->number;
                $sale = collect([$action]);
            } elseif ($action instanceof UnitFundPurchaseSale) {
                $number = -$action->number;
                $sale = collect([$action]);
            } else {
                throw new ClientInvestmentException("Unknown action type ".get_class($action));
            }

            $balance = $balance + $number;

            $descr = $action->description;

            return (object)[
                'date' => $action->date,
                'action' => $action,
                'sales' => $sale,
                'purchases' => $purchase,
                'number' => $number,
                'balance' => $balance,
                'description' => $descr ? $descr : ($number > 0 ? 'Purchase' : 'Sale'),
                'price' => $this->price($action->date)
            ];
        });
    }

    protected function balance(Carbon $date)
    {
        return $this->purchases($date)->sum('number')
            - $this->sales($date)->sum('number');
    }

    public function totalUnits()
    {
        return $this->prepared->total;
    }

    public function totalPurchases()
    {
        return $this->prepared->purchases;
    }

    public function totalSales()
    {
        return $this->prepared->sales;
    }

    //To be updated
    public function totalInterest()
    {
        return 0;
    }

    public function costValue()
    {
        $units = $this->totalUnits();

        $purchases = $this->purchases($this->date)->sortByDate('date', 'DESC');

        $remaining = $units;

        return $purchases->reduce(function ($carry, $purchase) use (&$remaining) {
            if ($purchase->number <= $remaining) {
                $cv = $purchase->number * $this->price($purchase->date);

                $remaining = $remaining - $purchase->number;

                return $carry + $cv;
            }

            $cv = $remaining * $this->price($purchase->date);
            $remaining = 0;
            return $carry + $cv;
        });
    }
}
