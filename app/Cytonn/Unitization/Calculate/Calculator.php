<?php
/**
 * Date: 06/06/2018
 * Time: 10:44
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Unitization\Calculate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;

abstract class Calculator
{
    use CalculatorTrait;

    protected $client;

    protected $fund;

    /**
     * @var Carbon
     */
    protected $date;

    protected $as_at_next_day;

    const WHT_START_DATE = '2019-03-01';

    protected static $caches = [];

    /**
     * @var Carbon
     */
    protected $start;

    protected $prepared;

    protected $purchase;

    /**
     * Calculator constructor.
     * @param Client $client
     * @param UnitFund $fund
     * @param Carbon $date
     * @param Carbon $start
     * @param bool $as_at_next_day
     * @param UnitFundPurchase|null $purchase
     */
    public function __construct(
        Client $client,
        UnitFund $fund,
        Carbon $date,
        Carbon $start = null,
        $as_at_next_day = true,
        UnitFundPurchase $purchase = null
    ) {
        $this->client = $client;

        $this->fund = $fund;

        $this->date = $date->copy()->endOfDay();

        $this->as_at_next_day = $as_at_next_day;

        $this->purchase = $purchase;

        $this->start = $this->date->copy()->subMonthNoOverflow()->addDay()->startOfDay();

        if ($start && $this->date->greaterThan($start)) {
            $this->start = $start;
        }

        $this->prepared = $this->prepare();
    }

    protected function sales(Carbon $date = null)
    {
        $date = $date ? $date : $this->date;

        $date = $date->copy()->endOfDay();

        if ($this->purchase) {
            return $this->purchase->purchaseSales()
                ->before($date)
                ->oldest('date')
                ->get();
        }

        if ($this->client->relationLoaded('unitFundSales')) {
            return $this->client->unitFundSales->filter(function (UnitFundSale $sale) use ($date) {
                return $sale->unitFund->id == $this->fund->id &&
                    $sale->date->lte($date);
            })->sortByDate('date', 'ASC');
        }

        return $this->fund->sales()
            ->where('client_id', $this->client->id)
            ->before($date)
            ->oldest('date')
            ->get();
    }

    protected function purchases(Carbon $date = null)
    {
        if ($this->purchase) {
            return collect([$this->purchase]);
        }

        $date = $date ? $date : $this->date;

        $date = $date->copy()->endOfDay();

        if ($this->client->relationLoaded('unitFundPurchases')) {
            return $this->client->unitFundPurchases->filter(function (UnitFundPurchase $purchase) use ($date) {
                return $purchase->unitFund->id == $this->fund->id &&
                    $purchase->date->lte($date);
            })->sortByDate('date', 'ASC');
        }

        return $this->fund->purchases()
            ->where('client_id', $this->client->id)
            ->before($date)
            ->oldest('date')
            ->get();
    }

    public function price($date = null)
    {
        $slug = $this->fund->category->calculation->slug;

        if ($slug == 'daily-yield' || $slug == 'income-distribution') {
            return 1;
        }

        $date = $date ? $date : $this->date;

        return $this->fund->unitPrice($date);
    }

    abstract protected function prepare();

    abstract protected function filterActions($openingBal, $actions);

    public function getPrepared()
    {
        return $this->prepared;
    }

    public function actions()
    {
        return $this->getPrepared()->all_actions;
    }

    public function filteredActions()
    {
        $prepared = $this->getPrepared();

        return $this->filterActions($prepared->opening->balance, $prepared->all_actions);
    }

    abstract public function totalUnits();

    public function totalAmount()
    {
        return $this->totalUnits() * $this->price();
    }

    public function marketValue()
    {
        return $this->price() * $this->totalUnits();
    }

    abstract public function costValue();

    /**
     * @param Carbon|null $date
     * @return float|int|mixed
     * @throws \Exception
     */
    protected function fixedPrice(Carbon $date = null)
    {
        $key = 'fixed_price_for_fund_' . $this->fund->id;

        if (isset(static::$caches[$key])) {
            return static::$caches[$key];
        }

        if (cache()->has($key)) {
            $price = cache()->get($key);

            static::$caches[$key] = $price;

            return $price;
        }

        $price = $this->fund->unitPrice($date ? $date : $this->date);

        static::$caches[$key] = $price;

        cache([$key => $price], Carbon::now()->addHours(24));

        return $price;
    }
}
