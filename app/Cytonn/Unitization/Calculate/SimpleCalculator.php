<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Calculate;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Support\Collection;

class SimpleCalculator extends Calculator
{
    /**
     * @return object
     */
    protected function prepare()
    {
        $preparedActions = $this->prepareActions($this->getActions());

        $actions = $preparedActions->actions;

        $inPeriodActions = $this->getInperiodActions($actions);

        $openingBalance = $this->openingBalance($actions);

        $closingBalance = $this->closingBalance($inPeriodActions);

        return (object)[
            'opening_balance' => $openingBalance,
            'opening' => (object) [
                'date' => $this->start,
                'price' => $this->price($this->start),
                'balance' => $openingBalance
            ],
            'purchases' => $inPeriodActions->sum('purchase_amount'),
            'sales'  =>  $inPeriodActions->sum('sale_amount'),
            'net_interest' => $inPeriodActions->sum('net_interest'),
            'net_interest_after' => $preparedActions->net_interest_cf,
            'price'  => $this->price(),
            'actions' => $inPeriodActions,
            'all_actions' => $actions,
            'closing_balance' => $closingBalance,
            'total' => $closingBalance,
            'tax_exempt_actions' => null,
            'non_tax_exempt_actions' => null
        ];
    }

    /**
     * @param $actions
     * @return object
     */
    private function prepareActions($actions)
    {
        $principal = $netInterestCf = 0;
        $previousAction = null;

        $actions = $actions->map(function ($action) use (&$principal, &$netInterestCf, &$previousAction) {
            $purchaseAmount = $saleAmount = $interest = 0;

            $purchase = $sale = collect([]);

            if ($previousAction) {
                $taxPrincipal = $originaTaxPrincipal = $previousAction->closing_tax_exempt_total;
                $nonTaxPrincipal = $originalNonTaxPrincipal = $previousAction->closing_non_tax_exempt_total;
                $taxInterest = $previousAction->net_tax_exempt_interest_after;
                $nonTaxInterest = $previousAction->net_non_tax_exempt_interest_after;
            } else {
                $taxPrincipal = $originaTaxPrincipal = $nonTaxPrincipal = $originalNonTaxPrincipal = 0;
                $taxInterest = $nonTaxInterest = 0;
            }

            $contributionType = $this->getContributionType($action);
            $transactionType = $this->getTransactionType($action);
            $exemptionType = $this->getExemptionType($action);

            if ($action instanceof UnitFundPurchase) {
                if ($transactionType == 'interest') {
                    $interest = $action->number * $action->price;
                } else {
                    $purchaseAmount = $action->number * $action->price;
                }

                $purchase = collect([$action]);
            } elseif ($action instanceof UnitFundSale) {
                $saleAmount = $action->number * $action->price;
                $sale = collect([$action]);
            } else {
                throw new ClientInvestmentException("Unknown action type " . get_class($action));
            }

            $original = $principal;

            $principal = $principal + $purchaseAmount + $interest - $saleAmount;

            if ($exemptionType == 'tax_exempt') {
                $taxPrincipal = $taxPrincipal + $purchaseAmount + $interest - $saleAmount;
                $taxInterest += $interest;
            } else {
                $nonTaxPrincipal = $nonTaxPrincipal + $purchaseAmount + $interest - $saleAmount;
                $nonTaxInterest += $interest;
            }

            $netInterestCf += $interest;

            $previousAction = (object)[
                'date' => $action->date,
                'opening_balance' => $original,
                'opening_tax_exempt_balance' => $originaTaxPrincipal,
                'opening_non_tax_exempt_balance' => $originalNonTaxPrincipal,
                'principal' => $original,
                'tax_exempt_principal' => $originaTaxPrincipal,
                'non_tax_exempt_principal' => $originalNonTaxPrincipal,
                'purchases' => $purchase,
                'purchase_amount' => $purchaseAmount,
                'sales' => $sale,
                'sale_amount' => $saleAmount,
                'transaction_type' => $transactionType,
                'contribution_type' => $contributionType,
                'exemption_type' => $exemptionType,
                'gross_interest' => $interest,
                'withholding_tax' => 0,
                'net_interest' => $interest,
                'net_interest_after' => $netInterestCf,
                'net_tax_exempt_interest_after' => $taxInterest,
                'net_non_tax_exempt_interest_after' => $nonTaxInterest,
                'closing_principal' => $principal,
                'closing_tax_exempt_principal' => $taxPrincipal,
                'closing_non_tax_exempt_principal' => $nonTaxPrincipal,
                'total' => $principal,
                'closing_tax_exempt_total' => $taxPrincipal,
                'closing_non_tax_exempt_total' => $nonTaxPrincipal,
            ];

            return $previousAction;
        });

        return (object) [
            'actions' => $actions,
            'net_interest_cf' => $netInterestCf
        ];
    }

    /**
     * @return mixed
     */
    private function getActions()
    {
        $purchases = $this->purchases();

        $sales = $this->sales();

        return collect([])->merge($purchases)->merge($sales)->sortByDate('date', "ASC")->values();
    }

    /**
     * @param $openingBal
     * @param $actions
     * @return mixed
     */
    protected function filterActions($openingBal, $actions)
    {
        $exemptedActions = $this->getTaxExemptActions();

        $nonTaxExemptActions = $this->getNonTaxExemptActions();

        $actionsArray = array();

        $employerContribution = $employeeContribution = $avc = $transferIn = $withdrawal = 0;

        foreach ($exemptedActions as $exemptedAction) {
            if ($exemptedAction->transaction_type == 'contribution') {
                if ($exemptedAction->contribution_type == 'employer') {
                    $employerContribution += $exemptedAction->purchase_amount;
                } elseif ($exemptedAction->contribution_type == 'employer_avc' || $exemptedAction->contribution_type == 'employee_avc') {
                    $avc += $exemptedAction->purchase_amount;
                } else {
                    $employeeContribution += $exemptedAction->purchase_amount;
                }
            } elseif ($exemptedAction->transaction_type == 'transfer_in') {
                $transferIn += $exemptedAction->purchase_amount;
            } elseif ($exemptedAction->transaction_type == 'withdrawal') {
                $withdrawal += $exemptedAction->sale_amount;
            }

            $exemptedAction = (array) $exemptedAction;
            $exemptedAction['employer_contribution'] = $employerContribution;
            $exemptedAction['employee_contribution'] = $employeeContribution;
            $exemptedAction['avc'] = $avc;
            $exemptedAction['transfer_in'] = $transferIn;
            $exemptedAction['withdrawal'] = $withdrawal;

            $actionsArray[] = (object)[
                'tax_exempt_action' => (object) $exemptedAction,
                'non_tax_exempt_action' => null
            ];
        }

        $count = 0;

        $employerContribution = $employeeContribution = $avc = $transferIn = $withdrawal = 0;

        foreach ($nonTaxExemptActions as $exemptedAction) {
            if ($exemptedAction->transaction_type == 'contribution') {
                if ($exemptedAction->contribution_type == 'employer') {
                    $employerContribution += $exemptedAction->purchase_amount;
                } elseif ($exemptedAction->contribution_type == 'employer_avc' || $exemptedAction->contribution_type == 'employee_avc') {
                    $avc += $exemptedAction->purchase_amount;
                } else {
                    $employeeContribution += $exemptedAction->purchase_amount;
                }
            } elseif ($exemptedAction->transaction_type == 'transfer_in') {
                $transferIn += $exemptedAction->purchase_amount;
            } elseif ($exemptedAction->transaction_type == 'withdrawal') {
                $withdrawal += $exemptedAction->sale_amount;
            }

            $exemptedAction = (array) $exemptedAction;
            $exemptedAction['employer_contribution'] = $employerContribution;
            $exemptedAction['employee_contribution'] = $employeeContribution;
            $exemptedAction['avc'] = $avc;
            $exemptedAction['transfer_in'] = $transferIn;
            $exemptedAction['withdrawal'] = $withdrawal;

            if (array_key_exists($count, $actionsArray)) {
                $actionsArray[$count]->non_tax_exempt_action = (object) $exemptedAction;
            } else {
                $actionsArray[] = (object)[
                    'tax_exempt_action' => null,
                    'non_tax_exempt_action' => (object) $exemptedAction
                ];
            }

            $count ++;
        }

        return collect($actionsArray);
    }

    /**
     * @return mixed
     */
    public function getTaxExemptActions()
    {
        if (is_null($this->prepared->tax_exempt_actions)) {
            $this->prepared->tax_exempt_actions = $this->getExemptionTypeActions(
                $this->prepared->actions,
                'tax_exempt'
            );
        }

        return $this->prepared->tax_exempt_actions;
    }

    /**
     * @return mixed
     */
    public function getNonTaxExemptActions()
    {
        if (is_null($this->prepared->non_tax_exempt_actions)) {
            $this->prepared->non_tax_exempt_actions = $this->getExemptionTypeActions(
                $this->prepared->actions,
                'non_tax_exempt'
            );
        }

        return $this->prepared->non_tax_exempt_actions;
    }

    /**
     * @param Collection $actions
     * @return Collection
     */
    private function getInperiodActions(Collection $actions)
    {
        return $actions->filter(function ($day) {
            return $day->date->gte($this->start) && $day->date->lte($this->date);
        });
    }

    /**
     * @param Collection $actions
     * @param $type
     * @return Collection
     */
    private function getExemptionTypeActions(Collection $actions, $type)
    {
        return $actions->filter(function ($action) use ($type) {
            return $action->exemption_type == $type;
        });
    }

    /**
     * @param Collection $actions
     * @return int
     */
    private function openingBalance(Collection $actions)
    {
        $before = $actions->filter(function ($day) {
            return $day->date->lt($this->start);
        });

        $last = $before->last();

        return $last ? $last->total : 0;
    }

    /**
     * @param Collection $actions
     * @return int
     */
    private function closingBalance(Collection $actions)
    {
        $lastAction = $actions->last();

        return $lastAction ? $lastAction->total : 0;
    }

    /**
     * @return mixed
     */
    public function totalUnits()
    {
        return $this->prepared->total;
    }

    /**
     * @return mixed
     */
    public function costValue()
    {
        $principal = $this->prepared->purchases;

        $netSales = $this->prepared->sales - $this->prepared->net_interest;

        return $netSales > 0 ? $principal - $netSales : $principal;
    }

    /**
     * @return mixed
     */
    public function totalPurchases()
    {
        return $this->prepared->purchases;
    }

    /**
     * @return mixed
     */
    public function totalSales()
    {
        return $this->prepared->sales;
    }

    /**
     * @return mixed
     */
    public function totalInterest()
    {
        return $this->prepared->net_interest;
    }

    /**
     * @param null $date
     * @return float|int|mixed
     */
    public function price($date = null)
    {
        return $this->fixedPrice($date);
    }

    /**
     * @param $action
     * @return string
     */
    private function getTransactionType($action)
    {
        return str_slug($action->transactionType->name, '_');
    }

    /**
     * @param $action
     * @return string
     */
    private function getContributionType($action)
    {
        return str_slug($action->contributionType->name, '_');
    }

    /**
     * @param $action
     * @return string
     */
    private function getExemptionType($action)
    {
        return str_slug($action->exemptionType->name, '_');
    }
}
