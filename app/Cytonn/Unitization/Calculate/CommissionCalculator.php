<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Unitization\Calculate;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Carbon\Carbon;

class CommissionCalculator
{
    protected $purchase;
    protected $date;
    protected $start;
    protected $prepared;
    protected $commission;

    /**
     * CommissionCalculator constructor.
     * @param UnitFundPurchase $purchase
     * @param Carbon $date
     * @param Carbon|null $start
     */
    public function __construct(UnitFundPurchase $purchase, Carbon $date, Carbon $start = null)
    {
        $this->purchase = $purchase;
        $this->start = $start ? $start : Carbon::parse($purchase->date);
        $this->date = $date <= $this->start ? $this->start->copy() : $date;
        $this->commission = $this->purchase->unitFundCommission;
        $this->price = $this->purchase->price;

        $this->prepared = $this->prepare();
    }

    /**
     * @return object
     */
    private function prepare()
    {
        $actions = $this->getActions();

        $startDate = $this->limitStart($this->start);
        $startingNumber = $this->balance($startDate);
        $netCommissionCF = 0;

        $actions = $actions->map(function ($action) use (&$startDate, &$startingNumber, &$netCommissionCF) {
            $data = array();

            $data['starting_number'] = $startingNumber;
            $data['starting_value'] = $startingNumber * $this->price ;

            $endDate = Carbon::parse($action->date);

            $commission = $this->getCommission($data['starting_value'], $startDate, $endDate);
            $netCommissionCF += $commission;

            $data['date'] = $endDate->copy();
            $data['net_commission'] = $commission;
            $data['net_commission_cumulative'] = $netCommissionCF;

            $startingNumber -= $action->number;
            $data['closing_number'] = $startingNumber;
            $data['closing_value'] = $startingNumber * $this->price;

            $data['action'] = $action;

            $startDate = $endDate;

            return (object) $data;
        });

        $data = array();

        $data['starting_number'] = $startingNumber;
        $data['starting_value'] = $startingNumber * $this->price ;

        $commission = $this->getCommission($data['starting_value'], $startDate, $this->date);
        $netCommissionCF += $commission;

        $data['date'] = $this->date->copy();
        $data['net_commission'] = $commission;
        $data['net_commission_cumulative'] = $netCommissionCF;

        $data['closing_number'] = $startingNumber;
        $data['closing_value'] = $startingNumber * $this->price;
        $paidCommission = $this->getPaidCommissions();
        $unpaidCommission = $netCommissionCF - $paidCommission;

        $data['paid_commission'] = $paidCommission;
        $data['unpaid_commission'] = $unpaidCommission;

        return (object) [
            'actions' => $actions->all(),
            'total' => (object) $data
        ];
    }

    /**
     * @return mixed
     */
    private function getActions()
    {
        return $this->purchase->purchaseSales()
            ->nonFees()
            ->before($this->date)
            ->get();
    }

    /**
     * @param $amount
     * @param Carbon $start
     * @param Carbon $end
     * @return float|int
     */
    private function getCommission($amount, Carbon $start, Carbon $end)
    {
        $end = $this->limitEnd($end->copy());

        $tenor = $end->diffInDays($start);

        return $amount * $this->commission->rate * $tenor / (100 * 365);
    }

    /**
     * @param Carbon $startDate
     * @return mixed
     */
    private function balance(Carbon $startDate)
    {
        $sales = $this->purchase->purchaseSales()->nonFees()->where('date', '<', $startDate)->sum('number');

        return $this->purchase->number - $sales;
    }

    /**
     * @param Carbon $end
     * @return Carbon
     */
    private function limitEnd(Carbon $end)
    {
        if ($this->purchase->sale_date) {
            $saleDate = Carbon::parse($this->purchase->sale_date);

            if ($saleDate <= $end) {
                return $saleDate;
            }
        }

        return $end;
    }

    /**
     * @param Carbon $start
     * @return Carbon
     */
    private function limitStart(Carbon $start)
    {
        $startDate = Carbon::parse($this->purchase->date);

        if ($start < $startDate) {
            return $startDate;
        }

        return $start;
    }

    /**
     * @return mixed
     */
    private function getPaidCommissions()
    {
        return $this->commission->schedules()->before($this->date)->sum('amount');
    }

    /**
     * @return mixed
     */
    public function paidCommissions()
    {
        return $this->prepared->total->paid_commission;
    }

    /**
     * @return mixed
     */
    public function unpaidCommission()
    {
        return $this->prepared->total->unpaid_commission;
    }
}
