<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 8/6/18
 * Time: 9:46 AM
 */

namespace App\Cytonn\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundBusinessConfirmation;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundClientSummaryTransfomer;
use Cytonn\Api\Transformers\Unitization\UnitFundInvestmentInstructionTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundPurchaseTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundSaleTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundTransferTransformer;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Mailers\Unitization\StatementMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Unitization\Reporting\StatementGenerator;
use Cytonn\Unitization\Trading\Sell;

class UnitFundClientRepository
{
    use AlternateSortFilterPaginateTrait;

    protected $client;

    protected $unitFund;

    protected $date;

    protected $start_date;

    public function __construct(Client $client, UnitFund $unitFund, $date = null, $start = null)
    {
        $this->client = $client;

        $this->unitFund = $unitFund;

        $this->date = ($date) ? Carbon::parse($date) : Carbon::today();

        $this->start_date = $start;
    }

    public function fundDetails()
    {
        $clientDetails = (new UnitFundClientSummaryTransfomer())->transform($this->client, $this->unitFund);

        return [
            'details' => $clientDetails,
            'purchases' => $this->fundPurchases($this->unitFund->id, $this->client->id),
            'sales' => $this->fundSales($this->unitFund->id, $this->client->id),
            'instructions' => $this->pendingInstructions($this->unitFund->id, $this->client->id),
            'transfers_given' => $this->fundTransfersGiven($this->unitFund->id, $this->client->id),
            'transfers_received' => $this->fundTransfersReceived($this->unitFund->id, $this->client->id),
        ];
    }

    public function totalAmountInvested()
    {
        return $this->fundPurchases()->sum('amountBeforeInvestment');
    }

    public function netInvestmentAmount()
    {
        return $this->fundPurchases()->sum(function ($purchase) {

            return $purchase['number'] * $purchase['unitPrice'];
        });
    }

    public function uninvestedAmount()
    {
        return UnitFundPurchase::where('unit_fund_id', $this->unitFund->id)
            ->where('client_id', $this->client->id)
            ->where('date','>', Carbon::now())
            ->sum('number');
    }

    public function fundPurchases()
    {
        return UnitFundPurchase::where('unit_fund_id', $this->unitFund->id)
            ->where('client_id', $this->client->id)
            ->latest()
            ->get()
            ->map(function ($purchase) {
                return (new UnitFundPurchaseTransformer())->transform($purchase);
            });
    }

    public function fundSales()
    {
        return UnitFundSale::where('unit_fund_id', $this->unitFund->id)
            ->where('client_id', $this->client->id)
            ->latest()
            ->get()
            ->each(function ($sale) {
                return (new UnitFundSaleTransformer())->transform($sale);
            });
    }

    public function fundTransfersGiven($fund_id, $client_id)
    {
        return UnitFundTransfer::where('unit_fund_id', $fund_id)
            ->where('transferer_id', $client_id)
            ->latest()
            ->get()
            ->map(function ($transfer) {
                return (new UnitFundTransferTransformer())->transform($transfer);
            });
    }

    public function fundTransfersReceived($fund_id, $client_id)
    {
        return UnitFundTransfer::where('unit_fund_id', $fund_id)
            ->where('transferee_id', $client_id)
            ->latest()
            ->get()
            ->map(function ($transfer) {
                return (new UnitFundTransferTransformer())->transform($transfer);
            });
    }

    public function downloadStatement()
    {
        $generator = new StatementGenerator();

        return $generator->generate(
            $this->client,
            $this->unitFund,
            $this->date->copy(),
            null,
            $this->start_date
        )->download();
    }

    public function excelStatement()
    {
        $generator = new StatementGenerator();

        return $generator->excel(
            $this->client,
            $this->unitFund,
            $this->date->copy(),
            null,
            $this->start_date
        )->download();
    }

    public function previewStatement($protect = true)
    {
        $generator = new StatementGenerator();

        $generator->passwordProtect($protect);

        return $generator->generate(
            $this->client,
            $this->unitFund,
            $this->date->copy(),
            auth()->user(),
            $this->start_date
        )->stream();
    }

    public function sendStatement()
    {
        $mailer = new StatementMailer();

        $unitFund = $this->unitFund;

        $statements[ $unitFund->manager->name .'- ' .
        ClientPresenter::presentJointFirstNameLastName($this->client->id) . '.pdf'] =
            (new StatementGenerator())->passwordProtect(true)
                ->generate(
                    $this->client,
                    $this->unitFund,
                    $this->date->copy(),
                    auth()->user(),
                    $this->start_date
                )->output();

        $mailer->sendStatementToClient(
            $this->client,
            $statements,
            $this->date->copy(),
            $unitFund,
            auth()->user()->id
        );

        return response(['sent' => true], 202);
    }

    public function loadStatement()
    {
        return (new StatementGenerator())
            ->html(
                $this->client,
                $this->unitFund,
                $this->date->copy(),
                null,
                $this->start_date
            );
    }

    public function balance()
    {
        return [
            'fetched' => true,
            'balance' => $this->client->fundPaymentBalance($this->unitFund, $this->date->copy())
        ];
    }

    public function ownedNumberOfUnits()
    {
        $unitsOwned = $this->client->repo->ownedNumberOfUnits($this->unitFund, $this->date->copy());

        $price = $this->unitFund->unitPrice($this->date->copy());

        $amount = $unitsOwned * $price;

        return [
            'ownedUnits' => $unitsOwned,
            'kycValidated' => $this->client->repo->checkClientKycValidated(),
            'price' => $price,
            'amount' => $amount,
            'withdrawable' => (new Sell($this->unitFund, $this->client, $this->date))->withdrawableUnits(),
            'fetched' => true,
        ];
    }

    public function buyBC($id)
    {
        $purchase = UnitFundPurchase::find($id);

        $confirmation = UnitFundBusinessConfirmation::buy($purchase);

        return response(['sent' => true, 'data' => $confirmation], 202);
    }

    public function sellBC($id)
    {
        $sale = UnitFundSale::find($id);

        $confirmation = UnitFundBusinessConfirmation::sell($sale);

        return response([ 'sent' => true, 'data' => $confirmation ], 202);
    }

    public function pendingInstructions($fund_id, $client_id)
    {
        return UnitFundInvestmentInstruction::whereNull('approval_id')
            ->where('client_id', $client_id)
            ->where('unit_fund_id', $fund_id)
            ->latest()
            ->get()
            ->map(function ($instruction) {
                return (new UnitFundInvestmentInstructionTransformer())->transform($instruction);
            });
    }
}
