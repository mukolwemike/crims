<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/8/18
 * Time: 9:20 AM
 */

namespace App\Cytonn\Unitization;

use App\Cytonn\Custodial\Transact\UnitFundTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeePayment;
use App\Cytonn\Models\Unitization\UnitFundFeeType;

class UnitFundFeePaymentRepository
{
    public function make(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $payment = UnitFundFeePayment::create([
            'fee_id' => $data['fee_id'],
            'amount' => $data['amount'],
            'date' => $data['date'],
            'unit_fund_id' => $data['unit_fund_id'],
            'recipient_id' => $data['recipient_id'],
            'approval_id' => $approval->id
        ]);

        $this->credit($payment, $approval);

        return $payment;
    }

    public function credit(UnitFundFeePayment $payment, ClientTransactionApproval $approval = null)
    {
        $fee = UnitFundFee::findOrFail($payment->fee_id);

        $feeType = UnitFundFeeType::findOrFail($fee->unit_fund_fee_type_id);

        $fund = UnitFund::findOrFail($payment->unit_fund_id);

        $custodialAccount = CustodialAccount::findOrFail($fund->custodial_account_id);

        $transaction = UnitFundTransaction::build(
            $custodialAccount,
            'EUF',
            $payment->date,
            "Payment of {$feeType->name} fee from {$fund->name}",
            $payment->amount,
            $approval,
            null
        )->credit();

        $payment->custodialTransaction()->associate($transaction);
        $payment->save();

        return $transaction;
    }
}
