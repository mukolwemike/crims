<?php

namespace Cytonn\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCommissionPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\Unitization\UnitFundSwitch;
use App\Cytonn\Models\Unitization\UnitFundSwitchRate;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Custodial\Transact\ClientTransaction;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Unitization\Trading\Buy;
use Cytonn\Unitization\Trading\Sell;
use Cytonn\Unitization\Trading\Trade;

class FundSwitch extends Trade
{
    protected $client;

    protected $date;

    protected $oldFund;

    protected $newFund;

    public function __construct(Client $client, UnitFund $oldFund, Carbon $date)
    {
        $this->client = $client;

        $this->oldFund = $oldFund;

        $this->date = $date;

        parent::__construct($oldFund, $date);
    }

    public function to(UnitFund $fund)
    {
        $this->newFund = $fund;

        return $this;
    }


    /**
     * @return UnitFundSwitch
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function switch()
    {
        $number = $this->client->ownedNumberOfUnits($this->fund, $this->date);

        //deduct fees
        $fees = $this->chargeFees($number);

        $deducted = $fees->sum(function ($fee) {
            return $fee->deduction->number;
        });

        $remaining = $number - $deducted;

        $sale = $this->sell($remaining);

        //transfer cash
        $transferred = $this->transferCash($sale);

        //purchase
        $purchase = $this->buy($transferred);

        return $this->recordSwitch($sale, $purchase);
    }

    public function sell($number)
    {
        return (new Sell($this->oldFund, $this->client, $this->date->copy()))
            ->deductUnits($number, ['narration' => 'Tranfer out (Switch)']);
    }

    /**
     * @param UnitFundSale $sale
     * @return float|int
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    protected function transferCash(UnitFundSale $sale)
    {
        $amount = $this->price() * $sale->number;

        $this->credit($amount);
        $this->debit($amount);

        return $amount;
    }

    public function recordSwitch(UnitFundSale $sale, UnitFundPurchase $purchase)
    {
        $switch = new UnitFundSwitch([
            'date' => $this->date->copy()
        ]);

        $switch->client()->associate($this->client);

        $switch->previousFund()->associate($this->oldFund);

        $switch->currentFund()->associate($this->newFund);

        $switch->sale()->associate($sale);

        $switch->purchase()->associate($purchase);

        $switch->save();

        return $switch;
    }

    /**
     * @param $units
     * @return mixed
     */
    protected function chargeFees($units)
    {
        return $this->calculateFees($units)->map(function (UnitFundFee $fee) use ($units) {
            $deduction = (new Sell($this->fund, $this->client, $this->date))
                ->deductUnits($fee->fee_units, ['narration' => 'Fee: '.$fee->type->name], false);

            $charge = $this->recordFee($fee, $fee->fee_units);

            $charge->deduction()->associate($deduction);
            $charge->save();

            return $charge;
        });
    }

    public function calculateFees($units)
    {
        return $this->fund->switchFees->map(function (UnitFundFee $fee) use ($units) {
            $feeUnits = $this->feeValue($fee, $units);

            $fee->fee_units = $feeUnits;
            return $fee;
        });
    }

    public function buy($amount)
    {
        $recipient = CommissionRecepient::find(1);

//        $this->client->getLatestFA();

        $purchase = (new Buy(
            $this->client,
            $recipient,
            $amount,
            $this->date->copy(),
            null,
            true,
            $this->newFund
        ));

        return $purchase->transferUnits($amount, "Transfer In (Switch)");
    }

    private function transaction(
        CustodialAccount $account,
        $type,
        $amount
    ) {
        return ClientTransaction::build(
            $account,
            $type,
            $this->date,
            'Fund switch for '.ClientPresenter::presentJointFullNames($this->client->id),
            $amount,
            null,
            [],
            $this->client,
            null,
            null
        );
    }

    private function payment(
        CustodialTransaction $transaction,
        $type,
        $description,
        UnitFund $fund
    ) {
        return Payment::make(
            $this->client,
            null,
            $transaction,
            null,
            null,
            null,
            $this->date,
            abs($transaction->amount),
            $type,
            $description,
            $fund
        );
    }

    /**
     * @param $amount
     * @return \App\Cytonn\Models\ClientPayment
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    protected function credit($amount)
    {
        $trans = $this->transaction($this->fund->custodialAccount, 'TO', $amount);

        $t =  $trans->credit();

        return $this->payment(
            $t,
            'TO',
            'Transfer to '.$this->newFund->name,
            $this->oldFund
        )->credit();
    }

    /**
     * @param $amount
     * @return \App\Cytonn\Models\ClientPayment
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    protected function debit($amount)
    {
        $trans = $this->transaction($this->newFund->custodialAccount, 'TI', $amount);

        $t =  $trans->debit();

        return $this->payment(
            $t,
            'TI',
            'Transfer from '.$this->oldFund->name,
            $this->newFund
        )->debit();
    }
}
