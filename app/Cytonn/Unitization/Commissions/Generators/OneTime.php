<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Commissions\Generators;

use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use App\Cytonn\Models\Unitization\UnitFundPurchase;

class OneTime
{
    /**
     * @param UnitFundPurchase $purchase
     * @return UnitFundCommissionSchedule|\Illuminate\Database\Eloquent\Model
     */
    public function generate(UnitFundPurchase $purchase)
    {
        $amount = $this->totalCommission($purchase);

        return UnitFundCommissionSchedule::create([
            'unit_fund_commission_id' => $purchase->unitFundCommission->id,
            'date' => $purchase->date,
            'amount' => $amount,
            'description' => 'One Time Full Commission Payment',
        ]);
    }

    /**
     * @param UnitFundPurchase $purchase
     * @return float|int
     */
    protected function totalCommission(UnitFundPurchase $purchase)
    {
        return $purchase->number * $purchase->price * $purchase->unitFundCommission->rate / 100;
    }
}
