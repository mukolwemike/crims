<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Unitization\Commissions\Generators;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Cytonn\Unitization\Commissions\Generators\OneTime;

class Generator
{
    const ONE_TIME_FUNDS = ['CID'];

    /**
     * @param UnitFundPurchase $purchase
     * @return \App\Cytonn\Models\Unitization\UnitFundCommissionSchedule|\Illuminate\Database\Eloquent\Model|void
     */
    public function createSchedules(UnitFundPurchase $purchase)
    {
        if ($purchase->unitFund->category->slug != 'pension-fund') {
            return;
        }

        if (!in_array($purchase->unitFund->short_name, static::ONE_TIME_FUNDS)) {
            return;
        }

        $purchase->unitFundCommission->schedules->each(function ($schedule) {
            $schedule->delete();
        });

        return (new OneTime())->generate($purchase);
    }
}
