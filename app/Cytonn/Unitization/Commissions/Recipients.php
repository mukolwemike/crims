<?php

namespace Cytonn\Unitization\Commissions;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;

class Recipients
{
    public function forDates(Carbon $start, Carbon $end)
    {
        return (new CommissionRecepient())
            ->whereHas(
                'unitFundCommissionPaymentSchedules',
                function ($schedule) use ($start, $end) {
                    $schedule->where('date', '>=', $start)->where('date', '<=', $end);
                }
            );
    }
}
