<?php

namespace Cytonn\Unitization\Commissions;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFundCommissionPayment;
use Carbon\Carbon;
use Crims\Unitization\Commission\RecipientCalculator;

class Payment
{
    protected $recipient;

    public function __construct(CommissionRecepient $recipient)
    {
        $this->recipient = $recipient;
    }

    public function make(BulkCommissionPayment $bcp, ClientTransactionApproval $approval)
    {
        $calculator = new RecipientCalculator($this->recipient, $bcp->start, $bcp->end);

        $summary = $calculator->summary();

        $override = $summary->override();

        $total = $summary->total();

        $finalComm = $total + $override;

        if ($total == 0 && $override == 0) {
            return null;
        }

        return UnitFundCommissionPayment::create([
            'date' => Carbon::parse($bcp->end),
            'amount' => $finalComm,
            'overrides' => $override,
            'description' => 'Commission Payment ' . $bcp->start->toFormattedDateString() . ' - ' .
                $bcp->end->toFormattedDateString(),
            'commission_recipient_id' => $this->recipient->id,
            'client_transaction_approval_id' => $approval->id
        ]);
    }
}
