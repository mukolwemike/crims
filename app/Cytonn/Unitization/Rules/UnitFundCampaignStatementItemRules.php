<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundCampaignStatementItemRules
{
    use Rules;

    public function itemCreate($request)
    {
        $rules = [
            'unit_fund_statement_campaign_id' => 'required|numeric',
            'client_id' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    public function itemUpdate($request, $id)
    {
        $rules = [
            'unit_fund_statement_campaign_id' => 'required|numeric',
            'client_id' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
