<?php

namespace Cytonn\Unitization\Rules;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use Carbon\Carbon;
use Cytonn\Rules\Rules;

trait UnitFundSaleRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund sale creation
     */
    public function saleCreate($request, Client $client, UnitFund $fund)
    {
        $max  = 0.01 + $client->ownedNumberOfUnits($fund, $request->date);

        $rules = [
            'date'      =>  'required|date',
            'number'    =>  'required|numeric|max:' .$max
        ];

        $messages = [
            'date.required'      =>  'Date Field is required.',
            'date.date'      =>  'Provide correct date format.',
            'number.required'    =>  'Specify number of units to sell before proceeding.',
            'number.numeric'    =>  'The value you provided is invalid. Please provide numeric values only',
            'number.max'    =>  'Number of units must not exceed owned units. i.e. 
            ('.$client->ownedNumberOfUnits($fund, $request->date).')'
        ];

        return $this->verdict($request, $rules, $messages);
    }

    public function reverseSaleMake($request)
    {
        $rules = [
            'sale_id' => 'required|numeric',
            'reason' => 'required|min:6',
        ];

        return $this->verdict($request, $rules);
    }
}
