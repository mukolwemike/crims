<?php

namespace Cytonn\Unitization\Rules;

use App\Cytonn\Models\Unitization\UnitFundFeeChargeType;
use Cytonn\Rules\Rules;

trait UnitFundFeeRules
{
    use Rules;

    public function feeCreate($request)
    {
        $type = UnitFundFeeChargeType::findOrFail($request->fee_charge_type_id)->slug;

        $rules = [
            'unit_fund_fee_type_id' => 'required|numeric',
            'date' => 'required|date',
            'fee_charge_type_id' => 'required|numeric'
        ];

        if ($type == 'recurrent') {
            $rules = $rules + [
                    'fee_charge_frequency_id' => 'required'
                ];
        }

        if ($type == 'once') {
            $rules = $rules + [
                    'charged_at' => 'required'
                ];
        }

        $rules = $this->validateSomeMore($request, $rules);

        $messages = [
            'is_amount.required' => 'Fee calculation type is required',
            'percentage.required' => 'Fee value is required',
            'percentage.numeric' => 'Fee value should be a number',
        ];

        return $this->verdict($request, $rules, $messages);
    }

    public function feeUpdate($request, $id)
    {
        $rules = [
            'unit_fund_fee_type_id' => 'required|numeric',
            'date' => 'required|date',
            'fee_charge_type_id' => 'required|numeric',
            'fee_charge_frequency_id' => 'numeric|required_if:fee_charge_type_id, == 2',
        ];

        $rules = $this->validateSomeMore($request, $rules);

        return $this->verdict($request, $rules);
    }

    public function feePercetage($request)
    {
        $rules = [
            'percentage' => 'required|numeric',
            'unit_fund_fee_id' => 'required|numeric',
            'unit_fund_fee_parameter_id' => 'required|numeric',
            'date' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    private function validateSomeMore($request, $rules)
    {
        return $request->dependent ? $this->isDependent($rules) : $this->isIndependent($rules);
    }

    private function isDependent($rules)
    {
        $rules['unit_fund_fee_parameter_id'] = 'required | numeric';
        $rules['percentage'] = 'required | numeric';
        $rules['is_amount'] = 'required | numeric';

        return $rules;
    }

    private function isIndependent($rules)
    {
        $rules['amount'] = 'required | numeric';

        return $rules;
    }
}
