<?php

namespace Cytonn\Unitization\Rules;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Rules\Rules;

trait UnitFundTransferRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund transfer creation
     */
    /**
     * @param $request
     * @param Client $client
     * @param UnitFund $fund
     * @return \Illuminate\Validation\Validator
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function transferCreate($request, Client $client, UnitFund $fund)
    {
        $rules = [
            'transferee_id' =>  'required',
            'date'          =>  'required|date',
            'number'        =>  'required|numeric|max:' . $client->repo->ownedNumberOfUnits($fund, $request->date),
        ];

        return $this->verdict($request, $rules);
    }

    public function reverseTransferMake($request)
    {
        $rules = [
        'transfer_id'       =>  'required|numeric',
        'reason'            =>  'required|min:6',
        ];

        return $this->verdict($request, $rules);
    }
}
