<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundSwitchRateRules
{
    use Rules;

    public function switchRateCreate($request)
    {
        $rules = [
            'date'      =>  'required|date',
            'amount'    =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    public function switchRateUpdate($request, $id)
    {
        $rules = [
            'date'      =>  'required|date',
            'amount'    =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
