<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundFeeParameterRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund fee parameter creation
     */
    public function feeParameterCreate($request)
    {
        $rules = [
            'name'  =>  'required|string|min:3|unique:unit_fund_fee_parameters',
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate unit fund fee parameter update
     */
    public function feeParameterUpdate($request, $id)
    {
        $rules = [
            'name'  =>  'required|string|min:3'
        ];

        return $this->verdict($request, $rules);
    }
}
