<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/7/18
 * Time: 3:45 PM
 */

namespace App\Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundFeePaymentRules
{
    use Rules;

    public function feePayment($request)
    {
        $rules = [
            'fee_id' => 'required|numeric',
            'recipient_id' => 'required|numeric',
            'date' =>  'required|date',
            'amount' =>  'required|numeric',
            'unit_fund_id' =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
