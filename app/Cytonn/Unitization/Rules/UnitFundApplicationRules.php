<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/12/17
 * Time: 8:12 AM
 */

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundApplicationRules
{
    use Rules;
    
    /**
     * @param $request
     * @return mixed
     */
    public function individualApplicationCreate($request)
    {
        $rules = [
            'unit_fund_id' =>  'required',
            'amount' => 'required|numeric',
        //            'payment_method' => 'required',
            
            'title_id' => 'required',
            'lastname' => 'required|string',
        //            'middlename' => '',
            'firstname' => 'required|string',
            'preferredname' => 'required|string',
            'gender_id' => 'required',
        //            'date_of_birth' => 'required',
            'pin_no' => 'required',
            'taxable' => 'required',
            'id_or_passport' => 'required',
            'email' => 'required|email',
        //            'postal_address' => '',
        //            'postal_code' => '',
            'street' => 'string',
            'town' => 'required|string',
        //            'country_id' => '',
            'phone' => 'required|string',
        //            'telephone_office' => '',
        //            'telephone_home' => '',
        //            'residential_address' => '',
            
        //            'employment_id' => '',
        //            'employment_other' => '',
        //            'present_occupation' => '',
        //            'business_sector' => '',
        //            'employer_name' => 'required|string',
        //            'employer_address' => '',
            'method_of_contact_id' => 'required',
            
            'fund_source_id' => 'required',
        //            'fund_source_other' => '',
            
            'investor_account_name' => 'required|string',
            'investor_account_number' => 'required',
            'client_bank_id' => 'required|numeric',
            'client_bank_branch_id' => 'required',
        ];
        
        return $this->verdict($request, $rules);
    }
    
    /**
     * @param $request
     * @return mixed
     */
    public function corporateApplicationCreate($request)
    {
        $rules = [
            'unit_fund_id' =>  'required',
            'amount' => 'required|numeric',
            'payment_method' => 'required',
    
            'investor_account_name' => 'required|string',
            'investor_account_number' => 'required',
            'client_bank_id' => 'required',
            'client_bank_branch_id' => 'required',
            
            'registered_name' => 'required',
            'trade_name' => 'required',
        //            'registered_address' => 'required',
            'registration_number' => 'required',
        //            'postal_address' => 'required',
        //            'postal_code' => 'required',
        //            'street' => 'required',
        //            'town' => 'required',
        //            'country_id' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        //            'residential_address' => '',
            'pin_no' => 'required',
            'taxable' => 'required',
            'method_of_contact_id' => 'required',
            
            'contact_person_title_id' => 'required',
            'contact_person_lname' => 'required',
            'contact_person_fname' => 'required',
            
            'fund_source_id' => 'required',
        //            'funds_source_other' => 'required',
            
            'corporate_investor_type_id' => 'required',
        //            'corporate_investor_type_other' => '',
        ];
        
        return $this->verdict($request, $rules);
    }

    public function existingApplicationCreate($request)
    {
        $rules = [
            'client_id' => 'required|numeric',
        //            'client_type' => 'required',
            'fund_source_id' => 'required|numeric',
            'amount' => 'required|numeric',
            'payment_method' => 'required',
            'unit_fund_id' => 'required|numeric',
            'application_type' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
