<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Models\Unitization\FundCategory;
use Cytonn\Rules\Rules;

trait UnitFundRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund creation
     */
    public function fundCreate($request)
    {
        $fundCategory = FundCategory::find($request->get('fund_category_id'));

        if ($fundCategory->slug == 'pension-fund') {
            return $this->pensionFund($request);
        }

        return $this->otherFunds($request);
    }

    public function pensionFund($request)
    {
        $rules = [
            'name'                      =>  'required|string|min:3|unique:unit_funds',
            'currency_id'               =>  'required|numeric',
            'fund_manager_id'           =>  'required|numeric',
            'custodial_account_id'      =>  'required|numeric',
            'contact_person'            =>  'required',
            'email'                     =>  'required|email'
        ];

        return $this->verdict($request, $rules);
    }

    public function otherFunds($request)
    {
        $rules = [
            'name'                      =>  'required|string|min:3|unique:unit_funds',
            'initial_unit_price'        =>  'required|numeric',
            'minimum_investment_amount' =>  'required|numeric',
            'fund_objectives'           =>  'required|string',
            'benefits'                  =>  'required|string',
            'minimum_investment_horizon'=>  'required|numeric',
            'currency_id'               =>  'required|numeric',
            'fund_manager_id'           =>  'required|numeric',
            'custodial_account_id'      =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate unit fund update
     */
    public function fundUpdate($request, $id)
    {
        $rules = [
            'name'                      =>  'required|string|min:3',
            'initial_unit_price'        =>  'required|numeric',
            'minimum_investment_amount' =>  'required|numeric',
            'fund_objectives'           =>  'required|string',
            'benefits'                  =>  'required|string',
            'minimum_investment_horizon'=>  'required|numeric',
            'currency_id'               =>  'required|numeric',
            'fund_manager_id'           =>  'required|numeric',
            'custodial_account_id'      =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
