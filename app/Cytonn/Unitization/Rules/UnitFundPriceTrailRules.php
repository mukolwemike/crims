<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundPriceTrailRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund price trail creation
     */
    public function priceTrailCreate($request)
    {
        $rules = [
            'date'      =>  'required|date',
            'price'     =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate unit fund price trail update
     */
    public function priceTrailUpdate($request, $id)
    {
        $rules = [
            'date'      =>  'required|date',
            'price'     =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
