<?php

namespace Cytonn\Unitization\Rules;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Rules\Rules;

trait UnitFundPurchaseRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund purchase creation
     */
    public function purchaseCreate($request, Client $client, UnitFund $fund)
    {
        $rules = [
            'date'      =>  'required|date',
            'amount'    =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    public function reversePurchaseMake($request)
    {
        $rules = [
            'purchase_id' => 'required|numeric',
            'reason' => 'required|min:6',
        ];

        return $this->verdict($request, $rules);
    }
}
