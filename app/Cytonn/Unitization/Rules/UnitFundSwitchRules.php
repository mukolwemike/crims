<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundSwitchRules
{
    use Rules;

    public function switchCreate($request)
    {
        $rules = [
            'unit_fund_id'  =>  'required|numeric',
            'date'          =>  'required|date',
        ];

        return $this->verdict($request, $rules);
    }

    public function switchUpdate($request, $id)
    {
        $rules = [
            'unit_fund_id'  =>  'required|numeric',
            'date'          =>  'required|date',
        ];

        return $this->verdict($request, $rules);
    }
}
