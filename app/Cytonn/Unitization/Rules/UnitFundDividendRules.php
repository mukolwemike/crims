<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundDividendRules
{
    use Rules;

    public function dividendCreate($request)
    {
        $rules = [
            'date'      =>  'required|date',
            'amount'    =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    public function dividendUpdate($request, $id)
    {
        $rules = [
            'date'      =>  'required|date',
            'amount'    =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
