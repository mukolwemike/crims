<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundFeeTypeRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund fee type creation
     */
    public function feeTypeCreate($request)
    {
        $rules = [
            'name'                      =>  'required|string|min:3|unique:unit_fund_fee_types',
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate unit fund fee type update
     */
    public function feeTypeUpdate($request, $id)
    {
        $rules = [
            'name'                      =>  'required|string|min:3'
        ];

        return $this->verdict($request, $rules);
    }
}
