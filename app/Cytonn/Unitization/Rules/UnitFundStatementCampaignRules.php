<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundStatementCampaignRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund statement campaign creation
     */
    public function campaignCreate($request)
    {
        $rules = [
            'name'          =>  'required|min:3|max:200',
            'type_id'       =>  'required|numeric',
            'send_date'     =>  'required|date',
            'unit_fund_id'  =>  'required'
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate unit fund statement campaign update
     */
    public function campaignUpdate($request, $id)
    {
        $rules = [
            'name'          =>  'required|min:3|max:200',
            'type_id'       =>  'required|numeric',
            'send_date'     =>  'required|date'
        ];

        return $this->verdict($request, $rules);
    }
}
