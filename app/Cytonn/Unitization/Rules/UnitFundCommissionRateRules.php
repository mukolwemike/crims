<?php

namespace Cytonn\Unitization\Rules;

use Cytonn\Rules\Rules;

trait UnitFundCommissionRateRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate unit fund commission rate creation
     */
    public function rateCreate($request)
    {
        $rules = [
            'date'                          =>  'required|date',
            'rate'                          =>  'required|numeric',
            'commission_recipient_type_id'  =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    /*
     * Validate unit fund commission rate update
     */
    public function rateUpdate($request, $id)
    {
        $rules = [
            'date'      =>  'required|date',
            'rate'      =>  'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
