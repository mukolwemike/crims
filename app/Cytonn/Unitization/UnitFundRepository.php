<?php

namespace Cytonn\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeePayment;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundPerformance;
use App\Cytonn\Portfolio\Summary\FundPricingSummary;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Unitization\Trading\Performance;
use Cytonn\Unitization\Trading\Trade;

class UnitFundRepository
{
    protected $unitFund;

    public function __construct(UnitFund $unitFund = null)
    {
        $this->unitFund = is_null($unitFund) ? new UnitFund() : $unitFund;
    }

    public function getUnits($date)
    {
        $purchases = $this->unitFund->purchases()->before($date)->sum('number');

        $sales = $this->unitFund->sales()->before($date)->sum('number');

        return $purchases - $sales;
    }

    public function unitFundFeePayments($data)
    {
        $feePayment = new UnitFundFeePayment();

        $feePayment->fill($data);
        $feePayment->save();

        return $feePayment;
    }

    public function switchFeeAsAt($date = null)
    {
        $date = Carbon::parse($date);

        return $this->unitFund
            ->switchFees()
            ->where('date', '<=', $date->copy())
            ->latest()
            ->first();
    }

    public function valueOfSwitchFeeAsAt(Client $client, $date = null)
    {
        $date = Carbon::parse($date);

        $fee = $this->switchFeeAsAt($date);

        if (is_null($fee)) {
            return $fee = 0;
        } elseif ($fee->dependent and $fee->percentages()->latest()->first()->parameter->slug == 'investment-amount') {
            return $fee->getPercentage() / 100 * $client->repo->valueOfUnitsOwnedAsAt($this->unitFund, $date->copy());
        } else {
            return $fee->getAmount();
        }
    }

    public function fundClients($date = null)
    {
        $date = Carbon::parse($date);

        return Client::whereHas('unitFundPurchases', function ($purchase) {
            $purchase
                ->whereNull('deleted_at')
                ->where('unit_fund_id', $this->unitFund->id);
        })
            ->get();
    }

    /**
     * @param Carbon $date
     * @param bool $checkPreviousDay
     * @throws ClientInvestmentException
     * @throws CrimsException
     */
    public function recordPerformance(Carbon $date, $checkPreviousDay = true)
    {
        $calculation = $this->unitFund->category->calculation->slug;

        if ($checkPreviousDay) {
            $this->checkPreviousDay($date);
        }

        if ($this->unitFund->performances()->where('date', $date)->exists()) {
            throw new ClientInvestmentException("Performance record already exists");
        }

        $performance = new Performance($this->unitFund, $date->copy());
        $pricing = new FundPricingSummary($this->unitFund, $date->copy());
        $totals = $pricing->summary()['totals'];

        $aum = $totals['aum'];

        $fundPerformance = new UnitFundPerformance([
            'unit_fund_id' => $this->unitFund->id,
            'date' => $date,
            'assets' => isset($totals['assets']) ? $totals['assets'] : $performance->assetMarketValue(),
            'cash' => $performance->cashBalance(),
            'aum' => $aum,
            'nav' => $performance->nav(),
            'liabilities' => $performance->liabilities(),
        ]);

        $fundPerformance->save();

        $previous = $fundPerformance->previous($date, $this->unitFund);

        switch ($calculation) {
            case 'variable-unit-price':
                $fundPerformance->total_units = $performance->totalUnits();
                $fundPerformance->price = $totals['price'];
                break;
            case 'daily-yield':
                $fundPerformance->total_units = $aum;
                $fundPerformance->price = $previous ? $previous->price : $this->unitFund->initial_unit_price;
                $fundPerformance->net_daily_yield = $totals['net_daily_yield'];
                $fundPerformance->gross_daily_yield = $totals['gross_daily_yield'];
                $fundPerformance->outperformance_yield  = $totals['outperformance_fee_percent'];

                if ((int)$fundPerformance->price !== 1) {
                    throw new CrimsException('Price for daily yield funds must be 1');
                }
                break;
            case 'simple-interest':
                $fundPerformance->total_units = $aum;
                $fundPerformance->price = $previous ? $previous->price : $this->unitFund->initial_unit_price;
                $fundPerformance->net_daily_yield = $totals['net_daily_yield'];
                $fundPerformance->gross_daily_yield = $totals['net_daily_yield'];
                break;
            default:
                $msg = "Fund {$this->unitFund->name} has an unsupported calculation method";
                throw new \InvalidArgumentException($msg);
                break;
        }

        $fundPerformance->fund()->associate($this->unitFund);
        $fundPerformance->save();

        $grossDailyYield = isset($totals['gross_daily_yield']) ? $totals['gross_daily_yield'] : null;

        $this->recordDailyFees($this->unitFund, $aum, $date, $grossDailyYield);
    }

    private function recordDailyFees(UnitFund $fund, $aum, Carbon $date, $grossYield)
    {
        $trade = new Trade($fund, $date->copy());

        $fees = $trade->todayFeesCharging($aum, null, $grossYield);

        $fees->each(function ($fee) use ($date, $fund) {
            $this->recordFee($fund, $fee->fee, $fee->amount, $date->copy());
        });
    }

    private function recordFee(UnitFund $fund, UnitFundFee $fee, $amount, Carbon $date)
    {
        $feesCharged = new UnitFundFeesCharged([
            'amount' => $amount,
            'date' => $date->copy()
        ]);

        $feesCharged->fund()->associate($fund);
        $feesCharged->fee()->associate($fee);

        $feesCharged->save();
    }

    /**
     * @param Carbon $date
     * @throws ClientInvestmentException
     */
    private function checkPreviousDay(Carbon $date)
    {
        if (!$this->unitFund->performances()->where('date', $date->copy()->subDay())->exists()) {
            throw new ClientInvestmentException("Previous day performance not found");
        }
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function collectionAccount($type = 'bank')
    {
        $operator = $type == 'mpesa' ? '=' : '!=';

        return $this->unitFund->operatingAccounts()
            ->where('bank_swift', $operator, 'MPESA')
            ->wherePivot('role', 'collection')
            ->orderBy('preferred_account', 'DESC')
            ->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Relations\BelongsTo[]
     */
    public function portfolioAccounts()
    {
        $accounts = $this->unitFund->operatingAccounts()
            ->wherePivot('role', 'portfolio')
            ->get();

        if ($accounts->count() >= 1) {
            return $accounts;
        }

        return $this->unitFund->custodialAccount()->get();
    }

    /**
     * @param null $secondaryTag
     * @param bool $strict
     * @return CustodialAccount | null
     */
    public function getDisbursementAccount($secondaryTag = null, $strict = false)
    {
        $finder = function ($tag) {
            return $this->unitFund->operatingAccounts()->wherePivot('role', $tag)->first();
        };

        $acc = $finder($secondaryTag ? 'disbursement:'.$secondaryTag : 'disbursement');

        if ($acc) {
            return $acc;
        }

        if ($strict) {
            return null;
        }

        if ($default = $finder('disbursement')) {
            return $default;
        }

        return $this->unitFund->custodialAccount;
    }
}
