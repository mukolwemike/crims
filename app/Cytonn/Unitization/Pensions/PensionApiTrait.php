<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Pensions;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;

trait PensionApiTrait
{
    /**
     * @param $requestData
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendRequest($requestData)
    {
        $client = new GuzzleClient();

        try {
            $response = $client->request('POST', $this->dataUrl, [
                'auth' => [
                    $this->username,
                    $this->password
                ],
                'form_params' => $requestData
            ]);
        } catch (RequestException $e) {
            $response = $e->getResponse();
        } catch (\Exception $e) {
            $response = null;
        }

        try {
            $data = \GuzzleHttp\json_decode($response->getBody());
        } catch (\Exception $e) {
            return [];
        }

        return $data;
    }
}
