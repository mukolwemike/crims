<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Pensions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Unitization\Pensions\Members\ProcessPensionMember;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Pensions\Contributions\ProcessPensionContribution;
use Illuminate\Support\Facades\DB;

class PensionManager
{
    use PensionApiTrait;

    protected $unitFund;

    protected $date;

    protected $client;

    private $username;

    private $password;

    private $dataUrl;

    private $dataArray;

    private $sendReport = true;

    /**
     * PensionMemberManager constructor.
     * @param UnitFund $unitFund
     * @param Carbon|null $date
     * @param Client|null $client
     */
    public function __construct(UnitFund $unitFund, Carbon $date = null, Client $client = null)
    {
        $this->unitFund = $unitFund;
        $this->date = $date;
        $this->client = $client;

        $this->username = env('PENSION_USERNAME');
        $this->password = env('PENSION_PASSWORD');
    }

    /**
     * @param mixed $sendReport
     */
    public function setSendReport($sendReport)
    {
        $this->sendReport = $sendReport;
    }

    /**
     * MEMBERS
     */
    /**
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncMembers()
    {
        $this->dataUrl = env('PENSION_MEMBERS_URL');

        $requestData = $this->prepareMemberRequestData();

        $requestData = [
            'Code' => $requestData
        ];

        $response = $this->sendRequest($requestData);

        $this->processMembers($response->Members);
    }

    /**
     * @return string
     */
    private function prepareMemberRequestData()
    {
        $data['company_code'] = $this->unitFund->code;

        if ($this->client) {
            $data['member_no'] = $this->client->client_code;
        } else {
            if ($this->date) {
                $data['registrationdate'] = $this->date->format('d/m/Y');
            }
        }

//        $data = [
//            'company_code' => '001',
//            'member_no' => 'CPP/11207325/19',
////            'registrationdate' => '01/01/1753'
//        ];

        return \GuzzleHttp\json_encode($data);
    }

    /**
     * @param $members
     */
    private function processMembers($members)
    {
        foreach ($members as $member) {
            $processPensionMember = null;

            try {
                DB::transaction(function () use ($member, &$processPensionMember) {
                    $processPensionMember = new ProcessPensionMember($member, $this->unitFund);

                    $processPensionMember->process();
                });
            } catch (\Exception $e) {
                $this->notifyError($e, $member);
            }

            if ($this->sendReport) {
                $this->generateReport($processPensionMember);
            }
        }

        if ($this->sendReport) {
            $fileName = 'Pension Members - ' . Carbon::parse($this->date)->toDateString();

            $message = 'Please find attached the pension members synchronization report for '
                . $this->unitFund->name . ' for date ' . Carbon::parse($this->date)->toDateString();

            $this->sendReportNotification($fileName, $message);
        }
    }

    /**
     * @param ProcessPensionMember $processPensionMember
     */
    private function generateReport(ProcessPensionMember $processPensionMember)
    {
        $client = $processPensionMember->getClient();

        $memberDetails = $processPensionMember->getMemberDetails();

        $this->dataArray[] = [
            'Client Added' => $client ? "Yes" : "No",
            'New Client' => $processPensionMember->getNewClient() ? 'Yes' : 'No',
            'Client Code' => $client ? $client->client_code : $memberDetails->No_,
            'Client Name' => $client ? ClientPresenter::presentFullNames($client->id) : $memberDetails->Member_Name,
            'Email' => $client ? $client->contact->email : $memberDetails->E_mail,
            'Phone' => $client ? $client->contact->phone : $memberDetails->Phone_No,
            'ID Number' => $client ? $client->id_or_passport : $memberDetails->ID_No,
            'Pin Number' => $client ? $client->pin_no : $memberDetails->Pin_No,
            'Date of Birth' => $client ? $client->dob ? DatePresenter::formatDate($client->dob) : '' :
                $memberDetails->DateofBirth,
            'Date of Joining' => $client ? $client->date_of_joining ?
                DatePresenter::formatDate($client->date_of_joining) : '' : $memberDetails->Dateofjoining,
            'Date of Retirement' => $client ? $client->date_of_retirement
                ? DatePresenter::formatDate($client->date_of_retirement) : '' : $memberDetails->DateofRetirement,
        ];
    }

    private function sendReportNotification($fileName, $message)
    {
        ExcelWork::generateAndStoreSingleSheet($this->dataArray, $fileName);

        Mailer::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text($message)
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        \File::delete($path);
    }

    /**
     * CONTRIBUTIONS
     */
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncContributions()
    {
        $this->dataUrl = env('PENSION_CONTRIBUTIONS_URL');

        $requestData = $this->prepareContributionRequestData();

        $requestData = [
            'p' => $requestData
        ];

        $response = $this->sendRequest($requestData);

        $this->processContributions($response->contributions);
    }

    /**
     * @return string
     */
    private function prepareContributionRequestData()
    {
        $data['company_code'] = $this->unitFund->code;

        if ($this->client) {
            $data['member_no'] = $this->client->client_code;
        } else {
            if ($this->date) {
                $data['postingdate'] = $this->date->toDateString();
            }
        }

//        $data = [
//            'company_code' => '001',
//            'member_no' => 'CPP/25174612/19',
//            'postingdate' => '2019-07-01'
//        ];

        return \GuzzleHttp\json_encode($data);
    }

    /**
     * @param $contributions
     */
    private function processContributions($contributions)
    {
        foreach ($contributions as $contribution) {
            $processPensionContribution = null;

            try {
                DB::transaction(function () use ($contribution, &$processPensionContribution) {
                    $processPensionContribution = new ProcessPensionContribution($contribution, $this->unitFund);

                    $processPensionContribution->process();
                });
            } catch (\Exception $e) {
                $this->notifyError($e, $contribution);
            }

            if ($this->sendReport) {
                $this->generateContributionReport($processPensionContribution);
            }
        }

        if ($this->sendReport) {
            $fileName = 'Pension Contribution - ' . Carbon::parse($this->date)->toDateString();

            $message = 'Please find attached the pension contributions synchronization report for '
                . $this->unitFund->name . ' for date ' . Carbon::parse($this->date)->toDateString();

            $this->sendReportNotification($fileName, $message);
        }
    }

    /**
     * @param ProcessPensionContribution $processPensionContribution
     */
    private function generateContributionReport(ProcessPensionContribution $processPensionContribution)
    {
        $client = $processPensionContribution->getClient();

        $contribution = $processPensionContribution->getContribution();

        $recordAdded = $recordType = null;

        if ($processPensionContribution->getPurchase()) {
            $recordAdded = $processPensionContribution->getPurchase();
            $recordType = "Unit Purchase";
        } elseif ($processPensionContribution->getSale()) {
            $recordAdded = $processPensionContribution->getSale();
            $recordType = "Unit Sale";
        }

        $client = is_null($client) && $recordAdded ? $recordAdded->client : null;

        $this->dataArray[] = [
            'Record Added' => $recordAdded ? "Yes" : "No",
            'New Record' => $processPensionContribution->isNewContribution() ? "Yes" : "No",
            'Client Code' => $client ? $client->client_code : '',
            'Client Name' => $client ? ClientPresenter::presentFullNames($client->id) : '',
            'Amount' => $contribution->Amount,
            'Date' => $recordAdded ? Carbon::parse($recordAdded->date)->toDateString() : '',
            'Type' => $recordType,
            'Number' => $recordAdded ? $recordAdded->number : '',
            'Price' => $recordAdded ? $recordAdded->price : '',
            'Value' => $recordAdded ? $recordAdded->number * $recordAdded->price : '',
            'Exemption Type' => $recordAdded ? $recordAdded->exemptionType->name : '',
            'Contribution Type' => $recordAdded ? $recordAdded->contributionType->name : '',
            'Transaction Type' => $recordAdded ? $recordAdded->transactionType->name : '',
            'Reference' => $recordAdded ? $recordAdded->reference_id : $this->unitFund->id . '#' . $contribution->ID,
            'Member No' => $contribution->Member_No,
            'ID' => $contribution->ID,
            'Posting Date' => $contribution->PostingDate,
        ];
    }

    /**
     * @param \Exception $exception
     * @param $data
     */
    private function notifyError(\Exception $exception, $data)
    {
        $data = json_encode($data);

        Mail::compose()
            ->to(systemEmailGroups(['operations']))
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : Pension Processing Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
                <p>Data: <br>
                    <pre>" . $data . " </pre>
                </p>
                <p>Trace : " . $exception->getTraceAsString() . "</p>")
            ->send();
    }
}
