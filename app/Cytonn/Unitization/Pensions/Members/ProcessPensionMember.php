<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Unitization\Pensions\Members;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Users\UserClientRepository;
use Illuminate\Support\Arr;

class ProcessPensionMember
{
    protected $memberDetails;

    protected $unitFund;

    protected $client;

    protected $newClient;

    /**
     * ProcessPensionMember constructor.
     * @param $memberDetails
     * @param UnitFund $unitFund
     */
    public function __construct($memberDetails, UnitFund $unitFund)
    {
        $this->memberDetails = $memberDetails;

        $this->unitFund = $unitFund;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getMemberDetails()
    {
        return $this->memberDetails;
    }

    /**
     * @return mixed
     */
    public function getNewClient()
    {
        return $this->newClient;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\ClientInvestmentException|\Exception
     */
    public function process()
    {
        if (isEmptyOrNull($this->memberDetails->No_)) {
            return;
        }

        $client = $this->checkExistingClient();

        if ($client) {
            $this->client = $this->updateClientDetails($client);

            $this->newClient = false;

            return;
        }

        $filledApplication = $this->createClientFilledApplication();

        $application = $filledApplication->clientRepo->process();

        $this->approveClientTransaction($application->approval);

        $this->client = $this->processApplication($application);

        $this->newClient = true;
    }

    /**
     * @return Client|\Illuminate\Database\Eloquent\Model|null
     */
    private function checkExistingClient()
    {
        return Client::where('client_code', trim($this->memberDetails->No_))->first();
    }

    /**
     * @param Client $client
     * @return Client|null
     */
    private function updateClientDetails(Client $client)
    {
        $contactDetails = $this->processContactDetails();

        $clientDetails = $this->processClientDetails();

        $contactDetails['phone'] = $clientDetails['telephone_home'];

        $clientDetails['date_of_joining'] = $this->processDate($this->memberDetails->Dateofjoining);

        $clientDetails['date_of_retirement'] = $this->processDate($this->memberDetails->DateofRetirement);

        $client->contact->update($contactDetails);

        $client->update($clientDetails);

        return $client->fresh();
    }

    /**
     * @param ClientInvestmentApplication $application
     * @return \Cytonn\Investment\ApplicationsRepository|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    private function processApplication(ClientInvestmentApplication $application)
    {
        $input = $application->toArray();

        $type = $application->clientType->name;

        $contact = $application->repo->saveContactFromApplicationForm($type, $input);

        $input['client_code'] = trim($this->memberDetails->No_);
        $input['date_of_joining'] = $this->processDate($this->memberDetails->Dateofjoining);
        $input['date_of_retirement'] = $this->processDate($this->memberDetails->DateofRetirement);

        $client = $application->repo->saveClientFromApplicationForm($type, $contact, $input);

        $client = $client->fresh();

        (new UserClientRepository())->provisionForClient($client, $application->telephone_home);

        $application->client_id = $client->id;

        $application->unit_fund_id = $this->unitFund->id;

        $application->save();

        return $client;
    }

    /**
     * @return ClientFilledInvestmentApplication|null
     */
    private function createClientFilledApplication()
    {
        $dataArray = $this->processContactDetails();

        $dataArray = array_merge($dataArray, $this->processClientDetails());

        $channel = Channel::where('slug', 'admin')->first();
        $dataArray['channel_id'] = $channel ? $channel->id : null;

        $country = Country::where('iso_abbr', 'KE')->first();
        $dataArray['country_id'] = $country ? $country->id : null;
        $dataArray['nationality_id'] = $country ? $country->id : null;

        $src = FundSource::where('name', 'Other')->first();
        $dataArray['funds_source_id'] = $src ? $src->id : null;

        $dataArray['individual'] = 1;
        $dataArray['new_client'] = 1;

        $application = new ClientFilledInvestmentApplication();

        $application->fill($dataArray);

        $application->save();

        (new ClientApplicationRepository())->checkAndNotifyRiskyClient($dataArray, "CRIMS-ADMIN");

        return $application->fresh();
    }

    /**
     * @return array
     */
    private function processContactDetails()
    {
        $dataArray = $this->processMemberNames($this->memberDetails->Member_Name);

        $dataArray['email'] = $this->memberDetails->E_mail;

        return $dataArray;
    }

    /**
     * @return array
     */
    private function processClientDetails()
    {
        $dataArray = array();

        $dataArray['id_or_passport'] = $this->memberDetails->ID_No;

        $dataArray['pin_no'] = $this->memberDetails->Pin_No;

        $dataArray['dob'] = $this->processDate($this->memberDetails->DateofBirth);

        $dataArray['telephone_home'] = $this->memberDetails->Phone_No;

        return $dataArray;
    }

    /**
     * @param $name
     * @return array
     */
    private function processMemberNames($name)
    {
        $nameArray = array();

        $names = $this->getNames($name);

        $nameArray['firstname'] = ucfirst(strtolower($names[0]));

        $nameArray['middlename'] = isNotEmptyOrNull($names[1]) ? ucfirst(strtolower($names[1])) : null;

        $nameArray['lastname'] = ucfirst(strtolower($names[2]));

        return $nameArray;
    }

    /**
     * @param $names
     * @return array
     */
    private function getNames($names)
    {
        $namesArr = explode(' ', trim($names));

        $middleName = $namesArr;

        unset($middleName[0], $middleName[count($namesArr) - 1]);

        return [
            Arr::first($namesArr),
            implode(" ", $middleName),
            Arr::last($namesArr)
        ];
    }

    /**
     * @param $date
     * @return Carbon|string|null
     */
    private function processDate($date)
    {
        if (isEmptyOrNull($date) || $date == '01/01/1753') {
            return null;
        }

        $finalDate = null;

        try {
            $finalDate = Carbon::createFromFormat("d/m/Y", $date);

            $finalDate = $finalDate->toDateString();
        } catch (\Exception $e) {
        }

        return $finalDate;
    }

    /**
     * @param ClientTransactionApproval $approval
     * @throws \App\Exceptions\CrimsException|\Exception
     */
    private function approveClientTransaction(ClientTransactionApproval $approval)
    {
        $systemUser = getSystemUser();

        $approval->update([
            'sent_by' => $systemUser->id,
            'approved' => 1,
            'approved_by' => $systemUser->id,
            'approved_on' => Carbon::now(),
            'awaiting_stage_id' => 6
        ]);

        $approve = new Approval($approval);

        $approve->forceApprove($systemUser);
    }
}
