<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Pensions\Contributions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Unitization\Pensions\PensionManager;

class ProcessPensionContribution
{
    protected $unitFund;

    protected $contribution;

    protected $client;

    protected $purchase;

    protected $sale;

    protected $newContribution = true;

    public function __construct($contribution, UnitFund $unitFund)
    {
        $this->contribution = $contribution;

        $this->unitFund = $unitFund;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function process()
    {
        $this->prepareContributionData();

        $validate = $this->validateContribution();

        if (! $validate) {
            $this->newContribution = false;

            return;
        }

        $this->client = $this->processClient();

        if (is_null($this->client)) {
            $this->syncContributionClient();
        }

        if (is_null($this->client)) {
            return;
        }

        $this->processContribution();
    }

    /**
     * @return bool
     */
    public function isNewContribution(): bool
    {
        return $this->newContribution;
    }

    /**
     * @return bool
     */
    private function validateContribution()
    {
        $existingPurchase = UnitFundPurchase::where('reference_id', $this->contribution->reference_id)->first();

        if ($existingPurchase) {
            $this->purchase = $existingPurchase;

            return false;
        }

        $existingSale = UnitFundSale::where('reference_id', $this->contribution->reference_id)->first();

        if ($existingSale) {
            $this->sale = $existingSale;

            return false;
        }

        return true;
    }

    /**
     * @return Client
     */
    private function processClient()
    {
        return Client::where('client_code', $this->contribution->Member_No)->first();
    }

    /**
     * @return mixed
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * @return mixed
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @return mixed
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function syncContributionClient()
    {
        $client = new Client();

        $client->client_code = $this->contribution->Member_No;

        $pensionManager = new PensionManager($this->unitFund, null, $client);

        $pensionManager->setSendReport(false);

        $pensionManager->syncMembers();

        $this->client = $this->processClient();
    }

    private function prepareContributionData()
    {
        $this->contribution->posting_date = Carbon::parse($this->contribution->PostingDate);

        $this->contribution->actual_amount = abs($this->contribution->Amount);

        $this->contribution->reference_id = $this->unitFund->id . "#" . $this->contribution->ID;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     */
    private function processContribution()
    {
        if (in_array($this->contribution->transaction_Type, [1, 3, 4])) {
            $processor = new ProcessPurchaseContribution($this->contribution, $this->unitFund, $this->client);

            $processor->process();

            $this->purchase = $processor->getPurchase();
        } elseif ($this->contribution->transaction_Type == 2) {
            $processor = new ProcessWithdrawalContribution($this->contribution, $this->unitFund, $this->client);

            $processor->process();

            $this->sale = $processor->getSale();
        } else {
            throw new ClientInvestmentException("The specified pension contribution type of ID " .
                $this->contribution->transaction_Type . ' is not supported');
        }
    }
}
