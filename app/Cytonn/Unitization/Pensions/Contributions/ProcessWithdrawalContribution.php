<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Pensions\Contributions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Custodial\Withdraw;
use Cytonn\Models\Unitization\UnitFundContributionType;
use Cytonn\Models\Unitization\UnitFundTransactionType;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;

class ProcessWithdrawalContribution
{
    protected $contribution;

    protected $client;

    protected $unitFund;

    protected $sale;

    public function __construct($contribution, UnitFund $unitFund, Client $client)
    {
        $this->client = $client;

        $this->contribution = $contribution;

        $this->unitFund = $unitFund;
    }

    /**
     * @throws \App\Exceptions\CrimsException|\Exception
     */
    public function process()
    {
        $approval = $this->processFundSale();

        $bankInstruction = $this->processOutflow();
    }

    /**
     * @return mixed
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function processFundSale()
    {
        $approval = $this->saveInflowApproval();

        $this->approveClientTransaction($approval);

        $this->updateSale($approval);

        return $approval;
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function saveInflowApproval()
    {
        $input = [
            'date' => $this->contribution->posting_date->toDateString(),
            'number' => abs($this->contribution->Amount),
            'unit_fund_id' => $this->unitFund->id,
            'client_id' => $this->client->id
        ];

        $instruction = (new UnitFundInstructionsRepository())->sale($this->client, $this->unitFund, $input);

        $data = [
            'instruction_id' => $instruction->id
        ];

        $approval = ClientTransactionApproval::create([
            'client_id' => $this->client->id,
            'transaction_type' => 'create_unit_fund_sale',
            'payload' => $data,
            'sent_by' => getSystemUser()->id,
            'approved' => false,
            'approved_by' => null,
            'approved_on' => null,
            'awaiting_stage_id' => 1
        ]);

        $instruction->update(['approval_id' => $approval->id]);

        (new Approval($approval))->forceApprove(getSystemUser());

        return $approval;
    }

    /**
     * @param $approval
     */
    protected function approveClientTransaction($approval)
    {
        $handler = $approval->handler();

        $approve = new Approval($approval);

//        $approve->lockForUpdate();

        $handler->handle($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return UnitFundSale|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null
     */
    private function updateSale(ClientTransactionApproval $approval)
    {
        $sale = UnitFundSale::where('approval_id', $approval->id)->latest()->first();

        $transactionType = UnitFundTransactionType::find($this->contribution->transaction_Type);

        $contributionType = UnitFundContributionType::find($this->contribution->contribution_Type);

        $sale->update([
            'description' => @$contributionType->name . ' ' . @$transactionType->name,
            'unit_fund_transaction_type_id' => $this->contribution->transaction_Type,
            'unit_fund_contribution_type_id' => $this->contribution->contribution_Type,
            'reference_id' => $this->contribution->reference_id,
            'exemption_type_id' => $this->contribution->Exemption_Type
        ]);

        return $this->sale = $sale->fresh();
    }

    /**
     * @return \Cytonn\Custodial\Instruction
     * @throws \Exception
     */
    private function processOutflow()
    {
        $amount = abs($this->contribution->Amount);

        $description = 'Withdrawal of ' . AmountPresenter::currency($amount) . ' by ' .
            ClientPresenter::presentFullNames($this->client->id);

        $systemUser = getSystemUser();

        $clientBankAccount = new ClientBankAccount();
        $clientBankAccount->active = 1;

        $bankInstruction = Withdraw::make(
            $this->contribution->posting_date,
            $this->unitFund->inflowCustodialAccount,
            $description,
            $this->client,
            $amount,
            null,
            null,
            null,
            null,
            null,
            $systemUser,
            $systemUser,
            $clientBankAccount,
            null,
            $this->unitFund
        );

        $clientPayment = $bankInstruction->custodialTransaction->clientPayment;

        $clientPayment->update([
            'submit_status' => 'success',
            'submitted_on' => $this->contribution->posting_date,
            'submit_reason' => 'Pension withdrawn successfully'
        ]);

        return $bankInstruction;
    }
}
