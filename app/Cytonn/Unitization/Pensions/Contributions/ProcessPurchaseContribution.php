<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Pensions\Contributions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Models\Unitization\UnitFundContributionType;
use Cytonn\Models\Unitization\UnitFundTransactionType;
use Cytonn\Presenters\ClientPresenter;

class ProcessPurchaseContribution
{
    protected $contribution;

    protected $unitFund;

    protected $client;

    protected $instruction;

    protected $purchase;

    public function __construct($contribution, UnitFund $unitFund, Client $client)
    {
        $this->contribution = $contribution;

        $this->unitFund = $unitFund;

        $this->client = $client;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     */
    public function process()
    {
        $inflowApproval = $this->processInflow();

        $unitInvestmentApproval = $this->processUnitFundInvestment();
    }

    /**
     * @return mixed
     */
    public function getInstruction()
    {
        return $this->instruction;
    }

    /**
     * @return mixed
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function processInflow()
    {
        $approval = $this->saveInflowApproval();

        $this->approveClientTransaction($approval);

        return $approval;
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function saveInflowApproval()
    {
        $data = $this->prepareInflowApprovalData();

        $data['amount'] = $this->contribution->actual_amount;

        $approval = ClientTransactionApproval::create([
            'client_id' => $data['client_id'],
            'transaction_type' => 'add_cash_to_account',
            'payload' => $data,
            'sent_by' => 1,
            'approved' => 1,
            'approved_by' => 1,
            'approved_on' => Carbon::now(),
            'awaiting_stage_id' => 6
        ]);

        (new Approval($approval))->forceApprove(getSystemUser());

        return $approval;
    }

    /**
     * @return array
     */
    private function prepareInflowApprovalData()
    {
        $input = array();

        $input['id'] = null;

        $input['type'] = CustodialTransactionType::where('name', 'FI')->first()->id;

        $input['custodial_account_id'] = $this->unitFund->inflow_custodial_account_id;

        $input['unit_fund_id'] = $this->unitFund->id;

        $input['exchange_rate'] = 1;

        $input['source'] = 'deposit';

        $input['date'] = $this->contribution->posting_date->toDateString();

        $input['client_id'] = $this->client->id;

        $input['received_from'] = ClientPresenter::presentFullNames($this->client->id);

        $fa = $this->client->getLatestFa('units', false);

        $input['commission_recipient_id'] = $fa ? $fa->id : null;

        $input['bank_reference_no'] = 'ENWEALTH-'. $this->contribution->reference_id;

        $input['description'] = 'Client pension funds deposited - Client Code : '
            . $this->client->client_code . ' - ' . $this->contribution->Reference;

        return $input;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     */
    private function processUnitFundInvestment()
    {
        $this->createUnitFundInstruction();

        $approval = $this->createUnitFundInvestmentApproval();

        $this->approveClientTransaction($approval);

        $this->updatePurchase($approval);

        return $approval;
    }

    /**
     * @return \App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction
     */
    private function createUnitFundInstruction()
    {
        $input = [
            'date' => $this->contribution->posting_date,
            'amount' => $this->contribution->actual_amount
        ];

        $this->instruction = (new UnitFundInstructionsRepository())->purchase($input, $this->client, $this->unitFund);

        return $this->instruction;
    }

    /**
     * @return ClientTransactionApproval|\Illuminate\Database\Eloquent\Model
     * @throws \App\Exceptions\CrimsException
     */
    private function createUnitFundInvestmentApproval()
    {
        $recipient = $this->getUnitFundCommissionRecipient($this->client);

        $input = [
            'amount' => $this->instruction->amount,
            'client_id' => $this->client->id,
            'date' => $this->instruction->date,
            'description' => 'Purchase of Units for Ksh. ' . $this->instruction->amount,
            'fees' => 1,
            'instruction_id' => $this->instruction->id,
            'units' => 0,
            'recipient' => $recipient ? $recipient->id : null
        ];

        $approval = ClientTransactionApproval::create([
            'client_id' => $this->client->id,
            'transaction_type' => 'unit_fund_investment',
            'payload' => $input,
            'sent_by' => getSystemUser()->id,
            'approved' => 1,
            'approved_by' => 1,
            'approved_on' => Carbon::now(),
            'awaiting_stage_id' => 6
        ]);

        (new Approval($approval))->forceApprove(getSystemUser());

        return $approval;
    }

    /**
     * @param $approval
     */
    protected function approveClientTransaction($approval)
    {
        $handler = $approval->handler();

        $approve = new Approval($approval);

//        $approve->lockForUpdate();

        $handler->handle($approval);
    }

    /**
     * @param Client $client
     * @return |null
     */
    private function getUnitFundCommissionRecipient(Client $client)
    {
        $hasPurchases = $client->unitFundPurchases()->exists();

        if (!$hasPurchases) {
            $client = $this->getExistingClient($client);

            if ($client) {
                $recipient = $client->getLatestFa("units", false);

                if ($recipient) {
                    return $recipient;
                }
            }
        } else {
            $latestPurchase = $client->unitFundPurchases()
                ->whereHas('unitFundCommission', function ($c) {
                    $c->whereNotNull('commission_recipient_id');
                })->latest('date')
                ->first();

            $recipient = $latestPurchase ? $latestPurchase->unitFundCommission->recipient : null;

            if ($recipient) {
                return $recipient;
            }

            $recipient = $client->getLatestFa("units", false);

            if ($recipient) {
                return $recipient;
            }
        }

        return CommissionRecepient::where('email', 'operations@cytonn.com')
            ->where('active', 1)
            ->first();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return UnitFundPurchase|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null
     */
    private function updatePurchase(ClientTransactionApproval $approval)
    {
        $purchase = UnitFundPurchase::where('approval_id', $approval->id)->latest()->first();

        $transactionType = UnitFundTransactionType::find($this->contribution->transaction_Type);

        $contributionType = UnitFundContributionType::find($this->contribution->contribution_Type);

        $purchase->update([
            'description' => @$contributionType->name . ' ' . @$transactionType->name,
            'unit_fund_transaction_type_id' => $this->contribution->transaction_Type,
            'unit_fund_contribution_type_id' => $this->contribution->contribution_Type,
            'reference_id' => $this->contribution->reference_id,
            'exemption_type_id' => $this->contribution->Exemption_Type
        ]);

        return $this->purchase = $purchase->fresh();
    }

    /**
     * @param Client $client
     * @return Client|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    private function getExistingClient(Client $client)
    {
        if (isNotEmptyOrNull($client->id_or_passport)) {
            $client = Client::where('id', '!=', $client->id)
                ->where('pin_no', $client->pin_no)->first();

            if ($client) {
                return $client;
            }
        }

        if (isNotEmptyOrNull($client->contact->email)) {
            $client = Client::where('id', '!=', $client->id)
                ->whereHas('contact', function ($q) use ($client) {
                    $q->where('email', $client->contact->email);
                })->first();

            if ($client) {
                return $client;
            }
        }

        if (isNotEmptyOrNull($client->pin_no)) {
            $client = Client::where('id', '!=', $client->id)
                ->where('pin_no', $client->pin_no)->first();

            if ($client) {
                return $client;
            }
        }

        return null;
    }
}
