<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Reports;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\EquityShareSale;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Support\Mails\Mailer;
use Maatwebsite\Excel\Facades\Excel;

class UnitFundTransactionsSummary
{
    protected $unitFund;

    protected $startDate;

    protected $endDate;

    protected $currency;

    protected $user;

    public function __construct(
        UnitFund $unitFund,
        Carbon $startDate,
        Carbon $endDate,
        Currency $currency = null,
        User $user = null
    ) {
        $this->unitFund = $unitFund;

        $this->startDate = $startDate;

        $this->endDate = $endDate;

        $this->currency = ($currency) ? $currency : $this->getDefaultCurrency();

        $this->user = $user;
    }

    private function getDefaultCurrency()
    {
        return Currency::findOrFail(1);
    }

    public function generate()
    {
        $report = array();

        $report['deposits'] = $this->getDeposits();

        $report['equity_holdings'] = $this->getEquities();

        $fileName = "Transaction_Report";

        $this->export($report, $fileName, $this->unitFund, $this->startDate, $this->endDate);

        $email = $this->user ? [$this->user->email] : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the transaction report')
            ->excel([$fileName])
            ->send();
    }

    private function export($data, $fileName, $unitFund, $startDate, $endDate)
    {
        Excel::create($fileName, function ($excel) use ($data, $unitFund, $startDate, $endDate) {
            $excel->sheet('Transactions', function ($sheet) use ($data, $unitFund, $startDate, $endDate) {
                $sheet->loadView('unitization.reports.transactions', [
                    'data' => $data,
                    'unitFund' => $unitFund,
                    'startDate' => $startDate,
                    'endDate' => $endDate
                ]);
            });
        })->store('xlsx', storage_path('exports'));
    }

    /*
     * EQUITY HOLDINGS
     */
    /**
     * @return array
     */
    private function getEquities()
    {
        $equityHoldingsArray = array();

        $securityHoldings = EquityHolding::forUnitFund($this->unitFund)
            ->where('date', '>=', $this->startDate)
            ->where('date', '<=', $this->endDate)
            ->get()->groupBy('portfolio_security_id');

        $securitySales = EquityShareSale::forUnitFund($this->unitFund)
            ->where('date', '>=', $this->startDate)
            ->where('date', '<=', $this->endDate)
            ->get()->groupBy('security_id');

        foreach ($securityHoldings as $key => $equityHolding) {
            $portolioSecurity = PortfolioSecurity::findOrFail($key);

            $equityHoldingsArray[$key] = $this->presentSecurityHoldings($equityHolding, $portolioSecurity);
        }

        foreach ($securitySales as $key => $securitySale) {
            $portolioSecurity = PortfolioSecurity::findOrFail($key);

            $saleArray = $this->presentSecuritySales($securitySale, $portolioSecurity);

            if (array_key_exists($key, $equityHoldingsArray)) {
                $holdingArray = $equityHoldingsArray[$key]['investments'];
            } else {
                $equityHoldingsArray[$key] = $this->presentSecurityHoldings([], $portolioSecurity);

                $holdingArray = $equityHoldingsArray[$key]['investments'];
            }

            $holdingArray = array_merge($holdingArray, $saleArray);

            $holdingCol = collect($holdingArray)->sortBy('sort_date');

            $equityHoldingsArray[$key]['investments'] = $holdingCol;
        }

        return $equityHoldingsArray;
    }

    private function presentSecuritySales($securitySales, PortfolioSecurity $portfolioSecurity)
    {
        $salesArray = array();

        foreach ($securitySales as $sale) {
            $amount = $sale->repo->saleValue();

            $salesArray[] = [
                'sort_date' => Carbon::parse($sale->date)->toDateString(),
                'code' => $portfolioSecurity->code,
                'name' => $portfolioSecurity->name,
                'description' => "Share Sale",
                'transaction' => "Sold",
                'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($sale->date),
                'set_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($sale->date),
                'currency' => $this->currency->code,
                'maturity_date' => '',
                'amount' => \Cytonn\Presenters\AmountPresenter::accounting(-$amount, false, 0)
            ];
        }

        return $salesArray;
    }

    private function presentSecurityHoldings($equityHoldings, PortfolioSecurity $portfolioSecurity)
    {
        $securityArray = array();

        $securityArray['security_details'] = $this->getPortfolioSecurityDetails($portfolioSecurity);

        $securityArray['starting_balance'] = $this->presentEquityBalances(
            $portfolioSecurity,
            "Opening Balance",
            $this->startDate->copy()->subDay()
        );

        $securityArray['investments'] = $this->getEquityHoldingActions($equityHoldings, $portfolioSecurity);

        $securityArray['closing_balance'] = $this->presentEquityBalances(
            $portfolioSecurity,
            "Closing Balance",
            $this->endDate
        );

        return $securityArray;
    }


    private function getEquityHoldingActions($equityHoldings, PortfolioSecurity $portfolioSecurity)
    {
        $holdingsArray = array();

        foreach ($equityHoldings as $holding) {
            $amount = $holding->repo->totalCost();

            $holdingsArray[] = [
                'sort_date' => Carbon::parse($holding->date)->toDateString(),
                'code' => $portfolioSecurity->code,
                'name' => $portfolioSecurity->name,
                'description' => "Share Purchase",
                'transaction' => "Bought",
                'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($holding->date),
                'set_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($holding->date),
                'currency' => $this->currency->code,
                'maturity_date' => '',
                'amount' => \Cytonn\Presenters\AmountPresenter::accounting($amount, false, 0)
            ];
        }

        return $holdingsArray;
    }

    private function presentEquityBalances(PortfolioSecurity $portfolioSecurity, $description, Carbon $date)
    {
        $total = $this->getEquityHoldingTotals($portfolioSecurity, $date);

        return [
            'sort_date' => $date->toDateString(),
            'code' => $portfolioSecurity->code,
            'name' => $portfolioSecurity->name,
            'description' => $description,
            'transaction' => '',
            'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($date),
            'set_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($date),
            'currency' => $this->currency->code,
            'maturity_date' => '',
            'amount' => AmountPresenter::accounting($total['total'], false, 0)
        ];
    }

    private function getEquityHoldingTotals(PortfolioSecurity $portfolioSecurity, Carbon $date)
    {
        $repo = $portfolioSecurity->repo;
        $repo = $repo->setFund($this->unitFund);


        return [
            'total' => $repo->currentPortfolioValue($date)
        ];
    }


    /*
     * DEPOSIT HOLDINGS
     */
    private function getDeposits()
    {
        $securityDepositArray = array();

        $securityDeposits = DepositHolding::forUnitFund($this->unitFund)->get()->groupBy('portfolio_security_id');

        foreach ($securityDeposits as $key => $securityDeposit) {
            $portolioSecurity = PortfolioSecurity::findOrFail($key);

            $securityDepositArray[$key] = $this->presentSecurityDeposits($securityDeposit, $portolioSecurity);
        }

        return $securityDepositArray;
    }

    private function presentSecurityDeposits($deposits, PortfolioSecurity $portolioSecurity)
    {
        $securityArray = array();

        $securityArray['security_details'] = $this->getPortfolioSecurityDetails($portolioSecurity);

        $securityArray['starting_balance'] = $this->presentBalances(
            $portolioSecurity,
            "Opening Balance",
            $this->startDate->copy()->subDay()
        );

        $securityArray['investments'] = $this->presentInvestmentActions($deposits, $portolioSecurity);

        $securityArray['closing_balance'] = $this->presentBalances(
            $portolioSecurity,
            "Closing Balance",
            $this->endDate
        );

        return $securityArray;
    }

    private function getPortfolioSecurityDetails(PortfolioSecurity $portfolioSecurity)
    {
        return [
            'name' => $portfolioSecurity->name
        ];
    }

    private function presentInvestmentActions($deposits, PortfolioSecurity $portfolioSecurity)
    {
        $actionsArray = array();

        $deposits = $deposits->map(function (DepositHolding $investment) {
            return $investment->calculate($this->endDate, true)->getPrepared();
        });

        foreach ($deposits as $deposit) {
            $actions = $deposit->actions;

            $investedDate = Carbon::parse($deposit->investment->invested_date);

            if ($investedDate >= $this->startDate && $investedDate <= $this->endDate) {
                $actionsArray[] = $this->presentNewDeposit($deposit->investment);
            }

            foreach ($actions as $action) {
                $date = Carbon::parse($action->date);

                if ($date >= $this->startDate && $date <= $this->endDate) {
                    $actionsArray[] = [
                        'sort_date' => Carbon::parse($action->date)->toDateString(),
                        'code' => $portfolioSecurity->code,
                        'name' => $portfolioSecurity->name,
                        'description' => $action->description,
                        'transaction' => $this->getTransactionType($action->amount),
                        'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($action->date),
                        'set_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($action->date),
                        'currency' => $this->currency->code,
                        'maturity_date' => '',
                        'amount' =>
                            \Cytonn\Presenters\AmountPresenter::accounting($action->amount, false, 0)
                    ];
                }
            }

            $actionsArray = $actionsArray + $this->getPortfolioRepayments($deposit->investment);
        }

        $actionsCol = collect($actionsArray)->sortBy('sort_date');

        return $actionsCol;
    }

    private function getPortfolioRepayments(DepositHolding $holding)
    {
        $repaymentsArray = array();

        $portfolioSecurity = $holding->security;

        $repayments = $holding->repayments()
            ->where('date', '>=', $this->startDate)
            ->where('date', '<=', $this->endDate)
            ->get();

        foreach ($repayments as $repayment) {
            $repaymentsArray[] = [
                'sort_date' => Carbon::parse($repayment->date)->toDateString(),
                'code' => $portfolioSecurity->code,
                'name' => $portfolioSecurity->name,
                'description' => $repayment->type->name,
                'transaction' => 'Sold',
                'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($repayment->date),
                'set_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($repayment->date),
                'currency' => $this->currency->code,
                'maturity_date' => '',
                'amount' =>
                    \Cytonn\Presenters\AmountPresenter::accounting(-($repayment->amount), false, 0)
            ];
        }

        return $repaymentsArray;
    }

    private function getTransactionType($amount)
    {
        if ($amount >= 0) {
            return "";
        } else {
            return "Sold";
        }
    }

    private function presentNewDeposit(DepositHolding $depositHolding)
    {
        $portfolioSecurity = $depositHolding->security;

        return [
            'sort_date' => Carbon::parse($depositHolding->invested_date)->toDateString(),
            'code' => $portfolioSecurity->code,
            'name' => $portfolioSecurity->name,
            'description' => 'New Investment',
            'transaction' => $this->getTransactionType($depositHolding->amount),
            'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($depositHolding->invested_date),
            'set_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($depositHolding->invested_date),
            'currency' => $this->currency->code,
            'maturity_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($depositHolding->maturity_date),
            'amount' =>
                \Cytonn\Presenters\AmountPresenter::accounting($depositHolding->amount, false, 0)
        ];
    }

    private function presentBalances(PortfolioSecurity $portfolioSecurity, $description, Carbon $date)
    {
        $total = $this->getSecurityTotals($portfolioSecurity, $date);

        return [
            'sort_date' => $date->toDateString(),
            'code' => $portfolioSecurity->code,
            'name' => $portfolioSecurity->name,
            'description' => $description,
            'transaction' => '',
            'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($date),
            'set_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($date),
            'currency' => $this->currency->code,
            'maturity_date' => '',
            'amount' => AmountPresenter::accounting($total['total'], false, 0)
        ];
    }

    private function getSecurityTotals(PortfolioSecurity $portfolioSecurity, Carbon $date)
    {
        $securityRepo = $portfolioSecurity->depositRepo($this->currency);

        return [
            'principal' => $securityRepo->principal($date),
            'total' => $securityRepo->total($date),
        ];
    }
}
