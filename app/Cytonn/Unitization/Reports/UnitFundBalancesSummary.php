<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Reports;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Support\Mails\Mailer;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class UnitFundBalancesSummary
 * @package Cytonn\Unitization\Reports
 */
class UnitFundBalancesSummary
{
    /**
     * @var UnitFund
     */
    protected $unitFund;

    /**
     * @var Carbon
     */
    protected $date;

    /**
     * @var
     */
    protected $endDate;

    /**
     * @var Currency|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    protected $currency;

    /**
     * @var User
     */
    protected $user;

    /**
     * UnitFundBalancesSummary constructor.
     * @param UnitFund $unitFund
     * @param Carbon $date
     * @param Currency|null $currency
     * @param User|null $user
     */
    public function __construct(UnitFund $unitFund, Carbon $date, Currency $currency = null, User $user = null)
    {
        $this->unitFund = $unitFund;

        $this->date = $date;

        $this->currency = ($currency) ? $currency : $this->getDefaultCurrency();

        $this->user = $user;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private function getDefaultCurrency()
    {
        return Currency::findOrFail(1);
    }

    /**
     *
     */
    public function generate()
    {
        $report = array();

        $depositData = $this->getSecurities();

        $report['deposits'] = $depositData['deposits'];

        $report['deposit_totals'] = $depositData['deposit_totals'];

        $fileName = "Holdings_Report";

        $this->export($report, $fileName, $this->unitFund, $this->date);

        $email = $this->user ? [$this->user->email] : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the unit fund holdings report')
            ->excel([$fileName])
            ->send();
    }

    /**
     * @param $data
     * @param $fileName
     * @param $unitFund
     * @param $date
     */
    private function export($data, $fileName, $unitFund, $date)
    {
        Excel::create($fileName, function ($excel) use ($data, $unitFund, $date) {
            $excel->sheet('Holdings', function ($sheet) use ($data, $unitFund, $date) {
                $sheet->loadView('unitization.reports.deposit_balances', ['data' => $data, 'unitFund' => $unitFund,
                    'date' => $date]);
            });
        })->store('xlsx', storage_path('exports'));
    }

    /**
     * @return array
     */
    private function getSecurities()
    {
        $startDay = Carbon::parse($this->date)->startOfDay();

        $endDay = Carbon::parse($this->date)->endOfDay();

        $securities = PortfolioSecurity::whereHas('depositHoldings', function ($q) use ($startDay, $endDay) {
            $q->forUnitFund($this->unitFund)->activeBetweenDates($startDay, $endDay);
        })->orWhereHas('equityHoldings', function ($q) use ($endDay) {
            $q->forUnitFund($this->unitFund)->before($endDay);
        })->get();

        $securitiesArray = array();

        $totalsArray = array();

        foreach ($securities as $security) {
            $dataArray = $this->presentSecurityInvestments($security, $startDay, $endDay);

            $assetClass = $this->presentAssetClass($security);

            $securitiesArray[$assetClass][] = $dataArray['data'];

            $totalsArray[] = $dataArray['totals'];
        }

        $totalsCol = collect($totalsArray);

        return [
            'deposits' => $securitiesArray,
            'deposit_totals' => [
                'nominal_number' => $totalsCol->sum('nominal_number'),
                'historical_cost' => $totalsCol->sum('historical_cost'),
                'amount' => $totalsCol->sum('amount'),
            ]
        ];
    }

    private function presentAssetClass(PortfolioSecurity $security)
    {
        $class = $security->subAssetClass->assetClass;

        if ($class->slug == 'deposits' && $holding = $security->depositHoldings()->first()) {
            $type = $holding->depositType;

            return $type ? $type->name : $class->name;
        }

        return $class->name;
    }

    /**
     * @param PortfolioSecurity $portfolioSecurity
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function presentSecurityInvestments(
        PortfolioSecurity $portfolioSecurity,
        Carbon $startDate,
        Carbon $endDate
    ) {
        $securityArray = array();

        $securityArray['security_details'] = $this->getSecurityDetails($portfolioSecurity);

        $depositsArray = array();

        $depositsArray = $this->getSecurityDeposits($portfolioSecurity, $startDate, $endDate, $depositsArray);

        $depositsArray = $this->getSecurityEquities($portfolioSecurity, $startDate, $endDate, $depositsArray);

        $depositsArray = $this->presentSecurityTotals($depositsArray);

        $securityArray['investments'] = $depositsArray['deposits'];

        return [
            'data' => $securityArray,
            'totals' => $depositsArray['totals']
        ];
    }

    /**
     * @param PortfolioSecurity $portfolioSecurity
     * @return array
     */
    private function getSecurityDetails(PortfolioSecurity $portfolioSecurity)
    {
        return [
            'name' => $portfolioSecurity->name
        ];
    }

    /**
     * @param PortfolioSecurity $portfolioSecurity
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param $depositsArray
     * @return array
     */
    private function getSecurityDeposits(
        PortfolioSecurity $portfolioSecurity,
        Carbon $startDate,
        Carbon $endDate,
        $depositsArray
    ) {
        $deposits = DepositHolding::activeBetweenDates($startDate, $endDate)->forSecurity($portfolioSecurity)
            ->forUnitFund($this->unitFund)
            ->orderBy('invested_date')
            ->get();

        foreach ($deposits as $deposit) {
            $depositRepo = $deposit->repo;

            $value = $depositRepo->getInvestmentTotal($endDate);

            if (round($value, 2) != 0.00) {
                $depositsArray[] = [
                    'code' => $portfolioSecurity->code,
                    'name' => $portfolioSecurity->name,
                    'isin_code' => $portfolioSecurity->isin_code,
                    'nominal_number' => $deposit->amount,
                    'historical_cost' => $deposit->amount,
                    'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($deposit->invested_date),
                    'maturity_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($deposit->maturity_date),
                    'price' => null,
                    'currency' => $this->currency->code,
                    'amount' => $value,
                ];
            }
        }

        return $depositsArray;
    }

    /**
     * @param PortfolioSecurity $portfolioSecurity
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param $depositArray
     * @return array
     */
    private function getSecurityEquities(
        PortfolioSecurity $portfolioSecurity,
        Carbon $startDate,
        Carbon $endDate,
        $depositArray
    ) {
        $equities = EquityHolding::before($endDate)->forSecurity($portfolioSecurity)->forUnitFund($this->unitFund)
            ->orderBy('date')->get();

        foreach ($equities as $equity) {
            $repo = $equity->repo->setFund($this->unitFund);

            $sharesNumber = $repo->netNumberOfShares($endDate);

            if ($sharesNumber != 0) {
                $marketPrice = $repo->marketPricePerShare($endDate);


                $depositArray[] = [
                    'code' => $portfolioSecurity->code,
                    'name' => $portfolioSecurity->name,
                    'isin_code' => $portfolioSecurity->isin_code,
                    'nominal_number' => $equity->number,
                    'historical_cost' => $repo->totalCost(),
                    'trade_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($equity->date),
                    'maturity_date' => \Cytonn\Presenters\DatePresenter::formatShortDate($equity->date),
                    'currency' => $this->currency->code,
                    'price' => $marketPrice,
                    'amount' => $marketPrice * $sharesNumber
                ];
            }
        }

        return $depositArray;
    }


    /**
     * @param $depositsArray
     * @return array
     */
    private function presentSecurityTotals($depositsArray)
    {
        $depositCol = collect($depositsArray);
        $depositCol->sortBy('trade_date');

        $principalSum = $depositCol->sum('nominal_number');
        $costSum = $depositCol->sum('historical_cost');
        $amountSum = $depositCol->sum('amount');

        $depositCol->put('total', [
            'code' => '',
            'name' => '',
            'isin_code' => '',
            'nominal_number' => $principalSum,
            'historical_cost' => $costSum,
            'trade_date' => '',
            'maturity_date' => '',
            'currency' => $this->currency->code,
            'amount' => $amountSum,
        ]);

        return [
            'deposits' => $depositCol,
            'totals' => [
                'nominal_number' => $principalSum,
                'historical_cost' => $costSum,
                'amount' => $amountSum,
            ]
        ];
    }
}
