<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Unitization\Reports;

use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Portfolio\AssetClasses\DepositPerformance;
use Cytonn\Portfolio\AssetClasses\EquitiesPerformance;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Performance;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class PerformanceAttributionSummary
 * @package Cytonn\Unitization\Reports
 */
class PerformanceAttributionSummary
{
    /**
     * @var static
     */
    protected $start;

    /**
     * @var static
     */
    protected $end;

    /**
     * @var FundManager
     */
    protected $fundManager;

    /**
     * @var array|mixed
     */
    protected $unitFund;

    /**
     * @var User
     */
    protected $user;


    /**
     * PerformanceAttributionSummary constructor.
     * @param Carbon $start
     * @param Carbon $end
     * @param FundManager $fundManager
     * @param UnitFund|null $unitFund
     * @param User|null $user
     */
    public function __construct(
        Carbon $start,
        Carbon $end,
        FundManager $fundManager,
        UnitFund $unitFund = null,
        User $user = null
    ) {
        $this->start = $start->startOfDay();

        $this->end = $end->endOfDay();

        $this->fundManager = $fundManager;

        $this->unitFund = is_null($unitFund) ? $this->getUnitFunds() : [$unitFund];

        $this->user = $user;
    }

    /**
     * @return mixed
     */
    private function getUnitFunds()
    {
        return $this->fundManager->unitFunds;
    }

    /**
     * @return array
     */
    public function generate()
    {
        $reportArray = array();

        $reportArray['heading'] = [
            'start' => $this->start->toDateString(),
            'end' => $this->end->toDateString(),
            'fundmanager' => $this->fundManager->name,
            'fund' => count($this->unitFund) === 1 ? array_first($this->unitFund)->name : null
        ];

        $startDate = $this->start->copy();

        while ($startDate <= $this->end) {
            $endDate = $this->end <= $startDate->copy()->endOfMonth()
                ? $this->end->copy() : $startDate->copy()->endOfMonth();

            $reportArray['months'][$startDate->format('F-Y')] = [
                'days' => $this->getMonthDays($startDate->copy(), $endDate->copy()),
                'funds' => $this->getUnitFundsData($startDate->copy(), $endDate->copy())
            ];

            $startDate = $startDate->addMonthNoOverflow()->startOfMonth();
        }

        $fileName = "Performance Attribution Summary";

        $view = 'unitization.reports.unit_fund_cash_flow';

        $this->exportExcel($reportArray, $view, $fileName);

        $email = $this->user ? [$this->user->email] : [];

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find attached the perfomance attribution report for the period from '
                . $this->start->toDateString() . ' to  ' . $this->end->toDateString())
            ->excel([$fileName])
            ->send();
    }


    /**
     * @param $reportArray
     * @param $view
     * @param $fileName
     */
    private function exportExcel($reportArray, $view, $fileName)
    {
        Excel::create(
            $fileName,
            function ($excel) use ($reportArray, $view, $fileName) {
                $excel->sheet(
                    $fileName,
                    function ($sheet) use ($reportArray, $view) {
                        $sheet->loadView($view, ['values' => $reportArray]);
                    }
                );
            }
        )->store('xlsx', storage_path('exports'));
    }

    private function getMonthDays(Carbon $startDate, Carbon $endDate)
    {
        $datesArray = array();

        $datesArray[] = $startDate->format('d-M');

        while ($startDate < $endDate->startOfDay()) {
            $datesArray[] = $startDate->addDay()->format('d-M');
        }

        return $datesArray;
    }

    private function getUnitFundsData(Carbon $startDate, Carbon $endDate)
    {
        $fundsArray = array();

        foreach ($this->unitFund as $unitFund) {
            $fundsArray[] = $this->getIndividualUnitFundData($unitFund, $startDate, $endDate);
        }

        $fundsArray = $this->calculateCompositeValues($fundsArray);

        return $fundsArray;
    }

    private function getIndividualUnitFundData(UnitFund $unitFund, Carbon $startDate, Carbon $endDate)
    {
        $unitFundArray = array();

        $unitFundArray['details'] = $this->getUnitFundDetails($unitFund);

        $unitFundArray['data'] = $this->getUnitFundMonthData($unitFund, $startDate, $endDate);

        $unitFundArray['totals'] = $this->calculateFundTotals($unitFund, $startDate, $endDate, $unitFundArray['data']);

        return $unitFundArray;
    }

    private function getUnitFundDetails(UnitFund $unitFund)
    {
        return [
            'name' => $unitFund->short_name
        ];
    }

    private function getUnitFundMonthData(UnitFund $unitFund, Carbon $startDate, Carbon $endDate)
    {
        $totalsArray = array();

        $localDeposits =
            $this->presentDepositHoldings($unitFund, $startDate, $endDate, false, $totalsArray);

        $localEquities =
            $this->presentEquityHoldings($unitFund, $startDate, $endDate, false, $totalsArray);

        $offshoreInvestments = $this->presentOffshoreHoldings($unitFund, $startDate, $endDate, $totalsArray);

        $dataArray = [
            'deposits' => $localDeposits,
            'equities' => $localEquities,
            'offshore' => $offshoreInvestments,
            'total_value' => $totalsArray
        ];

        return $dataArray;
    }

    private function presentDepositHoldings(
        UnitFund $unitFund,
        Carbon $startDate,
        Carbon $endDate,
        $offshore,
        &$totalsArray
    ) {
        $depositsArray = array();

        $deposits = $this->getDepositHoldings($unitFund, $startDate, $endDate, $offshore);

        foreach ($deposits as $deposit) {
            $day = Carbon::parse($deposit->invested_date)->format('d-M');

            $depositAmount = (float)$deposit->amount;

            if (array_key_exists($day, $depositsArray)) {
                $depositsArray[$day] += $depositAmount;
            } else {
                $depositsArray[$day] = $depositAmount;
            }

            if (array_key_exists($day, $totalsArray)) {
                $totalsArray[$day] += $depositAmount;
            } else {
                $totalsArray[$day] = $depositAmount;
            }
        }

        $repayments = $this->getPortfolioRepayments($unitFund, $startDate, $endDate, $offshore);

        foreach ($repayments as $repayment) {
            $day = Carbon::parse($repayment->date)->format('d-M');

            $repaymentAmount = -(float)$repayment->amount;

            if (array_key_exists($day, $depositsArray)) {
                $depositsArray[$day] += $repaymentAmount;
            } else {
                $depositsArray[$day] = $repaymentAmount;
            }

            if (array_key_exists($day, $totalsArray)) {
                $totalsArray[$day] += $repaymentAmount;
            } else {
                $totalsArray[$day] = $repaymentAmount;
            }
        }

        return $depositsArray;
    }

    private function getDepositHoldings(UnitFund $unitFund, Carbon $startDate, Carbon $endDate, $offshore)
    {
        return DepositHolding::where('invested_date', '>=', $startDate)
            ->where('invested_date', '<=', $endDate)
            ->forUnitFund($unitFund)
            ->offshore($offshore)
            ->get();
    }

    private function getPortfolioRepayments(UnitFund $unitFund, Carbon $startDate, Carbon $endDate, $offshore)
    {
        return PortfolioInvestmentRepayment::where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->forUnitFund($unitFund)
            ->offshore($offshore)
            ->get();
    }

    private function presentEquityHoldings(
        UnitFund $unitFund,
        Carbon $startDate,
        Carbon $endDate,
        $offshore,
        &$totalsArray
    ) {
        $equitiesArray = array();

        $equities = $this->getEquityHoldings($unitFund, $startDate, $endDate, $offshore);

        foreach ($equities as $equity) {
            $day = Carbon::parse($equity->date)->format('d-M');

            $totalCost = (float)$equity->repo->totalCost();

            if (array_key_exists($day, $equitiesArray)) {
                $equitiesArray[$day] += $totalCost;
            } else {
                $equitiesArray[$day] = $totalCost;
            }

            if (array_key_exists($day, $totalsArray)) {
                $totalsArray[$day] += $totalCost;
            } else {
                $totalsArray[$day] = $totalCost;
            }
        }

        $sales = $this->getShareSales($unitFund, $startDate, $endDate, $offshore);

        foreach ($sales as $sale) {
            $day = Carbon::parse($sale->date)->format('d-M');

            $saleValue = -(float)$sale->repo->saleValue();

            if (array_key_exists($day, $equitiesArray)) {
                $equitiesArray[$day] += $saleValue;
            } else {
                $equitiesArray[$day] = $saleValue;
            }

            if (array_key_exists($day, $totalsArray)) {
                $totalsArray[$day] += $saleValue;
            } else {
                $totalsArray[$day] = $saleValue;
            }
        }

        return $equitiesArray;
    }

    private function getEquityHoldings(UnitFund $unitFund, Carbon $startDate, Carbon $endDate, $offshore)
    {
        return EquityHolding::where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->forUnitFund($unitFund)
            ->offshore($offshore)
            ->get();
    }

    private function getShareSales(UnitFund $unitFund, Carbon $startDate, Carbon $endDate, $offshore)
    {
        return ActiveStrategyShareSale::where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->forUnitFund($unitFund)
            ->offshore($offshore)
            ->get();
    }

    private function presentOffshoreHoldings(UnitFund $unitFund, Carbon $startDate, Carbon $endDate, &$totalsArray)
    {
        $offshoreArray = $this->presentDepositHoldings($unitFund, $startDate, $endDate, true, $totalsArray);

        $equitiesArray = $this->presentEquityHoldings($unitFund, $startDate, $endDate, true, $totalsArray);

        foreach ($equitiesArray as $key => $equity) {
            if (array_key_exists($key, $offshoreArray)) {
                $offshoreArray[$key] += $equity;
            } else {
                $offshoreArray[$key] = $equity;
            }
        }

        return $offshoreArray;
    }

    private function calculateFundTotals(UnitFund $unitFund, Carbon $startDate, Carbon $endDate, $dataArray)
    {
        $totalsArray = array();

        $totalsArray['starting_value'] = $this->getFundValue($unitFund, $startDate);

        $sumArray = $this->getCashflowValue($dataArray);

        $totalsArray['net_cash_flow'] = $sumArray['net_cash_flow'];

        $totalsArray['weighted_cash_flow'] = $sumArray['weighted_cash_flow'];

        $totalsArray['ending_value'] = $this->getFundValue($unitFund, $endDate);

        $totalsArray['beginning_plus_weighted'] = $this->getBeginningPlusWeighted($totalsArray);

        $totalsArray = $this->getPeriodReturn($totalsArray);

        return $totalsArray;
    }

    private function getFundValue(UnitFund $unitFund, Carbon $date)
    {
        $deposits = $this->getUnitFundDepositsValue($unitFund, $date, false);

        $equities = $this->getUnitFundEquitiesValue($unitFund, $date, false);

        $offshore = $this->getUnitFundOffshoreValue($unitFund, $date, true);

        return [
            'deposits' => $deposits,
            'equities' => $equities,
            'offshore' => $offshore,
            'total_value' => $deposits + $equities + $offshore
        ];
    }

    private function getUnitFundDepositsValue(UnitFund $unitFund, Carbon $date, $offshore)
    {
        $perf = (new DepositPerformance($this->fundManager, $date))
            ->setFund($unitFund)
            ->setOffshore($offshore);

        return $perf->marketValue();
    }

    private function getUnitFundEquitiesValue(UnitFund $unitFund, Carbon $date, $offshore)
    {
        $perf = (new EquitiesPerformance($this->fundManager, $date))
            ->setFund($unitFund)
            ->setOffshore($offshore);

        return $perf->marketValue();
    }

    private function getUnitFundOffshoreValue(UnitFund $unitFund, Carbon $date, $offshore)
    {
        $perf = (new Performance($unitFund, $date))->setOffshore($offshore);

        return $perf->assetMarketValue();
    }

    private function getCashflowValue($dataArray)
    {
        $sumArray = array();

        foreach ($dataArray as $key => $categoryArray) {
            if (count($categoryArray) == 0) {
                $sumArray['net_cash_flow'][$key] = 0;
                $sumArray['weighted_cash_flow'][$key] = 0;
            } else {
                foreach ($categoryArray as $dayKey => $value) {
                    if (isset($sumArray['net_cash_flow'][$key])) {
                        $sumArray['net_cash_flow'][$key] += $value;
                    } else {
                        $sumArray['net_cash_flow'][$key] = $value;
                    }

                    $date = Carbon::createFromFormat('d-M', $dayKey);

                    if (isset($sumArray['weighted_cash_flow'][$key])) {
                        $sumArray['weighted_cash_flow'][$key] += $this->calculateWeightedValue($value, $date);
                    } else {
                        $sumArray['weighted_cash_flow'][$key] = $this->calculateWeightedValue($value, $date);
                    }
                }
            }
        }

        return $sumArray;
    }

    private function calculateWeightedValue($value, Carbon $date)
    {
        $daysInMonth = $date->daysInMonth;

        $day = $date->day;

        return (($daysInMonth - $day) / $daysInMonth) * $value;
    }

    private function getBeginningPlusWeighted($dataArray)
    {
        return [
            'deposits' => $dataArray['weighted_cash_flow']['deposits'] + $dataArray['starting_value']['deposits'],
            'equities' => $dataArray['weighted_cash_flow']['equities'] + $dataArray['starting_value']['equities'],
            'offshore' => $dataArray['weighted_cash_flow']['offshore'] + $dataArray['starting_value']['offshore'],
            'total_value' =>
                $dataArray['weighted_cash_flow']['total_value'] + $dataArray['starting_value']['total_value'],
        ];
    }

    private function getPeriodReturn($dataArray)
    {
        $this->getCategoryPeriodReturn($dataArray, 'deposits');

        $this->getCategoryPeriodReturn($dataArray, 'equities');

        $this->getCategoryPeriodReturn($dataArray, 'offshore');

        $this->getCategoryPeriodReturn($dataArray, 'total_value');

        return $dataArray;
    }

    private function getCategoryPeriodReturn(&$dataArray, $key)
    {
        $endingValue = $dataArray['ending_value'][$key];

        $startingValue = $dataArray['starting_value'][$key];

        $netCashflow = $dataArray['net_cash_flow'][$key];

        $beginningWeighted = $dataArray['beginning_plus_weighted'][$key];

        $totalBeginningWeighted = $dataArray['beginning_plus_weighted']['total_value'];

        $dataArray['period_return'][$key] =
            percentage(($endingValue - $startingValue - $netCashflow), $beginningWeighted, 2);

        $dataArray['percentage_value_at_beginning'][$key] =
            percentage($beginningWeighted, $totalBeginningWeighted, 2);

        $dataArray['avg_weighted_total'][$key] =
            percentage(($endingValue - $startingValue - $netCashflow), $totalBeginningWeighted, 2);
    }

    private function calculateCompositeValues($fundsArray)
    {
        $totalComposite = 0;

        $compositeAverage = 0;

        foreach ($fundsArray as $fundData) {
            $totalComposite += $fundData['totals']['beginning_plus_weighted']['total_value'];
        }

        foreach ($fundsArray as $key => $fundData) {
            $endingValue = $fundsArray[$key]['totals']['ending_value']['total_value'];

            $startingValue = $fundsArray[$key]['totals']['starting_value']['total_value'];

            $netCashflow = $fundsArray[$key]['totals']['net_cash_flow']['total_value'];

            $totalBeginningWeighted = $fundsArray[$key]['totals']['beginning_plus_weighted']['total_value'];

            $fundsArray[$key]['totals']['composite_percentage_value_at_beginning']['total_value'] =
                percentage($totalBeginningWeighted, $totalComposite, 2);

            if ($totalComposite != 0) {
                $compositeAverage += (($endingValue - $startingValue - $netCashflow) / $totalComposite);
            }
        }

        $keysArray = array_keys($fundsArray);

        $lastKey = $keysArray[count($keysArray) - 1];

        $fundsArray[$lastKey]['totals']['composite_beginning_value']['total_value'] = $totalComposite;

        $fundsArray[$lastKey]['totals']['composite_avg_weighted_total']['total_value'] =
            percentage($compositeAverage, 1, 2);

        return $fundsArray;
    }
}
