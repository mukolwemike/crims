<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 8/20/18
 * Time: 12:06 PM
 */

namespace App\Cytonn\Unitization;

use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFundInstructionType;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;

class UnitFundInstructionsRepository
{
    public function purchase($input, $client, $fund)
    {
        return $this->saveInstruction($client, $fund, $input, 'purchase');
    }

    public function sale($client, $fund, $input)
    {
        return $this->saveInstruction($client, $fund, $input, 'sale');
    }

    public function transfer($client, $fund, $input)
    {
        return $this->saveInstruction($client, $fund, $input, 'transfer');
    }

    /**
     * @param $client
     * @param $fund
     * @param $input
     * @param $type
     * @return UnitFundInvestmentInstruction
     */
    private function saveInstruction($client, $fund, $input, $type)
    {
        $instruction = new UnitFundInvestmentInstruction();

        $instruction->payload = \GuzzleHttp\json_encode($input);

        $instruction->number = isset($input['number']) ? str_replace(",", "", $input['number']) : null;

        $instruction->amount = isset($input['amount']) ? str_replace(",", "", $input['amount']) : null;

        $instruction->date = $input['date'];

        $instruction->account_id = isset($input['account_id']) ? $input['account_id'] : null;

        $type = UnitFundInstructionType::where('slug', $type)->first();

        $instruction->type()->associate($type);

        $instruction->client()->associate($client);

        $instruction->unitFund()->associate($fund);

        $instruction->user_id = isset($input['user_id']) ? $input['user_id'] : null;

        $instruction->channel_id = isset($input['channel']) ? Channel::where('slug', $input['channel'])->first()->id : null;

        $instruction->save();

        return $instruction;
    }

    public function approvedByClient(UnitFundInvestmentInstruction $instruction)
    {
        $mandate = $instruction->client->present()->getSigningMandate;

        $approvals = $instruction->clientApprovals();

        if (!$approvals->count()) {
            return true;
        }

        $approved = $instruction->clientApprovals()->approved(true)->get();

        switch ($mandate) {
            case 'Singly':
                return true;

            case 'All to sign':
                return $approvals->get()->count() == $approved->count();

            case 'Either to sign':
                return $approved ? true : false;

            case 'At Least Two to sign':
                return $approved > 2 ? true : false;

            default:
                throw new \InvalidArgumentException("Mandate type not supported");
        }
    }

    public function calculateAmountAffected(UnitFundInvestmentInstruction $instruction)
    {
        if ($instruction->type->slug == 'purchase') {
            return $instruction->amount;
        }

        return $instruction->number;
    }
}
