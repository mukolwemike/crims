<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/8/18
 * Time: 9:24 AM
 */

namespace App\Cytonn\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\Unitization\UnitFundClientSummaryTransfomer;
use Cytonn\Api\Transformers\Unitization\UnitFundInvestmentInstructionTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundPurchaseTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundSaleTransformer;
use Cytonn\Api\Transformers\Unitization\UnitFundTransferTransformer;

class UnitFundClientSummary
{
    use AlternateSortFilterPaginateTrait;

    protected $fund;

    protected $client;

    protected $date;

    protected $start;

    public function __construct(UnitFund $fund = null, Client $client, $date = null, $start = null)
    {
        $this->client = $client;

        $this->fund = $fund;

        $this->date = ($date) ? Carbon::parse($date) : Carbon::today();

        $this->start = $start;
    }

    /**
     * @return array
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function index()
    {
        $clientDetails = (new UnitFundClientSummaryTransfomer())->transform($this->client, $this->fund);

        return [
            'details' => $clientDetails,
            'purchases' => $this->fundPurchases(),
            'sales' => $this->fundSales(),
            'transfers_given' => $this->fundTransfersGiven(),
            'transfers_received' => $this->fundTransfersReceived(),
            'instructions' => $this->instruction()
        ];
    }

    public function fundPurchases()
    {
        return $this->sortFilterPaginate(

            new UnitFundPurchase(),
            [],
            function ($purchase) {
                return app(UnitFundPurchaseTransformer::class)->transform($purchase);
            },
            function ($model) {
                return $model->where('unit_fund_id', $this->fund->id)
                    ->where('client_id', $this->client->id);
            }
        );
    }

    public function fundSales()
    {
        return $this->sortFilterPaginate(

            new UnitFundSale(),
            [],
            function ($sale) {
                return app(UnitFundSaleTransformer::class)->transform($sale);
            },
            function ($model) {
                return $model->where('unit_fund_id', $this->fund->id)
                    ->where('client_id', $this->client->id);
            }
        );
    }

    public function fundTransfersGiven()
    {
        return $this->sortFilterPaginate(

            new UnitFundTransfer(),
            [],
            function ($transfer) {
                return app(UnitFundTransferTransformer::class)->transform($transfer);
            },
            function ($model) {
                return $model->where('unit_fund_id', $this->fund->id)
                    ->where('transferer_id', $this->client->id);
            }
        );
    }

    public function fundTransfersReceived()
    {
        return $this->sortFilterPaginate(

            new UnitFundTransfer(),
            [],
            function ($transfer) {
                return app(UnitFundTransferTransformer::class)->transform($transfer);
            },
            function ($model) {
                return $model->where('unit_fund_id', $this->fund->id)
                    ->where('transferee_id', $this->client->id);
            }
        );
    }

    public function instruction()
    {
        return $this->sortFilterPaginate(
            new UnitFundInvestmentInstruction(),
            [],
            function ($instruction) {
                return app(UnitFundInvestmentInstructionTransformer::class)->transform($instruction);
            },
            function ($model) {
                return $model->where('unit_fund_id', $this->fund->id)
                    ->where('client_id', $this->client->id)
                    ->where('created_at', '>=', Carbon::now()->subDay(14));
            }
        );
    }

    public function recentActivities()
    {
        $purchases = $this->client->unitFundPurchases()->latest('date')->take(10)->get()
            ->map(function (UnitFundPurchase $purchase) {
                return (object)[
                    'number' => $purchase->number ? $purchase->number : null,
                    'currency' => $purchase->unitFund->currency->name,
                    'unitFund' => $purchase->unitFund->short_name,
                    'type' => 'Purchase',
                    'amount' => $purchase->amount
                        ? $this->cleanAmount($purchase->amount)
                        : $this->cleanAmount(($purchase->number * $purchase->price)),
                    'date' => $purchase->date->toFormattedDateString()
                ];
            });

        $sales = $this->client->unitFundSales()->latest('date')->take(10)->get()
            ->map(function (UnitFundSale $sale) {
                return (object)[
                    'number' => $sale->number ? $sale->number : null,
                    'currency' => $sale->unitFund->currency->name,
                    'unitFund' => $sale->unitFund->short_name,
                    'type' => 'Sale',
                    'amount' => $sale->amount
                        ? $this->cleanAmount($sale->amount)
                        : $this->cleanAmount(($sale->number * $sale->price)),
                    'date' => $sale->date->toFormattedDateString()
                ];
            });

        $activities = collect([])
            ->merge($purchases)
            ->merge($sales)
            ->sortByDate('date', 'DESC')
            ->take(5)
            ->all();

        return [
            'data' => $activities,
            'current_page' => 1,
            'from' => 1,
            'last_page' => 1,
            'next_page_url' => null,
            'path' => "/",
            'per_page' => "5",
            'prev_page_url' => null,
            'to' => count($activities),
            'total' => count($activities),
        ];
    }

    private function cleanAmount($amount)
    {
        return AmountPresenter::currency($amount, false, 2);
    }
}
