<?php

namespace App\Cytonn\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Events\Unitization\UnitFundTransferPerformed;
use App\Exceptions\CrimsException;
use App\Jobs\USSD\SendMessages;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Support\Mails\Mailer;
use Exception;
use Illuminate\Support\Arr;

class ProcessTransfer
{
    protected $data;
    protected $client;
    protected $fund;
    protected $approval = null;
    protected $transferInstruction = null;

    const MIN_AMOUNT = 100;

    const MAX_AMOUNT = 100000;

    public function __construct($data, Client $client, UnitFund $fund)
    {
        $this->data = $data;

        $this->client = $client;

        $this->fund = $fund;
    }

    /**
     * @throws CrimsException
     * @throws Exception
     */
    public function process()
    {
        $this->createInstruction();

        $this->processInstruction();

        return $this->transferInstruction;
    }


    public function createInstruction()
    {
        $this->transferInstruction = (new UnitFundInstructionsRepository())->transfer($this->client, $this->fund, $this->data);
    }

    /**
     * @return void
     * @throws CrimsException
     * @throws Exception
     */
    public function processInstruction()
    {
        if ($this->client->joint || $this->client->clientType->name == 'corporate') {
            $mandate = $this->client->present()->getSigningMandate;

//            $this->sendNotifications('joint_or_corporate');

            if ($mandate != 'Singly' && $mandate != 'Either to sign') {
                event(new UnitFundTransferPerformed($this->transferInstruction));

                return;
            }
        }

        $approval = ClientTransactionApproval::create([
            'client_id' => $this->client->id,
            'transaction_type' => 'create_unit_fund_transfer',
            'payload' => $this->data,
            'sent_by' => getSystemUser() ? getSystemUser()->id : null,
            'awaiting_stage_id' => 1
        ]);

        $this->transferInstruction->approval_id = $approval->id;

        $this->transferInstruction->save();

        $this->approval = $approval;

        $this->validateInstruction();

        $this->approveClientTransaction();

        $this->sendNotifications();
    }


    protected function validateInstruction()
    {
        $amount = (int)$this->transferInstruction->number;

        if ($amount < self::MIN_AMOUNT) {
            throw new CRIMSGeneralException("Transfer amount is less than minimum of "
                . number_format(self::MIN_AMOUNT));
        }

        if ($amount > self::MAX_AMOUNT) {
            throw new CRIMSGeneralException("Transfer amount is more than maximum of "
                . number_format(self::MAX_AMOUNT));
        }
    }


    /**
     * @throws Exception
     */
    protected function approveClientTransaction()
    {
        $approve = new Approval($this->approval);

        $approve->systemExecute();
    }

    public function sendNotifications($type = null)
    {
        list($sender, $receiver) = $this->getSenderReceiver();

        $messengerSender = $type ? $type : 'sender';

        // notify sender
        $sender->contact->email
            ? $this->sendEmail($sender, $receiver, $messengerSender)
            : $this->sendSMS($sender, $receiver, $messengerSender);

        // notify receiver
        if (is_null($type)) {
            $receiver->contact->email
                ? $this->sendEmail($sender, $receiver, 'receiver')
                : $this->sendSMS($sender, $receiver, 'receiver');
        }

        // notify operations team
        $this->sendEmail($sender, $receiver, 'ops-notify');
    }


    /**
     * @param Client $sender
     * @param Client $receiver
     * @param $type
     */
    protected function sendEmail(Client $sender, Client $receiver, $type)
    {
        $to[] = $type === 'ops-notify'
            ? 'operations@cytonn.com'
            : ($type === 'joint_or_corporate'
                ? $sender->contact->email
                : ($type === 'sender' ? $sender->contact->email : $receiver->contact->email));
        $from = $type === 'ops-notify'
            ? ['support@cytonn.com' => 'Cytonn CRIMS']
            : ['operations@cytonn.com' => 'Cytonn Investments'];

        $fa = ($type === 'sender' ? $sender->getLatestFA() : $receiver->getLatestFA());

        $cc = [];

        if ($fa) {
            $cc[] = $fa->email;
        }

        Mailer::compose()
            ->from($from)
            ->to($to)
            ->cc($cc)
            ->subject('Unit Fund Transfer Confirmation')
            ->view(
                'emails.unitization.unit_fund_transfer_notification',
                [
                    'sender' => $sender, 'receiver' => $receiver,
                    'type' => $type, 'units' => $this->data['number'],
                    'trans_ref' => $this->transferInstruction->id
                ]
            )->queue();
    }

    protected function sendSMS(Client $sender, Client $receiver, $type)
    {
        $senderPhone = Arr::first($sender->getContactPhoneNumbersArray());
        $receiverPhone = Arr::first($receiver->getContactPhoneNumbersArray());
        $to = null;

        if ($type === 'sender' && $senderPhone) {
            $to = $senderPhone;
        } else if ($type === 'receiver' && $receiverPhone) {
            $to = $receiverPhone;
        }

        if ($to) {
            dispatch(new SendMessages(
                $to,
                'transfer-units',
                ['sender' => $sender, 'receiver' => $receiver, 'type' => $type, 'units' => $this->data['number']]
            ));
        }
    }

    /**
     * @return array
     */
    public function getSenderReceiver(): array
    {
        $sender = Client::find($this->data['transferer_id']);

        $receiver = Client::find($this->data['transferee_id']);

        return array($sender, $receiver);
    }
}
