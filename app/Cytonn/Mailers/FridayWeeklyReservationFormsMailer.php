<?php


namespace Cytonn\Mailers;

class FridayWeeklyReservationFormsMailer extends Mailer
{
    public function prepareReservationForms($looInsuanceReminders)
    {
        $this->subject('LOO Insuance Reminders');

        $this->view('emails.realestate.friday_weekly_reservation_forms');

        $this->to('legal@cytonn.com');

        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];

        $this->data(['looInsuanceReminders'=>$looInsuanceReminders]);

        $this->queue = true;

        $this->send();
    }
}
