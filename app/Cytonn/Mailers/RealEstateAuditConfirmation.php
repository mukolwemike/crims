<?php

namespace Cytonn\Mailers;

use App\Cytonn\Models\Client;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class RealEstateAuditConfirmation extends Mailer
{
    public function sendToClient(
        Client $client,
        $confirmation,
        $sender,
        $date,
        \App\Cytonn\Models\RealEstateAuditConfirmation $item,
        $test = true
    ) {
        $this->from(['operations@cytonn.com' => 'Cytonn Investments']);

        if (!$test) {
            $this->to($client->getContactEmailsArray());
            $cc = ['statements@cytonn.com', 'jane.wanjira@ke.gt.com', 'tejal.bharadva@ke.gt.com'];

            $fa = $client->getLatestFA();

            if ($fa) {
                $cc [] = $fa->email;
            }
            $this->cc($cc);
        } else {
            $this->to(
                ['lmugo@cytonn.com', 'dosellu@cytonn.com', 'tkimathi@cytonn.com',
                    'mchaka@cytonn.com', 'snnganga@cytonn.com', 'fgitau@cytonn.com']
            );
        }

        $this->bcc(['mchaka@cytonn.com', 'tkimathi@cytonn.com', 'snnganga@cytonn.com', 'dosellu@cytonn.com']);

        $this->subject($item->product . ' ' . $date->format('Y') . ' Audit Confirmation Reminder - ' .
            ClientPresenter::presentJointFirstNames($client->id));

        $this->view('emails.audit.re_confirmation');

        $this->data(
            ['client' => $client, 'email_sender' => UserPresenter::presentSignOff($sender->id),
            'date' => $date, 'product' => $item->product, 'house_number' => $item->house_number]
        );

        $this->dataFile = $confirmation;

        $this->dataFileDetails = 'Real Estate Audit Confirmation Letter.pdf';

        $this->queue(false);

        $this->send();
    }
}
