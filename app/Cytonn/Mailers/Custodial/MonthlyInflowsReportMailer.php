<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Mailers\Custodial;

use Carbon\Carbon;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Mailers\Mailer;

class MonthlyInflowsReportMailer extends Mailer
{
    public function sendEmail(Carbon $startDate, Carbon $endDate, array $attachments, $user = null)
    {
        $mailer = new GeneralMailer();

        $email = $user ? $user->email : config('system.administrators');

        $mailer->to($email);

        $mailer->from('support@cytonn.com');

        $mailer->subject(
            '[CRIMS] Monthly Inflows Summary - ' . $startDate->copy()->toFormattedDateString() . ' to ' .
            $endDate->copy()->toFormattedDateString()
        );

        $mailer->file($attachments);

        $mailer->queue(false);

        $mailer->sendGeneralEmail('Here is the monthly inflows summary between ' .
            $startDate->copy()->toFormattedDateString() . ' and ' . $endDate->copy()->toFormattedDateString());
    }
}
