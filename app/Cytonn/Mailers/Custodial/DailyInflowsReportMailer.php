<?php

namespace Cytonn\Mailers\Custodial;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class DailyInflowsReportMailer extends Mailer
{
    public function sendEmail(Carbon $today, $structuredProducts, $realEstate, array $attachments, $user = null)
    {
        //        $this->to(['operations@cytonn.com']);
        //
        if ($user) {
            $email = $user->email;
        } else {
            $email = systemEmailGroups(['administrators', 'operations_admins']);
        }

        $this->to($email);
        $this->bcc(['mchaka@cytonn.com',  'tkimathi@cytonn.com']);
        $this->subject('[CRIMS] Inflows Summary - ' . $today->copy()->toFormattedDateString());
        $this->view('emails.custodial.daily_inflow_report');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->file($attachments);
        $this->data(['today'=>$today, 'structured_products'=>$structuredProducts, 'real_estate'=>$realEstate]);
        $this->queue(false);
        $this->send();
    }
}
