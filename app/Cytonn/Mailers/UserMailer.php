<?php
/**
 * Date: 8/21/15
 * Time: 12:52 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\User;

/**
 * Class UserMailer
 *
 * @package Cytonn\Mailers
 */
class UserMailer extends Mailer
{

    /**
     * Send an activation email to the user
     *
     * @param User $user
     */
    public function sendActivationEmail(User $user)
    {
        $this->subject = 'Activate your new Cytonn Account';
        $this->view = 'emails.auth.activation';
        $this->from = 'support@cytonn.com';
        $this->to = $user->email;
        $this->bcc = config('system.emails.super_admins');

        $activation_key = $user->authRepository->createActivationKey();

        $this->data = ['activation_key'=> $activation_key, 'username'=>$user->username, 'email'=>$this->to];

        $this->send();
    }

    public function sendClientActivationLink(ClientUser $user, $activation_key)
    {
        //send activation link
        $this->subject = 'Activate your new Cytonn Account';
        $this->view = 'emails.auth.client.activation';
        $this->from = ['operations@cytonn.com'=>'Cytonn Investments'];
        $this->to = $user->email;

        $this->data = [
            'activation_key' => $activation_key,
            'username' => $user->username,
            'email' => $this->to,
            'firstname' => $user->firstname
        ];

        $this->queue(true);

        $this->send();

        $url = env('CLIENT_DOMAIN', 'https://clients.cytonn.com');

        \SMS::queue(
            $user->phone_country_code.''.$user->phone,
            "Hello $user->firstname,".PHP_EOL.
            "We have provisioned your account on $url. Kindly check your email ($user->email) for instructions ".
            "on how to activate your account. ".
            "Your assigned username is ".$user->username. PHP_EOL.
            "Call 0709101200 in case of any queries."
        );
    }

    public function sendClientPasswordResetLink(ClientUser $user, $code)
    {
        $this->to($user->email);

        $this->view('emails.auth.client.reminder');

        $this->from(['support@cytonn.com' => 'Cytonn Investments']);

        $this->subject('Reset your Cytonn Password');

        $this->data(['token'=>$code, 'username'=>$user->username]);

        $this->queue(true);

        $this->send();
    }

    /**
     * Send a login OTP to a user
     *
     * @param User $user
     * @param $otp
     */
    public function sendOTPToUser(User $user, $otp)
    {
        $this->subject = 'Cytonn Investments Login Code';
        $this->view = 'emails.auth.otp';

        $this->data = ['otp'=>$otp, 'name'=>$user->firstname, 'username'=>$user->username];

        $this->to = $user->email;
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];

        $this->queue(false);

        $this->send();
    }

    /**
     * Email admin when a user  submits a password reset request
     *
     * @param $username
     * @param $email
     */
    public function informAdminOfPasswordRequest($username, $email)
    {
        $this->subject = 'A user has requested a password reset';

        $this->view = 'emails.auth.paswordreminder-admin';

        $this->data  = ['username'=>$username, 'email'=>$email];

        $this->to = config('system.emails.super_admins');

        $this->from('support@cytonn.com');

        $this->send();
    }


    public function sendPasswordResetlink($user, $code)
    {
        $this->to($user->email);

        $this->view('emails.auth.reminder');

        $this->from('support@cytonn.com');

        $this->subject('Reset your Cytonn CRIMS Password');

        $this->data(['token'=>$code, 'username'=>$user->username]);

        $this->queue(true);

        $this->bcc(config('system.emails.super_admins'));

        $this->send();
    }
}
