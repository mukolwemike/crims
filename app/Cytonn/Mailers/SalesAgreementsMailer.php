<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 23/01/2017
 * Time: 18:45
 */

namespace Cytonn\Mailers;

class SalesAgreementsMailer extends Mailer
{
    /**
     * Send email operations and legal when a sales agreement is uploaded
     *
     * @param $salesAgreement
     */
    public function sendEmailWhenSalesAgreementHasBeenUploaded($salesAgreement)
    {
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->to(['legal@cytonn.com']);
        $this->cc('operations@cytonn.com');
        $this->bcc('mchaka@cytonn.com');
        $this->view = 'emails.realestate.salesAgreement_uploaded';
        $this->subject = 'A Sales Agreement Has Been Signed';
        $this->data = ['agreement'=>$salesAgreement];

        $this->send();
    }
}
