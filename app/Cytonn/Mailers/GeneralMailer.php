<?php
/**
 * Date: 09/12/2015
 * Time: 11:28 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

class GeneralMailer extends Mailer
{
    public function sendGeneralEmail($content)
    {
        $this->view('emails.general');
        $this->data = ['content'=>$content];
        $this->send();
    }
}
