<?php
/**
 * Date: 17/05/2017
 * Time: 17:29
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class ForfeitureLetterMailer extends Mailer
{
    public function sendMail(UnitHolding $holding, User $sender = null)
    {
        $project = $holding->project;
        $unit = $holding->unit;
        $client = $holding->client;
        $cc = ['operations@cytonn.com', 'legal@cytonn.com', 'pm@cytonn.com'];

        if ($fa = $client->getLatestFa('real_estate')) {
            $cc[] = $fa->email;
        }

        $this->to($client->getContactEmailsArray());
        $this->cc($cc);
        $this->bcc(config('system.administrators'));

        $this->from(['operations@cytonn.com' => 'Cytonn Real Estate']);
        $this->subject(
            'Notice of Expiry of Offer to purchase a Unit in "' .
            $project->name . '" - ' . ClientPresenter::presentJointFirstNames($client->id)
        );

        $attachment = @$this->generateAttachment($client, $holding)->output();

        $email_sender = "Cytonn Real Estate";

        if ($sender) {
            $email_sender = UserPresenter::presentLetterClosingNoSignature($sender->id);
        }

        $this->data(
            $data = [
                'project' => $project,
                'unit' => $unit,
                'client' => $client,
                'email_sender' => $email_sender
            ]
        );

        $this->dataFile($attachment);
        $this->dataFileDetails('NOTICE OF EXPIRY OF RESERVATION.pdf');

        $this->view('emails.realestate.forfeitures');

        $this->send();

        $holding->update(['forfeiture_letter_sent_on' => Carbon::now()]);
    }

    public function generateAttachment(Client $client, UnitHolding $holding)
    {
        $sender = $holding->project->projectManager;

        return \PDF::loadView(
            'realestate.reports.forfeiture_letter',
            [
                'client' => $client,
                'sender' => $sender,
                'project' => $holding->project,
                'unit' => $holding->unit
            ]
        );
    }
}
