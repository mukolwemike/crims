<?php

namespace Cytonn\Mailers\Coop;

use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;

class CoopPaymentRemindersMailer extends Mailer
{
    public function sendReminder(Client $client, Carbon $dueDate)
    {
        $this->view('emails.coop.payment_reminder');
        $this->subject('Cytonn Cooperative Payment Reminder - '. ClientPresenter::presentShortName($client->id));
        $this->from = ['operations@cytonn.com'=>'Cytonn Cooperative'];
        //        $this->to($client->getContactEmailsArray());
        //        $cc = ['operations@cytonn.com', 'coop@cytonn.com'];

        $this->to(systemEmailGroups(['operations']));

        //        if($fa = $client->getLatestFA())
        //        {
        //            $cc[] = $fa->email;
        //        }
        //
        //        $this->cc($cc);

        $this->bcc([ 'mchaka@cytonn.com']);

        $productPlans = $client->plans;
        $email_sender = $client->fundManager->fullname;

        $this->data([
            'client'=>$client,
            'email_sender'=>$email_sender,
            'due_date'=>$dueDate,
            'product_plans'=>$productPlans
        ]);

        $this->send();
    }
}
