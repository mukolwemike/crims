<?php
/**
 * Date: 19/09/2016
 * Time: 12:03 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers\Coop;

use App\Cytonn\Models\ClientPayment;
use Cytonn\Coop\Memberships\BusinessConfirmationGenerator;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class BusinessConfirmationMailer extends Mailer
{
    public function sendMembershipConfirmation(ClientPayment $payment, array $payments)
    {
        $client = $payment->client;

        $this->to($client->getContactEmailsArray());
        $cc = ['operations@cytonn.com'];
        $fa = $client->getLatestFA();
        if ($fa) {
            $cc[] = $fa->email;
        }

        try {
            $user = \Auth::user();

            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management Limited.';
        }

        $this->cc($cc);
        $this->from(['coop@cytonn.com' => 'Cytonn Cooperative']);
        $this->subject('Business Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id));
        $this->view('emails.coop.membership_confirmation');
        $this->data(['email_sender' => $email_sender, 'client' => $client]);
        $this->queue(false);

        $approved_by = null;
        if ($payment->approval) {
            $approved_by = $payment->approval->approved_by;
        }

        $this->dataFile = (new BusinessConfirmationGenerator())->generate($payment, $payments, $approved_by)->output();

        $this->dataFileDetails = 'Membership Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id) .
            '.pdf';

        $this->send();

        (new BusinessConfirmationGenerator())->save($payment, $payments, \Auth::user());
    }
}
