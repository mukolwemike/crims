<?php
/**
 * Date: 04/06/2016
 * Time: 12:42 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers;

use App\Cytonn\Models\FundManager;
use Carbon\Carbon;
use Exception;

class SaturdayWeeklyReportMailer extends Mailer
{
    public $nextWeekStart = null;

    public $otherWeekStart = null;

    public function prepareReport(
        $nextWeekInvestments,
        $otherWeekInvestments,
        $nextWeekPortfolio,
        $otherWeekPortfolio,
        $analytics,
        Carbon $today
    ) {
        if (is_null($this->nextWeekStart) || is_null($this->otherWeekStart)) {
            throw new Exception('The next two weeks date must be set');
        }

        $this->subject('CHYS Summary ' . $today->copy()->toFormattedDateString());

        $this->view('emails.investment.saturdayweeklyreport');

        $this->to('operations@cytonn.com');
        //        $this->to('mchaka@cytonn.com');

        $this->bcc(['mchaka@cytonn.com']);

        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];

        $fundManagers = FundManager::where('report', 1)->get();

        $this->data(
            [
                'nextWeekInvestments' => $nextWeekInvestments,
                'otherWeekInvestments' => $otherWeekInvestments,
                'nextWeekPortfolio' => $nextWeekPortfolio,
                'otherWeekPortfolio' => $otherWeekPortfolio,
                'nextWeekStart' => $this->nextWeekStart,
                'otherWeekStart' => $this->otherWeekStart,
                'analytics' => $analytics,
                'fundManagers' => $fundManagers
            ]
        );

        $this->send();
    }
}
