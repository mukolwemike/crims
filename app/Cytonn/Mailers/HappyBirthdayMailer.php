<?php
/**
 * Date: 24/07/2016
 * Time: 5:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
namespace Cytonn\Mailers;

use Cytonn\Core\DataStructures\Collection;

/**
 * Class HappyBirthdayMailer
 *
 * @package Cytonn\Mailers
 */
/**
 * Class HappyBirthdayMailer
 *
 * @package Cytonn\Mailers
 */
class HappyBirthdayMailer extends Mailer
{
    /**
     * HappyBirthdayMailer constructor.
     */
    public function __construct()
    {
        $this->queue(true);
    }

    /**
     * Send the birthday email
     *
     * @param $email
     * @param $preferredName
     * @param null $fa
     */
    public function sendBirthdayNotification($email, $preferredName, $fa = null)
    {
        $this->from(['clientservices@cytonn.com'=>'Cytonn Investments']);

        $this->to($email);
        $this->cc(['operations@cytonn.com', 'clientservices@cytonn.com']);

        if($fa) {
            $this->bcc($fa);
        }

        $this->subject('Happy Birthday '. $preferredName);

        $this->view('emails.client.birthday_v2');

        $image = $this->getBdayImage();
        $line = $this->getBdayMessage();
        $this->data(['firstname'=>$preferredName, 'image'=>$image, 'no_content'=>true, 'line'=>$line]);

        $this->send();
    }

    /**
     * Send email with list of clients with birthday in 14 days
     *
     * @param $all
     * @param $start
     */
    public function birthdaysIn14Days($all, $file)
    {
        $this->from(['clientservices@cytonn.com'=>'Cytonn Investments']);

        $this->to('diaspora@cytonn.com');

        $this->subject('Clients with Birthdays in 14 Days');

        $this->view('emails.client.birthday_in_14_days');

        $this->file($file);

        $this->data(['clients'=>$all->flatten()]);

        $this->queue(false);

        $this->send();
    }

    /**
     * Get the image that will be used randomly
     *
     * @return mixed
     */
    private function getBdayImage()
    {
        $images = [
            'bday_1.jpg',
            'bday_2.jpg'
        ];

        return Collection::make($images)->random();
    }

    private function getBdayMessage()
    {
        $messages = [
            'It\'s not a year older, it\'s a year wiser! Celebrate your big day in style!',
            'Cytonn Investments wishes you health, happiness and love on your special day and always.'
        ];

        return Collection::make($messages)->random();
    }
}
