<?php


namespace Cytonn\Mailers;

use App\Cytonn\Models\RealestateLetterOfOffer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Realestate\Sale\LetterOfOffer;

/**
 * Class LOOMailer
 *
 * @package Cytonn\Mailers
 */
class LOOMailer extends Mailer
{
    /**
     * @param $payment
     */
    public function sendEmailToLegal($payment)
    {
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        //        $this->to('legal@cytonn.com');
        //        $this->cc('operations@cytonn.com');
        $this->to('mchaka@cytonn.com');
        $this->view = 'emails.realestate.loo';
        $this->subject = 'LOO Reminder';

        $this->data = [
            'payment' => $payment,
            'price' => $payment->holding->price(),
            'client_code' => $payment->holding->client->client_code,
            'client_id' => $payment->holding->client_id,
            'email' => $payment->holding->client->contact->email,
            'phone' => $payment->holding->client->contact->phone,
            'unit_number' => $payment->holding->unit->number
        ];
        $this->queue(true);
        $this->setLater(2);

        $this->send();
    }

    /**
     * @param $loo
     */
    public function sendEmailToLegalWhenLOOHasBeenUploaded($loo)
    {
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        $this->to(['legal@cytonn.com']);
        $this->cc('operations@cytonn.com');
        $this->bcc(config('system.administrators'));
        $this->view = 'emails.realestate.loo_uploaded';
        $this->subject = 'A Letter Of Offer Has Been Signed';
        $this->data = ['loo' => $loo];

        $this->send();
    }

    public function sendEmailToOpsWhenLOOHasBeenRejected($loo_reject)
    {
        $loo = $loo_reject->loo;
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        $this->to('operations@cytonn.com');
        $this->bcc(config('system.administrators'));
        $this->view = 'emails.realestate.loo_rejected';
        $this->subject = 'LOO Has Been Rejected';
        $this->data = ['loo' => $loo, 'loo_reject' => $loo_reject];

        $this->send();
    }

    /**
     * Mail the Loo to the client
     *
     * @param RealestateLetterOfOffer $loo
     * @param $loo_compliance_documents
     */
    public function sendLOOToClient(RealestateLetterOfOffer $loo, $loo_compliance_documents = null)
    {
        /*
         * @var \Client;
         */
        $client = $loo->holding->client;

        $this->view('emails.realestate.LOO.letter_of_offer');
        $this->subject('Letter of Offer - ' . ClientPresenter::presentJointFirstNames($client->id));
        $this->from = ['operations@cytonn.com' => 'Cytonn Real Estate'];
        $this->to($client->getContactEmailsArray());
        $cc = ['operations@cytonn.com', 'pm@cytonn.com', 'legal@cytonn.com'];

        if ($fa = $client->getLatestFa('realestate')) {
            $cc[] = $fa->email;
        }

        $this->cc($cc);

        $this->bcc(config('system.administrators'));

        try {
            $user = \Auth::user();
            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Real Estate.';
        }

        $data_file = ['Letter of Offer.pdf' => $loo->document->data()];
        if ($loo_compliance_documents) {
            foreach ($loo_compliance_documents as $doc) {
                $data_file[$doc->qualifiedFilename()] = $doc->data();
            }
        }

        $this->data(['client' => $client, 'email_sender' => $email_sender, 'loo' => $loo,
            'loo_compliance_documents' => $loo_compliance_documents]);
        $this->dataFile($data_file);
        $this->dataFileDetails('Letter of Offer.pdf');

        $loo->updateSent(\Auth::user());

        $this->send();
    }

    /**
     * Send LOO Form to PM, FA and Operations
     *
     * @param RealestateLetterOfOffer $loo
     */
    public function sendLOOForm(RealestateLetterOfOffer $loo)
    {
        $this->view('emails.realestate.LOO.letter_of_offer_authorization_form');
        $this->subject('LOO Requisition Form - ' . $loo->holding->project->name . ' - ' .
            ClientPresenter::presentJointFirstNames($loo->holding->client_id));
        $this->from = ['operations@cytonn.com' => 'Cytonn Real Estate'];
        $this->to(['pm@cytonn.com', 'legal@cytonn.com', 'rei@cytonn.com']);

        $cc = ['operations@cytonn.com'];

        if ($r = $loo->holding->commission->recipient) {
            $cc[] = $r->email;
        }

        $this->cc($cc);

        $this->bcc(config('system.administrators'));

        try {
            $user = \Auth::user();
            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Real Estate.';
        }

        $this->data(['loo' => $loo, 'email_sender' => $email_sender]);
        $this->dataFile((new LetterOfOffer())->generateLooForm($loo->holding)->stream());
        $this->dataFileDetails('LOO Authorization Form.pdf');

        $loo->setAuthorizationFormSendDate();

        $this->send();
    }
}
