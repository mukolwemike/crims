<?php
/**
 * Date: 26/01/2017
 * Time: 15:53
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers;

trait ExcelMailer
{

    /**
     * @param $subject
     * @param $msg
     * @param $filename
     * @param array    $recipients
     */
    protected function sendExcel($subject, $msg, $filename, array $recipients = [])
    {
        if (is_array($filename)) {
            $path = array_map(
                function ($f) {
                    return  storage_path().'/exports/'.$f.'.xlsx';
                },
                $filename
            );
        } else {
            $path =  storage_path().'/exports/'.$filename.'.xlsx';
        }

        $mailer = new GeneralMailer();
        $mailer->to($recipients);
        $mailer->bcc(\config('system.administrators'));
        $mailer->from('support@cytonn.com');
        $mailer->subject($subject);
        $mailer->file($path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail($msg);

        \File::delete($path);
    }
}
