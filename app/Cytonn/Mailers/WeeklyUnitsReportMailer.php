<?php


namespace Cytonn\Mailers;

class WeeklyUnitsReportMailer extends Mailer
{
    public function prepareWeeklyUnitsReport($projects, $loos, $salesAgreements, $bookings)
    {
        $this->subject('Weekly Units Report');
        $this->view('emails.realestate.weekly_units');
        $this->to('creteam@cytonn.com');
        $this->bcc(systemEmailGroups(['administrators']));
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->data(
            [
            'projects'                  =>  $projects,
            'loos'                      =>  $loos,
            'salesAgreements'           =>  $salesAgreements,
            'bookings'                  =>  $bookings,
            ]
        );
        $this->queue = true;

        $this->send();
    }
}
