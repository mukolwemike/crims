<?php
/**
 * Date: 18/11/2015
 * Time: 2:01 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

use Carbon\Carbon;

class PortfolioMaturityMailer extends Mailer
{
    /**
     * Sends maturity notification for investments maturing in a week
     *
     * @param $investmentsGroupedByFundManager
     */
    public function inAWeek($investmentsGroupedByFundManager)
    {
        $this->to(systemEmailGroups(['operations']));

        $this->bcc(config('system.administrators'));

        $this->data(
            ['investmentsGroupedByFundManager' => $investmentsGroupedByFundManager, 'period' => 'in the next 7 days']
        );

        $this->view('emails.portfolio.maturitynotification');

        $this->subject('Funds Maturity Notification: ' . Carbon::now()->toDateString());

        $this->send();
    }


    /**
     * Sends maturity notification for investments maturing today
     *
     * @param $investmentsGroupedByFundManager
     */
    public function today($investmentsGroupedByFundManager)
    {
        $recipients = systemEmailGroups(['operations']);

        $recipients = array_merge($recipients, ['cmugendi@cytonn.com', 'vodendo@cytonn.com', 'dgitau@cytonn.com']) ;

        $this->to($recipients);

        $this->bcc(config('system.administrators'));

        $this->data(['investmentsGroupedByFundManager' => $investmentsGroupedByFundManager, 'period' => 'today']);

        $this->view('emails.portfolio.maturitynotification');

        $this->subject('Today\'s Funds Maturity Notification ' . Carbon::today()->toDateString());

        $this->send();
    }
}
