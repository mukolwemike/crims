<?php
/**
 * Date: 16/11/2015
 * Time: 3:06 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

/**
 * Class ClientMaturityNotificationMailer
 *
 * @package Cytonn\Mailers
 */
class ClientMaturityNotificationMailer extends Mailer
{
    public $sendToClient = true;
    /**
     * Process queue item to send notification
     *
     * @param $job
     * @param $data
     */
    public function queueNotifyWeekBefore($job, $data)
    {
        $investment = ClientInvestment::findOrFail($data['investment_id']);

        $approval = ClientTransactionApproval::findOrFail($data['approval_id']);

        $sender = User::find($approval->sent_by);

        $this->notifyWeekBefore([$investment], $sender);

        $job->delete();
    }

    /**
     * Send the notification email
     *
     * @param $investment
     * @param null $sender
     */
    public function notifyWeekBefore(array $investmentGroup, $sender = null)
    {
        $from = ['operations@cytonn.com' => 'Cytonn Investments'];

        $client = $investmentGroup[0]->client;

        $to = [];

        $cc = ['operations@cytonn.com'];

        $fa = $client->getLatestFA();

        if ($fa) {
            $cc[] = $fa->email;
        }

        if ($this->sendToClient) {
            $to = $client->getContactEmailsArray();
        }

        $this->cc($cc);

        $subject = 'Maturity Notification - ' . ClientPresenter::presentJointFirstNames($client->id) .
            ' - Client Code ' . $client->client_code;

        $this->notify($investmentGroup, $to, $from, $subject, false, $sender);
    }

    public function notifyFailure(array $investmentGroup)
    {
        $to = 'operations@cytonn.com';

        $from = ['operations@cytonn.com' => 'Cytonn Investments'];

        $subject = 'Maturity Notification Failure - ' .
            ClientPresenter::presentJointFirstNames($investmentGroup[0]->client_id);

        $this->notify($investmentGroup, $to, $from, $subject, true, null);
    }

    protected function notify(array $investmentGroup, $to, $from, $subject, $error = false, $sender = null)
    {
        $this->from = $from;

        $this->to($to);

        $this->subject($subject);

        try {
            if (is_null($sender)) {
                throw (new \Exception());
            }

            $email_sender = UserPresenter::presentLetterClosingNoSignature($sender->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management PLC.';
        }

        $this->view = 'emails.investment.clientmaturitynotification';

        $this->bcc(config('system.administrators'));

        $investment = $investmentGroup[0];

        $product = $investment->product;
        
        $maturityDate = Carbon::parse($investment->maturity_date);

        $clientTotal = $investmentGroup[0]->client->repo->getTodayInvestedAmountForProduct($product, $maturityDate);

        $productRates = $product->repo->productRates($clientTotal, $maturityDate);

        $scheduleDetail = null;

        if ($investment->product->present()->isSeip) {
            $scheduleDetail = $investment->repo->getInvestmentPaymentScheduleDetail();
        } else {
            $instructionFilePath = $product->fund_manager_id == 3 ?
                storage_path() . '/resources/CREPN_Instructions_Form.pdf' :
                storage_path() . '/resources/CHYS_Instructions_Form.pdf';

            $this->file($instructionFilePath);
        }

        $this->data = [
            'investment' => $investment,
            'investmentGroup' => $investmentGroup,
            'email_sender' => $email_sender,
            'error' => $error,
            'productRates' => $productRates,
            'scheduleDetail' => $scheduleDetail
        ];

        $this->queue = false; //don't queue this email

        $this->send();
    }
}
