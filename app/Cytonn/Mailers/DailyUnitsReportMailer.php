<?php


namespace Cytonn\Mailers;

class DailyUnitsReportMailer extends Mailer
{
    public function prepareDailyUnitsReport($soldUnits, $remainingUnits)
    {
        $this->subject('Daily Units Report');
        $this->view('emails.realestate.daily_units');
        $this->to('creteam@cytonn.com');
        $this->bcc(systemEmailGroups(['administrators']));
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->data(['soldUnits'=>$soldUnits, 'remainingUnits'=>$remainingUnits]);
        $this->queue = true;
        $this->send();
    }
}
