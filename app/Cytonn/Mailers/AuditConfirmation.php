<?php
/**
 * Date: 12/08/2016
 * Time: 10:16 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers;

use App\Cytonn\Models\Client;
use Cytonn\Mailers\Client\DocumentPasswordMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class AuditConfirmation extends Mailer
{
    public function sendToClient(
        Client $client,
        $confirmation,
        $sender,
        $date,
        \App\Cytonn\Models\AuditConfirmation $item,
        $test = true
    ) {
        $this->from(['operations@cytonn.com' => 'Cytonn Investments']);

        if (!$test) {
            $this->to($client->getContactEmailsArray());

            $this->cc(['statements@cytonn.com', $client->getLatestFA()->email]);
        } else {
            $this->to([
                'lmugo@cytonn.com',
                'gweru@cytonn.com',
                'mchaka@cytonn.com',
                'tkimathi@cytonn.com'
            ]);
        }

        $this->bcc(['mchaka@cytonn.com', 'tkimathi@cytonn.com', 'gweru@cytonn.com']);

        $subject = $item->product . ' ' . $date->format('Y') . ' Audit Confirmation - ' .
            ClientPresenter::presentJointFirstNames($client->id);

        $this->subject($subject);

        $this->view('emails.audit.confirmation');

        $this->data(
            [
                'client' => $client, 'email_sender' => UserPresenter::presentSignOff($sender->id), 'date' => $date,
                'currency' => $item->currency, 'amount' => $item->balance, 'spv' => $item->spv,
                'product' => $item->product
            ]
        );

        $this->dataFile = $confirmation;

        $this->dataFileDetails = 'Audit Confirmation Letter.pdf';

        $this->send();

        if (!$test) {
            $this->sendPassword($client, $subject, $item->spv);
        }
    }

    private function sendPassword(Client $client, $subject, $sign_off)
    {
        (new DocumentPasswordMailer($client))->dispatch($sign_off, $subject);
    }
}
