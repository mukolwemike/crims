<?php
/**
 * Date: 16/11/2015
 * Time: 2:53 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

use Carbon\Carbon;

class MaturityReminderMailer extends Mailer
{
    public function remind($investments)
    {
        $this->to(systemEmailGroups(['operations', 'operations_mgt', 'operations_admins']));
        $this->bcc(systemEmailGroups(['administrators']));

        $this->data(['investments'=>$investments]);

        $this->view('emails.investment.maturityreminder');

        $this->subject('Investments Maturity Reminder: '.Carbon::now()->toFormattedDateString());

        $this->send();
    }
}
