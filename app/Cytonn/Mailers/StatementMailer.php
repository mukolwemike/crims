<?php
/**
 * Date: 9/19/15
 * Time: 2:33 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\StatementCampaign;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class StatementMailer extends Mailer
{
    public function sendStatementToClient(
        Client $client,
        $statements,
        $statementDate,
        $sender = null,
        StatementCampaign $campaign = null
    ) {
        $this->from = ['operations@cytonn.com' => 'Cytonn Investments'];

        $client_emails = $client->getContactEmailsArray();

        $this->to($client_emails);

        $investment = $client->investments()->latest()->first();

        if ($investment) {
            $productName = $investment->product->shortName;
        } else {
            $portfolioSecurity = $client->portfolioSecurities()->latest()->first();

            $productName = ($portfolioSecurity) ? $portfolioSecurity->repo->getProductName() : '';
        }

        $cc = [];

//        $cc = systemEmailGroups(['statements']);
        $fa = $client->getLatestFA();
        if ($fa) {
            $cc[] = $fa->email;
        }

        $this->cc($cc);
        $this->subject = 'INVESTMENTS STATEMENT - ' . ClientPresenter::presentJointFirstNames($client->id);
        $this->view = 'emails.investment.statement';

        try {
            if (is_null($sender)) {
                throw new \Exception;
            }

            $email_sender = UserPresenter::presentLetterClosingNoSignature($sender->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management Limited.';
        }

        $this->data = ['client' => $client, 'productName' => $productName,
            'stmt_date' => new Carbon($statementDate), 'email_sender' => $email_sender, 'campaign' => $campaign];

        $this->dataFile = $statements;

        $this->queue = false; //don't queue this email

        $name =  $client->client_code.'~'.$client->id.'~'
            .time().'~'.ClientPresenter::presentJointFirstNames($client->id);

        $this->saveEmail($name, 'Sp_Statements');

        $this->send();
    }

    public function report(array $campaigns)
    {
        $this->to(['operations@cytonn.com','operations@serianiasset.com']);
        $this->from(['support@cytonn.com' => 'Cytonn CRIMS']);
        $this->bcc(config('system.administrators'));

        $this->subject('Report on Statements Sent - ' . head($campaigns)->first()->send_date->toFormattedDateString());

        $campaigns = collect($campaigns)->flatten()->all();

        $campaign = collect($campaigns)
            ->map(function ($campaign) {
                return [
                    'name' => $campaign::TYPE,
                    'products' => $campaign->repo->productSummary(),
                    'total' => $campaign->items()->count()
                ];
            })
            ->groupBy('name')
            ->map(function ($group, $key) {
                $total = $group->sum(function ($c) {
                    return $c['total'];
                });

                $products = $group->map(function ($campaign) {
                    return $campaign['products'];
                })->flatten(1);

                return [
                    'name' => $key,
                    'products' => $products,
                    'total' => $total
                ];
            })
        ;


        $this->view('emails.investment.statements_report', ['campaigns' => $campaign]);
        $this->send();

        collect($campaigns)->each(function ($c) {
            $c->update(['report_sent_on' => Carbon::now()]);
        });
    }
}
