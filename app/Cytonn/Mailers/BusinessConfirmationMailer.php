<?php
/**
 * Date: 31/10/2015
 * Time: 12:54 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\User;
use Cytonn\Investment\InvestmentPaymentSchedules\InvestmentPaymentScheduleGenerator;
use Cytonn\Mailers\Client\DocumentPasswordMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Reporting\BusinessConfirmationGenerator;
use Cytonn\Users\UserClientRepository;

class BusinessConfirmationMailer extends Mailer
{
    //TODO Test
    public function sendBusinessConfirmationToClient($client, $payload, $from, User $user = null)
    {

        $this->from =
            [config('system.emails.operations_group') => env('CRIMS_NAME', 'Cytonn Investments')];

        $this->subject('');

        $client_emails = $client->getContactEmailsArray();

        $generated_title = '';

        $investment = ClientInvestment::find($payload['investment_id']);

        if (is_null($investment)) {
            $rollover = false;
            $this->subject = '[' . $from . ']: Business Confirmation - ' .
                ClientPresenter::presentJointFirstNames($client->id) . $generated_title;
        } elseif ($investment->investmentType->name == 'rollover') {
            $rollover = true;
            $this->subject = '[' . $from . ']: Rollover Business Confirmation - ' .
                ClientPresenter::presentJointFirstNames($client->id) . $generated_title;
        } else {
            $rollover = false;
            $this->subject = '[' . $from . ']: Business Confirmation - ' .
                ClientPresenter::presentJointFirstNames($client->id) . $generated_title;
        }

        try {
            $user = \Auth::user();

            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management Limited.';
        }

        $cc = [config('system.emails.operations_group')];
        $fa = $client->getLatestFA();
        if ($fa) {
            $cc[] = $fa->email;
        }

        $this->to($client_emails);
        $this->cc($cc); //cc operations and the fa
        $this->bcc(config('system.administrators'));

        $this->view = 'emails.investment.businessconfirmation';

        $this->data = ['client' => $client, 'payload' => $payload, 'investment' => $investment,
            'rollover' => $rollover, 'email_sender' => $email_sender];
        $this->data = [
            'client' => $client,
            'payload' => $payload,
            'investment' => $investment,
            'rollover' => $rollover,
            'email_sender' => $email_sender
        ];

        $this->dataFile = (new BusinessConfirmationGenerator())->setProtectDocument(true)
            ->generateBusinessConfirmation($client, $payload)
            ->output();

        if ($rollover) {
            $this->dataFileDetails =
                'Rollover Business Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id) . '.pdf';
        } else {
            $this->dataFileDetails =
                'Business Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id) . '.pdf';
        }

        if ($investment->product->present()->isSeip) {
            $paymentSchedule = (new InvestmentPaymentScheduleGenerator())
                ->setProtectDocument(true)
                ->generate($investment)->output();

            $paymentScheduleName = 'Payment Schedule - ' . ClientPresenter::presentJointFirstNames($client->id) . '.pdf';

            $this->dataFile = [
                $this->dataFileDetails => $this->dataFile,
                $paymentScheduleName => $paymentSchedule
            ];
        }

        $this->queue = true;

        $this->send();

//        $sign_off = $investment->product->fundManager->fullname;
//
//        $this->sendPassword($client, $this->subject, $sign_off);

        if ($client->investments()->count() == 1) {
            $this->provisionClient($client);
        }
    }

    /**
     * @param Client $client
     * @param $subject
     * @param $sign_off
     */
    private function sendPassword(Client $client, $subject, $sign_off)
    {
        (new DocumentPasswordMailer($client))->dispatch($sign_off, $subject);
    }

    /**
     * @param Client $client
     */
    private function provisionClient(Client $client)
    {
        (new UserClientRepository())->provisionForClient($client);
    }
}
