<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  10/12/2018
 *
 */

namespace Cytonn\Mailers\CRM;

use Illuminate\Support\Facades\Mail;

class CrmMailers
{
    /*
     * Send a general CRM Mailer
     */
    public static function generalReport($data)
    {
        Mail::send('emails.reports.crm_general_report', $data, function ($message) use ($data) {
            $message->to($data['to'])
                ->from('crmsystem@cytonn.com', 'Cytonn CRM')
                ->subject($data['subject']);

            if (isset($data['path'])) {
                $message->attach($data['path']);
            }

            if (isset($data['cc'])) {
                $message->cc($data['cc']);
            }

            if (isset($data['bcc'])) {
                $message->bcc($data['bcc']);
            }
        });
    }
}
