<?php

namespace App\Cytonn\Mailers;

use App\Cytonn\Auth\User;
use Cytonn\Mailers\Mailer;

class ContactUsMailer extends Mailer
{
    public function sendContactUs(User $user, $subject, $message)
    {
        $this->view('emails.general.contactus');

        $this->data([ 'user'=>$user, 'subject'=>$subject, 'msg'=>$message ]);

        $this->subject('[CRIMS] [Message] '.$subject);

        $this->to(['operations@cytonn.com', 'clientservices@cytonn.com']);

        $this->cc('support@cytonn.com');

        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);

        $this->send();
    }
}
