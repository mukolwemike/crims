<?php

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class ClientUpdateBankAccountMailer extends Mailer
{
    public function notifyRequest(Client $client)
    {
        $this->to('operations@cytonn.com');

        $this->subject('Client Bank Account Update Notification on ' . Carbon::today()->toFormattedDateString());

        $this->view('emails.client.update_bank_Account', ['client' => $client]);

        $this->send();
    }

    public function notifyApproval(Client $client)
    {
        $account = ClientBankAccount::active()
            ->where('client_id', $client->id)
            ->latest()
            ->first();

        $this->to($client->getContactEmailsArray());

        $fa = $client ? $client->getLatestFA() : null;

        if ($fa) {
            $this->cc($fa->email);
        }

        $this->subject('Client Bank Account Update Notification on ' . Carbon::today()->toFormattedDateString());

        $this->view('emails.client.client_update_bank_Account', ['client' => $client, 'account' => $account]);

        $this->send();
    }
}
