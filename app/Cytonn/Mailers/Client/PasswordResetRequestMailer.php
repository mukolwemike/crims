<?php

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Auth\ClientPasswordReminder;
use App\Cytonn\Models\ClientUser as User;
use function bcrypt;
use Cytonn\Mailers\Mailer;

class PasswordResetRequestMailer extends Mailer
{
    public function sendResetRequest(User $user)
    {
        $this->to(['operations@cytonn.com', 'support@cytonn.com']);

        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);

        $this->subject($user->firstname.' requested a password reset');

        $this->view('emails.auth.password_reset');

        $this->data(['user'=>$user]);

        $this->send();
    }
    
    public function sendPasswordResetLink(User $user)
    {
        $this->to([$user->email]);
    
        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);
    
        $this->subject('Reset your crims password');
    
        $this->view('emails.auth.client_password_reset');
    
        $this->data(['username'=>$user->username, 'token' => $this->generateToken($user)]);
    
        $this->send();
    }
    
    private function generateToken($user)
    {
        $code = sha1(md5(time()));
    
        $reminder = new ClientPasswordReminder();
        $reminder->token = bcrypt($code);
        $reminder->user_id = $user->id;
        $reminder->save();
        
        return $code;
    }
}
