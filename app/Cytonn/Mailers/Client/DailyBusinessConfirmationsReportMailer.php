<?php

namespace Cytonn\Mailers\Client;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class DailyBusinessConfirmationsReportMailer extends Mailer
{
    public function sendEmail($file_path, Carbon $today, User $user = null)
    {
        $this->to(['operations@cytonn.com']);

        if ($user) {
            $this->to($user->email);
        }

        $this->bcc(['mchaka@cytonn.com']);
        $this->subject('[CRIMS] Business Confirmations Summary - ' . $today->toFormattedDateString());
        $this->view('emails.client.daily_business_confirmations_report');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->file($file_path);
        $this->data(['file_path'=>$file_path, 'today'=>$today]);
        $this->queue(false);
        $this->send();
    }
}
