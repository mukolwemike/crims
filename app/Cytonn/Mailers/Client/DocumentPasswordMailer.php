<?php
/**
 * Date: 28/02/2018
 * Time: 10:55
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers\Client;

use App\Cytonn\Models\Client;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;

class DocumentPasswordMailer
{
    protected $client;

    private $key;

    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->key = 'password_mail_for_client_id_'.$client->id;
    }

    /**
     * @param string $sign_off
     * @param null $subject
     * @throws \Exception
     */
    public function dispatch($sign_off = "Cytonn Investments", $subject = null)
    {
        if ($this->alreadySent()) {
            return;
        }

        if (!$subject || (strlen($subject) < 3)) {
            $subject = "Password for Documents from Cytonn";
        }

        $company = str_contains($sign_off, 'Cytonn Asset Manager') ? 'asset_manager' : null;

        $sender = 'operations@cytonn.com';

        if ($this->client->clientType->name == 'corporate') {
            $names = ClientPresenter::presentContactPersonShortName($this->client->id);
        } else {
            $names = ClientPresenter::presentSalutation($this->client->id);
        }

        Mail::compose()
            ->from([$sender => $sign_off])
            ->to($this->client->getContactEmailsArray())
            ->bcc(array_merge(systemEmailGroups(['statements']), systemEmailGroups(['super_admins'])))
            ->subject($subject)
            ->view('emails.client.password', [
                'client_names' => $names,
                'client' => $this->client,
                'sign_off' => $sign_off,
                'company' => $company
            ])->queue();

        cache([$this->key => true], Carbon::now()->addDays(30));
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function alreadySent()
    {
        return $this->client && cache()->has($this->key);
    }
}
