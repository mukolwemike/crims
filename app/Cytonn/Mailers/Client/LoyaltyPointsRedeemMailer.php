<?php

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Models\RewardVoucher;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Support\Facades\Storage;

class LoyaltyPointsRedeemMailer extends Mailer
{
    public function notifyForRequest(Client $client, LoyaltyPointsRedeemInstructions $instruction)
    {
        $voucherIds = \GuzzleHttp\json_decode($instruction->payload)->voucher_ids;

        $voucherNames = '';

        foreach ($voucherIds as $id) {
            $voucher = RewardVoucher::find($id);

            $voucherNames .= "{$voucher->name}, ";
        }

        $amount = AmountPresenter::currency($instruction->amount, false, 2);

        $this->to($client->getContactEmailsArray());

        $this->from(['clientservices@cytonn.com' => 'Cytonn Investments']);

        $this->subject('Loyalty Points Redeem Request Notification');

        $this->view('emails.client.loyalty_points_redeem_request_notification');

        $this->data([
            'client' => $client,
            'amount' => $amount,
            'voucherNames' => $voucherNames
        ]);

        $this->queue(false);

        $this->send();
    }

    public function notify(Client $client, LoyaltyPointsRedeemInstructions $instruction)
    {
        $amount = AmountPresenter::currency($instruction->amount, false, 2);

        $this->toClientService($client, $instruction, $amount);

        $this->toClient($client, $instruction, $amount);
    }

    private function toClientService(Client $client, $instruction, $amount)
    {
        $payload = \GuzzleHttp\json_decode($instruction->payload);

        $voucherNames = '';

        if (isset($payload->voucher_ids)) {
            foreach ($payload->voucher_ids as $id) {
                $voucher = RewardVoucher::find($id);

                $voucherNames .= "{$voucher->name}, ";
            }
        } elseif (isset($payload->voucher_id)) {
            $voucher = RewardVoucher::find($payload->voucher_id);

            $voucherNames .= "{$voucher->name}, ";
        }

        $totalPoints = AmountPresenter::currency($client->calculateLoyalty()
            ->getTotalPoints(), false, 2);

        $this->to('clientservices@cytonn.com');

        $this->subject('Loyalty Points Redeem Notification');

        $this->view('emails.client.loyalty_points_redeem_notification');

        $this->data([
            'client' => $client,
            'amount' => $amount,
            'totalPoints' => $totalPoints,
            'refId' => $instruction->id,
            'instruction' => $instruction,
            'voucherNames' => $voucherNames
        ]);

        $this->queue(false);

        $this->send();
    }

    private function toClient(Client $client, $instruction, $amount)
    {
        $payload = \GuzzleHttp\json_decode($instruction->payload);

        $voucherDoc = [];

        if (isset($payload->voucher_ids)) {
            foreach ($payload->voucher_ids as $id) {
                $voucher = RewardVoucher::find($id);

                $voucherDoc[$voucher->name] = $voucher->document;
            }
        } elseif (isset($payload->voucher_id)) {
            $voucher = RewardVoucher::find($payload->voucher_id);

            $voucherDoc = [$voucher->name => $voucher->document];
        }

        $this->to($client->getContactEmailsArray());

        $this->from(['clientservices@cytonn.com' => 'Cytonn Investments']);

        $this->subject('Loyalty Points Redeem Notification');

        $this->view('emails.client.loyalty_points_redeem_client_notification');

        if (count($voucherDoc)) {
            $docArray = [];
            foreach ($voucherDoc as $key => $doc) {
                if ($doc) {
                    $docArray[$doc->filename] = Storage::get($doc->path());
                }
            }

            $this->dataFile($docArray);
        }

        $totalPoints = AmountPresenter::currency($client->calculateLoyalty()
            ->getTotalPoints(), false, 2);

        $this->data([
            'client' => $client,
            'amount' => $amount,
            'voucherDoc' => $voucherDoc,
            'totalPoints' => $totalPoints
        ]);

        $this->queue(false);

        $this->send();
    }
}
