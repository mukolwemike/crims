<?php

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Models\ClientUser as User;
use Cytonn\Mailers\Mailer;

class ContactUsMailer extends Mailer
{
    public function sendContactUs(User $user, $subject, $message)
    {
        $this->view('emails.general.contactus');

        $this->data(['user' => $user, 'subject' => $subject, 'msg' => $message]);

        $this->subject('[CRIMS] [Message] ' . $subject);

        $this->to(['operations@cytonn.com', 'clientservices@cytonn.com']);

        $this->replyTo($user->email);

        $this->cc('support@cytonn.com');

        $this->from(['support@cytonn.com' => 'Cytonn CRIMS']);

        $this->send();
    }

    public function sendClientReferralNotification($data)
    {
        $this->view('emails.client.client_referral_notification');

        $this->data(['data' => $data]);

        $this->subject($data['client_name'] . ' Invites you to become a Cytonnaire');

        $this->to([$data['email']]);

        $this->bcc('crimssystem@cytonn.com');

        $this->from(['support@cytonn.com' => 'Cytonn CRIMS']);

        $this->send();
    }
}
