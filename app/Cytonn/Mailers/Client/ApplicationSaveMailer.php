<?php

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use Cytonn\Mailers\Mailer;

class ApplicationSaveMailer extends Mailer
{
    public function sendOnApplicationSave(ClientFilledInvestmentApplication $application)
    {
        $this->subject('A new investment application has been saved');

        if ($application->app_start_email) {
            $this->subject('An existing investment application has been updated');
        }

        $this->view('emails.applications.application_save');
        $this->data(['application'=>$application]);
        $this->to(['operations@cytonn.com', 'diaspora@cytonn.com']);
        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);
//        $this->bcc('mchaka@cytonn.com');
        $this->send();
    }

    public function sendResumeLinkToClient(ClientFilledInvestmentApplication $application)
    {
        $this->subject('Your Application with Cytonn Investments');

        $this->view('emails.applications.application_save_client');
        $this->data(['application'=>$application, 'uuid'=>$application->uuid]);
        $this->to([$application->email]);
        $this->from(['operations@cytonn.com'=>'Cytonn Investments']);
        $this->cc(['operations@cytonn.com']);
//        $this->bcc('mchaka@cytonn.com');

        $this->send();
    }
}
