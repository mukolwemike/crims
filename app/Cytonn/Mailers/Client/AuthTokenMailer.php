<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 1/15/19
 * Time: 3:49 PM
 */

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Models\ClientUser;
use Cytonn\Mailers\Mailer;

class AuthTokenMailer extends Mailer
{
    public function sendMail(ClientUser $user, $token)
    {
        $this->to([ $user->email ]);

        $this->from([ 'operations@cytonn.com' => 'Cytonn CRIMS' ]);

        $this->subject('Auth Token for Cytonn Crims');

        $this->view('emails.auth.client_transaction');

        $this->data([
            'user' => $user,
            'token' => $token
        ]);

        $this->send();
    }
}
