<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/9/18
 * Time: 3:36 PM
 */

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class ComplianceReminderMailer extends Mailer
{
    public function sendNotification(Client $client, UnitFund $fund)
    {
        $this->view('emails.client.compliance_notification');

        $sender = str_contains($fund->manager->name, 'Seriani')
            ? 'operations@serianiasset.com'
            : 'operations@cytonn.com';

        try {
            $user = auth()->user();
            $emailSender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $emailSender = $fund->manager->fullname;
        }

        $this->data([
            'checklist' => $client->repo->requiredPendingKYCDocuments(),
            'client' => $client,
            'subject' => 'Compliance Notification',
            'fund' => $fund,
            'emailSender'=> $emailSender,
            'companyAddress' => str_contains($fund->manager->name, 'Seriani') ? 'seriani' : null
        ]);

        $this->subject('KYC Documents - ' . ClientPresenter::presentJointFirstNames($client->id));
        $this->from = [ $sender => ucfirst($fund->name) ];
        $this->to($client->getContactEmailsArray());
        $this->cc([ $sender ]);
        $this->bcc(['mchaka@cytonn.com', 'operations@serianiasset.com']);

        $this->send();
    }
}
