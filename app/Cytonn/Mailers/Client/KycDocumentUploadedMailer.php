<?php

namespace App\Cytonn\Mailers\Client;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientUploadedKyc;
use Cytonn\Mailers\Mailer;

class KycDocumentUploadedMailer extends Mailer
{
    public function sendOnDocumentUploaded(ClientFilledInvestmentApplication $application, ClientUploadedKyc $document)
    {
        $this->queue(false);

        $this->view('emails.applications.kyc_upload');
        $this->data(['application'=>$application, 'document'=>$document]);
        $this->subject('A new KYC document has been uploaded');
        $this->to(['operations@cytonn.com']);
        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);
        $this->bcc('mchaka@cytonn.com');
        $this->send();
    }
}
