<?php

namespace Cytonn\Mailers\Client;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;

class TransactionNotificationMailer extends Mailer
{
    public function notify(ClientTransactionApproval $approval, array $details)
    {
        $listeners = $this->getNotificationListeners($approval->client);

        if (is_null($listeners) || count($listeners) == 0) {
            return false;
        }

        $this->to($listeners);
        $this->subject('[CRIMS] Transaction Notification - ' .
            ClientPresenter::presentJointFullNames($approval->client_id));
        $this->from('support@cytonn.com');

        $this->view('emails.client.approvals.transaction_notification');

        $this->data(['approval' => $approval, 'details' => $details]);

        $this->send();

        return true;
    }

    private function getNotificationListeners(Client $client)
    {
        return $client->notification_emails;
    }
}
