<?php

namespace Cytonn\Mailers;

//use App\Mail\Mail;
use App\Mail\Mail;
use Carbon\Carbon;

abstract class Mailer
{
    protected $message;

    protected $later = 0;

    protected $replyTo;

    protected $swiftMessageClosures = [];

    public function setLater($later)
    {
        $this->later = $later;
    }

    public $from = ['support@cytonn.com' => 'Cytonn Investments'];

    public function from($input)
    {
        $this->from = $input;
    }

    public $to;

    public function to($email)
    {
        $this->to = $email;
    }

    public $cc;

    public function cc($input)
    {
        $this->cc = $input;
    }

    public $bcc;

    public function bcc($input)
    {
        $this->bcc = $input;
    }

    public function replyTo($email)
    {
        $this->replyTo = $email;
    }

    /**
     * @var
     */
    public $subject;

    public function subject($input)
    {
        $this->subject = $input;
    }

    public $view;

    public function view($input, array $data = [])
    {
        $this->view = $input;

        if (count($data) > 0) {
            $this->data($data);
        }
    }

    /**
     * @var
     */
    public $data;

    /**
     * @param $input
     */
    public function data($input)
    {
        $this->data = $input;
    }

    /**
     * @var
     */
    public $file;

    /**
     * @param $input
     */
    public function file($input)
    {
        $this->file = $input;
    }

    /**
     * @var
     */
    public $file_data;

    /**
     * @param $input
     */
    public function fileData($input)
    {
        $this->file_data = $input;
    }

    /**
     * @var
     */
    public $dataFile;

    /**
     * @param $input
     */
    public function dataFile($input)
    {
        $this->dataFile = $input;
    }

    /**
     * @var
     */
    public $dataFileDetails;

    /**
     * @param $input
     */
    public function dataFileDetails($input)
    {
        $this->dataFileDetails = $input;
    }

    /**
     * @var
     */
    public $queue;

    /**
     * @param $true
     */
    public function queue($true)
    {
        $this->queue = $true;
    }


    /**
     * Send the email using the params provided
     */
    protected function send()
    {

        try {
            if ($this->later > 0) {
                $this->sendLater();

                return;
            }

            if ($this->queue) {
                $this->queueNow();

                return;
            }
        } catch (\Throwable $e) {
            $this->sendNow();

            return;
        }

        $this->sendNow();
    }

    /**
     * Send the email later
     */
    private function sendLater()
    {
        $this->prepareMessage()->later(Carbon::now()->addMinutes($this->later));
    }

    /**
     * Queue the email
     */
    private function queueNow()
    {
        $this->prepareMessage()->queue();
    }

    /**
     * Send out the email immediately
     */
    private function sendNow()
    {
        $this->prepareMessage()->send();
    }

    public function prepareMessage()
    {
        $mail = Mail::compose()
            ->view($this->view, $this->data)
            ->from($this->from)
            ->to($this->to);

        if ($this->replyTo) {
            $mail = $mail->replyTo($this->replyTo);
        }

        if ($this->cc) {
            $mail = $mail->cc($this->cc);
        }

        if ($this->bcc) {
            $mail = $mail->bcc($this->bcc);
        }

        if ($this->subject) {
            $mail = $mail->subject($this->subject);
        }

        $mail = $this->processAttachments($mail);

        $mail->withSwiftMessage(function () {
        });

        foreach ($this->swiftMessageClosures as $closure) {
            $mail->withSwiftMessage($closure);
        }

        return $mail;
    }

    protected function saveEmail($filename, $groupName)
    {
        $this->swiftMessageClosures[] = function (\Swift_Message $message) use ($filename, $groupName) {
            $contents = $message->toString();

            $now = Carbon::now();
            $d = $now->day;
            $m = $now->month;
            $y = $now->year;

            \Storage::disk(config('filesystems.default'))
                ->put("emails/$groupName/$y/$m/$d/$filename".".eml", $contents);
        };
    }

    private function processAttachments($mail)
    {
        if (!is_null($this->file)) {
            $mail = $mail->attach($this->file);
        }

        //attach from a data object, can take array of files (associative array with filenames as the key
        if (!is_null($this->dataFile)) {
            if (is_array($this->dataFile)) {
                $mail = $mail->attachData($this->dataFile);
            } else {
                $mail->attachData([$this->dataFileDetails => $this->dataFile]);
            }
        }

        return $mail;
    }

    private function getSwiftMessage()
    {
    }
}
