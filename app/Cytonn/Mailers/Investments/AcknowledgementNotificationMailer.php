<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 04/12/2018
 * Time: 10:30
 */

namespace App\Cytonn\Mailers\Investments;

use App\Cytonn\Models\ClientInvestment;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Mails\Mailer;

class AcknowledgementNotificationMailer
{
    public function sendEmail(ClientInvestment $investment, $pendingKycs)
    {
        $cc = ['operations@cytonn.com'];

        $fa = $investment->client->getLatestFA();

        if ($fa) {
            $cc[] = $fa->email;
        }

        $email_sender = UserPresenter::presentLetterClosingNoSignature(\Auth::user()->id);

        Mailer::compose()
            ->to($investment->client->getContactEmailsArray())
            ->cc($cc)
            ->subject('Business Confirmation Acknowledgement Notification ')
            ->view('emails.investment.acknowledgement_notification', [
                'investment' => $investment,
                'pendingKycs' => $pendingKycs,
                'client' => $investment->client,
                'email_sender' => $email_sender,
            ])
            ->send();
    }
}
