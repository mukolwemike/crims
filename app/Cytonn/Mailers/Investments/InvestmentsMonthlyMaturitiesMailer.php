<?php

namespace Cytonn\Mailers\Investments;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class InvestmentsMonthlyMaturitiesMailer extends Mailer
{
    public function sendEmail($maturities_grouped_by_product, $file_path, Carbon $today, Carbon $next_month)
    {
        $this->subject(
            'Maturities For The Month: ' . $today->copy()->toFormattedDateString() . ' to ' .
            $next_month->copy()->toFormattedDateString()
        );
        $this->to(['distributionmgt@cytonn.com']);
        $this->cc(['operations@cytonn.com']);
        $this->bcc(config('system.administrators'));
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];

        $this->view('emails.investment.investments_monthly_maturities');

        $this->file($file_path);
        $this->data(['maturities_grouped_by_product' => $maturities_grouped_by_product,
            'today' => $today, 'next_month' => $next_month]);

        $this->send();
    }
}
