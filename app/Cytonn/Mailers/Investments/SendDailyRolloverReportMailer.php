<?php

namespace Cytonn\Mailers\Investments;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class SendDailyRolloverReportMailer extends Mailer
{
    public function sendEmail($rollovers, $file_path, Carbon $today)
    {
        $this->to(['operations@cytonn.com', 'distributionmgt@cytonn.com']);
        $this->bcc(['mchaka@cytonn.com']);
        $this->subject('[CRIMS] Rollover Summary - ' . $today->toFormattedDateString());
        $this->view('emails.investment.daily_rollover_report');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->file($file_path);
        $this->data(['rollovers'=>$rollovers, 'file_path'=>$file_path, 'today'=>$today]);
        $this->queue(false);
        $this->send();
    }
}
