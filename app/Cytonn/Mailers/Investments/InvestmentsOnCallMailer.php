<?php


namespace Cytonn\Mailers\Investments;

use Cytonn\Mailers\Mailer;

class InvestmentsOnCallMailer extends Mailer
{
    public function sendEmail($portfolio_investments, $client_investments)
    {
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->to(systemEmailGroups(['operations_admins']));
        $this->bcc([ 'mchaka@cytonn.com']);
        $this->view = 'emails.investment.investment_maturity_report';
        $this->subject = 'Report of Investments with Extended Maturity';

        $this->data = [
            'portfolio_investments'     =>  $portfolio_investments,
            'client_investments'        =>  $client_investments,
        ];
        $this->queue = false;

        $this->send();
    }
}
