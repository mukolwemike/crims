<?php

namespace Cytonn\Mailers\Investments;

use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;

class InterestPaymentsMailer extends Mailer
{
    public function sendEmailReport($interest_payments_grouped_by_product, $file_path, $start_date, $end_date)
    {
        $this->view('emails.investment.interest_payments');
        $this->subject(
            'Interest Payments Reports - ' . DatePresenter::formatDate($start_date) . ' to ' .
            DatePresenter::formatDate($end_date)
        );
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        $this->to(['mchaka@cytonn.com']);
        $this->file($file_path);

        $this->data(['interest_payments_grouped_by_product' => $interest_payments_grouped_by_product,
            'start_date' => $start_date, 'end_date' => $end_date]);
        $this->send();
    }

    public function notifyClient(Client $client, $schedules, $date, $user)
    {
        $date = Carbon::parse($date);
        $schedules = Collection::make($schedules);

        $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);

        $this->view('emails.investment.interest_paid_notification');
        $this->data(['client' => $client, 'schedules' => $schedules, 'date' => $date, 'email_sender' => $email_sender]);

        $this->to($client->getContactEmailsArray());
        $this->cc(['operations@cytonn.com', $client->getLatestFA()->email]);
        $this->bcc('mchaka@cytonn.com');
        $this->subject('Interest Payment Notification for ' . ucwords($date->format('M Y')));

        $this->send();
    }
}
