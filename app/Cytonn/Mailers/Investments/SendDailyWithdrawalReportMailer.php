<?php


namespace Cytonn\Mailers\Investments;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class SendDailyWithdrawalReportMailer extends Mailer
{
    public function sendEmail($withdrawals, $file_path, Carbon $start, Carbon $end, $monthly = false, $user_id = null)
    {
        if ($user_id) {
            $user = User::findOrFail($user_id);
            $email = $user ? $user->email : [];
        } else {
            $email = ['operations@cytonn.com', 'cmusau@cytonn.com', 'pkagwi@cytonn.com'];
        }

        $this->to($email);
        $this->bcc(config('system.emails.administrators'));
        if ($monthly) {
            $this->subject(
                '[CRIMS] Monthly Rollover and Withdrawal Summary - ' . $start->toFormattedDateString()
            ) . ' to ' . $end->toFormattedDateString();
        } else {
            $this->subject('[CRIMS] Daily Rollover and Withdrawal Summary - ' . $start->toFormattedDateString());
        }
        $this->view('emails.investment.daily_withdrawal_report');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->file($file_path);
        $this->data(['withdrawals'=>$withdrawals, 'file_path'=>$file_path,
            'start'=>$start, 'end'=>$end, 'monthly'=>$monthly]);
        $this->queue(false);
        $this->send();
    }
}
