<?php

namespace Cytonn\Mailers\Investments;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class PortfolioMaturityReminderMailer extends Mailer
{
    public function sendEmail($investments)
    {
        $this->to(systemEmailGroups(['operations', 'operations_admins']));
        $this->bcc(systemEmailGroups(['administrators']));

        $this->data(['investments'=>$investments]);
        $this->view('emails.investment.portfolio_maturity_reminder');
        $this->subject('Portfolio Maturity Reminder: ' . Carbon::now()->toFormattedDateString());

        $this->send();
    }
}
