<?php

namespace Cytonn\Mailers\Investments;

use App\Cytonn\Models\PortfolioInvestor;
use Cytonn\Mailers\Mailer;

class DepositPlacementInstructionMailer extends Mailer
{
    public function sendEmail(PortfolioInvestor $investor, $file)
    {
        $this->from = ['support@cytonn.com'=>'CRIMS'];

        $this->to(\Auth::user()->email);

        $this->bcc([ 'mchaka@cytonn.com']);

        $this->subject = 'Deposit Placement Instructions - '. $investor->name;

        $this->view = 'emails.portfolio.deposit_placement_instructions';

        $this->data = ['investor'=>$investor];

        $this->dataFile = $file;

        $this->dataFileDetails($investor->name . ' - Deposit Placement.pdf');

        $this->queue = false;

        $this->send();
    }
}
