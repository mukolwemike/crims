<?php

namespace App\Cytonn\Mailers;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use Cytonn\Mailers\Mailer;

class InvestmentApplicationCompleteMailer extends Mailer
{
    public function sendEmailToClient(ClientFilledInvestmentApplication $application)
    {
        $this->to([$application->email]);
        $this->from(['operations@cytonn.com'=>'Cytonn Investments']);
        $this->cc(['operations@cytonn.com']);
//        $this->bcc(['mchaka@cytonn.com']);
        $this->view('emails.applications.complete_client');
        $this->data(['application'=>$application, 'compliant'=>$application->repo->checkKycComplete()]);
        $this->subject('Application successful');

        $this->send();
    }

    public function sendEmailToStaff(ClientFilledInvestmentApplication $application)
    {
        $this->to(['operations@cytonn.com']);
        $this->from(['support@cytonn.com'=>'Cytonn Investments']);
        $this->bcc('mchaka@cytonn.com');
        $this->view('emails.applications.complete_staff');
        $this->data(['application'=>$application]);

        //$this->send(); TODO not in use at the moment
    }
}
