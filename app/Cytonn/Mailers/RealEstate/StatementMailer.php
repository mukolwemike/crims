<?php
/**
 * Date: 9/19/15
 * Time: 2:33 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealestateStatementCampaign;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class StatementMailer extends Mailer
{
    public function sendStatementToClient(
        Client $client,
        $projects,
        $statements,
        $statementDate,
        $sender = null,
        RealestateStatementCampaign $campaign = null
    ) {
        $this->from = ['operations@cytonn.com' => 'Cytonn Real Estate'];

        $this->to($client->getContactEmailsArray());
        $cc = [];
//        $cc = systemEmailGroups(['statements']);

        try {
            $fa_email = $client->getLatestFA('realestate')->email;
        } catch (\Exception $e) {
            $fa_email = null;
        }

        if ($fa_email) {
            $cc[] = $fa_email;
        }

        $this->cc($cc);

        $this->bcc(systemEmailGroups(['administrators']));

        $this->subject = 'Real Estate Statement - ' . ClientPresenter::presentJointFirstNames($client->id);
        $this->view = 'emails.realestate.statement';

        try {
            if (is_null($sender)) {
                throw new \Exception;
            }

            $email_sender = UserPresenter::presentLetterClosingNoSignature($sender);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Real Estate';
        }

        $this->data = ['client' => $client, 'projects' => $projects, 'statementDate' => new Carbon($statementDate),
            'email_sender' => $email_sender, 'campaign' => $campaign];

        $this->dataFile = $statements;

        $this->dataFileDetails([]);

        $this->queue = false; //don't queue this email

        $name =  $client->client_code.'~'.$client->id.'~'
            .time().'~'.ClientPresenter::presentJointFirstNames($client->id);

        $this->saveEmail($name, 'RE_Statements');

        $this->send();
    }
}
