<?php

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Mailers\Mailer;

class TrancheCheckMailer extends Mailer
{
    public function sendEmail(RealEstateUnitTranche $tranche, RealestateUnit $unit, $content)
    {
        $sold =
            function (RealEstateUnitTranche $tranche, RealEstatePaymentPlan $paymentPlan, RealestateUnitSize $size) {
                return UnitHolding::whereHas(
                    'unit',
                    function ($unit) use ($size) {
                        $unit->where('size_id', $size->id);
                    }
                )
                    ->where('tranche_id', $tranche->id)
                    ->where('payment_plan_id', $paymentPlan->id)
                    ->where('active', 1)
                    ->count();
            };

        $findSize = function ($id) {
            return RealestateUnit::findOrFail($id);
        };

        $project = $tranche->project;

        $this->view('emails.realestate.tranche');
        $this->subject('[' . $project->name . ']' . 'Tranche Alert - ' . $tranche->name . "'s " .
            $unit->size->name . 's');
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        $this->to(['operations@cytonn.com', 'pm@cytonn.com', 'distribution@cytonn.com']);
        $this->bcc(config('system.administrators'));
        $this->data(
            ['tranche' => $tranche, 'unit' => $unit, 'content' => $content,
                'project' => $tranche->project, 'sold' => $sold, 'findSize' => $findSize]
        );
        $this->send();
    }
}
