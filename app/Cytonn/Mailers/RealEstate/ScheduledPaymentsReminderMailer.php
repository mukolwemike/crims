<?php

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Project;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class ScheduledPaymentsReminderMailer extends Mailer
{
    public function sendEmail(Client $client, $holdings)
    {
        dd('here');

        $this->view('emails.realestate.scheduled.index');
        $this->subject('Scheduled Payment Reminder - ' . ClientPresenter::presentJointFirstNames($client->id));
        $this->from = ['operations@cytonn.com' => 'Cytonn Real Estate'];
        $client_emails = [];

        array_push($client_emails, $client->contact->email);
        is_array($client->emails) ?: $client->emails = [];
        foreach ($client->emails as $email) {
            array_push($client_emails, $email);
        }

        $this->to($client_emails);
        $this->cc(['operations@cytonn.com', 'distribution@cytonn.com']);

        try {
            $user = \Auth::user();
            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management Limited.';
        }

        $this->data(
            ['client' => $client, 'email_sender' => $email_sender, 'holdings' => $holdings,
            'projects' => $this->getProjects($holdings)]
        );
        $this->send();
    }

    protected function getProjects($holdings)
    {
        return Project::whereHas(
            'units',
            function ($units) use ($holdings) {
                $units->whereHas(
                    'holdings',
                    function ($holding) use ($holdings) {
                        $holding->whereIn('id', $holdings->lists('id'));
                    }
                );
            }
        )->get();
    }
}
