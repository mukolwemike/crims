<?php

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\User;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class OverdueRealEstatePaymentRemindersForClientsMailer extends Mailer
{
    public function sendEmail($schedules_arr, Client $client, $start_date, $end_date, User $sender, $projects)
    {
        $this->view('emails.realestate.client_overdue_payments_reminder');

        $this->subject('Reminder for Overdue Real Estate Payments - ' .
            ClientPresenter::presentShortName($client->id));

        $this->from = ['operations@cytonn.com' => 'Cytonn Real Estate'];

        $this->to($client->getContactEmailsArray());
        $cc = ['operations@cytonn.com'];

        if ($fa = $client->getLatestFA('realestate')) {
            $cc[] = $fa->email;
        }
        $this->cc($cc);
        $this->bcc(config('system.administrators'));

        try {
            $email_sender = UserPresenter::presentLetterClosingNoSignature($sender->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management Limited.';
        }

        if (count($schedules_arr) == 0) {
            return false;
        }

        $this->data(
            ['schedules_arr' => $schedules_arr, 'client' => $client, 'email_sender' =>
            $email_sender, 'start_date' => $start_date, 'end_date' => $end_date, 'projects' => $projects]
        );

        $this->queue(false);
        $this->send();

        return true;
    }
}
