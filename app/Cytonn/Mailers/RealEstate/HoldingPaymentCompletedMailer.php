<?php

namespace App\Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\UnitHolding;
use Cytonn\Mailers\Mailer;

class HoldingPaymentCompletedMailer extends Mailer
{
    public function notify(UnitHolding $holding)
    {
        $client = $holding->client;

        $this->to('legal@cytonn.com');

        $this->subject('Client Completed Holding Payment Notification');

        $this->view(
            'emails.realestate.holding_payment_complete',
            ['holding' => $holding, 'client' => $client]
        );

        $this->send();
    }
}
