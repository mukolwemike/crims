<?php
/**
 * Date: 06/07/2016
 * Time: 12:20 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealEstatePayment;
use Cytonn\Mailers\Client\DocumentPasswordMailer;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class BusinessConfirmationMailer extends Mailer
{
    public function sendBC(Client $client, $bc, RealEstatePayment $payment)
    {
        $this->queue(true);

        $this->view('emails.realestate.businessconfirmation');

        $this->subject('[MBC]: Business Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id));

        $this->from = ['operations@cytonn.com' => 'Cytonn Real Estate'];

        $this->to($client->getContactEmailsArray());

        $cc = ['operations@cytonn.com', 'rei@cytonn.com'];
        $fa = $client->getLatestFA('realestate');
        if ($fa) {
            $cc[] = $fa->email;
        }

        $this->cc($cc); //cc operations and the fa

        $this->bcc(config('system.administrators'));

        try {
            $user = \Auth::user();

            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management Limited.';
        }

        $this->data(
            ['client' => $client, 'email_sender' => $email_sender, 'unit' => $payment->holding->unit,
            'project' => $payment->holding->project, 'payment' => $payment]
        );

        $this->dataFile($bc->output());
        $this->dataFileDetails('Business Confirmation.pdf');
        $this->send();

//        $is_first = RealEstatePayment::whereHas('holding', function ($holding) use ($client) {
//            $holding->where('client_id', $client->id);
//        })->count() == 1;

//        if ($is_first) {
//            (new DocumentPasswordMailer($client))->dispatch("Cytonn Real Estate", $this->subject);
//        }
    }
}
