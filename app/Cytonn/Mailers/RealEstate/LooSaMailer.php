<?php

namespace Cytonn\Mailers\RealEstate;

use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\DatePresenter;

class LooSaMailer extends Mailer
{
    public function sendEmail(
        $loos_sent_grouped_by_project,
        $loos_signed_grouped_by_project,
        $sas_signed_grouped_by_project,
        $today
    ) {
        $this->view('emails.realestate.loo_sa_report');
        $this->subject('LOO/SA Reporting - ' . DatePresenter::formatDate($today->copy()->toDateString()));
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        $this->to(['operations@cytonn.com', 'legal@cytonn.com']);
        $this->bcc(['mchaka@cytonn.com']);
        $this->data([
                'loos_sent_grouped_by_project' => $loos_sent_grouped_by_project,
                'loos_signed_grouped_by_project' => $loos_signed_grouped_by_project,
                'sas_signed_grouped_by_project' => $sas_signed_grouped_by_project,
                'today' => $today]);
        $this->send();
    }
}
