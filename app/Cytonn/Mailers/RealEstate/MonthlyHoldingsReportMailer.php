<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 20/03/2018
 * Time: 12:23
 */

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Presenters\General\DatePresenter;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class MonthlyHoldingsReportMailer extends Mailer
{
    public function sendEmail($holdings, $file_path)
    {
        $this->view('emails.realestate.units_loo_sa_report');
        $this->subject('Units with Pending Sales Agreements -  ' .
            DatePresenter::formatDate((new Carbon())->today()->toDateString()));
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->to(['operations@cytonn.com', 'pm@cytonn.com', 'legal@cytonn.com', 'distribution@cytonn.com']);
        $this->bcc(systemEmailGroups(['administrators']));
        $this->data([ 'holdings' => $holdings, 'today'=> Carbon::today() ]);
        $this->file($file_path);
        $this->send();
    }
}
