<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 07/02/2019
 * Time: 12:43
 */

namespace App\Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class ClientMakePaymentNotificationMailer extends Mailer
{
    public function notifyClientPayment(RealEstateInstruction $instruction)
    {
        $client = $instruction->client;
        $holding = UnitHolding::findOrFail($instruction->holding_id)->first();
        $unit_price = $holding->price();
        $total_paid = $holding->repo->unitTotalPaid($holding);
        $balance = $unit_price - $total_paid;

        $input = $instruction->payload;
        $schedule = RealEstatePaymentSchedule::find($input->schedule_id);
        $payment = $input;

        $fa_email = $client->getLatestFA();

        $this->to([$client->getContactEmailsArray()]);
        $cc = $fa_email ? ['operations@cytonn.com', $fa_email->email] : ['operations@cytonn.com'];
        $this->cc($cc);
        $this->bcc(config('system.administrators'));
        $this->from(['operations@cytonn.com' => 'Cytonn Investments']);

        $this->subject('Unit Holding Payment Notification made on ' . Carbon::today()->toFormattedDateString());
        $this->view('emails.realestate.action.payment_notification_client', [
            'holding' => $holding,
            'client' => $client,
            'payment' => $payment,
            'schedule' => $schedule,
            'balance' => $balance,
            'total_paid' => $total_paid,
            'unit_price' => $unit_price,
            'instruction' => $instruction
        ]);

        $this->send();
    }

    public function notifyPayment(RealEstateInstruction $instruction)
    {
        $client = $instruction->client;
        $holding = UnitHolding::findOrFail($instruction->holding_id)->first();
        $unit_price = $holding->price();
        $total_paid = $holding->repo->unitTotalPaid($holding);
        $balance = $unit_price - $total_paid;

        $input = $instruction->payload;

        $schedule = RealEstatePaymentSchedule::find($input->schedule_id);
        $payment = $input;
        $fa_email = $client->getLatestFA();

        $this->to(['operations@cytonn.com']);
        if ($fa_email) {
            $this->cc($fa_email->email);
        }
        $this->bcc(config('system.administrators'));
        $this->from(['support@cytonn.com' => 'Cytonn CRIMS']);

        $this->subject('Unit Holding Payment Notification made on ' . Carbon::today()->toFormattedDateString());
        $this->view('emails.realestate.action.payment_notification', [
            'holding' => $holding,
            'client' => $client,
            'payment' => $payment,
            'schedule' => $schedule,
            'balance' => $balance,
            'total_paid' => $total_paid,
            'unit_price' => $unit_price,
            'instruction' => $instruction
        ]);

        $this->send();
    }
}
