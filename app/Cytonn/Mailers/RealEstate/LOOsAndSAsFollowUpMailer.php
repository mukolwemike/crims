<?php

namespace Cytonn\Mailers\RealEstate;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\DatePresenter;

class LOOsAndSAsFollowUpMailer extends Mailer
{
    public function sendEmail($loos_sas_follow_up)
    {
        $this->subject('LOOs & SAs Follow Up - ' . DatePresenter::formatDate((new Carbon())->today()->toDateString()));
        $this->view('emails.realestate.loos_and_sas_follow_up');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->to(['operations@cytonn.com', 'pm@cytonn.com', 'legal@cytonn.com', 'clientservices@cytonn.com', 'rei@cytonn.com']);
        $this->bcc(config('system.administrators'));
        $this->data(['loos_sas_follow_up'=>$loos_sas_follow_up]);

        $this->send();
    }
}
