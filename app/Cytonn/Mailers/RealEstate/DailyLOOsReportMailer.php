<?php

namespace Cytonn\Mailers\RealEstate;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\DatePresenter;

class DailyLOOsReportMailer extends Mailer
{
    public function sendEmail(
        $loos_today_grouped_by_project,
        $loos_this_week_grouped_by_project,
        $loos_this_month_grouped_by_project
    ) {
        $this->view('emails.realestate.daily_loos_report');
        $this->subject('LOOs Sent to Clients - Daily Report ' .
            DatePresenter::formatDate((new Carbon())->today()->toDateString()));
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        $this->to(['operations@cytonn.com', 'pm@cytonn.com', 'legal@cytonn.com']);
        $this->bcc(['mchaka@cytonn.com']);
        $this->data(
            [
                'loos_today_grouped_by_project' => $loos_today_grouped_by_project,
                'loos_this_week_grouped_by_project' => $loos_this_week_grouped_by_project,
                'loos_this_month_grouped_by_project' => $loos_this_month_grouped_by_project
            ]
        );
        $this->send();
    }
}
