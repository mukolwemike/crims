<?php

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\DatePresenter;

class ClientsTrancheReportMailer extends Mailer
{
    public function sendEmail(array $file_paths, User $user)
    {
        $this->view('emails.realestate.clients_tranche_report');
        $this->subject('Clients Tranche Report - ' .
            DatePresenter::formatDate(Carbon::today()->toFormattedDateString()));
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];
        $this->to($user->email);
        $this->bcc(config('system.administrators'));
        $this->file($file_paths);
        $this->data([]);
        $this->send();
    }
}
