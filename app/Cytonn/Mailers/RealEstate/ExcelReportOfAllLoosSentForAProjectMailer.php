<?php


namespace Cytonn\Mailers\RealEstate;

use Cytonn\Mailers\Mailer;

class ExcelReportOfAllLoosSentForAProjectMailer extends Mailer
{
    public function sendEmail($file_path)
    {
        $this->to(['pm@cytonn.com', 'legal@cytonn.com', 'operations@cytonn.com']);
        //        $this->to('mchaka@cytonn.com');
        $this->bcc([ 'mchaka@cytonn.com']);
        $this->subject('Sent Letters of Offer');
        $this->view('emails.realestate.LOO.loos_sent_for_a_project');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->file($file_path);
        $this->data([]);
        $this->queue(false);
        $this->send();
    }
}
