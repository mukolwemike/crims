<?php

namespace Cytonn\Mailers\RealEstate;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\DatePresenter;

class TrancheReportsForEachProjectMailer extends Mailer
{
    public function sendEmail($tranches_grouped_by_project, $file)
    {
        $this->view('emails.realestate.tranche_reports');
        $this->subject('Tranche Reports - ' . Carbon::today()->toFormattedDateString());
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->to(['operations@cytonn.com', 'pm@cytonn.com', 'legal@cytonn.com']);
        $this->bcc(['mchaka@cytonn.com']);
        $this->dataFile = $file;

        $this->data(['tranches_grouped_by_project'=>$tranches_grouped_by_project]);
        $this->send();
    }
}
