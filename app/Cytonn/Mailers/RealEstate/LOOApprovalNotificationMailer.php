<?php

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\RealestateLetterOfOffer;
use Cytonn\Mailers\Mailer;

class LOOApprovalNotificationMailer extends Mailer
{
    public function sendEmail(RealestateLetterOfOffer $loo)
    {
        $this->view('emails.realestate.loo_approval_notification');
        $this->subject('LOO Approval Notification');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];

        $this->to(['operations@cytonn.com', 'pm@cytonn.com', 'legal@cytonn.com']);
        $this->bcc('mchaka@cytonn.com');
        $this->data(['loo'=>$loo]);
        $this->send();
    }
}
