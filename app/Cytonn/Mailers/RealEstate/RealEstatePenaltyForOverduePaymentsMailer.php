<?php

namespace Cytonn\Mailers\RealEstate;

use Cytonn\Mailers\Mailer;

class RealEstatePenaltyForOverduePaymentsMailer extends Mailer
{
    public function sendEmail($file)
    {
        $this->view('emails.realestate.scheduled.penalty');
        $this->subject('Real Estate Penalty for Overdue Payments');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];

        //        $this->to(['operations@cytonn.com']);
        //        $this->bcc([ 'mchaka@cytonn.com']);
        $this->to('mchaka@cytonn.com');

        $this->data([]);
        $this->file($file);
        $this->send();
    }
}
