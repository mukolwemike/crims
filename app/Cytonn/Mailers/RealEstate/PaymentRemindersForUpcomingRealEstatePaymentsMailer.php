<?php

namespace Cytonn\Mailers\RealEstate;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\DatePresenter;

class PaymentRemindersForUpcomingRealEstatePaymentsMailer extends Mailer
{
    public function sendEmail($reminders, $file_path)
    {
        $this->view('emails.realestate.payment_reminders_realestate');
        $this->subject('Payment Reminders for Upcoming Real Estate Payments ' .
            DatePresenter::formatDate((new Carbon())->today()->toDateString()));
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->to(systemEmailGroups(['operations_admins']));
        $this->bcc(config('system.administrators'));
        $this->data(['reminders'=>$reminders]);
        $this->file($file_path);
        $this->send();
    }
}
