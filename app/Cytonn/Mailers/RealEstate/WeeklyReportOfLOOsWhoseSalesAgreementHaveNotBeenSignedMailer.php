<?php


namespace Cytonn\Mailers\RealEstate;

use Cytonn\Mailers\Mailer;

class WeeklyReportOfLOOsWhoseSalesAgreementHaveNotBeenSignedMailer extends Mailer
{
    public function sendEmail($loos)
    {
        $this->subject('Weekly LOO Report Whose Sales Agreement Have Not Been Signed');
        $this->view('emails.realestate.LOO.sales_agreement_not_signed');
        $this->to('legal@cytonn.com');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->data(['loos'=>$loos]);
        $this->queue = true;
        $this->send();
    }
}
