<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 4/2/19
 * Time: 4:40 PM
 */

namespace App\Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\RealEstate\RealEstateContractVariation;
use App\Cytonn\Reporting\ContractVariationGenerator;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class ContractVariationMailer extends Mailer
{
    public function sendEmail(RealEstateContractVariation $variation)
    {
        $client = $variation->loo->holding->client;

        $this->view('emails.realestate.contract_variations.show');
        $this->subject('Contract Variation - ' . ClientPresenter::presentJointFirstNames($client->id));
        $this->from = ['operations@cytonn.com' => 'Cytonn Real Estate'];
        $this->to($client->getContactEmailsArray());
        $cc = ['operations@cytonn.com', 'pm@cytonn.com', 'legal@cytonn.com'];

        if ($fa = $client->getLatestFa('realestate')) {
            $cc[] = $fa->email;
        }

        $this->cc($cc);

        $this->bcc(config('system.administrators'));

        try {
            $user = \Auth::user();
            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Real Estate.';
        }

        $generator = new ContractVariationGenerator();

        $file = $generator->generate($variation)->stream()->content();

        $data_file = ['Contract Variation.pdf' => $file];

        $this->dataFile($data_file);

        $this->data(['client' => $client, 'email_sender' => $email_sender, 'variation' => $variation ]);

        $this->dataFileDetails('Contract Variation.pdf');

        $variation->updateSent(\Auth::user());

        $this->send();
    }
}
