<?php


namespace Cytonn\Mailers\RealEstate;

use Cytonn\Mailers\Mailer;

class WeeklyReportOfLOOsNotSentToAdvocateMailer extends Mailer
{
    public function sendEmail($loos)
    {
        $this->subject('Weekly Report of LOOs Not Sent To Lawyer');
        $this->view('emails.realestate.LOO.loos_not_sent_to_advocate');
        $this->to('legal@cytonn.com');
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->data(['loos'=>$loos]);
        $this->queue = true;
        $this->send();
    }
}
