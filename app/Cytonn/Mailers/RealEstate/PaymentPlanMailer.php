<?php
/**
 * Date: 06/10/2016
 * Time: 12:52
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers\RealEstate;

use App\Cytonn\Models\Client;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class PaymentPlanMailer extends Mailer
{
    public function sendPlan(Client $client, $paymentPlan)
    {
        //        $this->to('mchaka@cytonn.com');
        $this->to($client->getContactEmailsArray());
        $this->from(['operations@cytonn.com'=>'Cytonn Real Estate']);
        //        $cc = ['operations@cytonn.com'];
        //
        //        if($fa = $client->getLatestFA('realestate'))
        //        {
        //            $cc[] = $fa->email;
        //        }
        //
        //        $this->cc($cc);

        try {
            $user = \Auth::user();

            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = '';
        }

        $this->bcc(systemEmailGroups(['operations_admins']));
        $this->subject('Real Estate Payment Plan - '.ClientPresenter::presentShortName($client->id));
        $this->data(['email_sender'=>$email_sender, 'client'=>$client, 'projects'=>$client->projects()]);
        $this->view('emails.realestate.payment_plan');
        $this->dataFile($paymentPlan->output());
        $this->dataFileDetails('Payment plan.pdf');

        $this->send();
    }
}
