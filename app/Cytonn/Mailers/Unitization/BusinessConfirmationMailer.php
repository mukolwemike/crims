<?php

namespace Cytonn\Mailers\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Mailers\Client\DocumentPasswordMailer;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Unitization\Reporting\BusinessConfirmationGenerator;
use Cytonn\Users\UserClientRepository;
use Illuminate\Support\Arr;

class BusinessConfirmationMailer extends Mailer
{
    /**
     * @param UnitFundPurchase $purchase
     * @param null $manual
     * @throws CRIMSGeneralException
     */
    public function sendBC(UnitFundPurchase $purchase, $manual = null)
    {
        $this->queue(true);

        $client = $purchase->client;

        $fund = $purchase->unitFund;

        $sender = 'operations@cytonn.com';

        $sentType = $manual ? 'MBC' : (($purchase->approval->sender->username == 'system') ? 'ABC' : 'MBC');

        $subject = '[' . $sentType . ']: Business Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id);

        $this->view('emails.unitization.businessconfirmation');

        $this->subject($subject);

        $this->from = [$sender => ucfirst($fund->name)];

        $emails = $client->getContactEmailsArray();

        $this->to($emails);

        if (count($emails) == 0) {
            $this->sendAsSMS($purchase);
            $this->finalize($purchase);

            return;
        }

        $cc[] = $sender;
        $fa = $client->getLatestFA('units');
        if ($fa) {
            $cc[] = $fa->email;
        }

        $this->cc($cc);

        $this->bcc(config('system.administrators'));

        $user = null;

        try {
            $user = auth()->user();
            $emailSender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $emailSender = $fund->manager->fullname;
        }

        $this->data([
            'client' => $client,
            'unitFund' => $fund,
            'emailSender' => $emailSender,
            'purchase' => $purchase,
            'companyAddress' => str_contains($fund->manager->fullname, 'Cytonn Asset Manager') ? 'asset_manager' : null,
            'mpesa' => $fund->repo->collectionAccount('mpesa')
        ]);

        $this->dataFile = (new BusinessConfirmationGenerator())
            ->setProtectDocument(true)
            ->generate($purchase, $user)->stream();

        $this->dataFileDetails(
            'Unit Trust Business Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id) . '.pdf'
        );

//        $this->sendPassword($purchase, $subject);

        if ($purchase->isFirstPurchase()) {
            $this->provisionClient($purchase->client);
        }

        // $this->queue(true);

        $this->send();

        $this->finalize($purchase);
    }

    /**
     * @param $purchase
     * @param $subject
     * @throws \Exception
     */
    private function sendPassword($purchase, $subject)
    {
        (new DocumentPasswordMailer($purchase->client))->dispatch($purchase->unitFund->manager->fullname, $subject);
    }

    /**
     * @param Client $client
     */
    private function provisionClient(Client $client)
    {
        (new UserClientRepository())->provisionForClient($client);
    }

    /**
     * @param UnitFundPurchase $purchase
     * @throws CRIMSGeneralException
     */
    private function sendAsSMS(UnitFundPurchase $purchase)
    {
        $client = $purchase->client;

        $phone = $client->individualPhoneNumber();

        if (!$phone) {
            $numbers = $client->getContactPhoneNumbersArray();

            if (Arr::first($numbers)) {
                $phone = Arr::first($numbers);
            }
        }

        if (isEmptyOrNull($phone)) {
            return;
        }

        $fund = $purchase->unitFund;

        $amount = $purchase->number * $purchase->price;

        if ($amount == 0) {
            throw new CRIMSGeneralException("Purchase amount cannot be zero");
        }

        $message = 'We hereby confirm your investment of ' . $fund->currency->code . ' ' .
            (int)$amount . ' dated ' . DatePresenter::formatDate($purchase->date)
            . ' in ' . $fund->name . '. Thank you for investing with us. Call us on 0709101200. ' .
            'CMMF, a wallet that you stand to earn upto 11% p.a.';


        \SMS::queue($phone, $message);
    }

    /**
     * @param UnitFundPurchase $purchase
     */
    private function finalize(UnitFundPurchase $purchase)
    {
        $purchase->is_confirmation_sent = true;

        $purchase->save();
    }
}
