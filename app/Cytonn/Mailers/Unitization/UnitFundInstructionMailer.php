<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/15/18
 * Time: 6:07 PM
 */

namespace App\Cytonn\Mailers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\UserPresenter;

class UnitFundInstructionMailer extends Mailer
{
    public function notify(UnitFundInvestmentInstruction $instruction)
    {
        $client = $instruction->client;

        $fund = $instruction->unitFund;

        $sender = 'operations@cytonn.com';

        try {
            $user = auth()->user();
            $emailSender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $emailSender = $fund->manager->fullname;
        }

        $this->view('unitization.reports.instructions.index', [
            'client' => $client,
            'unitFund' => $fund,
            'instruction' => $instruction,
            'emailSender' => $emailSender,
            'companyAddress' => str_contains($fund->manager->fullname, 'Cytonn Asset Manager') ? 'asset_manager' : null,
        ]);

        $this->subject($instruction->type->name .' of '. ucfirst($fund->name) . ' Units');
        $this->from = [$sender => ucfirst($fund->name)];
        $this->to($client->getContactEmailsArray());
        $this->cc([ $sender ]);
        $this->bcc(['operations@cytonn.com']);

        $this->send();
    }
}
