<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-12
 * Time: 13:17
 */

namespace Cytonn\Mailers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Mailers\Client\DocumentPasswordMailer;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Unitization\Reporting\RedemptionConfirmationGenerator;

class RedemptionConfirmationMailer extends Mailer
{
    public function sendRC(UnitFundSale $sale)
    {
        $client = $sale->client;

        $fund = $sale->fund;

        $sender = str_contains($fund->manager->name, 'Seriani')
            ? 'operations@serianiasset.com' : 'operations@cytonn.com';

        $subject = 'Redemption Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id);

        $this->view('emails.unitization.redemptionconfirmation');

        $this->subject($subject);

        $this->from = [$sender => ucfirst($fund->name)];
        $this->to($client->getContactEmailsArray());

        $cc[] = $sender;
        $fa = $client->getLatestFA('units');
        if ($fa) {
            $cc[] = $fa->email;
        }

        $this->cc($cc);

        $this->bcc(config('system.administrators') + ['operations@cytonn.com']);

        try {
            $user = auth()->user();
            $emailSender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $emailSender = $fund->manager->fullname;
        }

        $this->data([
            'client' => $client,
            'unitFund' => $fund,
            'emailSender' => $emailSender,
            'sale' => $sale,
            'companyAddress' => str_contains($fund->manager->name, 'Seriani') ? 'seriani' : null
        ]);

        $this->dataFile = (new RedemptionConfirmationGenerator())
            ->setProtectDocument(true)
            ->generate($sale, $user)->stream();

        $this->dataFileDetails(
            'Unit Trust Redemption Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id) . '.pdf'
        );

        $this->send();
    }
}
