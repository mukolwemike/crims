<?php

namespace Cytonn\Mailers\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Users\UserClientRepository;

class StatementMailer extends Mailer
{
    public function sendStatementToClient(
        Client $client,
        $statements,
        $statementDate,
        UnitFund $unitFund,
        $sender = null,
        $campaign = null
    ) {
        $this->from = [ 'operations@cytonn.com' => 'Cytonn Asset Managers' ];

        $this->to($client->getContactEmailsArray());

        $cc = [];
        
//        $cc = systemEmailGroups(['statements']);

        try {
            $cc[] = $client->getLatestFA()->email;
        } catch (\Exception $e) {
        }

        $this->cc($cc);

        $this->bcc(systemEmailGroups(['administrators']));

        $this->subject = $unitFund->name .' Statement - ' . ClientPresenter::presentJointFirstNames($client->id);

        $this->view = 'emails.unitization.statement';

        try {
            if (is_null($sender)) {
                throw new \Exception;
            }
            $email_sender = UserPresenter::presentLetterClosingNoSignature($sender);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments';
        }

        $this->data = ['client'=>$client,
            'statementDate'=>new Carbon($statementDate),
            'email_sender'=>$email_sender,
            'companyAddress' => str_contains($unitFund->manager->fullname, 'Cytonn Asset Manager')
                ? 'asset_manager' : null,
            'unitFund' => $unitFund,
            'campaign' => $campaign
        ];

        $this->dataFile = $statements;
        $this->dataFileDetails([]);
        $this->queue = false;

        $name =  $client->client_code.'~'.$client->id.'~'
            .time().'~'.ClientPresenter::presentJointFirstNames($client->id);

        $this->saveEmail($name, 'UTF_Statements');

        $this->send();
    }
}
