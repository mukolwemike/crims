<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/15/18
 * Time: 6:07 PM
 */

namespace App\Cytonn\Mailers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\UserPresenter;

class UnitFundSaleMailer extends Mailer
{
    public function sendNotification(UnitFundSale $sale)
    {
        $client = $sale->client;

        $fund = $sale->fund;

        $fees = $sale->fees;

        $sender = 'operations@cytonn.com';

        try {
            $user = auth()->user();
            $emailSender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $emailSender = $fund->manager->fullname;
        }

        $this->view('unitization.reports.instructions.sell', [
            'client' => $client,
            'unitFund' => $fund,
            'emailSender' => $emailSender,
            'sale' => $sale,
            'companyAddress' => str_contains($fund->manager->fullname, 'Cytonn Asset Manager') ? 'asset_manager' : null,
            'fees' => $fees
        ]);

        $this->subject('Sale of ' . ucfirst($fund->name) . ' Units');
        $this->from = [$sender => ucfirst($fund->name)];
        $this->to($client->getContactEmailsArray());
        $this->cc([$sender, $client->getLatestFA()->email]);
        $this->bcc(['operations@cytonn.com']);

        $this->send();
    }
}
