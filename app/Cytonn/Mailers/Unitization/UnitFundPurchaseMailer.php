<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/15/18
 * Time: 6:07 PM
 */

namespace App\Cytonn\Mailers\Unitization;

use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Notifiers\SMS\SMS;

class UnitFundPurchaseMailer extends Mailer
{
    public function notify(UnitFundPurchase $purchase)
    {
        $client = $purchase->client;

        $fund = $purchase->unitFund;

        $fees = $purchase->fees;

        $sender = 'operations@cytonn.com';

        try {
            $user = auth()->user();
            $emailSender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $emailSender = $fund->manager->fullname;
        }

        $this->view('unitization.reports.instructions.purchase', [
            'client' => $client,
            'unitFund' => $fund,
            'emailSender' => $emailSender,
            'purchase' => $purchase,
            'companyAddress' => str_contains($fund->manager->name, 'Seriani') ? 'seriani' : null,
            'fees' => $fees
        ]);

        $this->subject('Purchase of '. ucfirst($fund->name) . ' Units');
        $this->from = [$sender => ucfirst($fund->name)];
        $this->to($client->getContactEmailsArray());
        $this->cc([ $sender ]);
        $this->bcc(['mchaka@cytonn.com', 'operations@cytonn.com']);

        $this->send();
    }
}
