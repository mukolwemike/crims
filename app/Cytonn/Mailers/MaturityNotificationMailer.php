<?php
/**
 * Date: 16/11/2015
 * Time: 11:29 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Mailers;

use Carbon\Carbon;

/**
 * Class MaturityNotificationMailer
 *
 * @package Cytonn\Mailers
 */
class MaturityNotificationMailer extends Mailer
{
    /**
     * Sends maturity notification for investments maturing in a week
     *
     * @param $investments
     */
    public function inAWeek($investments)
    {
        $this->to(
            systemEmailGroups(['operations'])
        );

        $this->cc(['operations@cytonn.com']);

        $this->bcc(config('system.administrators'));

        $this->data(['investments'=>$investments, 'period'=>'in the next 7 days' ]);

        $this->view('emails.investment.maturitynotification');

        $this->subject('Investments Maturity Notification: '.Carbon::now()->toDateString());

        $this->send();
    }


    /**
     * Sends maturity notification for investments maturing today
     *
     * @param $investments
     */
    public function today($investments)
    {
        $this->to(systemEmailGroups(['operations_mgt']));

        $this->cc(['operations@cytonn.com']);

        $this->bcc(config('system.administrators'));

        $this->data(['investments'=>$investments, 'period'=>'today']);

        $this->view('emails.investment.maturitynotification');

        $this->subject('Today\'s Investments Maturity Notification '.Carbon::today()->toDateString());

        $this->send();
    }
}
