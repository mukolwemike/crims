<?php

namespace Cytonn\Mailers\Shares;

use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class SharesSaleOrderConfirmationMailer extends Mailer
{
    public function sendEmail(SharesSalesOrder $salesOrder, $confirmation_document)
    {
        $client = $salesOrder->seller->client;

        $this->view('emails.shares.shares_sale_order_confirmation');

        $this->subject(
            'OTC Shares Sale Order Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id)
        );

        $this->from = ['operations@cytonn.com' => $salesOrder->seller->entity->name];

        $this->to($client->getContactEmailsArray());

        $this->cc('operations@cytonn.com');

        $this->bcc(['mchaka@cytonn.com']);

        try {
            $user = \Auth::user();
            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = $client->fundManager->fullname;
        }

        $this->data(['client' => $client, 'email_sender' => $email_sender, 'sale_order' => $salesOrder]);

        $this->dataFile($confirmation_document);
        $this->dataFileDetails(
            'OTC Shares Sale Order Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id) .
            '.pdf'
        );
        $this->send();
    }
}
