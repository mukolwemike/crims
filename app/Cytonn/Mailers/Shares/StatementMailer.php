<?php

namespace Cytonn\Mailers\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\SharesStatementCampaign;
use Carbon\Carbon;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class StatementMailer extends Mailer
{
    public function sendStatementToClient(
        Client $client,
        $statements,
        $statementDate,
        $sender = null,
        SharesStatementCampaign $campaign = null
    ) {
        $this->from = ['operations@cytonn.com' => 'Cytonn Investments'];

        $this->to($client->getContactEmailsArray());
        $cc = [];
//        $cc = systemEmailGroups(['statements']);

        try {
            $cc[] = $client->getLatestFA()->email;
        } catch (\Exception $e) {
        }


        $this->cc($cc);

        $this->bcc(systemEmailGroups(['administrators']));

        $this->subject = 'Shares Statement - ' . ClientPresenter::presentJointFirstNames($client->id);
        $this->view = 'emails.shares.statement';

        try {
            if (is_null($sender)) {
                throw new \Exception;
            }
            $email_sender = UserPresenter::presentLetterClosingNoSignature($sender);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments';
        }

        $this->data = ['client' => $client, 'statementDate' => new Carbon($statementDate),
            'email_sender' => $email_sender, 'campaign' => $campaign];

        $this->dataFile = $statements;

        $this->dataFileDetails([]);

        $this->queue = false;

        $name =  $client->client_code.'~'.$client->id.'~'
            .time().'~'.ClientPresenter::presentJointFirstNames($client->id);

        $this->saveEmail($name, 'Shares_Statements');

        $this->send();
    }
}
