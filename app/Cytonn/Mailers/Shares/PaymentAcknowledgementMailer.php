<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/17/18
 * Time: 1:25 PM
 */

namespace App\Cytonn\Mailers\Shares;

use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Shares\Reporting\PaymentAcknowledgementGenerator;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class PaymentAcknowledgementMailer extends Mailer
{
    public function notify(SharePurchases $purchase)
    {
        $client = $purchase->shareHolder->client;

        $entity = $purchase->shareHolder->entity;

        $fm = $purchase->shareHolder->entity->fundManager;

        $stampDuty = $purchase->stampDuty->sum(function ($duty) {
            return abs($duty->payment->amount);
        });

        $amount = abs($purchase->clientPayment->amount) + $stampDuty;

        $sender = 'otc@cytonn.com';

        $this->view('emails.shares.paymentAcknowledgement');
        $this->subject('Share Purchase Payment Acknowledgement - ' .
            ClientPresenter::presentJointFirstNames($client->id));
        $this->from = [ $sender => ucfirst($entity->name) ];
        $this->to($client->getContactEmailsArray());
        $this->cc([$sender]);
        $this->bcc(['operations@cytonn.com']);

        try {
            $user = auth()->user();
            $emailSender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $emailSender = $fm->fullname;
        }

        $this->data([
            'client' => $client,
            'emailSender' => $emailSender,
            'purchase' => $purchase,
            'amount' => $amount,
            'companyAddress' => str_contains($fm->name, 'Seriani') ? 'seriani' : null
        ]);

        $this->dataFile = (new PaymentAcknowledgementGenerator())
            ->generate($purchase, $user)
            ->stream();

        $this->dataFileDetails(
            'Unit Trust Business Confirmation - ' .
            ClientPresenter::presentJointFirstNames($client->id) .
            '.pdf'
        );

        $this->send();
    }
}
