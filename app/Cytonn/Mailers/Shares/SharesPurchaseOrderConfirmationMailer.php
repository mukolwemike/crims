<?php

namespace Cytonn\Mailers\Shares;

use App\Cytonn\Models\SharesPurchaseOrder;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class SharesPurchaseOrderConfirmationMailer extends Mailer
{
    public function sendEmail(SharesPurchaseOrder $purchaseOrder, $confirmation_document)
    {
        $client = $purchaseOrder->buyer->client;

        $this->view('emails.shares.shares_purchase_order_confirmation');

        $this->subject(
            'OTC Shares Purchase Order Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id)
        );

        $this->from = ['operations@cytonn.com' => $purchaseOrder->buyer->entity->name];

        $this->to($client->getContactEmailsArray());

        $this->cc('operations@cytonn.com');

        $this->bcc(config('system.administrators'));

        try {
            $user = \Auth::user();
            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = $client->fundManager->fullname;
        }

        $this->data(['client' => $client, 'email_sender' => $email_sender, 'purchase_order' => $purchaseOrder]);

        $this->dataFile($confirmation_document);
        $this->dataFileDetails(
            'OTC Shares Purchase Order Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id) .
            '.pdf'
        );
        $this->send();
    }
}
