<?php

namespace Cytonn\Mailers\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ShareHolding;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;

class BusinessConfirmationMailer extends Mailer
{
    public function sendBC(Client $client, $bc, ShareHolding $holding)
    {
        $this->view('emails.shares.businessconfirmation');

        $this->subject('Business Confirmation - ' . ClientPresenter::presentJointFirstNames($client->id));

        $this->from = ['operations@cytonn.com'=>'Cytonn Cooperative'];

        $this->to($client->getContactEmailsArray());

        $this->cc('operations@cytonn.com');

        $this->bcc([ 'mchaka@cytonn.com']);

        try {
            $user = \Auth::user();

            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = $client->fundManager->fullname;
        }

        $settled = $holding->payment;

        $this->data([
            'client' => $client,
            'email_sender' => $email_sender,
            'holding' => $holding,
            'settled'=> $holding->payment ? true : false
        ]);

        $this->dataFile($bc);
        $this->dataFileDetails(
            'Business Confirmation - '. ClientPresenter::presentJointFirstNames($client->id) . '.pdf'
        );
        $this->send();
    }
}
