<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/17/18
 * Time: 1:25 PM
 */

namespace App\Cytonn\Mailers\Shares;

use App\Cytonn\Models\Client;
use Cytonn\Mailers\Mailer;

class UnmatchedSalesOrdersMailer extends Mailer
{
    public function sendEmail($orders, Client $client)
    {
        $this->subject('Unmatched Share Sales Orders');
        $this->to([$client->getContactEmailsArray()]);
        $this->cc(['operations@cytonn.com', $client->getLatestFA()->email]);
        $this->bcc(config('system.administrators'));
        $this->from = ['support@cytonn.com' => 'Cytonn CRIMS'];

        $this->view('emails.shares.unmatched_share_sales_orders');

        $this->data([
            'orders' => $orders,
            'client' => $client
        ]);

        $this->send();
    }
}
