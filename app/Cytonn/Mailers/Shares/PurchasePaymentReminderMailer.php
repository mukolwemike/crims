<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/17/18
 * Time: 1:25 PM
 */

namespace App\Cytonn\Mailers\Shares;

use App\Cytonn\Models\SharePurchases;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Mails\Mailer;

class PurchasePaymentReminderMailer extends Mailer
{
    public function remind(SharePurchases $purchase)
    {
        $view = 'emails.shares.payment_reminder';

        try {
            $email_sender = UserPresenter::presentLetterClosingNoSignature(auth()->id());
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments';
        }

        $client = $purchase->shareHolder->client;

        $data = [
            'purchase' => $purchase,
            'client' => $client,
            'email_sender'  => $email_sender
        ];

        Mailer::compose()
            ->to([ $client->getContactEmailsArray() ])
            ->cc(['otc@cytonn.com'])
            ->bcc(config('system.administrators'))
            ->subject('Share Purchase Payment Reminder')
            ->view($view, $data)
            ->send();

        return false;
    }
}
