<?php
/**
 * Date: 05/08/2017
 * Time: 11:23
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Mailers\System;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class InvestmentAuditMailer extends Mailer
{
    public function sendEmail($counts, $files)
    {
        $recipients = collect(config('system.emails.operations_admins'))
            ->merge(config('system.emails.super_admins'))->all();

        $this->to($recipients);
        $this->from(['crims@cytonn.com' => 'Cytonn Crims']);
        $this->subject('Investment Check Report - ' . Carbon::today()->toFormattedDateString());

        $this->view('emails.system.investment_audit', ['counts' => $counts]);

        $this->file($files);

        $this->send();
    }
}
