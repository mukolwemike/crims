<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 08/11/2018
 * Time: 09:42
 */

namespace Cytonn\Mailers\System;

use Carbon\Carbon;
use Cytonn\Mailers\Mailer;

class FlaggedRiskyClientMailer extends Mailer
{
    public function sendFlaggedRiskyClientNotification($riskyClient, $channel = "CRIMS-ADMIN")
    {
        $this->to('risk@cytonn.com');

        $this->bcc(config('system.administrators'));

        $this->subject($channel . ' : Risky Client Flagged During On-Boarding on ' . Carbon::today()->toFormattedDateString());

        $this->view('emails.system.flagged_risky_client', ['riskyClient' => $riskyClient]);

        $this->send();
    }

    public function sendAddRiskyClientNotification($riskyClient)
    {
        $this->to('operations@cytonn.com');

        $this->cc('risk@cytonn.com ');

        $this->subject('Client Added to Risky Client Database');

        $this->view('emails.system.add_risky_client', ['riskyClient' => $riskyClient]);

        $this->send();
    }

    public function sendClearRiskyClientNotification($riskyClient)
    {
        $this->to('operations@cytonn.com');

        $this->cc('risk@cytonn.com ');

        $this->subject('Risky Client Details Cleared');

        $this->view('emails.system.clear_risky_client', ['riskyClient' => $riskyClient]);

        $this->send();
    }
}
