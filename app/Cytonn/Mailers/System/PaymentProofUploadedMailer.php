<?php

namespace App\Cytonn\Mailers\System;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Cytonn\Mailers\Mailer;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Facades\Storage;

class PaymentProofUploadedMailer extends Mailer
{
    public function notify($id, $type)
    {
        $instruction = $this->getInstruction($id, $type);

        $this->to('operations@cytonn.com');

        $this->subject('Proof of payment for ' . $type . ' instruction has been uploaded');

        $this->view('emails.system.payment_proof_uploaded');

        $clientCode = null;

        $docArray = [];

        if ($type === 'application') {
            $clientName = ClientPresenter::presentFullNameWithPartials(
                $instruction->firstname,
                $instruction->lastname,
                $instruction->middlename
            );

            $docFiles = $instruction->filledDocuments;

            foreach ($docFiles as $docFile) {
                $doc = $docFile->document;
                $docArray[$doc->filename] = Storage::get($doc->path());
            }
        } else {
            $client = $instruction->client;

            $clientName = ClientPresenter::presentFullNames($client->id);

            $clientCode = $client->client_code;

            $doc = $instruction->document;

            $docArray = [$doc->filename => Storage::get($doc->path())];
        }

        $this->dataFile($docArray);

        $this->data([
            'clientCode' => $clientCode,
            'clientName' => $clientName,
            'type' => $type
        ]);

        $this->queue(false);

        $this->send();
    }

    public function getInstruction($id, $type)
    {
        switch ($type) {
            case 'topup':
                return ClientTopupForm::findOrFail($id);
            case 'purchase':
                return UnitFundInvestmentInstruction::findOrFail($id);
            case 'application':
                return ClientFilledInvestmentApplication::findOrFail($id);
            default:
                return null;
        }
    }
}
