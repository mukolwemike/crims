<?php


namespace Cytonn\Mailers;

/**
 * Class ComplianceReportMailer
 *
 * @package Cytonn\Mailers
 */
class ComplianceReportMailer extends Mailer
{
    /**
     * @param $clients_grouped_by_fund_manager
     * @param $file_path
     */
    public function sendEmail($clients_grouped_by_fund_manager, $file_path)
    {
        $this->from = ['support@cytonn.com'=>'Cytonn CRIMS'];
        $this->to('operations@cytonn.com');
        $this->cc([ 'mchaka@cytonn.com']);
        $this->view = 'emails.client.compliance';
        $this->subject = 'Compliance Report';

        $this->file($file_path);
        $this->data = ['clients_grouped_by_compliance'=>$clients_grouped_by_fund_manager];
        $this->queue(false);

        $this->send();
    }
}
