<?php

namespace App\Cytonn\Mailers;

use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use Cytonn\Mailers\Mailer;

class InvestmentActionsMailer extends Mailer
{
    public function notifyOnTopup(ClientTopupForm $form)
    {
        $client = $form->client;

        $this->to(['operations@cytonn.com']);

        $fa_email = $client->getLatestFA();

        if ($fa_email) {
            $this->cc($fa_email->email);
        }

        $this->bcc(config('system.administrators'));
        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);
        $this->view('emails.investment.actions.topup_notification');

        $this->subject('[CRIMS] '.$client->present()->fullName.' has made a top up.');
        $this->data(['client'=>$client, 'form'=>$form]);

        $this->send();
    }

    public function notifyClientOnTopup(ClientTopupForm $form)
    {
        $client = $form->client;

        $this->to([$client->getContactEmailsArray()]);

        $fa_email = $client->getLatestFA();

        $cc = $fa_email ? ['operations@cytonn.com', $fa_email->email] : ['operations@cytonn.com'];

        $this->cc($cc);

        $this->bcc(config('system.administrators'));
        $this->from(['operations@cytonn.com'=>'Cytonn Investments']);
        $this->view('emails.investment.actions.topup_notification_client');

        $this->subject('Your top up has been received');

        $this->data([
            'client' => $client,
            'form' => $form
        ]);

        $this->send();
    }

    public function notifyOnRolloverOrWithdraw(ClientInvestmentInstruction $instruction)
    {
        $client = $instruction->investment->client;

        $type = $instruction->type->name;

        if ($type == 'withdraw') {
            $type = 'withdrawal';
        }

        $this->to(['operations@cytonn.com']);

        $fa_email = $client->getLatestFA();

        if ($fa_email) {
            $this->cc($fa_email->email);
        }

        $this->bcc(config('system.administrators'));
        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);
        $this->view('emails.investment.actions.rollover_withdraw_notification');
        $this->subject('[CRIMS] '.$client->present()->fullName.' has made a '.$type);
        $this->data(['client'=>$client, 'form'=>$instruction, 'type'=>$type]);

        $this->send();
    }

    public function notifyClientOnRolloverOrWithdraw(ClientInvestmentInstruction $instruction)
    {
        $investment = $instruction->investment;

        $client = $investment->client;

        $type = $instruction->type->name;

        $fa_email = $client->getLatestFA();

        if ($type == 'withdraw') {
            $type = 'withdrawal';
        }

        $this->to([$client->getContactEmailsArray()]);

        $cc = $fa_email ? ['operations@cytonn.com', $fa_email->email] : ['operations@cytonn.com'];

        $this->cc($cc);

        $this->bcc(config('system.administrators'));
        $this->from(['operations@cytonn.com'=>'Cytonn Investments']);
        $this->view('emails.investment.actions.rollover_withdraw_notification_client');
        $this->subject("Your ".ucfirst($type)." Request Has Been Received");
        $this->data(['client'=>$client, 'form'=>$instruction, 'type'=>$type, 'investment'=>$investment]);

        $this->send();
    }

    public function notifyClientOnTopUpApprovalCreated(ClientTopupForm $form)
    {
        if ($form->repo->approvedByClient()) {
            return;
        }

        $client = $form->client;

        $this->to([$client->getContactEmailsArray()]);

        $fa_email = $client->getLatestFA();

        $cc = $fa_email ? ['operations@cytonn.com', $fa_email->email] : ['operations@cytonn.com'];

        $this->cc($cc);

        $this->bcc(config('system.administrators'));
        $this->from(['operations@cytonn.com'=>'Cytonn Investments']);
        $this->view('emails.investment.actions.approval_notification');

        $this->subject('Top up instruction need your approval');

        $this->data([
            'client' => $client,
            'type' => 'top up'
        ]);

        $this->send();
    }

    public function notifyClientOnRolloverApprovalCreated(ClientInvestmentInstruction $instruction)
    {
        if (!$instruction->isPartialRollover()) {
            return;
        }

        if ($instruction->repo->approvedByClient()) {
            return;
        }

        $client = $instruction->investment->client;

        $type = $instruction->type->name;

        if ($type == 'withdraw') {
            $type = 'withdrawal';
        }

        $this->to(['operations@cytonn.com']);

        $fa_email = $client->getLatestFA();

        if ($fa_email) {
            $this->cc($fa_email->email);
        }

        $this->bcc(config('system.administrators'));

        $this->from(['support@cytonn.com'=>'Cytonn CRIMS']);

        $this->view('emails.investment.actions.approval_notification');

        $this->subject($type .' instruction require your approval');

        $this->data([
            'client' => $client,
            'type' => $type
        ]);

        $this->send();
    }
}
