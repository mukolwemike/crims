<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Excels;

use Maatwebsite\Excel\Facades\Excel;

class ExcelWork
{
    /*
     * Generate an excel with multiple sheets and store it
     */
    public static function generateAndStoreMultiSheet($data, $name)
    {
        Excel::create(
            $name,
            function ($excel) use ($data) {
                if (count($data) == 0) {
                    $excel->sheet(
                        "No Data",
                        function ($sheet) use ($data) {
                            $sheet->fromArray([]);
                        }
                    );
                } else {
                    foreach ($data as $key => $value) {
                        $excel->sheet(
                            trimText("$key", 30, false),
                            function ($sheet) use ($value) {
                                $sheet->fromArray($value, null, null, true);
                                $sheet->row(
                                    1,
                                    function ($row) {
                                        $row->setBackground('#006666');
                                        $row->setFontColor('#ffffff');
                                    }
                                );
                            }
                        );
                    }
                }
            }
        )->store('xlsx', storage_path('exports'));
    }

    /*
     * Generate an excel with multiple sheets and export it
     */
    public static function generateAndExportMultiSheet($data, $name)
    {
        Excel::create(
            $name,
            function ($excel) use ($data) {
                if (count($data) == 0) {
                    $excel->sheet(
                        "No Data",
                        function ($sheet) use ($data) {
                            $sheet->fromArray([]);
                        }
                    );
                } else {
                    foreach ($data as $key => $value) {
                        $excel->sheet(
                            trimText("$key", 30, false),
                            function ($sheet) use ($value) {
                                $sheet->fromArray($value, null, null, true);
                                $sheet->row(
                                    1,
                                    function ($row) {
                                        $row->setBackground('#006666');
                                        $row->setFontColor('#ffffff');
                                    }
                                );
                            }
                        );
                    }
                }
            }
        )->export('xlsx');
    }

    /*
     * Generate an excel with a single sheet and store it
     */
    public static function generateAndStoreSingleSheet($data, $name)
    {
        Excel::create(
            $name,
            function ($excel) use ($data, $name) {
                $excel->sheet(
                    trimText($name, 30, false),
                    function ($sheet) use ($data) {
                        $sheet->fromArray($data);
                        $sheet->row(
                            1,
                            function ($row) {
                                $row->setBackground('#006666');
                                $row->setFontColor('#ffffff');
                            }
                        );
                    }
                );
            }
        )->store('xlsx', storage_path() . '/exports/');
    }

    /*
     * Generate an excel with a single sheet and export it
     */
    public static function generateAndExportSingleSheet($data, $name)
    {
        Excel::create(
            $name,
            function ($excel) use ($data, $name) {
                $excel->sheet(
                    trimText($name, 30, false),
                    function ($sheet) use ($data) {
                        $sheet->fromArray($data);
                        $sheet->row(
                            1,
                            function ($row) {
                                $row->setBackground('#006666');
                                $row->setFontColor('#ffffff');
                            }
                        );
                    }
                );
            }
        )->export('xlsx');
    }

    /*
     * Generate excel from a view
     */
    public static function generateAndStoreViewExcel($data, $name, $view)
    {
        Excel::create(
            $name,
            function ($excel) use ($data, $view) {
                foreach ($data as $key => $value) {
                    $excel->sheet(
                        trimText($key, 30, false),
                        function ($sheet) use ($value, $view) {
                            $sheet->loadView($view, ['values' => $value]);
                        }
                    );
                }
            }
        )->store('xlsx', storage_path('exports'));
    }

    /*
     * Generate single sheet excel from a view
     */
    public static function generateSingleCustomExcelAndStore($data, $name, $view)
    {
        Excel::create(
            $name,
            function ($excel) use ($data, $view, $name) {
                $excel->sheet(
                    $name,
                    function ($sheet) use ($data, $view) {
                        $sheet->loadView($view, ['values' => $data]);
                    }
                );
            }
        )->store('xlsx', storage_path('exports'));
    }

    /*
     * Generate and store excel from a view
    */
    public static function generateMultiExcelFromViewAndStore($data, $name, $views)
    {
        Excel::create($name, function ($excel) use ($data, $views) {
            if (!is_array($views)) {
                foreach ($data as $key => $value) {
                    $excel->sheet(trimText($key, 30, false), function ($sheet) use ($value, $views) {

                        $sheet->loadView($views, ['values' => $value]);
                    });
                }
            } else {
                $index = 0;

                foreach ($data as $key => $value) {
                    $view = $views[$index];

                    $excel->sheet(trimText($key, 30, false), function ($sheet) use ($value, $view) {

                        $sheet->loadView($view, ['values' => $value]);
                    });

                    $index++;
                }
            }
        })->store('xlsx', storage_path('exports'));
    }
}
