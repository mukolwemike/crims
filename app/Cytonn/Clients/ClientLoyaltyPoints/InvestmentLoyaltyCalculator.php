<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Clients\ClientLoyaltyPoints;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\CalculatorTrait;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Models\ClientInvestmentWithdrawal;

class InvestmentLoyaltyCalculator
{
    use CalculatorTrait;

    protected $investment;
    protected $date;
    protected $start;
    protected $prepared;

    protected static $taxable_cache = [];

    protected static $clients_with_exemptions_cache = [];

    /**
     * InvestmentLoyaltyCalculator constructor.
     * @param ClientInvestment $investment
     * @param Carbon $date
     */
    public function __construct(ClientInvestment $investment, Carbon $date, Carbon $startDate = null)
    {
        $this->investment = $investment;
        $this->date = $date;
        $this->start = $startDate ? $this->limitStart($startDate) : $this->investment->invested_date->copy();

        $this->prepared = $this->prepare();
    }

    protected function prepare()
    {
        $date = $this->date;

        $actions = $this->getActions();

        $principal = $this->investment->amount;

        $pointsCf = $netInterestCF = 0;

        $startDate = $this->start->copy();

        $withdrawal = null;

        $actions = $actions->map(function ($w) use (&$principal, &$pointsCf, &$netInterestCF, &$withdrawal, &$startDate) {
            if ($w instanceof ClientInvestmentTopup) {
                $type = 'topup';
                $amount = $w->amount;
                if (!$w->description) {
                    $w->description = 'Topup';
                }
            } elseif ($w instanceof ClientInvestmentWithdrawal) {
                $type = 'withdrawal';
                $amount = -$w->amount;
            } elseif ($w instanceof EmptyModel) {
                $type = 'empty';
                $amount = 0;
            } else {
                throw new ClientInvestmentException("Only topup/withdrawal actions accepted");
            }

            $prepared = $this->calculate($principal, $startDate, $w->date, $amount, $pointsCf, $netInterestCF);

            $prepared->type = $type;
            $prepared->date = $w->date;
            $prepared->amount = $amount;
            $prepared->action = $w;
            $prepared->description = $w->description;

            $principal = $prepared->principal;
            $pointsCf = $prepared->total_points;
            $netInterestCF = $prepared->net_interest_after;
            $startDate = $w->date;
            $withdrawal = $w;

            return $prepared;
        });

        $start = $this->investment->invested_date;
        if ($withdrawal) {
            $start = $withdrawal->date;
        }

        $last = $this->calculate($principal, $start, $date, 0, $pointsCf, $netInterestCF);
        $last->type = 'end';
        $last->date = $date;
        $last->investment = null;
        $last->description = 'Total';

        return (object)[
            'investment' => $this->investment,
            'actions' => $actions->all(),
            'total' => $last
        ];
    }

    protected function calculate($principal, Carbon $start, Carbon $end = null, $changeAmount, $pointsCf, $netInterestCF)
    {
        $is_first = $this->investment->invested_date->copy()->startOfDay()->eq($start->copy()->startOfDay());

        if (!$is_first) {
            $start = $start->copy()->addDay();
        }

        $start = $this->limitStart($start->copy());

        $is_before_end_day = $end->copy()->startOfDay()->lt($this->date->copy()->startOfDay());

        if ($is_before_end_day) {
            $end = $end->copy()->addDay();
        }

        $end = $this->limitEnd($end->copy());

        //Interest
        $taxable = $this->checkTaxable($this->investment->client, $end);

        $rate = $this->investment->interest_rate;

        $gross_i = $this->getGrossInterest($principal, $rate, $start, $end);

        $net_i = $this->getNetInterest($gross_i, $taxable);

        $net_i_cumulative = $netInterestCF + $net_i;

        $net_i_after_withdr = max([$net_i_cumulative + $changeAmount, 0]);

        $total = $principal + $net_i_cumulative + $changeAmount;

        //Points
        $loyaltyRate = $this->investment->loyalty_rate;

        $points = $this->getGrossInterest($principal, $loyaltyRate * 100, $start, $end);

        $pointsCf += $points;

        return (object) [
            'original_principal' => $principal,
            'principal' => (float) min([$total, $principal]),
            'points' => $points,
            'total_points' => $pointsCf,
            'net_interest_after' => $net_i_after_withdr,
        ];
    }

    private function checkTaxable(Client $client, Carbon $date)
    {
        if (!$client->taxable) {
            return false;
        }

        if ($client->relationLoaded('taxExemptions') && !$client->taxExemptions->count()
        ) {
            return true;
        }

        $key = 'client_id_'.$client->id.'_date_'.$date->toDateString();

        if ($this->clientHasNoExemptions($client)) {
            return true;
        }

        if (isset(static::$taxable_cache[$key])) {
            return static::$taxable_cache[$key];
        }

        $exempt = !$client->taxExemptions()
            ->where(function ($q) use ($date) {
                $q->where(function ($q) use ($date) {
                    $q->where('start', '<=', $date)->where('end', '>=', $date);
                })
                    ->orWhere(function ($q) use ($date) {
                        $q->where('start', '<=', $date)->whereNull('end');
                    })
                    ->orWhere(function ($q) use ($date) {
                        $q->whereNull('start')->where('end', '>=', $date);
                    })
                    ->orWhere(function ($q) {
                        $q->whereNull('start')->whereNull('end');
                    });
            })->exists();

        static::$taxable_cache[$key] = $exempt;

        return (bool) $exempt;
    }

    private function clientHasNoExemptions(Client $client)
    {
        if (in_array($client->id, static::$clients_with_exemptions_cache)) {
            return false;
        }

        $exempt = $client->taxExemptions()->exists();

        if ($exempt) {
            static::$clients_with_exemptions_cache[] = $client->id;
        }

        return !$exempt;
    }

    private function getActions()
    {
         $withdrawals = $this->investment->repo->getWithdrawals($this->date);

         $topups = $this->investment->repo->getTopupsTo($this->date);

         $actions = $topups->merge($withdrawals->all())
             ->filter(function ($action) {
                 return $action->date->lte($this->date) && ($action->amount != 0);
             });

         return $actions->sortByDate('date', 'ASC')->values();
    }

    protected function limitEnd(Carbon $end)
    {
        if ($this->investment->withdrawal_date) {
            if ($this->investment->withdrawal_date->lte($end)) {
                return $this->investment->withdrawal_date;
            }
        }

        if ($this->investment->maturity_date->lte($end)) {
            return $this->investment->maturity_date;
        }

        return $end;
    }

    protected function limitStart(Carbon $start)
    {
        if ($this->investment->invested_date->gte($start)) {
            return $this->investment->invested_date;
        }

        return $start;
    }

    public function getPrepared()
    {
        return $this->prepared;
    }

    public function totalPoints()
    {
        return $this->prepared->total->total_points;
    }
}
