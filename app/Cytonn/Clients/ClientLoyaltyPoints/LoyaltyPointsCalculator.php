<?php

namespace App\Cytonn\Clients\ClientLoyaltyPoints;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use Carbon\Carbon;

class LoyaltyPointsCalculator
{
    protected $client;
    protected $date;
    protected $start;
    protected $prepared;

    /**
     * LoyaltyPointsCalculator constructor.
     * @param Client $client
     * @param Carbon $date
     * @param Carbon $start
     */
    public function __construct(Client $client, Carbon $date, Carbon $start = null)
    {
        $this->client = $client;
        $this->date = $date;
        $this->start = $start;
        $this->prepared = $this->prepare();
    }

    protected function prepare()
    {
        $investments = $this->client->investments()->get();
        $purchases = $this->client->unitFundPurchases()->get();
        $holdings = $this->client->unitHoldings()->get();

        $invPoints = $fundPoints = $rePoints = 0;

        foreach ($investments as $investment) {
            $invPoints += ($investment->calculateLoyalty($this->date, $this->start)->totalPoints());
        }

        foreach ($purchases as $purchase) {
            $fundPoints += (new  UnitFundLoyaltyCalculator($this->client, $purchase->unitFund, $this->date, $this->start))
                ->totalPoints();
        }

        foreach ($holdings as $holding) {
            $rePoints += ($holding->calculateLoyalty($this->date, $this->start)->totalPoints());
        }

        $accPoints = $invPoints + $fundPoints + $rePoints;
        $redeemedPoints = $this->redeemedPoints();

        $data['totalPoints'] = $accPoints;
        $data['redeemedPoints'] = $redeemedPoints;
        $data['availablePoints'] = $accPoints - $redeemedPoints;
        $data['invPoints'] = $invPoints;
        $data['fundPoints'] = $fundPoints;
        $data['rePoints'] = $rePoints;

        return (object)[
            'total' => $data
        ];
    }

    public function redeemedPoints()
    {
        $instructions = LoyaltyPointsRedeemInstructions::where('client_id', $this->client->id)
            ->whereNotNull('approval_id')
            ->get()
            ->map(function ($instruction) {
                return [
                    'amount' => $instruction->amount
                ];
            });

        $sum = 0;
        foreach ($instructions as $instruction) {
            $sum += $instruction['amount'];
        }

        return $sum;
    }

    public function getTotalInvestmentsPoints()
    {
        return $this->prepared->total['invPoints'];
    }

    public function getTotalFundsPoints()
    {
        return $this->prepared->total['fundPoints'];
    }

    public function getTotalHoldingsPoints()
    {
        return $this->prepared->total['rePoints'];
    }

    public function getTotalPoints()
    {
        return $this->prepared->total['totalPoints'];
    }

    public function getAvailablePoints()
    {
        return $this->prepared->total['availablePoints'];
    }

    public function getRedeemedPoints()
    {
        return $this->prepared->total['redeemedPoints'];
    }
}
