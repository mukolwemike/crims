<?php

namespace App\Cytonn\Clients\ClientLoyaltyPoints;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;

class LoyaltyRedeemInstructionsRepository
{
    const REDEEM_LIMIT = 2000;

    public function saveInstruction($input, $amount, $origin = null)
    {
        $client = Client::findOrFail($input['client_id']);

        $instruction = new LoyaltyPointsRedeemInstructions();

        $instruction->client()->associate($client);

        $instruction->payload = json_encode($input);

        $instruction->amount = $amount;

        $instruction->date = Carbon::now()->toDateString();

        $instruction->user_id = isset($input['user_id']) ? $input['user_id'] : null;

        $instruction->origin = $origin ? 1 : 0;

        $instruction->save();

        return $instruction;
    }

    public function getCalculatedDetails(Client $client)
    {
        $calc = $client->calculateLoyalty();

        return [
            'fundPoints' => $this->cleanAmount($calc->getTotalFundsPoints()),
            'invPoints' => $this->cleanAmount($calc->getTotalInvestmentsPoints()),
            'rePoints' => $this->cleanAmount($calc->getTotalHoldingsPoints()),
            'totalPoints' => $this->cleanAmount($calc->getTotalPoints()),
            'redeemedPoints' => $this->cleanAmount($calc->getRedeemedPoints()),
            'availablePoints' => $calc->getAvailablePoints(),
            'canRedeem' => $calc->getAvailablePoints() > self::REDEEM_LIMIT ? true : false
        ];
    }

    private function cleanAmount($data)
    {
        return AmountPresenter::currency($data, false, 2);
    }
}
