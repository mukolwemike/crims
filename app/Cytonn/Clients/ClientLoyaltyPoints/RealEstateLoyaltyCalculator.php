<?php


namespace App\Cytonn\Clients\ClientLoyaltyPoints;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstateRefund;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;

class RealEstateLoyaltyCalculator
{
    use CalculatorTrait;

    const DEFAULT_LOYALTY_RATE = 0.0005;

    private $holding;

    private $startDate;

    private $endDate;

    private $prepared;

    private $actions = null;

    /**
     * HoldingCalculator constructor.
     * @param UnitHolding $holding
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(UnitHolding $holding, Carbon $endDate, Carbon $startDate = null)
    {
        $this->holding = $holding;

        $this->startDate = $startDate ? $startDate : $this->getStartDate();

        $this->endDate = $endDate;

        $this->prepared = $this->prepare();
    }

    /**
     * @return object
     */
    protected function prepare()
    {
        $actions = $this->getPaymentsActions();

        $cumulativePoints = $cumulativeAvailablePoints = 0;

        $actions = $actions->map(function ($action) use (&$cumulativePoints, &$cumulativeAvailablePoints) {

            $startDate = $action->date;

            if ($action instanceof RealEstatePayment) {
                $amount = $action->amount;
            } elseif ($action instanceof RealEstateRefund) {
                $amount = -$action->amount;
            }

            $prepared = $this->calculate($startDate, $amount, $cumulativePoints, $cumulativeAvailablePoints);
            $prepared->action = $action;

            $cumulativePoints = $prepared->total_actual_points;
            $cumulativeAvailablePoints = $prepared->total_points;

            return $prepared;
        });

        $last = $this->calculate($this->endDate->copy(), 0, $cumulativePoints, $cumulativeAvailablePoints);

        return (object) [
            'actions' => $actions->all(),
            'total' => $last
        ];
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param $netPaidAmount
     * @param $changeAmount
     * @param $cumulativeNetInterest
     * @return object
     */
    private function calculate(Carbon $startDate, $amount, $cumulativePoints, $cumulativeAvailablePoints)
    {
        $oneYearLater = $startDate->copy()->addYear();

        $points = $amount * static::DEFAULT_LOYALTY_RATE;

        $availablePoints = $this->endDate >= $oneYearLater ? $points : 0;

        $cumulativeAvailablePoints += $availablePoints;

        $cumulativePoints += $points;

        return (object) [
            'principal' => $amount,
            'start' => $startDate,
            'points' => $availablePoints,
            'actual_points' => $points,
            'total_points' => $cumulativeAvailablePoints,
            'total_actual_points' => $cumulativePoints
        ];
    }

    /**
     * @return mixed
     */
    private function getPaymentsActions()
    {
        if (!$this->actions) {
            $payments = $this->holding->payments()->between($this->startDate, $this->endDate)->get();

            $refunds = $this->holding->refunds()->between($this->startDate, $this->endDate)->get();

            $actions = $payments->merge($refunds->all());

            $actions = $actions->sortByDate('date', 'ASC');

            $this->actions = $actions->values();
        }

        return $this->actions;
    }

    /**
     * @return object
     */
    public function getPrepared()
    {
        return $this->prepared;
    }

    /**
     * @return mixed
     */
    public function totalPoints()
    {
        return $this->prepared->total->total_points;
    }

    /**
     * @return Carbon
     */
    private function getStartDate()
    {
        $firstAction = $this->holding->payments()->orderBy('date')->first();

        return $firstAction ? Carbon::parse($firstAction->date) : Carbon::now();
    }
}
