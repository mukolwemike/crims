<?php


namespace App\Cytonn\Clients\ClientLoyaltyPoints;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\LoyaltyValues;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Codeception\Lib\Driver\Db;
use Cytonn\Exceptions\ClientInvestmentException;

class UnitFundLoyaltyCalculator
{
    protected $client;
    protected $unitFund;
    protected $date;
    protected $start;
    protected $prepared;
    protected $effectiveRate;
    protected $rateChangeDate;
    protected $purchases;
    protected $sales;


    public function __construct(Client $client, UnitFund $unitFund, Carbon $date, Carbon $start = null)
    {
        $this->client = $client;
        $this->unitFund = $unitFund;
        $this->date = $date;
        $this->start = $start ? $start : $this->getStart();
        $this->updateEffectiveRateAndDate($this->start);

        $this->prepared = $this->prepare();
    }

    /**
     * @return Carbon
     */
    private function getStart()
    {
        $purchase = $this->client->unitFundPurchases()->forUnitFund($this->unitFund)->orderBy('date')->first();

        return $purchase ? Carbon::parse($purchase->date) : Carbon::now();
    }

    private function updateEffectiveRateAndDate(Carbon $date)
    {
        $consecutiveMonths = $this->client->repo->getConsecutiveMonthsInvested(null, $date);

        $rates = LoyaltyValues::orderBy('duration')->get();

        $changeDuration = null;

        foreach ($rates as $rate) {
            if ($rate->duration <= $consecutiveMonths) {
                $this->effectiveRate = $rate->value;
            } else {
                $changeDuration = $rate->duration;
                break;
            }
        }

        $this->rateChangeDate = $changeDuration ? $this->client->repo->getNextLoyaltyRateChangeDate($changeDuration)
            : null;
    }

    protected function prepare()
    {
        $actions = $this->getActions();

        $startDate = $this->limitStart($this->start);

        $principal = $this->balance($startDate);

        $cumulativePoints = 0;

        $actions = $actions->map(function ($action) use (&$startDate, &$principal, &$cumulativePoints) {
            $data = array();

            if ($action instanceof UnitFundPurchase) {
                $type = 'purchase';
                $amount = $action->number * $action->price;
            } elseif ($action instanceof UnitFundSale) {
                $type = 'sale';
                $amount = -$action->repository()->getPurchaseValue();
            } else {
                throw new ClientInvestmentException("Only sales/purchases actions accepted");
            }

            $endDate = Carbon::parse($action->date);

            if ($endDate > $this->rateChangeDate) {
                $newEndDate = $this->rateChangeDate;

                $beforePoints = $this->getPoints($principal,  $startDate, $newEndDate);

                $this->updateEffectiveRateAndDate($endDate);

                $afterPoints = $this->getPoints($principal, $newEndDate, $endDate);

                $points = $beforePoints + $afterPoints;
            } else {
                $points = $this->getPoints($principal, $startDate, $endDate);
            }

            $cumulativePoints += $points;

            $data['principal'] = $principal;
            $data['start'] = $startDate->copy();
            $data['end'] = $endDate->copy();
            $data['points'] = $points;
            $data['rate'] = $this->effectiveRate;
            $data['total_points'] = $cumulativePoints;
            $data['action'] = $action;
            $data['type'] = $type;

            $principal += $amount;

            $startDate = $endDate;

            return (object) $data;
        });

        $data = array();

        $points = $this->getPoints($principal, $startDate, $this->date);
        $cumulativePoints += $points;

        $data['principal'] = $principal;
        $data['start'] = $startDate->copy();
        $data['end'] = $this->date->copy();
        $data['points'] = $points;
        $data['rate'] = $this->effectiveRate;
        $data['total_points'] = $cumulativePoints;

        return (object) [
            'actions' => $actions->all(),
            'total' => (object) $data
        ];
    }

    private function getActions()
    {
        $purchases = $this->purchases();

        $sales = $this->sales();

        $actions = $purchases->merge($sales->all());

        return $actions->sortByDate('date', 'ASC')->values();
    }

    /**
     * @return mixed
     */
    private function purchases()
    {
        if ($this->purchases) {
            return $this->purchases;
        }

        return $this->purchases = $this->client->unitFundPurchases()->forUnitFund($this->unitFund)
            ->before($this->date)->oldest('date')
            ->get();
    }

    /**
     * @return mixed
     */
    private function sales()
    {
        if ($this->sales) {
            return $this->sales;
        }
        return $this->sales = $this->client->unitFundSales()->forUnitFund($this->unitFund)
            ->before($this->date)->oldest('date')->get();
    }

    private function getPoints($amount, Carbon $start, Carbon $end)
    {
        $end = $this->limitEnd($end->copy());

        $tenor = $end->diffInDays($start);

        return $amount * $this->effectiveRate * $tenor / 365;
    }

    private function balance(Carbon $startDate)
    {
        $totalPurchases = $this->purchases()->sum(function (UnitFundPurchase $purchase) use ($startDate) {
            return $startDate->gt($purchase->date) ? $purchase->amount : 0;
        });

        $totalSales = $this->sales()->sum(function (UnitFundSale $sale) use ($startDate) {
            return $startDate->gt($sale->date) ? $sale->repository()->getPurchaseValue() : 0;
        });

        return max($totalPurchases - $totalSales, 0);
    }

    /**
     * @param Carbon $end
     * @return Carbon
     */
    private function limitEnd(Carbon $end)
    {
        return $end <= $this->date ? $end : $this->date;
    }

    private function limitStart(Carbon $start)
    {
        $startDate = $this->getStart();

        if ($start < $startDate) {
            return $startDate;
        }

        return $start;
    }

    public function totalPoints()
    {
        return $this->prepared->total->total_points;
    }
}
