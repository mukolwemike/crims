<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 16/10/2018
 * Time: 15:49
 */

namespace Cytonn\Clients\Forms;

use Laracasts\Validation\FormValidator;

class ConsentFormDataForm extends FormValidator
{
    public $rules = [
        'date'=>'required|date',
        'file'=>'mimes:pdf,png,jpg,bmp',
        'email'=>'required|email',
        'consent'=>'required'
    ];
}
