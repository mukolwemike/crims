<?php
/**
 * Date: 14/07/2016
 * Time: 8:06 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Forms;

use Laracasts\Validation\FormValidator;

class EmailIndemnityForm extends FormValidator
{
    public $rules = [
        'date'=>'required|date',
        'file'=>'required|mimes:pdf,png,jpg,bmp',
        'email'=>'required|email'
    ];
}
