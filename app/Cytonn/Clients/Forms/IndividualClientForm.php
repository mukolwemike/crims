<?php

namespace Cytonn\Clients\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

class IndividualClientForm extends FormValidator
{
    public $all_rules = [
//        'taxable'                               =>  'required',
        'country_id'                            =>  'required|numeric',
        'phone'                                 =>  'required',
        'postal_code'                           =>  'required_with:postal_address',
        'street'                                =>  'required_without:postal_address',
        'town'                                  =>  'required',
        'email'                                 =>  'required|email',
        'id_or_passport'                        =>  'required',
        'pin_no'                                =>  'required',
        'dob'                                   =>  'date|before:yesterday',
    ];

    public $messages = [
        'date'          =>  'The :attribute must be a valid date.',
        'numeric'       =>  'The :attribute must be numeric',
        'required'      =>  'The :attribute field is required.'
    ];

    public $incomplete_rules = [
        'title_id'                              =>  'required',
        'lastname'                              =>  'required|string',
        'firstname'                             =>  'required|alpha',
    ];

    /**
     * @param ValidatorFactory $validator
     */
    public function __construct(ValidatorFactory $validator)
    {
        $this->rules = $this->all_rules + $this->incomplete_rules;

        parent::__construct($validator);
    }

    /**
     * Handle incomplete requests
     */
    public function incomplete()
    {
        $this->rules = $this->incomplete_rules;
    }
}
