<?php
/**
 * Date: 11/01/2016
 * Time: 7:31 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Forms;

use Laracasts\Validation\FormValidator;

class AddBankDetailsForm extends FormValidator
{
    public $rules = [
        'investor_account_name'=>'required',
        'investor_account_number'=>'required',
        'bank_id'=>'required|numeric',
        'branch_id'=>'required|numeric',
        'currency_id'=>'required|numeric'
    ];
}
