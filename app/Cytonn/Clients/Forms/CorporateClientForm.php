<?php

namespace Cytonn\Clients\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

class CorporateClientForm extends FormValidator
{
    public $all_rules = [
        'contact_person_title'                  =>  'required|numeric',
        'contact_person_lname'                  =>  'required|alpha',
        'contact_person_fname'                  =>  'required|alpha',

//        'taxable'                               =>  'required',
        'country_id'                            =>  'required|numeric',
        'phone'                                 =>  'required',
        'telephone_office'                      =>  'required',
        'telephone_home'                        =>  'required',
        'postal_code'                           =>  'required_with:postal_address',
        'street'                                =>  'required_without:postal_address',
        'town'                                  =>  'required',

        'business_sector'                       =>  'required',
        'method_of_contact_id'                  =>  'required',
        'email'                                 =>  'required|email',
        'id_or_passport'                        =>  'required',
        'pin_no'                                =>  'required',

    //        'investor_account_name'                 =>  'required',
    //        'investor_account_number'               =>  'required',
    //        'investor_bank'                         =>  'required',
    //        'investor_bank_branch'                  =>  'required',
    //        'investor_clearing_code'                =>  'required',
    //        'investor_swift_code'                   =>  'required',
    ];

    public $incomplete_rules = [
        'client_code'                           =>  'required',
        'corporate_registered_name'             =>  'required',
        'corporate_trade_name'                  =>  'required',
        'registered_address'                    =>  'required',
    ];

    /**
     * @param ValidatorFactory $validator
     */
    public function __construct(ValidatorFactory $validator)
    {
        $this->rules = $this->all_rules + $this->incomplete_rules;

        parent::__construct($validator);
    }

    /**
     * Handle incomplete requests
     */
    public function incomplete()
    {
        $this->rules = $this->incomplete_rules;
    }
}
