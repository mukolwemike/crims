<?php

namespace Cytonn\Clients;

use App\Cytonn\Models\ClientBankAccount;
use Laracasts\Commander\CommandHandler;

class ClientBankDetailsAddCommandHandler implements CommandHandler
{
    /**
     * Handle the command.
     *
     * @param object $command
     * @return void
     */
    public function handle($command)
    {
        $data = $command->data->payload;

        $client = $command->data->client;

        $account = new ClientBankAccount();

        if (isset($data['id'])) {
            is_null($data['id']) ?: $account = ClientBankAccount::findOrFail($data['id']);

            unset($data['id']);
        }

        if (isset($data['branch_id'])) {
            $account->fill(
                [
                    'client_id' => $client->id,
                    'branch_id' => $data['branch_id'],
                    'account_name' => $data['investor_account_name'],
                    'account_number' => $data['investor_account_number'],
                    'active' => isset($data['active']) ? $data['active'] : 1,
                    'currency_id' => $data['currency_id'],
                    'document_id' => isset($data['document_id']) ? $data['document_id']: null,
                    'country_id' => isset($data['country_id']) ? $data['country_id'] : null,
                ]
            );

            $account->save();
        }
    }
}
