<?php
/**
 * Date: 8/10/15
 * Time: 10:17 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Clients;

use App\Cytonn\Clients\ClientComplianceChecklist;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\TaxExemption;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\Setting;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\TransactionOwner;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Jobs\USSD\SendMessages;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Investment\JointInvestorAddCommand;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Project\UnitHoldingRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Laracasts\Commander\CommanderTrait;

class ClientRepository
{
    use CommanderTrait;

    protected $client;

    public function __construct(Client $client = null)
    {
        is_null($client) ? $this->client = new Client() : $this->client = $client;
    }

    public function save($clientdata)
    {
        return $this->client->add($clientdata);
    }

    public function getActiveInvestmentsForProduct(Product $product = null, $date = null)
    {
        $date = new Carbon($date);

        $inv = ClientInvestment::where(
            'client_id',
            $this->client->id
        )->activeOnDate($date);

        if ($product) {
            $inv->where('product_id', $product->id);
        }

        return $inv->get();
    }

    public function getTodayInvestedAmountForProduct($product, $date = null)
    {
        $date = new Carbon($date);

        return $this->getActiveInvestmentsForProduct($product, $date)->sum(
            function (ClientInvestment $investment) use ($date) {
                return $investment->repo->principal($date);
            }
        );
    }

    public function getTodayManagementFees()
    {
        return 0;
    }

    public function getCustodyFeesForProduct($product)
    {
        $investments = ClientInvestment::where(
            'client_id',
            $this->client->id
        )->where('product_id', $product->id)->get();

        $custody_fees_rate = Setting::where('key', 'custody_fee_rate')->first()->value;

        $custody_fees_array = [];

        foreach ($investments as $investment) {
            $maturity_date = new Carbon($investment->maturity_date);

            if ($maturity_date->isPast()) {
                $tenor = $investment->repo->getTenorInDays();
            } else {
                $tenor = $investment->repo->getTenorInDays('today');
            }

            array_push(
                $custody_fees_array,
                $custody_fees_rate * $investment->amount * $tenor / (365 * 100)
            );
        }

        return array_sum($custody_fees_array);
    }

    public function getCustodyFeesForProductAtDate($product, $date)
    {
        $investments = ClientInvestment::where(
            'client_id',
            $this->client->id
        )->where('product_id', $product->id)->activeOnDate($date)->get();

        $custody_fees_rate = Setting::where('key', 'custody_fee_rate')->first()->value;

        $fees = 0;

        foreach ($investments as $investment) {
            if ($investment->withdrawal_date) {
                $tenor = (new Carbon($date))->diffInDays(new Carbon($investment->invested_date));
            } else {
                $tenor1 = (new Carbon($date))->diffInDays(new Carbon($investment->invested_date));
                $tenor2 = (new Carbon($investment->maturity_date))
                    ->diffInDays(
                        new Carbon($investment->invested_date)
                    );
                $tenor2 > $tenor1 ? $tenor = $tenor1 : $tenor = $tenor2;
            }

            $fees += $custody_fees_rate * $investment->amount * $tenor / (365 * 100);
        }

        return $fees;
    }

    public function getTodayTotalInterestForProduct($product)
    {
        $investments = $this->getActiveInvestmentsForProduct($product);

        return $investments->sum(
            function ($investment) {
                return $investment->repo->getNetInterestForInvestment();
            }
        );
    }

    /**
     * @param $product
     * @return number
     */
    public function getTodayTotalGrossInterestForProduct($product)
    {
        $investments = $this->getActiveInvestmentsForProduct($product);

        return $investments->sum(
            function ($investment) {
                return $investment->repo->getGrossInterestForInvestment();
            }
        );
    }

    /**
     * @param $product
     * @return float
     */
    public function getTodayTotalWithHoldingTaxForProduct(Product $product)
    {
        return $this->getActiveInvestmentsForProduct($product)->sum(
            function (ClientInvestment $investment) {
                return $investment->repo->getWithholdingTaxAtDate(Carbon::today());
            }
        );
    }

    /**
     * @param $product
     * @return number
     */
    public function getTodayTotalPaymentForProduct($product)
    {
        // use only active
        $investments = $this->getActiveInvestmentsForProduct($product);

        return $investments->sum(
            function (ClientInvestment $investment) {
                return $investment->repo->getTotalPayments();
            }
        );
    }

    /**
     * Get the investments value belonging to a client
     *
     * @param  $product
     * @return number
     */
    public function getTodayTotalInvestmentsValueForProduct($product, $date = null, $as_at_next_day = false)
    {
        $date = new Carbon($date);

        $investments = $this->getActiveInvestmentsForProduct($product, $date);

        return $investments->sum(
            function (ClientInvestment $investment) use ($date, $as_at_next_day) {
                return $investment->repo->getTotalValueOfInvestmentAtDate($date, $as_at_next_day);
            }
        );
    }

    /*
     * Get the total value for all the products linked to the client
     * and factor in the exchange rates
     */
    public function getTodayTotalInvestmentsValueForAllProducts($date = null, $as_at_next_day = false)
    {
        $date = new Carbon($date);

        $investments = $this->getActiveInvestmentsForProduct();

        return $investments->sum(
            function (ClientInvestment $investment) use ($date, $as_at_next_day) {
                return $investment
                    ->repo
                    ->getKesConvertedValue($investment
                        ->repo
                        ->getTotalValueOfInvestmentAtDate($date, $as_at_next_day));
            }
        );
    }

    /**
     * @param $product
     * @return number
     */
    public function getTodayTotalTaxForProduct($product)
    {

        //use only active for product
        return ClientInvestment::where(
            'product_id',
            $product->id
        )->where(
            'client_id',
            $this->client->id
        )->where('withdrawn', '!=', '1')
            ->get()
            ->sum(
                function (ClientInvestment $investment) {
                    return $investment->repo->getWithholdingTaxAtDate(Carbon::today());
                }
            );
    }


    /**
     * @param $product
     * @return mixed
     */
    public function getClientsOfAProduct($product)
    {
        return Client::whereHas(
            'investments',
            function ($q) use ($product) {
                $q->where('product_id', $product->id);
            }
        );
    }

    /**
     * @param $product
     * @return bool
     */
    public function hasProduct($product)
    {
        $investments = ClientInvestment::where(
            'client_id',
            $this->client->id
        )
            ->where('product_id', $product->id)->count();

        return $investments > 0;
    }

    /**
     * @param $product
     * @param null $date
     * @return mixed
     */
    public function getInvestmentsForProductForMonth($product, $date = null)
    {
        $date = new Carbon($date);

        $startMonth = $date->copy()->startOfMonth()->toDateString();

        $endMonth = $date->copy()->endOfMonth()->toDateString();

        return ClientInvestment::where('product_id', $product->id)
            ->where('client_id', $this->client->id)
            ->where('invested_date', '<', $endMonth)
            ->where('withdrawal_date', '>', $startMonth)->get();
    }

    /**
     * @param $product
     * @param null $date
     * @return number
     */
    public function getClientBalanceForProductAtDate($product, $date = null)
    {
        $date = Carbon::parse($date);
        $investments = ClientInvestment::where('product_id', $product->id)
            ->where('client_id', $this->client->id)
            ->where('invested_date', '<=', $date)->get();

        $value_arr = [];
        foreach ($investments as $investment) {
            array_push($value_arr, $investment->repo->getTotalValueOfInvestmentAtDate($date));
        }

        return array_sum($value_arr);
    }

    /**
     * Suggest a new Clients client code
     *
     * @return int
     */
    public function suggestClientCode()
    {
        $codes = Client::withTrashed()->latest()
            ->take(100)
            ->pluck('client_code')
            ->map(function ($code) {
                return intval($code);
            })->toArray();

        $updatedCodes = Client::withTrashed()->latest('updated_at')
            ->take(100)
            ->pluck('client_code')
            ->map(function ($code) {
                return intval($code);
            })->toArray();

        $codes = array_merge($codes, $updatedCodes);

        return count($codes) < 1 ? 1000 : 1 + max($codes);
    }

    public function suggestShareHolderCode()
    {
        $codes = Collection::make(ShareHolder::withTrashed()->pluck('number'))->map(
            function ($code) {
                return intval($code);
            }
        )->toArray();


        if (count($codes) == 0) {
            return 1;
        }
        if (is_array($codes) && count($codes) > 1) {
            return sprintf("%04s", 1 + max($codes));
        } elseif ($codes instanceof Collection) {
            return sprintf("%04s", 1 + max($codes->all()));
        }

        if (count($codes) == 0) {
            return 1000;
        }

        return sprintf("%04s", 1 + max($codes));
    }

    /**
     * Check whether a client is compliant or not
     *
     * @param Carbon|null $date
     * @return bool
     */
    public function compliant(Carbon $date = null)
    {
        $required = $this->requiredComplianceChecklist()->lists('slug');

        $checked = ClientUploadedKyc::where('client_id', $this->client->id);

        if ($date) {
            $checked->where('created_at', '<=', $date);
        }

        $checked = $checked->get()->map(function ($kyc) {
            return $kyc->type->slug;
        })->all();

        return collect($required)->reduce(function ($carry, $req) use ($checked) {
            return $carry && in_array($req, $checked);
        }, true);
    }

    public function checkValidatedID()
    {
        return ClientUploadedKyc::where('client_id', $this->client->id)
            ->whereHas('type', function ($type) {
                $type->where('slug', 'id_or_passport');
            })->exists();
    }

    public function checkValidatedPIN()
    {
        return ClientUploadedKyc::where('client_id', $this->client->id)
            ->whereHas('type', function ($type) {
                $type->whereIn('slug', ['pin_individual', 'pin_corporate']);
            })->exists();
    }

    /**
     * @return bool
     */
    public function checkClientKycValidated()
    {
        return $this->compliant();
    }

    /**
     * Save the joint holder from form input
     *
     * @param array $data
     * @return mixed
     */
    public function editJointHolder(array $data)
    {
        $input = array_add($data, 'joint_id', $data['joint_holder_id']);

        unset($input['joint_holder_id']);

        $this->execute(JointInvestorAddCommand::class, ['data' => $input]);

        return true;
    }

    /**
     * @return bool
     */
    public function isActive($date = null)
    {
        $date = new Carbon($date);

        $invs = ClientInvestment::activeOnDate($date)->where('client_id', $this->client->id)->count();

        return $invs > 0;
    }


    /**
     * @return TransactionOwner
     */
    public function getClientTransactionOwner()
    {
        //get transaction owner from client id
        $owner = TransactionOwner::where('client_id', $this->client->id)->first();

        if ($owner == null) { //client not in transaction owners table, add
            $owner = new TransactionOwner();
            $owner->fund_investor_id = null;
            $owner->client_id = $this->client->id;
            $owner->fund_manager_id = $this->client->fund_manager_id;
            $owner->save();
        }

        return $owner;
    }

    /**
     * Add the tenors of all investments (months)
     *
     * @return mixed
     */
    public function sumInvestmentsTenor()
    {
        $investments = $this->client->investments;

        return $investments->sum(
            function (ClientInvestment $investment) {
                return $investment->repo->getTenorInMonths();
            }
        );
    }

    /**
     * The compliance documents a client can submit
     *
     * @return Collection
     */
    public function complianceChecklist()
    {
        return $this->fundManager()->complianceChecklist()
            ->where('client_type_id', $this->client->client_type_id)
            ->get();
    }

    /**
     * Documents required for a client to be compliant
     *
     * @return Collection
     */
    public function requiredComplianceChecklist()
    {
        return ClientComplianceChecklist::where('fund_manager_id', $this->fundManager()->id)
            ->where('client_type_id', $this->client->client_type_id)
            ->where('required', true)
            ->get();
    }

    /**
     * @return FundManager
     */
    public function fundManager()
    {
        if ($this->client->investments()->exists()) {
            return $this->client->investments()->first()->product->fundManager;
        }

        if ($this->client->unitFundPurchases()->exists()) {
            return $this->client->unitFundPurchases()->first()->unitFund->fundManager;
        }

        if ($this->client->applications()->whereNotNull('product_id')->exists()) {
            return $this->client->applications()->whereNotNull('product_id')->first()->product->fundManager;
        }

        if ($this->client->applications()->whereNotNull('unit_fund_id')->exists()) {
            return $this->client->applications()->whereNotNull('unit_fund_id')->first()->unitFund->fundManager;
        }

        return FundManager::first();
    }

    /**
     * Return pending KYC documents
     * @return \Illuminate\Support\Collection
     */
    public function pendingKYCDocuments()
    {
        $checklist_slugs = $this->complianceChecklist()->pluck('slug');
        $uploadedKyc = $this->client->uploadedKyc->map(function ($kyc) {
            return $kyc->type->slug;
        });

        return $checklist_slugs->reject(function ($required) use ($uploadedKyc) {
            return in_array($required, $uploadedKyc->all());
        })->map(function ($kyc_slug) {
            return ClientComplianceChecklist::where('slug', $kyc_slug)->first();
        });
    }

    /**
     * Return the document models
     * @return \Illuminate\Support\Collection
     */
    public function requiredPendingKYCDocuments()
    {
        $checklist_slugs = $this->requiredComplianceChecklist()->pluck('slug');

        $uploadedKyc = $this->client->uploadedKyc->map(function ($kyc) {
            return $kyc->type->slug;
        });

        return $checklist_slugs->reject(function ($required) use ($uploadedKyc) {
            return in_array($required, $uploadedKyc->all());
        })->map(function ($kyc_slug) {
            return ClientComplianceChecklist::where('slug', $kyc_slug)->first();
        });
    }

    /**
     * @return Carbon
     */
    public function getInvestedDate()
    {
        return $this->client->investments()->oldest('invested_date')->first()->invested_date;
    }

    /**
     * @return Carbon
     */
    public function getMaturityDate()
    {
        return $this->client->investments()->latest('maturity_date')->first()->maturity_date;
    }

    /**
     * @return bool
     */
    public function doesNotHaveActiveInvestments()
    {
        return $this->client->investments()->where('withdrawn', null)->count() == 0 and
            $this->client->investments()->where('withdrawn', 0)->count() == 0;
    }

    /**
     * @param $input
     * @return bool
     */
    public function checkIfIndividualClientExists($input)
    {
        return $this->duplicate($input)->exists;
    }

    /**
     * @param $input
     * @return bool
     */
    public function checkIfCorporateClientExists($input)
    {
        return $this->duplicate($input)->exists;
    }

    public function getActiveInvestmentsForClientQuery()
    {
        return ClientInvestment::with('product', 'product.currency')
            ->where('client_id', $this->client->id)
            ->where(
                function ($q) {
                    $q->whereNull('withdrawn')->orWhere('withdrawn', 0);
                }
            )->orderBy('invested_date', 'DESC');
    }

    public function getActiveInvestmentForClientForProduct($product)
    {
        return $this->getActiveInvestmentsForClientQuery()
            ->where('product_id', $product->id)
            ->get();
    }

    public function getActiveInvestmentTotalValueForClientForProduct($product)
    {
        $investments = $this->getActiveInvestmentForClientForProduct($product);

        return $investments->sum(
            function (ClientInvestment $investment) {
                return $investment->repo->getTotalValueOfInvestmentAtDate(Carbon::now());
            }
        );
    }

    public function getPendingTopupsForClient()
    {
        return $this->client->topupForms()
            ->doesntHave('investment')
            ->latest()
            ->where('created_at', '>=', Carbon::today()->subWeeks(2))
            ->get();
    }

    public function withdrawalsToBeReinvestedForProduct(Product $product, Carbon $date)
    {
        return $this->client->investments()->where('invested_date', '>', $date)
            ->where('product_id', $product->id)
            ->whereHas('reinvestedFrom', function ($q) use ($date) {
                $q->where('date', '<=', $date);
            })->sum('amount');
    }

    public function documentPassword()
    {
        $code = $this->client->client_code;

        preg_match_all('/\d+/', $code, $matches);

        if (isset($matches[0][0])) {
            return $matches[0][0];
        }

        return 'password';
    }

    public function duplicate(array $attrs = [])
    {
        $filter = function ($attrs, $keys) {
            return array_filter($attrs, function ($attr, $key) use ($keys) {
                return in_array($key, $keys);
            }, ARRAY_FILTER_USE_BOTH);
        };

        $client = $filter($attrs, ['id_or_passport', 'pin_no']);
        $contact = $filter($attrs, ['phone', 'email', 'registration_number']);

        $contact_q = Contact::where('id', '>', 0)->where('id', '<', 0);
        $client_q = Client::where('id', '>', 0)->where('id', '<', 0);

        $contact_q = $this->checkExists($contact_q, $contact);
        $client_q = $this->checkExists($client_q, $client);

        if ($this->client->id) {
            $client_q = $client_q->where('id', '!=', $this->client->id);
            $contact_q = $contact_q->where('id', '!=', $this->client->contact_id);
        }

        $contact_q = $contact_q->has('client');

        $exists = $client_q->exists() || $contact_q->exists();

        $clients = collect([])->merge($client_q->get());
        $clients = $clients->merge($contact_q->get()->map->client);

        return (object)[
            'exists' => $exists,
            'clients' => $clients->unique('id')
        ];
    }

    private function checkExists($query, $attrs)
    {
        $forbidden = [null, '-', 'N/A'];

        $query->orWhere(function ($query) use ($attrs, $forbidden) {
            foreach ($attrs as $key => $attr) {
                $notForbidden = !in_array($attr, $forbidden);

                if ($notForbidden) {
                    $query = $query->orWhere($key, $attr);
                }
            }
        });

        return $query;
    }

    public function clientInvestments()
    {
        $investments = new Collection();

        $inv = (new InvestmentsRepository())->clientInvInvestments($this->client);

        $inv->each(function ($in) use ($investments) {
            $investments->push($in);
        });

        return $investments;
    }

    public function clientReInvestments()
    {
        return (new UnitHoldingRepository())->getClientUnitHoldings($this->client);
    }

    public function clientReProjects(Client $client)
    {
        return Project::whereHas('units', function ($q) use ($client) {
            $q->whereHas('holdings', function ($q) use ($client) {
                $q->where('client_id', $client->id);
            });
        })->get()->map(function (Project $project) {
            return [
                'id' => $project->id,
                'name' => $project->name
            ];
        });
    }

    /*
     * Check if a guard exists
     */
    public function checkExistingGaurd($type, $id = null)
    {
        $guard = $this->client->guards()->where('guard_type_id', $type);

        if ($id) {
            $guard->where('id', '!=', $id);
        }

        return $guard->exists();
    }

    /*
     * Check for real estate gaurds
     */
    public function checkReGaurds(ClientTransactionApproval $approval)
    {
        if ($this->client->guards()->reGuard()->count() == 0) {
            return true;
        }

        if (!$approval->exemption) {
            throw new ClientInvestmentException(
                'A real estate guard is set for this client. Please seek exemption first inorder to approve'
            );
        }
    }

    public function resetAccounts()
    {
        return $this->client->bankAccounts->each(function ($account) {
            $account->default = null;
            $account->save();
        });
    }

    public function setDefault($acountId)
    {
        $bankAccount = ClientBankAccount::findOrFail($acountId);

        $bankAccount->default = 1;

        $bankAccount->save();

        return $bankAccount;
    }

    public function investmentOfType($type)
    {
        return ClientInvestment::active()
            ->ofType($type)
            ->where('client_id', $this->client->id)
            ->get();
    }

    /**
     * @param UnitFund $fund
     * @param null $date
     * @return float|int
     * @throws ClientInvestmentException
     */
    public function valueOfUnitsOwnedAsAt(UnitFund $fund, $date = null)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return $this->ownedNumberOfUnits($fund, $date->copy()) * $fund->unitPrice($date->copy());
    }

    /**
     * @param UnitFund $fund
     * @param null $date
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function ownedNumberOfUnits(UnitFund $fund, $date = null)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return $this->client->calculateFund($fund, $date->copy(), null, false)->totalUnits();
    }

    /**
     * @param UnitFund $fund
     * @param null $date
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function interestAccruedAsAt(UnitFund $fund, $date = null, $start = null, $asAtNextDate = false)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return $this->client->calculateFund($fund, $date->copy(), $start, $asAtNextDate)->totalInterest();
    }

    /**
     * @param UnitFund $fund
     * @param Carbon|null $date
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function getUnitsBoughtAsAt(UnitFund $fund, Carbon $date = null)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return $this->client->calculateFund($fund, $date->copy())->totalPurchases();
    }

    public function getUnitsFromDividendsAsAt(UnitFund $fund, Carbon $date)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return 0;
    }

    public function getUnitsSoldAsAt(UnitFund $fund, Carbon $date = null)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return $this->client->calculateFund($fund, $date->copy())->totalSales();
    }

    /**
     * @param UnitFund $fund
     * @param Carbon|null $date
     * @return mixed
     */
    public function isActiveInUnitFundAsAt(UnitFund $fund, Carbon $date = null)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return $this->client->unitFundPurchases()->forUnitFund($fund)->activeOnDate($date)->exists();
    }

    public function getFeesIncurredAsAt(UnitFund $fund, Carbon $date)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        return $this->client->getFeesIncurred()
            ->where('date', '<=', $date->copy())
            ->where('unit_fund_id', $fund->id)
            ->sum('amount');
    }

    public function exceededLockInPeriod(Carbon $date, UnitFund $fund)
    {
        $first = $fund->purchases()->where('client_id', $this->client->id)->oldest('date')->first();

        if (!$first) {
            return false;
        }

        return $date->diffInDays($first->date) > $fund->lockInDays();
    }


    /**
     * @param UnitFund $fund
     * @param Carbon|null $date
     * @return mixed
     */
    public function totalUnitPurchasesWithinLockPeriod(UnitFund $fund, Carbon $date = null)
    {
        $startDate = Carbon::parse($date)->subDays($fund->lockInDays());

        return $fund->purchases()->where('client_id', $this->client->id)
            ->where('date', '>=', $startDate)
            ->sum('number');
    }

    public function hasProducts()
    {
        return Client::where('id', $this->client->id)
            ->where(function ($q) {
                $q->has('clientPayments')->orHas('investments')->orHas('portfolioInvestments')->orHas('unitHoldings')
                    ->orHas('unitFundPurchases');
            })->exists();
    }

    public function uploadTaxExemption($input, $document_id = null)
    {
        $tax = (isset($input['tax_id'])) ? TaxExemption::find($input['tax_id']) : new TaxExemption();

        $tax->client_id = $input['client_id'];

        $tax->document_id = (!is_null($document_id)) ? $document_id : $tax->document_id;

        $tax->start = Carbon::parse($input['start_date']);

        $tax->end = Carbon::parse($input['end_date']);

        $tax->amount = $input['amount'];

        return $tax->save();
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public function signingMandates()
    {
        $signingMandates = [
            '0' => 'Singly',
            '1' => 'All to sign',
            '2' => 'Either to sign',
            '3' => 'At Least Two to sign'
        ];

        $signingMandates = collect($signingMandates)->map(function ($mandate, $key) {
            return [
                'name' => $mandate,
                'id' => $key
            ];
        });

        return $signingMandates;
    }

    /**
     * @param Carbon|null $date
     * @return mixed
     */
    public function totalInvestmentInflows(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        $investments = $this->client->investments()->where('invested_date', '<=', $date)
            ->whereIn('investment_type_id', [1, 2])
            ->sum('amount');

        $topups = ClientInvestmentTopup::where('date', '<=', $date)
            ->whereHas('toInvestment', function ($q) {
                $q->where('client_id', $this->client->id);
            })->sum('amount');

        return $investments + $topups;
    }

    /**
     * @return Client|\Illuminate\Database\Eloquent\Builder
     */
    public function getDigitalClientsWithNoInvestments()
    {
        return Client::where(function ($q) {
            $q->doesntHave('unitFundPurchases')->doesntHave('unitHoldings')
                ->doesntHave('investments')->doesntHave('portfolioInvestments')
                ->doesntHave('shareHolders')->doesntHave('clientTransactionApprovals');
        })->whereHas('applications', function ($q) {
            $q->whereHas('form', function ($q) {
                $q->whereIn('channel_id', [1, 2, 3]);
            });
        });
    }

    public function sendNotification()
    {
        $client = $this->client;

        $fund = UnitFund::where('short_name', 'CMMF')->first();

        $phone = Arr::first($client->getContactPhoneNumbersArray());

        if (isEmptyOrNull($phone) || !$phone) {
            return;
        }

        $clientArr = [
            'client_code' => $client->client_code,
            'client_name' => ClientPresenter::presentFullNameNoTitle($client->id),
            'phone' => implode(", ", $client->getContactPhoneNumbersArray()),
            'created_date' => \Carbon\Carbon::parse($client->created_at)->toDateString(),
            'last_reminder_date' => $client->incomplete_application_reminder,
            'account' => $fund->repo->collectionAccount('bank'),
            'mpesa' => $fund->repo->collectionAccount('mpesa'),
        ];

        dispatch((new SendMessages($phone, 'incomplete-appl-reminder', $clientArr)));

        $client->incomplete_application_reminder = Carbon::now();

        $client->save();
    }

    /**
     * @param UnitFund $fund
     * @param Carbon|null $date
     * @return bool
     */
    public function eligibleForAutomaticWithdrawal(UnitFund $fund, Carbon $date = null)
    {
        $date = Carbon::parse($date)->startOfMonth();

        return !($this->client->unitFundSales()->where('unit_fund_id', $fund->id)
            ->between($date, $date->copy()->endOfMonth())
            ->whereHas('approval', function ($q) {
                $q->where('approved_by', getSystemUser()->id);
            })->exists());
    }

    /**
     * @param Carbon|null $globalStart
     * @param Carbon|null $globalEnd
     * @return int
     */
    public function getConsecutiveDaysInvested(Carbon $globalStart = null, Carbon $globalEnd = null)
    {
        $globalEnd = Carbon::parse($globalEnd);

        $globalStart = $globalStart ? Carbon::parse($globalStart) : Carbon::parse('2015-01-01');

        $investments = $this->client->investments()->orderBy('invested_date')->activeBetweenDates($globalStart, $globalEnd)
            ->get(['invested_date', 'maturity_date', 'withdrawal_date'])->toArray();

        $purchases = $this->client->unitFundPurchases()->orderBy('date')->activeBetweenDates($globalStart, $globalEnd)
            ->get(['date', 'sale_date']);

        foreach ($purchases as $purchase) {
            $investments[] = [
                'invested_date' => \Cytonn\Core\DataStructures\Carbon::parse($purchase->date)->toDateTimeString(),
                'maturity_date' => $purchase->sale_date,
                'withdrawal_date' => null
            ];
        }

        $investments = collect($investments)->sortBy('invested_date');

        $startDate = $endDate = null;

        $days = 0;

        foreach ($investments as $investment) {
            $investedDate = Carbon::parse($investment['invested_date']);

            $maturityDate = $investment['withdrawal_date'] ? Carbon::parse($investment['withdrawal_date'])
                : Carbon::parse($investment['maturity_date']);

            $investedDate = $investedDate <= $globalStart ? $globalStart->copy() : $investedDate;

            $maturityDate = $maturityDate >= $globalEnd ? $globalEnd->copy() : $maturityDate;

            if ($startDate) {
                $investedDate = $investedDate <= $endDate ? $endDate->copy() : $investedDate;

                $maturityDate = $maturityDate <= $investedDate ? $investedDate->copy() : $maturityDate;
            }

            $days += $maturityDate->diffInDays($investedDate);

            $startDate = $investedDate->copy();

            $endDate = $maturityDate->copy();

            if ($maturityDate >= $globalEnd) {
                break;
            }
        }

        return $days;
    }

    /**
     * @param $months
     * @return Carbon|null
     */
    public function getNextLoyaltyRateChangeDate($months)
    {
        $globalEnd = Carbon::now();

        $targetDays = $months * 30;

        $globalStart = Carbon::parse('2015-01-01');

        $investments = $this->client->investments()->orderBy('invested_date')->activeBetweenDates($globalStart, $globalEnd)
            ->get(['invested_date', 'maturity_date', 'withdrawal_date'])->toArray();

        $purchases = $this->client->unitFundPurchases()->orderBy('date')->activeBetweenDates($globalStart, $globalEnd)
            ->get(['date', 'sale_date']);

        foreach ($purchases as $purchase) {
            $investments[] = [
                'invested_date' => \Cytonn\Core\DataStructures\Carbon::parse($purchase->date)->toDateTimeString(),
                'maturity_date' => $purchase->sale_date,
                'withdrawal_date' => null
            ];
        }

        $investments = collect($investments)->sortBy('invested_date');

        $startDate = $endDate = $targetDate = null;

        $days = 0;

        foreach ($investments as $investment) {
            $investedDate = Carbon::parse($investment['invested_date']);

            $maturityDate = $investment['withdrawal_date'] ? Carbon::parse($investment['withdrawal_date'])
                : Carbon::parse($investment['maturity_date']);

            $investedDate = $investedDate <= $globalStart ? $globalStart->copy() : $investedDate;

            $maturityDate = $maturityDate >= $globalEnd ? $globalEnd->copy() : $maturityDate;

            if ($startDate) {
                $investedDate = $investedDate <= $endDate ? $endDate->copy() : $investedDate;

                $maturityDate = $maturityDate <= $investedDate ? $investedDate->copy() : $maturityDate;
            }

            $days += $maturityDate->diffInDays($investedDate);

            if ($days >= $targetDays) {
                $dayDifference = $days - $targetDays;

                $targetDate = $maturityDate->copy()->subDays($dayDifference);

                break;
            }

            $startDate = $investedDate->copy();

            $endDate = $maturityDate->copy();
        }

        if (is_null($targetDate)) {
            $dayDifference = $targetDays - $days;

            $targetDate = $endDate->copy()->addDays($dayDifference);
        }

        return $targetDate;
    }

    /**
     * @param Carbon|null $start
     * @param Carbon|null $end
     * @return float
     */
    public function getConsecutiveMonthsInvested(Carbon $start = null, Carbon $end = null)
    {
        return round($this->getConsecutiveDaysInvested($start, $end) / 30, 0);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param null $remainingValue
     * @return bool
     * @throws ClientInvestmentException
     */
    public function checkForInvestmentGuards(ClientTransactionApproval $approval, $remainingValue = null)
    {
        if ($approval->exemption) {
            return true;
        }

        if ($this->client->guards()->investmentsGuard()->count() == 0) {
            return true;
        }

        foreach ($this->client->guards as $guard) {
            switch ($guard->guard_type_id) {
                case 1:
                    throw new ClientInvestmentException(
                        'A withdrawal guard is set for this client. Please seek exemption first 
                            inorder to approve'
                    );
                    break;
                case 2:
                    if ($remainingValue < $guard->minimum_balance) {
                        throw new ClientInvestmentException(
                            "A minimum balance guard of " .
                            \Cytonn\Presenters\AmountPresenter::currency($guard->minimum_balance) .
                            " is set for this client. Please seek exemption first inorder to approve"
                        );
                    }

                    break;
                case 3:
                    $portfolioInvestor = $guard->portfolioInvestor;

                    $portfolioAmount = $portfolioInvestor->repo->totalDepositValueForAllProducts();

                    if ($remainingValue < $portfolioAmount) {
                        throw new ClientInvestmentException('A portfolio guard is set for this client.
                             Please seek exemption first inorder to approve');
                    }

                    break;
            }
        }
    }
}
