<?php

namespace Cytonn\Clients;

use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\Title;
use Carbon\Carbon;
use Cytonn\Presenters\ContactPresenter;
use Laracasts\Presenter\Presenter;

class ClientPresenter extends Presenter
{
    /*
     * Get the signing mandate
     */
    public function getSigningMandate()
    {
        $signingMandates = [
            '0' => 'Singly',
            '1' => 'All to sign',
            '2' => 'Either to sign',
            '3' => 'At Least Two to sign'
        ];

        if (is_null($this->signing_mandate)) {
            $this->signing_mandate = 0;
        }

        return $signingMandates[$this->signing_mandate];
    }

    /*
     * Get the combined fullname
     */
    public function clientCodeAndFullname()
    {
        return $this->client_code . ' - ' . ContactPresenter::fullName($this->contact_id);
    }

    public function fullName()
    {
        return \Cytonn\Presenters\ClientPresenter::presentJointFullNames($this->entity->id);
    }

    public function firstName()
    {
        return \Cytonn\Presenters\ClientPresenter::presentJointFirstNames($this->entity->id);
    }

    /**
     * @return mixed
     */
    public function firstNameLastName()
    {
        return \Cytonn\Presenters\ClientPresenter::presentFirstNameLastName($this->entity->id);
    }

    public function shortName()
    {
        return \Cytonn\Presenters\ClientPresenter::presentShortName($this->entity->id);
    }

    protected function jointShortName()
    {
        return \Cytonn\Presenters\ClientPresenter::presentJointShortNames($this->entity->id);
    }

    /**
     * @return mixed|string
     */
    protected function jointFirstNameLastName()
    {
        return \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($this->entity->id);
    }

    /**
     * @return string
     */
    public function address()
    {
        return \Cytonn\Presenters\ClientPresenter::presentAddress($this->entity->id);
    }

    /**
     * @return string
     */
    public function contactPerson()
    {
        return $this->contact_person_fname.' '.$this->contact_person_lname;
    }

    /**
     * @return string
     */
    public function contactPersonShortName()
    {
        $title = Title::find($this->contact_person_title);

        $title = $title ? $title->name : "";

        return $title . ' ' . $this->contact_person_lname;
    }

    /*
     * Determine the type of client based on investment amount
     */
    public function ratesClientType($productId, $amount = 0)
    {
        $sum = $this->entity->investments()->active()->get()
            ->sum(
                function ($investment) {
                    return convert(
                        $investment->amount,
                        $investment->product->currency,
                        $investment->invested_date
                    );
                }
            );

        $totalValue = $sum + $amount;

        return (floatval($totalValue) >= 500000) ? 1 : 2;
    }

    public function ratesClientAmount($productId, $amount = 0)
    {
        $sum = $this->entity->investments()->active()->get()
            ->sum(
                function ($investment) {
                    return convert(
                        $investment->amount,
                        $investment->product->currency,
                        $investment->invested_date
                    );
                }
            );

        $totalValue = $sum + $amount;

        return $totalValue;
    }

    /*
     * Get the country for a client
     */
    public function getCountry()
    {
        return $this->country ? $this->country->name : '';
    }

    /*
     * get nationality of a client
     */

    public function getNationality()
    {
        return $this->nationality ? $this->nationality->name : '';
    }

    /**
     * @return string
     */
    public function getResidency()
    {
        return $this->country && $this->country->id != "114" ? "Non Resident" : "Resident";
    }

    /*
     * Get normal address
     */
    public function getAddress()
    {
        if (strlen(trim($this->postal_address)) > 0) {
            return 'P.O Box ' . $this->postal_address . ' - ' . $this->postal_code . ' ' . $this->town;
        } else {
            return $this->street;
        }
    }

    /**
     * @return string
     */
    public function getReinvestmentInterestType()
    {
        if ($this->combine_interest_reinvestment == 1) {
            return "Combine All Reinvested Interest";
        } elseif ($this->combine_interest_reinvestment == 2) {
            return "Combined Reinvested Interest Monthly";
        } else {
            return "Dont Combine Reinvested Interest";
        }
    }

    /**
     * @return mixed|null
     */
    public function getAnyPhone()
    {
        if (isNotEmptyOrNull($this->contact->phone)) {
            return $this->contact->phone;
        } elseif (isNotEmptyOrNull($this->phone)) {
            return $this->phone;
        } elseif (isNotEmptyOrNull($this->telephone_home)) {
            return $this->telephone_home;
        } elseif (isNotEmptyOrNull($this->telophone_office)) {
            return $this->telophone_office;
        }

        return null;
    }

    /**
     * @return int|string
     */
    public function getRetirementAge()
    {
        return isEmptyOrNull($this->date_of_retirement) ? '' :
            Carbon::parse($this->date_of_retirement)->diffInYears(Carbon::now());
    }

    /**
     * @return ClientBankAccount
     */
    public function getDefaultAccount()
    {
        $account = $this->entity->bankAccounts()->where(['default' => 1, 'active' => 1])->latest()->first();

        if (!$account) {
            $account =  $this->entity->bankAccounts()->where('active', 1)->latest()->first();
        }

        if (!$account) {
            $account = new ClientBankAccount();
        }

        return $account;
    }
}
