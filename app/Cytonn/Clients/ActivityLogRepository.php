<?php

namespace App\Cytonn\Clients;

use App\Cytonn\Api\Transformers\Clients\LoyaltyRedeemInstructionsTransformer;
use App\Cytonn\Api\Transformers\Clients\RealEstatePaymentTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Models\RealEstatePayment;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Support\Collection;

class ActivityLogRepository
{
    use AlternateSortFilterPaginateTrait;

    public function briefActivityLog(Client $client, $perPage = 5)
    {
        $investments = ClientInvestment::where('client_id', $client->id)
            ->latest('invested_date');

        if ($perPage) {
            $investments->skip(0)->take($perPage);
        } else {
            $investments->where('invested_date', '>=', Carbon::now()->subMonths(6));
        }

        $investments = $investments->get()->each(function (ClientInvestment $investment) {
            $investment->transaction = $investment->type->name;
            unset($investment->investmentType);
            $investment->currency = $investment->product->currency->code;
            $investment->transaction_date = $investment->invested_date;
            $investment->id = $investment->uuid;
            unset($investment->uuid);
        });

        //withdrawals
        $withdrawals = ClientInvestmentWithdrawal::whereHas('investment', function ($investment) use ($client) {
            $investment->where('client_id', $client->id);
        })->latest('date')->whereNull('reinvested_to')->where('amount', '!=', 0);

        if ($perPage) {
            $withdrawals->skip(0)->take($perPage);
        } else {
            $withdrawals->where('date', '>=', Carbon::now()->subMonths(6));
        }

        $withdrawals = $withdrawals->get()->each(function (ClientInvestmentWithdrawal $withdrawal) {
            $withdrawal->transaction = $withdrawal->withdraw_type == 'interest' ? 'interest_withdrawal' : 'withdrawal';
            $withdrawal->currency = $withdrawal->investment->product->currency->code;
            $withdrawal->transaction_date = $withdrawal->date;
            $withdrawal->id = $withdrawal->uuid;
            unset($withdrawal->uuid);
            unset($withdrawal->withdraw_amount);
        });

        $all = new Collection();
        $all = $all->merge($investments);
        $all = $all->merge($withdrawals);

        return $all;
    }

    public function briefReActivityLog(Client $client)
    {
        return $this->sortFilterPaginate(

            new RealEstatePayment(),
            [],
            function ($payment) {
                return app(RealEstatePaymentTransformer::class)->transformPayment($payment);
            },
            function ($model) use ($client) {
                return $model->whereHas('holding', function ($holding) use ($client) {
                    $holding->active()->where('client_id', $client->id);
                });
            }
        );
    }
}
