<?php

namespace App\Cytonn\Clients;

use App\Cytonn\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class ApplicationTokenVerification extends BaseModel
{
    use PresentableTrait, SoftDeletes;

    protected $guarded = ['id'];

    protected $table = "application_token_verification";
}
