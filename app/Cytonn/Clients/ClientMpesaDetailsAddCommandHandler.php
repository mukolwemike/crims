<?php

namespace Cytonn\Clients;

use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use Laracasts\Commander\CommandHandler;

class ClientMpesaDetailsAddCommandHandler implements CommandHandler
{
    /**
     * Handle the command.
     *
     * @param object $command
     * @return void
     */
    public function handle($command)
    {
        $data = $command->data->payload;

        $client = $command->data->client;

        $account = new ClientBankAccount();

        $branch = ClientBankBranch::where('swift_code', 'MPESA')->first();

        if (isset($data['id'])) {
            is_null($data['id']) ?: $account = ClientBankAccount::findOrFail($data['id']);

            unset($data['id']);
        }

        if (isset($data['phone_number'])) {
            $account->fill(
                [
                    'client_id' => $client->id,
                    'branch_id' => $branch->id,
                    'account_name' => \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($client->id),
                    'account_number' => $data['phone_number'],
                    'active' => isset($data['active']) ? $data['active'] : 1,
                    'currency_id' => isset($data['currency_id']) ? $data['currency_id'] : 1,
                    'country_id' => isset($data['country_id']) ? $data['country_id'] : null,
                    'document_id' => isset($data['document_id']) ? $data['document_id']: null,
                ]
            );
            $account->save();
        }
    }
}
