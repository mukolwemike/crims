<?php
/**
 * Date: 30/04/2016
 * Time: 9:52 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Events;

use App\Cytonn\Models\ClientTransactionApproval;

class ApprovalIsReady
{
    public $approval;

    /**
     * InvestmentApprovalIsReady constructor.
     *
     * @param $approval
     */
    public function __construct(ClientTransactionApproval $approval)
    {
        $this->approval = $approval;
    }
}
