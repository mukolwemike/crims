<?php
/**
 * Date: 30/04/2016
 * Time: 11:02 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Events;

use App\Cytonn\Models\ClientTransactionApproval;

class ApprovalSuccessful
{
    public $approval;

    public $next;

    /**
     * InvestmentApprovalSuccessful constructor.
     *
     * @param $approval
     * @param $next
     * next represents the next action after approval, e.g. show view or redirect back
     */
    public function __construct(ClientTransactionApproval $approval, $next)
    {
        $this->approval = $approval;
        $this->next = $next;
    }
}
