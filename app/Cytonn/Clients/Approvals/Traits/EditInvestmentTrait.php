<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Clients\Approvals\Traits;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;

trait EditInvestmentTrait
{
    /**
     * @param ClientTransactionApproval $approval
     * @return bool
     * @throws ClientInvestmentException
     */
    public function checkForExemption(ClientTransactionApproval $approval)
    {
        if ($approval->exemption) {
            return true;
        }

        $investment = ClientInvestment::findOrFail($approval->payload['investment']);

        $otherApprovals = ClientTransactionApproval::where('client_id', $investment->client_id)
            ->where('id', '!=', $approval->id)
            ->where('transaction_type', 'edit_investment')
            ->where('approved', 1)
            ->where('approved_on', '>=', Carbon::now()->subDay())
            ->get();

        if (count($otherApprovals) == 0) {
            return true;
        }

        $otherApprovals = $otherApprovals
            ->filter(function (ClientTransactionApproval $existingApproval) use ($investment, $approval) {
                return $existingApproval->payload['investment'] == $investment->id &&
                    $existingApproval->sent_by != $approval->sent_by;
            });

        if (count($otherApprovals) == 0) {
            return true;
        }

        throw new ClientInvestmentException(
            'The investment has previously been edited within the last 24 hrs. Please seek exemption first 
                            inorder to approve'
        );
    }
}
