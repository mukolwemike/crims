<?php
/**
 * Date: 22/01/2018
 * Time: 12:10
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Engine;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use App\Events\Approvals\ClientTransactionApproved;
use App\Exceptions\CrimsException;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Exceptions\AuthorizationDeniedException;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStage;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStep;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalType;
use Exception;

class Approval
{
    use LocksTransactions;

    protected $approval;

    /**
     * Approval constructor.
     *
     * @param $approval
     */
    public function __construct(ClientTransactionApproval $approval)
    {
        $this->approval = $approval;
    }

    /**
     * @return mixed
     * @throws AuthorizationDeniedException
     * @throws CRIMSGeneralException
     * @throws Exception
     */
    public function approve()
    {
        $locks = [];
        try {
            $locks = $this->getLocks();

            $result = $this->executeApproval();
        } catch (Exception $exception) {
            throw $exception;
        } finally {
            $this->releaseLocks($locks);
        }

        return $result;
    }

    /**
     * @throws Exception
     */
    public function getLocks()
    {
        $locks = [];

        $client_id = $this->approval->client ? $this->approval->client->id : null;
        $key = get_class($this->approval->handler()).'_client_'.$client_id;
        $approval_id = 'approval_'.$this->approval->id;

        $locks[] = $this->lock($key);
        $locks[] = $this->lock('client_id'.$client_id);
        $locks[] = $this->lock($approval_id);

        if ($this->approval->fresh()->approved) {
            throw new \InvalidArgumentException("Approval {$this->approval->id} ".
                "{$this->approval->transaction_type} is Already approved!");
        }

        return $locks;
    }

    /**
     * @throws Exception
     */
    public function releaseLocks($locks)
    {
        foreach ($locks as $lock) {
            !$lock ? : $this->releaseAtomicLock($lock);
        }
    }

    /**
     * @param $key
     * @return bool|null
     * @throws Exception
     */
    private function lock($key)
    {
        if ($key) {
            return $this->getAtomicLock($key, 1800, 120);
        }

        return null;
    }


    /**
     * @return mixed
     * @throws AuthorizationDeniedException
     * @throws CRIMSGeneralException
     * @throws Exception
     */
    public function executeApproval()
    {
//        $this->lockForUpdate();

        //get the necessary stage
        $stage = $this->nextStage();

        //check access
        $this->checkAccess($stage);

        $handler = $this->approval->handler();

        //run any pre-approval validations
        if (method_exists($handler, 'preApproval')) {
            $handler->preApproval();
        }

        $this->validateApproval($stage);

        //approve
        $step = ClientTransactionApprovalStep::create([
            'approval_id' => $this->approval->id,
            'stage_id' => $stage->id,
            'user_id'  => \Auth::user()->id
        ]);

        if ($this->checkFullyApproved($step)) {
            $this->approval->approve();

            return $this->handle();
        }

        \Flash::success('The transaction has been sent to the next stage of approval');

        $next = redirect()->back();

        $this->approval->raise(new ApprovalSuccessful($this->approval, $next));

        return $this->approval->dispatchEventsFor($this->approval);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function handle()
    {
        $handler = $this->approval->handler();

        if (!($handler instanceof ApprovalHandlerInterface)) {
            throw new Exception('The handler should be an instance of ApprovalHandlerInterface');
        }

        $output = $this->approval->handler()->handle($this->approval);

        \Flash::success('Transaction is now fully approved, it has now been executed');

        event(new ClientTransactionApproved($this->approval));

        return $output;
    }

    private function checkFullyApproved(ClientTransactionApprovalStep $step)
    {
        $stages = $this->getRemainingStages()->reject(function ($stage) use ($step) {
            return $step->stage_id == $stage->id;
        });

        if ($nxt = $stages->first()) {
            $this->approval->update(['awaiting_stage_id' => $nxt->id]);
        }

        return $stages->count() == 0;
    }

    /**
     * @return ClientTransactionApproval|ClientTransactionApprovalType|\Illuminate\Database\Eloquent\Model|null
     * @throws ClientInvestmentException
     */
    public function nextStage()
    {
        return $this->getRemainingStages()->first();
    }

    /**
     * @return ClientTransactionApproval|ClientTransactionApprovalType|null
     * @throws ClientInvestmentException
     */
    private function getRemainingStages()
    {
        $type = $this->transactionType();

        if (!$type) {
            throw new ClientInvestmentException("Transaction type could not be found");
        }

        $approved = $this->approval->steps->pluck('stage_id')->all();

        return $type->allStages()->filter(function (ClientTransactionApprovalStage $stage) use ($approved) {
            return !in_array($stage->id, $approved);
        });
    }

    /**
     * @param $stage
     * @return bool
     * @throws AuthorizationDeniedException
     */
    public function checkAccess($stage)
    {
        if (\App::environment('testing') || \App::environment('local') || $stage->applies_to_all) {
            return true;
        }

        if ($stage->approvers()->where('user_id', \Auth::user()->id)->exists()) {
            return true;
        }

        throw new AuthorizationDeniedException('You cannot approve this transaction');
    }

    private function transactionType()
    {
        return $this->approval->type();
    }

    public function lockForUpdate()
    {
        $this->lockModelForUpdate($this->approval);

        if ($this->approval->client) {
            $this->lockModelForUpdate($this->approval->client);
        }

        if ($this->approval->investment) {
            $this->lockModelForUpdate($this->approval->investment);
        }
    }

    /**
     * @param $stage
     * @throws CRIMSGeneralException
     */
    private function validateApproval($stage)
    {
        if (is_null($stage)) {
            throw new CRIMSGeneralException("Request does not seem to have next step, try again");
        }

        $step = $this->approval->steps()->where('stage_id', $stage->id);

        if ($step->exists() || $this->approval->approved) {
            throw new CRIMSGeneralException("Transaction is already approved");
        }
    }

    /**
     * @param User $user
     * @throws CrimsException
     */
    public function forceApprove(User $user)
    {
        if ($user->username != 'system') {
            throw new CrimsException("Only system allowed to force approve transactions");
        }

        $this->approval->type()->allStages()->each(function ($stage) use ($user) {
            ClientTransactionApprovalStep::create([
                'approval_id' => $this->approval->id,
                'stage_id' => $stage->id,
                'user_id'  => $user->id
            ]);
        });

//        $this->approval->approve($user);
    }

    /**
     * @throws Exception
     */
    public function systemExecute()
    {
        $locks = [];

        try {
            $this->lockForUpdate();

            $locks = $this->getLocks();

            $this->forceApprove(getSystemUser());

            $this->approval->handler()->handle($this->approval);

            $this->approval->approve(getSystemUser());
        } catch (\Exception $e) {
            throw $e;
        } finally {
            $this->releaseLocks($locks);
        }
    }
}
