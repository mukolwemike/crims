<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Investment\Seip\SeipInvestment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Events\InvestmentHasBeenEdited;
use Illuminate\Support\Facades\Response;

class ExtendInvestmentPaymentSchedule implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        $oldInvestment = ClientInvestment::findOrFail($data['investment_id']);

        $investment->update([
            'maturity_date' => $data['maturity_date']
        ]);

        $approval->raise(new InvestmentHasBeenEdited($investment, $oldInvestment, $approval, $data));

        $approval->dispatchEventsFor($approval);

        $investment->childInvestments->each(function (ClientInvestment $investment) use ($data, $approval) {
            $oldInvestment = ClientInvestment::findOrFail($investment->id);

            $investment->update([
                'maturity_date' => $data['maturity_date']
            ]);

            $approval->raise(new InvestmentHasBeenEdited($investment, $oldInvestment, $approval, $data));

            $approval->dispatchEventsFor($approval);
        });

        (new SeipInvestment())->extendSchedules($investment, $oldInvestment);

        \Flash::success('The investment payment schedule has been extended');

        $next = \redirect('/dashboard/investments/investment_payment_schedules/' . $investment->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        return [
            'investment' => $investment,
            'data' => $data
        ];
    }
}
