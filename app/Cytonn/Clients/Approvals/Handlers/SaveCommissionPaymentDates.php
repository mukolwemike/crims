<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\BulkCommissionPayment;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Commission\Base;
use Cytonn\Realestate\Commissions\Calculator;
use Illuminate\Support\Facades\Response;

/**
 * Class SaveCommissionPaymentDates
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class SaveCommissionPaymentDates implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $data['approval_id'] = $approval->id;

        // Save Commission Payment Dates
        $bcp = new BulkCommissionPayment();
        $bcp->fill($data);
        $bcp->save();

        \Flash::success('The commission payment dates have been saved');

        $next = \redirect("/dashboard/investments/commission/payment-dates");

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }


    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        return ['data'=>$data];
    }
}
