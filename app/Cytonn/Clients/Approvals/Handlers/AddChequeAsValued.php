<?php
/**
 * Date: 06/03/2018
 * Time: 16:31
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class AddChequeAsValued extends AbstractApproval
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $transaction = CustodialTransaction::findOrFail($approval->payload['transaction_id']);
        $transaction->repo->valueCheque();

        $payment = $transaction->clientPayment;
        $payment->repo->valueCheque();

        \Flash::success('The cheque has now been valued');
        $next = redirect()->back();

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $transaction = CustodialTransaction::findOrFail($approval->payload['transaction_id']);

        $payment = $transaction->clientPayment;

        $value = $transaction->amount != 0 ? $transaction->amount : $transaction->value;

        return ['transaction' => $transaction, 'payment' => $payment, 'client' => $payment->client, 'value' => $value];
    }
}
