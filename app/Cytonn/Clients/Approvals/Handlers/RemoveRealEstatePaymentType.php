<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentType;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class RemoveRealEstatePaymentType implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * AddBankDetails constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws \Exception
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);
        $data = $approval->payload;

        $type = isset($data['id']) ? RealEstatePaymentType::findOrFail($data['id']) : null;

        if ($type->schedules()->count() > 0) {
            throw new ClientInvestmentException(
                'There is an existing payment schedule for this type, you cannot delete it'
            );
        }

        $type->delete();

        \Flash::success('RealEstate payment type removed successfully');

        $next = \Redirect::to("/dashboard/realestate/projects");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $type = isset($data['id']) ? RealEstatePaymentType::findOrFail($data['id']) : null;

        return [
            'type' => $type
        ];
    }
}
