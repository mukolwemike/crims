<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\StatementCampaignType;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateUnitFundStatementCampaign implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * UpdateUnitFundStatementCampaign constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files', 'fee_id']);

        $campaign = UnitFundStatementCampaign::findOrFail($data['fee_id']);
        $campaign->update($input);

        if ($campaign->type->slug == 'automated') {
            \Queue::push(
                'Cytonn\Unitization\Reporting\StatementCampaignRepository@fireAutomaticallyAddClients',
                ['campaign_id'=>$campaign->id]
            );
        }



        \Flash::success('Unit fund statement campaign has been successfully updated');

        $next = \Redirect::to($campaign->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $campaign = UnitFundStatementCampaign::findOrFail($data['campaign_id']);

        $type = StatementCampaignType::findOrFail($data['type_id']);

        return ['data'=>$data, 'campaign'=>$campaign, 'type'=>$type];
    }
}
