<?php
/**
 * Date: 17/06/2016
 * Time: 4:38 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Floor;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\ProjectFloor;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstateType;
use App\Cytonn\Models\RealestateUnitSize;
use App\Cytonn\Models\Type;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Cytonn\Realestate\Commands\SaveTrancheCommand;

class RealEstateProjectTranche implements ApprovalHandlerInterface
{
    use CommanderTrait;
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $this->execute(SaveTrancheCommand::class, $data);

        $next = \Redirect::back();

        \Flash::success('The tranche has been saved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $project = function ($id) {
            return Project::findOrFail($id);
        };

        $unitsize = RealestateUnitSize::all();
        $findPaymentPlan = function ($id) {
            return RealEstatePaymentPlan::findOrFail($id);
        };

        $realestateunitsize = function ($id) {
            return RealestateUnitSize::findOrFail($id);
        };

        $floor = function ($id) {
            return Floor::find($id);
        };

        $type = function ($id) {
            return Type::find($id);
        };

        return [
            'data'=>$data,
            'project'=>$project,
            'unitsize'=>$unitsize,
            'paymentplan'=>$findPaymentPlan,
            'realestateunitsize'=>$realestateunitsize,
            'floor'=>$floor,
            'type'=>$type
        ];
    }
}
