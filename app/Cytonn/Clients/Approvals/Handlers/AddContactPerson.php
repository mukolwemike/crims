<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/17/16
 * Time: 10:37 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientContactPerson;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Forms\ContactPersonAddForm;
use Illuminate\Support\Facades\App;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Investment\ContactPersonAddCommand;

class AddContactPerson implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * AddContactPerson constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $form = App::make(ContactPersonAddForm::class);

        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $data = $approval->payload;

        $form->validate($data);

        $this->execute(ContactPersonAddCommand::class, ['data'=>$data]);



        \Flash::success('Client contact person saved');

        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $client = $approval->client;

        $contactPerson = function ($id) {
            return ClientContactPerson::findOrFail($id);
        };

        return ['client'=>$client, 'data'=>$approval->payload, 'contactPerson' => $contactPerson];
    }
}
