<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstatePaymentType;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Support\Arr;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateRealEstatePaymentSchedule implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;

    /**
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     * @return null
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $paymentSchedule = RealEstatePaymentSchedule::findOrFail($data['schedule_id']);

        //Check for real estate guards
        $paymentSchedule->holding->client->repo->checkReGaurds($approval);

        $d = Arr::except($data, ['penalty_exempt', 'schedule_id']);

        $paymentSchedule->update($d);

        $paymentSchedule->holding->repo->setLastSchedule();

        \Flash::success('The payment schedule has been updated');

        $next = \Redirect::to('/dashboard/realestate/payments-schedules/show/' . $paymentSchedule->holding->id);

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;


        $paymentSchedule = RealEstatePaymentSchedule::findOrFail($data['schedule_id']);
        $holding = $paymentSchedule->holding;
        $schedules = RealEstatePaymentSchedule::where('unit_holding_id', $holding->id)->get();
        $total = $schedules->sum('amount');

        $paymenttypes = function ($id) {
            return RealEstatePaymentType::find($id);
        };

        return [
            'holding' => $holding,
            'schedules' => $schedules,
            'total' => $total,
            'schedule' => $data,
            'paymentSchedule' => $paymentSchedule,
            'paymenttypes' => $paymenttypes
        ];
    }
}
