<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharesPriceTrail;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Coop\Payments\CoopPaymentRepository;
use Cytonn\Shares\ShareHoldingRepository;

/**
 * Class BuyCoopShares
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class BuyCoopShares implements ApprovalHandlerInterface
{
    protected $coopPaymentRepository;

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $data['client_id'] = $approval->client_id;

        $repo = new ShareHoldingRepository();

        if (isset($data['purchase_price'])) {
            if ((int)$data['purchase_price'] > 0) {
                $data['price'] = $data['purchase_price'];
            }
        }
        if (!isset($data['price'])) {
            $data['price'] = SharesPriceTrail::getPrice($data['entity_id'], $data['date']);
        }

        $data['number'] = floor($data['amount'] / $data['price']);


        $data['approval_id'] = $approval->id;
        
        $repo->createHoldingWithPayment($data);




        \Flash::success('Purchase of Shares was successfully made!');
        $next = \Redirect::to('/dashboard/clients/details/'.$data['client_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;


        $value_of_share = !is_null($data['purchase_price']) ?
            $data['purchase_price']
                : SharesPriceTrail::getPrice($data['entity_id'], $data['date']);

        $number_of_shares = floor((double)$data['amount'] / $value_of_share);
        $client = Client::findOrFail($data['client_id']);

        return ['data'=>$data, 'number_of_shares'=>$number_of_shares, 'client'=>$client];
    }
}
