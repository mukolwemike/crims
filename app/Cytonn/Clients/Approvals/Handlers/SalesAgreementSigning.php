<?php
/**
 * Date: 27/07/2016
 * Time: 7:01 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Realestate\Events\SalesAgreementHasBeenSigned;
use Illuminate\Support\Facades\Response;
use App\Cytonn\Models\RealEstateSalesAgreement;

class SalesAgreementSigning implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $date = $approval->payload['date'];

        $salesAgreement = RealEstateSalesAgreement::findOrFail($approval->payload['sales_agreement_id']);

        //Check for real estate guards
        $salesAgreement->holding->client->repo->checkReGaurds($approval);

        $salesAgreement->update(
            [
            'signed_on' => $date
            ]
        );

        $salesAgreement->raise(new SalesAgreementHasBeenSigned($salesAgreement));
        $salesAgreement->dispatchEventsFor($salesAgreement);

        $next = \Redirect::to('/dashboard/realestate/projects');

        \Flash::success('The sales agreement has been updated');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $salesAgreement = RealEstateSalesAgreement::findOrFail($data['sales_agreement_id']);
        $project = $salesAgreement->holding->project;
        $unit = $salesAgreement->holding->unit;
        $client = $salesAgreement->holding->client;

        return ['project'=>$project, 'unit'=>$unit, 'client'=>$client, 'salesAgreement'=>$salesAgreement];
    }
}
