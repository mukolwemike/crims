<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use App\Cytonn\Models\SharesPurchaseOrder;

/**
 * Class CancelSharesPurchaseOrder
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class CancelSharesPurchaseOrder implements ApprovalHandlerInterface
{

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $order = SharesPurchaseOrder::findOrFail($data['order_id']);

        $order->cancelled = true;
        $order->save();



        \Flash::success('Shares purchase order was successfully cancelled!');
        $next = \redirect('/dashboard/shares/purchases/show/'.$order->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $order = SharesPurchaseOrder::findOrFail($data['order_id']);

        return ['data'=>$data, 'order'=>$order];
    }
}
