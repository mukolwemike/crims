<?php
/**
 * Date: 20/05/2016
 * Time: 3:11 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Title;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\Validation\ValidatorTrait;

/**
 * Class EditClient
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class EditClient implements ApprovalHandlerInterface
{
    use ValidatorTrait;

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $client = $approval->client;

        $data = array_except($approval->payload, [
            'client_uuid',
            'employment',
            'sector',
            'occupation',
            'employer',
            'client_id'
        ]);

        $filtered_data = $this->separateContactData($data, $client);

        $client->fill($filtered_data['client']);

        $client->save();

        $client->contact->fill($filtered_data['contact']);

        $client->contact->save();



        \Flash::success('The client has been edited');

        $next = \redirect('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $clients_data = $this->reject($data, ['created_at', 'updated_at']);

        $new = (new Client())->fill($clients_data);

        $old = $approval->client;

        $title = Title::find(isset($data['title_id']) ? $data['title_id'] : null);

        return [
            'old' => $old,
            'new' => $new,
            'title' => $title
        ];
    }

    /**
     * @param $data
     * @param Client $client
     * @return array
     */
    private function separateContactData($data, Client $client)
    {
        $contact_data_keys = $client->clientType->name == 'corporate'
            ? [
                'corporate_registered_name',
                'corporate_trade_name',
                'registered_address',
                'email',
                'registration_number',
                'title_id'
            ]
            : [
                'firstname',
                'lastname',
                'middlename',
                'email',
                'preferredname',
                'title_id'
            ];

        $contact_arr = [];

        unset($data['edited']);
        unset($data['file']);

        foreach ($contact_data_keys as $key) {
            $exist = array_key_exists($key, $data);

            if ($exist) {
                $contact_arr[$key] = $data[$key];
                unset($data[$key]);
            } else {
                unset($data[$key]);
            }
        }

        if (isset($data['contact_phone']) && isNotEmptyOrNull($data['contact_phone'])) {
            $contact_arr['phone'] = $data['contact_phone'];
        } else {
            $contact_arr['phone'] = $data['phone'];
        }

        unset($data['contact_phone']);
        
        return [
            'client' => $data,
            'contact' => $contact_arr
        ];
    }
}
