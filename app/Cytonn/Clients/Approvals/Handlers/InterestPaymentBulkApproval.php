<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\InterestPaymentSchedule;
use App\Cytonn\Models\User;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Excels\ExcelWork;
use Cytonn\Handlers\ExceptionHandler;
use Cytonn\Investment\Action\PayInterest;
use Cytonn\Investment\Action\Withdraw;
use Cytonn\Mailers\ExcelMailer;
use Cytonn\Mailers\Investments\InterestPaymentsMailer;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Reporting\InterestPaymentSchedulesUpForApprovalExcelGenerator;
use Illuminate\Support\Facades\DB;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class InterestPaymentBulkApproval implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, ExcelMailer;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        \Queue::push(static::class . '@pay', ['approval_id' => $approval->id]);

        \Flash::success('Interest payments have been queued for payment/reinvestment.');

        $next = \Redirect::to('/dashboard/investments/interest');
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function pay($job, $data)
    {
        $approval = ClientTransactionApproval::find($data['approval_id']);

        $scheduleArray = $this->separateSchedules($approval);

        $schedules = $scheduleArray['normal'];
        $combine_schedules = $scheduleArray['combine'];

        $total = $schedules->count() + $combine_schedules->count();

        if ($total != $this->getSchedules($approval)->count()) {
            throw new \InvalidArgumentException("The number of schedules selected for execution is incorrect");
        }

        DB::transaction(function () use ($schedules, $combine_schedules, $approval) {
            $this->singleInterestPayments($schedules, $approval);
            $this->combinedInterestPayments($combine_schedules, $approval);
        });

        if ($job) {
            $job->delete();
        }
    }

    public function separateSchedules($approval)
    {
        $schedules = $this->getSchedules($approval);

        $normalSchedules = array();
        $combineSchedules = array();

        foreach ($schedules as $schedule) {
            $investment = $schedule->investment;

            $client = $investment->client;

            if ($investment->interest_action_id == 2 && in_array($client->combine_interest_reinvestment, [1, 2])) {
                $combineSchedules[] = $schedule;
            } else {
                $normalSchedules[] = $schedule;
            }
        }

        return [
            'normal' => collect($normalSchedules),
            'combine' => collect($combineSchedules)
        ];
    }

    public function singleInterestPayments($schedules, $approval)
    {
        $date = $approval->payload['date'];

        try {
            $schedules->each(function (InterestPaymentSchedule $schedule) use ($approval, $date) {
                $investment = $schedule->investment;
                $interest = $schedule->calculatePaymentAmount($date);

                $with = new Withdraw();

                $action = $investment->interestAction ? $investment->interestAction->slug : null;

                $type = $action == 'reinvest' ? Withdraw::TYPE_REINVESTED : Withdraw::TYPE_SENT_TO_CLIENT;

                $with->all(
                    $investment,
                    Carbon::parse($date),
                    $approval,
                    $interest,
                    ['generate_instruction' => true, 'type' => $type]
                );
            });
        } catch (\Exception $e) {
            $this->notifyOnFailure($e, $approval);
            throw $e;
        }
    }

    public function combinedInterestPayments($schedules, $approval)
    {
        $schedulesArray = array();

        foreach ($schedules as $schedule) {
            $schedule->client_id = $schedule->investment->client_id;
            $schedule->product_id = $schedule->investment->product_id;

            $schedulesArray[$schedule->investment->client_id . '_' . $schedule->investment->product_id][] = $schedule;
        }

        $date = $approval->payload['date'];

        collect($schedulesArray)->each(
            function ($client_schedules) use ($approval, $date) {
                $client = Client::findOrFail($client_schedules[0]->client_id);

                $client_schedules = collect($client_schedules);

                if ($client->combine_interest_reinvestment == 1) {
                    $pastReinvestedInterestInvestments = ClientInvestment::active()
                        ->ofType('reinvested_interest')
                        ->where('client_id', $client->id)
                        ->where('product_id', $client_schedules[0]->product_id)
                        ->get();
                } else {
                    $pastReinvestedInterestInvestments = collect([]);
                }

                $reinvestment = (new Withdraw())->combineInterestReinvestment(
                    $client_schedules,
                    $pastReinvestedInterestInvestments,
                    $client,
                    $approval,
                    Carbon::parse($date)
                );

                $this->makeWithdrawals($client_schedules, $approval, $reinvestment);

                $this->makeExistingReinvestedInterestsWithdrawal(
                    $pastReinvestedInterestInvestments,
                    Carbon::parse($date),
                    $approval,
                    $reinvestment
                );


                //TODO validate amount withdrawn is same as amount reinvested
            }
        );
    }

    /*
     * Make withdrawals for the reinvested interests
     */
    public function makeExistingReinvestedInterestsWithdrawal(
        $pastReinvestedInterestInvestments,
        Carbon $date,
        ClientTransactionApproval $approval,
        ClientInvestment $reinvestment
    ) {
        $pastReinvestedInterestInvestments->each(function ($investment) use ($date, $approval, $reinvestment) {
            (new Withdraw())->withdrawReinvestedInterest($investment, $date, $approval, $reinvestment);
        });
    }

    public function makeWithdrawals($schedules, $approval, $reinvestment)
    {
        $date = $approval->payload['date'];

        try {
            $schedules
                ->each(function (InterestPaymentSchedule $schedule) use ($approval, $date, $reinvestment) {
                    $investment = $schedule->investment;

                    $interest = $schedule->calculatePaymentAmount($date);

                    $with = new Withdraw();

                    $type =  Withdraw::TYPE_REINVESTED;

                    $with->withdrawInterests(
                        $investment,
                        Carbon::parse($date),
                        $approval,
                        $interest,
                        $reinvestment,
                        ['generate_instruction' => true, 'type' => $type]
                    );
                });
        } catch (\Exception $e) {
            $this->notifyOnFailure($e, $approval);
            throw $e;
        }
    }

    private function getNormalSchedules($approval)
    {
        $date = $approval->payload['date'];

        return InterestPaymentSchedule::whereIn('id', $approval->payload['interest_payment_schedules'])
            ->whereNotIn('id', $approval->payload['removed'])
            ->whereHas(
                'investment',
                function ($investment) use ($date) {
                    $investment->active()
                        ->whereDoesntHave(
                            'schedule',
                            function ($schedule) use ($date) {
                                $schedule->whereNull('run_date')->where('action_date', '<=', Carbon::parse($date)
                                    ->endOfDay());
                            }
                        )
                        ->where(function ($q) {
                            $q->where('interest_action_id', '!=', 2)
                                ->orWhere(function ($q) {
                                    $q->where('interest_action_id', 2)->whereHas('client', function ($client) {
                                        $client->whereNull('combine_interest_reinvestment');
                                    });
                                });
                        });
                }
            )->get();
    }

    public function getSchedules($approval)
    {
        $date = $approval->payload['date'];

        return InterestPaymentSchedule::whereIn('id', $approval->payload['interest_payment_schedules'])
            ->whereNotIn('id', $approval->payload['removed'])
            ->whereHas(
                'investment',
                function ($investment) use ($date) {
                    $investment->active()
                        ->whereDoesntHave(
                            'schedule',
                            function ($schedule) use ($date) {
                                $schedule->whereNull('run_date')
                                    ->where('action_date', '<=', Carbon::parse($date)->endOfDay());
                            }
                        );
                }
            )->get();
    }

    private function schedulesToBeCombined($approval)
    {
        $date = $approval->payload['date'];

        return InterestPaymentSchedule::whereIn('id', $approval->payload['interest_payment_schedules'])
            ->whereNotIn('id', $approval->payload['removed'])
            ->whereHas(
                'investment',
                function ($investment) use ($date) {
                    $investment->active()
                        ->whereDoesntHave(
                            'schedule',
                            function ($schedule) use ($date) {
                                $schedule->whereNull('run_date')
                                    ->where('action_date', '<=', Carbon::parse($date)->endOfDay());
                            }
                        )
                        ->whereHas('client', function ($client) {
                            $client->whereNotNull('combine_interest_reinvestment');
                        })
                        ->whereHas('interestAction', function ($action) {
                            $action->where('slug', 'reinvest');
                        });
                }
            )->get();
    }

    /**
     * @param \Exception $exception
     * @param ClientTransactionApproval $approval
     */
    private function notifyOnFailure(\Exception $exception, ClientTransactionApproval $approval)
    {
        $mail = Mail::compose()
            ->to(systemEmailGroups(['super_admins']))
            ->subject('Interest payment bulk approval failed')
            ->text($exception->getMessage());

        if ($approval->approver) {
            $mail->to($approval->approver->email);
        }

        $mail->send();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedules = InterestPaymentSchedule::whereIn('id', [])->get();

        return ['schedules' => $schedules, 'date' => $data['date'], 'approval_id' => $approval->id];
    }

    /**
     * @param $approval
     * @param $action
     * @return mixed|void
     */
    public function action($approval, $action, $param)
    {
        switch ($action) {
            case 'notify':
                \Queue::push(
                    static::class . '@notify',
                    ['approval_id' => $approval->id, 'user_id' => \Auth::user()->id]
                );

                \Flash::message('The notifications are being sent out to the clients');

                return \Redirect::back();
                break;
            case 'export':
                return $this->export($approval);
                break;
            case 'instruction':
                return $this->instruction($approval);
                break;
            case 'remove':
                $payload = $approval->payload;
                $payload['removed'][] = $param;

                $approval->payload = $payload;
                $approval->save();

                \Flash::message('Successfully excluded');

                return \Redirect::back();

            default:
                throw new \InvalidArgumentException("Action $action not supported in " . static::class);
        }
    }

    /**
     * @param $approval
     */
    private function export($approval)
    {
        $email = \Auth::user()->email;

        sendToQueue(
            function () use ($approval, $email) {
                $schedules = $this->getSchedules($approval)
                    ->each(
                        function ($schedule) {
                            $schedule->interest_action_id = $schedule->investment->interest_action_id;
                        }
                    )
                    ->groupBy(
                        function ($item, $key) {
                            $key = $item->interest_action_id;
                            if (is_null($key)) {
                                $key = InterestAction::where('slug', 'send_to_bank')->first()->id;
                            }

                            return $key;
                        }
                    );

                $file = (new InterestPaymentSchedulesUpForApprovalExcelGenerator())
                    ->excel($schedules, (new Carbon($approval->payload['date'])), $approval->id);

                $file->store('xlsx');

                Mail::compose()->to($email)
                    ->bcc(config('system.administrators'))
                    ->excel(['Interest_Payment_Schedules'])
                    ->subject('Interest payments')
                    ->text('PFA')
                    ->send();
            }
        );

        \Flash::message('Report will be sent to your email');

        return \redirect()->back();
    }

    /**
     * @param $job
     * @param $data
     */
    public function notify($job, $data)
    {
        dd('disabled');

        $approval = ClientTransactionApproval::find($data['approval_id']);

        $date = $approval->payload['date'];
        $sender = User::findOrFail($data['user_id']);

        InterestPaymentSchedule::whereIn('id', $approval->payload['interest_payment_schedules'])
            ->get()
            ->each(
                function (InterestPaymentSchedule $schedule) use ($approval, $date) {
                    $schedule->client_id = $schedule->investment->client_id;
                    $schedule->amount_paid = InterestPayment::where('date_paid', $date)
                        ->where('investment_id', $schedule->investment_id)->sum('amount');
                    $schedule->bank = $schedule->investment->bankDetails()->bankName();
                    $schedule->account_number = $schedule->investment->bankDetails()->accountNumber();
                    $schedule->currency = $schedule->investment->product->currency->code;
                }
            )
            ->filter(
                function ($schedule) {
                    $action = $schedule->investment->interestAction;

                    if (is_null($action)) {
                        return true;
                    }

                    return $action->slug == 'send_to_bank';
                }
            )
            ->unique('investment_id')
            ->groupBy('client_id')
            ->each(
                function ($group) use ($date, $sender) {
                    try {
                        $client = Client::findOrFail($group[0]->client_id);

                        (new InterestPaymentsMailer())->notifyClient($client, $group, $date, $sender);
                    } catch (\Exception $e) {
                        app(ExceptionHandler::class)->reportException($e);
                    }
                }
            );

        $job->delete();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return mixed
     */
    private function instruction(ClientTransactionApproval $approval)
    {
        $email = \Auth::user()->email;

        sendToQueue(function () use ($approval, $email) {
            $data = BankInstruction::where('approval_id', $approval->id)
                ->has('custodialTransaction')
                ->get()
                ->map(
                    function (BankInstruction $instruction) {
                        $transaction = $instruction->custodialTransaction;
                        $client = $transaction->client;
                        $payment = $transaction->clientPayment->parent;
                        $withdrawal =
                            ClientInvestmentWithdrawal::where('payment_out_id', $payment->id)->first();
                        $investment = $withdrawal->investment;

                        $fund = $investment->product->fund;
                        $fund_name_uq = str_replace(" ", "", $fund ? $fund->name : 'Unknown');

                        $currency = $investment->product->currency;

                        $bank = $investment->bankDetails();

                        $out = new EmptyModel();

                        $unique = 'client_id_' . $client->id . '_bank_acc_id_' . $bank->id .
                            '_fund_' . $fund_name_uq . '_curr_' . $currency->id;

                        $out->fill([
                            'Code' => $client->client_code,
                            'Beneficiary Account Name' => $bank->accountName,
                            'Account Number' => $bank->accountNumber,
                            'Payment Details' => 'Interest Payment',
                            'Currency' => $currency->code,
                            'Payment Amount' => $instruction->amount,
                            'Swift Code' => $bank->swiftCode,
                            'Beneficiary E-Mail' => $client->contact->email,
                            'Payment Type' => $bank->bankName == 'Standard Chartered Bank' ? 'BT' : 'RTGS',
                            'unique' => $unique,
                            'Fund' => $fund ? $fund->short_name . ' - ' . $currency->code
                                : 'Not Specified - ' . $currency->code
                        ]);

                        return $out;
                    }
                )->sortBy('Code')
                ->groupBy('unique')
                ->map(
                    function ($group) {
                        $instr = $group->first();
                        $instr->{'Payment Amount'} = (float)$group->sum('Payment Amount');
                        unset($instr->unique);

                        return [$instr];
                    }
                )
                ->flatten(1)
                ->groupBy('Fund');

            ExcelWork::generateAndStoreMultiSheet($data, 'Interest_Payment_Instruction');
//                Excel::fromModel('Interest_Payment_Instruction', $data)->store('xlsx');

            Mail::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject('Interest Payment Instruction')
                ->excel(['Interest_Payment_Instruction'])
                ->text('PFA')
                ->send();
        });

        \Flash::message('Sending to email');

        return redirect()->back();
    }
}
