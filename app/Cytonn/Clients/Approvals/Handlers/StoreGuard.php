<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Guard;
use App\Cytonn\Models\GuardType;
use App\Cytonn\Models\PortfolioInvestor;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\System\Guards\GuardRepository;
use Illuminate\Support\Facades\Response;

class StoreGuard implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $guardRepository = new GuardRepository();

        $guardRepository->save($data, $data['id']);

        if ($data['id']) {
            \Flash::success('Client guard has been updated successfully');
        } else {
            \Flash::success('Client guard has been added successfully');
        }

        $next = \Redirect::to('/dashboard/clients');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $guardType = GuardType::findOrFail($data['guard_type_id']);

        $guard = ($data['id']) ? Guard::findOrFail($data['id']) : null;

        $portfolioInvestor = array_key_exists('portfolio_investor_id', $data)
            ? PortfolioInvestor::findOrFail($data['portfolio_investor_id']) : null;

        return [
            'guard' => $guard,
            'guardType' => $guardType,
            'portfolioInvestor' => $portfolioInvestor,
            'data' => $data
        ];
    }
}
