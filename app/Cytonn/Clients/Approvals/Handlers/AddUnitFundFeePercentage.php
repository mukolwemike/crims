<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundFee;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class AddUnitFundFeePercentage extends AbstractApproval
{

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fee = UnitFundFee::findOrFail($data['unit_fund_fee_id']);

        $fund = $fee->fund;

        $fee->percentages()->create($data);
        
        \Flash::success('The fund fee percentage is added successfully');

        $next = redirect()->to('/dashboard/unitization/unit-funds/'.$fund->id.'/unit-fund-fees/'.$fee->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {

        $data = $approval->payload;

        $fee = UnitFundFee::findOrFail($data['unit_fund_fee_id']);

        $fund = $fee->fund;

        return [
            'data' => $data,
            'fee' => $fee,
            'fund' => $fund
        ];
    }
}
