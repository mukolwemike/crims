<?php
/**
 * Date: 03/11/2017
 * Time: 12:14
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\SchedulableApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\FCM\FcmManager;
use Cytonn\Handlers\EventDispatchTrait;
use Illuminate\Support\Facades\Response;
use Laracasts\Commander\Events\EventGenerator;

class Withdrawal extends SchedulableApproval implements ApprovalHandlerInterface
{
    use EventGenerator, EventDispatchTrait;

    protected $formats = [
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'date' => 'date',
        'amount' => 'amount',
        'principal' => 'amount',
        'value' => 'amount',
        'penalty' => 'amount'
    ];

    /**
     * @param ClientTransactionApproval $approval
     * @return \Cytonn\Models\ClientInvestmentWithdrawal|Response|null|void
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->checkForGuards($approval);

        $instruction = $approval->withdrawInstruction;

        $investment = $instruction->investment;

        $action_date = $this->getActionDate($approval, $investment);

        $w = null;

        if ($action_date->isFuture()) {
            $this->schedule($approval, $action_date, ['investment_id' => $investment->id]);

            \Flash::success('The withdrawal has been scheduled');

            $next = redirect('/dashboard/investments');
        } else {
            if ($investment->isSeip()) {
                $investment->childInvestments->each(function (ClientInvestment $investment) use ($approval) {
                    $this->runTransaction($approval, false, $investment);
                });
            }

            $w = $this->runTransaction($approval, false);

            $fcmManager = new FcmManager();

            $fcmManager->sendFcmMessageToMultipleDevices(
                $investment->client,
                $investment->uuid,
                $investment->product->id,
                "Withdrawal approved",
                'Success, withdrawal request approved',
                'withdrawal'
            );

            \Flash::success('The investment withdrawal has been saved');

            $next = redirect('/dashboard/investments');
        }

        $this->raise(new ApprovalSuccessful($approval, $next));

        $this->dispatchEventsFor($this);

        return $w;
    }


    /**
     * @param $approval
     * @param bool $lock
     * @return mixed
     * @throws \Exception
     */
    public function runTransaction($approval, $lock = true, $otherInvestment = null)
    {
        $data = $approval->payload;

        $withdraw = new \Cytonn\Investment\Action\Withdraw();

        $investment = $otherInvestment ? $otherInvestment : $approval->investment;

//        if ($lock) {
//            $this->lockForUpdate($investment);
//        }

        $action_date = $this->getActionDate($approval, $otherInvestment);

        $amount = $this->calculateAmountWithdrawn($approval, $otherInvestment);

        return $this->executeWithinAtomicLock(function () use (
            $data,
            $withdraw,
            $action_date,
            $investment,
            $amount,
            $approval,
            $otherInvestment
) {
            if ($this->shouldHaveDeduction($data)) {
                $withdraw->deduction(
                    $investment,
                    $action_date,
                    $approval,
                    $amount,
                    ['description' => 'Penalty on Interest']
                );
            }

            $w_amt = $this->calculateAmountWithdrawn($approval, $otherInvestment);

            return $withdraw->all($investment, $action_date, $approval, $w_amt);
        }, $investment);
    }

    public function calculateAmountWithdrawn($approval, ClientInvestment $otherInvestment = null)
    {
        $data = $approval->payload;

        $withdraw = new \Cytonn\Investment\Action\Withdraw();

        if ($otherInvestment) {
            $investment = $otherInvestment;
            $amount = null;
        } else {
            $investment = isset($data['investment_id']) ? ClientInvestment::findOrFail($data['investment_id']) :
                $approval->investment;

            $amount = isset($data['amount']) ? $data['amount'] :
                $approval->withdrawInstruction->repo->amountWithdrawn();
        }

        $date = $this->getActionDate($approval, $investment);

        return $withdraw->calculateAmount($approval, $date, $amount, [], $investment)['amount'];
    }

    public function calculatePenalty($approval)
    {
        $data = $approval->payload;

        if (!$this->shouldHaveDeduction($data)) {
            return (object)[
                'amount' => 0,
                'rate' => 0
            ];
        }

        $withdraw = new \Cytonn\Investment\Action\Withdraw();

        $investment = $approval->investment;

        if ($instr = $approval->withdrawInstruction) {
            $amount = $instr->repo->amountWithdrawn();
        }

        if (isset($data['amount'])) {
            $amount = $data['amount'];
        }

        if (!$investment) {
            $investment = ClientInvestment::findOrFail($data['investment_id']);
        }

        $date = $this->getActionDate($approval, $investment);

        $date = Carbon::parse($date);

        return $withdraw->calculatePenalty($investment, $date, null, $amount);
    }

    public function getActionDate($approval, $investment)
    {
        if ($instr = $approval->withdrawInstruction) {
            return $instr->due_date;
        }

        $data = $approval->payload;

        if ($data['type'] == 'interest' || $data['type'] == 'deduction') {
            return Carbon::parse($data['end_date']);
        }

        if ($data['premature'] == 0) {
            return $investment->maturity_date;
        }

        return Carbon::parse($data['end_date']);
    }

    private function shouldHaveDeduction($data)
    {
        $type = $this->getType();

        if (in_array($type, ['deduction', 'interest'])) {
            return false;
        }

        return ($data['deduct_penalty'] == 1) && $this->premature();
    }

    private function premature()
    {
        $data = $this->approval->payload;

        $inv = $this->approval->investment;

        if (isset($data['investment_id'])) {
            $inv = ClientInvestment::findOrFail($data['investment_id']);
        }

        return $this->getActionDate($this->approval, $inv)->lt($inv->maturity_date);
    }

    public function prepareViewV1(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::find($approval->payload['investment_id']);

        if ($approval->investment_id == null) {
            $approval->investment_id = $investment->id;

            $approval->save();
        }

        $value_at_end = $investment->repo->getTotalValueOfInvestmentAtDate($data['end_date'], false);

        $action_date = Carbon::parse($this->getActionDate($approval, $investment));

        $scheduled = $action_date->isFuture();

        $withdraw = new \Cytonn\Investment\Action\Withdraw();

        $penaltyData = $withdraw->calculatePenalty(
            $investment,
            $action_date,
            null,
            $data['amount']
        );

        $data['penalty'] = $penaltyData->amount;

        $data['penalty_percentage'] = $penaltyData->rate;

        return [
            'investment' => $investment,
            'value_at_end' => $value_at_end,
            'type' => $data['type'],
            'scheduled' => $scheduled,
            'data' => $data,
            'action_date' => $action_date
        ];
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $this->approval = $approval;

        if (!$approval->withdrawInstruction) {
            return $this->prepareViewV1($approval, $vars);
        }

        $data = $approval->payload;

        $instruction = $approval->withdrawInstruction;

        $approvals = $instruction->clientApprovals->map(function (ClientInstructionApproval $appr) {

            $name = $appr->user ? $appr->user->firstname . ' ' . $appr->user->lastname : '';

            return [
                'id' => $appr->id,
                'user' => $name,
                'requested' => $appr->requestedByUser(),
                'status' => $appr->approvalStatus ? $appr->approvalStatus->name : 'pending approval',
                'approval_status' => $appr->status,
                'reason' => $appr->reason ? $appr->reason : 'N/A'
            ];
        });

        $approved = $instruction ? $instruction->repo->approvedByClient() : true;

        $investment = $approval->investment;

        $fa = $investment->commission->recipient;

        $action_date = Carbon::parse($this->getActionDate($approval, $investment));

        $value_at_end = $investment->repo->getTotalValueOfInvestmentAtDate($action_date, false);

        $scheduled = $action_date->isFuture();

        $data['premature'] = $this->premature();

        $data['partial_withdraw'] = false;

        $penaltyData = $this->calculatePenalty($approval);

        $penalty = $penaltyData->amount;

        $amount = $this->calculateAmountWithdrawn($approval);

        if (($amount + $penalty) > $value_at_end) {
            $amount = $value_at_end - $penalty;
        }

        $contributions = $contributionPenalty = 0;
        if ($investment->isSeip()) {
            $contributions = $investment->repo->getSeipChildrenInvestedValueAsAtDate($action_date, false);

            $contributionPenalty = $investment->childInvestments->sum(function (ClientInvestment $investment) use ($action_date) {
                $withdraw = new \Cytonn\Investment\Action\Withdraw();

                return $withdraw->calculateSeipPenalty($investment, $action_date)->amount;
            });

            $amount = $amount + $contributions - $contributionPenalty;
        }

        $data['penalty'] = $penalty;

        $data['penalty_percentage'] = $penaltyData->rate;

        $data['amount'] = $amount;

        return [
            'investment' => $investment,
            'value_at_end' => $value_at_end,
            'scheduled' => $scheduled,
            'data' => $data,
            'action_date' => $action_date,
            'type' => $this->getType($approval),
            'client' => $approval->client,
            'instruction' => $instruction,
            'fa' => $fa,
            'approvals' => $approvals,
            'approved' => $approved,
            'contributions' => $contributions,
            'contributionPenalty' => $contributionPenalty
        ];
    }

    public function getType()
    {
        if (isset($this->approval->payload['type'])) {
            return $this->approval->payload['type'];
        }

        $instr = $this->approval->withdrawInstruction;
        $type = $instr->withdraw_type;

        if ($type == 'deduction') {
            return $type;
        }
        if ($instr->amount_select == 'interest') {
            return 'interest';
        }

        if (!$type) {
            return 'withdrawal';
        }

        return $type;
    }

    /**
     * @param $approval
     * @return bool
     * @throws ClientInvestmentException
     */
    public function checkForGuards($approval)
    {
        if ($approval->exemption) {
            return true;
        }

        $client = $approval->client;

        if ($client->guards()->investmentsGuard()->count() == 0) {
            return true;
        }

        $investment = $approval->investment;

        $endDate = $this->getActionDate($approval, $investment);

        $value_at_end = $investment->repo->getTotalValueOfInvestmentAtDate($endDate) + $investment->withdraw_amount;

        $investedAmount = $client->repo->getTodayTotalInvestmentsValueForAllProducts();

        $client->repo->checkForInvestmentGuards($approval, $investedAmount - $value_at_end);
    }

    protected function pluck($name)
    {
        $approval = $this->approval;

        $data = $approval->payload;
        $investment = $approval->withdrawInstruction->investment;

        $date = $this->getActionDate($this->approval, $investment);


        $value = $approval->investment->calculate(Carbon::parse($date), false)->value();
        $penalty = $this->calculatePenalty($approval)->amount;
        $withdraw = $this->calculateAmountWithdrawn($approval);

        $amount = $withdraw - $penalty;
        if ($value >= ($penalty + $withdraw)) {
            $amount = $withdraw;
        }


        switch ($name) {
            case 'invested_date':
                return $investment->invested_date;
            case 'maturity_date':
                return $investment->maturity_date;
            case 'date':
                return $date;
            case 'principal':
                return $investment->amount;
            case 'penalty':
                return $penalty;
            case 'amount':
                return $amount;
            case 'value':
                return $investment->calculate(Carbon::parse($date), false)->value();
        }
    }

    protected function lockForUpdate($investment)
    {
        $i = ClientInvestment::where('id', $investment->id)->lockForUpdate()->get();

        $i->first() ? $i->first()->update(['updated_at' => Carbon::now()]) : null;
    }
}
