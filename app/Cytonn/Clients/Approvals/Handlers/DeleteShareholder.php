<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 16/10/2018
 * Time: 11:16
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;

class DeleteShareholder implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $shareHolder = ShareHolder::findOrFail($data['share_holder_id']);

//        $number_of_shares_owned = $shareHolder->currentShares();

        $cashBalance = $shareHolder->sharePaymentsBalance();

        if ($shareHolder->repo->hasShareProducts() || $cashBalance > 0) {
            throw new ClientInvestmentException(
                'The share holder has active share holdings or cash balance and thus cant be deleted'
            );
        }

        $shareHolder->delete();



        \Flash::success('Share Holder has been deleted succesfully');

        $next = \redirect('/dashboard/shares/shareholders');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $shareHolder = ShareHolder::findOrFail($data['share_holder_id']);

        return [
            'data' => $data,
            'shareHolder' => $shareHolder
        ];
    }
}
