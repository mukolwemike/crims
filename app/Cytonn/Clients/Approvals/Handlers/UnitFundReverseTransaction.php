<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 21/05/2018
 * Time: 09:20
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use App\Events\Unitization\UnitFundSaleHasBeenRemoved;
use App\Jobs\Unitization\UnitFundReverseTransfer;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Unitization\Trading\Reverse;

class UnitFundReverseTransaction extends AbstractApproval
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fund = UnitFund::findorFail($data['fundId']);

        $reverse = new Reverse();

        if ($data['type'] == 'unit_fund_purchase') {
            $purchase = UnitFundPurchase::findOrFail($data['purchase_id']);

            $reverse->purchase($purchase);
        } elseif ($data['type'] == 'unit_fund_sale') {
            $sale = UnitFundSale::findOrFail($data['sale_id']);

            $reverse->sale($sale);

            event(new UnitFundSaleHasBeenRemoved($sale));
        } elseif ($data['type'] == 'unit_fund_transfer') {
            $transfer = UnitFundTransfer::findOrFail($data['transfer_id']);

            $reverse->transfer($transfer);
        } else {
            \Flash::warning('Unit fund transaction not supported');
            return \Redirect::back();
        }


        $next =
            \Redirect::to('dashboard/unitization/unit-funds/' . $fund->id . '/unit-fund-clients/' . $data['clientId']);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fundId']);

        $client = Client::findOrFail($data['clientId']);

        switch ($data['type']) {
            case 'unit_fund_purchase':
                $purchase = UnitFundPurchase::findOrFail($data['purchase_id']);
                $purchase->fee = ($purchase->fees) ? $purchase->fees()->sum('amount') : 0;

                return [
                    'client' => $client,
                    'fund' => $fund,
                    'purchase' => $purchase
                ];

                break;
            case 'unit_fund_sale':
                $sale = UnitFundSale::findOrFail($data['sale_id']);
                $sale->fee = ($sale->fees) ? $sale->fees->sum('amount') : 0;

                return [
                    'client' => $client,
                    'fund' => $fund,
                    'sale' => $sale
                ];
            case 'unit_fund_transfer':
                $transfer = UnitFundTransfer::findOrFail($data['transfer_id']);

                return [
                    'client' => $client,
                    'fund' => $fund,
                    'transfer' => $transfer
                ];

            default:
                \Flash::message('Transaction type not supported');
        }
    }
}
