<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Action\Reverse;

class RealEstateReverseForfeiture implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        //Check for real estate guards
        $holding->client->repo->checkReGaurds($approval);

        $date = $holding->forfeit_date;

        $holding->reverseForfeiture();

        $refund = $holding->refunds()->where('date', $date)->latest()->first();

        if ($refund) {
            $payment = $refund->clientPayment;

            $rev = new Reverse();
            $rev->payment($payment);

            $refund->delete();
        }

        \Flash::success('The forfeiture reversal has been approved');

        $next = \redirect('/dashboard/realestate/projects');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $holding = UnitHolding::findOrFail($approval->payload['holding_id']);
        $date = $holding->forfeit_date;
        $refund = $holding->refunds()->where('date', $date)->latest()->first();

        return ['project' => $holding->project, 'holding' => $holding, 'refund' => $refund];
    }
}
