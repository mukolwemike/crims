<?php
/**
 * Date: 20/01/2017
 * Time: 08:51
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class UpdateRealestateCommissionPaymentSchedule implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $schedule = RealEstateCommissionPaymentSchedule::findOrFail($data['schedule_id']);

        $this->recordHistory($approval, $schedule);

        $history = $schedule->history;

        if (!is_array($history)) {
            $history = [];
        }

        $new = (object)[
          'reason'=>$data['reason'],
          'to'=>['date'=>$data['date']],
          'from'=>['date'=>$schedule->date]
        ];

        array_push($history, $new);

        $schedule->update(['date'=>$data['date'], 'history'=>$history]);

        \Flash::message('Schedule updated');

        $next = \Redirect::back();

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedule = RealEstateCommissionPaymentSchedule::findOrFail($data['schedule_id']);

        $this->recordHistory($approval, $schedule);

        $holding = $schedule->commission->holding;

        $commission = $schedule->commission;

        return ['schedule'=>$schedule, 'holding'=>$holding, 'unit'=>$holding->unit, 'commission'=>$commission];
    }

    protected function recordHistory($approval, $schedule)
    {
        $data = $approval->payload;

        if (!$approval->approved) {
            $data['before'] = $schedule->toArray();
            $approval->payload = $data;
            $approval->save();
        }
    }
}
