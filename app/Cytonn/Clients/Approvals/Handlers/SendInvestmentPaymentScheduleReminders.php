<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\InvestmentPaymentSchedules\InvestmentPaymentScheduleReminderMailer;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Illuminate\Support\Facades\Response;

class SendInvestmentPaymentScheduleReminders extends AbstractApproval
{
    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        \Queue::push(static::class . '@processReminders', ['approval_id' => $approval->id]);

        \Flash::success('The investment payment schedules reminders have been queued for dispatch.');

        $next = \Redirect::to('/dashboard/investments/clientinvestments');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @throws ClientInvestmentException
     */
    public function preApproval()
    {
        $approval = $this->approval;

        $data = $approval->payload;

        $start = Carbon::parse($data['start']);

        $end = Carbon::parse($data['end']);

        $count = count($data['investment_schedules']) - count($data['removed']);

        $schedulesCount = $this->getSchedulesQuery($start, $end, $data['investment_schedules'])
            ->whereNotIn('id', $data['removed'])->count();

        if ($schedulesCount != $count) {
            throw new ClientInvestmentException('Some of the investment payment schedules are ineligible for 
            sending of reminders.Please remove them from the transaction sending list');
        }
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param $scheduleIds
     * @return mixed
     */
    private function getSchedulesQuery(Carbon $start, Carbon $end, $scheduleIds)
    {
        return InvestmentPaymentSchedule::whereIn('id', $scheduleIds)
            ->between($start, $end)
            ->paid(false)
            ->whereHas('parentInvestment', function ($investment) {
                $investment->active();
            });
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedules = InvestmentPaymentSchedule::whereIn('id', $data['investment_schedules'])
            ->orderBy('date')->get();

        $start = Carbon::parse($data['start']);

        $end = Carbon::parse($data['end']);

        $removedSchedules = $data['removed'];

        $checkEligible = function ($id) use ($start, $end) {
            return $this->getSchedulesQuery($start, $end, [$id]);
        };

        return [
            'schedules' => $schedules,
            'removedSchedules' => $removedSchedules,
            'checkEligible' => $checkEligible
        ];
    }

    /**
     * @param $approval
     * @param $action
     * @param $param
     * @return \Illuminate\Http\RedirectResponse
     */
    public function action($approval, $action, $param)
    {
        switch ($action) {
            case 'add_schedule':
                return $this->addScheduleToList($approval, $param);
                break;
            case 'remove_schedule':
                return $this->removeScheduleFromList($approval, $param);
                break;
            default:
                throw new \InvalidArgumentException("Action $action not supported in " . static::class);
        }
    }

    /**
     * @param $approval
     * @param $scheduleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addScheduleToList($approval, $scheduleId)
    {
        $payload = $approval->payload;

        $removed = $payload['removed'];

        $removed = array_diff($removed, [$scheduleId]);

        $payload['removed'] = $removed;

        $approval->payload = $payload;

        $approval->save();

        \Flash::message('The investment schedule has been successfully added to the list');

        return back();
    }

    /**
     * @param $approval
     * @param $scheduleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeScheduleFromList($approval, $scheduleId)
    {
        $payload = $approval->payload;

        $removed = $payload['removed'];

        $removed[] = $scheduleId;

        $payload['removed'] = $removed;

        $approval->payload = $payload;

        $approval->save();

        \Flash::message('The investment schedule has been successfully excluded from the list');

        return back();
    }

    /**
     * @param $job
     * @param $data
     */
    public function processReminders($job, $data)
    {
        $approval = ClientTransactionApproval::findOrFail($data['approval_id']);

        $data = $approval->payload;

        $start = Carbon::parse($data['start']);

        $end = Carbon::parse($data['end']);

        $scheduleArray = $this->getSchedulesGroupedByClient($start, $end, $data);

        foreach ($scheduleArray as $key => $scheduleGroup) {
            $client = Client::findOrFail($key);

            (new InvestmentPaymentScheduleReminderMailer())->sendEmail($client, $scheduleGroup);
        }

        $job->delete();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param $data
     * @return array
     */
    private function getSchedulesGroupedByClient(Carbon $start, Carbon $end, $data)
    {
        $schedules = $this->getSchedulesQuery($start, $end, $data['investment_schedules'])
            ->whereNotIn('id', $data['removed'])
            ->get();

        $scheduleArray = array();

        foreach ($schedules as $schedule) {
            $scheduleArray[$schedule->parentInvestment->client_id][] = $schedule;
        }

        return $scheduleArray;
    }
}
