<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 26/10/2018
 * Time: 10:10
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharesPurchaseOrder;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Redirect;

class EditCommissionDetailsPurchaseOrder implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $order = SharesPurchaseOrder::findOrFail($data['order_id']);

        $order->commission_recipient_id = $data['commission_recepient'];
        $order->commission_rate = $data['commission_rate'];
        $order->commission_start_date = Carbon::parse($data['commission_start_date']);

        $order->save();

        \Flash::success('Commission Edit for shares purchase order has been successfully approval');

        $next = Redirect::to('/dashboard/shares/purchases/show/' . $data['order_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $order = SharesPurchaseOrder::findOrFail($data['order_id']);

        return [
            'order' => $order
        ];
    }
}
