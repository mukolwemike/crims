<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\ProductPlan;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Coop\Investments\Invest;
use Cytonn\Coop\Payments\CoopPaymentRepository;
use Cytonn\Core\DataStructures\Collection;

/**
 * Class MakeCoopInvestmentTopup
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class MakeCoopInvestmentTopup implements ApprovalHandlerInterface
{
    protected $coopPaymentRepository;

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $productPlan = ProductPlan::findOrFail($data['product_plan_id']);


        $investing = new Invest();
        $investing->invest($productPlan, $approval);



        \Flash::success('Coop investment topup was successfully made!');
        $next = \redirect('/dashboard/clients/details/'.$data['client_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $client = Client::findOrFail($data['client_id']);
        $productplan = ProductPlan::findOrFail($data['product_plan_id']);
        $commissionrecepient = CommissionRecepient::findOrFail($data['commission_recipient']);

        return [
            'data'=>$data,
            'client'=>$client,
            'productplan'=>$productplan,
            'commissionrecepient'=>$commissionrecepient];
    }
}
