<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class SendInvestmentContractTermination implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        \Flash::success('The investment contract termination notice has been dispatched successfully.');

        $next = \Redirect::to('/dashboard/investments/investment_payment_schedules/' . $investment->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        return [
            'data' => $data,
            'investment' => $investment
        ];
    }
}
