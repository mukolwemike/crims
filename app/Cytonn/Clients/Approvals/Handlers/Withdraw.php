<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/15/16
 * Time: 1:26 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Approvals\Events\Actions\InvestmentActionApproved;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Withdraw extends Withdrawal implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $this->checkForGuards($approval);

        $data = $approval->payload;

        if (isset($data['deduct_penalty'])) {
            if ($data['premature'] == 'true' and $data['deduct_penalty'] == 'true') {
                ClientInvestment::findOrFail($data['investment_id'])
                    ->repo->makeDeduction($approval);
            }
        }

        $this->raise(new InvestmentActionApproved($approval));
        $this->dispatchEventsFor($this);

        if ($approval->scheduled) {
            \Flash::success('The withdrawal has been scheduled');

            $next = \Redirect::to('/dashboard/investments');
        } else {
            \Flash::success('The investment has been withdrawn');

            $next = \Redirect::to('/dashboard/investments');
            //            return \View::make('investment.clientwithdrawsuccess', ['title' => 'Investment Withdrawn',
            // 'investment' => $investment, 'withdrawal' => $withdrawal]);
        }

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $p = $data['premature'];

        if ($p == 'true') {
            $data['premature'] = true;
        } elseif ($p == 'false') {
            $data['premature'] = false;
        }

        $partial = $data['partial_withdraw'];
        if ($partial == 'true') {
            $data['partial_withdraw'] = true;
        } elseif ($partial == 'false') {
            $data['partial_withdraw'] = false;
        }

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        $value_at_end = $investment->repo->getTotalValueOfInvestmentAtDate($data['end_date']) +
            $investment->withdraw_amount;

        $new_maturity_date = $investment->maturity_date;

        if (isset($data['new_maturity_date'])) {
            $new_maturity_date = $data['new_maturity_date'];
        }

        $findInvestment = function ($id) {
            return ClientInvestment::find($id);
        };

        return ['p' => $p, 'partial' => $partial, 'investment' => $investment, 'value_at_end' => $value_at_end,
            'new_maturity_date' => $new_maturity_date, 'data' => $data, 'findInvestment' => $findInvestment
        ];
    }
}
