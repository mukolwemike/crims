<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Models\RealEstateUnitGroup;
use Illuminate\Support\Facades\Response;

class SetupProjectUnitGroup implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        if ($data['id']) {
            $unitGroup = RealEstateUnitGroup::findOrFail($data['id']);

            $unitGroup->update($data);
        } else {
            RealEstateUnitGroup::create($data);
        }

        $next = \Redirect::to('/dashboard/realestate/projects');

        \Flash::success('The project unit groups have been set up');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $project = Project::findOrFail($data['project_id']);

        $unitGroup = $data['id'] ? RealEstateUnitGroup::findOrFail($data['id']) : null;

        return [
            'data' => $data,
            'project' => $project,
            'unitGroup' => $unitGroup
        ];
    }
}
