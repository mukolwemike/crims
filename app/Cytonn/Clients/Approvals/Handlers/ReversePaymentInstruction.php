<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\BankInstruction;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\BankDetails;

class ReversePaymentInstruction implements ApprovalHandlerInterface
{

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $instruction = BankInstruction::findOrFail($data['instruction_id']);
        $transaction = $instruction->custodialTransaction;
        $payment = $transaction->clientPayment;

        $slug = $payment->type->slug;

        if ($slug != 'TO') {
            $payment->delete();
        }
        $instruction->delete();
        if ($slug != 'TO') {
            $transaction->delete();
        }



        \Flash::success('Payment instruction has been successfully reversed.');

        $next = \redirect('/dashboard/investments/payment-instructions');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $instruction = BankInstruction::findOrFail($data['instruction_id']);
        $transaction = $instruction->custodialTransaction;
        $payment = $transaction->clientPayment;
        $client = $payment->client;
        $account = new BankDetails(null, $client, $instruction->clientBankAccount);

        return ['client'=>$client, 'data'=>$data, 'transaction'=>$transaction,
            'instruction'=>$instruction, 'payment'=>$payment, 'account' => $account];
    }
}
