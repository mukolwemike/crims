<?php
/**
 * Date: 09/06/2016
 * Time: 7:36 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class RemoveRealEstatePaymentSchedule implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;


    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $paymentSchedule = RealEstatePaymentSchedule::findOrFail($data['id']);

        $holding = UnitHolding::findOrFail($data['holding_id']);

        //Check for real estate guards
        $holding->client->repo->checkReGaurds($approval);

        if ($paymentSchedule->payments()->count() > 0) {
            throw new ClientInvestmentException(
                'There is an existing payment for this schedule, you cannot delete it'
            );
        }

        $paymentSchedule->update(['last_schedule' => 0]);
        $paymentSchedule->delete();

        $holding->fresh()->repo->setLastSchedule();

        \Flash::success('The payment schedule has been deleted');

        $next = \Redirect::to('/dashboard/realestate/projects/show/' . $holding->project->id);

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $paymentSchedule = RealEstatePaymentSchedule::findOrFail($data['id']);
        $holding = $paymentSchedule->holding;
        $schedules = RealEstatePaymentSchedule::where('unit_holding_id', $holding['id'])->get();
        $total = $schedules->sum('amount');
        $paymenttype = RealEstatePaymentType::find($paymentSchedule->payment_type_id);

        return [
            'holding' => $holding,
            'schedules' => $schedules,
            'total' => $total,
            'paymentSchedule' => $paymentSchedule,
            'paymenttype' => $paymenttype
        ];
    }
}
