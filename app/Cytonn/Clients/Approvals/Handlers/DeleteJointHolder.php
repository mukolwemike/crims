<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 12/10/2018
 * Time: 12:42
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class DeleteJointHolder implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $jointHolder = ClientJointDetail::findOrFail($data['id']);

        $holder = $client->jointHolders()->where('joint_detail_id', $jointHolder->id);

        $jointHolder->reason = $data['reason'];

        $jointHolder->save();

        $holder->delete();

        if ($client->jointHolders()->count() == 0) {
            $client->joint = 0;

            $client->save();
        }

        \Flash::success('The Joint Client account has been successfully deleted');

        $next = \redirect('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $jointHolder = ClientJointDetail::findOrFail($data['id']);

        return [
            'client' => $client,
            'jointHolder' => $jointHolder
        ];
    }
}
