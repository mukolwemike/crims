<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentType;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;

class BulkRemoveRealEstatePaymentSchedules implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;


    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        //Check for real estate guards
        $holding->client->repo->checkReGaurds($approval);

        foreach ($data['items'] as $item) {
            $paymentSchedule = RealEstatePaymentSchedule::find($item);

            if ($paymentSchedule && $paymentSchedule->payments()->count() > 0) {
                throw new ClientInvestmentException(
                    'There is an existing payment for one of the schedules, you cannot bulk delete'
                );
            }
        }
        foreach ($data['items'] as $item) {
            $paymentSchedule = RealEstatePaymentSchedule::find($item);

            if ($paymentSchedule) {
                $paymentSchedule->delete();
            }
        }

        \Flash::success('The payment schedules have been deleted');

        $next = \Redirect::to('/dashboard/realestate/projects/show/'.$holding->project->id);

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $items = $data['items'];
        $holding = UnitHolding::findOrFail($data['holding_id']);
        $schedules = RealEstatePaymentSchedule::where('unit_holding_id', $holding->id)->get();
        $total = $schedules->sum('amount');
        $paymentType = function ($id) {
            RealEstatePaymentType::find($id);
        };
        
        return [
            'holding'=>$holding, 'schedules'=>$schedules, 'total'=>$total, 'items'=>$items, 'paymentType'=>$paymentType
        ];
    }
}
