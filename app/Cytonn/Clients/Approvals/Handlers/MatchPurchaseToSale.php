<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Client;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Shares\ShareTradingRepository;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;

/**
 * Class MatchPurchaseToSale
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class MatchPurchaseToSale implements ApprovalHandlerInterface
{
    protected $repo;

    /**
     * MakeSharesPurchaseOrder constructor.
     */
    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        dd('Disabled');

        $data = $approval->payload;

        $buyer = Client::findOrFail($data['buyer_id']);

        $purchaseOrder = SharesPurchaseOrder::findOrFail($data['purchase_order_id']);

        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);

        if ((double) $purchaseOrder->number * $purchaseOrder->price > $buyer->sharePaymentsBalance()) {
            \Flash::error('The buyer does not have enough funds to make a shares purchase order.');

            return \Redirect::back();
        }

        $this->repo->matchPurchaseOrderToSalesOrder($purchaseOrder, $salesOrder, $data['sold_date'], $approval->id);



        \Flash::success('Shares purchase order was successfully matched to a shares sales order!');
        $next = \redirect('/dashboard/shares/purchases/shareholders/'.$buyer->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $buyer = Client::findOrFail($data['buyer_id']);
        $purchaseOrder = SharesPurchaseOrder::findOrFail($data['purchase_order_id']);
        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);
        $seller = $salesOrder->seller;

        return ['data'=>$data, 'buyer'=>$buyer, 'seller'=>$seller,
            'purchaseOrder'=>$purchaseOrder, 'salesOrder'=>$salesOrder
        ];
    }
}
