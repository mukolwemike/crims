<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientContactPerson;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class DeleteContactPerson implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return void
     * @throws \Exception
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $contactPerson = ClientContactPerson::find($data['contact_id']);

        $contactPerson->delete();

        \Flash::success("Contact Person has been deleted");

        $next = \Redirect::to('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $contactPerson = ClientContactPerson::withTrashed()->find($data['contact_id']);

        return ['data' => $data, 'client' => $client, 'contactPerson' => $contactPerson];
    }
}
