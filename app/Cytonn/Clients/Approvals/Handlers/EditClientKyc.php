<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUploadedKyc;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Investment\ClientInstruction\ClientUploadedKycRepository;
use Illuminate\Support\Arr;

class EditClientKyc implements ApprovalHandlerInterface
{
    use ValidatorTrait;

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $client = $approval->client;

        $data = $approval->payload;

        if (count($data['jointDetails'])) {
            foreach ($data['jointDetails'] as $input) {
                $jointDetail = ClientJointDetail::find($input['id']);

                $jointDetail->update(Arr::only($input, ['id_or_passport', 'pin_no']));
            }
        }

        if (count($data['docsUploaded'])) {
            foreach ($data['docsUploaded'] as $input) {
                if (!isset($input['slug'])) {
                    continue;
                }
                $kyc = ClientComplianceChecklist::where('slug', $input['slug'])->first();

                $jointDetail = isset($input['jointId']) ? ClientJointDetail::find((int)$input['jointId']) : null;

                $uploadedClientKyc = ClientUploadedKyc::where('client_id', $client->id)
                    ->where('kyc_id', $kyc->id)->first();

                $uploadedJointKyc = isset($input['jointId'])
                    ? ClientUploadedKyc::where('client_joint_id', $jointDetail->id)
                    ->where('kyc_id', $kyc->id)->first() : null;

                if ($uploadedClientKyc || $uploadedJointKyc) {
                    (new ClientUploadedKycRepository())
                        ->updateKyc($input, $uploadedClientKyc, $client, $jointDetail);
                }

                if (!$uploadedJointKyc || !$uploadedClientKyc) {
                    (new ClientUploadedKycRepository())->saveKyc($input, $kyc, $client, $jointDetail);
                }
            }
        }


        $client->id_or_passport = isset($data['id_or_passport']) ? $data['id_or_passport'] : $client->id_or_passport;
        $client->pin_no = isset($data['pin_no']) ? $data['pin_no'] : $client->pin_no;
        $client->save();

        \Flash::success('The client kyc details have been edited');

        $next = \redirect('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFailByUuid($data['client_uuid']);

        $kycClient = $client->uploadedKyc;

        return [
            'client' => $client,
            'data' => $data,
            'kycClient' => $kycClient
        ];
    }
}
