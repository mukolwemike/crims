<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Clients\Approvals\Traits\EditInvestmentTrait;
use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\InterestAction;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Base;
use Cytonn\Investment\Events\InvestmentHasBeenEdited;
use Illuminate\Support\Facades\Response;

class EditInvestmentPaymentDetails extends Base implements ApprovalHandlerInterface
{
    use LocksTransactions, EditInvestmentTrait;

    /**
     * @param ClientTransactionApproval $approval
     * @return Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->checkForExemption($approval);

        $data = $approval->payload;

        //new payload format
        if (array_key_exists('new', $data)) {
            $data = $data['new'];
        }

        $investment = ClientInvestment::findOrFail($approval->payload['investment']);

        $this->lockModelForUpdate($investment);

        if ($investment->withdrawn) {
            throw new ClientInvestmentException("You cannot edit a withdrawn investment");
        }

        $oldInvestment = ClientInvestment::findOrFail($approval->payload['investment']);

        if (array_key_exists('interest_payment_interval', $data)) {
            $investment->interest_payment_interval = $data['interest_payment_interval'];
        }

        if (array_key_exists('interest_payment_date', $data)) {
            $investment->interest_payment_date = $data['interest_payment_date'];
        }

        if (array_key_exists('interest_action_id', $data)) {
            $investment->interest_action_id = $data['interest_action_id'];
        }

        if (array_key_exists('interest_payment_start_date', $data)) {
            $investment->interest_payment_start_date = $data['interest_payment_start_date'];
        }

        if (array_key_exists('interest_reinvest_tenor', $data)) {
            $investment->interest_reinvest_tenor = $data['interest_reinvest_tenor'];
        }

        $investment->save();

        $approval->raise(new InvestmentHasBeenEdited($investment, $oldInvestment, $approval, $data));

        $approval->dispatchEventsFor($investment);



        \Flash::success('The Investment Payment Details have been changed');

        $next = \redirect('/dashboard/investments/clientinvestments/' . $investment->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }


    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $payload = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment']);

        //new payload format
        if (array_key_exists('new', $data)) {
            $data = $data['new'];
        }

        $interestPayment = [null => 'On Maturity', 0 => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months', 5 => 'Every 5 months',
            6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months', 8 => 'Every 8 months',
            9 => 'Every 9 months', 10 => 'Every 10 months', 11 => 'Every 11 months',
            12 => 'Every 12 months (Annually)'];

        if ($approval->approved) {
            if (isset($payload['edited']) && isset($payload['edited_commission'])) {
                $edited = (new ClientInvestment())->fill($payload['edited']);

                $edited_commission = $payload['edited_commission'];
            } else {
                $edited = ClientInvestment::findOrFail($approval->payload['investment']);

                $edited_commission = $edited->commission->toArray();
            }
        } else {
            $edited = ClientInvestment::findOrFail($approval->payload['investment']);

            $edited_commission = $edited->commission->toArray();

            $payload = $approval->payload;

            $payload['edited'] = $edited->toArray();

            $payload['edited_commission'] = $edited->commission->toArray();

            $approval->payload = $payload;

            $approval->save();
        }

        $recipient = function ($id) {
            return CommissionRecepient::findOrFail($id);
        };

        $interestAction = function ($id) {
            return InterestAction::find($id);
        };

        return [
            'interestPayment' => $interestPayment,
            'edited_commission' => $edited_commission,
            'edited' => $edited,
            'data' => $data,
            'investment' => $investment,
            'recipient' => $recipient,
            'interest_action' => $interestAction,
        ];
    }
}
