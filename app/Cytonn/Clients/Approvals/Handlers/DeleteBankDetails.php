<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Clients\ClientBankDetailsAddCommand;

class DeleteBankDetails implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * AddBankDetails constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $account = ClientBankAccount::findOrFail($data['account_id']);
        $account->delete();

        $client = $approval->client;


        $this->execute(ClientBankDetailsAddCommand::class, ['data'=>$approval]);



        \Flash::success('Client bank account details has been deleted');

        $next = Redirect::to("/dashboard/clients/details/{$client->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $client = $approval->client;
        $data = $approval->payload;

        $account = ClientBankAccount::findOrFail($data['account_id']);

        $branch = ClientBankBranch::find($account->branch_id);

        $bank = is_null($branch) ? null : $branch->bank;

        return ['client'=>$client, 'data'=>$data, 'account'=>$account, 'bank'=>$bank, 'branch'=>$branch];
    }
}
