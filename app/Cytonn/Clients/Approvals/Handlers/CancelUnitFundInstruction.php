<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CancelUnitFundInstruction implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $instruction = UnitFundInvestmentInstruction::find($data['instruction_id']);

        $instruction->cancelled = 1;

        $instruction->approval_id = $approval->id;

        $instruction->save();

        \Flash::success('Unit Fund Instruction have successfully been cancelled');

        $next = \Redirect::to('/dashboard/investments/client-instructions');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $reason = $data['reason'];

        $instruction = UnitFundInvestmentInstruction::find($data['instruction_id']);

        $client = $instruction->client;

        $data = json_decode($instruction->payload, true);

        $fund = $instruction->unitFund;

        return [
            'data' => $data,
            'fund' => $fund,
            'reason' => $reason,
            'client' => $client,
            'instruction' => $instruction
        ];
    }
}
