<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Commission\Payment;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Reporting\CombinedCommissionsExcelGenerator;
use Cytonn\Support\Mails\Mailer;
use function Cytonn\Unitization\make;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class BulkCombinedCommissionPayments implements ApprovalHandlerInterface
{
    protected $currencies;

    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        \Queue::push(static::class . '@makePayments', ['approval_id' => $approval->id]);

        $next = \Redirect::back();

        \Flash::message('Combined commissions have been queued for approval');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        return [
            'bulk' => $bulk
        ];
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param $actionName
     * @param $params
     * @return \Illuminate\Http\RedirectResponse|void
     * @throws \Throwable
     */
    public function action(ClientTransactionApproval $approval, $actionName, $params)
    {
        switch ($actionName) {
            case 'excel':
                \Queue::push(
                    static::class . '@exportJob',
                    ['approval_id' => $approval->id, 'user_id' => \Auth::user()->id]
                );

                \Flash::message('The excel report is being processed, it will be sent to your email when complete');

                return \Redirect::back();
                break;
            default:
                return \App::abort(404);
        }
    }

    /**
     * @param $job
     * @param $data
     */
    public function exportJob($job, $data)
    {
        ini_set('memory_limit', '1024M');

        ini_set('max_execution_time', 600);

        $start = Carbon::now();

        $approval = ClientTransactionApproval::findOrFail($data['approval_id']);

        $payload = $approval->payload;

        $bcp = BulkCommissionPayment::findOrFail($payload['bcp_id']);

        (new CombinedCommissionsExcelGenerator())->excel($bcp)->store('xlsx');

        $user = User::findOrFail($data['user_id']);

        $fileName = 'Combined_commissions';

        Mailer::compose()
            ->to($user->email)
            ->bcc(config('system.administrators'))
            ->subject('Combined Commission Report - ' . DatePresenter::formatDate($bcp->end))
            ->text("See attached report for {$bcp->start->toFormattedDateString()} 
                    to {$bcp->end->toFormattedDateString()} 
                    (Time taken: " . Carbon::now()->diffInMinutes($start->copy()) . ' minutes)')
            ->excel([$fileName])
            ->send();

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        \File::delete($path);

        if ($job) {
            $job->delete();
        }
    }

    /**
     * @param $job
     * @param $data
     * @throws \Throwable
     */
    public function makePayments($job, $data)
    {
        $approval = ClientTransactionApproval::findOrFail($data['approval_id']);

        try {
            DB::transaction(function () use ($data, $approval) {
                $recipients = CommissionRecepient::active()->get();

                $bcp = BulkCommissionPayment::findOrFail($approval->payload['bcp_id']);

                $overallCommission = 0;

                foreach ($recipients as $recipient) {
                    $overallCommission += $this->payInvestmentCommissions($approval, $bcp, $recipient);

                    $overallCommission += $this->payOtherCommissions($approval, $bcp, $recipient);

                    $this->payAdditionalCommission($approval, $bcp, $recipient, $overallCommission);
                }
            });

            $this->notify($approval);
        } catch (\Exception $exception) {
            $this->notify($approval, $exception);
        }

        if ($job) {
            $job->delete();
        }
    }

    /**
     * @param \Exception $exception
     * @param ClientTransactionApproval $approval
     */
    private function notify(ClientTransactionApproval $approval, \Exception $exception = null)
    {
        if ($exception) {
            $subject = 'Bulk combined commission payment failed';
            $text = $exception->getMessage();
        } else {
            $subject = 'Bulk combined commission payment completed';
            $text = "Bulk combined commission payment completed successfully";
        }

        $mail = Mailer::compose()
            ->bcc(config('system.administrators'))
            ->subject($subject)
            ->text($text);

        if ($approval->approver) {
            $mail->to($approval->approver->email);
        }

        $mail->send();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param BulkCommissionPayment $bcp
     * @param CommissionRecepient $recipient
     * @return float|int
     */
    private function payInvestmentCommissions(
        ClientTransactionApproval $approval,
        BulkCommissionPayment $bcp,
        CommissionRecepient $recipient
    ) {
        $totalCommission = 0;

        foreach ($this->currencies() as $currency) {
            $payment = (new Payment())->make($recipient, $currency, $approval, $bcp);

            if ($payment) {
                $totalCommission += convert($payment->amount, $currency, $bcp->end);
            }
        }

        return $totalCommission;
    }

    /**
     * @return Currency[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function currencies()
    {
        if (is_null($this->currencies)) {
            $this->currencies = Currency::whereHas('products', function ($products) {
                $products->has('investments');
            })->get();
        }

        return $this->currencies;
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param BulkCommissionPayment $bcp
     * @param CommissionRecepient $recipient
     * @return int|mixed
     */
    private function payOtherCommissions(
        ClientTransactionApproval $approval,
        BulkCommissionPayment $bcp,
        CommissionRecepient $recipient
    ) {
        $totalCommission = 0;

        $rePayment = (new \Cytonn\Realestate\Commissions\Payment($recipient))->make($bcp, $approval);
        $totalCommission += $rePayment ? $rePayment->amount : 0;

        $utfPayment = (new \Cytonn\Unitization\Commissions\Payment($recipient))->make($bcp, $approval);
        $totalCommission += $utfPayment ? $utfPayment->amount : 0;

        $sharePayment = (new \Cytonn\Shares\Commission\Payment())->make($recipient, $approval, $bcp);
        $totalCommission += $sharePayment ? $sharePayment->amount : 0;

        return $totalCommission;
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param BulkCommissionPayment $payment
     * @param CommissionRecepient $recepient
     * @param $overallCommission
     * @return \Cytonn\Models\Investment\AdditionalCommissionPayment|\Illuminate\Database\Eloquent\Model|void
     */
    private function payAdditionalCommission(
        ClientTransactionApproval $approval,
        BulkCommissionPayment $payment,
        CommissionRecepient $recepient,
        $overallCommission
    ) {
        return (new \Cytonn\Investment\AdditionalCommission\Payment())
            ->make($recepient, $payment, $approval, $overallCommission);
    }
}
