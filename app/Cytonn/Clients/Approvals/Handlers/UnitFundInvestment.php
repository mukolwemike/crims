<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Events\Investments\Actions\UnitFundPurchaseHasBeenMade;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Unitization\Trading\Buy;
use Illuminate\Support\Facades\Auth;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UnitFundInvestment extends AbstractApproval
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws CRIMSGeneralException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $this->approval = $approval;

        $recipient = isset($data['recipient']) ? CommissionRecepient::find($data['recipient']) : null;

//        $interestPayment = Carbon::parse($data['interest_payment']);
//        $interestStartDate = Carbon::parse($data['interest_start_date']);
//        $interestEndDate = Carbon::parse($data['interest_end_date']);

        $instruction = UnitFundInvestmentInstruction::findOrFail($data['instruction_id']);

        $client = $instruction->client;

        if (!$recipient) {
            $recipient = $client->getLatestFa();
        }

        $this->lockModelForUpdate($client);

        if (!$client->client_code) {
            $this->updateClient($client, $data);
        }

        $date = Carbon::parse($instruction->date);

        $purchase = (new Buy(
            $client,
            $recipient,
            $data['amount'],
            $date,
            null,
            $this->skipFees($data),
            $instruction->unitFund
            //            $interestPayment,
            //            $interestStartDate,
            //            $interestEndDate
        ))->purchase();

        $instruction->purchase()->associate($purchase);

        $instruction->approval()->associate($approval);

        $instruction->save();

        $purchase->update([
            'approval_id' => $approval->id
        ]);

        event(new UnitFundPurchaseHasBeenMade($purchase, $instruction));

        $next = \Redirect::to('dashboard/investments/client-instructions/unit_fund_instruction/' . $instruction->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param Client $client
     * @param $data
     * @return bool
     * @throws CRIMSGeneralException
     */
    public function updateClient(Client $client, $data)
    {
        $code = isset($data['client_code']) ? $data['client_code'] : null;

        if ($code && (strlen($code) >= 2)) {
            $available = Client::where('client_code', $code)->first();

            if ($available && ($available->id != $client->id)) {
                throw new CRIMSGeneralException("Client code entered is already assigned to another client");
            }

            $client->client_code = $code;

            return $client->save();
        }

        (new ApplicationsRepository())->setClientCode($client);

        return true;
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $instruction = UnitFundInvestmentInstruction::findOrFail($data['instruction_id']);

        $date = Carbon::parse($instruction->date);
//        $interestStartDate = Carbon::parse($data['interest_start_date']);
//        $interestEndDate = Carbon::parse($data['interest_end_date']);

//        $interestPayment = $instruction->interest_payment;

        $recipient = CommissionRecepient::find(isset($data['recipient']) ? $data['recipient'] : null);

        $investment = (new Buy(
            $instruction->client,
            $recipient,
            $instruction->amount,
            $date,
            null,
            $this->skipFees($data),
            $instruction->unitFund
            //            $interestPayment,
            //            $interestStartDate,
            //            $interestEndDate
        )
        );

        return [
            'date' => $date->toFormattedDateString(),
            'recipient' => $recipient,
            'instruction' => $instruction,
            'investment' => $investment,
            'unitPrice' => $instruction->unitFund->unitPrice($date),
//            'interest_start_date' => $interestStartDate->toFormattedDateString(),
//            'interest_end_date' => $interestEndDate->toFormattedDateString(),
            'data' => $data,
        ];
    }

    private function skipFees(array $data)
    {
        //if 1 then charge fees
        return !isTruthy($data['fees']);
    }
}
