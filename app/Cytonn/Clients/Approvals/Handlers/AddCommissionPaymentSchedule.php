<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class AddCommissionPaymentSchedule implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);
        $commission = $investment->commission;
        $recipient = $investment->commission->recipient;

        $schedule = new CommissionPaymentSchedule();
        $schedule->commission_id = $commission->id;
        $schedule->date = $data['date'];
        $schedule->description = $data['description'];
        $schedule->amount = $data['amount'];
        $schedule->save();

        \Flash::success('The commission payment schedule has been saved');

        $next = \redirect("/dashboard/investments/commission/payment/".$recipient->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $investment = ClientInvestment::findOrFail($data['investment_id']);

        return ['investment'=>$investment, 'data'=>$data];
    }
}
