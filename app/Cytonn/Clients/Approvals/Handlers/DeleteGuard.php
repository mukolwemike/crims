<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Guard;
use App\Cytonn\Models\GuardType;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\System\Guards\GuardRepository;
use Illuminate\Support\Facades\Response;

class DeleteGuard implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $guardRepository = new GuardRepository();

        $guardRepository->delete($data);

        \Flash::success('Client guard has been deleted successfully');

        $next = \Redirect::to('/dashboard/clients');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $guard = Guard::findOrFail($data['id']);

        return [
            'guard' => $guard,
            'data' => $data
        ];
    }
}
