<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentType;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AddRealEstatePaymentType implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * AddBankDetails constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);
        $data = $approval->payload;

        $type = new RealEstatePaymentType();

        if (isset($data['id'])) {
            is_null($data['id']) ?: $type = RealEstatePaymentType::findOrFail($data['id']);
        }

        $type->fill([
            'name' => $data['name'],
            'accrues_interest' => $data['accrues_interest'],
            'slug' => isset($data['id']) ? $data['slug'] : str_slug($data['name'], '_')
        ]);

        $type->save();

        \Flash::success('RealEstate payment type added successfully');

        $next = \Redirect::to("/dashboard/realestate/projects");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $type = isset($data['id']) ? RealEstatePaymentType::findOrFail($data['id']) : null;

        return [
            'data' => $data,
            'type' => $type
        ];
    }
}
