<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\StatementCampaignType;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundStatementCampaign;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundStatementCampaign implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateUnitFundStatementCampaign constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws \Throwable
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files']);

        $campaign = new UnitFundStatementCampaign();

        $campaign->fill($input);

        $campaign->save();

        if ($campaign->type->slug == 'automated') {
            \Queue::push(
                'Cytonn\Unitization\Reporting\StatementCampaignRepository@fireAutomaticallyAddClients',
                [
                    'campaign_id'=>$campaign->id,
                ],
                config('queue.priority.high')
            );
        }

        \Flash::success('Unit fund statement campaign has been successfully saved');

        $next = \Redirect::to($campaign->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $unitFund = UnitFund::findOrFail($data['unit_fund_id']);

        $type = StatementCampaignType::findOrFail($data['type_id']);

        return [
            'data'=>$data,
            'type'=>$type,
            'unitFund' => $unitFund
        ];
    }
}
