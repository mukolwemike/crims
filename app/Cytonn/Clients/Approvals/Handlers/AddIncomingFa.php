<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-02-25
 * Time: 10:25
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AddIncomingFa implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);
        $data = array_except($data, ['client_id', 'previous_fa_id']);

        $client->update($data);

        \Flash::success('Incoming Financial Advisor has been saved');
        $next = \redirect('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $incoming_fa = CommissionRecepient::findOrFail($data['incoming_fa_id']);

        return ['client' => $client, 'data' => $data, 'incoming_fa' => $incoming_fa];
    }
}
