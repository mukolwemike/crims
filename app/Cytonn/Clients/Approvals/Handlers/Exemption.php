<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\System\Exemptions\ExemptionRepository;
use Illuminate\Support\Facades\Response;

class Exemption implements ApprovalHandlerInterface
{
    /*
     * Setup the constructor for the approval
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('approveExemptions');

        $data = $approval->payload;

        $exemptionRepository = new ExemptionRepository();

        $exemptionRepository->save($data, $data['id']);

        $next = \Redirect::to('/dashboard/system/exemptions');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $exemptedApproval = ClientTransactionApproval::findOrFail($data['client_transaction_approval_id']);

        return [
            'data' => $data,
            'exemptedApproval' => $exemptedApproval
        ];
    }
}
