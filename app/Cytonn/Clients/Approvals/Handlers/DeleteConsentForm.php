<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 17/10/2018
 * Time: 09:05
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\ConsentForm;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class DeleteConsentForm implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $consentForm = ConsentForm::findOrFail($data['consent_form_id']);

        $consentForm->reason = $data['reason'];

        $consentForm->save();

        $consentForm->delete();



        \Flash::success('The Consent Form has been successfully deleted');

        $next = \Redirect::to('/dashboard/clients/details/' . $data['client_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $consentForm = ConsentForm::findOrFail($data['consent_form_id']);

        return [
            'client' => $client,
            'consentForm' => $consentForm
        ];
    }
}
