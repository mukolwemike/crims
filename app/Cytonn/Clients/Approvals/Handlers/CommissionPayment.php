<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/16/16
 * Time: 1:49 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Investment\Commands\CommissionPaymentCommand;

class CommissionPayment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CommissionPayment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $this->execute(CommissionPaymentCommand::class, ['approval'=>$approval]);



        \Flash::success('Commission payment has been made');

        $next = \redirect('/dashboard/investments/commission');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $currency = Currency::findOrFail($data['currency_id']);

        $recipient = CommissionRecepient::findOrFail($data['recipient_id']);

        $date = new Carbon($data['date']);

        $summary = $recipient->calculator($currency, $date)->summary();

        return ['summary'=>$summary, 'date'=>$date, 'recipient'=>$recipient];
    }
}
