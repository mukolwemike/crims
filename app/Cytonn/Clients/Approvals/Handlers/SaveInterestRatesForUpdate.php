<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Rate;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\DataStructures\Collection;

/**
 * Class SaveInterestRatesForUpdate
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class SaveInterestRatesForUpdate implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $update = InterestRateUpdate::findOrFail($data['interest_rate_update_id']);

        Collection::make($data['rates'])->map(
            function ($rate_arr) use ($update) {
                $check = $update->rates()
                    ->where('tenor', $rate_arr['tenor']);

                $rate = $check->exists() ? $check->first() : new Rate();
                $rate->tenor = $rate_arr['tenor'];
                $rate->rate = $rate_arr['rate'];
                $rate->interest_rate_update_id = $update->id;
                $rate->save();
            }
        );

        \Flash::success('The interest rates have been successfully saved.');

        $next = \redirect('/dashboard/investments/interest-rates/show/' . $update->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $update = InterestRateUpdate::findOrFail($data['interest_rate_update_id']);
        $rates = $data['rates'];

        return ['update' => $update, 'rates' => $rates];
    }
}
