<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 03/04/2018
 * Time: 13:16
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class SetDefaultBankAccount implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::find($approval->client_id);

        $client->repo->resetAccounts();

        $client->repo->setDefault($data['bank_account_id']);



        $next = redirect()->back();

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $bankAccount =  ClientBankAccount::findOrFail($data['bank_account_id']);
        $branch = ClientBankBranch::find($bankAccount->branch_id);

        return [
            'account' => $bankAccount,
            'branch' => $branch
        ];
    }
}
