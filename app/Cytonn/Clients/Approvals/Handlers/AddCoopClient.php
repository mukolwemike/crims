<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientContactPerson;
use App\Cytonn\Models\ClientContactPersonRelationship;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\CoopMembershipForm;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\ProductPlan;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesPriceTrail;
use App\Cytonn\Models\Title;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Shares\ShareHoldingRepository;

/**
 * Class AddCoopClient
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class AddCoopClient implements ApprovalHandlerInterface
{

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $form = CoopMembershipForm::findOrFail($data['form_id']);

        if (!isset($data['client_id'])) {
            if (Client::where('client_code', $form->client_code)->count()) {
                \Flash::success('A client with the client code already exists!');
                return \Redirect::back();
            }
            $client = $this->createClient($form);
        } else {
            $client = Client::find($data['client_id']);
        }

        //contact persons
        $contact_persons = Collection::make($form->contact_persons)
            ->map(
                function ($contact_arr) {
                    $contact_arr['name'] = $contact_arr['client_contact_person_name'];
                    unset($contact_arr['client_contact_person_name']);

                    return ClientContactPerson::create($contact_arr);
                }
            )->all();

        $client->contactPersons()->saveMany($contact_persons);

        $entity = SharesEntity::where('name', 'Cytonn Investments Cooperative Society')->first();

        Payment::make(
            $client,
            null,
            null,
            null,
            null,
            $entity,
            Carbon::parse($form->date),
            $form->membership_fee,
            'M',
            'Membership fee'
        )->credit();

        //deduct initial share purchase
        $value_of_share = SharesPriceTrail::getPrice($entity->id, $form->date);
        $number_of_shares = floor($form->share_purchase / $value_of_share);

        (new ShareHoldingRepository())->createHoldingWithPayment(
            [
                'entity_id' => $form->entity_id,
                'category_id' => $form->category_id,
                'client_id' => $client->id,
                'number' => $number_of_shares,
                'price' => $value_of_share,
                'date' => $form->date,
                'approval_id' => $approval->id
            ]
        );

        //add monthly product plans (shares & product)

        if ($form->product_id) {
            ProductPlan::create(
                [
                    'client_id' => $client->id,
                    'product_id' => $form->product_id,
                    'mode_of_payment' => $form->mode_of_payment,
                    'duration' => $form->duration,
                    'amount' => $form->amount_paid_for_product,
                    'start_date' => $form->payment_date,
                    'active' => true,
                ]
            );
        }


        if (!empty($form->shares)) {
            if ((int)$form->shares > 0) {
                $product_plan = new ProductPlan();
                $product_plan->fill(
                    [
                        'client_id' => $client->id,
                        'mode_of_payment' => $form->mode_of_payment,
                        'duration' => $form->duration,
                        'shares' => $form->shares,
                        'start_date' => $form->payment_date,
                        'active' => true,
                    ]
                );
                $product_plan->save();
            }
        }



        \Flash::success('Registration to Coop was successful!');

        $next = \Redirect::to('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /*
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $form = CoopMembershipForm::findOrFail($data['form_id']);

        $contact_persons = $form->contact_persons;

        $titles = function ($id) {
            return Title::find($id);
        };

        $country = Country::findOrFail($form->country_id);

        $employment = Employment::find($form->employment_id);
        $product = Product::find($form->product_id);

        $clientpersonrelationship = function ($id) {
            return ClientContactPersonRelationship::find($id);
        };

        $shareentity = SharesEntity::find($form->entity_id);

        return [
            'data' => $data,
            'contact_persons' => $contact_persons,
            'form' => $form,
            'titles' => $titles,
            'country' => $country,
            'employment' => $employment,
            'product' => $product,
            'shareentity' => $shareentity,
            'clientpersonrelationship' => $clientpersonrelationship
        ];
    }

    /**
     * @param $form
     * @return Client
     */
    public function createClient($form)
    {
        if (!$form->client_type_id) {
            $form->update(['client_type_id' => 1]);
        }

        //create contact
        $contact = new Contact();

        $contact->fill(
            [
                'title_id' => $form->title_id,
                'firstname' => $form->firstname,
                'lastname' => $form->lastname,
                'middlename' => $form->middlename,
                'entity_type_id' => $form->client_type_id,
                'email' => $form->email,
                'phone' => $form->phone,
                'country_id' => $form->country_id,
            ]
        );

        $contact->save();

        //create client
        $client = new Client();

        $client->fill(
            [
                'contact_id' => $contact->id,
                'client_type_id' => $form->client_type_id,
                'fund_manager_id' => $form->fund_manager_id,
                'client_code' => $form->client_code,
                'dob' => $form->dob,
                'phone' => $form->phone,
                'pin_no' => $form->pin_no,
                'id_or_passport' => $form->id_or_passport,
                'postal_code' => $form->postal_code,
                'postal_address' => $form->postal_address,
                'town' => $form->town,
                'street' => $form->street,
                'country_id' => $form->country_id,
                'referee_id' => $form->referee_id,
            ]
        );

        $client->save();

        return $client;
    }
}
