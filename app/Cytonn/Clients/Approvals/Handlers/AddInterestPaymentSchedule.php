<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestPaymentSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AddInterestPaymentSchedule implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        $interest_payment_schedule = new InterestPaymentSchedule();
        $interest_payment_schedule->fill($data);
        $interest_payment_schedule->save();

        \Flash::success('The interest payment schedule has been added');
        $next = \redirect('/dashboard/investments/pay/'.$investment->id);

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);
        $existing_schedules = InterestPaymentSchedule::where('investment_id', $data['investment_id'])->get();

        return ['investment'=>$investment, 'data'=>$data, 'schedules'=>$existing_schedules];
    }
}
