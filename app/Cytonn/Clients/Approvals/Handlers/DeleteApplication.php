<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/19/16
 * Time: 10:52 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Title;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class DeleteApplication implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * DeleteApplication constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     * Perform the removal
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $application = ClientInvestmentApplication::findOrFail($approval->payload['application_id']);


        if ($application->repo->removeApplication()) {
            \Flash::success('Investment application was successfully removed');
        } else {
            throw new ClientInvestmentException('Application could not be removed');
        }

        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $app = $application = ClientInvestmentApplication::findOrFail($data['application_id']);
        $jointHolders = [];
    
        if ($application->joint == 1) {
            $jointHolders = ClientJointDetail::where('application_id', $application->id)->get();
        }
    
        $invested = ClientInvestment::where('application_id', $application->id)->first();
        $corporateinvestortype = CorporateInvestorType::all();
        $titles = Title::all();
        $fundSource = $app->fundSource;

        return[
            'data' => $data,
            'app' => $app,
            'jointHolders' => $jointHolders,
            'invested' => $invested,
            'corporateinvestortype' => $corporateinvestortype,
            'titles' => $titles,
            'fundsource' => $fundSource,
            'product' => $app->product
        ];
    }
}
