<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Events\LOOHasBeenSigned;
use Cytonn\Realestate\Events\LOOHasBeenUploaded;
use Illuminate\Support\Facades\Response;
use App\Cytonn\Models\RealestateLetterOfOffer;

class LOOUploading implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $document_id = $approval->payload['document_id'];
        $date_signed = $approval->payload['date_signed'];

        $loo = RealestateLetterOfOffer::findOrFail($approval->payload['loo_id']);

        $loo = $this->saveHistory($loo, $document_id);

        $loo = $this->saveSigned($loo, $date_signed);

        if (isset($data['sent_on'])) {
            $loo->update([
                'sent_on' => $data['sent_on']
            ]);
        }

        if (isset($data['advocate_id'])) {
            $loo->update([
                'advocate_id' => $data['advocate_id']
            ]);
        }

        $loo->dispatchEventsFor($loo);

        $next = \redirect('/dashboard/realestate/projects');

        \Flash::success('The LOO details have been saved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    protected function saveSigned(RealestateLetterOfOffer $loo, $date_signed)
    {
        $date_signed = $date_signed == '' ? null : $date_signed;
        if (!is_null($loo->date_signed)) {
            $loo->update([
                'date_received' => Carbon::today()
            ]);
        } else {
            $loo->update([
                'date_signed' => $date_signed,
                'date_received' => Carbon::today()
            ]);
        }

        $loo->raise(new LOOHasBeenSigned($loo));

        return $loo;
    }

    protected function saveHistory(RealestateLetterOfOffer $loo, $document_id)
    {
        $old_doc = $loo->document;

        if ($old_doc) {
            $loo->addHistory($old_doc);
        }

        $loo->update([ 'document_id' =>  $document_id ]);

        $loo->raise(new LOOHasBeenUploaded($loo));

        return $loo;
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $loo = RealestateLetterOfOffer::findOrFail($data['loo_id']);
        $project = $loo->holding->project;
        $unit = $loo->holding->unit;
        $client = $loo->holding->client;
        $advocate = function ($id) {
            return Advocate::findOrFail($id);
        };

        return ['project'=>$project, 'unit'=>$unit, 'client'=>$client, 'loo'=>$loo, 'advocate' => $advocate];
    }
}
