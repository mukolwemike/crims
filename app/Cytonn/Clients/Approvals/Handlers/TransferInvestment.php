<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/16/16
 * Time: 11:24 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentTransfer;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Events\InvestmentHasBeenTransferred;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class TransferInvestment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;
    protected $authorizer;

    /**
     * TransferInvestment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
    }

    /**
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $investment = ClientInvestment::findOrFail($approval->payload['investment_id']);
        $client = Client::findOrFail($approval->payload['client_id']);


        $transfer = ClientInvestmentTransfer::createTransfer($investment, $client, $approval);

        $investment->client_id = $client->id;
        $investment->save();



        $investment->raise(new InvestmentHasBeenTransferred($transfer));
        $this->dispatchEventsFor($investment);


        \Flash::success('The transfer was successfully made');

        $next = \Redirect::to('/dashboard/investments');

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $investment = ClientInvestment::findOrFail($data['investment_id']);
        $value = $investment->repo->getTotalValueOfAnInvestment() + $investment->withdraw_amount;
        $dest = Client::findOrFail($data['client_id']);

        if ($approval->approved) {
            $payload = $approval->payload;
            $valueAtTransfer = $approval->payload['value'];
        } else {
            $valueAtTransfer = $value;

            $payload = $approval->payload;
            $payload['value'] = $value;
            $approval->payload = $payload;
            $approval->save();
        }

        return ['investment' => $investment, 'value' => $value, 'dest' => $dest,
            'valueAtTransfer' => $valueAtTransfer, 'payload' => $payload, 'data' => $data];
    }

    public function details(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $investment = ClientInvestment::findOrFail($data['investment_id']);

        return [
            'Investment Amount' => AmountPresenter::currency($investment->amount),
            'Date' => DatePresenter::formatDate($data['date']),
            'Transfer to' => ClientPresenter::presentJointFullNames($data['client_id']),
            'Narrative' => $data['narration']
        ];
    }
}
