<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionClawback;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class AddCommissionClawback implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);
        $commission = $investment->commission;
        $recipient = $investment->commission->recipient;

        $clawback = new CommissionClawback();
        $clawback->commission_id = $commission->id;
        $clawback->date = $data['date'];
        $clawback->narration = $data['narration'];
        $clawback->amount = $data['amount'];
        $clawback->type = 'withdraw';
        $clawback->save();

        \Flash::success('The commission clawback has been saved');

        $next = redirect("/dashboard/investments/commission/payment/".$recipient->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $investment = ClientInvestment::findOrFail($data['investment_id']);

        return ['investment'=>$investment, 'data'=>$data];
    }
}
