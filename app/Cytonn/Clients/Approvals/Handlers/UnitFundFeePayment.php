<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/7/18
 * Time: 4:50 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeePaymentRecipient;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use App\Cytonn\Unitization\UnitFundFeePaymentRepository;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Custodial\Transact\ClientTransaction;

class UnitFundFeePayment implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['unit_fund_id']);

        $payment = (new UnitFundFeePaymentRepository())->make($approval);



        \Flash::success('The payment has been added successfully');

        $next = \Redirect::to('/dashboard/unitization/unit-funds/'.$fund->id.'/unit-fund-fee-payments');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fee = UnitFundFee::findOrFail($data['fee_id']);

        $fee = UnitFundFeeType::findOrFail($fee->unit_fund_fee_type_id);

        $recipient = UnitFundFeePaymentRecipient::findOrFail($data['recipient_id']);

        $fund = UnitFund::findOrFail($data['unit_fund_id']);

        return [
            'data' => $data,
            'fund' => $fund,
            'recipient' => $recipient,
            'fee' => $fee
        ];
    }
}
