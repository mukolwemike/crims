<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientInvestmentInstruction;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class CancelClientWithdrawRolloverInstruction implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $instruction = ClientInvestmentInstruction::findOrFail($data['instruction_id']);
        $instruction->inactive = true;
        $instruction->save();



        $next = \redirect('/dashboard/investments/client-instructions/rollover/'.$instruction->id);

        \Flash::success('The client ' . $instruction->type->name . ' instruction has been successfully cancelled.');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $instruction = ClientInvestmentInstruction::findOrFail($data['instruction_id']);

        return ['instruction'=>$instruction];
    }
}
