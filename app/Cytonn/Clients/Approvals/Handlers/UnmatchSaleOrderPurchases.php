<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/2/18
 * Time: 3:39 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesSalesOrder;
use Carbon\Carbon;
use Cytonn\Api\Transformers\ShareHoldingsTransformer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Shares\ShareTradingRepository;

class UnmatchSaleOrderPurchases implements ApprovalHandlerInterface
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);

        $seller = $salesOrder->seller;

        $purchases = $purchases = SharePurchases::whereIn('id', $data['share_purchase_ids'])->get();

        $this->repo->reverseShareMatching($salesOrder, $purchases);



        \Flash::success('Shares matching was successfully unmatched!');

        $next = \redirect('/dashboard/shares/sales/shareholders/'.$seller->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);

        $seller = $salesOrder->seller;

        $sold = $salesOrder->numberSold();

        $balance = $salesOrder->balance();

        $purchases = SharePurchases::whereIn('id', $data['share_purchase_ids'])
            ->get()
            ->map(function ($purchase) {
                return (new ShareHoldingsTransformer())->details($purchase);
            });

        return [
            'seller' => $seller,
            'salesOrder' => $salesOrder,
            'sold' => $sold,
            'balance' => $balance,
            'purchases' => $purchases
        ];
    }
}
