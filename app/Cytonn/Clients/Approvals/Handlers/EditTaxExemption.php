<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client\TaxExemption;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientRepository;

class EditTaxExemption implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        (new ClientRepository())->uploadTaxExemption($data, $data['document_id']);

        \Flash::success('Tax Exemption Details successfully edited');

        $next = \Redirect::to('/dashboard/clients/details/' . $data['client_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $taxExemption = TaxExemption::find($data['tax_id']);

        return [
            'data' => $data,
            'taxExemption' => $taxExemption
        ];
    }
}
