<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Investment\Seip\SeipInvestment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Models\Investment\InvestmentPaymentScheduleDetail;
use Illuminate\Support\Facades\Response;

class StoreInvestmentPaymentScheduleDetail implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $scheduleDetail = isset($data['id']) ? InvestmentPaymentScheduleDetail::findOrFail($data['id']) : null;

        $investment = ClientInvestment::findOrFail($scheduleDetail->investment_id);

        $paidSchedules = $investment->childInvestmentPaymentSchedules()->whereNotNull('investment_id')->exists();

        if ($paidSchedules) {
            throw new ClientInvestmentException("There are existing schedules that are already paid");
        }

        $input = [
            'investment_id' => $data['investment_id'],
            'amount' => $data['amount'],
            'payment_date' => $data['payment_date'],
            'payment_interval' => $data['payment_interval']
        ];

        if ($scheduleDetail) {
            $scheduleDetail->update($input);
        } else {
            $scheduleDetail = InvestmentPaymentScheduleDetail::create($input);
        }

        (new SeipInvestment())->regenerateSchedules($investment->fresh());

        \Flash::success('The investment payment schedule detail has been saved');

        $next = \redirect('/dashboard/investments/investment_payment_schedules/' . $scheduleDetail->investment_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $scheduleDetail = isset($data['id']) ? InvestmentPaymentScheduleDetail::findOrFail($data['id']) : null;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        return [
            'data' => $data,
            'scheduleDetail' => $scheduleDetail,
            'investment' => $investment
        ];
    }
}
