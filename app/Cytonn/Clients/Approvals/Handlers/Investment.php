<?php
/**
 * Date: 30/04/2016
 * Time: 10:54 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Forms\BusinessConfirmationForm;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class Investment
 *
 * @package Cytonn\Investment\Approvals\Handlers
 */
class Investment extends AbstractApproval
{
    use EventGenerator, DispatchableTrait, CommanderTrait;
    /**
     * @var BusinessConfirmationForm
     */
    public $confirmationForm;

    protected $authorizer;

    protected $formats = [
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'amount' => 'amount',
        'tenor' => 'append: days',
        'interest_payment_interval' => 'append: months',
        'interest_rate' => 'append:%',
        'commission_rate' => 'append:%'
    ];


    /**
     * Investment constructor.
     */
    public function __construct(ClientTransactionApproval $approval = null)
    {
        $this->authorizer = new Authorizer();
        $this->confirmationForm = \app(BusinessConfirmationForm::class);
        parent::__construct($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        dd('Should not be used');
    }


    /**
     * @param ClientTransactionApproval $approval
     * @param array $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $interestPayment = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months',
            5 => 'Every 5 months', 6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months',
            8 => 'Every 8 months', 9 => 'Every 9 months', 10 => 'Every 10 months',
            11 => 'Every 11 months', 12 => 'Every 12 months (Annually)'];


        $clientinvestmentapplication = ClientInvestmentApplication::find($data['application_id']);
        $commissionrecepient = CommissionRecepient::find($data['commission_recepient']);

        $findInterestActions = function ($id) {
            return InterestAction::findOrFail($id);
        };

        $product = $clientinvestmentapplication->product;

        $commissionrecepientlist = CommissionRecepient::all()->lists('name', 'id');
        $commissionrates = CommissionRate::all()->lists('name', 'rate');

        return [
            'interestPayment' => $interestPayment,
            'data' => $data,
            'product' => $product,
            'clientinvestmentapplication' => $clientinvestmentapplication,
            'commissionrecepient' => $commissionrecepient,
            'interestActions' => $findInterestActions,
            'commissionrecepientlist' => $commissionrecepientlist,
            'commissionrates' => $commissionrates
        ];
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function pluck($name)
    {
        $payload = $this->approval->payload;

        switch ($name) {
            case 'product':
                return Product::find($payload['product_id'])->name;
            case 'tenor':
                return Carbon::parse($payload['invested_date'])->diffInDays(Carbon::parse($payload['maturity_date']));
            case 'fa_name':
                return CommissionRecepient::find($payload['commission_recepient'])->name;
            case 'amount':
                $application = ClientInvestmentApplication::findOrFail($payload['application_id']);
                return $application->amount;
        }
    }
}
