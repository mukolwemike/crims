<?php
/**
 * Date: 21/02/2018
 * Time: 15:12
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientPaymentAllowedMinimum;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\InterestAction;
use App\Exceptions\CrimsException;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\SchedulableApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\Action\Rollover;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Support\Facades\Response;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\FCM\FcmManager;

class RolloverInvestment extends SchedulableApproval
{
    use DispatchableTrait, EventGenerator;

    protected $formats = [
        'amount' => 'amount',
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'interest_rate' => 'append:%',
        'tenor' => 'append: days',
        'commission_rate' => 'append:%',
        'interest_payment_interval' => 'append: months'
    ];

    public function preApproval()
    {
        $instruction = $this->approval->withdrawInstruction;

        if ($instruction->topup_amount <= 0) {
            return;
        }

        $date = $instruction->repo->valueDate();

        $balance = ClientPayment::balance(
            $this->approval->client,
            $instruction->investment->product,
            null,
            null,
            $date
        );

        $bal = ClientPaymentAllowedMinimum::where('client_id', $this->approval->client->id)
            ->where('date', '>', $date)
            ->where(function ($q) use ($instruction) {
                $q->where('product_id', $instruction->investment->product->id);
            })
            ->sum('amount');

        $balance = $balance - $bal;

        if (($balance - $instruction->topup_amount) < -1) {
            throw new ClientInvestmentException("The client does not have enough cash for this transaction. 
                Balance: " . AmountPresenter::currency($balance)
                . " Requested: " . AmountPresenter::currency($instruction->topup_amount));
        }
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return Response|void
     * @throws CrimsException
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $instruction = $approval->withdrawInstruction;

        if ($instruction->due_date->isFuture()) {
            $this->schedule($approval, $instruction->due_date, ['investment_id' => $instruction->investment->id]);

            \Flash::success('The rollover has been scheduled');

            $next = redirect()->to('/dashboard/investments');
        } else {
            $rollover = $this->runTransaction($approval);

            $next = view('investment.actions.rollover.combinedrolloversuccess', [
                'Title' => 'Investment Rollover',
                'investments' => $instruction->allInvestments(),
                'rollover' => $rollover
            ]);

            $fcmManager = new FcmManager();

            $fcmManager->sendFcmMessageToMultipleDevices(
                $instruction->investment->client,
                $instruction->investment->uuid,
                $instruction->investment->product->id,
                "Rollover approved",
                'Success, rollover request approved',
                'rollover'
            );

            \Flash::success('The investment rollover has been saved');
        }



        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function calculatePenalty()
    {
        return 0;
    }

    public function calculateAmountWithdrawn()
    {
        $instruction = $this->approval->withdrawInstruction;
        $investment = $instruction->investment;

        $total = $investment->calculate($investment->due_date, false)->total();

        return $total - $instruction->repo->calculateAmountAffected();
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $this->prepare($approval);

        $instruction = $approval->withdrawInstruction;

        $approvals = $instruction->clientApprovals->map(function (ClientInstructionApproval $appr) {

            $name = $appr->user ? $appr->user->firstname . ' ' . $appr->user->lastname : '';

            return [
                'id' => $appr->id,
                'user' => $name,
                'requested' => $appr->requestedByUser(),
                'status' => $appr->approvalStatus ? $appr->approvalStatus->name : 'pending approval',
                'approval_status' => $appr->status,
                'reason' => $appr->reason ? $appr->reason : 'N/A'
            ];
        });

        $approved = $instruction ? $instruction->repo->approvedByClient() : true;

        $investments = $instruction->allInvestments()
            ->each(function (ClientInvestment $investment) use ($instruction) {
                $c = $investment->calculate($instruction->due_date, false);
                $investment->total_value = $c->total();
                $investment->net_interest = $c->total();
            });

        $commissionRecipient = CommissionRecepient::findOrFail($data['commission_recepient']);
        $prev = $commissionRecipient->getPreviousPosition();

        $commissionRecipient->previous = $prev ? $prev->type : null;

        $interestAction = function ($id) {
            return InterestAction::findOrFail($id);
        };

        $interestPayment = app(Controller::class)->interestIntervals();

        $product = $instruction->investment->product;

        $periodicRates = collect([]);

        if ($product->present()->isSeip) {
            $periodicRates = $product->periodicRatesAsAt($instruction->repo->valueDate());
        }

        return [
            'interestAction' => $interestAction,
            'interestPayment' => $interestPayment,
            'amount' => $instruction->repo->calculateAmountAffected(),
            'instruction' => $instruction,
            'investment' => $instruction->investment,
            'investments' => $investments,
            'invested_date' => $instruction->repo->valueDate(),
            'maturity_date' => $instruction->repo->maturityDate(),
            'commissionRecepient' => $commissionRecipient,
            'client' => $approval->client,
            'approvals' => $approvals,
            'approved' => $approved,
            'periodicRates' => $periodicRates
        ];
    }

    private function prepare($approval)
    {
        $instruction = $approval->withdrawInstruction;
        $investment = $instruction->investment;

        $investments = $instruction->allInvestments()->count();

        if ($investments > 1) {
            return;
        }

        $gt = $instruction->due_date->gt($investment->maturity_date->startOfDay());

        if ($gt) {
            $instruction->update(['due_date' => $investment->maturity_date]);
        }
    }

    protected function pluck($name)
    {
        $payload = $this->approval->payload;

        $instr = $this->approval->withdrawInstruction;

        switch ($name) {
            case 'invested_date':
                return $instr->repo->valueDate();
            case 'maturity_date':
                return $instr->repo->maturityDate();
            case 'product':
                return $instr->investment->product->name;
            case 'tenor':
                return $instr->repo->valueDate()->diffInDays($instr->repo->maturityDate());
            case 'fa_name':
                return CommissionRecepient::find($payload['commission_recepient'])->name;
            case 'amount':
                return $instr->repo->calculateAmountAffected();
        }
    }


    /**
     * @param $approval
     * @return mixed
     * @throws \Exception
     * @throws CRIMSGeneralException
     */
    public function runTransaction($approval)
    {
        $instruction = $approval->withdrawInstruction;

        $this->lockModelForUpdate($instruction->investment);
        $this->lockModelForUpdate($instruction->investment->client);

        if ($instruction->investment->withdrawn) {
            throw new CRIMSGeneralException("Investment Withdrawn");
        }

        return $this->executeWithinAtomicLock(function () use ($instruction) {
            $rollover = new Rollover();

            $rollover->run($instruction);

            return $rollover;
        }, 'investment_id' . $instruction->investment);
    }
}
