<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharesEntity;
use App\Http\Controllers\Flash;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Redirect;

class EditSharesPrice implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $shareEntity = SharesEntity::findOrFail($data['id']);

        foreach ($shareEntity->priceTrails as $trail) {
            $trail->price = $data['price'];
            $trail->save();
        }

        Flash::success('Share Entity has been edited succesfully');
        $next = Redirect::to('/dashboard/shares/entities/');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }


    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $entity = SharesEntity::findOrFail($data['id']);

        return ['data' => $data, 'entity' => $entity];
    }
}
