<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesEntityIssue;
use App\Cytonn\Models\SharesPriceTrail;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

/**
 * Class MakeSharesEntityIssue
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class MakeSharesEntityIssue implements ApprovalHandlerInterface
{

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $entity = SharesEntity::findOrFail($data['entity_id']);

        $issue = new SharesEntityIssue();
        $issue->fill($data);
        $issue->save();

        $trail = new SharesPriceTrail();
        $trail->entity_id = $issue->entity_id;
        $trail->category_id = 1;
        $trail->price = $issue->price;
        $trail->date = $issue->issue_date;
        $trail->save();




        \Flash::success('Entity shares were successfully issued!');
        $next = \redirect('/dashboard/shares/entities/show/' . $entity->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $entity = SharesEntity::findOrFail($data['entity_id']);

        return ['data'=>$data, 'entity'=>$entity];
    }
}
