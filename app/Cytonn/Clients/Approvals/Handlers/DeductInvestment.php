<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/16/16
 * Time: 1:19 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientInvestment;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Authorization\Authorizer;

class DeductInvestment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * DeductInvestment constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $investment = ClientInvestment::findOrFail($approval->payload['investment_id']);

        if ($investment->withdrawn) {
            throw new ClientInvestmentException(
                'The investment has already been withdrawn, you cannot make deductions to it'
            );
        }

        $investment->repo->makeDeduction($approval);



        \Flash::success('The deduction was succesfully made');

        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }


    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        try {
            $investment = ClientInvestment::findOrFail($data['investment_id']);
            $not_found = false;
        } catch (\Exception $e) {
            $not_found = true;
        }
        return ['investment'=>$investment, 'not_found'=>$not_found, 'data'=>$data];
    }
}
