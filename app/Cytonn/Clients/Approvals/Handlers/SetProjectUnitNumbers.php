<?php
/**
 * Date: 16/06/2016
 * Time: 2:56 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealestateUnitSize;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstateSizeNumber;

class SetProjectUnitNumbers implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $size = RealEstateSizeNumber::where('project_id', $data['project_id'])
            ->where('size_id', $data['size_id'])->first();

        $input = [
            'project_id' => $data['project_id'],
            'size_id' => $data['size_id'],
            'number' => $data['number']
        ];

        if ($size) {
            $size->update($input);
        } else {
            RealEstateSizeNumber::create($input);
        }

        $next = \Redirect::to('/dashboard/realestate/projects');

        \Flash::success('The number of units available have been set');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $realestateUnitSize = RealestateUnitSize::findOrFail($data['size_id']);

        $project = Project::findOrFail($approval->payload['project_id']);
        
        return [
           'project'=>$project, 'data'=>$data, 'realestateUnitSize'=>$realestateUnitSize
        ];
    }
}
