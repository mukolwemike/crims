<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 19/04/2018
 * Time: 15:31
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class EditClientTenor implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $client->combine_interest_reinvestment_tenor = $data['combine_interest_reinvestment_tenor'];
        $client->save();

        \Flash::success("Tenor has been successfully edited");

        $next = \Redirect::to('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        return [
            'data' => $data,
            'client' => $client
        ];
    }
}
