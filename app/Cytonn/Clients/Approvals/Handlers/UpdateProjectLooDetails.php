<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ProjectType;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Realestate\Commissions\Calculator;
use Laracasts\Commander\CommanderTrait;
use Cytonn\Realestate\Commands\SaveTrancheCommand;
use App\Cytonn\Models\Project;

class UpdateProjectLooDetails implements ApprovalHandlerInterface
{
    use CommanderTrait;
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $project = Project::findOrFail($data['project_id']);

        $project->update(array_except($data, ['project_id']));

        $next = \Redirect::back();

        \Flash::success('The project LOO details have been saved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $project = Project::findOrFail($data['project_id']);
        $calculators = (new Calculator())->getCalculatorList();
        $projectType = ProjectType::find($project->type_id);
        return ['data'=>$data, 'project'=>$project, 'calculators'=>$calculators, 'projectType'=>$projectType];
    }
}
