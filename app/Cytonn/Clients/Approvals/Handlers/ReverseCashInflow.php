<?php
/**
 * Date: 14/03/2017
 * Time: 15:08
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\CRIMSGeneralException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class ReverseCashInflow implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $transaction = CustodialTransaction::find($data['transaction_id']);

        if (!$transaction) {
            throw new CRIMSGeneralException('Custodial Transaction could not be found, its probably already'
                . ' deleted');
        }

        $payment = $transaction->clientPayment;

        if (!$payment) {
            throw new CRIMSGeneralException('Payment could not be found, its probably already deleted');
        }

        $transaction->delete();

        $payment->delete();

        $next = Redirect::to('/dashboard/investments/accounts-cash');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        \Flash::success('The transaction has been reversed');

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $transaction = CustodialTransaction::withTrashed()->findOrFail($data['transaction_id']);

        $payment = ClientPayment::where('custodial_transaction_id', $transaction->id)->withTrashed()->first();

        return [
            'data' => $data,
            'transaction' => $transaction,
            'payment' => $payment
        ];
    }
}
