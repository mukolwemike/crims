<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\Title;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class UpdateRealEstatePayment
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class UpdateRealEstatePayment implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;

    /**
     * Make the payment
     *
     * @param  ClientTransactionApproval $approval
     * @return null
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $payment = RealEstatePayment::findOrFail($data['payment_id']);

        //Check for real estate guards
        $payment->holding->client->repo->checkReGaurds($approval);

        $payment->update(array_except($data, ['payment_id', 'old', 'amount', 'date']));

        \Flash::success('Payment was successfully updated.');

        $next = \Redirect::to('/dashboard/realestate/projects/' .
            $payment->holding->project->id . '/units/show/' . $payment->holding->unit_id);

        $this->dispatchEventsFor($this);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Create vars required in the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $payment = RealEstatePayment::findOrFail($data['payment_id']);
        $holding = $payment->holding;

        $old = $this->getOld($approval, $payment);

        $schedule = RealEstatePaymentSchedule::find($data['schedule_id']);
        $reservationForm = $holding->reservation;
        $price = $holding->price();

        $titles = Title::all();

        if (isset($data['tranche_id']) && strlen(trim($data['tranche_id'])) > 0 && $price == 0) {
            $tranche = RealEstateUnitTranche::find($data['tranche_id']);
            try {
                $payment_plan = RealEstatePaymentPlan::find($data['payment_plan_id']);
                $price = $tranche->repo->getUnitPrice($holding->unit, $payment_plan);
            } catch (\Exception $e) {
                $price = 0;
            }
        }

        return [
            'holding' => $holding, 'form' => $holding->reservation, 'project' => $holding->unit->project,
            'unit' => $holding->unit, 'schedule' => $schedule, 'reservationForm' => $reservationForm,
            'joints' => $holding->reservation->jointClients, 'price' => $price, 'old' => $old, 'titles' => $titles
        ];
    }

    private function getOld($approval, $payment)
    {
        if (!$approval->approved) {
            //create the old
            $old = $payment;
            $old->schedule = $payment->schedule;
            $payload = $approval->payload;
            $payload['old'] = json_encode($old);
            $approval->payload = $payload;
            $approval->save();

            return json_decode($approval->payload['old']);
        } elseif (isset($approval->payload['old'])) {
            return json_decode($approval->payload['old']);
        } else {
            //just return current data
            return $payment;
        }
    }
}
