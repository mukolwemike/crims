<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Coop\Payments\CoopPaymentRepository;
use Cytonn\Core\DataStructures\Collection;

/**
 * Class MakeCoopPayment
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class MakeCoopPayment implements ApprovalHandlerInterface
{
    protected $coopPaymentRepository;

    /**
     * MakeCoopPayment constructor.
     *
     * @param $coopPaymentRepository
     */
    public function __construct()
    {
        $this->coopPaymentRepository = new CoopPaymentRepository();
    }


    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $custodial_account = CustodialAccount::findOrFail($data['account_id']);

        $data['approval_id'] = $approval->id;


        $this->coopPaymentRepository->createCoopPayment($data, $custodial_account);



        \Flash::success('Coop payment was successfully made!');
        $next = \redirect('/dashboard/clients/details/'.$data['client_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        return ['data'=>$data];
    }
}
