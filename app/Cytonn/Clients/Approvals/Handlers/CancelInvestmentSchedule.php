<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class CancelInvestmentSchedule implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $schedule = ClientTransactionApproval::findOrFail($data['schedule_id']);

        $schedule->update([
            'action_date'   =>  null
        ]);



        $next = \redirect('/dashboard/investments/clientinvestments/scheduled');

        \Flash::success('The scheduled investment has been successfully cancelled');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedule = ClientTransactionApproval::findOrFail($data['schedule_id']);

        $recipient = function ($id) {
            return CommissionRecepient::find($id);
        };

        $clientInvestment = ClientInvestment::findOrFail($schedule->investment_id);

        return [
            'schedule' => $schedule,
            'recipient' => $recipient,
            'clientinvestment' => $clientInvestment,
            'investment' => $clientInvestment
        ];
    }
}
