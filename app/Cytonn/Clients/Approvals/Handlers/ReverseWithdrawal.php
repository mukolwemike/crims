<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/19/16
 * Time: 10:15 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Reverse;
use Cytonn\Investment\Commands\ReverseWithdrawalCommand;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class ReverseWithdrawal implements ApprovalHandlerInterface
{
    use DispatchableTrait;
    use EventGenerator;
    use CommanderTrait;

    protected $authorizer;

    /**
     * ReverseWithdrawal constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }


    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $withdrawal = ClientInvestmentWithdrawal::findOrFail($approval->payload['withdrawal_id']);

        $reverse = new Reverse();

        $reverse->withdrawal($withdrawal);

        \Flash::success('The withdrawal has been reversed');

        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }


    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $withdrawal = null;
        $investment = null;
        if (isset($data['investment_id'])) {
            $investment = ClientInvestment::findOrFail($data['investment_id']);
        }

        if (isset($data['withdrawal_id'])) {
            $withdrawal = ClientInvestmentWithdrawal::withTrashed()->findOrFail($data['withdrawal_id']);

            $investment = $withdrawal->investment;
        }

        return['investment'=>$investment, 'withdrawal' => $withdrawal, 'data'=>$data];
    }
}
