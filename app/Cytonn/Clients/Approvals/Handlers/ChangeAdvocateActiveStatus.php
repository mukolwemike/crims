<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\AdvocateProject;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class ChangeAdvocateActiveStatus implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $advocate = Advocate::findOrFail($data['advocate_id']);
        $advocate->active = ! (bool) $advocate->active;
        $advocate->save();



        \Flash::success('Advocate active status been successfully updated');

        $next = \Redirect::to("/dashboard/realestate/advocates/{$advocate->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $advocate = Advocate::findOrFail($data['advocate_id']);

        return ['data'=>$data, 'advocate'=>$advocate];
    }
}
