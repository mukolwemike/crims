<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUserAccess;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;

/**
 * Class DeleteUnusedClientAccount
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class DeleteUnusedClientAccount implements ApprovalHandlerInterface
{

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @throws \Exception
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        if ($client->repo->hasProducts()) {
            throw new ClientInvestmentException('The client is already linked to a product or '
                . 'payment and thus cant be deleted');
        }

        ClientUserAccess::where('client_id', $client->id)->delete();

        $contact = $client->contact;

        $contact->delete();
    
        $client->delete();

        \Flash::success('The client account has been successfully deleted');

        $next = \redirect('/dashboard/clients');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        return ['client' => $client];
    }
}
