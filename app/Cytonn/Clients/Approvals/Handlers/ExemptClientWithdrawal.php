<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/13/18
 * Time: 12:39 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\DataStructures\Carbon;

class ExemptClientWithdrawal implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $date = isset($data['date']) ? Carbon::parse($data['date']) : Carbon::today();

        $payment = ClientPayment::findOrFail($data['payment_id']);

        $payment->date = $date;

        $payment->save();

        $client = Client::findOrFail($data['client_id']);

        \Flash::success('Client withdrawal payment exemption has been approved');

        $next = \Redirect::to('/dashboard/investments/client-payments/client/'.$client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $payment = ClientPayment::findOrFail($data['payment_id']);

        $client = Client::findOrFail($data['client_id']);

        $user = User::findOrFail($data['user_id']);

        return [
            'data' => $data,
            'client' => $client,
            'user' => $user,
            'payment' => $payment,
         ];
    }
}
