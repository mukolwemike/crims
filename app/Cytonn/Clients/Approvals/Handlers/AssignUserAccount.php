<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/19/16
 * Time: 11:20 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Users\UserClientRepository;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AssignUserAccount implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * AssignUserAccount constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $user = ClientUser::findOrFail($approval->payload['user_id']);

        if ($user->repo->hasAccess($approval->client)) {
            throw new ClientInvestmentException('The user already has access, no need to reassign access');
        }

        (new UserClientRepository($user))->assignAccess($approval->client, $approval);

        \Flash::success('Client account successfully given access');



        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }



    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $user = ClientUser::findOrFail($data['user_id']);

        $client = $approval->client;

        $signature_to_assign = (new UserClientRepository($user))->signature($client);

        return [
            'user' => $user,
            'data' => $data,
            'signature_to_assign' => $signature_to_assign
        ];
    }
}
