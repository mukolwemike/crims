<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSwitchRate;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundSwitchRate implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;
    
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }
    
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $input = array_except($data, ['_token', 'files', 'fund_id']);

        $rate = new UnitFundSwitchRate($input);
        $rate->fund()->associate($fund);
        $rate->save();



        \Flash::success('Unit fund switch rate has been successfully saved');

        $next = \Redirect::to($rate->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }
    
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $fund = UnitFund::findOrFail($data['fund_id']);

        return ['data'=>$data, 'fund'=>$fund];
    }
}
