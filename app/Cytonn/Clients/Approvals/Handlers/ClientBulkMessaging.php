<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBulkMessage;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Mailers\GeneralMailer;

/**
 * Class ClientBulkMessaging
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class ClientBulkMessaging implements ApprovalHandlerInterface
{
    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $message = is_null($data['message_id'])
            ? new ClientBulkMessage() : ClientBulkMessage::findOrFail($data['message_id']);

        $message->approval_id = $approval->id;
        $message->composer = $data['composer'];
        $message->sent = true;
        $message->subject = $data['subject'];
        $message->body = $data['body'];

        $message->save();



        \Queue::push(static::class.'@sendMessage', ['message_id'=>$message->id]);

        \Flash::success('The message has been sent to the clients');

        $next = \redirect('/dashboard/investments/bulk-messages/show/'.$message->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array                     $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $message = is_null($data['message_id'])
            ? new ClientBulkMessage() : ClientBulkMessage::findOrFail($data['message_id']);

        return ['data'=>$data, 'message'=>$message];
    }

    public function sendMessage($job, $data)
    {
        $message = ClientBulkMessage::findOrFail($data['message_id']);

        Client::with('contact')
            ->get()
            ->each(
                function ($client) use ($message) {
                    $this->email($client, $message);
                }
            );

        $job->delete();
    }

    private function email($client, $message)
    {
        $mailer = new GeneralMailer();
        $mailer->to($client->getContactEmailsArray());
        $mailer->from(['clientservices@cytonn.com' => 'Cytonn Investments']);
        $mailer->subject($message->subject);
        $mailer->sendGeneralEmail($message->body);
    }
}
