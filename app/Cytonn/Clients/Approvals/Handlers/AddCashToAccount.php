<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Custodial\Transact\ClientTransaction;
use Cytonn\Custodial\Validate\Inflow;
use Cytonn\Presenters\ClientPresenter;

class AddCashToAccount extends AbstractApproval
{
    use ValidatorTrait;

    protected $formats = [
        'date' => 'date',
        'entry_date' => 'date',
        'amount' => 'amount',
        'client_amount' => 'amount',
    ];

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $this->lockModelForUpdate($approval);
        $this->lockModelForUpdate($approval->client);

        $project = isset($data['project_id']) ? Project::find($data['project_id']) : null;
        $product = isset($data['product_id']) ? Product::find($data['product_id']) : null;
        $entity = isset($data['entity_id']) ? SharesEntity::find($data['entity_id']) : null;
        $fund = isset($data['unit_fund_id']) ? UnitFund::find($data['unit_fund_id']) : null;
        $client = !is_null($data['client_id']) ? Client::find($data['client_id']) : new Client();
        $recipient = !is_null($data['commission_recipient_id'])
            ? CommissionRecepient::find($data['commission_recipient_id']) : new CommissionRecepient();

        is_null($client->id) ?: $data['received_from'] = Client::findOrFail($data['client_id'])->client_code . ' : ' .
            ClientPresenter::presentFullNames($data['client_id']);

        $account = CustodialAccount::findOrFail($data['custodial_account_id']);

        $date = Carbon::parse($data['date']);

        $data = $this->fillMissingKeys($data, ['received_from', 'bank_reference_no']);

        (new Inflow())->validate($data);
        (new Inflow())->checkIfDuplicate($data);

        $sourceDetails = $this->filter($data, [
            'exchange_rate',
            'source',
            'entry_date',
            'cheque_number',
            'mpesa_confirmation_code',
            'received_date',
            'cheque_document_id'
        ]);

        $transaction = ClientTransaction::build(
            $account,
            'FI',
            $date,
            $data['description'],
            $data['amount'],
            $data['bank_reference_no'],
            $sourceDetails,
            $client,
            $recipient,
            $data['received_from']
        )->setClientApproval($approval)->debit();

        $transaction->repo->setChequeAsNotValued();

        $clientAmount = $data['amount'] * $data['exchange_rate'];

        $productGroup = [
            'fund' => $fund,
            'product' => $product,
            'entity' => $entity,
            'project' => $project
        ];

        $this->createClientPayment(
            $transaction,
            $approval,
            $clientAmount,
            $productGroup,
            $date,
            $client,
            $recipient
        );

        \Flash::success('Inflow has been successfully added to custodial account');

        $next = \Redirect::to('/dashboard/investments/accounts-cash');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param CustodialTransaction $transaction
     * @param ClientTransactionApproval $approval
     * @param $clientAmount
     * @param array $productGroup
     * @param Carbon $date
     * @param Client|null $client
     * @param CommissionRecepient|null $recipient
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function createClientPayment(
        CustodialTransaction $transaction,
        ClientTransactionApproval $approval,
        $clientAmount,
        array $productGroup,
        Carbon $date,
        Client $client = null,
        CommissionRecepient $recipient = null
    ) {
        $product = !isset($productGroup['product']) ? : $productGroup['product'];
        $project = !isset($productGroup['project']) ?: $productGroup['project'];
        $entity = !isset($productGroup['entity']) ?: $productGroup['entity'];
        $fund = !isset($productGroup['fund']) ?: $productGroup['fund'];

        $payment = Payment::make(
            $client,
            $recipient,
            $transaction,
            $product instanceof Product ? $product : null,
            $project instanceof Project ? $project : null,
            $entity instanceof SharesEntity ? $entity : null,
            $date,
            $clientAmount,
            'FI',
            'Client funds deposited',
            $fund instanceof UnitFund ? $fund : null
        )->debit();

        $payment->update(['approval_id' => $approval->id]);

        $payment->repo->setChequeAsNotValued();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $data = $this->fillMissingKeys(
            $data,
            ['received_from', 'bank_reference_no', 'cheque_number', 'mpesa_confirmation_code']
        );

        $client = is_null($data['client_id']) ? new Client() : Client::findOrFail($data['client_id']);

        $recipient = is_null($data['commission_recipient_id']) ? new CommissionRecepient() :
            CommissionRecepient::findOrFail($data['commission_recipient_id']);

        $transaction = is_null($data['id']) ? new CustodialTransaction() :
            CustodialTransaction::findOrFail($data['id']);
        $payment = is_null($transaction->id) ? new ClientPayment() :
            ClientPayment::where('custodial_transaction_id', $transaction->id)->first();

        $account = CustodialAccount::findOrFail($data['custodial_account_id']);

        $products = function ($id) {
            return Product::findOrFail($id);
        };

        $shareentities = function ($type) {
            return SharesEntity::find($type);
        };

        $projects = function ($id) {
            return Project::find($id);
        };

        $funds = function ($id) {
            return UnitFund::find($id);
        };

        $custodialtransactionaccount = function ($id) {
            return CustodialTransactionType::find($id);
        };

        return [
            'client' => $client, 'data' => $data,
            'transaction' => $transaction, 'recipient' => $recipient,
            'payment' => $payment, 'account' => $account,
            'products' => $products, 'shareentities' => $shareentities,
            'custodialtransactionaccount' => $custodialtransactionaccount,
            'projects' => $projects, 'funds' => $funds
        ];
    }

    protected function pluck($name)
    {
        $approval = $this->approval;
        $data = $approval['payload'];

        $account = CustodialAccount::findOrFail($data['custodial_account_id']);
        switch ($name) {
            case 'account':
                return $account->account_name;
            case 'currency':
                return $account->currency->code;
            case 'product':
                if (isset($data['project_id'])) {
                    return Project::findOrFail($data['project_id'])->name;
                } elseif (isset($data['product_id'])) {
                    return Product::findOrFail($data['product_id'])->name;
                } elseif (isset($data['entity_id'])) {
                    return SharesEntity::findOrFail($data['entity_id'])->name;
                } elseif (isset($data['unit_fund_id'])) {
                    return UnitFund::findOrFail($data['unit_fund_id'])->name;
                } else {
                    return "None";
                }
            // no break
            case "client_name":
                if (!$this->approval->client) {
                    return $data['received_from'] . ' (Not onboarded)';
                }
                return null;
            case 'client_amount':
                return (float)$data['exchange_rate'] * (float)$data['amount'];
        }
    }
}
