<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeeAmount;
use App\Cytonn\Models\Unitization\UnitFundFeeChargeFrequency;
use App\Cytonn\Models\Unitization\UnitFundFeeChargeType;
use App\Cytonn\Models\Unitization\UnitFundFeeParameter;
use App\Cytonn\Models\Unitization\UnitFundFeeParameterPercentage;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateUnitFundFee implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files', 'fee_id']);

        $frequency = (isset($input['fee_charge_frequency_id']))
            ? $input['fee_charge_frequency_id']
            : null;

        $fee = UnitFundFee::findOrFail($data['fee_id']);

        $fee->update(
            [
                'unit_fund_fee_type_id' => $input['unit_fund_fee_type_id'],
                'dependent' => $input['dependent'],
                'date' => $input['date'],
                'fee_charge_type_id' => $input['fee_charge_type_id'],
                'fee_charge_frequency_id' => $frequency,
            ]
        );

        $fee->dependent ? $this->saveParamAndPercentage($fee, $input) : $this->saveAmount($fee, $input);



        \Flash::success('Unit fund fee has been successfully updated');

        $next = \Redirect::to($fee->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $fee = UnitFundFee::findOrFail($data['fee_id']);

        $type = UnitFundFeeType::findOrFail($data['unit_fund_fee_type_id']);

        $chargeType = null;

        if (isset($data['fee_charge_type_id'])) {
            $chargeType = UnitFundFeeChargeType::find($data['fee_charge_type_id'])->name;
        }

        $frequency = (isset($data['fee_charge_frequency_id']))
            ? UnitFundFeeChargeFrequency::where('id', $data['fee_charge_frequency_id'])->pluck('name')[0]
            : null;

        return ['data' => $data, 'fee' => $fee, 'fund' => $fund,
            'type' => $type, 'chargeType' => $chargeType, 'frequency' => $frequency];
    }


    /**
     * @param UnitFundFee $fee
     * @param array $input
     */
    private function saveParamAndPercentage(UnitFundFee $fee, array $input)
    {
        $parameter = UnitFundFeeParameter::findOrFail($input['unit_fund_fee_parameter_id']);

        $percentage = new UnitFundFeeParameterPercentage();
        $percentage->parameter()->associate($parameter);
        $percentage->fee()->associate($fee);
        $percentage->percentage = $input['percentage'];

        $percentage->save();
    }

    private function saveAmount(UnitFundFee $fee, array $input)
    {
        $amount = new UnitFundFeeAmount();
        $amount->fee()->associate($fee);
        $amount->amount = $input['amount'];

        $amount->save();
    }
}
