<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\BankInstruction;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use App\Cytonn\Models\User;

class UpdatePaymentInstruction implements ApprovalHandlerInterface
{

    /**
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $instruction = BankInstruction::findOrFail($data['instruction_id']);

        // Check they exists
        $first_signatory = User::findOrFail($data['first_signatory_id']);
        $second_signatory = User::findOrFail($data['second_signatory_id']);

        $instruction->first_signatory_id = $first_signatory->id;
        $instruction->second_signatory_id = $second_signatory->id;

        $instruction->save();



        \Flash::success('The signatories for the payment instruction have been successfully updated.');

        $next = \redirect('/dashboard/investments/payment-instructions/show/'.$instruction->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $instruction = BankInstruction::findOrFail($data['instruction_id']);
        $payment = $instruction->custodialTransaction->clientPayment;
        $client = $payment->client;
        $users = function ($id) {
            return User::findOrFail($id);
        };

        return ['client'=>$client, 'data'=>$data, 'instruction'=>$instruction, 'payment'=>$payment, 'users'=>$users];
    }
}
