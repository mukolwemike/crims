<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

/**
 * Class ReverseSharesPurchaseFromEntity
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class ReverseSharesPurchaseFromEntity implements ApprovalHandlerInterface
{

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $order = SharesPurchaseOrder::findOrFail($data['order_id']);

        $sharePurchase = $order->sharePurchases()->first();


        $shareSale = $order->shareSales()->first();

        $payment = $sharePurchase->clientPayment;

        // Delete client credit record - return cash to share holder/client
        $payment->delete();

        // Delete share purchases record - take shares from share holder
        $sharePurchase->delete();

        // Delete shares sales record - return shares to entity
        $shareSale->delete();

        $order->matched = false;
        $order->save();



        \Flash::success('Shares purchase has been successfully reversed to entity!');
        $next = \redirect('/dashboard/shares/purchases/show/' . $order->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $order = SharesPurchaseOrder::findOrFail($data['order_id']);
        $buyer = $order->buyer;
        $bought = SharePurchases::where('shares_purchase_order_id', $order->id)
            ->sum('number');
        $sales = SharesSalesOrder::where('account_id', $order->account_id)
            ->where('matched', null)->get();

        return ['data' => $data, 'order' => $order, 'buyer' => $buyer, 'bought' => $bought, 'sales' => $sales];
    }
}
