<?php
/**
 * Date: 30/04/2016
 * Time: 10:49 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\CorporateInvestorType;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\EmailIndemnity;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Gender;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\USSD\USSDRepository;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\BooleanPresenter;
use Laracasts\Commander\CommanderTrait;

class Application implements ApprovalHandlerInterface
{
    use CommanderTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        $app = ClientInvestmentApplication::findOrFail($approval->payload['application_id']);

        $is_duplicate = ($app->client) ? $app->client->repo->duplicate($app->toArray())->exists : false;

        $new = (int)$app->new_client;

        $allow_duplicate = ($new == 3 || $new == 2);

        if ((!$allow_duplicate) && $is_duplicate) {
            throw new
            ClientInvestmentException(
                "This client already exists, you can only add the client as a duplicate"
            );
        }

        $application = $this->processApplication($app);

        $this->saveContactPersons($application);

        if ($application->clientType->name !== 'corporate') {
            $this->saveJointHolders($application);
        }

        $this->updateDocuments($application);

        $this->investmentInstruction($app, $approval);

        if ($app->unitFund) {
            if ($app->unitFund->short_name == 'CMMF' && $app->client) {
                (new USSDRepository())->createMpesaAccount($app->client);
            }
        }



        \Flash::success('Application has been approved');

        $next = \redirect('/dashboard/investments/client-instructions');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function updateDocuments(ClientInvestmentApplication $application)
    {
        $this->saveKyc($application, 'kyc');

        $this->saveKyc($application, 'payment');

        $this->saveEmailIndemnity($application);

        $this->saveClientSignature($application);

        return;
    }

    public function saveKyc(ClientInvestmentApplication $application, $type)
    {
        return $application->form->clientRepo->getDocumentDetails($type)
            ->each(function ($doc) use ($application, $type) {

                $document = Document::findOrFail($doc['document_id']);

                $kyc = new ClientUploadedKyc();

                $kyc->application_id = $application->form->id;

                $kyc->client_id = $application->client_id;

                $kyc->document_id = $document->id;

                $kyc->filename = $document->filename;

                $doc['doc_type'] = !is_null($doc['doc_type'])? $doc['doc_type'] : $doc['slug'];
                
                $kyc->kyc_id = ClientComplianceChecklist::where('slug', $doc['doc_type'])->first()->id;

                $kyc->save();
            });
    }

    public function saveEmailIndemnity(ClientInvestmentApplication $application)
    {
        return $application->form->clientRepo->getDocumentDetails('email_indemnity')
            ->each(function ($doc) use ($application) {
                $indemnity = new EmailIndemnity();

                $indemnity->email = $doc['email'];

                $indemnity->document_id = $doc['document_id'];

                $indemnity->client_id = $application->client_id;

                $indemnity->save();
            });
    }

    public function saveClientSignature(ClientInvestmentApplication $application)
    {
        return $application->form->clientRepo->getDocumentDetails('client_signatures')
            ->each(function ($doc) use ($application) {
                $signature = new ClientSignature();

                $signature->client_id = $application->client_id;

                $signature->document_id = $doc['document_id'];

                $signature->joint_client_holder_id = $doc['joint_client_holder_id'];

                $signature->save();
            });
    }

    /**
     * @param ClientInvestmentApplication $application
     * @return ClientInvestmentApplication
     * @throws \Exception
     */
    protected function processApplication(ClientInvestmentApplication $application)
    {
        $input = $application->toArray();

        $type = $application->clientType->name;

        $contact = $application->repo->saveContactFromApplicationForm($type, $input);

        $client = $application->repo->saveClientFromApplicationForm($type, $contact, $input);

        $application->client_id = $client->id;

        $application->save();

        return $application;
    }

    public function saveContactPersons(ClientInvestmentApplication $application)
    {
        $client = Client::findOrFail($application->client_id);

        if ($application->clientType->name == 'individual') {
            $application->repo->saveContactPersonFromApplication($client, $application);
        } elseif ($application->clientType->name == 'corporate') {
            $application->repo->saveContactPersonFromFilledContactPerson($client, $application);
        } else {
            $client = Client::findOrFail($client->id);

            $cps = $application->contactPersons;

            $client->contactPersons()->createMany($cps ? $cps : []);
        }

        return $application;
    }

    public function saveJointHolders(ClientInvestmentApplication $application)
    {
        $jointHolders = $application->form->jointHolders;

        $application->repo->addJointHolders($jointHolders);

        return $application;
    }

    public function investmentInstruction(ClientInvestmentApplication $app, ClientTransactionApproval $approval)
    {
        if ($app->unitFund) {
            return $this->unitFundPurchase($app);
        } elseif ($app->product) {
            return $this->investmentTopup($approval);
        } else {
            throw new ClientInvestmentException("No product specified");
        }
    }

    protected function investmentTopup(ClientTransactionApproval $approval)
    {
        $application = ClientInvestmentApplication::findOrFail($approval->payload['application_id']);

        $topup = ClientTopupForm::create([
            'client_id' => $application->client_id,
            'user_id' => null,
            'amount' => $application->amount,
            'agreed_rate' => $application->agreed_rate,
            'product_id' => $application->product_id,
            'tenor' => $application->tenor,
        ]);

        return $topup;
    }

    protected function unitFundPurchase(ClientInvestmentApplication $app)
    {
        $client = Client::findOrFail($app->client_id);

        $input = [
            'amount' => $app->amount,
            'date' => Carbon::now(),
            'client_id' => $client->id,
            'unit_fund_id' => $app->unitFund->id
        ];

        $instruction = new UnitFundInvestmentInstruction();

        return $instruction->repo->purchase($input, $client, $app->unitFund);
    }

    public function prepareView(ClientTransactionApproval $approval, array $data = null)
    {
        $app = $application = ClientInvestmentApplication::findOrFail($approval->payload['application_id']);

        $client = $app->form ? $app->form->client : $app->client;

        $app->client_id = $client ? $client->id : null;

        $invested = ClientInvestment::where('application_id', $application->id)->first();

        $product = $app->product;

        $unitFund = $app->unitFund;

//        $fundsource = FundSource::find($app->funds_source_id);

        $corporateinvestortype = CorporateInvestorType::find($application->corporate_investor_type);

        $title = Title::find($application->contact_person_title);

        $duplicate = ($client) ? $client->repo->duplicate($application->toArray()) : null;

        if (array_key_exists('edited_application', $approval->payload)) {
            $editedApplication = $approval->payload;

            $editedProduct = Product::find($editedApplication['product_id']);

            $editedFundSource = FundSource::find($editedApplication['funds_source_id']);
        } else {
            $editedApplication = $editedProduct = $editedFundSource = null;
        }

        $investortype = $application->corporate_investor_type ?
            CorporateInvestorType::findOrFail($application->corporate_investor_type)
            : null;

        $fundsource = FundSource::find($application->funds_source_id);

        $app->individual = $app->form ? $app->form->individual : $app->clientType->name == 'individual';

        $clientTitles = Title::all()->lists('name', 'id');

        $titles = Title::find($application->title);

        $genders = Gender::all()->lists('abbr', 'id');

        $countries = Country::all()->lists('name', 'id');

        $clientRepo = new ClientApplicationRepository($app->form);

        return [
            'client' => $client,
            'jointHolders' => $app->form ? $app->form->jointHolders : $app->jointHolders,
            'app' => $app,
            'application' => $app,
            'invested' => $invested,
            'product' => $product,
            'fundsource' => $fundsource,
            'corporateinvestortype' => $corporateinvestortype,
            'client_title' => $title,
            'duplicate' => $duplicate,
            'editedApplication' => $editedApplication,
            'editedProduct' => $editedProduct,
            'editedFundSource' => $editedFundSource,
            'contactPersons' => $app->form ? $app->form->contactPersons : $app->contactPersons,
            'unitFund' => $unitFund,
            'investortype' => $investortype,
            'titles' => $titles,
            'clientTitles' => $clientTitles,
            'gender' => $genders,
            'countries' => $countries,
            'kycDocuments' => $clientRepo->getDocumentDetails('kyc'),
            'paymentDocuments' => $clientRepo->getDocumentDetails('payment'),
            'emailIndemnity' => $clientRepo->getDocumentDetails('email_indemnity'),
            'clientSignature' => $clientRepo->getDocumentDetails('client_signatures'),
            'riskyClient' => (new ClientApplicationRepository())->getRiskyClientDetails($application->toArray())
        ];
    }
}
