<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\Employment;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Title;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientRepository;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Shares\ShareHolderRepository;

/**
 * Class RegisterShareholder
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class RegisterShareholder implements ApprovalHandlerInterface
{
    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $entity = SharesEntity::findOrFail($data['entity_id']);
        $data['fund_manager_id'] = $entity->fund_manager_id;

        if (!isset($data['client_id']) or empty($data['client_id'])) {
            $type = $data['shareholder_type'];
            if (!in_array($type, ['individual', 'corporate'])) {
                throw new ClientInvestmentException(
                    'The application type can only be individual or corporate'
                );
            }

            $client = $type == 'individual' ?
                $this->createIndividualClient($data, $entity)
                : $this->createCorporateClient($data, $entity);

            // Save the bank branch
            if (isset($data['branch_id'])) {
                $bankAccount = new ClientBankAccount(
                    [
                        'branch_id' => $data['branch_id'],
                        'account_name' => $data['investor_account_name'],
                        'account_number' => $data['investor_account_number'],
                        'default' => 1
                    ]
                );
                $client->bankAccounts()->save($bankAccount);
            }
        } else {
            $client = Client::find($data['client_id']);
        }

        $repo = new ShareHolderRepository();

        $holder = $repo->registerShareholder($client, $data);

        if (array_key_exists('holders', $data) && $data['holders'] != null) {
            $this->addJointHolders($data, $holder, $client);
        }



        \Flash::success('Shareholder registration was successful!');

        $next = \Redirect::to('/dashboard/shares/shareholders/show/' . $holder->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $duplicate = ($data['new_client'] == 3) ? (new ClientRepository())->duplicate($data) : null;

        $riskyClient = (new ClientApplicationRepository())->getRiskyClientDetails($data);

        $jointHolders = (isset($data['holders'])) ? $data['holders'] : null;

        $client = '';

        if (isset($data['client_id']) and !empty($data['client_id'])) {
            $client = Client::findOrFail($data['client_id']);
            $contact = $client->contact;
            $data = array_only($data, ['entity_id', 'number', 'shareholder_type']) +
                $client->toArray() + $contact->toArray();
            $data['registered_name'] = $data['corporate_registered_name'];
            $data['trade_name'] = $data['corporate_trade_name'];
            unset($data['corporate_registered_name']);
            unset($data['corporate_trade_name']);

            $registered_name = $data['registered_name'];

            $share_holder_title = isNotEmptyOrNull($data['title_id']) ? Title::find($data['title_id'])->name : '';
        } else {
            $registered_name = ($data['individual'] == 2) ? $data['registered_name'] : null;

            if ($data['individual'] != 2) {
                $title = ($data['new_client'] == 2) ? null : Title::find($data['title'])->name;
            }

            $share_holder_title = ($data['individual'] != 2) ? $title : '';
        }

        $shareentity = SharesEntity::findOrFail($data['entity_id']);

        $employment = isset($data['employment_id']) ? Employment::find($data['employment_id']) : new Employment();

        $branch = isset($data['branch_id']) ? ClientBankBranch::find($data['branch_id']) : new ClientBankBranch();

        if (!isset($data['new_client'])) {
            $data['new_client'] = 2;
        }

        $data['type'] = $approval->transaction_type;

        $documentData = ['processType' => $data['type'], 'application' => $approval->id];

        $kycDocs = (new ClientApplicationRepository())->getDocumentDetails('kyc', $documentData);

        return [
            'client' => $client,
            'data' => $data,
            'share_holder_title' => $share_holder_title,
            'shareentity' => $shareentity,
            'employment' => $employment,
            'branch' => $branch,
            'holders' => $jointHolders,
            'duplicate' => $duplicate,
            'riskyClient' => $riskyClient,
            'registered_name' => $registered_name,
            'kycDocuments' => $kycDocs
        ];
    }

    /**
     * @param array $data
     * @return Client
     */
    public function createIndividualClient(array $data, SharesEntity $entity)
    {
        //create contact
        $contact = $this->createClientContact($data, 'individual');

        //create client
        $client = new Client();

        $client->fill(
            [
                'contact_id' => $contact->id,
                'client_type_id' => ClientType::where('name', 'individual')->first()->id,
                'fund_manager_id' => $data['fund_manager_id'],
            //                'client_code' => $client_code,
                'dob' => isset($data['dob']) ? $data['dob'] : null,
                'phone' => isset($data['telephone_home']) ? $data['telephone_home'] : $data['telephone_office'],
                'telephone_office' => isset($data['telephone_office']) ? $data['telephone_office'] : null,
                'telephone_home' => isset($data['telephone_home']) ? $data['telephone_home'] : null,
                'pin_no' => isset($data['pin_no']) ? $data['pin_no'] : '',
                'id_or_passport' => isset($data['id_or_passport']) ? $data['id_or_passport'] : '',
                'postal_code' => isset($data['postal_code']) ? $data['postal_code'] : '',
                'postal_address' => isset($data['postal_address']) ? $data['postal_address'] : '',
                'town' => isset($data['town']) ? $data['town'] : '',
                'street' => (isset($data['street'])) ? $data['street'] : null,
                'country_id' => isset($data['country_id']) ? $data['country_id'] : 114,
                'method_of_contact_id' => $data['method_of_contact_id'],
                'business_sector' => isset($data['business_sector']) ? $data['business_sector'] : null,
                'investor_account_name' =>
                    (isset($data['investor_account_name'])) ? $data['investor_account_name'] : null,
                'investor_account_number' =>
                    (isset($data['investor_account_number'])) ? $data['investor_account_number'] : null,
                //            'investor_bank'             =>  $data['investor_bank'],
                //            'investor_bank_branch'      =>  $data['investor_bank_branch'],
                //            'investor_clearing_code'    =>  $data['investor_clearing_code'],
                //            'investor_swift_code'       =>  $data['investor_swift_code'],
            ]
        );
        $client->save();

        (new ApplicationsRepository())->setClientCode($client);

        return $client;
    }

    public function createCorporateClient(array $data, SharesEntity $entity)
    {
        //create contact
        $contact = $this->createClientContact($data, 'corporate');

        //create client
        $client = new Client();
        $client->fill(
            [
                'contact_id' => $contact->id,
                'client_type_id' => ClientType::where('name', 'corporate')->first()->id,
                'fund_manager_id' => $data['fund_manager_id'],
                'client_code' => $entity->code . '-' . $data['number'],
                'phone' => ($data['individual'] == 2) ? $data['telephone_office'] : $data['phone'],
                'pin_no' => $data['pin_no'],
                'postal_code' => $data['postal_code'],
                'postal_address' => ($data['individual'] == 2) ? $data['street'] : $data['postal_address'],
                'town' => $data['town'],
                'street' => $data['street'],
                'country_id' => $data['country_id'],
                'taxable' => isset($data['taxable']) ? $data['taxable'] : 0,
                'corporate_investor_type' =>
                    isset($data['corporate_investor_type']) ? $data['corporate_investor_type'] : null,
                'corporate_investor_type_other' =>
                    (isset($data['corporate_investor_type_other'])) ? $data['corporate_investor_type_other'] : null,
                'contact_person_title' =>
                    isset($data['contact_person_title']) ? $data['contact_person_title'] : null,
                'contact_person_lname' =>
                    isset($data['contact_person_lname']) ? $data['contact_person_lname'] : null,
                'contact_person_fname' =>
                    isset($data['contact_person_fname']) ? $data['contact_person_fname'] : null,
                'method_of_contact_id' =>
                    isset($data['method_of_contact_id']) ? $data['method_of_contact_id'] : null,
                'investor_account_name' =>
                    isset($data['investor_account_name']) ? $data['investor_account_name'] : null,
                'investor_account_number' =>
                    isset($data['investor_account_number']) ? $data['investor_account_number'] : null,
                //            'investor_bank'             =>  $data['investor_bank'],
                //            'investor_bank_branch'      =>  $data['investor_bank_branch'],
                //            'investor_clearing_code'    =>  $data['investor_clearing_code'],
                //            'investor_swift_code'       =>  $data['investor_swift_code'],
            ]
        );
        $client->save();

        return $client;
    }

    /**
     * @param array $data
     * @param $entity_type
     * @return Contact
     */
    private function createClientContact(array $data, $entity_type)
    {
        $contact = new Contact();
        $contact->fill(
            [
                'title_id' => ($data['individual'] != 2)
                    ? (isset($data['title']) ? $data['title'] : $data['contact_person_title']) : null,
                'firstname' => ($data['individual'] != 2)
                    ? (isset($data['firstname']) ? $data['firstname'] : $data['contact_person_fname']) : null,
                'lastname' => ($data['individual'] != 2)
                    ? (isset($data['lastname']) ? $data['lastname'] : $data['contact_person_lname']) : null,
                'middlename' => ($data['individual'] != 2)
                    ? (isset($data['middlename']) ? $data['middlename'] : null) : null,
                'entity_type_id' => ClientType::where('name', $entity_type)->first()->id,
                'email' => $data['email'],
                'phone' => isset($data['telephone_home']) ? $data['telephone_home'] : $data['telephone_office'],
                'country_id' => isset($data['country_id']) ? $data['country_id'] : 114,
                'registration_number' => isset($data['registration_number']) ? $data['registration_number'] : null,
                'registered_address' => isset($data['registered_address']) ? $data['registered_address'] : null,
                'corporate_trade_name' => isset($data['trade_name']) ? $data['trade_name'] : null,
                'corporate_registered_name' => isset($data['registered_name']) ? $data['registered_name'] : null,
            ]
        );
        $contact->save();
        return $contact;
    }

    /**
     * @param $data
     * @param $repo
     * @param $holder
     * @param $client
     */
    public function addJointHolders($data, $holder, $client)
    {
        $holders = collect($data['holders']);

        $holders->each(function ($joint) use ($holder, $client) {
            (new ShareHolderRepository())->saveJointApplicant($joint, $holder->id, $client->id);
        });
    }
}
