<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/28/18
 * Time: 12:52 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;

class AssignUserClientSignature implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $user = ClientUser::findOrFail($data['user_id']);

        $signature = ClientSignature::findOrFail($data['client_signature_id']);

        $user->clientSignatures()->save($signature, [ 'active' => true ]);



        \Flash::success('Client signature has been assigned');

        $next = \Redirect::to('/dashboard/users/client/details/'.$user->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $user = ClientUser::findOrFail($data['user_id']);

        $signature = ClientSignature::findOrFail($data['client_signature_id']);

        $client = $signature->client;

        return [
            'data' => $data,
            'user' => $user,
            'signature' => $signature,
            'client' => $client
        ];
    }
}
