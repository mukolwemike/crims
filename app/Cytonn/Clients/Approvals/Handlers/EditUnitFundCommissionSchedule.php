<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 14/02/2019
 * Time: 16:28
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class EditUnitFundCommissionSchedule implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $schedule = UnitFundCommissionSchedule::findOrFail($data['schedule_id']);
        $schedule->date = $data['schedule_date'];
        $schedule->save();

        \Flash::success('The commission payment schedule has been updated');

        $next = \redirect("/dashboard/investments/approve");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedule = UnitFundCommissionSchedule::findOrFail($data['schedule_id']);

        return ['schedule' => $schedule, 'data' => $data];
    }
}
