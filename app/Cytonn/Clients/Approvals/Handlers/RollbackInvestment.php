<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/16/16
 * Time: 12:24 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Reverse;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class RollbackInvestment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * RollbackInvestment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $investment = ClientInvestment::find($approval->payload['investment_id']);

        if ($investment->withdrawn) {
            throw new ClientInvestmentException(
                'You cannot rollback an investment that has been withdrawn, reverse the withdrawal first'
            );
        }

        if (is_null($investment)) {
            throw new ClientInvestmentException(
                'The investment does not exist, it may have already been rolled back'
            );
        }

        $reverse = new Reverse();

        $reverse->investment($investment);

        \Flash::success('The investment has been rolled back');

        $next = \Redirect::to('/dashboard/investments');

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }


    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $investment = ClientInvestment::withTrashed()->find($data['investment_id']);

        $found = true;
        if (is_null($investment)) {
            $found = false;
        }
        return ['investment' => $investment, 'found' => $found, 'data' => $data];
    }
}
