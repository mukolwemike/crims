<?php
/**
 * Date: 23/06/2016
 * Time: 11:09 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUserAccess;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class RevokeUserAccount implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $access = ClientUserAccess::findOrFail($approval->payload['access_id']);
        if ($access->active) {
            $access->active = false;
            $access->save();

            \Flash::success('Access has been revoked');
        } else {
            \Flash::message('Access was already revoked.');
        }

        $next = \Redirect::to('/dashboard/users/client/details/'.$access->user->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $access = ClientUserAccess::findOrFail($approval->payload['access_id']);

        return ['user'=>$access->user];
    }
}
