<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Shares\ShareTradingRepository;

/**
 * Class MakeSharesSalesOrder
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class MakeSharesSalesOrder implements ApprovalHandlerInterface
{
    protected $repo;

    /**
     * MakeSharesSalesOrder constructor.
     */
    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $seller = ShareHolder::findOrFail($data['seller_id']);

        if ($data['number'] > $seller->currentShares(new Carbon($data['request_date']))) {
            \Flash::error('The number of shares to be sold cannot be more than those owned.');

            return \Redirect::back();
        }

        $data['approval_id'] = $approval->id;

        $this->repo->makeSalesOrder($data);



        \Flash::success('Shares sales order was successfully made!');

        $next = \redirect('/dashboard/shares/sales/shareholders/' . $seller->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $seller = ShareHolder::findOrFail($data['seller_id']);

        return ['data' => $data, 'seller' => $seller];
    }
}
