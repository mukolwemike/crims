<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\Document;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class UploadComplianceForm implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $document = Document::findOrFail($data['document_id']);

        $filename = $data['file_name'];

        $kycs = ClientComplianceChecklist::whereIn('id', array_flatten($data['kycs']))->get();

        foreach ($kycs as $kyc) {
            $upload =  new ClientUploadedKyc();
            $upload->document_id = $document->id;
            $upload->kyc_id = $kyc->id;
            $upload->filename = $filename;
            $upload->client_id = $client->id;
            $upload->user_id = Auth::user()->id;
            $upload->save();
        }

        $document->approval_id = $approval->id;
        $document->save();



        $next = \redirect('/dashboard/investments/compliance/' . $approval->client_id);

        \Flash::success('The compliance form has been approved and saved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $document = Document::findOrFail($data['document_id']);

        $client = Client::findOrFail($approval->client_id);

        $kycs = ClientComplianceChecklist::whereIn('id', array_flatten($data['kycs']))->get();

        return ['document'=>$document, 'kycs'=>$kycs, 'client'=>$client];
    }
}
