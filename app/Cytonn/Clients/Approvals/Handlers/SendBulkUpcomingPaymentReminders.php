<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Mailers\RealEstate\UpcomingRealEstatePaymentRemindersForClientsMailer;
use Dompdf\Exception;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class SendBulkUpcomingPaymentReminders implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;


    /**
     * @param ClientTransactionApproval $approval
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Exception
     * @return null
     */
    public function handle(ClientTransactionApproval $approval)
    {
        \Queue::push(static::class.'@process', ['id'=>$approval->id, 'user_id'=>$approval]);

        \Flash::success('Upcoming payments reminders have been approved in bulk.');
        \Flash::message('Upcoming payments have been sent to clients.');

        $next = \Redirect::to('/dashboard/realestate/payments-schedules');
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function process($job, $data)
    {
        $approval = ClientTransactionApproval::findOrFail($data['id']);

        $schedules = RealEstatePaymentSchedule::whereIn('id', $approval->payload['payment_schedules'])
            ->whereNotIn('id', $approval->payload['removed'])
            ->get()
            ->unique('id')
            ->each(
                function (RealEstatePaymentSchedule $schedule) {
                    $schedule->amount_pending = $schedule->overduePayments();
                }
            )
            ->filter(
                function (RealEstatePaymentSchedule $schedule) {
                    return $schedule->amount_pending > 0;
                }
            )
            ->each(
                function (RealEstatePaymentSchedule $schedule) {
                    $schedule->client_id = (string) $schedule->holding->client_id;
                    $schedule->amount_payable = $schedule->amount_pending;

                    if ($schedule->amount_pending > $schedule->amount) {
                        $schedule->amount_payable = $schedule->amount;
                    }
                }
            )->groupBy('client_id');

        $collection = [];
        foreach ($schedules as $client => $schedules_arr) {
            $client = Client::findOrFail($client);
            $projects = Collection::make($schedules_arr)->map(
                function ($schedule) {
                    return $schedule->holding->project;
                }
            )->unique('id');

            array_push($collection, ['client' => $client, 'projects' => $projects, 'schedules' => $schedules_arr]);
        }

        foreach ($collection as $data) {
            (new UpcomingRealEstatePaymentRemindersForClientsMailer())->sendEmail(
                $data['schedules'],
                $data['client'],
                $approval->payload['start_date'],
                $approval->payload['end_date'],
                $approval->sender,
                $data['projects']
            );
        }

        $job->delete();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedules = RealEstatePaymentSchedule::whereIn('id', $data['payment_schedules'])->get();

        $schedules = $schedules->each(
            function (RealEstatePaymentSchedule $schedule) {
                $schedule->amount_pending = $schedule->overduePayments();
            }
        )->filter(
            function (RealEstatePaymentSchedule $schedule) {
                return $schedule->amount_pending > 0;
            }
        );

        $project_id = !isset($data['project_id']) ? null : $data['project_id'];
        
        return['schedules'=>$schedules, 'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'], 'approval_id'=>$approval->id, 'project_id'=>$project_id];
    }

    public function action(ClientTransactionApproval $approval, $action, $param)
    {
        switch ($action) {
            case 'remove':
                $payload = $approval->payload;
                $payload['removed'][] = $param;

                $approval->payload = $payload;
                $approval->save();

                \Flash::message('Successfully excluded');

                return \Redirect::back();
        }

        return App::abort(404);
    }
}
