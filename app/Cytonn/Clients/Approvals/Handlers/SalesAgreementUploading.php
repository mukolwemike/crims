<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstateSalesAgreement;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Realestate\Events\SalesAgreementHasBeenSigned;
use Cytonn\Realestate\Events\SalesAgreementHasBeenUploaded;
use Illuminate\Support\Facades\Response;

class SalesAgreementUploading implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $document_id = null;

        $data = $approval->payload;

        if (isset($approval->payload['document_id'])) {
            $document_id = $approval->payload['document_id'];
        }

        $salesAgreement = RealEstateSalesAgreement::findOrFail($approval->payload['sales_agreement_id']);

        //Check for real estate guards
        $salesAgreement->holding->client->repo->checkReGaurds($approval);

        $salesAgreement = $this->saveHistory($salesAgreement, $document_id);

        $salesAgreement = $this->saveSigned($salesAgreement, $data);

        $salesAgreement->dispatchEventsFor($salesAgreement);

        $next = \redirect('/dashboard/realestate/projects/' . $salesAgreement->holding->project->id .
            '/units/show/' . $salesAgreement->holding_id);

        \Flash::success('The sales agreement details have been saved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    protected function saveHistory(RealEstateSalesAgreement $salesAgreement, $document_id)
    {
        if ($document_id) {
            $old_doc = $salesAgreement->document;

            if ($old_doc) {
                $salesAgreement->addHistory($old_doc);
            }

            $salesAgreement->save();

            $salesAgreement->update(
                [
                    'document_id' => $document_id,
                    'uploaded_on' => Carbon::now()
                ]
            );

            $salesAgreement->raise(new SalesAgreementHasBeenUploaded($salesAgreement));
        }

        return $salesAgreement;
    }

    protected function saveSigned(RealEstateSalesAgreement $salesAgreement, array $data)
    {
        if (isset($data['date_signed'])) {
            $salesAgreement->update(
                [
                    'date_signed' => $data['date_signed'],
                    'date_received' => Carbon::today()
                ]
            );

            $salesAgreement->raise(new SalesAgreementHasBeenSigned($salesAgreement));
        }

        return $salesAgreement;
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $salesAgreement = RealEstateSalesAgreement::findOrFail($data['sales_agreement_id']);
        $project = $salesAgreement->holding->project;
        $unit = $salesAgreement->holding->unit;
        $client = $salesAgreement->holding->client;

        return ['project' => $project, 'unit' => $unit, 'client' => $client, 'salesAgreement' => $salesAgreement];
    }
}
