<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\AdvocateProject;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateAdvocate implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['project_ids', '_token', 'files']);

        $advocate = new Advocate();
        $advocate->fill($input);
        $advocate->save();

        Project::whereIn('id', $data['project_ids'])
            ->get()
            ->each(
                function ($project) use ($advocate) {
                    AdvocateProject::create(
                        [
                        'advocate_id'   =>  $advocate->id,
                        'project_id'    =>  $project->id,
                        ]
                    );
                }
            );



        \Flash::success('Advocate has been successfully saved');

        $next = \Redirect::to('/dashboard/realestate/advocates');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        return ['data'=>$data];
    }
}
