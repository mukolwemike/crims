<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionOverrideStructure;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFundCommissionPayment;
use App\Cytonn\Models\User;
use Crims\Commission\Override;
use Crims\Unitization\Commission\RecipientCalculator;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Unitization\Commissions\Payment;
use Cytonn\Unitization\Commissions\Recipients;
use Illuminate\Support\Facades\DB;

class BulkUnitizationCommissionPayments implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        throw new ClientInvestmentException('This approval function is disabled. Please used the combined approval');

        \Queue::push(static::class . '@makePayments', ['approval_id' => $approval->id]);

        $next = \Redirect::back();

        \Flash::message('Unitization commissions have been successfully approved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }


    public function makePayments($job, $queueData)
    {
        $approval = ClientTransactionApproval::findOrFail($queueData['approval_id']);
        $data = $approval->payload;

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        DB::transaction(
            function () use ($bulk, $approval) {
                CommissionRecepient::get()
                    ->each(
                        function ($recipient) use ($bulk, $approval) {
                            (new Payment($recipient))->make($bulk, $approval);
                        }
                    );
            }
        );

        $job->delete();
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);
        $recipients = (new Recipients())->forDates($bulk->start, $bulk->end)->paginate('10');

        $recipients->each(
            function ($recipient) use ($approval, $bulk) {
                $recipient->amount_payable = !$approval->approved ?
                    $recipient->amount_payable = (new RecipientCalculator($recipient, $bulk->start, $bulk->end))
                        ->summary()->total() :
                    $recipient->amount_payable = $this->getPaymentsMade($recipient, $approval);
            }
        );

        return ['recipients' => $recipients, 'bulk' => $bulk];
    }

    private function getPaymentsMade(CommissionRecepient $recipient, ClientTransactionApproval $approval)
    {
        return UnitFundCommissionPayment::where('commission_recipient_id', $recipient->id)
            ->where('client_transaction_approval_id', $approval->id)
            ->sum('amount');
    }

    public function action(ClientTransactionApproval $approval, $actionName, $params)
    {
        switch ($actionName) {
            case 'excel':
                $bulk = BulkCommissionPayment::find($approval->payload['bcp_id']);
                $recipients = (new Recipients())->forDates($bulk->start, $bulk->end)
                    ->get();

                if (!$recipients->count()) {
                    \Flash::error('There are no FAs in the the list!');
                    return \Redirect::back();
                }

                \Queue::push(
                    static::class . '@exportJob',
                    ['approval_id' => $approval->id, 'user_id' => auth()->id()]
                );
                \Flash::message('The excel report is being processed, it will be sent to your email when complete');

                return \Redirect::back();
                break;
            default:
                return \App::abort(404);
        }
    }

    /**
     * @param $job
     * @param $data
     */
    public function exportJob($job, $data)
    {
        $this->exportDetails($data['approval_id'], $data['user_id']);

        $job->delete();
    }

    public function exportDetails($approvalId, $userId)
    {
        ini_set('memory_limit', '512M');

        ini_set('max_execution_time', 300);

        $approval = ClientTransactionApproval::findOrFail($approvalId);

        $user = User::find($userId);

        $data = $approval->payload;

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        $data['bulk'] = $bulk;

        $filename = 'Unitization Commissions';

        $data['calculate'] = function (CommissionRecepient $recipient) use ($approval, $bulk) {
            if (!$approval->approved) {
                return (new RecipientCalculator($recipient, $bulk->start, $bulk->end))->summary()->finalCommission();
            } else {
                return $this->getPaymentsMade($recipient, $approval);
            }
        };

        $overrideStructure = CommissionOverrideStructure::where('slug', 'unitization')
            ->where('date', '<=', $bulk->end)
            ->latest('date')
            ->first();

        $models = function ($type) use ($data) {
            $dataArray = array();

            $recipients = CommissionRecepient::active()->where('recipient_type_id', $type->id)->get();

            foreach ($recipients as $recipient) {
                $total = $data['calculate']($recipient);

                if ($total != 0) {
                    $r = new EmptyModel();
                    $r->{'Employee ID'} = $recipient->employee_id;
                    $r->{'Employee Number'} = $recipient->employee_number;
                    $r->Name = $recipient->name;
                    $r->{'Active'} = BooleanPresenter::presentYesNo($recipient->active);
                    if ($type->slug == 'ifa') {
                        $r->{'Account Number'} = $recipient->account_number;
                        $r->{'KRA Pin'} = $recipient->kra_pin;
                        $r->{'Bank Code'} = $recipient->bank_code;
                        $r->{'Branch Code'} = $recipient->branch_code;
                    }
                    $calc = $data['calculate'];
                    $r->Amount = $calc($recipient);

                    $dataArray[] = $r;
                }
            }

            return collect($dataArray);
        };

        \Excel::create(
            $filename,
            function ($excel) use ($models, $data, $overrideStructure) {
                foreach (CommissionRecipientType::all() as $type) {
                    $excel->sheet(
                        $type->name,
                        function ($sheet) use ($models, $type) {
                            $sheet->fromModel($models($type));
                        }
                    );
                }

                // Compliant Commission
                foreach (CommissionRecepient::active()->get() as $recipient) {
                    $bulk = $data['bulk'];

                    $calculator = $recipient->unitizationCommissionCalculator($bulk->start, $bulk->end);

                    $overrideRate = $this->getOverrideRate($recipient, $overrideStructure);

                    $compliant = $calculator->schedules()->orderBy('date')->get();

                    $recipientSummary = $calculator->summary();

                    $reports = $calculator->getReports()->each(function ($fa) use ($recipientSummary, $overrideRate) {
                        $fa->commission = $recipientSummary->overrideCommission($fa);

                        $fa->override = $recipientSummary->overrideOnRecipient($fa);

                        $fa->rate = $overrideRate;
                    });

                    $override = $recipient
                        ->unitizationCommissionCalculator($bulk->start, $bulk->end)->summary()->override();
                    $commission = $recipient
                        ->unitizationCommissionCalculator($bulk->start, $bulk->end)->summary()->total();

                    if (($override + $commission) != 0) {
                        $excel->sheet(
                            str_limit($recipient->name, 20),
                            function ($sheet) use ($compliant, $reports, $override) {
                                $sheet->loadView(
                                    'unitization.exports.commission_schedules',
                                    ['schedules' => $compliant, 'override' => $override, 'reports' => $reports]
                                );
                            }
                        );
                    }
                }
            }
        )->store('xlsx');

        $this->mailResult(storage_path('exports/' . $filename . '.xlsx'), $user);
    }

    private function mailResult($file_path, $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->from('support@cytonn.com');
        $mailer->bcc(\config('system.administrators'));
        $mailer->subject('CRIMS Unitization Commissions Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Please find the attached the requested Unitization Commissions Report');
    }

    public function getOverrideRate($recipient, $structure)
    {
        if (is_null($structure)) {
            return 0;
        }

        $rate = $structure->rates()->where('rank_id', $recipient->rank_id)->first();

        if (!$rate) {
            return 0;
        }
        return $rate->rate;
    }
}
