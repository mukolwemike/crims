<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 16/10/2018
 * Time: 15:56
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client\ConsentForm;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class AddConsentForm implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $consentForm = new ConsentForm();
        $consentForm->client_id = $data['client_id'];
        $consentForm->email = $data['email'];
        $consentForm->date = $data['date'];
        $consentForm->consented = $data['consent'];
        $consentForm->document_id = (isset($data['document_id'])) ? $data['document_id'] : null;
        $consentForm->save();

        \Flash::success('Consent Form has been added');

        $next = \Redirect::to('/dashboard/clients');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        return [];
    }
}
