<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Shares\ShareHolderRepository;
use App\Cytonn\Models\ShareHolder;

/**
 * Class DepositMoneyForSharePurchase
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class DepositMoneyForSharePurchase implements ApprovalHandlerInterface
{
    protected $repo;

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holder = ShareHolder::findOrFail($data['share_holder_id']);

        $this->repo = new ShareHolderRepository($holder);

        $this->repo->depositMoney($data, $holder->entity->account);



        \Flash::success('Money deposit was successfully made!');
        $next = \redirect('/dashboard/shares/shareholders/show/'.$holder->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $holder = ShareHolder::findOrFail($data['share_holder_id']);

        return ['data'=>$data, 'holder'=>$holder];
    }
}
