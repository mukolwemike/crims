<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPriceTrail;
use App\Cytonn\Portfolio\Summary\FundPricingSummary;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Unitization\Trading\Performance;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundPriceTrail implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $fund->repo->recordPerformance(Carbon::parse($data['date']), true);

        \Flash::success('Unit fund price trail has been successfully saved');

        $next = \Redirect::to($fund->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $date = Carbon::parse($data['date']);

        $pricing = new FundPricingSummary($fund, $date);

        $totals = $pricing->summary()['totals'];

        return [
            'date' => $date,
            'pricing' => $pricing,
            'data'=>$data,
            'fund'=>$fund,
            'totals' => $totals,
            'calc' => $fund->category->calculation->slug
        ];
    }
}
