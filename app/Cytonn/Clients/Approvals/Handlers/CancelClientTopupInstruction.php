<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class CancelClientTopupInstruction implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $topup = ClientTopupForm::findOrFail($data['topup_id']);
        $topup->inactive = true;
        $topup->save();



        $next = \redirect('/dashboard/investments/client-instructions/topup/'.$topup->id);

        \Flash::success('The client topup instruction has been successfully cancelled.');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $topup = ClientTopupForm::findOrFail($data['topup_id']);

        return ['topup'=>$topup];
    }
}
