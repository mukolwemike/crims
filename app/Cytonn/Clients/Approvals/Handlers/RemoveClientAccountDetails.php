<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientBankDetailsAddCommand;
use Cytonn\Clients\ClientMpesaDetailsAddCommand;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class RemoveClientAccountDetails implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        $client = $approval->client;
        $data = $approval->payload;
        $account = isset($data['id']) ? ClientBankAccount::findOrFail($data['id']) : null;

        $account->active = null;
        $account->save();

        \Flash::success('Client Bank Account Deactivated successfully');

        $next = Redirect::to("/dashboard/clients/details/{$client->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $client = $approval->client;
        $data = $approval->payload;
        $account = isset($data['id']) ? ClientBankAccount::findOrFail($data['id']) : null;

        $branch = $account ? $account->branch : null;
        $bank = $branch ? $branch->bank : null;
        $currency = $account->currency ? $account->currency : null;

        return [
            'client' => $client,
            'data' => $data,
            'account' => $account,
            'bank' => $bank ? $bank : null,
            'branch' => $branch ? $branch : null,
            'currency' => $currency ? $currency->name : null,
        ];
    }
}
