<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/15/16
 * Time: 1:07 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientInvestment;
use Cytonn\Authorization\Authorizer;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class MaturityNotification implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * MaturityNotification constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }


    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $investments = ClientInvestment::where('maturity_date', $approval->payload['date'])
            ->where('withdrawn', '!=', 1)->get();

        //send notification emails to clients
        foreach ($investments as $investment) {
            \Queue::push(
                '\Cytonn\Mailers\ClientMaturityNotificationMailer@queueNotifyWeekBefore',
                ['investment_id' => $investment->id, 'approval_id'=>$approval->id]
            );
        }



        \Flash::message('Maturity notifications have been sent');

        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }


    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        return [];
    }
}
