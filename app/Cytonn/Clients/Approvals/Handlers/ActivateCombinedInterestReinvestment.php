<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 19/04/2018
 * Time: 15:31
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class ActivateCombinedInterestReinvestment implements ApprovalHandlerInterface
{
    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $this->activate($data);

        \Flash::success("Combined interest re-investment plan has been succesfully executed");

        $next = \Redirect::to('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        return [
            'data' => $data,
            'client' => $client
        ];
    }

    /**
     * @param $data
     */
    public function activate($data)
    {
        $tenor = isset($data['combine_interest_reinvestment_tenor']) ? $data['combine_interest_reinvestment_tenor']
            : null;

        $combine = (isset($data['action']) && $data['action'] == 'activate') ? true : null;

        $combine = isset($data['combine_interest_reinvestment']) ? $data['combine_interest_reinvestment']
            : $combine;

        $client = Client::findOrFail($data['client_id']);

        $client->combine_interest_reinvestment_tenor = $tenor;

        $client->combine_interest_reinvestment = $combine;

        $client->save();
    }
}
