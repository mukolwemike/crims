<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 8/30/18
 * Time: 10:26 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Portfolio\FundCompliance;
use App\Cytonn\Models\Portfolio\FundComplianceBenchmark;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class UnitFundCompliance implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $fund->fundCompliance->each(function ($compliance) {
            return $compliance->delete();
        });

        $benchmarks = collect($data['selectedCompliance']);

        $benchmarks->each(function ($benchmark) use ($fund) {
            $benchmark = FundComplianceBenchmark::findOrFail($benchmark['id']);

            return $this->save($fund, $benchmark);
        });



        \Flash::success('The fund compliance has been linked successfully');

        $next = \Redirect::to("/dashboard/unitization/unit-funds/$fund->id/unit-fund-compliance");

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function save(UnitFund $fund, FundComplianceBenchmark $benchmark)
    {
        $compliance = new FundCompliance();

        $compliance->unit_fund_id = $fund->id;

        $compliance->benchmark_id = $benchmark->id;

        $compliance->save();

        return $compliance;
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $selectedCompliance = collect($data['selectedCompliance']);

        return [
            'data' => $data,
            'fund' => $fund,
            'selectedCompliance' => $selectedCompliance
        ];
    }
}
