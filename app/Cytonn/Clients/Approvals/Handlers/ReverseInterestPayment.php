<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/17/16
 * Time: 1:56 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestPayment;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Reverse;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class ReverseInterestPayment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * ReverseInterestPayment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->authorizer->checkAuthority('addinvestment');

        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $payment = InterestPayment::find($approval->payload['payment_id']);

        if (is_null($payment)) {
            throw new ClientInvestmentException(
                "The payment is already deleted, it could have been reversed."
            );
        }

        $reverse = new Reverse();
        $reverse->interestPayment($payment);



        \Flash::success('The Interest Payment has been reversed');

        $next = \Redirect::to('/dashboard/investments/clientinvestments/' . $payment->investment->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $the_payment = InterestPayment::find($data['payment_id']);

        $notFound = false;
        if (is_null($the_payment)) {
            $notFound = true;
            $the_payment = InterestPayment::withTrashed()->find($data['payment_id']);
            $investment = $the_payment->investment;
        } else {
            $investment = $the_payment->investment;
        }

        $reversedPayments = InterestPayment::withTrashed()->where('investment_id', $investment->id)
            ->whereNotNull('deleted_at')->get();

        return ['the_payment' => $the_payment, 'notFound' => $notFound, 'investment' => $investment,
            'data' => $data, 'reversedPayments' => $reversedPayments];
    }
}
