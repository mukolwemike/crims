<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Shares\ShareTradingRepository;
use App\Cytonn\Models\ShareHolder;

class MakeSharesPurchaseOrder implements ApprovalHandlerInterface
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $buyer = ShareHolder::findOrFail($data['buyer_id']);

        $data['approval_id'] = $approval->id;

        $this->repo->makePurchaseOrder($buyer, $approval, $data);



        \Flash::success('Shares purchase order was successfully made!');

        $next = \redirect('/dashboard/shares/purchases/shareholders/'.$buyer->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $buyer = ShareHolder::findOrFail($data['buyer_id']);

        $recipient = CommissionRecepient::findOrFail($data['commission_recipient_id']);
        $prev = $recipient->getPreviousPosition();

        $recipient->previous = $prev ? $prev->type: null;

        return ['data'=>$data, 'buyer'=>$buyer, 'recipient'=>$recipient];
    }
}
