<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class UpdateSigningMandate implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $input = $approval->payload;

        $client = Client::findOrFail($input['client_id']);

        $client->signing_mandate = $input['signing_mandate'];

        $client->save();

        \Flash::success('Signing mandate has been updated');

        $next = \Redirect::to('/dashboard/clients');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $signingMandates = [
            '0' => 'Singly',
            '1' => 'All to sign',
            '2' => 'Either to sign',
            '3' => 'At Least Two to sign'
        ];

        $signingMandate = $signingMandates[$data['signing_mandate']];

        return [
            'client' => $client,
            'signingMandate' => $signingMandate,
            'date' => $data
        ];
    }
}
