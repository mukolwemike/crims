<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/16/16
 * Time: 2:16 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class EditAlternativeEmails implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * EditAlternativeEmail constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $emails = $approval->payload['emails'];

        is_array($emails) ?: $emails = [];

        foreach ($emails as $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new ClientInvestmentException('One or more of the emails in invalid');
            }
        }

        $client = $approval->client;
        $client->emails = $emails;
        $client->save();




        \Flash::success('Client emails updated');

        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $client = $approval->client;

        return ['client'=>$client];
    }
}
