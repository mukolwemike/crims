<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesEntity;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Shares\Trading\Redeem;
use Illuminate\Support\Facades\Response;

class RedeemShares implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holder = ShareHolder::findOrFail($data['share_holder_id']);

        $entity = $holder->entity;

        $redeem = new Redeem($holder);

        $redeem->redeemHolderShares($approval);

        if (isset($data['membership_fee']) && $data['membership_fee']) {
            $redeem->redeemMembershipFee($approval);
        }



        \Flash::success('Redemption of shares was successfully made!');

        $next = \redirect('/dashboard/shares/sales/shareholders/' . $holder->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $holder = ShareHolder::findOrFail($data['share_holder_id']);

        $entity = $holder->entity;

        $date = Carbon::parse($data['date']);

        if (isset($data['membership_fee'])) {
            $membershipAmount = $holder->client->clientPayments()
                ->ofType('M')
                ->sum('amount');

            $membershipAmount = - $membershipAmount;
        } else {
            $membershipAmount = 0;
        }

        $shares = $holder->repo->getSharesForRedemption($date);

        return [
            'holder' => $holder,
            'entity' => $entity,
            'shares' => $shares,
            'membershipAmount' => $membershipAmount
        ];
    }
}
