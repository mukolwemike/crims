<?php
/**
 * Date: 25/04/2016
 * Time: 8:59 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Illuminate\Support\Facades\Response;

use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

/**
 * Interface ApprovalHandlerInterface
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
interface ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval);

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null);
}
