<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Product;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class SaveInterestRateUpdate implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $update = InterestRateUpdate::create(array_except($data, ['update_id', 'rates']));


        $update->rates()->createMany($data['rates']);



        $next = \redirect('/dashboard/investments/interest-rates/show/' . $update->id);

        \Flash::success('The interest rate update has been approved and saved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $update = !is_null($data['update_id'])
            ? InterestRateUpdate::findOrFail($data['update_id']) : new InterestRateUpdate();

        $product = Product::findOrFail($data['product_id']);

        return ['date' => $data, 'update' => $update, 'rates' => $data['rates'], 'product' => $product];
    }
}
