<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\AdvocateProject;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateAdvocate implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['project_ids', '_token', 'files', 'advocate_id']);

        $advocate = Advocate::findOrFail($data['advocate_id']);
        $advocate->update($input);

        if (isset($data['project_ids'])) {
            Project::whereIn('id', $data['project_ids'])
                ->get()
                ->each(
                    function ($project) use ($advocate) {
                        if (!$this->projectAssignmentExists($advocate, $project)) {
                            AdvocateProject::create(
                                [
                                    'advocate_id' => $advocate->id,
                                    'project_id' => $project->id,
                                ]
                            );
                        }
                    }
                );
        }


        \Flash::success('Advocate has been successfully updated');

        $next = \Redirect::to("/dashboard/realestate/advocates/{$advocate->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $advocate = Advocate::findOrFail($data['advocate_id']);

        $currentProjects = $this->getCurrentProjects($advocate);

        $newProjects = array_key_exists('project_ids', $data) ? Project::whereIn('id', $data['project_ids'])->get() : null;

        return ['data' => $data, 'advocate' => $advocate, 'newProjects' => $newProjects, 'currentProjects' => $currentProjects];
    }

    /**
     * @param Advocate $advocate
     * @param Project $project
     * @return bool
     */
    private function projectAssignmentExists(Advocate $advocate, Project $project)
    {
        return AdvocateProject::where('advocate_id', $advocate->id)
            ->where('project_id', $project->id)
            ->exists();
    }

    private function getCurrentProjects($advocate)
    {
        return $advocate->projectAssignments->map(
            function ($assignment) {
                return $assignment->project->name;
            }
        )->all();
    }
}
