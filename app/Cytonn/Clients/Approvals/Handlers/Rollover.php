<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/17/16
 * Time: 11:59 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\SchedulableApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Approvals\Events\Actions\InvestmentActionApproved;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Rollover extends SchedulableApproval implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $formats = [
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'amount'        => 'amount',
        'tenor'         => 'append: days',
        'interest_payment_interval' => 'append: months',
        'interest_rate' => 'append:%',
        'commission_rate' => 'append:%'
    ];

    /**
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::find($data['investment']);
        $action_date = $investment->maturity_date;

        if ($action_date->isFuture()) {
            $this->schedule($approval, $action_date, ['investment_id' => $investment->id]);

            \Flash::success('The rollover has been scheduled');
        } else {
            $this->runTransaction($approval);

            \Flash::success('The investment withdrawal has been saved');
        }

        $next = redirect('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $interestPayment = [null=>'On Maturity', 0 => 'On Maturity', 1=>'Every 1 month', 2=>'Every 2 months',
            3=>'Every 3 months (Quarterly)', 4=>'Every 4 months', 5=>'Every 5 months',
            6=>'Every 6 months (Semi anually)', 7=>'Every 7 months', 8=>'Every 8 months',
            9=>'Every 9 months', 10=>'Every 10 months',
            11=>'Every 11 months', 12=>'Every 12 months (Annually)'];

        $investment = ClientInvestment::findOrFail($data['investment']);

        $value_today = $investment->repo->getTotalValueOfAnInvestment() + $investment->withdraw_amount;
        $final_value = $investment->repo->getFinalTotalValueOfInvestment() +  $investment->withdraw_amount;

        if ($data['reinvest'] == 'withdraw') {
            $reinvest = $final_value - (float) $data['amount'];
            $withdraw = $data['amount'];
        } else {
            $reinvest = $data['amount'];
            $withdraw = $final_value - (float) $data['amount'];
        }

        $clientinvestment = ClientInvestment::find($data['investment']);

        $findInvestment = $clientinvestment;

        $commissionrecepient = CommissionRecepient::find($data['commission_recepient']);

        $interestaction = function ($id) {
            return InterestAction::findOrFail($id);
        };

        $commissionrate = CommissionRate::all(['name', 'rate']);

        $action_date = $investment->maturity_date;

        return [
            'action_date' => $action_date,
            'scheduled' => $action_date->isFuture(),
            'interestPayment'=>$interestPayment,
            'findInvestment'=>$findInvestment,
            'investment'=>$investment,
            'value_today'=>$value_today,
            'final_value'=>$final_value,
            'value'=>$final_value,
            'reinvest'=>$reinvest,
            'withdraw'=>$withdraw,
            'data'=>$data,
            'clientinvestment'=>$clientinvestment,
            'commissionrecepient'=>$commissionrecepient,
            'interestaction'=>$interestaction,
            'commissionrate'=>$commissionrate
        ];
    }

    public function runTransaction($approval)
    {
        $data = $approval->payload;

        $oldInvestment = ClientInvestment::findOrFail($data['investment']);

        if ($oldInvestment->withdrawn) {
            throw new ClientInvestmentException('The investment has already been withdrawn');
        }

        is_null(\Auth::user()) ? $user = User::findOrFail($approval->approved_by) : $user = \Auth::user();

        if (!isset($data['on_call'])) {
            $data['on_call'] = false;
        }

        $amount = $this->calculateAmountReinvested($approval);

        $appl = $this->createRolloverApplication($oldInvestment, $amount, $data['interest_rate']);

        $data = array_add($data, 'approval_id', $approval->id);
        $rollover = new \Cytonn\Investment\Action\Rollover();

        $rollover->single($oldInvestment, $appl, $user, $data);
    }

    public function calculateAmountReinvested($approval)
    {
        $data = $approval->payload;

        $oldInvestment = ClientInvestment::findOrFail($data['investment']);

        if ($data['reinvest'] == 'withdraw') {
            $amount = $oldInvestment->repo
                    ->getTotalValueOfInvestmentAtDate($oldInvestment->maturity_date, true) - $data['amount'];
        } elseif ($data['reinvest'] == 'reinvest') {
            $amount = $data['amount'];
        } else {
            throw new ClientInvestmentException('Only reinvest and withdraw supported');
        }

        return $amount;
    }

    public function calculateAmountWithdrawn($approval)
    {
        $data = $approval->payload;

        $oldInvestment = ClientInvestment::findOrFail($data['investment']);

        if ($data['reinvest'] == 'withdraw') {
            return $data['amount'];
        }

        if ($data['reinvest'] == 'reinvest') {
            return $oldInvestment->repo
                    ->getTotalValueOfInvestmentAtDate($oldInvestment->maturity_date, true) - $data['amount'];
        }

        throw new ClientInvestmentException('Only reinvest and withdraw supported');
    }

    public function calculatePenalty($approval)
    {
        return 0;
    }

    private function createRolloverApplication($oldInvestment, $amount, $rate)
    {
        $clientRolloverApp = $oldInvestment->application;

        !is_null($clientRolloverApp) ?: $clientRolloverApp = new ClientInvestmentApplication();

        $clientRolloverApp->amount = $amount;
        $clientRolloverApp->application_type_id = 2;
        $clientRolloverApp->parent_application_id = $oldInvestment->application_id;
        $clientRolloverApp->agreed_rate = $rate;

        $clientRolloverApp->save();

        return $clientRolloverApp;
    }

    protected function pluck($name)
    {
        $payload = $this->approval->payload;

        switch ($name) {
            case 'product':
                return ClientInvestment::find($this->approval->payload['investment'])->product->name;
            case 'amount':
                return $this->calculateAmountReinvested($this->approval);
            case 'tenor':
                return Carbon::parse($payload['invested_date'])->diffInDays(Carbon::parse($payload['maturity_date']));
            case 'fa_name':
                return CommissionRecepient::find($payload['commission_recepient'])->name;
        }
    }
}
