<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Mailers\Client\LoyaltyPointsRedeemMailer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\LoyaltyPointsRedeemInstructions;
use App\Cytonn\Models\RewardVoucher;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class RedeemLoyaltyPoints implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $instruction = LoyaltyPointsRedeemInstructions::find($data['instruction_id']);

        $payload = \GuzzleHttp\json_decode($instruction->payload);

        if (!$this->vouchersAvailable($payload)) {
            throw new ClientInvestmentException("Reward Vouchers are unavailable");
        }

        $instruction->approval_id = $approval->id;

        $instruction->save();

        $client = $approval->client;

        if ($client->joint || $client->type->name == 'corporate' || $instruction->origin === 0) {
            (new LoyaltyPointsRedeemMailer())->notify($client, $instruction);
        }

        $this->updateVouchersCount($payload);

        \Flash::success('Client Loyalty Points Redeem Instruction have successfully been approved');

        $next = \Redirect::to('/dashboard/investments/client-instructions');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $instruction = LoyaltyPointsRedeemInstructions::find($data['instruction_id']);

        $payload = \GuzzleHttp\json_decode($instruction->payload);

        $vouchers = [];
        if (isset($payload->voucher_ids)) {
            foreach ($payload->voucher_ids as $id) {
                $voucher = RewardVoucher::findOrFail($id);

                array_push($vouchers, $voucher);
            }
        } else if (isset($payload->voucher_id)) {
            $voucher = RewardVoucher::findOrFail($payload->voucher_id);

            array_push($vouchers, $voucher);
        }
        $client = $approval->client;

        $calc = $client->calculateLoyalty();

        return [
            'client' => $client,
            'instruction' => $instruction,
            'vouchers' => $vouchers,
            'calc' => $calc
        ];
    }

    private function vouchersAvailable($payload)
    {
        if (isset($payload->voucher_ids)) {
            foreach ($payload->voucher_ids as $id) {
                $voucher = RewardVoucher::find($id);

                if ($voucher->number == 0) {
                    return false;
                }
            }
        } else if (isset($payload->voucher_id)) {
            $voucher = RewardVoucher::find($payload->voucher_id);

            if ($voucher->number == 0) {
                return false;
            }
        }

        return true;
    }

    private function updateVouchersCount($payload)
    {
        if (isset($payload->voucher_ids)) {
            foreach ($payload->voucher_ids as $id) {
                $voucher = RewardVoucher::find($id);

                $voucher->number -= 1;

                $voucher->used = 1;

                $voucher->save();
            }
        } else if (isset($payload->voucher_id)) {
            $voucher = RewardVoucher::find($payload->voucher_id);

            $voucher->number -= 1;

            $voucher->used = 1;

            $voucher->save();
        }
    }
}
