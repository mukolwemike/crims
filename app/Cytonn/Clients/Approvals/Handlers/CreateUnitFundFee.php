<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundFee;
use App\Cytonn\Models\Unitization\UnitFundFeeAmount;
use App\Cytonn\Models\Unitization\UnitFundFeeChargeFrequency;
use App\Cytonn\Models\Unitization\UnitFundFeeChargeType;
use App\Cytonn\Models\Unitization\UnitFundFeeParameter;
use App\Cytonn\Models\Unitization\UnitFundFeeParameterPercentage;
use App\Cytonn\Models\Unitization\UnitFundFeeType;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundFee implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files']);

        $fee = $this->saveUnitFundFee($input);

        $fee->dependent ? $this->saveParamAndPercentage($fee, $input) : $this->saveAmount($fee, $input);

        \Flash::success('Unit fund fee has been successfully saved');

        $next = \Redirect::to($fee->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $type = UnitFundFeeType::findOrFail($data['unit_fund_fee_type_id']);

        $chargeType = UnitFundFeeChargeType::where('id', $data['fee_charge_type_id'])->pluck('name')[0];

        $frequency = (isset($data['fee_charge_frequency_id']))
            ?  UnitFundFeeChargeFrequency::where('id', $data['fee_charge_frequency_id'])->pluck('name')[0]
            : null;

        return [ 'data'=>$data, 'type'=>$type, 'fund'=>$fund, 'chargeType'=>$chargeType, 'frequency'=>$frequency ];
    }

    private function saveUnitFundFee(array $input)
    {
        $fund = UnitFund::findOrFail($input['fund_id']);

        $frequency = (isset($input['fee_charge_frequency_id']))
            ?  $input['fee_charge_frequency_id']
            : null;

        $input['charged_at'] = (isset($input['charged_at']))
            ? $input['charged_at']
            : null;

        $fee = new UnitFundFee();

        $fee->fill(
            [
                'unit_fund_fee_type_id' =>  $input['unit_fund_fee_type_id'],
                'dependent'             =>  $input['dependent'],
                'date'             =>  $input['date'],
                'fee_charge_type_id'   => $input['fee_charge_type_id'],
                'fee_charge_frequency_id' => $frequency,
                'charged_at' => $input['charged_at']
            ]
        );
        $fee->fund()->associate($fund);
        $fee->save();

        return $fee;
    }

    private function saveParamAndPercentage(UnitFundFee $fee, array $input)
    {
        $parameter = UnitFundFeeParameter::findOrFail($input['unit_fund_fee_parameter_id']);

        $percentage = new UnitFundFeeParameterPercentage();
        $percentage->parameter()->associate($parameter);
        $percentage->fee()->associate($fee);
        $percentage->percentage = $input['percentage'];
        $percentage->date = $fee->date;
        $percentage->is_amount = $input['is_amount'];

        $percentage->save();
    }

    private function saveAmount(UnitFundFee $fee, array $input)
    {
        $amount = new UnitFundFeeAmount();
        $amount->fee()->associate($fee);
        $amount->amount = $input['amount'];

        $amount->save();
    }
}
