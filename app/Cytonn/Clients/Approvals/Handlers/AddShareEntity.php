<?php

namespace Cytonn\Clients\Approvals\Handlers;

use function app;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\SharesEntity;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Redirect;

/**
 * Class AddShareEntity
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class AddShareEntity implements ApprovalHandlerInterface
{
    
    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        
        $shareEntity = new SharesEntity();
        
        $shareEntity->name = $data['name'];
        $shareEntity->fund_manager_id = $data['fund_manager_id'];
        $shareEntity->account_id = $data['account_id'];
        $shareEntity->currency_id = $data['currency_id'];
        //        $shareEntity->max_holding = $data['max_holding'];
        
        $shareEntity->save();
        
        \Flash::success('Share Entity has been added successfully');
        $next = Redirect::to('/dashboard/shares/entities/');
    
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }
    
    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        
        $fundManager = FundManager::find($data['fund_manager_id']);
        $account = CustodialAccount::find($data['account_id']);
        $currency = Currency::find($data['currency_id']);
        
        return [
            'data'=>$data, 'fundManager'=>$fundManager, 'account'=>$account, 'currency'=>$currency
        ];
    }
}
