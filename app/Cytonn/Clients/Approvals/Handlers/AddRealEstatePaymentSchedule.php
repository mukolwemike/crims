<?php
/**
 * Date: 09/06/2016
 * Time: 7:36 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AddRealEstatePaymentSchedule implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;


    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return mixed
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        isset($data['schedule_id']) ? $schedule_id = $data['schedule_id'] : $schedule_id = null;

        if (!isset($data['schedules'])) {
            $data['schedules'] = [$data];
        }

        Collection::make($data['schedules'])->map(
            function ($schedule_arr) use ($holding, $schedule_id, $data) {
                $schedule = $this->checkExistingScheduleOnDate($holding, $schedule_arr['date'], $schedule_id);
                $schedule->date = $schedule_arr['date'];
                $schedule->unit_holding_id = $data['holding_id'];
                $schedule->description = $schedule_arr['description'];
                $schedule->payment_type_id = $schedule_arr['payment_type_id'];
                $schedule->amount = $schedule_arr['amount'];
                $schedule->save();
            }
        );

        $holding->fresh()->repo->setLastSchedule();

        \Flash::success('The payment schedule(s) has been added');

        $next = \Redirect::to('/dashboard/realestate/projects/show/' . $holding->project->id);

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return mixed
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $holding = UnitHolding::findOrFail($data['holding_id']);
        $schedules = RealEstatePaymentSchedule::where('unit_holding_id', $data['holding_id'])->get();
        if (!isset($data['schedules'])) {
            $data['schedules'] = [$data];
        }
        $total = $schedules->sum('amount');

        $paymenttype = function ($id) {
            return RealEstatePaymentType::find($id);
        };

        return [
            'holding' => $holding,
            'schedules' => $schedules,
            'total' => $total,
            'schedules_arr' => $data['schedules'],
            'paymenttype' => $paymenttype
        ];
    }

    /**
     * @param $holding
     * @param $date
     * @param null $schedule_id
     * @return RealEstatePaymentSchedule|RealEstatePaymentSchedule[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws ClientInvestmentException
     */
    private function checkExistingScheduleOnDate($holding, $date, $schedule_id = null)
    {
        $schedule = RealEstatePaymentSchedule::where('date', $date)
            ->where(
                'unit_holding_id',
                $holding->id
            )->first();

        if (is_null($schedule) && is_null($schedule_id)) {
            return new RealEstatePaymentSchedule();
        }

        if (!is_null($schedule_id)) {
            return RealEstatePaymentSchedule::find($schedule_id);
        }

        throw new ClientInvestmentException('There is an existing schedule for this unit on the set date, 
        this may be a duplicate');
    }
}
