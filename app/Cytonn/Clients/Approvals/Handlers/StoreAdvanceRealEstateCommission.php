<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Models\Investment\AdvanceCommissionType;
use Cytonn\Models\RealEstate\AdvanceRealEstateCommission;
use Illuminate\Support\Facades\Response;

class StoreAdvanceRealEstateCommission implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $advanceType = AdvanceCommissionType::findOrFail($data['advance_commission_type_id']);

        $data['amount'] = $advanceType->credit == 1 ? $data['amount'] : - $data['amount'];

        if (isNotEmptyOrNull($data['id'])) {
            $advanceCommission = AdvanceRealEstateCommission::findOrFail($data['id']);

            $advanceCommission->update($data);
        } else {
            AdvanceRealEstateCommission::create($data);
        }

        \Flash::success('The advance real estate commission has been saved');

        $next = \redirect('/dashboard/realestate/commissions/for-unit/' . $data['holding_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $advanceCommission = $data['id'] ? AdvanceRealEstateCommission::findOrFail($data['id'])
            : null;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        $recipient = CommissionRecepient::findOrFail($data['recipient_id']);

        return [
            'data' => $data,
            'advanceCommission' => $advanceCommission,
            'holding' => $holding,
            'recipient' => $recipient
        ];
    }
}
