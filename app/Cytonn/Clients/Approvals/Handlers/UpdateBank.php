<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Advocate;
use App\Cytonn\Models\AdvocateProject;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateBank implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files', 'advocate_id']);

        $bank = ClientBank::findOrFail($data['bank_id']);
        $bank->update(array_except($input, ['bank_id']));

        \Flash::success('Bank has been successfully updated');

        $next = \Redirect::to("/dashboard/banks/{$bank->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $bank = ClientBank::findOrFail($data['bank_id']);

        return ['data'=>$data, 'bank'=>$bank];
    }
}
