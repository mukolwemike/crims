<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Mailers\RealEstate\OverdueRealEstatePaymentRemindersForClientsMailer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class SendBulkOverduePaymentReminders implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    /**
     * @param ClientTransactionApproval $approval
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     * @throws \Exception
     * @return null
     */
    public function handle(ClientTransactionApproval $approval)
    {
        \Queue::push(static::class . '@process', ['id' => $approval->id, 'user_id' => $approval]);

        \Flash::success('Overdue payments reminders have been approved in bulk.');
        \Flash::message('Overdue payments reminders have been sent to clients.');

        $next = \Redirect::to('/dashboard/realestate/payments-schedules/overdue');
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function process($job, $data)
    {
        $approval = ClientTransactionApproval::findOrFail($data['id']);

        $schedules = RealEstatePaymentSchedule::whereIn('id', $approval->payload['overdue_payment_schedules'])
            ->whereNotIn('id', $approval->payload['removed'])
            ->get();

        $schedules = $this->filter($schedules);

        $projects = function ($schedules) {
            $schedules = Collection::make($schedules);

            return $schedules->map(
                function ($schedule) {
                    return $schedule->holding->project;
                }
            )->unique();
        };

        $schedules = $schedules->each(
            function ($schedule) {
                $schedule->client_id = $schedule->holding->client_id;
            }
        )->groupBy('client_id');

        $start_date = $approval->payload['start_date'];
        $end_date = $approval->payload['end_date'];

        foreach ($schedules as $client => $schedules_arr) {
            $client = Client::findOrFail($client);

            //TODO
            (new OverdueRealEstatePaymentRemindersForClientsMailer())
                ->sendEmail($schedules_arr, $client, $start_date, $end_date, $approval->sender, $projects);
        }

        $job->delete();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $schedules = RealEstatePaymentSchedule::whereIn('id', $data['overdue_payment_schedules'])->get();

        $schedules = $this->filter($schedules);

        $project_id = !isset($data['project_id']) ? null : $data['project_id'];

        return ['schedules' => $schedules, 'start_date' => $data['start_date'],
            'end_date' => $data['end_date'], 'approval_id' => $approval->id, 'project_id' => $project_id];
    }

    private function filter($schedules)
    {
        return $schedules->each(
            function (RealEstatePaymentSchedule $schedule) {
                $schedule->overdue = $schedule->repo->overdue();
            }
        )->filter(
            function (RealEstatePaymentSchedule $schedule) {
                return $schedule->overdue > 0;
            }
        );
    }

    public function action(ClientTransactionApproval $approval, $action, $param)
    {
        switch ($action) {
            case 'remove':
                $payload = $approval->payload;
                $payload['removed'][] = $param;

                $approval->payload = $payload;
                $approval->save();

                \Flash::message('Successfully excluded');

                return \Redirect::back();
        }

        return App::abort(404);
    }
}
