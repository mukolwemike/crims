<?php
/**
 * Date: 13/02/2018
 * Time: 14:22
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientPaymentAllowedMinimum;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecepientPosition;
use App\Cytonn\Models\InterestAction;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientRepository;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\FCM\FcmManager;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Support\Facades\Response;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class NewInvestment extends AbstractApproval
{
    use DispatchableTrait, EventGenerator;

    protected $formats = [
        'amount' => 'amount',
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'interest_rate' => 'append:%',
        'tenor' => 'append: days',
        'commission_rate' => 'append:%',
        'interest_payment_interval' => 'append: months'
    ];

    public function preApproval()
    {
        return;
        $form = $this->approval->topupForm;

        $date = $form->repo->valueDate();

        //check balance
        $balance = ClientPayment::balance($this->approval->client, $form->product, null, null, $date);

        $bal = ClientPaymentAllowedMinimum::where('client_id', $this->approval->client->id)
            ->where('date', '>', $date)
            ->where(function ($q) use ($form) {
                $q->where('product_id', $form->product->id);
            })
            ->sum('amount');

        $balance = $balance - $bal;

        if (($balance - $form->amount) < -1) {
            throw new ClientInvestmentException("The client does not have enough cash for this transaction. 
            Balance: " . AmountPresenter::currency($balance)
                . " Requested: " . AmountPresenter::currency($form->amount));
        }
    }


    /**
     * @param ClientTransactionApproval $approval
     * @return Response|void
     * @throws \Exception
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->approval = $approval;

        $topup = new \Cytonn\Investment\Action\Topup();

        $this->executeWithinAtomicLock(function () use (&$topup, &$approval) {
            $topup->handle($approval->topupForm);
        }, $approval->client);

        $next = view('investment.actions.topup.topupsuccess', [
            'title' => 'Investment Topup Successful', 'investment' => $topup->client]);

        $client = $topup->client->client;

        $investment = $topup->client;

        $fcmManager = new FcmManager();

        $fcmManager->sendFcmMessageToMultipleDevices(
            $client,
            $investment->uuid,
            $investment->product->id,
            "Top up approved",
            'Success, investment top up request approved',
            'topupApproved'
        );

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $form = $approval->topupForm;

        $approvals = $form->clientApprovals->map(function (ClientInstructionApproval $appr) {
            $name = $appr->user ? $appr->user->firstname . ' ' . $appr->user->lastname : '';
            return [
                'id' => $appr->id,
                'user' => $name,
                'requested' => $appr->requestedByUser(),
                'status' => $appr->approvalStatus ? ucwords($appr->approvalStatus->slug) : 'pending approval',
                'approval_status' => $appr->status,
                'reason' => $appr->reason ? $appr->reason : 'N/A'
            ];
        });

        $approved = $approval->topupForm ? $approval->topupForm->repo->approvedByClient() : true;

        $recipient = CommissionRecepient::findOrFail($data['commission_recepient']);

        $prev = $recipient->getPreviousPosition();

        $recipient->previous = $prev ? $prev->type : null;

        $interestAction = function ($id) {
            return InterestAction::findOrFail($id);
        };

        $interestPayment = app(Controller::class)->interestIntervals();

        $product = $form->product;

        $investedDate = $form->repo->valueDate();

        $balance = $form->client->clientBalance($investedDate, $product);

        $periodicRates = collect([]);

        if ($product->present()->isSeip && is_null($form->investmentSchedule)) {
            $periodicRates = $product->periodicRatesAsAt($investedDate);
        }

        return [
            'interestaction' => $interestAction,
            'interestPayment' => $interestPayment,
            'recipient' => $recipient,
            'form' => $form,
            'product' => $product->name,
            'amount' => $form->amount,
            'invested_date' => $investedDate,
            'maturity_date' => $form->repo->maturityDate(),
            'client' => $form->client,
            'balance' => $balance,
            'approved' => $approved,
            'approvals' => $approvals,
            'periodicRates' => $periodicRates
        ];
    }

    protected function assignClientCode()
    {
        if ($this->approval->client->client_code) {
            return;
        }

        $client = $this->approval->client;

        (new ApplicationsRepository())->setClientCode($client);
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function pluck($name)
    {
        $payload = $this->approval->payload;

        $form = $this->approval->topupForm;

        switch ($name) {
            case 'amount':
                return $form->amount;
            case 'invested_date':
                return $form->repo->valueDate();
            case 'maturity_date':
                return $form->repo->maturityDate();
            case 'product':
                return $form->product->name;
            case 'tenor':
                return $form->repo->maturityDate()->diffInDays($form->repo->valueDate());
            case 'fa_name':
                return CommissionRecepient::find($payload['commission_recepient'])->name;
        }
    }
}
