<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use function Amp\Promise\rethrow;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Excels\ExcelWork;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class BulkRolloverInvestment extends AbstractApproval
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        \Queue::push(static::class . '@processRollovers', ['approval_id' => $approval->id]);

        \Flash::success('The investments have been queued for reinvestment.');

        $next = \Redirect::to('/dashboard/investments/clientinvestments/maturity');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @throws ClientInvestmentException
     */
    public function preApproval()
    {
        $approval = $this->approval;

        $data = $approval->payload;

        $date = $data['date'];

        $count = count($data['investments']) - count($data['removed']);

        $investmentsCount = $this->getInvestmentsQuery($date, $data['investments'])
            ->whereNotIn('id', $data['removed'])->count();

        if ($investmentsCount != $count) {
            throw new ClientInvestmentException('Some of the scheduled investments are ineligible for bulk 
            rollover. Please redo the transaction');
        }
    }

    private function getInvestmentsQuery($date, $investmentIds)
    {
        return ClientInvestment::whereIn('id', $investmentIds)
            ->where('maturity_date', $date)
            ->active()->whereHas('product', function ($product) {
                $product->where('active', 1);
            })->where(function ($q) {
                $q->doesntHave('schedule')->orWhereHas('schedule', function ($q) {
                    $q->where('action_date', '<', Carbon::now());
                });
            })
            ->doesntHave('rolloverInstruction')
            ->where(function ($q) {
                $q->doesntHave('instructions')->orWhereHas('instructions', function ($q) {
                    $q->whereHas('approval', function ($q) {
                        $q->where('approved', 1);
                    });
                });
            })
            ->where(function ($query) {
                $query->where('on_call', 0)->orWhereNull('on_call');
            });
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $maturityDate = Carbon::parse($data['date']);

        $investments = ClientInvestment::whereIn('id', $data['investments'])
            ->orderBy('client_id')
            ->get();

        $removedInvestments = $data['removed'];

        $checkEligible = function ($id) use ($maturityDate) {
            return $this->getInvestmentsQuery($maturityDate, [$id])->exists();
        };

        return [
            'maturityDate' => $maturityDate,
            'investments' => $investments,
            'removedInvestments' => $removedInvestments,
            'checkEligible' => $checkEligible
        ];
    }

    /**
     * @param $approval
     * @param $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function action($approval, $action, $param)
    {
        switch ($action) {
            case 'export':
                return $this->export($approval);
                break;
            case 'send_bc':
                return $this->sendBusinessConfirmations($approval);
                break;
            case 'add_investment':
                return $this->addInvestmentToList($approval, $param);
                break;
            case 'remove_investment':
                return $this->removeInvestmentFromList($approval, $param);
                break;
            case 'bulk_remove_investments':
                return $this->bulkRemoveInvestmentFromList($approval, $param);
                break;
            default:
                throw new \InvalidArgumentException("Action $action not supported in " . static::class);
        }
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param $investmentId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addInvestmentToList(ClientTransactionApproval $approval, $investmentId)
    {
        $payload = $approval->payload;

        $removed = $payload['removed'];

        $removed = array_diff($removed, [$investmentId]);

        $payload['removed'] = $removed;

        $approval->payload = $payload;

        $approval->save();

        \Flash::message('The investment has been successfully added to the list');

        return \Redirect::back();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param $investmentId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeInvestmentFromList(ClientTransactionApproval $approval, $investmentId)
    {
        $payload = $approval->payload;

        $removed = $payload['removed'];

        $removed[] = $investmentId;

        $payload['removed'] = $removed;

        $approval->payload = $payload;

        $approval->save();

        \Flash::message('The investment has been successfully excluded from the list');

        return \Redirect::back();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param $input
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkRemoveInvestmentFromList(ClientTransactionApproval $approval, $input)
    {
        $payload = $approval->payload;

        $removed = $payload['removed'];

        foreach ($input as $investmentId => $item) {
            $removed[] = $investmentId;
        }

        $payload['removed'] = $removed;

        $approval->payload = $payload;

        $approval->save();

        \Flash::message('The investments have been successfully been excluded from the list');

        return \Redirect::back();
    }

    /**
     * @param $job
     * @param $data
     */
    public function processRollovers($job, $data)
    {
        $approval = ClientTransactionApproval::find($data['approval_id']);

        $data = $approval->payload;

        $investments = ClientInvestment::whereIn('id', $data['investments'])
            ->whereNotIn('id', $data['removed'])
            ->get();

        try {
            DB::transaction(function () use ($investments, $approval) {
                $investments->each(function (ClientInvestment $investment) use ($approval, &$newInvestments) {
                    $this->lockModelForUpdate($investment);
                    $this->lockModelForUpdate($investment->client);

                    $rollover = new \Cytonn\Investment\Action\Rollover();

                    $rollover->runBulkRollover($investment, $approval);
                });

//                $this->sendBusinessConfirmations($approval);
            });
        } catch (\Exception $e) {
            $this->notifyOnFailure($e, $approval);
            throw $e;
        }

        $job->delete();
    }

    /**
     * @param \Exception $exception
     * @param ClientTransactionApproval $approval
     */
    private function notifyOnFailure(\Exception $exception, ClientTransactionApproval $approval)
    {
        $mail = Mailer::compose()
            ->to(config('system.administrators'))
            ->subject('Bulk rollover investment approval failed')
            ->text($exception->getMessage());

        if ($approval->approver) {
            $mail->to($approval->approver->email);
        }

        $mail->send();
    }

    /**
     * @param $approval
     */
    public function sendBusinessConfirmations($approval)
    {
        $clientInvestments = ClientInvestment::where('approval_id', $approval->id)
            ->whereDoesntHave('confirmation')
            ->get();

        $clientInvestments->each(function ($investment) {
            $investment->repo->sendBusinessConfirmation();
        });

        return \Redirect::back();
    }

    /**
     * @param $approval
     * @return \Illuminate\Http\RedirectResponse
     */
    public function export($approval)
    {
        $email = \Auth::user()->email;

        sendToQueue(
            function () use ($approval, $email) {
                $data = $approval->payload;

                $investments = ClientInvestment::whereIn('id', $data['investments'])
                    ->whereNotIn('id', $data['removed'])
                    ->get();

                $investments = $investments->map(function (ClientInvestment $investment) {
                    $rollover = new \Cytonn\Investment\Action\Rollover();

                    $tenor = $rollover::DEFAULT_BULK_ROLLOVER_TENOR;

                    $invested_date = Carbon::parse($investment->maturity_date)->addDay();

                    $maturityDate = $rollover->getMaturityDate($invested_date, $tenor);

                    $amount = $investment->repo
                        ->getTotalValueOfInvestmentAtDate(Carbon::parse($investment->maturity_date));

                    $interestRate = $rollover->getProductRate($investment, $tenor, $invested_date);

                    $commissionRate = $rollover->getCommissionRate($investment, $invested_date);

                    return [
                        'ID' => $investment->id,
                        'Client Code' => $investment->client->client_code,
                        'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                        'Principal' => $investment->amount,
                        'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
                        'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                        'Value At Maturity' => $amount,
                        'Product' => $investment->product->name,
                        'FA' => $investment->commission->recipient->name,
                        'New Invested Date' => $invested_date->toDateString(),
                        'New Maturity Date' => $maturityDate->toDateString(),
                        'New Investment Principal' => $amount,
                        'New Interest Rate' => $interestRate,
                        'New Commission Rate' => $commissionRate
                    ];
                })->groupBy('Product');

                $fileName = 'Bulk Maturity Rollover ' . $data['date'];

                ExcelWork::generateAndStoreMultiSheet($investments, $fileName);

                Mailer::compose()
                    ->to($email)
                    ->bcc(config('system.administrators'))
                    ->subject($fileName)
                    ->text('Please find attached the bulk maturity rollover investments for the date ' .
                        $data['date'])
                    ->excel([$fileName])
                    ->send();

                $path = storage_path().'/exports/' . $fileName .'.xlsx';

                \File::delete($path);
            }
        );

        \Flash::message('Report will be sent to your email');

        return \redirect()->back();
    }
}
