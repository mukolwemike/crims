<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Illuminate\Support\Facades\Response;

class StoreInvestmentPaymentSchedule implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $schedule = $data['schedule_id'] ?  InvestmentPaymentSchedule::findOrFail($data['schedule_id']) : null;

        if ($schedule->investment) {
            throw new ClientInvestmentException("There investment payment schedule has already been processed");
        }

        $input = [
            'date' => $data['date'],
            'amount' => $data['amount']
        ];

        if ($schedule) {
            $schedule->update($input);
        } else {
        }

        \Flash::success('The investment payment schedule has been saved');

        $next = \redirect('/dashboard/investments/investment_payment_schedules/' . $schedule->parent_investment_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedule = $data['schedule_id'] ?  InvestmentPaymentSchedule::findOrFail($data['schedule_id']) : null;

        return [
            'data' => $data,
            'schedule' => $schedule
        ];
    }
}
