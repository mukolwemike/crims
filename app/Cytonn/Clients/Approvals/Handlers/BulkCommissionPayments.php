<?php
/**
 * Date: 31/10/2016
 * Time: 17:10
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionOverrideStructure;
use App\Cytonn\Models\CommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Crims\Investments\Commission\RecipientCalculator;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Commission\Payment;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

/**
 * Class BulkCommissionPayments
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class BulkCommissionPayments implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        throw new ClientInvestmentException('This approval function is disabled. Please used the combined approval');

        \Queue::push(static::class . '@makePayments', ['approval_id' => $approval->id]);

        $next = \Redirect::back();

        \Flash::message('Commission has been approved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param $job
     * @param $data
     * @throws \Throwable
     */
    public function makePayments($job, $data)
    {
        DB::transaction(
            function () use ($job, $data) {
                $approval = ClientTransactionApproval::find($data['approval_id']);

                $recipients = CommissionRecepient::active()->get();

                $bcp = BulkCommissionPayment::find($approval->payload['bcp_id']);

                $recipients->each(
                    function ($recipient) use ($approval, $bcp) {
                        foreach ($this->currencies() as $currency) {
                            (new Payment())->make($recipient, $currency, $approval, $bcp);
                        }
                    }
                );

                $job->delete();
            }
        );
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $currencies = $this->currencies();

        $recipients = CommissionRecepient::active()->paginate('10');

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        $calculate = function (CommissionRecepient $recipient, Currency $currency) use ($approval, $bulk) {
            if (!$approval->approved) {
                return (new RecipientCalculator($recipient, $currency, $bulk->start, $bulk->end))
                    ->summary()->finalCommission();
            }

            return $this->getPaymentsMade($recipient, $currency, $approval);
        };

        return ['currencies' => $currencies, 'calculate' => $calculate, 'recipients' => $recipients, 'bulk' => $bulk];
    }

    public function action(ClientTransactionApproval $approval, $actionName, $params)
    {
        switch ($actionName) {
            case 'excel':
                \Queue::push(
                    static::class . '@exportJob',
                    ['approval_id' => $approval->id, 'user_id' => \Auth::user()->id]
                );

                \Flash::message('The excel report is being processed, it will be sent to your email when complete');

                return \Redirect::back();
                break;
            default:
                return \App::abort(404);
        }
    }

    private function getPaymentsMade(
        CommissionRecepient $recipient,
        Currency $currency,
        ClientTransactionApproval $approval
    ) {
        return CommissionPayment::where('recipient_id', $recipient->id)
            ->where('currency_id', $currency->id)
            ->where('approval_id', $approval->id)
            ->sum('amount');
    }

    public function exportJob($job, $data)
    {
        $this->export($data['approval_id'], $data['user_id']);

        $job->delete();
    }

    public function export($approval_id, $user_id)
    {
        ini_set('memory_limit', '512M');

        ini_set('max_execution_time', 300);

        $approval = ClientTransactionApproval::find($approval_id);

        $user = User::find($user_id);

        $data = $this->prepareView($approval);

        $filename = 'Commission payments';

        $overrideStructure = CommissionOverrideStructure::where('slug', 'investments')
            ->where('date', '<=', $data['bulk']->end)
            ->latest('date')
            ->first();

        $models = function ($type) use ($data) {
            $dataArray = array();

            $recipients = CommissionRecepient::active()->where('recipient_type_id', $type->id)->get();

            foreach ($recipients as $recipient) {
                $noCommission = true;

                $r = new EmptyModel();
                $r->{'Employee ID'} = $recipient->employee_id;
                $r->{'Employee Number'} = $recipient->employee_number;
                $r->Name = $recipient->name;
                $r->{'Active'} = BooleanPresenter::presentYesNo($recipient->active);
                if ($type->slug == 'ifa') {
                    $r->{'Account Number'} = $recipient->account_number;
                    $r->{'KRA Pin'} = $recipient->kra_pin;
                    $r->{'Bank Code'} = $recipient->bank_code;
                    $r->{'Branch Code'} = $recipient->branch_code;
                }

                foreach ($data['currencies'] as $currency) {
                    $calc = $data['calculate'];
                    $r->{$currency->code} = $calc($recipient, $currency);

                    if ($r->{$currency->code} != 0) {
                        $noCommission = false;
                    }
                }

                if (!$noCommission) {
                    $dataArray[] = $r;
                }
            }

            return collect($dataArray);
        };

        \Excel::create(
            $filename,
            function ($excel) use ($models, $data, $overrideStructure) {
                foreach (CommissionRecipientType::all() as $type) {
                    $excel->sheet(
                        $type->name,
                        function ($sheet) use ($models, $type) {
                            $sheet->fromModel($models($type));
                        }
                    );
                }

                // Compliant Commission
                foreach (CommissionRecepient::active()->get() as $recipient) {
                    $bulk = $data['bulk'];
                    $currencies = (new CommissionRepository())->getCurrenciesForRecipient($recipient);

                    $currencies->each(
                        function ($c) use ($recipient, $bulk) {
                            $calculator = $recipient->calculator($c, $bulk->start, $bulk->end);

                            $summary = $calculator->summary();

                            $finalCommission = $summary->finalCommission();

                            $c->summary = $summary;
                            $c->calculator = $calculator;
                            $c->total_payable = $finalCommission;
                            $c->total = $finalCommission;
                        }
                    );

                    $overrideRate = $this->getOverrideRate($recipient, $overrideStructure);

                    foreach ($currencies as $currency) {
                        $compliant = $currency->calculator->schedules();

                        $clawbacks = $currency->calculator->clawbacks();

                        $recipientCalc = $recipient->calculator($currency, $bulk->start, $bulk->end);

                        $recipientSummary = $recipientCalc->summary();

                        $reports = $recipientCalc->getReports()->each(
                            function ($fa) use ($currency, $bulk, $recipient, $overrideRate, $recipientSummary) {
                                $fa->commission = $recipientSummary->overrideCommission($fa);

                                $fa->override = $recipientSummary->overrideOnRecipient($fa);

                                $fa->rate = $overrideRate;
                            }
                        );

                        $override = $recipient->calculator($currency, $bulk->start, $bulk->end)->summary()->override();
                        $commission = $recipient->calculator($currency, $bulk->start, $bulk->end)->summary()->total();

                        if (($override + $commission) != 0) {
                            $excel->sheet(
                                str_limit($recipient->name, 20) . ' - ' . $currency->code,
                                function ($sheet) use (
                                    $compliant,
                                    $clawbacks,
                                    $reports,
                                    $override,
                                    $recipientSummary
                                ) {
                                    $sheet->loadView(
                                        'exports.commission.investments_schedules',
                                        [
                                        'schedules' => $compliant,
                                        'claw_backs' => $clawbacks,
                                        'override' => $override,
                                        'reports' => $reports,
                                        ]
                                    );
                                }
                            );
                        }
                    }
                }
            }
        )->store('xlsx');

        $file_path = storage_path('/exports/' . $filename . '.xlsx');

        $this->mailResult($file_path, $user);

        \File::delete($file_path);
    }

    /**
     * @param $file_path
     * @param $user
     */
    private function mailResult($file_path, $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->from('support@cytonn.com');
        $mailer->bcc(\config('system.administrators'));
        $mailer->subject('CRIMS Commission Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Please find the attached Commission Report');
    }

    private function currencies()
    {
        return Currency::whereHas(
            'products',
            function ($products) {
                $products->has('investments');
            }
        )->get();
    }

    /*
     * Get the override rate to use
     */
    public function getOverrideRate($recipient, $structure)
    {
        $rate = $structure->rates()->where('rank_id', $recipient->rank_id)->first();

        if (!$rate) {
            return 0;
        }

        return $rate->rate;
    }
}
