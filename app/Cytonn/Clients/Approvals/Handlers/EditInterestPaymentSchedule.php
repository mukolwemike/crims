<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientInvestment;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use App\Cytonn\Models\InterestPaymentSchedule;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class EditInterestPaymentSchedule
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class EditInterestPaymentSchedule implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $schedule = InterestPaymentSchedule::findOrFail($data['schedule_id']);
        $investment = ClientInvestment::findOrFail($data['investment_id']);

        $schedule->date = $data['date'];
        $schedule->amount = $data['amount'];
        $schedule->description = $data['description'];
        $schedule->fixed = $data['fixed'];
        $schedule->save();

        \Flash::success('The interest payment schedule has been edited succesfully');
        $next = \redirect('/dashboard/investments/pay/'.$investment->id);

        $this->raise(new ApprovalSuccessful($approval, $next));
        $this->dispatchEventsFor($this);
    }

    /**
     * Generate the data required by the interest payment schedule edit approval view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);
        $previous_data = InterestPaymentSchedule::findOrFail($data['schedule_id']);

        return ['investment'=>$investment, 'data'=>$data, 'previous_data'=> $previous_data];
    }
}
