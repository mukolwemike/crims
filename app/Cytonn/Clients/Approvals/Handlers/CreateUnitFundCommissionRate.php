<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCommissionRate;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundCommissionRate implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateUnitFundCommissionRate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);
        $recipientType = CommissionRecipientType::findOrFail($data['commission_recipient_type_id']);

        $input = array_except($data, ['_token', 'files', 'fund_id', 'commission_recipient_type_id']);

        $rate = new UnitFundCommissionRate($input);
        $rate->fund()->associate($fund);
        $rate->recipientType()->associate($recipientType);
        $rate->save();



        \Flash::success('Unit fund commission rate has been successfully saved');

        $next = \Redirect::to($rate->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);
        $recipientType = CommissionRecipientType::findOrFail($data['commission_recipient_type_id']);

        return ['data'=>$data, 'fund'=>$fund, 'recipientType'=>$recipientType];
    }
}
