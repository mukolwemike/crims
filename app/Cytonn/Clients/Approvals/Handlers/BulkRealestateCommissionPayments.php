<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionOverrideStructure;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\RealEstateCommissionPayment;
use App\Cytonn\Models\RealEstateCommissionPaymentSchedule;
use App\Cytonn\Models\User;
use Crims\Commission\Override;
use Crims\Realestate\Commission\RecipientCalculator;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Collection;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Commissions\Payment;
use Cytonn\Realestate\Commissions\Recipients;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class BulkRealestateCommissionPayments
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class BulkRealestateCommissionPayments implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        throw new ClientInvestmentException('This approval function is disabled. Please used the combined approval');

        \Queue::push(static::class . '@makePayments', ['approval_id' => $approval->id]);

        $next = \Redirect::back();

        \Flash::message('Real Estate Commission has been approved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }


    public function makePayments($job, $queue_data)
    {
        $approval = ClientTransactionApproval::findOrFail($queue_data['approval_id']);
        $data = $approval->payload;

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        DB::transaction(
            function () use ($bulk, $approval) {
                CommissionRecepient::get()
                    ->each(
                        function ($recipient) use ($bulk, $approval) {
                            (new Payment($recipient))->make($bulk, $approval);
                        }
                    );
            }
        );

        $job->delete();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param $actionName
     * @param $params
     * @return mixed
     */
    public function action(ClientTransactionApproval $approval, $actionName, $params)
    {
        switch ($actionName) {
            case 'excel':
                $approval = ClientTransactionApproval::find($approval->id);
                $bulk = BulkCommissionPayment::find($approval->payload['bcp_id']);
                $recipients = (new Recipients())->forDates($bulk->start, $bulk->end)
                    ->get();

                if (!$recipients->count()) {
                    \Flash::error('There are no FAs in the the list!');
                    return \Redirect::back();
                }
                \Queue::push(
                    static::class . '@exportJob',
                    ['approval_id' => $approval->id, 'user_id' => Auth::user()->id]
                );

                \Flash::message('The excel report is being processed, it will be sent to your email when complete');

                return \Redirect::back();
                break;
            default:
                return \App::abort(404);
        }
    }

    /**
     * @param $job
     * @param $data
     */
    public function exportJob($job, $data)
    {
        $this->exportDetails($data['approval_id'], $data['user_id']);

        if ($job) {
            $job->delete();
        }
    }

    public function exportDetails($approvalId, $userId)
    {
        ini_set('memory_limit', '512M');

        ini_set('max_execution_time', 300);

        $approval = ClientTransactionApproval::findOrFail($approvalId);

        $user = User::find($userId);

        $data = $approval->payload;

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        $data['bulk'] = $bulk;

        $filename = 'Real Estate Commissions';

        $data['calculate'] = function (CommissionRecepient $recipient) use ($approval, $bulk) {
            if (!$approval->approved) {
                return (new RecipientCalculator($recipient, $bulk->start, $bulk->end))->summary()->finalCommission();
            } else {
                return $this->getPaymentsMade($recipient, $approval);
            }
        };

        $overrideStructure = CommissionOverrideStructure::where('slug', 'real_estate')
            ->where('date', '<=', $bulk->end)
            ->latest('date')
            ->first();

        $models = function ($type) use ($data) {
            $dataArray = array();

            $recipients = CommissionRecepient::active()->where('recipient_type_id', $type->id)->get();

            foreach ($recipients as $recipient) {
                $total = $data['calculate']($recipient);

                if ($total != 0) {
                    $r = new EmptyModel();
                    $r->{'Employee ID'} = $recipient->employee_id;
                    $r->{'Employee Number'} = $recipient->employee_number;
                    $r->Name = $recipient->name;
                    $r->{'Active'} = BooleanPresenter::presentYesNo($recipient->active);
                    if ($type->slug == 'ifa') {
                        $r->{'Account Number'} = $recipient->account_number;
                        $r->{'KRA Pin'} = $recipient->kra_pin;
                        $r->{'Bank Code'} = $recipient->bank_code;
                        $r->{'Branch Code'} = $recipient->branch_code;
                    }
                    $calc = $data['calculate'];
                    $r->Amount = $calc($recipient);

                    $dataArray[] = $r;
                }
            }

            return collect($dataArray);
        };

        \Excel::create(
            $filename,
            function ($excel) use ($models, $data, $overrideStructure) {
                foreach (CommissionRecipientType::all() as $type) {
                    $excel->sheet(
                        $type->name,
                        function ($sheet) use ($models, $type) {
                            $sheet->fromModel($models($type));
                        }
                    );
                }

                // Compliant Commission
                foreach (CommissionRecepient::active()->get() as $recipient) {
                    $bulk = $data['bulk'];

                    $calculator = $recipient->reCommissionCalculator($bulk->start, $bulk->end);

                    $overrideRate = $this->getOverrideRate($recipient, $overrideStructure);

                    $compliant = $calculator->schedules();

                    $recipientSummary = $calculator->summary();

                    $reports = $calculator->getReports()->each(function ($fa) use ($recipientSummary, $overrideRate) {
                        $fa->commission = $recipientSummary->overrideCommission($fa);

                        $fa->override = $recipientSummary->overrideOnRecipient($fa);

                        $fa->rate = $overrideRate;
                    });

                    $override = $recipient
                        ->reCommissionCalculator($bulk->start, $bulk->end)->summary()->override();
                    $commission = $recipient
                        ->reCommissionCalculator($bulk->start, $bulk->end)->summary()->total();

                    if (($override + $commission) != 0) {
                        $excel->sheet(
                            str_limit($recipient->name, 20),
                            function ($sheet) use ($compliant, $reports, $override) {
                                $sheet->loadView(
                                    'realestate.exports.commission_schedules',
                                    ['schedules' => $compliant, 'override' => $override, 'reports' => $reports]
                                );
                            }
                        );
                    }
                }
            }
        )->store('xlsx');

        $this->mailResult(storage_path('exports/' . $filename . '.xlsx'), $user);
    }

    /**
     * @param $file_path
     * @param $user
     */
    private function mailResult($file_path, $user)
    {
        $mailer = new GeneralMailer();
        $mailer->to($user->email);
        $mailer->from('support@cytonn.com');
        $mailer->bcc(\config('system.administrators'));
        $mailer->subject('CRIMS Commission Report');
        $mailer->file($file_path);
        $mailer->queue(false);
        $mailer->sendGeneralEmail('Please find the attached Real Estate Commission Report');
    }


    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        $recipients = (new Recipients())->forDates($bulk->start, $bulk->end)->paginate('10');

        $recipients->each(
            function ($recipient) use ($approval, $bulk) {
                if (!$approval->approved) {
                    $recipient->amount_payable =
                        (new RecipientCalculator($recipient, $bulk->start, $bulk->end))->summary()->total();
                } else {
                    $recipient->amount_payable = $this->getPaymentsMade($recipient, $approval);
                }
            }
        );

        return ['recipients' => $recipients, 'bulk' => $bulk];
    }

    /**
     * @param CommissionRecepient $recipient
     * @param ClientTransactionApproval $approval
     * @return mixed
     */
    private function getPaymentsMade(CommissionRecepient $recipient, ClientTransactionApproval $approval)
    {
        return RealEstateCommissionPayment::where('recipient_id', $recipient->id)
            ->where('approval_id', $approval->id)
            ->sum('amount');
    }

    /*
     * Get the override rate to use
     */
    public function getOverrideRate($recipient, $structure)
    {
        $rate = $structure->rates()->where('rank_id', $recipient->rank_id)->first();

        if (!$rate) {
            return 0;
        }

        return $rate->rate;
    }

    private function getCommissionDates(
        CommissionRecepient $supervisor,
        CommissionRecepient $fa,
        BulkCommissionPayment $bulk
    ) {
        $date = '2018-01-01';

        $startDate = Carbon::parse($date);

        $endDate = $bulk->end;

        $recipientPosition = $fa->commissionRecipientPositions()
            ->where('reports_to', $supervisor->id)
            ->orderBy('start_date', 'DESC')
            ->first();

        if ($recipientPosition) {
            $otherSupervisorsExist = $fa->commissionRecipientPositions()
                ->where('reports_to', '!=', $supervisor->id)
                ->where('start_date', '<=', $recipientPosition->start_date)
                ->exists();

            $positionStart = Carbon::parse($recipientPosition->start_date);

            if ($positionStart >= $startDate && $otherSupervisorsExist) {
                $startDate = $positionStart;
            }

            $positionEnd = Carbon::parse($recipientPosition->end_date);

            if ($positionEnd <= $endDate) {
                $endDate = $positionEnd;
            }

            if (Carbon::parse($startDate)->lessThanOrEqualTo(Carbon::parse($date))) {
                $startDate = Carbon::parse($date);
            }
        }

        return [
            'start' => $startDate,
            'end' => $endDate
        ];
    }
}
