<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Presenters\General\DatePresenter;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\ClientPresenter;

class LinkCashToClientAndFa extends AbstractApproval
{

    /**
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);
        $transaction = CustodialTransaction::findOrFail($data['id']);
        $payment = ClientPayment::where('custodial_transaction_id', $transaction->id)->first();

        if ($payment->client) {
            throw new ClientInvestmentException(
                "Transaction is already linked to client code ".$client->client_code
            );
        }

        $recipient = CommissionRecepient::find($data['recipient_id']);

        if (!$recipient) {
            $recipient = $client->getLatestFA();
        }

        if ($recipient) {
            $payment->update(['commission_recipient_id' => $recipient->id]);
            $transaction->update(['commission_recipient_id'=>$recipient->id]);
        }

        if ($client) {
            $payment->update(['client_id' => $client->id]);
            $transaction->update(['client_id'=>$client->id]);
        }



        \Flash::success('Account inflow has been successfully updated!');

        $next = \Redirect::to('/dashboard/investments/accounts-cash/show/'.$transaction->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $client = Client::findOrFail($data['client_id']);
        $recipient = CommissionRecepient::find($data['recipient_id']);
        if (!$recipient) {
            $recipient = $client->getLatestFA();
        }

        $transaction = CustodialTransaction::findOrFail($data['id']);
        $payment = ClientPayment::where('custodial_transaction_id', $transaction->id)->first();

        return [
            'client'=>$client,
            'data'=>$data,
            'transaction'=>$transaction,
            'recipient'=>$recipient,
            'payment'=>$payment
        ];
    }

    /**
     * @param $name
     * @return mixed|string
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     */
    protected function pluck($name)
    {
        $data = $this->approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $recipient = CommissionRecepient::find($data['recipient_id']);

        if (!$recipient) {
            $recipient = $client->getLatestFA();
        }

        $transaction = CustodialTransaction::findOrFail($data['id']);

        $payment = ClientPayment::where('custodial_transaction_id', $transaction->id)->first();

        switch ($name) {
            case 'client_code':
                return $client->client_code;
            case 'client':
                return ClientPresenter::presentFullNames($client->id);
            case 'amount':
                return \Cytonn\Presenters\AmountPresenter::currency($transaction->amount);
            case 'bank_reference':
                return $transaction->bank_reference_no;
            case 'narration':
                return $transaction->description;
            case 'date':
                return DatePresenter::formatDate($transaction->date);
            case 'payment_for':
                return $payment->present()->paymentFor;
            case 'fa':
                return $recipient ? $recipient->name : '';
        }
    }
}
