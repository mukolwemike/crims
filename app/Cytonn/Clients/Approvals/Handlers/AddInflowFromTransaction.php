<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 01/07/2019
 * Time: 16:36
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class AddInflowFromTransaction extends AbstractApproval
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $transaction = CustodialTransaction::find($approval->payload['transaction_id']);

        $acc = $transaction->custodialAccount;

        $product = [
            'product' => Product::find($acc->product_id),
            'project' => Project::find($acc->project_id),
            'entity' => Project::find($acc->entity_id),
            'fund' => UnitFund::find($acc->unit_fund_id)
        ];

        $client = $approval->client;
        $recipient = null;

        if ($client) {
            $recipient = $client->getLatestFa();
        }

        (new AddCashToAccount())->createClientPayment(
            $transaction,
            $approval,
            $transaction->amount,
            $product,
            Carbon::parse($transaction->date),
            $approval->client,
            $recipient
        );



        \Flash::success('Inflow has been successfully added to custodial account');

        $next = \Redirect::to('/dashboard/investments/accounts-cash');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        return [];
        // TODO: Implement prepareView() method.
    }
}
