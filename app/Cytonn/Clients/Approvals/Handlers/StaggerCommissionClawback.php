<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionClawback;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Clawbacks\Generators\ClawbackStaggered;
use Illuminate\Support\Facades\Response;

class StaggerCommissionClawback implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $eligibleClawbacks = collect([]);

        if (isset($data['clawback_id'])) {
            $clawBack = CommissionClawback::where('id', $data['clawback_id'])->withTrashed()->first();

            if ($clawBack && $clawBack->deleted_at) {
                throw new ClientInvestmentException('The linked clawback record is already deleted');
            }
        }

        if (isset($data['commission_id'])) {
            $commission = Commission::findOrFail($data['commission_id']);
        } else {
            $commission = $clawBack ? $clawBack->commission : null;
        }

        if (isset($data['eligible_clawbacks'])) {
            $eligibleClawbacks = CommissionClawback::whereIn('id', $data['eligible_clawbacks'])->get();
        }

        $startDate = $data['start_date'] != '' ? Carbon::parse($data['start_date']) : Carbon::parse($clawBack->date);

        $clawBackStagger = new ClawbackStaggered(
            $commission,
            $startDate,
            $eligibleClawbacks,
            $data['stagger_duration']
        );

        $clawBackStagger->generate();

        \Flash::success("Commission clawback stagger has been approved");

        $next = \Redirect::to('/dashboard/investments/commission/' . $commission->investment_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $eligibleClawbacks = [];

        $clawBack = null;

        if (isset($data['clawback_id'])) {
            $clawBack = CommissionClawback::where('id', $data['clawback_id'])->withTrashed()->first();
        }

        if (isset($data['commission_id'])) {
            $commission = Commission::findOrFail($data['commission_id']);
        } else {
            $commission = $clawBack ? $clawBack->commission : null;
        }

        if (isset($data['eligible_clawbacks'])) {
            $eligibleClawbacks = CommissionClawback::whereIn('id', $data['eligible_clawbacks'])->get();
        }

        return [
            'data' => $data,
            'commission' => $commission,
            'clawBack' => $clawBack,
            'eligibleClawbacks' => $eligibleClawbacks
        ];
    }
}
