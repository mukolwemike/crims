<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\SharesEntity;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Redirect;

/**
 * Class EditShareEntity
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class EditShareEntity implements ApprovalHandlerInterface
{
    
    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $shareEntity = SharesEntity::findOrFail($data['id']);

        $shareEntity->name = $data['name'];
        $shareEntity->fund_manager_id = $data['fund_manager_id'];
        $shareEntity->account_id = $data['account_id'];
        $shareEntity->currency_id = $data['currency_id'];
        //        $shareEntity->max_holding = $data['max_holding'];

        $shareEntity->save();

        \Flash::success('Share Entity has been edited succesfully');
        $next = Redirect::to('/dashboard/shares/entities/');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }
    
    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        
        $entity = SharesEntity::findOrFail($data['id']);

        $fundManager = function ($id) {
            return FundManager::find($id);
        };
        
        $account = function ($id) {
            return CustodialAccount::find($id);
        };
        
        $currency = function ($id) {
            return Currency::find($id);
        };

        return [
                'data'=>$data, 'fundManager'=>$fundManager, '
                account'=>$account, 'currency'=>$currency, 'entity'=>$entity
        ];
    }
}
