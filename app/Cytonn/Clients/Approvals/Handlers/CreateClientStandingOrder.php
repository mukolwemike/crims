<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/26/18
 * Time: 1:18 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientStandingOrder;
use App\Cytonn\Models\ClientStandingOrderType;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;

class CreateClientStandingOrder implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $order = new ClientStandingOrder();
        $order->fill($data);
        $order->save();



        \Flash::success('Standing order has been saved succesfully');

        $next = \Redirect::to("/dashboard/clients/details/{$client->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $orderType = ClientStandingOrderType::findOrFail($data['type_id']);

        $investment = (isset($data['investment_id']))? ClientInvestment::findOrFail($data['investment_id']) : null;

        $project = (isset($data['project_id']))? Project::findOrFail($data['project_id']) : null;

        $holding = (isset($data['unit_holding_id']))? UnitHolding::findOrFail($data['unit_holding_id']) : null;

        $client = Client::findOrFail($data['client_id']);

        return [
            'data'=>$data,
            'orderType' => $orderType,
            'client' => $client,
            'investment' => $investment,
            'project' => $project,
            'holding' => $holding
        ];
    }
}
