<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AddRelationshipPerson implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * AddRelationshipPerson constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);
        $data = $approval->payload;
        $client = Client::findOrFail($data['client_id']);
        unset($data['client_id']);
        $client->update($data);


        \Flash::success('Relationship person has been saved');
        $next = \redirect('/dashboard/clients/details/'.$client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $client = Client::findOrFail($data['client_id']);
        $relationship_person = CommissionRecepient::findOrFail($data['relationship_person_id']);
        return ['client'=>$client, 'data'=>$data, 'relationship_person'=>$relationship_person];
    }
}
