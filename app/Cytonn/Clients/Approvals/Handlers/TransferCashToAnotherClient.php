<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Custodial\Transact\ClientTransaction;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\ClientPresenter;

class TransferCashToAnotherClient implements ApprovalHandlerInterface
{

    /**
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        // Set up
        $data = $approval->payload;

        $sending_client = Client::findOrFail($data['sending_client_id']);
        $receiving_client = Client::findOrFail($data['receiving_client_id']);

        $data['description'] = 'Transfer of ' . abs($data['amount'])
            . ' from ' . ClientPresenter::presentFullNames($sending_client->id) . ' to '
            . ClientPresenter::presentFullNames($receiving_client->id);

        $recipient = CommissionRecepient::find($data['commission_recipient_id']);

        $previous_project = isset($data['previous_project_id']) ? Project::find($data['previous_project_id']) : null;
        $previous_product = isset($data['previous_product_id']) ? Product::find($data['previous_product_id']) : null;
        $previous_entity = isset($data['previous_entity_id']) ? SharesEntity::find($data['previous_entity_id']) : null;
        $previousFund = isset($data['previous_fund_id']) ? UnitFund::find($data['previous_fund_id']) : null;

        $new_project = isset($data['new_project_id']) ? Project::find($data['new_project_id']) : null;
        $new_product = isset($data['new_product_id']) ? Product::find($data['new_product_id']) : null;
        $new_entity = isset($data['new_entity_id']) ? SharesEntity::find($data['new_entity_id']) : null;
        $newFund = isset($data['new_fund_id']) ? UnitFund::find($data['new_fund_id']) : null;

        if (array_key_exists('source_custodial_account_id', $data)) {
            $sending_custodial_account = CustodialAccount::findOrFail($data['source_custodial_account_id']);
        } else {
            if ($previous_project) {
                $sending_custodial_account = $previous_project->custodialAccount;
            } elseif ($previous_product) {
                $sending_custodial_account = $previous_product->custodialAccount;
            } elseif ($previous_entity) {
                $sending_custodial_account = $previous_entity->custodialAccount;
            } elseif ($previousFund) {
                $sending_custodial_account = $previousFund->custodialAccount;
            }
        }

        $receiving_custodial_account = CustodialAccount::findOrFail($data['custodial_account_id']);

        // Validate
        $this->validate($sending_custodial_account, $receiving_custodial_account, $data);

        // Custodial Transfer
        $sending_transaction = $receiving_transaction = null;
        if ($sending_custodial_account->id != $receiving_custodial_account->id) {
            $transaction_array = $this->custodialTransfer(
                $sending_custodial_account,
                $receiving_custodial_account,
                $sending_client,
                $receiving_client,
                $recipient,
                $data
            );
            $sending_transaction = $transaction_array['sending_transaction'];
            $receiving_transaction = $transaction_array['receiving_transaction'];
        }

        // Book Transfer
        $this->bookTransfer(
            $sending_client,
            $receiving_client,
            $previous_project,
            $new_project,
            $previous_product,
            $new_product,
            $previous_entity,
            $new_entity,
            $previousFund,
            $newFund,
            $recipient,
            $sending_transaction,
            $receiving_transaction,
            $data
        );

        \Flash::success('Transfer has been successfully saved!');
        $next = \redirect('/dashboard/investments/client-payments/client/' . $sending_client->id);
        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param CustodialAccount $sending_custodial_account
     * @param CustodialAccount $receiving_custodial_account
     * @param array $data
     * @throws ClientInvestmentException
     */
    private function validate(
        CustodialAccount $sending_custodial_account,
        CustodialAccount $receiving_custodial_account,
        array $data
    ) {
        if ($data['exchange_rate'] < 0) {
            throw new ClientInvestmentException('Exchange rate cannot be less than 0');
        }
        if ($sending_custodial_account->currency_id == $receiving_custodial_account->currency_id
            and $data['exchange_rate'] != 1
        ) {
            throw new ClientInvestmentException(
                'Exchange rate is expected to be 1 for custodial accounts with the same currency!'
            );
        }
        if ($sending_custodial_account->currency_id != $receiving_custodial_account->currency_id
            and $data['exchange_rate'] == 1
        ) {
            throw new ClientInvestmentException(
                'Exchange rate is cannot be 1 for custodial accounts with the different currencies!'
            );
        }
    }

    /**
     * @param CustodialAccount $sending_custodial_account
     * @param CustodialAccount $receiving_custodial_account
     * @param Client $sending_client
     * @param Client $receiving_client
     * @param CommissionRecepient|null $recipient
     * @param $data
     * @return array
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    private function custodialTransfer(
        CustodialAccount $sending_custodial_account,
        CustodialAccount $receiving_custodial_account,
        Client $sending_client,
        Client $receiving_client,
        CommissionRecepient $recipient = null,
        $data
    ) {
        $date = Carbon::parse($data['date']);
        $recieveAmount = $data['amount'] * $data['exchange_rate'];

        // Credit the sending custodial account
        $sending_transaction = ClientTransaction::build(
            $sending_custodial_account,
            'TO',
            $date,
            $data['description'],
            $data['amount'],
            null,
            [],
            $sending_client,
            null,
            null
        )->credit();

        // Debit receiving custodial account
        $receiving_transaction = ClientTransaction::build(
            $receiving_custodial_account,
            'TI',
            $date,
            $data['description'],
            $recieveAmount,
            null,
            [],
            $receiving_client,
            null,
            null
        )->debit();

        return ['sending_transaction' => $sending_transaction, 'receiving_transaction' => $receiving_transaction];
    }

    /**
     * @param Client $sending_client
     * @param Client $receiving_client
     * @param Project|null $previous_project
     * @param Project|null $new_project
     * @param Product|null $previous_product
     * @param Product|null $new_product
     * @param SharesEntity|null $previous_entity
     * @param SharesEntity|null $new_entity
     * @param CommissionRecepient|null $recipient
     * @param CustodialTransaction|null $sending_transaction
     * @param CustodialTransaction|null $receiving_transaction
     * @param $data
     * @throws ClientInvestmentException
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    private function bookTransfer(
        Client $sending_client,
        Client $receiving_client,
        Project $previous_project = null,
        Project $new_project = null,
        Product $previous_product = null,
        Product $new_product = null,
        SharesEntity $previous_entity = null,
        SharesEntity $new_entity = null,
        UnitFund $previousFund = null,
        UnitFund $newFund = null,
        CommissionRecepient $recipient = null,
        CustodialTransaction $sending_transaction = null,
        CustodialTransaction $receiving_transaction = null,
        $data
    ) {
        $recieveAmount = $data['amount'] * $data['exchange_rate'];
        $date = Carbon::parse($data['date']);
        $credit_description = 'Cash transfer to ' . ClientPresenter::presentFullNames($receiving_client->id);
        $debit_description = 'Cash transfer from ' . ClientPresenter::presentFullNames($sending_client->id);

        // Credit sending client
        $out = Payment::make(
            $sending_client,
            null,
            $sending_transaction,
            $previous_product,
            $previous_project,
            $previous_entity,
            $date,
            $data['amount'],
            'TO',
            $credit_description,
            $previousFund
        )->credit();

        // Debit receiving client
        $in = Payment::make(
            $receiving_client,
            $recipient,
            $receiving_transaction,
            $new_product,
            $new_project,
            $new_entity,
            $date,
            $recieveAmount,
            'TI',
            $debit_description,
            $newFund
        )->debit();

        $in->parent()->associate($out);
        $in->save();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $sending_client = Client::findOrFail($data['sending_client_id']);
        $receiving_client = Client::findOrFail($data['receiving_client_id']);

        $recipient = CommissionRecepient::find($data['commission_recipient_id']);

        $previous_project = isset($data['previous_project_id']) ? Project::find($data['previous_project_id']) : null;
        $previous_product = isset($data['previous_product_id']) ? Product::find($data['previous_product_id']) : null;
        $previous_entity = isset($data['previous_entity_id']) ? SharesEntity::find($data['previous_entity_id']) : null;
        $previousFund = isset($data['previous_fund_id']) ? UnitFund::find($data['previous_fund_id']) : null;

        $new_project = isset($data['new_project_id']) ? Project::find($data['new_project_id']) : null;
        $new_product = isset($data['new_product_id']) ? Product::find($data['new_product_id']) : null;
        $new_entity = isset($data['new_entity_id']) ? SharesEntity::find($data['new_entity_id']) : null;
        $newFund = isset($data['new_fund_id']) ? UnitFund::find($data['new_fund_id']) : null;

        if ($previous_project) {
            $sending_custodial_account = $previous_project->custodialAccount;
        } elseif ($previous_product) {
            $sending_custodial_account = $previous_product->custodialAccount;
        } elseif ($previous_entity) {
            $sending_custodial_account = $previous_entity->custodialAccount;
        } elseif ($previousFund) {
            $sending_custodial_account = $previousFund->custodialAccount;
        }

        $receiving_custodial_account = CustodialAccount::findOrFail($data['custodial_account_id']);

        return [
            'data' => $data,
            'sending_client' => $sending_client,
            'receiving_client' => $receiving_client,
            'recipient' => $recipient,
            'previous_project' => $previous_project,
            'previous_product' => $previous_product,
            'previous_entity' => $previous_entity,
            'previous_fund' => $previousFund,
            'new_project' => $new_project,
            'new_product' => $new_product,
            'new_entity' => $new_entity,
            'new_fund' => $newFund,
            'receiving_custodial_account' => $receiving_custodial_account,
            'sending_custodial_account' => $sending_custodial_account
        ];
    }
}
