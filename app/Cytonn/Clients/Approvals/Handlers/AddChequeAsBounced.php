<?php
/**
 * Date: 06/03/2018
 * Time: 16:51
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class AddChequeAsBounced extends AbstractApproval
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $transaction = CustodialTransaction::findOrFail($approval->payload['transaction_id']);
        $transaction->repo->bounceCheque(Carbon::parse($approval->payload['date']));

//        $payment = $transaction->clientPayment;
//        $payment->repo->bounceCheque();

        $suspenseTransaction = $approval->suspenseTransaction;

        if ($suspenseTransaction) {
            $suspenseTransaction->repo->setMatched();
        }

        \Flash::success('The cheque has now been bounced');
        $next = redirect()->back();

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $transaction = CustodialTransaction::findOrFail($approval->payload['transaction_id']);

        $payment = $transaction->clientPayment;

        $value = $transaction->amount != 0 ? $transaction->amount : $transaction->value;

        return ['transaction' => $transaction, 'payment' => $payment, 'client' => $payment->client, 'value' => $value];
    }
}
