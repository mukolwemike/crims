<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionClawback;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class DeleteCommissionClawback implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $clawback = CommissionClawback::findOrFail($data['id']);
        $commission = $clawback->commission;
        $recipient = $commission->recipient;

        $clawback->delete();

        \Flash::success('The commission clawback has been successfully deleted!');

        $next = redirect("/dashboard/investments/commission/payment/".$recipient->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $clawback = CommissionClawback::findOrFail($data['id']);
        $commission = $clawback->commission;
        $investment = $commission->investment;

        return ['clawback'=>$clawback, 'commission'=>$commission, 'investment'=>$investment];
    }
}
