<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientContactPerson;
use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\JointClientHolder;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;

class UpdateClientSignature implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $clientSignature = ClientSignature::findOrFail($data['id']);

        $clientSignature->update([
            'name' => $data['name'],
            'email_address' => $data['email_address'],
            'phone' => $data['phone'],
            'document_id' => $data['document_id']
        ]);

        // assign signature to client


        \Flash::success('Client signature has been added');

        $next = \Redirect::to('/dashboard/clients');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function clientSignature($data)
    {
        $clientSignature = ClientSignature::where('client_id', $data['client_id']);

        if (isset($data['joint_client_holder_id'])) {
            $clientSignature = $clientSignature->where('joint_client_holder_id', $data['joint_client_holder_id']);
        }

        if (isset($data['client_contact_person_id'])) {
            $clientSignature = $clientSignature->where('client_contact_person_id', $data['client_contact_person_id']);
        }

        if (isset($data['commission_recepient_id'])) {
            $clientSignature = $clientSignature->where('commission_recepient_id', $data['commission_recepient_id']);
        }

        return $clientSignature->first();
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $signature_person = ClientSignature::findOrFail($data['id']);

        $user = null;

        if ($signature_person->email) {
            $user = $client->clientUsers()
                ->whereDoesntHave('clientSignatures')
                ->where('email', $data['email_address'])
                ->first();
        }

        $jointHolder_old = $signature_person->joint_client_holder_id
            ? JointClientHolder::findOrFail($signature_person->joint_client_holder_id)
            : null;

        $contactPerson_old = $signature_person->client_contact_person_id
            ? ClientContactPerson::findOrFail($signature_person->client_contact_person_id)
            : null;

        $relationshipPerson_old = $signature_person->commission_recepient_id
            ? CommissionRecepient::findOrFail($signature_person->commission_recepient_id)
            : null;

        $jointHolder = ($data['joint_client_holder_id'] !== 'null')
            ? JointClientHolder::findOrFail($data['joint_client_holder_id'])
            : $jointHolder_old;

        $contactPerson = ($data['client_contact_person_id'] !== 'null')
            ? ClientContactPerson::findOrFail($data['client_contact_person_id'])
            : $contactPerson_old;

        $relationshipPerson = ($data['commission_recepient_id'] !== 'null')
            ? CommissionRecepient::findOrFail($data['commission_recepient_id'])
            : $relationshipPerson_old;

        return [
            'client' => $client,
            'jointHolder' => $jointHolder,
            'contactPerson' => $contactPerson,
            'relationshipPerson' => $relationshipPerson,
            'jointHolder_old' => $jointHolder_old,
            'contactPerson_old' => $contactPerson_old,
            'relationshipPerson_old' => $relationshipPerson_old,
            'user' => $user,
            'signature_person' => $signature_person
        ];
    }
}
