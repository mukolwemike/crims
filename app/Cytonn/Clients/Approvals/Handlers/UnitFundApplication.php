<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Title;
use Cytonn\Api\Transformers\Unitization\UnitFundApplicationTransfomer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\ApplicationsRepository;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UnitFundApplication implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        dd('Not used');
        $app = ClientTransactionApproval::findOrFail($approval->id);

        $data = $approval->payload;

        $application = \App\Cytonn\Models\Unitization\UnitFundApplication::findOrFail($data['application_id']);

        $repo = new ApplicationsRepository();

        $type = $application->type_id;

        $repo->saveUnitFundApplication($type, $application);

        $next = \Redirect::to('dashboard/applications/');

        \Flash::success('Unit Fund Application succesfully approved');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        dd('Not used');
//        $app = \App\Cytonn\Models\Unitization\UnitFundApplication::findOrFail(($approval->payload['application_id']));
//
//        $application = (new UnitFundApplicationTransfomer())->applicationDetails($app);
//
//        $jointHolders= json_decode($app['holders']);
//
//        $client = $app->client;
//
//        $duplicate = ($client) ? $client->repo->duplicate($app->toArray()): null;
//
//        if ($jointHolders != null) {
//            $jointHolders = collect($jointHolders)->each(function ($joint) {
//                $joint->country = (isset($joint->country_id))? Country::find($joint->country_id)->name: '';
//                $joint->title = (new Title())->find($joint->title_id)->name;
//            });
//        }
//
//        return [
//            'data' => $application,
//            'holders' => $jointHolders ,
//            'duplicate' => $duplicate,
//            'app' => $app
//        ];
    }
}
