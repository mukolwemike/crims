<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\ProductDocument;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\ProductDocuments\ProductDocumentRepository;
use Illuminate\Support\Facades\Response;

class StoreProductDocument implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $productDocumentRepository = new ProductDocumentRepository();

        $productDocumentRepository->save($data, $data['id']);



        \Flash::success('Product document has been successfully saved');

        $next = \Redirect::to("/dashboard/products/details/" . $data['product_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $productDocument = ($data['id']) ? ProductDocument::findOrFail($data['id']) : null;

        $product = Product::findOrFail($data['product_id']);

        return [
            'data' => $data,
            'productDocument' => $productDocument,
            'product' => $product
        ];
    }
}
