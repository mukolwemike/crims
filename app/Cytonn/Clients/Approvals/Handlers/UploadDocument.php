<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class UploadDocument implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $document = Document::findOrFail($data['document_id']);
        $document->approval_id = $approval->id;
        $document->save();



        $next = \redirect('/dashboard/documents');

        \Flash::success('The document has been approved and saved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $document = Document::findOrFail($data['document_id']);

        return ['document'=>$document];
    }
}
