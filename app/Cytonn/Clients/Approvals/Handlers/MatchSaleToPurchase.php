<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Events\Shares\SharePurchaseOrderHasBeenMatched;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Shares\ShareTradingRepository;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;

class MatchSaleToPurchase implements ApprovalHandlerInterface
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $rules = [
            'sales_order_id'        =>  'required|numeric',
            'purchase_order_ids'    =>  'required|array',
            'date'                  =>  'required|date',
        ];

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            throw new ClientInvestmentException($validator->messages()->first());
        }

        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);

        $seller = $salesOrder->seller;

        $purchaseOrders = SharesPurchaseOrder::whereIn('id', $data['purchase_order_ids'])->get();

        $date = Carbon::parse($data['date']);

        $agreedPrice = (!is_null($data['bargain_price'])) ? $data['bargain_price'] : null;

        $this->repo->matchSaleOrder($salesOrder, $purchaseOrders, $date, $agreedPrice);

//        event(new SharePurchaseOrderHasBeenMatched($approval));



        \Flash::success('Shares sales order was successfully matched to a shares purchase order(s)!');

        $next = \redirect('/dashboard/shares/sales/shareholders/'.$seller->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $selling = 0;

        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);

        $seller = $salesOrder->seller;

        $purchaseOrders = SharesPurchaseOrder::whereIn('id', $data['purchase_order_ids'])->get()
            ->each(function ($po) use (&$selling, $salesOrder) {
                $available = $salesOrder->balance() - $selling;
                $balance = $po->balance();

                $match = $balance;
                if ($po->number >= $available) {
                    $match = $available;
                }

                $po->matched_number = $match;
                $po->po_balance = $balance;
                $selling += $match;
            });

        if ($selling > $salesOrder->number) {
            throw new \LogicException("Matching more than available number of shares");
        }

        $agreedPrice = (!is_null($data['bargain_price'])) ? $data['bargain_price'] : $salesOrder->price;

        $bought = $purchaseOrders->sum(function ($purchase) use ($agreedPrice) {
            return $purchase->matched_number * $agreedPrice;
        });

        $sold = $salesOrder->numberSold();

        $balance = $salesOrder->balance();

        return [
            'seller' => $seller,
            'purchaseOrders' => $purchaseOrders,
            'salesOrder' => $salesOrder,
            'agreedPrice' => $agreedPrice,
            'sold' => $sold,
            'bought' => $bought,
            'balance' => $balance
        ];
    }
}
