<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UpdateUnitFund implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files', 'fund_id', 'current_unit_price',
            'currency', 'manager', 'custodial_account_id']);
        
        $fund = UnitFund::findOrFail($data['fund_id']);

        $fund->update($input);

        $this->saveReceivingAccount($fund, $data);



        \Flash::success('Unit fund has been successfully updated');

        $next = \Redirect::to($fund->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);
        $fundManager = FundManager::findOrFail($data['fund_manager_id']);
        $currency = Currency::findOrFail($data['currency_id']);
        $account = isset($data['custodial_account_id'])
            ? CustodialAccount::find($data['custodial_account_id']) : new CustodialAccount();

        return ['data' => $data, 'fund' => $fund, 'fundManager' => $fundManager,
            'currency' => $currency, 'account' => $account];
    }

    private function saveReceivingAccount(UnitFund $fund, $data)
    {
        if (isset($data['custodial_account_id'])) {
            $account = CustodialAccount::findOrFail($data['custodial_account_id']);
            $fund->receivingAccounts()->save($account);
        }
    }
}
