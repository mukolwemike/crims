<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientContactPerson;
use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\JointClientHolder;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class AddClientSignature implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $clientSignature = null;

        if ($data['signature_person'] === 'new') {
            $clientSignature = ClientSignature::create($data);

            $user = $clientSignature->client->clientUsers()
                ->whereDoesntHave('clientSignatures')
                ->where('email', $data['email_address'])
                ->first();

            if ($user) {
                $user->clientSignatures()->save($clientSignature, [ 'active' => true ]);
            }
        } else {
            $clientSignature = $this->clientSignature($data);

            if ($clientSignature) {
                $clientSignature = $clientSignature->update($data);
            } else {
                $clientSignature = ClientSignature::create($data);
            }
        }

        // assign signature to client


        \Flash::success('Client signature has been added');

        $next = \Redirect::to('/dashboard/clients');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function clientSignature($data)
    {
        $clientSignature = ClientSignature::where('client_id', $data['client_id']);

        if (isset($data['joint_client_holder_id'])) {
            $clientSignature = $clientSignature->where('joint_client_holder_id', $data['joint_client_holder_id']);
        }

        if (isset($data['client_contact_person_id'])) {
            $clientSignature = $clientSignature->where('client_contact_person_id', $data['client_contact_person_id']);
        }

        if (isset($data['commission_recepient_id'])) {
            $clientSignature = $clientSignature->where('commission_recepient_id', $data['commission_recepient_id']);
        }

        return $clientSignature->first();
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $user = null;

        if ($data['signature_person'] === 'new') {
            $user = $client->clientUsers()
                ->whereDoesntHave('clientSignatures')
                ->where('email', $data['email_address'])
                ->first();
        }

        $jointHolder = isset($data['joint_client_holder_id'])
            ? JointClientHolder::findOrFail($data['joint_client_holder_id'])
            : null;

        $contactPerson = isset($data['client_contact_person_id'])
            ? ClientContactPerson::findOrFail($data['client_contact_person_id'])
            : null;

        $relationshipPerson = isset($data['commission_recepient_id'])
            ? CommissionRecepient::findOrFail($data['commission_recepient_id'])
            : null;

        return [
            'client' => $client,
            'jointHolder' => $jointHolder,
            'contactPerson' => $contactPerson,
            'relationshipPerson' => $relationshipPerson,
            'user' => $user
        ];
    }
}
