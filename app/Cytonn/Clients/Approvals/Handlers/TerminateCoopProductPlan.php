<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ProductPlan;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Coop\ProductPlans\CoopProductPlanRepository;

/**
 * Class TerminateCoopProductPlan
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class TerminateCoopProductPlan implements ApprovalHandlerInterface
{
    protected $coopProductPlanRepository;

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $this->coopProductPlanRepository = new CoopProductPlanRepository();

        $this->coopProductPlanRepository->deactivateProductPlan($data['id']);



        \Flash::success('Coop product plan was successfully terminated!');
        $next = \redirect('/dashboard/clients/details/' . $data['client_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $product_plan = ProductPlan::findOrFail($data['id']);

        return ['data' => $data, 'product_plan' => $product_plan];
    }
}
