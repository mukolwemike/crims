<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestPaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInterestSchedule;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Support\Facades\Response;

class StoreUnitFundInterestSchedule implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['old_interest_payment_start_date', 'old_interest_payment_interval']);

        if (is_null($input['id'])) {
            $existingSchedule = UnitFundInterestSchedule::where('client_id', $input['client_id'])
                ->where('unit_fund_id', $input['unit_fund_id'])->exists();

            if ($existingSchedule) {
                $message = 'The client already has an existing interest schedule. Please edit it to update the details';

                throw new ClientInvestmentException($message);
            }

            $schedule = UnitFundInterestSchedule::create($input);
        } else {
            $schedule = UnitFundInterestSchedule::findOrFail($input['id']);

            $schedule->update($input);
        }

        $startDate = is_null($input['interest_payment_start_date']) ? Carbon::now()
            : Carbon::parse($input['interest_payment_start_date']);

        $nextPaymentDate = $schedule->getNextPayment($startDate, $schedule->interest_payment_interval);

        $schedule->next_payment_date = $nextPaymentDate;

        $schedule->save();

        \Flash::success('The unit fund interest payment schedule has been saved');

        $next = \redirect("/dashboard/unitization/unit-funds/" . $schedule->unit_fund_id . "/unit-fund-clients/"
            . $schedule->client_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $unitFund = UnitFund::findOrFail($data['unit_fund_id']);

        $schedule = is_null($data['id']) ? null : UnitFundInterestSchedule::findOrFail($data['id']);

        return [
            'data' => $data,
            'client' => $client,
            'unitFund' => $unitFund,
            'schedule' => $schedule
        ];
    }
}
