<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientRepository;
use Illuminate\Support\Facades\Redirect;

/**
 * Class AddTaxExemption
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class AddTaxExemption implements ApprovalHandlerInterface
{
    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        (new ClientRepository())->uploadTaxExemption($data, $data['document_id']);



        \Flash::success('Tax Exemption Details successfully added');
        $next = Redirect::to('/dashboard/clients/details/'. $data['client_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        return [
            'data' => $data,
            'client' => $client
        ];
    }
}
