<?php
/**
 * Date: 21/04/2016
 * Time: 4:27 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Clients\Approvals\Events\ApprovalIsReady;
use Cytonn\Investment\Approvals\Events\Actions\InvestmentActionApproved;
use Cytonn\Clients\Approvals\SchedulableApproval;
use Exception;
use Cytonn\Exceptions\ClientInvestmentException as Error;
use DB;

/**
 * Class ApprovalHandler
 *
 * @package Cytonn\Investment\Approvals\Handlers
 */
class ApprovalHandler extends \Cytonn\Handlers\EventListener
{

    /**
     * Handle the investment action approval
     * Currently used for rollover and withdraw transactions
     *
     * @param InvestmentActionApproved $action
     */
    public function whenInvestmentActionApproved(InvestmentActionApproved $action)
    {
        $approval = $action->approval;

        if ($action->handlerClass instanceof SchedulableApproval) {
            $action->handlerClass->setRunningSchedule($action->runningSchedule);
        }

        $action->handlerClass->handle($approval);
    }

    /**
     * Handle a transaction when approved
     *
     * @param  ApprovalIsReady $event
     * @throws Error
     * @throws Exception
     * @return null
     */
    public function whenApprovalIsReady(ApprovalIsReady $event)
    {
        $handler = new Approval($event->approval);

        //run the action
        DB::transaction(
            function () use ($handler, $event) {
                $handler->approve();
            }
        );
    }
}
