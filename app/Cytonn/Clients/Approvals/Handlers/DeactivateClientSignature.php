<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/26/18
 * Time: 4:49 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientSignature;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;

class DeactivateClientSignature implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $signature = ClientSignature::findOrFail($data['client_signature_id']);

        $signature->active = !$signature->active;

        $signature->save();



        \Flash::success('Client signature approved successfully');

        $next = \Redirect::to('dashboard/clients/details/'.$signature->client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $signature = ClientSignature::findOrFail($data['client_signature_id']);

        return [
            'data' => $data,
            'signature' => $signature
        ];
    }
}
