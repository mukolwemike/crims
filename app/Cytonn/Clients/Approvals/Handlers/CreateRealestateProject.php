<?php
/**
 * Date: 19/05/2016
 * Time: 9:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\ProjectType;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Realestate\Commissions\Calculator;

/**
 * Class CreateRealEstateProject
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class CreateRealestateProject implements ApprovalHandlerInterface
{

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $project = new Project();

        if (isset($data['project_id'])) {
            $project = Project::findOrFail($data['project_id']);

            unset($data['project_id']);
        }

        $data['type_id'] = (int)$data['type_id'];
        $project->fill($data);
        $project->fund_manager_id = 1; //TODO change to dynamic

        $project->save();

        \Flash::success('The project has been saved');

        $next = \Redirect::to('/dashboard/realestate');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $calculators = (new Calculator())->getCalculatorList();
        $projectType = ProjectType::find($data['type_id']);
        $custodialAccount = isset($data['custodial_account_id'])
            ? CustodialAccount::find($data['custodial_account_id']) : new CustodialAccount();
        $currency = isset($data['currency_id']) ? Currency::find($data['currency_id']) : new Currency();
        $project = function ($id) {
            return Project::find($id);
        };
        return [
            'calculators' => $calculators,
            'projectType' => $projectType,
            'project' => $project,
            'custodialAccount' => $custodialAccount,
            'currency' => $currency
        ];
    }
}
