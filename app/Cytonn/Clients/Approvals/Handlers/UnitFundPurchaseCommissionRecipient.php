<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Unitization\Commissions\Generators\Generator;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Unitization\UnitFundCommissionRepository;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class UnitFundPurchaseCommissionRecipient implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $purchase = UnitFundPurchase::findOrFail($data['fund_purchase_id']);

        $recipient = CommissionRecepient::findOrFail($data['commission_recipient_id']);

        $commission = $purchase->unitFundCommission;

        if ($commission) {
            (new UnitFundCommissionRepository($purchase, $purchase->price, $recipient))->updateCommission();
        } else {
            (new UnitFundCommissionRepository($purchase, $purchase->price, $recipient))->saveCommission();
        }

        (new Generator())->createSchedules($purchase->fresh());

        \Flash::success('Unit fund commission recipient has been updated successfully');

        $next = \Redirect::to('/dashboard/unitization/unit-funds/'.$purchase->unitFund->id.'/unit-fund-client/'
        .$purchase->client->id.'/unit-fund-purchases/'.$purchase->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $purchase = UnitFundPurchase::findOrFail($data['fund_purchase_id']);

        $fund = $purchase->unitFund;

        $newRecipient = CommissionRecepient::findOrFail($data['commission_recipient_id']);

        $oldRecipient = $purchase->getRecipient();

         return [
            'data'=>$data,
            'purchase' => $purchase,
            'fund' => $fund,
            'newRecipient' => $newRecipient,
            'oldRecipient' => $oldRecipient
         ];
    }
}
