<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/17/16
 * Time: 1:06 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ContactMethod;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Clients\ClientRepository;

class EditJointHolder implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * EditJointHolder constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);


        if ((new ClientRepository())->editJointHolder($approval->payload)) {
            \Flash::success('Joint Holder details were successfully saved');
        } else {
            throw new ClientInvestmentException('Could not save joint holder details');
        }

        $next = \Redirect::to('/dashboard/investments');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $oldJointHolder = ClientJointDetail::find($data['joint_holder_id']);

        $holder = new ClientJointDetail();
        $holder->fill($data);

        $holder->methodsOfContact =
            ($holder->method_of_contact_id)? ContactMethod::find($holder->method_of_contact_id)->description: '';

        return['holder'=>$holder, 'data'=>$data, 'oldJointHolder' => $oldJointHolder];
    }
}
