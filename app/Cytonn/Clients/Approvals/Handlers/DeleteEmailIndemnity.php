<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\EmailIndemnity;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class DeleteEmailIndemnity implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return void
     * @throws \Exception
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $indemnity = EmailIndemnity::find($data['indemnity_id']);

        $indemnity->delete();

        $action = $data['action'] . 'd';

        \Flash::success("Email indemnity has been $action");

        $next = \Redirect::to('/dashboard/clients/details/' . $client->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $indemnity = EmailIndemnity::withTrashed()->find($data['indemnity_id']);

        return ['data' => $data, 'client' => $client, 'indemnity' => $indemnity];
    }
}
