<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client\TaxExemption;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Document;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Redirect;

/**
 * Class DeleteTaxExemption
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class DeleteTaxExemption implements ApprovalHandlerInterface
{
    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws \Exception
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $taxExemption = TaxExemption::findOrFail($data['tax_id']);

        $taxExemption->update(['reason' => $data['reason']]);

        $this->deleteDocument($taxExemption->document_id);

        $taxExemption->delete();



        \Flash::success('Tax Exemption Details successfully deleted');

        $next = Redirect::to('/dashboard/clients/details/' . $data['id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $taxExemption = TaxExemption::find($data['tax_id']);

        return [
            'data' => $data, 'taxExemption' => $taxExemption
        ];
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function deleteDocument($id)
    {
        $document = Document::findOrFail($id);

        $document->delete();
    }
}
