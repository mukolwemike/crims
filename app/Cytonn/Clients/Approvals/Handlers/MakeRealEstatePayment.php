<?php
/**
 * Date: 06/06/2016
 * Time: 8:24 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Cytonn\Models\RealestateCommissionRate;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\Title;
use App\Cytonn\Models\UnitHolding;
use App\Events\RealEstate\ClientHoldingPaymentHasBeenCompleted;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Custodial\Payments\Payment as Custodial_Payment;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Events\PaymentHasBeenMade;
use Cytonn\Realestate\Events\ReservationFeesPaid;
use Cytonn\Realestate\Payments\Payment;
use Cytonn\Realestate\Tranches\RealEstateUnitTrancheRepository;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class MakeRealEstatePayment extends AbstractApproval
{
    use EventGenerator, DispatchableTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        $holding->client->repo->checkReGaurds($approval);

        if (isset($data['payment_plan_id'])) {
            $holding->payment_plan_id = $data['payment_plan_id'];
            $holding->save();
        }

        $schedule = isset($data['schedule_id']) ? RealEstatePaymentSchedule::findOrFail($data['schedule_id']) : null;

        if (is_null($schedule)) {
            $this->makeMultiplePayment($approval, $holding, $data);
        } else {
            $this->makePayment($approval, $holding, $schedule, $data);
        }

        if ($holding->repo->getTotalUnpaidSchedulesAmount() == 0) {
            event(new ClientHoldingPaymentHasBeenCompleted($holding));
        }

        \Flash::success('Payment was successfully made');

        $url = '/dashboard/realestate/projects/' . $holding->project->id . '/units/show/' . $holding->unit_id;

        $next = \Redirect::to($url);

        $this->dispatchEventsFor($this);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function makeMultiplePayment(ClientTransactionApproval $approval, UnitHolding $holding, $data = [])
    {
        $amount = $data['amount'];

        $schedules = $holding->paymentSchedules()->inPricing()->get();

        foreach ($schedules as $schedule) {
            if (!$schedule->paid() && $amount > 0) {
                $amount_to_be_paid = $schedule->amountNotPaid();

                if ($amount_to_be_paid > $amount) {
                    $amount_to_be_paid = $amount;
                }

                $amount = $amount - $amount_to_be_paid;

                $data['amount'] = $amount_to_be_paid;

                $this->makePayment($approval, $holding, $schedule, $data);
            }
        }

        if ($amount > 0) {
            throw new ClientInvestmentException("The amount for payment is more the amount not paid for the
             unit by " . $amount);
        }

        return;
    }

    public function makePayment(
        ClientTransactionApproval $approval,
        UnitHolding $holding,
        RealEstatePaymentSchedule $schedule,
        $data = []
    ) {
        $data['date'] = new Carbon($data['date']);

        $existingPaymentCount = $holding->payments()->count();

        //add the payment
        $payment = RealEstatePayment::make($holding, $schedule, $data['date'], $data['amount'], $data['description']);

        $payment->approve($approval);

        if ($existingPaymentCount == 0) {
            if (isset($data['recipient_id'])) {
                if (!isset($data['rate'])) {
                    $data['rate'] = 0;
                }

                if (!isset($data['reason'])) {
                    $data['reason'] = null;
                }

                (new Payment($payment))
                    ->initCommission(
                        $data['recipient_id'],
                        $data['awarded'],
                        $data['rate'],
                        $data['reason']
                    );
            }

            if (!isset($data['negotiated_price'])) {
                $data['negotiated_price'] = null;
            }

            if (isset($data['discount'])) {
                if ($data['discount'] > 0) {
                    $holding->discount = $data['discount'];
                    $holding->save();
                }
            }

            if (isset($data['tranche_id']) && !$holding->tranche) {
                $tranche = RealEstateUnitTranche::findOrFail($data['tranche_id']);
                $trancheRepo = new RealEstateUnitTrancheRepository($tranche);
                if ($trancheRepo->trancheCheck($holding)) {
                    return \Redirect::back();
                };
                $setter = new Payment();
                $setter->addPricingInformation($holding, $tranche, $data['negotiated_price']);
            }

            if (isset($data['client_code']) && !$holding->client->client_code) {
                $holding->client->client_code = $data['client_code'];
                $holding->client->save();
            }
        }

        //approve reservation form
        if (!$holding->approval) {
            $holding->approve($approval);

            \Flash::message('Unit reservation successfully confirmed');

            $this->raise(new ReservationFeesPaid($payment));
        }

        // Transaction
        $date = Carbon::parse($data['date']);

        $description = 'RE Payment for unit number ' . $holding->unit->number . ' ' . $holding->unit->size->name . ' '
            . $holding->unit->type->name . ' in ' . $holding->project->name;

        // Client Payment
        $p = Custodial_Payment::make(
            $holding->client,
            $holding->commission->recipient,
            null,
            null,
            $holding->project,
            null,
            $date,
            $data['amount'],
            'I',
            $description
        )->credit();

        $payment->clientPayment()->associate($p);

        $payment->save();

        $payment->raise(new PaymentHasBeenMade($payment));

        $payment->dispatchEventsFor($payment);

        return $payment;
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $instruction = (isset($data['instruction_id']))
            ? RealEstateInstruction::findOrFail($data['instruction_id'])
            : null;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        $paymentSchedules = $holding->paymentSchedules;

        $schedule = isset($data['schedule_id']) ? RealEstatePaymentSchedule::find($data['schedule_id']) : null;

        $reservationForm = $holding->reservation;

        $existing_payments = RealEstatePayment::where('holding_id', $holding->id)->get();

        $price = $holding->price();


        if (isset($data['tranche_id']) && strlen(trim($data['tranche_id'])) > 0 && $price == 0) {
            $tranche = RealEstateUnitTranche::find($data['tranche_id']);

            try {
                $payment_plan = RealEstatePaymentPlan::find($data['payment_plan_id']);

                $price = $tranche->repo->getUnitPrice($holding->unit, $payment_plan);
            } catch (\Exception $e) {
                $price = 0;
            }
        }

        $unittranche = function ($id) {
            return RealEstateUnitTranche::find($id);
        };
        $paymentplan = function ($id) {
            return RealEstatePaymentPlan::find($id);
        };

        $commissionrecepient = function ($id) {
            return CommissionRecepient::find($id);
        };

        $commissionrate = function ($id) {
            return RealestateCommissionRate::findOrFail($id);
        };

        $titles = Title::all();

        return [
            'holding' => $holding,
            'form' => $holding->reservation,
            'project' => $holding->project,
            'unit' => $holding->unit,
            'schedule' => $schedule,
            'reservationForm' => $reservationForm,
            'joints' => $holding->reservation->jointClients,
            'price' => $price,
            'existing_payments' => $existing_payments,
            'unittranche' => $unittranche,
            'paymentplan' => $paymentplan,
            'commissionrecepient' => $commissionrecepient,
            'titles' => $titles,
            'paymentSchedules' => $paymentSchedules,
            'commissionrate' => $commissionrate,
            'instruction' => $instruction
        ];
    }

    protected function pluck($name)
    {
        $data = $this->approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        switch ($name) {
            case 'name':
                return ClientPresenter::presentFullNames($this->approval->client_id);
            case 'project':
                return $holding->project->name;
            case 'unit':
                return $holding->unit->number;
            case 'size':
                $unit = $holding->unit;
                return $unit->size->name . ' ' . $unit->type->name;
            case 'amount':
                return \Cytonn\Presenters\AmountPresenter::currency($data['amount']);
            case 'date':
                return $data['date'];
            case 'payment':
                $schedule = RealEstatePaymentSchedule::find($data['schedule_id']);
                return $schedule->description;
            case 'price':
                return \Cytonn\Presenters\AmountPresenter::currency($holding->price());
        }
    }
}
