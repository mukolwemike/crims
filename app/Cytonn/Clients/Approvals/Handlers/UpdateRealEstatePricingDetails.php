<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealestateCommissionRate;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\RealEstateUnitTranche;
use App\Cytonn\Models\UnitHolding;
use App\Events\RealEstate\UnitHasBeenEdited;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Realestate\Commissions\BaseCalculator;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class UpdateRealEstatePaymentDetails
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class UpdateRealEstatePricingDetails implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;

    /**
     * Make the payment
     *
     * @param  ClientTransactionApproval $approval
     * @return null
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        //Check for real estate guards
        $holding->client->repo->checkReGaurds($approval);

        // create cotract variation record
        $original_data = [
            'loo_signed_date' => $holding->loo->sent_on,
            'unit_size' => $holding->unit->size->name,
            'unit_number' => $holding->unit->number,
            'unit_price' => (int) $holding->price(),
        ];

        event(new UnitHasBeenEdited($holding, $original_data));

        //Update
        if (isset($data['discount'])) {
            $holding->discount = $data['discount'];
        }
        $holding->tranche_id = $data['tranche_id'];
        $holding->payment_plan_id = $data['payment_plan_id'];
        $holding->negotiated_price = $data['negotiated_price'];
        $holding->save();

        $this->updateCommission($holding, $data);

        \Flash::success('Payment Information was successfully updated');

        $next = \Redirect::to('/dashboard/realestate/projects/' . $holding->project->id .
            '/units/show/' . $holding->unit_id);


        $this->dispatchEventsFor($this);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    protected function updateCommission($holding, $data)
    {
        $holding->commission->recipient_id = $data['recipient_id'];

        $award = boolval($data['awarded']);

        if ($award && isset($data['rate'])) {
            $holding->commission->commission_rate = $data['rate'];
        } else {
            $holding->commission->commission_rate = 0;
        }

        if ($holding->commission->amount) {
            (new BaseCalculator())->saveTotalCommission($holding);
        }

        $holding->commission->awarded = $award;
        $holding->commission->save();
    }

    /**
     * Create vars required in the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $holding = UnitHolding::findOrFail($data['holding_id']);
        $realestateUnitTranche = RealEstateUnitTranche::findOrFail($data['tranche_id']);
        $realestatePaymentPlan = RealEstatePaymentPlan::findOrFail($data['payment_plan_id']);
        $commissionRecepient = CommissionRecepient::findOrFail($data['recipient_id']);
        $realestateCommissionRate = function ($id) {
            return RealestateCommissionRate::findOrFail($id);
        };

        return [
            'holding' => $holding, 'project' => $holding->project, 'unit' => $holding->unit,
            'realestateUnitTranche' => $realestateUnitTranche, 'realestatePaymentPlan' => $realestatePaymentPlan,
            'commissionRecepient' => $commissionRecepient, 'realestateCommissionRate' => $realestateCommissionRate
        ];
    }
}
