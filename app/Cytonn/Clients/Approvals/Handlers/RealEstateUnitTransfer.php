<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class RealEstateUnitTransfer implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;
        $holding = UnitHolding::findOrFail($data['holding_id']);

        //Check for real estate guards
        $holding->client->repo->checkReGaurds($approval);

        $holding->update(['client_id' => $data['client_id']]);

        \Flash::success('The unit transfer has been approved');
        $next = \redirect('/dashboard/realestate/projects');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $holding = UnitHolding::findOrFail($data['holding_id']);
        $current_client = $holding->client;
        $new_client = Client::findOrFail($data['client_id']);

        return ['project' => $holding->project, 'unit' => $holding->unit,
            'current_client' => $current_client, 'new_client' => $new_client, 'holding' => $holding];
    }
}
