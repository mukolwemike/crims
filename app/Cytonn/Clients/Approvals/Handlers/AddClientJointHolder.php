<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 04/06/2018
 * Time: 16:36
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\JointClientHolder;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\JointInvestorAddCommand;
use Laracasts\Commander\CommanderTrait;

class AddClientJointHolder implements ApprovalHandlerInterface
{
    use CommanderTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $client = Client::findOrFail($approval->client_id);

        $holders = $approval->payload;

        if ($holders) {
            return $this->saveJointHolder($client, $holders);
        } else {
            throw new ClientInvestmentException("There are joint hoders to add");
        }



        \Flash::success('Client joint holder added successfully');

        $next = \Redirect::to("/dashboard/clients/details/{$client->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function saveJointHolder(Client $client, $data)
    {
        return collect($data)->each(
            function ($holder) use ($client) {

                $holder = (collect($holder))->toArray();

                $holder = array_add($holder, 'joint_id', null);
                $holder = array_add($holder, 'client_id', $client->id);
                $holder = array_add($holder, 'existing_client_id', $client->id);

                return $this->execute(JointInvestorAddCommand::class, [ 'data' => $holder ]);
            }
        );
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($approval->client_id);

        $holders = collect($data);

        return [
            'client' => $client,
            'holders' => $holders
        ];
    }
}
