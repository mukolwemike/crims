<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Shares\ShareTradingRepository;
use Cytonn\Shares\Trading\Buy;

/**
 * Class BuyEntityShares
 *
 * @package Cytonn\Clients\Approvals\Handlers
 */
class BuyEntityShares implements ApprovalHandlerInterface
{
    protected $repo;

    /**
     * MakeSharesPurchaseOrder constructor.
     *
     * @param $
     */
    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $buyer = ShareHolder::findOrFail($data['buyer_id']);

        $entity = $buyer->entity;

        $price = isset($data['exemption']) ? $data['exemption'] : $entity->sharePrice($data['request_date']);

        $cost = $price * $data['number'];

        $data['approval_id'] = $approval->id;

        // Check if the buyer has enough funds to buy the shares
        if ((double) $cost > $buyer->sharePaymentsBalance() + $cost) {
            throw new ClientInvestmentException('The buyer does not have enough funds to buy the number of shares.');
        }


        // Check if the entity has enough shares to meet the request
        if ((double) $data['number'] > $entity->remainingShares($data['request_date'])) {
            throw new ClientInvestmentException('The entity does not have enough shares to match the request.');
        }

        (new Buy($buyer))->fromEntity($entity, $data);



        \Flash::success('Entity shares were successfully purchased!');
        $next = \redirect('/dashboard/shares/purchases/shareholders/'.$buyer->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @inheritdoc
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $buyer = ShareHolder::findOrFail($data['buyer_id']);
        $entity = $buyer->entity;
        $price = isset($data['exemption']) ? $data['exemption'] : $entity->sharePrice($data['request_date']);

        return ['data'=>$data, 'buyer'=>$buyer, 'entity'=>$entity, 'price'=>$price];
    }
}
