<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/19/16
 * Time: 9:29 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\InterestAction;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CombinedRollover extends AbstractApproval
{
    use DispatchableTrait, EventGenerator;

    protected $formats = [
        'amount' => 'amount',
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'interest_rate' => 'append:%',
        'tenor' => 'append: days',
        'commission_rate' => 'append:%',
        'interest_payment_interval' => 'append: months'
    ];

    /**
     * @param ClientTransactionApproval $approval
     * @return mixed
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        dd('This is now disabled');
    }

    public function getAmounts($approval)
    {
        $data = $approval->payload;

        $investments = ClientInvestment::whereIn('id', json_decode($data['investments']))->get();

        $total_value = $investments->sum(
            function ($inv) {
                if ($inv->withdrawn) {
                    throw new ClientInvestmentException(
                        'One or more of the investments has been withdrawn, it cannot be rolled over'
                    );
                }

                return $inv->repo->getTotalValueOfAnInvestment();
            }
        );

        if ($data['reinvest'] == 'topup') {
            $amount_reinvested = $total_value + $data['amount'];

            $amount_withdraw = 0;
        } elseif ($data['reinvest'] == 'reinvest') {
            if ($data['amount'] > $total_value) {
                throw new ClientInvestmentException(
                    'The amount withdrawn/rolled over cannot be more than the total value 
                    of the combined investments'
                );
            }

            $amount_reinvested = $data['amount'];

            $amount_withdraw = $total_value - $amount_reinvested;
        } elseif ($data['reinvest'] == 'withdraw') {
            if ($data['amount'] > $total_value) {
                throw new ClientInvestmentException(
                    'The amount withdrawn/reinvested cannot be more 
                than the total value of the combined investments'
                );
            }

            $amount_reinvested = $total_value - $data['amount'];

            $amount_withdraw = $data['amount'];
        } else {
            throw new ClientInvestmentException('Transaction unsupported: topup, withdraw or reinvest only');
        }

        return ['reinvest' => $amount_reinvested, 'withdraw' => $amount_withdraw];
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $interestPayment = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months', 5 => 'Every 5 months',
            6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months', 8 => 'Every 8 months',
            9 => 'Every 9 months', 10 => 'Every 10 months', 11 => 'Every 11 months',
            12 => 'Every 12 months (Annually)'];

        $investment = ClientInvestment::findOrFail($data['investment']);

        $value = $investment->repo->getTotalValueOfAnInvestment() + $investment->withdraw_amount;

        $commissionRecepient = CommissionRecepient::find($data['commission_recepient']);

        $interestAction = function ($id) {
            return InterestAction::findOrFail($id);
        };

        $investments = new \Illuminate\Support\Collection();

        foreach (json_decode($data['investments']) as $inv_id) {
            $investmt = ClientInvestment::withTrashed()->find($inv_id); //show trashed
            $investmt->value = $investmt->repo->getTotalValueOfAnInvestment() + $investmt->withdraw_amount;
            $investments->push($investmt);
        }

        return [
            'interestPayment' => $interestPayment,
            'investments' => $investments,
            'data' => $data,
            'investment' => $investment,
            'commissionRecepient' => $commissionRecepient,
            'interestAction' => $interestAction
        ];
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function pluck($name)
    {
        $payload = $this->approval->payload;

        switch ($name) {
            case 'product':
                return ClientInvestment::find($this->approval->payload['investment'])->product->name;
            case 'tenor':
                return Carbon::parse($payload['invested_date'])->diffInDays(Carbon::parse($payload['maturity_date']));
            case 'fa_name':
                return CommissionRecepient::find($payload['commission_recepient'])->name;
            case 'amount':
                return $this->getAmounts($this->approval)['reinvest'];
        }
    }
}
