<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Unitization\FundSwitch;
use Cytonn\Unitization\Trading\Buy;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundSwitch implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;
    
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }
    
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $date = Carbon::parse($data['date']);

        $client = Client::findOrFail($data['client_id']);

        $fundFrom = UnitFund::findOrFail($data['fund_id']);

        $fundTo = UnitFund::findOrFail($data['unit_fund_id']);

        (new FundSwitch($client, $fundFrom, $date->copy()))
                    ->to($fundTo)
                    ->switch();



        \Flash::success('Unit fund switch has been successfully saved.');

        $next = \Redirect::to($fundTo->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }
    
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Client::findOrFail($data['client_id']);

        $fundFrom = UnitFund::findOrFail($data['fund_id']);

        $fundTo = UnitFund::findOrFail($data['unit_fund_id']);

        $date = Carbon::parse($data['date']);

        $switch = (new FundSwitch($client, $fundFrom, $date->copy()))->to($fundTo);

        $number = $client->ownedNumberOfUnits($fundFrom, $date);

        $fees = $switch->calculateFees($number);

        $price =  $fundFrom->unitPrice($date);

        return [
            'data' => $data,
            'fundFrom' => $fundFrom,
            'fundTo' => $fundTo,
            'client' => $client,
            'number' => $number,
            'price'  => $price,
            'fees'   => $fees
        ];
    }
}
