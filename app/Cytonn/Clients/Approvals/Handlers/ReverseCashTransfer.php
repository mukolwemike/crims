<?php
/**
 * Date: 06/02/2020
 * Time: 10:54
 *
 * @author Isaac Kipng'eno <ikipngeno@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\CRIMSGeneralException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class ReverseCashTransfer implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     * @throws CRIMSGeneralException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $payment_out = ClientPayment::find($data['payment_id']);

        $payment_in = ClientPayment::where('parent_id', $data['payment_id'])->whereNull('custodial_transaction_id')->first();

        if (!$payment_out || !$payment_in) {
            throw new CRIMSGeneralException('Payment could not be found, it is probably already deleted');
        }

        $payment_out->delete();

        $payment_in->delete();

        $next = Redirect::to('dashboard/investments/client-payments/client/' . $payment_out->client_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        \Flash::success('The transaction has been reversed');

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $payment_out = ClientPayment::withTrashed()->findOrFail($data['payment_id']);

        return [
            'data' => $data,
            'payment_out' => $payment_out,
        ];
    }
}
