<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Sell;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class UnitFundBulkInterestPayment implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        \Queue::push(static::class . '@payInterests', ['approval_id' => $approval->id]);

        \Flash::success('The unit fund investments have been queued for interest payment.');

        $next = \Redirect::to('/dashboard/unitization/unit-funds/' . $approval->payload['unit_fund_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $date = Carbon::parse($data['date']);

        $fund = UnitFund::findOrFail($data['unit_fund_id']);

        return [
            'data' => $data,
            'date' => $date,
            'fund' => $fund
        ];
    }

    /**
     * @param $approval
     * @param $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function action($approval, $action, $param)
    {
        switch ($action) {
            case 'export':
                return $this->export($approval);
                break;
            case 'instruction':
                return $this->generateInstructions($approval);
                break;
            default:
                throw new \InvalidArgumentException("Action $action not supported in " . static::class);
        }
    }

    /**
     * @param $job
     * @param $data
     * @throws \Throwable
     */
    public function payInterests($job, $data)
    {
        $approval = ClientTransactionApproval::find($data['approval_id']);

        $data = $approval->payload;

        $clients = Client::whereIn('id', $data['clients'])->get();

        DB::transaction(function () use ($clients, $approval) {
            $this->processClients($clients, $approval);
        });

        if ($job) {
            $job->delete();
        }
    }

    /**
     * @param $clients
     * @param $approval
     */
    private function processClients($clients, $approval)
    {
        $data = $approval->payload;

        $date = Carbon::parse($data['date']);

        $fund = UnitFund::findOrFail($data['unit_fund_id']);

        $clients->each(function (Client $client) use ($fund, $date, $approval) {
            try {
                $sell = new Sell($fund, $client, $date);

                $sell->payInterest($approval);
            } catch (\Exception $e) {
                $this->notifyOnFailure($e, $approval, $client);
                throw $e;
            }
        });
    }

    /**
     * @param \Exception $exception
     * @param ClientTransactionApproval $approval
     * @param Client $client
     */
    private function notifyOnFailure(\Exception $exception, ClientTransactionApproval $approval, Client $client)
    {
        $mail = Mailer::compose()
            ->to(systemEmailGroups(['super_admins']))
            ->subject('Unit Fund Interest payment bulk approval failed ')
            ->text("Client Id : " . $client->id . ' \n' . $exception->getMessage());

        if ($approval->approver) {
            $mail->to($approval->approver->email);
        }

        $mail->send();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    private function export(ClientTransactionApproval $approval)
    {
        $email = \Auth::user()->email;

        sendToQueue(
            function () use ($approval, $email) {
                $data = $approval->payload;

                $clients = Client::whereIn('id', $data['clients'])->get();

                $date = Carbon::parse($data['date']);

                $fund = UnitFund::findOrFail($data['unit_fund_id']);

                $clients = $clients->map(function (Client $client) use ($date, $fund, $approval) {
                    $bank = $client->bankDetails();

                    $price = $fund->unitPrice($date->copy());

                    $sales = UnitFundSale::where('approval_id', $approval->id)
                        ->where('client_id', $client->id)->get();

                    if (count($sales) > 0) {
                        $amount = $sales->sum(function (UnitFundSale $sale) {
                            return $sale->number * $sale->price;
                        });
                    } else {
                        $amount = $client->unitFundClientRepo->getUnitFundInterestAsAt($fund, $date) * $price;
                    }

                    return [
                        'Payment Date' => $date->toDateString(),
                        'Client Code' => $client->client_code,
                        'Client Name' => ClientPresenter::presentFullNames($client->id),
                        'Email' => $client->contact->email,
                        'Unit Fund' => $fund->name,
                        'Total Units' => $client->repo->ownedNumberOfUnits($fund, $date),
                        'Price' => $price,
                        'Account Name' => @$bank->accountName(),
                        'Account Number' => @$bank->accountNumber(),
                        'Branch' => @$bank->branch(),
                        'Bank' => @$bank->bankName(),
                        'Payment Amount' => $amount,
                    ];
                });

                $fileName = $fund->short_name . ' Interest Payments ' . $data['date'];

                ExcelWork::generateAndStoreSingleSheet($clients, $fileName);

                Mailer::compose()
                    ->to($email)
                    ->bcc(config('system.administrators'))
                    ->subject($fileName)
                    ->text('Please find attached the ' . $fund->name . ' interest payments as at ' .
                        $data['date'])
                    ->excel([$fileName])
                    ->send();

                $path = storage_path().'/exports/' . $fileName .'.xlsx';

                \File::delete($path);
            }
        );

        \Flash::message('Report will be sent to your email');

        return \redirect()->back();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Http\RedirectResponse
     */
    private function generateInstructions(ClientTransactionApproval $approval)
    {
        $email = \Auth::user()->email;

        sendToQueue(function () use ($approval, $email) {
            $unitFund = UnitFund::find($approval->payload['unit_fund_id']);

            $data = BankInstruction::where('approval_id', $approval->id)
                ->has('custodialTransaction')
                ->get()
                ->map(
                    function (BankInstruction $instruction) {
                        $transaction = $instruction->custodialTransaction;
                        $client = $transaction->client;
                        $payment = $transaction->clientPayment->parent;

                        $unitSale = UnitFundSale::where('client_payment_id', $payment->id)->first();

                        $fund = $unitSale->fund;

                        $fund_name_uq = $fund->name;

                        $currency = $fund->currency;

                        $bank = $client->bankDetails();

                        $out = new EmptyModel();

                        $unique = 'client_id_' . $client->id . '_bank_acc_id_' . $bank->id .
                            '_fund_' . $fund_name_uq . '_curr_' . $currency->id;

                        $out->fill([
                            'Code' => $client->client_code,
                            'Beneficiary Account Name' => $bank->accountName,
                            'Account Number' => $bank->accountNumber,
                            'Payment Details' => 'Interest Payment',
                            'Currency' => $currency->code,
                            'Payment Amount' => $instruction->amount,
                            'Swift Code' => $bank->swiftCode,
                            'Beneficiary E-Mail' => $client->contact->email,
                            'Payment Type' => $bank->bankName == 'Standard Chartered Bank' ? 'BT' : 'RTGS',
                            'unique' => $unique,
                            'Fund' => $fund->short_name . ' - ' . $currency->code
                        ]);

                        return $out;
                    }
                )->sortBy('Code')
                ->groupBy('unique')
                ->map(
                    function ($group) {
                        $instr = $group->first();
                        $instr->{'Payment Amount'} = (float)$group->sum('Payment Amount');
                        unset($instr->unique);

                        return [$instr];
                    }
                )
                ->flatten(1)
                ->groupBy('Fund');

            $fileName = $unitFund->short_name . 'Interest_Payment_Instruction';

            ExcelWork::generateAndStoreMultiSheet($data, $fileName);

            Mailer::compose()
                ->to($email)
                ->bcc(config('system.administrators'))
                ->subject($unitFund->short_name  . ' Interest Payment Instruction')
                ->excel([$fileName])
                ->text('PFA')
                ->send();
        });

        \Flash::message('Sending to email');

        return redirect()->back();
    }
}
