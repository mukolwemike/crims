<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/17/16
 * Time: 12:14 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestAction;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Action\PayInterest;
use Cytonn\Investment\Forms\PaymentForm;
use Illuminate\Support\Facades\App;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;
use Cytonn\Investment\InvestmentTransaction;
use Cytonn\Investment\Events\ClientPaymentIsDue;
use Cytonn\Core\DataStructures\Carbon;

class InterestPayment implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $authorizer;

    /**
     * InterestPayment constructor.
     *
     * @param $authorizer
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        dd('should not be used');
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $investment = ClientInvestment::find($data['investment_id']);
        $interestaction = InterestAction::findOrFail($data['interest_action_id']);

        return[
            'investment'=>$investment,
            'data'=>$data,
            'interestaction'=>$interestaction
        ];
    }
}
