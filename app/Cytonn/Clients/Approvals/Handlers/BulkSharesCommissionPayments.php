<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionOverrideStructure;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecipientType;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Crims\Commission\Override;
use Crims\Shares\Commission\RecipientCalculator;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Mailers\GeneralMailer;
use Cytonn\Models\Shares\ShareCommissionPayment;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\Shares\Commission\Payment;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class BulkSharesCommissionPayments implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        throw new ClientInvestmentException('This approval function is disabled. Please used the combined approval');

        \Queue::push(static::class.'@makePayments', ['approval_id' => $approval->id]);

        $next = \Redirect::back();

        \Flash::message('Shares commission has been approved');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function makePayments($job, $data)
    {
        DB::transaction(
            function () use ($job, $data) {
                $approval = ClientTransactionApproval::find($data['approval_id']);

                $recipients = CommissionRecepient::active()->get();

                $bcp = BulkCommissionPayment::find($approval->payload['bcp_id']);

                $recipients->each(
                    function ($recipient) use ($approval, $bcp) {
                        (new Payment())->make($recipient, $approval, $bcp);
                    }
                );

                $job->delete();
            }
        );
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $bulk = BulkCommissionPayment::findOrFail($data['bcp_id']);

        $recipients = $this->getShareRecipients($bulk->start, $bulk->end)->paginate('10');

        $calculate = function (CommissionRecepient $recipient) use ($approval, $bulk) {
            if (!$approval->approved) {
                return (new RecipientCalculator($recipient, $bulk->start, $bulk->end))->summary()->total();
            } else {
                return $this->getPaymentsMade($recipient, $approval);
            }
        };

        return [
            'data' => $data,
            'recipients' => $recipients,
            'calculate' => $calculate,
            'bulk' => $bulk
        ];
    }

    public function getShareRecipients(Carbon $start, Carbon $end)
    {
        return CommissionRecepient::active()->whereHas(
            'shareCommissionPaymentSchedules',
            function ($schedule) use ($start, $end) {
                $schedule->where('date', '>=', $start)->where('date', '<=', $end);
            }
        );
    }

    private function getPaymentsMade(CommissionRecepient $recipient, ClientTransactionApproval $approval)
    {
        return ShareCommissionPayment::where('commission_recipient_id', $recipient->id)
            ->where('approval_id', $approval->id)
            ->sum('amount');
    }

    public function action(ClientTransactionApproval $approval, $actionName, $params)
    {
        switch ($actionName) {
            case 'excel':
                \Queue::push(
                    static::class.'@exportJob',
                    ['approval_id'=>$approval->id, 'user_id'=>\Auth::user()->id]
                );

                \Flash::message('The excel report is being processed, it will be sent to your email when complete');

                return \Redirect::back();
                break;
            default:
                return \App::abort(404);
        }
    }

    public function exportJob($job, $data)
    {
        $this->export($data['approval_id'], $data['user_id']);

        $job->delete();
    }

    public function export($approval_id, $user_id)
    {
        ini_set('memory_limit', '512M');

        ini_set('max_execution_time', 300);

        $approval = ClientTransactionApproval::find($approval_id);

        $user = User::find($user_id);

        $data = $this->prepareView($approval);

        $filename = 'Shares Commission Payments';

        $overrideStructure = CommissionOverrideStructure::where('slug', 'shares')
            ->where('date', '<=', $data['bulk']->end)
            ->latest('date')
            ->first();

        $models = function ($type) use ($data) {
            $dataArray = array();

            $recipients = CommissionRecepient::active()->where('recipient_type_id', $type->id)->get();

            foreach ($recipients as $recipient) {
                $total = $data['calculate']($recipient);

                if ($total != 0) {
                    $r = new EmptyModel();
                    $r->{'Employee ID'} = $recipient->employee_id;
                    $r->{'Employee Number'} = $recipient->employee_number;
                    $r->Name = $recipient->name;
                    $r->{'Active'} = BooleanPresenter::presentYesNo($recipient->active);
                    if ($type->slug == 'ifa') {
                        $r->{'Account Number'} = $recipient->account_number;
                        $r->{'KRA Pin'} = $recipient->kra_pin;
                        $r->{'Bank Code'} = $recipient->bank_code;
                        $r->{'Branch Code'} = $recipient->branch_code;
                    }
                    $calc = $data['calculate'];
                    $r->Amount = $calc($recipient);

                    $dataArray[] = $r;
                }
            }

            return collect($dataArray);
        };

        \Excel::create(
            $filename,
            function ($excel) use ($models, $data, $overrideStructure) {
                foreach (CommissionRecipientType::all() as $type) {
                    $excel->sheet(
                        $type->name,
                        function ($sheet) use ($models, $type) {
                            $sheet->fromModel($models($type));
                        }
                    );
                }

                // Compliant Commission
                foreach (CommissionRecepient::active()->get() as $recipient) {
                    $bulk = $data['bulk'];

                    $calculator = $recipient->shareCommissionCalculator($bulk->start, $bulk->end);

                    $overrideRate = $this->getOverrideRate($recipient, $overrideStructure);

                    $compliant = $calculator->schedules()
                        ->orderBy('date')
                        ->get();

                    $recipientSummary = $calculator->summary();

                    $reports = $calculator->getReports()->each(function ($fa) use ($recipientSummary, $overrideRate) {
                        $fa->commission = $recipientSummary->overrideCommission($fa);

                        $fa->override = $recipientSummary->overrideOnRecipient($fa);

                        $fa->rate = $overrideRate;
                    });

                    $override = $recipient->shareCommissionCalculator($bulk->start, $bulk->end)->summary()->override();
                    $commission = $recipient->shareCommissionCalculator($bulk->start, $bulk->end)->summary()->total();

                    if (($override + $commission) != 0) {
                        $excel->sheet(
                            str_limit($recipient->name, 20),
                            function ($sheet) use ($compliant, $reports, $override) {
                                $sheet->loadView(
                                    'shares.exports.commission_schedules',
                                    ['schedules' => $compliant, 'override' => $override, 'reports' => $reports]
                                );
                            }
                        );
                    }
                }
            }
        )->store('xlsx');

        $file_path = storage_path('/exports/'.$filename.'.xlsx');

        $this->mailResult($filename, $user);

        \File::delete($file_path);
    }

    /**
     * @param $file_path
     * @param $user
     */
    private function mailResult($filename, $user)
    {
        Mailer::compose()
            ->to($user->email)
            ->bcc(config('system.administrators'))
            ->subject('CRIMS Shares Commission Report')
            ->text('Please find attached the shares commissions report')
            ->excel([$filename])
            ->send();
    }

    /*
     * Get the override rate to use
     */
    public function getOverrideRate($recipient, $structure)
    {
        $rate = $structure->rates()->where('rank_id', $recipient->rank_id)->first();

        if (!$rate) {
            return 0;
        }

        return $rate->rate;
    }
}
