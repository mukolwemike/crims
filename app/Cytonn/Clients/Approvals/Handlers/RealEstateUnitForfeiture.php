<?php
/**
 * Date: 29/07/2016
 * Time: 12:52 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Custodial\Payments\Payment;
use Illuminate\Support\Facades\Response;
use App\Cytonn\Models\RealEstateRefund;
use App\Cytonn\Models\UnitHolding;

class RealEstateUnitForfeiture implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        //Check for real estate guards
        $holding->client->repo->checkReGaurds($approval);

        $date = new Carbon($data['forfeiture_date']);

        $holding->forfeit($date, $data['narration']);

        if ($data['amount'] > 0) {
            $description = 'RE Forfeiture for unit number ' . $holding->unit->number . ' ' .
                $holding->unit->size->name . ' ' . $holding->unit->type->name . ' in ' . $holding->project->name;

            $refund = RealEstateRefund::make($holding, $data['amount'], $date, $data['narration']);

            // Client Payment
            $p = Payment::make(
                $holding->client,
                $holding->commission->recipient,
                null,
                null,
                $holding->project,
                null,
                $date,
                $data['amount'],
                'W',
                $description
            )->debit();

            $refund->clientPayment()->associate($p);
            $refund->save();
        }

        \Flash::success('The forfeiture has been approved');

        $next = \redirect('/dashboard/realestate/projects');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $holding = UnitHolding::findOrFail($approval->payload['holding_id']);

        return ['project'=>$holding->project, 'unit'=>$holding->unit];
    }
}
