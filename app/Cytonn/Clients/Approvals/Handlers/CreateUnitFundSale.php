<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Events\Investments\Actions\UnitFundSaleHasBeenMade;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Unitization\Trading\Sell;
use Illuminate\Support\Facades\Auth;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundSale extends AbstractApproval implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * CreateAdvocate constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();

        parent::__construct();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->checkForGuards($approval);

        $data = $approval->payload;

        if (array_key_exists('utility_instruction_id', $data)) {
            $instruction = UtilityBillingInstructions::findOrFail($data['utility_instruction_id']);
            $saleData = [
                'narration' => 'Bill Payment',
                'sale_type' => 'bill_payment'
            ];
            $instructionType = 'Bill Payment';
        } else {
            $instruction = UnitFundInvestmentInstruction::findOrFail($data['instruction_id']);
            $saleData = [];
            $instructionType = 'Unit Sale';
        }

        $client = $instruction->client;

        $date = Carbon::parse($instruction->date);

        $fund = $instruction->unitFund;

        $sellClass = (new Sell($fund, $client, $date));

        $withdrawalCharge = null;

        if ($this->shouldCharge($approval) && $instructionType == "Unit Sale") {
            $withdrawalCharge = $sellClass->chargeTransactionFee($instruction);
        }

        $sale = $sellClass->sell((int)$instruction->number, $saleData);

        if ($withdrawalCharge) {
            $withdrawalCharge->feeCharged->update([
                'unit_fund_sale_id' => $sale->id
            ]);
        }

        $sale->approval()->associate($approval);

        $sale->save();

        $instruction->approval()->associate($approval);

        $instruction->sale()->associate($sale);

        $instruction->save();

        event(new UnitFundSaleHasBeenMade($sale));

        \Flash::success('Fund units have successfully been withdrawn');

        $next = \Redirect::to($fund->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function shouldCharge(ClientTransactionApproval $approval)
    {
        return is_null(Auth::id());
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        if (array_key_exists('utility_instruction_id', $data)) {
            $instruction = UtilityBillingInstructions::findOrFail($data['utility_instruction_id']);
            $instructionType = 'Bill Payment';
        } else {
            $instruction = UnitFundInvestmentInstruction::findOrFail($data['instruction_id']);
            $instructionType = 'Unit Sale';
        }

        $client = $instruction->client;

        $data = json_decode($instruction->payload, true);

        $data['instruction_type'] = $instructionType;

        $date = Carbon::parse($data['date']);

        $fund = $instruction->unitFund;

        $fund->price = $fund->unitPrice($date->copy());

        $sale = new Sell($fund, $client, $date);

        $fees = $sale->calculateFees($instruction->number);

        return [
            'data' => $data,
            'client' => $client,
            'fund' => $fund,
            'fees' => $fees,
            'instruction' => $instruction,
        ];
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return bool
     */
    private function checkForGuards(ClientTransactionApproval $approval)
    {
        if ($approval->exemption) {
            return true;
        }

        $client = $approval->client;

        if ($client->guards()->investmentsGuard()->count() == 0) {
            return true;
        }

        $data = $approval->payload;

        if (array_key_exists('utility_instruction_id', $data)) {
            $instruction = UtilityBillingInstructions::findOrFail($data['utility_instruction_id']);
        } else {
            $instruction = UnitFundInvestmentInstruction::findOrFail($data['instruction_id']);
        }

        $data = json_decode($instruction->payload, true);

        $date = Carbon::parse($data['date']);

        $ownedUnits = $client->repo->ownedNumberOfUnits($instruction->unitFund, $date);

        $client->repo->checkForInvestmentGuards($approval, $ownedUnits - $instruction->number);
    }
}
