<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\ProductType;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Investment\Products\ProductRepository;
use Illuminate\Support\Facades\Response;

class StoreProduct implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $product = new Product();

        $product->repo->save(array_except($data, ['id']), $data['id']);



        \Flash::success('Product has been successfully saved');

        $next = \Redirect::to("/dashboard/products");

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $product = ($data['id']) ? Product::findOrFail($data['id']) : null;

        $currency = Currency::findOrFail($data['currency_id']);

        $custodialAccount = CustodialAccount::findOrFail($data['custodial_account_id']);

        $productType = ProductType::findOrFail($data['type_id']);

        $fundManager = FundManager::findOrFail($data['fund_manager_id']);

        $status = ($data['active'] == 1) ? 'Active' : 'Inactive';

        return [
            'data' => $data,
            'product' => $product,
            'currency' => $currency,
            'custodialAccount' => $custodialAccount,
            'productType' => $productType,
            'fundManager' => $fundManager,
            'status' => $status
        ];
    }
}
