<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class StoreShareCommissionPaymentSchedule implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        if (isset($data['id'])) {
            $schedule = SharesCommissionPaymentSchedule::findOrFail($data['id']);

            $schedule->update($data);
        } else {
            $schedule = SharesCommissionPaymentSchedule::create($data);
        }

        \Flash::success('The share commission payment schedule has been saved');

        $next = \redirect("/dashboard/shares/commission/payment/" . $schedule->commission_recipient_id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        if (isset($data['id'])) {
            $schedule = SharesCommissionPaymentSchedule::findOrFail($data['id']);

            $sharePurchase = $schedule->sharePurchase;

            $recipient = $schedule->recipient;
        } else {
            $schedule = null;

            $sharePurchase = SharePurchases::findOrFail($data['share_purchase_id']);

            $recipient = CommissionRecepient::findOrFail($data['commission_recipient_id']);
        }

        return [
            'schedule' => $schedule,
            'sharePurchase' => $sharePurchase,
            'recipient' => $recipient
        ];
    }
}
