<?php
/**
 * Created by PhpStorm.
 * User: yerick
 * Date: 24/10/2018
 * Time: 09:26
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharePurchases;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Shares\Trading\Trade;

class SharePurchaseCustomDuty implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $purchase = SharePurchases::findOrFail($data['share_holding_id']);

        $holder = ShareHolder::findOrFail($data['share_holder_id']);

        $date = isset($data['date']) ? Carbon::parse($data['date']) : $purchase->date;

        $trading = new Trade();

        $trading->payStampDuty($purchase, $date->copy());

//        event(new SharePurchaseOrderHasBeenSettled($approval));



        $next = \redirect('/dashboard/shares/shareholders/show/'.$holder->id);

        \Flash::success('Custom duty is paid successfully');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $holder = ShareHolder::findOrFail($data['share_holder_id']);

        $holding = ShareHolding::findOrFail($data['share_holding_id']);

        return [
            'data' => $data,
            'holder' => $holder,
            'holding' => $holding
        ];
    }
}
