<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundDividend;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundDividend implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;
    
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }
    
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $fund = UnitFund::findOrFail($data['fund_id']);

        $input = array_except($data, ['_token', 'files', 'fund_id']);

        $dividend = new UnitFundDividend($input);
        $dividend->fund()->associate($fund);
        $dividend->save();



        \Flash::success('Unit fund dividend has been successfully saved');

        $next = \Redirect::to($dividend->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }
    
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $fund = UnitFund::findOrFail($data['fund_id']);

        return ['data'=>$data, 'fund'=>$fund];
    }
}
