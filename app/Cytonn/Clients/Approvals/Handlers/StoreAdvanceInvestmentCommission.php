<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Investment\AdditionalCommission;
use App\Cytonn\Models\Investment\AdditionalCommissionType;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Models\Investment\AdvanceCommissionType;
use Cytonn\Models\Investment\AdvanceInvestmentCommission;
use Illuminate\Support\Facades\Response;

class StoreAdvanceInvestmentCommission implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $advanceType = AdditionalCommissionType::findOrFail($data['type_id']);

        $data['amount'] = $advanceType->credit == 1 ? $data['amount'] : - $data['amount'];

        if (isNotEmptyOrNull($data['id'])) {
            $advanceCommission = AdditionalCommission::findOrFail($data['id']);

            $advanceCommission->update($data);
        } else {
            AdditionalCommission::create($data);
        }

        \Flash::success('The advance commission has been saved');

        $next = \redirect('/dashboard/investments/commission/payment/' . $data['recipient_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $advanceCommission = $data['id'] ? AdditionalCommission::findOrFail($data['id'])
            : null;

        $recipient = CommissionRecepient::findOrFail($data['recipient_id']);

        $additionalCommissionType = AdditionalCommissionType::findOrFail($data['type_id']);

        $holding = array_key_exists('holding_id', $data) && $data['holding_id'] ? UnitHolding::findOrFail($data['holding_id']) : null;

        return [
            'data' => $data,
            'advanceCommission' => $advanceCommission,
            'recipient' => $recipient,
            'advanceCommissionType' => $additionalCommissionType,
            'holding' => $holding
        ];
    }
}
