<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundCommissionSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class StoreUnitFundCommissionPaymentSchedule implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        if (isset($data['id'])) {
            $schedule = UnitFundCommissionSchedule::findOrFail($data['id']);

            $schedule->update($data);
        } else {
            $schedule = UnitFundCommissionSchedule::create($data);
        }

        \Flash::success('The unit fund commission payment schedule has been saved');

        $next = \redirect(
            "/dashboard/unitization/commission/payment/" . $schedule->unitFundCommission->commission_recipient_id
        );

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $schedule = UnitFundCommissionSchedule::findOrFail($data['id']);

        $recipient = $schedule->unitFundCommission->recipient;

        return [
            'schedule' => $schedule,
            'purchase' => $schedule->unitFundCommission->purchase,
            'recipient' => $recipient
        ];
    }
}
