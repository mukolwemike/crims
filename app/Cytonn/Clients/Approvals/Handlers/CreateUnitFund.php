<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPerformance;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Models\Unitization\FundCategory;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFund implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $input = array_except($data, ['_token', 'files']);

        $input['initial_unit_price'] = isset($input['initial_unit_price']) ? $input['initial_unit_price'] : 1;

        $fund = new UnitFund();

        $fund->fill($input);

        $fund->save();
        
        $this->saveReceivingAccount($fund, $data);

        $this->savePriceTrail($fund, $input['initial_unit_price']);



        \Flash::success('Unit fund has been successfully saved');

        $next = \Redirect::to($fund->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    private function savePriceTrail(UnitFund $fund, $initial_unit_price)
    {
        UnitFundPerformance::create([
            'unit_fund_id'      => $fund->id,
            'date'              => Carbon::yesterday(),
            'assets'            =>  0,
            'cash'              =>  0,
            'aum'               =>  0,
            'nav'               =>  0,
            'liabilities'       =>  0,
            'total_units'       =>  0,
            'price' => $initial_unit_price
        ]);
    }

    private function saveReceivingAccount(UnitFund $fund, $data)
    {
        if (isset($data['custodial_account_id'])) {
            $account = CustodialAccount::findOrFail($data['custodial_account_id']);
            $fund->receivingAccounts()->save($account);
        }
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $fundManager = FundManager::findOrFail($data['fund_manager_id']);

        $currency = Currency::findOrFail($data['currency_id']);

        $fundCategory = FundCategory::findOrFail($data['fund_category_id']);

        $account = isset($data['custodial_account_id'])
            ? CustodialAccount::findOrFail($data['custodial_account_id'])
            : new CustodialAccount();

        return [
            'data'=>$data,
            'fundManager' => $fundManager,
            'currency' => $currency,
            'account' => $account,
            'fundCategory' => $fundCategory
        ];
    }
}
