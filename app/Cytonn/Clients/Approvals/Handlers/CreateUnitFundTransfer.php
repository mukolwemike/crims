<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Events\Investments\Actions\UnitFundSaleHasBeenMade;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;
use Cytonn\Unitization\Trading\Transfer;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class CreateUnitFundTransfer implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     * @throws CrimsException
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $this->checkForGuards($approval);

        $data = $approval->payload;

        $instructionId = isset($data['instruction_id']) ? $data['instruction_id'] : null;

        if (!is_null($instructionId)) {
            $instruction = UnitFundInvestmentInstruction::find($data['instruction_id']);

            $data = collect(\GuzzleHttp\json_decode($instruction->payload))->toArray();
        }

        $input = array_except($data, ['_token', 'files']);

        $sender = Client::findOrFail($data['transferer_id']);

        $recipient = Client::findOrFail($data['transferee_id']);

        $date = Carbon::parse($input['date']);

        $fund = UnitFund::findOrFail($data['unit_fund_id']);

        $number = $input['number'];

        $transfer = (new Transfer($fund, $date, $number))
            ->from($sender)
            ->to($recipient)
            ->send();

        $transfer->sale->update([
            'approval_id' => $approval->id
        ]);

        $transfer->purchase->update([
            'approval_id' => $approval->id
        ]);

        $transfer->approval()->associate($approval);

        $transfer->save();

        if (!is_null($instructionId)) {
            $instruction = UnitFundInvestmentInstruction::find($instructionId);

            $instruction->approval_id = $approval->id;

            $instruction->save();
        }

        if ($sender->clientType->name === 'corporate' || $sender->joint) {
            $this->sendNotifications($sender, $recipient, $number, $instructionId);
        }

        event(new UnitFundSaleHasBeenMade($transfer->sale));

        $next = \Redirect::to($fund->path());

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        if (isset($data['instruction_id'])) {
            $instruction = UnitFundInvestmentInstruction::find($data['instruction_id']);

            $data = collect(\GuzzleHttp\json_decode($instruction->payload))->toArray();
        }

        $sender = Client::findOrFail($data['transferer_id']);

        $recipient = Client::findOrFail($data['transferee_id']);

        $fund = UnitFund::findorFail($data['unit_fund_id']);

        $date = Carbon::parse($data['date']);

        $number = $data['number'];

        $transfer = (new Transfer($fund, $date, $number))->from($sender)->to($recipient);

        return [
            'data' => $data,
            'sender' => $sender,
            'recipient' => $recipient,
            'transfer' => $transfer,
            'fund' => $fund
        ];
    }

    /**
     * @param Client $sender
     * @param Client $recipient
     * @param $number
     * @param $refId
     * @throws CrimsException
     */
    protected function sendNotifications(Client $sender, Client $recipient, $number, $refId)
    {
        $sender->contact->email
            ? $this->sendEmail($sender, $recipient, $sender->contact->email, $number, 'sender', $refId)
            : $this->sendSMS($sender, $recipient, $sender->contact->phone, $number, 'sender');

        $recipient->contact->email
            ? $this->sendEmail($sender, $recipient, $recipient->contact->email, $number, 'recipient', $refId)
            : $this->sendSMS($sender, $recipient, $sender->contact->phone, $number, 'sender');
    }

    private function sendEmail(Client $sender, Client $recipient, $email, $number, $type, $refId)
    {
        Mailer::compose()
            ->from(['operations@cytonn.com' => 'Cytonn Investments'])
            ->to($email)
            ->subject('Unit Fund Transfer Confirmation')
            ->view(
                'emails.unitization.unit_fund_transfer_notification',
                ['sender' => $sender, 'receiver' => $recipient,
                    'units' => $number, 'type' => $type,
                    'trans_ref' => $refId
                ]
            )->send();
    }

    /**
     * @param Client $sender
     * @param Client $recipient
     * @param $phone
     * @param $number
     * @param $type
     */
    private function sendSMS(Client $sender, Client $recipient, $phone, $number, $type)
    {
        $msg = '';

        if ($type === 'sender') {
            $msg .= "Hi " . ClientPresenter::presentFirstName($sender->id) . ", this to confirm
                the transfer request of " . $number . ' units from your CMMF to ' . ClientPresenter::presentFullNameNoTitle($recipient->id) . '-' .
                $recipient->client_code . ' has been processed and approved. Thank you for doing business with us.';
        } elseif ($type === 'recipient') {
            $msg .= "Great news, " . ClientPresenter::presentFullNameNoTitle($recipient->id) . ", has
                transferred " . $number . " units to your CMMF account. Thank you for doing
                business with us.";
        }

        \SMS::send([$phone], $msg);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return bool
     */
    private function checkForGuards(ClientTransactionApproval $approval)
    {
        if ($approval->exemption) {
            return true;
        }

        $data = $approval->payload;

        if (isset($data['instruction_id'])) {
            $instruction = UnitFundInvestmentInstruction::find($data['instruction_id']);

            $data = collect(\GuzzleHttp\json_decode($instruction->payload))->toArray();
        }

        $client = Client::findOrFail($data['transferer_id']);

        if ($client->guards()->investmentsGuard()->count() == 0) {
            return true;
        }

        $date = Carbon::parse($data['date']);

        $fund = UnitFund::findOrFail($data['unit_fund_id']);

        $ownedUnits = $client->repo->ownedNumberOfUnits($fund, $date);

        $client->repo->checkForInvestmentGuards($approval, $ownedUnits - $data['number']);
    }
}
