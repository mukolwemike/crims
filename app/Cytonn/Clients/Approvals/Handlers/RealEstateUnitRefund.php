<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 8/29/18
 * Time: 10:07 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealEstateRefund;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Exceptions\ClientInvestmentException;

class RealEstateUnitRefund implements ApprovalHandlerInterface
{
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        $this->refund($holding, $approval, $data);

        \Flash::success('The forfeiture reversal has been approved');

        $next = \redirect('/dashboard/realestate/projects');

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function refund(UnitHolding $holding, ClientTransactionApproval $approval, $data)
    {
        $holding->client->repo->checkReGaurds($approval);

        $date = new Carbon($data['date']);

        $balance = $holding->totalPayments() - $holding->refundedAmount();

        if ($data['amount'] > $balance) {
            throw new ClientInvestmentException('The amount to refund is more than balance');
        }

        if ($data['amount'] > 0) {
            $description = 'RE Refund for unit number ' .
                $holding->unit->number . ' ' .
                $holding->unit->size->name . ' ' .
                $holding->unit->type->name . ' in ' .
                $holding->project->name;

            $refund = RealEstateRefund::make($holding, $data['amount'], $date, $data['narration']);

            $p = Payment::make(
                $holding->client,
                $holding->commission->recipient,
                null,
                null,
                $holding->project,
                null,
                $date,
                $data['amount'],
                'W',
                $description
            )->debit();

            $refund->clientPayment()->associate($p);

            $refund->save();
        }

        return;
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        return [
            'project' => $holding->project,
            'holding' => $holding,
            'data' => $data,
        ];
    }
}
