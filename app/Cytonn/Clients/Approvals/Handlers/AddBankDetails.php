<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/17/16
 * Time: 11:15 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientBankDetailsAddCommand;
use Cytonn\Clients\Forms\AddBankDetailsForm;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AddBankDetails implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    protected $authorizer;

    /**
     * AddBankDetails constructor.
     */
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return \Illuminate\Support\Facades\Response|void
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $form = App::make(AddBankDetailsForm::class);
        $approval = ClientTransactionApproval::findOrFail($approval->id);

        $form->validate($approval->payload);

        $client = $approval->client;

        $this->execute(ClientBankDetailsAddCommand::class, ['data' => $approval]);

        \Flash::success('Client bank details saved');

        $next = Redirect::to("/dashboard/clients/details/{$client->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $client = $approval->client;
        $data = $approval->payload;
        $account = isset($data['id']) ? ClientBankAccount::findOrFail($data['id']) : null;
        $oldBranch = is_null($account) ?: ClientBankBranch::find($account->branch_id);

        $bank = isset($data['bank_id']) ? ClientBank::findOrFail($data['bank_id']) : null;
        $branch = isset($data['branch_id']) ? ClientBankBranch::findOrFail($data['branch_id']) : null;
        $currency = isset($data['currency_id']) ? Currency::findOrFail($data['currency_id']) : null;

        return [
            'client' => $client,
            'data' => $data,
            'account' => $account,
            'bank' => $bank,
            'branch' => $branch,
            'oldBranch' => $oldBranch,
            'currency' => $currency
        ];
    }
}
