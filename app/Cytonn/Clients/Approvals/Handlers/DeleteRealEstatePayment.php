<?php
/**
 * Date: 01/10/2016
 * Time: 16:41
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;
use App\Cytonn\Models\RealEstatePayment;

class DeleteRealEstatePayment implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $payment = RealEstatePayment::findOrFail($data['payment_id']);

        //Check for real estate guards
        $payment->holding->client->repo->checkReGaurds($approval);

        $unit = $payment->holding->unit;
        $payment->clientPayment()->delete();
        $payment->interestAccrueds()->delete();
        $payment->delete();

        \Flash::success('The payment has been deleted');

        $next = \Redirect::to("/dashboard/realestate/projects/".$unit->project->id."/units/show/".$unit->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $payment = RealEstatePayment::findOrFail($data['payment_id']);
        $holding = $payment->holding;
        $unit = $holding->unit;
        $project = $unit->project;

        return ['payment'=>$payment, 'holding'=>$holding, 'unit'=>$unit, 'project'=>$project];
    }
}
