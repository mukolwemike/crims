<?php
/**
 * Date: 02/08/2016
 * Time: 3:37 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecipientType;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Realestate\Commissions\RealEstateCommissionRateRepository;
use Illuminate\Support\Facades\Response;
use App\Cytonn\Models\Project;

class SaveRealEstateCommissionRate implements ApprovalHandlerInterface
{
    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $repo =  new RealEstateCommissionRateRepository();

        if (isset($data['rate_id'])) {
            $id = $data['rate_id'];
            unset($data['rate_id']);

            $repo->updateRate($data, $id);
        } else {
            $repo->createRate($data);
        }

        \Flash::success('The commission rate has been saved');

        $project_id = $data['project_id'];

        $next = \Redirect::to("/dashboard/realestate/projects/show/$project_id");

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $project = Project::find($data['project_id']);

        $recipientType = CommissionRecipientType::find($data['recipient_type_id']);

        return [
            'project' => $project,
            'recipientType' => $recipientType
        ];
    }
}
