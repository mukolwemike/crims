<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;

class EditCommissionPaymentSchedule implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $schedule = CommissionPaymentSchedule::findOrFail($data['schedule_id']);
        $schedule->date = $data['date'];
        $schedule->save();

        $investment = $schedule->commission->investment;

        \Flash::success('The commission payment schedule has been updated');

        $next = \redirect("/dashboard/investments/commission/".$investment->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;
        $investment = ClientInvestment::findOrFail($data['investment_id']);
        $schedule = CommissionPaymentSchedule::findOrFail($data['schedule_id']);

        return ['investment'=>$investment, 'schedule'=>$schedule, 'data'=>$data];
    }
}
