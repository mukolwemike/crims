<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Mailers\Client\ClientUpdateBankAccountMailer;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\Currency;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientBankDetailsAddCommand;
use Cytonn\Clients\ClientMpesaDetailsAddCommand;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class AddClientAccountDetails implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator, CommanderTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        $client = $approval->client;
        $data = $approval->payload;

        if ($data['formType'] === 'bank') {
            $this->execute(ClientBankDetailsAddCommand::class, ['data' => $approval]);
        } elseif ($data['formType'] === 'mpesa') {
            $this->execute(ClientMpesaDetailsAddCommand::class, ['data' => $approval]);
        }

        \Flash::success('Client Bank Account Details saved');

        (new ClientUpdateBankAccountMailer())->notifyApproval($client);

        $next = Redirect::to("/dashboard/clients/details/{$client->id}");

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $client = $approval->client;
        $data = $approval->payload;
        $account = isset($data['id']) ? ClientBankAccount::findOrFail($data['id']) : null;
        $oldBranch = is_null($account) ?: ClientBankBranch::find($account->branch_id);

        $bank = isset($data['bank_id']) ? ClientBank::findOrFail($data['bank_id']) : null;
        $branch = isset($data['branch_id']) ? ClientBankBranch::findOrFail($data['branch_id']) : null;
        $currency = isset($data['currency_id']) ? Currency::findOrFail($data['currency_id']) : Currency::findOrFail(1);
        $country = isset($data['country_id']) ? Country::findOrFail($data['country_id'])->name : null;

        return [
            'client' => $client,
            'data' => $data,
            'account' => $account,
            'bank' => $bank,
            'branch' => $branch,
            'currency' => $currency,
            'oldBranch' => $oldBranch,
            'country' => $country
        ];
    }
}
