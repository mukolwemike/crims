<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/16/16
 * Time: 10:12 AM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\InterestAction;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\AbstractApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Forms\BusinessConfirmationForm;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Topup extends AbstractApproval implements ApprovalHandlerInterface
{
    use DispatchableTrait, EventGenerator;

    protected $formats = [
        'amount' => 'amount',
        'invested_date' => 'date',
        'maturity_date' => 'date',
        'interest_rate' => 'append:%',
        'tenor' => 'append: days',
        'commission_rate' => 'append:%',
        'interest_payment_interval' => 'append: months'
    ];


    /**
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function handle(ClientTransactionApproval $approval)
    {
        dd('You can no longer approve this.');
        $form = \App::make(BusinessConfirmationForm::class);

        $data = $approval->payload;

        $form->validate($data);

        $oldInvestment = ClientInvestment::findOrFail($data['investment']);

        $clientTopupapp = $oldInvestment->application;

        !is_null($clientTopupapp) ?: $clientTopupapp = new ClientInvestmentApplication();

        $clientTopupapp->amount = $data['amount'];

        $clientTopupapp->application_type_id = 3;

        $clientTopupapp->parent_application_id = $oldInvestment->application_id;

        $clientTopupapp->agreed_rate = $data['interest_rate'];

        if (isset($data['on_call'])) {
            $clientTopupapp->on_call = $data['on_call'];
        }

        if (isset($data['topup_form_id'])) {
            if (ClientInvestment::where('topup_form_id', $data['topup_form_id'])->count() > 0) {
                throw new ClientInvestmentException(
                    'The topup is based on a form that has already been invested'
                );
            }
        }

        $data = array_add($data, 'approval_id', $approval->id);

        $topup = new \Cytonn\Investment\Action\Topup();

        $topup->handle($oldInvestment, $clientTopupapp, $data);



        $next = \View::make(
            'investment.actions.topup.topupsuccess',
            ['title' => 'Investment Topup Successful', 'investment' => $topup->client]
        );

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }


    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment']);

        $interestPayment = [null => 'On Maturity', 1 => 'Every 1 month', 2 => 'Every 2 months',
            3 => 'Every 3 months (Quarterly)', 4 => 'Every 4 months',
            5 => 'Every 5 months', 6 => 'Every 6 months (Semi anually)', 7 => 'Every 7 months',
            8 => 'Every 8 months', 9 => 'Every 9 months', 10 => 'Every 10 months',
            11 => 'Every 11 months', 12 => 'Every 12 months (Annually)'];

        $product = Product::find($data['product_id']);

        $recipient = CommissionRecepient::findOrFail($data['commission_recepient']);

        $interestaction = function ($id) {
            return InterestAction::findOrFail($id);
        };

        $commissionrate = CommissionRate::all();

        return [
            'interestPayment' => $interestPayment,
            'investment' => $investment,
            'data' => $data,
            'product' => $product,
            'recipient' => $recipient,
            'interestaction' => $interestaction,
            'commissionrate' => $commissionrate
        ];
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return array
     */
    public function details(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        return [
            'Amount' => AmountPresenter::currency($data['amount']),
            'Value Date' => DatePresenter::formatDate($data['invested_date']),
            'Maturity Date' => DatePresenter::formatDate($data['maturity_date']),
            'Interest rate' => $data['interest_rate'] . '%'
        ];
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function pluck($name)
    {
        $payload = $this->approval->payload;

        switch ($name) {
            case 'product':
                return ClientInvestment::find($this->approval->payload['investment'])->product->name;
            case 'tenor':
                return Carbon::parse($payload['invested_date'])->diffInDays(Carbon::parse($payload['maturity_date']));
            case 'fa_name':
                return CommissionRecepient::find($payload['commission_recepient'])->name;
        }
    }
}
