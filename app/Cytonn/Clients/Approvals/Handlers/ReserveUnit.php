<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 17/10/2018
 * Time: 14:05
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\RealestateUnit;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\ClientRepository;
use Cytonn\Realestate\Commands\ReserveRealEstateUnitCommand;
use Illuminate\Support\Arr;
use Laracasts\Commander\CommanderTrait;

class ReserveUnit implements ApprovalHandlerInterface
{
    use CommanderTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $unit = RealestateUnit::findOrFail($data['unit_id']);

        $data['client_type_id'] = $data['individual'];
        $data['title_id'] = ($data['individual'] == 2)
            ? null
            : (Arr::exists($data, 'client_id') ? null : $data['title']);

        unset($data['duplicate_reason']);
        unset($data['title']);

        $this->execute(ReserveRealEstateUnitCommand::class, ['data' => $data]);

        \Flash::success('The Unit has been successfuly reserved');

        $next =
            \redirect('/dashboard/realestate/projects/' . $unit->project->id . '/units/show/' . $data['unit_id']);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param array|null $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $client = Arr::exists($data, 'client_id') ? Client::findOrFail($data['client_id']) : null;

        $unit = RealestateUnit::findOrFail($data['unit_id']);

        $holding = UnitHolding::where('unit_id', $data['unit_id'])->where('active', 1)->first();

        $duplicate = (new ClientRepository())->duplicate($data);

        $title_id = ($data['individual'] == 2) ? null : (Arr::exists($data, 'client_id') ? null : $data['title']);

        $riskyClient = (new ClientApplicationRepository())->getRiskyClientDetails($data);

        $documentData = ['processType' => $data['type'], 'application' => $approval->id];

        $kycDocs = (new ClientApplicationRepository())->getDocumentDetails('kyc', $documentData);

        $contacts = (array_key_exists('contacts', $data)) ? $data['contacts'] : '';

        return [
            'data' => $data,
            'client' => $client,
            'unit' => $unit,
            'holding' => $holding,
            'duplicate' => $duplicate,
            'title_id' => $title_id,
            'riskyClient' => $riskyClient,
            'kycDocuments' => $kycDocs,
            'contacts' => $contacts
        ];
    }
}
