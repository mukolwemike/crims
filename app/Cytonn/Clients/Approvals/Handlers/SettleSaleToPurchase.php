<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/1/18
 * Time: 3:11 PM
 */

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesSalesOrder;
use App\Events\Shares\SharePurchaseOrderHasBeenSettled;
use Carbon\Carbon;
use Cytonn\Api\Transformers\ShareHoldingsTransformer;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Shares\ShareTradingRepository;

class SettleSaleToPurchase implements ApprovalHandlerInterface
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);

        $seller = $salesOrder->seller;

        $purchases = $purchases = SharePurchases::whereIn('id', $data['share_purchase_ids'])->get();

        $date = Carbon::parse($data['settlement_date']);

        $this->repo->sharePurchaseOrderSettlement($salesOrder, $purchases, $date);

//        event(new SharePurchaseOrderHasBeenSettled($approval));



        \Flash::success('Shares sales order was successfully matched to a shares purchase order(s)!');

        $next = \redirect('/dashboard/shares/sales/shareholders/'.$seller->id);

        $approval->raise(new ApprovalSuccessful($approval, $next));

        $approval->dispatchEventsFor($approval);
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $data = $approval->payload;

        $salesOrder = SharesSalesOrder::findOrFail($data['sales_order_id']);

        $seller = $salesOrder->seller;

        $sold = $salesOrder->numberSold();

        $balance = $salesOrder->balance();

        $purchases = SharePurchases::whereIn('id', $data['share_purchase_ids'])
            ->get()
            ->map(function ($purchase) {
                return (new ShareHoldingsTransformer())->details($purchase);
            });

        return [
            'seller' => $seller,
            'salesOrder' => $salesOrder,
            'sold' => $sold,
            'balance' => $balance,
            'purchases' => $purchases
        ];
    }
}
