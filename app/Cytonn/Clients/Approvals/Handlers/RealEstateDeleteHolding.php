<?php

namespace Cytonn\Clients\Approvals\Handlers;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Clients\Approvals\Events\ApprovalSuccessful;
use Illuminate\Support\Facades\Response;
use App\Cytonn\Models\UnitHolding;

class RealEstateDeleteHolding implements ApprovalHandlerInterface
{

    /**
     * Handle the approval
     *
     * @param  ClientTransactionApproval $approval
     * @return Response
     */
    public function handle(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $holding = UnitHolding::findOrFail($data['holding_id']);

        //Check for real estate guards
        $holding->client->repo->checkReGaurds($approval);

        $holding->delete();

        \Flash::success('The unit holding has been deleted.');

        $next = \redirect('/dashboard/realestate/projects');

        $approval->raise(new ApprovalSuccessful($approval, $next));
        $approval->dispatchEventsFor($approval);
    }

    /**
     * Generate the data required by the view
     *
     * @param  ClientTransactionApproval $approval
     * @param  array|null                $vars
     * @return array
     */
    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        $holding = UnitHolding::findOrFail($approval->payload['holding_id']);

        return ['project'=>$holding->project, 'unit'=>$holding->unit, 'holding'=>$holding];
    }
}
