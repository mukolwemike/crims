<?php
/**
 * Date: 29/01/2018
 * Time: 14:18
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Clients\ClientPresenter;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractApproval implements ApprovalHandlerInterface
{
    use LocksTransactions;

    protected $approval;

    protected $formats = [];

    public function __construct(ClientTransactionApproval $approval = null)
    {
        $this->approval = $approval;
    }

    public function get($fieldName)
    {
        if (method_exists($this, 'pluck')) {
            $val = $this->pluck($fieldName);

            if ($val) {
                return $this->format($fieldName, $val);
            }
        }

        $payload = $this->approval->payload;

        if ($f = $this->getters($fieldName)) {
            return $this->format($fieldName, $f);
        }

        if (isset($payload[$fieldName])) {
            return $this->format($fieldName, $payload[$fieldName]);
        }

        return null;
    }

    protected function getters($name)
    {
        switch ($name) {
            case 'client_name':
                if ($c = $this->approval->client) {
                    return \Cytonn\Presenters\ClientPresenter::presentFullNames($c->id);
                }
                // no break
            case 'client_code':
                if ($c = $this->approval->client) {
                    return $c->client_code;
                }
                // no break
            default:
                return null;
        }
    }

    protected function format($name, $value)
    {
        if (!isset($this->formats[$name])) {
            return $value;
        }

        $format = $this->formats[$name];

        switch ($format) {
            case 'date':
                return Carbon::parse($value)->toFormattedDateString();
            case 'time':
                return Carbon::parse($value)->toDayDateTimeString();
            case 'amount':
                return AmountPresenter::currency($value);
        }

        if (starts_with($format, 'append')) {
            $to_append = last(explode(':', $this->formats[$name]));
            return $value.$to_append;
        }

        return $value;
    }

    /**
     * @param Client|null $client
     * @param ClientInvestment|null $investment
     * @param User $user
     * @param array $data
     * @return ClientTransactionApproval
     */
    public static function create(Client $client = null, ClientInvestment $investment = null, User $user, array $data)
    {
        $name = strtolower(snake_case(class_basename(static::class)));

        return ClientTransactionApproval::create(
            [
                'client_id' => $client ? $client->id : null,
                'investment_id' => $investment ? $investment->id : null,
                'transaction_type' => $name,
                'sent_by' => $user->id,
                'payload' => $data
            ]
        );
    }

    /**
     * @throws \App\Exceptions\CrimsException
     */
    public function forceApprove()
    {
        $approval = new Approval($this);

        $approval->forceApprove(getSystemUser());
    }
}
