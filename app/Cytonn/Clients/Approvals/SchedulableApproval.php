<?php
/**
 * Date: 21/04/2016
 * Time: 3:58 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Clients\Approvals;

use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;

abstract class SchedulableApproval extends AbstractApproval
{
    protected $runningSchedule = false;

    /**
     * @param boolean $runningSchedule
     */
    public function setRunningSchedule($runningSchedule)
    {
        $this->runningSchedule = $runningSchedule;

        return $this;
    }

    protected function schedule(ClientTransactionApproval $approval, Carbon $date, array $args = [])
    {
        if (count($args) > 0) {
            $approval->update($args);
        }

        $approval->update(
            [
                'scheduled' => true,
                'action_date' => $date
            ]
        );
    }

    abstract public function runTransaction($approval);

    public function updateRunTransaction($approval)
    {
        $approval->update(
            [
                'run_date' => Carbon::now()
            ]
        );
    }
}
