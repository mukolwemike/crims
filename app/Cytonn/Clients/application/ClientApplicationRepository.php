<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/24/18
 * Time: 3:01 PM
 */

namespace App\Cytonn\Clients\application;

use App\Cytonn\Clients\ClientComplianceChecklist;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationDocument;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\REOTCFilledApplicationDocument;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\ClientApplications;
use Cytonn\Investment\Rules\InvestmentApplicationRules;
use Cytonn\Mailers\System\FlaggedRiskyClientMailer;

class ClientApplicationRepository
{
    use InvestmentApplicationRules;

    public $application;

    public function __construct(ClientFilledInvestmentApplication $application = null)
    {
        is_null($application)
            ? $this->application = new ClientFilledInvestmentApplication()
            : $this->application = $application;
    }

    public function saveApplicationInstruction($input)
    {
        $this->application->progress_risk = true;
        $this->application->progress_investment = true;
        $this->application->progress_subscriber = true;
        $this->application->fill($input);

        $this->application->save();

        return $this->application;
    }

    public function updateApplicationInstruction($input)
    {
        $input = array_except($input, ['id', 'joint_holders', 'contact_persons']);

        $this->application->update($input);

        $this->application->save();

        return $this->application;
    }

    public function saveContactPersons($input)
    {
        $contacts = $input['contacts'];

        $this->application->contactPersons()->createMany($contacts);

        return $this->application;
    }

    public function saveJointHolders($input)
    {
        $holders = isset($input['holders']) ? $input['holders'] : null;

        if (is_null($holders)) {
            return $this->application;
        }

        $this->application->jointHolders()->createMany($holders);

        return $this->application;
    }

    public function upload($input, $application, $document)
    {
        if (isset($input['document_type_ids'])) {
            $ids = collect(json_decode($input['document_type_ids']));

            return $ids->each(function ($check_list_id) use ($input, $application, $document) {

                $input['kyc_type'] = ClientComplianceChecklist::find($check_list_id)->slug;

                return $this->saveClientDocument($input, $document);
            });
        }

        $clientDocument = $this->saveClientDocument($input, $document);

        return $clientDocument;
    }

    public function saveClientDocument($input, Document $document)
    {
        $application = $this->getApplication($input);

        $filledDocument = $this->applicationDocument($input);

        $filledDocument->application_id = $application->id;

        $filledDocument->document_id = $document->id;

        if ($input['slug'] == 'kyc_type' || $input['slug'] == 'kyc') {
            $kyc_type = ClientComplianceChecklist::where('slug', $input['kyc_type'])->first();

            $this->deleteUploaded($kyc_type, $application->id, $input);

            $filledDocument->complianceDocument()->associate($kyc_type);
        }

        $filledDocument->payload = json_encode($input);

        $filledDocument->save();

        return $filledDocument;
    }

    public function getApplication($input, $id = null)
    {
        if ($input['process_type'] === 'reserve_unit' || $input['process_type'] === 'register_shareholder') {
            return ClientTransactionApproval::findOrFail($input['application_id']);
        }

        return ClientFilledInvestmentApplication::findOrFail($input['application_id']);
    }

    public function applicationDocument($input)
    {
        if ($input['process_type'] === 'reserve_unit' || $input['process_type'] === 'register_shareholder') {
            return new REOTCFilledApplicationDocument();
        }

        return new ClientFilledInvestmentApplicationDocument();
    }

    public function deleteUploaded(ClientComplianceChecklist $checklist, $id, $input)
    {

        $app = $this->getApplication($input, $id);

        return $app->filledDocuments()
            ->whereHas('complianceDocument', function ($compliance) use ($checklist) {
                return $compliance->where('id', $checklist->id);
            })
            ->get()
            ->each(function ($doc) {
                return $doc->delete();
            });
    }

    /**
     * @return mixed
     * @throws ClientInvestmentException
     * @throws \Throwable
     */
    public function process()
    {
        $application = $this->application;

        if ($application->filled) {
            throw new ClientInvestmentException('The form has already been used');
        }

        $input = $this->cleanApplication($application->toArray());

        $input['terms_accepted'] = true;

        $input['form_id'] = $application->id;

        return \DB::transaction(function () use ($application, $input) {
            $isIndividual = $application->individual === 1
                || $application->individual === '1'
                || $application->individual === true;

            $processed = $isIndividual
                ? $this->processIndividual($application, $input)
                : $this->processCorporate($application, $input);

            $approval = ClientTransactionApproval::add([
                'client_id' => $application->client_id,
                'transaction_type' => 'application',
                'payload' => ['application_id' => $processed->id],
                'scheduled' => 0
            ]);

            $processed->update(['approval_id' => $approval->id]);

            return $processed;
        });
    }

    protected function processIndividual(ClientFilledInvestmentApplication $application, array $input)
    {
        $app = new ClientApplications();

        $input['client_id'] = null;

        if (count($application->jointHolders)) {
            $input['joint'] = true;
        }

        $input = array_add($input, 'type_id', ClientType::where('name', 'individual')
            ->first()->id);

        return $app->individualApplication($input);
    }


    protected function processCorporate(ClientFilledInvestmentApplication $application, array $input)
    {
        $app = new ClientApplications();

        $input['client_id'] = null;

        $input = array_add($input, 'type_id', ClientType::where('name', 'corporate')
            ->first()->id);

        return $app->corporateApplication($input);
    }

    public function cleanApplicationData($input)
    {
        $input['complete'] = ($input['complete']) ? 1 : 0;

        $input['franchise'] = (isset($input['franchise'])) ? 1 : 0;

        $input = array_except($input, ['product_category', 'client']);

        return $input;
    }

    /**
     * @param $input
     * @return \Illuminate\Validation\Validator
     */
    public function validateEntryLevelApplication($input)
    {
        $clientType = $input['individual'] == 1 ? 'individual' : 'corporate';

        if ($input['type'] == 'reserve_unit') {
            $validator = $this->shortReserveUnitApplicationForm(request());
        } elseif (isset($input['client_id']) && $input['client_id'] != null) {
            $validator = $this->shortExistingApplicationForm(request());
        } elseif ($input['new_client'] == 3) {
            $validator = ($clientType == 'individual')
                ? $this->shortIndividualDuplicateApplicationForm(request())
                : $this->shortCorporateDuplicateApplicationForm(request());
        } else {
            $validator = ($clientType == 'individual')
                ? $this->shortIndividualApplicationForm(request())
                : $this->shortCorporateApplicationForm(request());
        }

        return $validator;
    }

    public function getDocType($type, $data)
    {
        // this will be used for OTC & RE client application.
        if ($data) {
            $approval = ClientTransactionApproval::findOrFail($data['application']);

            return $approval->filledDocuments()->documentType($type)->get();
        }

        return $this->application->filledDocuments()->documentType($type)->get();
    }

    public function getDocumentDetails($type, $data = null)
    {
        $docs = $this->getDocType($type, $data);

        return $docs->map(function ($doc) {
            $input = json_decode($doc->payload);

            if (isset($input->kyc_type)) {
                $type = ucwords(str_replace("_", " ", $input->kyc_type));
            } else {
                $type = null;
            }

            return [
                'id' => $doc->id,
                'name' => $doc->document->type->name,
                'application_id' => $doc->application_id,
                'slug' => $input->slug,
                'kyc_type' => $type,
                'kyc_type_id' => $type,
                'date' => (isset($input->date)) ? $input->date : null,
                'email' => (isset($input->email)) ? $input->email : null,
                'document_id' => $doc->document_id,
                'doc_type' => ($type == null) ? null : ($input->kyc_type),
                'joint_client_holder_id' => (isset($input->joint_client_holder_id))
                    ? ($input->joint_client_holder_id)
                    : null,
            ];
        });
    }

    private function cleanApplication($input)
    {
        $forbidden = ['individual', 'kin_name', 'kin_phone', 'kin_postal', 'kin_email', 'progress_investment',
            'progress_subscriber', 'progress_kyc', 'progress_payment', 'progress_mandate', 'complete',
            'app_start_email', 'uuid', 'id'
        ];

        foreach ($forbidden as $key) {
            unset($input[$key]);
        }

        return $input;
    }

    public function modify($str)
    {
        return ucwords(str_replace("_", " ", $str));
    }

    public function checkRiskyClients($data)
    {
        if ($data['new_client'] == 2) {
            return $this->checkRiskyExistingClient($data);
        }

        $email = isset($data['email']) ? $data['email'] : null;

        return ($data['individual'] == 1)
            ? $this->checkRiskyIndividualClient($data['firstname'], $data['lastname'], $email)
            : $this->checkRiskyCorporateClient($data['registered_name'], $data['trade_name'], $email);
    }

    public function getRiskyClientDetails($input)
    {
        if ($input['new_client'] == 2 && $input['client_id']) {
            return $this->checkRiskyExistingClient($input);
        }

        $email = isset($input['email']) ? $input['email'] : null;

        return ($input['individual'] == 1)
            ? $this->checkRiskyIndividualClient($input['firstname'], $input['lastname'], $email)
            : $this->checkRiskyCorporateClient($input['registered_name'], $input['trade_name'], $email);
    }

    public function checkRiskyIndividualClient($firstName, $lastName, $email = null)
    {
        if (isEmptyOrNull($firstName) && isEmptyOrNull($lastName) && isEmptyOrNull($email)) {
            return null;
        }

        $clientQuery = Client\RiskyClient::risky()->where(function ($q) use ($firstName, $lastName, $email) {
            $q->where(function ($q) use ($firstName, $lastName) {
                if (isNotEmptyOrNull($firstName)) {
                    $q->where('firstname', $firstName);
                }

                if (isNotEmptyOrNull($lastName)) {
                    $q->where('lastname', $lastName);
                }
            });

            if (isNotEmptyOrNull($email)) {
                $q->orWhere('email', $email);
            }
        });

        return $clientQuery->first();
    }

    public function checkRiskyCorporateClient($registeredName, $tradeName = null, $email = null)
    {
        if (isEmptyOrNull($registeredName) && isEmptyOrNull($tradeName) && isEmptyOrNull($email)) {
            return null;
        }

        $clientQuery = Client\RiskyClient::risky()->where(function ($clientQuery) use ($registeredName, $tradeName, $email) {
            if (isNotEmptyOrNull($registeredName)) {
                $clientQuery->where('organization', $registeredName);
            }

            if (isNotEmptyOrNull($tradeName)) {
                $clientQuery->orWhere('organization', $tradeName);
            }

            if (isNotEmptyOrNull($email)) {
                $clientQuery->orWhere('email', $email);
            }
        });

        return $clientQuery->first();
    }

    public function checkRiskyExistingClient($input)
    {
        $client = Client::findOrFail($input['client_id']);

        if ($client->client_type_id == 2) {
            return $this->checkRiskyCorporateClient(
                $client->contact->corporate_registered_name,
                $client->contact->corporate_trade_name,
                $client->contact->email
            );
        } else {
            return $this->checkRiskyIndividualClient(
                $client->contact->firstname,
                $client->contact->lastname,
                $client->contact->email
            );
        }
    }

    /**
     * @param $data
     * @param string $channel
     */
    public function checkAndNotifyRiskyClient($data, $channel = "CRIMS-ADMIN")
    {
        $riskyClient = $this->checkRiskyClients($data);

        if (is_null($riskyClient)) {
            return;
        }

        (new FlaggedRiskyClientMailer())->sendFlaggedRiskyClientNotification($riskyClient, $channel);
    }
}
