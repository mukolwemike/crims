<?php

namespace App\Cytonn\Clients;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\JointClientHolder;
use App\Cytonn\Models\Title;
use App\Cytonn\Presenters\JointHolderPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class ClientJointDetail extends BaseModel
{
    use PresentableTrait, SoftDeletes;

    protected $guarded = ['id'];

    protected $presenter = JointHolderPresenter::class;


    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function holder()
    {
        return $this->hasOne(JointClientHolder::class, 'joint_detail_id');
    }

    public function title()
    {
        return $this->belongsTo(Title::class, 'title_id');
    }

    public function uploadedKyc()
    {
        return $this->hasMany(ClientUploadedKyc::class, 'client_joint_id');
    }
}
