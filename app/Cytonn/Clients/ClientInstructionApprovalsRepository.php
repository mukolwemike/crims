<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/13/18
 * Time: 10:54 AM
 */

namespace App\Cytonn\Clients;

use App\Cytonn\Api\Transformers\Clients\ClientApprovalTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Carbon\Carbon;

class ClientInstructionApprovalsRepository
{
    public $approval;

    public function __construct(ClientInstructionApproval $approval = null)
    {
        $this->approval = $approval ? $approval : new ClientInstructionApproval();
    }

    public function topUpApprovals(ClientTopupForm $form)
    {
        $client = $form->client;

        return $this->saveApproval($client, null, $form, null);
    }

    public function rolloverApprovals(ClientInvestmentInstruction $instruction)
    {
        $client = $instruction->investment->client;

        if (!$instruction->isPartialRollover()) {
            return;
        }

        return $this->saveApproval($client, $instruction, null, null);
    }

    public function transferApprovals(UnitFundInvestmentInstruction $instruction)
    {
        $client = $instruction->client;

        if (!$instruction->client->joint) {
            return;
        }

        return $this->saveApproval($client, null, null, $instruction);
    }

    public function saveApproval(
        Client $client,
        ClientInvestmentInstruction $instruction = null,
        ClientTopupForm $topUpForm = null,
        UnitFundInvestmentInstruction $fundInstruction = null
    ) {
        $date = $fundInstruction ? $fundInstruction->updated_at
            : ($instruction ? $instruction->updated_at : $topUpForm->updated_at);

        $data = [
            'instruction_id' => $instruction ? $instruction->id : null,
            'client_topup_form_id' => $topUpForm ? $topUpForm->id : null,
            'date' => $date,
            'fund_instruction_id' => $fundInstruction ? $fundInstruction->id : null
        ];

        $clientUsers = $this->clientUsers($client);

        if ($clientUsers) {
            $clientUsers->each(function ($user) use ($data, $client, $fundInstruction) {

                $approval = new ClientInstructionApproval();

                $data['client_id'] = $client->id;

                $data['status'] = ($fundInstruction && $fundInstruction->user_id == $user->id) ? 1 : null;

                $data['client_user_id'] = $user->id;

                $approval->fill($data);

                $approval->save();
            });
        }

        return $client;
    }

    public function clientUsers(Client $client)
    {
        $mandate = $client->present()->getSigningMandate;

        if ($mandate == 'Singly' || $mandate == 'Either to sign') {
            return null;
        }

        return $client->clientUsers()
            ->whereHas('clientSignatures', function ($signature) use ($client) {
                $signature->where('client_id', $client->id);
            })
            ->get();
    }

    public function getApprovals(Client $client)
    {
        $approvals = $this->approvals($client);

        if (!$approvals->count()) {
            return;
        }

        $date = $client->clientApprovals()->orderBy('date', 'desc')->first()->date;

        return [
            'short_description' => 'Pending approvals',
            'description' => 'There is ' . $approvals->count() . ' approvals pending for this client.',
            'status' => 'unread',
            'url' => '/approvals/' . $client->uuid,
            'date' => Carbon::parse($date)->toFormattedDateString(),
            'approvals' => $approvals,
        ];
    }

    public function approvals(Client $client)
    {
        $approvals = collect();

        $investmentInstructions = $this->investmentInstructions($client);

        $topup = $this->topup($client);

        $unitFundInstructions = $this->unitFundInstructions($client);

        $approvals = $approvals->merge($topup);

        $approvals = $approvals->merge($investmentInstructions);

        $approvals = $approvals->merge($unitFundInstructions);

        return $approvals;
    }

    public function topup(Client $client)
    {
        return $client->topupForms()
            ->whereHas('clientApprovals', function ($approval) {
                $approval->whereNull('status');
            })
            ->whereDoesntHave('approval')
            ->get()
            ->map(function ($topup) {
                return (new ClientApprovalTransformer())->transformTopup($topup);
            });
    }

    public function investmentInstructions(Client $client)
    {
        return ClientInvestmentInstruction::whereHas('investment', function ($investment) use ($client) {
            $investment->where('client_id', $client->id);
        })
            ->whereDoesntHave('approval')
            ->whereHas('clientApprovals', function ($approval) {
                $approval->whereNull('status');
            })
            ->get()
            ->map(function ($instruction) {
                return (new ClientApprovalTransformer())->transformInstruction($instruction);
            });
    }

    public function unitFundInstructions(Client $client)
    {
        return UnitFundInvestmentInstruction::where('client_id', $client->id)
            ->whereDoesntHave('approval')
            ->whereHas('clientApprovals', function ($approval) {
                $approval->whereNull('status');
            })
            ->get()
            ->map(function ($instruction) {
                return (new ClientApprovalTransformer())->transformFundInstruction($instruction);
            });
    }
}
