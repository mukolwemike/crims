<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\ClientSignatures;

use Laracasts\Presenter\Presenter;

class ClientSignaturePresenter extends Presenter
{
    /*
     * Specify the name for the client signature
     */
    public function getName()
    {
        if ($this->joint_client_holder_id) {
            return $this->jointClientHolder->details->present()->fullname;
        } elseif ($this->client_contact_person_id) {
            return $this->contactPerson->name;
        } elseif ($this->commission_recepient_id) {
            return $this->relationPerson->name;
        } elseif ($this->name) {
            return $this->name;
        } else {
            return $this->client->contact->present()->fullname;
        }
    }

    public function getEmail()
    {
        if ($this->joint_client_holder_id) {
            return $this->jointClientHolder->details->email;
        } elseif ($this->client_contact_person_id) {
            return $this->contactPerson->email;
        } elseif ($this->commission_recepient_id) {
            return $this->relationPerson->email;
        } elseif ($this->email_address) {
            return $this->email_address;
        } else {
            return $this->client->contact->email;
        }
    }
}
