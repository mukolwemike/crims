<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\ClientSignatures;

use App\Cytonn\Models\ClientSignature;

class ClientSignatureRepository
{
    /*
     * Get a given client signature by its id
     */
    public function getClientSignatureById($id)
    {
        return ClientSignature::findOrFail($id);
    }
}
