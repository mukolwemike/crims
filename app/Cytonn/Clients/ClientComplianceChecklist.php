<?php

namespace App\Cytonn\Clients;

use App\Cytonn\BaseModel;

class ClientComplianceChecklist extends BaseModel
{
    public $table = 'client_compliance_checklist';
}
