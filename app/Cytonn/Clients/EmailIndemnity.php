<?php

namespace Cytonn\Clients;

use Cytonn\Core\Storage\StorageInterface;

class EmailIndemnity
{
    public function getStorageLocation()
    {
        $path = \App::make(StorageInterface::class)->getStorageLocation();

        return $path.'/email_indemnity';
    }
}
