<?php

namespace Cytonn\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\JointClientHolder;
use Cytonn\Oauth\ApiCredentialManager;
use GuzzleHttp\Client as GuzzleClient;

class ClientManager
{
    /*
     * Get the api credential manager
     */
    use ApiCredentialManager;

    /*
     * Send the given data to CRM
     */
    public function sendDataToCrm($type, $id=null)
    {
        switch ($type){
            case 'clients':
                $this->sendClientToCrm($id);
                break;
            case 'contacts':
                $this->sendContactToCrm($id);
                break;
            case 'client_joint_details':
                $this->sendJointDetailToCrm($id);
                break;
            case 'joint_client_holders':
                $this->sendJointHolderToCrm($id);
                break;
            default:
                return;
        }
    }

    /*
     * Test CRM api
     */
    public function testCrmApi($date, $unitId)
    {
        $accessToken = $this->seekCrmAuthorization();

        if (is_null($accessToken)) {
            return;
        }

        $client = new GuzzleClient();

        $response = $client->request(
            'GET',
            'https://crm.cytonn.com/api/bi-evaluation-report/' . $date . '/' . $unitId,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken
                ]
            ]
        );

        $data = \GuzzleHttp\json_decode($response->getBody());

        dd($data);
    }

    /*
     * Submit the data to CRM
     */
    private function submitDataToCrm($data)
    {
        $accessToken = $this->seekCrmAuthorization();

        if (is_null($accessToken)) {
            return;
        }

        $client = new GuzzleClient();

        $client->request(
            'GET',
            env('CRM_CONTACT_URL'),
            [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => [
                'contact' => $data,
            ]
            ]
        );
    }

    /*
     * CLIENT
     */
    /*
     * Send a given contact to CRM
     */
    public function sendClientToCrm($clientId)
    {
        $client = Client::where('id', $clientId)->with('contact')->first();

        if (is_null($client)) {
            return;
        }

        $this->submitDataToCrm($this->getClientData($client));
    }

    /*
     * Get the desired client data from the model
     */
    public function getClientData($client)
    {
        $contact = $client->contact;

        $fa = $client->getLatestFA();

        $faEmail = ($fa) ? $fa->email : null;

        return [
            'crims_client_id' => $client->id,
            'client_code' => $client->client_code,
            'type' => $contact->entity_type_id,
            'firstname' => $contact->firstname,
            'lastname' => $contact->lastname,
            'middlename' => $contact->middlename,
            'title_id' => ($contact->title_id < 8) ? $contact->title_id : 8,
            'email' => $contact->email,
            'phone' => $contact->phone,
            'gender_id' => $contact->gender_id,
            'address' => $client->postal_address . '-' . $client->postal_code,
            'organization' => $contact->entity_type_id == 2
                ? $contact->corporate_registered_name
                : $client->employer_name,
            'country_id' => $client->country_id,
            'city' => $client->town,
            'fa_email' => $faEmail
        ];
    }

    /*
     * CONTACT
     */
    /*
     * Send contact data to CRM
     */
    public function sendContactToCrm($contactId)
    {
        $contact = Contact::where('id', $contactId)->with('client')->first();

        if (is_null($contact)) {
            return;
        }

        $client = $contact->client;

        if (is_null($client)) {
            return;
        }

        $this->submitDataToCrm($this->getClientData($client));
    }

    /*
     * JOINT CLIENT HOLDER
     */
    /*
     * Send the joint detail data to CRM
     */
    public function sendJointDetailToCrm($jointDetailId)
    {
        $clientJointDetail = ClientJointDetail::find($jointDetailId);

        if (is_null($clientJointDetail)) {
            return;
        }

        $clientData = $this->getJointDetailData($clientJointDetail);

        if (! is_null($clientData)) {
            $this->submitDataToCrm($clientData);
        }
    }

    /*
     * Send the joint holder data to CRM
     */
    public function sendJointHolderToCrm($jointHolderId)
    {
        $jointHolder = JointClientHolder::find($jointHolderId);

        if (is_null($jointHolder)) {
            return;
        }

        $clientJointDetail = $jointHolder->details;

        if (is_null($clientJointDetail)) {
            return;
        }

        $clientData = $this->getJointDetailData($clientJointDetail);

        if (! is_null($clientData)) {
            $this->submitDataToCrm($clientData);
        }
    }

    /*
     * Get the client joint holder data
     */
    public function getJointDetailData($clientJointDetail)
    {
        $holder = $clientJointDetail->holder;

        if ($holder) {
            $client = $holder->client;

            if ($client) {
                $fa = $client->getLatestFA();

                $faEmail = ($fa) ? $fa->email : null;

                return [
                    'crims_client_id' => $client->id,
                    'client_code' => $client->client_code,
                    'type' => 1,
                    'firstname' => $clientJointDetail->firstname,
                    'lastname' => $clientJointDetail->lastname,
                    'middlename' => $clientJointDetail->middlename,
                    'title_id' => ($clientJointDetail->title_id < 8) ? $clientJointDetail->title_id : 8,
                    'email' => $clientJointDetail->email,
                    'phone' => $clientJointDetail->telephone_home,
                    'gender_id' => $clientJointDetail->gender_id,
                    'address' => $clientJointDetail->postal_address . '-' . $clientJointDetail->postal_code,
                    'organization' => '',
                    'country_id' => $clientJointDetail->country_id,
                    'city' => $clientJointDetail->town,
                    'fa_email' => $faEmail
                ];
            }
        }

        return null;
    }
}
