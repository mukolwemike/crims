<?php

namespace App\Cytonn\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use Carbon\Carbon;

class AccountOverviewRepository
{
    public function getBriefOverview(Client $client)
    {
        $products = Product::get(['id', 'name'])->lists('id', 'name');

        $totals = collect($products)->map(
            function ($id) use ($client) {
                return $client->repo->getTodayTotalInvestmentsValueForProduct(Product::find($id));
            }
        )->filter(
            function ($total) {
                return $total > 0;
            }
        );

        //maturities
        $remaining = 5 - count($totals);

        $maturities = ClientInvestment::where('client_id', $client->id)
            ->where('withdrawn', '!=', 1)
            ->orderBy('maturity_date', 'ASC')
            ->get(['uuid', 'product_id', 'amount', 'maturity_date'])
            ->take($remaining);

        foreach ($maturities as $m) {
            $m->currency  = $m->product->currency->code;
            $m->product = $m->product->name;
        }


        return [
            'totals'=>$totals,
            'maturities'=>$maturities,
            'updated'=>Carbon::now()->toDateTimeString()
        ];
    }
}
