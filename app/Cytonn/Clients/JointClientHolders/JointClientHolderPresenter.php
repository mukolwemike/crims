<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\JointClientHolders;

use Laracasts\Presenter\Presenter;

class JointClientHolderPresenter extends Presenter
{
    /**
     * @return mixed
     */
    public function getPhone()
    {
        $phone = $this->contact->present()->getPhone;

        return isNotEmptyOrNull($phone) ? $phone : $this->details->present()->getPhone;
    }
}
