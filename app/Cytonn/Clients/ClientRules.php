<?php

namespace Cytonn\Clients;

use Cytonn\Rules\Rules;

trait ClientRules
{
    use Rules;

    public function clientSignatureUpload($request)
    {
        $rules = [
            'file' => 'required|mimes:pdf,jpg,png,jpeg',
        ];

        return $this->verdict($request, $rules);
    }

    public function createGuard($request)
    {
        $messages = [
            'portfolio_investor_id.required_if' => 'Please provide the portfolio investor for the portfolio guard type',
            'minimum_balance.required_if' => 'Please provide the minimum balance for the minimum balance guard type'
        ];

        $rules = [
            'guard_type_id' => 'required',
            'client_id' => 'required',
            'minimum_balance' => 'required_if:guard_type_id,2',
            'portfolio_investor_id' => 'required_if:guard_type_id,3',
            'reason' => 'required'
        ];

        return $this->verdict($request, $rules, $messages);
    }

    public function deleteWithReasonRule($request)
    {
        $rules = [
            'reason' => 'required',
        ];

        return $this->verdict($request, $rules);
    }
}
