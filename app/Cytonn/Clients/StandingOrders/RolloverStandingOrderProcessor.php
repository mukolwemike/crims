<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 29/06/2018
 * Time: 13:35
 */

namespace App\Cytonn\Clients\StandingOrders;

use App\Cytonn\Investment\InvestmentActionHandler;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientStandingOrder;
use App\Cytonn\Models\User;
use Carbon\Carbon;

class RolloverStandingOrderProcessor
{
    protected $order;

    protected $client;

    /**
     * RolloverStandingOrderProcessor constructor.
     * @param $order
     */
    public function __construct(ClientStandingOrder $order)
    {
        $this->order = $order;
        $this->client = $order->client;
    }


    public function handle()
    {
        $date = Carbon::today()->addDays(7);

        $matured = $this->client->investments()
            ->where('maturity_date', '<=', $date->copy())
            ->doesntHave('rolloverInstruction')
            ->whereDoesntHave('instructions', function ($instr) {
                $instr->where('due_date', '>=', Carbon::today());
            })
            ->where(function ($q) {
                $q->doesntHave('schedule')->orWhereHas('schedule', function ($q) {
                    $q->where('action_date', '<', Carbon::now());
                });
            })
            ->active()
            ->get();

        $matured->each(function (ClientInvestment $inv) use ($date) {
            $amt = $this->client->repo->getTodayInvestedAmountForProduct($inv->product, $date);
            $tenor = $this->order->tenor ? $this->order->tenor : 12;

            $instr = (new InvestmentActionHandler())->investmentRollover($inv, [
                'investment_id' => $inv->id,
                'topup_amount' => 0,
                'amount_select' => 'principal_interest',
                'tenor' => $tenor,
                'agreed_rate' => $inv->product->repo->interestRate($tenor, $amt),
                'automatic_rollover' => false
            ]);

            $instr->update(['filled_by' => User::where('username', 'system')->first()->id]);
        });

        return $matured;
    }
}
