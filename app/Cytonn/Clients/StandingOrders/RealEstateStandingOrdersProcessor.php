<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 28/06/2018
 * Time: 11:14
 */

namespace App\Cytonn\Clients\StandingOrders;

use App\Cytonn\Investment\InvestmentActionHandler;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientStandingOrder;
use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class RealEstateStandingOrdersProcessor
{
    protected $client;

    protected $orders;

    protected $deductions = [];

    /**
     * RealEstateStandingOrdersProcessor constructor.
     * @param $client
     * @param $orders
     */
    public function __construct(Client $client, Collection $orders)
    {
        $this->client = $client;
        $this->orders = $this->sortOrders($orders);
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function handle()
    {
        if ($this->orders->count() == 0) {
            return [];
        }

        \DB::transaction(function () {
            $this->orders->each(function ($order) {
                $this->process($order);
            });
        });

        return collect($this->deductions)
            ->filter(function ($deduction) {
                return $deduction['amount'] >= 0;
            })->map(function ($ded) {
                return (object) $ded;
            })->all();
    }

    protected function process(ClientStandingOrder $standingOrder)
    {
        $holdings = $this->getUnitHoldings($standingOrder);

        $holdings->each(function (UnitHolding $holding) use ($standingOrder) {
            $investments = $this->investments($standingOrder);

            $instrs = $investments->map(function ($inv) use ($holding) {
                $this->requestDeduction($holding, $inv);
            })->filter(function ($inst) {
                return !is_null($inst);
            });

            return $instrs;
        });
    }

    /**
     * @param UnitHolding $holding
     * @param ClientInvestment $investment
     * @return InvestmentActionHandler
     */
    protected function requestDeduction(UnitHolding $holding, ClientInvestment $investment)
    {
        $overdue = $holding->repo->amountOverdue();

        $paid = $this->checkPaid($holding);

        $remaining = $overdue - $paid;

        $inv_remaining = $investment->calculate(Carbon::today(), true)->total()
            - $this->deducted($investment);

        if ($overdue < $remaining) {
            return $this->recordDeduction($holding, $investment, $remaining);
        }

        return $this->recordDeduction($holding, $investment, $inv_remaining);
    }

    /**
     * @param UnitHolding $holding
     * @param ClientInvestment $investment
     * @param $amount
     * @return InvestmentActionHandler
     */
    protected function recordDeduction(UnitHolding $holding, ClientInvestment $investment, $amount)
    {
        if ($amount < 1) {
            return null;
        }
        array_push($this->deductions, [
            'investment_id' => $investment->id,
            'holding_id' => $holding->id,
            'amount' => $amount
        ]);

        $instr = (new InvestmentActionHandler())->withdrawInvestment($investment, [
            'amount' => $amount,
            'amount_select' => "amount",
            'investment_id' => $investment->id,
            'reason' => 'Standing order - RE payment '. $holding->project->name.' '.$holding->unit->number,
            'withdraw_type' => "withdrawal",
            'withdrawal_date' => "2018-06-29",
            'withdrawal_stage' => Carbon::today()->gte($investment->maturity_date) ? "mature": "premature"
        ]);

        $instr->update([
           'filled_by' => User::where('username', 'system')->first()->id,
        ]);

        return $instr;
    }

    protected function checkPaid(UnitHolding $holding)
    {
        return collect($this->deductions)
            ->filter(function ($payment) use ($holding) {
                return $payment['holding_id'] == $holding->id;
            })->sum('amount');
    }

    protected function deducted(ClientInvestment $investment)
    {
        return collect($this->deductions)
            ->filter(function ($deduction) use ($investment) {
                return $deduction['investment_id'] == $investment->id;
            })->sum('amount');
    }

    /**
     * @param ClientStandingOrder $standingOrder
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getUnitHoldings(ClientStandingOrder $standingOrder)
    {
        $unitQ = $this->client->unitHoldings()->whereHas('paymentSchedules', function ($schedules) {
            $schedules->where('date', Carbon::today()->subDay());
        });

        if ($standingOrder->project) {
            $unitQ = $unitQ->whereHas('unit', function ($unit) use ($standingOrder) {
                $unit->where('project_id', $standingOrder->project_id);
            });
        }

        if ($standingOrder->unit) {
            $unitQ = $unitQ->where('unit_id', $standingOrder->unit_holding_id);
        }

        return $unitQ->get();
    }

    private function investments(ClientStandingOrder $order)
    {
        if ($inv = $order->investment) {
            return collect([$inv]);
        }

        return $this->client->investments()->active()->oldest()->get();
    }

    private function sortOrders(Collection $orders)
    {
        return $orders;
    }
}
