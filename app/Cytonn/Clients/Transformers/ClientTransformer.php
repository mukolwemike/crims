<?php

namespace App\Cytonn\Clients\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use Cytonn\Core\DataStructures\Carbon;
use League\Fractal\TransformerAbstract;

class ClientTransformer extends TransformerAbstract
{
    public function transform(Client $client)
    {
        $hasFund = $client->unitFundPurchases()
            ->whereHas('unitFund', function ($fund) {
                $fund->ofType('cis')->active();
            })->exists();

        return [
            'id' => $client->uuid,
            'client_code' => $client->client_code,
            'fullname' => $client->name(),
            'total_inflow' => $this->clientInflow($client),
            'next_maturity_date' => $this->nextMaturity($client),
            'hasSP' => $client->investments()->active()->count() ? true : false,
            'hasUT' => $hasFund,
            'hasRE' => count($client->repo->clientReInvestments()['realEstateDetails']) ? true : false,
        ];
    }

    public function clientInflow(Client $client)
    {
        $investments = $client->investments()->get();

        if ($investments) {
            $totalInflow = 0;

            $invInflow = $investments->map(
                function ($investment) {
                    return [
                        'inflow' => $this->inflow($investment)
                    ];
                }
            )->sum('inflow');

            $totalInflow += $invInflow;
            return $totalInflow;
        }

        return 0;
    }

    public function nextMaturity(Client $client)
    {
        $investment = ClientInvestment::active()
            ->where('client_id', $client->id)
            ->orderBy('maturity_date', 'asc')
            ->first();

        if ($investment) {
            return Carbon::parse($investment->maturity_date)->toFormattedDateString();
        }

        return null;
    }

    public function inflow(ClientInvestment $investment)
    {
        if ($investment->type->name == 'partial_withdraw') {
            return 0;
        }

        if ($investment->type->name == 'rollover') {
            if ($investment->approval->transaction_type == 'combined_rollover') {
                $data = $investment->approval->payload;

                if ($data['reinvest'] == 'topup') {
                    return $data['amount'];
                }
            }
            return 0;
        }
        return $investment->amount;
    }
}
