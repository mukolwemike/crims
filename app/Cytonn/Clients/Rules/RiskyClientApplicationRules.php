<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 07/11/2018
 * Time: 12:27
 */

namespace App\Cytonn\Clients\Rules;

use Cytonn\Rules\Rules;

trait RiskyClientApplicationRules
{
    use Rules;

    private $commonRules = [
        'email' => 'nullable | email',
        'date_flagged' => 'required',
        'reason' => 'required',
        'risky_status_id' => 'required',
        'risk_source' => 'required'
    ];

    public function validateIndividualForm($request)
    {
        $rules = $this->commonRules + [
                'firstname' => 'required',
                'lastname' => 'required',
            ];

        return $this->verdict($request, $rules);
    }

    public function validateCorporateForm($request)
    {
        $rules = $this->commonRules + [
                'organization' => 'required',
            ];

        return $this->verdict($request, $rules);
    }
}
