<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/26/18
 * Time: 12:53 PM
 */

namespace App\Cytonn\Clients\Rules;

use App\Cytonn\Models\ClientStandingOrderType;
use Cytonn\Rules\Rules;

trait StandingOrderRules
{
    use Rules;

    public function standingOrderCreate($request)
    {
        $type = ClientStandingOrderType::findOrFail($request->type_id)->slug;

        $rules = [
            'type_id' => 'required|numeric'
        ];

        return $this->verdict($request, $rules);
    }
}
