<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/18/18
 * Time: 11:15 AM
 */

namespace App\Cytonn\Clients\Rules;

use Cytonn\Rules\Rules;

trait ClientApprovalRules
{
    use Rules;

    public function validateApproval($request)
    {
        $rules = [
            'approval_id' => 'required|numeric',
            'value' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
