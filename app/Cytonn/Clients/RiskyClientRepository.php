<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 07/11/2018
 * Time: 13:04
 */

namespace App\Cytonn\Clients;

use App\Cytonn\Models\Client\RiskyClient;

class RiskyClientRepository
{
    public function saveRiskyClient($request)
    {
        $request['flagged_by'] = \Auth::user()->id;

        $client = (new RiskyClient())->create($request);

        return $client;
    }

    public function updateRiskyClient($input)
    {
        $client = RiskyClient::findOrFail($input['risk_id']);

        unset($input['risk_id']);

        $client->update($input);

        $client->save();
    }
}
