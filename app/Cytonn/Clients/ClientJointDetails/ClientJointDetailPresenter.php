<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Clients\ClientJointDetails;

use Carbon\Carbon;
use Cytonn\Presenters\CountryPresenter;
use Laracasts\Presenter\Presenter;

class ClientJointDetailPresenter extends Presenter
{
    /*
     * Get the full name for the client joint details
     */
    public function fullname()
    {
        return trim($this->getTitle() . ' ' . ucfirst($this->firstname) . ' ' . ucfirst($this->lastname));
    }

    /*
     * Get the short name for the joint detail
     */
    public function getShortName()
    {
        return trim($this->getTitle() . ' ' . ucfirst($this->lastname));
    }

    /*
     * Get the full name for the client joint details
     */
    public function completeName()
    {
        $name = '';

        if ($this->title) {
            $name .= $this->title->name;
        }

        if ($this->firstname != '') {
            $name .= ' ' . ucfirst($this->firstname);
        }

        if ($this->middlename != '') {
            $name .= ' ' . ucfirst($this->middlename);
        }

        if ($this->lastname != '') {
            $name .= ' ' . ucfirst($this->lastname);
        }

        return trim($name);
    }

    /*
     * Get the gender name
     */
    public function genderName()
    {
        if ($this->gender_id) {
            return $this->gender->abbr;
        }

        return '';
    }

    /*
     * Get the date of birth
     */
    public function getDob()
    {
        if ($this->dob) {
            return Carbon::parse($this->dob)->toFormattedDateString();
        }

        return null;
    }

    /*
     * Get the country for the joint detail
     */
    public function getCountry()
    {
        if ($this->country) {
            return $this->country->name;
        }

        return null;
    }

    /*
     * Get Nationality
     */

    public function getNationality()
    {
        if ($this->nationality) {
            return $this->nationality->name;
        }

        return null;
    }

    /*
     * Get the title for the joint detail
     */
    public function getTitle()
    {
        return $this->title ? $this->title->name : null;
    }

    /**
     * @return mixed|null
     */
    public function getPhone()
    {
        $phone = trimPhoneNumber($this->telephone_home);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        $phone = trimPhoneNumber($this->telephone_cell);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        $phone = trimPhoneNumber($this->telephone_office);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        return null;
    }
}
