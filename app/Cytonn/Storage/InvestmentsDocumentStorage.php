<?php

namespace App\Cytonn\Storage;

class InvestmentsDocumentStorage
{
    protected $storage;

    public function __construct(DocumentStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function save($document, $name, $type)
    {
        $path = $type.'/'.$name;
        
        $this->storage->saveFile($document, $path);
    }

    public function get($type, $name)
    {
        $path = $type.'/'.$name;

        return $this->storage->getFile($path);
    }
}
