<?php

namespace App\Cytonn\Storage;

interface DocumentStorageInterface
{
    public function saveFile($file, $path);

    public function getFile($path);

    public function filesystem();
}
