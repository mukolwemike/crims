<?php
/**
 * Date: 04/07/2017
 * Time: 10:04
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Currencies\Converter\Adapters;

use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use GuzzleHttp\Client;

/**
 * Class CurrencyLayer
 *
 * @package Cytonn\Currencies\Converter\Adapters
 */
class CBKScrapingAdapter
{
    /**
     * @var Currency
     */
    protected $base;

    protected $currencies = [
        'USD' => 'US DOLLAR',
        'GBP' => 'STG POUND',
        'EUR' => 'EURO'
    ];

    /**
     * CurrencyLayer constructor.
     *
     * @param Currency $base
     */
    public function __construct(Currency $base)
    {
        $this->base = $base;
    }

    protected function client()
    {
        return new Client([
            'base_uri' => 'https://www.centralbank.go.ke'
        ]);
    }

    /**
     * @param Carbon $date
     * @param array $to
     * @return array
     */
    public function convert(Carbon $date, array $to)
    {
        if ($this->base->code != 'KES') {
            throw new \InvalidArgumentException("CBK Only supports KES as the base currency");
        }

        $responses = [];

        foreach ($to as $currency) {
            if (!isset($this->currencies[$currency->code])) {
                break;
            }
            
            $responses[$currency->code] = $this->get($date, $currency);
        }

        return $responses;
    }

    protected function get($date, $currency)
    {
        $date = Carbon::parse($date)->format('d/m/Y');

        $response = $this->client()->post(
            '/wp-admin/admin-ajax.php',
            [
                'query' => [
                    'action' => 'get_wdtable',
                    'table_id' => 32
                ],
                'form_params' => [
                    'draw' => 4,
                    'columns' => [
                        [
                            'data' => "0",
                            'name' => 'date_r',
                            'searchable' => "true",
                            'orderable' => "true",
                            'search' => [
                                'value' => $date . '~' . $date,
                                'regex' => "true"
                            ]
                        ],
                        [
                            'data' => "1",
                            'name' => 'currency',
                            'searchable' => "true",
                            'orderable' => "true",
                            'search' => [
                                'value' => $this->currencies[$currency->code],
                                'regex' => "false"
                            ]
                        ],
                        [
                            'data' => "2",
                            'name' => 'new_mean',
                            'searchable' => "true",
                            'orderable' => "true",
                            'search' => [
                                'value' => null,
                                'regex' => false
                            ]
                        ]
                    ],
                    'order' => [
                        'column' => "0",
                        'dir' => 'asc'
                    ],
                    'start' => "0",
                    'length' => "100",
                    'search' => [
                        'value' => null,
                        'regex' => false
                    ],
                    'sRangeSeparator' => '~'
                ]
            ]
        );

        $raw = \GuzzleHttp\json_decode($response->getBody()->getContents());

        return $this->parse($currency, $date, $raw);
    }

    protected function parse($currency, $date, $response)
    {
        $data = $response->data;

        if (count($data) > 1) {
            throw new \InvalidArgumentException("Only one result expected from cbk response");
        }

        if (count($data) == 0) {
            return null;
        }

        $exchange = $data[0]; //pick the first

        if (!isset($this->currencies[$currency->code])) {
            throw new \InvalidArgumentException("Currency not mapped to CBK equivalent");
        }

        if (($exchange[0] == $date) && ($exchange[1] == $this->currencies[$currency->code])) {
            return (float)$exchange[2];
        }

        throw new \InvalidArgumentException("The exchange response could not be validated");
    }
}
