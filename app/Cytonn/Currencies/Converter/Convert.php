<?php
/**
 * Date: 15/03/2017
 * Time: 11:06
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Currencies\Converter;

use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Adapters\CBKScrapingAdapter;
use Cytonn\Models\ExchangeRate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;

/**
 * Class Convert
 *
 * @package Cytonn\Currencies\Converter
 */
class Convert
{
    protected $adapter;

    protected $base;

    protected $cacheFor = 0;

    public static $system_base_currency;

    public static $cache;

    /**
     * Convert constructor.
     *
     * @param    Currency $base
     * @internal param $adapter
     */
    public function __construct(Currency $base)
    {
        $this->adapter = new CBKScrapingAdapter($base);

        $this->base = $base;

        if (!static::$system_base_currency) {
            $b = config('system.base_currency');

            static::$system_base_currency = Currency::where('code', $b)->first();
        }
    }

    public function fetch(Carbon $date, array $currencies)
    {
        return $this->adapter->convert($date, $currencies);
    }

    public function read(Currency $to, Carbon $date)
    {
        if ($this->base->id == $to->id) {
            return 1;
        }

        $key = $this->keyGen($to, $date);

        if (($val = $this->cache($key)) && $this->cacheFor > 0) {
            return $val;
        }

        $forward = ExchangeRate::where('base_id', $this->base->id)
            ->where('to_id', $to->id)
            ->where('date', '<=', $date->toDateString())
            ->first();

        if ($forward) {
            return $this->cache($key, $forward->rate);
        }

        $reverse = ExchangeRate::where('to_id', $this->base->id)
            ->where('base_id', $to->id)
            ->where('date', '<=', $date->toDateString())
            ->first();

        if ($reverse) {
            return $this->cache($key, 1/$reverse->rate);
        }

        throw new ModelNotFoundException(
            "Could not find exchange rate from {$this->base->code} to {$to->code} on {$date->toDateString()}"
        );
    }

    public function convert(Currency $to, Carbon $date)
    {
        return $this->read($to, $date);
    }
    
    protected function cache($key, $value = false)
    {
        if ($value) {
            $this->localCache($key, $value);

            Cache::put($key, $value, $this->cacheFor);

            return $this->validate($value);
        }

        if ($value = $this->localCache($key)) {
            return $value;
        }

        if (Cache::has($key)) {
            $value =  $this->validate(Cache::get($key));

            $this->localCache($key, $value);

            return $value;
        }

        return false;
    }

    protected function localCache($key, $value = false)
    {
        if ($value) {
            static::$cache[$key] = $value;

            return $value;
        }

        if (isset(static::$cache[$key])) {
            return static::$cache[$key];
        }

        return $value;
    }

    private function validate($result)
    {
        if ($result == 0 || $result == 1) {
            throw new \InvalidArgumentException("The exchange rate cannot be zero or 1");
        }

        return $result;
    }

    protected function keyGen(Currency $currency, Carbon $date)
    {
        return static::class."_exchange_rate_from{$this->base->id}_to_{$currency->id}_on_{$date->toDateString()}";
    }

    public function enableCaching($minutes = 0)
    {
        $this->cacheFor = $minutes;

        return $this;
    }
}
