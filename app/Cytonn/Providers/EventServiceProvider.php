<?php
/**
 * Date: 8/21/15
 * Time: 2:25 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Providers;

use Cytonn\Clients\Approvals\Handlers\ApprovalHandler;
use Cytonn\Handlers\EventHandler;
use Cytonn\Investment\Handlers\EventHandler as InvestmentsEventHandler;
use Cytonn\Portfolio\Handlers\EventHandler as PortfolioEventHandler;
use Cytonn\Realestate\Handlers\EventHandler as RealEstateEventHandler;
use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app['events']->listen('Cytonn.Investment.Approvals.Events.*', ApprovalHandler::class);
        $this->app['events']->listen('Cytonn.Clients.Approvals.Events.*', ApprovalHandler::class);

        $this->app['events']->listen('Cytonn.Investment.*', InvestmentsEventHandler::class);
        $this->app['events']->listen('Cytonn.Portfolio.*', PortfolioEventHandler::class);
        $this->app['events']->listen('Cytonn.Realestate.*', RealEstateEventHandler::class);
        $this->app['events']->listen('Cytonn.*', EventHandler::class);
    }
}
