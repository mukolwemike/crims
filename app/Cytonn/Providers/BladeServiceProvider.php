<?php
/**
 * Date: 19/09/2016
 * Time: 2:43 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    public function register()
    {
        Blade::directive(
            'module',
            function ($name) {
                return "<?php if(accessModule($name)): ?>";
            }
        );

        Blade::directive(
            'endModule',
            function () {
                return '<?php endif; ?>';
            }
        );

        Blade::directive(
            'permission',
            function ($name) {
                return "<?php if(hasPermission($name)): ?>";
            }
        );

        Blade::directive(
            'endPermission',
            function () {
                return '<?php endif; ?>';
            }
        );
    }
}
