<?php
/**
 * Date: 30/05/2016
 * Time: 11:42 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Providers;

use App\Cytonn\Storage\DocumentStorageInterface;
use Cytonn\Core\Storage\LocalStorage;
use Cytonn\Core\Storage\S3Storage;
use Cytonn\Core\Storage\StorageInterface;
use Illuminate\Support\ServiceProvider;

class StorageProvider extends ServiceProvider
{
    /**
     * @inheritDoc
     */
    public function register()
    {
        $driver = \config('filesystems.default');

        if ($driver == 's3') {
            $provider = S3Storage::class;
        } else {
            $provider = LocalStorage::class;
        }

        $this->app->bind(StorageInterface::class, $provider);
        $this->app->bind(DocumentStorageInterface::class, $provider);
    }
}
