<?php
/**
 * Date: 30/09/2016
 * Time: 15:09
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Providers;

use App\Cytonn\System\Locks\LockServiceProvider;
use Barryvdh\Debugbar\ServiceProvider as DebugbarServiceProvider;
use Collective\Html\FormFacade;
use Collective\Html\HtmlBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

//use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;

class Bootstrap extends ServiceProvider
{
    protected $localProviders = [
        \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
        DebugbarServiceProvider::class
    ];

    protected $stagingProviders = [
        DebugbarServiceProvider::class
    ];

    protected $providers = [
        \Cytonn\Authorization\ServiceProvider::class,
        LockServiceProvider::class
    ];

    public function boot()
    {
        $this->customValidationRules();
        $this->registerMacros();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServices();
    }

    protected function registerServices()
    {
        //Local only
        if ($this->app->environment() == 'local') {
            foreach ($this->localProviders as $provider) {
                $this->app->register($provider);
            }
        }

        if (in_array($this->app->environment(), ['staging', 'stage'])) {
            foreach ($this->stagingProviders as $provider) {
                $this->app->register($provider);
            }
        }

        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }

        $this->validation();
        $this->commandBus();
    }

    private function registerMacros()
    {
        Builder::macro(
            'lists',
            function ($column, $key = null) {
                return $this->pluck($column, $key)->all();
            }
        );

        Collection::macro(
            'lists',
            function ($column, $key = null) {
                return $this->pluck($column, $key)->all();
            }
        );

        Collection::macro(
            'toAssoc',
            function () {
                return $this->reduce(
                    function ($assoc, $keyValuePair) {
                        list($key, $value) = $keyValuePair;
                        $assoc[$key] = $value;
                        return $assoc;
                    },
                    new static
                );
            }
        );

        Collection::macro(
            'mapToAssoc',
            function ($callback) {
                return $this->map($callback)->toAssoc();
            }
        );

        Collection::macro(
            'reduceUntil',
            function (callable $callback, $stopWhen, $initial = null) {
                $val = $initial;

                foreach ($this->items as $item) {
                    $val = call_user_func_array($callback, [$val, $item]);

                    if ($val === $stopWhen) {
                        return $val;
                    }
                }

                return $val;
            }
        );

        Collection::macro('sortByDate', function ($column = 'created_at', $order = SORT_DESC) {
            if ($order == 'ASC') {
                $order = SORT_ASC;
            }
            if ($order == 'DESC') {
                $order = SORT_ASC;
            }

            return $this->sortBy(
                function ($item) use ($column) {
                    return strtotime($item->$column);
                },
                SORT_REGULAR,
                $order == SORT_DESC
            );
        });

        FormFacade::macro(
            'date',
            function ($name, $value, $options = []) {
                return '<input type="date" name="' . $name . '" value="' . $value . '"' .
                    app(HtmlBuilder::class)->attributes($options) . ' />';
            }
        );

        FormFacade::macro(
            'number',
            function ($name, $value, $options = []) {
                if (!isset($options['step'])) {
                    $options = array_add($options, 'step', 'any');
                }

                return '<input type="number" name="' . $name . '" value="' . $value . '"' .
                    app(HtmlBuilder::class)->attributes($options) . ' />';
            }
        );
    }

    private function validation()
    {
        $this->app->bind(
            'Laracasts\Validation\FactoryInterface',
            'Laracasts\Validation\LaravelValidator'
        );
    }

    private function commandBus()
    {
        $this->app->bind(
            'Laracasts\Commander\CommandBus',
            function ($app) {
                return $app->make('Laracasts\Commander\DefaultCommandBus');
            }
        );

        $this->app->bind(
            'Laracasts\Commander\CommandTranslator',
            'Laracasts\Commander\BasicCommandTranslator'
        );
    }

    private function customValidationRules()
    {
        Validator::extend('same_or_after', '\Cytonn\Core\Validation\CustomValidation@validateSameOrAfter');
        Validator::extend('same_or_before', '\Cytonn\Core\Validation\CustomValidation@validateSameOrBefore');
    }
}
