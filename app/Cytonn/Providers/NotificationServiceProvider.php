<?php
/**
 * Date: 05/12/2015
 * Time: 11:34 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Providers;

use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app['events']->listen('Cytonn.*', 'Cytonn\Handlers\NotificationHandler');
    }
}
