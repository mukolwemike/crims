<?php
/**
 * Date: 9/14/15
 * Time: 8:49 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Providers;

use Illuminate\Support\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app['events']->listen('Cytonn.*', 'Cytonn\Handlers\LogHandler');
    }
}
