<?php
/**
 * Date: 19/08/2016
 * Time: 10:26 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Providers;

use Cytonn\Investment\FundManager\FundManagerScope;
use Illuminate\Support\ServiceProvider;

class FundManagerServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            FundManagerScope::class,
            function () {
                $scope = new FundManagerScope();

                try {
                    if (\Session::has('selected_fund_manager_id')) {
                        $scope->setSelectedFundManager(\Session::get('selected_fund_manager_id'));
                    } else {
                        $scope->setSelectedFundManager(0);
                    }
                } catch (\Exception $e) {
                }

                return $scope;
            }
        );
    }
}
