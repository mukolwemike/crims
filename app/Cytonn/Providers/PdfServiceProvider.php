<?php
/**
 * Date: 27/02/2018
 * Time: 19:34
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Providers;

use Barryvdh\DomPDF\ServiceProvider;
use Cytonn\Core\DataStructures\PDF;

class PdfServiceProvider extends ServiceProvider
{
    public function register()
    {
        parent::register();

        $this->app->bind('dompdf.wrapper', function ($app) {
            return new PDF($app['dompdf'], $app['config'], $app['files'], $app['view']);
        });
    }
}
