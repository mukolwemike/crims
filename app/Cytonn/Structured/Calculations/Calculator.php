<?php
/**
 * Date: 05/02/2018
 * Time: 11:56
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Structured\Calculations;

use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;

abstract class Calculator
{
    use CalculatorTrait;

    protected $investment;

    protected $prepared;

    public function principal()
    {
        return $this->prepared->total->principal;
    }

    public function netInterest()
    {
        return $this->prepared->total->net_interest_cumulative;
    }

    public function netInterestBeforeDeductions()
    {
        return collect($this->prepared->actions)->sum('net_interest') + $this->prepared->total->net_interest;
    }

    public function grossInterestBeforeDeductions()
    {
        return collect($this->prepared->actions)->sum('gross_interest') + $this->prepared->total->gross_interest;
    }

    public function grossInterest()
    {
        return $this->prepared->total->gross_interest_cumulative;
    }

    public function withholdingTax()
    {
        return $this->grossInterestBeforeDeductions() - $this->netInterestBeforeDeductions();
    }

    public function value()
    {
        return $this->prepared->total->total;
    }

    public function total()
    {
        return $this->value();
    }

    public function adjustedMarketValue()
    {
        return $this->principal() + $this->adjustedGrossInterest();
    }

    public function amortizedPrincipal(Carbon $date = null)
    {
        return 0;
    }

    public function marketValueBeforeDeductions()
    {
        return $this->investment->amount + $this->netInterestBeforeDeductions() + $this->amortizedPrincipal();
    }

    public function grossMarketValueBeforeDeductions()
    {
        return $this->investment->amount + $this->grossInterestBeforeDeductions() + $this->amortizedPrincipal();
    }

    abstract public function adjustedGrossInterest();
}
