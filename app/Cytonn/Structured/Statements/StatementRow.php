<?php
/**
 * Date: 31/10/2017
 * Time: 12:03
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Structured\Statements;

use Carbon\Carbon;
use Cytonn\Models\StructuredInvestment;

abstract class StatementRow
{
    protected $investment;

    private $date;

    /**
     * StatementRow constructor.
     *
     * @param $investment
     * @param Carbon     $date
     */
    public function __construct(StructuredInvestment $investment, Carbon $date)
    {
        $this->investment = $investment;
        $this->date = $date;
    }


    public function process()
    {
        $actions = $this->actions();

        $actions = $this->computeBalances($actions);

        return [
            'principal' => $this->principal(),
            'rate' => $this->rate(),
            'value_date' => $this->valueDate(),
            'actions' => $actions,
        ];
    }

    protected function principal()
    {
        return $this->investment->amount;
    }

    protected function rate()
    {
        return $this->investment->interest_rate;
    }

    protected function valueDate()
    {
        return $this->investment->invested_date;
    }

    abstract protected function actions();

    protected function computeBalances(array $actions)
    {
        $actions = collect($actions)->sortByDesc('date')
            ->each(
                function ($action) {
                    $date = $action->date;
                }
            );

        return $actions;
    }
}
