<?php
/**
 * Date: 31/10/2017
 * Time: 11:52
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Structured\Statements;

use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;

abstract class Processor
{
    use CalculatorTrait;

    protected $statementDate;

    abstract public function process(Carbon $date);
}
