<?php
/**
 * Date: 09/02/2018
 * Time: 18:31
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Structured\Analytics;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;
use Cytonn\Models\StructuredInvestment;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

abstract class Analytics
{
    protected $activeInvestments;

    protected $date;

    protected $baseCurrency;

    const REMEMBER = 10;

    public static $productCurrencies = [];

    /**
     * Analytics constructor.
     */
    public function __construct()
    {
        memoryLimit('1024M');
    }


    /**
     *
     * @return Collection
     */
    public function getInvestments()
    {
        if (!$this->activeInvestments) {
            $this->activeInvestments = $this->activeInvestments()
                ->get()
                ->each(function (StructuredInvestment $investment) {
                    $investment->calculated = $investment->calculate($this->date, true);
                });
        }

        return $this->activeInvestments;
    }

    /**
     * @return Builder
     */
    abstract protected function activeInvestments();

    /**
     * @param mixed $baseCurrency
     * @return Analytics
     */
    public function setBaseCurrency($baseCurrency) : Analytics
    {
        $this->baseCurrency = $baseCurrency;

        return $this;
    }

    protected function getCurrencyForClientInvestment(ClientInvestment $investment)
    {
        $key = 'product_'.$investment->product_id;

        if (isset(static::$productCurrencies[$key])) {
            return static::$productCurrencies[$key];
        }

        $currency = $investment->product->currency;

        return static::$productCurrencies[$key] = $currency;
    }

    protected function convertClientInvestmentAmount(ClientInvestment $investment, $amount, $date)
    {
        $currency = $this->getCurrencyForClientInvestment($investment);

        return $this->convertAmount($amount, $currency, $date);
    }

    protected function convertAmount($amount, Currency $currency, Carbon $date)
    {
        $base = $currency;

        if ($this->baseCurrency) {
            $base = $this->baseCurrency;
        }

        $exchange = (new Convert($base))->enableCaching(static::REMEMBER)->read($currency, $date);

        return $amount * $exchange;
    }

    protected function convertClientInvestmentRate(ClientInvestment $investment, $rate, $date)
    {
        $currency = $this->getCurrencyForClientInvestment($investment);

        return $this->convertRate($rate, $currency, $date);
    }

    protected function convertRate($rate, Currency $currency, Carbon $date)
    {
        $base = $currency;
        if ($this->baseCurrency) {
            $base = $this->baseCurrency;
        }

        $converter = new Convert($currency);
        $converter->enableCaching(static::REMEMBER);
        $today = 1/$converter->read($base, $date);
        $yest = 1/$converter->read($base, $date->copy()->subDay());

        return (100* ($today - $yest)/$yest) + $rate;
    }
}
