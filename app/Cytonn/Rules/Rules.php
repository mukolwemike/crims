<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Rules;

use Illuminate\Support\Facades\Validator;

trait Rules
{
    /*
     * Overall validation checker depending on the passed request and rules
     */
    public function verdict($request, $rules, $messages = [])
    {
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return $validator;
        }
    }

    /**
     * For test purposes
     *
     * @param  $request
     * @param  $rules
     * @param  array   $messages
     * @return mixed
     */
    public function check($request, $rules, $messages = [])
    {
        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            return $validator;
        }
    }
}
