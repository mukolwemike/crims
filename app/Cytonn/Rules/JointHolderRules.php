<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 10/12/17
 * Time: 8:12 AM
 */

namespace Cytonn\Rules;

trait JointHolderRules
{
    use Rules;

    public function jointHolder($request)
    {
        $rules = [
            'title_id' => 'required|numeric',
            'firstname' => 'required',
            'lastname' => 'required',
            'email'=>'required|email',
            'pin_no'=>'required',
            'id_or_passport'=>'required',
            'telephone_cell' => 'required'
        ];

        return $this->verdict($request, $rules);
    }
}
