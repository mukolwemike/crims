<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Models\Client;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\System\Locks\Facade\Lock;
use App\Cytonn\Unitization\ProcessTransfer;
use App\Cytonn\USSD\Action;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\ClientPresenter;

class TransferAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Please input the amount you would like to transfer: ' .
                    '{account_balance}',
                'pre' => 'getUserBalance',
                'post' => 'confirmAmount',
                'field' => 'number'
            ],
            [
                'query' => 'Please input the Client Code of client to transfer to. ' . PHP_EOL . PHP_EOL .
                    '0:BACK 00.BACK HOME',
                'post' => 'confirmClient',
                'field' => 'receiver_code'
            ],
            [
                'query' => 'Please confirm transfer details.' . PHP_EOL . PHP_EOL .
                    'From: {sender}' . PHP_EOL .
                    'To: {recipient}' . PHP_EOL .
                    'Amount: {number} units' . PHP_EOL . PHP_EOL .
                    '1:CONFIRM 2:CHANGE 00:BACK HOME',
                'pre' => 'getDetailsToConfirm',
                'post' => 'confirmDetails',
                'field' => 'details_confirmed'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond(
            'CMMF units transfer was successfully done. You will receive an Email/SMS for confirmation.',
            'end'
        );
    }

    /**
     * @param $step
     * @return array|object
     * @throws \Exception
     */
    protected function getUserBalance($step)
    {
        $clientFunds = $this->handler->getClientFundDetails();

        if ((int)$clientFunds['ownedUnits'] < 1) {
            return $this->respond(
                "You have zero units to transfer. Dial *809# to invest.",
                'end'
            );
        }

        $str = PHP_EOL . 'Account Balance: ' . $clientFunds['ownedUnits'] . ' units,' . PHP_EOL;

        $withdrawable = AmountPresenter::currency($clientFunds['withdrawable_units'], false, 0);

        $str = $str . 'Transferable Units: ' . $withdrawable . ' units.';

        $str = $str . PHP_EOL . PHP_EOL . 'OO:BACK HOME';

        $q = str_replace('{account_balance}', $str, $step['query']);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function confirmAmount($step)
    {
        $input = $this->latestInput();

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $units = str_replace(',', '', ($this->handler->getClientFundDetails())['withdrawable_units']);
        $input = str_replace(',', '', $input);

        if ((float)$input < ProcessTransfer::MIN_AMOUNT || (float)$input > ProcessTransfer::MAX_AMOUNT) {
            return (object)[
                'status' => false,
                'message' => "The units to be transferred " . (float)$input . " are not between " .
                    ProcessTransfer::MIN_AMOUNT . " and " . ProcessTransfer::MAX_AMOUNT . " " . $step['query']
            ];
        } else {
            if ((float)$input > (int)$units) {
                return (object)[
                    'status' => false,
                    'message' => 'You can\'t transfer units more than your transferable units, try again. ' . $step['query']
                ];
            } else {
                return true;
            }
        }
    }

    protected function confirmClient($step)
    {
        $input = $this->latestInput();

        if ($input === '0') {
            return (object)[
                'status' => 'back'
            ];
        }

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $client = Client::where('client_code', (int)$input)->first();

        if ($client->client_code === $this->handler->getSelectedClient()->client_code) {
            return (object)[
                'status' => false,
                'message' => 'You can\'t transfer units to your own account, try again. ' . $step['query']
            ];
        }

//        if (!$this->handler->checkIfCMMFClient($client)) {
//            return (object)[
//                'status' => false,
//                'message' => 'Can\'t make this transfer as the client has no CMMF investment, try again. ' . $step['query']
//            ];
//        }

        if ($client) {
            return true;
        }

        $msg = 'Client not found, confirm the client code and try again. ' . PHP_EOL . $step['query'];

        return (object)[
            'status' => false,
            'message' => $msg
        ];
    }

    /**
     * @param $step
     * @param null $message
     * @return array
     * @throws \Exception
     */
    protected function getDetailsToConfirm($step, $message = null)
    {
        if (!$message) {
            $message = $step['query'];
        }

        $data = $this->handler->getSessionData('data');

        $sender = $this->handler->getSelectedClient();
        $receiver = Client::where('client_code', (int)$data['receiver_code'])->first();

        $sender = ClientPresenter::presentFullNameNoTitle($sender->id) . ' - ' . $sender->client_code;
        $receiver = ClientPresenter::presentFullNameNoTitle($receiver->id) . ' - ' . $receiver->client_code;

        $message = str_replace('{sender}', $sender, $message);
        $message = str_replace('{recipient}', $receiver, $message);
        $message = str_replace('{number}', $data['number'], $message);

        return $this->respond($message, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Throwable
     */
    protected function confirmDetails($step)
    {
        $input = $this->latestInput();

        if ($input == 00) {
            return (object)[
                'status' => 'reset'
            ];
        }

        $client = $this->handler->getSelectedClient();

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return (object)[
                'status' => 'end',
                'message' => 'We are processing one of your transaction, kindly wait a few seconds then try again. ' .
                    'Contact 0709101000 for assistance.'
            ];
        }

        if ($input == 1 && $this->processTransfer()) {
            return true;
        }

        if ($input == 2) {
            return (object)[
                'status' => 'back',
                'steps' => 2
            ];
        }

        return (object)[
            'status' => false,
            'message' => 'Incorrect input, try again. ' . PHP_EOL . $step['query']
        ];
    }

    /**
     * @throws \Throwable
     */
    protected function processTransfer()
    {
        $details = $this->handler->getSessionData('data');
        $fund = $this->handler->getFund();
        $client = $this->handler->getSelectedClient();

        $input['unit_fund_id'] = $fund->id;
        $input['number'] = $details['number'];
        $input['transferer_id'] = $client->id;
        $input['user_id'] = $this->handler->getUser()->id;
        $input['date'] = Carbon::now()->format('Y-m-d');
        $input['transferee_id'] = Client::where('client_code', (int)$details['receiver_code'])->first()->id;
        $input['channel'] = 'ussd';

        \DB::transaction(function () use ($input, $client, $fund) {
            $transferProcessor = new ProcessTransfer($input, $client, $fund);

            $transferProcessor->process();
        });
    }
}
