<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;
use App\Jobs\USSD\ReferClient;

class ReferClientAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Please Enter the Referred Client\'s Full Name:' . PHP_EOL . PHP_EOL .
                    '00:BACK HOME',
                'post' => 'confirmInput',
                'field' => 'fullname'
            ],
            [
                'query' => 'Please Enter the Referred Client\'s Phone Number:',
                'field' => 'phone'
            ],
            [
                'query' => 'Please confirm the referred client details.' . PHP_EOL . PHP_EOL .
                    'Name: {fullname}' . PHP_EOL .
                    'Phone: {phone}' . PHP_EOL . PHP_EOL .
                    '1:CONFIRM 2:CHANGE 00:BACK HOME',
                'pre' => 'getDetailsToConfirm',
                'post' => 'confirmDetails',
                'field' => 'details_confirmed'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond('Thank you for referring this client. We shall follow up on the lead.', 'end');
    }

    protected function confirmInput()
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        return true;
    }

    protected function getDetailsToConfirm($step, $message = null)
    {
        if (!$message) {
            $message = $step['query'];
        }

        $data = $this->handler->getSessionData('data');

        $message = str_replace('{fullname}', $data['fullname'], $message);
        $message = str_replace('{phone}', $data['phone'], $message);

        return $this->respond($message, 'con');
    }

    protected function confirmDetails($step)
    {
        $input = $this->latestInput();

        if ($input == 00) {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input == 1) {
            $this->saveDetails();

            return true;
        }

        if ($input == 2) {
            return (object)[
                'status' => 'back',
            ];
        }

        $msg = 'Incorrect input, try again ' . PHP_EOL . $step['query'];

        return (object)[
            'status' => false,
            'message' => $msg
        ];
    }

    protected function saveDetails()
    {
        $clientUser = $this->handler->getUser();

        $data = $this->handler->getSessionData('data');

        $data['client_name'] = $clientUser->firstname . ' ' . $clientUser->lastname;

        $data['source'] = 'USSD';

        $data['product'] = 'Cytonn Money Market Fund';

        dispatch(new ReferClient($data));
    }
}
