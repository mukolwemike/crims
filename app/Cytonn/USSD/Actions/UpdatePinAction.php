<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Models\Client;
use App\Cytonn\USSD\Action;
use App\Cytonn\USSD\USSDRepository;
use Cytonn\Presenters\ClientPresenter;
use Exception;

class UpdatePinAction extends Action
{
    public function steps()
    {
        return [
            [
                'pre' => 'getUserAccounts',
                'query' => "Select Account to update" . PHP_EOL . PHP_EOL .
                    '{accounts}',
                'field' => 'account_option',
                'post' => 'confirmAccountOption'
            ],
            [
                'query' => "Please enter your Tax Pin Number" . PHP_EOL . PHP_EOL .
                    '0:BACK 00:BACK HOME',
                'field' => 'pin_no',
                'post' => 'updateUserIdDetails'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond(
            "Account TAX PIN updated successfully.",
            'end'
        );
    }

    /**
     * @param $step
     * @return array|object
     */
    protected function getUserAccounts($step)
    {
        $clients = $this->handler->getClientsAccounts();

        $str = '';
        foreach ($clients as $index => $client) {
            $name = ClientPresenter::presentFullNameNoTitle($client->id);

            $key = $index + 1;
            $str .= "{$key}: {$name} - {$client->client_code}" . PHP_EOL;
        }

        $output = $str . PHP_EOL . 'OO:BACK HOME';

        $q = str_replace('{accounts}', $output, $step['query']);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return array|bool|object
     * @throws Exception
     */
    protected function confirmAccountOption($step)
    {
        $input = $this->latestInput();

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $clients = $this->handler->getClientsAccounts();

        if ($input < 1 && (int)$input > $clients->count()) {
            return (object)[
                'status' => 'reset',
                'message' => 'Invalid input ' . PHP_EOL . $step['query']
            ];
        }

        $this->handler->setSessionData('account_option', $input);

        if ($this->pinVerified(($this->handler->getSelectedClient()))) {
            return (object)[
                "message" => "Your account already has a validated PIN Number.",
                "status" => "end"
            ];
        }

        return true;
    }

    protected function pinVerified(Client $client)
    {
        return (bool)$client->repo->checkValidatedPIN();
    }

    /**
     * @return bool|object
     * @throws Exception
     */
    protected function updateUserIdDetails()
    {
        $input = $this->latestInput();

        if ($input === '0') {
            return (object)[
                'status' => 'back',
                'steps' => 1
            ];
        }

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $pin = trim($input);

        $client = $this->handler->getSelectedClient();

        $pinVerified = (bool)$this->pinVerified($client);

        if ($client->pin_no === $pin && $pinVerified) {
            return (object)[
                "message" => "Your account already has a valid TAX PIN Number.",
                "status" => "end"
            ];
        }

        if ($client->pin_no === $pin && !$pinVerified) {
            return (object)[
                'status' => 'end',
                'message' => "Your TAX PIN Number is awaiting validation."
            ];
        }

        $client->update(['pin_no' => $pin]);

        (new USSDRepository())->sendValidateEmailRequestOps($client);

        return true;
    }
}
