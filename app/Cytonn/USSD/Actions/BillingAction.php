<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;

class BillingAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Select Billing Option' . PHP_EOL . PHP_EOL .
                    '1. All Bills.' . PHP_EOL .
                    '2. My Bills.' . PHP_EOL .
                    '3. Buy Airtime.' . PHP_EOL .
                    '00: BACK HOME',
                'post' => 'confirmBillingOption',
                'field' => 'bill_option'
            ]
        ];
    }

    /**
     * @throws \Exception
     */
    public function nextAction()
    {
        $input = $this->handler->getSessionData('data.bill_option');

        $client = $this->handler->getSelectedClient();

        switch ($input) {
            case '1':
            case '2':
                return $this->redirect(PayBillAction::class);
                break;
            case '3':
                return $this->redirect(BuyAirtimeAction::class);
                break;
            default:
                return $this->redirect(BillingAction::class);
        }
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function confirmBillingOption($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input < 1 || $input > 3) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        $this->handler->setBillOption($input);

        return true;
    }
}
