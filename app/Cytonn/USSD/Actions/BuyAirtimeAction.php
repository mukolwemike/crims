<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Billing\BillingRepository;
use App\Cytonn\Models\Billing\UtilityBillingServices;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\USSD\Action;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class BuyAirtimeAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Select Vendor' . PHP_EOL . PHP_EOL .
                    '{vendorOption}',
                'post' => 'vendorOption',
                'pre' => 'getVendorOption',
                'field' => 'bill_id'
            ],
            [
                'query' => 'Enter Phone Number(e.g: 0712345678)',
                'post' => 'verifyInput',
                'field' => 'phone_number'
            ],
            [
                'query' => 'Enter Amount to buy. {balance}',
                'pre' => 'getBalance',
                'post' => 'verifyAmount',
                'field' => 'amount'
            ],
            [
                'query' => 'Select Account to buy from: ' . PHP_EOL . PHP_EOL .
                    '{sourceFunds}',
                'pre' => 'getSourceFund',
                'post' => 'verifySourceFund',
                'field' => 'source_fund'
            ],
            [
                'query' => '{confirmMessage}',
                'pre' => 'getConfirmMessage',
                'post' => 'confirmDetails',
                'field' => 'confirm_option'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond(
            'You will receive confirmation text messages.',
            'end'
        );
    }

    /**
     * @param $step
     * @return array
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    protected function getBalance($step)
    {
        $clientFunds = $this->handler->getClientFundDetails();

        if ((int)$clientFunds['ownedUnits'] < 1) {
            return $this->respond(
                "You have zero units to pay from. Dial *809# to invest.",
                'end'
            );
        }

        $payableAmount = \App\Cytonn\Presenters\General\AmountPresenter::currency($clientFunds['withdrawable_units'], false, 0);

        $str = 'Available Balance: ' . $payableAmount . '.';

        $str = $str . PHP_EOL . PHP_EOL . '00:BACK HOME';

        $q = str_replace('{balance}', $str, $step['query']);

        return $this->respond($q, 'con');
    }

    protected function getVendorOption($step)
    {
        $query = $step['query'];

        $options = $this->handler->getAllBillsNames(2);

        $str = '';

        foreach ($options as $index => $option) {
            $key = $index + 1;
            $str .= "{$key}: {$option}" . PHP_EOL;
        }

        $output = $str . PHP_EOL . '00:BACK HOME';

        $q = str_replace('{vendorOption}', $output, $query);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     */
    protected function vendorOption($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input < 1 || $input > count($this->handler->getAllBillsNames(2))) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function verifyInput($step)
    {
        $input = $this->latestInput();

        if ($input == '' || $this->handler->validateEmpty($input)) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function verifyAmount($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input == '' || $this->handler->validateEmpty($input)) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        $name = $this->handler->getAirtimeName();

        $clientFunds = $this->handler->getClientFundDetails();

        $units = str_replace(',', '', $clientFunds['withdrawable_units']);
        $input = str_replace(',', '', $input);

        if ((float)$input > $units) {
            return (object)[
                'status' => false,
                'message' => 'Amount entered is more than the available balance, try again. ' . $step['query']
            ];
        }

        $bill = $this->handler->getBill($name);

        if ((float)$input < $bill->minimum_pay_amount) {
            return (object)[
                'status' => false,
                'message' => 'Minimum airtime amount is KES ' . $bill->minimum_pay_amount . ', try again. ' . $step['query']
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return array
     */
    protected function getSourceFund($step)
    {
        $query = $step['query'];

        $sources = $this->handler->getBillsSources();

        $str = '';
        foreach ($sources as $index => $source) {
            $key = $index + 1;
            $str .= "{$key}: {$source}" . PHP_EOL;
        }

        $q = str_replace('{sourceFunds}', $str, $query);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     */
    protected function verifySourceFund($step)
    {
        $input = $this->latestInput();

        if ($input == '') {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return array
     * @throws \Exception
     */
    protected function getConfirmMessage($step)
    {
        $query = $step['query'];

        $data = $this->handler->getSessionData('data');

        $str = 'Confirm buying airtime for ' . $data['phone_number'] . ' KSH ' . $data['amount'] . PHP_EOL . PHP_EOL .
            '1. Confirm ' . PHP_EOL .
            '2. Cancel ' . PHP_EOL;

        $q = str_replace('{confirmMessage}', $str, $query);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function confirmDetails($step)
    {
        $input = $this->latestInput();

        if ($input == '' || $input < 1 || $input > 2) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        if ($input == 1) {
            $this->saveDetails();

            return true;
        }

        if ($input == 2) {
            return (object)[
                'status' => 'reset'
            ];
        }

        return (object)[
            'status' => false,
            'message' => 'Incorrect input, try again. ' . PHP_EOL . $step['query']
        ];
    }

    /**
     * @throws \Exception
     */
    protected function saveDetails()
    {
        $input = $this->handler->getSessionData('data');

        $client = $this->handler->getSelectedClient();

        $phone_to_buy = $input['phone_number'];

        $fund = UnitFund::where('name', $this->handler->getBillSourceName())->remember(static::CACHE_FOR)->first();

        $input['client_id'] = $client->id;

        $input['account_number'] = $this->cleanPhone($phone_to_buy);

        $input['user_id'] = $this->handler->getUser() ? $this->handler->getUser()->id : null;

        $input['unit_fund_id'] = $fund ? $fund->id : null;

        $utility = UtilityBillingServices::where('type_id', 2)
            ->where('name', $this->getBillName())
            ->remember(static::CACHE_FOR)->first();

        $input['account_name'] = $utility->name;

        $phone = Arr::first($client->getContactPhoneNumbersArray());

        $input['msisdn'] = $phone ? $phone : $this->handler->getPhoneNumber();

        $input['date'] = Carbon::now()->toDateString() ;

        $input['channel'] = 'ussd';

        (new BillingRepository())->saveBuyAirtimeRequest($input, $client, $utility);

//        $this->sendRequestNotification($client, $utility, $input, 'bill');
    }

    private function cleanPhone($data)
    {
        $phone = ltrim($data, '+');
        $phone = ltrim($phone, '254');
        $phone = ltrim($phone, '0');

        return '254' . $phone;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getBillName()
    {
        $bill_id = $this->handler->getSessionData('data.bill_id');

        $index = (int)$bill_id - 1;

        $bills = $this->handler->getAllBillsNames(2);

        return isset($bills[$index]) ? $bills[$index] : 'Not Found';
    }
}
