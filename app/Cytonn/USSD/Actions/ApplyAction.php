<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\Country;
use App\Cytonn\Models\FundSource;
use App\Cytonn\Models\Investment\UssdTransactionLog;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\USSD\Action;
use App\Cytonn\USSD\USSDRepository;
use App\Jobs\USSD\ProcessClientAccountCreation;
use Cytonn\Presenters\AmountPresenter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 28/03/2019
 * Time: 08:31
 */
class ApplyAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Please enter your Full Name' . PHP_EOL . 'Format: Firstname Middlename Lastname.',
                'field' => 'fullname',
                'post' => 'saveApplicationAttempt'
            ],
            [
                'query' => 'Please enter your ID No./Passport No:',
                'field' => 'id_or_passport',
                'post' => 'validateID'
            ],
            [
                'query' => 'Please enter your email address (Optional):' . PHP_EOL .
                    'Enter 00 if you have no email' . PHP_EOL,
                'field' => 'email',
                'post' => 'saveApplicationAttempt'
            ],
            [
                'query' => '(Optional) If you have been referred by our staff or financial advisor, enter referral code. ' . PHP_EOL.
                    'Enter 00 to skip'
                    .PHP_EOL . PHP_EOL .
                    'Referral Code: ',
                'field' => 'referrer_code',
                'post' => 'validateReferrer'
            ],
            [
                'query' => 'Please enter the source of funds' . PHP_EOL . PHP_EOL .
                    '{details}',
                'pre' => 'getSourcesOfFunds',
                'post' => 'otherOption',
                'field' => 'funds_source_id'
            ],
            [
                'query' => 'Please specify the other option', 'field' => 'funds_source_other'
            ],
            [
                'query' => 'Please enter the amount to invest:',
                'post' => 'validateAmount',
                'field' => 'amount'
            ],
            [
                'query' => 'Please choose the method of payment:' . PHP_EOL
                    . PHP_EOL .
                    '1.  M-PESA.' . PHP_EOL .
                    '2.  Bank Transfer or Deposit.',
                'field' => 'payment_method',
                'post' => 'sendPaymentInfo'
            ],
            [
                'query' => 'Please confirm your details.' . PHP_EOL .
                    'Name: {fullname}' . PHP_EOL .
                    'ID/Passport: {id_or_passport}' . PHP_EOL .
                    'Email: {email}' . PHP_EOL .
//                    'Source of funds: {funds_source_id}' . PHP_EOL .
//                    'Referred by: {referrer}' . PHP_EOL .
//                    'Amount to invest: {amount}' . PHP_EOL . PHP_EOL .
                    'Enter 1 to Confirm Details, 2 Change Details',
                'pre' => 'getDetailsToConfirm',
                'post' => 'confirmDetails',
                'field' => 'details_confirmed'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond(
            'Thank you for your application. You will receive a text message advising on next steps.',
            'end'
        );
    }

    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function sendPaymentInfo($step)
    {
        $input = $this->latestInput();

        if ($input < 1 || $input > 2) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return array
     */
    protected function getSourcesOfFunds($step)
    {
        $query = $step['query'];

        $sources = FundSource::where('name', '!=', 'Other')
            ->where('active', '=', 1)
            ->orderBy('weight')
            ->remember(static::CACHE_FOR)
            ->pluck('name')
            ->values()->all();

        $str = '';
        foreach ($sources as $index => $source) {
            $key = $index + 1;
            $str .= "{$key}: {$source}" . PHP_EOL;
        }

        $output = $str . '00: Other';

        $q = str_replace('{details}', $output, $query);

        return $this->respond($q, 'con');
    }

    protected function otherOption($step)
    {
        $input = $this->latestInput();

        if ($input == '') {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again ' . PHP_EOL . $step['query']
            ];
        }

        if ($input === '00') {
            return true;
        }

        return (object)[
            'status' => 'skip',
            'steps' => 1
        ];
    }

    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function validateAmount($step)
    {
        $input = $this->latestInput();
        $fund = $this->handler->getFund();
        $minInvest = AmountPresenter::currency($fund->minimum_investment_amount);

        if ($input == '') {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again ' . PHP_EOL . $step['query']
            ];
        }

        if ($this->validateInvestmentAmount()) {
            return true;
        }

        return (object)[
            'status' => false,
            'message' => "Initial minimum investment: {$fund->currency->code} {$minInvest}. " . $step['query']
        ];
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function validateInvestmentAmount()
    {
        $input = str_replace(',', '', $this->latestInput());
        $fund = $this->handler->getFund();

        return ((float)$input) >= $fund->minimum_investment_amount;
    }

    protected function validateID($step)
    {
        $input = $this->latestInput();

        if (strlen($input) < 5) {
            return (object)[
                'status' => false,
                'message' => 'ID must be at least 5 characters. Try again ' . PHP_EOL . $step['query']
            ];
        }

        return true;
    }


    /**
     * @return string
     * @throws \Exception
     */
    protected function getSourceName()
    {
        $src_id = $this->handler->getSessionData('data.funds_source_id');

        $sources = FundSource::where('name', '!=', 'Other')
            ->remember(static::CACHE_FOR)
            ->pluck('name')
            ->values()->all();

        if ($src_id == '00') {
            return 'Other';
        }

        $index = (int)$src_id - 1;

        return isset($sources[$index]) ? $sources[$index] : 'Not Found';
    }

    /**
     * @param $step
     * @return mixed
     * @throws \Exception
     */
    protected function validateReferrer($step)
    {
        $code = $this->latestInput();
        $fa = (new USSDRepository())->financialAdvisor($code);

        if ($this->handler->validateEmpty($code) || $fa) {
            return true;
        }

        $msg = 'Referrer not found, try again or leave empty to proceed ' . PHP_EOL . $step['query'];

        return (object)[
            'status' => false,
            'message' => $msg
        ];
    }

    /**
     * @param $step
     * @param null $message
     * @return array
     * @throws \Exception
     */
    protected function getDetailsToConfirm($step, $message = null)
    {
        if (!$message) {
            $message = $step['query'];
        }

        $data = $this->handler->getSessionData('data');

        $src_id = $data['funds_source_id'];

        $source = $this->getSourceName();

        if ($src_id == '00') {
            $source = $data['funds_source_other'];
        }

        $referrer = (new USSDRepository())->financialAdvisor($data['referrer_code']);

        $referred_by = $referrer ? $referrer->name . " ($referrer->referral_code)" : 'None';

        $email = 'None';

        $amount = AmountPresenter::currency($data['amount'], false, 2);

        if (!$this->handler->validateEmpty(trim($data['email']))) {
            $email = empty(trim($data['email'])) ? 'None' : trim($data['email']);
        }

        $message = str_replace('{fullname}', $data['fullname'], $message);
        $message = str_replace('{id_or_passport}', $data['id_or_passport'], $message);
        $message = str_replace('{funds_source_id}', $source, $message);
        $message = str_replace('{referrer}', $referred_by, $message);
        $message = str_replace('{email}', $email, $message);
        $message = str_replace('{amount}', $amount, $message);

        return $this->respond($message, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function confirmDetails($step)
    {
        $input = $this->latestInput();

        if ($input == 1) {
            if ($this->handler->checkUserApplication()) {
                return (object)[
                    'status' => 'end',
                    'message' => "An application already exists for this number. Please contact us for clarifications."
                ];
            }

            $this->saveDetails();

            return true;
        }

        if ($input == 2) {
            return (object)[
                'status' => 'reset'
            ];
        }

        $msg = 'Incorrect input, try again ' . PHP_EOL . $step['query'];

        return (object)[
            'status' => false,
            'message' => $msg
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    protected function saveDetails()
    {
        $details = $this->handler->getSessionData('data');

        $application = new ClientFilledInvestmentApplication();

        $src = FundSource::where('name', $this->getSourceName())->remember(static::CACHE_FOR)->first();

        $channel = Channel::where('slug', 'ussd')->remember(static::CACHE_FOR)->first();

        $country = Country::where('iso_abbr', 'KE')->remember(static::CACHE_FOR)->first();

        if (!$src) {
            $src = FundSource::where('name', 'Other')->remember(static::CACHE_FOR)->first();
        }

        $names = $this->getNames($details['fullname']);

        $id_or_pass = trim($details['id_or_passport']);

        $existingClient = $this->getExistingClient($id_or_pass);

        $fa = (new USSDRepository())->financialAdvisor($details['referrer_code']);

        $email = null;

        $isEmail = str_contains($details['email'], '@') && str_contains($details['email'], '.');

        if ($isEmail && !$this->handler->validateEmpty($details['email'])) {
            $email = preg_replace('/\s/', '', $details['email']);
        }

        $data = [
            'firstname' => ucfirst(strtolower($names[0])),
            'middlename' => empty($names[1]) ? null : ucfirst(strtolower($names[1])),
            'lastname' => ucfirst(strtolower($names[2])),
            'id_or_passport' => $id_or_pass,
            'telephone_home' => $this->handler->getPhoneNumber(),
            'funds_source_id' => $src->id,
            'funds_source_other' => isset($details['funds_source_other']) ? $details['funds_source_other'] : '',
            'individual' => 1,
            'country_id' => $country ? $country->id : null,
            'nationality_id' => $country ? $country->id : null,
            'channel_id' => $channel ? $channel->id : null,
            'client_id' => $existingClient ? $existingClient->id : null,
            'new_client' => $existingClient ? 2 : 1,
            'unit_fund_id' => $this->handler->getFund()->id,
            'financial_advisor' => $fa ? $fa->id : null,
            'email' => $email,
            'amount' => (int)$details['amount']
        ];

        $application->fill($data);

        $application->save();

        $this->deleteTransactionLog();

        dispatch((new ProcessClientAccountCreation($application))->onConnection('sync'));

        $application = $application->fresh();

        $client = $application->client ? $application->client : $application->application->client;

        if ($details['payment_method'] == 1) {
            $this->handleMpesa($client);
        }

        return true;
    }

    /**
     * @param Client $client
     * @return bool|object
     * @throws Exception
     */
    private function handleMpesa(Client $client)
    {
        $fund = $this->handler->getFund();
        $amount = $this->handler->getSessionData('data.amount');
        $phone = $this->handler->getPhoneNumber();

        (new USSDRepository())->sendStkPush($client, $fund, $amount, $phone);

        return true;
    }

    private function getExistingClient($id_or_pass)
    {
        $user = $this->handler->getUser();

        if (!$user) {
            return null;
        }

        return $user->clients()
            ->where('id_or_passport', $id_or_pass)
            ->where('client_code', 'not like', 'COOP%')
            ->doesntHave('jointDetail')
            ->first();
    }

    private function getNames($names)
    {
        $namesArr = explode(' ', $names);

        $middleName = $namesArr;

        unset($middleName[0], $middleName[count($namesArr) - 1]);

        return [
            Arr::first($namesArr),
            implode(" ", $middleName),
            Arr::last($namesArr)
        ];
    }

    /**
     * @param $step
     * @return bool | object
     */
    protected function saveApplicationAttempt($step)
    {
        $input = $this->latestInput();

        if ($input == '') {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again ' . PHP_EOL . $step['query']
            ];
        }

        if ($input === '00')
        {
            return true;
        }

        if ($step['field'] === 'fullname' && (!$this->validateNames($input))) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, please enter full names ' . PHP_EOL . $step['query']
            ];
        }

        if ($step['field'] === 'email' && !(boolean) filter_var($input, FILTER_VALIDATE_EMAIL))
        {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, please enter valid email address ' . PHP_EOL . $step['query']
            ];
        }

        $log = UssdTransactionLog::where('phone', $this->handler->getPhoneNumber())->first();

        if ($step['field'] == 'fullname') {
            $data = [
                'session_id' => $this->handler->getSessionId(),
                'name' => $input,
                'phone' => $this->handler->getPhoneNumber()
            ];

            $log ? $log->update($data) : UssdTransactionLog::create($data);
        } elseif ($step['field'] == "email") {
            UssdTransactionLog::where('session_id', $this->handler->getSessionId())
                ->update([
                    'email' => $input
                ]);
        }

        return true;
    }

    private function validateNames($input)
    {
        $names = explode(" ", $input);

        $longNames = array_filter($names, function ($name) {
            return strlen($name) > 2;
        });

        return count($longNames) >= 2;
    }

    /**
     * @throws \Exception
     */
    private function deleteTransactionLog()
    {
        UssdTransactionLog::where('session_id', $this->handler->getSessionId())
            ->orWhere('phone', $this->handler->getPhoneNumber())
            ->delete();
    }
}
