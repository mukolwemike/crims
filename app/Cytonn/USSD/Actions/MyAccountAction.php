<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;
use App\Jobs\USSD\SendMessages;
use Cytonn\Presenters\AmountPresenter;
use Exception;

class MyAccountAction extends Action
{
    protected $risk;

    protected $balance;

    protected $currency;

    protected $fund;

    protected $accounts;

    public function steps()
    {
        return [
            [
                'query' => 'Choose option:' . PHP_EOL .
                    '1. Check Balance' . PHP_EOL .
                    '2. Request Bank Details' . PHP_EOL .
                    '3. Update Risk Info' . PHP_EOL .
                    '4. Update Email & Register for web account' . PHP_EOL .
                    '5. Update KYC Details' . PHP_EOL .
                    '6. Download Cytonn App' . PHP_EOL . PHP_EOL .
                    '00:BACK HOME',
                'post' => 'confirmChoiceOption',
                'field' => 'choice_option'
            ],
            [
                'query' => '{message}',
                'pre' => 'getInvestmentTotals',
                'post' => 'confirmBackOption',
                'field' => 'choice_back'
            ],
        ];
    }

    public function nextAction()
    {
        return $this->redirect(RiskInfoAction::class);
    }

    /**
     * @param $step
     * @return array|bool|object
     * @throws Exception
     */
    protected function confirmChoiceOption($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input == 1) {
            return true;
        }

        if ($input == 2) {
            $this->sendBankDetails();

            return (object)[
                'status' => 'end',
                'message' => 'Thank you for investing with us. Bank Details sent via SMS. Dial *809# to continue.'
            ];
        }

        if ($input == 3) {
            return (object)[
                'status' => 'skip',
                'steps' => 3
            ];
        }

        if ($input == 4) {
            return (object)[
                'status' => 'reset',
                'action' => RegisterWebAccount::class
            ];
        }

        if ($input == 5) {
            return (object)[
                'status' => 'reset',
                'action' => UpdateKycDetailsAction::class
            ];
        }

        if ($input == 6) {
            $this->sendDownloadAppDetails();

            return (object)[
                'status' => 'end',
                'message' =>
                    'You will receive an SMS shortly. Click on the link to download the Cytonn Investment Management App.'
            ];
        }

        return (object)[
            'status' => 'reset',
            'message' => 'Invalid input ' . PHP_EOL . $step['query']
        ];
    }

    /**
     * @param $step
     * @return array
     * @throws Exception
     */
    protected function getInvestmentTotals($step)
    {
        $query = $step['query'];

        $clients = $this->handler->getClientsAccounts();
        $user = $this->handler->getUser()->firstname;

        $str = '';
        foreach ($clients as $index => $client) {
            $this->handler->setClientBalance($client);
            $bal = AmountPresenter::currency($this->handler->getBalance(), false, 0);
            $unInvested = AmountPresenter::currency($this->handler->getUnInvestedAmount(), false, 0);
            $currency = $this->handler->getCurrency();

            $key = $index + 1;

            if ($unInvested > 0) {
                $str .= "{$key} : Account {$client->client_code} - {$currency} {$bal} (UnInvested {$currency} {$unInvested})" . PHP_EOL;
            } else {
                $str .= "{$key} : Account {$client->client_code} - {$currency} {$bal}" . PHP_EOL;
            }
        }

        $output = $str . PHP_EOL . '0.BACK 00:BACK HOME';

        $msg = ($this->handler->getUserFund())
            ? $user . ' here is a breakdown of your investments:' . PHP_EOL . PHP_EOL . $output
            : $user . ', you have no Account with CMMF investment. ' .
            'Please invest so as to be able to monitor your investments.' .
            PHP_EOL . PHP_EOL . '00: Back to Main Menu';

        $query = str_replace('{message}', $msg, $query);

        return $this->respond($query, 'con');
    }

    /**
     * @throws Exception
     */
    protected function sendBankDetails()
    {
        $data = [
            'client_code' => $this->handler->getClient()->client_code,
            'account' => $this->handler->getFund()->repo->collectionAccount('bank'),
            'mpesa' => $this->handler->getFund()->repo->collectionAccount('mpesa')
        ];

        dispatch(new SendMessages($this->handler->getPhoneNumber(), 'bank-details', $data));
    }

    /**
     * @throws Exception
     */
    protected function sendDownloadAppDetails()
    {
        dispatch(
            new SendMessages(
                $this->handler->getPhoneNumber(),
                'download-app'
            )
        );
    }

    protected function confirmBackOption()
    {
        $input = $this->latestInput();

        if ($input === '0') {
            return (object)[
                'status' => 'back',
                'steps' => 1
            ];
        }

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        return $this->respond(
            'Thank you for investing with us. Dial *809# to continue.',
            'end'
        );
    }
}
