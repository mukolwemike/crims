<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;

class ResetPINAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Please enter your ID No./Passport No:',
                'post' => 'verifyUser',
                'field' => 'id_or_passport'
            ],
            [
                'query' => 'Enter new PIN (4 digits)',
                'post' => 'changePin',
                'field' => 'pin'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond(
            'Your PIN has been changed successfully. New pin is sent by SMS.',
            'end'
        );
    }

    protected function changePin($step)
    {
        $pin = trim($this->latestInput());

        if (strlen($pin) < 4) {
            return (object)[
                'status' => false,
                'message' => 'Error - Pin must be at least 4 digits long' . PHP_EOL . $step['query']
            ];
        }

        $user = $this->handler->getUser();
        $user->update(['pin' => $pin]);
        $this->sendPinMessage($pin);

        return true;
    }

    protected function sendPinMessage($pin)
    {
        $msg = "Hello, your PIN has been reset. The new PIN is " . $pin
            . '. Dial *809# to continue. Call us on 0709101000';

        \SMS::send($this->handler->getPhoneNumber(), $msg);
    }

    protected function verifyUser()
    {
        $id = $this->latestInput();

        if ($this->handler->verifyClient($id)) {
            return true;
        }

        return (object)[
            'status' => 'end',
            'message' => 'No account available with given ID or Passport. '.
                'Please create account or contact 0709101000 for assistance.'
        ];
    }
}
