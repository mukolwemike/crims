<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 17/06/2019
 * Time: 08:29
 */

namespace App\Cytonn\USSD\Actions;

use App\Console\Commands\SendClientActivationEmails;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\User;
use App\Cytonn\USSD\Action;

class RegisterWebAccount extends Action
{

    public function steps()
    {
        return [
            [
                'pre' => 'checkAccountExists',
                'query' => "Please enter your email address",
                'field' => 'account_email',
                'post' => 'registerAccount'
            ]
        ];
    }

    protected function checkAccountExists($step)
    {
        $user = $this->handler->getUser();

        if ($user->email && $user->active) {
            return $this->respond(
                "You already have an active account. Please reset your password on ".$this->url(),
                "end"
            );
        }

        if ($user->email) {
            $this->sendActivationEmail($user);

            return $this->respond(
                "An activation email will be sent with details on how to activate your account",
                "end"
            );
        }

        return $this->respond(
            $step['query'],
            "con"
        );
    }

    protected function registerAccount($step)
    {
        $user = $this->handler->getUser();
        $email = trim($this->latestInput());

        if ($user->email) {
            return (object)[
                'status' => 'end',
                'message' => "Seems you already have an existing account, reset your password on ".$this->url()
            ];
        }

        $isEmail = str_contains($email, '@') && str_contains($email, '.');

        if (!$isEmail) {
            $msg = 'Invalid email, try again ' . PHP_EOL . $step['query'];

            return (object)[
                'status' => false,
                'message' => $msg
            ];
        }

        $this->doRegistration($user, $email);

        return true;
    }

    private function doRegistration(ClientUser $user, $email)
    {
        $user->update(['email' => $email]);

        $user->clients->each(function (Client $client) use ($email) {
            if (!$client->contact->email) {
                $client->contact->update(['email' => $email]);
            }
        });

        $this->sendActivationEmail($user);
    }

    private function sendActivationEmail(ClientUser $user)
    {
        $job = function () use ($user) {
            (new SendClientActivationEmails())->processUser($user);
        };

        sendToQueue($job);
    }

    private function url()
    {
        return config('app.url');
    }

    public function nextAction()
    {
        return $this->respond(
            "Account created successfully. " . PHP_EOL .
            "You will receive an email with details on how to activate your account.",
            'end'
        );
    }
}
