<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;
use App\Jobs\USSD\RequestCall;
use App\Jobs\USSD\SendMessages;

class WelcomeAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Welcome to Cytonn Money Market Fund!' . PHP_EOL . PHP_EOL .
                    'Choose Options' . PHP_EOL .
                    '1. Create Account' . PHP_EOL .
                    '2. Request Call' . PHP_EOL .
                    '3. More info on MMF',
                'post' => 'confirmOptions',
                'field' => 'welcome_option',
            ]
        ];
    }

    public function nextAction()
    {
        return $this->redirect(ApplyAction::class);
    }

    protected function confirmOptions($step)
    {
        $input = $this->latestInput();

        if ($input == 1) {
            return true;
        }

        if ($input == 2) {
            $this->sendToCRM();

            return (object)[
                'status' => 'end',
                'message' => 'Thank you for your interest in Cytonn Money Market Fund. '
                    . PHP_EOL . 'You will receive a call from one of our Financial Advisers'
            ];
        }

        if ($input == 3) {
            $this->sendMoreInfo();

            return (object)[
                'status' => 'end',
                'message' => 'You will receive an SMS with more information on Cytonn MMF. '
            ];
        }

        $msg = 'Incorrect input, try again ' . PHP_EOL . $step['query'];

        return (object)[
            'status' => false,
            'message' => $msg
        ];
    }

    private function sendToCRM()
    {
        dispatch(new RequestCall($this->handler->getPhoneNumber()));
    }

    private function sendMoreInfo()
    {
        dispatch(new SendMessages($this->handler->getPhoneNumber(), 'mmf-info'));
    }
}
