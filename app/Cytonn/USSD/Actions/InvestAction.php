<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Models\Client;
use App\Cytonn\USSD\Action;
use App\Cytonn\USSD\USSDRepository;
use App\Jobs\USSD\SendMessages;
use Cytonn\Presenters\AmountPresenter;
use Exception;

class InvestAction extends Action
{
    private $isMpesa = false;

    public function steps()
    {
        $fund = $this->handler->getFund();
        $minInvest = AmountPresenter::currency($fund->minimum_investment_amount);
        $minTopup = AmountPresenter::currency($fund->minimum_topup_amount);

        return [
            [
                'query' => 'Please input the amount you would like to invest. ' .
                    "Initial investment minimum: {$fund->currency->code} {$minInvest}." . PHP_EOL .
                    "Subsequent top up minimum: {$fund->currency->code} {$minTopup}." . PHP_EOL . PHP_EOL .
                    '00.BACK HOME',
                'post' => 'confirmPayment',
                'field' => 'amount'
            ],
            [
                'query' => 'By investing,you agree to the terms and conditions of the fund. Please choose the method of payment:' . PHP_EOL
                    . PHP_EOL .
                    '1.  M-PESA.' . PHP_EOL .
                    '2.  Bank Transfer or Deposit.' . PHP_EOL . PHP_EOL .
                    '0:BACK 00.BACK HOME',
                'field' => 'payment_method',
                'post' => 'sendPaymentInfo'
            ]
        ];
    }

    public function nextAction()
    {
        $msg = "Thank you for investing with us. " .
            "You will receive a business confirmation once the funds are received on our end.";

        $msg = $this->isMpesa ?
            "Enter your MPESA PIN in the next prompt. " . PHP_EOL . $msg
            : " We have sent you our bank details via SMS. " . PHP_EOL . $msg;

        return $this->respond($msg, 'end');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function confirmPayment($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($this->validateInvestmentAmount()) {
            return true;
        }

        return (object)[
            'status' => false,
            'message' => 'Invalid amount. ' . $step['query']
        ];
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function validateInvestmentAmount()
    {
        $input = str_replace(',', '', $this->latestInput());
        $fund = $this->handler->getFund();
        $client = $this->handler->getSelectedClient();

        $isFirstTime = $client->unitFundPurchases()->where('unit_fund_id', $fund->id)->count() === 0;

        if ($isFirstTime) {
            return ((float)$input) >= $fund->minimum_investment_amount;
        }

        if ((float)$input >= $fund->minimum_topup_amount) {
            return true;
        }

        return false;
    }

    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function sendPaymentInfo($step)
    {
        $input = $this->latestInput();

        if ($input == '0') {
            return (object)[
                'status' => 'back',
                'steps' => 1
            ];
        }

        if ($input == '00') {
            return (object)[
                'status' => 'reset',
            ];
        }

        if ($input < 0 || $input > 2) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        if ($input == 1) {
            $this->acceptTerms();

            return $this->handleMpesa();
        }

        if ($input == 2) {
            $this->acceptTerms();

            $this->sendBankDetails();

            return true;
        }

        return (object)[
            'status' => false,
            'message' => 'Incorrect input ' . PHP_EOL . $step['query']
        ];
    }

    /**
     * @return bool|object
     * @throws Exception
     */
    private function handleMpesa()
    {
        $client = $this->handler->getSelectedClient();
        $fund = $this->handler->getFund();
        $amount = $this->handler->getSessionData('data.amount');

        $mpesa = (new USSDRepository())->sendStkPush($client, $fund, $amount, $this->handler->getPhoneNumber());

        $this->isMpesa = $mpesa;
        return true;
    }

    /**
     * @throws Exception
     */
    private function sendBankDetails()
    {
        $data = [
            'client_code' => $this->handler->getSelectedClient()->client_code,
            'account' => $this->handler->getFund()->repo->collectionAccount('bank'),
            'mpesa' => $this->handler->getFund()->repo->collectionAccount('mpesa')
        ];

        dispatch(new SendMessages($this->handler->getPhoneNumber(), 'bank-details', $data));
    }

    /**
     * @throws Exception
     */
    private function acceptTerms()
    {
        $client = $this->handler->getSelectedClient();

        if (is_null($client->terms_accepted)) {
            $client->terms_accepted = 1;
            $client->save();
        }
    }
}
