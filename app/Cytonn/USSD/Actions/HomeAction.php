<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;
use Exception;

class HomeAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Select Option' . PHP_EOL . PHP_EOL .
                    '1. Transact.' . PHP_EOL .
                    '2. Refer Client.' . PHP_EOL .
                    '3. My Account.',
                'post' => 'confirmOption',
                'field' => 'home_option'
            ],
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function nextAction()
    {
        $input = $this->handler->getSessionData('data.home_option');

        switch ($input) {
            case '1':
                return $this->redirect(TransactionsAction::class);
                break;
            case '2':
                return $this->redirect(ReferClientAction::class);
                break;
            case '3':
                return $this->redirect(MyAccountAction::class);
                break;
            default:
                return $this->redirect(HomeAction::class);
        }
    }

    protected function confirmOption($step)
    {
        $input = $this->latestInput();

        if ($input <= 0 || $input >= 4) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        return true;
    }
}
