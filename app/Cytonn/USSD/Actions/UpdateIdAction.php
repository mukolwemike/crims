<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Data\Iprs;
use App\Cytonn\Models\Client;
use App\Cytonn\USSD\Action;
use Cytonn\Presenters\ClientPresenter;
use Exception;

class UpdateIdAction extends Action
{
    public function steps()
    {
        return [
            [
                'pre' => 'getUserAccounts',
                'query' => "Select Account to update" . PHP_EOL . PHP_EOL .
                    '{accounts}',
                'field' => 'account_option',
                'post' => 'confirmAccountOption'
            ],
            [
                'query' => "Please enter your ID Number" . PHP_EOL . PHP_EOL .
                    '0:BACK 00:BACK HOME',
                'field' => 'id_or_passport',
                'post' => 'updateUserIdDetails'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond(
            "ID Number updated successfully.",
            'end'
        );
    }

    /**
     * @param $step
     * @return array
     * @throws Exception
     */
    protected function getUserAccounts($step)
    {
        $clients = $this->handler->getClientsAccounts();

        $str = '';
        foreach ($clients as $index => $client) {
            $name = ClientPresenter::presentFullNameNoTitle($client->id);

            $key = $index + 1;
            $str .= "{$key}: {$name} - {$client->client_code}" . PHP_EOL;
        }

        $output = $str . PHP_EOL . 'OO:BACK HOME';

        $q = str_replace('{accounts}', $output, $step['query']);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return array|bool|object
     * @throws Exception
     */
    protected function confirmAccountOption($step)
    {
        $input = $this->latestInput();

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $clients = $this->handler->getClientsAccounts();

        if ($input < 1 && (int)$input > $clients->count()) {
            return (object)[
                'status' => 'reset',
                'message' => 'Invalid input ' . PHP_EOL . $step['query']
            ];
        }

        $this->handler->setSessionData('account_option', $input);

        if ($this->idVerified(($this->handler->getSelectedClient()))) {
            return (object)[
                'status' => 'end',
                'message' => "Your account already has a validated ID Number."
            ];
        }

        return true;
    }

    protected function idVerified(Client $client)
    {
        return (bool)$client->repo->checkValidatedID();
    }

    /**
     * @return bool|object|array
     * @throws Exception
     */
    protected function updateUserIdDetails()
    {
        $input = $this->latestInput();

        if ($input === '0') {
            return (object)[
                'status' => 'back',
                'steps' => 1
            ];
        }

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $id = trim($input);

        $client = $this->handler->getSelectedClient();

        $idVerified = (bool)$this->idVerified($client);

        if ($client->id_or_passport === $id && $idVerified) {
            return (object)[
                'status' => 'end',
                'message' => "Your account already has a validated ID Number."
            ];
        }

        if ($client->id_or_passport === $id && !$idVerified) {
            return (object)[
                'status' => 'end',
                'message' => "Your ID Number is currently awaiting validation"
            ];
        }

        $client->update(['id_or_passport' => $id]);

        (new Iprs())->createRequest($client, $id);

        return true;
    }
}
