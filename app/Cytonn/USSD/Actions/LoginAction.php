<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Models\ClientUser;
use App\Cytonn\USSD\Action;
use Carbon\Carbon;

class LoginAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => '{welcome_message}',
                'pre' => 'checkUserActive',
                'post' => 'confirmLoginDetails',
                'field' => 'pin'
            ],
        ];
    }

    public function nextAction()
    {
        return $this->redirect(HomeAction::class);
    }

    protected function checkUserActive($step)
    {
        $query = $step['query'];

        $user = $this->handler->getUser();

        if (!$user->active) {
            $str = 'Your account is inactive, please contact operations at operations@cytonn.com.';

            $q = str_replace('{welcome_message}', $str, $query);

            return $this->respond($q, 'end');
        } else {
            $str = 'Welcome to Cytonn Money Market Fund. Please Enter your PIN' . PHP_EOL . PHP_EOL .
                '00: Change or Reset PIN' . PHP_EOL;

            $q = str_replace('{welcome_message}', $str, $query);

            return $this->respond($q, 'con');
        }
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function confirmLoginDetails($step)
    {
        $input = $this->latestInput();

        if ($input == 00) {
            return (object)[
                'status' => 'reset',
                'action' => ResetPINAction::class
            ];
        }

        $user = $this->handler->getUser();

        if ($this->tooManyAttempts($user)) {
            return (object)[
                'status' => 'end',
                'message' => 'Too many incorrect PIN attempts. Try again after one hour.'
            ];
        }

        if (!($this->verifyPin(trim($input)))) {
            $this->incrementAttempts($user);

            return (object)[
                'status' => false,
                'message' => 'Incorrect Pin, try again. ' . PHP_EOL . $step['query']
            ];
        }

        $this->resetAttempts($user);

        $this->handler->setSessionData('user_id', $this->handler->getUser()->id);

        return true;
    }

    private function authKey(ClientUser $user)
    {
        return 'ussd_user_auth_attempts_' . $user->id;
    }

    /**
     * @param ClientUser $user
     * @return bool
     * @throws \Exception
     */
    private function tooManyAttempts(ClientUser $user)
    {
        $attempts = (int)cache()->get($this->authKey($user));

        return $attempts >= 3;
    }

    /**
     * @param ClientUser $user
     * @throws \Exception
     */
    private function incrementAttempts(ClientUser $user)
    {
        $key = $this->authKey($user);

        if (!cache()->has($key)) {
            cache([$key => 1], Carbon::now()->addHour());
        }

        $attempts = (int)cache()->get($key);

        cache([$key => $attempts + 1], Carbon::now()->addHour());
    }


    /**
     * @param ClientUser $user
     * @throws \Exception
     */
    private function resetAttempts(ClientUser $user)
    {
        cache([$this->authKey($user) => 0], Carbon::now()->addMinutes(15));
    }

    /**
     * @param $input
     * @return bool
     */
    protected function verifyPin($input)
    {
        $user = $this->handler->getUser();

        return (\Hash::check($input, $user->pin));
    }
}
