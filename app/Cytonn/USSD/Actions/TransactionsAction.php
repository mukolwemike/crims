<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Models\Client;
use App\Cytonn\USSD\Action;
use App\Jobs\USSD\SendMessages;
use Cytonn\Presenters\ClientPresenter;
use Exception;

class TransactionsAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Select Transaction Option' . PHP_EOL . PHP_EOL .
                    '1. Deposit/Invest.' . PHP_EOL .
                    '2. Withdraw.' . PHP_EOL .
                    '3. Transfer.' . PHP_EOL .
                    '4. Bill Payments.' . PHP_EOL . PHP_EOL .
                    '00:BACK HOME',
                'post' => 'confirmOption',
                'field' => 'trans_option'
            ],
            [
                'query' => 'Select account.' . PHP_EOL . PHP_EOL .
                    '{accounts}',
                'pre' => 'getUserAccounts',
                'post' => 'confirmAccountOption',
                'field' => 'account_option'
            ],
            [
                'query' => 'Do you understand and accept the terms and conditions of investing in CMMF?' . PHP_EOL
                    . PHP_EOL .
                    '1. Yes' . PHP_EOL .
                    '2. No',
                'post' => 'confirmTermsOptions',
                'field' => 'terms'
            ]
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function nextAction()
    {
        $input = $this->handler->getSessionData('data.trans_option');

        switch ($input) {
            case '1':
                return $this->redirect(InvestAction::class);
                break;
            case '2':
                return $this->redirect(WithdrawalAction::class);
                break;
            case '3':
                return $this->redirect(TransferAction::class);
                break;
            case '4':
                return $this->redirect(BillingAction::class);
                break;
            default:
                return $this->redirect(TransactionsAction::class);
        }
    }

    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function confirmOption($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input <= 0 || $input >= 5) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        $clients = $this->handler->getClientsAccounts();

        if ($clients->count() == 1) {
            if ($this->termsAccepted($this->handler->getSelectedClient())) {
                if ($input == 2) {
                    if ($this->checkValidate($this->handler->getSelectedClient())) {
                        return (object)[
                            'status' => 'skip',
                            'steps' => 2
                        ];
                    }

                    return (object)[
                        'status' => 'end',
                        'message' => "Selected Client Details have not been validated. Please contact 0709101200 for assistance."
                    ];
                }

                return (object)[
                    'status' => 'skip',
                    'steps' => 2
                ];
            }

            return (object)[
                'status' => 'skip',
                'steps' => 1
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return array
     * @throws Exception
     */
    protected function getUserAccounts($step)
    {
        $query = $step['query'];

        $clients = $this->handler->getClientsAccounts();

        $str = '';
        foreach ($clients as $index => $client) {
            $name = ClientPresenter::presentFullNameNoTitle($client->id);

            $key = $index + 1;
            $str .= "{$key}: {$name} - {$client->client_code}" . PHP_EOL;
        }

        $output = $str . PHP_EOL . '0:BACK ' . 'OO:BACK HOME';

        $q = str_replace('{accounts}', $output, $query);

        return $this->respond($q, 'con');
    }


    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function confirmAccountOption($step)
    {
        $input = $this->latestInput();

        if ($input === '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input === '0') {
            return (object)[
                'status' => 'back'
            ];
        }

        $clients = $this->handler->getClientsAccounts();

        if ($input <= 0 || $input > $clients->count()) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        $this->handler->setSessionData('account_option', $input);

        if ($this->termsAccepted(($this->handler->getSelectedClient()))) {
            if ($this->handler->getSessionData('data')['trans_option'] == 2) {
                if ($this->checkValidate($this->handler->getSelectedClient())) {
                    return (object)[
                        'status' => 'skip',
                        'steps' => 1
                    ];
                }

                return (object)[
                    'status' => 'end',
                    'message' => "Selected Client Details have not been validated. Please contact 0709101200 for assistance."
                ];
            }

            return (object)[
                'status' => 'skip',
                'steps' => 1
            ];
        }

        return true;
    }

    protected function checkValidate(Client $client)
    {
        return (bool)($client->repo->checkClientKycValidated());
    }

    /**
     * @param Client $client
     * @return bool
     * @throws Exception
     */
    protected function termsAccepted(Client $client)
    {
        $app = $client->applications()->where('unit_fund_id', $this->handler->getFund()->id)->first();

        if (is_null($app)) {
            return false;
        }

        return (bool)$app->terms_accepted;
    }

    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function confirmTermsOptions($step)
    {
        $input = $this->latestInput();

        if ($input == 1) {
            if ($this->acceptTerms()) {
                return true;
            }

            return (object)[
                'status' => 'end',
                'message' => "Selected Client has no existing CMMF Application. Contact 0709101200 for assistance."
            ];
        }

        if ($input == 2) {
            $this->resendTerms(
                ['client_id' => $this->handler->getSelectedClient()->id]
            );

            $message = 'Thank you. You will receive a text message with the terms and conditions.' . PHP_EOL .
                'You will need to accept the terms in order to proceed';

            return (object)[
                'status' => 'end',
                'message' => $message
            ];
        }

        $msg = 'Incorrect input, try again ' . PHP_EOL . $step['query'];

        return (object)[
            'status' => false,
            'message' => $msg
        ];
    }

    /**
     * @throws Exception
     */
    protected function acceptTerms()
    {
        $client = $this->handler->getSelectedClient();

        $app = $client->applications()->where('unit_fund_id', $this->handler->getFund()->id)->first();

        if (is_null($app)) {
            return false;
        }

        return (bool)$app->update(['terms_accepted' => true]);
    }

    protected function resendTerms($data)
    {
        dispatch(new SendMessages(
            $this->handler->getPhoneNumber(),
            'terms',
            $data
        ));
    }
}
