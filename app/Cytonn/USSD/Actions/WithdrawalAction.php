<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\System\Locks\Facade\Lock;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use App\Cytonn\USSD\Action;
use App\Events\Investments\Actions\UnitFundInstructionHasBeenMade;
use App\Exceptions\CrimsException;
use App\Jobs\USSD\SendMessages;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Throwable;
use function date;

class WithdrawalAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Please input amount you would like to withdraw:' .
                    '{withdrawable_units}',
                'pre' => 'getWithdrawableUnits',
                'post' => 'confirmInput',
                'field' => 'number'
            ],
            [
                'query' => 'Where would you like us to send the money:' . PHP_EOL . PHP_EOL .
                    '1. M-PESA' . PHP_EOL .
                    '2. BANK Transfer' . PHP_EOL . PHP_EOL .
                    '0.BACK 00.BACK HOME',
                'post' => 'selectTransferType',
                'field' => 'deposit_to'
            ],
            [
                'query' => 'Select the account to send the funds to',
                'field' => 'account',
                'post' => 'confirmAccountBalance',
                'pre' => 'insertAccounts'
            ]
        ];
    }

    /**
     * @return array
     * @throws Throwable
     */
    public function nextAction()
    {
        $this->saveWithdrawalInstruction();

        $data = $this->handler->getSessionData('data');

        dispatch((new SendMessages($this->handler->getPhoneNumber(), 'request-withdrawal', $data)));

        return $this->respond(
            "Withdrawal details submitted successfully. " . PHP_EOL .
            "You will receive a confirmation on the same. Thank you.",
            'end'
        );
    }

    /**
     * @param $step
     * @return array|object
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    protected function getWithdrawableUnits($step)
    {
        $clientFunds = $this->handler->getClientFundDetails();

        if ((int)$clientFunds['ownedUnits'] < 1) {
            return $this->respond(
                "You have zero units to withdraw. Dial *809# to invest.",
                'end'
            );
        }

        $str = PHP_EOL . 'Account Balance: ' . $clientFunds['ownedUnits'] . ' units, ' . PHP_EOL;

        $withdrawable = AmountPresenter::currency($clientFunds['withdrawable_units'], false, 0);

        $str = $str . 'Withdrawable Units: ' . $withdrawable . ' units.';

        $str = $str . PHP_EOL . PHP_EOL . '00:BACK HOME';

        $q = str_replace('{withdrawable_units}', $str, $step['query']);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    protected function confirmInput($step)
    {
        $input = $this->latestInput();

        $clientFunds = $this->handler->getClientFundDetails();

//        $this->handler->setSessionData('data.deposit_to', 2);

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $units = str_replace(',', '', $clientFunds['withdrawable_units']);
        $input = str_replace(',', '', $input);

        if ((float)$input > $units) {
            return (object)[
                'status' => false,
                'message' => 'The units to be withdrawn are more than the withdrawable units, try again. ' . $step['query']
            ];
        }

        return true;
    }

    /**
     * @return bool|object
     * @throws Exception
     */
    protected function confirmAccountBalance($step)
    {
        $input = $this->latestInput();

        $accounts = $this->bankAccounts();

        if ($input <= 0 || $input > $accounts->count()) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        $amount = (float)$this->handler->getSessionData('data.number');

        $client = $this->handler->getSelectedClient();

        $this->handler->setClientBalance($client);

        $investment = $client->unitFundPurchases()->first();

        if (is_null($investment)) {
            return (object)[
                'status' => 'end',
                'message' => 'Please make an Investment before withdrawing or contact 0709101000 for assistance.'
            ];
        }

        $mpesaOption = $this->handler->getSessionData('data.deposit_to');

        if ($mpesaOption == 1 && (int)$amount > 70000) {
            return (object)[
                'status' => 'end',
                'message' => 'The maximum payment amount for MPESA is 70,000. Please try again or contact 0709101000 for assistance.'
            ];
        }

        $bal = $this->handler->getBalance();

        if ($amount > $bal) {
            return (object)[
                'status' => 'end',
                'message' => 'Your Balance is less than amount requested for withdrawal.' . PHP_EOL .
                    'Your balance is ' . AmountPresenter::currency($bal) .
                    PHP_EOL . 'Please contact 0709101000 for assistance.'
            ];
        }

        $lock = Lock::acquireLock('transaction_' . $client->id, 10);

        if (!$lock) {
            return (object)[
                'status' => 'end',
                'message' => 'We are processing one of your transaction, kindly wait a few seconds then try again. ' .
                    'Contact 0709101000 for assistance.'
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @param null $message
     * @return array
     * @throws Exception
     */
    protected function insertAccounts($step, $message = null)
    {
        $accounts = $this->bankAccounts();

        if (!$message) {
            $message = $step['query'];
        }

        if (count($accounts) === 0) {
            return $this->respond('You have no accounts for the selected payment method.', 'end');
        }

        foreach ($accounts as $index => $account) {
            $no = $index + 1;
            $message = $message . PHP_EOL . $no . ': ' . $account->account_number . ' ' . $account->bank->name;
        }

        return $this->respond($message, 'con');
    }


    /**
     * @param null $selection
     * @return ClientBankAccount[]|Builder[]|\Illuminate\Database\Eloquent\Collection|Collection
     * @throws Exception
     */
    protected function bankAccounts($selection = null)
    {
        $data = $this->handler->getSessionData('data.deposit_to');

        $checker = $selection ? $selection : $data;

        $operator = $checker == 1 ? '=' : '!=';

        return ClientBankAccount::active()->where('client_id', $this->handler->getSelectedClient()->id)
            ->whereHas('branch', function ($branch) use ($operator) {
                $branch->whereHas('bank', function ($bank) use ($operator) {
                    $bank->where('swift_code', $operator, 'MPESA');
                });
            })->get();
    }

    protected function confirmAmount($step)
    {
        $input = (float)$this->latestInput();

        $is_thousand = $input >= 1000;

        if ($is_thousand) {
            return true;
        }

        return (object)[
            'status' => $is_thousand,
            'message' => 'Input should be more than 1,000' . PHP_EOL . $step['query']
        ];
    }

    /**
     * @param $step
     * @return bool|object
     * @throws Exception
     */
    protected function selectTransferType($step)
    {
        $input = $this->latestInput();

        $clientFunds = $this->handler->getClientFundDetails();

        if ($input == '0') {
            return (object)[
                'status' => 'back',
            ];
        }

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input <= 0 || $input > 2) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        if ($input == 1 || $input == 2) {
            $accounts = $this->bankAccounts($input);

            if ($accounts->count() == 0) {
                $provider = $input == 1 ? 'Mpesa' : 'Bank';
                return (object)[
                    'status' => false,
                    'message' => "You do not have any $provider accounts. Email operations@cytonn.com to add one, or " .
                        "select another option. " . PHP_EOL . $step['query']
                ];
            }

            $amount = (float)$this->handler->getSessionData('data.number');

            $withdrawal_fee = $clientFunds['withdrawal_fee'];

            $withdrawable = $clientFunds['withdrawable_units'];

            $amount += $withdrawal_fee;

            if ($input == 1 && $amount > $withdrawable) {
                return (object)[
                    'status' => false,
                    'message' => 'The Amount requested is more than the withdrawable amount of ' .
                        AmountPresenter::currency($withdrawable) . ', plus transaction charges of KES ' .
                        AmountPresenter::currency($withdrawal_fee) . '. Select 00 to re-enter amount. '
                        . PHP_EOL . $step['query']
                ];
            }

            return true;
        }

        return (object)[
            'status' => 'reset',
            'message' => 'Invalid input ' . PHP_EOL . $step['query']
        ];
    }

    /**
     * @return void
     * @throws \Throwable
     */
    protected function saveWithdrawalInstruction()
    {
        $client = $this->handler->getSelectedClient();

        $fund = $this->handler->getFund();

        $input['number'] = $this->handler->getSessionData('data.number');

        $input['unit_fund_id'] = $fund->id;

        $input['date'] = date("Y-m-d");

        $acc = $this->bankAccounts()->all();

        $acc_index = $this->handler->getSessionData('data.account');

        $account = isset($acc[$acc_index - 1]) ? $acc[$acc_index - 1] : null;

        $input['account_id'] = $account ? $account->id : null;

        if ($account) {
            $this->checkAccountBelongsToClient($account);
        }

        $input['user_id'] = $this->handler->getUser()->id;
        $input['channel'] = 'ussd';

        \DB::transaction(function () use ($client, $fund, $input) {
            $sale = (new UnitFundInstructionsRepository())->sale($client, $fund, $input);

            event(new UnitFundInstructionHasBeenMade($sale));
        });
    }

    /**
     * @param $account
     * @throws CrimsException
     * @throws Exception
     */
    private function checkAccountBelongsToClient($account)
    {
        $client = $this->handler->getSelectedClient();

        if ($client->id != $account->client_id) {
            throw new CrimsException("Selected account does not belong to client. client_id $client->id " .
                "account_id $account->id");
        }
    }
}
