<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;

class UpdateKycDetailsAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Select Option' . PHP_EOL . PHP_EOL .
                    '1. Update Id Number' . PHP_EOL .
                    '2. Update Tax Pin Number' . PHP_EOL . PHP_EOL .
                    '00:BACK HOME',
                'post' => 'confirmOption',
                'field' => 'update_option'
            ],
        ];
    }

    /**
     * @throws \Exception
     */
    public function nextAction()
    {
        $input = $this->handler->getSessionData('data.update_option');

        switch ($input) {
            case '1':
                return $this->redirect(UpdateIdAction::class);
                break;
            case '2':
                return $this->redirect(UpdatePinAction::class);
                break;
            default:
                return $this->redirect(UpdateKycDetailsAction::class);
        }
    }

    protected function confirmOption($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input <= 0 || $input >= 5) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input ' . PHP_EOL . $step['query']
            ];
        }

        return true;
    }
}
