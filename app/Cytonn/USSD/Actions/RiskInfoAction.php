<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\USSD\Action;
use App\Jobs\USSD\UpdateUserRiskInformation;

class RiskInfoAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Select your Occupation' . PHP_EOL . PHP_EOL .
                    '1. Financial Services' . PHP_EOL .
                    '2. Hospitality Services' . PHP_EOL .
                    '3. Legal/Account' . PHP_EOL .
                    '4. Government/ Public Service' . PHP_EOL .
                    '5. Entrepreneur' . PHP_EOL .
                    '6. Student' . PHP_EOL .
                    '7. Other' . PHP_EOL . PHP_EOL .
                    '00:BACK HOME',
                'post' => 'confirmOccupationOption',
                'field' => 'present_occupation'
            ],
            [
                'query' => 'Please specify the other option', 'field' => 'occupation_other'
            ],
            [
                'query' => 'How long have you been in investments (No. of years)?' . PHP_EOL . PHP_EOL .
                    '1. 0-1 years' . PHP_EOL .
                    '2. 2-3 years' . PHP_EOL .
                    '3. 3-5 years' . PHP_EOL .
                    '4. More than 5 years',
                'post' => 'confirmYearsOption',
                'field' => 'risk_duration'
            ],
            [
                'query' => 'What is the average percent of your income are you willing to invest?' . PHP_EOL . PHP_EOL .
                    '1. 0-10 percent' . PHP_EOL .
                    '2. 10-30 percent' . PHP_EOL .
                    '3. 30-50 percent' . PHP_EOL,
                'post' => 'confirmIncomeOption',
                'field' => 'average_income'
            ],
            [
                'query' => 'Do you have an Emergency fund?' . PHP_EOL . PHP_EOL .
                    '1. No, I Don\'t' . PHP_EOL .
                    '2. Yes, for 6 months' . PHP_EOL .
                    '3. Yes, for more than a year',
                'post' => 'calculateRisk',
                'field' => 'emergency_option'
            ]
        ];
    }

    public function nextAction()
    {
        $msg = 'Thank you for investing with us. Dial *809# to continue.';

        return $this->respond(
            $msg,
            'end'
        );
    }

    protected function confirmOccupationOption($step)
    {
        $input = $this->latestInput();

        if ($input == 00) {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input == 7) {
            return true;
        }

        if ($input < 1 || $input >= 8) {
            return (object)[
                'status' => 'reset',
                'message' => 'Invalid input ' . PHP_EOL . $step['query']
            ];
        }

        return (object)[
            'status' => 'skip',
            'steps' => 1
        ];
    }

    protected function confirmYearsOption($step)
    {
        $input = $this->latestInput();

        if ($input < 1 || $input > 4) {
            return (object)[
                'status' => 'reset',
                'message' => 'Invalid input ' . PHP_EOL . $step['query']
            ];
        }

        return true;
    }

    protected function confirmIncomeOption($step)
    {
        $input = $this->latestInput();

        if ($input < 1 || $input > 3) {
            return (object)[
                'status' => 'reset',
                'message' => 'Invalid input ' . PHP_EOL . $step['query']
            ];
        }

        return true;
    }

    protected function calculateRisk($step)
    {
        $input = $this->latestInput();

        if ($input < 1 || $input > 3) {
            return (object)[
                'status' => 'reset',
                'message' => 'Invalid input ' . PHP_EOL . $step['query']
            ];
        }

        $data = $this->handler->getSessionData()['data'];

        dispatch(new UpdateUserRiskInformation($data, $this->handler->getPhoneNumber()));

        return true;
    }
}
