<?php

namespace App\Cytonn\USSD\Actions;

use App\Cytonn\Billing\BillingRepository;
use App\Cytonn\Models\Billing\ClientUtilityBills;
use App\Cytonn\Models\Billing\UtilityBillingServices;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\USSD\Action;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Support\Arr;

class PayBillAction extends Action
{
    public function steps()
    {
        return [
            [
                'query' => 'Select Bill to pay: ' . PHP_EOL . PHP_EOL .
                    '{billOptions}',
                'pre' => 'getBillOption',
                'post' => 'verifyBillOption',
                'field' => 'bill_id'
            ],
            [
                'query' => 'Enter Account Name: ',
                'post' => 'verifyInput',
                'field' => 'account_name'
            ],
            [
                'query' => 'Enter Account Number: ',
                'post' => 'verifyInput',
                'field' => 'account_number'
            ],
            [
                'query' => 'Enter Amount to pay. {balance}',
                'pre' => 'getBalance',
                'post' => 'verifyAmount',
                'field' => 'amount'
            ],
            [
                'query' => 'Select Account to pay from: ' . PHP_EOL . PHP_EOL .
                    '{sourceFunds}',
                'pre' => 'getSourceFund',
                'post' => 'verifySourceFund',
                'field' => 'source_fund'
            ],
            [
                'query' => 'Save Bill Details to My Bills: ' . PHP_EOL . PHP_EOL .
                    '1: YES 2: NO',
                'post' => 'verifySave',
                'field' => 'save_bill'
            ],
            [
                'query' => '{confirmMessage}',
                'pre' => 'getConfirmMessage',
                'post' => 'confirmDetails',
                'field' => 'confirm_option'
            ]
        ];
    }

    public function nextAction()
    {
        return $this->respond(
            'You will receive confirmation text messages.',
            'end'
        );
    }

    /**
     * @param $step
     * @return array
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    protected function getBalance($step)
    {
        $clientFunds = $this->handler->getClientFundDetails();

        if ((int)$clientFunds['ownedUnits'] < 1) {
            return $this->respond(
                "You have zero units to pay from. Dial *809# to invest.",
                'end'
            );
        }

        $payableAmount = \App\Cytonn\Presenters\General\AmountPresenter::currency($clientFunds['withdrawable_units'], false, 0);

        $str =  'Available Balance: ' . $payableAmount . '.';

        $str = $str . PHP_EOL . PHP_EOL . '00:BACK HOME';

        $q = str_replace('{balance}', $str, $step['query']);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return array
     * @throws \Exception
     */
    protected function getBillOption($step)
    {
        $query = $step['query'];

        $str = '';

        $option = $this->handler->getBillOption();

        $this->handler->setSessionData('bill_option', $option);

        if ($option == 1) {
            $allBills = $this->handler->getAllBillsNames(1);

            foreach ($allBills as $index => $bill) {
                $key = $index + 1;
                $str .= "{$key}: {$bill}" . PHP_EOL;
            }
        } elseif ($option == 2) {
            $clientBills = $this->handler->getClientBillsNames();

            if (count($clientBills) == 0) {
                $str = 'Selected user has no saved bills. ';
            } else {
                foreach ($clientBills as $index => $bill) {
                    $key = $index + 1;
                    $str .= "{$key}: {$bill}" . PHP_EOL;
                }
            }
        }

        $output = $str . PHP_EOL . PHP_EOL . '00:BACK HOME';

        $q = str_replace('{billOptions}', $output, $query);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function verifyBillOption($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        $option = $this->handler->getSessionData('bill_option');

        $count = $option == 1 ? count($this->handler->getAllBillsNames($option)) : count($this->handler->getClientBillsNames());

        if ($input < 1 || $input > $count) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        // client bills option
        if ($option == 2) {
            return (object)[
                'status' => 'skip',
                'steps' => 2
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return bool|object
     */
    protected function verifyInput($step)
    {
        $input = $this->latestInput();

        if ($input == '' || $this->handler->validateEmpty($input)) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function verifyAmount($step)
    {
        $input = $this->latestInput();

        if ($input == '00') {
            return (object)[
                'status' => 'reset'
            ];
        }

        if ($input == '' || $this->handler->validateEmpty($input)) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        $name = $this->handler->getBillName();

        $clientFunds = $this->handler->getClientFundDetails();

        $units = str_replace(',', '', $clientFunds['withdrawable_units']);
        $input = str_replace(',', '', $input);

        if ((float)$input > $units) {
            return (object)[
                'status' => false,
                'message' => 'Amount entered is more than the available balance, try again. ' . $step['query']
            ];
        }
        $option = $this->handler->getSessionData('bill_option');

        $bill = $option === 1 ? $this->handler->getBill($name) : $this->getBillFromClient($name);

        if ((float)$input < $bill->minimum_pay_amount) {
            return (object)[
                'status' => false,
                'message' => 'Minimum bill amount is KES ' . $bill->minimum_pay_amount . ', try again. ' . $step['query']
            ];
        }

        return true;
    }

    private function getBillFromClient($name)
    {
        $client = $this->handler->getSelectedClient();

        $clientBill = ClientUtilityBills::where('client_id', $client->id)
            ->where('account_name', $name)
            ->first();

        return $clientBill->utility;
    }

    /**
     * @param $step
     * @return bool|object
     */
    public function verifySave($step)
    {
        $input = $this->latestInput();

        if ($input == '' || $input < 1 || $input > 2) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return array
     */
    protected function getSourceFund($step)
    {
        $query = $step['query'];

        $sources = $this->handler->getBillsSources();

        $str = '';
        foreach ($sources as $index => $source) {
            $key = $index + 1;
            $str .= "{$key}: {$source}" . PHP_EOL;
        }

        $q = str_replace('{sourceFunds}', $str, $query);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function verifySourceFund($step)
    {
        $input = $this->latestInput();

        if ($input == '' || $input < 1 || $input > count($this->handler->getBillsSources())) {
            return (object)[
                'status' => false,
                'message' => 'Incorrect input, try again. ' . $step['query']
            ];
        }

        $option = $this->handler->getSessionData('bill_option');

        if ($option == 2) {
            return (object)[
                'status' => 'skip',
                'steps' => 1
            ];
        }

        return true;
    }

    /**
     * @param $step
     * @return array
     * @throws \Exception
     */
    protected function getConfirmMessage($step)
    {
        $query = $step['query'];

        $data = $this->handler->getSessionData('data');

        $source = $this->handler->getBillSourceName();

        $str = 'Confirm bill details ' . PHP_EOL . PHP_EOL;

        $option = $this->handler->getSessionData('bill_option');

        if ($option == 1) {
            $str .= 'Bill: ' . $this->handler->getBillName() . PHP_EOL .
                'Account Name: ' . $data['account_name'] . PHP_EOL .
                'Account Number: ' . $data['account_number'] . PHP_EOL;
        } else if ($option == 2) {
            $billName = $this->handler->getBillName();

            $clientBill = ClientUtilityBills::where('account_name', $billName)->remember(static::CACHE_FOR)->first();

            $str .= 'Bill: ' . $clientBill->utility->name . PHP_EOL .
                'Account Name: ' . $clientBill->account_name . PHP_EOL .
                'Account Number: ' . $clientBill->account_number . PHP_EOL;
        }

        $output = $str . 'Account Source: ' . $source . PHP_EOL .
            'Bill Amount: ' . AmountPresenter::currency($data['amount']) . PHP_EOL . PHP_EOL .
            '1: CONFIRM 2:CHANGE DETAILS';

        $q = str_replace('{confirmMessage}', $output, $query);

        return $this->respond($q, 'con');
    }

    /**
     * @param $step
     * @return bool|object
     * @throws \Exception
     */
    protected function confirmDetails($step)
    {
        $input = $this->latestInput();

        if ($input == 1) {
            $this->saveDetails();

            return true;
        }

        if ($input == 2) {
            return (object)[
                'status' => 'reset'
            ];
        }

        return (object)[
            'status' => false,
            'message' => 'Incorrect input, try again. ' . PHP_EOL . $step['query']
        ];
    }

    /**
     * @throws \Exception
     */
    protected function saveDetails()
    {
        $input = $this->handler->getSessionData('data');

        $client = $this->handler->getSelectedClient();

        $fund = UnitFund::where('name', $this->handler->getBillSourceName())->remember(static::CACHE_FOR)->first();

        $input['unit_fund_id'] = $fund ? $fund->id : null;

        $input['client_id'] = $client->id;

        $input['user_id'] = $this->handler->getUser() ? $this->handler->getUser()->id : null;

        $option = $this->handler->getSessionData('bill_option');

        if ($option == 1) {
            $utility = UtilityBillingServices::where('name', $this->handler->getBillName())->remember(static::CACHE_FOR)->first();
        } else {
            $clientBill = ClientUtilityBills::where('account_name', $this->handler->getBillName())->remember(static::CACHE_FOR)->first();

            $utility = $clientBill->utility;

            $input['account_name'] = $clientBill->account_name;

            $input['account_number'] = $clientBill->account_number;
        }

        $input['checked'] = (isset($input['save_bill']) && $input['save_bill'] == 1) ? true : false;

        $phone = Arr::first($client->getContactPhoneNumbersArray());

        $input['msisdn'] = $phone ? $phone : $this->handler->getPhoneNumber();

        $input['date'] = Carbon::now()->toDateString();

        $input['channel'] = 'ussd';

        (new BillingRepository())->saveBillingInstruction($input, $client, $utility);

//        $this->sendRequestNotification($client, $utility, $input, 'bill');
    }
}
