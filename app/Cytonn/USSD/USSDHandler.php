<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-27
 * Time: 09:15
 */

namespace App\Cytonn\USSD;

use App\Cytonn\Models\Billing\ClientUtilityBills;
use App\Cytonn\Models\Billing\UtilityBillingServices;
use App\Cytonn\Models\Billing\UtilityBillingType;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Unitization\UnitFundClientSummary;
use App\Cytonn\USSD\Actions\ApplyAction;
use App\Cytonn\USSD\Actions\BillingAction;
use App\Cytonn\USSD\Actions\BuyAirtimeAction;
use App\Cytonn\USSD\Actions\HomeAction;
use App\Cytonn\USSD\Actions\InvestAction;
use App\Cytonn\USSD\Actions\LoginAction;
use App\Cytonn\USSD\Actions\MyAccountAction;
use App\Cytonn\USSD\Actions\PayBillAction;
use App\Cytonn\USSD\Actions\ReferClientAction;
use App\Cytonn\USSD\Actions\ResetPINAction;
use App\Cytonn\USSD\Actions\TransactionsAction;
use App\Cytonn\USSD\Actions\WelcomeAction;
use App\Cytonn\USSD\Actions\WithdrawalAction;
use Carbon\Carbon;
use Cytonn\Api\Transformers\Unitization\UnitFundClientSummaryTransfomer;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Arr;

class USSDHandler
{
    const CACHE_FOR = 240;

    private $sessionId;
    private $serviceCode;
    private $phoneNumber;
    private $text = '';

    private $currency;
    private $balance;
    private $unInvestedAmount;
    private $userFund;
    private $billOption;

    protected $actions = [
        ApplyAction::class,
        LoginAction::class,
        TransactionsAction::class,
        WelcomeAction::class,
        WithdrawalAction::class,
        MyAccountAction::class,
        ReferClientAction::class,
        ResetPINAction::class,
        InvestAction::class,
        HomeAction::class,
        BillingAction::class,
        BuyAirtimeAction::class,
        PayBillAction::class
    ];

    /**
     * USSDHandler constructor.
     * @param $sessionId
     * @param $serviceCode
     * @param $phoneNumber
     * @param $text
     * @throws Exception
     */
    public function __construct($sessionId, $serviceCode, $phoneNumber, $text)
    {
        $this->sessionId = $sessionId;
        $this->serviceCode = $serviceCode;
        $this->phoneNumber = $phoneNumber;
        $this->text = $text;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function handle()
    {
        if ($this->text == '' && $this->allowNullInput()) {
            return $this->handleFirst();
        }

        return $this->handleCont();
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function allowNullInput()
    {
        $session = $this->getSessionData();

        // will allow if its the first step
        return is_null($session['step']);
    }

    /**
     * @return array
     * @throws Exception
     */
    private function handleFirst()
    {
        if ($this->checkUserExists()) {
            return $this->render(new LoginAction($this));
        }

        return $this->render(new WelcomeAction($this));
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function handleCont()
    {
        $session = $this->getSessionData();

        $action = $session['action'] ? $session['action'] : null;

        if (!$action) {
            return $this->resetMenu();
        }

        return $this->render(new $action($this));
    }

    /**
     * @param Action $action
     * @return array
     * @throws Exception
     */
    private function render(Action $action)
    {
        return $action->handle();
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function checkUserExists()
    {
        $userExists = $this->getUserQuery()
            ->remember(1)
            ->exists();

        if (!$userExists) {
            return false;
        }

        $client = $this->getSelectedClient();

        return (bool)$client;
    }

    private function getUserQuery()
    {
        list($phone_alt, $phone, $phone_country_code) = $this->cleanUpForm();

        return ClientUser::where(function ($user) {
            $user->where('phone_country_code', '+254')
                ->orWhere('phone_country_code', '254');
        })->where(function ($user) use ($phone, $phone_alt, $phone_country_code) {
            $user->where('phone', $phone)
                ->orWhere('phone', $phone_alt)
                ->orWhere('phone', $phone_country_code);
        })->has('clients');
    }

    public function verifyClient($id_or_passport)
    {
        $user = $this->getUser();

        if (!$user) {
            return false;
        }

        return $user->clients()->where('id_or_passport', trim($id_or_passport))->exists();
    }

    /**
     * @return ClientUser
     */
    public function getUser()
    {
        $users = $this->getUserQuery()->latest()->get();

        if (count($users) <= 1) {
            return $users->first();
        }

        foreach ($users as $user) {
            if ($user->ussd_account == 1) {
                return $user;
            }
        }

        return $users->first();
    }

    /**
     * @return Client
     * @throws Exception
     */
    public function getClient()
    {
        return $this->getSelectedClient();
    }

    /**
     * @return UnitFund
     * @throws Exception
     */
    public function getFund()
    {
        return UnitFund::where('short_name', 'CMMF')
            ->remember(static::CACHE_FOR)
            ->first();
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function getText()
    {
        return $this->text;
    }

    private function buildSessionData()
    {
        return [
            'reset_menu' => '',
            'action' => '',
            'data' => [],
            'step' => null,
            'user_id' => null
        ];
    }

    /**
     * @param null $data
     * @throws Exception
     */
    public function createCache($data = null)
    {
        $data = is_null($data) ? $this->buildSessionData() : $data;

        cache()->put('sessionId_' . $this->sessionId, $data, Carbon::now()->addMinutes(15));
    }

    /**
     * @param null $action
     * @return array
     * @throws Exception
     */
    public function resetMenu($action = null)
    {
        $this->setSessionData('reset_menu', $this->text);
        $user_id = $this->getSessionData('user_id');

        if (!$action) {
            return $user_id ?
                $this->render(new HomeAction($this))
                : $this->handleFirst();
        }

        return $this->render(new $action($this));
    }

    /**
     * @param $key
     * @param $value
     * @throws Exception
     */
    public function setSessionData($key, $value)
    {
        $array = $this->getSessionData();

        Arr::set($array, $key, $value);

        $this->createCache($array);
    }

    /**
     * @param null $key
     * @return mixed
     * @throws Exception
     */
    public function getSessionData($key = null)
    {
        $data = cache()->get('sessionId_' . $this->sessionId);

        if (!$data) {
            $data = $this->buildSessionData();
        }

        return Arr::get($data, $key);
    }

    /**
     * @throws Exception
     */
    public function forgetSession()
    {
        cache()->forget('sessionId_' . $this->getSessionId());
    }

    private function getClientBuilder($phone, $phone_alt, $phone_country_code)
    {
        return Client::where(function ($user) use ($phone, $phone_alt, $phone_country_code) {
            $user->where('phone', $phone)->orWhere('phone', $phone_alt)->orWhere('phone', $phone_country_code);
        })->orWhere(function ($user) use ($phone, $phone_alt, $phone_country_code) {
            $user->where('telephone_home', $phone)
                ->orWhere('telephone_home', $phone_alt)
                ->orWhere('telephone_home', $phone_country_code);
        })->orWhere(function ($user) use ($phone, $phone_alt, $phone_country_code) {
            $user->where('telephone_office', $phone)
                ->orWhere('telephone_office', $phone_alt)
                ->orWhere('telephone_office', $phone_country_code);
        })->latest();
    }

    /**
     * Check if application exists for phone
     * @return bool
     * @throws Exception
     */
    public function checkUserApplication()
    {
        list($phone, $phone_alt, $phone_country_code, $phone_plus) = $this->cleanUpForm();

        return ClientFilledInvestmentApplication::where('unit_fund_id', $this->getFund()->id)
            ->where(function ($inv) use ($phone, $phone_alt, $phone_country_code, $phone_plus) {
                $inv->where('telephone_home', $phone)
                    ->orWhere('telephone_home', $phone_alt)
                    ->orWhere('telephone_home', $phone_plus)
                    ->orWhere('telephone_home', $phone_country_code);
            })->orWhere(function ($inv) use ($phone, $phone_alt, $phone_country_code, $phone_plus) {
                $inv->where('telephone_office', $phone)
                    ->orWhere('telephone_office', $phone_alt)
                    ->orWhere('telephone_office', $phone_plus)
                    ->orWhere('telephone_office', $phone_country_code);
            })->exists();
    }

    /**
     * @return array
     */
    protected function cleanUpForm(): array
    {
        $phone = ltrim($this->getPhoneNumber(), '+');
        $phone = ltrim($phone, '254');
        $phone = ltrim($phone, '0');

        $phone_alt = '0' . $phone;

        $phone_country_code = '254' . $phone;

        $phone_plus = '+254' . $phone;

        return array($phone, $phone_alt, $phone_country_code, $phone_plus);
    }

    /**
     * @return Builder[]|Collection|BelongsToMany[]
     * @throws Exception
     */
    public function getClientsAccounts()
    {
        $user = $this->getUser();

        $fund = $this->getFund();

        $clients = $user->clients()->whereHas('unitFundPurchases', function ($purchase) use ($fund) {
            $purchase->where('unit_fund_id', $fund->id);
        })->get();

        if ($clients->count()) {
            return $clients;
        }

        $clients = $user->clients()->whereHas('applications', function ($appl) use ($fund) {
            $appl->where('unit_fund_id', $fund->id);
        })->get();

        if ($clients->count()) {
            return $clients;
        }

        return $user->clients()->get();
    }

    /**
     * @return Client
     * @throws Exception
     */
    public function getSelectedClient()
    {
        $acc_opt = (is_null($this->getSessionData('account_option')))
            ? $this->getSessionData('data.account_option')
            : $this->getSessionData('account_option');

        $clients = $this->getClientsAccounts();

        $index = $acc_opt - 1;

        if (!$acc_opt) {
            return $clients->first();
        }

        return $clients[$index];
    }

    public function checkIfCMMFClient(Client $client)
    {
        return $client->unitFundPurchases()
            ->whereHas('unitFund', function ($fund) {
                $fund->where('short_name', 'CMMF')->ofType('cis')->active();
            })->exists();
    }

    public function setClientBalance(Client $client)
    {
        $unitFunds = UnitFund::whereHas('purchases', function ($purchases) use ($client) {
            $purchases->where('client_id', $client->id);
        })
            ->where('active', 1)
            ->get()
            ->map(function ($fund) use ($client) {
                return (new UnitFundClientSummary($fund, $client))->index();
            });

        foreach ($unitFunds as $fund) {
            $this->userFund = $fund;

            $this->currency = $fund['details']['currency_code'];

            $this->balance = $fund['details']['calculate']->total;

            $this->unInvestedAmount = $fund['details']['un_invested_amount'];
        }
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function getUnInvestedAmount()
    {
        return $this->unInvestedAmount;
    }

    public function getUserFund()
    {
        return $this->userFund;
    }

    /**
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws Exception
     */
    public function getClientFundDetails()
    {
        return (new UnitFundClientSummaryTransfomer())->transform($this->getSelectedClient(), $this->getFund());
    }

    public function validateEmpty($code)
    {
        return $code == 'none' || $code == 'None' || $code == 'no one' || strtolower($code) == 'n/a' ||
            $code == '0' || $code == '1' || $code == '00' || strtolower($code) == 'o' || strtolower($code) == 'oo' ||
            is_null($code) || empty($code);
    }

    public function setBillOption($option)
    {
        $this->billOption = $option;
    }

    public function getBillOption()
    {
        return $this->billOption;
    }

    public function getAllBills()
    {
        return UtilityBillingType::where('slug', '=', 'bill')
            ->get()
            ->map(function ($bill) {
                return [
                    'id' => $bill->id,
                    'name' => $bill->name,
                    'slug' => $bill->slug,
                ];
            });
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getClientBills()
    {
        $client = $this->getSelectedClient();

        return ClientUtilityBills::where('client_id', $client->id)
            ->get()
            ->map(function ($bill) {
                return [
                    'id' => $bill->id,
                    'bill_name' => $bill->utility->name,
                    'slug' => $bill->utility->slug,
                    'account_name' => $bill->account_name,
                    'account_number' => $bill->account_number,
                ];
            });
    }

    /**
     * @return mixed|string
     * @throws \Exception
     */
    public function getBillSourceName()
    {
        $source_id = $this->getSessionData('data.source_fund');

        $index = (int)$source_id - 1;

        $sources = $this->getBillsSources();

        return isset($sources[$index]) ? $sources[$index] : 'Not Found';
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getBillName()
    {
        $bill_id = $this->getSessionData('data.bill_id');

        $index = (int)$bill_id - 1;

        $option = $this->getSessionData('bill_option');

        $bills = $option == 1 ? $this->getAllBillsNames($option) : $this->getClientBillsNames();

        return isset($bills[$index]) ? $bills[$index] : 'Not Found';
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function getAirtimeName()
    {
        $bill_id = $this->getSessionData('data.bill_id');

        $index = (int)$bill_id - 1;

        $bills = $this->getAllBillsNames(2);

        return isset($bills[$index]) ? $bills[$index] : 'Not Found';
    }


    /**
     * @return mixed
     */
    public function getAllBillsNames($id)
    {
        $type_id = (int)$id;

        return UtilityBillingServices::active()
            ->where('type_id', $type_id)
            ->orderBy('name')
            ->remember(static::CACHE_FOR)
            ->pluck('name')
            ->values()->all();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getClientBillsNames()
    {
        $client = $this->getSelectedClient();

        return ClientUtilityBills::where('client_id', $client->id)
            ->orderBy('account_name')
            ->remember(static::CACHE_FOR)
            ->pluck('account_name')
            ->values()->all();
    }

    /**
     * @return mixed
     */
    public function getBillsSources()
    {
        return UnitFund::where('short_name', 'CMMF')
            ->remember(static::CACHE_FOR)
            ->pluck('name')
            ->values()->all();
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getBill(string $name)
    {
        return UtilityBillingServices::active()
            ->where('name', $name)
            ->remember(static::CACHE_FOR)
            ->first();
    }
}
