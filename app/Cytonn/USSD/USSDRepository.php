<?php

namespace App\Cytonn\USSD;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBank;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Jobs\USSD\CreateStkPush;
use App\Jobs\USSD\SendMessages;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStep;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Users\UserClientRepository;
use Exception;
use Illuminate\Support\Arr;

class USSDRepository
{
    public function requestCallFromCRM($phone, $name = null, $source = null, $email = null)
    {
        $postUrl = getenv('LEADS_STORE');

        $name = $name ? $name : $phone;

        if ($source) {
            $description = "I'm interested in one of your products. Please call me. Referred From " . $source;
            $sourceId = ($source == 'USSD') ? env('USSD_SOURCE_ID') : env('WEB_SOURCE_ID');
        } else {
            $description = "I'm interested in one of your products. Please call me.";
            $sourceId = env('USSD_SOURCE_ID');
        }


        $parameters = [
            'name' => urlencode($name),
            'email' => urlencode($email),
            'phone' => urlencode($phone),
            'subject' => urlencode('bd'),
            'source_id' => urlencode($sourceId),
            'description' => urlencode($description),
            'contact' => urlencode(''),
            'token' => urlencode(env('CRM_TOKEN')),
            'sitename' => urlencode(env('CRM_SITENAME')),
            'country' => urlencode("+254")
        ];

        $parameterString = "";

        foreach ($parameters as $key => $value) {
            $parameterString .= $key . '=' . $value . '&';
        }

        rtrim($parameterString, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_POST, count($parameters));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameterString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute post

        try {
            curl_exec($ch);
        } catch (Exception $e) {
            throw new Exception($e);
        }

        $response = curl_getinfo($ch);

        //Close the connection
        curl_close($ch);

        return $response;
    }

    /**
     * @param ClientFilledInvestmentApplication $app
     * @return mixed
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     * @throws \Throwable
     */
    public function processApplication(ClientFilledInvestmentApplication $app)
    {
        $processed = $app->clientRepo->process();

        $approval = $processed->approval;

        $this->updateApplicationApproval($approval);

        return $processed;
    }

    private function updateApplicationApproval(ClientTransactionApproval $approval)
    {
        $approval->update([
            'sent_by' => 1,
            'approved' => 1,
            'approved_by' => 1,
            'approved_on' => Carbon::now(),
            'awaiting_stage_id' => 6
        ]);

        foreach ($approval->type()->allStages() as $stage) {
            ClientTransactionApprovalStep::create([
                'approval_id' => $approval->id,
                'stage_id' => $stage->id,
                'user_id' => 1
            ]);
        }

        return $approval;
    }

    public function approveApplication(ClientInvestmentApplication $application)
    {
        $input = $application->toArray();

        $type = $application->clientType->name;

        $existingClient = $this->checkExistingClient($input);

        if ($existingClient->exists() && $application->form->channel) {
            $client = $existingClient->first();

            $form = $application->form;

            $form->new_client = 2;

            $form->client_id = $client->id;

            $form->save();

            $application->new_client = 2;
        } else {
            $contact = $application->repo->saveContactFromApplicationForm($type, $input);

            $client = $application->repo->saveClientFromApplicationForm($type, $contact, $input);
        }

        $application->client_id = $client->id;

        $application->unit_fund_id = $this->getFund()->id;

        $application->save();

        if ($application->form->jointHolders && $application->clientType->name !== 'corporate') {
            $jointHolders = $application->form->jointHolders;

            $application->repo->addJointHolders($jointHolders);
        }

        return $application;
    }

    private function checkExistingClient($input)
    {
        list($phone, $phone_alt, $phone_country_code, $phone_country_code_plus) = $this->cleanUpForm($input['telephone_home']);

        $query = Client::where('id_or_passport', $input['id_or_passport'])
            ->where(function ($user) use ($phone, $phone_alt, $phone_country_code, $phone_country_code_plus) {
                $user->where('phone', $phone)
                    ->orWhere('phone', $phone_alt)
                    ->orWhere('phone', $phone_country_code)
                    ->orWhere('phone', $phone_country_code_plus);
            })->orWhere(function ($user) use ($phone, $phone_alt, $phone_country_code, $phone_country_code_plus) {
                $user->where('telephone_home', $phone)
                    ->orWhere('telephone_home', $phone_alt)
                    ->orWhere('telephone_home', $phone_country_code)
                    ->orWhere('telephone_home', $phone_country_code_plus);
            })->orWhere(function ($user) use ($phone, $phone_alt, $phone_country_code, $phone_country_code_plus) {
                $user->where('telephone_office', $phone)
                    ->orWhere('telephone_office', $phone_alt)
                    ->orWhere('telephone_office', $phone_country_code)
                    ->orWhere('telephone_office', $phone_country_code_plus);
            });

        if (isset($data['email'])) {
            $query->whereHas('contact',
                function ($query) use ($phone, $phone_alt, $phone_country_code, $phone_country_code_plus, $input) {
                    $query->where('email', $input['email'])
                        ->where(function ($query) use (
                            $phone,
                            $phone_alt,
                            $phone_country_code,
                            $phone_country_code_plus
                        ) {
                            $query->where('phone', $phone)
                                ->orWhere('phone', $phone_alt)
                                ->orWhere('phone', $phone_country_code_plus)
                                ->orWhere('phone', $phone_country_code);
                        });
                });
        }

        return $query->latest();
    }

    /**
     * @return array
     */
    protected function cleanUpForm($telephone): array
    {
        $phone = ltrim($telephone, '+');
        $phone = ltrim($phone, '254');
        $phone = ltrim($phone, '0');

        $phone_alt = '0' . $phone;

        $phone_country_code = '254' . $phone;

        $phone_country_code_plus = '+254' . $phone;

        return array($phone, $phone_alt, $phone_country_code, $phone_country_code_plus);
    }

    private function getFund()
    {
        return UnitFund::where('short_name', 'CMMF')->first();
    }

    /**
     * @param ClientInvestmentApplication $app
     * @param Client $client
     * @return ClientUser|\Illuminate\Database\Eloquent\Model|void|null
     */
    public function createClientUser(ClientInvestmentApplication $app, Client $client)
    {
        $client = $client->fresh();

        (new UserClientRepository())->provisionForClient($client, $app->telephone_home);
    }

    public function financialAdvisor($code = null)
    {
        if (is_null($code) || empty($code)) {
            return null;
        }

        $fa = CommissionRecepient::where('referral_code', $code)->first();

        if ($fa) {
            return $fa;
        }

        return CommissionRecepient::where('email', $code)->first();
    }


    public function createMpesaAccount(Client $client)
    {
        if ($client->jointDetail()->count() >= 1 || $client->clientType->name == 'corporate') {
            return;
        }

        if (ClientBankAccount::where('client_id', $client->id)
            ->whereHas('branch', function ($branch) {
                $branch->whereHas('bank', function ($bank) {
                    $bank->where('swift_code', 'MPESA');
                });
            })->exists()
        ) {
            return;
        }

        $phone = $this->getPhoneNumber($client);

        if (!$phone) {
            return;
        }

        $bank = ClientBank::where('swift_code', 'MPESA')->remember(1000)->first();
        $branch = ClientBankBranch::where('bank_id', $bank->id)->remember(1000)->first();

        ClientBankAccount::create([
            'client_id' => $client->id,
            'branch_id' => $branch->id,
            'account_name' => ClientPresenter::presentFullNameNoTitle($client->id),
            'account_number' => str_replace(" ", "", trim(ltrim($phone, '+'))),
            'active' => true
        ]);
    }

    private function getPhoneNumber(Client $client)
    {
        $phone = $client->contact->phone ? $client->contact->phone : $client->phone;

        if (!$phone) {
            $phone = $client->telephone_home ? $client->telephone_home : $client->telephone_office;
        }

        $phone = trimPhoneNumber($phone);

        if (!$phone) {
            $phone = Arr::first($client->getContactPhoneNumbersArray());
        }

        if (!$phone) {
            return;
        }

        if (starts_with($phone, '0')) {
            $phone = '254' . ltrim($phone, '0');
        }

        if (!starts_with($phone, '254')) {
            return;
        }

        return $phone;
    }

    public function sendValidateEmailRequestOps(Client $client)
    {
        Mail::compose()
            ->to('operations@cytonn.com')
            ->subject('Validate Client KYC Compliance Reminder - ' . $client->client_code)
            ->view('emails.client.validation.ops_validate_kyc', ['client' => $client])
            ->queue();
    }

    private function sendAccountCreationSMS(Client $client, $pin)
    {
        $link = "https://cytonn.com/cmmf-terms";

        $message = 'Hello ' . ClientPresenter::presentFirstName($client->id) .
            ', Thank you for activating Cytonn Unit Trust Funds account.' .
            ' You can dial *809# to continue. Your PIN is ' . $pin .
            '. Please read the terms and conditions provided here ' . $link .
            '. For queries, call us on 0709101200.';

        \SMS::send([$client->telephone_home], $message);
    }

    public function getPayBill()
    {
        $fund = UnitFund::where('short_name', 'CMMF')->first();

        $acc = $fund->repo->collectionAccount('mpesa');

        return $acc ? $acc->account_no : null;
    }

    /**
     * @param Client $client
     * @param UnitFund $fund
     * @param $phone
     * @param $amount
     * @return bool
     */
    public function sendStkPush(Client $client, UnitFund $fund, $amount, $phone = null)
    {
        $mpesa = $fund->repo->collectionAccount('mpesa');

        if (!$phone) {
            $phone = Arr::first($client->getContactPhoneNumbersArray());
        }

        if (!$mpesa) {
            $account = $fund->repo->collectionAccount('mpesa');

            dispatch(
                new SendMessages($phone, 'bank-details', ['mpesa' => null, 'account' => $account])
            );

            return false;
        }

        dispatch(new CreateStkPush($mpesa->account_no, $phone, $amount, $client->client_code));

        return true;
    }
}
