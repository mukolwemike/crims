<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Shares\Trading;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesEntity;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Custodial\Withdraw;
use Cytonn\Exceptions\ClientInvestmentException;

class Redeem
{
    /**
     * @var ShareHolder
     */
    protected $holder;

    /**
     * Redeem constructor.
     * @param ShareHolder $holder
     */
    public function __construct(ShareHolder $holder)
    {
        $this->holder = $holder;
    }

    /**
     * Redeem the shares linked to the shareholder
     *
     * @param SharesEntity $entity
     * @param ClientTransactionApproval $approval
     */
    public function redeemHolderShares(ClientTransactionApproval $approval)
    {
        $date = Carbon::parse($approval->payload['date']);

        $shares = $this->holder->repo->getSharesToBeRedeemed($date);

        $shares->each(function ($share) use ($approval, $date) {
            $this->redeemSharePurchase($share, $approval, $date);
        });
    }

    /**
     * @param Carbon $date
     */
    protected function massRedeemShares(Carbon $date)
    {
        $shares = $this->holder->repo->getSharesToBeRedeemed($date->copy());

        return $shares->map(function ($share) use ($date) {
            return $this->redeemSharePurchase($share, null, $date->copy());
        });
    }

    /*
     * Redeem share purchase
     */
    /**
     * @param SharePurchases $purchase
     * @param ClientTransactionApproval $approval
     * @param Carbon $date
     * @return mixed
     */
    public function redeemSharePurchase(
        SharePurchases $purchase,
        ClientTransactionApproval $approval = null,
        Carbon $date
    ) {
        if ($purchase->shareSale) {
            throw new ClientInvestmentException('The shares have already been redeemed');
        }

        if ($purchase->number > $this->holder->currentShares()) {
            throw new ClientInvestmentException(
                'The number of shares to be redeemed cannot be more than those owned.'
            );
        }

        return $this->redeem($purchase, $approval, $date);
    }

    /**
     * @param SharePurchases $purchase
     * @param ClientTransactionApproval $approval
     * @param Carbon $date
     */
    private function redeem(SharePurchases $purchase, ClientTransactionApproval $approval = null, Carbon $date)
    {
        $amount = $purchase->repo->getNetPurchaseValue($date);

        $amount = round($amount, 2);

        $interest = $amount - ($purchase->number * $purchase->purchase_price);

        $entity = $purchase->entity;
        $client = $this->holder->client;
        $recipient = is_null($this->holder->client) ? null : $this->holder->client->getLatestFA();

        $description = 'Redeemed ' . $purchase->number . ' shares at ' . $purchase->purchase_price . ' each.';

        $payment = Payment::make(
            $client,
            $recipient,
            null,
            null,
            null,
            $entity,
            $date,
            $amount,
            'I',
            $description
        )->debit();

        return $this->createShareSale([
            'number' => $purchase->number,
            'sale_price' => $purchase->purchase_price,
            'date' => $date,
            'payment_id' => $payment->id,
            'shares_purchase_order_id' => null,
            'shares_sales_order_id' => null,
            'share_holder_id' => $this->holder->id,
            'entity_id' => $purchase->entity_id,
            'category_id' => 1,
            'type' => 2,
            'approval_id' => $approval ? $approval->id : null,
            'buyer_id' => null,
            'share_purchase_id' => $purchase->id,
            'redeem_interest' => $interest
        ]);
    }

    /**
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function redeemMembershipFee(ClientTransactionApproval $approval)
    {
        $date = Carbon::parse($approval->payload['date']);

        $this->effectMembershipRedemption($date);
    }

    /**
     * @param Carbon $date
     * @throws ClientInvestmentException
     * @return ClientPayment
     */
    protected function effectMembershipRedemption(Carbon $date)
    {
        $client = $this->holder->client;

        if ($client->clientPayments()
            ->ofType('MR')->exists()) {
            return;
            throw new ClientInvestmentException('The client has already redeemed their membership fees');
        }

        $amount = $client->clientPayments()
            ->ofType('M')
            ->sum('amount');

        $amount = -$amount;

        if ($amount > 0) {
            $entity = $this->holder->entity;

            $description = 'Redeemed membership fees';

            return Payment::make(
                $client,
                null,
                null,
                null,
                null,
                $entity,
                $date,
                $amount,
                'MR',
                $description
            )->debit();
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createShareSale(array $data)
    {
        return ShareSale::create($data);
    }

    /**
     * @param Carbon $date
     * @return array
     * @throws ClientInvestmentException
     */
    public function massRedeem(Carbon $date)
    {
        $payment = $this->effectMembershipRedemption($date->copy());

        $redeemed = $this->massRedeemShares($date->copy());

        $this->paymentOut($payment);

        $redeemed->each(function ($r) {
            $this->paymentOut($r->payment);
        });

        return [
            'payment' => $payment,
            'shares' => $redeemed
        ];
    }

    protected function paymentOut(ClientPayment $payment = null)
    {
        if (!$payment) {
            return;
        }

        if ($payment->amount < 0) {
            throw new ClientInvestmentException("Cannot withdraw payments");
        }

        $entity = SharesEntity::find(1);

        $w = Withdraw::make(
            Carbon::today(),
            $entity->custodialAccount,
            "Interest payment",
            $payment->client,
            $payment->amount,
            null,
            null,
            $entity,
            null,
            $payment,
            null,
            null,
            null,
            new ClientScheduledTransaction() //allow negative balances
        );

        return $w;
    }
}
