<?php
/**
 * Date: 24/02/2017
 * Time: 11:08
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Shares\Trading;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\Shares\StampDuty;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Shares\SharesCommissionRepository;
use Cytonn\Shares\ShareTradingRepository;

class Trade
{
    protected $repo;

    protected $date;

    public function __construct()
    {
        $this->repo = new ShareTradingRepository();
    }

    protected function transfer(
        ShareHolder $buyer,
        ShareHolder $seller = null,
        SharesEntity $sellingEntity = null,
        SharesPurchaseOrder $purchaseOrder,
        SharesSalesOrder $salesOrder = null,
        Carbon $date,
        $agreedPrice = null,
        $exemption = null
    ) {
        if (is_null($seller) && is_null($sellingEntity)) {
            throw new \InvalidArgumentException('Both seller and selling entity cannot be null');
        }

        $this->date = $date;

        $number = $this->getNumberOfSharesToTransfer($salesOrder, $purchaseOrder);

        $this->buy(
            $buyer,
            $seller,
            $sellingEntity,
            $purchaseOrder,
            $salesOrder,
            $date,
            $number,
            $agreedPrice,
            $exemption
        );

        $this->sell(
            $buyer,
            $seller,
            $sellingEntity,
            $purchaseOrder,
            $salesOrder,
            $date,
            $number,
            $agreedPrice,
            $exemption
        );

        $this->saveMatch($purchaseOrder, $number, $salesOrder, $sellingEntity);
    }

    private function buy(
        ShareHolder $buyer,
        ShareHolder $seller = null,
        SharesEntity $receiving = null,
        SharesPurchaseOrder $purchaseOrder,
        SharesSalesOrder $salesOrder = null,
        Carbon $date,
        $number,
        $agreedPrice = null,
        $exemption = null
    ) {
        if ($agreedPrice) {
            $orderPrice = $agreedPrice;
        } else {
            $orderPrice = $salesOrder ? $salesOrder->price : $receiving->sharePrice($date);
        }

        $price = $orderPrice;

        if (!is_null($exemption)) {
            $price = $exemption;
        }

        $amount = abs($price * $number);

        $entity = is_null($buyer) ? null : $buyer->entity;

        $payment = null;

        $this->repo->createSharePurchase([
            'number'                    => $number,
            'purchase_price'            => $price,
            'date'                      => $this->date,
            'client_payment_id'         => $payment ? $payment->id : null,
            'shares_purchase_order_id'  => $purchaseOrder->id,
            'shares_sales_order_id'     => is_null($salesOrder) ? null : $salesOrder->id,
            'share_holder_id'           => $buyer->id,
            'seller_id'                 => is_null($seller) ? null : $seller->id,
            'entity_id'                 => is_null($receiving) ? null : $receiving->id,
            'category_id'               => 1,
            'approval_id'               => $purchaseOrder->approval_id
        ]);
    }

    protected function creditBuyer(
        ShareHolder $buyer,
        SharesEntity $entity = null,
        $number,
        $price,
        Carbon $date
    ) {
        $client = $buyer->client;

        $recipient = $client->getLatestFA();

        $amount = abs($price * $number);

        $description = 'Bought ' . $number . ' shares at ' . $price . ' each.';

        return Payment::make(
            $client,
            $recipient,
            null,
            null,
            null,
            $entity,
            $date,
            $amount,
            'I',
            $description
        )->credit();
    }

    public function payStampDuty(
        SharePurchases $purchase,
        Carbon $date
    ) {

        $price = $purchase->purchase_price;

        $number = $purchase->number;

        $client = $purchase->shareHolder->client;

        $amount = abs($price * $number)/100;

        $description = 'Stamp duty for the purchase of ' . $number . ' shares at ' . $price . ' each.';

        $payment = Payment::make(
            $client,
            null,
            null,
            null,
            null,
            $purchase->shareHolder->entity,
            $date,
            $amount,
            'I',
            $description
        )->credit();

        $stampDuty = new StampDuty();

        $stampDuty->fill([
            'date' => $date,
            'client_payment_id' => $payment->id,
            'share_purchase_id' => $purchase->id
        ]);

        $stampDuty->save();

        return $stampDuty;
    }

    private function sell(
        ShareHolder $buyer,
        ShareHolder $seller = null,
        SharesEntity $sellingEntity = null,
        SharesPurchaseOrder $purchaseOrder,
        SharesSalesOrder $salesOrder = null,
        Carbon $date,
        $number,
        $agreedPrice = null,
        $exemption = null
    ) {

        if ($agreedPrice) {
            $orderPrice = $agreedPrice;
        } else {
            $orderPrice = $salesOrder ? $salesOrder->price : $sellingEntity->sharePrice($date);
        }

        $price = $orderPrice;

        if ($exemption) {
            $price = $exemption;
        }

        $entity = is_null($seller) ? null : $seller->entity;

        $payment = null;

        $this->repo->createShareSale([
            'number'                    => $number,
            'sale_price'                => $price,
            'date'                      => $this->date,
            'payment_id'                => $payment ? $payment->id : null,
            'shares_purchase_order_id'  => $purchaseOrder->id,
            'shares_sales_order_id'     => is_null($salesOrder) ? null : $salesOrder->id,
            'share_holder_id'           => is_null($seller) ? null : $seller->id,
            'entity_id'                 => is_null($sellingEntity) ? null :$sellingEntity->id,
            'category_id'               => 1,
            'approval_id'               => $purchaseOrder->approval_id,
            'buyer_id'                   => $buyer->id
        ]);
    }

    protected function debitSeller(
        ShareHolder $seller = null,
        SharesEntity $entity = null,
        $number,
        $price,
        Carbon $date
    ) {
        if ($seller) {
            $amount = abs($price * $number);

            $client = is_null($seller) ? null : $seller->client;

            $recipient = is_null($seller) ? null : $client->getLatestFA();

            $description = 'Sold ' . $number . ' shares at ' . $price . ' each.';

            return Payment::make(
                $client,
                $recipient,
                null,
                null,
                null,
                $entity,
                $date,
                $amount,
                'I',
                $description
            )->debit();
        }

        return null;
    }

    public function getNumberOfSharesToTransfer(SharesSalesOrder $salesOrder = null, SharesPurchaseOrder $purchaseOrder)
    {
        if (is_null($salesOrder)) {
            return $this->remainingSharesInEntity($purchaseOrder);
        }

        $canBeSold = $salesOrder->balance();

        $canBePurchased = $purchaseOrder->balance();

        $numberToSale = $canBeSold > $canBePurchased ? $canBePurchased : $canBeSold;

        if ($numberToSale <= 0) {
            \Flash::warning("No shares to be sold");
            return 0;
        }

        return $numberToSale;
    }

    private function saveMatch(
        SharesPurchaseOrder $purchaseOrder,
        $number,
        SharesSalesOrder $salesOrder = null,
        SharesEntity $entity = null
    ) {
        if (is_null($salesOrder) && is_null($entity)) {
            throw new \InvalidArgumentException('An order must be matched to an entity or sale order');
        }

        if (is_null($salesOrder)) {
            $this->saveFullyMatchedForEntity($purchaseOrder, $entity, $number);

            return;
        }

        $purchaseOrder->matchedSaleOrders()->attach($salesOrder->id, ['number'=>$number]);

        $this->saleFullyMatched($salesOrder);

        $this->purchaseFullyMatched($purchaseOrder);
    }

    protected function saveCommissionPayment(SharesSalesOrder $salesOrder, SharePurchases $purchase)
    {
        $shareSale = $this->getSale($salesOrder, $purchase);

        (new SharesCommissionRepository($shareSale, $purchase))->saveCommissionPaymentSchedule();
    }

    public function getSale(SharesSalesOrder $salesOrder, SharePurchases $purchase)
    {
        return ShareSale::where([
            'shares_sales_order_id' => $salesOrder->id,
            'shares_purchase_order_id' => $purchase->shares_purchase_order_id
        ])->first();
    }

    protected function saveFullyMatchedForEntity($purchaseOrder, $entity, $number)
    {
        $purchaseOrder->matchedEntitySales()->attach($entity, ['number' => $number]);

        $purchaseOrder->update(['matched' => true]);
    }

    protected function saleFullyMatched(SharesSalesOrder $salesOrder)
    {
        $matched = $salesOrder->balance() <= 0;

        $salesOrder->matched = $matched;

        $salesOrder->save();
    }

    protected function purchaseFullyMatched(SharesPurchaseOrder $purchaseOrder)
    {
        $matched = $purchaseOrder->balance() <= 0;

        $purchaseOrder->matched = $matched;

        $purchaseOrder->matched_date = Carbon::today();

        $purchaseOrder->save();
    }

    private function remainingSharesInEntity(SharesPurchaseOrder $purchaseOrder)
    {
        $entity = $purchaseOrder->buyer->entity;

        $remaining = $entity->remainingShares();

        $requested = $purchaseOrder->number;

        return $requested > $remaining ? $remaining : $requested;
    }
}
