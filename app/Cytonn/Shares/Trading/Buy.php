<?php
/**
 * Date: 24/02/2017
 * Time: 11:08
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Shares\Trading;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Shares\ShareTradingRepository;

class Buy extends Trade
{
    protected $buyer;

    protected $repo;

    public function __construct(ShareHolder $buyer)
    {
        $this->buyer = $buyer;
        $this->repo = new ShareTradingRepository();

        parent::__construct();
    }

    public function fromEntity(SharesEntity $entity, array $data)
    {
        $buyer = $this->buyer;

        $price = isset($data['exemption']) ? $data['exemption'] : $entity->sharePrice($data['request_date']);

        $data['price'] = $price;

        $approval = ClientTransactionApproval::findOrFail($data['approval_id']);

        $purchaseOrder = $this->repo->makePurchaseOrder($buyer, $approval, $data);

        //transfer shares and cash
        $date = Carbon::parse($data['request_date']);

        $exemption = isset($data['exemption']) ? $data['exemption'] : null;

        $this->transfer($buyer, null, $entity, $purchaseOrder, null, $date, $exemption);
    }

    public function trade(
        SharesSalesOrder $salesOrder,
        SharesPurchaseOrder $purchaseOrder,
        Carbon $date,
        $agreedPrice = null
    ) {
        $this->transfer(
            $purchaseOrder->buyer,
            $salesOrder->seller,
            null,
            $purchaseOrder,
            $salesOrder,
            $date,
            $agreedPrice,
            null
        );
    }

    public function makePayment(SharesSalesOrder $salesOrder, SharePurchases $purchase, Carbon $date)
    {
        $buyer = $purchase->shareHolder;

        $number = $purchase->number;

        $seller = $salesOrder->seller;

        $entity = is_null($buyer) ? null : $buyer->entity;

        $price = is_null($salesOrder) ? $entity->sharePrice($date) : $purchase->purchase_price;

        $amount = $number * $price;

        $total_amount = $amount + $amount/100;

        if ($total_amount < $buyer->sharePaymentsBalance() || $total_amount == $buyer->sharePaymentsBalance()) {
            $this->payStampDuty($purchase, $date->copy());
        }

        $purchasePayment = $this->creditBuyer($buyer, $entity, $number, $price, $date->copy());

        $this->updatePurchase($purchase, $purchasePayment);

        $salePayment = $this->debitSeller($seller, $entity, $number, $price, $date->copy());

        $this->updateSale($salesOrder, $purchase, $salePayment);

        $this->saveCommissionPayment($salesOrder, $purchase);
    }

    public function updatePurchase(SharePurchases $purchases, ClientPayment $payment)
    {
        $purchases->clientPayment()->associate($payment);

        return $purchases->save();
    }

    public function updateSale(SharesSalesOrder $salesOrder, SharePurchases $purchase, ClientPayment $payment)
    {
        $shareSale = ShareSale::where([
            'shares_sales_order_id' => $salesOrder->id,
            'shares_purchase_order_id' => $purchase->shares_purchase_order_id
        ])->first();

        $shareSale->payment()->associate($payment);

        return $shareSale->save();
    }

    public function reverse(SharesSalesOrder $salesOrder, SharePurchases $purchase)
    {
        $this->getSale($salesOrder, $purchase)->delete();

        $purchaseOrder = $purchase->purchaseOrder;

        $purchase->delete();

        $purchaseOrder->matchedSaleOrders()->detach($salesOrder->id);

        $this->saleFullyMatched($salesOrder);

        $this->purchaseFullyMatched($purchaseOrder);
    }
}
