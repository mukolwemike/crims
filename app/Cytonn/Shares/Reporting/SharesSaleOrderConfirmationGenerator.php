<?php

namespace Cytonn\Shares\Reporting;

use App\Cytonn\Models\SharesSalesOrder;
use App\Cytonn\Models\User;
use Cytonn\Presenters\UserPresenter;

/**
 * Class SharesSaleOrderConfirmationGenerator
 *
 * @package Cytonn\Shares\Reporting
 */
class SharesSaleOrderConfirmationGenerator
{
    /**
     * @param SharesSalesOrder $salesOrder
     * @param User             $sender
     * @param bool             $successful
     * @return mixed
     */
    public function generate(SharesSalesOrder $salesOrder, User $sender, $successful = true)
    {
        $seller = $salesOrder->seller;
        $client = $seller->client;
        $entity = $seller->entity;

        $email_sender = UserPresenter::presentLetterClosing($sender->id);

        $template = $successful ? 'shares_sale_order_confirmation_successful' : 'shares_sale_order_confirmation_failed';

        return \PDF::loadView(
            'shares.reports.'.$template,
            ['email_sender'=>$email_sender,
            'sale_order'=>$salesOrder, 'client'=>$client, 'seller'=>$seller, 'entity'=>$entity, 'logo'=>$entity->logo
            ]
        );
    }
}
