<?php

namespace Cytonn\Shares\Reporting;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CoopPayment;
use App\Cytonn\Models\ShareHolding;
use Carbon\Carbon;
use Cytonn\Presenters\UserPresenter;

/**
 * Class BusinessConfirmationGenerator
 *
 * @package Cytonn\Shares\Reporting
 */
class BusinessConfirmationGenerator
{
    /**
     * @param ShareHolding $holding
     * @param $sender_id
     * @return mixed
     */
    public function generate(ShareHolding $holding, $sender_id)
    {
        return $this->shares($holding, $sender_id);
    }

    /**
     * Generate a share confirmation (subsequent shares)
     *
     * @param  ShareHolding $holding
     * @param  $sender_id
     * @return mixed
     */
    protected function shares(ShareHolding $holding, $sender_id)
    {
        $client = Client::findOrFail($holding->shareHolder->client_id);

        $holdings = ShareHolding::where('client_id', $holding->client_id)->get();

        $email_sender = UserPresenter::presentLetterClosing($sender_id);

        $membershipFee = abs(CoopPayment::where('client_id', $client->id)
            ->where('type', 'membership')->sum('amount'));

        $totalSharePurchase = $holdings->sum(
            function ($holding) {
                return $holding->number * $holding->purchase_price;
            }
        );

        $entity = $holding->shareHolder->entity;

        $payment = ClientPayment::ofType('FI')
            ->where('share_entity_id', $entity->id)
            ->where('client_id', $client->id)
            ->sum('amount');

        $entity = $holding->shareHolder->entity;

        $payments = CoopPayment::where('amount', '<', 0)
            ->where('client_id', $client->id)->get();

        $payments = $this->generateDescriptions($payments);

        $stampDuty = $holding->stampDuty->sum(function ($duty) {
            return abs($duty->payment->amount);
        });

        $stamp_duty_amount = $stampDuty;

        $total = $payments->sum(
            function ($p) {
                return abs($p->amount);
            }
        );

        $stampDuty = count($holding->stampDuty) ? true : false;

        return \PDF::loadView('shares.reports.business_confirmation', ['email_sender' => $email_sender,
                'holding' => $holding,
                'holdings' => $holdings,
                'client' => $client,
                'membership_fee' => $membershipFee,
                'total_share_purchase' => $totalSharePurchase,
                'logo' => $entity->fundManager->logo,
                'payments' => $payments,
                'total' => $total,
                'entity' => $entity,
                'fundManager' => $entity->fundManager,
                'stampDuty' => $stamp_duty_amount,
                'payment' => $payment,
        ]);
    }


    /**
     * @param $payments
     * @return mixed
     */
    protected function generateDescriptions($payments)
    {
        $payments->each(
            function ($p) {
                $p->description = $p->repo->describe();
            }
        );

        return $payments;
    }
}
