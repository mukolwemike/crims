<?php
/**
 * Date: 15/09/2016
 * Time: 2:34 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Shares\Reporting;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharesCampaignStatementItem;
use App\Cytonn\Models\SharesStatementCampaign;
use App\Cytonn\Models\StatementLog;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Mailers\Shares\StatementMailer;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Shares\Reporting\Statements\Processor;

class StatementGenerator
{
    public function generate(ShareHolder $shareHolder, $date, User $sender, SharesStatementCampaign $campaign = null)
    {
        $client = $shareHolder->client;

        $processor = new Processor($shareHolder, $date);

        $processed = $processor->process();

        $entity = $shareHolder->entity;

        $email_sender = UserPresenter::presentLetterClosing($sender->id);

        return \PDF::loadView(
            'shares.reports.statement',
            [
                'email_sender'=>$email_sender,
                'processed'=>$processed,
                'date'=>$date,
                'client'=>$client,
                'entity'=>$entity,
                'logo'=>$entity->logo,
                'share_holder'=>$shareHolder,
                'campaign' => $campaign
            ]
        );
    }

    /**
     * Send a shares campaign statement item
     *
     * @param  SharesCampaignStatementItem $item
     * @return bool
     */
    public function sendSharesCampaignStatementItem(SharesCampaignStatementItem $item)
    {
        $campaign = $item->campaign;

        $shareHolder = ShareHolder::findOrFail($item->share_holder_id);

        $sender = User::findOrFail($campaign->sender_id);

        $client = $shareHolder->client;

        if (is_null($client->getContactEmailsArray())) {
            return;
        }

        $statements = [];

        $statements['Shares Statement - '. ClientPresenter::presentJointFirstNameLastName($client->id).'.pdf'] =
            $this->generate($shareHolder, $campaign->send_date, $sender, $campaign)->output();

        $mailer = new StatementMailer();
        $mailer->sendStatementToClient($client, $statements, $campaign->send_date, $campaign->sender_id, $campaign);

        $log = new StatementLog();
        $log->fill(['client_id'=>$item->client_id, 'sent_by'=>$campaign->sender_id]);
        $log->save();

        $item->sent = true;
        //        $item->sent_on = Carbon::now();
        $item->sent_on = $campaign->send_date;//Changed to date send date of the campaign

        return $item->save();
    }
}
