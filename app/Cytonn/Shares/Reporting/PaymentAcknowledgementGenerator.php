<?php
/**
 * Created by PhpStorm.
 * User: yerick
 * Date: 24/10/2018
 * Time: 15:44
 */

namespace App\Cytonn\Shares\Reporting;

use App\Cytonn\Models\SharePurchases;
use Cytonn\Presenters\UserPresenter;

class PaymentAcknowledgementGenerator
{
    public function generate(SharePurchases $purchase)
    {
        $client = $purchase->shareHolder->client;

        $entity = $purchase->shareHolder->entity;

        $fm = $purchase->shareHolder->entity->fundManager;

        $emailSender = UserPresenter::presentLetterClosing(\Auth::user()->id);

        $stampDuty = $purchase->stampDuty->sum(function ($duty) {
            return abs($duty->payment->amount);
        });

        $amount = abs($purchase->clientPayment->amount) + $stampDuty;

        return \PDF::loadView('shares.reports.payment_acknowledgement', [
            'emailSender'   =>  $emailSender,
            'client'        =>  $client,
            'purchase'      =>  $purchase,
            'logo'          =>  $fm->logo,
            'fundManager'   => $fm,
            'amount'        => $amount,
            'entity' => $entity,
            'companyAddress' => str_contains($fm->fullname, 'Cytonn Asset Managers') ? 'asset_managers' : null
        ]);
    }
}
