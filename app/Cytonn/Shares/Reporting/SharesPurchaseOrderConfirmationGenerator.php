<?php

namespace Cytonn\Shares\Reporting;

use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\User;
use Cytonn\Presenters\UserPresenter;

/**
 * Class SharesPurchaseOrderConfirmationGenerator
 *
 * @package Cytonn\Shares\Reporting
 */
class SharesPurchaseOrderConfirmationGenerator
{
    /**
     * @param SharesPurchaseOrder $purchaseOrder
     * @param User $sender
     * @param $successful | bool
     * @return mixed
     */
    public function generate(SharesPurchaseOrder $purchaseOrder, User $sender, $successful = true)
    {
        $buyer = $purchaseOrder->buyer;
        $client = $buyer->client;
        $entity = $buyer->entity;

        $email_sender = UserPresenter::presentLetterClosing($sender->id);

        $template = $successful
            ? 'shares_purchase_order_confirmation_successful' : 'shares_purchase_order_confirmation_failed';

        return \PDF::loadView(
            'shares.reports.' . $template,
            [
                'email_sender' => $email_sender,
                'purchase_order' => $purchaseOrder,
                'client' => $client,
                'buyer' => $buyer,
                'entity' => $entity,
                'logo' => $entity->logo
            ]
        );
    }
}
