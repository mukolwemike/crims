<?php
/**
 * Date: 04/04/2018
 * Time: 12:34
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Shares\Reporting\Statements;

use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\ShareSale;
use Carbon\Carbon;

class Processor
{
    protected $shareHolder;

    protected $date;

    /**
     * Processor constructor.
     * @param $shareHolder
     * @param $date
     */
    public function __construct(ShareHolder $shareHolder, Carbon $date)
    {
        $this->shareHolder = $shareHolder;
        $this->date = $date;
    }


    public function process()
    {
        $price = $this->shareHolder->entity->priceTrails()
            ->latest('date')
            ->first()->price;

        $holdings = ShareHolding::where('share_holder_id', $this->shareHolder->id)
            ->where('date', '<=', $this->date)
            ->get()
            ->map(function (ShareHolding $holding) use ($price) {
                return (object)[
                    'action' => 'Buy',
                    'date'   => $holding->date,
                    'number' => $holding->number,
                    'actual_number' => $holding->number,
                    'cost_price'  => $holding->purchase_price,
                    'cost_value' => $holding->number * $holding->purchase_price,
                    'market_price' => $price,
                    'market_value' => $price * $holding->number,
                    'redeem_interest' => 0
                ];
            });


        $sales = ShareSale::where("share_holder_id", $this->shareHolder->id)
            ->where('date', '<=', $this->date)
            ->get()
            ->map(function (ShareSale $sale) use ($price) {
                return (object)[
                    'action' => $sale->type == 2 ? 'Redeem' : "Sell",
                    'date'   => $sale->date,
                    'number' => -1 * $sale->number,
                    'cost_price' => $sale->sale_price,
                    'cost_value' => -1 * $sale->sale_price * $sale->number,
                    'market_price' => $price,
                    'market_value' => -1 * $price * $sale->number,
                    'redeem_interest' => ($sale->redeem_interest) ? $sale->redeem_interest : 0
                ];
            });

        $activities = collect([])->merge($holdings)->merge($sales)->sortByDate('date', 'ASC');

        $totals = (object)[
            'number' => $activities->sum('number'),
            'cost_value' => $activities->sum('cost_value'),
            'market_value' => $activities->sum('market_value')
        ];

        return (object)[
            'actions' => $activities,
            'total' => $totals
        ];
    }
}
