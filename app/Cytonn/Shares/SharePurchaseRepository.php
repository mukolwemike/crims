<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Shares;

use App\Cytonn\Models\SharePurchases;
use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;

class SharePurchaseRepository
{
    use CalculatorTrait;

    const INTEREST_RATE = 15;

    /**
     * @var SharePurchases
     */
    protected $holder;

    /**
     * SharePurchaseRepository constructor.
     *
     * @param SharePurchases $purchase
     */
    public function __construct(SharePurchases $purchase = null)
    {
        is_null($purchase) ? $this->purchase = new SharePurchases() : $this->purchase = $purchase;
    }

    /*
     * Get the principal for the shares
     */
    public function getPurchasePrice($number = null)
    {
        $number = (is_null($number)) ? $this->purchase->number : $number;

        return $number * $this->purchase->purchase_price;
    }

    /*
     * Get the interest rate
     */
    public function getInterestRate()
    {
        return static::INTEREST_RATE;
    }

    /*
     * Get the shares gross interest
     */
    public function getShareGrossInterest(Carbon $date = null)
    {
        $date = is_null($date) ? Carbon::now() : $date;

        return $this
            ->getGrossInterest($this->getPurchasePrice(), $this->getInterestRate(), $this->purchase->date, $date);
    }

    /*
     * Get the total withholding tax for the share purchase
     */
    public function getShareWithholdingTax(Carbon $date)
    {
        return $this->getWithHoldingTax($this->getShareGrossInterest($date));
    }

    /*
     * Get the net interest for th share purchase
     */
    public function getShareNetInterest(Carbon $date)
    {
        $taxable = $this->purchase->shareHolder->client->taxable;

        return $this->getNetInterest($this->getShareGrossInterest($date), $taxable);
    }

    /*
     * Get the net value for the share purchase
     */
    public function getNetPurchaseValue(Carbon $date)
    {
        return $this->getPurchasePrice() + $this->getShareNetInterest($date);
    }
}
