<?php
namespace Cytonn\Shares\Forms;

use Laracasts\Validation\FormValidator;

/**
 * Class AddShareEntityForm
 *
 * @package Cytonn\Shares\Forms
 */
class AddShareEntityForm extends FormValidator
{
    public $rules = [
        'name'=>'required|unique:shares_entities',
        'fund_manager_id'=>'required|numeric',
        'account_id'=>'required|numeric',
        'currency_id'=>'required|numeric',
    //        'max_holding'=>'required|numeric'
    ];
}
