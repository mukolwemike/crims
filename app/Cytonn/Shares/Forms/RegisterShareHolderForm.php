<?php

namespace Cytonn\Shares\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

/**
 * Class RegisterShareHolderForm
 *
 * @package Cytonn\Shares\Forms
 */
class RegisterShareHolderForm extends FormValidator
{
    /**
     * @var array
     */
    public $rules = [];

    /**
     * @var array $corporate
     */
    protected $corporate = [
//        'number'                    =>  'required|unique:share_holders',
        'branch_id'                 =>  'required_with:investor_account_name|numeric',
        'corporate_investor_type'   =>  'required|numeric',
        'registered_name'           =>  'required',
        'registered_address'        =>  'required',
        'phone'                     =>  'required',
        'email'                     =>  'required|email',
        'method_of_contact_id'      =>  'required|numeric',
        'postal_code'               =>  'required_with:postal_address',
        'street'                    =>  'required_without:postal_address',
        'contact_person_title'      =>  'required',
        'contact_person_fname'      =>  'required',
        'contact_person_lname'      =>  'required'
        ];

    /**
     * @var array $individual
     */
    protected $individual = [
//        'number'                    =>  'required|unique:share_holders',
        'town'                      =>  'required',
        'title_id'                  =>  'required|numeric',
        'lastname'                  =>  'required',
        'firstname'                 =>  'required',
        'email'                     =>  'required|email',
        'method_of_contact_id'      =>  'required|numeric',
        'phone'                     =>  'required',
        'postal_code'               =>  'required_with:postal_address',
        'street'                    =>  'required_without:postal_address'
    ];

    /**
     * Constructor
     *
     * @param ValidatorFactory $validator
     */
    public function __construct(ValidatorFactory $validator)
    {
        parent::__construct($validator);
    }

    public function corporate()
    {
        $this->rules = $this->corporate;
    }

    public function individual()
    {
        $this->rules = $this->individual;
    }
}
