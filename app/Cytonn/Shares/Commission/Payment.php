<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Shares\Commission;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Models\Shares\ShareCommissionPayment;

class Payment
{
    public function make(CommissionRecepient $recipient, ClientTransactionApproval $approval, BulkCommissionPayment $bulk)
    {
        $calculation = $recipient->shareCommissionCalculator($bulk->start, $bulk->end);

        $summary = $calculation->summary();

        $total = $summary->total();

        $overrides = $summary->override();

        if ($total == 0 && $overrides == 0) {
            return;
        }

        $amount = $total + $overrides;

        return (new ShareCommissionPayment())->create([
                'commission_recipient_id' => $recipient->id,
                'amount' => $amount,
                'overrides' => $overrides,
                'date' => $bulk->end,
                'description' => $this->createNarrative($bulk),
                'approval_id' => $approval->id
            ]);
    }

    private function createNarrative(BulkCommissionPayment $bulk)
    {
        return 'Commission Payment for ' . $bulk->start->toFormattedDateString() . ' to ' .
            $bulk->end->toFormattedDateString();
    }
}
