<?php
/**
 * Date: 02/05/2017
 * Time: 09:48
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Shares;

use App\Cytonn\Models\SharesEntity;
use Carbon\Carbon;

class ShareEntityRepository
{
    /**
     * @var
     */
    protected $entity;

    /**
     * ShareEntityRepository constructor.
     *
     * @param $entity
     */
    public function __construct(SharesEntity $entity = null)
    {
        is_null($entity)? $this->entity = new SharesEntity(): $this->entity = $entity;
    }

    public function pricePerTrail(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $latest = $this->entity->priceTrails()->latest('date')->before($date)->first();

        if ($latest) {
            return $latest->price;
        }

        if ($issue = $this->entity->issues()->latest('issue_date')->before($date)->first()) {
            return $issue->price;
        }

        return 0;
    }
}
