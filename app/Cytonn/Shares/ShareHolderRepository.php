<?php

namespace Cytonn\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ShareHolder;
use Carbon\Carbon;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Investment\JointInvestorAddCommand;
use Cytonn\Shares\Rules\ShareHolderApplicationRules;
use Illuminate\Support\Arr;
use Laracasts\Commander\CommanderTrait;

/**
 * Class ShareHolderRepository
 *
 * @package Cytonn\Shares
 */
class ShareHolderRepository
{
    use ValidatorTrait, CommanderTrait, ShareHolderApplicationRules;
    /**
     * @var ShareHolder
     */
    protected $holder;


    /**
     * ShareHolderRepository constructor.
     *
     * @param ShareHolder $holder
     */
    public function __construct(ShareHolder $holder = null)
    {
        is_null($holder) ? $this->holder = new ShareHolder() : $this->holder = $holder;
    }

    /**
     * @param Client $client
     * @param array $data
     * @return ShareHolder
     */
    public function registerShareholder(Client $client, array $data)
    {
        $holder = new ShareHolder();

        $holder->fill(
            [
                'client_id' => $client->id,
                'number' => $data['number'],
                'entity_id' => $data['entity_id'],
            ]
        );

        $holder->save();
        return $holder;
    }

    public function saveJointApplicant($data, $id, $client, $jointID = null)
    {
        $data = (array)$data;
        $input = array_add($data, 'share_holding_id', $id);
        $input = array_add($input, 'joint_id', $jointID);
        $input = array_add($input, 'client_id', $client);

        return $this->execute(JointInvestorAddCommand::class, ['data' => $input]);
    }

    /**
     * @param $input
     * @return bool
     */
    public function checkIfIndividualShareholderExists($input)
    {
        return ShareHolder::where('number', $input['number'])
            ->where('entity_id', $input['entity_id'])
            ->whereHas(
                'client',
                function ($client) use ($input) {
                    $client->where('pin_no', $input['pin_no'])
                        ->where('id_or_passport', $input['id_or_passport'])
                        ->where('phone', $input['phone'])
                        ->whereHas(
                            'contact',
                            function ($contact) use ($input) {
                                $contact->where('email', $input['email'])
                                    ->orWhere('phone', $input['phone']);
                            }
                        );
                }
            )->exists();
    }

    /**
     * @param $input
     * @return bool
     */
    public function checkIfCorporateShareholderExists($input)
    {
        return ShareHolder::where('number', $input['number'])
            ->where('entity_id', $input['entity_id'])
            ->whereHas(
                'client',
                function ($client) use ($input) {
                    $client->where('pin_no', $input['pin_no'])
                        ->where('phone', $input['phone'])
                        ->whereHas(
                            'contact',
                            function ($contact) use ($input) {
                                $contact->where('email', $input['email'])
                                    ->where('registration_number', $input['registration_number'])
                                    ->orWhere('phone', $input['phone']);
                            }
                        );
                }
            )->exists();
    }

    /*
     * Get the shares to be redeemed
     */
    public function getSharesToBeRedeemed(Carbon $date)
    {
        $purchases = $this->holder->sharePurchases()
            ->where('date', '<=', $date)
            ->orderBy('date')
            ->get();

        $sales_bal = $this->holder->shareSales()
            ->where('date', '<=', $date)
            ->orderBy('date')
            ->sum('number');

        return $purchases->filter(function ($purchase) use (&$sales_bal) {
            if ($sales_bal <= 0) {
                $purchase->balance = $purchase->number;
            } else {
                if ($purchase->number > $sales_bal) {
                    $purchase->balance = $purchase->number - $sales_bal;
                    $sales_bal = 0;
                } else {
                    $purchase->balance = 0;
                    $sales_bal = $sales_bal - $purchase->number;
                }
            }

            $purchase->number = $purchase->balance;

            return $purchase->balance > 0;
        });
    }

    /*
     * Get the shares for redemption for a given share holder
     */
    public function getSharesForRedemption(Carbon $date)
    {
        return $this->getSharesToBeRedeemed($date)
            ->map(function ($share) use ($date) {
                return [
                    'number' => $share->number,
                    'price' => $share->purchase_price,
                    'date' => $share->date,
                    'amount' => $share->repo->getPurchasePrice(),
                    'rate' => $share->repo->getInterestRate(),
                    'gross_interest' => $share->repo->getShareGrossInterest($date),
                    'withholding' => $share->repo->getShareWithholdingTax($date),
                    'net_interest' => $share->repo->getShareNetInterest($date),
                    'total' => $share->repo->getNetPurchaseValue($date)
                ];
            });
    }

    /*
     * Check if shareholder has linked purchases or sales
     */
    public function hasShareProducts()
    {
        return $this->holder->shareSalesOrders()->exists() ||
            $this->holder->sharePurchaseOrders()->exists();
    }

    public function validateShareHolderApplication($input)
    {
        $clientType = $input['individual'] == 1 ? 'individual' : 'corporate';

        if (Arr::exists(request()->all(), 'risk_reason')) {
            $validator = ($clientType == 'individual')
                ? $this->validateIndividualRiskyForm(request())
                : $this->validateCorporateRiskyForm(request());
        } elseif ($input['new_client'] == 2) {
            $validator = $this->validateExistingForm(request());
        } elseif ($input['new_client'] == 3) {
            $validator = ($clientType == 'individual')
                ? $this->validateIndividualDuplicateForm(request())
                : $this->validateCorporateDuplicateForm(request());
        } else {
            $validator = ($clientType == 'individual')
                ? $this->validateIndividualForm(request())
                : $this->validateCorporateForm(request());
        }


        return $validator;
    }
}
