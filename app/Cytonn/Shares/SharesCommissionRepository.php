<?php

namespace Cytonn\Shares;

use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;

/**
 * Class SharesCommissionRepository
 *
 * @package Cytonn\Shares
 */
class SharesCommissionRepository
{

    /**
     * @var ShareSale $shareSale
     */
    protected $shareSale;

    /**
     * @var SharePurchases $sharePurchase
     */
    protected $sharePurchase;

    /**
     * SharesCommissionRepository constructor.
     *
     * @param ShareSale      $shareSale
     * @param SharePurchases $sharePurchase
     */
    public function __construct(ShareSale $shareSale, SharePurchases $sharePurchase)
    {
        $this->shareSale = $shareSale;
        $this->sharePurchase = $sharePurchase;
    }

    /**
     * Save commission payment schedule for new share order match
     *
     * @return SharesCommissionPaymentSchedule
     */
    public function saveCommissionPaymentSchedule()
    {
        $schedule = new SharesCommissionPaymentSchedule();
        $schedule->share_sale_id = $this->shareSale->id;
        $schedule->share_purchase_id = $this->sharePurchase->id;
        $schedule->commission_recipient_id = $this->sharePurchase->purchaseOrder->commission_recipient_id;
        $schedule->date = $this->shareSale->date;
        $schedule->amount = $this->calculateCommission();
        $schedule->description =
            'Share purchase of ' . $this->sharePurchase->number . ' @ ' . $this->sharePurchase->purchase_price;
        $schedule->save();

        return $schedule;
    }

    /**
     * @return number
     */
    private function calculateCommission()
    {
        return abs(
            $this->sharePurchase->purchaseOrder->commission_rate
            * $this->shareSale->payment->amount / 100
        );
    }
}
