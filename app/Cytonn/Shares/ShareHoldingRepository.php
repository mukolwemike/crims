<?php
/**
 * Date: 13/09/2016
 * Time: 10:04 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharesBusinessConfirmation;
use App\Cytonn\Models\SharesCategory;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\SharesPriceTrail;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\Payment;

class ShareHoldingRepository
{
    protected $holding;

    public function __construct(ShareHolding $holding = null)
    {
        is_null($holding) ? $this->holding = new ShareHolding() : $this->holding = $holding;
    }

    public function createHoldingWithPayment(array $data)
    {
        $client = Client::findOrFail($data['client_id']);
        $entity = SharesEntity::findOrFail($data['entity_id']);
        $category = SharesCategory::findOrFail($data['category_id']);

        $holding = $this->createHolding(
            $client,
            $entity,
            $category,
            $data['number'],
            $data['price'],
            $data['date'],
            ClientTransactionApproval::find($data['approval_id'])
        );

        $payment = Payment::make(
            $client,
            null,
            null,
            null,
            null,
            $entity,
            Carbon::parse($data['date']),
            (float)$data['number'] * $data['price'],
            'I',
            'Purchase of shares'
        )->credit();

        $holding->client_payment_id = $payment->id;
        $holding->save();

        $this->holding = $holding;

        return $holding;
    }

    public function createHolding(
        Client $client,
        SharesEntity $entity,
        SharesCategory $category,
        $number,
        $price,
        $date,
        ClientTransactionApproval $approval
    ) {
        $shareHolder = $client->shareHolders()->where('entity_id', $entity->id)->first();

        $this->holding->fill(
            [
                'number' => $number,
                'purchase_price' => $price,
                'date' => $date,
                'entity_id' => $entity->id,
                'client_id' => $client->id,
                'category_id' => $category->id,
                'approval_id' => $approval->id,
                'share_holder_id' => $shareHolder ? $shareHolder->id : null
            ]
        );

        $this->holding->save();

        return $this->holding;
    }

    public function checkIfConfirmationHasBeenSent()
    {
        return SharesBusinessConfirmation::where('holding_id', $this->holding->id)->count() > 0 ? 1 : 0;
    }

    public function checkIfSharePurchaseIsDuplicate(array $data)
    {
        //        dd($data);
        if (empty($data['purchase_price'])) {
            $data['purchase_price'] = SharesPriceTrail::getPrice($data['entity_id'], $data['date']);
        }

        $data['number'] = floor($data['amount'] / $data['purchase_price']);
        $data =
            array_only($data, ['date', 'entity_id', 'category_id', 'purchase_price', 'number', 'client_id', 'amount']);

        if (empty($data['purchase_price'])) {
            $data['purchase_price'] = SharesPriceTrail::getPrice($data['entity_id'], $data['date']);
        }

        $data['number'] = floor($data['amount'] / $data['purchase_price']);

        $data = array_only($data, ['date', 'entity_id', 'category_id', 'purchase_price', 'number', 'client_id']);
        return (bool)ShareHolding::where($data)->first();
    }

    public function createBusinessConfirmation()
    {
        $payload = $this->holding->toArray();


        if ($this->checkIfConfirmationHasBeenSent()) {
            return;
        }

        return SharesBusinessConfirmation::add([
            'holding_id' => $this->holding->id,
            'payload' => $payload,
            'sent_by'=> auth()->id()
        ]);
    }
}
