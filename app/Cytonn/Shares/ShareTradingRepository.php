<?php

namespace Cytonn\Shares;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;
use Carbon\Carbon;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Shares\Trading\Buy;
use Cytonn\Shares\Trading\Trade;
use Illuminate\Support\Collection;

class ShareTradingRepository
{
    use ValidatorTrait;

    protected $sales_order;

    protected $purchase_order;

    public function makeSalesOrder(array $data)
    {
        $rules = [
            'number' => 'required|numeric',
            'price' => 'required|numeric',
            'request_date' => 'required|date',
            'seller_id' => 'required|numeric',
            'approval_id' => 'required|numeric',
            'good_till_filled_cancelled' => 'required',
            'expiry_date' => 'date|nullable'
        ];

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            throw new ClientInvestmentException($validator->messages()->first());
        }

        $order = new SharesSalesOrder();

        $order->fill($data);

        $order->save();
    }

    public function makePurchaseOrder(ShareHolder $buyer, ClientTransactionApproval $approval, array $data)
    {
        $rules = [
            'number' => 'required|numeric',
            'price' => 'required|numeric',
            'request_date' => 'required|date',
            'buyer_id' => 'required|numeric',
            'approval_id' => 'required|numeric',
            'good_till_filled_cancelled' => '',
            'expiry_date' => 'date|nullable'
        ];

        $validator = \Validator::make($data, $rules);
        if ($validator->fails()) {
            throw new ClientInvestmentException($validator->messages()->first());
        }

        $this->purchase_order = SharesPurchaseOrder::create(
            [
                'number' => $data['number'],
                'price' => $data['price'],
                'request_date' => $data['request_date'],
                'approval_id' => $approval->id,
                'buyer_id' => $buyer->id,
                'good_till_filled_cancelled' => isset($data['good_till_filled_cancelled'])
                    ? $data['good_till_filled_cancelled'] : null,
                'expiry_date' => isset($data['expiry_date']) ? $data['expiry_date'] : null,
                'commission_recipient_id' =>
                    isset($data['commission_recipient_id']) ? $data['commission_recipient_id'] : null,
                'commission_rate' => isset($data['commission_rate']) ? abs($data['commission_rate']) : 0,
            ]
        );

        return $this->purchase_order;
    }

    public function matchSaleOrder(
        SharesSalesOrder $salesOrder,
        Collection $purchaseOrders,
        Carbon $date,
        $agreedPrice = null
    ) {
        $purchaseOrders->each(function (SharesPurchaseOrder $purchaseOrder) use ($salesOrder, $date, $agreedPrice) {
            $buy = new Buy($purchaseOrder->buyer);

            $buy->trade($salesOrder, $purchaseOrder, $date, $agreedPrice);
        });
    }

    public function createSharePurchase(array $data)
    {
        $rules = [
            'number' => 'required|numeric',
            'purchase_price' => 'required|numeric',
            'date' => 'required|date',
            'shares_purchase_order_id' => 'required',
            'shares_sales_order_id' => 'required_with:seller_id',
            'share_holder_id' => 'required',
            'entity_id' => 'required_without:seller_id',
            'category_id' => 'required',
            'approval_id' => 'required'
        ];

        if (!$this->isValid($data, $rules)) {
            throw new ClientInvestmentException('The share purchase details are malformed');
        }

        return ShareHolding::create($data);
    }

    public function createShareSale(array $data)
    {
        $rules = [
            'number' => 'required|numeric',
            'sale_price' => 'required|numeric',
            'date' => 'required',
            'shares_purchase_order_id' => 'required',
            'shares_sales_order_id' => 'required_with:share_holder_id',
            'buyer_id' => 'required',
            'entity_id' => 'required_without:share_holder_id',
            'category_id' => 'required',
            'approval_id' => 'required'
        ];

        if (!$this->isValid($data, $rules)) {
            throw new ClientInvestmentException('The share sale details are malformed');
        }

        return ShareSale::create($data);
    }

    public function sharePurchaseOrderSettlement(SharesSalesOrder $salesOrder, Collection $purchases, Carbon $date)
    {
        $purchases->each(function (SharePurchases $purchase) use ($salesOrder, $date) {

            $buy = new Buy($purchase->shareHolder);

            $buy->makePayment($salesOrder, $purchase, $date);
        });
    }

    public function reverseShareMatching(SharesSalesOrder $salesOrder, Collection $purchases)
    {
        $purchases->each(function (SharePurchases $purchase) use ($salesOrder) {

            $buy = new Buy($purchase->shareHolder);

            $buy->reverse($salesOrder, $purchase);
        });
    }
}
