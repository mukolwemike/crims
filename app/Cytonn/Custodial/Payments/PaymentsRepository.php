<?php

namespace Cytonn\Custodial\Payments;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ShareHolding;
use Cytonn\Exceptions\CRIMSGeneralException;

class PaymentsRepository
{
    protected $payment;

    /**
     * PaymentsRepository constructor.
     *
     * @param $payment
     */
    public function __construct(ClientPayment $payment)
    {
        $this->payment = $payment;
    }

    public function describe()
    {
        $description = '';

        switch ($this->payment->type->slug) {
            case 'I':
                $holding = ShareHolding::where('client_payment_id', $this->payment->id)->first();

                if (!is_null($holding)) {
                    $description = "Share purchase ($holding->number shares @ $holding->purchase_price)";
                }
                break;
            case 'M':
                $description = 'Membership fees';
                break;
            default:
                $description = $this->payment->description;
        }

        return  $description;
    }

    public function setChequeAsNotValued()
    {
        if (!$this->isCheque()) {
            return;
        }

        $amount = $this->payment->amount;

        if ($amount == 0) {
            throw new CRIMSGeneralException("Cannot unvalue a cheque of value 0");
        }

        $this->payment->update(['amount' => 0, 'value' => $amount]);
    }

    public function valueCheque()
    {
        if (!$this->isCheque()) {
            return;
        }

        if ($this->payment->value == 0) {
            throw new CRIMSGeneralException("Cannot value a cheque of value 0");
        }

        $this->payment->update(['amount' => $this->payment->value]);
    }

    public function bounceCheque()
    {
        if (!$this->isCheque()) {
            return;
        }
    }

    public function isCheque()
    {
        $transaction = $this->payment->custodialTransaction;

        return $transaction->repo->isCheque();
    }
}
