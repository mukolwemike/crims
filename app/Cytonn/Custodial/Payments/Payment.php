<?php
/**
 * Date: 02/03/2017
 * Time: 16:18
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Payments;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientPaymentAllowedMinimum;
use App\Cytonn\Models\ClientPaymentType;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use DateTime;

/**
 * Class Payment
 *
 * @package Cytonn\Custodial\Payments
 */
class Payment
{
    use LocksTransactions;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var CommissionRecepient
     */
    protected $recipient;

    protected $approval;

    /**
     * @var CustodialTransaction
     */
    protected $custodialTransaction;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var
     */
    protected $amount;

    /**
     * @var
     */
    protected $description;
    /**
     * @var ClientPaymentType
     */
    protected $type;

    protected $transferAllowed = 1;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var SharesEntity
     */
    private $sharesEntity;

    /**
     * @var int $minimumBalance
     */
    private $minimumBalance = 0;


    private $attemptedTransfers = 0;

    /**
     * @var UnitFund
     */
    private $fund;

    /**
     * Payment constructor.
     *
     * @param Client $client
     * @param CommissionRecepient $recipient
     * @param CustodialTransaction $custodialTransaction
     * @param Product $product
     * @param Project $project
     * @param \DateTime $date
     * @param $amount
     * @param ClientPaymentType $type
     * @param $description
     * @param SharesEntity $sharesEntity
     * @param UnitFund $fund
     */
    public function __construct(
        Client $client = null,
        CommissionRecepient $recipient = null,
        CustodialTransaction $custodialTransaction = null,
        Product $product = null,
        Project $project = null,
        SharesEntity $sharesEntity = null,
        DateTime $date,
        $amount,
        ClientPaymentType $type,
        $description,
        UnitFund $fund = null
    ) {
        $this->client = $client;
        $this->recipient = $recipient;
        $this->custodialTransaction = $custodialTransaction;
        $this->date = $date;
        $this->amount = $amount;
        $this->description = $description;
        $this->type = $type;
        $this->product = $product;
        $this->project = $project;
        $this->sharesEntity = $sharesEntity;
        $this->fund = $fund;

        $this->minimumBalance = $this->calculateMinimumBalance();

        if (($this->product && $this->project) ||
            ($this->product && $this->sharesEntity) ||
            ($this->project && $this->sharesEntity)
        ) {
            throw new \InvalidArgumentException('You cannot have more than one product/project/entity');
        }
    }


    protected function calculateMinimumBalance()
    {
        if (!$this->client) {
            return 0;
        }
        if (!$this->product && !$this->project && !$this->sharesEntity && !$this->fund) {
            return 0;
        }


        $bal = ClientPaymentAllowedMinimum::where('client_id', $this->client->id)
            ->where('date', '>', $this->date)
            ->where(
                function ($q) {
                    if ($this->product) {
                        $q->where('product_id', $this->product->id);
                    }
                    if ($this->project) {
                        $q->where('project_id', $this->project->id);
                    }
                    if ($this->sharesEntity) {
                        $q->where('entity_id', $this->sharesEntity->id);
                    }
                    if ($this->fund) {
                        $q->where('unit_fund_id', $this->fund->id);
                    }
                }
            )
            ->sum('amount');

        return -abs($bal);
    }


    /**
     * @param Client|null $client
     * @param CommissionRecepient|null $recipient
     * @param CustodialTransaction|null $custodialTransaction
     * @param Product|null $product
     * @param Project|null $project
     * @param SharesEntity $sharesEntity
     * @param DateTime $date
     * @param $amount
     * @param $type
     * @param $description
     * @param UnitFund $fund
     * @return static
     */
    public static function make(
        Client $client = null,
        CommissionRecepient $recipient = null,
        CustodialTransaction $custodialTransaction = null,
        Product $product = null,
        Project $project = null,
        SharesEntity $sharesEntity = null,
        DateTime $date,
        $amount,
        $type,
        $description,
        UnitFund $fund = null
    ) {
        if (!($type instanceof ClientPaymentType)) {
            $type = ClientPaymentType::where('slug', $type)->first();
        }

        return new static(
            $client,
            $recipient,
            $custodialTransaction,
            $product,
            $project,
            $sharesEntity,
            $date,
            $amount,
            $type,
            $description,
            $fund
        );
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'client_id' => is_null($this->client) ? null : $this->client->id,
            'custodial_transaction_id' => is_null($this->custodialTransaction) ? null : $this->custodialTransaction->id,
            'commission_recipient_id' => is_null($this->recipient) ? null : $this->recipient->id,
            'amount' => $this->amount,
            'date' => $this->date,
            'description' => $this->description,
            'type_id' => $this->type->id,
            'product_id' => is_null($product = $this->product) ? null : $product->id,
            'project_id' => is_null($project = $this->project) ? null : $project->id,
            'share_entity_id' => is_null($this->sharesEntity) ? null : $this->sharesEntity->id,
            'unit_fund_id' => is_null($this->fund) ? null : $this->fund->id,
        ];
    }

    /**
     * @return ClientPayment
     */
    public function debit()
    {
//        $this->lockForUpdate();

        $amount = abs($this->amount);

        $data = $this->toArray();
        $data['amount'] = $amount;

        $payment = ClientPayment::create($data);

        return $payment;
    }


    /**
     * @return ClientPayment
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     * @throws \Exception
     */
    public function credit()
    {
//        $this->lockForUpdate();

        $future = $this->finalDate();

        //validate that there is enough balance now and in future, only allow transfers now
        $this->validateCredit($this->date, true);

        $this->validateCredit($future, false);

        $amount = -1 * abs($this->amount);

        $data = $this->toArray();

        $data['amount'] = $amount;

        $payment = ClientPayment::create($data);

        return $payment;
    }


    /**
     * @return \DateTime
     */
    private function finalDate()
    {
        $latest = $this->client->clientPayments()->latest('date')->first();

        return is_null($latest) ? $this->date : $latest->date;
    }

    public function canCredit(\DateTime $date)
    {
    }

    /**
     * @param \DateTime $date
     * @param bool $transfer
     * @return bool
     * @throws ClientInvestmentException
     * @throws CRIMSGeneralException
     */
    private function validateCredit(\DateTime $date, $transfer)
    {
        $balance = ClientPayment::balance(
            $this->client,
            $this->product,
            $this->project,
            $this->sharesEntity,
            $date,
            $this->fund
        ) - $this->minimumBalance;

        $amount = abs($this->amount);

        if ($transfer && $this->transferAllowed) {
            if ($this->product && ($balance < $amount)) {
                $balance = $this->attemptTransferToProduct(
                    $this->product,
                    $amount - $balance
                ) - $this->minimumBalance;
            }

            if ($this->sharesEntity && ($balance < $amount)) {
                $balance = $this->attemptTransferToEntity(
                    $this->sharesEntity,
                    $amount - $balance
                ) - $this->minimumBalance;
            }

            $this->attemptedTransfers++;
        }

        if ($balance < $amount - 0.5) {
            $b = AmountPresenter::currency($balance);
            $r = AmountPresenter::currency($amount);
            $d = DatePresenter::formatDate($date);
            $c = $this->client->client_code;

            throw new CRIMSGeneralException(
                "The client does not have enough balance to make this transaction. 
                $c requested $r Balance $b as at $d"
            );
        }

        return true;
    }

    /**
     * @param Product|null $product
     * @param SharesEntity|null $entity
     * @param \DateTime|null $date
     * @return mixed
     */
    private function balance(
        Product $product = null,
        SharesEntity $entity = null,
        DateTime $date = null,
        UnitFund $fund = null
    ) {
        !is_null($date) ?: $date = $this->date;

        return ClientPayment::balance($this->client, $product, null, $entity, $date, $fund);
    }

    /**
     * @param Product $product
     * @param $amountRequired
     * @return mixed
     */
    private function attemptTransferToProduct(Product $product, $amountRequired)
    {
        $products = Product::where('fund_manager_id', $product->fund_manager_id)
            ->where('id', '!=', $product->id)
            ->where('currency_id', $product->currency_id)
            ->where('custodial_account_id', $product->custodial_account_id)
            ->get()
            ->filter(
                function (Product $product) use ($amountRequired) {
                    $balance = $this->balance($product, null, $this->finalDate());

                    return $balance >= $amountRequired;
                }
            )
            ->sortByDesc('balance');

        $entities = SharesEntity::where('fund_manager_id', $product->fund_manager_id)
            ->where('id', '!=', $product->id)
            ->where('currency_id', $product->currency_id)
            ->where('account_id', $product->custodial_account_id)
            ->get()
            ->filter(
                function (SharesEntity $entity) use ($amountRequired) {
                    $balance = $this->balance(null, $entity, $this->finalDate());

                    return $balance >= $amountRequired;
                }
            )
            ->sortByDesc('balance');

        if ($products->count() >= 1) {
            $from = $products->first();

            $this->transfer($from, null, $product, null, $amountRequired);
        } elseif ($entities->count() >= 1) {
            $from = $entities->first();

            $this->transfer(null, $from, $product, null, $amountRequired);
        }

        return $this->balance($product);
    }

    /**
     * @param SharesEntity $entity
     * @param $amountRequired
     * @return mixed
     */
    private function attemptTransferToEntity(SharesEntity $entity, $amountRequired)
    {
        $products = Product::where('fund_manager_id', $entity->fund_manager_id)
            ->where('currency_id', $entity->currency_id)
            ->where('custodial_account_id', $entity->account_id)
            ->get()
            ->filter(
                function (Product $product) use ($amountRequired) {
                    $balance = $this->balance($product, null, $this->finalDate());

                    return $balance >= $amountRequired;
                }
            )
            ->sortByDesc('balance');


        if ($products->count() >= 1) {
            $from = $products->first();

            $this->transfer($from, null, null, $entity, $amountRequired);
        }

        return $this->balance(null, $entity);
    }

    /**
     * @return static
     */
    public function disableTransfer()
    {
        $this->transferAllowed = false;
        return $this;
    }

    /**
     * @param Product $fromProduct
     * @param SharesEntity|null $fromEntity
     * @param Product|null $to
     * @param SharesEntity|null $entity
     * @param $amount
     */
    private function transfer(
        Product $fromProduct = null,
        SharesEntity $fromEntity = null,
        Product $to = null,
        SharesEntity $entity = null,
        $amount
    ) {
        $out = new static(
            $this->client,
            $this->recipient,
            null,
            $fromProduct,
            null,
            $fromEntity,
            $this->date,
            $amount,
            ClientPaymentType::where('slug', 'TO')->first(),
            $this->description
        );
        $out->disableTransfer()->credit();

        $in = new static(
            $this->client,
            $this->recipient,
            null,
            $to,
            null,
            $entity,
            $this->date,
            $amount,
            ClientPaymentType::where('slug', 'TI')->first(),
            $this->description
        );
        $in->debit();
    }

    /**
     * Prevents race conditions when creating payments for a client, which could result into negative balances
     * @throws \Exception
     */
    private function lockForUpdate()
    {
        //lock client if client is involved
        if ($this->client) {
            $this->lockModelForUpdate($this->client);
        }
    }
}
