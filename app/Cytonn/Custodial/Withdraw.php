<?php
/**
 * Date: 29/03/2017
 * Time: 15:05
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial;

use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientPayment;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Custodial\Transact\ClientTransaction;
use App\Cytonn\Models\User;
use Cytonn\Exceptions\ClientInvestmentException;

/**
 * Class Withdraw
 *
 * @package Cytonn\Custodial
 */
class Withdraw
{
    /**
     * @param Carbon                         $date
     * @param CustodialAccount               $account
     * @param $description
     * @param Client                         $client
     * @param $amount
     * @param Product|null                   $product
     * @param Project|null                   $project
     * @param SharesEntity|null              $entity
     * @param ClientTransactionApproval|null $approval
     * @param ClientPayment|null             $current_payment
     * @param User                           $sig1
     * @param User                           $sig2
     * @param ClientBankAccount              $bankAccount
     * @param ClientScheduledTransaction     $schedule
     * @return Instruction
     */
    public static function make(
        Carbon $date,
        CustodialAccount $account,
        $description,
        Client $client,
        $amount,
        Product $product = null,
        Project $project = null,
        SharesEntity $entity = null,
        ClientTransactionApproval $approval = null,
        ClientPayment $current_payment = null,
        User $sig1 = null,
        User $sig2 = null,
        ClientBankAccount $bankAccount = null,
        ClientScheduledTransaction $schedule = null,
        UnitFund $fund = null
    ) {
        return \DB::transaction(
            function () use (
                $date,
                $account,
                $description,
                $client,
                $amount,
                $product,
                $project,
                $entity,
                $approval,
                $current_payment,
                $sig1,
                $sig2,
                $bankAccount,
                $schedule,
                $fund
            ) {

                // Affect Custodial Transaction
                if ($current_payment && $current_payment->type->slug == 'TO') {
                    $transaction = $current_payment->custodialTransaction;
                    $receivingTransaction = $current_payment->child->custodialTransaction;
                } else {
                    $receivingTransaction = null;

                    $transaction = ClientTransaction::build(
                        $account,
                        'FO',
                        Carbon::parse($date),
                        $description,
                        $amount,
                        null,
                        [],
                        $client,
                        null,
                        null
                    )->setClientApproval($approval)
                        ->credit();

                    // Make payment
                    if (!$schedule) {
                        $payment = Payment::make(
                            $client,
                            null,
                            $transaction,
                            $product,
                            $project,
                            $entity,
                            $date,
                            $transaction->amount,
                            'FO',
                            'Client funds withdrawn',
                            $fund
                        )
                        ->credit();
                    } else {
                        $payment = Payment::make(
                            $client,
                            null,
                            $transaction,
                            $product,
                            $project,
                            $entity,
                            $date,
                            $transaction->amount,
                            'FO',
                            'Client funds withdrawn',
                            $fund
                        )
                        ->credit();
                    }

                    if ($current_payment) {
                        $payment->parent()->associate($current_payment);
                        $payment->save();
                    }
                }

                if (is_null($bankAccount)) {
                    $bankAccount = $client->present()->getDefaultAccount;
                }

                if (!$bankAccount->active) {
                    $message = "You cannot pay to an inactive account for client ". $client->client_code;

                    throw new ClientInvestmentException($message);
                }

                $transaction->outgoing_reference = $client->client_code. '-' . $transaction->clientPayment->id;

                $transaction->save();

                return (new Instruction())->create(
                    $transaction,
                    $receivingTransaction,
                    $sig1,
                    $sig2,
                    $bankAccount,
                    $approval
                );
            }
        );
    }
}
