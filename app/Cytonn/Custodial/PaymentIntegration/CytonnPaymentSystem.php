<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 08/11/2018
 * Time: 11:18
 */

namespace App\Cytonn\Custodial\PaymentIntegration;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\CustodialAccount;
use Carbon\Carbon;
use Cytonn\Investment\BankDetails;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Collection;

class CytonnPaymentSystem
{
    /**
     * @param Collection $collection
     * @param CustodialAccount $account
     * @return mixed
     * @throws \Exception
     */
    public function submit(Collection $collection, CustodialAccount $account)
    {
        $processed = $collection->map(function ($instruction) {
            return $this->processPayment($instruction);
        });

        $submission = [
            'account_number' => $account->account_no,
            'bank_swift_code' => $account->bank_swift,
            'transactions' => $processed->all()
        ];

        $response = $this->send(config('services.cytonn_payments.submit_payments'), $submission);

        if ((!isset($response->failed)) || (!isset($response->success))) {
            throw new \InvalidArgumentException('An error occurred');
        }

        $this->update($response->failed, 'failed');
        $this->update($response->success, 'success');

        return $response;
    }

    /**
     * @param $paybill
     * @param $phone
     * @param $amount
     * @param $reference
     * @param string $description
     * @return bool
     * @throws \Exception
     */
    public function requestMpesaPayment($paybill, $phone, $amount, $reference, $description = "Payment")
    {
        $output = $this->send('api/payments/mpesa/stk_push', [
            'paybill_no' => $paybill,
            'phone' => $phone,
            'amount' => $amount,
            'reference' => $reference,
            'description' => $description
        ]);

        if (!isset($output->status)) {
            throw new \InvalidArgumentException('Malformed STK push output - ' . json_encode($output));
        }

        return (bool)$output->status;
    }

    /**
     * @param BankInstruction $bankInstruction
     * @return array
     */
    private function processPayment(BankInstruction $bankInstruction)
    {
        $payment = $bankInstruction->custodialTransaction->clientPayment;

        if ($payment->amount > 0) {
            throw new \LogicException("Payment cannot be processed, only negative amounts will be processed");
        }

        $bank = new BankDetails(null, $payment->client, $bankInstruction->clientBankAccount);
        $cc = $payment->client ? $payment->client->client_code : '';

        return [
            'amount' => abs($payment->amount),
            'narration' => 'Client withdrawal',
            'date' => $payment->date->toDateString(),
            'reference' => $cc . '-' . $payment->id,
            'beneficiary_name' => $bank->accountName(),
            'beneficiary_account_number' => $this->processMpesaAccountNumbers(
                $bank->accountNumber(),
                $bank->swiftCode()
            ),
            'beneficiary_swift_code' => $bank->swiftCode(),
            'beneficiary_clearing_code' => $bank->clearingCode(),
            'beneficiary_email' => $payment->client->contact->email,
            'beneficiary_bank' => $bank->bankName(),
            'category' => $bank->paymentType(),
            'identifier' => $bankInstruction->identifier
        ];
    }

    private function processMpesaAccountNumbers($number, $swift)
    {
        if (strtoupper($swift) !== 'MPESA') {
            return $number;
        }

        $number = ltrim($number, '+');

        if (starts_with($number, '0')) {
            return '254' . ltrim($number, '0');
        }

        return $number;
    }


    /**
     * @param $path
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    private function send($path, array $data)
    {
        $url = config('services.cytonn_payments.url');

        $http = new Client([
            'base_uri' => $url,
            'verify' => false
        ]);


        if (cache()->has('crims_payments_access_token')) {
            $token = decrypt(cache()->get('crims_payments_access_token'));
        } else {
            $token = $this->authorize($http);
        }

        try {
            $response = $http->post($path, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $token
                ],
                'json' => $data
            ]);
        } catch (RequestException $exception) {
            $response = $exception->getResponse();
        }

        return json_decode($response->getBody()->getContents());
    }

    /**
     * @param Client $http
     * @return mixed
     * @throws \Exception
     */
    private function authorize(Client $http)
    {
        $id = config('services.cytonn_payments.client_id');
        $secret = config('services.cytonn_payments.client_secret');

        $response = $http->post('/oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $id,
                'client_secret' => $secret,
                'scope' => '*',
            ]
        ]);

        $token = json_decode((string)$response->getBody(), true)['access_token'];

//        cache()->put('crims_payments_access_token', encrypt($token), Carbon::now()->addMinutes(59));

        return $token;
    }

    private function update($items, $status)
    {
        foreach ($items as $item) {
            $id = null;

            $exploded = explode('-', $item->transaction->reference);

            if (isset($exploded[1])) {
                $id = $exploded[1];
            }

            $payment = ClientPayment::find($id);

            if ($payment) {
                $payment->update([
                    'submit_status' => $status,
                    'submitted_on' => Carbon::now(),
                    'submit_reason' => $item->reasons ? str_limit(json_encode($item->reasons), 250) : ''
                ]);
            }
        }
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function requestOutflowTransactions($data)
    {
        $path = 'api/payments/export_transactions_out';

        return $this->send($path, $data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function sendUtilityPayment($data)
    {
        $path = "/api/utilities/eclectics";

        return $this->send($path, $data);
    }
}
