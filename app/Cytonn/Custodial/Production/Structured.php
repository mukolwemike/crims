<?php
/**
 * Date: 21/03/2017
 * Time: 13:28
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Production;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\InterestPayment;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;

class Structured
{
    public function generate(Carbon $start, Carbon $end)
    {
        $start = $start->copy()->startOfDay();

        $end = $end->copy()->endOfDay();

        return FundManager::all()
            ->each(
                function (FundManager $fm) use ($start, $end) {
                    return $this->getForFundManager($fm, $start, $end);
                }
            )->filter(
                function ($fm) {
                    return $fm->p_currencies->count() > 0;
                }
            );
    }

    public function excel(FundManager $fm, Carbon $from, Carbon $to, $monthly = false)
    {
        $filename = $fm->name;

        $transactions = [];

        $excel = \Excel::create(
            $filename,
            function ($excel) use ($from, $to, $fm, &$transactions, $monthly) {
                $date = $from->copy();

                while ($date->lte($to)) {
                    $name = ($monthly) ? $date->format('F Y') : $date->toFormattedDateString();

                    $excel->sheet(
                        $name,
                        function ($sheet) use ($fm, $date, &$transactions, $monthly) {
                            $endDate = ($monthly) ? $date->copy()->endOfMonth() : $date;

                            $data = $this->getForFundManager($fm, $date, $endDate);

                            foreach ($data->p_currencies as $currency) {
                                $transactions[$currency->code][$date->toFormattedDateString()] = $currency;
                            }

                            $sheet->loadView(
                                'reports.custody.production.structured.excel_product',
                                ['fundManager' => $data]
                            );
                        }
                    );

                    $date = ($monthly) ? $date->addMonth()->startOfMonth() : $date->addDay();
                }

                $excel->sheet(
                    'Summary',
                    function ($sheet) use ($fm, $to, $transactions) {
                        $sheet->loadView(
                            'reports.custody.production.structured.excel_summary',
                            ['fundManager' => $fm, 'transactions' => $transactions]
                        );
                    }
                );
            }
        );

        return (object)[
            'file' => $excel,
            'stored_path' => storage_path() . '/exports/' . $filename . '.xlsx',
            'transactions' => $transactions
        ];
    }

    /**
     * @param FundManager $fm
     * @param Carbon $start
     * @param Carbon $end
     * @return FundManager
     */
    private function getForFundManager(FundManager $fm, Carbon $start, Carbon $end)
    {
        $currencies = Currency::all();

        $currencies = $currencies->each(
            function ($currency) use ($fm, $start, $end) {
                $currency->p_transactions = $this->inflows($start, $end, $fm, $currency);
                $currency->p_interest = $this->interestPayments($start, $end, $fm, $currency);
                $currency->p_withdrawals = $this->withdrawals($start, $end, $fm, $currency);
            }
        )->filter(
            function ($currency) {
                return $currency->p_transactions->count() > 0 ||
                    $currency->p_interest->count() > 0 ||
                    $currency->p_withdrawals->count() > 0;
            }
        );

        $fm->p_currencies = $currencies;

        return $fm;
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param FundManager $fundManager
     * @param Currency $currency
     * @return mixed
     */
    private function inflows(Carbon $start, Carbon $end, FundManager $fundManager, Currency $currency)
    {
        return CustodialTransaction::whereHas('clientPayment', function ($payment) use ($fundManager) {
            $payment->fundManager($fundManager);
        })->ofType('FI')->whereHas('custodialAccount', function ($account) use ($currency) {
            $account->where('currency_id', $currency->id);
        })->where('date', '>=', $start)->where('date', '<=', $end)->get();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param FundManager $fundManager
     * @param Currency $currency
     * @return ClientInvestmentWithdrawal[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function withdrawals(Carbon $start, Carbon $end, FundManager $fundManager, Currency $currency)
    {
        return ClientInvestmentWithdrawal::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('amount', '>', 0)
            ->where('type_id', 1)
            ->where('withdraw_type', 'withdrawal')
            ->whereHas(
                'investment',
                function ($q) use ($fundManager, $currency) {
                    $q->whereHas(
                        'product',
                        function ($product) use ($fundManager, $currency) {
                            $product->where('fund_manager_id', $fundManager->id)
                                ->where('currency_id', $currency->id);
                        }
                    );
                }
            )->get();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param FundManager $fundManager
     * @param Currency $currency
     * @return ClientInvestmentWithdrawal[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function interestPayments(Carbon $start, Carbon $end, FundManager $fundManager, Currency $currency)
    {
        return ClientInvestmentWithdrawal::where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->where('amount', '>', 0)
            ->where('type_id', 1)
            ->where('withdraw_type', 'interest')
            ->whereHas(
                'investment',
                function ($q) use ($fundManager, $currency) {
                    $q->whereHas(
                        'product',
                        function ($product) use ($fundManager, $currency) {
                            $product->where('fund_manager_id', $fundManager->id)
                                ->where('currency_id', $currency->id);
                        }
                    );
                }
            )->get();
    }
}
