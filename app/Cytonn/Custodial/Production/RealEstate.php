<?php
/**
 * Date: 21/03/2017
 * Time: 13:28
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Production;

use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Project;
use Carbon\Carbon;

class RealEstate
{
    public function generate(Carbon $start, Carbon $end)
    {
        $start = $start->copy()->startOfDay();
        $end = $end->copy()->endOfDay();

        return Project::all()
            ->each(
                function (Project $project) use ($start, $end) {
                    $project->p_transactions = $this->inflows($start, $end, $project);
                }
            )
            ->filter(
                function ($project) {
                    return $project->p_transactions->count() > 0;
                }
            );
    }

    public function excel(Carbon $start, Carbon $end, $monthly = false)
    {
        $fname = 'Real Estate';
        
        $excel = \Excel::create(
            $fname,
            function ($excel) use ($start, $end, $monthly) {
                $date = $start->copy();

                while ($date->lte($end)) {
                    $endDate = ($monthly) ? $date->copy()->endOfMonth() : $date;

                    $name = ($monthly) ? $date->format('F Y') : $date->toFormattedDateString();

                    $excel->sheet(
                        $name,
                        function ($sheet) use ($date, $endDate) {
                            $sheet->loadView(
                                'reports.custody.production.real_estate.excel_project',
                                ['projects' => $this->generate($date, $endDate)]
                            );
                        }
                    );

                    $date = ($monthly) ? $date->addMonth()->startOfMonth() : $date->addDay();
                }

                $excel->sheet(
                    'Summary',
                    function ($sheet) use ($start, $end) {
                        $sheet->loadView(
                            'reports.custody.production.real_estate.excel_summary',
                            ['projects' => $this->generate($start, $end)]
                        );
                    }
                );
            }
        );

        return (object)[
            'file'=>$excel,
            'stored_path' => storage_path().'/exports/'.$fname.'.xlsx'
        ];
    }

    private function inflows(Carbon $start, Carbon $end, Project $project)
    {
        return CustodialTransaction::whereHas(
            'clientPayment',
            function ($payment) use ($project) {
                $payment->whereHas(
                    'project',
                    function ($p) use ($project) {
                        $p->where('project_id', $project->id);
                    }
                );
            }
        )
        ->ofType('FI')
        ->where('date', '>=', $start)
        ->where('date', '<=', $end)
        ->get();
    }
}
