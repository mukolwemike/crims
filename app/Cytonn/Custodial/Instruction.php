<?php
/**
 * Date: 09/03/2017
 * Time: 19:08
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientBankBranch;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\User;

class Instruction
{
    protected $instruction;

    /**
     * Instruction constructor.
     *
     * @param $instruction
     */
    public function __construct(BankInstruction $instruction = null)
    {
        $this->instruction = $instruction;
    }


    /**
     * @param CustodialTransaction      $transaction
     * @param CustodialTransaction|null $receivingTransaction
     * @param User                      $sig1
     * @param User                      $sig2
     * @param ClientBankAccount         $clientBankAccount
     * @param ClientTransactionApproval $approval
     * @return $this
     */
    public function create(
        CustodialTransaction $transaction,
        CustodialTransaction $receivingTransaction = null,
        User $sig1 = null,
        User $sig2 = null,
        ClientBankAccount $clientBankAccount,
        ClientTransactionApproval $approval = null
    ) {
        $this->instruction = BankInstruction::create([
            'amount' => abs($transaction->amount),
            'description' => $transaction->description,
            'type' => 'withdrawal',
            'date' => $transaction->date,
            'first_signatory_id' => is_null($sig1) ? null : $sig1->id,
            'second_signatory_id' => is_null($sig2) ? null : $sig2->id,
            'client_account_id' => $clientBankAccount ? $clientBankAccount->id : null,
            'custodial_transaction_id' => $transaction->id,
            'receiving_transaction_id' => is_null($receivingTransaction) ? null : $receivingTransaction->id,
            'approval_id' => is_null($approval) ? null : $approval->id
        ]);

        return $this;
    }

    /**
     * @return mixed
     */
    public function view()
    {
        $branch = is_null($this->instruction->clientBankAccount) ? new ClientBankBranch() :
            ClientBankBranch::find($this->instruction->clientBankAccount->branch_id);

        $paymentFor = null;
        $payment = null;

        $trans = $this->instruction->custodialTransaction;
        $payment = $trans->clientPayment;
        $payment ? $paymentFor = $payment->present()->paymentFor : null;

        $fundManager = $trans->custodialAccount->fundManager;

        if (str_contains($fundManager->name, 'Seriani')) {
            $companyAddress = 'seriani';
            $logo = $fundManager->logo;
        } else {
            $companyAddress = null;
            $logo = null;
        }

        return \PDF::loadView('reports.client_bank_instruction', [
            'clientBankAccount' => $this->instruction->clientBankAccount,
            'instruction' => $this->instruction,
            'branch' => $branch,
            'paymentFor' => $paymentFor,
            'logo' => $logo,
            'companyAddress' => $companyAddress
        ])->stream();
    }

    public function update(User $sig1, User $sig2)
    {
        $this->instruction->firstSignatory()->associate($sig1);
        $this->instruction->secondSignatory()->associate($sig2);
        $this->instruction->save();

        return $this;
    }

    /**
     * @return BankInstruction
     */
    public function getInstruction()
    {
        return $this->instruction;
    }
}
