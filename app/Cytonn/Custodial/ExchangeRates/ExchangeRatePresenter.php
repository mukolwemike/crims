<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Custodial\ExchangeRates;

use Laracasts\Presenter\Presenter;

class ExchangeRatePresenter extends Presenter
{
    /*
     * Get the currency combinations
     */
    public function currencyCombination()
    {
        return $this->baseCurrency->code . '  =>  ' . $this->toCurrency->code;
    }
}
