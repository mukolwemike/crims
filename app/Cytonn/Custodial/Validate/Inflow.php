<?php
/**
 * Date: 15/03/2017
 * Time: 17:26
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Validate;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\SharesEntity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Exceptions\CRIMSGeneralException;

class Inflow
{
    public function validate(array $data)
    {
        $this->mpesa($data);
        $this->exchange($data);
    }

    /**
     * @param array $input
     * @throws CRIMSGeneralException
     */
    public function checkIfDuplicate(array $input)
    {
        foreach (['cheque_number', 'mpesa_confirmation_code'] as $key) {
            if (!isset($input[$key])) {
                $input[$key] = null;
            }
        }

        $mpesa = false;

        if ($input['mpesa_confirmation_code']) {
            $mpesa = CustodialTransaction::where('mpesa_confirmation_code', $input['mpesa_confirmation_code'])
                ->whereNotNull('mpesa_confirmation_code')
                ->exists();
        }

        $cheque = false;

        if ($input['cheque_number']) {
            $cheque = CustodialTransaction::where([
                'cheque_number' => $input['cheque_number'],
                'date' => $input['date'],
                'entry_date' => isset($input['entry_date']) ? $input['entry_date'] : $input['date'],
                'client_id' => $input['client_id'],
                'bank_reference_no' => $input['bank_reference_no'],
            ])
            ->whereNotNull('cheque_number')
            ->where('custodial_account_id', $input['custodial_account_id'])
            ->exists();
        }

        $duplicate = CustodialTransaction::where([
            'custodial_account_id' => $input['custodial_account_id'],
            'client_id' => $input['client_id'],
            'received_from' => $input['received_from'],
            'exchange_rate' => $input['exchange_rate'],
            'source' => $input['source'],
            'bank_reference_no' => $input['bank_reference_no'],
            'date' => $input['date'],
            'amount' => $input['amount'],
            'mpesa_confirmation_code' => $input['mpesa_confirmation_code']
        ])->exists();

        if ($duplicate || $mpesa || $cheque) {
            throw new CRIMSGeneralException('Duplicate! Another inflow with these details already exists!');
        }
    }

    private function mpesa(array $data)
    {
        $account = CustodialAccount::findOrFail($data['custodial_account_id']);

        if ($account->mpesaAccounts()->count() == 0) {
            if ($data['source'] == 'mpesa') {
                throw new CRIMSGeneralException('This account does not have mpesa');
            }
        }
    }

    private function exchange(array $data)
    {
        if (isset($data['product_id'])) {
            $product = Product::findOrFail($data['product_id']);
        }
        if (isset($data['project_id'])) {
            $product = Project::findOrFail($data['project_id']);
        }
        if (isset($data['entity_id'])) {
            $product = SharesEntity::findOrFail($data['entity_id']);
        }
        if (isset($data['unit_fund_id'])) {
            $product = UnitFund::findOrFail($data['unit_fund_id']);
        }

        if (!isset($product)) {
            throw new CRIMSGeneralException('A product/project/entity must be defined');
        }

        $rate = $data['exchange_rate'];
        $account = CustodialAccount::findOrFail($data['custodial_account_id']);

        if ($account->currency->id == $product->currency->id) {
            if ($rate != 1) {
                throw new CRIMSGeneralException(
                    'The product & account have the same currency, exchange rate should be 1'
                );
            }
        } else {
            if ($rate == 1) {
                throw new CRIMSGeneralException(
                    'The product & account don\'t have the same currency, exchange rate should not be 1'
                );
            }
        }

        //TODO add more validation
    }

    public function cleanPaymentTo(array $input)
    {
        if ($input['category'] == 'project') {
            Project::findOrFail($input['project_id']);
            unset($input['product_id']);
            unset($input['entity_id']);
            unset($input['unit_fund_id']);
        } elseif ($input['category'] == 'product') {
            Product::findOrFail($input['product_id']);
            unset($input['project_id']);
            unset($input['entity_id']);
            unset($input['unit_fund_id']);
        } elseif ($input['category'] == 'shares') {
            SharesEntity::findOrFail($input['entity_id']);
            unset($input['product_id']);
            unset($input['project_id']);
            unset($input['unit_fund_id']);
        } elseif ($input['category'] == 'funds') {
            UnitFund::findOrFail($input['unit_fund_id']);
            unset($input['product_id']);
            unset($input['project_id']);
            unset($input['entity_id']);
        }

        return $input;
    }
}
