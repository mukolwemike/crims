<?php
/**
 * Date: 06/03/2018
 * Time: 11:26
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialTransaction;
use Carbon\Carbon;
use Cytonn\Exceptions\CRIMSGeneralException;

class TransactionRepository
{
    protected $transaction;

    /**
     * TransactionRepository constructor.
     * @param $transaction
     */
    public function __construct(CustodialTransaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @throws CRIMSGeneralException
     */
    public function setChequeAsNotValued()
    {
        if (!$this->isCheque()) {
            return;
        }

        $amount = $this->transaction->amount;

        if ($amount == 0) {
            throw new CRIMSGeneralException("Cannot unvalue a cheque of value 0");
        }

        $this->transaction->update(['amount' => 0, 'value' => $amount]);
    }

    /**
     * @throws CRIMSGeneralException
     */
    public function valueCheque()
    {
        if (!$this->isCheque()) {
            return;
        }

        if ($this->transaction->value == 0) {
            throw new CRIMSGeneralException("Cannot value a cheque of value 0");
        }

        $this->transaction->update(['amount' => $this->transaction->value]);
    }

    /**
     * @param Carbon $date
     * @return CustodialTransaction|\Illuminate\Database\Eloquent\Model|void
     * @throws CRIMSGeneralException
     */
    public function bounceCheque(Carbon $date)
    {
        if (!$this->isCheque()) {
            return;
        }

        $this->valueCheque();

        $bouncing = $this->transaction->fresh()->toArray();
        $bouncing['bank_transaction_id'] = '[Bounced] ' . $bouncing['bank_transaction_id'];
        $bouncing['amount'] = -1 * $bouncing['amount'];
        $bouncing['date'] = $date;
        $bouncing['description'] = '[Bounced] '.$bouncing['description'];
        unset($bouncing['id']);

        return CustodialTransaction::create($bouncing);
    }

    public function isCheque()
    {
        $notCheque = is_null($this->transaction->cheque_number) || empty($this->transaction->cheque_number);

        return (!$notCheque) || $this->transaction->source == 'cheque';
    }

    public function hasBeenValued()
    {
        return $this->transaction->amount > 0;
    }
}
