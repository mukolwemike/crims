<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/8/18
 * Time: 11:24 AM
 */

namespace App\Cytonn\Custodial\Transact;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use Cytonn\Custodial\Transact\Transaction;

class UnitFundTransaction extends Transaction
{
    public function __construct(
        CustodialAccount $account,
        $type,
        \DateTime $date,
        $description,
        $amount,
        $bank_reference_no = null,
        ClientTransactionApproval $approval = null
    ) {
        $this->account = $account;
        $this->type = $type;
        $this->date = $date;
        $this->description = $description;
        $this->amount = $amount;
        $this->bank_reference_no = $bank_reference_no;
        $this->clientApproval = $approval;

        parent::__construct();
    }

    public static function build(
        CustodialAccount $account,
        $type,
        \DateTime $date,
        $description,
        $amount,
        ClientTransactionApproval $approval,
        $bank_reference_no = null
    ) {
        $trans = CustodialTransactionType::where('name', $type)->first();

        return new static($account, $trans, $date, $description, $amount, $bank_reference_no, $approval);
    }
}
