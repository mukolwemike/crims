<?php
/**
 * Date: 02/03/2017
 * Time: 11:02
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;

class PortfolioTransaction extends Transaction
{
    /**
     * ClientTransaction constructor.
     *
     * @param CustodialAccount $account
     * @param $type
     * @param \DateTime $date
     * @param $description
     * @param $amount
     * @param null $bank_reference_no
     * @param PortfolioTransactionApproval $approval
     * @param PortfolioInvestor $institution
     */
    public function __construct(
        CustodialAccount $account,
        $type,
        \DateTime $date,
        $description,
        $amount,
        $bank_reference_no = null,
        PortfolioTransactionApproval $approval = null,
        PortfolioInvestor $institution
    ) {
        $this->account = $account;
        $this->type = $type;
        $this->date = $date;
        $this->description = $description;
        $this->amount = $amount;
        $this->bank_reference_no = $bank_reference_no;
        $this->approval = $approval;
        $this->institution = $institution;

        parent::__construct();
    }


    /**
     * @param CustodialAccount $account
     * @param $type
     * @param \DateTime $date
     * @param $description
     * @param $amount
     * @param $bank_reference_no
     * @param PortfolioTransactionApproval $approval
     * @param PortfolioInvestor $institution
     * @return static
     */
    public static function build(
        CustodialAccount $account,
        $type,
        \DateTime $date,
        $description,
        $amount,
        PortfolioTransactionApproval $approval,
        PortfolioInvestor $institution,
        $bank_reference_no = null
    ) {
        $trans = CustodialTransactionType::where('name', $type)->first();

        return new static($account, $trans, $date, $description, $amount, $bank_reference_no, $approval, $institution);
    }
}
