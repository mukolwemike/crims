<?php

namespace Cytonn\Custodial\Transact;

class DepositDebit
{
    public static function make(TransactDeposits $transaction)
    {
        $t = $transaction->save();

        $t->update(['amount'=> $transaction->getDebitAmount()]);

        return $t;
    }
}
