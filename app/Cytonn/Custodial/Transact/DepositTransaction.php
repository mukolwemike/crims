<?php
/**
 * Date: 02/03/2017
 * Time: 11:02
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use DateTime;

class DepositTransaction extends TransactDeposits
{
    public function __construct(
        CustodialAccount $account,
        $type,
        DateTime $date,
        $description,
        $amount,
        $bank_reference_no = null,
        PortfolioTransactionApproval $approval = null,
        PortfolioInvestor $institution
    ) {
        $this->account = $account;
        $this->type = $type;
        $this->date = $date;
        $this->description = $description;
        $this->amount = $amount;
        $this->bank_reference_no = $bank_reference_no;
        $this->approval = $approval;
        $this->institution = $institution;

        parent::__construct();
    }

    public static function build(
        CustodialAccount $account,
        $type,
        DateTime $date,
        $description,
        $amount,
        PortfolioTransactionApproval $approval,
        PortfolioInvestor $institution,
        $bank_reference_no = null
    ) {
        $trans = (new CustodialTransactionType())->where('name', $type)->first();

        return new static($account, $trans, $date, $description, $amount, $bank_reference_no, $approval, $institution);
    }
}
