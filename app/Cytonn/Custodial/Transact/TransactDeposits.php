<?php

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialTransaction;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Workflow\Hooks\ClosingPeriods;

abstract class TransactDeposits
{
    protected $account;

    protected $type;

    protected $date;

    protected $owner;

    protected $description;

    protected $approval;

    protected $clientApproval;

    protected $amount;

    protected $client;

    protected $bank_reference_no;

    protected $recipient;

    protected $received_from;

    protected $institution;

    protected $sourceDetails = [];

    public function __construct()
    {
        if (!$this->type) {
            throw new \InvalidArgumentException('Each transaction must have a type');
        }
    }

    public function toArray()
    {
        return [
            'custodial_account_id' => $this->account->id,
            'type' => $this->type->id,
            'amount' => $this->amount,
            'description' => $this->description,
            'date' => $this->date,
            'transaction_owners_id' => is_null($this->owner) ? null : $this->owner->id,
            'approval_id' => is_null($this->approval) ? null : $this->approval->id,
            'client_id' => is_null($client = $this->client) ? null : $client->id,
            'commission_recipient_id' => is_null($this->recipient) ? null : $this->recipient->id,
            'bank_reference_no' => $this->bank_reference_no,
            'received_from' => $this->received_from,
            'portfolio_institution_id' => is_null($inst = $this->institution) ? null : $inst->id
        ];
    }

    public function getDebitAmount()
    {
        return abs($this->amount);
    }

    public function getCreditAmount()
    {
        return -1 * abs($this->amount);
    }

    public function credit()
    {
        $this->checkWorkFlow();

        return DepositCredit::make($this);
    }

    public function debit()
    {
        $this->checkWorkFlow();

        return DepositDebit::make($this);
    }

    protected function checkWorkFlow()
    {
        $approval = $this->approval ? $this->approval : $this->clientApproval;

        $is_closed = (new ClosingPeriods())->periodIsClosed($this->date, $approval);

        if ($is_closed) {
            throw new CRIMSGeneralException(
                "Could not complete transaction, the date 
                {$this->date->toFormattedDateString()} is in closed period"
            );
        }
    }

    public function save()
    {
        $data = $this->toArray();
        $data['amount'] = 0;

        $this->padSourceDetails();

        $saved = (new CustodialTransaction())->create($data);
        $saved->fill($this->sourceDetails);
        $saved->save();

        return $saved;
    }

    private function padSourceDetails()
    {
        if (!isset($this->sourceDetails['entry_date'])) {
            $this->sourceDetails['entry_date'] = $this->date;
        }
    }
}
