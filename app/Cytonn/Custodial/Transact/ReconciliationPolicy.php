<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 20/06/2019
 * Time: 18:02
 */

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialAccountBalanceTrail;
use App\Cytonn\Models\User;
use Carbon\Carbon;

class ReconciliationPolicy
{
    protected $account;

    protected $date;

    protected $user;


    public function __construct(CustodialAccount $account, Carbon $date)
    {
        $this->account = $account;

        $this->date = $date;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function check()
    {
        return true;

        $noReconNeeded = !$this->account->enforce_reconciliation;
        $isReconciled = cache()->get($this->key());
        $withinGracePeriod = !$this->gracePeriodPassed();
        $isSystemUser = $this->isSystemUser();

        if ($noReconNeeded || $isReconciled || $withinGracePeriod || $isSystemUser) {
            return true;
        }

        return $this->checkForBalance();
    }

    private function checkForBalance()
    {
        $reconciled =  $this->account->balanceTrail()
            ->where('date', $this->date->copy()->subDay())
            ->whereNotNull('reconciled_on', true)
            ->exists();

        cache()->put($this->key(), $reconciled);

        return $reconciled;
    }

    private function gracePeriodPassed()
    {
        $hours = Carbon::now()->diffInHours($this->date->copy()->startOfDay());

        return $hours >= 12;
    }

    private function key()
    {
        return 'account_reconciled_'.$this->account->id;
    }

    public function reconcile()
    {
        $balance = $this->account->balance($this->date);

        $trails = $this->account->balanceTrail()->where('date', $this->date->copy())
            ->get();

        $trails->each(function (CustodialAccountBalanceTrail $trail) use ($balance) {
            $amount = $trail->amount;

            $diff = $balance - $amount;


            if (abs($diff) < 1) {
                $trail->update(['reconciled_on' => Carbon::now(), 'delta' => $diff]);

                return;
            }

            $trail->update(['delta' => $diff]);
        });
    }

    private function isSystemUser()
    {
        if (!$this->user) {
            return false;
        }

        return $this->user->username === 'system';
    }
}
