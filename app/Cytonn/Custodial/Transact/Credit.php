<?php
/**
 * Date: 03/12/2016
 * Time: 13:27
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialTransaction;

/**
 * Class Credit
 *
 * @package Cytonn\Custodial\Transact
 */
class Credit
{
    /**
     * @param Transaction $transaction
     * @return CustodialTransaction
     */
    public static function make(Transaction $transaction)
    {
        $t = $transaction->save();

        $t->update(['amount'=> $transaction->getCreditAmount()]);

        return $t;
    }
}
