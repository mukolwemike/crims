<?php
/**
 * Date: 03/12/2016
 * Time: 13:27
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Transact;

class DepositCredit
{
    public static function make(TransactDeposits $transaction)
    {
        $t = $transaction->save();

        $t->update(['amount'=> $transaction->getCreditAmount()]);

        return $t;
    }
}
