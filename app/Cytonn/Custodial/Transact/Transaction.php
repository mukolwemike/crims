<?php
/**
 * Date: 03/12/2016
 * Time: 13:28
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\TransactionOwner;
use Carbon\Carbon;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Workflow\Hooks\ClosingPeriods;

/**
 * Class Transaction
 *
 * @package Cytonn\Custodial\Transact
 */
abstract class Transaction
{
    /**
     * @var CustodialAccount
     */
    protected $account;

    /**
     * @var CustodialTransactionType
     */

    protected $type;

    /**
     * @var Carbon
     */
    protected $date;

    /**
     * @var TransactionOwner
     */
    protected $owner;

    /*
     * @var string
     */
    /**
     * @var
     */
    protected $description;

    /**
     * @var PortfolioTransactionApproval
     */
    protected $approval;

    /**
     * @var
     */
    protected $clientApproval;

    /**
     * @var
     */
    protected $amount;

    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $bank_reference_no;

    /**
     * @var
     */
    protected $recipient;

    /**
     * @var
     */
    protected $received_from;

    /**
     * @var
     */
    protected $institution;

    /**
     * @var array
     */
    protected $sourceDetails = [];

    /**
     * Transaction constructor.
     */
    public function __construct()
    {
        if (!$this->type) {
            throw new \InvalidArgumentException('Each transaction must have a type');
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'type'                      => $this->type->id,
            'custodial_account_id'      => $this->account->id,
            'amount'                    => $this->amount,
            'description'               => $this->description,
            'date'                      => $this->date,
            'transaction_owners_id'     => is_null($this->owner) ? null : $this->owner->id,
            'approval_id'               => is_null($this->approval) ? null :$this->approval->id,
            'client_id'                 => is_null($client = $this->client) ? null : $client->id,
            'commission_recipient_id'   => is_null($this->recipient) ? null :$this->recipient->id,
            'bank_reference_no'         => $this->bank_reference_no,
            'received_from'             => $this->received_from,
            'portfolio_institution_id'  => is_null($inst = $this->institution) ? null : $inst->id
        ];
    }

    /**
     * @return number
     */
    public function getDebitAmount()
    {
        return abs($this->amount);
    }

    /**
     * @return int
     */
    public function getCreditAmount()
    {
        return -1 * abs($this->amount);
    }

    /**
     *
     * @return CustodialTransaction
     * @throws CRIMSGeneralException
     */
    public function credit()
    {
        $this->checkWorkFlow();

        return Credit::make($this);
    }

    /**
     *
     * @return CustodialTransaction
     * @throws CRIMSGeneralException
     */
    public function debit()
    {
        $this->checkWorkFlow();

        return Debit::make($this);
    }


    /**
     * @throws CRIMSGeneralException
     */
    protected function checkWorkFlow()
    {
        $approval = $this->approval ? $this->approval : $this->clientApproval;

        $is_closed = (new ClosingPeriods())->periodIsClosed($this->date, $approval);

        if ($is_closed) {
            throw new CRIMSGeneralException(
                "Could not complete transaction, the date 
                {$this->date->toFormattedDateString()} is in closed period"
            );
        }

        $reconciler = new ReconciliationPolicy($this->account, $this->date);

        if ($approval && $approval->sender) {
            $reconciler = $reconciler->setUser($approval->sender);
        }

        $isReconciled = $reconciler->check();

        if (!$isReconciled) {
            throw new CRIMSGeneralException("Account is not reconciled");
        }
    }

    /**
     * @return CustodialTransaction
     */
    public function save()
    {
        $data = $this->toArray();
        $data['amount'] = 0;

        $this->padSourceDetails();

        $saved =  CustodialTransaction::create($data);
        $saved->fill($this->sourceDetails);
        $saved->save();

        return $saved;
    }

    private function padSourceDetails()
    {
        if (!isset($this->sourceDetails['entry_date'])) {
            $this->sourceDetails['entry_date'] = $this->date;
        }
    }
}
