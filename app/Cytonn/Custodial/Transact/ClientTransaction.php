<?php
/**
 * Date: 02/03/2017
 * Time: 11:02
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\CustodialTransactionType;

class ClientTransaction extends Transaction
{
    /**
     * ClientTransaction constructor.
     *
     * @param CustodialAccount $account
     * @param $type
     * @param \DateTime $date
     * @param $description
     * @param Client|null $client
     * @param CommissionRecepient|null $recipient
     * @param $amount
     * @param $received_from
     * @param null $bank_reference_no
     * @param array $sourceDetails
     */
    public function __construct(
        CustodialAccount $account,
        $type,
        \DateTime $date,
        $description,
        Client $client = null,
        CommissionRecepient $recipient = null,
        $amount,
        $received_from = null,
        $bank_reference_no = null,
        array $sourceDetails = []
    ) {
        $this->account = $account;
        $this->type = $type;
        $this->date = $date;
        $this->description = $description;
        $this->amount = $amount;
        $this->client = $client;
        $this->recipient = $recipient;
        $this->received_from = $received_from;
        $this->bank_reference_no = $bank_reference_no;

        parent::__construct();
        $this->sourceDetails = $sourceDetails;
    }

    /**
     * @param CustodialAccount $account
     * @param $type
     * @param \DateTime $date
     * @param $description
     * @param Client|null $client
     * @param CommissionRecepient|null $recipient
     * @param $amount
     * @param null $received_from
     * @param $bank_reference_no
     * @param array $sourceDetails
     * @return static
     */
    public static function build(
        CustodialAccount $account,
        $type,
        \DateTime $date,
        $description,
        $amount,
        $bank_reference_no,
        array $sourceDetails = [],
        Client $client = null,
        CommissionRecepient $recipient = null,
        $received_from = null
    ) {
        $trans = CustodialTransactionType::where('name', $type)->first();

        return new static(
            $account,
            $trans,
            $date,
            $description,
            $client,
            $recipient,
            $amount,
            $received_from,
            $bank_reference_no,
            $sourceDetails
        );
    }

    public function setClientApproval(ClientTransactionApproval $approval = null)
    {
        $this->clientApproval = $approval;

        return $this;
    }

    public function chequeDetails()
    {
        return $this;
    }
}
