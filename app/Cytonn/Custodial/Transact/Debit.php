<?php
/**
 * Date: 03/12/2016
 * Time: 13:27
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Custodial\Transact;

use App\Cytonn\Models\CustodialTransaction;

/**
 * Class Debit
 *
 * @package Cytonn\Custodial\Transact
 */
class Debit
{
    /**
     * @param Transaction $transaction
     * @return CustodialTransaction
     */
    public static function make(Transaction $transaction)
    {
        $t = $transaction->save();

        $t->update(['amount'=> $transaction->getDebitAmount()]);

        return $t;
    }
}
