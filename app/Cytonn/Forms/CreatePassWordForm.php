<?php
/**
 * Date: 8/22/15
 * Time: 11:19 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Forms;

use Laracasts\Validation\FormValidator;

class CreatePassWordForm extends FormValidator
{
    protected $rules = [
    //        'password'=>'required|min:8|confirmed|case_diff|numbers|letters'
        'password'=>'required|min:8|confirmed|regex:/^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/'
    ];


    protected $messages = [
    //        'password.case_diff'=>'The password must contain: Uppercase & lowercase letters, numbers and symbols',
    //        'password.numbers'=>'The password must contain: Uppercase & lowercase letters, numbers and symbols'
        'password.regex'=>'The password must contain: Uppercase & lowercase letters, numbers and symbols'
    ];
}
