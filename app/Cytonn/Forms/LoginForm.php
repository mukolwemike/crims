<?php
/**
 * Date: 8/21/15
 * Time: 2:59 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Forms;

use Laracasts\Validation\FormValidator;

/**
 * Class LoginForm
 *
 * @package Cytonn\Forms
 */
class LoginForm extends FormValidator
{
    /**
     * Rules to validate the login form
     *
     * @var array
     */
    protected $rules = [
        'username'=>'required',
        'password'=>'required'
    ];
}
