<?php
/**
 * Date: 8/21/15
 * Time: 2:45 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Forms;

use Laracasts\Validation\FormValidator;

/**
 * Class RegisterUserForm
 *
 * @package Cytonn\Forms
 */
class RegisterUserForm extends FormValidator
{
    /**
     * Form validation rules for New user Registration
     *
     * @var array
     */
    protected $rules = [

    ];
}
