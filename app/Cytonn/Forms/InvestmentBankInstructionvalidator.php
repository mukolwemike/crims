<?php
/**
 * Date: 01/12/2015
 * Time: 1:08 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Forms;

use Laracasts\Validation\FormValidator;

class InvestmentBankInstructionvalidator extends FormValidator
{
    public $rules = [
        'first_signatory'=>'required',
        'second_signatory'=>'required'
    ];
}
