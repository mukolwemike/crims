<?php
/**
 * Date: 8/23/15
 * Time: 12:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Forms;

use Laracasts\Validation\FormValidator;

class OTPForm extends FormValidator
{
    protected $rules = [
        'one_time_key'=>'required'
    ];
}
