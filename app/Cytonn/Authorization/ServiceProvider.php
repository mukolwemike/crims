<?php
/**
 * Date: 04/09/2017
 * Time: 15:18
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Authorization;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->app->singleton(
            'crims.authorizer',
            function () {
                return new Authorizer();
            }
        );
    }
}
