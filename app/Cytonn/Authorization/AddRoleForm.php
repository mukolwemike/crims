<?php
/**
 * Date: 9/24/15
 * Time: 9:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Authorization;

use Laracasts\Validation\FormValidator;

class AddRoleForm extends FormValidator
{
    public $rules = [
        'name'=>'required',
        'description'=>'required'
    ];
}
