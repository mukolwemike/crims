<?php

namespace Cytonn\Authorization;

/**
 * Trait to include within the model implementing the Authorizable Interface
 *
 * Class AuthorizableImpl
 */
trait AuthorizableImpl
{
    /**
     * @return mixed
     */
    public function roles()
    {
        return $this
            ->belongsToMany(
                'Cytonn\Authorization\Role',
                'authoritaire_memberships',
                'authorizable_id'
            );
    }

    // Adds a row to the join table to make the authorizable a member of a role.

    /**
     * @param Role $role
     */
    public function addRole(Role $role)
    {
        $arr = [
            'authorizable_id' => $this->id,
            'authorizable_type' => static::class,
            'role_id' => $role->id
        ];

        if (Membership::where($arr)->exists()) {
            return;
        }

        return Membership::create($arr);
    }

    /**
     * @return mixed
     */
    public function userPermissions()
    {
        return $this
            ->belongsToMany(
                'Cytonn\Authorization\Permission',
                'authoritaire_user_permissions'
            )
            ->withTimestamps();
    }

    public function groupPermissions(callable $where = null, $remember = 0)
    {
        $roles = $this->roles;

        $identifier = 'null';

        if (!is_null($where)) {
            $identifier = md5(serializeClosure($where));
        }

        $key = 'group_permissions' . $this->username . '__with_callable' . $identifier;

        if (cache()->has($key)) {
            return cache()->get($key);
        }

        $permissions = $roles->map(
            function (Role $role) use ($remember, $where) {
                $builder = $role->permissions();

                if ($where) {
                    $builder = $builder->where($where);
                }

                return $builder->remember($remember)->get();
            }
        )->flatten();

        cache([$key => $permissions], Authorizer::REMEMBER);

        return $permissions;
    }

    public function permissions()
    {
        return $this->directPermissions();
    }

    public function directPermissions()
    {
        return $this->belongsToMany(Permission::class, 'authoritaire_user_permissions');
    }

    /**
     * @author Mwaruwa Chaka
     * @param Permission $permission
     */
    public function allow(Permission $permission)
    {
        $allowed = new UserPermission();
        $allowed->user_id = $this->id;
        $allowed->permission_id = $permission->id;
        $allowed->save();
    }


    /**
     * @return mixed
     */
    public function memberships()
    {
        return $this->morphMany('Cytonn\Authorization\Membership', 'authorizable');
    }

    /**
     * @param string[]|string $checkPermissions
     * @return bool
     */
    public function isAbleTo($permission, $args = [], $throwException = false)
    {
        return app('crims.authorizer')->authorize($permission, $args, $throwException);
    }
}
