<?php

namespace Cytonn\Authorization;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Role extends Model
{
    protected $table = 'authoritaire_roles';

    protected $fillable = [
        'name',
        'description'
    ];

    protected $with = [
        'permissions'
    ];

    //
    // These are hacks until many-to-many polymorphic relations are possible.
    //
    // https://github.com/laravel/framework/issues/1922

    // A read-only "relation" to obtain all current authorizables.
    public function authorizables()
    {

        // The problem here is that I can't `fetch()` polymorphic relations.
        // So instead I get every instance of the join table model and dereference.

        $authorizables = new Collection();


        foreach ($this->memberships as $membership) {
            if (!is_null($membership->authorizable)) {
                $authorizables[] = $membership->authorizable;
            }
        }

        return $authorizables;
    }

    // Adds a row to the join table to make the authorizable a member of a role.
    public function addAuthorizable(Authorizable $authorizable)
    {
        $membership = new Membership();
        $authorizable->memberships()->save($membership);
        $this->memberships()->save($membership);
    }

    /*
    // This is one possible imagining of many-to-many polymorphic relations.
    public function authorizables() {
    return $this
    ->morphToMany('App\authorizable',                        // Morph label.
                'authoritaire_memberships'            // Join table.
    )
    ;
    }
    */
    //
    //
    //

    public function permissions()
    {
        return $this
            ->belongsToMany(
                'Cytonn\Authorization\Permission',
                'authoritaire_role_permissions',
                'role_id',
                'permission_id'
            )
            ->withTimestamps();
    }

    public function memberships()
    {
        return $this->hasMany('Cytonn\Authorization\Membership');
    }
}
