<?php

namespace Cytonn\Authorization;

use App\Cytonn\Models\BaseModel;

class Membership extends BaseModel
{
    protected $table = 'authoritaire_memberships';

    protected $guarded = [];

    public function role()
    {
        return $this->belongsTo('Cytonn\Authorization\Role');
    }

    public function authorizable()
    {
        return $this->morphTo();
    }
}
