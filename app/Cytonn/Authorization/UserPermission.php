<?php
/**
 * Date: 9/22/15
 * Time: 11:46 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Authorization;

use App\Cytonn\Models\BaseModel;

class UserPermission extends BaseModel
{
    protected $table = 'authoritaire_user_permissions';

    protected $fillable = ['user_id', 'permission_id'];
}
