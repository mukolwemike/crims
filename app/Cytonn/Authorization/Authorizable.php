<?php

namespace Cytonn\Authorization;

use Cytonn\Authorization\Role;

/**
 * Interface models must implement to access the roles/permissions system
 *
 * Interface Authorizable
 */
interface Authorizable
{
    public function roles();

    public function addRole(Role $role);

    public function memberships();

    public function permissions();

    /**
     * @param string[]|string $checkPermissions
     * @return bool
     */
    public function isAbleTo($checkPermissions, $args = []);
}
