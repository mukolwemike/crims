<?php

namespace Cytonn\Authorization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\User;

class Permission extends BaseModel
{
    protected $table = 'authoritaire_permissions';

    protected $fillable = [
        'name',
        'description'
    ];

    public function roles()
    {
        return $this
            ->belongsToMany(
                'Cytonn\Authorization\Role',
                'authoritaire_role_permissions'
            )
            ->withTimestamps();
    }

    public function directlyAssigned()
    {
        return $this->belongsToMany(User::class, 'authoritaire_user_permissions');
    }

    public function handler()
    {
        return $this->hasOne(PermissionHandler::class, 'permission_id');
    }
}
