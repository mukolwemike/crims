<?php
/**
 * Date: 06/09/2017
 * Time: 09:49
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Authorization;

use App\Cytonn\Models\BaseModel;

class PermissionHandler extends BaseModel
{
    protected $table = 'authoritaire_permission_handlers';
}
