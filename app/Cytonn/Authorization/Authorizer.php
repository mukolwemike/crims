<?php
/**
 * Date: 9/30/15
 * Time: 3:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Authorization;

use App\Cytonn\Models\User;
use Cytonn\Exceptions\AuthorizationDeniedException;
use Illuminate\Support\Collection;

/**
 * Class Authorizer
 *
 * @package Cytonn\Authorization
 */
class Authorizer
{
    const SEPARATOR = ':';

    const REMEMBER = 3;

    const MAX_CHILDREN = 2;

    public static function make()
    {
        return new static();
    }

    /**
     * @param $action
     * @deprecated
     * @return bool
     * @throws AuthorizationDeniedException
     */
    public function checkAuthority($action)
    {
        return true;

        if (env('APP_ENV') == 'testing') {
            return true;
        }

        $user = \Auth::user();

        if (!$user) {
            return false;
        }

        if ($user->isAbleTo($action)) {
            return true;
        }

        throw(new AuthorizationDeniedException($action));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function permissions()
    {
        $user = \Auth::user();

        if (!$user) {
            return [];
        }

        if (cache()->has('user_permissions'.$user->id)) {
            return cache()->get('user_permissions'.$user->id);
        }

        $direct = $user->userPermissions()
            ->remember(static::REMEMBER)
            ->get();

        $group = $user->groupPermissions(null, static::REMEMBER);

        $permissions = (collect([])->merge($direct)->merge($group))->lists('name');

        cache(['user_permissions'.$user->id => $permissions], static::REMEMBER);

        return $permissions;
    }


    /**
     * @param $action
     * @param array $args
     * @param bool $throwException
     * @return bool
     * @throws AuthorizationDeniedException
     */
    public function authorize($action, array $args = [], $throwException = true)
    {
        if (app()->environment() == 'testing') {
            return true;
        }

        $this->handleMissing($action);

        if ($this->matchDirect($action, $args)) {
            return true;
        }

        if ($this->matchGroups($action, $args)) {
            return true;
        }

        if ($throwException) {
            throw new AuthorizationDeniedException($action);
        }

        return false;
    }

    private function matchGroups($action, $args)
    {
        /**
         * @var User $user
         */
        $user = \Auth::user();

        $where = function ($model) use ($action) {
            return $model->where('name', 'like', $action . '%');
        };

        $permissions = $user->groupPermissions($where, static::REMEMBER);

        if (!count($permissions)) {
            return false;
        }

        return $this->match($permissions, $action, $args);
    }

    private function matchDirect($action, $args)
    {
        /**
         * @var User $user
         */
        $user = \Auth::user();

        $permissions = $user->userPermissions()
            ->where('name', 'like', $action . '%')
            ->remember(static::REMEMBER)
            ->get();

        if (!count($permissions)) {
            return false;
        }

        return $this->match($permissions, $action, $args);
    }

    private function match(Collection $permissions, $action, $args)
    {
        $matched = $permissions->reduceUntil(
            function ($carry, $permission) use ($action) {
                $permission_name = $permission->name;

                $base = $permission_name;

                if (str_contains($permission, static::SEPARATOR)) {
                    list($base, $child) = explode(
                        static::SEPARATOR,
                        $permission,
                        static::MAX_CHILDREN
                    );
                }

                return $carry || ($base === $action) || ($permission_name === $action);
            },
            true,
            false
        );

        if (!$matched) {
            return false;
        }

        return $this->invokeHandler($action, $args);
    }

    private function invokeHandler($action, $args)
    {
        $permissions = explode(
            static::SEPARATOR,
            $action,
            static::MAX_CHILDREN
        );

        return collect($permissions)->reduce(
            function ($carry, $action) use ($args) {
                $permission = Permission::where('name', $action)->first();

                $verdict = true;

                if ($permission && $permission->handler) {
                    $class = $permission->handler->class_name;

                    $obj = app($class);

                    $verdict = $obj->handle($args);
                }

                return $carry && $verdict;
            },
            true
        );
    }

    private function handleMissing($action)
    {
        if (strlen($action) < 3) {
            throw new \InvalidArgumentException("Action name must be at least 3 characters, given $action");
        }

        //create permission if not already defined
        if (!Permission::where('name', $action)->exists()) {
            Permission::create(
                [
                    'name' => $action
                ]
            );
        }
    }

    public function users(Permission $permission)
    {
        $direct = $permission->directlyAssigned;

        $permission->roles->each(function (Role $role) use (&$direct) {
            $direct = $direct->merge($role->authorizables());
        });

        return $direct;
    }
}
