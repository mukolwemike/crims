<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/15/18
 * Time: 12:44 PM
 */

namespace App\Cytonn\Dashboard;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\FundManager;

class ClientDashboardGenerator extends Dashboard
{
    public function activeClients(FundManager $manager)
    {
        return $manager->repo->activeClientsQuery()->remember(2)->count();
    }
}
