<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/18/18
 * Time: 12:27 PM
 */

namespace App\Cytonn\Dashboard;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Unitization\Trading\Client\Performance;
use Cytonn\Core\DataStructures\Carbon;

class DashboardChartsSummary extends Dashboard
{


    /**
     * @param FundManager $fundManager
     * @return mixed
     * @throws \Exception
     */
    public function fundValuation(FundManager $fundManager)
    {
        $months = $this->getMonths();

        if (Carbon::today()->day !== 1) {
            array_push($months, Carbon::today());
        }

        $key = 'dashboard_fund_valuation_for_fm_'. $fundManager->id;

        return $this->cache($key, function () use ($fundManager, $months, $key) {
            $valuation = $fundManager->unitFunds->map(function ($fund) use ($months) {
                return [
                    'name' => $fund->short_name,
                    'data' => $this->fundMonthlyPerformance($fund, $months)
                ];
            });

            return $this->saveToCache($key, $valuation, Carbon::today()->endOfDay());
        });
    }

    private function getMonths()
    {
        return Carbon::monthsBetweenArray(
            Carbon::today()->subMonths(12)->startOfMonth(),
            Carbon::today()->startOfMonth()
        );
    }

    /**
     * @param UnitFund $fund
     * @param $months
     * @return array
     * @throws \Exception
     */
    private function fundMonthlyPerformance(UnitFund $fund, $months)
    {
        foreach ($months as $month) {
            $label = $month->copy()->startOfDay()->eq(Carbon::today()->startOfDay())
            ? 'Today'
            : $month->copy()->format('M-Y');

            $key = 'aum_for_fund_id_'.$fund->id.'_on__'.$month->toDateString();
            $now = Carbon::now();

            if (php_sapi_name() == 'cli') {
                print('Started key '.$key.PHP_EOL);
            }

            if (cache()->has($key)) {
                $data[$label] = cache()->get($key);
            } else {
                $data[$label] = (new Performance($fund, $month->copy()))->aum();

                cache([$key => $data[$label]], Carbon::now()->addHours(72));
            }

            if (php_sapi_name() == 'cli') {
                $secs = Carbon::now()->diffInSeconds($now->copy());
                print("Completed key after $secs secs ".$key.PHP_EOL);
            }
        }

        return $data;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function reValuationChart()
    {
        $key = 'real_estate_valuation_chart';

        return $this->cache($key, function () use ($key) {
            $booked = [
                'name' => 'Scheduled Payments',
                'data' => []
            ];

            $paid = [
                'name' => 'Paid',
                'data' => []
            ];

            Project::all()->each(function ($project) use (&$booked, &$paid) {
                $active = function ($holding) use ($project) {
                    return $holding->active()->inProjectIds([$project->id]);
                };

                $payments = RealEstatePayment::whereHas('holding', $active)->sum('amount');

                $scheduled = RealEstatePaymentSchedule::inPricing()->whereHas('holding', $active)->sum('amount');

                $booked['data'][$project->name] = $scheduled;

                $paid['data'][$project->name] = $payments;
            });

            return $this->saveToCache($key, [$booked, $paid], Carbon::today()->endOfDay());
        });
    }
}
