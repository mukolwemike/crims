<?php
/**
 * Date: 29/11/2015
 * Time: 11:25 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Dashboard;

use App\Cytonn\Dashboard\Dashboard;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Unitization\Trading\Client\Performance;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;

class DashboardGenerator extends Dashboard
{


    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    /**
     * @param FundManager $fm
     * @return array|void
     * @throws \Exception
     */
    public function clients(FundManager $fm)
    {
        $key = 'dashboard_clients_count_for_'.$fm->id;

        $date = Carbon::today();

        return $this->cache($key, function () use ($key, $date, $fm) {
            $date = Carbon::parse($date);

            return $this->saveToCache($key, [
                'ytd' => $this->activeClients($fm)->count(),
                'today' => $this->activeClients($fm)->where('created_at', '>=', $date->startOfDay())->count()
            ]);
        });
    }

    public function activeClients(FundManager $fundManager)
    {
        return $fundManager->repo->activeClientsQuery();
    }


    /**
     * @param FundManager $fundManager
     * @return array|mixed
     * @throws \Exception
     */
    public function withdrawals(FundManager $fundManager)
    {
        $key = 'dashboard_withdrawals_for_fm-'.$fundManager->id;

        return $this->cache($key, function () use ($key, $fundManager) {
            $date = Carbon::today();

            $convertor = function (UnitFundSale $sale) {
                $price = $sale->price;

                if (!$price) {
                    $price = $sale->fund->unitPrice(Carbon::parse($sale->date));
                }

                return convert($sale->number * $price, $sale->fund->currency, $sale->date);
            };

            $ytd = $this->investmentWithdrawals($fundManager, Carbon::today()->startOfYear(), Carbon::today())
                ->get()
                ->sum(function ($withdrawal) {
                    return convert($withdrawal->amount, $withdrawal->investment->product->currency);
                });

            $ytd_fund = $this->fundWithdrawals($fundManager, Carbon::today()->startOfYear(), Carbon::today())
                ->get()
                ->sum($convertor);

            $today = $this->investmentWithdrawals($fundManager, Carbon::today())
                ->get()
                ->sum(function ($withdrawal) use ($date) {
                    return convert($withdrawal->amount, $withdrawal->investment->product->currency, $date->copy());
                });

            $today_fund = $this->fundWithdrawals($fundManager, Carbon::today())
                ->get()
                ->sum($convertor);

            return $this->saveToCache($key, [
                'ytd' => $ytd + $ytd_fund,
                'today' => $today + $today_fund
            ]);
        });
    }

    public function investmentWithdrawals(FundManager $fundManager, Carbon $start, Carbon $end = null)
    {
        $start = $start->copy()->startOfDay();

        if (!$end) {
            $end = $start->copy();
        }

        $end = $end->endOfDay();

        return ClientInvestmentWithdrawal::ofType('sent_to_client')
            ->where('withdraw_type', 'withdrawal')
            ->where('amount', '>', 0)
            ->doesntHave('reinvestedTo')
            ->where('date', '>=', $start)
            ->where('date', '<=', $end)
            ->whereHas('investment', function ($inv) use ($fundManager) {
                $inv->fundManager($fundManager);
            });
    }

    public function fundWithdrawals(FundManager $fundManager, Carbon $start, Carbon $end = null)
    {
        $start = $start->copy()->startOfDay();

        if (!$end) {
            $end = $start->copy();
        }

        $end = $end->endOfDay();

        return UnitFundSale::whereHas('fund', function ($fund) use ($fundManager) {
                $fund->where('fund_manager_id', $fundManager->id);
        })
            ->where('date', '>=', $start)
            ->where('date', '<=', $end);
    }


    /**
     * @param FundManager $fundManager
     * @return mixed
     * @throws \Exception
     */
    public function inflows(FundManager $fundManager)
    {
        $key = 'dashboard_inflows_fm'.$fundManager->id;

        return $this->cache($key, function () use ($key, $fundManager) {
            return $this->saveToCache($key, [
                'ytd' => $this->inflowsYTD($fundManager),
                'today' => $this->inflowsToday($fundManager)
            ]);
        });
    }

    /**
     * @param FundManager $fundManager
     * @param Carbon|null $start
     * @return mixed
     */
    public function investmentInflows(FundManager $fundManager, Carbon $start = null)
    {
        $date = Carbon::today();

        $trans = CustodialTransaction::whereHas('clientPayment', function ($payment) use ($fundManager) {
            $payment->whereHas('product', function ($product) use ($fundManager) {
                $product->where('fund_manager_id', $fundManager->id);
            })
            ->orWhereHas('fund', function ($fund) use ($fundManager) {
                $fund->where('fund_manager_id', $fundManager->id);
            });
        })->before($date)->ofType('FI');

        if ($start) {
            $trans = $trans->where('date', '>=', $start);
        }

        return $trans;
    }

    /**
     * @param FundManager $fundManager
     * @return int
     * @throws \Exception
     */
    private function inflowsYTD(FundManager $fundManager)
    {
        return $this->investmentInflows($fundManager, Carbon::today()->startOfYear())
                ->get()
                ->sum(function ($inflow) {
                    return convert($inflow->amount, $inflow->custodialAccount->currency);
                });
    }

    /**
     * @param FundManager $fundManager
     * @return mixed
     * @throws \Exception
     */
    private function inflowsToday(FundManager $fundManager)
    {
        return $this->investmentInflows($fundManager, Carbon::today())
                ->get()
                ->sum(function ($inflow) {
                    return convert($inflow->amount, $inflow->custodialAccount->currency, $inflow->date);
                });
    }

    /**
     * @param FundManager $fundManager
     * @return mixed
     * @throws \Exception
     */
    public function aum(FundManager $fundManager)
    {
        $key = 'aum_dashboard_for__fm'.$fundManager->id;

        return $this->cache($key, function () use ($key, $fundManager) {
            $aum = $fundManager->unitFunds->sum(function ($fund) {
                return (new Performance($fund, Carbon::today()))->aum();
            });

            $clients = $fundManager->repo->countActiveClients();

            return $this->saveToCache($key, [
                'total' => $aum,
                'average' => $clients == 0 ? 0 :$aum/$clients,
            ]);
        });
    }

    public function getActiveClientInvestmentCount()
    {
        return ClientInvestment::active()->remember(2)->count();
    }

    public function getActivePortfolioInvestmentCount()
    {
        return DepositHolding::active()->remember(2)->count();
    }

    public function getClientTransactionsAwaitingApprovalCount()
    {
        return ClientTransactionApproval::where(function ($q) {
            $q->where('approved', null)->orWhere('approved', 0);
        })->remember(2)->count();
    }

    public function getPortfolioTransactionsAwaitingApprovalCount()
    {
        return PortfolioTransactionApproval::where('approved', '!=', 1)->remember(2)->count();
    }
}
