<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/15/18
 * Time: 9:00 AM
 */

namespace App\Cytonn\Dashboard;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\ShareHolder;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Cytonn\Models\SharesSalesOrder;

class SharesDashboardGenerator extends Dashboard
{
    public function shareHoldersCount(FundManager $manager)
    {
        $key = 'dashboard_shareholder_count';

        return $this->cache($key, function () use ($manager) {
            return ShareHolder::whereHas('entity', function ($entity) use ($manager) {
                $entity->where('fund_manager_id', $manager->id);
            })
            ->remember(2)
            ->get()
            ->filter(function ($holder) {
                return $holder->currentShares() == 0;
            })->count();
        });
    }

    public function salesOrdersCount(FundManager $manager)
    {
        return SharesSalesOrder::matched(false)->cancelled(false)->forFundManager()->remember(2)->count();
    }

    public function purchaseOrdersCount(FundManager $manager)
    {
        return SharesPurchaseOrder::matched(false)->cancelled(false)->forFundManager()->remember(2)->count();
    }
}
