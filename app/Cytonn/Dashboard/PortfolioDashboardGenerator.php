<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/14/18
 * Time: 4:28 PM
 */

namespace App\Cytonn\Dashboard;

use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;

class PortfolioDashboardGenerator extends Dashboard
{
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function portfolioInvestments(FundManager $manager)
    {
        return DepositHolding::active()->forFundManager($manager)->remember(2)->count();
    }

    public function transactions(FundManager $manager)
    {
        return PortfolioTransactionApproval::notApproved()
            ->where('fund_manager_id', $manager->id)
            ->remember(2)
            ->count();
    }
}
