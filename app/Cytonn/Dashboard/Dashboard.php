<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 19/07/2018
 * Time: 14:17
 */

namespace App\Cytonn\Dashboard;

use Carbon\Carbon;

abstract class Dashboard
{
    const CACHE_FOR = 360;

    protected $forceUpdate = false;

    protected $sync = false;


    public function runInSync($mode = true)
    {
        $this->sync = $mode;

        return $this;
    }

    /**
     * @param $key
     * @param \Closure $getter
     * @return mixed
     * @throws \Exception
     */
    public function cache($key, \Closure $getter)
    {
        if (cache()->has($key) && (!$this->forceUpdate)) {
            return cache()->get($key);
        }

        if ($this->sync) {
            config()->set('queue.default', 'sync');
        }

        return sendToQueue($getter, $key, $this->forceUpdate, config('queue.priority.low'));
    }

    /**
     * @param $key
     * @param $value
     * @param Carbon|null $timeout
     * @return mixed
     * @throws \Exception
     */
    protected function saveToCache($key, $value, Carbon $timeout = null)
    {
        $t = $timeout ? $timeout : Carbon::now()->addMinute(static::CACHE_FOR);

        cache([$key => $value], $t);

        return $value;
    }

    /**
     * @param bool $forceUpdate
     * @return static
     */
    public function setForceUpdate(bool $forceUpdate)
    {
        $this->forceUpdate = $forceUpdate;
        return $this;
    }
}
