<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/14/18
 * Time: 8:57 AM
 */

namespace App\Cytonn\Dashboard;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Cytonn\Authorization\Authorizer;

class InvestmentsDashboardGenerator extends Dashboard
{
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function applicationsCount(FundManager $manager)
    {
        return ClientInvestmentApplication::forToday()->forFundManager()->remember(30)->count();
    }

    public function activeSPInvestments(FundManager $manager)
    {
        return ClientInvestment::active()->remember(30)->count();
    }

    public function activeUTFInvestments(FundManager $manager)
    {
        return UnitFundPurchase::active()->remember(30)->count();
    }

    public function clientInstructions(FundManager $manager)
    {
        $appCount = $this->appInstructionCount($manager);

        $rolloverCount = $this->rolloverCount($manager);

        $topUpCount = $this->topupCount($manager);

        $utfInstructions = $this->utfInstructionsCount($manager);

        $sum = $appCount + $rolloverCount + $topUpCount + $utfInstructions;

        return $sum;
    }

    public function appInstructionCount()
    {
        return ClientFilledInvestmentApplication::whereDoesntHave('application')->remember(30)->count();
    }

    public function rolloverCount(FundManager $manager)
    {
        return ClientInvestmentInstruction::whereDoesntHave('approval')
            ->whereHas('investment', function ($investment) use ($manager) {
                $investment->whereHas('product', function ($product) use ($manager) {
                    $product->where('fund_manager_id', $manager->id);
                });
            })->remember(30)->count();
    }

    public function topupCount(FundManager $manager)
    {
        return ClientTopupForm::whereDoesntHave('approval')
            ->whereHas('investment', function ($investment) use ($manager) {
                $investment->whereHas('product', function ($product) use ($manager) {
                    $product->where('fund_manager_id', $manager->id);
                });
            })->remember(30)->count();
    }

    public function utfInstructionsCount(FundManager $manager)
    {
        return UnitFundInvestmentInstruction::whereDoesntHave('approval')
            ->whereHas('unitFund', function ($fund) use ($manager) {
                $fund->where('fund_manager_id', $manager->id);
            })->remember(30)->count();
    }
}
