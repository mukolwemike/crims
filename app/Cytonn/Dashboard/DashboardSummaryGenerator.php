<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/18/18
 * Time: 8:48 AM
 */

namespace App\Cytonn\Dashboard;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Authorization\Authorizer;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Dashboard\DashboardGenerator;
use Cytonn\Models\ClientInvestmentWithdrawal;

class DashboardSummaryGenerator extends Dashboard
{
    public function __construct()
    {
        $this->authorizer = new Authorizer();
    }

    public function todayWithdrawals(FundManager $fundManager)
    {
        return (new DashboardGenerator())->investmentWithdrawals($fundManager, Carbon::today());
    }

    /**
     * @param FundManager $fundManager
     * @return mixed
     * @throws \Exception
     */
    public function matureWithdrawals(FundManager $fundManager)
    {
        $key = 'mature_withdrawals_for__'.$fundManager->id;

        return $this->cache($key, function () use ($key, $fundManager) {
            $today = Carbon::today();

            $convertor = function (UnitFundSale $sale) {
                $price = $sale->price;

                if (!$price) {
                    $price = $sale->fund->unitPrice(Carbon::parse($sale->date));
                }

                return convert($sale->number * $price, $sale->fund->currency, $sale->date);
            };

            $fw = (new DashboardGenerator())->fundWithdrawals($fundManager, Carbon::today())
                ->get()
                ->sum($convertor);

            $wd = $this->todayWithdrawals($fundManager)
                ->whereHas('investment', function ($investment) use ($today) {
                    $investment->where('maturity_date', '<=', $today);
                })->get()
                ->sum(function (ClientInvestmentWithdrawal $withdrawal) {
                    return convert(
                        $withdrawal->amount,
                        $withdrawal->investment->product->currency,
                        $withdrawal->date
                    );
                });

            return $this->saveToCache(
                $key,
                $fw + $wd
            );
        });
    }

    /**
     * @param FundManager $fundManager
     * @throws \Exception
     */
    public function prematureWithdrawals(FundManager $fundManager)
    {
        $key = 'premature_withdrawals_for_-_'.$fundManager->id;

        return $this->cache($key, function () use ($key, $fundManager) {
            $today = Carbon::today();

            $wd = $this->todayWithdrawals($fundManager)
                ->whereHas('investment', function ($investment) use ($today) {
                    $investment->where('maturity_date', '>', $today);
                })->get()
                ->sum(function (ClientInvestmentWithdrawal $withdrawal) {
                    return convert($withdrawal->amount, $withdrawal->investment->product->currency, $withdrawal->date);
                });

            return $this->saveToCache(
                $key,
                $wd
            );
        });
    }

    public function activeInvestments($fundManager)
    {
        $fund = UnitFundPurchase::where('unit_fund_id', $fundManager->id)->active()->count();

        return ClientInvestment::fundManager($fundManager)->active()->count() + $fund;
    }

    public function pendingInvestmentTransactions()
    {
        return ClientTransactionApproval::where(
            function ($q) {
                $q->where('approved', null)->orWhere('approved', 0);
            }
        )->count();
    }

    public function activePortfolioInvestments()
    {
        return DepositHolding::active()->count();
    }

    public function pendingPortfolioTransactions()
    {
        return PortfolioTransactionApproval::where('approved', '!=', 1)->count();
    }
}
