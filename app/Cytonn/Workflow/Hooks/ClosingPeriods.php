<?php
/**
 * Date: 13/07/2017
 * Time: 09:45
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Workflow\Hooks;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClosedPeriod;
use App\Cytonn\Models\ClosedPeriodExemption;
use App\Cytonn\Models\PortfolioTransactionApproval;
use DateTime;

class ClosingPeriods
{
    public function periodIsClosed(DateTime $date, $approval = null)
    {
        $exempted = false;

        if ($approval instanceof ClientTransactionApproval) {
            $exempted = $this->checkExemption($approval);
        }

        if ($approval instanceof PortfolioTransactionApproval) {
            $exempted = $this->checkPortfolioExemption($approval);
        }

        if ($exempted) {
            return true;
        }

        return ClosedPeriod::where('start', '<=', $date)
            ->where('end', '>=', $date)
            ->where('active', 1)
            ->exists();
    }

    protected function checkExemption(ClientTransactionApproval $approval)
    {
        return ClosedPeriodExemption::where('client_approval_id', $approval->id)->exists();
    }

    protected function checkPortfolioExemption(PortfolioTransactionApproval $approval)
    {
        return ClosedPeriodExemption::where('portfolio_approval_id', $approval->id)->exists();
    }
}
