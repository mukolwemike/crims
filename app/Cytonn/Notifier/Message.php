<?php
/**
 * Date: 08/10/2015
 * Time: 4:05 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Notifier;

/**
 * Used as a DTO for the messages
 * Class Message
 *
 * @package Cytonn\Notifier
 */
class Message
{
    /**
     * @var
     */
    public $level;

    /**
     * @var
     */
    public $message;

    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    public $overlay;

    /**
     * Message constructor.
     */
    public function __construct()
    {
        $this->overlay = false;
        $this->title = 'Notice';
    }
}
