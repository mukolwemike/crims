<?php
/**
 * Date: 08/10/2015
 * Time: 4:02 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Notifier;

use Illuminate\Support\Facades\Session;

/**
 * Class MessageFlasher
 *
 * @package Cytonn\Notifier
 */
class MessageFlasher
{
    /**
     * @param $message
     */
    public function postMessage($message)
    {
        if (Session::has('flash_notifier_message')) {
            $sess_msg = Session::get('flash_notifier_message');
        } else {
            $sess_msg = [];
        }

        array_push($sess_msg, $message);

        Session::flash('flash_notifier_message', $sess_msg);
    }
}
