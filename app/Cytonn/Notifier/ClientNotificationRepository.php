<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/10/18
 * Time: 11:20 AM
 */

namespace App\Cytonn\Notifier;

use App\Cytonn\Api\Transformers\Clients\ClientNotificationTransformer;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Cytonn\Notifier\Internal\ClientNotification;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Models\Client\ClientNotification as ClientNotificationModel;

class ClientNotificationRepository
{
    protected $notifier;

    public function __construct()
    {
        $this->notifier = new ClientNotification();
    }

    public function notifications(Client $client)
    {
        return ClientNotificationModel::orderBy('created_at', 'desc')
            ->where('client_id', $client->id)
            ->get()
            ->map(function ($notification) {
                return (new ClientNotificationTransformer())->transform($notification);
            });
    }

    public function updateNotification(ClientNotificationModel $notification)
    {
        $user = \Auth::user();

        $notification_reads = $notification->notificationReads;

        $notification_read = $notification_reads->where('client_user_id', $user->id)->first()->delete();

        if (!$notification_reads->count()) {
            $notification->update([
                'read' => true
            ]);
        }

        return $notification;
    }

    public function topUpNotification(ClientTopupForm $form, $status = true)
    {
        $action = $status ? 'sent.' : 'processed.';

        $short_description = 'Top Up Instruction ' . $action;

        $description = 'Top up instruction of '. AmountPresenter::currency($form->amount)
            . ' on ' .
            $form->product->name . ' has been '. $action;

        $target_route = '';

        $users = $form->client->clientUsers;

        $this->notifier->create([
            'client_id' => $form->client->id,
            'short_description' => $short_description,
            'description' => $description,
            'target_route' => '/investments/topup/details/'.$form->uuid,
            'by' => $form->user->id,
            'product_id' => $form->product->id
        ])
        ->subscribe($users);

        return $this;
    }

    public function rolloverNotification(ClientInvestmentInstruction $instruction, $status = true)
    {
        if (!$instruction->isPartialRollover()) {
            return $this;
        }

        $action = $status ? 'sent.' : 'processed.';

        $short_description = ucfirst($instruction->type->name) .' Instruction ' . $action;

        $description = ucfirst($instruction->type->name) .
            ' instruction of '.
            AmountPresenter::currency($instruction->repo->calculateAmountAffected())
            . ' on ' .
            $instruction->investment->product->name . ' has been '. $action;

        $users = $instruction->investment->client->clientUsers;

        $this->notifier->create([
            'client_id' => $instruction->investment->client->id,
            'short_description' => $short_description,
            'description' => $description,
            'target_route' => '/investments/'.$instruction->investment->uuid,
            'by' => $instruction->filledBy ? $instruction->filledBy->id : null,
            'product_id' => $instruction->investment->product->id
        ])
        ->subscribe($users);

        return $this;
    }

    public function schedulePaymentNotification(RealEstateInstruction $instruction, $status = true)
    {
        $action = $status ? 'sent.' : 'processed.';

        $short_description = 'Unit holding payment instruction' . $action;

        $description = 'Payment instruction of '. AmountPresenter::currency($instruction->amount)
            . ' on ' .
            $instruction->holding->unit->name . 'of project' .$instruction->holding->project->name
            .' has been '. $action;

        $target_route = '/investments/realestate/payment/'.$instruction->id;

        $users = $instruction->client->clientUsers;

        $this->notifier->create([
            'client_id' => $instruction->client->id,
            'short_description' => $short_description,
            'description' => $description,
            'target_route' => $target_route,
            'by' => $instruction->user->id,
            'project_id' => $instruction->holding->project->id
        ])
        ->subscribe($users);

        return $this;
    }
}
