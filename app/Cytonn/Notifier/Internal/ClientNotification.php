<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/10/18
 * Time: 11:07 AM
 */

namespace App\Cytonn\Notifier\Internal;

use App\Cytonn\Models\Client\ClientNotification as ClientNotify;
use App\Events\ClientUserNotificationUpdated;

class ClientNotification
{
    protected $notification;

    public function __construct(ClientNotify $notification = null)
    {
        $this->notification = $notification ? $notification : new ClientNotify();
    }

    public function create($data)
    {
        $this->notification = ClientNotify::create($data);

        return $this;
    }

    public function subscribe($users)
    {
        $this->notification->users()->attach($users);

        return $this;
    }

    public function notification()
    {
        return $this->notification;
    }
}
