<?php
/**
 * Date: 27/02/2018
 * Time: 09:49
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Notifier\Internal;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Notification as NotificationModel;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Authorization\Permission;

class Notification
{
    protected $notification;

    /**
     * Notification constructor.
     * @param $notification
     */
    public function __construct(NotificationModel $notification = null)
    {
        $this->notification = $notification ? $notification : new NotificationModel();
    }

    public function create($content, $location, $type)
    {
        $this->notification = NotificationModel::create([
            'location' => $location,
            'content' => $content,
            'type' => $type
        ]);

        return $this;
    }

    public function subscribe($users)
    {
        $this->notification->users()->attach($users);

        return $this;
    }

    public function subscribeByPermission($permission)
    {
        if (!$permission instanceof Permission) {
            $permission = Permission::where('name', $permission)->first();
        }

        $users = (new Authorizer())->users($permission);

        return $this->subscribe($users);
    }

    public function linkClientApproval(ClientTransactionApproval $approval)
    {
        $this->notification->update(['approval_id' => $approval->id]);

        return $this;
    }

    public function linkPortfolioApproval(PortfolioTransactionApproval $approval)
    {
        $this->notification->update(['portfolio_approval_id' => $approval->id]);

        return $this;
    }

    /**
     * @return NotificationModel
     */
    public function getNotification(): NotificationModel
    {
        return $this->notification;
    }
}
