<?php
/**
 * Date: 08/12/2015
 * Time: 8:35 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Notifier;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Notification as NotificationModel;
use App\Cytonn\Models\Portfolio\PortfolioOrderAllocation;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\User;
use Cytonn\Authorization\Permission;
use Cytonn\Notifier\Internal\Notification;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\InstitutionPresenter;

class NotificationRepository
{
    protected $notifier;

    /**
     * NotificationRepository constructor.
     */
    public function __construct()
    {
        $this->notifier = new Notification();
    }


    public function createNotificationForNewClientTransactionApproval($transaction)
    {
        $content = ClientPresenter::presentFullNameNoTitle($transaction->client_id) . ' '
            . ucfirst(str_replace('_', ' ', $transaction->transaction_type)) . '
             was sent for approval';
        $permission = Permission::where('name', 'client-approvals:approve')->first();

        return $this->notifier->create(
            $content,
            '/dashboard/investments/approve/' . $transaction->id,
            'client_transaction_sent_for_approval'
        )->subscribeByPermission($permission)
            ->linkClientApproval($transaction)
            ->getNotification();
    }

    public function createNotificationForPortfolioSettlementDue(PortfolioOrderAllocation $allocation)
    {
        $content = "Order allocation for " . $allocation->order->security->name . " has been confirmed for settlement";

        $permission = $this->getPermission('portfolio-orders:settle');

        $notification = $this->notifier->create(
            $content,
            '/dashboard/portfolio/orders/' . $allocation->order->id,
            'order_settlement_due'
        )->subscribeByPermission($permission)
            ->getNotification();

        $notification->update(['details->order_allocation_id' => $allocation->id]);

        return $notification;
    }

    public function readNotificationWhenPortfolioSettlementDone(PortfolioOrderAllocation $allocation)
    {
        NotificationModel::where('type', 'order_settlement_due')
            ->where('details->order_allocation_id', $allocation->id)
            ->update(['permanent_read' => true]);
    }

    public function createNotificationForPortfolioConfirmationDue(PortfolioOrderAllocation $allocation)
    {
        $content =
            "Order allocation for " . $allocation->order->security->name . " has been allocated for confirmation";

        $permission = $this->getPermission('portfolio-orders:confirm-allocation');

        $notification = $this->notifier->create(
            $content,
            '/dashboard/portfolio/orders/' . $allocation->order->id,
            'order_confirmation_due'
        )
            ->subscribeByPermission($permission)
            ->getNotification();

        $notification->update(['details->order_allocation_id' => $allocation->id]);

        return $notification;
    }

    public function readNotificationWhenPortfolioConfirmationDone(PortfolioOrderAllocation $allocation)
    {
        NotificationModel::where('type', 'order_confirmation_due')
            ->where('details->order_allocation_id', $allocation->id)
            ->update(['permanent_read' => true]);
    }

    public function createNotificationForNewPortfolioTransactionApproval($transaction)
    {
        $content = InstitutionPresenter::presentName($transaction->institution_id) . ' '
            . ucfirst(str_replace('_', ' ', $transaction->transaction_type))
            . ' was sent for approval';

        $permission = $this->getPermission('portfolio-approvals:approve');

        return $this->notifier->create(
            $content,
            '/dashboard/portfolio/approve/' . $transaction->id,
            'portfolio_transaction_sent_for_approval'
        )->subscribeByPermission($permission)
            ->linkPortfolioApproval($transaction)
            ->getNotification();
    }

    public function createNotificationWhenClientTransactionRejected(ClientTransactionApproval $transaction)
    {
        $content = ClientPresenter::presentFullNameNoTitle($transaction->client_id) . ' '
            . ucfirst(str_replace('_', ' ', $transaction->transaction_type))
            . ' was rejected';

        $permission = Permission::where('name', 'client-approvals:approve')->first();

        $users = User::whereIn('id', $transaction->steps->lists('user_id'))->get();

        $users = $users->push($transaction->sender);

        $this->notifier->create(
            $content,
            '/dashboard/investments/approve/' . $transaction->id,
            'client_approval_rejection'
        )->subscribeByPermission($permission)
            ->subscribe($users)
            ->linkClientApproval($transaction)
            ->getNotification();
    }

    public function createNotificationWhenPortfolioTransactionRejected(PortfolioTransactionApproval $transaction)
    {
        $content = InstitutionPresenter::presentName($transaction->institution_id) . ' '
            . ucfirst(str_replace('_', ' ', $transaction->transaction_type))
            . ' was rejected';

        $permission = Permission::where('name', 'portfolio-approvals:approve')->first();

        $users = User::whereIn('id', $transaction->steps->lists('user_id'))->get();

        $users = $users->push($transaction->sender);

        $this->notifier->create(
            $content,
            '/dashboard/portfolio/approve/' . $transaction->id,
            'portfolio_transaction_sent_for_approval'
        )->subscribeByPermission($permission)
            ->subscribe($users)
            ->linkPortfolioApproval($transaction)
            ->getNotification();
    }

    public function getNotificationsForUser(User $user = null)
    {
        return !is_null($user) ? $user->notifications()->latest()->get() : [];
    }

    public function getAllUnreadNotificationsForUser(User $user = null)
    {
        return is_null($user) ?
            []
            : $user->notifications()
                ->where('permanent_read', '!=', 1)
                ->remember(1)
                ->latest()
                ->get();
    }

    public function getUnreadNotificationCount($user)
    {
        return count($this->getAllUnreadNotificationsForUser($user));
    }

    public function setClientApprovalNotificationPermanentlyRead($transaction)
    {
        $notifications = NotificationModel::where(
            'location',
            '/dashboard/investments/approve/' . $transaction->id
        )->get();

        foreach ($notifications as $notification) {
            $notification->permanent_read = true;
            $notification->save();
        }
    }

    public function setClientApprovalNotificationPermanentlyReadOnResolve($transaction)
    {
        $notifications = NotificationModel::where(
            'location',
            '/dashboard/investments/approve/' . $transaction->id
        )->where('type', 'client_approval_rejection')->get();

        foreach ($notifications as $notification) {
            $notification->permanent_read = true;
            $notification->save();
        }
    }

    public function setPortfolioApprovalNotificationPermanentlyRead($transaction)
    {
        $notifications = NotificationModel::where(
            'location',
            '/dashboard/portfolio/approve/' . $transaction->id
        )->get();

        foreach ($notifications as $notification) {
            $notification->permanent_read = true;
            $notification->save();
        }
    }

    protected function getPermission($permission)
    {
        return Permission::where('name', $permission)->first();
    }
}
