<?php
/**
 * Date: 08/10/2015
 * Time: 4:04 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Notifier;

/**
 * Class FlashNotifier
 *
 * @package Cytonn\Notifier
 */
class FlashNotifier
{
    /**
     * @param $message
     */
    public static function message($message)
    {
        $msg = self::prepare($message);
        $msg->level = 'info';
        (new MessageFlasher())->postMessage($msg);
    }

    /**
     * @param $message
     */
    public static function warning($message)
    {
        $msg = self::prepare($message);
        $msg->level = 'warning';
        (new MessageFlasher())->postMessage($msg);
    }

    /**
     * @param $message
     */
    public static function error($message)
    {
        $msg = self::prepare($message);
        $msg->level = 'error';
        (new MessageFlasher())->postMessage($msg);
    }

    /**
     * @param $message
     */
    public static function success($message)
    {
        $msg = self::prepare($message);
        $msg->level = 'success';
        (new MessageFlasher())->postMessage($msg);
    }

    /**
     * @param $message
     * @param string $title
     */
    public static function overlay($message, $title = 'Notice')
    {
        $msg = self::prepare($message);
        $msg->title = $title;
        $msg->overlay = true;
        (new MessageFlasher())->postMessage($msg);
    }


    /**
     * @param $message
     * @return Message
     */
    private static function prepare($message)
    {
        $msg = new Message();
        $msg->message = $message;
        return $msg;
    }
}
