<?php
/**
 * Date: 29/11/2016
 * Time: 12:19
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Notifier\Transactions;

use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Mailers\Client\TransactionNotificationMailer;

class ClientTransactionNotifier
{
    public function build(ClientTransactionApproval $approval)
    {
        if (!$approval->client) {
            return false;
        }

        $details = [];

        if (method_exists($handler = $approval->handler(), 'details')) {
            $details = $handler->details($approval);
        }

        $mailer = new TransactionNotificationMailer();

        return $mailer->notify($approval, $details);
    }
}
