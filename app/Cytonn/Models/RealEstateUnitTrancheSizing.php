<?php

namespace App\Cytonn\Models;

/**
 * Date: 17/06/2016
 * Time: 6:03 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateUnitTrancheSizing extends BaseModel
{
    protected $table = 'realestate_unit_tranche_sizing';

    protected $guarded = ['id'];

    public function prices()
    {
        return $this->hasMany(RealEstateUnitTranchePricing::class, 'sizing_id');
    }

    public function size()
    {
        return $this->belongsTo(RealestateUnitSize::class, 'size_id');
    }

    public function tranche()
    {
        return $this->belongsTo(RealEstateUnitTranche::class, 'tranche_id');
    }

    public function projectFloor()
    {
        return $this->belongsTo(ProjectFloor::class, 'project_floor_id');
    }

    public function realEstateType()
    {
        return $this->belongsTo(RealEstateType::class, 'real_estate_type_id');
    }
}
