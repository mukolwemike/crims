<?php
/**
 * Date: 17/07/2017
 * Time: 09:09
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientUser;
use Cytonn\Cassandra\Eloquent\Model;

class CrimsClientLoginTrail extends Model
{
    protected $guarded = [];

    public function __construct(array $attrs = [])
    {
        $this->connection = 'cassandra';

        parent::__construct($attrs);
    }


    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }
}
