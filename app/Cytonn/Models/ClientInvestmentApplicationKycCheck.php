<?php

namespace App\Cytonn\Models;

/**
 * Date: 9/8/15
 * Time: 4:46 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ClientInvestmentApplicationKycCheck extends BaseModel
{
    public $table = 'client_investment_application_kyc_check';

    public $guarded = ['id'];

    /**
     * Unserialise value when retrieving
     *
     * @param  $value
     * @return mixed
     */
    public function getCheckedAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Serialise value when saving
     *
     * @param $value
     */
    public function setCheckedAttribute($value)
    {
        $this->attributes['checked'] = serialize($value);
    }
}
