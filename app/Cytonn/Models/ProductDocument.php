<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

class ProductDocument extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_documents';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'product_id',
        'document_id',
    ];

    /*
     * Relationship between a product document and a product
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /*
     * Relationship between a product document and a document
     */
    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }
}
