<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commission extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string
     */
    public $table = 'commissions';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    public function schedules()
    {
        return $this->hasMany(CommissionPaymentSchedule::class, 'commission_id');
    }

    public function clawBacks()
    {
        return $this->hasMany(CommissionClawback::class, 'commission_id');
    }

    public function scopeWrittenOff($query, $true = true)
    {
        $comparator = $true ? '=' : '!=' ;

        return $query->where('written_off', $comparator, 1);
    }

    public function scopeCurrency($commission, Currency $currency)
    {
        return $commission->whereHas(
            'investment',
            function ($investment) use ($currency) {
                $investment->whereHas(
                    'product',
                    function ($product) use ($currency) {
                        $product->where('currency_id', $currency->id);
                    }
                );
            }
        );
    }

    public function scopeRecipient($commission, CommissionRecepient $recipient)
    {
        return $commission->where('recipient_id', $recipient->id);
    }

    public function scopeRecipientSlug($commission, $slug, $truth = true)
    {
        return $commission->whereHas(
            'recipient',
            function ($recipient) use ($slug, $truth) {
                $recipient->whereHas(
                    'type',
                    function ($type) use ($slug, $truth) {
                        $comparator = $truth ? '=' : '!=';

                        is_array($slug) ?
                        $type->whereIn('slug', $slug, 'and', !$truth)
                        : $type->where('slug', $comparator, $slug);
                    }
                );
            }
        );
    }

    public function scopeCompliant($commission, $status = true)
    {
        return $commission->whereHas(
            'investment',
            function ($investment) use ($status) {
                $investment->whereHas(
                    'client',
                    function ($client) use ($status) {
                        $comparator = $status ? '=' : '!=';

                        $client->where('compliant', $comparator, true);
                    }
                );
            }
        );
    }

    public function scopeFromClient($query, Client $client)
    {
        $query->whereHas(
            'investment',
            function ($i) use ($client) {
                $i->where('client_id', $client->id);
            }
        );
    }

    public function scopeForProduct($query, Product $product)
    {
        return $query->whereHas(
            'investment',
            function ($i) use ($product) {
                $i->where('product_id', $product->id);
            }
        );
    }

    public function scopeComplianceDate($commission, $comparator, $date)
    {
        return $commission->whereHas(
            'investment',
            function ($investment) use ($comparator, $date) {
                $investment->whereHas(
                    'client',
                    function ($client) use ($comparator, $date) {
                        $client->where('compliance_date', $comparator, $date);
                    }
                );
            }
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overrideSchedules()
    {
        return $this->hasMany(CommissionPaymentOverride::class, 'commission_id');
    }
}
