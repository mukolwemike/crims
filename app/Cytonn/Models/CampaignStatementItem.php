<?php

namespace App\Cytonn\Models;

/**
 * Date: 09/03/2016
 * Time: 12:25 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class CampaignStatementItem extends BaseModel
{
    use \Nicolaslopezj\Searchable\SearchableTrait;

    /**
     * @var string
     */
    public $table = 'campaign_statement_item';

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10
        ],
        'joins' => [
            'clients' => ['campaign_statement_item.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Relationship to client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Relationship to the campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(StatementCampaign::class, 'campaign_id');
    }
}
