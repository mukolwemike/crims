<?php

namespace App\Cytonn\Models;

/**
 * Date: 04/08/2016
 * Time: 9:39 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class Backup extends BaseModel
{
    public function getFullFilename()
    {
        $storageDriver = \config('filesystems.default');

        $folder = \config("backup-manager." . $storageDriver . ".root");

        if ($storageDriver == 'local') {
            $folder = 'backups';
        }

        return $folder . '/' . $this->filename;
    }
}
