<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 23/01/2017
 * Time: 16:04
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealestateInterestAccrued extends BaseModel
{
    use SoftDeletes;

    protected $table = 'realestate_interest_accrued';

    protected $guarded = ['id'];

    public function payment()
    {
        return $this->belongsTo(RealEstatePayment::class, 'payment_id');
    }

    public function schedule()
    {
        return $this->belongsTo(RealEstatePaymentSchedule::class, 'schedule_id');
    }
}
