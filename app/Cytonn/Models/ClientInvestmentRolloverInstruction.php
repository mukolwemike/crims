<?php

namespace App\Cytonn\Models;

class ClientInvestmentRolloverInstruction extends BaseModel
{
    protected $table = 'client_investment_rollover_instructions';

    protected $guarded = ['id'];
}
