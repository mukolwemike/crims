<?php

namespace App\Cytonn\Models;

use App\Cytonn\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentSchedule extends BaseModel
{
    use SoftDeletes;

    protected $table = 'commission_payment_schedule';

    public function commission()
    {
        return $this->belongsTo(Commission::class, 'commission_id');
    }

    public function recipient()
    {
        return $this->commission->recipient;
    }

    public function pay()
    {
        $this->paid = true;
        $this->paid_at = Carbon::now();

        return $this->save();
    }

    public function investment()
    {
        return $this->commission->investment();
    }

    public function scopeInMonth($query, Carbon $month)
    {
        return $query->where(
            function ($q) use ($month) {
                $q->where('date', '<=', $month->copy()->endOfMonth())
                    ->where('date', '>=', $month->copy()->startOfMonth());
            }
        );
    }

    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function scopeBeforeMonth($query, Carbon $month)
    {
        return $query->where('date', '<', $month->copy()->startOfMonth());
    }

    public function scopeClawBack($query, $condition = true)
    {
        if ($condition) {
            return $query->where('claw_back', true);
        }

        return $query->where(
            function ($q) {
                $q->whereNull('claw_back')->orWhere('claw_back', false);
            }
        );
    }

    public function scopeClients($query, array $clients)
    {
        return $query->whereHas(
            'commission',
            function ($commission) use ($clients) {
                $commission->whereHas(
                    'investment',
                    function ($investment) use ($clients) {
                        $investment->whereIn('client_id', $clients);
                    }
                );
            }
        );
    }
}
