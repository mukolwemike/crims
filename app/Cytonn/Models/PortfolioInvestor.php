<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class PortfolioInvestor extends BaseModel
{
    use SearchableTrait, SoftDeletes;

    protected $table = 'portfolio_investors';

    protected $guarded = ['id'];

    public $repo;

    protected $searchable = [
        'columns' => [
            'portfolio_investors.name' => 10,
            'portfolio_investors.code' => 10,
        ],
        'joins' => [

        ],
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new \Cytonn\Portfolio\PortfolioInvestorRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function fundType()
    {
        return $this->belongsTo(FundType::class, 'fund_type_id');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'portfolio_investor_id');
    }

    public function securities()
    {
        return $this->hasMany(PortfolioSecurity::class, 'portfolio_investor_id');
    }

    public function deposits()
    {
        return $this->hasManyThrough(DepositHolding::class, PortfolioSecurity::class);
    }

    public function investments()
    {
        return $this->deposits();
    }
}
