<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class ShareSales
 */
class ShareSale extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'share_sales';

    protected $guarded = ['id'];

    public $repo;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5
        ],
        'joins' => [
            'clients' => ['clients.id', 'share_holdings.client_id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shareHolder()
    {
        return $this->belongsTo(ShareHolder::class, 'share_holder_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(ClientPayment::class, 'payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salesOrder()
    {
        return $this->belongsTo(SharesSalesOrder::class, 'shares_sales_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function purchaseOrder()
    {
        return $this->belongsTo(SharesPurchaseOrder::class, 'shares_purchase_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sharePurchase()
    {
        return $this->belongsTo(SharePurchases::class, 'share_purchase_id');
    }

    /**
     * @param $holding
     * @return mixed
     */
    public function scopeForFundManager($holding)
    {
        if (!$this->currentFundManager()) {
            return $holding;
        }

        return $holding->whereHas(
            'shareHolder',
            function ($holder) {
                return $holder->whereHas(
                    'entity',
                    function ($entity) {
                        $entity->where('fund_manager_id', $this->currentFundManager()->id);
                    }
                );
            }
        );
    }
}
