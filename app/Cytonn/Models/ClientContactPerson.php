<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 11/01/2016
 * Time: 11:20 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */
class ClientContactPerson extends BaseModel
{
    use SoftDeletes;

    public $table = 'client_contact_persons';

    protected $guarded = ['id'];

    public function relationship()
    {
        $this->belongsTo(ClientContactPersonRelationship::class, 'relationship_id');
    }
}
