<?php

namespace App\Cytonn\Models;

use App\Cytonn\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClawBack extends BaseModel
{
    use SoftDeletes;

    protected $table = 'commission_clawback';

    public function commission()
    {
        return $this->belongsTo(Commission::class, 'commission_id');
    }

    public function payments()
    {
        return $this->hasMany(ClawbackPayment::class, 'clawback_id');
    }

    public function scopeBetweenDates($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>', $start)->where('date', '<=', $end);
    }
}
