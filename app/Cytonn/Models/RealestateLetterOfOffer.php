<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\RealEstate\RealEstateContractVariation;
use Carbon\Carbon;
use Cytonn\Handlers\EventDispatchTrait;
use Cytonn\Mailers\RealEstate\LOOApprovalNotificationMailer;
use Cytonn\Realestate\Events\LOOHasBeenUploaded;
use Cytonn\Realestate\Sale\LetterOfOffer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 07/06/2016
 * Time: 1:07 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealestateLetterOfOffer extends BaseModel
{
    use SoftDeletes, EventGenerator, SearchableTrait, EventDispatchTrait, PresentableTrait;
    /**
     * @var string
     */
    protected $table = 'realestate_letter_of_offer';

    /*
     * Get the presenter
     */
    protected $presenter = 'Cytonn\Presenters\RealEstateLetterOfOfferPresenter';

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'advocates.name' => 10,
            'realestate_units.number' => 10,
            'realestate_unit_sizes.name' => 10,
            'realestate_unit_types.name' => 10,
            'projects.name' => 10,
            'projects.vendor' => 10,
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.lastname' => 10,
            'contacts.middlename' => 10,
        ],
        'joins' => [
            'unit_holdings' => ['realestate_letter_of_offer.holding_id', 'unit_holdings.id'],
            'realestate_units' => ['unit_holdings.unit_id', 'realestate_units.id'],
            'projects' => ['realestate_units.project_id', 'projects.id'],
            'realestate_unit_sizes' => ['realestate_units.size_id', 'realestate_unit_sizes.id'],
            'realestate_unit_types' => ['realestate_units.type_id', 'realestate_unit_types.id'],
            'clients' => ['unit_holdings.client_id', 'clients.id'],
            'contacts' => ['contacts.id', 'clients.contact_id'],
            'advocates' => ['realestate_letter_of_offer.advocate_id', 'advocates.id'],
        ],
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $dates = ['date_signed', 'date_received'];


    /**
     * Upload a letter of offer - save in db and upload the file to storage
     *
     * @param  $file_contents
     * @param  $file_ext
     * @param  $date_returned_by_client
     * @param  $date_sent_to_client
     * @return bool
     */
    public function upload($file_contents, $file_ext, $date_returned_by_client, $date_sent_to_client)
    {
        //upload file to storage
        $document = (new LetterOfOffer($this))->uploadLoo($file_contents, $file_ext);

        $this->document_id = $document->id;
        $this->date_returned_by_client = $date_returned_by_client;
        $this->date_sent_to_client = $date_sent_to_client;
        $this->uploaded_on = \Carbon\Carbon::now();

        $this->raise(new LOOHasBeenUploaded($this));
        $this->dispatchEventsFor($this);

        return $this->save();
    }


    /**
     * Get the location (folder) of the files
     *
     * @return string
     */
    public function getLocation()
    {
        $config = \config('filesystems');

        $path = 'private';

        if ($config['default'] == 's3') {
            $path = 'private';
        }

        return $path . '/loos';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advocate()
    {
        return $this->belongsTo(Advocate::class, 'advocate_id');
    }

    public function contractVariation()
    {
        return $this->hasMany(RealEstateContractVariation::class, 'loo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rejects()
    {
        return $this->hasMany(RealestateLetterOfOfferReject::class, 'loo_id');
    }

    public function scopeApproved($query, $condition = true)
    {
        if ($condition) {
            return $query->where(
                function ($q) {
                    $q->whereNotNull('pm_approved_on');
                }
            );
        }

        return $query->where(
            function ($q) {
                $q->whereNull('pm_approved_on');
            }
        );
    }

    public function scopeForProject($query, Project $project, $active = true, Carbon $date = null)
    {
        return $query->whereHas('holding', function ($q) use ($project, $active, $date) {
            $q->whereHas('unit', function ($unit) use ($project, $active, $date) {
                $unit->where('project_id', $project->id);
            });

            if ($active) {
                $date ? $q->active() : $q->activeAtDate($date);
            }
        });
    }

    /**
     * @param User $user
     */
    public function updateSent(User $user)
    {
        $this->sent_by = $user->id;
        $this->sent_on = \Carbon\Carbon::now();
        $this->save();
    }

    /**
     * json_encode payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getHistoryAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setHistoryAttribute($value)
    {
        $this->attributes['history'] = json_encode($value);
    }

    /**
     * @param User $user
     */
    public function setAuthorizationFormSendDate()
    {
        $this->authorization_sent_on = Carbon::now()->toDateString();
        $this->save();
    }

    /**
     * @return $this
     */
    public function pmApprove()
    {
        $this->update(
            [
                'pm_approval_id' => \Auth::user()->id,
                'pm_approved_on' => Carbon::now()
            ]
        );

        (new    LOOApprovalNotificationMailer())->sendEmail($this);

        return $this;
    }

    public function addHistory(Document $document)
    {
        $history = is_null($this->history) ? [] : $this->history;

        $history[] = $document->id;

        $this->history = $history;

        $this->save();
    }
}
