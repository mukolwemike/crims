<?php

namespace App\Cytonn\Models;

class Gender extends BaseModel
{
    protected $guarded = ['id'];

    protected $table = 'gender';
}
