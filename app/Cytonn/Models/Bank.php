<?php

namespace App\Cytonn\Models;

class Bank extends BaseModel
{
    protected $table = 'banks';

    protected $guarded = ['id'];
}
