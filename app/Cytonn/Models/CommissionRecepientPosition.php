<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

use Cytonn\Investment\CommissionRecipientPositions\CommissionRecipientPositionPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class CommissionRecepientPosition extends BaseModel
{
    /*
     * Get the soft deletes trait
     */
    use SoftDeletes;

    /*
     * Get the presentable trait
     */
    use PresentableTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = CommissionRecipientPositionPresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'commission_recipient_positions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commission_recipient_id',
        'recipient_type_id',
        'rank_id',
        'reports_to',
        'supervisor_rank_id',
        'start_date',
        'end_date'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = array('id', 'created_at', 'updated_at');

    /*
     * Relationship between a commissionRecipient and a commissionRecipient position
     */
    public function commissionRecipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(CommissionRecipientType::class, 'recipient_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rank()
    {
        return $this->belongsTo(CommissionRecipientRank::class, 'rank_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supervisorRank()
    {
        return $this->belongsTo(CommissionRecipientRank::class, 'supervisor_rank_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reportsTo()
    {
        return $this->belongsTo(CommissionRecepient::class, 'reports_to');
    }
}
