<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 05/04/2016
 * Time: 11:34 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ActiveStrategyHolding extends BaseModel
{
    use SoftDeletes;

    public $repo;

    /**
     * @var string
     */
    public $table = 'active_strategy_holdings';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * ActiveStrategyHolding constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->repo = new \Cytonn\Portfolio\ActiveStrategy\ActiveStrategyHoldingRepository($this);

        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function security()
    {
        return $this->belongsTo(ActiveStrategySecurity::class, 'security_id');
    }

    public function transactionCostRate()
    {
        return $this->belongsTo(ActiveStrategyTransactionCost::class, 'transaction_cost_id');
    }

    public function scopeBefore($query, \Carbon\Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }

    public static function boot()
    {
        static::created(
            function ($model) {
                try {
                    $model->transaction_cost_id = ActiveStrategyTransactionCost::latest()->first()->id;
                } catch (\Exception $e) {
                    $model->transaction_cost_id = null;
                }

                $model->save();
            }
        );
    }
}
