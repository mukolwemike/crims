<?php

namespace App\Cytonn\Models;

use App\Cytonn\Authentication\Client\AuthRepository;
use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Exceptions\AuthorizationDeniedException;
use Cytonn\Users\ClientUserPresenter;
use Cytonn\Users\UserClientRepository;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\Hash;
use Laracasts\Presenter\PresentableTrait;
use Laravel\Passport\HasApiTokens;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 15/01/2016
 * Time: 3:12 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientUser extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use SoftDeletes, Uuids, SearchableTrait, Authenticatable, Authorizable, PresentableTrait, HasApiTokens;

    /**
     * @var string
     */
    public $table = 'client_users';

    /**
     * @var array
     */
    protected $guarded = ['id', 'authy_id'];

    /**
     * @var array
     */
    protected $hidden = ['password', 'pin'];

    protected $presenter = ClientUserPresenter::class;

    /**
     * @var UserClientRepository
     */
    public $repo;

    /**
     * @var array
     */
    public $dates = ['created_at', 'updated_at', 'deleted_at', 'activation_key_created_at'];

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'client_users.id' => 10,
            'client_users.username' => 10,
            'client_users.email' => 10,
            'client_users.firstname' => 10,
            'client_users.lastname' => 10,
            'client_users.phone' => 10,
        ],
        'joins' => [
        ],
    ];

    /**
     * ClientUser constructor.
     *
     * @param $repo
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new UserClientRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function access()
    {
        return $this->hasMany(ClientUserAccess::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function financialAdvisors()
    {
        return $this->belongsToMany(
            CommissionRecepient::class,
            'fa_users',
            'user_id',
            'fa_id'
        );
    }

    public function clientSignatures()
    {
        return $this->belongsToMany(
            ClientSignature::class,
            'user_client_signatures',
            'client_user_id',
            'client_signature_id'
        )->withTimestamps();
    }

    /**
     * @param $password
     * passwords must always be hashed
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * @param $pin
     * pin must always be hashed
     */
    public function setPinAttribute($pin)
    {
        $this->attributes['pin'] = Hash::make($pin);
    }

    public function authRepo()
    {
        return new AuthRepository($this);
    }

    /**
     * @param Client $client
     * @return bool
     * @throws AuthorizationDeniedException
     */
    public function shouldAccessClient(Client $client)
    {
        $access = $this->can('access-client', $client);

        if (!$access) {
            throw new AuthorizationDeniedException;
        }

        return true;
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_user_access', 'user_id', 'client_id')
            ->wherePivot('active', 1);
    }

    /**
     * @param Client $client
     * @param bool $exception
     * @return bool
     * @throws AuthorizationDeniedException
     */
    public function isFaOrOwn(Client $client, $exception = true)
    {
        $own = $this->can('access-client', $client);

        $fa = $this->can('be-fa-for-client', $client);

        if ($own || $fa) {
            return true;
        }

        if ($exception) {
            throw new AuthorizationDeniedException;
        }

        return false;
    }

    public function hasNoClients()
    {
        return $this->access()->active()->count() == 0;
    }

    public function accessClient(Client $client)
    {
        return $this->can('access-client', $client);
    }

    public function canAccessClient(Client $client)
    {
        return $this->accessClient($client);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deviceTokens()
    {
        return $this->hasMany(ClientUserDeviceToken::class);
    }

    public function approvals()
    {
        return $this->hasMany(ClientInstructionApproval::class, 'client_user_id');
    }
}
