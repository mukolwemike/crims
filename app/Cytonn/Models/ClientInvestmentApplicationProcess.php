<?php

namespace App\Cytonn\Models;

class ClientInvestmentApplicationProcess extends BaseModel
{
    protected $table = 'client_investment_application_process';

    protected $guarded = ['id'];
}
