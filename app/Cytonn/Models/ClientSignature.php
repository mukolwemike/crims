<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class ClientSignature extends BaseModel
{
    use PresentableTrait, SoftDeletes;

    /*
     * Get the presenter
     */
    protected $presenter = 'Cytonn\Clients\ClientSignatures\ClientSignaturePresenter';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_signatures';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'joint_client_holder_id',
        'client_contact_person_id',
        'commission_recepient_id',
        'name',
        'email_address',
        'phone',
        'document_id',
    ];

    /*
     * Relationship between a client signature and a client
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /*
     * Relationship between a client signature and a joint client holder
     */
    public function jointClientHolder()
    {
        return $this->belongsTo(JointClientHolder::class, 'joint_client_holder_id');
    }

    /*
     * Relationship between a client signature and a document
     */
    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function contactPerson()
    {
        return $this->belongsTo(ClientContactPerson::class, 'client_contact_person_id');
    }

    public function relationPerson()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recepient_id');
    }

    public function scopeActive($query, $condition = true)
    {
        if ($condition) {
            return $query->where('active', 1);
        }

        return $query->where('active', 0);
    }

    public function userClientSignatures()
    {
        return $this->belongsToMany(
            ClientUser::class,
            'user_client_signatures',
            'client_signature_id',
            'client_user_id'
        )->withTimestamps();
    }

    public function isNew()
    {
        if ($this->contactPerson || $this->relationPerson || $this->jointClientHolder) {
            return false;
        }

        return true;
    }
}
