<?php

namespace App\Cytonn\Models;

/**
 * Date: 22/04/2016
 * Time: 9:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientInvestmentTransfer extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'client_investment_transfers';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Record a transfer of investment between two clients
     *
     * @param  ClientInvestment $investment
     * @param  Client $dest
     * @param  ClientTransactionApproval $approval
     * @return static
     */
    public static function createTransfer(
        ClientInvestment $investment,
        Client $dest,
        ClientTransactionApproval $approval
    ) {
        $transfer = new static();
        $transfer->investment_id = $investment->id;
        $transfer->source_id = $investment->client_id;
        $transfer->destination_id = $dest->id;
        $transfer->approval_id = $approval->id;
        $transfer->narration = $approval->payload['narration'];
        $transfer->date = $approval->payload['date'];

        $transfer->save();

        return $transfer;
    }
}
