<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use App\Cytonn\Models\RealEstate\RealestateCommissionPaymentOverride;
use App\Cytonn\Models\Shares\ShareCommissionPaymentOverride;
use App\Cytonn\Models\Unitization\UnitFundCommission;
use App\Cytonn\Models\Unitization\UnitFundCommissionOverride;
use App\Cytonn\Models\Unitization\UnitFundCommissionPayment;
use App\Cytonn\Models\Unitization\UnitFundCommissionPaymentSchedule;
use Carbon\Carbon;
use Crims\Investments\Commission\Models\Recipient;
use Crims\Investments\Commission\RecipientCalculator;
use Crims\Realestate\Commission\RecipientCalculator as ReRecipientCalculator;
use Crims\Shares\Commission\RecipientCalculator as ShareRecipientCalculator;
use Crims\Unitization\Commission\RecipientCalculator as UnitizationRecipientCalculator;
use Crims\AdditionalCommission\Commission\RecipientCalculator as AdditionalCommissionRecipientCalculator;
use Cytonn\Investment\CommissionRecipient\CommissionRecipientPresenter;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Models\Investment\CommissionRecepientUserPosition;
use Cytonn\Models\Shares\ShareCommissionPayment;
use Cytonn\Uuids\UuidTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class CommissionRecepient extends BaseModel implements Recipient
{
    use SearchableTrait, UuidTrait, SoftDeletes, PresentableTrait;

    protected $presenter = CommissionRecipientPresenter::class;

    public $table = 'commission_recepients';

    protected $guarded = ['id'];

    public $repo;

    protected $dates = ['reporting_date'];

    protected $searchable = [
        'columns' => [
            'commission_recepients.name' => 10,
            'commission_recepients.email' => 9,
            'commission_recepients.phone' => 9,
            'commission_recepients.referral_code' => 10,
        ],
        'joins' => [

        ],
    ];

    protected $fillable = [
        'uuid',
        'recipient_type_id',
        'email',
        'name',
        'phone',
        'phone_country_code',
        'department_unit_id',
        'zero_commission',
        'rank_id',
        'reports_to',
        'employee_id',
        'employee_number',
        'reporting_date',
        'account_number',
        'bank_code',
        'branch_code',
        'kra_pin',
        'active',
        'crm_id'
    ];

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new CommissionRecipientRepository($this);
    }

    public function commissions()
    {
        return $this->hasMany(Commission::class, 'recipient_id');
    }

    public function commissionPayments()
    {
        return $this->hasMany(CommissionPayment::class, 'recipient_id');
    }

    public function clientPayments()
    {
        return $this->hasMany(ClientPayment::class, 'commission_recipient_id');
    }

    public function realEstateCommissions()
    {
        return $this->hasMany(RealestateCommission::class, 'recipient_id');
    }

    public function realEstateCommissionPayments()
    {
        return $this->hasMany(RealEstateCommissionPayment::class, 'recipient_id');
    }

    public function unitFundCommissionPaymentSchedules()
    {
        return $this->hasMany(UnitFundCommissionPaymentSchedule::class, 'commission_recipient_id');
    }

    public function shareCommissionPayments()
    {
        return $this->hasMany(ShareCommissionPayment::class, 'commission_recipient_id');
    }

    public function shareCommissionPaymentSchedules()
    {
        return $this->hasMany(SharesCommissionPaymentSchedule::class, 'commission_recipient_id');
    }

    public function unitFundCommissionPayments()
    {
        return $this->hasMany(UnitFundCommissionPayment::class, 'commission_recipient_id');
    }

    /*
     * Relationship between a unit fund commission and commission recipient
     */
    public function unitFundCommissions()
    {
        return $this->hasMany(UnitFundCommission::class, 'commission_recipient_id');
    }

    public function departmentUnit()
    {
        return $this->belongsTo(DepartmentUnit::class);
    }

    public function investments()
    {
        $commissions = $this->commissions;

        $investments = new Collection();

        foreach ($commissions as $c) {
            $investments->add($c->investment);
        }

        return $investments;
    }

    public function schedules()
    {
        $commissions = $this->commissions;

        $schedules = new Collection();
        foreach ($commissions as $commission) {
            foreach ($commission->schedules as $schedule) {
                $schedules->add($schedule);
            }
        }
        return $schedules;
    }

    public function realEstateSchedules()
    {
        $commissions = $this->realEstateCommissions;

        $schedules = new Collection();
        foreach ($commissions as $commission) {
            foreach ($commission->schedules as $schedule) {
                $schedules->add($schedule);
            }
        }
        return $schedules;
    }

    public function type()
    {
        return $this->belongsTo(CommissionRecipientType::class, 'recipient_type_id');
    }

    public function rank()
    {
        return $this->belongsTo(CommissionRecipientRank::class, 'rank_id');
    }

    public function reportsTo()
    {
        return $this->belongsTo(CommissionRecepient::class, 'reports_to');
    }

    public function userAccounts()
    {
        return $this->belongsToMany(
            ClientUser::class,
            'fa_users',
            'fa_id',
            'user_id'
        );
    }

    public function calculator(Currency $currency, Carbon $start, Carbon $end)
    {
        return new RecipientCalculator($this, $currency, $start, $end);
    }

    public function reCommissionCalculator(Carbon $start, Carbon $end)
    {
        return new ReRecipientCalculator($this, $start, $end);
    }

    public function shareCommissionCalculator(Carbon $start, Carbon $end)
    {
        return new ShareRecipientCalculator($this, $start, $end);
    }

    public function unitizationCommissionCalculator(Carbon $start, Carbon $end)
    {
        return new UnitizationRecipientCalculator($this, $start, $end);
    }

    public function additionalCommissionCalculator(Carbon $start, Carbon $end, $commission = null)
    {
        return new AdditionalCommissionRecipientCalculator($this, $start, $end, $commission);
    }

    public function commissionRecipientPositions()
    {
        return $this->hasMany(CommissionRecepientPosition::class, 'commission_recipient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionRecipientUserPositions()
    {
        return $this->hasMany(CommissionRecepientUserPosition::class, 'commission_recipient_id');
    }

    public function getPreviousPosition()
    {
        return CommissionRecepientPosition::where('commission_recipient_Id', $this->id)
            ->orderBy('start_date', 'desc')
            ->skip(1)
            ->take(1)
            ->first();
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeOfType($query, $type = null)
    {
        if ($type) {
            return $query->whereHas('type', function ($recipientType) use ($type) {
                $recipientType->where('slug', $type);
            });
        }

        return $query;
    }

    public function scopeIsStaff($query, $excludeIFas = true)
    {
        if ($excludeIFas) {
            return $query->where('recipient_type_id', '!=', 3);
        }

        return $query;
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeIsFa($query, $condition = true)
    {
        if ($condition) {
            return $query->whereIn('recipient_type_id', [2, 4, 5]);
        }

        return $query->whereIn('recipient_type_id', [1, 3]);
    }

    public function reportInvestmentOverrides()
    {
        return $this->hasMany(CommissionPaymentOverride::class, 'report_id');
    }

    public function reportRealEstateOverrides()
    {
        return $this->hasMany(RealestateCommissionPaymentOverride::class, 'report_id');
    }

    public function reportShareOverrides()
    {
        return $this->hasMany(ShareCommissionPaymentOverride::class, 'report_id');
    }

    public function reportUnitFundOverrides()
    {
        return $this->hasMany(UnitFundCommissionOverride::class, 'report_id');
    }
}
