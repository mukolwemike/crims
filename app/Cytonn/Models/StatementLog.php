<?php

namespace App\Cytonn\Models;

/**
 * Date: 24/02/2016
 * Time: 2:45 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class StatementLog extends BaseModel
{
    public $table = 'statement_log';

    protected $guarded = ['id'];
}
