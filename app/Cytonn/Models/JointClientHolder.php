<?php

namespace App\Cytonn\Models;

use Cytonn\Crm\CrmSyncTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

/**
 * Date: 17/11/2015
 * Time: 10:32 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class JointClientHolder extends BaseModel
{
    use SoftDeletes;
    use CrmSyncTrait;

    /*
     * Get the presentable trait
     */
    use PresentableTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = 'Cytonn\Clients\JointClientHolders\JointClientHolderPresenter';

    public $table = 'joint_client_holders';

    protected $guarded = ['id'];

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    public function details()
    {
        return $this->belongsTo(ClientJointDetail::class, 'joint_detail_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
