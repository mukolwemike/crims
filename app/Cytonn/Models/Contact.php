<?php

namespace App\Cytonn\Models;

use Cytonn\Contacts\Events\ContactHasBeenCreated;
use Cytonn\Crm\CrmSyncTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Contact
 */
class Contact extends BaseModel
{
    /*
     * Get the presentable trait
     */
    use PresentableTrait, SoftDeletes;

    use \Laracasts\Commander\Events\EventGenerator;

    use CrmSyncTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = 'Cytonn\Contacts\ContactPresenter';

    protected $table = 'contacts';

    protected $guarded = ['id'];

    /**
     * Contact constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }


    /**
     * @return mixed
     * relationship to clients table
     */
    public function client()
    {
        return $this->hasOne(Client::class);
    }

    /**
     * @return mixed
     * find contact originator #staff
     */
    public function originator()
    {
        return $this->belongsTo(Staff::class, 'contact_originator');
    }

    /**
     * find relationship manager
     *
     * @return mixed
     */
    public function relationshipManager()
    {
        return $this->belongsTo(Staff::class, 'relationship_contact');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }

    public function title()
    {
        return $this->belongsTo(Title::class);
    }

    /*
     * Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Add a new contact from form data
     *
     * @param $contactdata
     */
    public function add($contactdata)
    {
        $this->fill($contactdata);

        $this->save();

        $this->raise(new ContactHasBeenCreated($this));

        return $this;
    }

    public function entityType()
    {
        return $this->belongsTo(ContactEntity::class, 'entity_type_id');
    }
}
