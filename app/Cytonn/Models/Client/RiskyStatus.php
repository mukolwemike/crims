<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 09/11/2018
 * Time: 16:25
 */

namespace App\Cytonn\Models\Client;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiskyStatus extends BaseModel
{
    use SoftDeletes;

    protected $table= "risky_status";

    protected $guarded=['id'];

//    public function riskyClient()
//    {
//        return $this->belongsTo(RiskyClient::class);
//    }
}
