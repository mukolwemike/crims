<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 16/10/2018
 * Time: 15:19
 */

namespace App\Cytonn\Models\Client;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsentForm extends BaseModel
{
    use SoftDeletes;

    protected $table = 'consent_form';

    protected $guarded = ['id'];

    /**
     * @return bool
     */
    public function status()
    {
        $status = (bool)$this->deactivated ? false : true;

        return $status;
    }
}
