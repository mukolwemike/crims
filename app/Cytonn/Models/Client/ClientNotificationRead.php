<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/10/18
 * Time: 11:01 AM
 */

namespace App\Cytonn\Models\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientNotificationRead extends Model
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'client_notification_reads';

    protected $guarded = ['id'];
}
