<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 09/07/2019
 * Time: 12:13
 */

namespace App\Cytonn\Models\Client;


use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;

class ClientSetting extends BaseModel
{

    protected $table = 'client_settings';

    protected $guarded = ['id'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}