<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 10/06/2019
 * Time: 10:47
 */

namespace App\Cytonn\Models\Client\Kyc;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientJointDetail;

class IprsRequest extends BaseModel
{
    protected $guarded = ['id'];

    public function validation()
    {
        return $this->belongsTo(IprsValidation::class, 'validation_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function joint()
    {
        return $this->belongsTo(ClientJointDetail::class, 'joint_id');
    }
}
