<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 10/06/2019
 * Time: 10:48
 */

namespace App\Cytonn\Models\Client\Kyc;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\DocumentType;
use App\Cytonn\Models\Gender;
use Cytonn\Core\Storage\StorageInterface;

class IprsValidation extends BaseModel
{
    protected $guarded = ['id'];

    public function clients()
    {
        return $this->belongsToMany(
            Client::class,
            'client_iprs_validations',
            'iprs_id',
            'client_id'
        );
    }

    public function joints()
    {
        return $this->belongsToMany(
            ClientJointDetail::class,
            'client_iprs_validations',
            'iprs_id',
            'joint_id'
        );
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }

    public function requests()
    {
        return $this->hasMany(IprsRequest::class, 'validation_id');
    }

    public function imageAsData()
    {
        if (!$this->photo_filename) {
            return null;
        }

        $type = DocumentType::where('slug', 'iprs_files')->remember(1000)->first();

        $provider = app()->make(StorageInterface::class);

//        $fs = $provider->filesystem();

        $fullPath = $type->path().'/'.$this->photo_filename;

        $data = $provider->get($fullPath);

        $base64 = base64_encode($data);

        return 'data:'.\Storage::mimeType($fullPath) . ';base64,' . $base64;
    }
}
