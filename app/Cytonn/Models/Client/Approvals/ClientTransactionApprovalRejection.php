<?php
/**
 * Date: 01/02/2018
 * Time: 14:25
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models\Client\Approvals;

use App\Cytonn\BaseModel;

class ClientTransactionApprovalRejection extends BaseModel
{
    protected $guarded = ['id'];
}
