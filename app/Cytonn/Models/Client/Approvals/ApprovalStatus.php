<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/13/18
 * Time: 1:51 PM
 */

namespace App\Cytonn\Models\Client\Approvals;

use App\Cytonn\Models\BaseModel;

class ApprovalStatus extends BaseModel
{
    protected $table = 'approval_status';

    protected $guarded = ['id'];
}
