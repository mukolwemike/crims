<?php
/**
 * Date: 22/01/2018
 * Time: 11:59
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models\Client\Approvals;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;

class ClientTransactionApprovalStage extends BaseModel
{
    public function approvers()
    {
        return $this->belongsToMany(
            User::class,
            'client_transaction_approvers',
            'stage_id',
            'user_id'
        )->withTimestamps();
    }

    public function transactionTypes()
    {
        return $this->belongsToMany(
            ClientTransactionApprovalType::class,
            'client_transaction_approval_required_stages',
            'stage_id',
            'type_id'
        )->withTimestamps();
    }

    public function awaitingTransactions()
    {
        return $this->hasMany(
            ClientTransactionApproval::class,
            'awaiting_stage_id'
        );
    }
}
