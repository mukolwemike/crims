<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 14/08/2019
 * Time: 09:21
 */

namespace App\Cytonn\Models\Client\Approvals;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientTransactionApproval;

class ApprovalUniqueCheck extends BaseModel
{
    protected $guarded = ['id'];

    protected $table = 'approvals_unique_check';

    public static function createForClient(ClientTransactionApproval $approval)
    {
        if (!$approval->approved) {
            throw new \InvalidArgumentException("Cannot create is transaction is not approved");
        }

        $approver = $approval->approver;

        return static::create([
            'key' => get_class($approval).'_'.$approval->getKeyName().'_'.$approval->getKey(),
            'model_name' => get_class($approval),
            'model_id' => $approval->getKey(),
            'user_id' => $approver ? $approver->id : null
        ]);
    }
}
