<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/13/18
 * Time: 10:52 AM
 */

namespace App\Cytonn\Models\Client\Approvals;

use App\Cytonn\Clients\ClientInstructionApprovalsRepository;
use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;

class ClientInstructionApproval extends BaseModel
{
    protected $repo;

    protected $table = 'client_instruction_approvals';

    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        $this->repo = new ClientInstructionApprovalsRepository($this);

        parent::__construct($attributes);
    }

    public function approvalStatus()
    {
        return $this->belongsTo(ApprovalStatus::class, 'status');
    }

    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'client_user_id');
    }

    public function instruction()
    {
        return $this->belongsTo(ClientInvestmentInstruction::class, 'instruction_id');
    }

    public function topUpForm()
    {
        return $this->belongsTo(ClientTopupForm::class, 'client_topup_form_id');
    }

    public function fundInstruction()
    {
        return $this->belongsTo(UnitFundInvestmentInstruction::class, 'fund_instruction_id');
    }

    public function scopeApproved($query, $approved = true)
    {
        if (!$approved) {
            return $query->whereDoesntHave('approvalStatus')
                ->orWhereHas('approvalStatus', function ($status) {
                    $status->where('slug', 'rejected');
                });
        }

        return $query->whereHas('approvalStatus', function ($status) {
            $status->where('slug', 'approved');
        });
    }

    public function scopeFor($query, ClientTopupForm $form = null, ClientInvestmentInstruction $instruction = null)
    {
        if ($form) {
            return $query->where('client_topup_form_id', $form->id);
        }

        if ($instruction) {
            return $query->where('instruction_id', $instruction->id);
        }

        return $query;
    }

    /**
     * @return bool
     */
    public function requestedByUser()
    {
        $requested_user = $this->fundInstruction ? $this->fundInstruction->user_id :
            ($this->instruction ? $this->instruction->user_id : $this->topUpForm->user_id);

        return $requested_user == $this->client_user_id;
    }
}
