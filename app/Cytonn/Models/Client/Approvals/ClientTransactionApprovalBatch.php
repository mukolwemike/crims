<?php
/**
 * Date: 29/01/2018
 * Time: 08:40
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models\Client\Approvals;

use App\Cytonn\Models\BaseModel;

class ClientTransactionApprovalBatch extends BaseModel
{
    public function types()
    {
        return $this->hasMany(ClientTransactionApprovalType::class, 'batch_id');
    }

    public function columns()
    {
        return $this->hasMany(ClientTransactionApprovalBatchColumn::class, 'batch_id');
    }
}
