<?php
/**
 * Date: 22/01/2018
 * Time: 11:59
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models\Client\Approvals;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientTransactionApprovalStep extends BaseModel
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function stage()
    {
        return $this->belongsTo(ClientTransactionApprovalStage::class, 'stage_id');
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
