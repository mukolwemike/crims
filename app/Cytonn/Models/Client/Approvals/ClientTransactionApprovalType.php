<?php
/**
 * Date: 22/01/2018
 * Time: 12:00
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models\Client\Approvals;

use App\Cytonn\Models\BaseModel;

class ClientTransactionApprovalType extends BaseModel
{
    protected $guarded = ['id'];

    public function allStages()
    {
        $default = ClientTransactionApprovalStage::where('applies_to_all', true)
            ->get();

        return $this->stages()->get()->merge($default)->sortBy('weight');
    }

    public function stages()
    {
        return $this->belongsToMany(
            ClientTransactionApprovalStage::class,
            'client_transaction_approval_required_stages',
            'type_id',
            'stage_id'
        );
    }
}
