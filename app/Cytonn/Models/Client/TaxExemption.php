<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 11/10/2018
 * Time: 12:46
 */

namespace App\Cytonn\Models\Client;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxExemption extends BaseModel
{
    use SoftDeletes;

    protected $table = 'tax_exemptions';

    protected $guarded = ['id'];
}
