<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/10/18
 * Time: 11:01 AM
 */

namespace App\Cytonn\Models\Client;

use App\Cytonn\Models\ClientUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientNotification extends Model
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'client_notifications';

    protected $guarded = ['id'];

    public function users()
    {
        return $this->belongsToMany(
            ClientUser::class,
            'client_notification_reads',
            'client_notification_id',
            'client_user_id'
        );
    }

    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'by');
    }

    public function notificationReads()
    {
        return $this->hasMany(ClientNotificationRead::class, 'client_notification_id');
    }

    public function read(ClientUser $clientUser = null)
    {
        if (!$clientUser) {
            return false;
        }

        $notificationReads = ClientNotificationRead::where('client_notification_id', $this->id)
            ->where('client_user_id', $clientUser->id)
            ->get();

        if ($notificationReads->count()) {
            return false;
        }

        return true;
    }
}
