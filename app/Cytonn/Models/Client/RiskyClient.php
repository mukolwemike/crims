<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 06/11/2018
 * Time: 10:21
 */

namespace App\Cytonn\Models\Client;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class RiskyClient extends BaseModel
{
    use SoftDeletes;
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'risky_clients.firstname' => 10,
            'risky_clients.lastname' => 10,
            'risky_clients.email' => 10,
            'risky_clients.organization' => 5,
        ],
    ];

    protected $table='risky_clients';

    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function scopeStatus($query, $status, $condition = true)
    {
        if ($condition) {
            return $query->where('risky_status_id', $status);
        }

        return $query->where('risky_status_id', '!=', $status);
    }

    public function riskyStatus()
    {
        return $this->belongsTo(RiskyStatus::class, 'risky_status_id');
    }

    public function scopeRisky($query)
    {
        return $query->whereHas('riskyStatus', function ($status) {
            $status->where('name', '!=', 'Cleared');
        });
    }
}
