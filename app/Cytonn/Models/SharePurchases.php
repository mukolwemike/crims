<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Shares\StampDuty;
use Cytonn\Shares\SharePurchaseRepository;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 27/01/2017
 * Time: 14:38
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class SharePurchases extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string $table
     */
    protected $table = 'share_purchases';

    public $repo;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new SharePurchaseRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function purchaseOrder()
    {
        return $this->belongsTo(SharesPurchaseOrder::class, 'shares_purchase_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shareHolder()
    {
        return $this->belongsTo(ShareHolder::class, 'share_holder_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientPayment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shareSale()
    {
        return $this->hasOne(ShareSale::class, 'share_purchase_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    public function saleOrder()
    {
        return $this->belongsTo(SharesSalesOrder::class, 'shares_sales_order_id');
    }

    public function stampDuty()
    {
        return $this->hasMany(StampDuty::class, 'share_purchase_id');
    }

    public function category()
    {
        return $this->belongsTo(SharesCategory::class, 'category_id');
    }

    public function commissionSchedule()
    {
        return $this->hasMany(SharesCommissionPaymentSchedule::class, 'share_purchase_id');
    }
}
