<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Floor extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'floors';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'floor',
    ];

    /*
     * Relationship between a unit and a unit group
     */
    public function projectFloors()
    {
        return $this->hasMany(ProjectFloor::class, 'floor_id');
    }
}
