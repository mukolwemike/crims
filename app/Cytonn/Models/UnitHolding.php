<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Investment\AdditionalCommission;
use App\Cytonn\Clients\ClientLoyaltyPoints\RealEstateLoyaltyCalculator;
use Carbon\Carbon;
use Cytonn\Models\RealEstate\AdvanceRealEstateCommission;
use Cytonn\Realestate\Payments\Interest;
use Cytonn\Realestate\Project\UnitHoldingRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 02/06/2016
 * Time: 10:36 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class UnitHolding extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'realestate_units.number' => 10,
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'realestate_unit_sizes.name' => 5
        ],
        'joins' => [
            'realestate_units' => ['unit_holdings.unit_id', 'realestate_units.id'],
            'clients' => ['unit_holdings.client_id', 'clients.id'],
            'contacts' => ['contacts.id', 'clients.contact_id'],
            'reservation_forms' => ['reservation_forms.holding_id', 'unit_holdings.id'],
            'realestate_unit_sizes' => ['realestate_units.size_id', 'realestate_unit_sizes.id']
        ],
    ];

    /**
     * @var string
     */
    protected $table = 'unit_holdings';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    public $repo;

    protected $dates = ['reservation_date'];

    /**
     * UnitHolding constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new UnitHoldingRepository($this);
    }

    /**
     * @param Carbon|null $date
     * @param Carbon|null $start
     * @return RealEstateLoyaltyCalculator
     */
    public function calculateLoyalty(
        Carbon $date = null,
        Carbon $start = null
    ) {
        $date = Carbon::parse($date);

        return new  RealEstateLoyaltyCalculator($this, $date, $start);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reservation()
    {
        return $this->hasOne(ReservationForm::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(RealestateUnit::class, 'unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->unit->project();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function loo() //letter of offer
    {
        return $this->hasOne(RealestateLetterOfOffer::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function letterOfOffer()
    {
        return $this->loo();
    }

    /**
     * @param ClientTransactionApproval $approval
     */
    public function approve(ClientTransactionApproval $approval)
    {
        $this->approval_id = $approval->id;
        $this->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    /**
     * Check whether approved
     *
     * @return bool
     */
    public function approved()
    {
        if (!$this->approval) {
            return false;
        }

        return $this->approval->approved;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(RealEstatePayment::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function salesAgreement()
    {
        return $this->hasOne(RealEstateSalesAgreement::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentSchedules()
    {
        return $this->hasMany(RealEstatePaymentSchedule::class, 'unit_holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tranche()
    {
        return $this->belongsTo(RealEstateUnitTranche::class, 'tranche_id');
    }

    public function unitTranche()
    {
        return $this->belongsTo(RealEstateUnitTranche::class, 'tranche_id');
    }

    /**
     * Get the price of the unit.
     * Check for a negotiated price then use the tranche to find the price for the unit size
     *
     * @param bool $discounted
     * @return int
     */
    public function price($discounted = true)
    {
        if ((int)$this->negotiated_price > 0) {
            return $this->negotiated_price;
        }

        if (is_null($this->unitTranche)) {
            return null;
        }

        try {
            $price = $this->unitTranche->repo->getUnitPrice($this->unit, $this->paymentPlan);
        } catch (\Exception $e) {
            return null;
        }

        if ($discounted) {
            $discount = (float)$this->discount;

            return (int)((100 - $discount) * $price / 100);
        }

        return $price;
    }

    public function interest($date = null, $date_from = null)
    {
        $date = $date ? Carbon::parse($date) : Carbon::today();

        $holding = $this;

        $holding->load('payments');

        $schedules = $this->paymentSchedules()->inPricing()
            ->accruesInterest()
            ->oldest('date');

        if ($date_from) {
            $date_from = Carbon::parse($date_from);

            $schedules = $schedules->where('date', '>=', $date_from);
        }

        return $schedules
            ->get()
            ->sum(function (RealEstatePaymentSchedule $schedule) use ($date, $holding) {
                $interest = new Interest($holding);

                return $interest->interest($schedule, $date);
            });
    }

    /**
     * The total amount paid for the unit
     *
     * @return mixed
     */
    public function totalPayments($date = null)
    {
        if (is_null($date)) {
            return $this->payments()->sum('amount');
        }

        return $this->payments()->where('date', '<=', $date)->sum('amount');
    }

    /**
     * The total amount remaining for the amount to be fully paid for
     *
     * @return mixed
     */
    public function amountRemaining()
    {
        return $this->price() - $this->totalInPricingPayments();
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function totalInPricingPayments($date = null)
    {
        if (is_null($date)) {
            return $this->payments()->inPricingPayments()->sum('amount');
        }

        return $this->payments()->inPricingPayments()->where('date', '<=', $date)->sum('amount');
    }

    /**
     * @param null $date
     * @param null $date_from dd
     * @return mixed
     */
    public function refundedAmount($date = null, $date_from = null)
    {
        if (is_null($date)) {
            return $this->refunds()->sum('amount');
        }

        $refunds = $this->refunds()->where('date', '<=', $date);

        if ($date_from) {
            $refunds = $refunds->where('date', '>=', $date_from);
        }

        return $refunds->sum('amount');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentPlan()
    {
        return $this->belongsTo(RealEstatePaymentPlan::class, 'payment_plan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function commission()
    {
        return $this->hasOne(RealestateCommission::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function refunds()
    {
        return $this->hasMany(RealEstateRefund::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function advanceCommissions()
    {
        return $this->hasMany(AdditionalCommission::class, 'holding_id');
    }

    /**
     * @param $date
     * @param $narration
     * @return $this
     */
    public function forfeit(Carbon $date, $narration)
    {
        $this->forfeit_reason = $narration;
        $this->forfeit_date = $date;
        $this->active = false;
        $this->save();

        return $this;
    }

    public function reverseForfeiture()
    {
        $this->forfeit_reason = null;
        $this->forfeit_date = null;
        $this->active = true;
        $this->save();

        return $this;
    }

    public function scopeInProjectIds($query, array $ids, $condition = true)
    {
        if ($condition) {
            return $query->whereHas('unit', function ($unit) use ($ids) {
                $unit->whereIn('project_id', $ids);
            });
        }

        return $query->whereHas('unit', function ($unit) use ($ids) {
            $unit->whereNotIn('project_id', $ids);
        });
    }

    public function scopeActive($query, $active = true)
    {
        if ($active) {
            return $query->where('active', 1);
        }

        return $query->where(function ($q) {
            $q->whereNull('active')->orWhere('active', 0);
        });
    }

    public function scopeActiveAtDate($query, Carbon $date, $active = true)
    {
        if ($active) {
            return $query->where(function ($q) use ($date) {
                $q->where('reservation_date', '<=', $date)->orWhere(function ($q) use ($date) {
                    $q->whereNull('reservation_date')->where('created_at', '<=', $date);
                });
            })->where(function ($q) use ($date) {
                $q->where('active', 1)->orWhere(function ($q) use ($date) {
                    $q->where('active', 0)->where('forfeit_date', '>', $date);
                });
            });
        }

        return $query->where('forfeit_date', '<=', $date)->where(function ($q) use ($date) {
            $q->whereNull('active')->orWhere('active', 0);
        });
    }

    /**
     * @param $query
     * @param Carbon|null $date
     * @return mixed
     */
    public function scopeDate($query, Carbon $date = null)
    {
        if ($date) {
            return $query->where('created_at', '>=', $date->copy()->startOfDay())
                ->where('created_at', '<=', $date->copy()->endOfDay());
        }

        return $query;
    }

    public function scopeCreatedBetween($query, Carbon $date_to = null, Carbon $date_from = null)
    {
        $date = $date_to ? Carbon::parse($date_to) : Carbon::today();

        $date_from = $date_from ? Carbon::parse($date_from) : null;

        $query = $query->where('created_at', '<=', $date_to);

        if ($date_from) {
            $query = $query->where('created_at', '>=', $date_from->copy()->startOfDay());
        }

        return $query;
    }

    public function scopeMonthlyLoos($query, Carbon $date = null)
    {
        return $query->whereHas('loo', function ($loo) use ($date) {
            $loo->whereBetween('sent_on', [$date->copy()->startOfMonth(), $date->copy()->endOfMonth()]);
        });
    }

    /**
     * Holding without sales agreements or which sa are not uploaded
     * @param $query
     * @return mixed
     */
    public function scopeWithoutSalesAgreement($query)
    {
        return $query->has('payments')->whereDoesntHave('salesAgreement')
            ->orWhereHas('salesAgreement', function ($sa) {
                $sa->whereNull('document_id')
                    ->orWhereNull('date_signed');
            });
    }

    public function scopeLooSentBefore($query, Carbon $date = null)
    {
        return $query->whereHas('loo', function ($loo) use ($date) {
            $loo->where('sent_on', '<=', $date);
        });
    }

    public function realestateForfeitureNotices()
    {
        return $this->hasMany(RealestateForfeitureNotice::class);
    }

    public function scopeReservedAfter($query, Carbon $date)
    {
        return $query->where(function ($q) use ($date) {
            $q->where('reservation_date', '>=', $date)->orWhere(function ($q) use ($date) {
                $q->whereNull('reservation_date')->where('created_at', '>=', $date);
            });
        });
    }

    public function scopeReservedBefore($query, Carbon $date)
    {
        return $query->where(function ($q) use ($date) {
            $q->where('reservation_date', '<=', $date)->orWhere(function ($q) use ($date) {
                $q->whereNull('reservation_date')->where('created_at', '<=', $date);
            });
        });
    }
}
