<?php

namespace App\Cytonn\Models;

/**
 * Date: 8/10/15
 * Time: 10:22 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


class TransactionOwner extends BaseModel
{
    protected $table = 'transaction_owners';

    protected $guarded = ['id'];

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    public static function getOwner(Client $client = null, PortfolioInvestor $institution = null)
    {
        if ($client) {
            //get transaction owner from client id
            $owner = TransactionOwner::where('client_id', $client->id)->first();

            if ($owner == null) { //client not in transaction owners table, add
                $owner = new TransactionOwner();
                $owner->fund_investor_id = null;
                $owner->client_id = $client->id;
                $owner->fund_manager_id = $client->fund_manager_id;
                $owner->save();
            }
        } elseif ($institution) {
            //get transaction owner from client id
            $owner = TransactionOwner::where('fund_investor_id', $institution->id)->first();

            if ($owner == null) { //client not in transaction owners table, add
                $owner = new TransactionOwner();
                $owner->fund_investor_id = $institution->id;
                $owner->client_id = null;
                $owner->fund_manager_id = null;
                $owner->save();
            }
        } else {
            throw new \Exception('Both client and institution cannot be null');
        }

        return $owner;
    }
}
