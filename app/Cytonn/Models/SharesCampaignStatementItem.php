<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class SharesCampaignStatementItem extends BaseModel
{
    use SearchableTrait, SoftDeletes;

    /**
     * @var string
     */
    public $table = 'shares_campaign_statement_items';
    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10
        ],
        'joins' => [
            'clients' => ['shares_campaign_statement_items.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Relationship to client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Relationship to the campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(SharesStatementCampaign::class, 'campaign_id');
    }

    public function shareHolder()
    {
        return $this->belongsTo(ShareHolder::class, 'share_holder_id');
    }
}
