<?php

namespace App\Cytonn\Models;

class Payments extends BaseModel
{
    protected $table = 'realestate_payments';

    protected $dates = ['date'];

    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }
    
    public function paymentSchedule()
    {
        return $this->belongsTo(RealEstatePaymentSchedule::class, 'id');
    }
    
    public function clientPayment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }
}
