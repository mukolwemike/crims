<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class PortfolioInstruction extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string
     */
    public $table = 'portfolio_instructions';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(CustodialAccount::class, 'sender_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(CustodialAccount::class, 'receiver_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(PortfolioTransactionApproval::class, 'approval_id');
    }
}
