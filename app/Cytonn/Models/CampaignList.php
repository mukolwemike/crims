<?php

namespace App\Cytonn\Models;

class CampaignList extends BaseModel
{
    protected $table = 'campaign_lists';

    protected $guarded = ['id'];
}
