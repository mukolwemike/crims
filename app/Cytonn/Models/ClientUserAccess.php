<?php

namespace App\Cytonn\Models;

/**
 * Date: 16/01/2016
 * Time: 1:00 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientUserAccess extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'client_user_access';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }

    public function scopeActive($query, $condition = true)
    {
        if (!$condition) {
            return $query->where('active', 0)
                ->orWhereNull('active');
        }

        return $query->where('active', 1);
    }
}
