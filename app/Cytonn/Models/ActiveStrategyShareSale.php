<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\ActiveStrategy\ActiveStrategyShareSaleRepository;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 06/04/2016
 * Time: 9:43 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ActiveStrategyShareSale extends BaseModel
{
    use SoftDeletes;

    public $table = 'active_strategy_share_sales';

    protected $guarded = ['id'];

    public $repo;

    /**
     * ActiveStrategyShareSale constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->repo = new ActiveStrategyShareSaleRepository($this);

        parent::__construct($attributes);
    }

    public function security()
    {
        return $this->belongsTo(ActiveStrategySecurity::class, 'security_id');
    }

    public function portfolioSecurity()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'security_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function scopeForUnitFund($query, UnitFund $fund)
    {
        return $query->where('unit_fund_id', $fund->id);
    }

    public function scopeOffshore($query, $condition = false)
    {
        return $query->whereHas('portfolioSecurity', function ($q) use ($condition) {
            if ($condition) {
                $q->where('offshore', 1);
            } else {
                $q->where('offshore', '!=', 1);
            }
        });
    }
}
