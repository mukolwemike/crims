<?php

namespace App\Cytonn\Models;

use App\Cytonn\Clients\ClientLoyaltyPoints\LoyaltyPointsCalculator;
use App\Cytonn\Clients\ClientLoyaltyPoints\UnitFundLoyaltyCalculator;
use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\Client\ClientSetting;
use App\Cytonn\Models\Client\ConsentForm;
use App\Cytonn\Models\Client\Kyc\IprsValidation;
use App\Cytonn\Models\Client\TaxExemption;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundCampaignStatementItem;
use App\Cytonn\Models\Unitization\UnitFundFeesCharged;
use App\Cytonn\Models\Unitization\UnitFundHolder;
use App\Cytonn\Models\Unitization\UnitFundInterestSchedule;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use Carbon\Carbon;
use Cytonn\Clients\ClientRepository;
use Cytonn\Crm\CrmSyncTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\BankDetails;
use Cytonn\Investment\TaxRepository;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Unitization\Calculate\MoneyMarketCalculator;
use Cytonn\Unitization\Calculate\NavFundCalculator;
use Cytonn\Unitization\Calculate\SimpleCalculator;
use Cytonn\Unitization\Clients\UnitFundClientRepository;
use Cytonn\Uuids\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class Client extends BaseModel
{
    use SearchableTrait, UuidTrait, SoftDeletes, PresentableTrait;
    use CrmSyncTrait;

    /*
     * Specify the presenter for the client
     */
    protected $presenter = \Cytonn\Clients\ClientPresenter::class;


    protected $guarded = ['id'];

    public $repo;

    protected $dates = ['dob'];

    public $taxrepo;

    public $unitFundClientRepo;

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'clients.employee_number' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'clients.investor_bank' => 4,
            'clients.investor_bank_branch' => 4,
            'clients.investor_account_name' => 10,
            'clients.investor_account_number' => 10,
            'client_bank_accounts.account_name' => 4,
            'client_bank_accounts.account_number' => 10,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10
        ],
        'joins' => [
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_bank_accounts' => ['clients.id', 'client_bank_accounts.client_id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id']
        ],
        'groupBy' => 'clients.id'
    ];

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new ClientRepository($this);

        $this->taxrepo = new TaxRepository($this);

        $this->unitFundClientRepo = new UnitFundClientRepository($this);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany(TransactionOwner::class);
    }

    public function unitHoldings()
    {
        return $this->hasMany(UnitHolding::class);
    }

    public function clientType()
    {
        return $this->belongsTo(ClientType::class);
    }

    public function type()
    {
        return $this->clientType();
    }

    public function contactPersonTitle()
    {
        return $this->belongsTo(Title::class, 'contact_person_title');
    }

    public function investments()
    {
        return $this->hasMany(ClientInvestment::class);
    }

    public function portfolioInvestments()
    {
        return $this->hasManyThrough(DepositHolding::class, PortfolioSecurity::class);
    }

    public function add($clientdata)
    {
        $this->fill($clientdata);

        $this->save();

        return $this;
    }

    public function name()
    {
        return ClientPresenter::presentFullNames($this->id);
    }

    public function jointShortName()
    {
        return ClientPresenter::presentJointShortName($this->id);
    }

    public function jointHolders()
    {
        return $this->hasMany(JointClientHolder::class, 'client_id');
    }

    public function jointDetail()
    {
        return $this->hasMany(ClientJointDetail::class, 'client_id');
    }

    public function getEmailsAttribute($value)
    {
        return unserialize($value);
    }

    public function setEmailsAttribute($value)
    {
        $this->attributes['emails'] = serialize($value);
    }

    public function contactPersons()
    {
        return $this->hasMany(ClientContactPerson::class, 'client_id');
    }

    public function bankAccounts()
    {
        return $this->hasMany(ClientBankAccount::class, 'client_id');
    }

    public function bankDetails()
    {
        return new BankDetails(null, $this);
    }

    public function emailIndemnity()
    {
        return $this->hasMany(EmailIndemnity::class, 'client_id');
    }

    public function consentForm()
    {
        return $this->hasMany(ConsentForm::class, 'client_id');
    }

    public function bankInstructions()
    {
        return $this->hasMany(BankInstruction::class, 'client_id');
    }

    public function scopeBirthday($query, $date = null)
    {
        $date = (new Carbon($date))->startOfDay();

        return $query->whereHas(
            'clientType',
            function ($type) {
                $type->where('name', 'individual');
            }
        )
            ->whereRaw('extract(month from dob) = ?', [$date->month])
            ->whereRaw('extract(day from dob) = ?', [$date->day]);
    }

    public function coopPayment()
    {
        return $this->coopPayments();
    }

    public function coopPayments()
    {
        return $this->hasMany(CoopPayment::class, 'client_id');
    }

    public function clientPayments()
    {
        return $this->hasMany(ClientPayment::class, 'client_id');
    }

    public function shareHolders()
    {
        return $this->hasMany(ShareHolder::class, 'client_id');
    }

    public function shareHoldings()
    {
        return $this->hasMany(ShareHolding::class, 'client_id');
    }

    public function unitFundPurchases()
    {
        return $this->hasMany(UnitFundPurchase::class, 'client_id');
    }

    public function unitFundSales()
    {
        return $this->hasMany(UnitFundSale::class, 'client_id');
    }

    public function unitFundInterestSchedules()
    {
        return $this->hasMany(UnitFundInterestSchedule::class, 'client_id');
    }

    public function unitFundTransfers()
    {
        return $this->hasMany(UnitFundPurchase::class, 'client_id');
    }

    public function unitFundTransfersReceived()
    {
        return $this->hasMany(UnitFundTransfer::class, 'transferee_id');
    }

    public function unitFundTransfersGiven()
    {
        return $this->hasMany(UnitFundTransfer::class, 'transferer_id');
    }

    public function unitFundHoldings()
    {
        return $this->hasMany(UnitFundHolder::class, 'client_id');
    }

    public function standingOrders()
    {
        return $this->hasMany(ClientStandingOrder::class, 'client_id');
    }

    public function unitFundInstructions()
    {
        return $this->hasMany(UnitFundInvestmentInstruction::class, 'client_id');
    }

    public function getFeesIncurred()
    {
        return UnitFundFeesCharged::active()
            ->whereHas('purchase', function ($purchase) {
                $purchase->where('client_id', $this->id);
            })
            ->orWhereHas('sale', function ($sale) {
                $sale->where('client_id', $this->id);
            });
//                ->orWhereHas('transferGiven', function($transfer) {
//                    $transfer->where('transferer_id', $this->id);
//                })
//                ->orWhereHas('transferReceived', function($transfer) {
//                    $transfer->where('transferee_id', $this->id);
//                });
    }

    public function getContactEmailsArray()
    {
        $client_emails = [];

        if (trim($this->contact->email)) {
            array_push($client_emails, trim($this->contact->email));
        }

        is_array($this->emails) ?: $this->emails = [];

        foreach ($this->emails as $email) {
            if (trim($email)) {
                array_push($client_emails, trim($email));
            }
        }

        foreach ($this->jointHolders as $holder) {
            if (trim($holder->details->email)) {
                array_push($client_emails, trim($holder->details->email));
            }
        }

        return $client_emails;
    }

    public function incomingFa()
    {
        return $this->belongsTo(
            CommissionRecepient::class,
            'incoming_fa_id'
        );
    }

    public function getContactPhoneNumbersArray()
    {
        $phoneNumbers = [];

        if ($this->joint && $this->jointHolders->count()) {
            $phoneNumbers = $this->jointHoldersPhoneNumbers();
        }

        if ($this->clientType->name == 'corporate') {
            $phoneNumbers = $this->corporateClientPhoneNumbers();
        }

        $individualPhone = $this->individualPhoneNumber();

        if (isNotEmptyOrNull($individualPhone)) {
            array_push($phoneNumbers, $individualPhone);
        }

        return $phoneNumbers;
    }

    public function individualPhoneNumber()
    {
        $phone = trimPhoneNumber($this->phone);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        $phone = trimPhoneNumber($this->telephone_home);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        return $this->contact->present()->getPhone;
    }

    public function jointHoldersPhoneNumbers()
    {
        $phones = [];

        foreach ($this->jointHolders as $holder) {
            $phone = $holder->present()->getPhone;

            if (isNotEmptyOrNull($phone)) {
                array_push($phones, $phone);
            }
        }

        return $phones;
    }

    public function corporateClientPhoneNumbers()
    {
        $phones = [];

        foreach ($this->contactPersons as $person) {
            $phone = trimPhoneNumber($person->phone);

            if (isNotEmptyOrNull($phone)) {
                array_push($phones, $phone);
            }
        }

        return $phones;
    }

    public function getLatestFA($bias = 'investments', $useRelationship = true)
    {
        if ($useRelationship && $this->relationshipPerson) {
            return $this->relationshipPerson;
        }

        $inv = $this->getLatestInvestmentsFa($useRelationship);
        $re = $this->getRealEstateLatestFa($useRelationship);
        $shares = $this->getLatestSharesFa($useRelationship);
        $units = $this->getLatestUnitFundsFa($useRelationship);

        if ($bias == 'investments' && $inv) {
            return $inv;
        }

        if ($bias == 'realestate' && $re) {
            return $re;
        }

        if ($bias == 'shares' && $shares) {
            return $shares;
        }

        if ($bias == 'units' && $units) {
            return $units;
        }

        if ($re) {
            return $re;
        }

        if ($inv) {
            return $inv;
        }

        if ($shares) {
            return $shares;
        }

        if ($units) {
            return $units;
        }

        if ($fromApplications = $this->applicationsFinancialAdvisor()) {
            return $fromApplications;
        }

        if (! $useRelationship && $this->relationshipPerson) {
            return $this->relationshipPerson;
        }

        return $inv;
    }

    protected function getLatestSharesFa($useRelationship = true)
    {
        if ($useRelationship && $this->relationshipPerson) {
            return $this->relationshipPerson;
        }

        $latestPurchase = SharesPurchaseOrder::whereNotNull('commission_recipient_id')
            ->forClient($this)->latest('request_date')->first();

        if ($latestPurchase) {
            return $latestPurchase->recipient;
        }

        return null;
    }

    protected function getLatestUnitFundsFa($useRelationship = true)
    {
        if ($useRelationship && $this->relationshipPerson) {
            return $this->relationshipPerson;
        }

        $latestPurchase = $this->unitFundPurchases()->whereHas(
            'unitFundCommission',
            function ($c) {
                $c->whereNotNull('commission_recipient_id');
            }
        )->latest('date')->first();

        if ($latestPurchase) {
            return $latestPurchase->unitFundCommission->recipient;
        }

        return null;
    }

    protected function getLatestInvestmentsFa($useRelationship = true)
    {
        if ($useRelationship && $this->relationshipPerson) {
            return $this->relationshipPerson;
        }

        $latestInvestment = $this->investments()->whereHas(
            'commission',
            function ($c) {
                $c->whereNotNull('recipient_id');
            }
        )->latest('invested_date')->first();

        if ($latestInvestment) {
            return $latestInvestment->commission->recipient;
        }

        return null;
    }

    protected function getRealEstateLatestFa($useRelationship = true)
    {
        if ($useRelationship && $this->relationshipPerson) {
            return $this->relationshipPerson;
        }

        $holding = $this->unitHoldings()->whereHas(
            'commission',
            function ($c) {
                $c->whereNotNull('recipient_id');
            }
        )->latest()->first();

        if (!is_null($holding)) {
            return $holding->commission->recipient;
        }

        return null;
    }

    protected function applicationsFinancialAdvisor()
    {
        $appl = ClientFilledInvestmentApplication::has('commissionRecipient')
            ->whereHas('application', function ($app) {
                $app->where('client_id', $this->id);
            })->latest()
            ->first();

        if ($appl) {
            return $appl->commissionRecipient;
        }

        return ClientInvestmentApplication::has('commissionRecipient')
            ->where('client_id', $this->id)
            ->latest()
            ->first();
    }

    public function relationshipPerson()
    {
        return $this->belongsTo(CommissionRecepient::class, 'relationship_person_id');
    }

    public function coopPaymentsBalance()
    {
        return $this->coopPayments()->sum('amount');
    }

    public function coopProductPlans()
    {
        return $this->hasMany(ProductPlan::class, 'client_id');
    }

    public function productPlans()
    {
        return $this->coopProductPlans();
    }

    public function projects()
    {
        return $this->unitHoldings->map(
            function ($holding) {
                return $holding->project;
            }
        )->unique('id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function nationality()
    {
        return $this->belongsTo(Country::class, 'nationality_id');
    }

    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    public function employment()
    {
        return $this->belongsTo(Employment::class, 'employment_id');
    }

    public function complianceCheck()
    {
        return $this->hasOne(ClientInvestmentApplicationKycCheck::class, 'client_id');
    }

    public function applications()
    {
        return $this->hasMany(ClientInvestmentApplication::class, 'client_id');
    }

    public function uploadedKyc()
    {
        return $this->hasMany(ClientUploadedKyc::class, 'client_id');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'client_id');
    }

    public function clientUsers()
    {
        return $this->belongsToMany(
            ClientUser::class,
            'client_user_access',
            'client_id',
            'user_id'
        );
    }

    public static function boot()
    {
        parent::boot();

        static::saving(
            function (Model $client) {
                $client_code = $client->client_code;

                if (is_null($client->client_code) || is_null($client->id)) {
                    return true;
                }

                $same_code = static::where(
                    'client_code',
                    $client_code
                )->where('id', '!=', $client->id)->first();

                if (!is_null($same_code)) {
                    throw new ClientInvestmentException(
                        "Client code $client_code is already taken!"
                    );
                }

                return true;
            }
        );
    }

    public function scopeForFundManager($client)
    {
        if (!$this->currentFundManager()) {
            return $client;
        }

        return $client->where(function ($c) {
            $c->where('fund_manager_id', $this->currentFundManager()->id)
                ->orWhereHas('investments', function ($i) {
                    $i->forFundManager();
                })->orWhereHas('unitFundPurchases', function ($purchase) {
                    $purchase->forFundManager();
                })->orWhereHas('applications', function ($appl) {
                    $appl->whereHas('product', function ($product) {
                        $product->where('fund_manager_id', $this->currentFundManager()->id);
                    })->orWhereHas('unitFund', function ($unitFund) {
                        $unitFund->where('fund_manager_id', $this->currentFundManager()->id);
                    });
                });
        });
    }

    public function scopeHasProduct($query)
    {
        return $query->where(function ($query) {
            $query->has('investments')
                ->orHas('unitHoldings')
                ->orHas('shareHoldings')
                ->orHas('unitFundPurchases');
        });
    }

    public function scopeCompliant($client, $date = null, $state = true)
    {
        $date = new Carbon($date);

        if (!$state) {
            return $client->where(
                function ($c) use ($date) {
                    $c->where(
                        function ($co) {
                            $co->whereNull('compliant')->orWhere('compliant', false);
                        }
                    )->orWhere('compliance_date', '>', $date);
                }
            );
        }

        return $client->where(
            function ($c) use ($date) {
                $c->where('compliant', true)->where('compliance_date', '<=', $date);
            }
        );
    }

    public function scopeActive(
        $query,
        $investments = true,
        $realEstate = true,
        $portfolio = true,
        $funds = true
    ) {
        return $query->where(function ($query) use ($investments, $realEstate, $portfolio, $funds) {
            if ($investments) {
                $query = $query->whereHas('investments', function ($q) {
                    $q->active();
                });
            }

            if ($portfolio) {
                $query = $query->orWhereHas('portfolioInvestments', function ($q) {
                    $q->active();
                });
            }

            if ($realEstate) {
                $query = $query->orWhereHas('unitHoldings', function ($q) {
                    $q->active();
                });
            }

            if ($funds) {
                $query = $query->orWhereHas('unitFundPurchases', function ($q) {
                    $q->active();
                });
            }

            return $query;
        });
    }

    public function scopeActiveOnDate(
        $query,
        Carbon $date,
        $investments = true,
        $realEstate = true,
        $portfolio = true,
        $funds = true
    ) {
        return $query->where(function ($query) use ($date, $investments, $realEstate, $portfolio, $funds) {
            if ($investments) {
                $query = $query->whereHas(
                    'investments',
                    function ($q) use ($date) {
                        $q->activeOnDate($date);
                    }
                );
            }

            if ($portfolio) {
                $query = $query->orWhereHas(
                    'portfolioInvestments',
                    function ($q) use ($date) {
                        $q->activeOnDate($date);
                    }
                );
            }

            if ($realEstate) {
                $query = $query->orWhereHas(
                    'unitHoldings',
                    function ($q) use ($date) {
                        $q->activeAtDate($date);
                    }
                );
            }

            if ($funds) {
                $query = $query->orWhereHas(
                    'unitFundPurchases',
                    function ($q) use ($date) {
                        $q->activeOnDate($date);
                    }
                );
            }

            return $query;
        });
    }

    public function getNotificationEmailsAttribute($value)
    {
        return json_decode($value);
    }

    public function setNotificationEmailsAttribute($value)
    {
        $this->attributes['notification_emails'] = json_encode($value);
    }

    public function getDefaultBankAccountAttribute()
    {
        return $this->investor_bank . ' - ' . $this->investor_account_name . ' - ' . $this->investor_account_number;
    }

    public function clientSignatures()
    {
        return $this->hasMany(ClientSignature::class);
    }

    public function guards()
    {
        return $this->hasMany(Guard::class);
    }

    public function portfolioInvestors()
    {
        return $this->hasMany(PortfolioInvestor::class);
    }

    public function portfolioSecurities()
    {
        return $this->hasMany(PortfolioSecurity::class);
    }

    public function clientApprovals()
    {
        return $this->hasMany(ClientInstructionApproval::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientTransactionApprovals()
    {
        return $this->hasMany(ClientTransactionApproval::class, 'client_id');
    }

    public function paymentBalance($payment)
    {
        if (is_null($payment)) {
            return 0;
        }

        $paymentsTable = (new ClientPayment())->getTable();

        $date = new Carbon($payment->date);

        $totalYesterdayAmountQ = DB::table($paymentsTable)
            ->select(DB::raw('sum(amount) as account_balance'))
            ->where('client_id', $this->id)
            ->where('date', '<', $date)
            ->whereNull('deleted_at');

        if ($eid = $payment->share_entity_id) {
            $totalYesterdayAmountQ = $totalYesterdayAmountQ->where('share_entity_id', $eid);
        }

        if ($pid = $payment->product_id) {
            $totalYesterdayAmountQ = $totalYesterdayAmountQ->where('product_id', $pid);
        }

        if ($rid = $payment->project_id) {
            $totalYesterdayAmountQ = $totalYesterdayAmountQ->where('project_id', $rid);
        }

        $totalYesterdayAmount = $totalYesterdayAmountQ->first();

        $todayPaymentAmountQ = ClientPayment::where('date', $date)
            ->where('client_id', $this->id)
            ->where('id', '<=', $payment->id)
            ->whereNull('deleted_at');

        if ($eid) {
            $todayPaymentAmountQ->where('share_entity_id', $eid);
        }

        if ($pid) {
            $todayPaymentAmountQ->where('product_id', $pid);
        }

        if ($rid) {
            $todayPaymentAmountQ->where('project_id', $rid);
        }

        $todayPaymentAmount = $todayPaymentAmountQ->sum('amount');

        return $totalYesterdayAmount->account_balance + $todayPaymentAmount;
    }

    public function topupForms()
    {
        return $this->hasMany(ClientTopupForm::class, 'client_id');
    }

    public function allowedMinimumBalance()
    {
        return $this->hasMany(ClientPaymentAllowedMinimum::class, 'client_id');
    }

    public function taxExemptions()
    {
        return $this->hasMany(TaxExemption::class, 'client_id');
    }

    public function settings()
    {
        return $this->hasOne(ClientSetting::class, 'client_id');
    }

    public function combinedInterest()
    {
        return ($this->combine_interest_reinvestment) ? true : false;
    }

    public function clientFundDetails(UnitFund $fund)
    {
        $fund_id = $fund->id;

        return "/dashboard/unitization/unit-funds/$fund_id/unit-fund-clients/$this->id";
    }

    public function fundPaymentBalance(UnitFund $fund, $date = null)
    {
        $date = Carbon::parse($date);


        return (double)(new ClientPayment())->balance(
            $this,
            null,
            null,
            null,
            $date,
            $fund
        );
    }

    public function iprsValidations()
    {
        return $this->belongsToMany(
            IprsValidation::class,
            'client_iprs_validations',
            'client_id',
            'iprs_id'
        );
    }

    public function clientBalance(
        $date = null,
        Product $product = null,
        Project $project = null,
        SharesEntity $entity = null,
        UnitFund $fund = null
    ) {
        $date = Carbon::parse($date);

        return (double)(new ClientPayment())->balance(
            $this,
            $product,
            $project,
            $entity,
            $date,
            $fund
        );
    }

    public function ownedNumberOfUnits(UnitFund $fund, $date = null)
    {
        return (float) $this->repo->ownedNumberOfUnits($fund, $date);
    }

    /**
     * @param UnitFund $fund
     * @param Carbon $date
     * @param Carbon|null $start
     * @param bool $as_at_next_day
     * @param UnitFundPurchase|null $purchase
     * @return MoneyMarketCalculator|NavFundCalculator|SimpleCalculator
     * @throws ClientInvestmentException
     */
    public function calculateFund(
        UnitFund $fund,
        Carbon $date,
        Carbon $start = null,
        $as_at_next_day = true,
        UnitFundPurchase $purchase = null
    ) {
        $calc = $fund->category->calculation->slug;

        switch ($calc) {
            case 'variable-unit-price':
                return new NavFundCalculator($this, $fund, $date, $start, $as_at_next_day, $purchase);
            case 'daily-yield':
                return new MoneyMarketCalculator($this, $fund, $date, $start, $as_at_next_day, $purchase);
            case 'income-distribution':
                return new SimpleCalculator($this, $fund, $date, $start, $as_at_next_day, $purchase);
            default:
                throw new ClientInvestmentException("Not Supported");
        }
    }

//    public function calculateUnitFundLoyalty(UnitFund $unitFund, Carbon $date = null, Carbon $start = null)
//    {
//        $date = Carbon::parse($date);
//
//        return new  UnitFundLoyaltyCalculator($this, $unitFund, $date, $start);
//    }

    /**
     * @param Carbon|null $date
     * @param Carbon|null $start
     * @return LoyaltyPointsCalculator
     */
    public function calculateLoyalty(
        Carbon $date = null,
        Carbon $start = null
    ) {
        $date = Carbon::parse($date);

        $startingDate = $start ? $start : Carbon::parse('2020-01-01');

        return new  LoyaltyPointsCalculator($this, $date, $startingDate);
    }

    public function phone()
    {
        $phoneArray = array_filter(
            $this->getContactPhoneNumbersArray(),
            function ($value) {
                return !empty($value);
            }
        );

        return implode($phoneArray, ", ");
    }

    public function mpesaBankAccount(Model $client)
    {
        return ClientBankAccount::active()->where('client_id', $client->id)
            ->whereHas('branch', function ($query) {
                $query->where('swift_code', 'MPESA');
            });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function investmentCampaignItems()
    {
        return $this->hasMany(CampaignStatementItem::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function realEstateCampaignItems()
    {
        return $this->hasMany(RealestateCampaignStatementItem::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shareCampaignItems()
    {
        return $this->hasMany(SharesCampaignStatementItem::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function unitFundCampaignItems()
    {
        return $this->hasMany(UnitFundCampaignStatementItem::class, 'client_id');
    }
}
