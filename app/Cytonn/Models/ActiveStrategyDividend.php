<?php

namespace App\Cytonn\Models;

/**
 * Date: 06/04/2016
 * Time: 12:16 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ActiveStrategyDividend extends BaseModel
{
    public $table = 'active_strategy_dividends';

    protected $guarded = ['id'];

    /**
     * ActiveStrategyDividend constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function security()
    {
        return $this->belongsTo(ActiveStrategySecurity::class, 'security_id');
    }

    /**
     * @return mixed
     */
    public function prevailingTaxRate()
    {
        return Setting::where('key', 'dividend_tax_rate')->first()->value;
    }
}
