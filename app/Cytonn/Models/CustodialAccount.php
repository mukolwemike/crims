<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Investment\FundManager\FundManagerScopingTrait;
use Cytonn\Portfolio\CustodialRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class CustodialAccount
 */
class CustodialAccount extends BaseModel
{
    use FundManagerScopingTrait;

    /**
     * @var string
     */
    protected $table = 'custodial_accounts';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $with = ['currency'];

    /**
     * @var CustodialRepository
     */
    public $repo;

    /**
     * CustodialAccount constructor.
     *
     * @param    array $attributes
     * @internal param $repo
     */
    public function __construct(array $attributes = [])
    {
        $this->repo = new CustodialRepository($this);
        parent::__construct($attributes);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(CustodialTransaction::class, 'custodial_account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * @return int
     */
    public function balance($date = null)
    {
        $date = Carbon::parse($date);

        $trans_table = (new CustodialTransaction())->table;

        $trans =  DB::table($trans_table)
            ->select(DB::raw('sum(amount) as custodial_balance'))
            ->where('date', '<=', $date)
            ->where('custodial_account_id', $this->id)
            ->whereNull('deleted_at')
            ->groupBy('custodial_account_id')
            ->first();

        if (is_null($trans)) {
            return 0;
        }

        return (float) $trans->custodial_balance;
    }

    /**
     * @param $transaction
     * @return int
     */
    public function transactionBalance($transaction)
    {
        if (is_null($transaction)) {
            return 0;
        }

        $trans_table = (new CustodialTransaction())->getTable();

        $date = new Carbon($transaction->date);

        $trans =  DB::table($trans_table)
                ->select(DB::raw('sum(amount) as custodial_balance'))
                ->where('custodial_account_id', $this->id)
                ->where('date', '<', $date)
                ->whereNull('deleted_at')
                ->groupBy('custodial_account_id')
                ->first();

        $today = CustodialTransaction::where('date', $date)->where('custodial_account_id', $this->id)
                                        ->where('id', '<=', $transaction->id)
                                        ->whereNull('deleted_at')
                                        ->sum('amount');

        if (is_null($trans)) {
            return 0 + $today;
        }

        return $trans->custodial_balance+$today;
    }

    /**
     * @param $account
     * @return mixed
     */
    public function scopeForFundManager($account)
    {
        if (!$this->currentFundManager()) {
            return $account;
        }

        return $account->where('fund_manager_id', $this->currentFundManager()->id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balanceTrail()
    {
        return $this->hasMany(CustodialAccountBalanceTrail::class, 'account_id');
    }

    public function mpesaAccounts()
    {
        return $this->hasMany(MpesaAccount::class);
    }

    /**
     * @param $date
     * @return mixed
     */
    public function latestBankBalance($date = null)
    {
        $date = Carbon::parse($date);

        return $this->balanceTrail()->where('date', '<=', $date)->latest('date')->first();
    }

    /**
     * @param $query
     * @param bool  $condition
     * @return mixed
     */
    public function scopeActive($query, $condition = true)
    {
        if ($condition) {
            return $query->where('investment_account', 1);
        }

        return $query->where(
            function ($q) {
                $q->where('investment_account', 0)->orWhereNull('investment_account');
            }
        );
    }

    public function getFullNameAttribute()
    {
        $alias = $this->alias ? ' ('.$this->alias.')': '';
        
        $fm = $this->fundManager? $this->fundManager->name.' - ' : '';

        return $fm . $this->account_name." $this->account_no ".$alias;
    }

    public function accountProducts()
    {
        return $this->belongsToMany(
            Product::class,
            'products_projects_receiving_accounts',
            'account_id',
            'product_id'
        )->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany(
            Project::class,
            'products_projects_receiving_accounts',
            'account_id',
            'project_id'
        )->withTimestamps();
    }

    public function funds()
    {
        return $this->belongsToMany(
            UnitFund::class,
            'products_projects_receiving_accounts',
            'account_id',
            'unit_fund_id'
        )->withTimestamps();
    }

    public function shares()
    {
        return $this->belongsToMany(
            SharesEntity::class,
            'products_projects_receiving_accounts',
            'account_id',
            'entity_id'
        )->withTimestamps();
    }
}
