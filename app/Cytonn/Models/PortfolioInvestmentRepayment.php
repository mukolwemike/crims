<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Unitization\UnitFund;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 22/06/2016
 * Time: 12:20 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class PortfolioInvestmentRepayment extends BaseModel
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var string
     */
    protected $table = 'portfolio_investments_repayments';

    protected $dates = ['date'];

    /**
     * PortfolioInvestmentRepayment constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo(DepositHolding::class, 'investment_id');
    }

    /**
     * @param $query
     * @param $slug
     * @return mixed
     */
    public function scopeOfType($query, $slug)
    {
        return $query->whereHas(
            'type',
            function ($type) use ($slug) {
                $type->where('slug', $slug);
            }
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(PortfolioRepaymentType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(PortfolioTransactionApproval::class, 'approval_id');
    }

    public function scopeBefore($q, $date)
    {
        return $q->where('date', '<=', $date);
    }

    public function scopeForUnitFund($query, UnitFund $fund)
    {
        return $query->whereHas('investment', function ($q) use ($fund) {
            $q->where('unit_fund_id', $fund->id);
        });
    }

    public function scopeOffshore($query, $condition = false)
    {
        return $query->whereHas('investment', function ($q) use ($condition) {
            $q->whereHas('security', function ($q) use ($condition) {
                if ($condition) {
                    $q->where('offshore', 1);
                } else {
                    $q->where('offshore', '!=', 1);
                }
            });
        });
    }
}
