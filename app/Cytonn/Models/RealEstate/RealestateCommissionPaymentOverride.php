<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\RealEstate;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealestateCommission;
use Carbon\Carbon;
use Crims\Realestate\Commission\Models\RealEstateCommissionPaymentSchedule;
use Illuminate\Database\Eloquent\SoftDeletes;

class RealestateCommissionPaymentOverride extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'realestate_commission_payment_overrides';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commission_id',
        'schedule_id',
        'recipient_id',
        'date',
        'rate',
        'amount',
        'description',
        'schedule_amount',
        'report_id'
    ];

    /*
     * Relationship between a commission and commission payment override
     */
    public function commission()
    {
        return $this->belongsTo(RealestateCommission::class, 'commission_id');
    }

    /*
     * Relationship between a schedule and commission payment override
     */
    public function schedule()
    {
        return $this->belongsTo(RealEstateCommissionPaymentSchedule::class, 'schedule_id');
    }

    /*
     * Relationship between a recipient and commission payment override
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->where('recipient_id', $recipient->id);
    }

    public function scopeForReport($query, CommissionRecepient $recipient)
    {
        return $query->where('report_id', $recipient->id);
    }
}
