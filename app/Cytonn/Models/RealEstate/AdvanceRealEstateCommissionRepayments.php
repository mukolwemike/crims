<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\RealEstate;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvanceRealEstateCommissionRepayments extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'advance_real_estate_commission_repayments';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'advance_real_estate_commission_id',
        'date',
        'amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advanceCommission()
    {
        return $this->belongsTo(AdvanceRealEstateCommission::class, 'advance_real_estate_commission_id');
    }
}
