<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 04/02/2019
 * Time: 11:41
 */

namespace App\Cytonn\Models\RealEstate;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\RealEstateInstructionType;
use App\Cytonn\Models\UnitHolding;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class RealEstateInstruction extends BaseModel
{
    use SoftDeletes, SearchableTrait, PresentableTrait;

    protected $table = 'real_estate_instructions';

    protected $guarded = ['id'];

    public $repo;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    public function type()
    {
        return $this->belongsTo(RealEstateInstructionType::class, 'instruction_type_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function getPayloadAttribute($value)
    {
        return $value ? \GuzzleHttp\json_decode($value) : null;
    }

    public function scopeUploaded($query, $status)
    {
        if ($status === 'uploaded') {
            return $query->whereNotNull('document_id');
        }

        if ($status === 'not-uploaded') {
            return $query->whereNull('document_id');
        }

        return $query;
    }

    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }
}
