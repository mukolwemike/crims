<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\RealEstate;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Illuminate\Database\Eloquent\SoftDeletes;

class RealestateInterestPayment extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'realestate_interest_payments';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'holding_id',
        'payment_id',
        'client_payment_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(RealEstatePayment::class, 'payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientPayment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }
}
