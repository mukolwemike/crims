<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/29/19
 * Time: 12:15 PM
 */

namespace App\Cytonn\Models\RealEstate;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\User;

class RealEstateContractVariation extends BaseModel
{
    protected $table = 'real_estate_contract_variation';

    protected $guarded = ['id'];

    public function loo()
    {
        return $this->belongsTo(RealestateLetterOfOffer::class, 'loo_id');
    }

    public function prevLoo()
    {
        return $this->belongsTo(RealestateLetterOfOffer::class, 'prev_loo_id');
    }

    public function getLocation()
    {
        $config = \config('filesystems');

        $path = 'private';

        if ($config['default'] == 's3') {
            $path = 'private';
        }

        return $path . '/contract_variation';
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function approved()
    {
        if ($this->approval) {
            return true;
        }

        return false;
    }

    public function rejected()
    {
        if ($this->approved_by && $this->approval == 0) {
            return true;
        }

        return false;
    }

    public function updateSent(User $user)
    {
        $this->sent_by = $user->id;
        $this->sent_on = \Carbon\Carbon::now();
        $this->save();
    }
}
