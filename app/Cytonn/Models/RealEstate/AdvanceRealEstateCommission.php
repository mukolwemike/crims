<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\RealEstate;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\RealestateCommission;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Models\Investment\AdvanceCommissionType;
use Cytonn\Realestate\Commissions\AdvanceRealEstateCommissionPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class AdvanceRealEstateCommission extends BaseModel
{
    use SoftDeletes;
    use PresentableTrait;

    /*
     * Get the presenter
     */
    protected $presenter = AdvanceRealEstateCommissionPresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'advance_real_estate_commissions';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recipient_id',
        'date',
        'amount',
        'paid_on',
        'advance_commission_type_id',
        'holding_id',
        'description',
        'repayable',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitHolding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AdvanceCommissionType::class, 'advance_commission_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionRepayments()
    {
        return $this->hasMany(AdvanceRealEstateCommissionRepayments::class, 'advance_real_estate_commission_id');
    }

    /**
     * @param $query
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    /**
     * @param $query
     * @param CommissionRecepient $recipient
     * @return mixed
     */
    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->where('recipient_id', $recipient->id);
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeUnpaidAsAt($query, Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        return $query->where(function ($q) use ($date) {
            $q->whereNull('paid_on')->orWhere('paid_on', '>=', $date);
        });
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeRepayable($query, $condition = true)
    {
        if ($condition) {
            return $query->where('repayable', 1);
        }

        return $query->where('repayable', '!=', 1);
    }
}
