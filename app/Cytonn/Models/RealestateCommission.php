<?php

namespace App\Cytonn\Models;

use Cytonn\Realestate\Commissions\RealEstateCommissionPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class RealestateCommission extends BaseModel
{
    use SoftDeletes;
    use PresentableTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = RealEstateCommissionPresenter::class;

    public $table = 'realestate_commissions';

    protected $guarded = ['id'];

    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    public function realEstateCommissionPaymentSchedules()
    {
        return $this->hasMany(RealEstateCommissionPaymentSchedule::class, 'commission_id');
    }

    public function schedules()
    {
        return $this->realEstateCommissionPaymentSchedules();
    }

    public function rate()
    {
        return $this->belongsTo(RealestateCommissionRate::class, 'commission_rate_id');
    }

    public function scopeForClient($query, Client $client)
    {
        return $query->wherehas(
            'holding',
            function ($holding) use ($client) {
                $holding->where('client_id', $client->id);
            }
        );
    }
}
