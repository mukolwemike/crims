<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/26/18
 * Time: 9:24 AM
 */

namespace App\Cytonn\Models;

use Carbon\Carbon;

class ClientStandingOrder extends BaseModel
{
    protected $table = 'client_standing_orders';

    protected $guarded = ['id'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function type()
    {
        return $this->belongsTo(ClientStandingOrderType::class, 'type_id');
    }

    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function unit()
    {
        return $this->belongsTo(UnitHolding::class, 'unit_holding_id');
    }

    public function scopeOfType($query, $t)
    {
        return $query->whereHas('type', function ($type) use ($t) {
            $type->where('slug', $t);
        });
    }

    public function scopeActive($query, $active = true)
    {
        return $query->where('active', $active)->where(function ($q) {
            $q->whereNull('end_date')->orWhere('end_date', '<=', Carbon::today());
        });
    }
}
