<?php

namespace App\Cytonn\Models;

class Title extends BaseModel
{
    protected $table = 'titles';

    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany(User::class, 'user_id');
    }
}
