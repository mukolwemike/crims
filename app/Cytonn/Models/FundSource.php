<?php

namespace App\Cytonn\Models;

class FundSource extends BaseModel
{
    protected $table = 'funds_sources';

    protected $guarded = ['id'];
}
