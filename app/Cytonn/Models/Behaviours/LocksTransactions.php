<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 28/08/2018
 * Time: 15:48
 */

namespace App\Cytonn\Models\Behaviours;

use App\Cytonn\System\Locks\Facade\Lock;
use App\Cytonn\System\Locks\LockRecord;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

trait LocksTransactions
{
    protected function lockModelForUpdate(Model $model = null, $column = 'updated_at', $updateVal = 'carbon_now')
    {
        if (is_null($model)) {
            return false;
        }

        $mod = $model->where($model->getKeyName(), $model->getKey())
            ->lockForUpdate()
            ->get()
            ->first();

        $updateVal = $updateVal != 'carbon_now' ? : Carbon::now();

        !$mod ? :  $mod->update([$column => $updateVal]);

        return true;
    }

    /**
     * @param \Closure $closure
     * @param $lockKey
     * @param int $lockFor
     * @param int $lockWaitTimeout
     * @return mixed
     * @throws \Exception
     */
    protected function executeWithinAtomicLock(\Closure $closure, $lockKey, $lockFor = 120, $lockWaitTimeout = 0)
    {
        return Lock::executeWithinLock($closure, $lockKey, $lockFor, $lockWaitTimeout);
    }


    /**
     * @param $lockKey
     * @param int $lockFor
     * @param int $lockWaitTimeout
     * @return bool
     * @throws \Exception
     */
    protected function getAtomicLock($lockKey, $lockFor = 120, $lockWaitTimeout = 0)
    {
        return Lock::blockUntilLocked($lockKey, $lockFor, $lockWaitTimeout);
    }

    /**
     * @param $lockKey
     *
     * @return void
     */
    protected function releaseAtomicLock(LockRecord $lockKey = null)
    {
        if (!$lockKey) {
            return ;
        }
        
        Lock::releaseLock($lockKey);
    }
}
