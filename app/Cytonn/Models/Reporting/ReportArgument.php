<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 04/09/2018
 * Time: 12:14
 */

namespace App\Cytonn\Models\Reporting;

use App\Cytonn\Models\BaseModel;

class ReportArgument extends BaseModel
{
    protected $table = 'report_arguments';

    protected $guarded = ['id'];

    public function column()
    {
        return $this->belongsTo(ReportColumn::class, 'column_id');
    }
}
