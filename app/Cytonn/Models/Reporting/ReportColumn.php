<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 04/09/2018
 * Time: 12:00
 */

namespace App\Cytonn\Models\Reporting;

use App\Cytonn\Models\BaseModel;

class ReportColumn extends BaseModel
{
    protected $table = 'report_columns';

    protected $guarded = ['id'];

    public function report()
    {
        return $this->belongsTo(Report::class, 'report_id');
    }

    public function arguments()
    {
        return $this->hasMany(ReportArgument::class, 'column_id');
    }
}
