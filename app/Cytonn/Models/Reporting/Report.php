<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 04/09/2018
 * Time: 11:54
 */

namespace App\Cytonn\Models\Reporting;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends BaseModel
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function path()
    {
        return "/dashboard/investments/reports/$this->id";
    }

    public function export()
    {
        return "/dashboard/investments/reports/export/$this->id";
    }

    public function columns()
    {
        return $this->hasMany(ReportColumn::class, 'report_id');
    }

    public function filters()
    {
        return $this->hasMany(ReportFilter::class, 'report_id');
    }
}
