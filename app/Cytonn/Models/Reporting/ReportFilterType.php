<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 05/09/2018
 * Time: 11:46
 */

namespace App\Cytonn\Models\Reporting;

use App\Cytonn\Models\BaseModel;

class ReportFilterType extends BaseModel
{

    protected $guarded = ['id'];

    protected $table = 'report_filter_types';
}
