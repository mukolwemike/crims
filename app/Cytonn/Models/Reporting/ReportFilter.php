<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 05/09/2018
 * Time: 11:38
 */

namespace App\Cytonn\Models\Reporting;

use App\Cytonn\Models\BaseModel;

class ReportFilter extends BaseModel
{
    protected $guarded = ['id'];

    protected $table = 'report_filters';

    public function type()
    {
        return $this->belongsTo(ReportFilterType::class, 'type_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function getValueAttribute($value)
    {
        return unserialize($value);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = serialize($value);
    }
}
