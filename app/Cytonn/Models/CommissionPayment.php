<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 09/10/2015
 * Time: 3:49 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class CommissionPayment extends BaseModel
{
    use SoftDeletes;

    public $table = 'commission_payments';

    protected $guarded = ['id'];

    /**
     * CommissionPayment constructor.
     *
     * @param array $attrs
     */
    public function __construct(array $attrs = [])
    {
        parent::__construct($attrs);
    }

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }

    public function scopeInMonth($query, Carbon $month)
    {
        return $query->where(
            function ($q) use ($month) {
                $q->where('date', '>=', $month->copy()->startOfMonth())
                    ->where('date', '<=', $month->copy()->endOfMonth());
            }
        );
    }

    public function commissionRecipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
