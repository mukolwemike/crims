<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class BulkCommissionPayment extends BaseModel
{
    use SoftDeletes;

    protected $table = 'bulk_commission_payments';

    protected $guarded = ['id'];

    protected $dates = ['start', 'end'];

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function investmentsApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'investments_approval_id');
    }

    public function realEstateApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'realestate_approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitizationApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'unitization_approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sharesApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'shares_approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function combinedApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'combined_approval_id');
    }

    public function scopeIncludes($query, \Carbon\Carbon $date)
    {
        return $query->where('start', '<=', $date)->where('end', '>=', $date);
    }

    public function commissionDates(Carbon $date)
    {
        $bulk = static::includes($date)->first();

        if ($bulk) {
            $start = $bulk->start;
            $end = $bulk->end;
        } else {
            $start = static::latest()->first()->end->addDay();
            $end = Carbon::today();
        }

        return [
            'start' => $start,
            'end' => $end
        ];
    }

    public function overrideCutoffDates(Carbon $date)
    {
        $bulk = static::includes($date)->first();

        if ($bulk) {
            return [
                'start' => $bulk->start,
                'end' => $bulk->end
            ];
        }

        $bulk = static::latest()->first();

        if (is_null($bulk)) {
            return [
                'start' => Carbon::now()->startOfMonth(),
                'end' => Carbon::now()
            ];
        }

        if (! $this->isApproved($bulk)) {
            return [
                'start' => $bulk->start,
                'end' => $bulk->end
            ];
        }

        return [
            'start' => $bulk->end->addDay(),
            'end' => Carbon::now()
        ];
    }

    public function isApproved(BulkCommissionPayment $bulk)
    {
        if ($bulk->investmentsApproval && $bulk->investmentsApproval->approved == 1) {
            return true;
        }

        if ($bulk->combinedApproval && $bulk->combinedApproval->approved == 1) {
            return true;
        }

        return false;
    }
}
