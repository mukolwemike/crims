<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\Model;
use Cytonn\Cassandra\Eloquent\Model as CModel;

/**
 * This class should not extend BaseModel since that would cause an excessive nesting error
 * Class AccessLog
 */
class AccessLog extends CModel
{


    /**
     * @var string
     */
    protected $table = 'access_log';

    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * AccessLog constructor.
     */
    public function __construct(array $attrs = [])
    {
//        $this->connection = config('database.logging');
        $this->connection = 'cassandra';

        parent::__construct($attrs);
    }
}
