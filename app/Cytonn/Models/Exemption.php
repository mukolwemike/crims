<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

use Laracasts\Presenter\PresentableTrait;

class Exemption extends BaseModel
{
    use PresentableTrait;
    /*
     * Get the presenters
     */
    protected $presenter = 'Cytonn\System\Exemptions\ExemptionPresenter';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exemptions';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'client_transaction_approval_id',
        'portfolio_transaction_approval_id',
        'reason',
        'date'
    ];

    /*
     * Relationship between a exemption and client transaction approval
     */
    public function clientTransactionApproval()
    {
        return $this->belongsTo(
            ClientTransactionApproval::class,
            'client_transaction_approval_id'
        );
    }

    /*
     * Relationship between a exemption and portfolio transaction approval
     */
    public function portfolioTransactionApproval()
    {
        return $this->belongsTo(
            PortfolioTransactionApproval::class,
            'portfolio_transaction_approval_id'
        );
    }
}
