<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Client\Approvals\ApprovalUniqueCheck;
use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Handlers\EventDispatchTrait;
use Cytonn\Investment\FundManager\Traits\HasClientOrFundManagerTrait;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalRejection;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStage;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalStep;
use Cytonn\Models\Client\Approvals\ClientTransactionApprovalType;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientTransactionApprovalPresenter;
use Cytonn\SearchOperations\ElasticOperationsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;

class ClientTransactionApproval extends BaseModel
{
    use SoftDeletes, EventDispatchTrait, EventGenerator, HasClientOrFundManagerTrait, PresentableTrait;
    use ElasticOperationsTrait;

    public $table = 'client_transaction_approval';

    public $guarded = ['id'];

    protected $dates = ['approved_on'];

    protected $presenter = ClientTransactionApprovalPresenter::class;

    public function getIndexDocumentData()
    {
        $dataArray = [
            'id' => $this->id,
            'transaction_type' => $this->transaction_type,
            'payload' => $this->payload,
            'created_at' => Carbon::parse($this->created_at)->toDateTimeString(),
            'approved_on' => ($this->approved_on)
                ? Carbon::parse($this->approved_on)->toDateTimeString()
                : null,
            'client_id' => $this->client_id,
            'sent_by' => $this->sent_by,
            'approved' => $this->approved,
            'awaiting_stage_id' => $this->awaiting_stage_id
        ];

        $client = $this->client;

        if ($client) {
            $dataArray = array_merge(
                $dataArray,
                [
                    'client_code' => $client->client_code
                ]
            );

            $contact = $client->contact;

            if ($contact) {
                $dataArray = array_merge(
                    $dataArray,
                    [
                        'firstname' => $contact->firstname,
                        'lastname' => $contact->lastname,
                        'middlename' => $contact->middlename,
                        'corporate_registered_name' => $contact->corporate_registered_name,
                        'corporate_trade_name' => $contact->corporate_trade_name
                    ]
                );
            }

            $clientJointDetails = $client->jointDetail;

            $jointDetailArray = array();

            foreach ($clientJointDetails as $clientJointDetail) {
                $jointDetailArray['firstname_' . $clientJointDetail->id] = $clientJointDetail->firstname;
                $jointDetailArray['lastname_' . $clientJointDetail->id] = $clientJointDetail->lastname;
                $jointDetailArray['middlename_' . $clientJointDetail->id] = $clientJointDetail->middlename;
                $jointDetailArray['id_or_passport_' . $clientJointDetail->id] = $clientJointDetail->id_or_passport;
                $jointDetailArray['email_' . $clientJointDetail->id] = $clientJointDetail->email;
            }

            $dataArray = array_merge($dataArray, $jointDetailArray);
        }

        return $dataArray;
    }

    protected $mappingProperties = [
    ];

    public static function add($data)
    {
        isset($data['scheduled']) ? $scheduled = $data['scheduled'] : $scheduled = false;

        (new \Cytonn\Authorization\Authorizer())->checkAuthority('post_transactions');

        $approval = new static();

        $approval->client_id = $data['client_id'];

        if (array_key_exists('investment_id', $data)) {
            $approval->investment_id = $data['investment_id'];
        }

        $clientUser = true;

        if (\Auth::user() instanceof User) {
            $clientUser = false;
        }

        $approval->transaction_type = $data['transaction_type'];
        $approval->payload = $data['payload'];
        $approval->sent_by = $clientUser ? (getSystemUser() ? getSystemUser()->id : null) : \Auth::user()->id;
        $approval->client_user_id = $clientUser ? null : \Auth::user()->id;
        $approval->scheduled = $scheduled;

        if (isset($data['fm_id'])) {
            $approval->fm_id = $data['fm_id'];
        }

        $approval->save();

        return $approval;
    }

    public static function make($clientID, $transactionType, $payload, $scheduled = false, $sender_id = null)
    {
        !is_null($sender_id) ?: $sender_id = Auth::user() ? Auth::id() : getSystemUser()->id;

        (new \Cytonn\Authorization\Authorizer())->checkAuthority('post_transactions');

        $approval = new static();
        $approval->client_id = $clientID;
        $approval->transaction_type = $transactionType;
        $approval->payload = $payload;
        $approval->sent_by = $sender_id;
        $approval->scheduled = $scheduled;
        $approval->save();

        return $approval;
    }

    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function approvalDocuments()
    {
        return $this->hasMany(TransactionApprovalDocument::class, 'approval_id');
    }

    public function withdrawal()
    {
        return $this->belongsTo(ClientInvestmentWithdrawal::class, 'approval_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sent_by');
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'approved_by');
    }

    public function steps()
    {
        return $this->hasMany(ClientTransactionApprovalStep::class, 'approval_id');
    }

    public function exemption()
    {
        return $this->hasOne(Exemption::class, 'client_transaction_approval_id');
    }

    public function approve(User $user = null)
    {
        $fresh = $this->fresh();

        if ($fresh->approved || $fresh->approved_by || $fresh->approved_on) {
            throw new \InvalidArgumentException('Transaction already approved');
        }

        $this->approved = true;
        $this->approved_by = $user ? $user->id : Auth::id();
        $this->approved_on = \Carbon\Carbon::now();
        $this->save();

        ApprovalUniqueCheck::createForClient($this);

        $this->raise(new \Cytonn\Investment\Events\TransactionApproved($this));
        $this->dispatchEventsFor($this);

        return $this;
    }

    /**
     * @throws \App\Exceptions\CrimsException
     */
    public function systemExecute()
    {
        $approve = new Approval($this);

        $approve->systemExecute();
    }

    public function handler()
    {
        $className = ucfirst(camel_case($this->transaction_type));

        $namespace = 'Cytonn\Clients\Approvals\Handlers';

        $h = $namespace . '\\' . $className;

        return new $h($this);
    }


    public function resend()
    {
        $this->approved = false;
        $this->approved_by = null;
        $this->approved_on = null;
        $this->save();

        //reproduce notification
        $this->raise(new \Cytonn\Investment\Events\ClientTransactionApprovalRequested($this));
        $this->dispatchEventsFor($this);
    }

    public static function boot()
    {
        parent::boot();

        static::created(
            function ($model) {
                $type = $model->transaction_type;

                if (!ClientTransactionApprovalType::where('slug', $type)->exists()) {
                    ClientTransactionApprovalType::create(
                        [
                            'name' => ucfirst(str_replace('_', ' ', $type)),
                            'slug' => $type
                        ]
                    );
                }

                $firstStage = $model->type()->allStages()->first();
                $model->update(['awaiting_stage_id' => $firstStage->id]);

                $model->raise(new \Cytonn\Investment\Events\ClientTransactionApprovalRequested($model));
                $model->dispatchEventsFor($model);
            }
        );

        static::deleted(
            function ($model) {
                $model->raise(new \Cytonn\Investment\Events\TransactionApproved($model));
                $model->dispatchEventsFor($model);
            }
        );
    }

    public function scopeNotApproved($query)
    {
        return $query->where(
            function ($q) {
                $q->where('approved', 0)->orWhere('approved', null);
            }
        );
    }

    /**
     * @return ClientTransactionApprovalType|null|static
     */
    public function type()
    {
        return ClientTransactionApprovalType::where('slug', $this->transaction_type)->first();
    }

    public function awaitingStage()
    {
        return $this->belongsTo(ClientTransactionApprovalStage::class, 'awaiting_stage_id');
    }

    public function rejections()
    {
        return $this->hasMany(ClientTransactionApprovalRejection::class, 'approval_id');
    }

    public function scopeRejected($query)
    {
        return $query->whereHas(
            'rejections',
            function ($r) {
                $r->whereNull('resolved_by');
            }
        );
    }

    public function scopeNotRejected($query)
    {
        return $query->whereDoesntHave(
            'rejections',
            function ($r) {
                $r->whereNull('resolved_by');
            }
        );
    }

    public function topupForm()
    {
        return $this->hasOne(ClientTopupForm::class, 'approval_id');
    }

    public function withdrawInstruction()
    {
        return $this->hasOne(ClientInvestmentInstruction::class, 'approval_id');
    }

    public function clientPayment()
    {
        return $this->hasOne(ClientPayment::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function suspenseTransaction()
    {
        return $this->hasOne(SuspenseTransaction::class, 'client_transaction_approval_id');
    }

    public function url()
    {
        return "/dashboard/investments/approve/$this->id";
    }

    public function scopeScheduled($query)
    {
        return $query->where('scheduled', true)->whereNotNull('action_date')->whereNull('run_date');
    }

    /*
     *  for application shareholder and reserve unit
     */

    public function filledDocuments()
    {
        return $this->hasMany(REOTCFilledApplicationDocument::class, 'application_id');
    }

    public function emailIndemnity() //TODO refactor this method
    {
        $indemnityDoc = $this->filledDocuments()
            ? $this->filledDocuments()->whereHas('document', function ($document) {
                $document->ofType('email_indemnity');
            }) : null;

        if ($indemnityDoc) {
            return true;
        }

        return false;
    }
}
