<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 12/03/2016
 * Time: 10:20 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientDeduction extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletes, SearchableTrait;
    /**
     * @var string
     */
    public $table = 'client_deductions';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $dates = ['date'];

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.corporate_trade_name' => 10,
            'contacts.email' => 10,
            'clients.client_code' => 10
        ],
        'joins' => [
            'client_investments' => ['client_deductions.investment_id', 'client_investments.id'],
            'clients' => ['client_investments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * ClientDeduction constructor.
     * ClientDeduction constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * The investment deducted from
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    /**
     * @param $query
     * @param $start
     * @param $end
     * @return mixed
     */
    public function scopeBetweenDates($query, $start, $end)
    {
        $start = (new Carbon($start))->startOfDay();
        $end = (new Carbon($end))->endOfDay();

        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }
}
