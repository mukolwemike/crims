<?php
/**
 * Created by PhpStorm.
 * User: rkaranja
 * Date: 12/11/2018
 * Time: 14:49
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\Model;

class BankBranch extends Model
{
    protected $table = 'bank_branches';

    protected $guarded = ['id'];

    public function bank()
    {
        return $this->belongsTo(HrBank::class, 'bank_id');
    }
}
