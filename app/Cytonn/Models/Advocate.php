<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class Advocate
 */
class Advocate extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $table = 'advocates';

    protected $searchable = [
        'columns' => [
            'advocates.name'=>10,
            'advocates.postal_code'=>10,
            'advocates.postal_address'=>10,
            'advocates.street'=>10,
            'advocates.town'=>10,
            'advocates.email'=>10,
            'advocates.building'=>10,
            'advocates.telephone_office'=>10,
            'advocates.telephone_cell'=>10,
            'advocates.address'=>10,
            'countries.name'=>10,
            'projects.name'=>10,
        ],
        'joins' => [
            'countries'=>['advocates.country_id', 'countries.id'],
            'advocate_projects'=>['advocates.id', 'advocate_projects.advocate_id'],
            'projects'=>['projects.id', 'advocate_projects.project_id'],
        ],
        'groupBy' => 'advocates.id'
    ];

    /**
     * Inject the repo
     * Client constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add a new advocate from array of data
     * Add a new advocate
     *
     * @param  $advocate_data
     * @return $this
     */
    public function add($advocate_data)
    {
        $this->fill($advocate_data);
        $this->save();
        return $this;
    }

    /**
     * Get LOOs belonging to advocate
     *
     * @return mixed
     */
    public function loos()
    {
        return $this->hasMany(RealestateLetterOfOffer::class, 'advocate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectAssignments()
    {
        return $this->hasMany(AdvocateProject::class, 'advocate_id');
    }
}
