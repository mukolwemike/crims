<?php

namespace App\Cytonn\Models;

use Cytonn\Reporting\StatementCampaignRepository;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 09/03/2016
 * Time: 12:24 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class StatementCampaign extends BaseModel
{
    use SoftDeletes;

    const TYPE = 'Structured Products Statements';
    /**
     * @var StatementCampaignRepository
     */
    public $repo;


    /**
     * @var string
     */
    public $table = 'statement_campaigns';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['send_date', 'closed_on'];

    /**
     * StatementCampaign constructor.
     */
    public function __construct(array $data = [])
    {
        $this->repo = new StatementCampaignRepository($this);

        parent::__construct($data);
    }

    /**
     * Get each individual in campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(CampaignStatementItem::class, 'campaign_id');
    }

    /**
     * The campaign type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(StatementCampaignType::class, 'type_id');
    }

    /**
     * Close the campaign so that statements are not added anymore
     *
     * @return bool
     */
    public function close()
    {
        $this->closed = true;
        return $this->save();
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeClosed($query, $condition = true)
    {
        if ($condition) {
            return $query->where('closed', 1);
        }

        return $query->where(function ($q) {
            $q->whereNull('closed')->orWhere('closed', '!=', 1);
        });
    }
}
