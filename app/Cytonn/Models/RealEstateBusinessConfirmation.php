<?php

namespace App\Cytonn\Models;

/**
 * Date: 05/07/2016
 * Time: 12:39 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateBusinessConfirmation extends BaseModel
{
    protected $table = 'realestate_business_confirmations';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(RealEstatePayment::class, 'payment_id');
    }

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }
}
