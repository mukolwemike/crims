<?php

namespace App\Cytonn\Models;

use Cytonn\Realestate\Tranches\RealEstateUnitTrancheRepository;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 17/06/2016
 * Time: 6:02 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateUnitTranche extends BaseModel
{
    use SoftDeletes;

    protected $table = 'realestate_unit_tranches';

    protected $guarded = ['id'];

    public $repo;

    /**
     * RealEstateUnitTranche constructor.
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new RealEstateUnitTrancheRepository($this);
    }

    public function sizes()
    {
        return $this->hasMany(RealEstateUnitTrancheSizing::class, 'tranche_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function holdings()
    {
        return $this->hasMany(UnitHolding::class, 'tranche_id');
    }

    public function prices()
    {
        return $this->hasMany(RealEstateUnitTranchePricing::class, 'tranche_id');
    }
}
