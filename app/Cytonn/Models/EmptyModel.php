<?php
/**
 * Date: 25/02/2018
 * Time: 08:26
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Cytonn\Models;

/**
 * Class EmptyModel
 */
class EmptyModel extends BaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];
}
