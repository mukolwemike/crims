<?php

namespace App\Cytonn\Models;

/**
 * Date: 9/8/15
 * Time: 4:04 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ClientInvestmentApplicationCompletenessCheck extends BaseModel
{
    public $table = 'client_investment_application_completeness_check';

    public $guarded = ['id'];
}
