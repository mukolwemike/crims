<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class ClientBankBranch
 */
class ClientBankBranch extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $table = 'client_bank_branches';

    protected $searchable = [
        'columns' => [
            'client_bank_branches.name' => 10,
            'client_bank_branches.swift_code' => 10,
            'client_bank_branches.branch_code' => 10,
        ]
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(ClientBank::class, 'bank_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bankAccounts()
    {
        return $this->hasMany(ClientBankAccount::class, 'branch_id');
    }
}
