<?php

namespace App\Cytonn\Models;

/**
 * Date: 12/08/2016
 * Time: 9:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class AuditConfirmation extends BaseModel
{
    protected $fillable = [
        'batch',
        'client_code',
        'currency',
        'balance',
        'sent',
        'spv',
        'product',
        'name'
    ];
}
