<?php

namespace App\Cytonn\Models\System;

use App\Cytonn\Models\BaseModel;

/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 29/04/2019
 * Time: 15:28
 */

class Channel extends BaseModel
{
    protected $table = "channels";
}
