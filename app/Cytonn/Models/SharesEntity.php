<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class SharesEntity
 */
class SharesEntity extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'shares_entities';

    protected $guarded = ['id'];

    protected $repo;
    /**
     * @var array
     */
    private $attrs;

    /**
     * SharesEntity constructor.
     *
     * @param $repo
     */
    public function __construct(array $attrs = [])
    {
        parent::__construct($attrs);

        $this->repo = new \Cytonn\Shares\ShareEntityRepository($this);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function priceTrails()
    {
        return $this->hasMany(SharesPriceTrail::class, 'entity_id');
    }

    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'account_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function receivingAccounts()
    {
        return $this->belongsToMany(
            CustodialAccount::class,
            'products_projects_receiving_accounts',
            'entity_id',
            'account_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function issues()
    {
        return $this->hasMany(SharesEntityIssue::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(CustodialAccount::class, 'account_id');
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function maximumIssues($date = null)
    {
        $date = \Carbon\Carbon::parse($date);

        return $this->issues()->where('issue_date', '<=', $date)->sum('number');
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function sharePrice($date = null)
    {
        $date = Carbon::parse($date);

        return $this->repo->pricePerTrail($date);
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function soldShares($date = null)
    {
        $holdings = ShareHolding::where(['entity_id' => $this->id, 'shares_sales_order_id' => null]);

        if (is_null($date)) {
            return $holdings->sum('number');
        }

        return $holdings->where('date', '<=', $date)->sum('number');
    }

    /**
     * @param null $date
     * @return int|mixed
     */
    public function remainingShares($date = null)
    {
        return $this->maximumIssues($date) - $this->soldShares($date);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(ClientPayment::class, 'share_entity_id');
    }
}
