<?php

namespace App\Cytonn\Models;

use Cytonn\Presenters\AmountPresenter;

/**
 * Date: 17/06/2016
 * Time: 6:03 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateUnitTranchePricing extends BaseModel
{
    protected $table = 'realestate_unit_tranche_pricing';

    protected $guarded = ['id'];

    public function paymentPlan()
    {
        return $this->belongsTo(RealEstatePaymentPlan::class, 'payment_plan_id');
    }

    public function size()
    {
        return $this->belongsTo(RealEstateUnitTrancheSizing::class, 'sizing_id');
    }

    public function getTrancheAndPriceAttribute()
    {
        return $this->size->tranche->name . ' - ' . AmountPresenter::currency($this->price);
    }
}
