<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Author: Edwin Mukiri
 * Email: emukiri@cytonn.com / edwinmukiri@gmail.com
 * Date: 11/21/15
 * Time: 6:14 PM
 */
class ClientScheduledTransaction extends BaseModel
{
    use SoftDeletes;
    /**
     * @var string
     */
    public $table = 'client_scheduled_transactions';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    /**
     * Add a new schedule
     *
     * @param  array $data
     * @return static
     */
    public static function add($data)
    {
        $transaction = new static();
        $transaction->investment_id = $data['investment_id'];
        $transaction->done = false;
        $transaction->data = $data['data'];
        $transaction->action = $data['action'];
        $transaction->time = $data['time'];
        $transaction->save();

        return $transaction;
    }

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }
}
