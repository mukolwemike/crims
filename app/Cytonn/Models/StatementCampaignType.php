<?php

namespace App\Cytonn\Models;

/**
 * Date: 29/06/2016
 * Time: 9:47 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class StatementCampaignType extends BaseModel
{
    protected $table = 'statement_campaign_types';

    protected $guarded = ['id'];
}
