<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Investment\FundManager\FundManagerScopingTrait;
use Cytonn\Investment\Products\ProductPresenter;
use Cytonn\Investment\Products\ProductRepository;
use Cytonn\Models\Investment\PeriodicCommissionRateUpdate;
use Laracasts\Presenter\PresentableTrait;

class Product extends BaseModel
{
    use FundManagerScopingTrait, PresentableTrait;

    protected $presenter = ProductPresenter::class;

    protected $table = 'products';

    /**
     * Fields that should not be mass assigned
     * @var array
     */
    protected $guarded = array('id', 'created_at', 'updated_at');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'currency_id',
        'custodial_account_id',
        'fund_manager_id',
        'shortname',
        'longname',
        'type_id',
        'active'
    ];

    public $repo;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new ProductRepository($this);
    }

    public function applications()
    {
        return $this->hasMany(ClientInvestment::class);
    }

    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'custodial_account_id');
    }

    public function receivingAccounts()
    {
        return $this->belongsToMany(
            CustodialAccount::class,
            'products_projects_receiving_accounts',
            'product_id',
            'account_id'
        );
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    public function scopeForFundManager($product)
    {
        if (!$this->currentFundManager()) {
            return $product;
        }

        return $product->where('fund_manager_id', $this->currentFundManager()->id);
    }

    public function type()
    {
        return $this->belongsTo(ProductType::class, 'type_id');
    }

    public function investments()
    {
        return $this->hasMany(ClientInvestment::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(ClientPayment::class, 'product_id');
    }

    public function interestRateUpdates()
    {
        return $this->hasMany(InterestRateUpdate::class, 'product_id');
    }

    public function rates()
    {
        return $this->hasMany(Rate::class, 'product_id');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionRates()
    {
        return $this->hasMany(CommissionRate::class, 'product_id');
    }

    public function productDocuments()
    {
        return $this->hasMany(ProductDocument::class, 'product_id');
    }

    /**
     * @param $tenor
     * @param null $date
     * @param int $amount
     * @return mixed
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function rate($tenor, $date = null, $amount = 0)
    {
        $date = \Carbon\Carbon::parse($date);

        $rates = $this->interestRateUpdates()->where('date', '<=', $date)
            ->latest('date')
            ->first();

        if (!$rates) {
            throw new \Cytonn\Exceptions\CRIMSGeneralException(
                "Please set up interest rates for " . $this->name
            );
        }

        $rate = $rates->rates()
            ->where('tenor', '<=', $tenor)
            ->where('min_amount', '<=', $amount)
            ->orderBy('tenor', 'DESC')
            ->orderBy('min_amount', 'DESC')
            ->first();

        if ($rate) {
            return $rate->rate;
        }

        $rate = $rates->rates()
            ->where('tenor', '>=', $tenor)
            ->where('min_amount', '<=', $amount)
            ->orderBy('tenor', 'ASC')
            ->orderBy('min_amount', 'DESC')
            ->first();

        if ($rate) {
            return $rate->rate;
        }

        $record = $rates->rates()
            ->where('min_amount', '<=', $amount)
            ->orderBy('min_amount', 'DESC')
            ->where('tenor', '<=', $tenor)
            ->get()
            ->groupBy('min_amount')
            ->first();

        if (!$record) {
            throw new \Cytonn\Exceptions\CRIMSGeneralException(
                "There are no rates for " . $this->name . " $tenor months " . $amount . " amount " .
                $date->toDateString() . " date"
            );
        }

        $record = $record->sortByDesc('tenor')->first();

        if (!$record) {
            throw new \Cytonn\Exceptions\CRIMSGeneralException(
                "There are no rates for " . $this->name . " $tenor months"
            );
        }

        return $record->rate;
    }

    public function scopeActive($query, $state = true)
    {
        if ($state) {
            return $query->where('active', 1);
        }
        return $query->where('active', 0);
    }

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'fund_id');
    }

    public function scopeForFund($query, UnitFund $fund)
    {
        return $query->where('fund_id', $fund->id);
    }

    public function operatingAccounts()
    {
        return $this->belongsToMany(
            CustodialAccount::class,
            'unit_fund_operating_accounts',
            'product_id',
            'account_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function periodicRateUpdates()
    {
        return $this->hasMany(PeriodicCommissionRateUpdate::class);
    }

    /**
     * @param Carbon|null $date
     * @return \Illuminate\Support\Collection|mixed
     */
    public function periodicRatesAsAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $update = $this->periodicRateUpdates()->where('date', '<=', $date)
            ->latest('date')->first();

        if (is_null($update)) {
            return collect([]);
        }

        return $update->rates()->orderBy('tenor')->get();
    }
}
