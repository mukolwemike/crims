<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Cytonn\Authorization\Authorizable as CytonnAuthorization;
use Cytonn\Authorization\AuthorizableImpl;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class User
 */
class User extends BaseModel implements
    AuthorizableContract,
    AuthenticatableContract,
    CytonnAuthorization
{
    use EventGenerator, AuthorizableImpl, Notifiable, Authenticatable, Authorizable;

    public $repo;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $guarded = ['id', 'password', 'group_id'];

    protected $dates = ['expiry_date'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'login_attempt', 'one_time_key', 'remember_token'];

    /**
     * @var \Cytonn\Authentication\AuthRepository
     */
    public $authRepository;

    /**
     * User constructor.
     * Inject the AuthRepository
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->authRepository = new \Cytonn\Authentication\AuthRepository($this);

        $this->repo = new \Cytonn\Users\UserRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function staff()
    {
        return $this->hasOne(Staff::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client()
    {
        return $this->hasOne(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function title()
    {
        return $this->belongsTo(Title::class, 'title_id');
    }

    /**
     * @param $password
     * passwords must always be hashed
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->firstname . ' ' . $this->middlename . ' ' . $this->lastname;
    }

    public function scopeActive()
    {
        return User::where(
            function ($q) {
                $q->where('active', 1)->whereNull('expiry_date')->orWhere('expiry_date', '>=', Carbon::now());
            }
        );
    }

    /**
     * @param array $userdata
     */
    public function add($userdata)
    {
        $this->fill($userdata);

        $this->save();

        return $this;
    }

    public function notifications()
    {
        return $this->belongsToMany(Notification::class, 'notifications_audience', 'user_id', 'notification_id');
    }

    public function expired()
    {
        if (!$this->expiry_date) {
            return false;
        }

        return $this->expiry_date->lt(\Carbon\Carbon::now());
    }
}
