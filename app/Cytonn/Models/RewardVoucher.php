<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class RewardVoucher extends BaseModel
{
    use SoftDeletes;

    public $table = 'reward_vouchers';

    protected $guarded = ['id'];

    protected $fillable = ['name', 'description', 'value'];

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }
}
