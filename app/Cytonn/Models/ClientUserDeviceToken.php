<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 7/16/18
 * Time: 12:57 PM
 */

namespace App\Cytonn\Models;

class ClientUserDeviceToken extends BaseModel
{
    public $table = 'client_user_device_tokens';

    public $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_user_id',
        'platform',
        'device_fcm_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientUser()
    {
        return $this->belongsTo(ClientUser::class);
    }
}
