<?php

namespace App\Cytonn\Models;

/**
 * Date: 9/6/15
 * Time: 10:41 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ClientType extends BaseModel
{
    public $table = 'client_type';
}
