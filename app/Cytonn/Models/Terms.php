<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 31/08/2018
 * Time: 09:28
 */

namespace App\Cytonn\Models;

class Terms extends BaseModel
{
    protected $guarded = ['id'];

    protected $table = 'terms';

    public function scopeIsActive($query, $active = true)
    {
        if (!$active) {
            return $query->where('active', null)->orWhere('active', 0);
        }

        return $query->where('active', 1);
    }

    public function scopeOfType($query, $type = 'topup')
    {
        return $query->where('action', $type);
    }
}
