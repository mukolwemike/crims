<?php

namespace App\Cytonn\Models;

/**
 * Date: 14/10/2015
 * Time: 11:58 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class StatementQuote extends BaseModel
{
    public $table = 'statement_quotes';

    protected $guarded = ['id'];
}
