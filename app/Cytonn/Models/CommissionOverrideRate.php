<?php
/**
 * Date: 18/09/2017
 * Time: 23:19
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionOverrideStructure;

class CommissionOverrideRate extends BaseModel
{
    protected $table = 'commission_override_rates';

    /*
     * Relationship between commission override structure and commission override rates
     */
    public function structure()
    {
        return $this->belongsTo(CommissionOverrideStructure::class, 'structure_id');
    }
}
