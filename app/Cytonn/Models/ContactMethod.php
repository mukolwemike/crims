<?php

namespace App\Cytonn\Models;

class ContactMethod extends BaseModel
{
    protected $table = 'contact_method';

    protected $guarded = ['id'];
}
