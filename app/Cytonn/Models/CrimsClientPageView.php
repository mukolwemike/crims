<?php
/**
 * Date: 14/07/2017
 * Time: 18:19
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use Cytonn\Cassandra\Eloquent\Model;

class CrimsClientPageView extends Model
{
    public function __construct(array $attrs = [])
    {
        $this->connection = 'cassandra';

        parent::__construct($attrs);
    }
}
