<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Crims\Commission\CommissionScheduleTrait;
use Crims\Investments\Commission\OverrideCalculator;
use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommissionPaymentSchedule extends BaseModel
{
    use SoftDeletes;
    use CommissionScheduleTrait;

    /**
     * @var string
     */
    public $table = 'commission_payment_schedule';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['date'];

    /**
     * CommissionPaymentSchedule constructor.
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commission()
    {
        return $this->belongsTo(Commission::class, 'commission_id');
    }

    /**
     * @return mixed
     */
    public function recipient()
    {
        return $this->commission->recipient;
    }

    /**
     * @return bool
     */
    public function pay()
    {
        $this->paid = true;
        $this->paid_at = Carbon::now();

        return $this->save();
    }

    /**
     * @return mixed
     */
    public function investment()
    {
        return $this->commission->investment();
    }

    public function scopeBetween($query, DateTime $start, DateTime $end)
    {
        return $query->where('date', '>=', $start)
            ->where('date', '<=', $end);
    }

    public function scopeNormal($query, $condition = true)
    {
        if ($condition) {
            return $query->where('commission_payment_schedule_type_id', 1);
        }

        return $query->where('commission_payment_schedule_type_id', '!=', 1);
    }

    public function scopeScheduleType($query, $type)
    {
        return $query->where('commission_payment_schedule_type_id', $type);
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeClawBack($query, $condition = true)
    {
        if ($condition) {
            return $query->where('claw_back', true);
        }

        return $query->where(
            function ($q) {
                $q->whereNull('claw_back')->orWhere('claw_back', false);
            }
        );
    }

    /**
     * @param $query
     * @param array $clients
     * @return mixed
     */
    public function scopeClients($query, array $clients)
    {
        return $query->whereHas(
            'commission',
            function ($commission) use ($clients) {
                $commission->whereHas(
                    'investment',
                    function ($investment) use ($clients) {
                        $investment->whereIn('client_id', $clients);
                    }
                );
            }
        );
    }

    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->whereHas('commission', function ($query) use ($recipient) {
            $query->where('recipient_id', $recipient->id);
        });
    }

    /**
     * @param $query
     * @param $date
     * @return mixed
     */
    public function scopeInMonth($query, $date)
    {
        $date = new \Carbon\Carbon($date);

        return $query->where('date', '>=', $date->copy()->startOfMonth())
            ->where('date', '<=', $date->copy()->endOfMonth());
    }

    /**
     * @param Carbon|null $date
     * @return OverrideCalculator
     */
    public function overrideCalculator(Carbon $date = null)
    {
        $date = $date ? $date : $this->getOverrideDate();

        return new OverrideCalculator($date, $this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overrideSchedules()
    {
        return $this->hasMany(CommissionPaymentOverride::class, 'schedule_id');
    }

    /**
     * @throws CrimsException
     */
    public function generateOverrides()
    {
        $this->overrideCalculator()->awardOverrides();
    }

    /**
     * @return Carbon
     * @throws CrimsException
     */
    public function getOverrideDate()
    {
        $investment = @$this->commission->investment;

        if (is_null($investment)) {
            throw new CrimsException("No investment linked to commission schedule id : " . $this->id);
        }

        return Carbon::parse($investment->invested_date);
    }
}
