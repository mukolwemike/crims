<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/19/18
 * Time: 4:43 PM
 */

namespace App\Cytonn\Models;

use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class WithholdingTax extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'investments_withholding_tax';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'clients2.client_code' => 10
        ],

        'joins' => [
            'client_investment_withdrawals' => [
                'investments_withholding_tax.client_withdrawal_id',
                'client_investment_withdrawals.id'
            ],
            'client_investments' => [
                'client_investment_withdrawals.investment_id',
                'client_investments.id'
            ],
            'clients' => [
                'client_investments.client_id',
                'clients.id'
            ],
            'unit_fund_sales' => [
                'investments_withholding_tax.unit_fund_sale_id',
                'unit_fund_sales.id'
            ],
            'clients2' => [
                'unit_fund_sales.client_id',
                'clients.id'
            ]
        ],
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function withdrawal()
    {
        return $this->belongsTo(ClientInvestmentWithdrawal::class, 'client_withdrawal_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitFundSale()
    {
        return $this->belongsTo(UnitFundSale::class, 'unit_fund_sale_id');
    }

    /**
     * @param $query
     * @param null $custodial_account_id
     * @return mixed
     */
    public function scopeForAccount($query, $custodial_account_id = null)
    {
        if (!$custodial_account_id) {
            return $query;
        }

        return $query->where(function ($q) use ($custodial_account_id) {
            $q->whereHas('withdrawal', function ($q) use ($custodial_account_id) {
                $q->whereHas('investment', function ($q) use ($custodial_account_id) {
                    $q->whereHas('product', function ($q) use ($custodial_account_id) {
                        $q->where('custodial_account_id', $custodial_account_id);
                    });
                });
            })->orWhereHas('unitFundSale', function ($q) use ($custodial_account_id) {
                $q->whereHas('fund', function ($q) use ($custodial_account_id) {
                    $q->whereHas('receivingAccounts', function ($q) use ($custodial_account_id) {
                        $q->where('account_id', $custodial_account_id);
                    });
                });
            });
        });
    }

    /**
     * @param $query
     * @param null $client_code
     * @return mixed
     */
    public function scopeForClient($query, $client_code = null)
    {
        if (!$client_code) {
            return $query;
        }

        return $query->where(function ($q) use ($client_code) {
            $q->whereHas('withdrawal', function ($q) use ($client_code) {
                $q->whereHas('investment', function ($q) use ($client_code) {
                    $q->whereHas('client', function ($q) use ($client_code) {
                        $q->where('client_code', $client_code);
                    });
                });
            })->orWhereHas('unitFundSale', function ($q) use ($client_code) {
                $q->whereHas('client', function ($q) use ($client_code) {
                    $q->where('client_code', $client_code);
                });
            });
        });
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopePaid($query, $condition = true)
    {
        if ($condition) {
            return $query->where('paid', 1);
        }

        return $query->whereNull('paid');
    }

    /**
     * @param $query
     * @param $start
     * @param $end
     * @return mixed
     */
    public function scopeBetween($query, $start, $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }
}
