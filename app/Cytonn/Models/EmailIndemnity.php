<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 14/07/2016
 * Time: 10:27 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class EmailIndemnity extends BaseModel
{
    use SoftDeletes;

    protected $table = 'email_indemnity';

    protected $guarded = ['id'];

    /**
     * @return bool
     */
    public function status()
    {
        $status = (bool)$this->deactivated ? false : true;

        return $status;
    }
}
