<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 11/01/2016
 * Time: 6:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */
class ClientBankAccount extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string
     */
    public $table = 'client_bank_accounts';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The client who owns the account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return string
     */
    public function getAccountNameAndNumberAttribute()
    {
        $bank_name = ($this->branch) ? $this->branch->bank->name : $this->bank_name;

        return $bank_name . ' - ' . $this->account_name . ' - ' . $this->account_number;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(ClientBankBranch::class, 'branch_id');
    }

    /**
     * @return mixed
     */
    public function bank()
    {
        return $this->branch->bank();
    }

    public function clientBankBranch()
    {
        return $this->branch();
    }

    public function clientBank()
    {
        return $this->clientBankBranch ? $this->clientBankBranch->bank() : null;
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function scopeActive($query, $condition = true)
    {
        if ($condition) {
            return $query->where('active', 1);
        }

        return $query->whereNull('active');
    }
}
