<?php

namespace App\Cytonn\Models;

use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 26/10/2015
 * Time: 12:41 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class InterestPayment extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string
     */
    public $table = 'interest_payments';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['date_paid'];

    /**
     * InterestPayment constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function paymentOut()
    {
        return $this->belongsTo(ClientPayment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function action()
    {
        return $this->belongsTo(InterestAction::class, 'action_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    public function scopeBetween($query, DateTime $start, DateTime $end)
    {
        return $query->where('date_paid', '>=', $start)->where('date_paid', '<=', $end);
    }

    public function scopeBefore($query, \Carbon\Carbon $date)
    {
        return $query->where('date_paid', '<=', $date);
    }

    public function scopeActionSlug($query, $slug)
    {
        return $query->whereHas(
            'action',
            function ($action) use ($slug) {
                $action->where('slug', $slug);
            }
        );
    }
}
