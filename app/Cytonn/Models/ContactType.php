<?php

namespace App\Cytonn\Models;

class ContactType extends BaseModel
{
    protected $table = 'contact_types';

    protected $guarded = ['id'];
}
