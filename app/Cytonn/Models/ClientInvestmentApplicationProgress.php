<?php

namespace App\Cytonn\Models;

class ClientInvestmentApplicationProgress extends BaseModel
{
    protected $table = 'client_investment_application_progress';

    protected $guarded= ['id'];
}
