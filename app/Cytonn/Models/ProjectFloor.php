<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class ProjectFloor
 * @package App\Cytonn\Models
 */
class ProjectFloor extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var string
     */
    protected $table = 'project_floors';

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'floors.floor' => 10,
            'projects.name' => 10,
        ],
        'joins' => [
            'floors' => ['project_floors.floor_id', 'floors.id'],
            'projects' => ['project_floors.project_id', 'projects.id'],
        ],
        'groupBy' => 'project_floors.id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function floor()
    {
        return $this->belongsTo(Floor::class, 'floor_id');
    }
}
