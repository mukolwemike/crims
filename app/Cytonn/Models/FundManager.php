<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Investment\FundManager\FundManagerRepository;
use Illuminate\Support\Facades\App;

/**
 * Class FundManager
 */
class FundManager extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'fund_managers';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    public $repo;

    /**
     * FundManager constructor.
     *
     * @param    array $attributes
     * @internal param string $table
     */
    public function __construct(array  $attributes = [])
    {
        $this->repo = new FundManagerRepository($this);

        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'fund_manager_id');
    }

    public function securities()
    {
        return $this->hasMany(PortfolioSecurity::class, 'fund_manager_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shareEntities()
    {
        return $this->hasMany(SharesEntity::class, 'fund_manager_id');
    }

    public function defaultEntity()
    {
        return $this->hasOne(SharesEntity::class, 'fund_manager_id')
            ->where('default_for', $this->id);
    }

    public function custodialAccounts()
    {
        return $this->hasMany(CustodialAccount::class, 'fund_manager_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function modules()
    {
        return $this->belongsToMany(Module::class, 'fund_manager_module');
    }

    public function complianceChecklist()
    {
        return $this->belongsToMany(
            ClientComplianceChecklist::class,
            'fund_managers_compliance_checklist',
            'fund_manager_id',
            'compliance_document_id'
        );
    }

    public function unitFunds()
    {
        return $this->hasMany(UnitFund::class, 'fund_manager_id');
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasModule($name)
    {
        if (App::environment('testing')) {
            return true;
        }

        return $this->modules()->where('name', $name)->count() > 0;
    }
}
