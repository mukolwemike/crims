<?php

namespace App\Cytonn\Models;

use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;

class RealEstatePaymentPlan extends BaseModel
{
    use EventGenerator, PresentableTrait;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var string
     */
    protected $table = 'realestate_payment_plans';
}
