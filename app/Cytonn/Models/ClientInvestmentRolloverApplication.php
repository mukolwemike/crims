<?php

namespace App\Cytonn\Models;

class ClientInvestmentRolloverApplication extends BaseModel
{
    protected $table = 'client_investment_rollover_application';


    protected $guarded = ['id'];


    protected function initialApplication()
    {
        return $this->belongsTo('App\ClientInvestmentApplication', 'initial_application_id');
    }
}
