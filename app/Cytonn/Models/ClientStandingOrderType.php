<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/26/18
 * Time: 9:59 AM
 */

namespace App\Cytonn\Models;

class ClientStandingOrderType extends BaseModel
{
    protected $table = 'client_standing_order_types';

    protected $guarded = ['id'];
}
