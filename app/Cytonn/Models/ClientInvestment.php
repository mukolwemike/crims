<?php

namespace App\Cytonn\Models;

use App\Cytonn\Clients\ClientLoyaltyPoints\InvestmentLoyaltyCalculator;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Handlers\EventDispatchTrait;
use Cytonn\Investment\BankDetails;
use Cytonn\Investment\Calculate\Calculator;
use Cytonn\Investment\CalculatorTrait;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Investment\FundManager\Traits\InvestmentFundManagerScope;
use Cytonn\Investment\InvestmentsRepository;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Cytonn\Models\Investment\InvestmentPaymentScheduleDetail;
use Cytonn\Models\StructuredInvestment;
use Cytonn\Reporting\Custom\Models\ModelPresenters\ClientInvestmentReport;
use Cytonn\Reporting\Custom\Models\ReportableModel;
use Cytonn\Uuids\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientInvestment extends StructuredInvestment
{
    use CalculatorTrait, EventGenerator, EventDispatchTrait, SoftDeletes;

    use  SearchableTrait, UuidTrait, InvestmentFundManagerScope, PresentableTrait, ReportableModel;

    public $table = 'client_investments';

    protected $presenter = 'Cytonn\Investment\ClientInvestments\ClientInvestmentPresenter';

    protected $guarded = ['id'];

    protected $with = ['client', 'client.taxExemptions'];

    public $repo;

    public $commissionRepo;

    protected $reportPresenter = ClientInvestmentReport::class;

    protected $dates = ['invested_date', 'maturity_date', 'withdrawal_date'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10
        ],
        'joins' => [
            'clients' => ['client_investments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id']
        ],
        'groupBy' => 'client_investments.id'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new InvestmentsRepository($this);

        $this->commissionRepo = new CommissionRepository();
    }

    public function path()
    {
        return "/investments/$this->uuid";
    }

    public function calculate(
        Carbon $date = null,
        $as_at_next_day = false,
        $interestRate = null,
        $expanded = false,
        $fresh = false
    ) {
        $date = Carbon::parse($date);

        return new  Calculator($this, $date, $as_at_next_day, $interestRate, $expanded, $fresh);
    }

    /**
     * @param Carbon|null $date
     * @param Carbon|null $start
     * @return InvestmentLoyaltyCalculator
     */
    public function calculateLoyalty(
        Carbon $date = null,
        Carbon $start = null
    ) {
        $date = Carbon::parse($date);

        return new  InvestmentLoyaltyCalculator($this, $date, $start);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function payments()
    {
        return $this->hasMany(InterestPayment::class, 'investment_id');
    }

    public function taxRecord()
    {
        return $this->belongsTo(ClientTax::class, 'investment_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function productPlan()
    {
        return $this->belongsTo(ProductPlan::class, 'product_plan_id');
    }

    public function application()
    {
        return $this->belongsTo(ClientInvestmentApplication::class, 'application_id');
    }

    public function commissionPayments()
    {
        return $this->hasMany(CommissionPayment::class, 'investment_id');
    }

    public function commission()
    {
        return $this->hasOne(Commission::class, 'investment_id');
    }

    public function investmentType()
    {
        return $this->belongsTo(ClientInvestmentType::class, 'investment_type_id');
    }

    public function interestAction()
    {
        return $this->belongsTo(InterestAction::class, 'interest_action_id');
    }

    public function interestPayments()
    {
        return $this->hasMany(InterestPayment::class, 'investment_id');
    }

    public function withdrawals()
    {
        return $this->hasMany(ClientInvestmentWithdrawal::class, 'investment_id');
    }

    public function topupsTo()
    {
        return $this->hasMany(ClientInvestmentTopup::class, 'topup_to');
    }

    public function topupsFrom()
    {
        return $this->hasMany(ClientInvestmentTopup::class, 'topup_from');
    }

    public function reinvestedFrom()
    {
        return $this->hasMany(ClientInvestmentWithdrawal::class, 'reinvested_to');
    }

    public function type()
    {
        return $this->investmentType();
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function withdrawApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'withdraw_approval_id');
    }

    public function schedule()
    {
        return $this->hasMany(ClientTransactionApproval::class, 'investment_id')
            ->where('scheduled', 1);
    }

    public function approvals()
    {
        return $this->hasMany(ClientTransactionApproval::class, 'investment_id');
    }

    public function confirmation()
    {
        return $this->hasMany(ClientBusinessConfirmation::class, 'investment_id');
    }

    public function descendant()
    {
        $desc = static::where('parent_id', $this->id)->first();

        if (is_null($desc)) {
            return false;
        }

        return $desc;
    }

    public function parent()
    {
        $parent = static::find($this->parent_id);

        if (is_null($parent)) {
            return false;
        }

        return $parent;
    }

    public function bankAccount()
    {
        return $this->belongsTo(ClientBankAccount::class, 'bank_account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interestSchedules()
    {
        return $this->hasMany(InterestPaymentSchedule::class, 'investment_id');
    }

    public function deductions()
    {
        return $this->hasMany(ClientDeduction::class, 'investment_id');
    }

    public function scopeActiveOnDate($query, $date)
    {
        $date = new Carbon($date);

        return $this->scopeActiveBetweenDates($query, $date->startOfDay(), $date->copy()->endOfDay());
    }

    public function scopeActiveBetweenDates($query, $start, $end)
    {
        return $query->where('invested_date', '<=', $end)
            ->where(
                function ($q) use ($start) {
                    $q->where(
                        function ($query) use ($start) {
                            $query->where('withdrawn', 1)->where('withdrawal_date', '>=', $start);
                        }
                    )->orWhere(
                        function ($query) use ($start) {
                            $query->where('maturity_date', '>=', $start)
                                ->where(
                                    function ($q) {
                                        $q->where('withdrawn', 0)->orWhereNull('withdrawn');
                                    }
                                );
                        }
                    );
                }
            );
    }

    public function scopeWithdrawnBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('withdrawal_date', '>=', $start)->where('withdrawal_date', '<=', $end);
    }

    public function scopeWithdrawnOn($query, Carbon $date)
    {
        return $query->where('withdrawn_on', '>=', $date->copy()->startOfDay())
            ->where('withdrawn_on', '<=', $date->copy()->endOfDay());
    }

    public function scopeWithdrawn($query)
    {
        return $this->scopeActive($query, false);
    }

    public function scopeInvestedBetweenDates($query, $start, $end)
    {
        $start = (new Carbon($start))->startOfDay();
        $end = (new Carbon($end))->endOfDay();

        return $this->scopeColumnBetween($query, 'invested_date', $start, $end);
    }

    public function scopeForProduct($query, Product $product)
    {
        return $query->whereHas('product', function ($prod) use ($product) {
            $prod->where('id', $product->id);
        });
    }

    public function custodialInvestTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_invest_transaction_id');
    }

    public function bankDetails()
    {
        return new BankDetails($this);
    }

    public function custodialWithdrawTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_withdraw_transaction_id');
    }

    public function paymentIn()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_in');
    }

    public function paymentOut()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_out');
    }

    public function withdrawInstruction()
    {
        return $this->belongsTo(BankInstruction::class, 'withdrawal_instruction_id');
    }

    public function instructions()
    {
        return $this->hasMany(ClientInvestmentInstruction::class, 'investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function investmentPaymentScheduleDetail()
    {
        return $this->hasOne(InvestmentPaymentScheduleDetail::class, 'investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function investmentPaymentSchedule()
    {
        return $this->hasOne(InvestmentPaymentSchedule::class, 'investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childInvestmentPaymentSchedules()
    {
        return $this->hasMany(InvestmentPaymentSchedule::class, 'parent_investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentInvestment()
    {
        return $this->belongsTo(ClientInvestment::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childInvestments()
    {
        return $this->hasMany(ClientInvestment::class, 'parent_id');
    }

    public function rolloverInstruction()
    {
        return $this->belongsToMany(
            ClientInvestmentInstruction::class,
            'client_investment_rollover_instructions',
            'investment_id',
            'instruction_id'
        );
    }

    public function scopeForFundManager($investment)
    {
        if (!$this->currentFundManager()) {
            return $investment;
        }

        return $investment->whereHas(
            'product',
            function ($product) {
                $product->where('fund_manager_id', $this->currentFundManager()->id);
            }
        );
    }

    public function scopeForFund($query, UnitFund $fund)
    {
        return $query->whereHas('product', function ($product) use ($fund) {
            $product->forFund($fund);
        });
    }

    public function scopeFundManager($query, $fundManager)
    {
        if ($fundManager instanceof Collection) {
            $fundManager = $fundManager->all();
        }

        if ($fundManager instanceof FundManager) {
            $fundManager = [$fundManager];
        }

        if (!is_array($fundManager)) {
            throw new \InvalidArgumentException("Please pass a fund manager or an array of fund managers");
        }

        return $query->whereHas(
            'product',
            function ($product) use ($fundManager) {
                $product->whereIn('fund_manager_id', collect($fundManager)->lists('id'));
            }
        );
    }

    public function scopeCurrency($query, Currency $currency)
    {
        return $query->whereHas(
            'product',
            function ($product) use ($currency) {
                $product->where('currency_id', $currency->id);
            }
        );
    }

    public function scopeActive($query, $condition = true)
    {
        if ($condition) {
            return $query->where(
                function ($q) {
                    $q->whereNull('withdrawn')->orWhere('withdrawn', 0);
                }
            );
        }

        return $query->where('withdrawn', 1);
    }

    public function isNew()
    {
        $type = $this->investmentType->name;

        $reinvestment = $this->reinvestedFrom()->count() > 0;

        $not_new = $type == 'partial_withdraw' || $type == 'rollover' || $reinvestment;

        return !$not_new;
    }

    public function scopeStatement($query, Carbon $statementDate, Carbon $from = null)
    {
        $cutOff = $statementDate->copy()->subMonthNoOverflow();

        if ($from) {
            $cutOff = $from;
        }

        $query = $query->where('invested_date', '<=', $statementDate->copy()->addDay());

        return $query->where(
            function ($query) use ($statementDate, $cutOff) {
                $query->active()
                    ->orWhere(
                        function ($q) use ($statementDate, $cutOff) {
                            $q->active(false)
                                ->where('withdrawal_date', '>=', $cutOff);
                        }
                    );
            }
        )->orderBy('invested_date', 'ASC');
    }

    public function scopeOfType($query, $type)
    {
        return $query->whereHas('investmentType', function ($invType) use ($type) {
            $invType->where('name', $type);
        });
    }

    /**
     * @return mixed
     */
    public function isSeip()
    {
        return $this->product->present()->isSeip;
    }
}
