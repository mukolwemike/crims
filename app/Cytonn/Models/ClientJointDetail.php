<?php

namespace App\Cytonn\Models;

use Cytonn\Crm\CrmSyncTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

/**
 * Date: 17/11/2015
 * Time: 10:33 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ClientJointDetail extends BaseModel
{
    use SoftDeletes, CrmSyncTrait;

    /*
     * Get the presentable trait
     */
    use PresentableTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = 'Cytonn\Clients\ClientJointDetails\ClientJointDetailPresenter';

    public $table = 'client_joint_details';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $fillable = [
        'title_id',
        'lastname',
        'middlename',
        'firstname',
        'gender_id',
        'dob',
        'pin_no',
        'id_or_passport',
        'email',
        'postal_code',
        'postal_address',
        'country_id',
        'telephone_office',
        'telephone_home',
        'telephone_cell',
        'residential_address',
        'town',
        'method_of_contact_id',
        'client_id',
        'application_id',
        'reservation_form_id'
    ];

    public function add($data)
    {
        $this->fill($data);
        $this->save();
        return $this;
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function application()
    {
        return $this->belongsTo(ClientInvestmentApplication::class, 'application_id');
    }

    public function holder()
    {
        return $this->hasOne(JointClientHolder::class, 'joint_detail_id');
    }

    public function title()
    {
        return $this->belongsTo(Title::class, 'title_id');
    }

    /*
     * Relationship between gender and client joint detail
     */
    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }

    /*
     * Relationship between country and client joint detail
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function methodOfContact()
    {
        return $this->belongsTo(ContactMethod::class, 'method_of_contact_id');
    }

    public function uploadedKyc()
    {
        return $this->hasMany(ClientUploadedKyc::class, 'client_joint_id');
    }

    /**
     * Query the clients with birthdays on a date
     *
     * @param  $query
     * @param null $date
     * @return mixed
     */
    public function scopeBirthday($query, $date = null)
    {
        $date = (new \Carbon\Carbon($date))->startOfDay();

        return $query->whereHas(
            'client',
            function ($client) {
                $client->whereHas(
                    'clientType',
                    function ($type) {
                        $type->where('name', 'individual');
                    }
                );
            }
        )
            ->whereRaw('extract(month from dob) = ?', [$date->month])
            ->whereRaw('extract(day from dob) = ?', [$date->day]);
    }

    /**
     * @param $query
     * @param $start
     * @param $end
     * @return mixed
     */
    public function scopeBirthdayBetween($query, $start, $end)
    {
        $start = new \Carbon\Carbon($start);
        $end = new \Carbon\Carbon($end);

        return $query->where(
            function ($q) use ($start) {
                $q->whereRaw('extract(month from dob) >= ?', [$start->month])
                    ->whereRaw('extract(day from dob) >= ?', [$start->day]);
            }
        )->where(
            function ($q) use ($end) {
                $q->whereRaw('extract(month from dob) <= ?', [$end->month])
                    ->whereRaw('extract(day from dob) <= ?', [$end->day]);
            }
        );
    }
}
