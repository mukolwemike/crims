<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/7/18
 * Time: 11:41 AM
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundFeePaymentRecipient extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_fee_payment_recipients';

    protected $guarded = ['id'];
}
