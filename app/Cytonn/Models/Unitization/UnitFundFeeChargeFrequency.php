<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class UnitFundFeeChargeFrequency extends BaseModel
{
    protected $table = 'unit_fund_fee_charge_frequencies';

    protected $guarded = ['id'];
}
