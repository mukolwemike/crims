<?php
/**
 * Date: 25/05/2018
 * Time: 09:04
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models\Unitization;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\Unitization\UnitFundType;

class FundCategory extends BaseModel
{
    protected $table = 'fund_categories';

    public function calculation()
    {
        return $this->belongsTo(FundCalculation::class, 'fund_calculation_type_id');
    }

    public function unitFundType()
    {
        return $this->belongsTo(UnitFundType ::class, 'fund_type_id');
    }

    public function type()
    {
        return $this->unitFundType();
    }
}
