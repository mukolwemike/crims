<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use Carbon\Carbon;
use Cytonn\Unitization\UnitFundHolderRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundHolder
 */
class UnitFundHolder extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string $table
     */
    protected $table = 'unit_fund_holders';

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    /**
     * @var array $searchable
     */
    protected $searchable = [
        'columns' => [
            'unit_fund_holders.number' => 10,
            'unit_funds.name' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
        ],
        'joins' => [
            'unit_funds' => ['unit_fund_holders.unit_fund_id', 'unit_funds.id'],
            'clients' => ['unit_fund_holders.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
        ],
        'groupBy' => 'unit_fund_holders.id'
    ];

    /**
     * @var $repo
     */
    public $repo;

    /**
     * Inject the repo
     * UnitFundHolder constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new UnitFundHolderRepository($this);
    }

    /**
     * @return string
     */
    public function path()
    {
        return "/dashboard/unitization/unit-fund-holders/$this->id";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feesIncurred()
    {
        return $this->hasMany(UnitFundFeesCharged::class, 'unit_holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchases()
    {
        return $this->hasMany(UnitFundPurchase::class, 'unit_fund_holder_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transfersReceived()
    {
        return $this->hasMany(UnitFundTransfer::class, 'transferee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transfersGiven()
    {
        return $this->hasMany(UnitFundTransfer::class, 'transferer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany(UnitFundSale::class, 'unit_fund_holder_id');
    }

    /**
     * @param Carbon $date | null
     * @return mixed
     */
    public function getUnitsBoughtAsAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);
        return $this->purchases()
            ->where('date', '<=', $date->toDateString())
            ->sum('number');
    }

    /**
     * @param Carbon $date | null
     * @return mixed
     */
    public function getUnitsSoldAsAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);
        return $this->sales()
            ->where('date', '<=', $date->toDateString())
            ->sum('number');
    }

    /**
     * @param Carbon $date | null
     * @return mixed
     */
    public function getUnitsReceivedAsAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);
        return $this->transfersReceived()
            ->where('date', '<=', $date->toDateString())
            ->sum('number');
    }

    /**
     * @param Carbon $date | null
     * @return mixed
     */
    public function getUnitsGivenAsAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);
        return $this->transfersGiven()
            ->where('date', '<=', $date->toDateString())
            ->sum('number');
    }

    /**
     * @param Carbon $date | null
     * @return mixed
     */
    public function getUnitTransfersReceivedAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);
        return $this->transfersReceived()
            ->where('date', '<=', $date->toDateString())
            ->sum('number');
    }

    /**
     * @param Carbon $date | null
     * @return mixed
     */
    public function getUnitTransfersGivenAt(Carbon $date = null)
    {
        $date = Carbon::parse($date);
        return $this->transfersGiven()
            ->where('date', '<=', $date->toDateString())
            ->sum('number');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function balance($date = null)
    {
        $date = Carbon::parse($date);
        return (double)(new ClientPayment())
            ->balance($this->client, null, null, null, $date, $this->fund);
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function ownedNumberOfUnits($date = null)
    {
        $date = Carbon::parse($date);
        return (int)(($this->getUnitsBoughtAsAt($date) + $this->getUnitsReceivedAsAt($date))
            - ($this->getUnitsSoldAsAt($date) + $this->getUnitsGivenAsAt($date)));
    }

    public function exceededLockInPeriod(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        return $date->diffInDays($this->updated_at) > $this->fund->lockInDays();
    }
}
