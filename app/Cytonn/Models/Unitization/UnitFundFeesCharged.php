<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/4/18
 * Time: 12:23 PM
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientPayment;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundFeesCharged extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_fees_charged';

    protected $guarded = ['id'];

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function fee()
    {
        return $this->belongsTo(UnitFundFee::class, 'fee_id');
    }

    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }

    public function sale()
    {
        return $this->belongsTo(UnitFundSale::class, 'unit_fund_sale_id');
    }

    public function transfer()
    {
        return $this->belongsTo(UnitFundTransfer::class, 'unit_fund_transfer_id');
    }

    public function fundSwitch()
    {
        return $this->belongsTo(UnitFundSwitch::class, 'unit_fund_switch_id');
    }

    public function deduction()
    {
        return $this->belongsTo(UnitFundSale::class, 'deduction_id');
    }

    public function payment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }

    public function scopeActive($query)
    {
        return $query->whereNull('deleted_at');
    }

    public function scopeBefore($query, $date, $strict = false)
    {
        $comparator = $strict ? '<' : '<=';

        return $query->where('date', $comparator, $date);
    }

    public function getclient()
    {
        if ($this->purchase) {
            return $this->purchase->client;
        }
        if ($this->sale) {
            return $this->sale->client;
        }

        return null;
    }
}
