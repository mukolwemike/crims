<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundHolderFee
 */
class UnitFundHolderFee extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string $table
     */
    protected $table = 'unit_fund_holder_fees';

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    protected $dates = ['date'];

    /**
     * @var array $searchable
     */
    protected $searchable = [
        'columns' => [
            'unit_fund_fee_types.name' => 10,
            'contacts.firstname' => 10,
            'contacts.lastname' => 10,
            'contacts.middlename' => 10,
            'contacts.email' => 10,
            'unit_fund_holders.number' => 10,
        ],
        'joins' => [
            'unit_fund_fees' => ['unit_fund_holder_fees.unit_fund_fee_id', 'unit_fund_fees.id'],
            'unit_fund_fee_types' => ['unit_fund_fees.unit_fund_fee_type_id', 'unit_fund_fee_types.id'],
            'unit_fund_holders' => ['unit_fund_holder_fees.unit_fund_holder_id', 'unit_fund_holders.id'],
            'clients' => ['unit_fund_holders.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
        ],
        'groupBy' => 'unit_fund_holder_fees.id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holder()
    {
        return $this->belongsTo(UnitFundHolder::class, 'unit_fund_holder_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fee()
    {
        return $this->belongsTo(UnitFundFee::class, 'unit_fund_fee_id');
    }

    /**
     * @param UnitFund $fund
     * @return mixed
     */
    public function getNewLiabilities(UnitFund $fund)
    {
        $today = Carbon::today();

        return $this->whereHas(
            'fee',
            function ($fee) use ($fund) {
                return $fee->where('unit_fund_id', $fund->id);
            }
        )
            ->betweenDates($today->copy()->startOfDay(), $today->copy()->endOfDay())
            ->sum('amount');
    }

    /**
     * @param $query
     * @param $start
     * @param $end
     * @return mixed
     */
    public function scopeBetweenDates($query, $start, $end)
    {
        return $query->where('created_at', '>', $start)
            ->where('created_at', '<=', $end);
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->toDateString();
    }
}
