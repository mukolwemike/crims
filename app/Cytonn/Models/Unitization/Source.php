<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Source
 */
class Source extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'sources';

    /**
     * @var array
     */
    protected $guarded = ['id'];
}
