<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundTransfer extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_transfers';

    protected $guarded = ['id'];

    protected $dates = ['date'];

    public function recipient()
    {
        return $this->belongsTo(Client::class, 'transferee_id');
    }

    public function sender()
    {
        return $this->belongsTo(Client::class, 'transferer_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }

    public function sale()
    {
        return $this->belongsTo(UnitFundSale::class, 'unit_fund_sale_id');
    }

    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }
}
