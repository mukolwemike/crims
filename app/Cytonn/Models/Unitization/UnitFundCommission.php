<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundCommission extends BaseModel
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_fund_commissions';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commission_recipient_id',
        'unit_fund_purchase_id',
        'date',
        'rate',
        'last_payment_date',
        'description'
    ];

    /*
     * Relationship between a unit fund commission and client
     */
    public function schedules()
    {
        return $this->hasMany(UnitFundCommissionSchedule::class);
    }

    /*
     * Relationship between a unit fund commission and recipient
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_id');
    }

    /*
     * Relationship between a unit fund commission and unit fund purchase
     */
    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overrideSchedules()
    {
        return $this->hasMany(UnitFundCommissionOverride::class, 'commission_id');
    }

    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->where('commission_recipient_id', $recipient->id);
    }
}
