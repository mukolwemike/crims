<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class UnitFundFee extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'unit_fund_fees';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $dates = ['date'];

    protected $with = ['type', 'fund', 'amounts', 'percentages'];

    protected $casts = [
        'dependent' => 'boolean'
    ];

    protected $searchable = [
        'columns' => [
            'unit_fund_fee_types.name' => 10,
        ],
        'joins' => [
            'unit_fund_fee_types' => ['unit_fund_fees.unit_fund_fee_type_id', 'unit_fund_fee_types.id'],
        ],
        'groupBy' => 'unit_fund_fees.id'
    ];

    public function path()
    {
        return "/dashboard/unitization/unit-funds/$this->unit_fund_id/unit-fund-fees/$this->id";
    }

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function type()
    {
        return $this->belongsTo(UnitFundFeeType::class, 'unit_fund_fee_type_id');
    }

    public function amounts()
    {
        return $this->hasMany(UnitFundFeeAmount::class, 'unit_fund_fee_id');
    }

    public function getAmount()
    {
        return $this->amounts()->count() ? $this->amounts()->latest()->first()->amount : null;
    }

    public function percentages()
    {
        return $this->hasMany(UnitFundFeeParameterPercentage::class, 'unit_fund_fee_id');
    }

    public function feeChargeType()
    {
        return $this->belongsTo(UnitFundFeeChargeType::class, 'fee_charge_type_id');
    }

    public function feeChargeFrequency()
    {
        return $this->belongsTo(UnitFundFeeChargeFrequency::class, 'fee_charge_frequency_id');
    }

    public function getPercentageAsAtDate(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::today();

        if ($this->date && $this->date->gt($date)) {
            return null;
        }

        return $this->percentages()->where('date', '<=', $date)->latest('date')->first();
    }

    public function charges()
    {
        return $this->hasMany(UnitFundFeesCharged::class, 'fee_id');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->toDateString();
    }

    public function scopeInitialFees($query)
    {
        return $query->whereHas('type', function ($type) {
            return $type->whereNull('exception');
        });
    }

    public function scopeInitialChargedFees($query)
    {
        return $query->whereHas('feeChargeType', function ($type) {
            return $type->where('slug', 'once');
        });
    }

    public function scopeOfType($query, $type)
    {
        return $query->whereHas('type', function ($t) use ($type) {
            $t->where('slug', $type);
        });
    }

    public function calculateRecurrentAmount($dependency, Carbon $date = null)
    {
        $percentage = $this->getPercentageAsAtDate($date)->percentage;

        return (1 / $this->getRecurrenceDivisor()) * ($percentage / 100) * $dependency;
    }

    public function calculateFixedFee()
    {
        return (1 / $this->getRecurrenceDivisor()) * $this->getAmount();
    }

    private function getRecurrenceDivisor()
    {
        $recurrence = $this->feeChargeFrequency->slug;

        switch ($recurrence) {
            case 'daily':
                $divisor = 365;
                break;
            case 'monthly':
                $divisor = 12;
                break;
            case 'annually':
                $divisor = 1;
                break;
            default:
                throw new \InvalidArgumentException("Recurrence {$recurrence} recurrent fee type not supported");
        }

        return $divisor;
    }
}
