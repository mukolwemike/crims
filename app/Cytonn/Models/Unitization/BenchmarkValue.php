<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 10/04/2019
 * Time: 08:12
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class BenchmarkValue extends BaseModel
{
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function benchmark()
    {
        return $this->belongsTo(Benchmark::class, 'benchmark_id');
    }

    /**
     * @param $query
     * @param $name
     * @return mixed
     */
    public function scopeForBenchmarkName($query, $name)
    {
        return $query->whereHas('benchmark', function ($q) use ($name) {
            $q->where('name', $name);
        });
    }
}
