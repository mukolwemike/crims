<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Portfolio\EquityHolding;
use App\Cytonn\Models\Portfolio\FundCompliance;
use App\Cytonn\Models\Portfolio\FundComplianceBenchmarkLimit;
use App\Cytonn\Models\Portfolio\FundLiquidityLimit;
use App\Cytonn\Models\Portfolio\FundNoGoZone;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\Portfolio\SubAssetClass;
use Carbon\Carbon;
use Cytonn\Models\Unitization\FundCategory;
use Cytonn\Unitization\UnitFundRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class UnitFund extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'unit_funds';

    protected $fillable = [
        'name',
        'initial_unit_price',
        'minimum_investment_amount',
        'fund_objectives',
        'benefits',
        'minimum_investment_horizon',
        'currency_id',
        'fund_manager_id',
        'custodial_account_id',
        'fund_category_id',
        'short_name',
        'contact_person',
        'email',
        'active',
        'allow_premature_withdrawal'
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $appends = ['current_unit_price'];

    protected $with = ['currency', 'manager'];

    protected $searchable = [
        'columns' => [
            'unit_funds.name' => 10,
        ]
    ];

    public $repo;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new UnitFundRepository($this);
    }

    public function path()
    {
        return "/dashboard/unitization/unit-funds/$this->id";
    }

    public function fees()
    {
        return $this->hasMany(UnitFundFee::class, 'unit_fund_id');
    }

    public function initialFees()
    {
        return $this->fees()
            ->whereHas('feeChargeType', function ($type) {
                $type->where('slug', 'once');
            })->where('charged_at', 'initial');
    }

    public function terminalFees()
    {
        return $this->fees()
            ->whereHas('feeChargeType', function ($type) {
                $type->where('slug', 'once');
            })->where('charged_at', 'terminal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactionFees()
    {
        return $this->fees()->whereHas('feeChargeType', function ($type) {
            $type->where('slug', 'once');
        })->where('charged_at', 'transaction');
    }

    public function recurrentFees()
    {
        return $this->fees()->whereHas('type', function ($type) {
            $type->where('slug', 'recurrent');
        });
    }

    public function switchFees()
    {
        return $this->fees()
            ->whereHas('feeChargeType', function ($type) {
                $type->where('slug', 'once');
            })->where('charged_at', 'switch');
    }

    public function performances()
    {
        return $this->hasMany(UnitFundPerformance::class, 'unit_fund_id');
    }

    public function latestPerformance()
    {
        return $this->performances()->latest('date')->first();
    }

    public function priceTrails()
    {
        return $this->hasMany(UnitFundPriceTrail::class, 'unit_fund_id');
    }

    public function statementCampaigns()
    {
        return $this->hasMany(UnitFundStatementCampaign::class, 'unit_fund_id');
    }

    public function unitPrice(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $price = $this->performances()
            ->where('date', '<', $date)
            ->latest('date')
            ->first();

        return $price ? abs($price->price) : $this->initial_unit_price;
    }

    public function dividends()
    {
        return $this->hasMany(UnitFundDividend::class, 'unit_fund_id');
    }

    public function switchRates()
    {
        return $this->hasMany(UnitFundSwitchRate::class, 'unit_fund_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function manager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    public function fundManager()
    {
        return $this->manager();
    }

    public function scopeForFundManager($query, $id = null)
    {
        return $query->where('fund_manager_id', $id);
    }

    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'custodial_account_id');
    }

    public function inflowCustodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'inflow_custodial_account_id');
    }

    public function equityHoldings()
    {
        return $this->hasMany(EquityHolding::class, 'unit_fund_id');
    }

    public function depositHoldings()
    {
        return $this->hasMany(DepositHolding::class, 'unit_fund_id');
    }

    public function purchases()
    {
        return $this->hasMany(UnitFundPurchase::class, 'unit_fund_id');
    }

    public function sales()
    {
        return $this->hasMany(UnitFundSale::class, 'unit_fund_id');
    }

    public function salesBetweenDates(Carbon $start, Carbon $end)
    {
        return $this->sales()
            ->nonFees(true)
            ->whereBetween('date', [ $start->startOfDay(), $end->endOfDay()]);
    }

    public function feesCharged()
    {
        return $this->hasMany(UnitFundFeesCharged::class, 'unit_fund_id');
    }

    public function getCurrentUnitPriceAttribute()
    {
        return $this->unitPrice(Carbon::today());
    }

    public function fundCompliance()
    {
        return $this->hasMany(FundCompliance::class, 'unit_fund_id');
    }

    public function benchmarks()
    {
        return $this->hasMany(UnitFundBenchmark::class, 'unit_fund_id');
    }

    public function benchmarkLimit(SubAssetClass $sub)
    {
        return FundComplianceBenchmarkLimit::whereHas('benchmark', function ($benchmark) {
                $benchmark->whereHas('fundCompliance', function ($compliance) {
                    $compliance->where('unit_fund_id', $this->id);
                });
        })
            ->where('sub_asset_class_id', $sub->id)
            ->orderBy('limit', 'asc')
            ->first();
    }

    public function liquidityLimits()
    {
        return $this->hasMany(FundLiquidityLimit::class, 'unit_fund_id');
    }

    public function noGoZones()
    {
        return $this->hasMany(FundNoGoZone::class, 'fund_id');
    }

    public function noGoZone(PortfolioSecurity $security)
    {
        return $this->noGoZones()
            ->where('security_id', $security->id)
            ->orderBy('updated_at', 'desc')
            ->first();
    }

    public function getPerformance(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        return $this->performances()
            ->where('date', '<=', $date->copy()->subDay()->endOfDay())
            ->latest('date')
            ->first();
    }

    public function previousPerformance(Carbon $date = null)
    {
        $date = ($date) ? Carbon::parse($date) : Carbon::today();

        return $this->performances()
            ->whereBetween('date', [
                $date->copy()->subDay(1)->startOfDay(),
                $date->copy()->subDay(1)->endOfDay()
            ])
            ->orderBy('date', 'desc')
            ->first();
    }

    public function receivingAccounts()
    {
        return $this->belongsToMany(
            CustodialAccount::class,
            'products_projects_receiving_accounts',
            'unit_fund_id',
            'account_id'
        )->withTimestamps();
    }

    public function operatingAccounts()
    {
        return $this->belongsToMany(
            CustodialAccount::class,
            'unit_fund_operating_accounts',
            'fund_id',
            'account_id'
        )->withTimestamps();
    }

    public function getDividends(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        return $this->dividends()
            ->where('date', $date->copy()->toDateString())
            ->sum('amount');
    }

    public function category()
    {
        return $this->belongsTo(FundCategory::class, 'fund_category_id');
    }

    public function scopeForCategory($query, $slug)
    {
        return $query->whereHas('category', function ($category) use ($slug) {
            $category->where('slug', $slug);
        });
    }

    public function scopeCalculatedBy($query, $slug)
    {
        return $query->whereHas('category', function ($category) use ($slug) {
            $category->whereHas('calculation', function ($calc) use ($slug) {
                $calc->where('slug', $slug);
            });
        });
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas('category', function ($calculation) use ($slug) {
            $calculation->whereHas('type', function ($type) use ($slug) {
                return $type->where('slug', $slug);
            });
        });
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function securities()
    {
        return PortfolioSecurity::whereHas('equityHoldings', function ($holding) {
            $holding->where('unit_fund_id', $this->id);
        })->orWhereHas('depositHoldings', function ($holding) {
            $holding->where('unit_fund_id', $this->id);
        });
    }

    /**
     * @param null $secondaryTag
     * @return mixed
     */
    public function getDisbursementAccount($secondaryTag = null)
    {
        return $this->repo->getDisbursementAccount($secondaryTag);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(ClientPayment::class, 'unit_fund_id');
    }

    public function lockInDays()
    {
        return $this->minimum_investment_horizon;

        // TODO::remove after fix
//        $min = $this->minimum_investment_horizon;
//
//        $months = (int) $min;
//        $days = ($min - $months) * 100;
//
//        return $days + (30 * $months);
    }
}
