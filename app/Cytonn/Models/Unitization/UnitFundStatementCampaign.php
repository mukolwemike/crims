<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\StatementCampaignType;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Unitization\Reporting\StatementCampaignRepository;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundStatementCampaign extends BaseModel
{
    use SoftDeletes;

    const TYPE = 'Unit Trust Funds Statements';

    public $repo;

    protected $table = 'unit_fund_statement_campaigns';

    protected $guarded = ['id'];

    protected $appends = ['sent_items_count', 'total_items_count'];

    protected $searchable = [
        'columns' => [
            'unit_fund_statement_campaigns.name' => 10,
        ],
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new StatementCampaignRepository($this);
    }

    public function path()
    {
        return "/dashboard/unitization/unit-fund-statement-campaigns/$this->unit_fund_id/$this->id";
    }

    public function items()
    {
        return $this->hasMany(
            UnitFundCampaignStatementItem::class,
            'unit_fund_statement_campaign_id'
        );
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function type()
    {
        return $this->belongsTo(StatementCampaignType::class, 'type_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function closedBy()
    {
        return $this->belongsTo(User::class, 'closed_by');
    }

    public function close()
    {
        $this->closed = true;
        return $this->save();
    }

    public function scopeOpen($query)
    {
        return $query->whereNull('closed')->latest();
    }

    public function setSendDateAttribute($value)
    {
        $this->attributes['send_date'] = Carbon::parse($value)->toDateString();
    }

    public function getSentItemsCountAttribute()
    {
        return $this->items()
            ->where('sent', 1)
            ->count();
    }

    public function getTotalItemsCountAttribute()
    {
        return $this->items()->count();
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeClosed($query, $condition = true)
    {
        if ($condition) {
            return $query->where('closed', 1);
        }

        return $query->where(function ($q) {
            $q->whereNull('closed')->orWhere('closed', '!=', 1);
        });
    }
}
