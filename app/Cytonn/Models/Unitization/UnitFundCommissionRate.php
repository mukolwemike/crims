<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecipientType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundCommissionRate extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_commission_rates';

    protected $guarded = ['id'];

    protected $with = ['recipientType'];

    public function path()
    {
        return "/dashboard/unitization/unit-funds/$this->unit_fund_id/unit-fund-commission-rates/$this->id";
    }

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function recipientType()
    {
        return $this->belongsTo(CommissionRecipientType::class, 'commission_recipient_type_id');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->toDateString();
    }
}
