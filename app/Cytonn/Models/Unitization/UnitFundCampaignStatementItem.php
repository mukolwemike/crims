<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class UnitFundCampaignStatementItem extends BaseModel
{
    use SoftDeletes, SearchableTrait;
    
    protected $table = 'unit_fund_campaign_statement_items';
    
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'unit_fund_holders.number'=>10,
            'contacts.firstname'=>10,
            'contacts.middlename'=>10,
            'contacts.lastname'=>10,
            'contacts.corporate_registered_name'=>10,
            'contacts.email'=>10,
            'users.firstname'=>10,
            'users.middlename'=>10,
            'users.lastname'=>10,
        ],
        'joins' => [
            'unit_fund_holders'=>['unit_fund_campaign_statement_items.unit_fund_holder_id', 'unit_fund_holders.id'],
            'clients'=>['unit_fund_holders.client_id', 'clients.id'],
            'contacts'=>['clients.contact_id', 'contacts.id'],
            'users'=>['unit_fund_campaign_statement_items.added_by', 'users.id']
        ],
        'groupBy' => 'unit_fund_campaign_statement_items.id'
    ];

    public function campaign()
    {
        return $this->belongsTo(UnitFundStatementCampaign::class, 'unit_fund_statement_campaign_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
