<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundSwitchRate extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_switch_rates';

    protected $guarded = ['id'];

    protected $dates = ['date'];

    public function path()
    {
        return "/dashboard/unitization/unit-funds/$this->unit_fund_id/unit-fund-switch-rates/$this->id";
    }

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->toDateString();
    }
}
