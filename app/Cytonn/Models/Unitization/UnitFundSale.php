<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\WithholdingTax;
use App\Cytonn\Unitization\UnitFundSaleRepository;
use Cytonn\Models\Unitization\ExemptionType;
use Cytonn\Models\Unitization\UnitFundContributionType;
use Cytonn\Models\Unitization\UnitFundTransactionType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundSale
 */
class UnitFundSale extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'unit_fund_sales';

    protected $guarded = ['id'];

    protected $dates = ['date'];

    protected $appends = ['unit_price'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'clients.investor_bank' => 4,
            'clients.investor_bank_branch' => 4,
            'clients.investor_account_name' => 10,
            'clients.investor_account_number' => 10,
            'client_bank_accounts.account_name' => 4,
            'client_bank_accounts.account_number' => 10,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10
        ],
        'joins' => [
            'clients' => ['unit_fund_sales.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_bank_accounts' => ['clients.id', 'client_bank_accounts.client_id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id'],
        ],
        'groupBy' => 'unit_fund_sales.id'
    ];

    public function payment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function unitFund()
    {
        return $this->fund();
    }

    public function instruction()
    {
        return $this->hasOne(UnitFundInvestmentInstruction::class, 'unit_fund_sale_id');
    }

    public function fees()
    {
        return $this->hasMany(UnitFundFeesCharged::class, 'unit_fund_sale_id');
    }

    public function purchaseSales()
    {
        return $this->hasMany(UnitFundPurchaseSale::class, 'unit_fund_sale_id');
    }

    public function unitFundPurchasesSales()
    {
        return $this->belongsToMany(
            UnitFundPurchase::class,
            'unit_fund_purchase_sales',
            'unit_fund_sale_id',
            'unit_fund_purchase_id'
        )->withTimestamps();
    }

    public function feeCharged()
    {
        return $this->hasOne(UnitFundFeesCharged::class, 'deduction_id');
    }

    public function transfer()
    {
        return $this->hasOne(UnitFundTransfer::class, 'unit_fund_sale_id');
    }

    public function getUnitPriceAttribute()
    {
        return $this->fund->unitPrice($this->date);
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function scopeForUnitFund($query, UnitFund $unitFund)
    {
        return $query->where('unit_fund_id', $unitFund->id);
    }

    public function scopeNonFees($query, $condition = true)
    {
        if ($condition) {
            return $query->doesntHave('feeCharged');
        }

        return $query->has('feeCharged');
    }

    public function repository()
    {
        return new UnitFundSaleRepository($this->fund, $this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contributionType()
    {
        return $this->belongsTo(UnitFundContributionType::class, 'unit_fund_contribution_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transactionType()
    {
        return $this->belongsTo(UnitFundTransactionType::class, 'unit_fund_transaction_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function exemptionType()
    {
        return $this->belongsTo(ExemptionType::class, 'exemption_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function withholdingTaxes()
    {
        return $this->hasMany(WithholdingTax::class, 'unit_fund_sale_id');
    }
}
