<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundFeeType
 */
class UnitFundFeeType extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string
     */
    protected $table = 'unit_fund_fee_types';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'unit_fund_fee_types.name' => 10,
        ]
    ];

    /**
     * @return string
     */
    public function path()
    {
        return "/dashboard/unitization/unit-fund-fee-types/$this->id";
    }
}
