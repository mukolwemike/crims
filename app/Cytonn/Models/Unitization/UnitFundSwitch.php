<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundSwitch extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_switches';

    protected $guarded = ['id'];

    protected $dates = ['date'];

    public function path()
    {
        return "/dashboard/unitization/unit-fund-holders/$this->unit_fund_holder_id/unit-fund-switches/$this->id";
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function previousFund()
    {
        return $this->belongsTo(UnitFund::class, 'previous_unit_fund_id');
    }

    public function currentFund()
    {
        return $this->belongsTo(UnitFund::class, 'current_unit_fund_id');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->toDateString();
    }

    public function sale()
    {
        return $this->belongsTo(UnitFundSale::class, 'unit_fund_sale_id');
    }

    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }
}
