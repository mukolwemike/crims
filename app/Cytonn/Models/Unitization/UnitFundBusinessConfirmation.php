<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundBusinessConfirmation extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_business_confirmations';

    protected $guarded = ['id'];

    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }

    public static function buy(UnitFundPurchase $purchase)
    {
        $payload = $purchase->toArray();

        $payload['payment'] = $purchase->payment;

        $data = [
            'unit_fund_purchase_id' =>  $purchase->id,
            'payload'               => $payload,
            'sent_by'               => auth()->id() ? auth()->id() : $purchase->fresh()->approval->sent_by,
        ];

        $confirmation = new static();

        $confirmation->fill($data);

        $confirmation->save();

        return $confirmation;
    }

    public static function sell(UnitFundSale $sale)
    {
        $payload = $sale->toArray();

        $payload['payment'] = $sale->payment;

        $user = auth()->id();

        $data = [
            'unit_fund_sale_id' =>  $sale->id,
            'payload'               => $payload,
            'sent_by'               => $user ? $user : getSystemUser()->id ,
        ];

        $confirmation = new static();

        $confirmation->fill($data);

        $confirmation->save();

        return $confirmation;
    }

    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }
}
