<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 10/04/2019
 * Time: 08:11
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class UnitFundBenchmark extends BaseModel
{

    public function benchmark()
    {
        return $this->belongsTo(Benchmark::class, 'benchmark_id');
    }

    public function fee()
    {
        return $this->belongsTo(UnitFundFee::class, 'fee_id');
    }
}
