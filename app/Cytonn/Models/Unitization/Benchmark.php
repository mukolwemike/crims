<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 10/04/2019
 * Time: 08:11
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class Benchmark extends BaseModel
{

    public function benchmarkValues()
    {
        return $this->hasMany(BenchmarkValue::class, 'benchmark_id');
    }
}
