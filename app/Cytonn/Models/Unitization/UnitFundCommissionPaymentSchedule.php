<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecepient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundCommissionPaymentSchedule extends BaseModel
{
    use SoftDeletes;
    
    protected $table = 'unit_fund_commission_payment_schedules';
    
    protected $guarded = ['id'];

    public function purchase()
    {
        return $this->belongsTo(
            UnitFundPurchase::class,
            'unit_fund_purchase_id'
        );
    }

    public function recipient()
    {
        return $this->belongsTo(
            CommissionRecepient::class,
            'commission_recipient_id'
        );
    }

    public function rate()
    {
        return $this->belongsTo(
            UnitFundCommissionRate::class,
            'unit_fund_commission_rate_id'
        );
    }
}
