<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UnitFundFeeAmount
 */
class UnitFundFeeAmount extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'unit_fund_fee_amounts';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fee()
    {
        return $this->belongsTo(UnitFundFee::class, 'unit_fund_fee_id');
    }
}
