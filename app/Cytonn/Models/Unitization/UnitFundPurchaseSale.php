<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundPurchaseSale extends BaseModel
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_fund_purchase_sales';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'unit_fund_purchase_id',
        'unit_fund_sale_id',
        'number',
    ];

    /**
     * @var array $dates
     */
    protected $dates = ['date'];

    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }

    public function sale()
    {
        return $this->belongsTo(UnitFundSale::class, 'unit_fund_sale_id');
    }

    public function scopeBetweenDates($query, $start, $end)
    {
        return $query->where('date', '<=', $end)->where('date', '>=', $start);
    }

    public function scopeNonFees($query, $condition = true)
    {
        if ($condition) {
            return $query->whereHas('sale', function ($q) {
                $q->doesntHave('feeCharged');
            });
        }

        return $query->whereHas('sale', function ($q) {
            $q->has('feeCharged');
        });
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }
}
