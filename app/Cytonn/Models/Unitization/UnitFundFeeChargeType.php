<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class UnitFundFeeChargeType extends BaseModel
{
    protected $table = 'unit_fund_fee_charge_types';

    protected $guarded = ['id'];
}
