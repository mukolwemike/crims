<?php
/**
 * Date: 25/05/2018
 * Time: 09:08
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class FundCalculation extends BaseModel
{
    protected $table = 'fund_calculation_types';
}
