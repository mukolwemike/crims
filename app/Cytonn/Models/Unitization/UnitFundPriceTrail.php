<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundPriceTrail extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_price_trails';

    protected $guarded = ['id'];

    protected $dates = ['date'];

    /**
     * @return string
     */
    public function path()
    {
        return "/dashboard/unitization/unit-funds/$this->unit_fund_id/unit-fund-price-trails/$this->id";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    /**
     * @param $value
     */
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->toDateString();
    }
}
