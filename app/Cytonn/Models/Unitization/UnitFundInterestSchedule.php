<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundInterestSchedule extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_fund_interest_schedules';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'unit_fund_id',
        'interest_payment_interval',
        'next_payment_date',
        'interest_payment_start_date',
    ];

    public function path(UnitFund $fund, Client $client)
    {
        return "/dashboard/unitization/unit-funds/$fund->id/unit-fund-client/$client->id/interest-payments/$this->id";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    /**
     * @param $query
     * @param Carbon $date
     * @return mixed
     */
    public function scopeAfterDate($query, Carbon $date)
    {
        return $query->where(function ($q) use ($date) {
            return $q->whereNull('interest_payment_start_date')->orWhere(function ($q) use ($date) {
                $q->whereNotNull('interest_payment_start_date')->where('interest_payment_start_date', '<=', $date);
            });
        });
    }

    /**
     * @param Carbon $date
     */
    public function getNextPayment(Carbon $date = null, $interval = null)
    {
        $date = $date ? $date : Carbon::now();

        $interval = $interval ? $interval : $this->interest_payment_interval;

        return $date->addMonthsNoOverflow($interval)->endOfMonth();
    }
}
