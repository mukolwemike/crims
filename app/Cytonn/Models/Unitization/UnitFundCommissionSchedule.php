<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecepient;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Crims\Commission\CommissionScheduleTrait;
use Crims\Unitization\Commission\OverrideCalculator;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundCommissionSchedule extends BaseModel
{
    use CommissionScheduleTrait;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_fund_commission_schedules';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unit_fund_commission_id',
        'date',
        'amount',
        'description',
        'reason',
        'automatic',
    ];

    /*
     * Relationship between a unit fund commission and unit fund commission schedule
     */
    public function unitFundCommission()
    {
        return $this->belongsTo(UnitFundCommission::class);
    }

    public function commission()
    {
        return $this->belongsTo(UnitFundCommission::class, 'unit_fund_commission_id');
    }

    public function purchase()
    {
        return $this->unitFundCommission->purchase();
    }

    public function scopeBefore($query, Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }

    /**
     * @param Carbon $date
     * @return OverrideCalculator
     */
    public function overrideCalculator(Carbon $date = null)
    {
        $date = $date ? $date : $this->getOverrideDate();

        return new OverrideCalculator($date, $this);
    }

    /**
     * @throws CrimsException
     */
    public function generateOverrides()
    {
        $this->overrideCalculator()->awardOverrides();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overrideSchedules()
    {
        return $this->hasMany(UnitFundCommissionOverride::class, 'schedule_id');
    }

    /**
     * @param $query
     * @param CommissionRecepient $recipient
     * @return mixed
     */
    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->whereHas('unitFundCommission', function ($query) use ($recipient) {
            $query->where('commission_recipient_id', $recipient->id);
        });
    }

    /**
     * @return Carbon
     * @throws CrimsException
     */
    public function getOverrideDate()
    {
        $purchase = @$this->commission->purchase;

        if (is_null($purchase)) {
            throw new CrimsException("No unit fund purchase linked to commission schedule id : " . $this->id);
        }

        return Carbon::parse($purchase->date);
    }
}
