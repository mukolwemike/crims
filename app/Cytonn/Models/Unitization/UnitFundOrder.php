<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 11:46 AM
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Portfolio\PortfolioOrder;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundOrder extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_orders';

    protected $guarded = ['id'];

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function portfolioOrder()
    {
        return $this->belongsTo(PortfolioOrder::class, 'order_id');
    }

    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'custodial_account_id');
    }
}
