<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Employment;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundApplication
 */
class UnitFundApplication extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string
     */
    protected $table = 'unit_fund_applications';

    /**
     * @var array
     */
    protected $guarded = ['id'];
    
    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'unit_fund_applications.lastname'=>10,
            'unit_fund_applications.middlename'=>10,
            'unit_fund_applications.firstname'=>10,
        ]
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employment()
    {
        return $this->belongsTo(Employment::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function approved()
    {
        if ($this->approval) {
            if ($this->approval->approved != null) {
                return true;
            }

            return false;
        }
        return false;
    }

    public function invested()
    {
        return is_null($this->purchase) ? false : true;
    }
    
    /**
     * @return string
     */
    public function path()
    {
        return "/dashboard/unitization/applications/$this->id";
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }
}
