<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 08/06/2018
 * Time: 17:10
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Unitization\UnitFundInstructionsRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class UnitFundInvestmentInstruction extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10,
            'unit_funds.name' => 5,
            'unit_funds.short_name' => 5
        ],
        'joins' => [
            'clients' => ['unit_fund_investment_instructions.client_id', 'clients.id'],
            'unit_funds' => ['unit_fund_investment_instructions.unit_fund_id', 'unit_funds.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id']
        ],
        'groupBy' => 'unit_fund_investment_instructions.id'
    ];

    protected $table = 'unit_fund_investment_instructions';

    protected $guarded = ['id'];

    public $repo;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new UnitFundInstructionsRepository();
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function purchase()
    {
        return $this->belongsTo(UnitFundPurchase::class, 'unit_fund_purchase_id');
    }

    public function sale()
    {
        return $this->belongsTo(UnitFundSale::class, 'unit_fund_sale_id');
    }

    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }

    public function type()
    {
        return $this->belongsTo(UnitFundInstructionType::class, 'instruction_type_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function bankAccount()
    {
        return $this->belongsTo(ClientBankAccount::class, 'account_id');
    }

    public function clientApprovals()
    {
        return $this->hasMany(ClientInstructionApproval::class, 'fund_instruction_id');
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class, 'channel_id');
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas('type', function ($type) use ($slug) {
            $type->where('slug', $slug);
        });
    }
}
