<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Clients\ClientLoyaltyPoints\UnitFundLoyaltyCalculator;
use App\Cytonn\Investment\LoyaltyRateTrait;
use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Portfolio\EquityDividend;
use App\Cytonn\Unitization\Calculate\CommissionCalculator;
use Carbon\Carbon;
use Cytonn\Models\Unitization\ExemptionType;
use Cytonn\Models\Unitization\UnitFundContributionType;
use Cytonn\Models\Unitization\UnitFundTransactionType;
use Cytonn\Unitization\Trading\Calculator;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundPurchase
 */
class UnitFundPurchase extends BaseModel
{
    use SoftDeletes, SearchableTrait, LoyaltyRateTrait;

    /**
     * @var string $table
     */
    protected $table = 'unit_fund_purchases';

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    /**
     * @var array $dates
     */
    protected $dates = ['date'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'clients.investor_bank' => 4,
            'clients.investor_bank_branch' => 4,
            'clients.investor_account_name' => 10,
            'clients.investor_account_number' => 10,
            'client_bank_accounts.account_name' => 4,
            'client_bank_accounts.account_number' => 10,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10
        ],
        'joins' => [
            'clients' => ['unit_fund_purchases.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_bank_accounts' => ['clients.id', 'client_bank_accounts.client_id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id'],
        ],
        'groupBy' => 'unit_fund_purchases.id'
    ];

    /**
     * @var array $appends
     */
    protected $appends = ['unit_price', 'fees_incurred'];

    public function path()
    {
        return "/api/unitization/unit-fund-purchases/$this->id";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function businessConfirmations()
    {
        return $this->hasMany(UnitFundBusinessConfirmation::class, 'unit_fund_purchase_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function fees()
    {
        return $this->hasMany(UnitFundFeesCharged::class, 'unit_fund_purchase_id');
    }

    public function purchaseSales()
    {
        return $this->hasMany(UnitFundPurchaseSale::class, 'unit_fund_purchase_id');
    }

    public function unitFundPurchaseSales()
    {
        return $this->belongsToMany(
            UnitFundSale::class,
            'unit_fund_purchase_sales',
            'unit_fund_purchase_id',
            'unit_fund_sale_id'
        )->withTimestamps();
    }

    /**
     * @return float
     */
    public function getUnitPriceAttribute()
    {
        return (double)$this->unitFund->unitPrice($this->date);
    }

    /**
     * @return float
     */
    public function getFeesIncurredAttribute()
    {
        $calculator = new Calculator($this->unitFund, null, $this->number, $this->date);

        return (double)$calculator->feesCharged();
    }

    public function scopeBetweenDates($query, $start = null, $end = null)
    {
        $end = Carbon::parse($end);

        $query = $query->where('date', '<=', $end);

        return is_null($start) ? $query : $query->where('date', '>=', Carbon::parse($start));
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function scopeFundManager($query, FundManager $manager)
    {
        return $query->whereHas('unitFund', function ($fund) use ($manager) {
            $fund->where('fund_manager_id', $manager->id);
        });
    }

    public function scopeForUnitFund($query, UnitFund $unitFund)
    {
        return $query->where('unit_fund_id', $unitFund->id);
    }

    public function commissionPaymentSchedule()
    {
        return $this->hasMany(UnitFundCommissionPaymentSchedule::class, 'unit_fund_purchase_id');
    }

    /*
     * Relationship between a unit fund commission and unit fund
     */
    public function unitFundCommission()
    {
        return $this->hasOne(UnitFundCommission::class, 'unit_fund_purchase_id');
    }

    public function dividend()
    {
        return $this->belongsTo(EquityDividend::class, 'dividend_id');
    }

    public function getRecipient()
    {
        $commission = $this->unitFundCommission;

        return $commission ? $commission->recipient : null;
    }

    public function transfer()
    {
        return $this->hasOne(UnitFundTransfer::class, 'unit_fund_purchase_id');
    }

    public function isFirstPurchase()
    {
        $previousPurchases = $this->client->unitFundPurchases()->where('date', '<', $this->date)->exists();

        return $previousPurchases ? false : true;
    }

    public function commissionCalculator(Carbon $date = null, Carbon $start = null)
    {
        $date = Carbon::parse($date);

        return new  CommissionCalculator($this, $date, $start);
    }

    public function scopeActive($query, $condition = true)
    {
        if ($condition) {
            return $query->where(
                function ($q) {
                    $q->whereNull('sold')->orWhere('sold', 0);
                }
            );
        }

        return $query->where('sold', 1);
    }

    public function scopeActiveOnDate($query, $date)
    {
        $date = new Carbon($date);

        return $this->scopeActiveBetweenDates($query, $date->startOfDay(), $date->copy()->endOfDay());
    }

    public function scopeActiveBetweenDates($query, $start, $end)
    {
        return $query->where('date', '<=', $end)
            ->where(function ($q) use ($start, $end) {
                $q->where(function ($q) {
                    $q->whereNull('sold')->orWhere('sold', 0);
                })->orWhere(function ($q) use ($start, $end) {
                    $q->where('sold', 1)->where('sale_date', '>=', $start);
                });
            });
    }

    public function scopeForFundManager($purchase)
    {
        if (!$this->currentFundManager()) {
            return $purchase;
        }

        return $purchase->whereHas('unitFund', function ($fund) {
            $fund->where('fund_manager_id', $this->currentFundManager()->id);
        });
    }

    public function calculate(
        Carbon $date = null,
        $as_at_next_day = true,
        Carbon $start = null
    ) {
        $client = $this->client;

        $date = Carbon::parse($date);

        return $client->calculateFund(
            $this->unitFund,
            $date,
            $start,
            $as_at_next_day,
            $this
        );
    }

    /**
     * @return float|int
     */
    public function getAmount()
    {
        return $this->number * $this->price;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contributionType()
    {
        return $this->belongsTo(UnitFundContributionType::class, 'unit_fund_contribution_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transactionType()
    {
        return $this->belongsTo(UnitFundTransactionType::class, 'unit_fund_transaction_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function exemptionType()
    {
        return $this->belongsTo(ExemptionType::class, 'exemption_type_id');
    }
}
