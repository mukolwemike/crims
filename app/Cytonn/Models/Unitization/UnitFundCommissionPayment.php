<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundCommissionPayment extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_commission_payments';

    protected $guarded = ['id'];

    public function recipient()
    {
        return $this->belongsTo(
            CommissionRecepient::class,
            'commission_recipient_id'
        );
    }

    public function approval()
    {
        return $this->belongsTo(
            ClientTransactionApproval::class,
            'client_transaction_approval_id'
        );
    }
}
