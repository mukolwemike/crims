<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundFeeParameter
 */
class UnitFundFeeParameter extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string $table
     */
    protected $table = 'unit_fund_fee_parameters';

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'unit_fund_fee_parameters.name' => 10,
        ]
    ];

    /**
     * @return string
     */
    public function path()
    {
        return "/dashboard/unitization/unit-fund-fee-parameters/$this->id";
    }
}
