<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 8/20/18
 * Time: 12:07 PM
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class UnitFundInstructionType extends BaseModel
{
    protected $table = 'client_unit_fund_instruction_types';

    protected $guarded = ['id'];
}
