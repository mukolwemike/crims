<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UnitFundFeeParameterPercentage
 */
class UnitFundFeeParameterPercentage extends BaseModel
{
    use SoftDeletes;

    /**
     * @var string $table
     */
    protected $table = 'unit_fund_fee_parameter_percentages';

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    /**
     * @var array $with
     */
    protected $with = ['parameter'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parameter()
    {
        return $this->belongsTo(UnitFundFeeParameter::class, 'unit_fund_fee_parameter_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fee()
    {
        return $this->belongsTo(UnitFundFee::class, 'unit_fund_fee_id');
    }
}
