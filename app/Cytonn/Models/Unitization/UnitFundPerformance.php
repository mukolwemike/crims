<?php

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundPerformance extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_performances';

    protected $guarded = ['id'];

    /**
     * @return string
     */
    public function path()
    {
        return "/dashboard/unitization/unit-funds/$this->unit_fund_id/unit-fund-performances/$this->id";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    /**
     * @param Carbon $date
     * @param UnitFund|null $unitFund
     * @return UnitFundPerformance|\Illuminate\Database\Eloquent\Model|null
     */
    public function previous(Carbon $date, UnitFund $unitFund)
    {
        return $this->where('date', $date->copy()->subDay())
            ->where('unit_fund_id', $unitFund->id)
            ->first();
    }
}
