<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 5/7/18
 * Time: 10:29 AM
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CustodialTransaction;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFundFeePayment extends BaseModel
{
    use SoftDeletes;

    protected $table = 'unit_fund_fees_payments';

    protected $guarded = ['id'];

    protected $dates = ['date'];

    public function path()
    {
        $fund = $this->unitFund->id;
        return "/dashboard/unitization/unit-funds/$fund/unit-fund-fee-payments/$this->id";
    }

    public function fee()
    {
        return $this->belongsTo(UnitFundFee::class, 'fee_id');
    }

    public function recipient()
    {
        return $this->belongsTo(UnitFundFeePaymentRecipient::class, 'recipient_id');
    }

    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }
}
