<?php
/**
 * Created by PhpStorm.
 * User: emwazonga
 * Date: 13/07/2018
 * Time: 14:20
 */

namespace App\Cytonn\Models\Unitization;

use App\Cytonn\Models\BaseModel;

class UnitFundType extends BaseModel
{
    protected $table = 'unit_fund_types';

    protected $guarded = ['id'];
}
