<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Investment\FundManager\Traits\InvestmentFundManagerScope;
use Cytonn\Uuids\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Commander\Events\EventGenerator;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientInvestmentApplication extends BaseModel
{
    use EventGenerator, SoftDeletes, SearchableTrait, UuidTrait, InvestmentFundManagerScope;

    public $repo;

    protected $table = 'client_investment_application';

    protected $guarded = ['id', 'compliance_check_by'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'clients.id_or_passport' => 5
        ],
        'joins' => [
            'clients' => ['client_investment_application.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * ClientInvestmentApplication constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new ApplicationsRepository($this);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function jointHolders()
    {
        return $this->hasMany(ClientJointDetail::class, 'application_id');
    }

    public function investorType()
    {
        return $this->hasOne(InvestorType::class);
    }

    public function type()
    {
        return $this->belongsTo(ClientInvestmentType::class, 'application_type_id');
    }

    public function gender()
    {
        return $this->hasOne(Gender::class);
    }

    public function getTitle()
    {
        return $this->belongsTo(Title::class, 'title');
    }

    public function employment()
    {
        return $this->belongsTo(Employment::class, 'employment_id');
    }

    public function corporateInvestorType()
    {
        return $this->hasOne(CorporateInvestorType::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function fundSource()
    {
        return $this->belongsTo(FundSource::class, 'funds_source_id');
    }

    public function add($appData)
    {
        $this->fill($appData);

        $this->save();

        return $this;
    }

    public function kycChecks()
    {
        return $this->hasMany(ClientInvestmentApplicationKycCheck::class, 'app_id');
    }

    public function completenessChecks()
    {
        return $this->hasMany(ClientInvestmentApplicationCompletenessCheck::class, 'app_id');
    }

    public function investment()
    {
        return $this->hasOne(ClientInvestment::class, 'application_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function form()
    {
        return $this->belongsTo(ClientFilledInvestmentApplication::class, 'form_id');
    }

    public function clientType()
    {
        return $this->belongsTo(ClientType::class, 'type_id');
    }

    public function commissionRecipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'financial_advisor');
    }

    public function scopeForFundManager($query)
    {
        if (!$this->currentFundManager()) {
            return $query;
        }

        return $query->whereHas(
            'client',
            function ($client) {
                $client->where('fund_manager_id', $this->currentFundManager()->id);
            }
        );
    }

    public function bank()
    {
        return $this->belongsTo(ClientBank::class, 'investor_bank');
    }

    public function branch()
    {
        return $this->belongsTo(ClientBankBranch::class, 'investor_bank_branch');
    }

    public function scopeForToday($query, $date = null)
    {
        $date = $date ? Carbon::parse($date) : Carbon::today();

        return $query->whereBetween('updated_at', [ $date->copy()->startOfDay(), $date->copy()->endOfDay() ]);
    }
}
