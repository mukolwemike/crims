<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models;

class RealEstateOverridePercentage extends BaseModel
{
    /*
     * Table for the model
     */
    protected $table = 'realestate_override_percentages';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commission_recipient_type_id',
        'rate',
        'structure_id',
        'start_date'
    ];

    /*
     * Relationship between commission structure and  real estate override percentages
     */
    public function structure()
    {
        return $this->belongsTo(CommissionOverrideStructure::class, 'structure_id');
    }

    /*
     * Relationship between recepient type and  real estate override percentages
     */
    public function recepientType()
    {
        return $this->belongsTo(CommissionRecipientType::class, 'commission_recipient_type_id');
    }
}
