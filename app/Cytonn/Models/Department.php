<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models;

class Department extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'departments';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'department_branch_id',
    ];

    /*
     * Relationship between a department branch and department unit
     */
    public function departmentBranch()
    {
        return $this->belongsTo(DepartmentBranch::class);
    }

    /*
     * Relationship between a departments and department unit
     */
    public function departmentUnits()
    {
        return $this->hasMany(DepartmentUnit::class);
    }
}
