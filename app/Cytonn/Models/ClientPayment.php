<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\RealEstate\RealestateInterestPayment;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Custodial\Payments\PaymentsRepository;
use Cytonn\Investment\ClientPayments\ClientPaymentPresenter;
use Cytonn\Models\ClientInvestmentWithdrawal;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class ClientPayment
 */
class ClientPayment extends BaseModel
{
    use SoftDeletes, SearchableTrait, PresentableTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = ClientPaymentPresenter::class;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'client_payments.description' => 10,
            'commission_recepients.name' => 10,
            'commission_recepients.email' => 10,
            'custodial_transactions.bank_transaction_id' => 10,
        ],
        'joins' => [
            'clients' => ['client_payments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'commission_recepients' => ['commission_recepients.id', 'client_payments.commission_recipient_id'],
            'custodial_transactions' => ['custodial_transactions.id', 'client_payments.custodial_transaction_id']
        ]
    ];

    /**
     * @var string $table
     */
    public $table = 'client_payments';

    public $repo;

    protected $dates = ['date'];


    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    /**
     * ClientPayment constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->repo = new PaymentsRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(ClientPaymentType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shareEntity()
    {
        return $this->belongsTo(SharesEntity::class, 'share_entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function child()
    {
        return $this->hasOne(static::class, 'parent_id');
    }

    public function withdrawal()
    {
        return $this->hasOne(ClientInvestmentWithdrawal::class, 'payment_out_id');
    }

    public function realEstateInterests()
    {
        return $this->hasMany(RealestateInterestPayment::class, 'client_payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function membershipConfirmation()
    {
        return $this->hasOne(MembershipConfirmation::class, 'payment_id');
    }

    /**
     * @param $payment
     * @return mixed
     */
    public function scopeForFundManager($payment)
    {
        if (!$this->currentFundManager()) {
            return $payment;
        }

        return $payment->whereHas(
            'client',
            function ($client) {
                $client->where('fund_manager_id', $this->currentFundManager()->id);
            }
        );
    }

    /**
     * @param $query
     * @param FundManager $fundManager
     * @return mixed
     */
    public function scopeFundManager($query, FundManager $fundManager)
    {
        return $query->where(function ($q) use ($fundManager) {
            $q->whereHas('product', function ($q) use ($fundManager) {
                $q->where('fund_manager_id', $fundManager->id);
            })->orWhereHas('fund', function ($q) use ($fundManager) {
                $q->where('fund_manager_id', $fundManager->id);
            });
        });
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas(
            'type',
            function ($type) use ($slug) {
                $type->where('slug', $slug);
            }
        );
    }


    /**
     * @param Client $client
     * @param Product|null $product
     * @param Project|null $project
     * @param SharesEntity $entity
     * @param DateTime|null $date
     * @param UnitFund $fund
     */
    public static function balance(
        Client $client,
        Product $product = null,
        Project $project = null,
        SharesEntity $entity = null,
        DateTime $date = null,
        UnitFund $fund = null
    ) {
        $date = Carbon::parse($date);

        if ($product && $project || $project && $entity || $product && $entity) {
            throw new \InvalidArgumentException(
                'You can only specify a product/project/entity not more than one'
            );
        }

        if ($product) {
            return self::productBalance($client, $product, $date);
        }

        if ($project) {
            return self::projectBalance($client, $project, $date);
        }

        if ($entity) {
            return self::sharesBalance($client, $entity, $date);
        }

        if ($fund) {
            return self::fundBalance($client, $fund, $date);
        }

        throw new \InvalidArgumentException(
            'A product/project/entity/fund must be given when checking balance'
        );
    }

    public static function productBalance(Client $client, Product $product, DateTime $date)
    {
        return self::calculateBalance(static::where('product_id', $product->id), $client, $date);
    }

    public static function projectBalance(Client $client, Project $project, DateTime $date)
    {
        return self::calculateBalance(static::where('project_id', $project->id), $client, $date);
    }

    public static function sharesBalance(Client $client, SharesEntity $entity, DateTime $date)
    {
        return self::calculateBalance(static::where('share_entity_id', $entity->id), $client, $date);
    }

    public static function fundBalance(Client $client, UnitFund $fund, DateTime $date)
    {
        return self::calculateBalance(static::where('unit_fund_id', $fund->id), $client, $date);
    }

    public static function calculateBalance(Builder $query, Client $client, DateTime $date)
    {
        return $query->where('date', '<=', $date)
            ->where('client_id', $client->id)
            ->sum('amount');
    }

    /**
     * Get a withdrawal approval related to transaction
     *
     * @return ClientTransactionApproval|null
     */
    public function getWithdrawApproval()
    {
        $investment = ClientInvestment::where('client_payment_out', $this->id)->first();

        if (!is_null($investment)) {
            return $investment->withdrawApproval;
        }

        $payment = InterestPayment::where('payment_out_id', $this->id)->first();

        if (!is_null($payment)) {
            return $payment->approval;
        }

        return null;
    }

    public function withdrawnFromInvestment()
    {
        return $this->hasOne(ClientInvestment::class, 'client_payment_out');
    }

    public function investedInInvestment()
    {
        return $this->hasOne(ClientInvestment::class, 'client_payment_in');
    }

    public function withdrawnFromInterestPayment()
    {
        return $this->hasOne(InterestPayment::class, 'payment_out_id');
    }

    /*
     * Get the payment balance
     */
    public function paymentBalance()
    {
        return $this->client->paymentBalance($this);
    }

    /**
     * @param UnitFund $fund
     * @return number
     */
    public function getInvestedAmount(UnitFund $fund)
    {
        $today = Carbon::today()->subDay();

        return abs(
            $this->ofType('FI')
                ->where('unit_fund_id', $fund->id)
                ->where('date', $today->toDateString())
                ->get()
                ->sum('amount')
        );
    }
}
