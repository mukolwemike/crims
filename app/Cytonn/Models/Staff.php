<?php

namespace App\Cytonn\Models;

/**
 * Class Staff
 */
class Staff extends BaseModel
{
    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var string
     */
    protected $table = 'staff';

    public $repo;

    /**
     * Staff constructor.
     */
    public function __construct()
    {
        $this->repo = new \Cytonn\Staff\StaffRepository($this);

        parent::__construct();
    }

    /**
     * find user account asociated with this staff member
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * find the contact details of this staff member
     *
     * @return mixed
     */
    public function ownContact()
    {
        return $this->belongsTo('App\Contact', 'contact_id');
    }

    public function contactsOriginated()
    {
        return $this->hasMany('App\Contact');
    }

    public function contactsManaging()
    {
        return $this->hasMany('App\Contact');
    }

    public function leads()
    {
        return $this->hasMany('App\Lead');
    }

    public function add($staffdata)
    {
        $this->fill($staffdata);

        $this->save();

        return $this;
    }
}
