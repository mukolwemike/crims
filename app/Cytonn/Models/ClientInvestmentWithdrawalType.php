<?php
/**
 * Date: 03/11/2017
 * Time: 15:29
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;

class ClientInvestmentWithdrawalType extends BaseModel
{
    protected $table = 'client_investment_withdrawal_types';
}
