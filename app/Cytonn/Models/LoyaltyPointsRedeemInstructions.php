<?php

namespace App\Cytonn\Models;

use App\Cytonn\Clients\ClientLoyaltyPoints\LoyaltyRedeemInstructionsRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class LoyaltyPointsRedeemInstructions extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10,
            'unit_funds.name' => 5,
            'unit_funds.short_name' => 5
        ],
        'joins' => [
            'clients' => ['loyalty_points_redeem_instructions.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id']
        ],
        'groupBy' => 'loyalty_points_redeem_instructions.id'
    ];

    protected $table = 'loyalty_points_redeem_instructions';

    protected $guarded = ['id'];

    public $repo;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new LoyaltyRedeemInstructionsRepository();
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }
}
