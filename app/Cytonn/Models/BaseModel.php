<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Cytonn\Investment\FundManager\FundManagerScope;
use Cytonn\Logger\Logger;
use Cytonn\Reporting\Custom\Models\Reportable;
use Cytonn\Reporting\Custom\Models\ReportableModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Watson\Rememberable\Rememberable;

/**
 * Date: 8/7/15
 * Time: 3:31 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

/**
 * Class BaseModel
 * Should be extended for authorization
 *
 * @inheritdoc
 */
abstract class BaseModel extends Model implements Reportable
{
    use Rememberable, ReportableModel;

    /**
     * BaseModel constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::created(
            function ($model) {
                $model->log('created');
            }
        );

        static::updated(
            function ($model) {
                $model->log('updated');
            }
        );

        static::deleted(
            function ($model) {
                $model->log('deleted');
            }
        );
    }

    /**
     * @param $action
     */
    public function log($action)
    {
        (new Logger())->log(Auth::user(), $this, $action, null);
    }

    /**
     * Get an array of the table column
     *
     * @return array
     */
    public static function getTableColumnsAsArray()
    {
        $model = new static();

        return $model->getConnection()->getSchemaBuilder()->getColumnListing($model->getTable());
    }

    /**
     * @return FundManager;
     */
    protected function currentFundManager()
    {
        $scope = app(FundManagerScope::class);

        return FundManager::find($scope->getSelectedFundManagerId());
    }

    public function scopeDuplicates($query, array $columns)
    {
        return $query->select($columns)
            ->groupBy($columns)
            ->havingRaw('COUNT(*) > 1');
    }

    public static function allDuplicates(Collection $collection, array $columns)
    {
        return $collection->map(function ($instance) use ($columns) {
            $agg = [];

            foreach ($columns as $item) {
                $agg[$item] = $instance->{$item};
            }

            return static::where($agg)->get();
        })->flatten();
    }

    public function scopeCreatedBetween($query, Carbon $start, Carbon $end)
    {
        return $this->scopeColumnBetween($query, 'created_at', $start, $end);
    }

    public function scopeUpdatedBetween($query, Carbon $start, Carbon $end)
    {
        return $this->scopeColumnBetween($query, 'updated_at', $start, $end);
    }

    public function scopeColumnBetween($query, $column, Carbon $start, Carbon $end)
    {
        return $query->where($column, '>=', $start)->where($column, '<=', $end);
    }

    public static function findByUuid($key)
    {
        return static::where('uuid', $key)->first();
    }

    public static function findOrFailByUuid($key)
    {
        $model = static::findByUuid($key);

        if (is_null($model)) {
            throw (new ModelNotFoundException())->setModel(static::class);
        }

        return $model;
    }

    public static function findOrNewByUuid($key)
    {
        $model = static::findByUuid($key);

        if (is_null($model)) {
            $model = new static();
            $model->uuid = $key;
        }

        return $model;
    }
}
