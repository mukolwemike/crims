<?php

namespace App\Cytonn\Models;

class Setting extends BaseModel
{
    protected $table = 'settings';

    protected $guarded = ['id'];
}
