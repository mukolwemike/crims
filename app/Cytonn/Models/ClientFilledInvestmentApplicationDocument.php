<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 7/18/17
 * Time: 12:11 PM
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClientFilledInvestmentApplicationDocument extends BaseModel
{
    use SoftDeletes;

    protected $table = 'client_filled_investment_application_documents';
    
    protected $guarded = ['id'];

    public function application()
    {
        return $this->belongsTo(ClientFilledInvestmentApplication::class, 'application_id');
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function complianceDocument()
    {
        return $this->belongsTo(ClientComplianceChecklist::class, 'client_compliance_checklist_id');
    }

    public function scopeDocumentType($query, $type)
    {
        return $query->whereHas('document', function ($q) use ($type) {
            return $q->ofType($type);
        });
    }
}
