<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class SharesEntityIssue
 */
class SharesEntityIssue extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'shares_entity_issues';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    public function scopeBefore($query, \Carbon\Carbon $date)
    {
        return $query->where('issue_date', '<=', $date);
    }
}
