<?php

namespace App\Cytonn\Models;

use Cytonn\Presenters\AmountPresenter;

class RealestateCommissionRate extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    public $table = 'realestate_commission_rates';

    protected $guarded = ['id'];

    protected $fillable = [
        'project_id',
        'recipient_type_id',
        'type',
        'amount',
        'date'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function recipientType()
    {
        return $this->belongsTo(CommissionRecipientType::class, 'recipient_type_id');
    }

    public function getRecipientTypeAndAmountAttribute()
    {
        if (is_null($this->project)) {
            return 'Zero Commission';
        }
        $string = $this->recipientType->name . ' - ';
        if ($this->type === 'percentage') {
            $string .= $this->amount . '%';
        } else {
            $string .= AmountPresenter::currency($this->amount);
        }
        return $string;
    }
}
