<?php

namespace App\Cytonn\Models;

/**
 * Date: 08/04/2016
 * Time: 4:43 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
use Webpatser\Uuid\Uuid;

trait Uuids
{
    /**
     * Boot function from laravel.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(
            function ($model) {
                $model->uuid = Uuid::generate()->string;
            }
        );
    }
}
