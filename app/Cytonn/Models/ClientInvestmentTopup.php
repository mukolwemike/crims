<?php
/**
 * Date: 21/11/2017
 * Time: 11:39
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientInvestmentTopup extends BaseModel
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $table = 'client_investment_topups';

    protected $dates = ['date'];

    /**
     * ClientInvestmentTopup constructor.
     */
    public function __construct(array $attrs = [])
    {
        parent::__construct($attrs);
    }

    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function payment()
    {
        return $this->belongsTo(ClientPayment::class, 'payment_id');
    }

    public function scopeBefore($query, Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function fromInvestment()
    {
        return $this->belongsTo(ClientInvestment::class, 'topup_to');
    }

    public function toInvestment()
    {
        return $this->belongsTo(ClientInvestment::class, 'topup_from');
    }
}
