<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 7/18/17
 * Time: 12:11 PM
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClientFilledInvestmentApplicationContactPersons extends BaseModel
{
    use SoftDeletes;

    protected $table = 'client_filled_investment_application_contact_persons';
    
    protected $guarded = ['id'];

    public function application()
    {
        return $this->belongsTo(ClientFilledInvestmentApplication::class, 'application_id');
    }
}
