<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Shares;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecipientType;

class ShareCommissionRate extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'share_commission_rates';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'commission_recipient_type_id',
        'date',
        'rate',
    ];

    /*
     * Relationship between a currency and exchange rate
     */
    public function commissionRecipientType()
    {
        return $this->belongsTo(CommissionRecipientType::class, 'commission_recipient_type_id');
    }
}
