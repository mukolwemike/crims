<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/23/18
 * Time: 4:20 PM
 */

namespace App\Cytonn\Models\Shares;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientPayment;

class StampDuty extends BaseModel
{
    protected $table = 'share_purchase_stamp_duties';

    protected $guarded = ['id'];

    public function payment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }
}
