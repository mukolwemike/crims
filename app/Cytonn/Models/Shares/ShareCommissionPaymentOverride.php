<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Shares;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\SharesCommissionPaymentSchedule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShareCommissionPaymentOverride extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'share_commission_payment_overrides';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'schedule_id',
        'recipient_id',
        'date',
        'rate',
        'amount',
        'description',
        'schedule_amount',
        'report_id'
    ];

    /*
     * Relationship between a schedule and commission payment override
     */
    public function schedule()
    {
        return $this->belongsTo(SharesCommissionPaymentSchedule::class, 'schedule_id');
    }

    /*
     * Relationship between a recipient and commission payment override
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->where('recipient_id', $recipient->id);
    }

    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function scopeForReport($query, CommissionRecepient $recipient)
    {
        return $query->where('report_id', $recipient->id);
    }
}
