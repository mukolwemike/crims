<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Shares;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;

class ShareCommissionPayment extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'share_commission_payments';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commission_recipient_id',
        'amount',
        'description',
        'date',
        'approval_id',
        'overrides'
    ];

    /*
     * Relationship between a commission recipient and share commission payment
     */
    public function commissionRecipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_id');
    }

    /*
     * Relationship between a approvals and share commission payment
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }
}
