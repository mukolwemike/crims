<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class SharesCategory
 */
class SharesCategory extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'share_categories';

    protected $guarded = ['id'];
}
