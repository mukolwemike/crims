<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductPlan
 */
class ProductPlan extends BaseModel
{
    use SoftDeletes;
    /**
     * @var string
     */
    public $table = 'product_plans';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * ProductPlan constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function investments()
    {
        return $this->hasMany(ClientInvestment::class, 'product_plan_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        if (is_null($this->product_id)) {
            return 'Shares - ' . $this->shares . ' shares - ' . $this->duration . ' years';
        }
        return $this->product->name . ' - ' .
            AmountPresenter::currency($this->amount) . ' - ' .
            $this->duration . 'years';
    }

    public function scopePaymentDue($query, Carbon $date)
    {
        return $query->where('active', 1)->whereRaw('extract(day from start_date) = ?', [$date->day]);
    }
}
