<?php

namespace App\Cytonn\Models;

/**
 * Date: 22/06/2016
 * Time: 11:43 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class PortfolioInvestmentType extends BaseModel
{
    public $table = 'portfolio_investment_types';

    protected $guarded = ['id'];
}
