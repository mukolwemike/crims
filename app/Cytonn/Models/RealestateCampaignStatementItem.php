<?php

namespace App\Cytonn\Models;

class RealestateCampaignStatementItem extends BaseModel
{
    use \Nicolaslopezj\Searchable\SearchableTrait;

    /**
     * @var string
     */
    public $table = 'realestate_campaign_statement_items';
    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10
        ],
        'joins' => [
            'clients' => ['realestate_campaign_statement_items.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Relationship to client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Relationship to the campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(RealestateStatementCampaign::class, 'campaign_id');
    }

    /**
     * Relationship to the project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
