<?php

namespace App\Cytonn\Models;

class InterestAction extends BaseModel
{
    public $table = 'interest_actions';

    protected $guarded = ['id'];
}
