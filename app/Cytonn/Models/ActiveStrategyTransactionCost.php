<?php

namespace App\Cytonn\Models;

/**
 * Date: 05/04/2016
 * Time: 4:24 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */


class ActiveStrategyTransactionCost extends BaseModel
{
    public $table = 'active_strategy_transaction_cost';

    protected $guarded = ['id'];
}
