<?php

namespace App\Cytonn\Models;

class ClientContactPersonRelationship extends BaseModel
{
    public $table = 'client_contact_person_relationships';

    protected $guarded = ['id'];
}
