<?php
/**
 * Date: 13/07/2017
 * Time: 09:40
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace App\Cytonn\Models;

use Laracasts\Presenter\PresentableTrait;

class ClosedPeriod extends BaseModel
{
    use PresentableTrait;

    /*
     * Get the presenter
     */
    protected $presenter = 'Cytonn\Investment\ClosedPeriods\ClosedPeriodPresenter';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'closed_periods';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start',
        'end',
        'approval_id',
        'active'
    ];

    /*
     * Relationship between a closed period and approval
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }
}
