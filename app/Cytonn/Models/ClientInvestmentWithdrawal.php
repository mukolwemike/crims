<?php
/**
 * Date: 03/11/2017
 * Time: 15:30
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\WithholdingTax;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientInvestmentWithdrawal extends BaseModel
{
    use SoftDeletes;

    protected $table = 'client_investment_withdrawals';

    protected $guarded = ['id'];

    protected $dates = ['date'];

    protected $searchable = [
        'columns' => [
            'client_investment_withdrawals.description' => 10,
            'client_investment_withdrawals.withdraw_type' => 10
        ],

        'joins' => [
            'client_investments' => [
                'client_investment_withdrawals.investment_id',
                'client_investments.id'
            ]
        ],
    ];

    public function type()
    {
        return $this->belongsTo(ClientInvestmentWithdrawalType::class, 'type_id');
    }

    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function reinvestedTo()
    {
        return $this->belongsTo(ClientInvestment::class, 'reinvested_to');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function payment()
    {
        return $this->belongsTo(ClientPayment::class, 'payment_out_id');
    }

    public function withholdingTax()
    {
        return $this->hasMany(WithholdingTax::class, 'client_withdrawal_id');
    }

    public function scopeBefore($query, Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '<=', $end)->where('date', '>=', $start);
    }

    public function scopeFundManager($query, FundManager $fundManager)
    {
        return $query->whereHas('investment', function ($q) use ($fundManager) {
            $q->fundManager($fundManager);
        });
    }

    public function scopeCurrency($query, Currency $currency)
    {
        return $query->whereHas('investment', function ($q) use ($currency) {
            $q->currency($currency);
        });
    }

    public function scopeOfType($query, $type_slug)
    {
        return $query->whereHas('type', function ($type) use ($type_slug) {
            $type->where('slug', $type_slug);
        });
    }

    public function instruction()
    {
        return $this->belongsTo(ClientInvestmentInstruction::class, 'form_id');
    }

    public function isPremature()
    {
        return $this->withdraw_type == 'withdrawal'
            && $this->date->copy()->endOfDay()->lt($this->investment->maturity_date);
    }

    public function principalWithdrawn()
    {
        $prepared = collect($this->investment->calculate($this->date, true)->getPrepared()->actions)
            ->filter(function ($action) {
                if (isset($action->action)) {
                    if ($action->action instanceof ClientInvestmentWithdrawal) {
                        return $action->action->id == $this->id;
                    }
                }

                return false;
            })->first();

        $amount = 0;
        if ($prepared) {
            $amount = $prepared->original_principal - $prepared->principal;
        }

        return $amount;
    }
}
