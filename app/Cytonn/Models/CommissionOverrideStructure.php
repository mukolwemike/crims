<?php

namespace App\Cytonn\Models;

use Cytonn\Models\CommissionOverrideRate;

class CommissionOverrideStructure extends BaseModel
{
    protected $table = 'commission_override_structure';

    public function rates()
    {
        return $this->hasMany(CommissionOverrideRate::class, 'structure_id');
    }
}
