<?php

namespace App\Cytonn\Models;

use Cytonn\Handlers\EventDispatchTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 06/06/2016
 * Time: 11:41 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstatePayment extends BaseModel
{
    use EventGenerator, EventDispatchTrait, PresentableTrait, SearchableTrait, SoftDeletes;

    protected $searchable = [
        'columns' => [
            'realestate_payments.description' => 8,
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
        ],
        'joins' => [
            'unit_holdings' => ['realestate_payments.holding_id', 'unit_holdings.id'],
            'clients' => ['unit_holdings.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ]
    ];


    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at', 'updated_at', 'date'];

    /**
     * @var string
     */
    protected $table = 'realestate_payments';

    protected $presenter = 'Cytonn\Presenters\RealEstatePaymentPresenter';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }


    /**
     * @param $approval
     * @return bool
     */
    public function approve($approval)
    {
        $this->approval_id = $approval->id;
        return $this->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    /**
     * @return bool
     */
    public function approved()
    {
        if (!$this->approval) {
            return false;
        }

        return $this->approval->approved;
    }

    public function type()
    {
        return $this->belongsTo(RealEstatePaymentType::class, 'payment_type_id');
    }

    public function schedule()
    {
        return $this->belongsTo(RealEstatePaymentSchedule::class, 'schedule_id');
    }

    public function interestAccrueds()
    {
        return $this->hasMany(RealestateInterestAccrued::class, 'payment_id');
    }

    public function clientPayment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }

    public function scopeInMonth($query, $date)
    {
        $date = new \Carbon\Carbon($date);

        return $query->where('date', '<=', $date->copy()->endOfMonth())
            ->where('date', '>=', $date->copy()->startOfMonth());
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function scopeForProjects($query, array $project_ids)
    {
        return $query->wherehas(
            'holding',
            function ($holding) use ($project_ids) {
                $holding->whereHas(
                    'unit',
                    function ($unit) use ($project_ids) {
                        $unit->whereIn('project_id', $project_ids);
                    }
                );
            }
        );
    }

    /**
     * Make a payment
     *
     * @param  $holding
     * @param  $paymentType
     * @param  RealEstatePaymentSchedule $schedule
     * @param  $date
     * @param  $amount
     * @param  $description
     * @return static
     */
    public static function make(UnitHolding $holding, RealEstatePaymentSchedule $schedule, $date, $amount, $description)
    {
        $payment = new static();

        $payment->holding_id = $holding->id;
        $payment->schedule_id = $schedule->id;
        $payment->date = $date;
        $payment->amount = $amount;
        $payment->description = $description;
        $payment->payment_type_id = $schedule->type->id;
        $payment->save();

        return $payment;
    }

    //collect only those payments that are included in unit pricing i.e. Reservation, Deposit & Installment
    public function scopeInPricingPayments($query, $condition = true)
    {
        if ($condition) {
            return $query->whereIn('payment_type_id', [1,2,3]);
        }

        return $query->whereNotIn('payment_type_id', [1,2,3]);
    }
}
