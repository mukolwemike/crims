<?php

namespace App\Cytonn\Models;

use Cytonn\Investment\FundManager\FundManagerScopingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Notification extends BaseModel
{
    use SoftDeletes, SearchableTrait, FundManagerScopingTrait;

    protected $searchable = [
        'columns' => [
            'notifications.content' => 10
        ]
    ];

    public $table = 'notifications';

    protected $guarded = ['id'];

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'notifications_audience',
            'notification_id',
            'user_id'
        );
    }
}
