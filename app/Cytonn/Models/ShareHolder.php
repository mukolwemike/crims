<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Cytonn\Shares\ShareHolderRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class ShareHolder
 */
class ShareHolder extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'share_holders';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.lastname' => 10,
            'contacts.middlename' => 10,
        ],
        'joins' => [
            'clients' => ['share_holders.client_id', 'clients.id'],
            'contacts' => ['contacts.id', 'clients.contact_id'],
        ],
    ];

    public $repo;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->repo = new ShareHolderRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shareHoldings()
    {
        return $this->hasMany(ShareHolding::class, 'share_holder_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sharePurchases()
    {
        return $this->hasMany(SharePurchases::class, 'share_holder_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shareSales()
    {
        return $this->hasMany(ShareSale::class, 'share_holder_id');
    }

    public function shareSalesOrders()
    {
        return $this->hasMany(SharesSalesOrder::class, 'seller_id');
    }

    public function sharePurchaseOrders()
    {
        return $this->hasMany(SharesPurchaseOrder::class, 'buyer_id');
    }

    /**
     * @param $entity_id
     * @param null $date
     * @return mixed
     */
    public function boughtShares($date = null)
    {
        $date = Carbon::parse($date);

        return $this->shareHoldings()->where('date', '<=', $date)->sum('number');
    }

    /**
     * @param $entity_id
     * @param null $date
     * @return mixed
     */
    public function soldShares($date = null)
    {
        $date = Carbon::parse($date);

        return $this->shareSales()->where('date', '<=', $date)->sum('number');
    }

    /**
     * @param $entity_id
     * @param null $date
     * @return mixed
     */
    public function currentShares($date = null)
    {
        $date = $date ? Carbon::parse($date) : Carbon::today();

        return $this->boughtShares($date) - $this->soldShares($date);
    }

    /**
     * @param null $date
     * @return mixed
     */
    public function sharePaymentsBalance($date = null)
    {
        $date = Carbon::parse($date);
        
        return (new ClientPayment())->balance($this->client, null, null, $this->entity, $date);
    }
}
