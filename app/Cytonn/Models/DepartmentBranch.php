<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models;

class DepartmentBranch extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'department_branches';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];

    /*
     * Relationship between a departments and department branches
     */
    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    /*
     * Relationship between a departments units and department branches
     */
    public function departmentUnits()
    {
        return $this->hasMany(DepartmentUnit::class);
    }
}
