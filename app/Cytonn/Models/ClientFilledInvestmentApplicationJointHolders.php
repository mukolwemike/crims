<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 7/18/17
 * Time: 12:11 PM
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ClientFilledInvestmentApplicationJointHolders extends BaseModel
{
    use SoftDeletes;

    protected $table = 'client_filled_investment_application_joint_holders';

    protected $fillable = [
        'title_id', 'firstname', 'middlename', 'lastname', 'gender_id', 'dob', 'pin_no', 'id_or_passport',
        'email', 'postal_code', 'postal_address', 'country_id', 'telephone_cell',
        'residential_address', 'town', 'nationality_id', 'holder_type', 'duplicate_reason'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }
}
