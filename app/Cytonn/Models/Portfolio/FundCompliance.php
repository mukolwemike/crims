<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/9/18
 * Time: 9:49 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Unitization\UnitFund;
use Illuminate\Database\Eloquent\SoftDeletes;

class FundCompliance extends BaseModel
{
    use SoftDeletes;

    protected $table = 'fund_compliance';

    protected $guarded = ['id'];

    public function benchmark()
    {
        return $this->belongsTo(FundComplianceBenchmark::class, 'benchmark_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }
}
