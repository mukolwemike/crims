<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Cytonn\Portfolio\SuspenseTransactions\SuspenseTransactionRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class SuspenseTransaction extends BaseModel
{
    use SoftDeletes;
    use SearchableTrait;

    public $repo;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'suspense_transactions';

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'suspense_transactions.account_no' => 5,
            'suspense_transactions.date' => 5,
            'suspense_transactions.reference' => 10,
            'suspense_transactions.bank_reference' => 10,
            'suspense_transactions.transaction_id' => 10,
            'suspense_transactions.outgoing_reference' => 10,
            'suspense_transactions.source_identity' => 10,
            'custodial_accounts.account_name' => 5,
            'custodial_accounts.alias' => 5,
        ],
        'joins' => [
            'custodial_accounts' => ['suspense_transactions.custodial_account_id', 'custodial_accounts.id'],
        ],
    ];

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'custodial_account_id',
        'account_no',
        'matched',
        'amount',
        'date',
        'reference',
        'bank_reference',
        'transaction_id',
        'outgoing_reference',
        'payload',
        'source_identity',
        'exchange_rate',
        'approval_id',
        'client_transaction_approval_id'
    ];

    /**
     * @var array
     */
    protected $dates = ['date'];

    /**
     * SuspenseTransaction constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new SuspenseTransactionRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(PortfolioTransactionApproval::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'client_transaction_approval_id');
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeMatched($query, $condition = true)
    {
        if ($condition) {
            return $query->where('matched', 1);
        }

        return $query->where('matched', '!=', 1);
    }
}
