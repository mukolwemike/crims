<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/20/18
 * Time: 3:22 PM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class PortfolioOrderType extends BaseModel
{
    protected $table = 'portfolio_order_types';

    protected $guarded = ['id'];
}
