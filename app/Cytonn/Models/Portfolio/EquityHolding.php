<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Portfolio\Equities\EquityHoldingRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class EquityHolding extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'equity_holdings';

    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date', 'deleted_at'];

    protected $searchable = [
        'columns' => [
            'asset_classes.name'=>10,
        ]
    ];
    
    public $repo;
    
    public function __construct(array $attributes = [])
    {
        $this->repo = new EquityHoldingRepository($this);
        
        parent::__construct($attributes);
    }
    
    public function path()
    {
        return "/dashboard/portfolio/securities/$this->portfolio_security_id/equity-holdings/$this->id";
    }

    public function security()
    {
        return $this->belongsTo(
            PortfolioSecurity::class,
            'portfolio_security_id'
        );
    }
    
    public function transactionCostRate()
    {
        return $this->belongsTo(
            EquityTransactionCost::class,
            'active_strategy_transaction_cost_id'
        );
    }

    public function getTransactionCostRate()
    {
        $rate = $this->transactionCostRate;

        return $rate ? $rate->rate : 0;
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function custodialTransaction()
    {
        return $this->belongsTo(
            CustodialTransaction::class,
            'custodial_transaction_id'
        );
    }

    public function approval()
    {
        return $this->belongsTo(
            PortfolioTransactionApproval::class,
            'approval_id'
        );
    }

    public function allocation()
    {
        return $this->belongsTo(
            PortfolioOrderAllocation::class,
            'portfolio_order_allocation_id'
        );
    }

    public function scopeForUnitFund($query, UnitFund $fund)
    {
        return $query->where('unit_fund_id', $fund->id);
    }

    public function scopeForSecurity($query, PortfolioSecurity $security)
    {
        return $query->where('portfolio_security_id', $security->id);
    }
    
    public function scopeBefore($query, Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }
    
    public static function boot()
    {
        static::created(function ($model) {
            $model->active_strategy_transaction_cost_id = EquityTransactionCost::latest()->first()->id;

            $model->save();
        });

        parent::boot();
    }

    public function scopeOffshore($query, $condition = false)
    {
        return $query->whereHas('security', function ($q) use ($condition) {
            if ($condition) {
                $q->where('offshore', 1);
            } else {
                $q->where('offshore', '!=', 1);
            }
        });
    }

    public function scopeActiveBetweenDates($query, $start, $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function scopeActiveAsAt($query, $date = null)
    {
        return $query->where('date', '<=', $date);
    }
}
