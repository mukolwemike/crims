<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;

class EquityPriceTrail extends BaseModel
{
    public $table = 'active_strategy_price_trail';

    protected $guarded = ['id'];

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'security_id');
    }

    public function scopeBefore($query, Carbon $date = null)
    {
        $date = Carbon::parse($date);

        return $query->where('date', '<=', $date);
    }
}
