<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 05/06/2018
 * Time: 12:20
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class BondInterestPaymentSchedule extends BaseModel
{
    protected $table = 'bond_interest_payment_schedules';

    protected $guarded = ['id'];
}
