<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class EquityHoldingType extends BaseModel
{
    protected $table = 'active_strategy_holding_types';
    
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'active_strategy_holding_types.name'=>10,
        ]
    ];
}
