<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use Carbon\Carbon;
use Cytonn\Portfolio\Deposits\PortfolioSecurityDepositRepository;
use Cytonn\Portfolio\Equities\EquitySecurityRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class PortfolioSecurity extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'portfolio_securities';


    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $fillable = [
        'name',
        'code',
        'portfolio_investor_id',
        'fund_manager_id',
        'sub_asset_class_id',
        'ticker',
        'contact_person',
        'deposit_type_id',
        'client_id',
        'offshore',
        'currency_id',
        'country_id',
        'sector_id',
        'batch_pricing',
        'isin_code',
        'issuer'
    ];

    protected $searchable = [
        'columns' => [
            'asset_classes.name'=>10,
            'sub_asset_classes.name'=>10,
            'portfolio_investors.name'=>10,
        ],
        'joins' => [
            'sub_asset_classes' => ['portfolio_securities.sub_asset_class_id','sub_asset_classes.id'],
            'asset_classes'=>['sub_asset_classes.asset_class_id', 'asset_classes.id'],
            'portfolio_investors'=>['portfolio_securities.portfolio_investor_id', 'portfolio_investors.id']
        ],
        'groupBy' => 'portfolio_securities.id'
    ];
    
    public $repo;


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new EquitySecurityRepository($this);
    }

    public function depositRepo(Currency $currency)
    {
        return new PortfolioSecurityDepositRepository($this, $currency);
    }

    public function path()
    {
        return "/dashboard/portfolio/securities/$this->id";
    }

    public function investor()
    {
        return $this->belongsTo(PortfolioInvestor::class, 'portfolio_investor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function cur()
    {
        return $this->currency();
    }

    public function subAssetClass()
    {
        return $this->belongsTo(SubAssetClass::class, 'sub_asset_class_id');
    }

    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }
    
    public function depositHoldings()
    {
        return $this->hasMany(DepositHolding::class, 'portfolio_security_id');
    }

    public function investments()
    {
        return $this->depositHoldings();
    }
    
    public function equityHoldings()
    {
        return $this->hasMany(EquityHolding::class, 'portfolio_security_id');
    }
    
    public function bondHoldings()
    {
        return $this->hasMany(BondHolding::class, 'portfolio_security_id');
    }
    
    public function sales()
    {
        return $this->hasMany(EquityShareSale::class, 'security_id');
    }

    public function priceTrail()
    {
        return $this->hasMany(EquityPriceTrail::class, 'security_id');
    }

    public function latestPrice(Carbon $date = null)
    {
        return $this->priceTrail()
            ->where('date', '<', $date)
            ->latest('date')
            ->first();
    }

    public function targetPrices()
    {
        return $this->hasMany(EquityTargetPrice::class, 'security_id');
    }

    public function dividends()
    {
        return $this->hasMany(EquityDividend::class, 'security_id');
    }

    public function depositType()
    {
        return $this->belongsTo(PortfolioInvestmentType::class, 'deposit_type_id');
    }

    public function scopeForAssetClass($query, $asset_class_slug)
    {
        return $query->whereHas('subAssetClass', function ($sub) use ($asset_class_slug) {
            $sub->whereHas('assetClass', function ($asset) use ($asset_class_slug) {
                $asset->where('slug', $asset_class_slug);
            });
        });
    }

    public function scopeOffshore($query, $condition = false)
    {
        if ($condition) {
            return $query->where('offshore', 1);
        }

        return $query->where('offshore', '!=', 1);
    }
}
