<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class SubAssetClass extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'sub_asset_classes';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'asset_classes.name'=>10,
        ]
    ];

    public function assetClass()
    {
        return $this->belongsTo(AssetClass::class, 'asset_class_id');
    }

    public function path()
    {
        return "/dashboard/portfolio/sub-asset-classes/$this->id";
    }

    public function portfolioSecurities()
    {
        return $this->hasMany(PortfolioSecurity::class, 'sub_asset_class');
    }
}
