<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/9/18
 * Time: 9:49 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class FundComplianceBenchmark extends BaseModel
{
    protected $table = 'fund_compliance_benchmarks';

    protected $guarded = ['id'];

    public function path()
    {
        return "/dashboard/portfolio/compliance/$this->id";
    }

    public function fundCompliance()
    {
        return $this->hasMany(FundCompliance::class, 'benchmark_id');
    }

    public function benchmarkLimits()
    {
        return $this->hasMany(FundComplianceBenchmarkLimit::class, 'benchmark_id');
    }

    public function type()
    {
        return $this->belongsTo(FundComplianceType::class, 'compliance_type_id');
    }
}
