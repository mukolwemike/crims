<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 11:32 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PortfolioOrderAllocation extends BaseModel
{
    use SoftDeletes;

    protected $table = 'portfolio_order_allocations';

    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(PortfolioOrder::class, 'portfolio_order_id');
    }

    public function equityHoldings()
    {
        return $this->hasMany(EquityHolding::class, 'portfolio_order_allocation_id');
    }
}
