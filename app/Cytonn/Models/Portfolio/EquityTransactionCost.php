<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class EquityTransactionCost extends BaseModel
{
    public $table = 'active_strategy_transaction_cost';

    protected $guarded = ['id'];
}
