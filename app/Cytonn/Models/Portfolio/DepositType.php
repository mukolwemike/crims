<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 29/05/2018
 * Time: 16:43
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class DepositType extends BaseModel
{
    protected $table = 'deposit_types';

    protected $guarded = ['id'];
}
