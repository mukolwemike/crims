<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Portfolio\Approvals;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PortfolioTransactionApprovalType extends BaseModel
{
    /*
     * Get the softdeletes trait
     */
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio_transaction_approval_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'batch_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Fields that should not be mass assigned
     * @var array
     */
    protected $guarded = array('id', 'created_at', 'updated_at');

    /**
     * @return static
     */
    public function allStages()
    {
        $default = PortfolioTransactionApprovalStage::where('applies_to_all', true)
            ->get();

        return $this->stages()->get()->merge($default)->sortBy('weight');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function stages()
    {
        return $this->belongsToMany(
            PortfolioTransactionApprovalStage::class,
            'portfolio_transaction_approval_required_stages',
            'type_id',
            'stage_id'
        );
    }
}
