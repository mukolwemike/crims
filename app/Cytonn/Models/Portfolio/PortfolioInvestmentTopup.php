<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class PortfolioInvestmentTopup extends BaseModel
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio_investment_topups';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $dates = ['deleted_at', 'date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'date',
        'topup_from',
        'topup_to',
        'approval_id',
        'custodial_transaction_id',
        'description'
    ];

    /*
     * Get the holding  linked to the topup
     */
    public function topupFrom()
    {
        return $this->belongsTo(DepositHolding::class, 'topup_from');
    }

    /*
     * Get the holding  linked to the topup
     */
    public function topupTo()
    {
        return $this->belongsTo(DepositHolding::class, 'topup_to');
    }

    /*
     * Get the transaction  linked to the topup
     */
    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    /*
     * Get the approval  linked to the topup
     */
    public function approval()
    {
        return $this->belongsTo(PortfolioTransactionApproval::class, 'approval_id');
    }

    public function scopeBefore($query, Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }
}
