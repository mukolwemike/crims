<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/21/18
 * Time: 11:32 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CustodialAccount;
use App\Cytonn\Models\Unitization\UnitFundOrder;
use Illuminate\Database\Eloquent\SoftDeletes;

class PortfolioOrder extends BaseModel
{
    use SoftDeletes;

    protected $table = 'portfolio_orders';

    protected $guarded = [];

    public function path()
    {
        return "/dashboard/portfolio/orders/$this->id";
    }

    public function unitFundOrders()
    {
        return $this->hasMany(UnitFundOrder::class, 'order_id');
    }

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'security_id');
    }

    public function type()
    {
        return $this->belongsTo(PortfolioOrderType::class, 'order_type_id');
    }

    public function orderAllocations()
    {
        return $this->hasMany(PortfolioOrderAllocation::class, 'portfolio_order_id');
    }

    public function equityHoldings()
    {
        return $this->hasMany(EquityHolding::class, 'portfolio_order_id');
    }

    public function equityShareSales()
    {
        return $this->hasMany(EquityShareSale::class, 'portfolio_order_id');
    }

    public function depositHoldings()
    {
        return $this->hasMany(DepositHolding::class, 'portfolio_order_id');
    }

    public function scopeApproved($query)
    {
        return $query->whereNotNull('approval_id');
    }
}
