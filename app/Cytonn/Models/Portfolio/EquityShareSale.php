<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\ActiveStrategyShareSale;
use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Portfolio\Equities\EquityShareSaleRepository;
use Illuminate\Database\Eloquent\SoftDeletes;

class EquityShareSale extends ActiveStrategyShareSale
{
    use SoftDeletes;

    public $table = 'active_strategy_share_sales';

    protected $guarded = ['id'];

    public $repo;

    protected $dates = ['date'];

    public function __construct()
    {
        $this->repo = new EquityShareSaleRepository($this);

        parent::__construct();
    }

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'security_id');
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function custodialTransaction()
    {
        return $this->belongsTo(
            CustodialTransaction::class,
            'custodial_transaction_id'
        );
    }

    public function approval()
    {
        return $this->belongsTo(
            PortfolioTransactionApproval::class,
            'approval_id'
        );
    }

    public function allocation()
    {
        return $this->belongsTo(
            PortfolioOrderAllocation::class,
            'portfolio_order_allocation_id'
        );
    }

    public function scopeForUnitFund($query, UnitFund $fund)
    {
        return $query->where('unit_fund_id', $fund->id);
    }
}
