<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/9/18
 * Time: 9:49 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class FundComplianceType extends BaseModel
{
    protected $table = 'compliance_types';

    protected $guarded = ['id'];

    public function compliance()
    {
        return $this->belongsTo(FundCompliance::class, 'compliance_type_id');
    }
}
