<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class UnitFundFeeType
 */
class AssetClass extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string
     */
    protected $table = 'asset_classes';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'asset_classes.name'=>10,
        ]
    ];

    /**
     * @return string
     */
    public function path()
    {
        return "/dashboard/portfolio/asset-classes/$this->id";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subAsset()
    {
        return $this->hasMany(SubAssetClass::class, 'asset_class_id');
    }
}
