<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/11/18
 * Time: 11:55 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Unitization\UnitFund;

class FundLiquidityLimit extends BaseModel
{
    protected $table = 'fund_liquidity_limits';

    protected $guarded = ['id'];

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }
}
