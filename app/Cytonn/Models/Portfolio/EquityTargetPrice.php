<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class EquityTargetPrice extends BaseModel
{
    public $table = 'active_strategy_target_price'; //to add equity_target_price

    protected $guarded = ['id'];

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'security_id');
    }
}
