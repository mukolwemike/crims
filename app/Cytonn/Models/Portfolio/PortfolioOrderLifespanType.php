<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 6/20/18
 * Time: 3:22 PM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class PortfolioOrderLifespanType extends BaseModel
{
    protected $table = 'good_till_types';

    protected $guarded = ['id'];
}
