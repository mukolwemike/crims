<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 11/24/17
 * Time: 2:01 PM
 */

namespace Cytonn\Models\Portfolio;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioRepaymentType;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepositHoldingRepayments extends BaseModel
{
    use SoftDeletes;
  
    protected $guarded = ['id'];
  
    protected $table = 'portfolio_investments_repayments';
    
    protected $dates = ['date'];
  
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function investment()
    {
        return $this->belongsTo(DepositHolding::class, 'investment_id');
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas(
            'type',
            function ($type) use ($slug) {
                $type->where('slug', $slug);
            }
        );
    }

    public function type()
    {
        return $this->belongsTo(PortfolioRepaymentType::class, 'type_id');
    }

    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }
    
    public function scopeBefore($q, $date)
    {
        return $q->where('date', '<=', $date);
    }
}
