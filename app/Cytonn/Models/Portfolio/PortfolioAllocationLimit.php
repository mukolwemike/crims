<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 09/06/2018
 * Time: 15:25
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Unitization\UnitFund;
use Illuminate\Database\Eloquent\Model;

class PortfolioAllocationLimit extends BaseModel
{
    protected $table = 'portfolio_allocation_limits';

    protected $guarded = ['id'];

    public function fund()
    {
        return $this->belongsTo(UnitFund::class, 'fund_id');
    }

    public function subAssetClass()
    {
        return $this->belongsTo(SubAssetClass::class, 'sub_asset_class_id');
    }

    public function assetClass()
    {
        return $this->subAssetClass->assetClass;
    }
}
