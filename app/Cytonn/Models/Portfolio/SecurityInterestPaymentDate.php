<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class SecurityInterestPaymentDate extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'security_interest_payment_dates';
    protected $guarded = ['id'];

    public function holding()
    {
        return $this->belongsTo(BondHolding::class, 'bond_holding_id');
    }
}
