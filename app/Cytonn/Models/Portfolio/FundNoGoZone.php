<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/11/18
 * Time: 12:30 PM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Unitization\UnitFund;

class FundNoGoZone extends BaseModel
{
    protected $table = 'fund_no_go_zone';

    protected $guarded = ['id'];

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'fund_id');
    }

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'security_id');
    }
}
