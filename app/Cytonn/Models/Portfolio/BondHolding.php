<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\Unitization\UnitFund;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class BondHolding extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'bond_holdings';
    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'bond_holdings.name'=>10,
        ]
    ];

    public function subAssetClass()
    {
        return $this->belongsTo(SubAssetClass::class, 'sub_asset_class_id');
    }

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'portfolio_security_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }
    
    public function path()
    {
        return "/dashboard/portfolio/securities/$this->portfolio_security_id/bond-holdings/$this->id";
    }
}
