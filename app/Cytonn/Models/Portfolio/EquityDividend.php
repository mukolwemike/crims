<?php
namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use Carbon\Carbon;
use App\Cytonn\Models\Unitization\UnitFund;

class EquityDividend extends BaseModel
{
    public $table = 'active_strategy_dividends'; //create equity_dividend

    protected $guarded = ['id'];

    protected $dates = ['date'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'security_id');
    }

    public function prevailingTaxRate()
    {
        return Setting::where('key', 'dividend_tax_rate')->first()->value;
    }

    public function scopeBefore($query, $date = null)
    {
        $date = Carbon::parse($date);

        return $query->where('book_closure_date', '<=', $date);
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    public function approval()
    {
        return $this->belongsTo(PortfolioTransactionApproval::class, 'approval_id');
    }

    public function equityHolding()
    {
        return $this->belongsTo(EquityHolding::class, 'holding_id');
    }
}
