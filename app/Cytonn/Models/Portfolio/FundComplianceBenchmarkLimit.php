<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/9/18
 * Time: 9:49 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class FundComplianceBenchmarkLimit extends BaseModel
{
    protected $table = 'fund_compliance_benchmark_limits';

    protected $fillable = [ 'warn', 'limit', 'sub_asset_class_id'];

    public function benchmark()
    {
        return $this->belongsTo(FundComplianceBenchmark::class, 'benchmark_id');
    }

    public function subAssetClass()
    {
        return $this->belongsTo(SubAssetClass::class, 'sub_asset_class_id');
    }
}
