<?php

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Cytonn\Models\PortfolioInvestmentType;
use App\Cytonn\Models\PortfolioInvestor;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Cytonn\Models\Setting;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Models\StructuredInvestment;
use Cytonn\Portfolio\DepositHoldingRepository;
use Cytonn\Portfolio\Deposits\Calculator;
use Cytonn\Portfolio\Deposits\OldCalculator;
use Cytonn\Portfolio\FundManager\Traits\HasCustodialAccountFundManagerTrait;
use Cytonn\Reporting\Custom\Models\ReportableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Laracasts\Commander\Events\EventGenerator;
use Nicolaslopezj\Searchable\SearchableTrait;

class DepositHolding extends StructuredInvestment
{
    use SoftDeletes, SearchableTrait, EventGenerator, HasCustodialAccountFundManagerTrait;

    protected $table = 'deposit_holdings';

    protected $guarded = [];

    protected $dates = ['maturity_date', 'invested_date', 'withdrawal_date', 'bond_purchase_date'];

    protected $with = ['wtaxRate'];

    public $repo;

    public static $taxRates = [];

    protected $searchable = [
        'columns' => [
            'portfolio_investors.name'=>10,
            'sub_asset_classes.name'=>10
        ],
        'joins' => [
            'portfolio_securities' => ['deposit_holdings.portfolio_security_id','portfolio_securities.id'],
            'portfolio_investors' => ['portfolio_securities.portfolio_investor_id','portfolio_investors.id'],
            'sub_asset_classes' => ['portfolio_securities.sub_asset_class_id','sub_asset_classes.id']
        ],
    ];
    
    public function __construct(array $attrs = [])
    {
        parent::__construct($attrs);
        
        $this->repo = new DepositHoldingRepository($this);
    }

    public function calculate(Carbon $date = null, $as_at_next_day = false)
    {
        $date = Carbon::parse($date);

        return new Calculator($this, $date, $as_at_next_day);
    }

    public function path()
    {
        return "/dashboard/portfolio/securities/$this->portfolio_security_id/deposit-holdings/$this->id";
    }

    public function security()
    {
        return $this->belongsTo(PortfolioSecurity::class, 'portfolio_security_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(
            UnitFund::class,
            'unit_fund_id'
        );
    }
    
    public function repayments()
    {
        return $this->hasMany(
            PortfolioInvestmentRepayment::class,
            'investment_id'
        );
    }

    public function reinvestedFrom()
    {
        return $this->hasMany(PortfolioInvestmentRepayment::class, 'reinvested_to');
    }

    public function topupsTo()
    {
        return $this->hasMany(
            PortfolioInvestmentTopup::class,
            'topup_to'
        );
    }

    public function withdrawTransaction()
    {
        return $this->belongsTo(
            CustodialTransaction::class,
            'custodial_withdraw_transaction_id'
        );
    }

    public function investApproval()
    {
        return $this->belongsTo(
            PortfolioTransactionApproval::class,
            'invest_transaction_approval_id'
        );
    }

    public function withdrawApproval()
    {
        return $this->belongsTo(
            PortfolioTransactionApproval::class,
            'withdraw_transaction_approval_id'
        );
    }

    public function type()
    {
        return $this->belongsTo(
            PortfolioInvestmentType::class,
            'type_id'
        );
    }

    public function depositType()
    {
        return $this->belongsTo(
            DepositType::class,
            'deposit_type_id'
        );
    }

    public function investTransaction()
    {
        return $this->belongsTo(
            CustodialTransaction::class,
            'custodial_invest_transaction_id'
        );
    }

    public function custodialAccount()
    {
        return $this->investTransaction->custodialAccount;
    }

    public function scopeActiveOnDate($query, $date)
    {
        return $this->scopeActiveBetweenDates($query, $date, $date);
    }

    public function interestSchedules()
    {
        return $this->hasMany(
            BondInterestPaymentSchedule::class,
            'deposit_holding_id'
        );
    }

    public function allocation()
    {
        return $this->belongsTo(
            PortfolioOrderAllocation::class,
            'portfolio_order_allocation_id'
        );
    }

    public function scopeActive($query, $condition = true)
    {
        if ($condition) {
            return $query->where(function ($q) {
                $q->whereNull('withdrawn')->orWhere('withdrawn', 0);
            });
        }
        
        return $query->where('withdrawn', 1);
    }
    
    public function scopeActiveBetweenDates($query, $start, $end)
    {
        return $query->where('invested_date', '<=', $end)
            ->where(
                function ($q) use ($start) {
                    $q->where(
                        function ($query) use ($start) {
                            $query->where('withdrawn', 1)->where('withdrawal_date', '>=', $start);
                        }
                    )
                    ->orWhere(
                        function ($query) use ($start) {
                            $query
                        //                                ->where('maturity_date', '>=', $start)
                            ->where(function ($q) {
                                $q->where('withdrawn', 0)->orWhereNull('withdrawn');
                            });
                        }
                    );
                }
            );
    }

    public function scopeInvestedBetweenDates($query, $start, $end)
    {
        $start = (new Carbon($start))->startOfDay();
        $end = (new Carbon($end))->endOfDay();

        return $this->scopeColumnBetween($query, 'invested_date', $start, $end);
    }

    public function scopeForFundManager($investment)
    {
        if (!$this->currentFundManager()) {
            throw new \InvalidArgumentException("No FundManager in session");
        }

        return $this->scopeFundManager($investment, $this->currentFundManager());
    }
    
    public function scopeFundManager($investment, $fundManager)
    {
        if ($fundManager instanceof Collection) {
            $fundManager = $fundManager->all();
        }

        if ($fundManager instanceof FundManager) {
            $fundManager = [$fundManager];
        }

        if (!is_array($fundManager)) {
            throw new \InvalidArgumentException("Please pass a fund manager or an array of fund managers");
        }

        return $investment->whereHas(
            'security',
            function ($security) use ($fundManager) {
                $security->whereIn('fund_manager_id', collect($fundManager)->lists('id'));
            }
        );
    }
  
    public function scopeCurrency($q, Currency $currency, $withTrashed = false)
    {
        return $q->whereHas('investTransaction', function ($transaction) use ($currency, $withTrashed) {
            $transaction->whereHas('custodialAccount', function ($account) use ($currency) {
                $account->where('currency_id', $currency->id);
            });

            if ($withTrashed) {
                $transaction->withTrashed();
            }
        });
    }

    public function getNominalValueAttribute()
    {
        return $this->amount;
    }
    
    public function scopeInstitution($query, PortfolioInvestor $institution)
    {
        return $query->whereHas(
            'security',
            function ($security) use ($institution) {
                $security->where('portfolio_investor_id', $institution->id);
            }
        );
    }

    public function scopeForSecurity($query, PortfolioSecurity $security)
    {
        return $query->where('portfolio_security_id', $security->id);
    }
 
    public function parent()
    {
        return $this->belongsTo(static::class, 'rollover_from');
    }

    public function child()
    {
        return $this->belongsTo(static::class, 'rollover_to');
    }

    public function wtaxRate()
    {
        return $this->belongsTo(Setting::class, 'tax_rate_id');
    }

    public function withHoldingTaxRate()
    {
        if ($rate = $this->tax_rate_id) {
            if (isset(static::$taxRates[$rate])) {
                return static::$taxRates[$rate];
            }

            $val = $this->wtaxRate->value;

            static::$taxRates[$rate] = $val;

            return $val;
        }

        if ($this->taxable) {
            return \setting('withholding_tax_rate');
        }

        return 0;
    }

    public function scopeForUnitFund($query, UnitFund $fund)
    {
        return $query->where('unit_fund_id', $fund->id);
    }

    public function scopeForFund($query, $fund)
    {
        return $this->scopeForUnitFund($query, $fund);
    }

    public function scopeOffshore($query, $condition = false)
    {
        return $query->whereHas('security', function ($q) use ($condition) {
            if ($condition) {
                $q->where('offshore', 1);
            } else {
                $q->where('offshore', '!=', 1);
            }
        });
    }
}
