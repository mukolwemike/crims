<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/3/18
 * Time: 9:43 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class Sector extends BaseModel
{
    protected $table  = 'sectors';

    protected $fillable = ['id'];
}
