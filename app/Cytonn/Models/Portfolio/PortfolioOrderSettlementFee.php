<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 7/4/18
 * Time: 11:23 AM
 */

namespace App\Cytonn\Models\Portfolio;

use App\Cytonn\Models\BaseModel;

class PortfolioOrderSettlementFee extends BaseModel
{
    protected $table = 'portfolio_order_settlement_fees';

    protected $guarded = ['id'];
}
