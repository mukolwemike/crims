<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class RealEstateType
 * @package App\Cytonn\Models
 */
class RealEstateType extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var string
     */
    protected $table = 'realestate_types';

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'types.name' => 10,
            'projects.name' => 10,
        ],
        'joins' => [
            'types' => ['realestate_types.type_id', 'types.id'],
            'projects' => ['realestate_types.project_id', 'projects.id'],
        ],
        'groupBy' => 'realestate_types.id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }
}
