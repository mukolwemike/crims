<?php

namespace App\Cytonn\Models;

class FundPool extends BaseModel
{
    protected $table = 'fund_pools';

    protected $guarded = ['id'];
}
