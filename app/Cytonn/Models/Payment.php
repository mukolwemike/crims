<?php

namespace App\Cytonn\Models;

use App\Cytonn\BaseModel;
use Carbon\Carbon;

class Payment extends BaseModel
{
    protected $table = 'commission_payments';

    public function scopeInMonth($query, Carbon $month)
    {
        return $query->where(
            function ($q) use ($month) {
                $q->where('date', '>=', $month->copy()
                    ->startOfMonth())->where('date', '<=', $month->copy()->endOfMonth());
            }
        );
    }
}
