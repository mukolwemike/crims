<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Investment\CommissionPaymentOverride;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Crims\Commission\CommissionScheduleTrait;
use Crims\Investments\Commission\OverrideCalculator;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 28/03/2016
 * Time: 4:52 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class CommissionClawback extends BaseModel
{
    use SoftDeletes;
    use CommissionScheduleTrait;

    public $table = 'commission_clawback';

    protected $guarded = ['id'];

    public function commission()
    {
        return $this->belongsTo(Commission::class, 'commission_id');
    }

    public function scopeBetweenDates($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function scopeBefore($query, Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function scopeUnpaid($query, $condition = true)
    {
        if ($condition) {
            return $query->whereNull('fully_paid_on');
        }

        return $query->whereNotNull('fully_paid_on');
    }

    public function scopeUnpaidBetweenDates($query, Carbon $start, Carbon $end)
    {
        return $query->where(function ($q) use ($start, $end) {
            $q->whereNull('fully_paid_on')->orWhere(function ($q) use ($start, $end) {
                $q->where('fully_paid_on', '>=', $start)->where('fully_paid_on', '<=', $end);
            });
        });
    }

    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->whereHas('commission', function ($query) use ($recipient) {
            $query->where('recipient_id', $recipient->id);
        });
    }

    public function withdrawal()
    {
        return $this->belongsTo(ClientInvestmentWithdrawal::class, 'withdrawal_id');
    }

    /*
     * Relationship between a commission clawbacks and commission clawback payments
     */
    public function clawbackPayments()
    {
        return $this->belongsTo(CommissionClawbackPayment::class, 'commission_clawback_id');
    }

    /**
     * @param Carbon|null $date
     * @return OverrideCalculator
     */
    public function overrideCalculator(Carbon $date = null)
    {
        $date = $date ? $date : $this->getOverrideDate();

        return new OverrideCalculator($date, null, $this);
    }

    /**
     * @throws CrimsException
     */
    public function generateOverrides()
    {
        $this->overrideCalculator()->clawbackOverrides();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overrideSchedules()
    {
        return $this->hasMany(CommissionPaymentOverride::class, 'clawback_id');
    }

    /**
     * @return Carbon
     * @throws CrimsException
     */
    public function getOverrideDate()
    {
        $investment = @$this->commission->investment;

        if (is_null($investment)) {
            throw new CrimsException("No investment linked to commission schedule id : " . $this->id);
        }

        return Carbon::parse($investment->invested_date);
    }
}
