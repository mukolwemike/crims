<?php

namespace App\Cytonn\Models;

/**
 * Date: 23/05/2016
 * Time: 4:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealestateUnitType extends BaseModel
{
    protected $table = 'realestate_unit_types';
}
