<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommissionClawbackPayment extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'commission_clawback_payments';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commission_clawback_id',
        'date',
        'amount',
    ];

    /*
     * Relationship between a commission clawbacks and commission clawback payments
     */
    public function commissionClawback()
    {
        return $this->belongsTo(CommissionClawback::class, 'commission_clawback_id');
    }

    public function scopeBefore($query, Carbon $date)
    {
        return $query->where('date', '<', $date);
    }

    public function scopeBetweenDates($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }
}
