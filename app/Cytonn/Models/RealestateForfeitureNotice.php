<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class RealestateForfeitureNotice extends BaseModel
{
    use SoftDeletes;

    protected $table = 'realestate_forfeiture_notices';

    protected $fillable = [
        'unit_holding_id',
        'sent_by',
        'reviewed_by',
        'reason',
        'status',
        'review_reason',
    ];

    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'unit_holding_id');
    }

    public function scopeReviewed($query, $condition = true)
    {
        if ($condition) {
            return $query->where('status', '!=', 0);
        }

        return $query->where('status', 0);
    }
}
