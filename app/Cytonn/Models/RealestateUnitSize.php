<?php

namespace App\Cytonn\Models;

use Laracasts\Presenter\PresentableTrait;

/**
 * Date: 23/05/2016
 * Time: 4:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealestateUnitSize extends BaseModel
{
    /*
     * Get the presentable trait
     */
    use PresentableTrait;

    /*
     * Specify the presenter for the model
     */
    protected $presenter = '\Cytonn\Presenters\RealEstateSizePresenter';

    /*
     * Table for the model
     */
    protected $table = 'realestate_unit_sizes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'realestate_land_size_id'
    ];

    /*
     * Relationship between real estate unit sizes and realestate units
     */
    public function units()
    {
        return $this->hasMany(RealestateUnit::class, 'size_id');
    }

    /*
     * Relationship between real estate unit sizes and realestate land sizes
     */
    public function landSize()
    {
        return $this->belongsTo(RealestateLandSize::class, 'realestate_land_size_id');
    }
}
