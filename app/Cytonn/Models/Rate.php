<?php

namespace App\Cytonn\Models;

use Cytonn\Investment\Rates\RatePresenter;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Rate
 */
class Rate extends BaseModel
{
    use PresentableTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = RatePresenter::class;

    protected $guarded = ['id'];

    protected $table = 'rates';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interestRateUpdate()
    {
        return $this->belongsTo(InterestRateUpdate::class, 'interest_rate_update_id');
    }

    public function scopeHidden($query, $condition = true)
    {
        if ($condition) {
            return $query->where('hidden', 1);
        }

        return $query->where('hidden', '!=', 1);
    }
}
