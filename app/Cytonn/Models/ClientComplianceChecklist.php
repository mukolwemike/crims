<?php

namespace App\Cytonn\Models;

/**
 * Date: 07/12/2015
 * Time: 3:14 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ClientComplianceChecklist extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'client_compliance_checklist';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    public function type()
    {
        return $this->belongsTo(ClientType::class, 'client_type_id');
    }
}
