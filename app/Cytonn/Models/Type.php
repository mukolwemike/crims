<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Type extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'types';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /*
     * Relationship between a unit and a unit group
     */
    public function realEstateTypes()
    {
        return $this->hasMany(RealEstateType::class, 'type_id');
    }
}
