<?php

namespace App\Cytonn\Models;

/**
 * Class ClientBulkMessage
 */
class ClientBulkMessage extends BaseModel
{

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $table = 'client_bulk_messages';

    /**
     * Get sender
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sent_by');
    }

    /**
     * Get composer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function composer()
    {
        return $this->belongsTo(User::class, 'composed_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }
}
