<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 11/28/18
 * Time: 2:58 PM
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserClientSignature
{
    use SoftDeletes;

    protected $table = 'user_client_signatures';

    protected $guarded = ['id'];

    public function users()
    {
        return $this->hasMany(ClientUser::class, 'client_user_id');
    }

    public function signatures()
    {
        return $this->hasMany(ClientSignature::class, 'client_signature_id');
    }
}
