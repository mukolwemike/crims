<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Shares\ShareCommissionPaymentOverride;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Crims\Commission\CommissionScheduleTrait;
use Crims\Shares\Commission\OverrideCalculator;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SharesCommissionPaymentSchedule
 */
class SharesCommissionPaymentSchedule extends BaseModel
{
    use SoftDeletes;
    use CommissionScheduleTrait;

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    /**
     * @var array $table
     */
    protected $table = 'shares_commission_payment_schedules';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shareSale()
    {
        return $this->belongsTo(ShareSale::class, 'share_sale_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sharePurchase()
    {
        return $this->belongsTo(SharePurchases::class, 'share_purchase_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_id');
    }

    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->where('commission_recipient_id', $recipient->id);
    }

    public function scopeForClient($query, Client $client)
    {
        return $query->whereHas('sharePurchase', function ($purchase) use ($client) {
            $purchase->whereHas('shareHolder', function ($holder) use ($client) {
                $holder->where('client_id', $client->id);
            });
        });
    }

    public function scopeForCurrency($query, Currency $currency)
    {
        return $query->whereHas('sharePurchase', function ($purchase) use ($currency) {
            $purchase->whereHas('shareHolder', function ($holder) use ($currency) {
                $holder->whereHas('entity', function ($entity) use ($currency) {
                    $entity->where('currency_id', $currency->id);
                });
            });
        });
    }

    /**
     * @param Carbon $date
     * @return OverrideCalculator
     */
    public function overrideCalculator(Carbon $date = null)
    {
        $date = $date ? $date : $this->getOverrideDate();

        return new OverrideCalculator($date, $this);
    }

    /**
     * @throws CrimsException
     */
    public function generateOverrides()
    {
        $this->overrideCalculator()->awardOverrides();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overrideSchedules()
    {
        return $this->hasMany(ShareCommissionPaymentOverride::class, 'schedule_id');
    }

    /**
     * @return Carbon
     * @throws CrimsException
     */
    public function getOverrideDate()
    {
        $purchase = $this->sharePurchase;

        if (is_null($purchase)) {
            throw new CrimsException("No share purchase linked to commission schedule id : " . $this->id);
        }

        return Carbon::parse($purchase->date);
    }
}
