<?php

namespace App\Cytonn\Models;

/**
 * Date: 15/10/2015
 * Time: 7:43 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ContactEntity extends BaseModel
{
    public $table = 'contact_entity_types';

    protected $guarded = ['id'];
}
