<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 04/02/2019
 * Time: 11:39
 */

namespace App\Cytonn\Models;

class RealEstateInstructionType extends BaseModel
{
    public $table = 'real_estate_instruction_type';

    protected $guarded = ['id'];
}
