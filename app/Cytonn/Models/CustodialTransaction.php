<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Portfolio\DepositHolding;
use Cytonn\Custodial\Transact\TransactionRepository;
use Cytonn\Investment\BaseInvestment;
use Cytonn\Portfolio\Portfolio;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class CustodialTransaction
 */
class CustodialTransaction extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string
     */
    public $table = 'custodial_transactions';


    /**
     * @var array
     */
    protected $guarded = ['id'];

    public $repo;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'custodial_transactions.description' => 10,
            'custodial_transactions.received_from' => 10,
            'custodial_transactions.bank_transaction_id' => 10,
            'custodial_transactions.bank_reference_no' => 10,
            'custodial_transactions.mpesa_confirmation_code' => 10,
            'custodial_transactions.outgoing_reference' => 10,
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10,
            'custodial_transactions.amount' => 10,
            'commission_recepients.name' => 10,
            'commission_recepients.email' => 10
        ],
        'joins' => [
            'clients' => ['custodial_transactions.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id'],
            'commission_recepients' => ['custodial_transactions.commission_recipient_id', 'commission_recepients.id']
        ],
    ];


    /**
     * CustodialTransaction constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new TransactionRepository($this);
    }

    /**
     * @return mixed|string
     */
    public function typeName()
    {
        $type = CustodialTransactionType::find($this->type);

        if (!is_null($type)) {
            return $type->name;
        }

        return '';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transactionType()
    {
        return $this->belongsTo(CustodialTransactionType::class, 'type');
    }

    public function transferIn()
    {
        return $this->hasOne(CustodialTransfer::class, 'in_transaction_id');
    }

    public function transferOut()
    {
        return $this->hasOne(CustodialTransfer::class, 'out_transaction_id');
    }

    public function scopeIsTransferFrom($query, $accounts)
    {
        return $query->whereHas('transferIn', function ($trf) use ($accounts) {
            $trf->whereHas('sender', function ($sender) use ($accounts) {
                $sender->whereNotIn('custodial_account_id', $accounts->lists('id'));
            });
        });
    }

    public function scopeIsTransferTo($query, $accounts)
    {
        return $query->whereHas('transferOut', function ($trf) use ($accounts) {
            $trf->whereHas('receiver', function ($sender) use ($accounts) {
                $sender->whereNotIn('custodial_account_id', $accounts->lists('id'));
            });
        });
    }


    public function scopeIsExpense($query)
    {
        return $query->whereHas('transactionType', function ($type) {
            $type->where('is_expense', 1);
        });
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'custodial_account_id');
    }

    public function balance()
    {
        return $this->custodialAccount->transactionBalance($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(PortfolioTransactionApproval::class, 'approval_id');
    }

    public function clientPayment()
    {
        return $this->hasOne(ClientPayment::class, 'custodial_transaction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bankInstruction()
    {
        return $this->hasOne(BankInstruction::class, 'custodial_transaction_id');
    }

    public function payment()
    {
        return $this->hasOne(ClientPayment::class, 'custodial_transaction_id');
    }

    /*
     * Relationship between a custodial transaction and a deposit holding
     */
    public function depositHolding()
    {
        return $this->hasOne(DepositHolding::class, 'custodial_invest_transaction_id');
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    /**
     * @param $investment
     * @param $type_text
     * @param $amount
     * @param $description
     * @param $date
     * @return static
     */
    public static function makeFromInvestment($investment, $type_text, $amount, $description, $date)
    {
        $trans = new static();
        $trans->type = CustodialTransactionType::where('name', $type_text)->first()->id;
        $trans->amount = $amount;
        $trans->date = $date;
        $trans->description = $description;

        if ($investment instanceof ClientInvestment) {
            $trans->transaction_owners_id = (new BaseInvestment())->getClientTransactionOwner($investment)->id;
            $trans->custodial_account_id = $investment->product->custodial_account_id;
        } elseif ($investment instanceof DepositHolding) {
            $trans->transaction_owners_id = (new Portfolio())->getPortfolioTransactionOwner($investment)->id;
            $trans->custodial_account_id = $investment->custodial_account_id;
        } else {
            throw new \InvalidArgumentException('The investment is not a valid type');
        }

        $trans->save();

        return $trans;
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas('transactionType', function ($type) use ($slug) {
            if (!is_array($slug)) {
                $slug = [$slug];
            }

            $type->whereIn('name', $slug);
        });
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    public function whtTax()
    {
        return $this->hasMany(WithholdingTax::class, 'custodial_transaction_id');
    }
}
