<?php

namespace App\Cytonn\Models;

/**
 * Date: 07/10/2015
 * Time: 2:08 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class PasswordReminderRequest extends BaseModel
{
    public $table = 'password_reminder_requests';

    protected $guarded = ['$id'];
}
