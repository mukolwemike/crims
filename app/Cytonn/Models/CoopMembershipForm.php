<?php

namespace App\Cytonn\Models;

/**
 * Date: 06/09/2016
 * Time: 8:50 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class CoopMembershipForm extends BaseModel
{
    use \Nicolaslopezj\Searchable\SearchableTrait;

    protected $table = 'coop_membership_forms';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'client_code' => 10,
            'phone' => 10,
            'email' => 10,
            'id_or_passport' => 10,
            'firstname' => 10,
            'middlename' => 10,
            'lastname' => 10,
        ]
    ];


    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getContactPersonsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setContactPersonsAttribute($value)
    {
        $this->attributes['contact_persons'] = serialize($value);
    }

    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
