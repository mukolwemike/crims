<?php

namespace App\Cytonn\Models;

use Cytonn\Investment\FundManager\FundManagerScopingTrait;

/**
 * Date: 11/12/2015
 * Time: 3:31 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ActivityLog extends BaseModel
{
    use FundManagerScopingTrait;

    public $table = 'activity_log';

    protected $guarded = ['id'];
}
