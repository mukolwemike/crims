<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class RealEstateCommissionPercentage extends BaseModel
{
    /*
    * Get the soft deletes trait
    */
    use SoftDeletes;

    /*
     * Table for the model
     */
    protected $table = 'realestate_commission_percentages';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'realestate_commission_structure_id',
        'realestate_commission_parameter_id',
        'value',
        'stage'
    ];

    /*
     * Relationship between real estate commission structure and  real estate commission percentages
     */
    public function structure()
    {
        return $this->belongsTo(RealEstateCommissionStructure::class, 'realestate_commission_structure_id');
    }

    /*
     * Relationship between real estate commission structure and  real estate commission percentages
     */
    public function parameter()
    {
        return $this->belongsTo(RealEstateCommissionParameter::class, 'realestate_commission_parameter_id');
    }
}
