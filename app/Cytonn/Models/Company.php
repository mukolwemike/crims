<?php

namespace App\Cytonn\Models;

class Company extends BaseModel
{
    protected $table = 'companies';

    protected $guarded = ['id'];
}
