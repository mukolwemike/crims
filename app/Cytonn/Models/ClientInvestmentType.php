<?php

namespace App\Cytonn\Models;

/**
 * Date: 9/14/15
 * Time: 9:32 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ClientInvestmentType extends BaseModel
{
    public $table = 'client_investment_types';

    public $guarded = ['id'];
}
