<?php

namespace App\Cytonn\Models;

/**
 * Date: 21/11/2015
 * Time: 4:21 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class CmsHistoricalSummary extends BaseModel
{
    public $table = 'cms_historical_summary';

    protected $guarded = ['id'];
}
