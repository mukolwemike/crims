<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Guard extends BaseModel
{
    /*
     * Get the softdeleted trait
     */
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'guards';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guard_type_id',
        'client_id',
        'minimum_balance',
        'portfolio_investor_id',
        'reason'
    ];

    /*
     * Relationship between a gaurd and client
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /*
     * Relationship between a gaurd and portfolio investor
     */
    public function portfolioInvestor()
    {
        return $this->belongsTo(PortfolioInvestor::class);
    }

    /*
     * Relationship between a gaurd and guard type
     */
    public function guardType()
    {
        return $this->belongsTo(GuardType::class);
    }

    /*
     * Relationship between a gaurd and guard type
     */
    public function scopeReGuard($query)
    {
        return $query->whereIn('guard_type_id', [4]);
    }

    /*
     * Relationship between a gaurd and guard type
     */
    public function scopeInvestmentsGuard($query)
    {
        return $query->whereIn('guard_type_id', [1, 2, 3]);
    }
}
