<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\RealEstate\RealestateCommissionPaymentOverride;
use App\Exceptions\CrimsException;
use Carbon\Carbon;
use Crims\Commission\CommissionScheduleTrait;
use Crims\Realestate\Commission\OverrideCalculator;
use Illuminate\Database\Eloquent\SoftDeletes;

class RealEstateCommissionPaymentSchedule extends BaseModel
{
    use SoftDeletes;
    use CommissionScheduleTrait;

    protected $table = 'realestate_commission_payment_schedules';

    protected $guarded = ['id'];

    public function commission()
    {
        return $this->belongsTo(RealestateCommission::class, 'commission_id');
    }

    public function type()
    {
        return $this->belongsTo(RealEstateCommissionPaymentScheduleType::class, 'type_id');
    }

    public function getHistoryAttribute($value)
    {
        return json_decode($value);
    }

    public function setHistoryAttribute($value)
    {
        $this->attributes['history'] = json_encode($value);
    }

    /**
     * @param Carbon|null $date
     * @return OverrideCalculator
     */
    public function overrideCalculator(Carbon $date = null)
    {
        $date = $date ? $date : $this->getOverrideDate();

        return new OverrideCalculator($date, $this);
    }

    /**
     * @throws CrimsException
     */
    public function generateOverrides()
    {
        $this->overrideCalculator()->awardOverrides();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overrideSchedules()
    {
        return $this->hasMany(RealestateCommissionPaymentOverride::class, 'schedule_id');
    }

    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->whereHas('commission', function ($query) use ($recipient) {
            $query->where('recipient_id', $recipient->id);
        });
    }

    /**
     * @return Carbon
     * @throws CrimsException
     */
    public function getOverrideDate()
    {
        $holding = @$this->commission->holding;

        if (is_null($holding)) {
            throw new CrimsException("No unit holding linked to commission schedule id : " . $this->id);
        }

        if (isNotEmptyOrNull($holding->reservation_date)) {
            $date = $holding->reservation_date;
        } else {
            $payment = $holding->payments()->oldest('date')->first();

            if ($payment) {
                $holding->update(['reservation_date' => $payment->date]);
                $date = $payment->date;
            } else {
                $date = $holding->created_at;
            }
        }

        return Carbon::parse($date);
    }
}
