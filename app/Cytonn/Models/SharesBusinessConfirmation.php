<?php

namespace App\Cytonn\Models;

use Illuminate\Support\Facades\Auth;

class SharesBusinessConfirmation extends BaseModel
{
    public $table = 'shares_business_confirmations';

    protected $guarded = ['id'];

    public static function add($data)
    {
        $bc = new static();
        $bc->fill($data);
        $bc->sent_by = Auth::user()->id;
        $bc->save();
        return $bc;
    }

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holding()
    {
        return $this->belongsTo(ShareHolding::class, 'holding_id');
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }
}
