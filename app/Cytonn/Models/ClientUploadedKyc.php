<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Client\Kyc\IprsValidation;
use Cytonn\Core\Storage\StorageInterface;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\ClientInstruction\ClientUploadedKycRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ClientUploadedKyc extends BaseModel
{
    use SoftDeletes;

    protected $table = 'client_uploaded_kyc';

    protected $guarded = ['id'];

    protected $fileProvider;

    protected $filesystem;

    public $repo;

    public function __construct(array $attributes = [])
    {
        $this->repo = new ClientUploadedKycRepository($this);

        parent::__construct($attributes);

        $this->fileProvider = \App::make(StorageInterface::class);
        $this->filesystem = $this->fileProvider->filesystem();
    }

    public function application()
    {
        return $this->belongsTo(ClientFilledInvestmentApplication::class, 'application_id');
    }

    public function type()
    {
        return $this->belongsTo(ClientComplianceChecklist::class, 'kyc_id');
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function jointClient()
    {
        return $this->belongsTo(ClientJointDetail::class, 'client_joint_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function iprsValidation()
    {
        return $this->belongsTo(IprsValidation::class, 'iprs_validation_id');
    }

    public function upload(array $documents, Client $client)
    {
        //upload file to storage
        foreach ($documents as $kyc => $document) {
            $upload = new ClientUploadedKyc();
            $filename = $document['doc']->getClientOriginalName();
            $document = (new ClientUploadedKycRepository($this))
                ->uploadKYC(file_get_contents($document['doc']), $document['doc']->getClientOriginalExtension());
            $upload->document_id = $document->id;
            $upload->kyc_id = $kyc;
            $upload->filename = $filename;
            $upload->client_id = $client->id;
            $upload->user_id = Auth::user()->id;
            $upload->save();
        }
    }

    public function uploadForm($document, array $items, Client $client)
    {
        //upload file to storage
        $document = (new ClientUploadedKycRepository($this))
            ->uploadKYC(file_get_contents($document['doc']), $document['doc']->getClientOriginalExtension());

        return $document;
    }

    public function path()
    {
        if (!$this->type) {
            throw new CRIMSGeneralException('Document has no type');
        }

        $path = 'kyc';

        return $this->fileProvider->getStorageLocation() . '/' . $path . '/' . $this->filename;
    }
}
