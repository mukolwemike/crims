<?php
/**
 * Date: 31/10/2017
 * Time: 12:11
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use Carbon\Carbon;
use Cytonn\Reporting\Custom\Models\Reportable;
use Cytonn\Structured\Calculations\Calculator;

/**
 * Class StructuredInvestment
 *
 * @package Cytonn\Models
 */
abstract class StructuredInvestment extends BaseModel implements Reportable
{

    /**
     * @var null
     */
    protected $table = null;

    /**
     * @param $query
     * @param Carbon $statementDate
     * @param Carbon|null $from
     * @return mixed
     */
    public function scopeStatement($query, Carbon $statementDate, Carbon $from = null)
    {
        $cutOff = $statementDate->copy()->subMonthNoOverflow();

        if ($from) {
            $cutOff = $from;
        }

        return $query->where('invested_date', '<=', $statementDate->copy()->addDay())
            ->where(
                function ($query) use ($statementDate, $cutOff) {
                    $query->active()
                        ->orWhere(
                            function ($q) use ($statementDate, $cutOff) {
                                //withdrawn within one month
                                $q->active(false)
                                    ->where('withdrawal_date', '>=', $cutOff);
                            }
                        );
                }
            )->orderBy('invested_date', 'ASC');
    }

    /**
     * @param $date
     * @param $as_at_next_day
     * @return Calculator
     */
    abstract public function calculate(Carbon $date = null, $as_at_next_day);
}
