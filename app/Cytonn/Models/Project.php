<?php

namespace App\Cytonn\Models;

use Cytonn\Investment\FundManager\FundManagerScopingTrait;
use Cytonn\Models\RealEstateUnitGroup;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 18/05/2016
 * Time: 5:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class Project extends BaseModel
{
    use FundManagerScopingTrait;

    /**
     * @var \Cytonn\Realestate\Project\ProjectRepository
     */
    public $repo;


    use SearchableTrait, SoftDeletes;

    protected $dates = ['completion_date', 'penalty_start_date'];

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'projects.name' => 10
        ]
    ];


    /**
     * @var string
     */
    protected $table = 'projects';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->repo = new \Cytonn\Realestate\Project\ProjectRepository($this);

        parent::__construct();
    }

    public function site()
    {
        return 'https://crm.cytonn.com/events/sharp-investors-tour';
    }

    /**
     * @return mixed
     */
    public function reservationFee()
    {
        return $this->reservation_fee;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function units()
    {
        return $this->hasMany(RealestateUnit::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(ProjectType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tranches()
    {
        return $this->hasMany(RealEstateUnitTranche::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function realestateCampaignStatementItems()
    {
        return $this->hasMany(RealestateCampaignStatementItem::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionRates()
    {
        return $this->hasMany(RealestateCommissionRate::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sizeNumbers()
    {
        return $this->hasMany(RealEstateSizeNumber::class, 'project_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'custodial_account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function receivingAccounts()
    {
        return $this->belongsToMany(
            CustodialAccount::class,
            'products_projects_receiving_accounts',
            'project_id',
            'account_id'
        );
    }

    public function leviesInterest()
    {
        return (bool)$this->levies_interest;
    }

    /**
     * @return mixed
     */
    public function availableSizes()
    {
        $sizeNumbers = $this->sizeNumbers()->get();

        return $sizeNumbers->map(
            function ($sn) {
                return $sn->size;
            }
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    public function projectManager()
    {
        return $this->belongsTo(User::class, 'project_manager_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function unitGroups()
    {
        return $this->hasMany(RealEstateUnitGroup::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany(Document::class, 'project_id');
    }

    public function projectFloors()
    {
        return $this->hasMany(ProjectFloor::class, 'project_id');
    }

    public function projectTypes()
    {
        return $this->hasMany(RealEstateType::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(ClientPayment::class, 'project_id');
    }
}
