<?php

namespace App\Cytonn\Models;

class FundType extends BaseModel
{
    protected $table = 'fund_types';

    protected $guarded = ['id'];
}
