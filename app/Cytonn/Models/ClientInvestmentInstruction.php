<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use Cytonn\Investment\ClientInstruction\ClientInvestmentInstructionRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientInvestmentInstruction extends BaseModel
{
    use SearchableTrait, SoftDeletes;

    public $table = 'client_investment_instructions';

    protected $guarded = ['id'];

    public $repo;

    protected $dates = ['due_date'];

    protected $searchable = [
        'columns' => [
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.corporate_trade_name' => 10,
            'contacts.email' => 10,
            'clients.client_code' => 10
        ],
        'joins' => [
            'client_investments' => ['client_investment_instructions.investment_id', 'client_investments.id'],
            'clients' => ['client_investments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    public function __construct(array $attributes = [])
    {
        $this->repo = new ClientInvestmentInstructionRepository($this);

        parent::__construct($attributes);
    }

    public static function add($data)
    {
        $instruction = new static();

        $instruction->fill($data);

        $instruction->save();

        return $instruction;
    }

    public function type()
    {
        return $this->belongsTo(ClientInstructionType::class, 'type_id');
    }

    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function isRollover()
    {
        return $this->type->name == 'rollover';
    }

    public function isWithdrawal()
    {
        return $this->type->name == 'withdraw';
    }

    public function isPartialRollover()
    {
        if (!$this->isRollover()) {
            return false;
        }

        if ($this->amount_select === 'principal_interest') {
            return false;
        }

        $amount_affected = $this->repo->calculateAmountAffected();

        $investment_value = $this->investment->repo->getTotalValueOfAnInvestment();

        if ($amount_affected == $investment_value || $amount_affected > $investment_value) {
            return false;
        }

        return true;
    }

    public function filledBy()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }

    public function executed()
    {
        if ($this->investment) {
            if ($this->investment->withdrawn || $this->investment->rolled) {
                return true;
            }

            if ($this->investment->withdrawals()->where('created_at', '>=', $this->created_at)->exists()) {
                return true;
            }

            return false;
        }

        return false;
    }

    public function scopeOfType($query, $name)
    {
        return $query->whereHas(
            'type',
            function ($t) use ($name) {
                $t->where('name', $name);
            }
        );
    }

    public function investments()
    {
        return $this->belongsToMany(
            ClientInvestment::class,
            'client_investment_rollover_instructions',
            'instruction_id',
            'investment_id'
        )->withTimestamps();
    }

    /**
     * @return Collection
     */
    public function allInvestments()
    {
        return $this->investments->push($this->investment)->unique('id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function rolloverInstructions()
    {
        return $this->hasMany(ClientInvestmentRolloverInstruction::class, 'instruction_id');
    }

    public function account()
    {
        return $this->belongsTo(ClientBankAccount::class, 'client_account_id');
    }

    public function clientApprovals()
    {
        return $this->hasMany(ClientInstructionApproval::class, 'instruction_id');
    }
}
