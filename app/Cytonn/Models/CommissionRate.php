<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 05/10/2015
 * Time: 11:51 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class CommissionRate extends BaseModel
{
    use SoftDeletes;

    public $table = 'commission_rates';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'commission_recipient_type_id',
        'rate',
        'currency_id',
        'product_id',
        'date'
    ];

    /*
     * Relationship between a commission rate and recipient type
     */
    public function commissionRecipientType()
    {
        return $this->belongsTo(CommissionRecipientType::class, 'commission_recipient_type_id');
    }

    /*
     * Relationship between a commission rate and currency
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /*
     * Relationship between a commission rate and product
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /*
     * Filter the rates based on a product if any
     */
    public function scopeForProduct($query, Product $product)
    {
        return $query->where('product_id', $product->id);
    }

    /*
     * Filter the rates based on a currency if any
     */
    public function scopeForCurrency($query, Currency $currency)
    {
        return $query->where('currency_id', $currency->id);
    }


    public static function forForm($investment = null)
    {
        $rates = CommissionRate::all();

        if ($investment instanceof ClientInvestment) {
            $currency = $investment->product->currency;
        }
        if ($investment instanceof Product) {
            $currency = $investment->currency;
        }

        if (!isset($currency)) {
            $investment = null;
        }

        if ($investment) {
            $rates = CommissionRate::where('currency_id', $currency->id)->get();
        }

        $r = [];
        foreach ($rates as $rate) {
            $r[(string)$rate->rate] = $rate->name;
        }

        return $r;
    }
}
