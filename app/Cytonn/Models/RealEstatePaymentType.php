<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Date: 06/06/2016
 * Time: 11:43 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstatePaymentType extends BaseModel
{
    use SoftDeletes;

    protected $table = 'realestate_payment_types';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function schedules()
    {
        return $this->hasMany(RealEstatePaymentSchedule::class, 'payment_type_id');
    }
}
