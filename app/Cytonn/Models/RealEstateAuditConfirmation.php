<?php

namespace App\Cytonn\Models;

class RealEstateAuditConfirmation extends BaseModel
{
    protected $table = 'real_estate_audit_confirmations';

    protected $fillable = [
        'batch',
        'client_code',
        'name',
        'sent',
        'house_number',
        'spv',
        'product',
    ];

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
