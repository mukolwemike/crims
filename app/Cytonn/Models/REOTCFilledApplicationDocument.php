<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 07/01/2019
 * Time: 12:41
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class REOTCFilledApplicationDocument extends BaseModel
{
    use SoftDeletes;

    protected $table = 're_otc_filled_application_documents';

    protected $guarded = ['id'];

    public function application()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'application_id');
    }

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function complianceDocument()
    {
        return $this->belongsTo(ClientComplianceChecklist::class, 'client_compliance_checklist_id');
    }

    public function scopeDocumentType($query, $type)
    {
        return $query->whereHas('document', function ($q) use ($type) {
            return $q->ofType($type);
        });
    }
}
