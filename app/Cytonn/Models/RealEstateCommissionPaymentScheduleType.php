<?php

namespace App\Cytonn\Models;

/**
 * Date: 12/01/2017
 * Time: 12:32
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateCommissionPaymentScheduleType extends BaseModel
{
    protected $table = 'realestate_commission_payment_schedule_types';
}
