<?php

namespace App\Cytonn\Models;

use Cytonn\Investment\CurrencyRepository;
use Cytonn\Models\ExchangeRate;

/**
 * Class Currency
 */
class Currency extends BaseModel implements \Crims\Base\Models\Currency
{

    /**
     * @var string
     */
    protected $table = 'currencies';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var CurrencyRepository
     */
    public $repo;

    /**
     * Currency constructor.
     *
     * @param $repo
     */
    public function __construct(array $attributes = [])
    {
        $this->repo = new CurrencyRepository($this);

        parent::__construct($attributes);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(CustodialAccount::class, 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'currency_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->name . ' (' . $this->code . ')';
    }

    /*
     * Relationship between a currency and exchange rates
     */
    public function baseExchangeRates()
    {
        return $this->hasMany(ExchangeRate::class, 'base_id');
    }

    /*
     * Relationship between a currency and exchange rates
     */
    public function toExchangeRates()
    {
        return $this->hasMany(ExchangeRate::class, 'to_id');
    }
}
