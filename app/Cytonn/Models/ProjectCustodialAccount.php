<?php

namespace App\Cytonn\Models;

/**
 * Class ProjectCustodialAccount
 */
class ProjectCustodialAccount extends BaseModel
{

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $table = 'project_custodial_accounts';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'custodial_account_id');
    }
}
