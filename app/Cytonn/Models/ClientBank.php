<?php

namespace App\Cytonn\Models;

use Cytonn\SearchOperations\ElasticOperationsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ClientBank
 */
class ClientBank extends BaseModel
{
    use SoftDeletes;

    //    use SearchableTrait;

    /*
     * Get the elastic operations trait
     */
    use ElasticOperationsTrait;

    /*
     * Define the mapping properties
     */
    protected $mappingProperties = [
    ];

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    /**
     * @var array $table
     */
    protected $table = 'client_banks';

    protected $searchable = [
        'columns' => [
            'client_banks.name' => 10,
            'client_banks.swift_code' => 10,
            'client_banks.clearing_code' => 10,
        ]
    ];

    /**
     * @var array $with
     */
    protected $with = ['branches'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function branches()
    {
        return $this->hasMany(ClientBankBranch::class, 'bank_id');
    }
}
