<?php

namespace App\Cytonn\Models;

//use = 'Cytonn\Clients\Approvals\Handlers'\ClientTransactionApproval;
use App\Cytonn\Models\Shares\StampDuty;
use Cytonn\Shares\ShareHoldingRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class ShareHolding
 */
class ShareHolding extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'share_purchases';

    protected $guarded = ['id'];

    public $repo;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5
        ],
        'joins' => [
            'clients' => ['clients.id', 'share_purchases.client_id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * ShareHolding constructor.
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new ShareHoldingRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shareHolder()
    {
        return $this->belongsTo(ShareHolder::class, 'share_holder_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(SharesCategory::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        // return $this->belongsTo(CoopPayment::class, 'payment_id');
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coopPayment()
    {
        return $this->belongsTo(CoopPayment::class, 'payment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function confirmation()
    {
        return $this->hasOne(SharesBusinessConfirmation::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function purchaseOrder()
    {
        return $this->belongsTo(SharesPurchaseOrder::class, 'shares_purchase_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salesOrder()
    {
        return $this->belongsTo(SharesSalesOrder::class, 'shares_sales_order_id');
    }

    /**
     * @param $holding
     * @return mixed
     */
    public function scopeForFundManager($holding)
    {
        if (!$this->currentFundManager()) {
            return $holding;
        }

        return $holding->whereHas(
            'shareHolder',
            function ($holder) {
                return $holder->whereHas(
                    'entity',
                    function ($entity) {
                        $entity->where('fund_manager_id', $this->currentFundManager()->id);
                    }
                );
            }
        );
    }

    public function stampDuty()
    {
        return $this->hasMany(StampDuty::class, 'share_purchase_id');
    }
}
