<?php

namespace App\Cytonn\Models;

//use = 'Cytonn\Clients\Approvals\Handlers'\ClientTransactionApproval;
use Cytonn\Coop\Payments\CoopPaymentRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class CoopPayment extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
        ],
        'joins' => [
            'clients' => ['coop_payments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
        ]
    ];

    public $table = 'coop_payments';

    protected $guarded = ['id'];

    public $repo;

    protected $dates = ['date'];


    /**
     * CoopPayment constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new CoopPaymentRepository($this);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function shareHolding()
    {
        return $this->hasOne(ShareHolding::class, 'payment_id');
    }

    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    public function scopeForFundManager($payment)
    {
        if (!$this->currentFundManager()) {
            return $payment;
        }

        return $payment->whereHas(
            'client',
            function ($client) {
                $client->where('fund_manager_id', $this->currentFundManager()->id);
            }
        );
    }
}
