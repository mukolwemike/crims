<?php

namespace App\Cytonn\Models;

class ClientTopUpApplication extends BaseModel
{
    protected $guarded = ['id'];

    protected $table = 'client_investment_topup_applications';

    public function initialApplication()
    {
        return $this->belongsTo('App\ClientInvestmentApplication', 'initial_application_id');
    }
}
