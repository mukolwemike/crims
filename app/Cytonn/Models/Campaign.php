<?php

namespace App\Cytonn\Models;

class Campaign extends BaseModel
{
    protected $table = 'campaigns';

    protected $guarded = ['id'];
}
