<?php

namespace App\Cytonn\Models;

class ClientInterest extends BaseModel
{
    protected $table = 'client_interests';

    protected $guarded = ['id'];
}
