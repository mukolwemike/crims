<?php
/**
 * Date: 04/07/2017
 * Time: 11:52
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Currency;
use Laracasts\Presenter\PresentableTrait;

class ExchangeRate extends BaseModel
{
    use PresentableTrait;

    /*
     * Get the presenter
     */
    protected $presenter = 'Cytonn\Custodial\ExchangeRates\ExchangeRatePresenter';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exchange_rates';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'base_id',
        'to_id',
        'rate',
        'date'
    ];

    /*
     * Relationship between a currency and exchange rate
     */
    public function baseCurrency()
    {
        return $this->belongsTo(Currency::class, 'base_id');
    }

    /*
     * Relationship between a currency and exchange rate
     */
    public function toCurrency()
    {
        return $this->belongsTo(Currency::class, 'to_id');
    }
}
