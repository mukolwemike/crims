<?php

namespace App\Cytonn\Models;

use Cytonn\Core\Storage\StorageInterface;
use Cytonn\Uuids\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Webpatser\Uuid\Uuid;

/**
 * Date: 14/07/2016
 * Time: 8:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class Document extends BaseModel
{
    use SoftDeletes, UuidTrait;

    /**
     * @var string
     */
    protected $table = 'documents';

    /*
     * @var StorageInterface
     */
    /**
     * @var
     */
    protected $filesystem;

    /*
     * @var StorageInterface
     */
    /**
     * @var
     */
    protected $fileProvider;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Document constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fileProvider = \App::make(StorageInterface::class);

        $this->filesystem = $this->fileProvider->filesystem();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(DocumentType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientUploadedKyc()
    {
        return $this->hasOne(ClientUploadedKyc::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function portfolioInvestor()
    {
        return $this->belongsTo(PortfolioInvestor::class, 'portfolio_investor_id');
    }

    public static function upload(
        $file_contents,
        $file_ext,
        $type_slug,
        $title = null,
        $date = null,
        $project_id = null,
        $product_id = null,
        $compliance_document = false,
        $portfolioInvestorId = null,
        $client_id = null,
        $user_id = null
    ) {
        $filename = Uuid::generate()->string . '.' . $file_ext;

        return \DB::transaction(
            function () use (
                $type_slug,
                $filename,
                $title,
                $date,
                $file_contents,
                $product_id,
                $project_id,
                $portfolioInvestorId,
                $compliance_document,
                $client_id,
                $user_id
            ) {
                $type = (new DocumentType())->where('slug', $type_slug)->first();

                $document = (new Document())->create(
                    [
                        'type_id' => $type->id,
                        'filename' => $filename,
                        'title' => $title,
                        'project_id' => $project_id == 0 ? null : $project_id,
                        'product_id' => $product_id == 0 ? null : $product_id,
                        'portfolio_investor_id' => $portfolioInvestorId == 0 ? null : $portfolioInvestorId,
                        'compliance_document' => $compliance_document,
                        'client_id' => $client_id,
                        'user_id' => $user_id
                    ]
                );

                \App::make(StorageInterface::class)->filesystem()->put($document->path(), $file_contents);

                return $document;
            }
        );
    }

    public function updateDoc(
        $file_contents,
        $file_ext,
        $type_slug,
        $title = null,
        $date = null,
        $project = null,
        $product = null,
        $compliance_document = null,
        $portfolioInvestor = null
    ) {
        $filename = Uuid::generate()->string . '.' . $file_ext;

        return \DB::transaction(
            function () use (
                $type_slug,
                $filename,
                $title,
                $date,
                $product,
                $project,
                $file_contents,
                $compliance_document,
                $portfolioInvestor
            ) {
                $type = DocumentType::where('slug', $type_slug)->first();

                $this->fill(
                    [
                        'type_id' => $type->id,
                        'filename' => $filename,
                        'title' => $title,
                        'date' => $date,
                        'project_id' => $project == 0 ? null : $project,
                        'product_id' => $product == 0 ? null : $product,
                        'portfolio_investor_id' => $portfolioInvestor == 0 ? null : $portfolioInvestor,
                        'compliance_document' => $compliance_document
                    ]
                );
                $this->save();

                //save the file in the filesystem
                \App::make(StorageInterface::class)->put($this->path(), $file_contents);

                return $this;
            }
        );
    }

    public static function make($file_contents, $file_ext, $type_slug, $title = null)
    {
        $doc = new static();

        return $doc->upload($file_contents, $file_ext, $type_slug, $title);
    }

    public function path()
    {
        if (!$this->type) {
            throw new \Cytonn\Exceptions\CRIMSGeneralException('Document has no type');
        }

        $path = $this->type->path();

        return $path . '/' . $this->filename;
    }

    /**
     * @return mixed
     */
    public function extension()
    {
        return pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    /**
     * @return mixed|string
     */
    public function qualifiedFilename()
    {
        if ($title = $this->title) {
            return $title . '.' . $this->extension();
        }

        return $this->filename;
    }

    public function data()
    {
        return $this->filesystem->read($this->path());
    }

    public function scopeComplianceDocuments($query, Project $project = null, $state = true)
    {
        if ($project) {
            return $query->where([
                'compliance_document' => true,
                'project_id' => $project->id
            ])->whereNotNull('approval_id');
        }
        return $query->where('compliance_document', true)->whereNotNull('approval_id');
    }

    public function scopeTypeSlug($query, $slug)
    {
        return $query->whereHas(
            'type',
            function ($type) use ($slug) {
                return $type->where('slug', $slug);
            }
        );
    }

    public function scopeModule($query, $module)
    {
        if (!in_array($module, ['investments', 'realestate', 'portfolio_investors', 'others'])) {
            return $query;
        }

        if ($module == 'investments') {
            return $query->whereNotNull('product_id');
        } elseif ($module == 'realestate') {
            return $query->whereNotNull('project_id');
        } elseif ($module == 'portfolio_investors') {
            return $query->whereNotNull('portfolio_investor_id');
        } elseif ($module == 'others') {
            return $query->whereNull('product_id')->whereNull('project_id');
        }
    }

    public function scopeApproved($query, $approved = true)
    {
        if ($approved) {
            return $query->whereNotNull('approval_id');
        }

        return $query->where(
            function ($q) {
                $q->whereNull('approval_id')->orWhere('approval_id', 0);
            }
        );
    }

    public function getHistoryAttribute($value)
    {
        return json_decode($value);
    }

    public function setHistoryAttribute($value)
    {
        $this->attributes['history'] = json_encode($value);
    }

    public function addHistory(Document $document)
    {
        $history = is_null($this->history) ? [] : $this->history;

        $history[] = $document->id;
        $this->history = $history;

        $this->save();
    }


    public function checkAccess(ClientUser $user)
    {
        if ($this->public) {
            return true;
        }

        $client = $this->client ? $this->client : $user->clients()->first();

        return $user->shouldAccessClient($client);
    }

    public function saleAgreement()
    {
        return $this->hasOne(SalesAgreement::class, 'document_id');
    }

    public function loo()
    {
        return $this->hasOne(Loo::class, 'document_id');
    }


    public function scopeOfType($query, $slug)
    {
        return $query->whereHas('type', function ($type) use ($slug) {
            $type->where('slug', $slug);
        });
    }

    /**
     * @param $query
     * @param Client $clients | array $clients
     * @return mixed
     */
    public function scopeBelongsToClient($query, $clients)
    {
        if ($clients instanceof Client) {
            $clients = [$clients->id];
        }

        return $query->where($this->realEstateDocs($clients))
            ->orWhere($this->structuredProductsDocs($clients))
            ->orWhere($this->financialDocs());
    }

    private function structuredProductsDocs($clients)
    {
        return function ($query) use ($clients) {
            $query->ofType('information_memorandum')
                ->whereHas('product', function ($product) use ($clients) {
                    $product->whereHas('investments', function ($i) use ($clients) {
                        $i->whereIn('client_id', $clients);
                    });
                })->where('public', true);
        };
    }

    private function realEstateDocs($clients)
    {
        $holdingClosure = function ($holding) use ($clients) {
            $holding->whereIn('client_id', $clients);
        };

        return function ($query) use ($holdingClosure) {
            $query->where(function ($doc) use ($holdingClosure) {
                $doc->ofType('sales_agreement')->whereHas('saleAgreement', function ($sa) use ($holdingClosure) {
                    $sa->whereHas('holding', $holdingClosure);
                });
            })->orWhere(function ($doc) use ($holdingClosure) {
                $doc->ofType('loo')->whereHas('loo', function ($loo) use ($holdingClosure) {
                    $loo->whereHas('holding', $holdingClosure);
                });
            });
        };
    }

    private function financialDocs()
    {
        return function ($query) {
            return $query->ofType('financials')->where('public', true);
        };
    }

    public function approvalDocuments()
    {
        return $this->hasOne(TransactionApprovalDocument::class);
    }
}
