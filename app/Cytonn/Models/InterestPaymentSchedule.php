<?php

namespace App\Cytonn\Models;

/**
 * Date: 05/11/2015
 * Time: 3:40 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class InterestPaymentSchedule extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Nicolaslopezj\Searchable\SearchableTrait;


    /**
     * @var string
     */
    public $table = 'interest_payment_schedule';

    /**
     * @var array
     */
    protected $dates = ['date'];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10
        ],
        'joins' => [
            'client_investments' => ['interest_payment_schedule.investment_id', 'client_investments.id'],
            'clients' => ['client_investments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * @return mixed
     */
    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id')->orderBy('client_investments.client_id');
    }

    public function calculatePaymentAmount($date)
    {
        $available = $this->investment->repo->getTotalAvailableInterestAtNextDay($date);

        if (!$this->fixed) {
            return $available;
        }

        if ($available > $this->amount) {
            return $this->amount;
        }

        return $available;
    }

    /**
     * @param $query
     * @param $start
     * @param $end
     * @return mixed
     */
    public function scopeBetweenDates($query, $start, $end)
    {
        return $query->where('date', '>=', $start)
            ->where('date', '<=', $end);
    }
}
