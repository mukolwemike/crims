<?php

namespace App\Cytonn\Models;

use App\Cytonn\BaseModel;

class RecipientRank extends BaseModel
{
    protected $table = 'commission_recipients_ranks';
}
