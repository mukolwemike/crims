<?php

namespace App\Cytonn\Models;

use App\Cytonn\Clients\application\ClientApplicationRepository;
use App\Cytonn\Investment\Applications\ClientFilledInvestmentApplicationRepository;
use App\Cytonn\Models\System\Channel;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Presenters\ClientFilledApplicationPresenter;
use Cytonn\Uuids\UuidTrait;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientFilledInvestmentApplication extends BaseModel
{
    use PresentableTrait, SearchableTrait, UuidTrait;

    public $table = 'client_filled_investment_applications';

    const EXISTING_CLIENT = 2;
    const NEW_CLIENT = 1;

    /*
     * @var ClientFilledInvestmentApplicationRepository
     */
    public $repo;

    public $clientRepo;

    protected $presenter = ClientFilledApplicationPresenter::class;

    protected $guarded = ['id', 'uuid'];

    protected $searchable = [
        'columns' => [
            'client_filled_investment_applications.firstname' => 10,
            'client_filled_investment_applications.middlename' => 10,
            'client_filled_investment_applications.lastname' => 10,
            'client_filled_investment_applications.email' => 10,
            'client_filled_investment_applications.registered_name' => 10,
            'client_filled_investment_applications.trade_name' => 10,
            'client_filled_investment_applications.id_or_passport' => 5
        ],
        'joins' => [
        ],
    ];

    /**
     * ClientFilledInvestmentApplication constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->repo = new ClientFilledInvestmentApplicationRepository($this);

        $this->clientRepo = new ClientApplicationRepository($this);

        parent::__construct($attributes);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function application()
    {
        return $this->hasOne(ClientInvestmentApplication::class, 'form_id');
    }

    public function filled()
    {
        return $this->application();
    }

    public function documents()
    {
        return $this->hasMany(ClientUploadedKyc::class, 'application_id');
    }

    public function jointHolders()
    {
        return $this->hasMany(
            ClientFilledInvestmentApplicationJointHolders::class,
            'application_id'
        );
    }

    public function contactPersons()
    {
        return $this->hasMany(ClientFilledInvestmentApplicationContactPersons::class, 'application_id');
    }

    public function bank()
    {
        return $this->belongsTo(ClientBank::class, 'investor_bank');
    }

    public function branch()
    {
        return $this->belongsTo(ClientBankBranch::class, 'investor_bank_branch');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function clientTitle()
    {
        return $this->belongsTo(Title::class, 'title');
    }

    public function commissionRecipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'financial_advisor');
    }

    public function filledDocuments()
    {
        return $this->hasMany(ClientFilledInvestmentApplicationDocument::class, 'application_id');
    }

    public function reFilledDocuments()
    {
        return $this->hasMany(REOTCFilledApplicationDocument::class, 'application_id');
    }

    public function isShortApplication()
    {
        if (!$this->unit_fund_id) {
            return false;
        }

        return $this->unitFund->short_name == 'CMMF' && $this->individual;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class, 'channel_id');
    }
}
