<?php

namespace App\Cytonn\Models;

class Country extends BaseModel
{
    protected $table = 'countries';

    protected $guarded = ['id'];

    /*
     * Relationship between contact and title
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }
}
