<?php

namespace App\Cytonn\Models;

use Cytonn\Investment\BankInstructionRepository;
use Cytonn\Investment\FundManager\Traits\HasInvestmentFundManagerTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 09/12/2015
 * Time: 10:41 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */
class BankInstruction extends BaseModel
{
    use SoftDeletes, SearchableTrait, HasInvestmentFundManagerTrait;

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'clients.id_or_passport' => 5,
            //            'clients.investor_bank'=>4,
            //            'clients.investor_bank_branch'=>4,
            //            'clients.investor_account_name'=>10,
            //            'clients.investor_account_number'=>10,
            'client_bank_accounts.account_name' => 4,
            'client_bank_accounts.account_number' => 10,
            //            'client_bank_accounts.bank'=>4,
            //            'client_bank_accounts.branch'=>4,
        ],
        'joins' => [
            'client_investments' => ['bank_instructions.investment_id', 'client_investments.id'],
            'clients' => ['client_investments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_bank_accounts' => ['clients.id', 'client_bank_accounts.client_id']
        ],
    ];


    /**
     * @var string
     */
    public $table = 'bank_instructions';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    public $repo;

    /**
     * BankInstruction constructor.
     *
     * @param    array $attributes
     * @internal param $repo
     */
    public function __construct(array $attributes = [])
    {
        $this->repo = new BankInstructionRepository($this);

        parent::__construct($attributes);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function combinationOf()
    {
        return $this->hasMany(BankInstruction::class, 'combined_to');
    }

    public function custodialTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'custodial_transaction_id');
    }

    public function receivingTransaction()
    {
        return $this->belongsTo(CustodialTransaction::class, 'receiving_transaction_id');
    }

    /**
     * Decode value before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getMembersAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Encode value after retrieval
     *
     * @param $value
     */
    public function setMembersAttribute($value)
    {
        $this->attributes['members'] = json_encode($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firstSignatory()
    {
        return $this->belongsTo(User::class, 'first_signatory_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function secondSignatory()
    {
        return $this->belongsTo(User::class, 'second_signatory_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientBankAccount()
    {
        return $this->belongsTo(ClientBankAccount::class, 'client_account_id');
    }

    /**
     * @param $summary
     * @return mixed
     */
    public function scopeForFundManager($summary)
    {
        if (!$this->currentFundManager()) {
            return $summary;
        }
        return $summary->whereHas(
            'investment',
            function ($investment) {
                $investment->whereHas(
                    'product',
                    function ($product) {
                        $product->where('fund_manager_id', $this->currentFundManager()->id);
                    }
                );
            }
        );
    }

    /**
     * @param $query
     * @param $start
     * @param $end
     * @return mixed
     */
    public function scopeBetweenDates($query, $start = null, $end = null)
    {
        if (is_null($start) and is_null($end)) {
            return $query;
        }

        if ($start) {
            $query = $query->where('date', '>=', $start);
        }
        if ($end) {
            $query = $query->where('date', '<=', $end);
        }

        return $query;
    }

    public function paymentFor()
    {
        $transaction = $this->custodialTransaction;

        $payment = $transaction->clientPayment;

        if ($payment->product) {
            return $payment->product->fund->name;
        }

        $for = $payment->present()->paymentFor;

        if (empty($for)) {
            return 'Unknown';
        }

        return $for;
    }
}
