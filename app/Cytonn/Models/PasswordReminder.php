<?php

namespace App\Cytonn\Models;

/**
 * Date: 07/10/2015
 * Time: 5:31 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class PasswordReminder extends BaseModel
{
    public $table = 'password_reminders';

    protected $guarded = ['id'];
}
