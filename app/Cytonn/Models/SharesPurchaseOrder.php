<?php

namespace App\Cytonn\Models;

//use = 'Cytonn\Clients\Approvals\Handlers'\ClientTransactionApproval;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class SharesPurchaseOrder
 */
class SharesPurchaseOrder extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'shares_purchase_orders';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.lastname' => 10,
            'contacts.middlename' => 10,
        ],
        'joins' => [
            'share_holders' => ['shares_purchase_orders.buyer_id', 'share_holders.id'],
            'clients' => ['share_holders.client_id', 'clients.id'],
            'contacts' => ['contacts.id', 'clients.contact_id'],
        ],
    ];

    /**
     * SharesPurchaseOrder constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function buyer()
    {
        return $this->belongsTo(ShareHolder::class, 'buyer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shareSales()
    {
        return $this->hasMany(ShareSale::class, 'shares_purchase_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sharePurchases()
    {
        return $this->hasMany(SharePurchases::class, 'shares_purchase_order_id');
    }

    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_id');
    }

    public function matchedSaleOrders()
    {
        return $this->belongsToMany(
            SharesSalesOrder::class,
            'share_order_matches',
            'purchase_order_id',
            'sale_order_id'
        )->withPivot('number')
            ->withTimestamps();
    }

    public function matchedEntitySales()
    {
        return $this->belongsToMany(
            SharesEntity::class,
            'share_order_matches',
            'purchase_order_id',
            'entity_id'
        )->withPivot('number')
            ->withTimestamps();
    }

    /**
     * @param $order
     * @param bool $matched
     * @return mixed
     */
    public function scopeMatched($order, $matched = true)
    {
        if ($matched) {
            return $order->where('matched', 1);
        }

        return $order->where('matched', 0)->orWhereNull('matched');
    }

    /**
     * @param $order
     * @param bool $matched
     * @return mixed
     */
    public function scopeForEntity($query, $entity)
    {
        return $query->whereHas('buyer', function ($q) use ($entity) {
            return $q->where('entity_id', $entity->id);
        });
    }

    /**
     * @param $query
     * @param $client
     * @return mixed
     */
    public function scopeForClient($query, $client)
    {
        return $query->whereHas('buyer', function ($q) use ($client) {
            return $q->where('client_id', $client->id);
        });
    }

    /**
     * @param $order
     * @param bool $cancelled
     * @return mixed
     */
    public function scopeCancelled($order, $cancelled = true)
    {
        if ($cancelled) {
            return $order->where('cancelled', 1);
        }

        return $order->where('cancelled', 0)->orWhereNull('cancelled');
    }

    /**
     * @param $order
     * @return mixed
     */
    public function scopeForFundManager($order)
    {
        if (!$this->currentFundManager()) {
            return $order;
        }

        return $order->whereHas('buyer', function ($holder) {
            return $holder->whereHas('entity', function ($entity) {
                $entity->where('fund_manager_id', $this->currentFundManager()->id);
            });
        });
    }

    /**
     * @return bool
     */
    public function boughtFromEntity()
    {
        return $this->matched
            && $this->shareSales()->count() == 1
            && is_null($this->shareSales()->first()->share_holder_id);
    }

    public function numberSold()
    {
        return SharePurchases::where('shares_purchase_order_id', $this->id)->sum('number');
    }

    public function balance()
    {
        return $this->number - $this->numberSold();
    }
}
