<?php

namespace App\Cytonn\Models;

class SharesSaleOrderConfirmation extends BaseModel
{
    /**
     * @var string $table
     */
    public $table = 'shares_sale_order_confirmations';

    /**
     * @var array $guarded
     */
    protected $guarded = ['id'];

    /**
     * @param $data
     * @return static
     */
    public static function add($data)
    {
        $bc = new static();
        $bc->fill($data);
        $bc->sent_by = \Auth::user()->id;
        $bc->save();
        return $bc;
    }

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function saleOrder()
    {
        return $this->belongsTo(SharesSalesOrder::class, 'share_sale_order_id');
    }
}
