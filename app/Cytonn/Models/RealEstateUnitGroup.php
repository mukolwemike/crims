<?php
/**
 * Date: 19/07/2017
 * Time: 10:48
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Models;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateUnit;

class RealEstateUnitGroup extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'real_estate_unit_groups';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'project_id'
    ];

    /*
     * Relationship between a project and a unit group
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /*
     * Relationship between a unit and a unit group
     */
    public function units()
    {
        return $this->hasMany(RealestateUnit::class, 'group_id');
    }
}
