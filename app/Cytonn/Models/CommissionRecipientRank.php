<?php

namespace App\Cytonn\Models;

use Cytonn\Models\CommissionOverrideRate;

/**
 * Date: 08/02/2017
 * Time: 15:31
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class CommissionRecipientRank extends BaseModel
{
    protected $table = 'commission_recipients_ranks';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipients()
    {
        return $this->hasMany(CommissionRecepient::class, 'rank_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rates()
    {
        return $this->hasMany(CommissionOverrideRate::class, 'rank_id');
    }
}
