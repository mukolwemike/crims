<?php

namespace App\Cytonn\Models;

class Employment extends BaseModel
{
    protected $table = 'employment';

    protected $guarded = ['id'];
}
