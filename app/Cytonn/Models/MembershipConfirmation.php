<?php

namespace App\Cytonn\Models;

/**
 * Date: 19/09/2016
 * Time: 12:44 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class MembershipConfirmation extends BaseModel
{
    protected $guarded = ['id'];

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }

    public function payment()
    {
        return $this->belongsTo(CoopPayment::class, 'payment_id');
    }

    public function clientPayment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }
}
