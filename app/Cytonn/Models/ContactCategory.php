<?php

namespace App\Cytonn\Models;

class ContactCategory extends BaseModel
{
    protected $guarded = ['id'];

    protected $table = 'contact_categories';
}
