<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Portfolio\SuspenseTransaction;
use App\Cytonn\Presenters\PortfolioTransactionApprovalPresenter;
use Cytonn\Handlers\EventDispatchTrait;
use Cytonn\Investment\FundManager\FundManagerScope;
use Cytonn\Investment\FundManager\FundManagerScopingTrait;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalRejection;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStage;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalStep;
use Cytonn\Models\Portfolio\Approvals\PortfolioTransactionApprovalType;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 09/11/2015
 * Time: 10:58 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class PortfolioTransactionApproval extends BaseModel
{
    use SoftDeletes, EventDispatchTrait, EventGenerator, SearchableTrait, FundManagerScopingTrait, PresentableTrait;


    /**
     * @var string
     */
    public $table = 'portfolio_transaction_approval';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['approved_on'];

    protected $presenter = PortfolioTransactionApprovalPresenter::class;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'portfolio_transaction_approval.transaction_type' => 10,
            'portfolio_investors.name' => 10
        ],
        'joins' => [
            'portfolio_investors' => ['portfolio_transaction_approval.institution_id', 'portfolio_investors.id'],
        ],
    ];

    /**
     * @return $this
     */
    public function approve($user = null)
    {
        $this->approved = true;
        $this->approved_by = $user ? $user->id : Auth::id();
        $this->approved_on = \Carbon\Carbon::now();
        $this->save();

        $this->raise(new \Cytonn\Portfolio\Events\PortfolioTransactionApproved($this));
        $this->dispatchEventsFor($this);

        return $this;
    }

    /**
     * Add a new approval request
     *
     * @param  array $data
     * @return static
     * @throws \Cytonn\Exceptions\AuthorizationDeniedException
     */
    public static function add($data)
    {
        (new \Cytonn\Authorization\Authorizer())->checkAuthority('post_transactions');

        $approval = new static();
        $approval->institution_id = $data['institution_id'];
        $approval->transaction_type = $data['transaction_type'];
        $approval->fund_manager_id = isset($data['fund_manager_id'])
            ? $data['fund_manager_id']
            : \App::make(FundManagerScope::class)->getSelectedFundManagerId();
        $approval->payload = $data['payload'];
        $approval->sent_by = array_key_exists('sent_by', $data) ? $data['sent_by'] : Auth::id();
        $approval->save();

        return $approval;
    }

    public function handler()
    {
        $className = ucfirst(camel_case($this->transaction_type));

        $namespace = 'Cytonn\Portfolio\Approvals\Handlers';

        $h = $namespace . '\\' . $className;

        return new $h($this);
    }

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::created(
            function ($model) {
                $type = $model->transaction_type;

                if (!PortfolioTransactionApprovalType::where('slug', $type)->exists()) {
                    PortfolioTransactionApprovalType::create([
                        'name' => ucfirst(str_replace('_', ' ', $type)),
                        'slug' => $type
                    ]);
                }

                $firstStage = $model->type()->allStages()->first();
                $model->update(['awaiting_stage_id' => $firstStage->id]);

                $model->raise(new \Cytonn\Investment\Events\PortfolioTransactionApprovalRequested($model));
                $model->dispatchEventsFor($model);
            }
        );

        static::deleted(
            function ($model) {
                $model->raise(new \Cytonn\Portfolio\Events\PortfolioTransactionApproved($model));
                $model->dispatchEventsFor($model);
            }
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function institution()
    {
        return $this->belongsTo(PortfolioInvestor::class, 'institution_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function exemption()
    {
        return $this->hasOne(Exemption::class, 'portfolio_transaction_approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fundManager()
    {
        return $this->belongsTo(FundManager::class, 'fund_manager_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sent_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function type()
    {
        return PortfolioTransactionApprovalType::where('slug', $this->transaction_type)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function awaitingStage()
    {
        return $this->belongsTo(PortfolioTransactionApprovalStage::class, 'awaiting_stage_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rejections()
    {
        return $this->hasMany(PortfolioTransactionApprovalRejection::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function steps()
    {
        return $this->hasMany(PortfolioTransactionApprovalStep::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function suspenseTransaction()
    {
        return $this->hasOne(SuspenseTransaction::class, 'approval_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matchedSuspenseTransactions()
    {
        return $this->hasMany(SuspenseTransaction::class, 'approval_id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeRejected($query)
    {
        return $query->whereHas(
            'rejections',
            function ($r) {
                $r->whereNull('resolved_by');
            }
        );
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotRejected($query)
    {
        return $query->whereDoesntHave(
            'rejections',
            function ($r) {
                $r->whereNull('resolved_by');
            }
        );
    }

    public function scopeNotApproved($query)
    {
        return $query->where(
            function ($q) {
                $q->where('approved', 0)->orWhere('approved', null);
            }
        );
    }

    public function url()
    {
        return "/dashboard/portfolio/approve/$this->id";
    }
}
