<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

class ClientPaymentAllowedMinimum extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_payments_allowed_minimum';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'product_id',
        'project_id',
        'entity_id',
        'amount',
        'date',
        'schedule_id',
        'approval_id'
    ];

    /*
     * Relationship between a project and a client payment allowed minimum
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /*
     * Relationship between a client and a client payment allowed minimum
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /*
     * Relationship between a product and a client payment allowed minimum
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /*
     * Relationship between a share entity and a client payment allowed minimum
     */
    public function shareEntity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }
}
