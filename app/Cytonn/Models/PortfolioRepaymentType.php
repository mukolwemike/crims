<?php

namespace App\Cytonn\Models;

/**
 * Date: 18/10/2016
 * Time: 12:58
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class PortfolioRepaymentType extends BaseModel
{
    protected $table = 'portfolio_repayment_types';
}
