<?php

namespace App\Cytonn\Models;

use Cytonn\Core\Storage\StorageInterface;

class DocumentType extends BaseModel
{
    protected $table = 'document_types';

    protected $guarded = ['id'];

    protected $fileProvider;

    /**
     * DocumentType constructor.
     */
    public function __construct(array $attrs = [])
    {
        parent::__construct($attrs);

        $this->fileProvider();
    }

    public function path()
    {
        return $this->fileProvider()->getStorageLocation().'/'.$this->folder();
    }

    public function folder()
    {
        if (!is_null($folder = $this->folder)) {
            return $folder;
        }

        return $this->slug;
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'type_id');
    }

    private function fileProvider()
    {
        if ($this->fileProvider) {
            return $this->fileProvider;
        }

        return $this->fileProvider = \App::make(StorageInterface::class);
    }
}
