<?php

namespace App\Cytonn\Models;

/**
 * Date: 05/04/2016
 * Time: 3:19 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ActiveStrategyTargetPrice extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'active_strategy_target_price';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function security()
    {
        return $this->belongsTo(ActiveStrategySecurity::class, 'security_id');
    }
}
