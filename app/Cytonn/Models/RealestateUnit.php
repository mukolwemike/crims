<?php

namespace App\Cytonn\Models;

use Cytonn\Models\RealEstateUnitGroup;
use Cytonn\Realestate\RealestateUnits\RealEstateUnitPresenter;
use Laracasts\Presenter\PresentableTrait;

/**
 * Date: 23/05/2016
 * Time: 5:34 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealestateUnit extends BaseModel
{
    use \Nicolaslopezj\Searchable\SearchableTrait;

    use PresentableTrait;

    /*
     * Specify the presenter
     */
    protected $presenter = RealEstateUnitPresenter::class;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'realestate_units.number' => 10,
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'realestate_unit_sizes.name' => 5
        ],
        'joins' => [
            'unit_holdings' => ['realestate_units.id', 'unit_holdings.unit_id'],
            'clients' => ['unit_holdings.client_id', 'clients.id'],
            'contacts' => ['contacts.id', 'clients.contact_id'],
            'reservation_forms' => ['reservation_forms.holding_id', 'unit_holdings.id'],
            'realestate_unit_sizes' => ['realestate_units.size_id', 'realestate_unit_sizes.id']
        ],
    ];

    /**
     * @var string
     */
    protected $table = 'realestate_units';

    /**
     * @var array
     */
    protected $guarded = ['id', 'project_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(RealestateUnitType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function size()
    {
        return $this->belongsTo(RealestateUnitSize::class, 'size_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function activeHolding()
    {
        return $this->hasOne(UnitHolding::class, 'unit_id')->where('active', 1);
    }

    public function holdings()
    {
        return $this->hasMany(UnitHolding::class, 'unit_id');
    }

    public function group()
    {
        return $this->belongsTo(RealEstateUnitGroup::class, 'group_id');
    }

    public function realEstateType()
    {
        return $this->belongsTo(RealEstateType::class, 'real_estate_type_id');
    }

    public function projectFloor()
    {
        return $this->belongsTo(ProjectFloor::class, 'project_floor_id');
    }
}
