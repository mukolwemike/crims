<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionApprovalDocument extends BaseModel
{
    use SoftDeletes;

    public $table = 'client_transactional_approval_documents';

    protected $guarded = ['id'];

    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }
}
