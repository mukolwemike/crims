<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InterestRateUpdate
 */
class InterestRateUpdate extends BaseModel
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $table = 'interest_rates_updates';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rates()
    {
        return $this->hasMany(Rate::class, 'interest_rate_update_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
