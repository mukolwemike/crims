<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Client\Approvals\ClientInstructionApproval;
use Cytonn\Investment\ClientInstruction\ClientTopupFormRepository;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Cytonn\Uuids\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientTopupForm extends BaseModel
{
    use SearchableTrait, SoftDeletes, UuidTrait;

    public $table = 'client_topup_forms';

    protected $guarded = ['id'];

    public $repo;

    protected $searchable = [
        'columns' => [
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.corporate_trade_name' => 10,
            'contacts.email' => 10,
            'clients.client_code' => 10
        ],
        'joins' => [
            'clients' => ['client_topup_forms.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new ClientTopupFormRepository($this);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function latestInvestment()
    {
        return $this->client->investments()->latest()->first();
    }

    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investmentSchedule()
    {
        return $this->belongsTo(InvestmentPaymentSchedule::class, 'schedule_id');
    }

    public function investment()
    {
        return $this->hasOne(ClientInvestment::class, 'topup_form_id');
    }


    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function invested()
    {
        if ($this->investment()->exists()) {
            return true;
        }

        $invested = $this->client->investments()
                ->where('amount', $this->amount)
                ->where('created_at', '>=', $this->created_at)
                ->count() > 0;

        return $invested;
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function clientApprovals()
    {
        return $this->hasMany(ClientInstructionApproval::class, 'client_topup_form_id');
    }
}
