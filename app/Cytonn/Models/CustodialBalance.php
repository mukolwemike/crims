<?php

namespace App\Cytonn\Models;

/**
 * Class CustodialBalance
 */
class CustodialBalance extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'custodial_balances';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array
     * @return null
     */
    public function save(array $options = [])
    {
        return null;
    }

    /**
     * @param $currency_id
     * @param $date
     * @return int|null
     */
    public static function balanceForCurrencyAtDate($currency_id, $date)
    {
        $accounts = CustodialAccount::where('currency_id', $currency_id)->get();

        $bal = null;

        foreach ($accounts as $account) {
            $balance = static::where('custodial_accounts_id', $account->id)
                ->where('date', $date)->latest()->first();

            if (!is_null($balance)) {
                !is_null($bal) ?: $bal = 0;
                $bal += $balance;
            }
        }

        return $bal; // returns null when balance is not found
    }
}
