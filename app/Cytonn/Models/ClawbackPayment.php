<?php

namespace App\Cytonn\Models;

use App\Cytonn\BaseModel;

class ClawbackPayment extends BaseModel
{
    protected $table = 'commission_clawback_payments';
}
