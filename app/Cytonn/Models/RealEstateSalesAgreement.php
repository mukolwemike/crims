<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Cytonn\Handlers\EventDispatchTrait;
use Cytonn\Realestate\Sale\SalesAgreement;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 08/06/2016
 * Time: 4:08 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateSalesAgreement extends BaseModel
{
    use EventGenerator, EventDispatchTrait, SearchableTrait, PresentableTrait;


    /**
     * @var string
     */
    protected $table = 'realestate_sales_agreements';

    /*
     * Get the presenter
     */
    protected $presenter = 'Cytonn\Presenters\RealEstateSalesAgreementPresenter';

    protected $searchable = [
        'columns' => [
            'realestate_units.number' => 10,
            'realestate_unit_sizes.name' => 10,
            'realestate_unit_types.name' => 10,
            'projects.name' => 10,
            'projects.vendor' => 10,
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.lastname' => 10,
            'contacts.middlename' => 10,
        ],
        'joins' => [
            'unit_holdings' => ['realestate_sales_agreements.holding_id', 'unit_holdings.id'],
            'realestate_units' => ['unit_holdings.unit_id', 'realestate_units.id'],
            'projects' => ['realestate_units.project_id', 'projects.id'],
            'realestate_unit_sizes' => ['realestate_units.size_id', 'realestate_unit_sizes.id'],
            'realestate_unit_types' => ['realestate_units.type_id', 'realestate_unit_types.id'],
            'clients' => ['unit_holdings.client_id', 'clients.id'],
            'contacts' => ['contacts.id', 'clients.contact_id'],
        ],
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $dates = ['date_signed', 'date_received'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        $config = \config('filesystems');

        $path = 'files';

        if ($config['default'] == 's3') {
            $path = 'private';
        }

        return $path . '/sales_agreements';
    }

    /**
     * @param $holding
     * @param null $document
     * @param $date
     * @return static
     */
    public static function make($holding, $date = null, $document = null)
    {
        $agreement = new static();

        is_null($document) ?: $agreement->document_id = $document->id;
        $agreement->date = $date;
        $agreement->holding_id = $holding->id;
        $agreement->save();

        return $agreement;
    }

    /**
     * @param $file_contents
     * @param $file_ext
     * @param $date_sent_to_client
     * @param $date_received_from_client
     * @param $date_sent_to_lawyer
     * @return bool
     * @throws \Exception
     */
    public function upload(
        $file_contents,
        $file_ext,
        $date_sent_to_client,
        $date_received_from_client,
        $date_sent_to_lawyer
    ) {
        $document = (new SalesAgreement($this))->upload($file_contents, $file_ext);

        $this->document_id = $document->id;
        $this->uploaded_on = \Carbon\Carbon::now();
        $this->date_sent_to_client = $date_sent_to_client;
        $this->date_received_from_client = $date_received_from_client;
        $this->date_sent_to_lawyer = $date_sent_to_lawyer;

        return $this->save();
    }

    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    /**
     * json_encode payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getHistoryAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Unserialise value after retrieval
     *
     * @param $value
     */
    public function setHistoryAttribute($value)
    {
        $this->attributes['history'] = json_encode($value);
    }

    /**
     * @param Document $document
     */
    public function addHistory(Document $document)
    {
        $history = is_null($this->history) ? [] : $this->history;
        $history[] = $document->id;
        $this->history = $history;
        $this->save();
    }

    public function scopeForProject($query, Project $project, $active = true, Carbon $date = null)
    {
        return $query->whereHas('holding', function ($q) use ($project, $active, $date) {
            $q->whereHas('unit', function ($unit) use ($project, $active, $date) {
                $unit->where('project_id', $project->id);
            });

            if ($active) {
                $date ? $q->active() : $q->activeAtDate($date);
            }
        });
    }
}
