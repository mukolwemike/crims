<?php

namespace App\Cytonn\Models;

/**
 * Date: 09/12/2015
 * Time: 4:25 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class CustodialWithdrawType extends BaseModel
{
    public $table = 'custodial_withdraw_type';

    protected $guarded = ['id'];
}
