<?php

namespace App\Cytonn\Models;

class RealEstateCommissionPayment extends BaseModel
{
    protected $table = 'realestate_commission_payments';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }
}
