<?php

namespace App\Cytonn\Models;

use App\Cytonn\BaseModel;

class SalesAgreement extends BaseModel
{
    protected $table = 'realestate_sales_agreements';
    
    public function holding()
    {
        return $this->belongsTo(UnitHolding::class);
    }
}
