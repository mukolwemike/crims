<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;

/**
 * Date: 29/07/2016
 * Time: 12:42 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateRefund extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'realestate_refunds';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $dates = ['date'];

    /**
     * @param UnitHolding $holding
     * @param $amount
     * @param Carbon $date
     * @param $narration
     * @return static
     */
    public static function make(UnitHolding $holding, $amount, Carbon $date, $narration)
    {
        $refund = new static();
        $refund->holding_id = $holding->id;
        $refund->amount = $amount;
        $refund->date = $date;
        $refund->narration = $narration;
        $refund->save();

        return $refund;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    public function clientPayment()
    {
        return $this->belongsTo(ClientPayment::class, 'client_payment_id');
    }

    public function scopeBefore($query, $date)
    {
        return $query->where('date', '<=', $date);
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }
}
