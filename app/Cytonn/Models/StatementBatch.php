<?php

namespace App\Cytonn\Models;

/**
 * Date: 27/10/2015
 * Time: 12:42 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


class StatementBatch extends BaseModel
{
    public $table = 'statement_batches';

    protected $guarded = ['id'];

    public function jobs()
    {
        return $this->hasMany('App\StatementJob', 'batch_id');
    }
}
