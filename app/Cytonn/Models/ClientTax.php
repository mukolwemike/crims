<?php

namespace App\Cytonn\Models;

/**
 * Date: 13/11/2015
 * Time: 6:14 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */


class ClientTax extends BaseModel
{
    use \Nicolaslopezj\Searchable\SearchableTrait;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10
        ],
        'joins' => [
            'client_investments' => ['client_tax.investment_id', 'client_investments.id'],
            'clients' => ['client_investments.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ],
    ];

    /**
     * @var string
     */
    public $table = 'client_tax';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo('App\ClientInvestment', 'investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
