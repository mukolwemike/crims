<?php

namespace App\Cytonn\Models;

use Cytonn\Reporting\RealestateStatementCampaignRepository;
use Cytonn\Reporting\SharesStatementCampaignRepository;

class SharesStatementCampaign extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    const TYPE = 'Shares Statements';

    /**
     * @var RealestateStatementCampaignRepository
     */
    public $repo;


    /**
     * @var string
     */
    public $table = 'shares_statement_campaigns';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = ['send_date', 'closed_on'];

    /**
     * SharesStatementCampaign constructor.
     */
    public function __construct(array $data = [])
    {
        $this->repo = new SharesStatementCampaignRepository($this);

        parent::__construct($data);
    }

    /**
     * Get each individual in campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(SharesCampaignStatementItem::class, 'campaign_id');
    }

    /**
     * The campaign type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(StatementCampaignType::class, 'type_id');
    }

    /**
     * Close the campaign so that statements are not added anymore
     *
     * @return bool
     */
    public function close()
    {
        $this->closed = true;
        return $this->save();
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeClosed($query, $condition = true)
    {
        if ($condition) {
            return $query->where('closed', 1);
        }

        return $query->where(function ($q) {
            $q->whereNull('closed')->orWhere('closed', '!=', 1);
        });
    }
}
