<?php

namespace App\Cytonn\Models;

/**
 * Date: 18/10/2015
 * Time: 11:54 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
use Cytonn\Models\Shares\ShareCommissionRate;

class CommissionRecipientType extends BaseModel
{
    public $table = 'commission_recepient_type';

    protected $guarded = ['id'];

    public function realestateCommissionRates()
    {
        return $this->hasMany(RealestateCommissionRate::class, 'recipient_type_id');
    }

    public function rate()
    {
        return $this->belongsTo(CommissionRate::class, 'rate_id');
    }

    /*
     * Relation between commission rates and commission recipient type
     */
    public function commissionRates()
    {
        return $this->hasMany(CommissionRate::class);
    }

    /*
     * Relation between share commission rates and commission recipient type
     */
    public function shareCommissionRates()
    {
        return $this->hasMany(ShareCommissionRate::class);
    }

    /*
     * Relation between commission rates and commission recipient type
     */
    public function realestateOverridePercentages()
    {
        return $this->hasMany(RealEstateOverridePercentage::class);
    }
}
