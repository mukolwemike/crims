<?php

namespace App\Cytonn\Models;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use Carbon\Carbon;

class LoyaltyValues extends BaseModel
{
    public $table = 'loyalty_points_rates';

    protected $guarded = ['id'];
}
