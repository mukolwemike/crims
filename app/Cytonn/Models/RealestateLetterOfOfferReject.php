<?php

namespace App\Cytonn\Models;

use Cytonn\Handlers\EventDispatchTrait;
use Cytonn\Realestate\Events\LOOHasBeenRejected;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laracasts\Commander\Events\EventGenerator;

class RealestateLetterOfOfferReject extends BaseModel
{
    use SoftDeletes, EventDispatchTrait, EventGenerator;
    /**
     * @var string
     */
    protected $table = 'realestate_letter_of_offer_rejects';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param RealestateLetterOfOffer $loo
     * @param $reason
     * @return bool
     */
    public function reject(RealestateLetterOfOffer $loo, $reason)
    {
        $this->loo_id = $loo->id;
        $this->pm_id = Auth::user()->id;
        $this->reason = $reason;

        // TODO
        $this->raise(new LOOHasBeenRejected($this));
        $this->dispatchEventsFor($this);

        return $this->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function loo()
    {
        return $this->belongsTo(RealestateLetterOfOffer::class, 'loo_id');
    }
}
