<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Investment;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Investment\InvestmentPaymentSchedules\InvestmentPaymentScheduleRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class InvestmentPaymentSchedule extends BaseModel
{
    use SoftDeletes;
    use SearchableTrait;

    public $repo;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'investment_payment_schedules';

    protected $searchable = [
        'columns' => [
            'investment_payment_schedules.date' => 5,
            'investment_payment_schedules.description' => 5,
            'investment_payment_schedules.amount' => 5,
        ]
    ];


    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'investment_id',
        'parent_investment_id',
        'amount',
        'date',
        'paid',
        'date_paid',
        'interest_rate',
        'description',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new InvestmentPaymentScheduleRepository($this);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentInvestment()
    {
        return $this->belongsTo(ClientInvestment::class, 'parent_investment_id');
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopePaid($query, $condition = true)
    {
        if ($condition) {
            return $query->where('paid', 1);
        }

        return $query->where('paid', 0);
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopePaidBefore($query, Carbon $date, $condition = true)
    {
        if ($condition) {
            return $query->where(function ($q) use ($date) {
                $q->whereNotNull('date_paid')->where('date_paid', '<=', $date);
            });
        }

        return $query->where(function ($q) use ($date) {
            $q->whereNull('date_paid')->orWhere('date_paid', '>', $date);
        });
    }

    /**
     * @param $query
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }
}
