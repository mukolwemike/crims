<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Investment;

use App\Cytonn\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvanceInvestmentCommissionRepayment extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'advance_investment_commission_repayments';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'advance_investment_commission_id',
        'date',
        'amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advanceCommission()
    {
        return $this->belongsTo(AdvanceInvestmentCommission::class, 'advance_investment_commission_id');
    }
}
