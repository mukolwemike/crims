<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Investment;

use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Investment\AdvanceCommission\AdvanceInvestmentCommissionPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class AdvanceInvestmentCommission extends BaseModel
{
    use SoftDeletes;
    use PresentableTrait;

    /*
     * Get the presenter
     */
    protected $presenter = AdvanceInvestmentCommissionPresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'advance_investment_commissions';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recipient_id',
        'date',
        'amount',
        'paid_on',
        'advance_commission_type_id',
        'description',
        'repayable',
        'currency_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AdvanceCommissionType::class, 'advance_commission_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionRepayments()
    {
        return $this->hasMany(AdvanceInvestmentCommissionRepayment::class, 'advance_investment_commission_id');
    }

    /**
     * @param $query
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    /**
     * @param $query
     * @param CommissionRecepient $recipient
     * @return mixed
     */
    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->where('recipient_id', $recipient->id);
    }

    /**
     * @param $query
     * @param Currency $currency
     * @return mixed
     */
    public function scopeForCurrency($query, Currency $currency)
    {
        return $query->where('currency_id', $currency->id);
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeUnpaidAsAt($query, Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        return $query->where(function ($q) use ($date) {
            $q->whereNull('paid_on')->orWhere('paid_on', '>=', $date);
        });
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeRepayable($query, $condition = true)
    {
        if ($condition) {
            return $query->where('repayable', 1);
        }

        return $query->where('repayable', '!=', 1);
    }
}
