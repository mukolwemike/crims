<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models\Investment;

use App\Cytonn\Investment\AdditionalCommission\AdditionalCommissionPresenter;
use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class AdditionalCommission extends BaseModel
{
    use SoftDeletes;
    use PresentableTrait;

    /*
     * Get the presenter
     */
    protected $presenter = AdditionalCommissionPresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'additional_commissions';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recipient_id',
        'date',
        'amount',
        'paid_on',
        'type_id',
        'description',
        'repayable',
        'holding_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'recipient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AdditionalCommissionType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionRepayments()
    {
        return $this->hasMany(AdditionalCommissionRepayment::class, 'commission_id');
    }

    /**
     * @param $query
     * @param Carbon $start
     * @param Carbon $end
     * @return mixed
     */
    public function scopeBetween($query, Carbon $start, Carbon $end)
    {
        return $query->where('date', '>=', $start)->where('date', '<=', $end);
    }

    /**
     * @param $query
     * @param CommissionRecepient $recipient
     * @return mixed
     */
    public function scopeForRecipient($query, CommissionRecepient $recipient)
    {
        return $query->where('recipient_id', $recipient->id);
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeUnpaidAsAt($query, Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        return $query->where(function ($q) use ($date) {
            $q->whereNull('paid_on')->orWhere('paid_on', '>=', $date);
        });
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeRepayable($query, $condition = true)
    {
        if ($condition) {
            return $query->where('repayable', 1);
        }

        return $query->where('repayable', '!=', 1);
    }
}
