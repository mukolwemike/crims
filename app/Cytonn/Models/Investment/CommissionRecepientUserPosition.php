<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Investment;

use App\Cytonn\BaseModel;
use App\Cytonn\Investment\CommissionRecepientUserPositions\CommissionRecepientUserPositionPresenter;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\DepartmentUnit;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class CommissionRecepientUserPosition extends BaseModel
{
    /*
     * Get the soft deletes trait
     */
    use SoftDeletes;

    /*
     * Get the presentable trait
     */
    use PresentableTrait;

    protected $presenter = CommissionRecepientUserPositionPresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'commission_recipient_user_positions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commission_recipient_id',
        'position_id',
        'department_unit_id',
        'start_date',
        'end_date'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = array('id', 'created_at', 'updated_at');

    /*
     * Relationship between a commissionRecipient and a commissionRecipient position
     */
    public function commissionRecipient()
    {
        return $this->belongsTo(CommissionRecepient::class, 'commission_recipient_Id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function departmentUnit()
    {
        return $this->belongsTo(DepartmentUnit::class, 'department_unit_id');
    }
}
