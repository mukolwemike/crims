<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Models\Investment;

use App\Cytonn\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeriodicCommissionRate extends BaseModel
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'periodic_commission_rates';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rate_update_id',
        'tenor',
        'rate'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rateUpdate()
    {
        return $this->belongsTo(PeriodicCommissionRateUpdate::class, 'rate_update_id');
    }

    public function getName()
    {
    }
}
