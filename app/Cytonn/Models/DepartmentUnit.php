<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Models;

use Cytonn\DepartmentUnits\DepartmentUnitPresenter;
use Laracasts\Presenter\PresentableTrait;

class DepartmentUnit extends BaseModel
{
    use PresentableTrait;

    protected $presenter = DepartmentUnitPresenter::class;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'department_units';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'head_id',
        'department_id',
        'department_branch_id'
    ];

    /*
     * Relationship between a department and department unit
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /*
     * Relationship between a department branch and department unit
     */
    public function departmentBranch()
    {
        return $this->belongsTo(DepartmentBranch::class);
    }

    /*
     * Relationship between a commission recipients and department unit
     */
    public function commissionRecipients()
    {
        return $this->hasMany(CommissionRecepient::class);
    }

    /*
     * Relationship between a commission recipients and department unit
     */
    public function head()
    {
        return $this->belongsTo(CommissionRecepient::class, 'head_id');
    }
}
