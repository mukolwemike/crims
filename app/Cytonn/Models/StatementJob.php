<?php

namespace App\Cytonn\Models;

/**
 * Date: 27/10/2015
 * Time: 12:40 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class StatementJob extends BaseModel
{
    public $table = 'statement_job';

    protected $guarded = ['id'];
}
