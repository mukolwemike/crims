<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 31/08/2018
 * Time: 09:28
 */

namespace App\Cytonn\Models;

class TermsAndCondition extends BaseModel
{
    protected $guarded = ['id'];

    protected $table = 'terms_and_conditions';

    public function scopeIsActive($query, $active = true)
    {
        if (!$active) {
            return $query->where('active', null)->orWhere('active', 0);
        }

        return $query->where('active', 1);
    }
}
