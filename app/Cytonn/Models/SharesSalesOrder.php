<?php

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class SharesSalesOrder
 */
class SharesSalesOrder extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    protected $table = 'shares_sales_orders';

    protected $guarded = ['id'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.lastname' => 10,
            'contacts.middlename' => 10,
        ],
        'joins' => [
            'share_holders' => ['shares_sales_orders.seller_id', 'share_holders.id'],
            'clients' => ['share_holders.client_id', 'clients.id'],
            'contacts' => ['contacts.id', 'clients.contact_id'],
        ],
    ];

    /**
     * SharesSalesOrder constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seller()
    {
        return $this->belongsTo(ShareHolder::class, 'seller_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function custodialAccount()
    {
        return $this->belongsTo(CustodialAccount::class, 'account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sharePurchases()
    {
        return $this->hasMany(SharePurchases::class, 'shares_sales_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function matchedPurchaseOrders()
    {
        return $this->belongsToMany(
            SharesPurchaseOrder::class,
            'share_order_matches',
            'sale_order_id',
            'purchase_order_id'
        )->withPivot('number')->withTimestamps();
    }

    /**
     * @param $order
     * @param bool $matched
     * @return mixed
     */
    public function scopeMatched($order, $matched = true)
    {
        if ($matched) {
            return $order->where('matched', 1);
        }

        return $order->where(function ($q) {
            return $q->where('matched', 0)->orWhereNull('matched');
        });
    }

    /**
     * @param $order
     * @param bool $cancelled
     * @return mixed
     */
    public function scopeCancelled($order, $cancelled = true)
    {
        if ($cancelled) {
            return $order->where('cancelled', 1);
        }

        return $order->where(function ($q) {
            return $q->where('cancelled', 0)->orWhereNull('cancelled');
        });
    }

    /**
     * @param $order
     * @return mixed
     */
    public function scopeForFundManager($order)
    {
        if (!$this->currentFundManager()) {
            return $order;
        }

        return $order->whereHas(
            'seller',
            function ($holder) {
                return $holder->whereHas(
                    'entity',
                    function ($entity) {
                        $entity->where('fund_manager_id', $this->currentFundManager()->id);
                    }
                );
            }
        );
    }

    public function numberSold()
    {
        return ShareSale::where('shares_sales_order_id', $this->id)->sum('number');
    }

    public function balance()
    {
        return $this->number - $this->numberSold();
    }

    public function pendingSharePurchases()
    {
        return $this->sharePurchases()->whereNull('client_payment_id')->get();
    }
}
