<?php

namespace App\Cytonn\Models;

use Illuminate\Support\Facades\Auth;

/**
 * Date: 12/11/2015
 * Time: 11:25 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class StatementSendRequest extends BaseModel
{
    public $table = 'statement_send_requests';

    protected $guarded = ['id'];

    public static function add($data)
    {
        $request = new static();
        $request->name = $data['name'];
        $request->sent_by = Auth::user()->id;
        $request->sent_on = \Carbon\Carbon::now();
        $request->save();
        return $request;
    }

    public function approve()
    {
        $this->approved_by = Auth::user()->id;
        $this->Approved_on = \Carbon\Carbon::now();
        $this->save();
        return $this;
    }

    public function useRequest()
    {
        $this->used = true;
        $this->used_by = Auth::user()->id;
        $this->used_on = \Carbon\Carbon::now();
        $this->save();
        return $this;
    }
}
