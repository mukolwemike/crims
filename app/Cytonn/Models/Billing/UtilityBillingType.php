<?php

namespace App\Cytonn\Models\Billing;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UtilityBillingType extends BaseModel
{
    use SoftDeletes;

    protected $table = 'utility_billing_type';

    protected $guarded = ['id'];
}
