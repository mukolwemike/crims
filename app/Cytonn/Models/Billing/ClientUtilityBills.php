<?php

namespace App\Cytonn\Models\Billing;

use App\Cytonn\Billing\BillingRepository;
use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class ClientUtilityBills extends BaseModel
{
    use SoftDeletes;
    use SearchableTrait;

    protected $table = 'client_utility_bills';

    protected $guarded = ['id'];

    protected $repo;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new BillingRepository();
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function utility()
    {
        return $this->belongsTo(UtilityBillingServices::class, 'bill_id');
    }
}
