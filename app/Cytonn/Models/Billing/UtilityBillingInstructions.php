<?php

namespace App\Cytonn\Models\Billing;

use App\Cytonn\Billing\BillingRepository;
use App\Cytonn\Models\BaseModel;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Http\Requests\Investments\Withdrawal;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class UtilityBillingInstructions extends BaseModel
{
    use SoftDeletes;

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
            'clients.id_or_passport' => 5,
            'client_joint_details.firstname' => 10,
            'client_joint_details.lastname' => 10,
            'client_joint_details.middlename' => 10,
            'client_joint_details.id_or_passport' => 8,
            'client_joint_details.email' => 10,
            'unit_funds.name' => 5,
            'unit_funds.short_name' => 5
        ],
        'joins' => [
            'clients' => ['utility_billing_instructions.client_id', 'clients.id'],
            'unit_funds' => ['utility_billing_instructions.unit_fund_id', 'unit_funds.id'],
            'contacts' => ['clients.contact_id', 'contacts.id'],
            'client_joint_details' => ['clients.id', 'client_joint_details.client_id']
        ],
        'groupBy' => 'utility_billing_instructions.id'
    ];

    protected $table = 'utility_billing_instructions';

    protected $guarded = ['id'];

    protected $repo;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new BillingRepository();
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function unitFund()
    {
        return $this->belongsTo(UnitFund::class, 'unit_fund_id');
    }

    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    public function utility()
    {
        return $this->belongsTo(UtilityBillingServices::class, 'bill_id');
    }

    public function utilityPayment()
    {
        return $this->belongsTo(ClientPayment::class, 'utility_payment_id');
    }

    public function user()
    {
        return $this->belongsTo(ClientUser::class, 'user_id');
    }

    public function approval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'approval_id');
    }

    public function reverseApproval()
    {
        return $this->belongsTo(ClientTransactionApproval::class, 'reversal_approval_id');
    }

    public function status()
    {
        return $this->belongsTo(UtilityBillingStatus::class, 'billing_status_id');
    }

    public function sale()
    {
        return $this->belongsTo(UnitFundSale::class, 'unit_fund_sale_id');
    }

    public function withdrawal()
    {
        return $this->belongsTo(Withdrawal::class, 'withdrawal_id');
    }

    public function type()
    {
        return $this->belongsTo(UtilityBillingType::class, 'instruction_type_id');
    }

    public function scopeStatus($query, $slug)
    {
        return $query->whereHas('status', function ($status) use ($slug) {
            $status->where('slug', $slug);
        });
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas('type', function ($type) use ($slug) {
            $type->where('slug', $slug);
        });
    }

    /**
     * @param $query
     * @param $start
     * @param $end
     * @return mixed
     */
    public function scopeBetweenDates($query, $start = null, $end = null)
    {
        if (is_null($start) and is_null($end)) {
            return $query;
        }

        if ($start) {
            $query = $query->where('date', '>=', $start);
        }
        if ($end) {
            $query = $query->where('date', '<=', $end);
        }

        return $query;
    }
}
