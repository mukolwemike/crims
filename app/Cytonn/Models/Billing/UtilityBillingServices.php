<?php

namespace App\Cytonn\Models\Billing;

use App\Cytonn\Billing\BillingRepository;
use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class UtilityBillingServices extends BaseModel
{
    use SoftDeletes;
    use SearchableTrait;

    protected $table = 'utility_billing_services';

    protected $guarded = ['id'];

    protected $repo;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new BillingRepository();
    }

    public function type()
    {
        return $this->belongsTo(UtilityBillingType::class, 'type_id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
