<?php

namespace App\Cytonn\Models\Billing;

use App\Cytonn\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UtilityBillingStatus extends BaseModel
{
    use SoftDeletes;

    protected $table = 'utility_billing_status';

    protected $guarded = ['id'];
}
