<?php

namespace App\Cytonn\Models;

/**
 * Date: 28/11/2016
 * Time: 12:16
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ProductType extends BaseModel
{
    public function products()
    {
        return $this->hasMany(Product::class, 'type_id');
    }
}
