<?php

namespace App\Cytonn\Models;

/**
 * Date: 08/12/2015
 * Time: 8:34 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

class NotificationRead extends Basemodel
{
    public $table = 'notification_reads';

    protected $guarded = ['id'];
}
