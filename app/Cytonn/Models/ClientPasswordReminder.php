<?php

namespace App\Cytonn\Models;

/**
 * Date: 13/05/2016
 * Time: 6:06 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientPasswordReminder extends BaseModel
{
    protected $table = 'client_password_reminders';

    protected $guarded = ['id'];
}
