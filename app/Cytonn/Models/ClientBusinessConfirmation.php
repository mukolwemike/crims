<?php

namespace App\Cytonn\Models;

/**
 * Date: 01/11/2015
 * Time: 1:28 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class ClientBusinessConfirmation extends BaseModel
{
    public $table = 'client_business_confirmation';

    protected $guarded = ['id'];

    public static function add($data, $sender = null)
    {
        $user = Auth()->user();
        $sender = (!is_null($user)) ? $user->id : $sender;

        $bc = new static();
        $bc->fill($data);
        $bc->sent_by = $sender;
        $bc->save();
        return $bc;
    }

    /**
     * Serialise payload before sending the db
     *
     * @param  $value
     * @return mixed
     */
    public function getPayloadAttribute($value)
    {
        return unserialize($value);
    }

    public function investment()
    {
        return $this->belongsTo(ClientInvestment::class, 'investment_id');
    }

    /**
     * Unserialise value after retriaval
     *
     * @param $value
     */
    public function setPayloadAttribute($value)
    {
        $this->attributes['payload'] = serialize($value);
    }

    /**
     * @param $confirmation
     * @return mixed
     */
    public function scopeForFundManager($confirmation)
    {
        if (!$this->currentFundManager()) {
            return $confirmation;
        }
        return $confirmation->whereHas(
            'investment',
            function ($investment) {
                $investment->whereHas(
                    'product',
                    function ($product) {
                        $product->where('fund_manager_id', $this->currentFundManager()->id);
                    }
                );
            }
        );
    }
}
