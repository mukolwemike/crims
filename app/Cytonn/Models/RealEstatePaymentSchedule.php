<?php

namespace App\Cytonn\Models;

use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Realestate\Payments\PaymentScheduleRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 06/06/2016
 * Time: 11:44 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstatePaymentSchedule extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string
     */
    protected $table = 'realestate_payments_schedules';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    public $repo;

    protected $dates = ['date', 'interest_date'];

    protected $searchable = [
        'columns' => [
            'clients.client_code' => 10,
            'contacts.firstname' => 10,
            'contacts.middlename' => 10,
            'contacts.lastname' => 10,
            'contacts.corporate_registered_name' => 10,
            'contacts.email' => 10,
            'clients.emails' => 5,
        ],
        'joins' => [
            'unit_holdings' => ['realestate_payments_schedules.unit_holding_id', 'unit_holdings.id'],
            'clients' => ['unit_holdings.client_id', 'clients.id'],
            'contacts' => ['clients.contact_id', 'contacts.id']
        ]
    ];

    /**
     * RealEstatePaymentSchedule constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->repo = new PaymentScheduleRepository($this);
    }


    /**
     * @param $type_slug
     * @param $holding
     * @param $amount
     * @param $description
     * @param $penalty_excempt | null
     * @return static
     * @throws ClientInvestmentException
     */
    public static function make($type_slug, $holding, $amount, $description)
    {
        if (strlen($type_slug) < 1) {
            throw new ClientInvestmentException('The payment type could not be found');
        }

        $schedule = new static();
        $schedule->payment_type_id = RealEstatePaymentType::where('slug', $type_slug)->first()->id;
        $schedule->unit_holding_id = $holding->id;
        $schedule->amount = $amount;
        $schedule->description = $description;
        $schedule->save();

        return $schedule;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(RealEstatePaymentType::class, 'payment_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'unit_holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(RealEstatePayment::class, 'schedule_id');
    }

    public function interestAccrued()
    {
        return $this->hasMany(RealestateInterestAccrued::class, 'schedule_id');
    }


    /**
     * @param $query
     * @param $query
     * @return mixed
     * /*
     * @method inPricing()
     * /*
     * @method inPricing()
     * @return mixed
     */
    public function scopeInPricing($query)
    {
        return $query->whereHas('type', function ($type) {
            $type->where('slug', '<>', 'reservation_fee');
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAccruesInterest($query)
    {
        return $query->whereHas('type', function ($q) {
            $q->where('accrues_interest', 1);
        });
    }

    /**
     * @param $query
     * @param bool $condition
     * @return mixed
     */
    public function scopeIsServiceCharge($query, $condition = true)
    {
        if ($condition) {
            return $query->whereHas('type', function ($type) {
                $type->where('slug', '==', 'service_charge');
            });
        }

        return $query->whereHas('type', function ($type) {
            $type->where('slug', '<>', 'service_charge');
        });
    }

    /**
     * @param $query
     * @param $date
     * @return mixed
     */
    public function scopeBefore($query, $date)
    {
        return $query->where(
            function ($q) use ($date) {
                if ($date) {
                    $q->where('date', '<=', $date);
                }
                $q->orWhereNull('date');
            }
        );
    }

    public function scopeAfter($query, $date = null)
    {
        if ($date) {
            return $query;
        }

        return $query->where(
            function ($q) use ($date) {
                $q->where('date', '>=', $date)
                    ->orWhereNull('date');
            }
        );
    }

    public function scopeBetween($query, \Carbon\Carbon $start, \Carbon\Carbon $end)
    {
        return $query->where(
            function ($q) use ($start, $end) {
                $q->where('date', '<=', $end)->where('date', '>=', $start);
            }
        );
    }

    /**
     * @param $query
     * @param array $project_ids
     * @return mixed
     */
    public function scopeForProjects($query, array $project_ids)
    {
        return $query->wherehas(
            'holding',
            function ($holding) use ($project_ids) {
                $holding->whereHas(
                    'unit',
                    function ($unit) use ($project_ids) {
                        $unit->whereIn('project_id', $project_ids);
                    }
                );
            }
        );
    }

    /**
     * @param $query
     * @param $date
     * @return mixed
     */
    public function scopeInMonth($query, $date)
    {
        $date = new Carbon($date);

        return $query->where('date', '>=', $date->copy()->startOfMonth())
            ->where('date', '<=', $date->copy()->endOfMonth());
    }

    public function scopePenalty($query, $state = true)
    {
        return $query->where('penalty', $state);
    }

    /**
     * Check whether schedule has been paid
     *
     * @return bool
     */
    public function paid()
    {
        $diff = $this->overduePayments();

        if ($diff <= 0 || abs($diff) <= 1000) {
            return true;
        }

        return false;
    }

    /**
     * Get the amount remaining for holding as per the current schedule
     *
     * @return number
     */
    public function overduePayments()
    {
        $holding = $this->holding;

        $date = new Carbon($this->date);

        $schedules = $holding->paymentSchedules()
            ->inPricing()
            ->before($date)
            ->sum('amount');

        $payments = $holding->payments()->sum('amount');

        return $schedules - $payments;
    }

    public function amountNotPaid()
    {
        $diff = $this->overduePayments();

        if ($diff <= 0) {
            return 0;
        }

        if ($diff > $this->amount) {
            return $this->amount;
        }

        return $diff;
    }

    /**
     * Get the previous payment
     *
     * @return static | null
     */
    public function previous()
    {
        return $this->holding->paymentSchedules()
            ->where('date', '<', $this->date)
            ->latest('date')
            ->first();
    }

    /**
     * Get the next payment
     *
     * @return static | null
     */
    public function next()
    {
        return $this->holding->paymentSchedules()
            ->where('date', '>', $this->date)
            ->oldest('date')
            ->first();
    }

    public function scopeIsLast($query, $condition = true)
    {
        if ($condition) {
            return $query->where('last_schedule', 1);
        }

        return $query->where('last_schedule', '!=', 1);
    }

//    /**
//     * @return bool
//     */
//    public function isLastSchedule($lastSchedule = null)
//    {
//        if (is_null($lastSchedule)) {
//
//        }
//
//        return $this->id == $lastSchedule->id;
//    }

    /**
     * @return mixed
     */
    public function lastSchedule()
    {
        return $this->holding->paymentSchedules()
            ->isLast()
            ->first();
    }

    /**
     * Get previous schedules that have not been paid
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function previousUnpaidSchedules()
    {
        return static::where('unit_holding_id', $this->unit_holding_id)
            ->inPricing()
            ->before($this->date)
            ->get()
            ->filter(
                function (RealEstatePaymentSchedule $schedule) {
                    return !$schedule->paid();
                }
            );
    }

    public function scopeOfNotType($query, $type = null)
    {
        return $query->whereHas('type', function ($type) {
            $type->where('slug', '!=', 'reservation_fee');
        });
    }
}
