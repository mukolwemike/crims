<?php

namespace App\Cytonn\Models;

class CustodialTransfer extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'custodial_transfers';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(CustodialTransaction::class, 'out_transaction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(CustodialTransaction::class, 'in_transaction_id');
    }
}
