<?php

namespace App\Cytonn\Models;

/**
 * Date: 02/06/2016
 * Time: 10:41 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ReservationForm extends BaseModel
{
    use \Laracasts\Commander\Events\EventGenerator;

    /**
     * @var string
     */
    protected $table = 'reservation_forms';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return mixed
     */
    public function client()
    {
        return $this->holding->client();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holding()
    {
        return $this->belongsTo(UnitHolding::class, 'holding_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nationality()
    {
        return $this->belongsTo(Country::class, 'nationality_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jointClients()
    {
        return $this->hasMany(ClientJointDetail::class, 'reservation_form_id');
    }

    public function date()
    {
        if ($this->reservation_date) {
            return $this->reservation_date;
        }

        return $this->updated_at;
    }
}
