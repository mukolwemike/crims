<?php

namespace App\Cytonn\Models;

class CorporateInvestorType extends BaseModel
{
    protected $table = 'corporate_investor_type';

    protected $guarded = ['id'];
}
