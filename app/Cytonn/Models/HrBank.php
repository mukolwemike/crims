<?php
/**
 * Created by PhpStorm.
 * User: rkaranja
 * Date: 12/11/2018
 * Time: 14:47
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\Model;

class HrBank extends Model
{
    protected $table = 'hr_banks';

    protected $guarded = ['id'];
}
