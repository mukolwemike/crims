<?php

namespace App\Cytonn\Models;

use Cytonn\Portfolio\ActiveStrategy\ActiveStrategySecurityRepository;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Date: 04/04/2016
 * Time: 8:17 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ActiveStrategySecurity extends BaseModel
{
    use SearchableTrait;

    /**
     * @var string
     */
    public $table = 'active_strategy_security';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var ActiveStrategySecurityRepository
     */
    public $repo;

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'active_strategy_security.name' => 10
        ],
        'joins' => [

        ],
    ];

    /**
     * ActiveStrategySecurity constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->repo = new ActiveStrategySecurityRepository($this);

        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function holdings()
    {
        return $this->hasMany(ActiveStrategyHolding::class, 'security_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany(ActiveStrategyShareSale::class, 'security_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function priceTrail()
    {
        return $this->hasMany(ActiveStrategyPriceTrail::class, 'security_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function targetPrices()
    {
        return $this->hasMany(ActiveStrategyTargetPrice::class, 'security_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dividends()
    {
        return $this->hasMany(ActiveStrategyDividend::class, 'security_id');
    }
}
