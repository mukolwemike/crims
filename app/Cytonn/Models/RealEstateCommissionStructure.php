<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace App\Cytonn\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class RealEstateCommissionStructure extends BaseModel
{
    /*
     * Get the soft deletes trait
     */
    use SoftDeletes;

    /*
     * Table for the model
     */
    protected $table = 'realestate_commission_structures';

    /**
     * Fields that should not be mass assigned
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'effective_date',
    ];

    /*
     * Relationship between real estate commission structure and  real estate commission percentages
     */
    public function percentages()
    {
        return $this->hasMany(RealEstateCommissionPercentage::class, 'realestate_commission_structure_id');
    }
}
