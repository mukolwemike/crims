<?php

namespace App\Cytonn\Models;

/**
 * Date: 16/06/2016
 * Time: 3:00 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class RealEstateSizeNumber extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'realestate_size_numbers';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function size()
    {
        return $this->belongsTo(RealestateUnitSize::class, 'size_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
