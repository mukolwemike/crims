<?php

namespace App\Cytonn\Models;

/**
 * Date: 20/11/2015
 * Time: 4:13 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */
class CustodialTransactionType extends BaseModel
{
    public $table = 'custodial_transaction_types';

    protected $guarded = ['id'];
}
