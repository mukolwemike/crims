<?php

namespace App\Cytonn\Models;

/**
 * Date: 02/03/2017
 * Time: 16:40
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */
class ClientPaymentType extends BaseModel
{
    protected $fillable = ['name', 'slug'];
}
