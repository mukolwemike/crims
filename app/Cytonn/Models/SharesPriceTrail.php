<?php

namespace App\Cytonn\Models;

use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class SharesPriceTrail
 */
class SharesPriceTrail extends BaseModel
{
    use SoftDeletes, SearchableTrait;

    /**
     * @var string
     */
    protected $table = 'shares_price_trail';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo(SharesEntity::class, 'entity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(SharesCategory::class, 'category_id');
    }

    /**
     * @param $entity_id
     * @param $date
     * @throws ClientInvestmentException
     * @return mixed
     */
    public static function getPrice($entity_id, $date)
    {
        $date = \Carbon\Carbon::parse($date);
        $pricing = self::where('entity_id', $entity_id)
            ->where('date', '<=', $date)
            ->latest('date')
            ->first();

        if (is_null($pricing)) {
            throw new ClientInvestmentException(
                'The price could not be determined, check that the price trail is up to date'
            );
        }

        return $pricing->price;
    }

    public function scopeBefore($query, \Carbon\Carbon $date)
    {
        return $query->where('date', '<=', $date);
    }
}
