<?php
/**
 * Date: 9/14/15
 * Time: 8:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Logger;

use App\Console\Commands\System\UUIDGenerator;
use App\Cytonn\Models\AccessLog;
use App\Cytonn\Models\ActivityLog;
use Cytonn\Models\CrimsAdminLoginTrail;
use Cytonn\Models\CrimsAdminPageView;
use Cytonn\Models\CrimsClientLoginTrail;
use Cytonn\Models\CrimsClientPageView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as Monolog;
use Webpatser\Uuid\Uuid;

class Logger
{
    protected $skipWhenTesting = true;

    protected $excempted = [
        CrimsAdminLoginTrail::class,
        CrimsClientLoginTrail::class,
        CrimsAdminPageView::class,
        CrimsClientPageView::class
    ];


    public function log($user = null, $model = null, $action = null, $message = null)
    {
        $env = app()->environment();

        if (in_array($env, ['testing', 'local']) && $this->skipWhenTesting) {
            return;
        }

        if (in_array(get_class($model), $this->excempted)) {
            return;
        }

        try {
            is_null($user) ? $user_id = null : $user_id = $user->id;

            //access log file
            $log = new Monolog('Access');
            $log->pushHandler(new StreamHandler(storage_path() . '/logs/access.log', Monolog::INFO));

            $msg = 'User: ' . $user_id . ' ' . $action . ' ' . get_class($model) . ' ID: '
                . $model->id . ' on ' . date('Y-m-d H:i:s', time());

            $log->addInfo($msg);

            //access log in db, for easy retrieval & analysis
            $this->logToDB($user_id, $action, $model, $message);
        } catch (\Exception $e) {
            reportException($e);

            \Log::error('Could not add an access log');
            \Log::error($e);
        }
    }

    protected function logToDB($user_id, $action, $model, $details = null)
    {
        $original = [];
        $new = [];

        if ($model && $model->isDirty()) {
            $original = $model->getOriginal();
            $new = $model->getDirty();
        }

        $changed = array_only($original, array_keys($new));

        $this->delaySave(AccessLog::class, [
            'id' => Uuid::generate()->string,
            'user_id' => $user_id,
            'action' => $action,
            'model_name' => get_class($model),
            'model_id' => $model->id,
            'details' => $details ? $details : 'DB Operation performed',
            'before' => $changed == [] ? null : json_encode($changed),
            'after' => $new == [] ? null : json_encode($new),
            'sapi' => php_sapi_name(),
            'app' => config('app.name')
        ]);
    }

    public function addActivityLog($description, $target, $user_id, $model_id, $model, $fund_manager_id)
    {
        $this->delaySave(ActivityLog::class, [
            'text' => $description,
            'target' => $target,
            'user_id' => $user_id,
            'model_id' => $model_id,
            'model' => $model,
            'fund_manager_id' => $fund_manager_id
        ]);
    }

    public function logAuthenticated()
    {
        $request = app(Request::class);

        if (app()->environment() == 'production') {
            $this->delaySave(CrimsAdminLoginTrail::class, [
                'id' => Uuid::generate()->string,
                'session_id' => Session::getId(),
                'user_id' => auth()->id(),
                'user_agent' => $request->server('HTTP_USER_AGENT'),
                'ip' => $request->ip()
            ]);
        }
    }

    /**
     * @param bool $skipWhenTesting
     * @return Logger
     */
    public function setSkipWhenTesting(bool $skipWhenTesting): Logger
    {
        $this->skipWhenTesting = $skipWhenTesting;
        return $this;
    }

    private function delaySave($model, $data)
    {
        delaySave($model, $data);
    }
}
