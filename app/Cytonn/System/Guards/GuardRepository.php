<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\System\Guards;

use App\Cytonn\Models\Guard;

class GuardRepository
{
    /*
     * Get a given guard by its id
     */
    public function getGuardById($id)
    {
        return Guard::findOrFail($id);
    }

    /*
     * Store or update a guard
     */
    public function save($input, $id)
    {
        if ($id) {
            $guard = $this->getGuardById($id);

            $guard->update($input);

            return $guard;
        }

        return Guard::create($input);
    }

    /*
     * Delete an existing client guard
     */
    public function delete($data)
    {
        $guard = $this->getGuardById($data['id']);

        $guard->delete();
    }
}
