<?php

namespace App\Cytonn\System;

use App\Cytonn\BaseModel;

class CrimsClientLoginTrail extends BaseModel
{
    protected $guarded = ['id'];

    public function __construct(array $attrs = [])
    {
        $this->connection = config('database.logging');

        parent::__construct($attrs);
    }
}
