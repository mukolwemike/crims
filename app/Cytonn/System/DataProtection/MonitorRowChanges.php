<?php
/**
 * Date: 19/07/2017
 * Time: 12:47
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\System\DataProtection;

use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Database\Eloquent\Model;

class MonitorRowChanges
{
    public function saving(Model $model)
    {
        //        $this->createIntegrityCheckSum($model);
    }

    protected function createIntegrityCheckSum(Model $model)
    {
        $hash = $model->getOriginal('hash');



        if (is_null($hash)) {
            throw new ClientInvestmentException("Data does not have integrity checksum");
        }

        dd('passed');
    }
}
