<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 24/07/2019
 * Time: 10:41
 */

namespace App\Cytonn\System\Locks;

interface Lock
{
    /**
     * @param $lockKey
     * @param int $lockFor
     * @param int $lockWaitTimeout
     * @return mixed
     */
    public function blockUntilLocked($lockKey, $lockFor = 60, $lockWaitTimeout = 0);

    public function acquireLock($lockKey, $lockFor = 60);

    public function releaseLock(LockRecord $lock);

    public function executeWithinLock(\Closure $closure, $lockKey, $lockFor = 60, $lockWaitTimeout = 0);
}
