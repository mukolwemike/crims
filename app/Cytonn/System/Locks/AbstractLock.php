<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 24/07/2019
 * Time: 10:30
 */

namespace App\Cytonn\System\Locks;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractLock implements Lock
{

    /**
     * @param $lockKey
     * @param int $lockFor
     * @param int $lockWaitTimeout
     * @return LockRecord
     * @throws LockException
     */
    public function blockUntilLocked($lockKey, $lockFor = 60, $lockWaitTimeout = 0)
    {
        $start = Carbon::now();

        while (!$lock = $this->acquireLock($lockKey, $lockFor)) {
            if ($lockWaitTimeout > 0) {
                $waited = Carbon::now()->diffInSeconds($start->copy());

                if ($waited > $lockWaitTimeout) {
                    throw new LockException(
                        "Timeout exceeded while waiting to obtain lock for $lockKey. Waited $lockFor seconds"
                    );
                }
            }

            usleep(rand(1000, 10000));
        }

        return $lock;
    }

    /**
     * @param \Closure $closure
     * @param $lockKey
     * @param int $lockFor
     * @param int $lockWaitTimeout
     * @return mixed
     * @throws \Exception
     */
    public function executeWithinLock(\Closure $closure, $lockKey, $lockFor = 60, $lockWaitTimeout = 0)
    {

        $lock = $this->blockUntilLocked($lockKey, $lockFor, $lockWaitTimeout);

        try {
            $results = call_user_func($closure);

            $this->releaseLock($lock);
        } catch (\Exception $e) {
            $this->releaseLock($lock);

            throw $e;
        }

        return $results;
    }

    /**
     * @param LockRecord $lock
     * @param $existing
     * @return bool
     * @throws LockException
     */
    protected function allowLockToBeReleased(LockRecord $lock, $existing)
    {

        if (!$existing) {
            throw new LockException("Lock for release not found. Probably already expired");
        }

        if ($lock->id !== json_decode($existing)->id) {
            throw new LockException("Lock IDs do not match, lock may have been acquired by another process");
        }

        return true;
    }

    /**
     * @param $lockKey
     * @return string
     */
    protected function getAtomicKey($lockKey)
    {
        if ($lockKey instanceof Model) {
            $lockKey = get_class($lockKey).'_'.$lockKey->getKeyName().'_'.$lockKey->getKey();
        }

        return 'atomic_lock_'.$lockKey;
    }
}
