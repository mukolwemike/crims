<?php

namespace App\Cytonn\System\Locks;

use Illuminate\Support\ServiceProvider;

class LockServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $locker = function () {
            if (config('cache.default') === 'redis') {
                return new RedisLock();
            }

            return new CacheLock();
        };

        $this->app->singleton('cytonn_atomic_lock', $locker);
        $this->app->singleton(Lock::class, $locker);
        $this->app->singleton(AbstractLock::class, $locker);
    }
}
