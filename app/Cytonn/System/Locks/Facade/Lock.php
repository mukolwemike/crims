<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 25/07/2019
 * Time: 09:09
 */

namespace App\Cytonn\System\Locks\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Class Lock
 * @package App\Cytonn\System\Locks\Facade
 * @method static bool blockUntilLocked($lockKey, $lockFor = 60, $lockWaitTimeout = 0);
 * @method static bool acquireLock($lockKey, $lockFor = 60);
 * @method static void releaseLock($lockKey);
 * @method static mixed executeWithinLock(\Closure $closure, $lockKey, $lockFor = 60, $lockWaitTimeout = 0);
 *
 */
class Lock extends Facade
{

    public static function getFacadeAccessor()
    {
        return 'cytonn_atomic_lock';
    }
}
