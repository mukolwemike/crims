<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 29/07/2019
 * Time: 10:53
 */

namespace App\Cytonn\System\Locks;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class LockRecord
{
    public $id;

    public $expiry;

    public $key;

    public $utime;

    /**
     * LockRecord constructor.
     * @param $key
     * @param $expiry
     * @throws \Exception
     */
    public function __construct(string $key, Carbon $expiry)
    {
        $this->expiry = $expiry;

        $this->key = $key;

        $this->id = Uuid::generate(4)->string;

        $this->utime = time();
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
