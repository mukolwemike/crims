<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 24/07/2019
 * Time: 10:40
 */

namespace App\Cytonn\System\Locks;

use Carbon\Carbon;

class CacheLock extends AbstractLock
{

    /**
     * @param $lockKey
     * @param $lockFor
     * @return bool | LockRecord
     * @throws \Exception
     */
    public function acquireLock($lockKey, $lockFor = 60)
    {
        $key = $this->getAtomicKey($lockKey);

        if (!cache()->has($key)) {
            $expiry = Carbon::now()->addSeconds($lockFor);
            $record = new LockRecord($key, $expiry);

            cache([$key => $record], $expiry);

            return $record;
        }

        return false;
    }

    /**
     * @param $lock
     * @return bool
     * @throws \Exception
     */
    public function releaseLock(LockRecord $lock)
    {
        $existing = cache()->get($lock->key);

        $this->allowLockToBeReleased($lock, $existing);

        cache()->forget($lock->key);

        return true;
    }
}
