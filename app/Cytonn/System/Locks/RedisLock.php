<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 24/07/2019
 * Time: 10:35
 */

namespace App\Cytonn\System\Locks;

use Carbon\Carbon;
use Illuminate\Contracts\Redis\Factory;
use Illuminate\Database\Eloquent\Model;

class RedisLock extends AbstractLock
{
    protected $connection;

    public function test($id)
    {
//        $this->executeWithinLock(function () {
//            sleep(15);
//        }, 'lock_test'.$id);
//
//        return Carbon::now()->toDateString();

        return $this->acquireLock('test'.$id, 600);
    }

    /**
     * @param LockRecord $lock
     * @throws LockException
     * @throws LockException
     */
    public function releaseLock(LockRecord $lock)
    {
        $key = $lock->key;

        $existing = $this->redis()->get($key);

        $this->allowLockToBeReleased($lock, $existing);

        $this->redis()->del([$key]);
    }

    /**
     * @param $lockKey
     * @param int $lockFor
     * @return bool | LockRecord
     * @throws \Exception
     */
    public function acquireLock($lockKey, $lockFor = 60)
    {
        $key = $this->getAtomicKey($lockKey);

        $redis = $this->redis();

        $record = new LockRecord($key, Carbon::now()->addSeconds($lockFor));

        $result = $redis->setnx($key, $record);

        if ($res = $result === 1) {
            $redis->expire($key, $lockFor);

            return $record;
        }

        return $res;
    }

    /**
     * @return \Illuminate\Redis\Connections\Connection
     */
    private function redis()
    {
        if ($this->connection) {
            return $this->connection;
        }

        return $this->connection =  app(Factory::class)
            ->connection(config('cache.stores.redis.connection'));
    }

    /**
     * @param $lockKey
     * @return string
     */
    protected function getAtomicKey($lockKey)
    {
        if ($lockKey instanceof Model) {
            $lockKey = get_class($lockKey).'_'.$lockKey->getKeyName().'_'.$lockKey->getKey();
        }

        return config('cache.prefix').':atomic_lock:'.$lockKey;
    }
}
