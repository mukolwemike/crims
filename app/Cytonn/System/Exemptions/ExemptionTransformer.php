<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\System\Exemptions;

use App\Cytonn\Models\Exemption;
use Cytonn\Presenters\ContactPresenter;
use League\Fractal\TransformerAbstract;

class ExemptionTransformer extends TransformerAbstract
{
    /**
     * @param Exemption $exemption
     * @return array
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     */
    public function transform(Exemption $exemption)
    {
        $client = $exemption->present()->getApproval->client;

        return [
            'id' => $exemption->id,
            'client_code' => $client->client_code,
            'name' => ContactPresenter::fullName($client->contact_id),
            'reason' => $exemption->reason,
            'date' => $exemption->date
        ];
    }
}
