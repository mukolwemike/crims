<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\System\Exemptions;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;

class ExemptionPresenter extends Presenter
{
    /*
     * Get the approval
     */
    public function getApproval()
    {
        if ($this->type == 1) {
            return $this->clientTransactionApproval;
        } else {
            return $this->portfolioTransactionApproval;
        }
    }
}
