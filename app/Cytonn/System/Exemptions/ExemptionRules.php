<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\System\Exemptions;

use Cytonn\Rules\Rules;

trait ExemptionRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate the creation of an exemption
     */
    public function createExemption($request)
    {
        $messages = [
            'date.required_if' => 'Please provide the date for the payment dates exemption type'
        ];

        $rules = [
            'type' => 'required',
            'date' => 'required_if:type,2',
            'reason' => 'required',
        ];

        return $this->verdict($request, $rules, $messages);
    }
}
