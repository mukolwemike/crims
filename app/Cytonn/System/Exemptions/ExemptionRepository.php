<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\System\Exemptions;

use App\Cytonn\Models\Exemption;

class ExemptionRepository
{
    /*
     * Get an exemption by its id
     */
    public function getExemptionById($id)
    {
        return Exemption::findOrFail($id);
    }

    /*
     * Save or update an exemption
     */
    public function save($input, $id)
    {
        if ($id) {
            $exemption = $this->getExemptionById($id);

            $exemption->update($input);

            return $exemption;
        }

        return Exemption::create($input);
    }
}
