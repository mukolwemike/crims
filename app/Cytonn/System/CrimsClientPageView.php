<?php

namespace App\Cytonn\System;

use App\Cytonn\BaseModel;
use Cytonn\Cassandra\Eloquent\Model;

class CrimsClientPageView extends Model
{
    protected $guarded = [];

    public function __construct(array $attrs = [])
    {
        $this->connection = 'cassandra';

        parent::__construct($attrs);
    }
}
