<?php
/**
 * Date: 06/01/2018
 * Time: 21:19
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\System\Processing\Support;

class AsyncProcessingException extends \Exception
{
}
