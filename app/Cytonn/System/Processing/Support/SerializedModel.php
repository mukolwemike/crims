<?php
/**
 * Date: 15/12/2017
 * Time: 12:19
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\System\Processing\Support;

use Illuminate\Database\Eloquent\Model;

class SerializedModel
{
    protected $model;

    protected $attributes;

    protected $keyName;

    protected $key;

    public function make(Model $model)
    {
        $this->model = get_class($model);
        $this->attributes = $model->getAttributes();
        $this->key = $model->getKey();
        $this->keyName = $model->getKeyName();

        return $this;
    }

    public function wake()
    {
        $model = app($this->model)->fill($this->attributes);

        $model->{$this->keyName} = $this->key;

        return $model;
    }
}
