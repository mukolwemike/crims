<?php
/**
 * Date: 15/12/2017
 * Time: 17:14
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\System\Processing\Tasks;

use App\Applications\CrimsAdmin;
use Cytonn\System\Processing\Support\SerializedModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class Task implements \Amp\Parallel\Worker\Task
{
    private $basePath;

    private $classes = [];

    /**
     * @param callable $function Do not use a closure or non-serializable object.
     * @param mixed    ...$args  Arguments to pass to the function. Must be serializable.
     */
    public function __construct()
    {
        $this->basePath = base_path();

        $this->classes = [
            'http' => get_class(app('Illuminate\Contracts\Http\Kernel')),
            'console' => get_class(app('Illuminate\Contracts\Console\Kernel')),
            'handler' => get_class(app('Illuminate\Contracts\Debug\ExceptionHandler'))
        ];
    }

    protected function boot()
    {
        include_once $this->basePath."/bootstrap/autoload.php";

        $app = new CrimsAdmin($this->basePath);

        $app->singleton(
            'Illuminate\Contracts\Http\Kernel',
            $this->classes['http']
        );

        $app->singleton(
            'Illuminate\Contracts\Console\Kernel',
            $this->classes['console']
        );

        $app->singleton(
            'Illuminate\Contracts\Debug\ExceptionHandler',
            $this->classes['handler']
        );

        $app->make($this->classes['console'])->bootstrap();

        return $app;
    }

    public function unserializeModels($args)
    {
        return array_map(
            function ($arg) {
                if ($arg instanceof SerializedModel) {
                    return $this->performModelUnSerialization($arg);
                }

                if ($arg instanceof Collection || is_array($arg)) {
                    $out = [];

                    foreach ($arg as $item) {
                        $out[] = $this->performModelUnSerialization($item);
                    }

                    if ($arg instanceof Collection) {
                        $out = collect($out);
                    }

                    return $out;
                }

                return $arg;
            },
            $args
        );
    }

    private function performModelSerialization($model)
    {
        if ($model instanceof Model) {
            return (new SerializedModel())->make($model);
        }

        return $model;
    }

    private function performModelUnSerialization($model)
    {
        if ($model instanceof SerializedModel) {
            return $model->wake();
        }

        return $model;
    }

    protected function serializeModels($args)
    {
        return array_map(
            function ($arg) {
                if ($arg instanceof Model) {
                    return $this->performModelSerialization($arg);
                }

                if ($arg instanceof Collection || is_array($arg)) {
                    $out = [];

                    foreach ($arg as $item) {
                        $out[] = $this->performModelSerialization($item);
                    }

                    if ($arg instanceof Collection) {
                        $out = collect($out);
                    }

                    return $out;
                }

                return $arg;
            },
            $args
        );
    }
}
