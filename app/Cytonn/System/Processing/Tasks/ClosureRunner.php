<?php

/**
 * Date: 12/12/2017
 * Time: 22:30
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\System\Processing\Tasks;

use Amp\Parallel\Worker\Environment;
use Cytonn\Presenters\BooleanPresenter;
use Cytonn\System\Processing\Support\AsyncProcessingException;
use Cytonn\System\Processing\Tasks\Task;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\Collection;

class ClosureRunner extends Task
{

    /**
     * @var callable
     */
    private $function;

    /**
     * @var mixed[]
     */
    private $args;

    private $asBatch = false;

    /**
     * @param callable $function Do not use a closure or non-serializable object.
     * @param mixed    ...$args  Arguments to pass to the function. Must be serializable.
     */
    public function __construct(callable $function, ...$args)
    {
        $this->function = serializeClosure($function);
        $this->args = $this->serializeModels($args);

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function run(Environment $environment)
    {
        $this->boot();

        $closure = unserializeClosure($this->function);

        $args = $this->unserializeModels($this->args);

        try {
            if (!$this->asBatch) {
                return call_user_func_array($closure, $args);
            }

            return $this->executeBatch($closure, $args);
        } catch (\Exception $exception) {
            $e = new AsyncProcessingException($exception->getMessage(), 0, $exception);

            app(ExceptionHandler::class)->report($e);

            throw $exception;
        }
    }

    private function executeBatch($closure, $args)
    {
        $batches = $args[0];

        $out = [];

        foreach ($batches as $batch) {
            $args[0] = $batch;
            $out[] = call_user_func_array($closure, $args);
        }

        //        if($batches instanceof Collection) $out = collect($out);

        print("completed batch".PHP_EOL);
    }

    public function asBatch()
    {
        $this->asBatch = true;

        return $this;
    }
}
