<?php
/**
 * Date: 15/12/2017
 * Time: 17:26
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\System\Processing;

use Amp\Coroutine;
use Amp\Loop;
use Amp\Parallel\Worker\DefaultPool;
use Amp\Parallel\Worker\DefaultWorkerFactory;
use Cytonn\System\Processing\Tasks\ClosureRunner;
use Cytonn\System\Processing\Tasks\Task;
use Illuminate\Support\Collection;

class Async
{
    const MIN_THREADS = 1;

    const MAX_THREADS = 20;

    public function map($collection, \Closure $closure)
    {
        if ($collection instanceof Collection) {
            $collection = $collection->all();
        }

        $tasks = array_map(
            function ($item) use ($closure) {
                return new ClosureRunner($closure, $item);
            },
            $collection
        );

        return $this->sendMultipleToEventLoop($tasks);
    }

    public function batchMap($collection, $batchSize, \Closure $closure)
    {
        $array = false;

        if (is_array($collection)) {
            $array = true;

            $collection = new Collection($collection);
        }

        $batches = $collection->chunk($batchSize);


        $tasks = array_map(
            function ($item) use ($closure) {
                $task = new ClosureRunner($closure, $item);

                return $task->asBatch();
            },
            $batches->all()
        );

        $out = $this->sendMultipleToEventLoop($tasks);

        print("Received ".count($out). " batches");

        $out = collect($out)->flatten();

        if ($array) {
            $out = $out->all();
        }

        return $out;
    }

    public function processClosure(\Closure $closure, ...$args)
    {
        $arr[] = $closure;
        $merged = array_merge($arr, $args);

        $task = new \ReflectionClass(ClosureRunner::class);

        $task = $task->newInstanceArgs($merged);

        return $this->sendSingleToEventLoop($task);
    }

    protected function sendSingleToEventLoop(Task $task)
    {
        $result = null;

        Loop::run(
            function () use ($task, &$result) {
                $factory = new DefaultWorkerFactory();
                $worker = $factory->create();
                $worker->start();

                $result = yield $worker->enqueue($task);

                yield $worker->shutdown();
            }
        );

        return $result;
    }

    protected function sendMultipleToEventLoop(array $tasks)
    {
        $results = [];
        // Event loop for parallel tasks
        Loop::run(
            function () use (&$results, &$tasks) {
                $pool = new DefaultPool(static::MIN_THREADS, static::MAX_THREADS);
                $pool->start();
                $coroutines = [];

                foreach ($tasks as $task) {
                    $coroutines[] = function () use ($pool, $task, &$results) {
                        array_push($results, yield $pool->enqueue($task));
                    };
                }

                $coroutines = array_map(
                    function (callable $coroutine): Coroutine {
                        return new Coroutine($coroutine());
                    },
                    $coroutines
                );

                yield \Amp\Promise\all($coroutines);
                return yield $pool->shutdown();
            }
        );

        return $results;
    }
}
