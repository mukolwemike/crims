<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Contacts;

use Laracasts\Presenter\Presenter;

class ContactPresenter extends Presenter
{
    /*
     * Present the fullname
     */
    public function fullname()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * @return mixed|null
     */
    public function getPhone()
    {
        $phone = trimPhoneNumber($this->phone);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        $phone = trimPhoneNumber($this->telephone_home);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        $phone = trimPhoneNumber($this->telephone_cell);

        if (isNotEmptyOrNull($phone)) {
            return $phone;
        }

        return null;
    }
}
