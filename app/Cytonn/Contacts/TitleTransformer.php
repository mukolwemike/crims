<?php

namespace App\Cytonn\Contact;

use App\Cytonn\Models\Title;
use League\Fractal\TransformerAbstract;

class TitleTransformer extends TransformerAbstract
{
    public function transform(Title $title)
    {
        return [
            'id'=>$title->id,
            'name'=>$title->name
        ];
    }
}
