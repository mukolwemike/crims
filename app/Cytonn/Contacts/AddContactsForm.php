<?php

namespace Cytonn\Contacts;

use Laracasts\Validation\FormValidator;

class AddContactsForm extends FormValidator
{
    public $rules = [
        'contact_category_id'=>'required',
        'contact_type_id'=>'required',
        'phone'=>'required'
    ];

    public function updaterules()
    {
        $this->rules = array_add($this->rules, 'email', 'required|email');
    }

    public function createrules()
    {
        $this->rules = array_add($this->rules, 'email', 'required|email');
    }

    public function individual()
    {
        $this->rules = array_add($this->rules, 'firstname', 'required');

        $this->rules = array_add($this->rules, 'lastname', 'required');
    }

    public function corporate()
    {
        $this->rules = array_add($this->rules, 'firstname', 'required');
    }
}
