<?php
/**
 * Date: 8/7/15
 * Time: 7:04 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Contacts;

/**
 * Class ContactsRepository
 *
 * @package Cytonn\Contacts
 */
use App\Cytonn\Models\Contact;
use Laracasts\Commander\Events\DispatchableTrait;

/**
 * Class ContactsRepository
 *
 * @package Cytonn\Contacts
 */
class ContactsRepository
{
    use DispatchableTrait;

    protected $contact;

    /**
     * ContactsRepository constructor.
     */
    public function __construct()
    {
        $this->contact = new Contact();
    }

    /**
     * @param array $contactdata
     * @return Contact
     */
    public function save($contactdata)
    {
        $this->contact->add($contactdata);

        $this->dispatchEventsFor($this->contact);

        return $this->contact;
    }
}
