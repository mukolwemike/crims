<?php
/**
 * Date: 8/28/15
 * Time: 7:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Contacts\Events;

class ContactHasBeenCreated
{
    /**
     * ContactHasBeenCreated constructor.
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }
}
