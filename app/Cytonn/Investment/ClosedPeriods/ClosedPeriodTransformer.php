<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ClosedPeriods;

use App\Cytonn\Models\ClosedPeriod;
use League\Fractal\TransformerAbstract;

class ClosedPeriodTransformer extends TransformerAbstract
{
    /**
     * @param ClosedPeriod $closedPeriod
     * @return array
     */
    public function transform(ClosedPeriod $closedPeriod)
    {
        return [
            'id' => $closedPeriod->id,
            'start' => $closedPeriod->start,
            'end' => $closedPeriod->end,
            'active' => $closedPeriod->present()->getActive,
        ];
    }
}
