<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ClosedPeriods;

use Laracasts\Presenter\Presenter;

class ClosedPeriodPresenter extends Presenter
{
    /*
     * Check on whether a closed period is active or not
     */
    public function getActive()
    {
        return $this->active == 1 ? 'Active' : "Inactive";
    }
}
