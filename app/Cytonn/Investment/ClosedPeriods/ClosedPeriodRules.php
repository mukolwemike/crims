<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ClosedPeriods;

use Cytonn\Rules\Rules;

trait ClosedPeriodRules
{
    /*
     * Get the overall rules trait
     */
    use Rules;

    /*
     * Validate closed period creation
     */
    public function closedPeriodCreate($request)
    {
        $rules = [
            'start' => 'required',
            'end' => 'required | after:start',
        ];

        return $this->verdict($request, $rules);
    }
}
