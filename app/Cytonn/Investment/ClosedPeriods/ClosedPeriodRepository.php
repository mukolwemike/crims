<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ClosedPeriods;

use App\Cytonn\Models\ClosedPeriod;

class ClosedPeriodRepository
{
    /*
     * Get a closed period by its id
     */
    public function getClosedPeriodById($id)
    {
        return ClosedPeriod::findOrFail($id);
    }

    /*
     * Save or update a closed period
     */
    public function save($input, $id)
    {
        if ($id) {
            $closedPeriod = $this->getClosedPeriodById($id);

            $closedPeriod->update($input);
        }

        return ClosedPeriod::create($input);
    }
}
