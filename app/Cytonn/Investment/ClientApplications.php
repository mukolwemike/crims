<?php

namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Core\Validation\ValidatorTrait;
use Laracasts\Commander\Events\DispatchableTrait;

class ClientApplications extends BaseInvestment
{
    use DispatchableTrait, ValidatorTrait;

    public function __construct()
    {
    }

    public function individualApplication($investorData)
    {
        $forbidden = [
            'corporate_investor_type', 'registered_name', 'trade_name',
            'registered_address', 'registration_number', 'company_pin_no', 'filled'
        ];

        $data = $this->cleanData($investorData, $forbidden); //remove fields that do not belong to applicant type

        $app = new ClientInvestmentApplication();

        $app_data = $this->filter($data, ClientInvestmentApplication::getTableColumnsAsArray());

        $app->add($app_data);

        $app->application_type_id = ClientInvestmentType::where('name', 'investment')->first()->id;

        $app->save();

        $app->raise(new Events\ApplicationHasBeenSubmitted($app));

        $this->dispatchEventsFor($app);

        return $app;
    }

    public function corporateApplication($investorData)
    {
        $forbidden = [
            'title', 'surname', 'middlename', 'firstname', 'gender', 'dob', 'id_or_passport_no', 'filled'
        ];

        $data = $this->cleanData($investorData, $forbidden); //remove fields that do not belong to applicant type


        $app = new ClientInvestmentApplication();
        $app_data = $this->filter($data, ClientInvestmentApplication::getTableColumnsAsArray());

        $app->add($app_data);

        $app->application_type_id = ClientInvestmentType::where('name', 'investment')->first()->id;

        $app->save();

        $app->raise(new Events\ApplicationHasBeenSubmitted($app));

        $this->dispatchEventsFor($app);

        return $app;
    }

    public function saveCompliance(User $user, ClientInvestmentApplication $app, $status)
    {
        $app->compliance_check_by = $user->id;
        $app->compliance_checked_on = new Carbon();
        $app->compliance_approved = $status;
        $app->save();

        return $app;
    }

    public function saveCompleteness(User $user, ClientInvestmentApplication $app, $status)
    {
        $app->completeness_check_by = $user->id;
        $app->completeness_checked_on = new Carbon();
        $app->completeness_approved = $status;

        $app->save();

        return $app;
    }

    protected function cleanData($arrayData, $forbidden)
    {
        foreach ($forbidden as $f) {
            unset($arrayData[$f]);
        }
        return $arrayData;
    }
}
