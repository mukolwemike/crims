<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 5/30/18
 * Time: 11:42 AM
 */

namespace App\Cytonn\Investment;

use App\Cytonn\Models\Product;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Investment\CalculatorTrait;

class Calculator
{
    use CalculatorTrait;

    public function calculateGoal($input)
    {
        $rate = $this->getInterestRate($input->product_id, $input->tenor, $input->amount);

        $interest = $input->goal - $input->amount;

        $total_value = $input->amount + $interest;

        $duration = (100 * $interest) / ($input->amount * $rate);

        return [
            'amount' => $total_value,
            'interest' => $interest,
            'duration' => $duration,
            'interest_rate' => $rate
        ];
    }

    public function calculateReturn($input)
    {
        $rate = $this->getInterestRate($input->product_id, $input->tenor, $input->amount);

        $interest = ($input->amount * ($rate/100) * ($input->tenor/12));

        $total_value = $input->amount + $interest;

        return [
            'amount' => $total_value,
            'return' => $interest,
            'interest_rate' => $rate
        ];
    }

    public function getInterestRate($productId, $tenor, $amount)
    {
        $product = Product::findOrFail($productId);

        return $product->repo->interestRate($tenor, $amount);
    }

    public function calculateInvestmentReturns($data)
    {
        $rate = $this->getInterestRate(
            $data['product_id'],
            $data['tenor'],
            $data['amount']
        );

        $start = Carbon::now();

        $end = $start->copy()->addMonth($data['tenor']);

        $g_interest = $this->getGrossInterest($data['amount'], $rate, $start, $end);

        $wht = $this->getWithHoldingTax($g_interest);

        $n_interest = $this->getNetInterest($g_interest, true);

        return [
            'principal' => $data['amount'],
            'gross_interest' => $g_interest,
            'net_interest' => $n_interest,
            'wht' => $wht,
            'wht_rate' => $this->withHoldingTaxRate(),
            'rate' => $rate,
            'tenor' => $data['tenor']
        ];
    }

    public function calculateInvestmentGoal($data)
    {
        $rate = $this->getInterestRate(
            $data['product'],
            12,
            $data['deposit']
        );

        $net_interest = $data['goal'] - $data['deposit'];

        $tax_rate = $this->withHoldingTaxRate();

        $gross_interest = 100 * $net_interest / (100 - $tax_rate);

        $duration = round((100 * $gross_interest * 12) / ($data['deposit'] * $rate));

        $wht = $gross_interest - $net_interest;

        return [
            'principal' => $data['deposit'],
            'goal' => $data['goal'],
            'duration' => $duration,
            'tax_rate' => $tax_rate,
            'net_interest' => $net_interest,
            'gross_interest' => $gross_interest,
            'wht' => $wht,
            'interest_rate' => $rate
        ];
    }
}
