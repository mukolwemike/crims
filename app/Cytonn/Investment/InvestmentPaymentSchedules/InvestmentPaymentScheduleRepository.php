<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\InvestmentPaymentSchedules;

use App\Cytonn\Models\ClientInvestment;
use Cytonn\Investment\CalculatorTrait;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;

class InvestmentPaymentScheduleRepository
{
    use CalculatorTrait;

    public $schedule;

    public function __construct(InvestmentPaymentSchedule $schedule = null)
    {
        $this->schedule = $schedule ? $schedule : new InvestmentPaymentSchedule();
    }

    public function calculate()
    {
        return (object) [
            'principal' => $this->schedule->amount,
            'gross_interest' => $this->grossInterest(),
            'net_interest' => $this->netInterest(),
            'total' => $this->totalValueAtMaturity()
        ];
    }

    public function grossInterest()
    {
        $investment = ClientInvestment::find($this->schedule->parent_investment_id);

        return $this->getGrossInterest(
            $this->schedule->amount,
            $this->schedule->interest_rate,
            $this->schedule->date,
            $investment->maturity_date
        );
    }

    public function netInterest()
    {
        $investment = ClientInvestment::find($this->schedule->parent_investment_id);

        $taxable = $investment->client->taxable;

        return $this->getNetInterest($this->grossInterest(), $taxable);
    }

    public function totalValueAtMaturity()
    {
        return $this->netInterest() + (float) $this->schedule->amount;
    }
}
