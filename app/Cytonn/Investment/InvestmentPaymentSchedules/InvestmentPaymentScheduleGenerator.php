<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\InvestmentPaymentSchedules;

use App\Cytonn\Models\ClientInvestment;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Reporting\BusinessConfirmationGenerator;

class InvestmentPaymentScheduleGenerator
{
    protected $protectDocument = false;

    /**
     * @param ClientInvestment $investment
     * @return \PDF
     * @throws \Exception
     */
    public function generate(ClientInvestment $investment)
    {
        $schedules = $investment->childInvestmentPaymentSchedules;

        $client = $investment->client;

        try {
            $user = \Auth::user();

            $email_sender = UserPresenter::presentLetterClosingNoSignature($user->id);
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments Management Limited.';
        };

        $fundManager = $investment->product->fundManager;

        $pdf = \PDF::loadView('reports.investments.payment_schedule', [
            'investment' => $investment,
            'schedules' => $schedules,
            'client' => $client,
            'product' => $investment->product,
            'sender' => $email_sender,
            'signature' => $fundManager->signature,
            'logo' => $fundManager->logo,
            'emailSender' => $fundManager->principal_partner ? $fundManager->principal_partner : $fundManager->fullname,
        ]);

        if ($this->protectDocument) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    /**
     * @param bool $protectDocument
     * @return $this
     */
    public function setProtectDocument(bool $protectDocument)
    {
        $this->protectDocument = $protectDocument;

        return $this;
    }
}
