<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\InvestmentPaymentSchedules;

use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use League\Fractal\TransformerAbstract;

class InvestmentPaymentScheduleTransformer extends TransformerAbstract
{
    /**
     * @param InvestmentPaymentSchedule $paymentSchedule
     * @return array
     */
    public function transform(InvestmentPaymentSchedule $paymentSchedule)
    {
        return [
            'id' => $paymentSchedule->id,
            'date' => DatePresenter::formatDate($paymentSchedule->date),
            'description' => $paymentSchedule->description,
            'amount' => AmountPresenter::currency($paymentSchedule->amount),
            'interest_rate' => $paymentSchedule->interest_rate,
            'paid' => $paymentSchedule->paid == 1 ? "Yes" : "No",
            'date_paid' => $paymentSchedule->date_paid ? DatePresenter::formatDate($paymentSchedule->date_paid) : '',
            'investment_id' => $paymentSchedule->investment_id,
            'parent_investment_id' => $paymentSchedule->parent_investment_id,
            'show_url' => $paymentSchedule->investment ? '/dashboard/investments/clientinvestments/' . $paymentSchedule->investment_id
                : '/dashboard/investments/investment_payment_schedules/details/' . $paymentSchedule->id,
        ];
    }
}
