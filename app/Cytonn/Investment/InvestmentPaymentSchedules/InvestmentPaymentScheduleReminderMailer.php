<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\InvestmentPaymentSchedules;

use App\Cytonn\Models\Client;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Mails\Mailer;

class InvestmentPaymentScheduleReminderMailer
{
    /**
     * @param Client $client
     * @param $scheduleGroup
     */
    public function sendEmail(Client $client, $scheduleGroup)
    {
        $user = \Auth::user();

        $email_sender = $user ? UserPresenter::presentLetterClosingNoSignature($user->id) :
            'Cytonn Investments Management Limited.';

        $data = [
            'client' => $client,
            'schedules' => $scheduleGroup,
            'email_sender' => $email_sender
        ];

        $subject = 'Reminder for Overdue Investment Payments - ' . ClientPresenter::presentShortName($client->id);

        $cc = ['operations@cytonn.com'];

        if ($fa = $client->getLatestFA()) {
            $cc[] = $fa->email;
        }

        Mailer::compose()
            ->from(['operations@cytonn.com' => 'Cytonn Investments'])
            ->to($client->getContactEmailsArray())
            ->cc($cc)
            ->bcc(config('system.administrators'))
            ->subject($subject)
            ->view('emails.investment.investment_payment_schedules.payment_reminder', $data)
            ->send();
    }
}
