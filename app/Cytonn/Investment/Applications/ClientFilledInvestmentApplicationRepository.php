<?php

namespace App\Cytonn\Investment\Applications;

use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationDocument;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Document;
use App\Events\InvestmentApplicationSaved;
use App\Events\KycDocumentUploaded;
use Cytonn\Core\Storage\StorageInterface;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Investment\ClientApplications;
use Cytonn\Investment\JointInvestorAddCommand;
use Cytonn\Investment\Rules\InvestmentApplicationRules;
use Laracasts\Commander\CommanderTrait;
use Webpatser\Uuid\Uuid;

class ClientFilledInvestmentApplicationRepository
{
    use CommanderTrait, InvestmentApplicationRules;

    public $application;

    protected $applicationRepo;

    public function __construct(ClientFilledInvestmentApplication $application = null)
    {
        is_null($application)
            ? $this->application = new ClientFilledInvestmentApplication()
            : $this->application = $application;

        $this->applicationRepo = new ApplicationsRepository();
    }

    public function saveApplication($input)
    {
        $this->application->fill($input);
        $this->application->progress_risk = true;
        $this->application->progress_investment = true;
        $this->application->progress_subscriber = true;
        $this->application->save();

        event(new InvestmentApplicationSaved($this->application));

        return $this->application;
    }

    public function saveApplicationInstruction($input)
    {
        $this->application->fill($input);
        $this->application->progress_risk = true;
        $this->application->progress_investment = true;
        $this->application->progress_subscriber = true;
        $this->application->save();

        return $this->application;
    }

    public function updateApplicationInstruction($input)
    {
        $input = array_except($input, 'id');

        $this->application->update($input);

        $this->application->save();

        return $this->application;
    }

    public function saveContactPersons($input)
    {
        $contacts = $input['contacts'];

        $this->application->contactPersons()->createMany($contacts);

        return $this->application;
    }

    public function saveJointHolders($input)
    {
        $holders = $input['holders'];

        $this->application->jointHolders()->createMany($holders);

        return $this->application;
    }

    public function saveClientDocument($input, Document $document)
    {
        $filledDocument = new ClientFilledInvestmentApplicationDocument();

        $filledDocument->application()->associate($this->application);

        $filledDocument->document()->associate($document);

        $filledDocument->payload = json_encode($input);

        $filledDocument->save();

        return $filledDocument;
    }


    public function saveKycDocument($file, $slug, ClientUser $user = null)
    {
        $filename = Uuid::generate()->string . '.' . $file->getClientOriginalExtension();

        $doc = new ClientUploadedKyc();
        $doc->application_id = $this->application->id;
        $doc->filename = $filename;
        $doc->kyc_id = ClientComplianceChecklist::where('slug', $slug)->first()->id;

        $file_contents = file_get_contents($file);

        \App::make(StorageInterface::class)->put($doc->path(), $file_contents);

        if ($this->checkKycComplete()) {
            $this->application->progress_kyc = true;
            $this->application->save();
        }

        if ($user) {
            event(new KycDocumentUploaded($this->application, $doc));
        }

        return $doc->save();
    }

    public function savePaymentDocument($file, $slug, ClientUser $user = null)
    {
        $filename = Uuid::generate()->string . '.' . $file->getClientOriginalExtension();

        $doc = new ClientUploadedKyc();
        $doc->application_id = $this->application->id;
        $doc->filename = $filename;
        $doc->kyc_id = ClientComplianceChecklist::where('slug', $slug)->first()->id;

        $this->application->progress_payment = true;
        $this->application->save();

        $file_contents = file_get_contents($file);
        \App::make(StorageInterface::class)->put($doc->path(), $file_contents);

        if ($user) {
            event(new KycDocumentUploaded($this->application, $doc));
        }

        return $doc->save();
    }

    public function saveMandateDocument()
    {
        $input = request()->all();

        $slug = $input['slug'];

        $file = request()->file('file');

        unset($input['file']);

        $application = ClientFilledInvestmentApplication::findOrFailByUuid($input['application_id']);

        $document = Document::make(file_get_contents($file), $file->getClientOriginalExtension(), $slug);

        unset($input['application_id']);
        $input['application_id'] = $application->id;

        $application->clientRepo->upload($input, $application, $document);

        return response(['created' => true]);
    }

    public function aboveDocumentLimit($slug)
    {
        $document = ClientComplianceChecklist::where('slug', $slug)->first();

        if (is_null($document)) {
            throw new \Exception('Document type was not found');
        }

        return ClientUploadedKyc::where('application_id', $this->application->id)
                ->where('kyc_id', $document->id)
                ->count() >= 5;
    }

    public function checkKycComplete()
    {
        if ($this->application->individual) {
            $c_id = ClientType::where('name', 'individual')->first()->id;
        } else {
            $c_id = ClientType::where('name', 'corporate')->first()->id;
        }

        $required = ClientComplianceChecklist::where('client_type_id', $c_id)
            ->where('slug', '!=', 'transfer_individual')
            ->where('slug', '!=', 'transfer_corporate')
            ->pluck('id')
            ->all();

        $uploaded = ClientUploadedKyc::where('application_id', $this->application->id)
            ->pluck('kyc_id')->all();

        return !(bool)array_diff($required, $uploaded);
    }

    public function process()
    {
        $application = $this->application;

        if ($application->filled) {
            throw new ClientInvestmentException('The form has already been used');
        }

        $input = $this->cleanApplication($application->toArray());

        $input['terms_accepted'] = true;

        $input['form_id'] = $application->id;

        return \DB::transaction(function () use ($application, $input) {
            $processed = $application->individual

                ? $this->processIndividual($application, $input)
                : $this->processCorporate($application, $input);

            $approval = ClientTransactionApproval::add([
                'client_id' => $application->client_id,
                'transaction_type' => 'application',
                'payload' => ['application_id' => $processed->id],
                'scheduled' => 0
            ]);

            $processed->update(['approval_id' => $approval->id]);

            return $processed;
        });
    }

    protected function processIndividual(ClientFilledInvestmentApplication $application, array $input)
    {
        $app = new ClientApplications();

        $input['client_id'] = null;

        if ($application->jointHolders) {
            $input['joint'] = true;
        }

        $input = array_add(
            $input,
            'type_id',
            ClientType::where('name', 'individual')->first()->id
        );

        $processed = $app->individualApplication($input);

        return $processed;
    }


    protected function processCorporate(ClientFilledInvestmentApplication $application, array $input)
    {
        $app = new ClientApplications();

        $input['client_id'] = null;

        $input = array_add($input, 'type_id', ClientType::where('name', 'corporate')
            ->first()->id);

        return $app->corporateApplication($input);
    }

    private function cleanApplication($input)
    {
        $forbidden = ['individual', 'kin_name', 'kin_phone', 'kin_postal', 'kin_email', 'progress_investment',
            'progress_subscriber', 'progress_kyc', 'progress_payment', 'progress_mandate', 'complete',
            'app_start_email', 'uuid', 'id'
        ];

        foreach ($forbidden as $key) {
            unset($input[$key]);
        }

        return $input;
    }

    private function addJointHolders($jointHolders, $applId, $clientId)
    {
        $jointHolders->each(
            function ($jointHolder) use ($applId, $clientId) {
                $jointHolder = $jointHolder->toArray();
                $jointHolder = array_add($jointHolder, 'joint_id', null);
                $jointHolder = array_add($jointHolder, 'client_id', $clientId);

                $jointHolder['application_id'] = $applId;

                $this->execute(JointInvestorAddCommand::class, ['data' => $jointHolder]);
            }
        );
    }

    public function cleanApplicationData($input)
    {
        $input['individual'] = request('client_type');

        $input['complete'] = ($input['complete']) ? 1 : 0;

        $input = array_except($input, ['client_type', 'application_type', 'product_category']);

        return $input;
    }

    public function validateEntryLevelApplication($input)
    {
        $clientType = $input['client_type'] == 0 ? 'individual' : 'corporate';

        if ($input['client_id']) {
            return null;
        }

        $validator = ($clientType == 'individual')
            ? $this->shortIndividualApplicationForm(request())
            : $this->shortCorporateApplicationForm(request());

        return $validator;
    }
}
