<?php
/**
 * Date: 09/12/2015
 * Time: 7:45 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class MaturityRepository
{
    public function getWeeklyMaturityAnalysisFor($start, $end, $product)
    {
        $start = new Carbon($start);
        $end = new Carbon($end);

        $date = new Carbon($start->copy());

        $weekly_coll = new Collection();

        while ($date->lte($end->copy())) {
            $investments = ClientInvestment::where('product_id', $product->id)
                ->where('maturity_date', '>=', $date->copy()->startOfWeek()->toDateString())
                ->where('maturity_date', '<=', $date->copy()->endOfWeek()->toDateString())
                ->where('withdrawn', '!=', 1);

            $weekly_coll->push([
                'amount'=>$investments->sum('amount'),
                'start'=>$date->copy()->startOfWeek(),
                'end'=>$date->copy()->endOfWeek()]);

            $date->addWeek();
        }

        return $weekly_coll;
    }

    public function getMonthlyMaturityAnalysisFor($start, $end, $product)
    {
        $start = new Carbon($start);
        $end = new Carbon($end);

        $monthly_coll = new Collection();

        $date = new Carbon($start->copy()); //reset

        while ($date->lte($end->copy())) {
            $investments = ClientInvestment::where('product_id', $product->id)
                ->where('maturity_date', '>=', $date->copy()->startOfMonth()->toDateString())
                ->where('maturity_date', '<=', $date->copy()->endOfMonth()->toDateString())
                ->where('withdrawn', '!=', 1);

            $monthly_coll->push([
                'amount'=>$investments->sum('amount'),
                'start'=>$date->copy()->startOfMonth(),
                'end'=>$date->copy()->endOfMonth()]);

            $date->addMonth();
        }

        return $monthly_coll;
    }
}
