<?php
/**
 * Date: 08/06/2016
 * Time: 8:28 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Handlers;

use Cytonn\Investment\Commands\RegisterCommissionClawBackCommand;
use Cytonn\Investment\Commission\Generator as CommissionGenerator;
use Cytonn\Investment\Commission\InvestmentEdit;
use Cytonn\Investment\Events\ClientInvestmentHasBeenAdded;
use Cytonn\Investment\Events\ClientPaymentIsDue;
use Cytonn\Investment\Events\CombinedInvestmentsRolledOver;
use Cytonn\Investment\Events\InterestHasBeenReinvested;
use Cytonn\Investment\Events\InvestmentHasBeenEdited;
use Cytonn\Investment\Events\InvestmentHasBeenRolledOver;
use Cytonn\Investment\Events\InvestmentHasBeenWithdrawn;
use Cytonn\Investment\InterestRepository;
use Cytonn\Investment\TaxRepository;
use Laracasts\Commander\CommanderTrait;
use Cytonn\Investment\Events\InvestmentHasBeenToppedUp;

class EventHandler extends \Cytonn\Handlers\EventListener
{
    use CommanderTrait;

    /**
     * @param ClientPaymentIsDue $event
     */
    public function whenClientPaymentIsDue(ClientPaymentIsDue $event)
    {
        //        //record payment
        //        $instruction = new \BankInstruction();
        //        $instruction->investment_id = $event->investment->id;
        //        $instruction->amount = $event->amount;
        //        $instruction->description = $event->description;
        //        $instruction->type = $event->type;
        //        $instruction->date = $event->date;
        //        $instruction->save();
        //
        //        if($event->type == 'withdraw')
        //        {
        //            $event->investment->withdrawal_instruction_id = $instruction->id;
        //            $event->investment->save();
        //        }
        //        elseif($event->type == 'interest_payment')
        //        {
        //            $payment = $event->investment->payments()->latest()->first();
        //            $payment->instruction_id = $instruction->id;
        //            $payment->save();
        //        }
        //
        //        if($event->approval)
        //        {
        //            $instruction->approval_id = $event->approval->id;
        //            $instruction->save();
        //        }
    }

    /**
     * @param ClientInvestmentHasBeenAdded $event
     */
    public function whenClientInvestmentHasBeenAdded(ClientInvestmentHasBeenAdded $event)
    {
        (new CommissionGenerator())->createSchedules($event->investment);
        (new InterestRepository())->generateInterestScheduleForInvestment($event->investment);
    }

    /**
     * @param InvestmentHasBeenRolledOver $event
     */
    public function whenInvestmentHasBeenRolledOver(InvestmentHasBeenRolledOver $event)
    {
        //        (new TaxRepository())->recordWithholdingTaxForInvestment($event->rollover->oldInvestment);
    }

    /**
     * @param CombinedInvestmentsRolledOver $event
     */
    public function whenCombinedInvestmentsRolledOver(CombinedInvestmentsRolledOver $event)
    {
    }

    public function whenInvestmentHasBeenToppedUp(InvestmentHasBeenToppedUp $topUp)
    {
        $investment = $topUp->topup->client;

        $sender = $investment->approval ? $investment->approval->sender : null;

        if (!$investment->isSeip()) {
            $investment->repo->sendBusinessConfirmation($sender);
        }
    }

    public function whenInterestHasBeenReinvested(InterestHasBeenReinvested $event)
    {
        $investment = $event->reinvestment;

        $sender = $investment->approval ? $investment->approval->sender : null;

        if (!$investment->isSeip()) {
            $investment->repo->sendBusinessConfirmation($sender);
        }
    }

    /**
     * @param InvestmentHasBeenWithdrawn $event
     */
    public function whenInvestmentHasBeenWithdrawn(InvestmentHasBeenWithdrawn $event)
    {
        //record taxation for the investment
        (new TaxRepository())->recordWithholdingTaxForInvestment($event->withdrawal->client);

        if ($event->withdrawal->premature) {
            $this->execute(
                RegisterCommissionClawBackCommand::class,
                ['investment' => $event->withdrawal->client, 'approval' => $event->withdrawal->client->withdrawApproval,
                    'newInvestment' => $event->withdrawal->newInvestment]
            );
        }
    }

    /**
     * @param InvestmentHasBeenEdited $event
     */
    public function whenInvestmentHasBeenEdited(InvestmentHasBeenEdited $event)
    {

        //correct interest payment
        (new InterestRepository())->reGenerateInterestScheduleForInvestment($event->investment, $event->oldInvestment);

        //handle commission
        InvestmentEdit::handle($event->oldInvestment, $event->investment, $event->approval, $event->editedColumns);
    }
}
