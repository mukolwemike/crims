<?php

namespace App\Cytonn\Investment;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInstructionType;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\System\Channel;
use App\Exceptions\CrimsException;
use Carbon\Carbon;

class InvestmentActionHandler
{
    public function topUp(Client $client, array $data, ClientUser $user)
    {
        if (isset($data['period'])) {
            unset($data['period']);
        }

        if (isset($data['agreed_to_terms'])) {
            unset($data['agreed_to_terms']);
        }

        $form = new ClientTopupForm();

        $product = Product::findOrFail($data['product']);
        $channel = $this->getChannelId($data);

        unset($data['product']);
        unset($data['entered_tenor']);
        unset($data['file']);
        unset($data['channel']);

        $form->fill($data);
        $form->user_id = $user->id;
        $form->client_id = $client->id;
        $form->product_id = $product ? $product->id : null;
        $form->amount = $data['amount'];
        $form->channel_id = $channel;
        $form->agreed_rate = $data['agreed_rate'];
        $form->tenor = $data['tenor'];
        $form->mode_of_payment = (isset($data['mode_of_payment']))
            ? $data['mode_of_payment'] : null;
        $form->agreed_to_terms = (isset($data['agreed_to_terms'])) ? $data['agreed_to_terms'] : null;

        $form->save();


        return $form;
    }

    public function topUpInvestment(Client $client, Product $product, array $data, ClientUser $user = null)
    {
        $channel = $this->getChannelId($data);

        $form = new ClientTopupForm();

        unset($data['product']);

        $form->fill($data);
        $form->user_id = null;
        $form->client_id = $client->id;
        $form->product_id = $product->id;
        $form->amount = $data['amount'];
        $form->agreed_rate = $data['agreed_rate'];
        $form->tenor = $data['tenor'];
        $form->channel_id = $channel;
        $form->mode_of_payment = isset($data['mode_of_payment']) ? $data['mode_of_payment'] : null;
        $form->save();

        return $form;
    }

    public function editInvestmentTopupInstruction($data)
    {
        $form = (new ClientTopupForm())->findOrFail($data['id']);
        $form->fill($data);
        $form->save();
    }

    public function rollover(ClientInvestment $investment, $data, ClientUser $user)
    {
        $type_id = ClientInstructionType::where('name', 'rollover')->first()->id;
        $channel = $this->getChannelId($data);

        unset($data['channel']);

        $data = array_add($data, 'channel_id', $channel);
        $data = array_add($data, 'amount', $data['amount']);
        $data = array_add($data, 'investment_id', $investment->id);
        $data = array_add($data, 'user_id', $user->id);
        $data = array_add($data, 'type_id', $type_id);
        $data = array_add($data, 'due_date', (new Carbon($investment->maturity_date))->addDay());

        return ClientInvestmentInstruction::add($data);
    }

    public function withdraw(ClientInvestment $investment, $data, ClientUser $user)
    {
        $due_date = (new Carbon($investment->maturity_date));
        $data["amount_select"] = "principal_interest";
        $type_id = (new ClientInstructionType())->where('name', 'withdraw')->first()->id;
        $channel = $this->getChannelId($data);

        unset($data['account']);
        unset($data['currency']);
        unset($data['channel']);

        $data['investment_id'] = $investment->id;
        $data = array_add($data, 'user_id', $user->id);
        $data = array_add($data, 'channel_id', $channel);
        $data = array_add($data, 'type_id', $type_id);
        $data = array_add($data, 'due_date', $due_date);

        return ClientInvestmentInstruction::add($data);
    }

    public function withdrawInvestment(ClientInvestment $investment, $data)
    {
        $due_date = $investment->maturity_date;

        if (isset($data['withdrawal_stage']) && $data['withdrawal_stage'] == 'premature') {
            $due_date = Carbon::parse($data['withdrawal_date']);
        }

        $channel = $this->getChannelId($data);
        $type_id = ClientInstructionType::where('name', 'withdraw')->first()->id;

        $data['investment_id'] = $investment->id;
        $data = array_add($data, 'user_id', null);
        $data = array_add($data, 'type_id', $type_id);
        $data = array_add($data, 'due_date', $due_date);
        $data = array_add($data, 'channel_id', $channel);

        unset($data['channel']);

        return ClientInvestmentInstruction::add($data);
    }

    public function editWithdrawalInstruction($data)
    {
        $form = (new ClientInvestmentInstruction())->findOrFail($data['id']);
        $form->fill($data);
        $form->save();
    }

    public function investmentRollover(ClientInvestment $investment, $data)
    {
        $type_id = ClientInstructionType::where('name', 'rollover')->first()->id;

        $due_date = $investment->maturity_date;

        if (isset($data['combine_date']) && strlen($data['combine_date']) > 0) {
            $due_date = Carbon::parse($data['combine_date']);
        }
        $channel = $this->getChannelId($data);

        unset($data['combine_date']);
        unset($data['channel']);

        $data = array_add($data, 'investment_id', $investment->id);
        $data = array_add($data, 'user_id', null);
        $data = array_add($data, 'channel_id', $channel);
        $data = array_add($data, 'type_id', $type_id);
        $data = array_add($data, 'due_date', $due_date);

        if ($data['due_date']->gt($investment->maturity_date)) {
            throw new CrimsException("The due date can not be after maturity");
        }

        return ClientInvestmentInstruction::add($data);
    }

    public function editRolloverInstruction($data)
    {
        $form = (new ClientInvestmentInstruction())->findOrFail($data['id']);
        $form->fill($data);
        $form->save();
    }

    /**
     * @param $data
     * @return mixed|null
     */
    public function getChannelId($data)
    {
        return isset($data['channel']) ? Channel::where('slug', $data['channel'])->first()->id : null;
    }
}
