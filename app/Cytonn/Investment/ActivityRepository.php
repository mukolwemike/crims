<?php
/**
 * Date: 21/12/2015
 * Time: 9:02 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\ClientBusinessConfirmation;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplicationKycCheck;
use App\Cytonn\Models\InterestPayment;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class ActivityRepository
{
    public function getInvestmentActivitiesForPeriod($start, $end, $useCache = true)
    {
        $start = (new Carbon($start))->toDateString();
        $end = (new Carbon($end))->toDateString();

        //cache results to improve experience
        $expiresAt = Carbon::now()->addMinutes(10);
        $cacheKey = $start.$end.'investment_activity_log';

        if (\Cache::has($cacheKey) && $useCache) {
            return \Cache::get($cacheKey);
        }
        
        $investments = ClientInvestment::where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)->latest()->get();

        $inv_coll = new Collection();

        foreach ($investments as $investment) {
            try {
                $investment->client_code = $investment->client->client_code;
                $investment->currency = $investment->product->currency->code;
                $investment->name = \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id);
                $investment->type = ucfirst($investment->investmentType->name);
                $investment->originator = UserPresenter::presentFullNamesNoTitle($investment->approval->sent_by);
                $investment->approver = UserPresenter::presentFullNamesNoTitle($investment->approval->approved_by);
            } catch (\Exception $e) {
            }

            $inv_coll->push($investment);
        }

        $confirmations = ClientBusinessConfirmation::with('investment')
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)->latest()->get();

        foreach ($confirmations as $confirmation) {
            $confirmation->sender = UserPresenter::presentFullNamesNoTitle($confirmation->sent_by);

            $confirmation->client_name = ClientPresenter::presentFullNames($confirmation->investment->client_id);

            $confirmation->type = ucfirst($confirmation->investment->investmentType->name);
        }

        $applications = ClientInvestmentApplication::where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)->latest()->get();

        foreach ($applications as $app) {
            $app->name = ClientPresenter::presentFullNames($app->client_id);

            try {
                $app->approved_by = UserPresenter::presentFullNamesNoTitle($app->approval->approved_by);

                $app->originator = UserPresenter::presentFullNamesNoTitle($app->approval->sent_by);

                $app->compliant = $app->repo->compliance()->approved;
            } catch (\Exception $e) {
            }
        }

        $instructions = BankInstruction::where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)->latest()->get();

        foreach ($instructions as $instr) {
            try {
                $instr->name = ClientPresenter::presentFullNames($instr->investment->client_id);
            } catch (\Exception $e) {
            }
        }

        $compliance = ClientInvestmentApplicationKycCheck::where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)->latest()->get();

        $payment = InterestPayment::where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)->latest()->get();

        foreach ($payment as $p) {
            $p->name = ClientPresenter::presentFullNames($p->investment->client_id);
        }

        $log =  [
            'investments'=>$investments,
            'confirmations'=>$confirmations,
            'applications'=>$applications,
            'instructions'=>$instructions,
            'compliance'=>$compliance,
            'payment'=>$payment
        ];

        //\Cache::put($cacheKey, $log, $expiresAt);

        return $log;
    }
}
