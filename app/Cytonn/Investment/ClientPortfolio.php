<?php

namespace App\Cytonn\Investment;

use App\Cytonn\Investment\Reports\InvestmentReporting;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use Carbon\Carbon;

class ClientPortfolio extends InvestmentReporting
{
    public function clientInvestment(Product $product, Client $client, Carbon $date)
    {
        return $this->invSummary($client, $product, $date);
    }

    public function invSummary($client, $product, $date)
    {
        $inv = ClientInvestment::where('client_id', $client->id)
            ->where('product_id', $product->id)
            ->where('invested_date', '<=', $date->copy()->addDay())
            ->where(
                function ($query) use ($date) {
                    $query->where('withdrawn', null)->orWhere('withdrawn', 0)
                        ->orWhere('withdrawn', true)
                        ->where('withdrawal_date', '>=', $date->copy()->subMonthNoOverflow());
                }
            )->orderBy('invested_date', 'ASC')
            ->get();

        $inv->map(
            function ($investment) use ($date, $product) {
                $this->simpleStatement($investment, $date, $product);
            }
        );

        $invTotals = $this->calculateTotals($inv);
        
        return [ 'investments'=>$inv, 'totals'=>$invTotals];
    }
}
