<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ProductDocuments;

use App\Cytonn\Models\ProductDocument;

class ProductDocumentRepository
{
    /*
     * Get a product document record by its id
     */
    public function getProductDocumentById($id)
    {
        return ProductDocument::findOrFail($id);
    }
    
    /*
     * Store or update a product document
     */
    public function save($input, $id)
    {
        if ($id) {
            $productDocument = $this->getProductDocumentById($id);

            $productDocument->update($input);

            return $productDocument;
        }

        return ProductDocument::create($input);
    }
}
