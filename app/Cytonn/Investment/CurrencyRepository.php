<?php
/**
 * Date: 13/08/2016
 * Time: 5:24 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use Cytonn\Investment\Summary\Analytics;

/**
 * Class CurrencyRepository
 *
 * @package Cytonn\Investment
 */
class CurrencyRepository
{
    /**
     * @var Currency
     */
    protected $currency;

    /**
     * CurrencyRepository constructor.
     *
     * @param Currency $currency
     */
    public function __construct(Currency $currency = null)
    {
        is_null($currency) ? $this->currency = new Currency() : $this->currency = $currency;
    }

    /**
     * Get investments for currency that were active on a date
     *
     * @param  $date
     * @return mixed
     */
    public function investmentsActiveOnDate($date)
    {
        return ClientInvestment::whereHas(
            'product',
            function ($product) {
                $product->where('currency_id', $this->currency->id);
            }
        )->activeOnDate($date)
            ->get();
    }


    /**
     * Weighted rate for currency
     *
     * @param  $product
     * @param  $date
     * @return float|int
     */
    public function weightedRateAtDate($fm, $date)
    {
        return (new Analytics($fm, $date))->setBaseCurrency($this->currency)->weightedRate();
    }

    /**
     * Weighted tenor for currency
     *
     * @param  $product
     * @param  null    $date
     * @return number
     */
    public function weightedTenorAtDate($fm, $date)
    {
        return (new Analytics($fm, $date))->setBaseCurrency($this->currency)->weightedTenor();
    }

    /**
     * Calculate the persistence for the currency
     *
     * @param  $product
     * @param  null    $startDate
     * @param  null    $endDate
     * @return float|int
     */
    public function persistence($fm, $startDate, $endDate)
    {
        return (new Analytics($fm, $endDate))->setBaseCurrency($this->currency)->persistence($startDate, $endDate);
    }
}
