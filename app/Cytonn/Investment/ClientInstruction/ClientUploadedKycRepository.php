<?php
/**
 * Date: 30/05/2016
 * Time: 12:02 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\ClientInstruction;

use App\Cytonn\Data\Iprs;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Client\Kyc\IprsRequest;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\ClientUploadedKyc;
use App\Cytonn\Models\Document;
use App\Cytonn\Models\DocumentType;
use Cytonn\Core\Storage\StorageInterface;
use Illuminate\Support\Facades\Auth;
use Webpatser\Uuid\Uuid;

class ClientUploadedKycRepository
{
    protected $kyc;

    public function __construct(ClientUploadedKyc $kyc = null)
    {
        is_null($kyc) ? $this->kyc = new ClientUploadedKyc() : $this->kyc = $kyc;
    }

    public function getLocation()
    {
        $path = app(StorageInterface::class)->getStorageLocation();

        if ($this->kyc->document_id == 5 || $this->kyc->document_id == 11) {
            return $path . '/payment';
        } else {
            return $path . '/kyc';
        }
    }

    public function uploadKYC($file_contents, $file_ext, ClientUploadedKyc $kyc = null)
    {
        if ($kyc) {
            $filename = $kyc->document->filename;
            $document = Document::findOrFail($kyc->document_id);
        } else {
            $filename = Uuid::generate()->string . '.' . $file_ext;
            $document = new Document();
        }
        $url = $this->getLocation() . '/' . $filename;

        \App::make(StorageInterface::class)->filesystem()->put($url, $file_contents);

        $document->type_id = DocumentType::where('slug', 'kyc')->first()->id;
        $document->filename = $filename;
        $document->save();

        return $document;
    }

    public function saveKyc($data, ClientComplianceChecklist $kyc, Client $client, ClientJointDetail $jointDetail = null)
    {
        if ($kyc->slug === 'id_or_passport') {
            $id_number = isset($data['number']) ?
                isset($data['number']) : ($client ? $client->id_or_passport : $jointDetail->id_or_passport);

            $this->createIPRSRequest($client, $id_number, $jointDetail);
        }

        $this->save($data, $kyc, $client, $jointDetail);
    }

    public function save($data, ClientComplianceChecklist $kyc, Client $client, ClientJointDetail $jointDetail = null)
    {
        $upload = (new ClientUploadedKyc());
        $upload->kyc_id = $kyc->id;
        $upload->client_id = $jointDetail ? null : $client->id;
        $upload->client_joint_id = $jointDetail ? $jointDetail->id : null;
        $upload->user_id = Auth::user()->id;
        $upload->document_id = isset($data['document_id']) ? $data['document_id'] : null;
        $upload->filename = isset($data['filename']) ? $data['filename'] : '';
        $upload->number = isset($data['number'])
            ? $data['number']
            : ($kyc->slug === 'id_or_passport'
                ? ($client ? $client->id_or_passport : $jointDetail->id_or_passport)
                : ($client ? $client->pin_no : $jointDetail->id_or_passport)
            );

        $upload->comment = isset($data['comment']) ? $data['comment'] : '';

        $upload->save();
    }

    public function updateKyc($data, ClientUploadedKyc $kyc, Client $client = null, ClientJointDetail $jointDetail = null)
    {
        $kyc->update([
            'document_id' => isset($data['document_id']) ? $data['document_id'] : null,
            'filename' => isset($data['filename']) ? $data['filename'] : '',
            'user_id' => Auth::user()->id,
            'number' => isset($input['number'])
                ? $data['number']
                : $kyc->type->slug === 'id_or_passport'
                    ? $client ? $client->id_or_passport : $jointDetail->id_or_passport
                    : $client ? $client->pin_no : $jointDetail->id_or_passport,
            'comment' => isset($data['comment']) ? $data['comment'] : ''
        ]);
    }

    public function createIPRSRequest(Client $client, $id, ClientJointDetail $joint = null)
    {
        $exists = IprsRequest::where('client_id', $client->id)
            ->where('id_number', $id)
            ->whereNull('status')
            ->exists();

        if ($exists) {
            return;
        }

        $iprs = new Iprs();
        $iprs->createRequest($client, $id, $joint);
    }
}
