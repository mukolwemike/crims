<?php
/**
 * Created by PhpStorm.
 * User: mchaka
 * Date: 08/11/2018
 * Time: 11:53
 */

namespace App\Cytonn\Investment\ClientInstruction\ExpressProcess;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Engine\Approval;
use Cytonn\Clients\Approvals\Handlers\Withdrawal;
use Cytonn\Investment\Action\Withdraw;
use Cytonn\Models\ClientInvestmentWithdrawal;

class ClientInvestmentInstructionProcessor
{
    protected $instruction;

    private static $user;

    /**
     * ClientInvestmentInstructionProcessor constructor.
     * @param $instruction
     */
    public function __construct(ClientInvestmentInstruction $instruction)
    {
        $this->instruction = $instruction;
    }

    /**
     * @return BankInstruction
     */
    public function process($wrap_in_trans = true)
    {
        $action = function () {
            $approval = $this->saveForApproval($this->instruction, $this->systemUser());

            $result =  $this->performWithdrawal($approval);

            $this->finishApproval($approval);

            return $result;
        };

        if (!$wrap_in_trans) {
            return call_user_func($action);
        }

        return \DB::transaction($action);
    }

    private function saveForApproval(ClientInvestmentInstruction $instruction, User $user)
    {
        $investment = $instruction->investment;

        $data = [
            'deduct_penalty' => 1,
            'description' => 'express client withdrawal',
            'narration' => 'express client withdrawal'
        ];

        $approval = Withdrawal::create($investment->client, $investment, $user, $data);

        $instruction->update(['approval_id' => $approval->id]);

        return $approval;
    }

    /**
     * @param ClientTransactionApproval $approval
     * @return BankInstruction
     * @throws \Exception
     */
    private function performWithdrawal(ClientTransactionApproval $approval)
    {
        $handler = $approval->handler();

        $action_date = $handler->getActionDate($approval, $approval->investment);

        if ($action_date->isFuture()) {
            throw new \LogicException("Cannot process scheduled withdrawal");
        }

        $withdrawal = $handler->handle($approval);

        if (!$withdrawal instanceof ClientInvestmentWithdrawal) {
            throw new \LogicException("Withdrawal failed");
        }

        $instr = $this->generateInstruction($withdrawal);

        $payment = $instr->custodialTransaction->clientPayment;

        if ($payment->date->gt(Carbon::today()->endOfDay())) {
            throw new \LogicException("Cannot process payment dated in future");
        }

        return $instr;
    }

    /**
     * @param ClientTransactionApproval $approval
     * @throws \App\Exceptions\CrimsException
     */
    private function finishApproval(ClientTransactionApproval $approval)
    {
        $approve = new Approval($approval);

        $approve->forceApprove($this->systemUser());
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @return BankInstruction
     * @throws \Exception
     */
    private function generateInstruction(ClientInvestmentWithdrawal $withdrawal)
    {
        $w = new Withdraw();

        return $w->generateInstruction($withdrawal, Carbon::today())->getInstruction();
    }

    private function systemUser()
    {
        if (!static::$user) {
            static::$user = getSystemUser();
        }

        return static::$user;
    }
}
