<?php
/**
 * Date: 21/02/2018
 * Time: 18:37
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\ClientInstruction;

use App\Cytonn\Models\ClientTopupForm;
use Carbon\Carbon;

class ClientTopupFormRepository
{
    protected $form;

    /**
     * ClientTopupFormRepository constructor.
     *
     * @param $form
     */
    public function __construct(ClientTopupForm $form)
    {
        $this->form = $form;
    }

    public function valueDate()
    {
        if (($approval = $this->form->approval) && isset($approval->payload['invested_date'])) {
            return Carbon::parse($approval->payload['invested_date']);
        }

        if (isNotEmptyOrNull($this->form->invested_date)) {
            return Carbon::parse($this->form->invested_date);
        }

        if ($this->form->investmentSchedule) {
            return Carbon::parse($this->form->investmentSchedule->date);
        }

        return Carbon::today();
    }

    public function maturityDate()
    {
        if (($approval = $this->form->approval) && isset($approval->payload['maturity_date'])) {
            return Carbon::parse($approval->payload['maturity_date']);
        }

        if ($this->form->investmentSchedule) {
            return Carbon::parse($this->form->investmentSchedule->parentInvestment->maturity_date);
        }

        $duration = ((int)($this->form->tenor/12) * 365) + (30*($this->form->tenor %12));

        $maturity = $this->valueDate()->addDays($duration);

        if (!$maturity->isMonday()) {
            $maturity = $maturity->addWeek()->startOfWeek();
        }

        return $maturity;
    }

    public function approvedByClient()
    {
        $mandate = $this->form->client->present()->getSigningMandate;

        $approvals = $this->form->clientApprovals();

        if (!$approvals->count()) {
            return true;
        }

        $approved = $this->form->clientApprovals()->approved(true)->get();

        switch ($mandate) {
            case 'Singly':
                return true;

            case 'All to sign':
                return $approvals->count() == $approved->count();

            case 'Either to sign':
                return $approved->count() ? true : false;

            case 'At Least Two to sign':
                return $approved->count() > 2 ? true : false ;

            default:
                throw new \InvalidArgumentException("Mandate type not supported");
        }
    }
}
