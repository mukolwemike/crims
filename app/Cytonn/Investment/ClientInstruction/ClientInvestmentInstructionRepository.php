<?php

namespace Cytonn\Investment\ClientInstruction;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;

class ClientInvestmentInstructionRepository
{

    /**
     * @var ClientInvestmentInstruction
     */
    protected $instruction;

    /**
     * @var ClientInvestment
     */
    protected $investment;

    /**
     * ClientInvestmentInstructionRepository constructor.
     *
     * @param $instruction
     */
    public function __construct(ClientInvestmentInstruction $instruction = null)
    {
        is_null($instruction)
            ? $this->instruction = new ClientInvestmentInstruction() : $this->instruction = $instruction;

        $this->getInvestment();
    }

    public function getInvestment()
    {
        if (is_null($this->investment)) {
            $investment = ClientInvestment::find($this->instruction->investment_id);

            $this->investment = $investment;

            return $investment;
        }

        return $this->investment;
    }

    /**
     * @return mixed
     */
    public function calculateAmountAffected()
    {
        $this->getInvestment();

        //TODO validate due date
        $due_date = $this->instruction->due_date;

        if ($this->instruction->withdrawal_stage == 'mature') {
            $due_date = $this->investment->maturity_date;
        }

        $select = $this->instruction->amount_select;

        $investments = $this->getAllInvestments();

        switch ($select) {
            case 'amount':
                return $this->instruction->amount;
            case 'principal':
                $principal = $investments->sum(function ($i) use ($due_date) {
                    return $i->fresh()->calculate($due_date, false)->principal();
                });

                return round($principal);
            case 'principal_interest':
                return $this->instruction->topup_amount +
                    $investments->sum(function ($i) use ($due_date) {
                        return $i->fresh()->calculate($due_date, false)->value();
                    });
            case 'interest':
                return $investments->sum(function ($i) use ($due_date) {
                    return $i->fresh()->calculate($due_date, true)->netInterest();
                });
            default:
                throw new ClientInvestmentException("An amount must be specified");
        }
    }

    /**
     * @return string
     */
    public function showAmountSelect()
    {
        $this->getInvestment();

        $select = $this->instruction->amount_select;

        switch ($select) {
            case 'principal_interest':
                return 'Principal + Interest';
                break;
            case 'principal':
                return 'Principal';
            case 'interest':
                return 'Interest';
            default:
                return 'Amount';
        }
    }

    public function amountWithdrawn()
    {
        if ($this->instruction->type->name == 'rollover') {
            throw new \Exception('Rollovers not supported');
        }

        return $this->calculateAmountAffected();
    }

    public function valueDate()
    {
        if (($approval = $this->instruction->approval) && isset($approval->payload['invested_date'])) {
            return Carbon::parse($approval->payload['invested_date']);
        }

        $date = Carbon::parse($this->instruction->due_date)->addDay();

        if ($date->isWeekend()) {
            return $date->addWeek()->startOfWeek();
        }

        return $date;
    }

    public function maturityDate()
    {
        if (($approval = $this->instruction->approval) && isset($approval->payload['maturity_date'])) {
            return Carbon::parse($approval->payload['maturity_date']);
        }

        $value = $this->valueDate();

        $maturity = $value->copy()->addMonthsNoOverflow($this->instruction->tenor);

        if ($maturity->isMonday()) {
            return $maturity;
        }

        return $maturity->addWeek()->startOfWeek();
    }

    public function scheduledApproval()
    {
        return $this->instruction->approval()->scheduled()->latest()->first();
    }


    public function approvedByClient()
    {
        $mandate = $this->instruction->investment->client->present()->getSigningMandate;

        $approvals = $this->instruction->clientApprovals();

        if (!$approvals->count()) {
            return true;
        }

        $approved = $this->instruction->clientApprovals()->approved(true)->get();

        switch ($mandate) {
            case 'Singly':
                return true;

            case 'All to sign':
                return $approvals->get()->count() == $approved->count();

            case 'Either to sign':
                return $approved ? true : false;

            case 'At Least Two to sign':
                return $approved > 2 ? true : false ;

            default:
                throw new \InvalidArgumentException("Mandate type not supported");
        }
    }

    /**
     * @return mixed
     */
    public function getSeipInvestmentCollection()
    {
        $this->getInvestment();

        $investments = $this->investment->childInvestments;

        return $investments->merge(collect([$this->investment]));
    }

    /**
     * @return \Illuminate\Support\Collection|mixed
     */
    public function getAllInvestments()
    {
        $this->getInvestment();

        if ($this->investment->repo->isSeipParent()) {
            return $this->getSeipInvestmentCollection();
        }

        return $this->instruction->allInvestments();
    }
}
