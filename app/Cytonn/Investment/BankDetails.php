<?php
/**
 * Date: 24/06/2016
 * Time: 9:09 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientInvestment;

/**
 * Class BankDetails
 *
 * @package Cytonn\Investment
 */
class BankDetails
{
    /**
     * @var ClientInvestment
     */
    protected $investment;

    /**
     * @var
     */
    protected $account;

    /**
     * BankDetails constructor.
     *
     * @param ClientInvestment       $investment
     * @param Client|null            $client
     * @param ClientBankAccount|null $account
     */
    public function __construct(
        ClientInvestment $investment = null,
        Client $client = null,
        ClientBankAccount $account = null
    ) {
        $this->investment = $investment;

        if ($account) {
            $this->account = $account;
        } else {
            if ($investment) {
                $this->setupUsingInvestment($investment);
            }

            if ($client) {
                $this->setupUsingClient($client);
            }
        }
    }

    private function setupUsingClient($client)
    {
        $this->account =  $client->bankAccounts()->where('default', true)->first();

        if (!$this->account) {
            $this->account = $client->bankAccounts()->first();
        }
    }

    private function setupUsingInvestment($investment)
    {
        if ($this->investment->bankAccount) {
            $this->account = $this->investment->bankAccount;
            return;
        }

        $this->setupUsingClient($investment->client);
    }

    public function id()
    {
        if (is_null($this->account)) {
            return null;
        }

        return $this->account->id;
    }


    /**
     * @return mixed
     */
    public function bankName()
    {
        $bank = $this->hasBank();

        if ($bank) {
            return $bank->name;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function branch()
    {
        $branch = $this->hasBranch();

        if ($branch) {
            return $branch->name;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function accountNumber()
    {
        if (is_null($this->account)) {
            return null;
        }

        return $this->account->account_number;
    }

    /**
     * @return mixed
     */
    public function accountName()
    {
        if (is_null($this->account)) {
            return null;
        }

        return $this->account->account_name;
    }

    /**
     * @return mixed
     */
    public function clearingCode()
    {
        if ($bank = $this->hasBank()) {
            return $bank->clearing_code;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function swiftCode()
    {
        if ($bank = $this->hasBank()) {
            return $bank->swift_code;
        }

        return null;
    }

    public function branchCode()
    {
        if ($branch = $this->hasBranch()) {
            return $branch->branch_code;
        }

        return null;
    }

    private function hasBank()
    {
        if (is_null($this->account)) {
            return null;
        }

        if (is_null($this->account->branch)) {
            return null;
        }

        return $this->account->branch->bank;
    }

    private function hasBranch()
    {
        if (is_null($this->account)) {
            return null;
        }

        if (is_null($this->account->branch)) {
            return null;
        }

        return $this->account->branch;
    }

    public function paymentType()
    {
        $bank = $this->account->branch->bank;

        $type = $bank->transfer_type;

        if (!(is_null($type) || empty(trim($type)))) {
            return $type;
        }

        return 'bank';
    }

    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }

        throw new \InvalidArgumentException("The method/property $property does not exist");
    }
}
