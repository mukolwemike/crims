<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Investment\Seip;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Exceptions\CrimsException;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Seip\ScheduleGenerator;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;
use Cytonn\Models\Investment\InvestmentPaymentScheduleDetail;

class SeipInvestment
{
    /**
     * @param ClientInvestment $investment
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    public function processTopup(ClientInvestment $investment, ClientTransactionApproval $approval)
    {
        $form = $approval->topupForm;

        if (is_null($form->investmentSchedule)) {
            $this->processParentInvestmentTopup($investment, $approval);
        } else {
            $this->processChildInvestmentTopup($investment, $approval);
        }
    }

    /**
     * @param ClientInvestment $investment
     * @param ClientTransactionApproval $approval
     */
    public function processRollover(ClientInvestment $investment, ClientTransactionApproval $approval)
    {
        $this->processParentInvestmentTopup($investment, $approval);
    }

    /**
     * @param ClientInvestment $investment
     * @param ClientTransactionApproval $approval
     */
    private function processParentInvestmentTopup(ClientInvestment $investment, ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $this->storePaymentScheduleDetails($investment, $data);

        $this->generateSchedules($investment);
    }

    /**
     * @param ClientInvestment $investment
     */
    public function regenerateSchedules(ClientInvestment $investment)
    {
        $investment->childInvestmentPaymentSchedules()->forceDelete();

        $this->generateSchedules($investment);
    }

    /**
     * @param ClientInvestment $investment
     */
    public function generateSchedules(ClientInvestment $investment)
    {
        $generator = new ScheduleGenerator($investment->fresh());

        $generator->generate();
    }

    /**
     * @param ClientInvestment $investment
     * @param $data
     * @return InvestmentPaymentScheduleDetail|\Illuminate\Database\Eloquent\Model
     */
    private function storePaymentScheduleDetails(ClientInvestment $investment, $data)
    {
        return InvestmentPaymentScheduleDetail::create([
            'investment_id' => $investment->id,
            'payment_date' => $data['contribution_payment_date'],
            'payment_interval' => $data['contribution_payment_interval'],
            'amount' => $data['contribution_amount']
        ]);
    }

    /**
     * @param ClientInvestment $investment
     * @param ClientTransactionApproval $approval
     * @throws ClientInvestmentException
     */
    private function processChildInvestmentTopup(ClientInvestment $investment, ClientTransactionApproval $approval)
    {
        $investmentSchedule = $approval->topupForm->investmentSchedule;

        $investmentSchedule->update([
            'investment_id' => $investment->id,
            'paid' => 1,
            'date_paid' => $investment->invested_date
        ]);

        $investment->update([
            'parent_id' => $investmentSchedule->parent_investment_id
        ]);

        $this->validateChildInvestment($investment, $investmentSchedule);
    }

    /**
     * @param ClientInvestment $investment
     * @param InvestmentPaymentSchedule $paymentSchedule
     * @throws ClientInvestmentException
     */
    private function validateChildInvestment(ClientInvestment $investment, InvestmentPaymentSchedule $paymentSchedule)
    {
//        if (Carbon::parse($investment->invested_date) != Carbon::parse($paymentSchedule->date)) {
//            throw new ClientInvestmentException("The investment invested date does not match
//            with the expected schedule date of " . Carbon::parse($paymentSchedule->date)->toDateString());
//        }

//        if ($investment->amount < $paymentSchedule->amount) {
//            throw new ClientInvestmentException("The investment amount is less than the set scheduled amount
//            of " . AmountPresenter::currency($paymentSchedule->amount));
//        }
    }

    /**
     * @param ClientInvestment $investment
     * @param ClientInvestment $oldInvestment
     */
    public function extendSchedules(ClientInvestment $investment, ClientInvestment $oldInvestment)
    {
        $generator = new ScheduleGenerator($investment->fresh());

        $generator->extendSchedules(Carbon::parse($oldInvestment->maturity_date));
    }
}
