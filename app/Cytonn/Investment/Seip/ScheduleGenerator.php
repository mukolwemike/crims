<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\Seip;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Models\Investment\InvestmentPaymentSchedule;

class ScheduleGenerator
{
    private $investment;

    /**
     * ScheduleGenerator constructor.
     * @param ClientInvestment $investment
     */
    public function __construct(ClientInvestment $investment)
    {
        $this->investment = $investment;
    }

    public function generate()
    {
        $scheduleDetail = $this->investment->investmentPaymentScheduleDetail;

        $investedDate = Carbon::parse($this->investment->invested_date);

        $maturityDate = Carbon::parse($this->investment->maturity_date);

        $paymentDate = $this->getPaymentDate(
            $investedDate,
            $scheduleDetail->payment_date,
            $scheduleDetail->payment_interval
        );

        $schedules = [];

        while ($paymentDate <= $maturityDate) {
            $schedules[] = [
                'parent_investment_id' => $this->investment->id,
                'amount' => $scheduleDetail->amount,
                'date' => $paymentDate,
                'interest_rate' => $this->investment->interest_rate,
                'description' => $paymentDate->format('F Y') . " contribution"
            ];

            $paymentDate = $this->getPaymentDate(
                $paymentDate,
                $scheduleDetail->payment_date,
                $scheduleDetail->payment_interval
            );
        }

        $this->investment->childInvestmentPaymentSchedules()->createMany($schedules);
    }

    /**
     * @param $startDate
     * @param $paymentDay
     * @param $interval
     * @return mixed
     */
    private function getPaymentDate($startDate, $paymentDay, $interval)
    {
        $payDate = $startDate->copy()->addMonthsNoOverflow($interval);

        $endMonth = $payDate->copy()->endOfMonth()->startOfDay();

        $payDate = $payDate->startOfMonth()->day($paymentDay);

        return $payDate > $endMonth ? $endMonth : $payDate;
    }

    /**
     * @param ClientInvestment $investment
     * @param ClientInvestment $oldInvestment
     */
    public function extendSchedules(Carbon $oldMaturityDate)
    {
        $schedules = $this->investment->childInvestmentPaymentSchedules()->paid(false)
            ->get();

        $newMaturityDate = Carbon::parse($this->investment->maturity_date);

        $addedMonths = $newMaturityDate->diffInMonths($oldMaturityDate);

        $schedules->each(function (InvestmentPaymentSchedule $schedule) use ($addedMonths) {
            $newDate = Carbon::parse($schedule->date)->addMonthsNoOverflow($addedMonths);

            $schedule->update([
                'date' => $newDate,
                'description' => $newDate->format('F Y') . " contribution"
            ]);
        });
    }
}
