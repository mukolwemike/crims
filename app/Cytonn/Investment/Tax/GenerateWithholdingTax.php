<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 10/4/18
 * Time: 5:03 PM
 */

namespace App\Cytonn\Investment\Tax;

use App\Cytonn\Api\Transformers\Portfolio\WithholdingTaxTransformer;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\User;
use App\Cytonn\Models\WithholdingTax;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;

class GenerateWithholdingTax
{
    protected $user;

    protected $start;

    protected $end;

    protected $product;

    protected $ids;

    protected $unitFund;

    public function __construct(
        User $user = null,
        Carbon $start = null,
        Carbon $end = null,
        Product $product = null,
        $ids = null,
        UnitFund $unitFund = null
    ) {
        $this->user = $user;

        $this->start = $start;

        $this->end = $end;

        $this->product = $product;

        $this->unitFund = $unitFund;

        $this->ids = $ids;
    }

    /**
     * @return WithholdingTax[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function generate()
    {
        if ($this->ids) {
            $taxes = WithholdingTax::whereIn('id', $this->ids);
        } elseif ($this->unitFund) {
            $taxes = WithholdingTax::whereHas('unitFundSale', function ($q) {
                $q->where('unit_fund_id', $this->unitFund->id);
            })->whereBetween('date', [$this->start->startOfDay(), $this->end->startOfDay()]);
        } else {
            $taxes = WithholdingTax::whereHas('withdrawal', function ($withdrawal) {
                $withdrawal->whereHas('investment', function ($investment) {
                    if ($this->product) {
                        $investment->where('product_id', $this->product->id);
                    }
                });
            })->whereBetween('date', [$this->start->startOfDay(), $this->end->startOfDay()]);
        }

        return $taxes->orderBy('date')->get()->map(function ($withholding) {
            return (new WithholdingTaxTransformer())->details($withholding);
        });
    }

    public function send()
    {
        $dataArray = $this->generate();

        $fileName = 'Withholding tax status report';

        ExcelWork::generateAndStoreSingleSheet($dataArray, $fileName);

        if ($this->user) {
            $email = $this->user ? $this->user->email : [];
        } else {
            $email = [];
        }

        Mailer::compose()
            ->to($email)
            ->bcc(config('system.administrators'))
            ->subject($fileName)
            ->text('Please find the attached w/tax status report')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';
    }
}
