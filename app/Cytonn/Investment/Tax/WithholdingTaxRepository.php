<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 9/19/18
 * Time: 3:44 PM
 */

namespace Cytonn\Investment\Tax;

use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\Models\WithholdingTax;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Models\ClientInvestmentWithdrawal;

class WithholdingTaxRepository
{
    protected $investment;

    protected $topup;

    protected $withdrawal;

    protected $sale;

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @return WithholdingTax|\Illuminate\Database\Eloquent\Model|void
     */
    public function recordForWithdrawal(ClientInvestmentWithdrawal $withdrawal)
    {
        $this->withdrawal = $withdrawal;

        $this->investment = $this->withdrawal->investment;

        if ($this->withdrawal->amount == 0) {
            return;
        }

        $action = $this->action();

        if (!$action || $action->wtax == 0) {
            return;
        }

        $description = 'Withholding tax from a ' . $action->type . ' of amount ' .
            $this->investment->product->currency->code . ' ' . number_format($this->withdrawal->amount);

        return WithholdingTax::create([
            'client_withdrawal_id' => $this->withdrawal->id,
            'amount' => $action->wtax,
            'gross_interest_amount' => $action->gross_interest_cumulative,
            'net_interest_amount' => $action->net_interest_cumulative,
            'type' => $action->type,
            'date' => $action->action->date,
            'description' => $description
        ]);
    }

    /**
     * @return mixed
     */
    public function action()
    {
        $date = ($this->topup) ? Carbon::parse($this->topup->date) : Carbon::parse($this->withdrawal->date);

        $actions = collect($this->investment->fresh()->calculate(
            $date,
            false,
            null,
            false,
            true
        )
            ->getPrepared()
            ->actions);

        $action = $actions->filter(function ($action) use ($actions) {

            return ($action->action->id == $this->withdrawal->id);
        })->first();

        if ($action && $action->wtax == 0) {
            return $actions->where('wtax', '>', 0)->where('date', $action->date)->first();
        }

        return $action;
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     */
    public function reverseForWithdrawal(ClientInvestmentWithdrawal $withdrawal)
    {
        $recordedTax = $withdrawal->withholdingTax->first();

        if (!$recordedTax) {
            return;
        }

        if ($recordedTax->paid) {
            return $recordedTax->custodialTransaction->delete();
        }

        $recordedTax->delete();
    }

    /**
     * @param UnitFundSale $sale
     */
    public function recordForUnitSale(UnitFundSale $sale)
    {
        $this->sale = $sale;

        $action = $this->getActionForSale();

        if (!$action || !isset($action->interest_data) || round($action->interest_data->withdrawn_wht, 2) == 0) {
            return;
        }

        $saleIds = $action->sales->map(function ($sale) {
            return $sale->id;
        })->toArray();

        WithholdingTax::whereIn('unit_fund_sale_id', $saleIds)->paid(false)->delete();

        $paidGross = $paidNet = $paidWht = 0;

        $paidTaxes = WithholdingTax::whereIn('unit_fund_sale_id', $saleIds)->paid()->get();

        foreach ($paidTaxes as $paidTax) {
            $paidGross += $paidTax->gross_interest_amount;
            $paidNet += $paidTax->net_interest_amount;
            $paidWht += $paidTax->amount;
        }

        $currentWht = $action->interest_data->withdrawn_wht - $paidWht;

        if ($currentWht < 0) {
            //What happens when already we have overpaid due to backdating
            return;
        }

        $description = "Withholding tax from a Unit Sale of amount ". $this->sale->unitFund->currency->code ." "
            . number_format($this->sale->price * $this->sale->number);

        return WithholdingTax::create([
            'unit_fund_sale_id' => $this->sale->id,
            'amount' => $currentWht,
            'gross_interest_amount' => $action->interest_data->withdrawn_adjusted_gross_interest - $paidGross,
            'net_interest_amount' => $action->interest_data->withdrawn_adjusted_gross_interest - $action->interest_data->withdrawn_wht - $paidNet,
            'type' => 'unit_sale',
            'date' => $action->date,
            'description' => $description
        ]);
    }

    /**
     * @return mixed
     */
    private function getActionForSale()
    {
        $client = $this->sale->client;

        $date = Carbon::parse($this->sale->date);

        $calculator = $client->calculateFund($this->sale->unitFund, $date);

        return $calculator->actions()->filter(function ($action) {
            if (count($action->sales) == 0) {
                return false;
            }

            $sales = $action->sales;

            foreach ($sales as $sale) {
                if ($sale->id == $this->sale->id) {
                    return true;
                }
            }

            return false;
        })->first();
    }

    /**
     * @param UnitFundSale $sale
     */
    public function reverseForUnitSale(UnitFundSale $sale)
    {
        $this->sale = $sale;

        $this->sale->withholdingTaxes->each(function ($tax) {
            if ($tax->custodialTransaction) {
                $tax->custodialTransaction->delete();
            }

            $tax->delete();
        });

        $sameDaySale = UnitFundSale::where('client_id', $this->sale->client_id)
            ->where('unit_fund_id', $this->sale->unit_fund_id)
            ->where('date', $this->sale->date)
            ->first();

        if ($sameDaySale) {
            $this->recordForUnitSale($sameDaySale);
        }
    }
}
