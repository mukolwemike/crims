<?php

namespace Cytonn\Investment\Rules;

use Cytonn\Rules\Rules;

/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 2/14/18
 * Time: 9:05 AM
 */
trait InvestmentApplicationRules
{
    use Rules;

    protected $commonRules = [
        'email' => 'required|email',
        'method_of_contact_id' => 'required',
//        'pin_no'=>'required',
        'investor_account_name' => 'required',
        'investor_account_number' => 'required',
        'investor_bank' => 'required',
        'investor_bank_branch' => 'required'
    ];

    public function riskAssesment($request)
    {
        $rules = [
            'risk_duration' => 'required',
            'risk_inv_types' => 'required',
            'risk_inv_volatility' => 'required',
            'risk_inv_objective' => 'required',
            'risk_trade_frequency' => 'required',
            'risk_knowledge' => 'required',
            'risk_profile' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    private function shortApplicationForm()
    {
        return [
            'amount' => 'required|numeric',
            'tenor' => 'required_if:product_category, "products"',
            'product_id' => 'required_if:product_category, "products"',
            'unit_fund_id' => 'required_if:product_category, "funds"'
        ];
    }

    public function shortIndividualApplicationForm($request)
    {
        $rules = $this->shortApplicationForm() + [
                'firstname' => 'required',
                'lastname' => 'required',
                'title' => 'required',
                'risk_reason' => 'required_if:risky,"true"'
            ];

        return $this->verdict($request, $rules);
    }

    public function shortCorporateApplicationForm($request)
    {
        $rules = $this->shortApplicationForm() + [
                'registered_name' => 'required'
            ];

        return $this->verdict($request, $rules);
    }

    public function shortReserveUnitApplicationForm($request)
    {
        $rules = [];

        return $this->verdict($request, $rules);
    }

    public function shortExistingApplicationForm($request)
    {
        $rules = $this->shortApplicationForm() + [
                'client_id' => 'required|numeric',
            ];

        return $this->verdict($request, $rules);
    }

    public function individualApplicationForm($request)
    {
        $rules = $this->commonRules + [
                'telephone_home' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required'
            ];

        return $this->check($request, $rules);
    }

    public function corporateApplicationForm($request)
    {
        $rules = $this->commonRules + [
                'registered_name' => 'required',
                'telephone_office' => 'required'
            ];

        return $this->check($request, $rules);
    }

    public function investmentDetails($request)
    {
        $rules = [
            'amount' => 'required|numeric',
            'tenor' => 'required_if:product_category, "products"|numeric',
            'product_id' => 'required_if:product_category, "products"|exists:products,id',
            'unit_fund_id' => 'required_if:product_category, "funds"|exists:unit_funds,id'
        ];

        return $this->verdict($request, $rules);
    }

    public function shareholderAccountDetails($request)
    {
        $rules = [
            'entity_id' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    public function individualSubscriber($request)
    {
        $rules = $this->commonRules + [
                'telephone_home' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required'
            ];

        return $this->verdict($request, $rules);
    }

    public function corporateSubscriber($request)
    {
        $rules = $this->commonRules + [
                'registered_name' => 'required',
                'street' => 'required',
                'telephone_office' => 'required'
            ];

        return $this->verdict($request, $rules);
    }

    public function existingSubscriber($request)
    {
        $rules = [
            'client_id' => 'required|numeric',
            'funds_source_id' => 'required',
            'amount' => 'required|numeric',
            'tenor' => 'required|numeric',
            'product_id' => 'required|exists:products,id'
        ];

        return $this->verdict($request, $rules);
    }

    protected $jointRules = [
        'title_id' => 'required',
        'firstname' => 'required',
        'lastname' => 'required',
        'pin_no' => 'required',
        'id_or_passport' => 'required',
        'email' => 'required|email',
        'telephone_cell' => 'required',
    ];

    public function jointNewHolder($request)
    {
        $rules = $this->jointRules;

        return $this->verdict($request, $rules);
    }

    public function jointExistingHolder($request)
    {
        $rules = [
            'client_id' => 'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    public function jointDuplicateHolder($request)
    {
        $rules = $this->jointRules + [
                'duplicate_reason' => 'required',
            ];

        return $this->verdict($request, $rules);
    }

    public function contactPersons($request)
    {
        $input = $request->all();

        $other = !isset($input['email'])
            ? ['phone' => 'required']
            : ['email' => 'required'];

        $rules = $other + [
                'name' => 'required',
            ];

        return $this->verdict($request, $rules);
    }

    public function validateApplication($request)
    {
        $rules = [
            'email' => 'required|email',
            'method_of_contact_id' => 'required',
            'investor_account_name' => 'required',
            'investor_account_number' => 'required',
            'investor_bank' => 'required',
            'investor_bank_branch' => 'required',
            'amount' => 'required|numeric',
            'tenor' => 'required_if:product_category, "products"',
            'product_id' => 'required_if:product_category, "products"',
            'unit_fund_id' => 'required_if:product_category, "funds"|exists:unit_funds,id'
        ];

        return $this->verdict($request, $rules);
    }

    public function shortIndividualDuplicateApplicationForm($request)
    {
        $rules = $this->shortApplicationForm() + [
                'firstname' => 'required',
                'lastname' => 'required',
                'title' => 'required',
                'duplicate_reason' => 'required',
            ];

        return $this->verdict($request, $rules);
    }

    public function shortCorporateDuplicateApplicationForm($request)
    {
        $rules = $this->shortApplicationForm() + [
                'registered_name' => 'required',
                'duplicate_reason' => 'required'
            ];

        return $this->verdict($request, $rules);
    }
}
