<?php

namespace Cytonn\Investment\Rules;

use Cytonn\Rules\Rules;

/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 2/14/18
 * Time: 9:05 AM
 */

trait InvestmentInstructionRules
{
    use Rules;

    public function topupValidate($request)
    {
        $rules = [
            'amount'=>'required|numeric',
            'product_id'=>'required',
            'tenor'=>'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }

    public function withdrawValidate($request)
    {
        $rules = [
            'amount_select' => 'required_if:withdraw_type,withdraw',
            'amount'=>'required_if:withdraw_type,deduction|numeric',
            'withdrawal_stage'=>'required',
        ];

        return $this->verdict($request, $rules);
    }

    public function editWithdrawValidate($request)
    {
        $rules = [
            'amount_select' => 'required_if:withdraw_type,withdraw',
            'amount'=>'required_if:withdraw_type,deduction',
            'withdrawal_stage'=>'required',
        ];

        return $this->verdict($request, $rules);
    }

    public function rolloverValidate($request)
    {
        $rules = [
//            'agreed_rate'=>'numeric',
            'amount_select'=>'required',
            'amount'=>'required_if:amount_select,amount|numeric',
            'tenor'=>'required|numeric'
        ];

        return $this->verdict($request, $rules);
    }

    public function rolloverUpdateValidate($request)
    {
        $rules = [
            'amount'=>'required|numeric',
            'tenor'=>'required|numeric',
        ];

        return $this->verdict($request, $rules);
    }
}
