<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Investment\CommissionRecepientUserPositions;

use Laracasts\Presenter\Presenter;

class CommissionRecepientUserPositionPresenter extends Presenter
{
    /**
     * @return mixed
     */
    public function getDepartmentUnit()
    {
        return $this->departmentUnit ? $this->departmentUnit->name : $this->position->name;
    }

    /**
     * @return mixed
     */
    public function getDepartmentBranch()
    {
        return $this->departmentUnit ? $this->departmentUnit->present()->getBranch : $this->position->name;
    }
}
