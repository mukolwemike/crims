<?php
/**
 * Date: 02/11/2015
 * Time: 4:58 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTax;
use Carbon\Carbon;

/**
 * Class TaxRepository
 *
 * @package Cytonn\Investment
 */
class TaxRepository
{
    /**
     * @var Client
     */
    public $client;

    /**
     * TaxRepository constructor.
     *
     * @param $client
     */
    public function __construct(Client $client = null)
    {
        is_null($client) ? $this->client = new Client() : $this->client = $client;
    }

    /**
     * @param $product
     * @param null $date
     * @return number
     */
    public function getTotalGrossInterestForProductForMonth($product, $date = null)
    {
        $investments = $this->client->repo->getInvestmentsForProductForMonth($product, $date);

        $interest_array = [];

        foreach ($investments as $investment) {
            $firstDayOfMonth = Carbon::now()->startOfMonth()->toDateString();
            $endDate = $firstDayOfMonth;
            if ($investment->withdrawn) {
                $maturity_date = new Carbon($investment->maturity_date);
                $withdraw_date = new Carbon($investment->withdrawal_date);
                $maturity_date->gt($withdraw_date) ? $endDate = $withdraw_date : $endDate = $maturity_date;
            } else {
                $endDate = Carbon::today()->toDateString();
            }

            $grossInterest = $investment->repo
                ->getCurrentGrossInterest($investment->amount, $investment->interest_rate, $firstDayOfMonth, $endDate);
            array_push($interest_array, $grossInterest);
        }

        return array_sum($interest_array);
    }

    /**
     * @param $product
     * @param null $date
     * @return float
     */
    public function getTotalTaxForProductForMonth($product, $date = null)
    {
        $grossInterest = $this->getTotalGrossInterestForProductForMonth($product, $date);

        return (new InvestmentsRepository())->getWithHoldingTax($grossInterest);
    }

    /**
     * @param $product
     * @param $date
     * @return float|int
     */
    public function getWeightedRateForProductForMonth($product, $date)
    {
        $grossInterest = $this->getTotalGrossInterestForProductForMonth($product, $date);

        $principal = $this->getTotalInvestedAmountForMonthForProduct($product, $date);

        $month_start = Carbon::now()->startOfMonth();

        $tenor = Carbon::now()->copy()->diffInDays($month_start);

        if ($principal != 0) {
            return $grossInterest * 100 * 365 / ($principal * $tenor);
        }

        return 0;
    }

    /**
     * @param $product
     * @param null $date
     * @return number
     */
    public function getTotalInvestedAmountForMonthForProduct($product, $date = null)
    {
        return $this->client->repo->getInvestmentsForProductForMonth($product, $date)->sum('amount');
    }

    /**
     * @param ClientInvestment $investment
     */
    public function recordWithholdingTaxForInvestment(ClientInvestment $investment)
    {
        $tax = new ClientTax();
        $tax->client_id = $investment->client_id;
        $tax->investment_id = $investment->id;
        $tax->date = $investment->withdrawal_date;
        $tax->amount = $investment->repo->getWithHoldingTax($investment->repo->getFinalGrossInterest());
        $tax->save();
    }
}
