<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ClientInvestments;

use Laracasts\Presenter\Presenter;

class ClientInvestmentPresenter extends Presenter
{
    /*
     * Get the name of the commision recipient for the investment
     */
    public function commissionRecipientName()
    {
        if ($this->commission) {
            return $this->commission->recipient->name;
        }

        return '';
    }

    /*
     * Get the interest payment details
     */
    public function getInterestPaymentDetails()
    {
        $interestPayment = [
            null => 'On Maturity', 0 => 'On Maturity', 1=>'Every 1 month', 2=>'Every 2 months',
            3=>'Every 3 months (Quarterly)', 4=>'Every 4 months',
            5=>'Every 5 months', 6=>'Every 6 months (Semi anually)', 7=>'Every 7 months',
            8=>'Every 8 months', 9=>'Every 9 months', 10=>'Every 10 months',
            11=>'Every 11 months', 12=>'Every 12 months (Annually)'];

        $paymentString = $interestPayment[$this->interest_payment_interval];

        if ($this->interest_payment_date) {
            $paymentString .= ' on date ' . $this->interest_payment_date;
        }

        return $paymentString;
    }

    /*
     * Get the interest action details
     */
    public function getInterestActionDetails()
    {
        if ($this->interestAction) {
            return $this->interestAction->name;
        }

        return 'No Action';
    }
}
