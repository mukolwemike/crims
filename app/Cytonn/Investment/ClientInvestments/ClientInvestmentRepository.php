<?php
/**
 * Created by PhpStorm.
 * User: yerick
 * Date: 29/10/2018
 * Time: 16:09
 */

namespace App\Cytonn\Investment\ClientInvestments;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Http\CrimsClient\Transformers\InvestmentTransformer;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Core\DataStructures\Carbon;

class ClientInvestmentRepository
{
    use AlternateSortFilterPaginateTrait;

    public function investments(Client $client, Product $product)
    {
        return $this->sortFilterPaginate(

            new ClientInvestment(),
            [],
            function ($purchase) {
                return app(InvestmentTransformer::class)->details($purchase);
            },
            function ($model) use ($client, $product) {

                return $model
                    ->where('client_id', $client->id)
                    ->where('product_id', $product->id)
                    ->where(
                        function ($q) {
                            $q->whereNull('withdrawn')->orWhere('withdrawn', 0);
                        }
                    )->orderBy('invested_date', 'DESC');
            }
        );
    }

    public function totals(Client $client, Product $product)
    {
        return $client->investments()->active()->forProduct($product)->get()
            ->reduce(function ($carry, ClientInvestment $investment) {
                $calc = $investment->calculate(Carbon::today(), true);

                $carry['original_principal'] += $investment->amount;
                $carry['principal'] += $calc->principal();
                $carry['net_interest'] += $calc->netInterest();
                $carry['net_interest_before_deductions'] += $calc->netInterestBeforeDeductions();
                $carry['value'] += $calc->total();

                return $carry;
            }, [
                'principal' => 0,
                'original_principal' => 0,
                'net_interest' => 0,
                'net_interest_before_deductions' => 0,
                'value' => 0
            ]);
    }
}
