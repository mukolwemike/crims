<?php namespace Cytonn\Investment;

use App\Cytonn\Models\ClientContactPerson;
use Laracasts\Commander\CommandHandler;

class ContactPersonAddCommandHandler implements CommandHandler
{

    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        $data = $command->data;

        $contact_person = new ClientContactPerson();
        if (isset($data['id'])) {
            is_null($data['id'])
                ? $contact_person = new ClientContactPerson()
                : $contact_person = ClientContactPerson::findOrFail($data['id']);
        }

        unset($data['id']);

        $contact_person->fill($data);

        $contact_person->save();
    }
}
