<?php

namespace Cytonn\Investment\Clawbacks\Generators;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionClawback;
use Carbon\Carbon;

class ClawbackStaggered
{
    protected $clawBack;

    protected $startDate;

    protected $commission;

    protected $duration;

    protected $eligibleClawbacks;

    public function __construct(
        Commission $commission,
        Carbon $startDate,
        $eligibleClawbacks,
        $duration = 1
    ) {
        $this->commission = $commission;
        $this->eligibleClawbacks = $eligibleClawbacks;
        $this->startDate = $startDate;
        $this->duration = $duration;
    }

    public function generate()
    {
        if (count($this->eligibleClawbacks) == 0) {
            return;
        }

        $clawBack = $this->eligibleClawbacks->first();

        $runningAmount = $this->eligibleClawbacks->sum('amount');

        $this->deleteClawbacks();

        $startDate = $this->startDate->copy();

        $index = 0;

        $staggerAmount = round($runningAmount / $this->duration, 2);

        while ($index < $this->duration) {
            (new CommissionClawback())->create([
                'commission_id' => $this->commission->id,
                'date' => $startDate->toDateString(),
                'amount' => ($index == $this->duration - 1) ? $runningAmount : $staggerAmount,
                'narration' => $clawBack->narration,
                'type' => $clawBack->type,
                'no_date' => $clawBack->no_date,
                'fully_paid' => $clawBack->fully_paid,
            ]);

            $runningAmount -= $staggerAmount;
            $startDate->addMonthNoOverflow();
            $index++;
        }
    }

    public function deleteClawbacks()
    {
        $this->eligibleClawbacks->each(
            function ($clawBack) {
                $clawBack->delete();
            }
        );
    }
}
