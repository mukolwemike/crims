<?php

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientInvestment;

class InterestHasBeenReinvested
{
    public $reinvestment;

    /**
     * InterestHasBeenReinvested constructor.
     *
     * @param $reinvestment
     */
    public function __construct(ClientInvestment $reinvestment)
    {
        $this->reinvestment = $reinvestment;
        $this->reinvestment->raise(new ClientInvestmentHasBeenAdded($reinvestment));
        $this->reinvestment->dispatchEventsFor($this->reinvestment);
    }
}
