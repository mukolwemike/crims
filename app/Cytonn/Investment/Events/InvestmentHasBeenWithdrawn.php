<?php
/**
 * Date: 9/11/15
 * Time: 11:34 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use Cytonn\Investment\Action\Withdraw;

class InvestmentHasBeenWithdrawn
{
    public $withdrawal;

    /**
     * InvestmentHasBeenWithdrawn constructor.
     *
     * @param $withdrawal
     */
    public function __construct(Withdraw $withdrawal)
    {
        $this->withdrawal = $withdrawal;

        if (is_null($withdrawal->client->withdrawal_instruction_id)) {
            $this->withdrawal->raise(
                new ClientPaymentIsDue(
                    $withdrawal->client->withdraw_amount,
                    'Withdrawal',
                    $withdrawal->client,
                    'withdraw',
                    $withdrawal->client->withdrawal_date,
                    $withdrawal->client->withdrawApproval
                )
            );
        }

        if ($this->withdrawal->premature && $this->withdrawal->partial) {
            $this->withdrawal->raise(new ClientInvestmentHasBeenAdded($this->withdrawal->newInvestment));
        }
    }
}
