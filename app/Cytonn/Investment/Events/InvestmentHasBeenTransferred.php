<?php
/**
 * Date: 22/04/2016
 * Time: 10:24 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientInvestmentTransfer;

class InvestmentHasBeenTransferred
{
    public $transfer;

    /**
     * InvestmentHasBeenTransferred constructor.
     *
     * @param $transfer
     */
    public function __construct(ClientInvestmentTransfer $transfer)
    {
        $this->transfer = $transfer;
    }
}
