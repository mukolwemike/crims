<?php
/**
 * Date: 09/12/2015
 * Time: 10:17 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;

/**
 * Class ClientPaymentIsDue
 *
 * @package Cytonn\Investment\Events
 */
class ClientPaymentIsDue
{
    /**
     * @var ClientInvestment
     */
    public $investment;

    /**
     * @var
     */
    public $amount;

    /**
     * @var
     */
    public $description;

    /**
     * @var null
     */
    public $type;

    /**
     * @var Carbon
     */
    public $date;

    /**
     * @var ClientTransactionApproval|null
     */
    public $approval = null;


    /**
     * @param $amount
     * @param $description
     * @param ClientInvestment $investment
     * @param ClientTransactionApproval $approval
     * @param null $type
     * @param null $date
     */
    public function __construct(
        $amount,
        $description,
        ClientInvestment $investment,
        $type = null,
        $date = null,
        ClientTransactionApproval $approval = null
    ) {
        $this->amount = $amount;

        $this->description = $description;

        $this->investment = $investment;

        $this->type = $type;

        is_null($date) ? $this->date = Carbon::today() : $this->date = new Carbon($date);

        $this->approval = $approval;
    }
}
