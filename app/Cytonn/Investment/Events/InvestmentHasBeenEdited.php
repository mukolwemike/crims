<?php
/**
 * Date: 23/11/2015
 * Time: 10:36 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;

class InvestmentHasBeenEdited
{
    public $investment;
    public $oldInvestment;
    public $editedColumns;
    /**
     * @var ClientTransactionApproval
     */
    public $approval;

    public function __construct(
        ClientInvestment $investment,
        ClientInvestment $oldInvestment,
        ClientTransactionApproval $approval,
        array $editedColumns
    ) {
        $this->investment = $investment;
        $this->oldInvestment = $oldInvestment;
        $this->editedColumns = $editedColumns;
        $this->approval = $approval;
    }
}
