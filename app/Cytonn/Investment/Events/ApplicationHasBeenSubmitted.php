<?php
/**
 * Date: 9/5/15
 * Time: 12:18 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientInvestmentApplication;

class ApplicationHasBeenSubmitted
{
    public $application;

    /**
     * ApplicationHasBeenSubmitted constructor.
     */
    public function __construct(ClientInvestmentApplication $application)
    {
        $this->application = $application;
    }
}
