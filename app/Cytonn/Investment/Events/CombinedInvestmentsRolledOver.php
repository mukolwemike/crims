<?php
/**
 * Date: 07/01/2016
 * Time: 9:55 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Events;

use Cytonn\Investment\Action\Rollover;

/**
 * Class CombinedInvestmentsRolledOver
 *
 * @package Cytonn\Investment\Events
 */
class CombinedInvestmentsRolledOver
{
    /**
     * @var Rollover
     */
    public $rollover;


    /**
     * @param Rollover $rollover
     */
    public function __construct(Rollover $rollover)
    {
        $this->rollover = $rollover;
        $this->rollover->raise(new ClientInvestmentHasBeenAdded($rollover->client));

        //make payment
        $this->rollover->raise(
            new ClientPaymentIsDue(
                $rollover->amountWithdrawn,
                'Combine & Rollover',
                $rollover->oldInvestment,
                'withdraw',
                $rollover->oldInvestment->maturity_date,
                $rollover->oldInvestment->withdrawApproval
            )
        );
    }
}
