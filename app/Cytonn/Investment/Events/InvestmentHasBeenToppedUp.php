<?php
/**
 * Date: 9/12/15
 * Time: 4:48 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use Cytonn\Investment\Action\Topup;

class InvestmentHasBeenToppedUp
{
    public $topup;

    /**
     * InvestmentHasBeenToppedUp constructor.
     *
     * @param $topup
     */
    public function __construct(Topup $topup)
    {
        $this->topup = $topup;
        $this->topup->raise(new ClientInvestmentHasBeenAdded($topup->client));
    }
}
