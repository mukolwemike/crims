<?php
/**
 * Date: 9/9/15
 * Time: 8:38 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientInvestment;

class InvestmentHasBeenConfirmed
{
    public $investment;

    /**
     * InvestmentHasBeenConfirmed constructor.
     *
     * @param $investment
     */
    public function __construct(ClientInvestment $investment)
    {
        $this->investment = $investment;
    }
}
