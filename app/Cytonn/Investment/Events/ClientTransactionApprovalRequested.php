<?php
/**
 * Date: 07/12/2015
 * Time: 7:46 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientTransactionApproval;

/**
 * Class ClientTransactionApprovalRequested
 *
 * @package Cytonn\Investment\Events
 */
class ClientTransactionApprovalRequested
{
    /**
     * @var ClientTransactionApproval
     */
    public $request;


    /**
     * @param ClientTransactionApproval $request
     */
    public function __construct(ClientTransactionApproval $request)
    {
        $this->request = $request;
    }
}
