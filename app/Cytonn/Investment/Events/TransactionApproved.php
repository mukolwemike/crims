<?php
/**
 * Date: 14/12/2015
 * Time: 2:19 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientTransactionApproval;

class TransactionApproved
{
    public $transaction;

    /**
     * TransactionApproved constructor.
     *
     * @param $transaction
     */
    public function __construct(ClientTransactionApproval $transaction)
    {
        $this->transaction = $transaction;
    }
}
