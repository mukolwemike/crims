<?php
/**
 * Date: 03/11/2015
 * Time: 11:19 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

class ClientInvestmentHasBeenAdded
{
    public $investment;

    /**
     * ClientInvestmentHasBeenAdded constructor.
     *
     * @param $investment
     */
    public function __construct($investment)
    {
        $this->investment = $investment;
    }
}
