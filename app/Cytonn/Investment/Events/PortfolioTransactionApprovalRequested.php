<?php
/**
 * Date: 08/12/2015
 * Time: 9:04 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\PortfolioTransactionApproval;

class PortfolioTransactionApprovalRequested
{
    public $request;

    /**
     * PortfolioTransactionApprovalRequested constructor.
     *
     * @param $request
     */
    public function __construct(PortfolioTransactionApproval $request)
    {
        $this->request = $request;
    }
}
