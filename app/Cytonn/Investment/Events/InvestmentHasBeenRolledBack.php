<?php
/**
 * Date: 29/02/2016
 * Time: 10:41 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Events;

use App\Cytonn\Models\ClientInvestment;

class InvestmentHasBeenRolledBack
{
    /**
     * @var
     */
    public $investment;

    /**
     * InvestmentHasBeenRolledBack constructor.
     *
     * @param $investment
     */
    public function __construct(ClientInvestment $investment)
    {
        $this->investment = $investment;
    }
}
