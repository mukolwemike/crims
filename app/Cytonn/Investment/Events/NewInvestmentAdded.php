<?php
/**
 * Date: 11/12/2015
 * Time: 3:55 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

class NewInvestmentAdded
{
    public $investment;

    /**
     * ClientInvestmentHasBeenAdded constructor.
     *
     * @param $investment
     */
    public function __construct($investment)
    {
        $this->investment = $investment;
    }
}
