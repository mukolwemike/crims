<?php
/**
 * Date: 9/12/15
 * Time: 12:16 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Events;

use Cytonn\Investment\Action\Rollover;

class InvestmentHasBeenRolledOver
{
    public $rollover;

    /**
     * InvestmentHasBeenRolledOver constructor.
     *
     * @param $rollover
     */
    public function __construct(Rollover $rollover)
    {
        $this->rollover = $rollover;
        $this->rollover->raise(new ClientInvestmentHasBeenAdded($rollover->client));

        if (is_null($rollover->client->withdrawal_instruction_id)) {
            $this->rollover->raise(
                new ClientPaymentIsDue(
                    $rollover->oldInvestment->withdraw_amount,
                    'Rollover',
                    $rollover->oldInvestment,
                    'withdraw',
                    $rollover->oldInvestment->maturity_date,
                    $rollover->oldInvestment->withdrawApproval
                )
            );
        }
    }
}
