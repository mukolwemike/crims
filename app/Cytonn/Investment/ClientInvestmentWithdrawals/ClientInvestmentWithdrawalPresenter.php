<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ClientInvestmentWithdrawals;

use Cytonn\Core\DataStructures\Carbon;
use Laracasts\Presenter\Presenter;

class ClientInvestmentWithdrawalPresenter extends Presenter
{
    /*
     * Check if a withdrawal was a partial withdrawal
     */
    public function isPartialWithdrawal()
    {
        if (is_null($this->investment->withdrawal_date)) {
            return true;
        }

        return Carbon::parse($this->investment->withdrawal_date)->greaterThan(Carbon::parse($this->date));
    }
}
