<?php

namespace Cytonn\Investment\FundManager\Traits;

use Cytonn\Investment\FundManager\Scopes\HasInvestmentScope;

trait HasInvestmentFundManagerTrait
{
    public static function bootHasInvestmentFundManagerTrait()
    {
        static::addGlobalScope(new HasInvestmentScope());
    }
}
