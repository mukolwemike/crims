<?php

namespace Cytonn\Investment\FundManager\Traits;

use Cytonn\Investment\FundManager\Scopes\HasClientScope;

trait HasClientFundManagerTrait
{
    public static function bootHasClientFundManagerTrait()
    {
        static::addGlobalScope(new HasClientScope());
    }
}
