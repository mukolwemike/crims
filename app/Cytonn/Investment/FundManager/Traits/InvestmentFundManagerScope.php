<?php

namespace Cytonn\Investment\FundManager\Traits;

use Cytonn\Investment\FundManager\Scopes\InvestmentScope;

trait InvestmentFundManagerScope
{
    public static function bootInvestmentFundManagerScope()
    {
        static::addGlobalScope(new InvestmentScope());
    }
}
