<?php

namespace Cytonn\Investment\FundManager\Traits;

use Cytonn\Investment\FundManager\Scopes\ClientsScope;

trait ClientsFundManagerTrait
{
    public static function bootClientsFundManagerTrait()
    {
        static::addGlobalScope(new ClientsScope());
    }
}
