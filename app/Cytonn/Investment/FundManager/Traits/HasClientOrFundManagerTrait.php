<?php

namespace Cytonn\Investment\FundManager\Traits;

use Cytonn\Investment\FundManager\Scopes\HasClientOrFundManagerScope;

trait HasClientOrFundManagerTrait
{
    public static function bootHasClientOrFundManagerTrait()
    {
        static::addGlobalScope(new HasClientOrFundManagerScope());
    }
}
