<?php

namespace Cytonn\Investment\FundManager\Scopes;

use Cytonn\Investment\FundManager\Scope;
use Illuminate\Database\Eloquent\Builder;

class HasClientScope extends Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return void
     */
    public function whenApplied(Builder $builder)
    {
        $builder->whereHas(
            'client',
            function ($client) {
                $client->where('fund_manager_id', $this->fundManagerScope->getSelectedFundManagerId());
            }
        );
    }
}
