<?php

namespace Cytonn\Investment\FundManager\Scopes;

use Cytonn\Investment\FundManager\Scope;
use Illuminate\Database\Eloquent\Builder;

class HasInvestmentScope extends Scope
{
    public function whenApplied(Builder $builder)
    {
        $builder->wherehas(
            'investment',
            function ($i) {
                $i->whereHas(
                    'product',
                    function ($p) {
                        $p->where('fund_manager_id', $this->fundManagerScope->getSelectedFundManagerId());
                    }
                );
            }
        );
    }
}
