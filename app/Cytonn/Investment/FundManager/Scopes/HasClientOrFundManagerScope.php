<?php

namespace Cytonn\Investment\FundManager\Scopes;

use Cytonn\Investment\FundManager\Scope;
use Illuminate\Database\Eloquent\Builder;

class HasClientOrFundManagerScope extends Scope
{
    public function whenApplied(Builder $builder)
    {
        $builder->where(
            function ($builder) {
                $builder->whereHas(
                    'client',
                    function ($client) {
                        $client->where('fund_manager_id', $this->fundManagerScope->getSelectedFundManagerId())
                            ->whereNotNull('fund_manager_id');
                    }
                )->orWhere(
                    function ($builder) {
                        $builder->whereNotNull('fm_id')->where(
                            'fm_id',
                            $this->fundManagerScope->getSelectedFundManagerId()
                        )->whereNull('client_id');
                    }
                );
            }
        );
    }
}
