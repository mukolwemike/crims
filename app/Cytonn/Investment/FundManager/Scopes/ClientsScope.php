<?php

namespace Cytonn\Investment\FundManager\Scopes;

use Cytonn\Investment\FundManager\Scope;
use Illuminate\Database\Eloquent\Builder;

class ClientsScope extends Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @return void
     */
    public function whenApplied(Builder $builder)
    {
        $builder->where('fund_manager_id', $this->fundManagerScope->getSelectedFundManagerId());
    }
}
