<?php

namespace Cytonn\Investment\FundManager\Scopes;

use Cytonn\Investment\FundManager\Scope;
use Illuminate\Database\Eloquent\Builder;

class InvestmentScope extends Scope
{
    public function whenApplied(Builder $builder)
    {
        $builder->whereHas(
            'product',
            function ($product) {
                $product->where('fund_manager_id', $this->fundManagerScope->getSelectedFundManagerId());
            }
        );
    }
}
