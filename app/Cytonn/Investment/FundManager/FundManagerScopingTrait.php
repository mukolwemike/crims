<?php

namespace Cytonn\Investment\FundManager;

trait FundManagerScopingTrait
{
    public static function bootFundManagerScopingTrait()
    {
        $scope = \App::make(FundManagerScope::class);

        static::addGlobalScope($scope);
    }
}
