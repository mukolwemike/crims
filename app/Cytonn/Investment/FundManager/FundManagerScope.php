<?php

namespace Cytonn\Investment\FundManager;

use App\Cytonn\Models\FundManager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope as Scope2;

class FundManagerScope implements Scope2
{
    const REMEMBER=10;

    const DEFAULT_FUND_MANAGER = 1;

    protected $allFundManagers = false;

    protected $selectedFundManager;

    protected $fundManagerModel;

    public function getSelectedFundManager()
    {
        return FundManager::remember(static::REMEMBER)->find($this->getSelectedFundManagerId());
    }

    public function setSelectedFundManager($id)
    {
        $id = (int) $id;

        if ($id == 0) {
            $this->allFundManagers = false;
            $this->fundManagerModel = FundManager::orderBy('id', 'ASC')
                ->remember(static::REMEMBER)->first();

            if ($this->fundManagerModel) {
                $this->selectedFundManager = $this->fundManagerModel->id;
            }
        } else {
            $this->allFundManagers = false;
            $this->fundManagerModel = FundManager::remember(static::REMEMBER)->findOrFail($id);
            $this->selectedFundManager = $id;
        }

        \Session::put('selected_fund_manager_id', $id);
    }
    public function getFundManagersArr()
    {
        $managers = FundManager::remember(static::REMEMBER)->pluck('name', 'id')->all();
        //        $managers[0] = 'All';

        return $managers;
    }

    public function getSelectedFundManagerId()
    {
        return $this->selectedFundManager;
    }

    public function applyScope()
    {
        return !$this->allFundManagers;
    }

    public function hasAccessToModule($name)
    {
        if ($this->allFundManagers) {
            return true;
        }

        return $this->fundManagerModel->hasModule($name);
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model   $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // TODO: Implement apply() method.
    }
}
