<?php

namespace Cytonn\Investment\FundManager;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class Scope implements \Illuminate\Database\Eloquent\Scope
{
    protected $fundManagerScope;


    public function __construct()
    {
        $this->fundManagerScope = \App::make(FundManagerScope::class);
    }

    public function apply(Builder $builder, Model $model)
    {
        if ($this->fundManagerScope->applyScope()) {
            //            $this->whenApplied($builder);
        }
    }

    public function remove(Builder $builder)
    {
        if (method_exists($this, 'whenRemoved')) {
            //            $this->whenRemoved($builder);
        }
    }

    abstract public function whenApplied(Builder $builder);
}
