<?php

namespace Cytonn\Investment\FundManager;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\FundManager;

class FundManagerRepository
{
    private $fundManager;

    public function __construct(FundManager $fundManager)
    {
        $this->fundManager = $fundManager;
    }

    public function clientsQuery()
    {
        return Client::where(function ($q) {
            $q->whereHas('investments', function ($investment) {
                $investment->fundManager($this->fundManager);
            })->orWhereHas('unitFundPurchases', function ($purchases) {
                $purchases->fundManager($this->fundManager);
            });
        });
    }

    public function activeClientsQuery()
    {
        return Client::where(function ($q) {
            $q->whereHas('investments', function ($investment) {
                $investment->fundManager($this->fundManager)->active();
            })->orWhereHas('unitFundPurchases', function ($purchases) {
                $purchases->fundManager($this->fundManager);
            });
        });
    }

    public function countClients()
    {
        return $this->clientsQuery()->count();
    }

    public function countActiveClients()
    {
        return $this->activeClientsQuery()->count();
    }
}
