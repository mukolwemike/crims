<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\Cytonn\Investment\AdditionalCommission;

use App\Cytonn\Models\BulkCommissionPayment;
use Carbon\Carbon;
use Laracasts\Presenter\Presenter;

class AdditionalCommissionPresenter extends Presenter
{
    /**
     * @return bool
     */
    public function canEdit()
    {
        $date = Carbon::parse($this->date);

        $bulkCommission = BulkCommissionPayment::includes($date)->first();

        return ($bulkCommission
            && $bulkCommission->combinedApproval
            && $bulkCommission->combinedApproval->approved == 1) ? false : true;
    }

    /**
     * @param Carbon $date
     * @return mixed
     */
    public function unpaidAmount(Carbon $date)
    {
        return $this->amount - $this->entity->commissionRepayments()->where('date', '<=', $date)->sum('amount');
    }
}
