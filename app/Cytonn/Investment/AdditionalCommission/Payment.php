<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\AdditionalCommission;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionRecepient;
use Crims\AdditionalCommission\Commission\RecipientCalculator;
use Cytonn\Models\Investment\AdditionalCommissionPayment;

class Payment
{
    /**
     * @param CommissionRecepient $recipient
     * @param BulkCommissionPayment $payment
     * @param ClientTransactionApproval $approval
     * @param $overallCommission
     * @return AdditionalCommissionPayment|\Illuminate\Database\Eloquent\Model|void
     */
    public function make(
        CommissionRecepient $recipient,
        BulkCommissionPayment $payment,
        ClientTransactionApproval $approval,
        $overallCommission
    ) {
        $calculator = new RecipientCalculator($recipient, $payment->start, $payment->end, $overallCommission);

        $summary = $calculator->summary();

        $amount = $summary->getAdjustmentCommission();

        $additional = $summary->additionalCommission();

        $repayments = $summary->netRepayments();

        $systemAdditions = $summary->getSystemAdditionalCommission();

        if ($amount == 0 && $additional == 0 && $repayments == 0 && $systemAdditions == 0) {
            return;
        }

        return AdditionalCommissionPayment::create([
            'recipient_id' => $recipient->id,
            'date' => $payment->end,
            'approval_id' => $approval->id,
            'amount' => $amount,
            'additions' => $additional,
            'net_repayments' => $repayments,
            'system_additions' => $systemAdditions,
            'description' => 'Commission Payment ' . $payment->start->toFormattedDateString() . ' - ' .
                $payment->end->toFormattedDateString(),
        ]);
    }
}
