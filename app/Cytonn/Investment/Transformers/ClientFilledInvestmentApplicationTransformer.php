<?php

namespace App\Cytonn\Investment\Transformers;

use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationJointHolders;
use App\Cytonn\Models\ClientUploadedKyc;
use League\Fractal\TransformerAbstract;

class ClientFilledInvestmentApplicationTransformer extends TransformerAbstract
{
    public function transform(ClientFilledInvestmentApplication $application)
    {
        $app = $application->toArray();

        $jointHolders = ClientFilledInvestmentApplicationJointHolders::where('application_id', $application->id)->get();

        $app['progress'] = [
                                'investment'=> (bool) $application->progress_investment,
                                'subscriber'=> (bool) $application->progress_subscriber,
                                'kyc'=> (bool) $application->progress_kyc,
                                'payment'=> (bool) $application->progress_payment,
                                'mandate'=> (bool) $application->progress_mandate,
                                'risk' => (bool) $application->progress_risk
                            ];

        $forbidden = ['id', 'uuid'];

        $uploadedDocs = ClientUploadedKyc::where('application_id', $application->id)->pluck('kyc_id')->all();

        $app['documents'] = ClientComplianceChecklist::whereIn('id', $uploadedDocs)->pluck('slug')->all();
        $app['jointHolders'] = $jointHolders;

        $cleaned = $this->unsetItems($app, $forbidden);
        $cleaned['id'] = $application->uuid;
        

        return $cleaned;
    }

    private function unsetItems($app, $items = [])
    {
        foreach ($items as $item) {
            unset($app[$item]);
        }

        return $app;
    }
}
