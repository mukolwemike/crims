<?php

namespace App\Cytonn\Investment\Transformers;

use App\Cytonn\Models\CommissionClawback;
use League\Fractal\TransformerAbstract;

class RecipientClawbacksTransformer extends TransformerAbstract
{
    public function transform(CommissionClawback $claw)
    {
        $investment = $claw->commission->investment;

        return [
            'description' =>$claw->narration,
            'principal'=>$investment->amount,
            'invested_date'=>$investment->invested_date->toDateString(),
            'maturity_date'=>$investment->maturity_date->toDateString(),
            'amount' => $claw->amount
        ];
    }
}
