<?php

namespace App\Cytonn\Investment\Transformers;

use Crims\Investments\Commission\Models\Recipient;
use League\Fractal\TransformerAbstract;

class RecipientTransformer extends TransformerAbstract
{
    public function transform(Recipient $recipient)
    {
        return [
            'id' => $recipient->uuid,
            'email' => $recipient->email,
            'name' => $recipient->name,
            'fullname' => $recipient->name . ' - ' . $recipient->email,
            'staff_id' => $recipient->staff_id,
            'phone' => $recipient->phone
        ];
    }
}
