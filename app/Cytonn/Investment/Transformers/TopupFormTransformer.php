<?php

namespace App\Cytonn\Investments\Transformers;

use App\Cytonn\Models\ClientTopupForm;
use App\Cytonn\Models\Product;
use League\Fractal\TransformerAbstract;

class TopupFormTransformer extends TransformerAbstract
{
    public function transform(ClientTopupForm $form)
    {
        $doc = $form->document;

        return [
            'amount'=>$form->amount,
            'tenor'=>$form->tenor,
            'mode_of_payment'=>$form->mode_of_payment,
            'product'=> Product::findOrFail($form->product_id)->name,
            'id'=>$form->uuid,
            'document_id'=>is_null($doc) ? null : $doc->uuid,
            'agreed_rate'=>$form->agreed_rate,
        ];
    }
}
