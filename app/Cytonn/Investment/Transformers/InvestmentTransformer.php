<?php

namespace App\Cytonn\Investment\Transformers;

use App\Cytonn\Investment\Reports\InvestmentReporting;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Presenters\General\AmountPresenter;
use App\Cytonn\Presenters\General\DatePresenter;
use Carbon\Carbon;
use Cytonn\Api\Transformers\ProductTransformer;
use League\Fractal\TransformerAbstract;

class InvestmentTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'product'
    ];

    protected $defaultIncludes = [
        'product'
    ];

    protected $appends = [];

    public function append($keys)
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }

        foreach ($keys as $key) {
            array_push($this->appends, $key);
        }

        return $this;
    }


    public function transform(ClientInvestment $investment)
    {
        return [
            'id'=>$investment->uuid,
            'maturity_date'=>$investment->maturity_date->toDateString(),
            'invested_date'=>$investment->invested_date->toDateString(),
            'amount'=>$investment->amount,
            'interest_rate'=>$investment->interest_rate,
            'gross_interest'=>$investment->repo->getGrossInterestForInvestment(),
            'net_interest'=>$investment->repo->getNetInterestForInvestment(),
            'total_value'=>$investment->repo->getTotalValueOfAnInvestment(),
            'withdrawal'=>$investment->repo->getWithdrawnAmount(),
            'currency_code' => $investment->product->currency->code,
            'active'=> !((bool)$investment->withdrawn),
            'activities' => $this->activities($investment),
            'instructions' => $this->instructions($investment),
            'can_withdraw' => $this->canWithdraw($investment)
        ];
    }

    protected function canWithdraw(ClientInvestment $investment)
    {
        if ($investment->withdrawn) {
            return ['status'=>false, 'reason'=>'Investment Withdrawn'];
        }

        if ($investment->instructions()->ofType('rollover')->where('inactive', 0)->count()) {
            return
            [
                'status' => false,
                'reason' => 'Rollover Scheduled'
            ];
        }

        $interest = $investment->repo->netInterestAfterWithdrawals();

        $i = $investment->instructions()->ofType('withdraw')->where('inactive', 0)->get()
            ->filter(
                function (ClientInvestmentInstruction $inst) use ($interest) {
                    $type = $inst->amount_select;

                    if ($type == 'principal' || $type == 'principal_interest') {
                        return true;
                    }

                    if ($type == 'interest') {
                        return false;
                    }

                    if ($inst->amount <= $interest) {
                        return false;
                    }

                    return true;
                }
            );

        if ($i->count()) {
            return [
                'status' => false,
                'reason' => 'Withdrawal scheduled',
            ];
        }

        return [
            'status' => true,
            'reason' => 'No instruction given',
        ];
    }

    protected function instructions(ClientInvestment $investment)
    {
        if (!in_array('instructions', $this->appends)) {
            return null;
        }

        return $investment->instructions()->latest()->take(10)->get()->map(
            function ($instr) use ($investment) {
                $type = $instr->type->name;

                return [
                'type'=> ucfirst($type),
                'sent'=>$instr->created_at->toIso8601String(),
                'amount' => $instr->amount_select == 'amount'
                    ? AmountPresenter::currency($instr->amount)
                    : ucfirst(str_replace('_', ' + ', $instr->amount_select)),
                'date' => $type == 'rollover' || $instr->withdrawal_stage =='mature'
                    ? 'On Maturity' : DatePresenter::formatDate($instr->due_date),
                'status' => $instr->inactive ? 'Cancelled' : ($investment->withdrawn ? 'Completed' : 'Pending')
                ];
            }
        );
    }

    public function includeProduct(ClientInvestment $investment)
    {
        $product = $investment->product;

        return $this->item($product, new ProductTransformer());
    }

    protected function activities(ClientInvestment $investment)
    {
        if (!in_array('activities', $this->appends)) {
            return null;
        }

        $data = (new InvestmentReporting())->processInvestment($investment, Carbon::today());

        return $data->statement;
    }
}
