<?php

namespace App\Cytonn\Investment\Transformers;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Crims\Commission\Summary;
use League\Fractal\TransformerAbstract;

class FaOverridesTransformer extends TransformerAbstract
{
    protected $recipient;

    protected $currency;

    protected $start;

    protected $end;

    public function __construct(CommissionRecepient $recipient, Currency $currency, Carbon $start, Carbon $end)
    {
        $this->recipient = $recipient;
        $this->currency = $currency;
        $this->start = $start;
        $this->end = $end;
    }


    public function transform(CommissionRecepient $recipient)
    {
        $summary = new Summary($this->recipient, $this->currency, $this->start, $this->end);

        return [
            'name'=>$recipient->name,
            'amount'=> $summary->overrideCommission($recipient),
            'rank'=>$recipient->rank->name,
            'rate'=> '',
            'override'=> $summary->overrideOnRecipient($recipient)
        ];
    }
}
