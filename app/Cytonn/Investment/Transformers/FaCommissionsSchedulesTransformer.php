<?php

namespace App\Cytonn\Investment\Transformers;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Crims\Investments\Commission\RecipientCalculator as InvRecipientCalculator;
use Crims\Realestate\Commission\RecipientCalculator as CRERecipientCalculator;
use Crims\Unitization\Commission\RecipientCalculator as UTFRecipientCalculator;
use Crims\Shares\Commission\RecipientCalculator as OTCRecipientCalculator;
use League\Fractal\TransformerAbstract;

class FaCommissionsSchedulesTransformer extends TransformerAbstract
{
    protected $recipient;

    protected $start;

    protected $end;

    protected $currency;

    public function __construct(
        CommissionRecepient $recipient,
        Carbon $start,
        Carbon $end,
        Currency $currency = null
    ) {
        $this->recipient = $recipient;

        $this->start = $start;

        $this->end = $end;

        $this->currency = $currency
            ? $currency
            : Currency::where('code', 'KES')->first();
    }

    public function transform(CommissionRecepient $recipient)
    {
        $products = collect([
            $this->investments(),
            $this->realEstate(),
            $this->unitTrust(),
            $this->OTCShares()
        ]);

        return [
            'id' => $recipient->uuid,
            'name'=>$recipient->name,
            'email'=>$recipient->email,
            'phone'=>$recipient->phone,
            'products' => $products->all(),
            'currency'=>$this->currency->code,
        ];
    }

    protected function investments()
    {
        $cms = (new InvRecipientCalculator($this->recipient, $this->currency, $this->start, $this->end));

        $out = [
            'name' => 'Investments',
            'short_name' => 'CHYS',
            'retainerCommission' => 0
        ];

        $out['schedules'] = $cms->compliantSchedulesQuery()->get()
            ->map(
                function ($sch) use ($cms) {
                    $investment = $sch->commission->investment;
                    return (object)[
                        'client_code' => $investment->client->client_code,
                        'client_name' => $investment->client->present()->fullName,
                        'description' => $sch->description,
                        'principal'=>$investment->amount,
                        'invested_date'=>$investment->invested_date->toDateString(),
                        'maturity_date'=>$investment->maturity_date->toDateString(),
                        'rate'=>$sch->commission->rate,
                        'amount' => $sch->amount,
                        'offRetainerComm' => 0
                    ];
                }
            );

        return (object)$out;
    }

    protected function realEstate()
    {
        $re =  (new CRERecipientCalculator($this->recipient, $this->start, $this->end));

        $out = [
            'name' => 'Real Estate',
            'short_name' => 'RE',
            'retainerCommission' => 0
        //            'retainerCommission' => $re->offRetainerSummary()
        ];

        $out['schedules'] = $re->schedules()
            ->map(
                function ($sch) use ($re) {
                    $holding = $sch->commission->holding;

                    return (object)[
                    'client_name' => $holding->client->present()->fullName,
                    'client_code' => $holding->client->client_code,
                    'project' => $holding->project->name,
                    'unit' => $holding->unit->number,
                    'description' => $sch->description,
                    'date' => $sch->date,
                    'amount' => $sch->amount,
                    'offRetainerComm' => 0
                    //                    'offRetainerComm'=> $re->offRetainerCommission($sch->amount)
                    ];
                }
            );
        
        return (object)$out;
    }

    protected function unitTrust()
    {

        $utf = (new UTFRecipientCalculator(
            $this->recipient,
            $this->start,
            $this->end,
            null,
            $this->currency
        ));

        $out = [
            'name' => 'Unit Trust',
            'short_name' => 'UTF',
            'retainerCommission' => 0
        ];

        $out['schedules'] = $utf->schedules()->get()
            ->map(
                function ($sch) use ($utf) {
                    $purchase = $sch->purchase;
                    return (object)[
                        'client_code' => $purchase->client->client_code,
                        'client_name' => $purchase->client->present()->fullName,
                        'description' => $sch->description,
                        'fund' => $purchase->unitFund->name,
                        'purchase_amount' => $purchase->number * $purchase->price,
                        'date' => $sch->date,
                        'price' => $purchase->price,
                        'amount' => $sch->amount,
                        'offRetainerComm' => 0
                    ];
                }
            );

        return (object)$out;
    }


    protected function OTCShares()
    {
        $otc = (new OTCRecipientCalculator(
            $this->recipient,
            $this->start,
            $this->end,
            null,
            $this->currency
        ));

        $out = [
            'name' => 'OTC Shares',
            'short_name' => 'OTC',
            'retainerCommission' => 0
        ];

        $out['schedules'] = $otc->schedules()->get()
            ->map(
                function ($sch) use ($otc) {

                    $purchase = $sch->sharePurchase;

                    $client = $purchase->shareHolder->client;

                    return (object)[
                        'client_code' => $client->client_code,
                        'client_name' => $client->present()->fullName,
                        'description' => $sch->description,
                        'Entity' => $purchase->shareHolder->entity->name,
                        'purchase_amount' => $purchase->number * $purchase->purchase_price,
                        'date' => $sch->date,
                        'price' => $purchase->purchase_price,
                        'amount' => $sch->amount,
                        'offRetainerComm' => 0
                    ];
                }
            );

        return (object)$out;
    }
}
