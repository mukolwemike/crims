<?php

namespace Cytonn\Investment\Calculate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\EmptyModel;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Support\Collection;

class Calculator extends \Cytonn\Structured\Calculations\Calculator
{
    protected $fresh = false;

    protected $investment;
    public static $count = 0;
    protected $date;
    protected $prepared;
    protected $as_at_next_day;
    protected $interestRate = null;

    protected $expanded = false;

    protected static $taxable_cache = [];

    protected static $clients_with_exemptions_cache = [];

    public function __construct(
        ClientInvestment $investment,
        Carbon $date,
        $as_at_next_day = false,
        $interestRate = null,
        $expanded = false,
        $fresh = false
    ) {
        $this->investment = $investment;

        $this->date = $date;

        $this->as_at_next_day = $as_at_next_day;

        $this->expanded = $expanded;

        $this->interestRate = $interestRate;

        $this->fresh = $fresh;

        $this->prepared = $this->prepare();
    }

    protected function prepare()
    {
        $date = $this->date;

        $actions = $this->getActions();

        $principal = $this->investment->amount;
        $netInterestCF = 0;
        $startDate = $this->investment->invested_date->copy();

        $withdrawal = null;
        $actions = $actions->map(function ($w) use (&$principal, &$netInterestCF, &$withdrawal, &$startDate) {
            if ($w instanceof ClientInvestmentTopup) {
                $type = 'topup';
                $amount = $w->amount;
                if (!$w->description) {
                    $w->description = 'Topup';
                }
            } elseif ($w instanceof ClientInvestmentWithdrawal) {
                $type = 'withdrawal';
                $amount = -$w->amount;
            } elseif ($w instanceof EmptyModel) {
                $type = 'empty';
                $amount = 0;
            } else {
                throw new ClientInvestmentException("Only topup/withdrawal actions accepted");
            }

            $prepared = $this->calculate($principal, $startDate, $w->date, $netInterestCF, $amount);

            $prepared->type = $type;
            $prepared->date = $w->date;
            $prepared->amount = $amount;
            $prepared->action = $w;
            $prepared->description = $w->description;

            $principal = $prepared->principal;
            $netInterestCF = $prepared->net_interest_after;
            $startDate = $w->date;
            $withdrawal = $w;

            return $prepared;
        });


        $start = $this->investment->invested_date;
        if ($withdrawal) {
            $start = $withdrawal->date;
        }

        $last = $this->calculate($principal, $start, $date, $netInterestCF, 0);
        $last->type = 'end';
        $last->date = $date;
        $last->investment = null;
        $last->description = 'Total';

        return (object)[
            'investment' => $this->investment,
            'actions' => $actions->all(),
            'total' => $last
        ];
    }

    protected function calculate($principal, Carbon $start, Carbon $end = null, $netInterestCF, $changeAmount)
    {
        $is_first = $this->investment->invested_date->copy()->startOfDay()->eq($start->copy()->startOfDay());

        if (!$is_first) {
            $start = $start->copy()->addDay();
        }

        $start = $this->limitStart($start->copy());

        $is_before_end_day = $end->copy()->startOfDay()->lt($this->date->copy()->startOfDay());

        if ($is_before_end_day || $this->as_at_next_day) {
            $end = $end->copy()->addDay();
        }

        $end = $this->limitEnd($end->copy());
        $taxable = $this->checkTaxable($this->investment->client, $end);

        $rate = (is_null($this->interestRate)) ? $this->investment->interest_rate : $this->interestRate;

        $gross_i = $this->getGrossInterest($principal, $rate, $start, $end);

        $net_i = $this->getNetInterest($gross_i, $taxable);
        $net_i_cumulative = $netInterestCF + $net_i;
        $gross_i_cumulative = $this->calculateGrossFromNet($net_i_cumulative, $taxable);
        $net_i_after_withdr = max([$net_i_cumulative + $changeAmount, 0]);
        $gross_i_after_withdr = $this->calculateGrossFromNet($net_i_after_withdr, $taxable);
        $wtax_after_withdr = $gross_i_after_withdr - $net_i_after_withdr;

        $total = $principal + $net_i_cumulative + $changeAmount;

        return (object)[
            'original_principal' => (float)$principal,
            'principal' => (float)min([$total, $principal]),
            'gross_interest' => $gross_i,
            'net_interest' => $net_i,
            'wtax' => $gross_i - $net_i,
            'gross_interest_cumulative' => $gross_i_cumulative,
            'net_interest_cumulative' => $net_i_cumulative,
            'wtax_cumulative' => $gross_i_cumulative - $net_i_cumulative,
            'gross_interest_after' => $gross_i_after_withdr,
            'net_interest_after' => $net_i_after_withdr,
            'wtax_after' => $wtax_after_withdr,
            'total' => $total
        ];
    }

    private function checkTaxable(Client $client, Carbon $date)
    {
        if (!$client->taxable) {
            return false;
        }

        if ($client->relationLoaded('taxExemptions') && !$client->taxExemptions->count()
        ) {
            return true;
        }

        $key = 'client_id_'.$client->id.'_date_'.$date->toDateString();

        if ($this->clientHasNoExemptions($client)) {
            return true;
        }

        if (isset(static::$taxable_cache[$key])) {
            return static::$taxable_cache[$key];
        }

        $exempt = !$client->taxExemptions()
            ->where(function ($q) use ($date) {
                $q->where(function ($q) use ($date) {
                    $q->where('start', '<=', $date)->where('end', '>=', $date);
                })
                ->orWhere(function ($q) use ($date) {
                    $q->where('start', '<=', $date)->whereNull('end');
                })
                ->orWhere(function ($q) use ($date) {
                    $q->whereNull('start')->where('end', '>=', $date);
                })
                ->orWhere(function ($q) {
                    $q->whereNull('start')->whereNull('end');
                });
            })->exists();

        static::$taxable_cache[$key] = $exempt;

        return (bool) $exempt;
    }

    private function clientHasNoExemptions(Client $client)
    {
        if (in_array($client->id, static::$clients_with_exemptions_cache)) {
            return false;
        }

        $exempt = $client->taxExemptions()->exists();

        if ($exempt) {
            static::$clients_with_exemptions_cache[] = $client->id;
        }

        return !$exempt;
    }

    protected function limitStart(Carbon $start)
    {
        if ($this->investment->invested_date->gte($start)) {
            return $this->investment->invested_date;
        }

        return $start;
    }


    protected function limitEnd(Carbon $end)
    {
        if ($this->investment->withdrawal_date) {
            if ($this->investment->withdrawal_date->lte($end)) {
                return $this->investment->withdrawal_date;
            }
        }

        if ($this->investment->maturity_date->lte($end)) {
            return $this->investment->maturity_date;
        }

        return $end;
    }

    public function getPrepared()
    {
        return $this->prepared;
    }

    public function withdrawals(Carbon $date)
    {
        return $this->investment->withdrawals()->before($date)->sum('amount');
    }

    private function expandMonthly(Collection $actions)
    {
        $start = $this->investment->invested_date;
        $end = $this->date;

        $months = \Cytonn\Core\DataStructures\Carbon::monthsBetweenArray($start, $end);

        foreach ($months as $month) {
            $month = $month->endOfMonth();

            if ($actions->where('date', '=', $month->startOfDay())->count() == 0) {
                $endMonth = (new EmptyModel())->fill(
                    [
                        'date' => $month,
                        'description' => 'End of ' . $month->format('M Y'),
                        'amount' => 0
                    ]
                );

                $actions->push($endMonth);
            }
        }

        return $actions;
    }

    private function getActions()
    {
        $withdrawals = $this->fresh ? $this->investment->repo->getWithdrawals($this->date)
            : $this->investment->withdrawals;

        $topups = $this->fresh ? $this->investment->repo->getTopupsTo($this->date)
            : $this->investment->topupsTo;

        $actions = $topups->merge($withdrawals->all())
            ->filter(function ($action) {
                return $action->date->lte($this->date) && ($action->amount != 0);
            });

        if ($this->expanded) {
            $actions = $this->expandMonthly($actions);
        }

        return $actions->sortByDate('date', 'ASC')->values();
    }

    public function netInterestRepaid()
    {
        return $this->netInterestBeforeDeductions() - $this->netInterest();
    }

    public function adjustedGrossInterest()
    {
        return $this->grossInterest();
    }
}
