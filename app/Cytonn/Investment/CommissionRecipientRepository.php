<?php

namespace Cytonn\Investment;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientUser;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundPurchaseSale;
use App\Mail\Mail;
use Carbon\Carbon;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Users\UserClientRepository;
use Cytonn\Users\UsernameGenerator;
use Illuminate\Support\Arr;

class CommissionRecipientRepository
{
    public $recipient;

    public function __construct(CommissionRecepient $recipient = null)
    {
        $this->recipient = $recipient ? $recipient : new CommissionRecepient();
    }

    public function hasAccount()
    {
        return $this->recipient->userAccounts()->count() > 0;
    }

    public function investments()
    {
        return $this->investmentsQuery()->get();
    }

    public function investmentsQuery()
    {
        return (new ClientInvestment())->whereHas(
            'commission',
            function ($commission) {
                $commission->where('recipient_id', $this->recipient->id);
            }
        );
    }

    public function createAccount()
    {
        $fa = $this->recipient;

        $firstname = $lastname = '';

        $exploded = explode(" ", $fa->name);

        try {
            $firstname = $exploded[0];
            $lastname = isset($exploded[2]) ? $exploded[2] : $exploded[1];
        } catch (\Exception $e) {
        }

        $user = (new ClientUser())->create([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email'    => $fa->email,
                'phone_country_code' => $fa->phone_country_code,
                'phone' =>$fa->phone,
                'username'=> UserClientRepository::generateUsername($firstname, '', $lastname, $fa->email)
            ]);

        $user->repo->provisionUser();

        $fa->userAccounts()->save($user);

        return $user;
    }

    public function lastRecipientPosition()
    {
        return $this->recipient->commissionRecipientPositions()->orderBy('start_date', 'DESC')->first();
    }

    /**
     * @param Client $client
     * @return bool
     */
    public function hasClient(Client $client)
    {
        $investments = $client->investments()->whereHas('commission', function ($commission) {
            $commission->where('recipient_id', $this->recipient->id);
        })->exists();

        if ($investments) {
            return $investments;
        }

        $utf = $client->unitFundPurchases()->whereHas('unitFundCommission', function ($commission) {
            $commission->where('commission_recipient_id', $this->recipient->id);
        })->exists();

        if ($utf) {
            return $utf;
        }

        $re = $client->unitHoldings()->whereHas('commission', function ($commission) {
            $commission->where('recipient_id', $this->recipient->id);
        })->exists();

        if ($re) {
            return $re;
        }

        return $client->shareHolders()->whereHas('sharePurchases', function ($commission) {
            $commission->whereHas('purchaseOrder', function ($q) {
                $q->where('commission_recipient_id', $this->recipient->id);
            });
        })->exists();
    }

    /*
     * Get the commission recipients using display name
     */
    public function getCommissionRecipientByDisplayName()
    {
        return CommissionRecepient::active()
            ->orderBy('name')
            ->get()
            ->each(function ($recipient) {
                $recipient->display_name = $recipient->name . ' - ' .$recipient->email . ' - ' . $recipient->type->name;
            })->lists('display_name', 'id');
    }

    /*
     * Get the commission recipients for the selects
     */
    public function getRecipientsForSelect()
    {
        $recipients = CommissionRecepient::active()->orderBy('name')->get();

        $recipientArray = array();

        foreach ($recipients as $recipient) {
            $type = $recipient->type ? $recipient->type->name : null;
            $recipientArray[$recipient->id] = $recipient->name . ' - ' . $recipient->email . ' - ' . $type;
        }

        return $recipientArray;
    }

    /*
     * Get the commission recipient position based on date
     */
    public function getRecipientPositionByDate(Carbon $date)
    {
        return $this->recipient->commissionRecipientPositions()
            ->where('start_date', '<=', $date)
            ->orderBy('start_date', 'DESC')
            ->first();
    }

    /*
     * Get the commission recipient typre based on the passed date
     */
    public function getRecipientType(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::today();

        $position = $this->getRecipientPositionByDate($date);

        if ($position) {
            return $position->type;
        }

        return $this->recipient->type;
    }

    /**
     * @param Carbon $date
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|null
     */
    public function getRecipientUserPositionByDate(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::today();

        $position = $this->recipient->commissionRecipientUserPositions()
            ->where('start_date', '<=', $date)
            ->orderBy('start_date', 'DESC')
            ->first();

        if ($position) {
            return $position;
        }

        return $this->recipient->commissionRecipientUserPositions()
            ->where('start_date', '>=', $date)
            ->orderBy('start_date')
            ->first();
    }

    /**
     * @param Carbon|null $date
     * @return mixed|null
     */
    public function getRecipientDepartmentUnitByDate(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::today();

        $position = $this->getRecipientUserPositionByDate($date);

        $unit = $position ? $position->departmentUnit : null;

        return $unit ? $unit : $this->recipient->departmentUnit;
    }

    /*
     * Get the real estate commission rate
     */
    public function getRealEstateCommissionRate(Project $project, Carbon $date = null)
    {
        $type = $this->getRecipientType($date);

        if ($type) {
            return $type->realestateCommissionRates()
                ->where('date', '<=', $date)
                ->latest('date')
                ->where('project_id', $project->id)
                ->first();
        }

        return null;
    }

    public function clients()
    {
        return Client::whereHas('investments', function ($investment) {
            $investment->active()->whereHas('commission', function ($commission) {
                $commission->where('recipient_id', $this->recipient->id);
            });
        });
    }

    /*
     * Get the commission rate to be applied for an investment
     */
    public function getInvestmentCommissionRate(
        Product $product,
        Carbon $date = null,
        ClientInvestment $investment = null,
        $isParent = false
    ) {
        $zeroCommission = $this->recipient->zero_commission == 1;

        $franchise = $investment && @$investment->client->franchise == 1;

        if ($zeroCommission || $franchise) {
            $rate = CommissionRate::where('currency_id', $product->currency_id)
                ->where('rate', 0)
                ->where('date', '<=', $date)->latest('date')->first();

            if ($rate && $franchise && !$zeroCommission) {
                $rate->name = "Franchise Client - 0%";
            }

            return $rate;
        }

        if ($product->present()->isSeip && $isParent) {
            $rates = $product->periodicRatesAsAt($date);

            $rate = $rates->first();

            if (is_null($rate)) {
                return $rate;
            }

            $rate->name = "SEIP Year $rate->tenor - $rate->rate%";

            return $rate;
        }

        $rate = $product->commissionRates()
            ->where('date', '<=', $date)
            ->latest('date')
            ->where('currency_id', $product->currency_id)
            ->first();

        if ($rate) {
            return $rate;
        }

        $type = $this->getRecipientType($date);

        if ($type) {
            return $type->commissionRates()
                ->where('date', '<=', $date)
                ->latest('date')
                ->where('currency_id', $product->currency_id)
                ->first();
        }

        return null;
    }

    public function checkRolloverInvestmentReward($investmentType, $date, $rate)
    {
        if ($investmentType == 3
            && $date >= Carbon::parse('2018-09-17')
            && $date <= Carbon::parse('2019-03-08')
            && $rate->rate) {
            $additionalRate = $rate->rate * 0.2;
            $rate->rate = $rate->rate + $additionalRate;
            $rate->name = $rate->name . ' + Rollover Rate - ' . $additionalRate . '%';
        }

        return $rate;
    }

    /*
     * Get the commission rate to be applied for shares
     */
    public function getSharesCommissionRate(Carbon $date = null)
    {
        $type = $this->getRecipientType($date);

        if ($type) {
            return $type->shareCommissionRates()
                ->orderBy('date', 'DESC')
                ->first();
        }

        return null;
    }

    /*
     * Get the start date for the recipient as a fa
     */
    public function getFaStartDate()
    {
        $position = $this->recipient->commissionRecipientPositions()
            ->whereIn('recipient_type_id', [1, 3])
            ->latest('start_date')->first();

        return $position ? Carbon::parse($position->end_date)->addDay() : null;
    }

    /*
     * Given an email, address, get the commission recipient to which the email belong to
     */
    public function getCommissionRecipientIdByEmail($email)
    {
        return $this->recipient
            ->where('email', $email)
            ->pluck('id')
            ->first();
    }

    /*
     * Get the supervisor for the fa at a given date
     */
    public function getSupervisorAsAt(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        $position = $this->getRecipientPositionByDate($date);

        return $position ? $position->reportsTo : null;
    }

    /*
     * Get recipient rank as at a given date
     */
    public function getRecipientRankAsAt(Carbon $date = null)
    {
        $date = $date ? $date : Carbon::now();

        $position = $this->getRecipientPositionByDate($date);

        return $position ? $position->rank : null;
    }

    /*
     * Get recipient unit fund portfolio
     */
    public function getUnitFundPortfolio(UnitFund $unitFund, Carbon $date = null)
    {
        $date = is_null($date) ? Carbon::now() : $date;

        $purchases = UnitFundPurchase::where('unit_fund_id', $unitFund->id)->before($date)
            ->whereHas('unitFundCommission', function ($q) {
                $q->where('commission_recipient_id', $this->recipient->id);
            })->get()->sum(function (UnitFundPurchase $purchase) {
                return $purchase->number * $purchase->price;
            });

        $sales = UnitFundPurchaseSale::before($date)->whereHas('purchase', function ($q) {
            $q->whereHas('unitFundCommission', function ($q) {
                $q->where('commission_recipient_id', $this->recipient->id);
            });
        })->get()->sum(function (UnitFundPurchaseSale $purchaseSale) {
            return $purchaseSale->number * $purchaseSale->sale->price;
        });

        return $purchases - $sales;
    }

    /**
     * @param null $code
     * @return bool
     * @throws CRIMSGeneralException
     */
    public function assignReferralCode($code = null)
    {
        $nullOrEmpty = is_null($code) || empty($code);

        if ($nullOrEmpty && $this->recipient->referral_code) {
            throw new CRIMSGeneralException("FA already has referral code ".$this->recipient->referral_code);
        }
        if ($nullOrEmpty) {
            $code = $this->suggestReferralCode();
        }

        $code = str_replace(" ", "", $code);

        if (CommissionRecepient::where('referral_code', $code)->exists()) {
            throw new CRIMSGeneralException("Code $code is already taken, kindly choose another one");
        }

        $this->recipient->referral_code = $code;
        $this->recipient->save();

        $this->sendReferralCode();
    }

    private function suggestReferralCode()
    {
        $tries = 0;

        $names = explode($this->recipient->name, " ");

        while (true) {
            $code = UsernameGenerator::generate(
                Arr::first($names),
                Arr::last($names),
                $this->recipient->lastname,
                $this->recipient->email,
                $tries
            );

            $isAvailable = !CommissionRecepient::where('referral_code', $code)->exists();

            if (strlen($code) >= 4 && $isAvailable) {
                break;
            }

            $tries++;
        }

        return $code;
    }

    public function sendReferralCode()
    {
        Mail::compose()
            ->to($this->recipient->email)
            ->subject('Your Assigned Referral Code')
            ->view('emails.fa.referral_code', ['recipient' => $this->recipient])
            ->queue();
    }
}
