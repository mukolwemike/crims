<?php
/**
 * Date: 13/08/2016
 * Time: 5:19 PM
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Summary;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Unitization\UnitFund;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class Analytics
 * @package Cytonn\Investment\Summary
 */
class Analytics extends \Cytonn\Structured\Analytics\Analytics
{
    /**
     * @var
     */
    protected $fundManager;

    /**
     * @var
     */
    protected $currency;

    /**
     * @var Product
     */
    protected $product;

    protected $costValue;

    protected $fund;

    /**
     * Analytics constructor.
     * @param $fundManager
     * @param Carbon $date
     */
    public function __construct($fundManager, Carbon $date)
    {
        if ($fundManager instanceof FundManager) {
            $fundManager = [$fundManager];
        }
        if ($fundManager instanceof Collection) {
            $fundManager = $fundManager->all();
        }

        $this->fundManager = $fundManager;
        $this->date = $date;

        parent::__construct();
    }


    /**
     * @param Carbon|null $date
     * @return Builder
     */
    protected function activeInvestments(Carbon $date = null): Builder
    {
        if (is_null($date)) {
            $date = $this->date;
        }

        $q = ClientInvestment::activeOnDate($date)->with('topupsTo', 'withdrawals', 'client', 'client.taxExemptions');

        if (!$this->baseCurrency && !$this->currency) {
            throw new \InvalidArgumentException("Please specify currency/base currency");
        }

        return $this->addFilterQueries($q);
    }

    /**
     * @param Builder $q
     * @return Builder
     */
    protected function addFilterQueries($q)
    {
        if ($this->fundManager) {
            $q = $q->fundManager($this->fundManager);
        }

        if ($this->product) {
            $q = $q->where('product_id', $this->product->id);
        }

        if ($this->currency) {
            if (!$this->baseCurrency) {
                $this->baseCurrency = $this->currency;
            }

            $q = $q->currency($this->currency);
        }

        if ($this->fund) {
            $q = $q->whereHas('product', function ($product) {
                $product->where('fund_id', $this->fund->id);
            });
        }

        return $q;
    }

    protected function compliantActiveInvestments($reverse = false)
    {
        return $this->activeInvestments($this->date)->whereHas('client', function ($client) use ($reverse) {
            $reverse ?
                $client->where(function ($q) {
                    $q->where('compliant', '!=', 1)->orWhere('compliance_date', '>=', $this->date);
                })
                : $client->where('compliant', 1)->where('compliance_date', '<=', $this->date);
        });
    }


    /**
     * @return float
     */
    public function costValue(): float
    {
        if ($this->costValue) {
            return $this->costValue;
        }

        return $this->costValue = (float)$this->getInvestments()
            ->sum(function ($investment) {
                $principal = $investment->calculated->principal();

                return $this->convertClientInvestmentAmount($investment, $principal, $this->date);
            });
    }

    /**
     * @param null $date
     * @return float
     */
    public function aum(): float
    {
        return $this->marketValue();
    }

    /**
     * @return float
     */
    public function marketValue(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (ClientInvestment $investment) {
                $amount = $investment->calculated->value();

                return $this->convertClientInvestmentAmount($investment, $amount, $this->date);
            });
    }

    /**
     * @return float
     */
    public function adjustedMarketValue(): float
    {
        return (float)$this->getInvestments()
            ->sum(function (ClientInvestment $investment) {
                $amount = $investment->calculated->adjustedMarketValue();

                return $this->convertClientInvestmentAmount($investment, $amount, $this->date);
            });
    }

    /**
     * @return float|int
     */
    public function weightedRate(): float
    {
        return (float)$this->getInvestments()
            ->sum(function ($investment) {
                $principal = $this->convertClientInvestmentAmount(
                    $investment,
                    $investment->calculated->principal(),
                    $this->date
                );

                $rate = $this->convertClientInvestmentRate($investment, $investment->interest_rate, $this->date);

                $costValue = $this->costValue();

                return $costValue == 0 ? 0 : ($principal / $costValue) * $rate;
            });
    }

    /**
     * @return float
     */
    public function weightedTenor(): float
    {
        return $this->getInvestments()->sum(function ($investment) {
            $tenor = $this->date->copy()->diffInDays(new Carbon($investment->maturity_date));

            $total = $this->costValue();

            $amount = $this
                ->convertClientInvestmentAmount($investment, $investment->calculated->principal(), $this->date);

            return $total == 0 ? 0 : ($amount / $total) * ($tenor / 30);
        });
    }


    /**
     * @return float
     */
    public function clientBalances(): float
    {
        $products = Product::all();

        if ($this->fundManager) {
            $products =
                Product::whereIn('fund_manager_id', collect($this->fundManager)->pluck('id')->all())
                ->get();
        }

        if ($this->product) {
            $products = collect([$this->product]);
        }

        return (float)$products->sum(function (Product $product) {
            $bal = ClientPayment::where('product_id', $product->id)
                ->where('date', '<=', $this->date)
                ->sum('amount');

            return $this->convertAmount($bal, $product->currency, $this->date);
        });
    }

    public function compliantAum($date, $reverse = false): float
    {
        return (float)$this->compliantActiveInvestments($reverse)
            ->get()->sum(function ($investment) use ($date) {
                return $this->convertClientInvestmentAmount($investment, $investment->amount, $date);
            });
    }

    public function persistence($start, $end)
    {
        $investmentsAtStart = $this->getPersistenceData($start);

        $investmentsAtEnd = $this->getPersistenceData($end);

        $all = collect([])
            ->merge($investmentsAtStart->values())
            ->merge($investmentsAtEnd->values());

        $started = $investmentsAtStart->sum('amount');

        $minimum = $all->groupBy('client_id')
            ->map(function ($client_grp) {
                if ($client_grp->count() <= 1) {
                    return 0;
                }

                return $client_grp->min('amount');
            })->sum();

        return $started == 0 ? 0 : $minimum / $started;
    }

    private function getPersistenceData($date)
    {
        return $this->activeInvestments($date)
            ->get()
            ->groupBy('client_id')
            ->map(function ($client, $id) use ($date) {
                $amt = $client->sum(function ($investment) use ($date) {
                    return $this->convertClientInvestmentAmount(
                        $investment,
                        $investment->repo->principal($date),
                        $date
                    );
                });

                return (object)[
                    'client_id' => $id,
                    'amount' => $amt
                ];
            });
    }

    public function persistenceValues($startDate, $endDate)
    {
        $clients = Client::whereHas('investments', function ($investments) use ($endDate) {
            if ($this->product) {
                $investments->where('product_id', $this->product->id);
            }
            if ($this->fundManager) {
                $investments->fundManager($this->fundManager);
            }

            $investments->where('invested_date', '<=', $endDate);
        })->get();

        $totals = $clients->map(function (Client $client) use ($startDate, $endDate) {
            $investmentsAtStart = $this->addFilterQueries($client->investments())
                ->activeOnDate($startDate)
                ->get()
                ->sum(function (ClientInvestment $investment) use ($startDate) {
                    return $this->convertClientInvestmentAmount(
                        $investment,
                        $investment->repo->principal($startDate),
                        $startDate
                    );
                });

            $investmentsAtEnd = $this->addFilterQueries($client->investments())
                ->activeOnDate($endDate)
                ->get()
                ->sum(function (ClientInvestment $investment) use ($endDate) {
                    return $this->convertClientInvestmentAmount(
                        $investment,
                        $investment->repo->principal($endDate),
                        $endDate
                    );
                });

            $minimum = min([$investmentsAtStart, $investmentsAtEnd]);

            return ['minimum' => $minimum, 'started' => $investmentsAtStart];
        });

        $started = $totals->sum('started');
        $minimum = $totals->sum('minimum');

        $persistency = $started != 0 ? $minimum / $started : 0;

        return [
            'minimum' => $minimum,
            'started' => $started,
            'persistency' => $persistency
        ];
    }

    public function residualIncome($startDate = null, $endDate = null)
    {
        if (is_null($startDate) & !is_null($endDate)) {
            $startDate = Carbon::parse($endDate)->startOfMonth();
            $endDate = Carbon::parse($startDate);
        } elseif (is_null($endDate)) {
            $startDate = Carbon::parse($startDate)->startOfMonth();
            $endDate = Carbon::parse($endDate);
        } else {
            $startDate = Carbon::parse($startDate);
            $endDate = Carbon::parse($endDate);
        }

        if (!$this->fundManager) {
            throw new ClientInvestmentException("You can only calculate residual income for a fund");
        }

        return Collection::make(\Cytonn\Core\DataStructures\Carbon::daysBetweenArray($startDate, $endDate))
            ->sum(function ($day) {
                $p_analytics = new \Cytonn\Portfolio\Summary\Analytics($this->fundManager, $day);
                $assetYield = $p_analytics->weightedRate();
                $liabilityYield = $this->weightedRate();
                $spread = $assetYield - $liabilityYield;
                $aum = $this->aum();

                return (float)($spread / 100) * $aum;
            });
    }


    /**
     * @param Product $product
     * @return Analytics
     */
    public function setProduct(Product $product): Analytics
    {
        $this->product = $product;
        return $this;
    }

    public function setFund(UnitFund $fund)
    {
        $this->fund = $fund;

        return $this;
    }

    /**
     * @param mixed $currency
     * @return Analytics
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }
}
