<?php
/**
 * Date: 23/11/2015
 * Time: 5:06 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\Setting;
use Carbon\Carbon;

/**
 * Generates values used in CMS summary reports
 * Class CMSSummariesRepository
 *
 * @package Cytonn\Investment
 */
class CMSSummariesRepository
{
    /**
     * @param $product
     * @param $date
     * @return number
     */
    public function getTotalCommissionForProductForMonth($product, $date)
    {
        $date = new Carbon($date);

        $commissions = Commission::whereHas(
            'investment',
            function ($q) use ($product) {
                $q->where('product_id', $product->id);
            }
        )->get();

        $amounts_arr = [];
        foreach ($commissions as $commission) {
            $schedules = CommissionPaymentSchedule::where('commission_id', $commission->id)
                ->where('date', '<=', $date->endOfMonth()->toDateString())
                ->where('date', '>=', $date->startOfMonth()->toDateString())->get();

            foreach ($schedules as $schedule) {
                array_push($amounts_arr, $schedule->amount);
            }
        }

        return array_sum($amounts_arr);
    }

    /**
     * @param $product
     * @param $date
     * @return number
     */
    public function getTotalCustodyFeesForProductForMonth($product, $date)
    {
        $date = new Carbon($date);

        //get investments for month
        $investments = ClientInvestment::where('product_id', $product->id)
            ->where('maturity_date', '>=', $date->startOfMonth()->toDateString())
            ->where('invested_date', '<=', $date->endOfMonth()->toDateString())->get();

        $custody_fees_rate = Setting::where('key', 'custody_fee_rate')->first()->value;

        $custody_fees_array = [];

        foreach ($investments as $investment) {
            $maturity_date = new Carbon($investment->maturity_date);

            if ($maturity_date->isPast()) {
                $tenor = $investment->repo->getTenorInDays();
            } else {
                $tenor = $investment->repo->getTenorInDays('today');
            }

            array_push($custody_fees_array, $custody_fees_rate * $investment->amount * $tenor / (365*100));
        }

        return array_sum($custody_fees_array);
    }
}
