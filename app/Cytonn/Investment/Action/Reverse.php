<?php
/**
 * Date: 06/12/2016
 * Time: 10:23
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Action;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\InterestPayment;
use App\Events\Investments\Actions\WithdrawalHasBeenRemoved;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Models\ClientInvestmentWithdrawal;

/**
 * Class Reverse
 *
 * @package Cytonn\Investment\Action
 */
class Reverse extends Base
{
    /**
     * @param ClientPayment|null $payment
     * @throws \Exception
     */
    public function payment(ClientPayment $payment = null)
    {
        if (is_null($payment)) {
            return;
        }

        $payment->custodialTransaction()->delete();

        $payment->delete();
    }

    /**
     * @param ClientInvestment $investment
     * @throws ClientInvestmentException
     * @throws \Exception
     */
    public function investment(ClientInvestment $investment)
    {
        if ($investment->withdrawn) {
            throw new ClientInvestmentException('You cannot rollback an investment that is withdrawn');
        }

        //delete commission
        $investment->commission->schedules->each(function ($schedule) {
            $schedule->delete();
        });

        $investment->commission()->delete();

        $investment->interestSchedules()->delete();

        if ($investment->isSeip()) {
            $this->rollbackSeip($investment);
        }

        $investment->withdrawals()->delete();

        //check if rollover or partial withdraw, then reverse withdrawal
        $type = $investment->type->name;

        $topups = $investment->topupsFrom;

        foreach ($topups as $topup) {
            $this->topup($topup);
        }

        $reinvested_from = ClientInvestmentWithdrawal::where('reinvested_to', $investment->id)->get();

        if (($type == 'rollover' || $type == 'partial_withdraw') && $reinvested_from->count() == 0) {
            throw new ClientInvestmentException('Parent not found for rollover/partial withdrawal');
        }

        foreach ($reinvested_from as $withdrawal) {
            $reversed = $withdrawal->investment;

            $this->reverseWithdrawal($withdrawal);

            event(new WithdrawalHasBeenRemoved($reversed, $withdrawal));
        }

        $this->payment($investment->paymentIn);
        //delete investment
        $investment->delete();
    }

    public function topup(ClientInvestmentTopup $topup)
    {
        (new Reverse())->payment($topup->payment);

        $topup->delete();
    }

    /**
     * @param ClientInvestment $investment
     * @throws ClientInvestmentException
     */
    public function withdrawal(ClientInvestmentWithdrawal $withdrawal)
    {
        $investment = $withdrawal->investment;

        if ($withdrawal->reinvestedTo) {
            throw new ClientInvestmentException(
                'This withdrawal was reinvested to an investment that still exists, please roll it back first'
            );
        }

        $this->reverseWithdrawal($withdrawal);

        $this->payment($withdrawal->payment);

        event(new WithdrawalHasBeenRemoved($investment, $withdrawal));
    }

    /**
     * @param ClientInvestment $investment
     */
    private function rollbackSeip(ClientInvestment $investment)
    {
        $investment->childInvestmentPaymentSchedules()->delete();

        $investmentSchedule = $investment->investmentPaymentSchedule;

        if ($investmentSchedule) {
            $investmentSchedule->update([
                'investment_id' => null,
                'date_paid' => null
            ]);
        }
    }
}
