<?php
/**
 * Date: 8/17/15
 * Time: 8:40 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Action;

use function Amp\Promise\first;
use App\Cytonn\Investment\Seip\SeipInvestment;
use App\Cytonn\Models\ClientInstructionType;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\User;
use App\Events\Investments\Actions\WithdrawalHasBeenMade;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\Commission\ClawBack;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Investment\Events\ClientInvestmentHasBeenAdded;
use Cytonn\Investment\Events\CombinedInvestmentsRolledOver;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class Rollover extends Base
{
    use EventGenerator, DispatchableTrait;

    public $oldInvestment;

    public $combined = false;

    public $rolledInvestments;

    public $amountWithdrawn;

    protected $original;

    private $isBulk = false;

    const DEFAULT_BULK_ROLLOVER_TENOR = 3;

    /**
     * @param ClientInvestmentInstruction $instruction
     * @return ClientInvestment
     * @throws ClientInvestmentException
     */
    public function run(ClientInvestmentInstruction $instruction)
    {
        $this->original = $instruction->repo->getAllInvestments()
            ->map(function ($inv) use ($instruction) {
                return (object)[
                    'id' => $inv->id,
                    'maturity_date' => $inv->maturity_date,
                    'principal' => $inv->repo->principal($instruction->due_date, true),
                    'due_date' => $instruction->due_date
                ];
            });

        $this->updateInvestmentMaturityDates($instruction);

        $instruction = $instruction->fresh();

        $investments = $instruction->repo->getAllInvestments();

        $total = $instruction->topup_amount
            +
            $investments->sum(function ($i) use ($instruction) {
                return $i->calculate($instruction->due_date, true, null, false, true)->total();
            });

        $rollover_amt = $instruction->repo->calculateAmountAffected();

        $withdraw_amt = $total - $rollover_amt;

        $newInvestment = $this->performRollover($instruction, $rollover_amt);

        $this->recordTopup($instruction, $newInvestment, $instruction->investment);

        $records = $this->recordWithdrawals($instruction, $withdraw_amt);

        $this->recordReinvestment($newInvestment, $records);

        $this->validateRolloverSuccessful(
            $instruction->repo->getAllInvestments(),
            $instruction->approval,
            $records,
            $newInvestment
        );

        $this->oldInvestment = $instruction->investment;
        $this->client = $newInvestment;
        $this->combined = true;
        $this->rolledInvestments = $investments;
        $this->amountWithdrawn = $withdraw_amt;

//        if ($this->amountWithdrawn === 0 && !$instruction->automatic_rollover) {
//            $sender = $newInvestment->approval ? $newInvestment->approval->sender : null;
//
//            $newInvestment->repo->sendBusinessConfirmation($sender);
//        }

        if ($newInvestment->product->present()->isSeip) {
            $seipInvestment = new SeipInvestment();
            $seipInvestment->processRollover($newInvestment, $instruction->approval);
        }

        //raise events for investment
        $this->raise(new CombinedInvestmentsRolledOver($this));
        $this->dispatchEventsFor($this);
        
        return $newInvestment;
    }

    private function recordWithdrawals(ClientInvestmentInstruction $instruction, $amount)
    {
        $remaining = $amount;

        return $instruction->repo->getAllInvestments()
            ->map(function (ClientInvestment $investment) use (&$remaining, $instruction) {
                $value = $investment->calculate(
                    $instruction->due_date,
                    true,
                    null,
                    false,
                    true
                )->value();

                $amount = $remaining;
                if ($value < $remaining) {
                    $amount = $value;
                }

                $r = $this->rollover(
                    $investment,
                    $value - $amount,
                    $amount,
                    $instruction->approval,
                    \Cytonn\Core\DataStructures\Carbon::parse($instruction->due_date),
                    $instruction,
                    $instruction->automatic_rollover
                );

                event(new WithdrawalHasBeenMade($investment, $r->withdrawal));
                event(new WithdrawalHasBeenMade($investment, $r->rollover));

                $original = $this->original->filter(function ($inv) use ($investment) {
                    return $inv->id == $investment->id;
                })->first();

                (new ClawBack($investment, $r->rollover))->onRollover(
                    $original->principal,
                    $original->due_date,
                    $original->maturity_date
                );

                $this->paymentOut($r->withdrawal);

                $remaining = $remaining - $amount;

                return $r;
            });
    }

    private function recordReinvestment(ClientInvestment $investment, $records)
    {
        $records->each(function ($record) use ($investment) {
            $rollover = $record->rollover;

            $rollover->update(['reinvested_to' => $investment->id]);
        });
    }

    private function performRollover(ClientInvestmentInstruction $instruction, $amount)
    {
        $data = $instruction->approval->payload;

        if ($this->isBulk) {
            $data = $this->buildDefaultData($instruction);
        }

        $investment = $instruction->investment;

        $description = isset($data['description'])
            ? $data['description'] : null;

        $interest_action_id = isset($data['interest_action_id'])
            ? $data['interest_action_id'] : null;

        $interest_reinvest_tenor = (array_key_exists('interest_reinvest_tenor', $data))
            ? $data['interest_reinvest_tenor'] : 0;

        $account = $instruction->account ? $instruction->account : $investment->bankAccount;

        $newInvestment = $this->createInvestment(
            ClientInvestmentType::where('name', 'rollover')->first(),
            $amount,
            $instruction->repo->valueDate(),
            $instruction->repo->maturityDate(),
            $investment->client,
            $investment->product,
            $data['interest_rate'],
            $data['interest_payment_interval'],
            $data['interest_payment_date'],
            $instruction->approval,
            false,
            $description,
            $interest_action_id,
            $interest_reinvest_tenor,
            $account
        );

        (new CommissionRepository())->saveCommission($newInvestment, $data);

        return $newInvestment;
    }

    private function recordTopup(
        ClientInvestmentInstruction $instruction,
        ClientInvestment $newInvestment,
        ClientInvestment $oldInvestment
    ) {
        if ($instruction->topup_amount <= 0) {
            return;
        }

        $message =
            'Client: ' . ClientPresenter::presentFullNames($oldInvestment->client_id) .
            ' Topped up an existing investment';

        $top = $this->paymentIn($newInvestment, $message, $instruction->topup_amount);

        $this->topup($newInvestment, $oldInvestment, $instruction->approval, $top, $instruction->due_date);
    }

    private function validateRolloverSuccessful(
        $investments,
        ClientTransactionApproval $approval,
        $records,
        ClientInvestment $newInvestment
    ) {
        $investments->each(
            function (ClientInvestment $investment) use ($approval) {
                $investment = $investment->fresh();

                if ($investment->withdrawals()
                    ->where('date', '>', $investment->withdrawal_date)
                    ->first()) {
                    throw new CRIMSGeneralException("Investment has actions in future, cannot rollover");
                }

                $val = $investment->calculate(
                    $investment->withdrawal_date,
                    true,
                    null,
                    false,
                    true
                )->total();

                if (abs($val) > 1) {
                    throw new \LogicException(
                        "An investment should have value 0 after rollover 
                        (Appr ID: $approval->id Inv ID: $investment->id Bal: " . AmountPresenter::currency($val) . " )"
                    );
                }
            }
        );

        $rolled = $records->sum(function ($record) {
            return $record->rollover->amount;
        });

        if (abs($rolled - $newInvestment->amount) > 1) {
            throw new \LogicException(
                "The amount rolled over does not match what was deducted from investments - 
                $rolled & $newInvestment->amount"
            );
        }
    }

    private function topup(
        ClientInvestment $from,
        ClientInvestment $to,
        ClientTransactionApproval $approval,
        ClientPayment $payment,
        Carbon $date
    ) {
        $this->validateTopup($from, $date);

        return ClientInvestmentTopup::create([
            'amount' => abs($payment->amount),
            'date' => $date,
            'topup_to' => $to->id,
            'topup_from' => $from->id,
            'payment_id' => $payment->id,
            'description' => 'Topup',
            'approval_id' => $approval->id
        ]);
    }

    private function validateTopup(ClientInvestment $investment, Carbon $topupDate)
    {
        if ($topupDate > Carbon::parse($investment->maturity_date)) {
            throw new ClientInvestmentException(
                "Withdrawal failed, the withdrawal date should not be after the investment maturity date"
            );
        }
    }

    /*
     * Update the maturity date for the investments to match the combine date
     */
    public function updateInvestmentMaturityDates(ClientInvestmentInstruction $instruction)
    {
        $investments = $instruction->repo->getAllInvestments();

        $date = $instruction->due_date;

        $investments->each(function (ClientInvestment $investment) use ($date) {
            $investment = $investment->fresh();

            if ($investment->withdrawn) {
                throw new ClientInvestmentException("Investment of principal "
                    . AmountPresenter::currency($investment->amount) . " is withdrawn");
            }

            if ($investment->maturity_date->lt($date)) {
                throw new ClientInvestmentException(
                    "The combine date must be before or on maturity of all investments"
                );
            }

            if ($investment->maturity_date->startOfDay()->ne($date->copy()->startOfDay())) {
                $investment->update(['maturity_date' => $date]);
            }
        });
    }

    /*
     * BULK ROLLOVER
     */
    /**
     * @param ClientInvestment $investment
     * @param ClientTransactionApproval $approval
     * @return ClientInvestment
     * @throws ClientInvestmentException
     */
    public function runBulkRollover(ClientInvestment $investment, ClientTransactionApproval $approval)
    {
        $this->isBulk = true;

        $total = $investment->calculate(
            $investment->maturity_date,
            false,
            null,
            false,
            true
        )->total();

        $roll_date = Carbon::parse($investment->maturity_date)->addDay();

        //CREATE INSTR
        $instr = ClientInvestmentInstruction::create([
            'investment_id' => $investment->id,
            'filled_by' => User::where('username', 'system')->remember(10)->first()->id,
            'type_id' => ClientInstructionType::where('name', 'rollover')->remember(10)->first()->id,
            'automatic_rollover' => true,
            'approval_id' => $approval->id,
            'amount' => $total,
            'amount_select' => 'principal_interest',
            'agreed_rate' => $this->getProductRate($investment, static::DEFAULT_BULK_ROLLOVER_TENOR, $roll_date),
            'due_date' => $investment->maturity_date,
            'approval_id' => $approval->id,
            'tenor' => static::DEFAULT_BULK_ROLLOVER_TENOR
        ]);


        return $this->run($instr);
    }

    /**
     * @param ClientInvestmentInstruction $instruction
     * @return array
     */
    private function buildDefaultData(ClientInvestmentInstruction $instruction)
    {
        $investment = $instruction->investment;

        $md = Carbon::parse($investment->maturity_date)->addDay();

        return [
            'interest_action_id' => $investment->interest_action_id,
            'description' => 'Automatic rollover',
            'interest_rate' => $this->getProductRate($investment, static::DEFAULT_BULK_ROLLOVER_TENOR, $md),
            'interest_payment_interval' => $investment->interest_payment_interval,
            'interest_payment_date' => $investment->interest_payment_date,
            'commission_rate' => $this->getCommissionRate($investment, $md),
            'commission_recepient' => $investment->commission->recipient->id,
            'commission_start_date' => Carbon::today()
        ];
    }

    /**
     * @param Carbon $investedDate
     * @param $tenor
     * @return Carbon
     */
    public function getMaturityDate(Carbon $investedDate, $tenor)
    {
        $tenor = ((int)($tenor / 12) * 365) + (30 * ($tenor % 12));

        $date = $investedDate->copy()->addDays($tenor);

        return $date->dayOfWeek == 1 ? $date : $date->addWeek()->startOfWeek();
    }

    /**
     * @param ClientInvestment $investment
     * @param $tenor
     * @param Carbon $invested_date
     * @return mixed
     */
    public function getProductRate(ClientInvestment $investment, $tenor, Carbon $invested_date)
    {
        $clientAmount = $investment->client->present()->ratesClientAmount($investment->product_id, 0);

        return $investment->product->rate($tenor, $invested_date, $clientAmount);
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $invested_date
     * @return mixed
     */
    public function getCommissionRate(ClientInvestment $investment, Carbon $invested_date)
    {
        if ($investment->client->franchise == 1) {
            return 0;
        }

        $recipient = $investment->commission->recipient;

        $rate = $recipient->repo->getInvestmentCommissionRate($investment->product, $invested_date);

        if ($rate && $rate->rate) {
            $rate = $recipient->repo->checkRolloverInvestmentReward(3, $invested_date, $rate);
        }

        $rate = $rate ? $rate->rate : $investment->commission->rate;

        return $rate;
    }
}
