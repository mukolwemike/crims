<?php
/**
 * Date: 8/17/15
 * Time: 8:40 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Action;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientPaymentAllowedMinimum;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\InterestPaymentSchedule;
use App\Cytonn\Models\Unitization\BenchmarkValue;
use App\Events\Investments\Actions\WithdrawalHasBeenMade;
use Carbon\Carbon;
use Cytonn\Clients\Approvals\Handlers\InterestPaymentBulkApproval;
use Cytonn\Clients\Approvals\Handlers\Withdrawal;
use Cytonn\Custodial\Withdraw as CustodyWithdraw;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\CalculatorTrait;
use Cytonn\Investment\Commission\ClawBack;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Investment\Events\InterestHasBeenReinvested;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class ClientWithdrawals
 *
 * @package Cytonn\Investment
 */
class Withdraw extends Base
{
    use EventGenerator, DispatchableTrait;

    use CalculatorTrait;

    /**
     * @var bool
     */
    public $premature = false;

    /**
     * @var bool
     */
    public $partial = false;

    /**
     * @var null
     */
    public $newInvestment = null;

    const DEFAULT_DEDUCTION_PERCENTAGE = 3;

    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @param ClientTransactionApproval $approval
     * @param array $args
     * @param $withdrawnAmount
     * @return ClientInvestmentWithdrawal
     * @throws CRIMSGeneralException
     */
    public function deduction(
        ClientInvestment $investment,
        Carbon $date,
        ClientTransactionApproval $approval,
        $withdrawnAmount,
        array $args = []
    ) {
        $percentage = $this->getFromArray($args, 'percentage', static::DEFAULT_DEDUCTION_PERCENTAGE);

        $amount = $this->getFromArray(
            $args,
            'amount',
            function () use ($investment, $percentage, $date, $withdrawnAmount) {
                return $this->calculatePenalty($investment, $date, $percentage, $withdrawnAmount)->amount;
            }
        );

        if ($amount <= 0) {
            return;
        }

        $withdrawal = $this->withdraw(
            $investment,
            $date,
            $amount,
            $approval,
            static::TYPE_RETAINED_IN_FUND,
            'deduction',
            null,
            $this->getFromArray($args, 'description', 'Deduction'),
            $this->getFromArray($approval->payload, 'narration', '')
        );

        event(new WithdrawalHasBeenMade($investment, $withdrawal));

        return $withdrawal;
    }

    /**
     * @param ClientTransactionApproval $approval
     * @param Carbon $date
     * @param $amount
     * @param $args
     * @param ClientInvestment|null $investment
     * @return array
     * @throws ClientInvestmentException
     */
    public function calculateAmount(
        ClientTransactionApproval $approval,
        Carbon $date,
        $amount,
        $args,
        ClientInvestment $otherInvestment = null
    ) {
        $investment = $otherInvestment ? $otherInvestment : ($approval->investment);

        $investment = $investment->fresh();

        if ($approval->handler() instanceof InterestPaymentBulkApproval) {
            $interest = $this->calculateInterestAmount($investment, $date);

            if (!$amount) {
                $amount = $interest;
            }

            if ($amount > $interest) {
                throw new ClientInvestmentException("You cannot withdraw more than available interest");
            }

            $withdrawType = 'interest';
        } else {
            $withdrawType = $approval->handler()->getType();

            switch ($withdrawType) {
                case 'withdrawal':
                    if (!$amount) {
                        $amount = $this->calculateWithdrawAmount($investment, $date);
                    } else {
                        $amountAvailable = $this->calculateWithdrawAmount($investment, $date);

                        $amount = $amount > $amountAvailable ? $amountAvailable : $amount;
                    }
                    break;
                case 'interest':
                    $interest = $this->calculateInterestAmount($investment, $date);
                    if (!$amount) {
                        $amount = $interest;
                    }
                    if ($amount > $interest) {
                        throw new ClientInvestmentException("You cannot withdraw more than available interest");
                    }
                    break;
                case 'deduction':
                    break;
                default:
                    throw new ClientInvestmentException("Unknown withdrawal type " . $approval->payload['type']);
            }
        }

        return ['type' => $withdrawType, 'amount' => $amount];
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @param ClientTransactionApproval $approval
     * @param $amount
     * @param array $args
     * @return ClientInvestmentWithdrawal|void
     * @return ClientInvestmentWithdrawal
     * @throws ClientInvestmentException
     * @throws CRIMSGeneralException
     */
    public function all(
        ClientInvestment $investment,
        Carbon $date,
        ClientTransactionApproval $approval,
        $amount,
        array $args = []
    ) {
        if ($approval->handler() instanceof Withdrawal) {
            $type = $approval->handler()->getType();
            if ($type == 'deduction') {
                if ($amount) {
                    $args['amount'] = $amount;
                }

                return $this->deduction($investment, $date, $approval, $amount, $args);
            }
        }

        $vals = $this->calculateAmount($approval, $date, $amount, $args, $investment);

        $amount = $vals['amount'];

        $withdrawType = $vals['type'];

        $type = $this->getFromArray($args, 'type', static::TYPE_SENT_TO_CLIENT);

        $form = $this->getForm($approval);

        $withdrawal = $this->withdraw(
            $investment,
            $date,
            $amount,
            $approval,
            $type,
            $withdrawType,
            $form,
            $this->getFromArray($args, 'description', $this->getDefaultDescription($withdrawType)),
            $this->getFromArray($approval->payload, 'narration', '')
        );


        if ($type == static::TYPE_SENT_TO_CLIENT) {
            $description = $this->getFromArray($args, 'description', 'Client Withdrawal');

            $paymentDate = Carbon::parse($date);

//            if ($withdrawal->isPremature() && $investment->product->fund_manager_id != 2) {
//                $paymentDate = $paymentDate->addWeekdays(static::PREMATURE_WITHDRAW_DELAY);
//            }

            $this->paymentOut($withdrawal, $description, $amount, $paymentDate);

            if ($this->getFromArray($args, 'generate_instruction', false)) {
                $this->generateInstruction($withdrawal, $paymentDate);
            }
        }

        if ($type == static::TYPE_REINVESTED) {
            $this->reinvestInterest($investment, $withdrawal, $approval);
        }

        event(new WithdrawalHasBeenMade($investment, $withdrawal));

        $v = $investment->calculate($withdrawal->date, true, null, false, true)
            ->value();

        if ($v < (-1)) {
            throw new CRIMSGeneralException("The investment has a val of 1 after withdrawal");
        }

        $fdate = $investment->withdrawals()->latest('date')->first();

        if (!$fdate) {
            return;
        }

        $fv = $investment->calculate($fdate->date, true, null, false, true)
            ->value();

        if ($fv < (-1)) {
            throw new CRIMSGeneralException(
                "This withdrawal will overdraw the account, it cannot be processed"
            );
        }

        return $withdrawal;
    }

    private function getForm($approval)
    {
        $data = $approval->payload;

        $client_form = null;

        if (isset($data['rollover_form_id'])) {
            $client_form = $this->cleanFormID($data['rollover_form_id']);
        }

        return ClientInvestmentInstruction::find($client_form);
    }

    private function calculateWithdrawAmount(ClientInvestment $investment, Carbon $date)
    {
        return $investment->calculate(
            $date,
            false,
            null,
            false,
            true
        )->value();
    }

    private function calculateInterestAmount(ClientInvestment $investment, Carbon $date)
    {
        return $investment->repo->getNetInterestForInvestmentAtDate($date, true);
    }

    /**
     * Compute penalty while factoring in the tenor and adjusting the interest rate accordingly
     *
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @param null $penaltyPercentage
     * @param null $amount
     * @return mixed
     */
    public function calculatePenalty(
        ClientInvestment $investment,
        Carbon $date,
        $penaltyPercentage = null,
        $amount = null
    ) {
        $investment = $investment->fresh();

        $product = $investment->product;

        if ($investment->isSeip()) {
            return $this->calculateSeipPenalty($investment, $date);
        }

        $principal = $investment->repo->principal($date);

        $amount = $amount > $principal ? $principal : $amount;

//        $penaltyPercentage = (is_null($penaltyPercentage))
//            ? static::DEFAULT_DEDUCTION_PERCENTAGE
//            : $penaltyPercentage;

        $penaltyPercentage = $investment->interest_rate / 2;

        $investedDate = $investment->invested_date;

        $tenor = $this->getTenorInMonths($investedDate, $date);

        $totalPortfolio = $investment->client->repo->getTodayTotalInvestmentsValueForProduct($product, $date);

        if ($totalPortfolio * 0.25 >= $amount) {
            $client_total = $investment->client->repo->getTodayInvestedAmountForProduct($product, $investedDate);

            $rate = $product->repo->interestRate($tenor, $client_total, $investedDate);

            if ($rate) {
                $penaltyPercentage = $rate / 2 + $investment->interest_rate - $rate;
            }

            $grossPenalty = $this->getGrossInterest($amount, $penaltyPercentage, $investedDate, $date->copy());
        } else {
            $penaltyPercentage = $investment->interest_rate;

            $grossPenalty = $this->getGrossInterest($amount, $investment->interest_rate, $investedDate, $date->copy());
        }

        return (object)[
            'amount' => $this->getNetInterest($grossPenalty, $investment->client->taxable),
            'rate' => $penaltyPercentage
        ];
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @return object
     */
    public function calculateSeipPenalty(
        ClientInvestment $investment,
        Carbon $date
    ) {
        $investedDate = Carbon::parse($investment->invested_date);

        $maturityDate = Carbon::parse($investment->maturity_date);

        $investmentDays = $date->diffInDays($investedDate);

        $amount = $investment->repo->principal($date);

        $penaltyPercentage = '';
        $penaltyAmount = 0;

        if ($investmentDays >= 365 && $investmentDays < 730) {
            $totalCommission = $investment->repo->getTotalCommissionAlreadyPaidToFa();

            $interest = $investment->repo->getNetInterestForInvestmentAtDate($date);

            $penaltyAmount = $totalCommission + $interest;

            $penaltyPercentage = $investment->interest_rate;
        } elseif ($investmentDays >= 730 && $date < $maturityDate) {
            $penaltyPercentage = $investment->interest_rate - $this->getBondRate();

            $grossPenalty = $this->getGrossInterest($amount, $penaltyPercentage, $investedDate, $date);

            $penaltyAmount = $this->getNetInterest($grossPenalty, $investment->client->taxable);
        }

        return (object)[
            'amount' => $penaltyAmount,
            'rate' => $penaltyPercentage
        ];
    }

    /**
     * @return float
     */
    private function getBondRate()
    {
        return BenchmarkValue::forBenchmarkName('T-bond-5')->latest('date')->first()->value;
    }

    /*
     * Generate a description based on the withdrawal type
     */
    public function getDefaultDescription($type)
    {
        switch ($type) {
            case 'withdrawal':
                return "Withdrawal";
                break;
            case 'interest':
                return "Interest Payment";
                break;
            case 'deduction':
                return "Deduction";
                break;
            default:
                return "Withdrawal";
        }
    }

    /**
     * Get the tenor in months when give a startdate and enddate
     *
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return float|int
     */
    private function getTenorInMonths(Carbon $startDate, Carbon $endDate)
    {
        $days = $startDate->diffInDays($endDate);

        return ceil($days / 30);
    }

    /**
     * @param ClientInvestment $investment
     * @param ClientInvestmentWithdrawal $withdrawal
     * @param ClientTransactionApproval $approval
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    private function reinvestInterest(
        ClientInvestment $investment,
        ClientInvestmentWithdrawal $withdrawal,
        ClientTransactionApproval $approval
    ) {
        $invested_date = $withdrawal->date->copy()->addDay();

        $tenor = $this->getInterestReinvestTenor($investment, $invested_date);

        $clientAmount =
            $investment->client
                ->present()
                ->ratesClientAmount($investment->product_id, $withdrawal->amount);

        $rate = $investment->product->rate($tenor, $invested_date, $clientAmount);

        if (is_null($rate)) {
            throw new ClientInvestmentException(
                'Rate for ' . $investment->product->name . ' with tenor ' . $tenor . ' months does not exist!'
            );
        }

        if (is_null($rate)) {
            throw new ClientInvestmentException(
                'Rate for ' . $investment->product->name . 'ID: ' .
                $investment->id . ' with tenor ' . $tenor . ' months does not exist!'
            );
        }

        $maturity_date = $investment->interest_reinvest_tenor != 0 ? $this->getMaturityDate($invested_date, $tenor)
            : $investment->maturity_date;

        $description = 'Reinvested interest from ' . $investment->amount . ' maturing on ' .
            DatePresenter::formatDate($investment->maturity_date);

        $reinvestment = $this->createInvestment(
            ClientInvestmentType::where('name', 'reinvested_interest')->first(),
            $withdrawal->amount,
            $invested_date,
            $maturity_date,
            $investment->client,
            $investment->product,
            $rate,
            0,
            $investment->interest_payment_date,
            $approval,
            false,
            $description,
            null
        );

        $reinvestment->update([
            'bank_account_id' => $investment->bank_account_id
        ]);

        $reinvestment->reinvestedFrom()->save($withdrawal);

        $recipient = ($investment->client->incomingFa)
            ? ($investment->client->incomingFa) : $investment->commission->recipient;

        if ($investment->client->franchise == 1) {
            $rate = 0;
        } else {
            $rate = $recipient->repo->getInvestmentCommissionRate($investment->product, $invested_date);

            $rate = $rate ? $rate->rate : $investment->commission->rate;
        }

        $data = [
            'commission_rate' => $rate,
            'commission_recepient' => $investment->commission->recipient_id
        ];

        (new CommissionRepository())->saveCommission($reinvestment, $data);

        $this->raise(new InterestHasBeenReinvested($reinvestment));
        $this->dispatchEventsFor($this);
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @param Carbon|null $date
     * @return \Cytonn\Custodial\Instruction
     * @throws \Exception
     */
    public function generateInstruction(ClientInvestmentWithdrawal $withdrawal, Carbon $date = null)
    {
        $b = $this->createRequiredBalance($withdrawal);

        $investment = $withdrawal->investment;


        $w = CustodyWithdraw::make(
            $date ? $date : $withdrawal->date,
            $investment->product->custodialAccount,
            "Interest payment",
            $investment->client,
            $withdrawal->amount,
            $investment->product,
            null,
            null,
            $withdrawal->approval,
            $withdrawal->payment,
            null,
            null,
            $investment->bankAccount()->where('active', 1)->first(),
            new ClientScheduledTransaction() //allow negative balances
        );

        $b->delete();

        return $w;
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @return ClientPaymentAllowedMinimum
     */
    private function createRequiredBalance(ClientInvestmentWithdrawal $withdrawal)
    {
        $amt = 0;

        $bal = ClientPayment::balance(
            $withdrawal->investment->client,
            $withdrawal->investment->product,
            null,
            null,
            $withdrawal->date
        );

        if ($bal < 0) {
            $amt = -1 * $bal;
        }

        return ClientPaymentAllowedMinimum::create([
            'product_id' => $withdrawal->investment->product_id,
            'client_id' => $withdrawal->investment->client_id,
            'date' => $withdrawal->date->addDay(),
            'amount' => $amt + $withdrawal->amount
        ]);
    }

    /*
     * Calculate the appropriate interest reinvest tenor
     */
    public function getInterestReinvestTenor(ClientInvestment $investment, $investedDate)
    {
        if ($investment->interest_reinvest_tenor != 0) {
            $tenor = $investment->interest_reinvest_tenor;
        } else {
            $tenor = $investment->maturity_date->diffInMonths($investedDate);
        }

        if ($tenor == 0) {
            $tenor = 1;
        }

        return $tenor;
    }

    /*
     * Combine the reinvestment interest and create one
     */
    public function combineInterestReinvestment(
        $client_schedules,
        $pastReinvestedInterestInvestments,
        $client,
        $approval,
        Carbon $date
    ) {
        $investment = $client_schedules->last()->investment;

        $product = $investment->product;

        $amount = $client_schedules->sum(function (InterestPaymentSchedule $schedule) use ($date, $approval) {
            $interest = $schedule->calculatePaymentAmount($date);

            $vals = $this->calculateAmount($approval, $date, $interest, [], $schedule->investment);

            return $vals['amount'];
        });

        $pastInterestAmount = $pastReinvestedInterestInvestments->sum(function ($investment) use ($date) {
            return $investment->calculate($date, false, null, false, true)->total();
        });

        $amount += $pastInterestAmount;

        $invested_date = $date->copy()->addDay();

        $tenor = $client->combine_interest_reinvestment_tenor;

        $maturity_date = $this->getMaturityDate($invested_date, $tenor);

        $rate = $product->repo->interestRate(
            $tenor,
            $client->repo->getTodayTotalInvestmentsValueForProduct($product)
        );

        if (is_null($rate)) {
            throw new ClientInvestmentException(
                'Rate for ' . $product->name . ' with tenor ' . $tenor . ' months does not exist!'
            );
        }

        if (is_null($rate)) {
            throw new ClientInvestmentException(
                'Rate for ' .
                $product->name . 'ID: ' .
                $client->id . ' with tenor ' .
                $tenor . ' months does not exist!'
            );
        }

        $description = 'Combined Interest Reinvested from client investments\' interests maturing on ' .
            DatePresenter::formatDate($maturity_date);

        $reinvestment = $this->createInvestment(
            ClientInvestmentType::where('name', 'reinvested_interest')->first(),
            $amount,
            $invested_date,
            $maturity_date,
            $client,
            $product,
            $rate,
            0,
            null,
            $approval,
            false,
            $description,
            1
        );

        $recipient = ($investment->client->incomingFa)
            ? ($investment->client->incomingFa) : $investment->commission->recipient;

        $rate = $recipient->repo->getInvestmentCommissionRate($investment->product, $invested_date);

        $rate = $rate ? $rate->rate : $investment->commission->rate;

        $data = [
            'commission_rate' => $rate,
            'commission_recepient' => $investment->commission->recipient_id
        ];

        (new CommissionRepository())->saveCommission($reinvestment, $data);

        $this->raise(new InterestHasBeenReinvested($reinvestment));
        $this->dispatchEventsFor($this);

        return $reinvestment;
    }

    /*
     * Withdraw the initially reinvested interest
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @param ClientTransactionApproval $approval
     * @param ClientInvestment $reinvestment
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    public function withdrawReinvestedInterest(
        ClientInvestment $investment,
        Carbon $date,
        ClientTransactionApproval $approval,
        ClientInvestment $reinvestment
    ) {
        $this->updateReinvestedInterestMaturityDates($investment, $date);

        $this->withdrawFromReinvestedInterests(
            $investment,
            $date,
            $approval,
            $reinvestment,
            ['description' => 'Rolled Reinvested Interest']
        );
    }

    /*
     * Update the maturity dates for the reinvested interests
     */
    public function updateReinvestedInterestMaturityDates(ClientInvestment $investment, Carbon $date)
    {
        if ($investment->withdrawn) {
            throw new ClientInvestmentException("Investment of principal " .
                AmountPresenter::currency($investment->amount) . " is withdrawn");
        }

        if ($investment->maturity_date->lt($date)) {
            throw new ClientInvestmentException(
                "The combine date must be before or on maturity of all investments"
            );
        }

        $maturityDate = $investment->maturity_date;
        $principal = $investment->repo->principal($date, true);

        if ($investment->maturity_date->startOfDay()->ne($date->copy()->startOfDay())) {
            $investment->update(['maturity_date' => $date]);
        }

        $investment->old_maturity_date = $maturityDate;
        $investment->old_principal = $principal;

        return $investment;
    }

    /*
     * Withdraw from the reinvested interests
     */
    public function withdrawFromReinvestedInterests(
        ClientInvestment $investment,
        Carbon $date,
        ClientTransactionApproval $approval,
        ClientInvestment $reinvestment,
        array $args = []
    ) {
        $amount = $this->calculateWithdrawAmount($investment, $date);

        $initialPrincipal = $investment->old_principal;
        $initialMaturity = $investment->old_maturity_date;
        unset($investment->old_principal);
        unset($investment->old_maturity_date);

        $withdrawType = 'withdrawal';

        $type = $this->getFromArray($args, 'type', static::TYPE_REINVESTED);

        $form = $this->getForm($approval);

        $withdrawal = $this->withdraw(
            $investment,
            $date,
            $amount,
            $approval,
            $type,
            $withdrawType,
            $form,
            $this->getFromArray($args, 'description', $this->getDefaultDescription($withdrawType)),
            $this->getFromArray($approval->payload, 'narration', '')
        );

        $reinvestment->reinvestedFrom()->save($withdrawal);

        $investment->update([
            'rolled' => true,
            'automatic_rollover' => false
        ]);

        event(new WithdrawalHasBeenMade($investment, $withdrawal));

        (new ClawBack($investment, $withdrawal))->onRollover(
            $initialPrincipal,
            $date,
            $initialMaturity
        );

        $v = $investment
            ->calculate($withdrawal->date, true, null, false, true)
            ->value();

        if ($v < (-1)) {
            throw new CRIMSGeneralException("The investment has a val of 1 after withdrawal");
        }
    }


    /*
     * Withdraw the interests scheduled for reinvestment
     */
    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @param ClientTransactionApproval $approval
     * @param $amount
     * @param ClientInvestment $reinvestment
     * @param array $args
     * @throws CRIMSGeneralException
     * @throws ClientInvestmentException
     */
    public function withdrawInterests(
        ClientInvestment $investment,
        Carbon $date,
        ClientTransactionApproval $approval,
        $amount,
        ClientInvestment $reinvestment,
        array $args = []
    ) {
        $vals = $this->calculateAmount($approval, $date, $amount, $args, $investment);

        $amount = $vals['amount'];

        $withdrawType = $vals['type'];

        $type = $this->getFromArray($args, 'type', static::TYPE_REINVESTED);

        $form = $this->getForm($approval);

        $withdrawal = $this->withdraw(
            $investment,
            $date,
            $amount,
            $approval,
            $type,
            $withdrawType,
            $form,
            $this->getFromArray($args, 'description', $this->getDefaultDescription($withdrawType)),
            $this->getFromArray($approval->payload, 'narration', '')
        );

        $reinvestment->reinvestedFrom()->save($withdrawal);

        event(new WithdrawalHasBeenMade($investment, $withdrawal));

        $v = $investment->calculate(
            $withdrawal->date,
            true,
            null,
            false,
            true
        )
            ->value();

        if ($v < (-1)) {
            throw new CRIMSGeneralException("The investment has a val of 1 after withdrawal");
        }
    }

    /*
     * Get the maturity date for reinvested investment
     */
    public function getMaturityDate(Carbon $investedDate, $tenor)
    {
        $tenor = ((int)($tenor / 12) * 365) + (30 * ($tenor % 12));

        $date = $investedDate->copy()->addDays($tenor);

        return $date->dayOfWeek == 1 ? $date : $date->addWeek()->startOfWeek();
    }
}
