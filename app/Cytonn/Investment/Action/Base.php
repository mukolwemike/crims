<?php
/**
 * Date: 05/12/2016
 * Time: 10:48
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Action;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Product;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Custodial\Payments\Payment;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\BaseInvestment;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Models\ClientInvestmentWithdrawalType;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Workflow\Hooks\ClosingPeriods;
use DateTime;

/**
 * Class Base
 *
 * @package Cytonn\Investment\Action
 */
abstract class Base extends BaseInvestment
{
    protected $exemptedApproval;

    const TYPE_SENT_TO_CLIENT = 'sent_to_client';

    const TYPE_REINVESTED = 'reinvested';

    const TYPE_RETAINED_IN_FUND = 'retained_in_fund';

    const PREMATURE_WITHDRAW_DELAY = 0;

    /**
     * @param mixed $exemptedApproval
     */
    public function setExemptedApproval($exemptedApproval)
    {
        $this->exemptedApproval = $exemptedApproval;
    }

    /**
     * @param ClientInvestmentApplication|null $application
     * @param ClientInvestmentType $type
     * @param $amount
     * @param DateTime $invested_date
     * @param DateTime $maturity_date
     * @param Client $client
     * @param Product $product
     * @param $interest_rate
     * @param $interest_payment_interval
     * @param $interest_payment_date
     * @param ClientTransactionApproval $approval
     * @param bool $on_call
     * @param $description
     * @param $interest_action_id
     * @return ClientInvestment
     * @throws ClientInvestmentException
     * @internal param $automatic_rollover
     * @throws CRIMSGeneralException
     */
    protected function createInvestment(
        ClientInvestmentType $type,
        $amount,
        DateTime $invested_date,
        DateTime $maturity_date,
        Client $client,
        Product $product,
        $interest_rate,
        $interest_payment_interval,
        $interest_payment_date,
        ClientTransactionApproval $approval,
        $on_call = false,
        $description = null,
        $interest_action_id = null,
        $interest_reinvest_tenor = 0,
        $account = null
    ) {
        if ($amount < 1) {
            throw new ClientInvestmentException('You cannot invest an amount of less than 1 for client '
                . $client->client_code . ' and amount ' . $amount);
        }

        $this->checkClosingPeriods($invested_date, $approval);

        $investment = new ClientInvestment();
        $investment->investment_type_id = $type->id;
        $investment->amount = $amount;
        $investment->invested_date = $invested_date;
        $investment->maturity_date = $maturity_date;
        $investment->client_id = $client->id;
        $investment->product_id = $product->id;
        $investment->interest_rate = $interest_rate;
        $investment->interest_payment_interval = $interest_payment_interval;
        $investment->interest_payment_date = $interest_payment_date;
        $investment->approval_id = $approval->id;
        $investment->on_call = $on_call;
        $investment->description = $description;
        $investment->interest_action_id = $interest_action_id;
        $investment->interest_reinvest_tenor = $interest_reinvest_tenor;
        $investment->bank_account_id = $account ? $account->id : null;
        $investment->save();


        return $investment;
    }

    /**
     * @param ClientInvestment $investment
     * @param $amount
     * @param $withdrawAmount
     * @param ClientTransactionApproval $approval
     * @param ClientInvestmentInstruction|null $form
     * @param bool $automatic
     * @return object
     * @throws CRIMSGeneralException
     */
    protected function rollover(
        ClientInvestment $investment,
        $amount,
        $withdrawAmount,
        ClientTransactionApproval $approval,
        $withdrawalDate,
        ClientInvestmentInstruction $form = null,
        $automatic = false
    ) {
        $r = $this->withdraw(
            $investment,
            $withdrawalDate,
            $amount,
            $approval,
            static::TYPE_REINVESTED,
            'withdrawal',
            $form,
            $automatic ? 'Automatic Rollover' : 'Rollover',
            ''
        );

        $w = $this->withdraw(
            $investment,
            $withdrawalDate,
            $withdrawAmount,
            $approval,
            static::TYPE_SENT_TO_CLIENT,
            'withdrawal',
            $form,
            'Withdrawal',
            ''
        );

        $investment->update([
            'withdrawn' => true,
            'withdrawal_date' => $w->date,
            'rolled' => true,
            'automatic_rollover' => $automatic
        ]);

        return (object)[
            'withdrawal' => $w,
            'rollover' => $r
        ];
    }

    /**
     * @param ClientInvestment $investment
     * @param \DateTime $date
     * @param $amount
     * @param ClientTransactionApproval $approval
     * @param $type
     * @param $withdrawType
     * @param ClientInvestmentInstruction|null $form
     * @param $description
     * @param $narration
     * @return ClientInvestmentWithdrawal
     * @throws CRIMSGeneralException
     */
    protected function withdraw(
        ClientInvestment $investment,
        DateTime $date,
        $amount,
        ClientTransactionApproval $approval,
        $type,
        $withdrawType,
        ClientInvestmentInstruction $form = null,
        $description,
        $narration
    ) {
        if (!in_array($withdrawType, ['deduction', 'interest', 'withdrawal'])) {
            throw new \InvalidArgumentException("Unknown withdrawal type $withdrawType");
        }

        $this->checkClosingPeriods($date, $approval);

        $type = ClientInvestmentWithdrawalType::where("slug", $type)->first();

        $w = ClientInvestmentWithdrawal::create(
            [
                'investment_id' => $investment->id,
                'amount' => $amount,
                'date' => $date,
                'approval_id' => $approval->id,
                'type_id' => $type->id,
                'description' => $description,
                'narration' => $narration,
                'form_id' => $form ? $form->id : null,
                'withdraw_type' => $withdrawType
            ]
        );

        return $w;
    }


    /**
     * @param ClientInvestment $investment
     * @return ClientInvestment
     */
    protected function reverseWithdrawal(ClientInvestmentWithdrawal $withdrawal)
    {
        $investment = $withdrawal->investment;
        //commission
        $commission = $investment->commission;

        $commission->clawBacks()->delete();

        $commission->schedules()
            ->where('claw_back', true)
            ->get()
            ->each(
                function ($c) {
                    $c->update(['claw_back' => null]);
                }
            );

        (new Reverse())->payment($withdrawal->payment);

        $withdrawal->delete();

        return $investment;
    }

    protected function getParents(ClientInvestment $investment)
    {
        return ClientInvestment::where('withdraw_approval_id', $investment->approval_id)->get();
    }

    protected function checkClosingPeriods(DateTime $date, ClientTransactionApproval $approval)
    {
        $is_closed = (new ClosingPeriods())->periodIsClosed($date, $approval);

        if ($is_closed) {
            throw new CRIMSGeneralException("Could not complete transaction, period is closed");
        }
    }

    public function getChildren(ClientInvestment $investment)
    {
        return ClientInvestment::where('approval_id', $investment->withdraw_approval_id)->get();
    }

    /**
     * @param ClientInvestment $investment
     * @param null $description
     * @return ClientPayment
     */
    protected function paymentIn(
        ClientInvestment $investment,
        $description = null,
        $amount = null
    ) {
        if (is_null($amount)) {
            $amount = $investment->amount;
        }

        if (is_null($description)) {
            $description = "Investment of " . AmountPresenter::currency($amount) . ' in ' . $investment->product->name;
        }

        $payment = Payment::make(
            $investment->client,
            $investment->commission->recipient,
            null,
            $investment->product,
            null,
            null,
            $investment->invested_date,
            $amount,
            'I',
            $description
        )->credit();

        $investment->paymentIn()->associate($payment);
        $investment->save();

        return $payment;
    }

    public function paymentOut(
        ClientInvestmentWithdrawal $withdrawal,
        $description = null,
        $amount = null,
        $date = null
    ) {
        if (is_null($amount)) {
            $amount = $withdrawal->amount;
        }

        $investment = $withdrawal->investment;

        if (is_null($description)) {
            $description = "Withdrawal of " . AmountPresenter::currency($amount) .
                ' from ' . $investment->product->name;
        }

        if (is_null($date)) {
            $date = $withdrawal->date;
        }

        $payment = Payment::make(
            $investment->client,
            $investment->commission->recipient,
            null,
            $investment->product,
            null,
            null,
            $date,
            $amount,
            'W',
            $description
        )->debit();

        $withdrawal->payment()->associate($payment);
        $withdrawal->save();

        return $payment;
    }

    public function investmentPaymentOut(
        ClientInvestment $investment,
        $description = null,
        $amount = null,
        $date = null
    ) {
        if (is_null($amount)) {
            $amount = $investment->withdraw_amount;
        }

        if (is_null($description)) {
            $description = "Withdrawal of " . AmountPresenter::currency($amount) .
                ' from ' . $investment->product->name;
        }

        if (is_null($date)) {
            $date = $investment->withdrawal_date;
        }

        $payment = Payment::make(
            $investment->client,
            $investment->commission->recipient,
            null,
            $investment->product,
            null,
            null,
            $date,
            $amount,
            'W',
            $description
        )->debit();

        return $payment;
    }

    protected function getFromArray($array, $key, $default = null)
    {
        if (isset($array[$key])) {
            if (!empty($array[$key])) {
                return $array[$key];
            }
        }

        if ($default instanceof \Closure) {
            return call_user_func($default);
        }

        return $default;
    }
}
