<?php
/**
 * Date: 8/17/15
 * Time: 8:41 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Action;

use App\Cytonn\Investment\Seip\SeipInvestment;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentType;
use App\Cytonn\Models\ClientTopupForm;
use Cytonn\Clients\ClientRepository;
use Cytonn\Investment\ApplicationsRepository;
use Cytonn\Investment\CommissionRepository;
use Cytonn\Investment\Events\InvestmentHasBeenToppedUp;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

/**
 * Class Topup
 *
 * @package Cytonn\Investment\Action
 */
class Topup extends Base
{
    use EventGenerator, DispatchableTrait;
    /**
     * @var ClientInvestment
     */
    public $client;

    /**
     * @param ClientTopupForm $form
     * @return $this
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    public function handle(ClientTopupForm $form)
    {
        $investment = $this->investment($form);

        $this->clientCode($investment->client);

        $this->client = $investment;

        //generate commission
        (new CommissionRepository())->saveCommission($investment, $form->approval->payload);

        $this->paymentIn($investment, 'Topup in '.$investment->product->name);

        if ($investment->product->present()->isSeip) {
            $seipInvestment = new SeipInvestment();
            $seipInvestment->processTopup($investment, $form->approval);
        }

        //raise events
        $this->raise(new InvestmentHasBeenToppedUp($this));
        $this->dispatchEventsFor($this);

        return $this;
    }

    private function clientCode(Client $client)
    {
        if ($client->client_code) {
            return;
        }

        (new ApplicationsRepository())->setClientCode($client);
    }

    /**
     * @param ClientTopupForm $form
     * @return ClientInvestment
     * @throws \Cytonn\Exceptions\ClientInvestmentException
     */
    private function investment(ClientTopupForm $form)
    {
        $product = $form->product;
        $data = $form->approval->payload;

        $description = isset($data['description']) ? $data['description'] : null;
        $interest_action_id = isset($data['interest_action_id']) ? $data['interest_action_id'] : null;
        $interest_reinvest_tenor =
            (array_key_exists('interest_reinvest_tenor', $data)) ? $data['interest_reinvest_tenor'] : 0;

        return $this->createInvestment(
            ClientInvestmentType::where('name', 'topup')->first(),
            $form->amount,
            $form->repo->valueDate(),
            $form->repo->maturityDate(),
            $form->client,
            $product,
            $data['interest_rate'],
            $data['interest_payment_interval'],
            $data['interest_payment_date'],
            $form->approval,
            false,
            $description,
            $interest_action_id,
            $interest_reinvest_tenor
        );
    }
}
