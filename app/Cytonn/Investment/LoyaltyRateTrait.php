<?php

namespace App\Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Carbon\Carbon;

trait LoyaltyRateTrait
{
    public function updateInvestmentLoyaltyRate(ClientInvestment $investment)
    {
        $tenor = $investment->client->repo->getConsecutiveMonthsInvested(Carbon::parse($investment->invested_date));

        $investment->loyalty_rate = $this->getLoyaltyRate($tenor);

        $investment->save();
    }

    public function updateUnitPurchasesLoyaltyRate(UnitFundPurchase $purchase)
    {
        $tenor = $purchase->client->repo->getConsecutiveMonthsInvested(Carbon::parse($purchase->date));

        $purchase->loyalty_rate = $this->getLoyaltyRate($tenor);

        $purchase->save();
    }

    /**
     * @param ClientInvestment $investment
     * @param $tenor
     * @return float|null
     */
    protected function getLoyaltyRate($tenor)
    {
        $rate = null;

        if ($tenor < 36) {
            $rate = 0.00025;
        } elseif ($tenor >= 36 && $tenor < 72) {
            $rate = 0.00035;
        } elseif ($tenor >= 72 && $tenor < 132) {
            $rate = 0.00045;
        } elseif ($tenor >= 132) {
            $rate = 0.0005;
        }

        return $rate;
    }
}
