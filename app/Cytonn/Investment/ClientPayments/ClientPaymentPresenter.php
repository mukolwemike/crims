<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\ClientPayments;

use Laracasts\Presenter\Presenter;

class ClientPaymentPresenter extends Presenter
{
    /*
     * Get the payment to
     */
    public function paymentFor()
    {
        if ($this->product) {
            return $this->product->name;
        } elseif ($this->project) {
            return $this->project->name;
        } elseif ($this->shareEntity) {
            return $this->shareEntity->name;
        } elseif ($this->fund) {
            return $this->fund->name;
        } else {
            return '';
        }
    }

    /*
     * Get commission recipient
     */
    public function getCommissionRecipient($bias = '')
    {
        $bias = $bias == '' ? $this->checkPaymentFaBias() : $bias;

        if ($this->client) {
            return $this->client->getLatestFA($bias, false);
        }

        if ($this->recipient) {
            return $this->recipient;
        }

        return null;
    }

    /**
     * @return string
     */
    private function checkPaymentFaBias()
    {
        if ($this->project) {
            return "realestate";
        } elseif ($this->shareEntity) {
            return "shares";
        } elseif ($this->fund) {
            return "units";
        } else {
            return "investments";
        }
    }

    public function commissionRecipientName()
    {
        $fa = $this->getCommissionRecipient();

        if ($fa) {
            return $fa->name;
        }

        return null;
    }
}
