<?php

namespace App\Cytonn\Investment\Commission\Repos;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;

class RecipientRepository
{
    protected $recipient;

    public function __construct(CommissionRecepient $recipient = null)
    {
        is_null($recipient) ? $this->recipient = new CommissionRecepient() : $this->recipient = $recipient;
    }

    public function hasClient(Client $client)
    {
        $investments = $client->investments()->whereHas(
            'commission',
            function ($commission) {
                $commission->where('recipient_id', $this->recipient->id);
            }
        )->count();

        return $investments > 0;
    }
}
