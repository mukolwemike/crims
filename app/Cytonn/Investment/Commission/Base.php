<?php
/**
 * Date: 31/10/2016
 * Time: 10:45
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Core\DataStructures\Carbon;

/**
 * Class Base
 *
 * @package Cytonn\Investment\Commission
 */
class Base
{
    /**
     * The date when commission cuts off, and starts being paid at the end of month
     */
    const COMMISSION_CUT_OFF_DATE = 20;

    /**
     * Don't check compliance for FAs of these types
     */
    const DONT_CHECK_COMPLIANCE = ['ifa', 'fa', 'staff'];

    /**
     * @param ClientInvestment $investment
     * @param $diffInDays
     * @return float
     */
    protected function commissionDifference(ClientInvestment $investment, $diffInDays)
    {
        return $investment->commission->rate * $investment->amount * $diffInDays / (100 * 365);
    }

    /**
     * @param ClientInvestment $investment
     * @return float
     */
    protected function calculateTotalCommission(ClientInvestment $investment)
    {
        return $investment->commission->rate * $investment->amount * $investment->repo->getTenorInDays() / (100 * 365);
    }

    /**
     * @param ClientInvestment $investment
     * @return mixed
     */
    protected function scheduledCommission(ClientInvestment $investment)
    {
        return $investment->commission->schedules()
            ->where('claw_back', false)
            ->sum('amount');
    }

    public function shouldCheckCompliance(CommissionRecepient $recipient)
    {
        return !in_array($recipient->type->slug, static::DONT_CHECK_COMPLIANCE);
    }

    protected function shouldNotCheckCompliance(CommissionRecepient $recipient)
    {
        return !$this->shouldCheckCompliance($recipient);
    }

    protected function tenorInYears(ClientInvestment $investment)
    {
        $days = $investment->maturity_date->copy()->diffInDays($investment->invested_date);

        return (int)($days / 364);
    }

    protected function totalCommission(ClientInvestment $investment, $durationInDays = null)
    {
        if (!$durationInDays) {
            $durationInDays = $investment->repo->getTenorInDays();
        }

        return $investment->amount * $investment->commission->rate * $durationInDays / (100 * 365);
    }


    protected function calculateFirstPayment(ClientInvestment $investment, $months, $month)
    {
        //the month must be divisible by 12
        $not_divisible_by_12 = ($month - 1) % 12 !== 0;

        if ($not_divisible_by_12) {
            return 0;
        }

        //if investment spills over the next year
        if ($months > ($month + 11)) {
            $start = $investment->invested_date->copy();
            $end = $investment->invested_date->copy()->addYear();
            $tenor = $end->diffInDays($start);

            return $this->totalCommission($investment, $tenor) / 2;
        }

        //calculate for rest of year
        $start = $investment->invested_date->copy()->addMonths($month - 1);
        $end = $investment->maturity_date->copy();
        $tenor = $end->diffInDays($start);

        return $this->totalCommission($investment, $tenor) / 2;
    }

    /**
     * Get the first commission payment date for the investment
     *
     * @param  $startDate
     * @return \Carbon\Carbon
     */
    protected function getFirstPaymentDate(\Carbon\Carbon $startDate)
    {
        return $startDate->copy();
    }

    protected function getCommissionSkip(\Carbon\Carbon $startDate)
    {
        $startDate = Carbon::make($startDate);

        $bcp = BulkCommissionPayment::includes($startDate->copy()->startOfMonth())->first();

        $cutOff = $startDate;

        if ($bcp) {
            $cutOff = $bcp->end;
        }

        if ($startDate->daysIntoMonth() > Carbon::make($cutOff)->daysIntoMonth()) {
            return 1;
        }

        return 0;
    }

    /**
     * Get the investment duration in months, minimum is 1
     *
     * @param  ClientInvestment $investment
     * @return int
     */
    protected function getDurationInMonths(ClientInvestment $investment)
    {
        return $investment->repo->getTenorInMonths();
    }

    /**
     * @param ClientInvestment $investment
     * @return float
     */
    protected function getDurationInYears(ClientInvestment $investment)
    {
        return $this->getDurationInMonths($investment) / 12;
    }

    /**
     * Get the amount of one installment
     *
     * @param  ClientInvestment $investment
     * @return float
     */
    protected function getInstallmentAmount(ClientInvestment $investment)
    {
        $totalCommission =
            $investment->commission->rate * $investment->amount * $investment->repo->getTenorInDays() / (100 * 365);

        return 0.5 * $totalCommission / ($this->getDurationInMonths($investment) + 1);
    }
}
