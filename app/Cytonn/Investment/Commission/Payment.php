<?php
/**
 * Date: 31/10/2016
 * Time: 12:43
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;

class Payment
{
    public function make(
        CommissionRecepient $recipient,
        Currency $currency,
        ClientTransactionApproval $approval,
        BulkCommissionPayment $bulk
    ) {
        $calculation = $recipient->calculator($currency, $bulk->start, $bulk->end);

        $summary = $calculation->summary();

        $amount = $summary->finalCommission();

        $clawback = $summary->getClawBacks();

        $earned = $summary->earned();

        $override = $summary->override();

        if ($earned == 0 && $clawback == 0 &&  $override == 0) {
            return;
        }

        return (new CommissionPayment())->create([
            'recipient_id' => $recipient->id,
            'amount' => $amount,
            'claw_back' => $clawback,
            'earned' => $earned,
            'overrides' => $override,
            'currency_id' => $currency->id,
            'date' => $bulk->end,
            'description' => $this->createNarrative($bulk),
            'approval_id' => $approval->id
        ]);
    }

    private function createNarrative(BulkCommissionPayment $bulk)
    {
        return 'Commission Payment for ' . $bulk->start->toFormattedDateString() . ' to ' .
            $bulk->end->toFormattedDateString();
    }
}
