<?php

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Exceptions\CrimsException;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Investment\Commission\Generators\OneTime;
use Cytonn\Investment\Commission\Generators\Periodic;
use Cytonn\Investment\Commission\Generators\Staggered;

class Generator extends Base
{
    const ONE_TIME_START_DATE = '2017-02-01';

    public function createSchedules(ClientInvestment $investment)
    {
        if ($investment->commission->rate == 0 || $investment->repo->getTenorInDays() == 0) {
            return true;
        }

        $form = $investment->approval->topupForm;

        if ($investment->isSeip() && (is_null($form) || is_null($form->investmentSchedule))) {
            return (new Periodic())->generate($investment);
        }

        if ($this->shouldStaggerCommission($investment)) {
            return (new Staggered())->generate($investment);
        }

        return (new OneTime())->generate($investment);
    }

    public function recreateSchedules(ClientInvestment $investment)
    {
        $investment->commission->schedules->each(function ($schedule) {
            $schedule->delete();
        });

        $investment->commission->clawBacks->each(function ($clawBack) {
            $clawBack->delete();
        });

        $this->createSchedules($investment);
    }

    public function createSchedulesForExtension(ClientInvestment $investment, $oldMaturityDate)
    {
        if ($investment->repo->isSeipParent()) {
            return (new Periodic())->extend($investment, $oldMaturityDate);
        }

//        return $this->awardRolloverCommission($investment, $oldMaturityDate);

        if ($this->noPaidCommission($investment)) {
            return $this->recreateSchedules($investment);
        } else {
            if ($this->shouldStaggerCommission($investment)) {
                return (new Staggered())->extend($investment, $oldMaturityDate);
            }

            return (new OneTime())->extend($investment, $oldMaturityDate);
        }
    }

    protected function shouldStaggerCommission(ClientInvestment $investment)
    {
        $cut_off = new Carbon(static::ONE_TIME_START_DATE);

        $before = $investment->invested_date->copy()->lt($cut_off);
        $less_than_year = $this->tenorInYears($investment) < 1;

        return $before || $less_than_year;
    }

    protected function noPaidCommission(ClientInvestment $investment)
    {
        $bulk = BulkCommissionPayment::orderBy('end', 'DESC')->first();

        if (is_null($bulk)) {
            return true;
        }

        return $investment->commission->schedules()->where('date', '<=', $bulk->end)->count() == 0;
    }

    public function awardRolloverCommission(ClientInvestment $newInvestment, $oldMaturity)
    {
        $recipient = $newInvestment->commission->recipient;

        if ($recipient->zero_commission || $newInvestment->client->franchise == 1) {
            return;
        }

        $rate = $recipient->repo->getInvestmentCommissionRate($newInvestment->product, Carbon::now());

        if (is_null($rate)) {
            throw new CrimsException("Could not find commission rate for recipient " . $recipient->id);
        }

        if ($rate->rate == 0) {
            return;
        }

        $additionalDays = Carbon::parse($newInvestment->maturity_date)
            ->diffInDays(Carbon::parse($oldMaturity));

        if ($additionalDays > 90) {
            $rate = $recipient->repo->checkRolloverInvestmentReward(3, Carbon::now(), $rate);
        }

        $addedCommission = ($rate->rate * $newInvestment->amount * $additionalDays / (100 * 365));

        $commission = $newInvestment->commission;

        if ($this->shouldStaggerCommission($newInvestment)) {
            $this->makeStaggeredPayment($newInvestment, $addedCommission);
        } else {
            CommissionPaymentSchedule::create([
                'commission_id' => $commission->id,
                'date' => Carbon::now(),
                'amount' => $addedCommission,
                'description' => 'Bonus for investment extension and Rollover Bonus',
                'first' => false,
                'commission_payment_schedule_type_id' => 2
            ]);
        }
    }

    private function makeStaggeredPayment(ClientInvestment $investment, $addedCommission)
    {
        $months = getDateRange(Carbon::now(), Carbon::parse($investment->maturity_date));

        $monthsCount = count($months);

        if ($monthsCount == 0) {
            return CommissionPaymentSchedule::create([
                'commission_id' => $investment->commission->id,
                'date' => Carbon::now(),
                'amount' => $addedCommission,
                'description' => 'Bonus for investment extension and Rollover Bonus',
                'first' => false,
                'commission_payment_schedule_type_id' => 2
            ]);
        }

        $firstAmount = round($addedCommission / 2, 2);

        $staggerAmount = $addedCommission - $firstAmount;

        $monthlyStaggerAmount = round(($staggerAmount) / $monthsCount, 2);

        $paid = 0;

        foreach ($months as $key => $month) {
            if ($key == 0) {
                CommissionPaymentSchedule::create([
                    'commission_id' => $investment->commission->id,
                    'date' => $month,
                    'amount' => $firstAmount,
                    'description' => 'Bonus for investment extension and Rollover Bonus',
                    'first' => false,
                    'commission_payment_schedule_type_id' => 2
                ]);
            }

            $amountToStagger = $monthsCount == $key + 1 ? $staggerAmount - $paid : $monthlyStaggerAmount;

            CommissionPaymentSchedule::create([
                'commission_id' => $investment->commission->id,
                'date' => $month,
                'amount' => $amountToStagger,
                'description' => 'Bonus for investment extension and Rollover Bonus installment : ' . ($key + 1),
                'first' => false
            ]);

            $paid += $amountToStagger;
        }
    }
}
