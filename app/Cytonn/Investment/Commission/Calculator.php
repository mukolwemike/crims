<?php

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\Currency;
use Carbon\Carbon;

class Calculator extends Base
{
    protected $start;

    protected $end;

    protected $currency;

    public function __construct(Currency $currency, Carbon $start, Carbon $end)
    {
        $this->start = $start;

        $this->end = $end;

        $this->currency = $currency;
    }

    protected function cutOffDate()
    {
        return $this->end;
    }

    protected function lastMonthCutOff()
    {
        return $this->start;
    }
}
