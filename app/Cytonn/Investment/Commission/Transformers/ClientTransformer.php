<?php

namespace App\Cytonn\Investments\Commission\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Payments;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Crims\Investments\Commission\RecipientCalculator as InvRecipientCalculator;
use Crims\Realestate\Commission\RecipientCalculator as CRERecipientCalculator;
use League\Fractal\TransformerAbstract;

class ClientTransformer extends TransformerAbstract
{
    protected $recipient;

    protected $start;

    protected $end;

    public $schedules = false;

    public function __construct(CommissionRecepient $recipient, Carbon $start, Carbon $end)
    {
        $this->recipient = $recipient;
        $this->start = $start->copy();
        $this->end = $end->copy();
    }

    public function transform(Client $client)
    {
        $currency = Currency::where('code', 'KES')->first();

        $products = collect([$this->investments($client, $currency), $this->realEstate($client, $currency)]);

        return [
            'id' => $client->uuid,
            'code' => $client->client_code,
            'name' => $client->present()->fullName,
            'email' => $client->contact->email,
            'since' => $this->clientSince($client),
            'phone' => $client->phone,
            'total' => $products->sum('total'),
            'products' => $products->all(),
            'currency' => $currency->code,
            'active' => $this->active($client),
        ];
    }

    protected function active($client)
    {
        $inv = ClientInvestment::where('client_id', $client->id)->withdrawn(false)->count() > 0;

        $re = UnitHolding::where('client_id', $client->id)->where('active', 1)->count() > 0;

        return $inv || $re;
    }

    protected function investments($client, $currency)
    {
        $cms_calc =
            new InvRecipientCalculator($this->recipient, $currency, $this->start->copy(), $this->end->copy(), $client);

        $out = [
            'name' => 'Investments',
            'total' => $cms_calc->summary()->total(),
            'short_name' => 'CHYS'
        ];

        if ($this->schedules) {
            $out['schedules'] = $cms_calc->compliantSchedulesQuery()
                ->get()
                ->map(
                    function ($sch) {
                        $investment = $sch->commission->investment;
                        return (object)[
                            'description' => $sch->description,
                            'principal' => $investment->amount,
                            'invested_date' => $investment->invested_date->toDateString(),
                            'maturity_date' => $investment->maturity_date->toDateString(),
                            'rate' => $sch->commission->rate,
                            'amount' => $sch->amount,
                        ];
                    }
                );

            $out['claw_backs'] = $cms_calc->clawBackQuery()
                ->get()
                ->map(
                    function ($claw) {
                        $investment = $claw->commission->investment;
                        return (object)[
                            'description' => $claw->narration,
                            'principal' => $investment->amount,
                            'invested_date' => $investment->invested_date->toDateString(),
                            'maturity_date' => $investment->maturity_date->toDateString(),
                            'amount' => $claw->amount
                        ];
                    }
                );
        }

        return (object)$out;
    }

    protected function realEstate($client)
    {
        $re_calc = (new CRERecipientCalculator($this->recipient, $this->start->copy(), $this->end->copy(), $client));

        $out = [
            'name' => 'Real Estate',
            'total' => $re_calc->summary()->total(),
            'short_name' => 'RE'
        ];

        if ($this->schedules) {
            $out['schedules'] = $re_calc->schedules()
                ->map(
                    function ($sch) {
                        $holding = $sch->commission->holding;

                        return (object)[
                            'project' => $holding->project->name,
                            'unit' => $holding->unit->number,
                            'description' => $sch->description,
                            'date' => $sch->date,
                            'amount' => $sch->amount
                        ];
                    }
                );
        }

        return (object)$out;
    }

    private function clientSince(Client $client)
    {
        $inv = $client->investments()->oldest('invested_date')->first();
        $holdings = $client->unitHoldings;

        $re = Payments::whereIn('holding_id', $holdings->lists('id'))->oldest('date')->first();

        if ($inv && $re) {
            if ($re->date->lt($inv->invested_date)) {
                return $re->date;
            }

            return $inv->date;
        }

        if ($inv) {
            return $inv->invested_date->toDateString();
        }
        if ($re) {
            return $re->date->toDateString();
        }

        return null;
    }
}
