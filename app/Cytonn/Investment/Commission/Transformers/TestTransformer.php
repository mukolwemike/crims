<?php

namespace App\Cytonn\Investments\Commission\Transformers;

use App\Cytonn\Investments\Commission\Recipient;
use App\Cytonn\Models\Client;
use League\Fractal\TransformerAbstract;

class TestTransformer extends TransformerAbstract
{
    protected $recipient;
    
    public function __construct(Recipient $recipient)
    {
        $this->recipient = $recipient;
    }
    
    public function transform(Client $client)
    {
        return [];
    }
}
