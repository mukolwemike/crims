<?php

namespace App\Cytonn\Investments\Commission\Transformers;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\RealEstate\Commissions\ReProjectionsCalculator;
use Carbon\Carbon;
use Cytonn\Investment\Commission\ProjectionsCalculator;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;
use function collect;

class ProjectionsTransformer extends TransformerAbstract
{
    protected $recipient;

    protected $months;

    public function __construct(CommissionRecepient $recipient, array $months)
    {
        $this->recipient = $recipient;
        $this->months = $months;
    }

    public function transform(Client $client)
    {
        $currency = Currency::where('code', 'KES')->first();

        $commission = collect($this->months)->map(
            function ($month) use ($client, $currency) {
                $inv = (new ProjectionsCalculator($this->recipient, $currency, $month->start, $month->end))
                    ->calculate($client);
                $re = (new ReProjectionsCalculator($this->recipient, $client, $month->start, $month->end))
                    ->reProjections();

                return (object)[
                    'month' => $month->start->startOfMonth()->toDateString(),
                    'on_retainer' => [
                        'investments' => $inv->sum('on_retainer'),
                        'real_estate' => $re->sum('on_retainer')
                    ],
                    'off_retainer' => [
                        'investments' => $inv->sum('off_retainer'),
                        'real_estate' => $re->sum('off_retainer')
                    ]
                ];
            }
        )->all();

        return [
            'id' => $client->uuid,
            'code' => $client->client_code,
            'name' => $client->present()->fullName,
            'projections' => $commission
        ];
    }

    private function getForMonth(Collection $collection, Carbon $month)
    {
        return $collection->filter(
            function ($item) use ($month) {
                $m = Carbon::parse($item->month);
                return $m->copy()->gte($month->copy()->startOfMonth()) && $m->copy()->lte($month->copy()->endOfMonth());
            }
        )->first();
    }

    private function getTotals($commission)
    {
        $clientTotals = 0;

        $clientTotals += $commission->map(
            function ($comm) {
                return ['total' => $comm->totals];
            }
        )->sum('total');

        return $clientTotals;
    }

    private function getInvTotals($commission)
    {
        $invTotal = 0;

        $invTotal += $commission->map(
            function ($inv) {
                return ['total' => $inv->inv_total];
            }
        )->sum('total');

        return $invTotal;
    }

    private function getReTotals($commission)
    {
        $reTotals = 0;
        $reTotals += $commission->map(
            function ($re) {
                return ['total' => $re->re_total];
            }
        )->sum('total');

        return $reTotals;
    }
}
