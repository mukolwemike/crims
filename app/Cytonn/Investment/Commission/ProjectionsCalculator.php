<?php
/**
 * Created by PhpStorm.
 * User: yeric
 * Date: 11/1/17
 * Time: 11:11 AM
 */

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Crims\Investments\Commission\Models\Recipient;
use Crims\Investments\Commission\RecipientCalculator;
use Cytonn\Investment\Commission\Projections\Projector;

class ProjectionsCalculator extends Projector
{
    protected $recipient;
    protected $start;
    protected $end;
    protected $currency;

    public function __construct(Recipient $recipient, Currency $currency, Carbon $start, Carbon $end)
    {
        $this->recipient = $recipient;
        $this->start = $start;
        $this->end = $end;
        $this->currency = $currency;
    }

    public function calculate(Client $client = null)
    {
        $calculator = (new RecipientCalculator($this->recipient, $this->currency, $this->start, $this->end, $client));

        return $calculator->compliantSchedulesQuery()
            ->get()
            ->each(
                function ($schedule) {
                    $schedule->month = Carbon::parse($schedule->date)->startOfMonth()->toDateString();
                    $schedule->converted_amount = $this->convert($schedule->amount, $schedule->commission->rate);
                }
            )->groupBy('month')
            ->map(
                function ($group) {
                    return (object)[
                        'month' => $group->first()->month,
                        'amount' => $group->sum('converted_amount')
                    ];
                }
            );
    }

    private function convert($amount, $rate)
    {
        if (is_null($this->retained)) {
            return $amount;
        }

        $converted_rate = $this->calculateRate($this->getRequiredType(), $rate);

        return ($converted_rate / $rate) * $amount;
    }


    private function calculateRate($name, $default)
    {
        $key = 'investment_commission_rate_currency' . $this->currency->id . '_type_' . $name;

        if (cache()->has($key)) {
            return cache()->get($key);
        }

        $rate = CommissionRate::where('currency_id', $this->currency->id)
            ->where('name', 'like', '%' . $name . '%')
            ->first();

        if ($rate) {
            $r = $rate->rate;
            cache()->put([$key => $r], 10);
            return $r;
        }

        return $default;
    }
}
