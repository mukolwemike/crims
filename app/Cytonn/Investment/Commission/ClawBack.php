<?php

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Investment\Commands\RegisterCommissionClawBackCommandHandler;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\DatePresenter;

class ClawBack
{
    protected $investment;

    protected $withdrawal;

    public function __construct(ClientInvestment $investment, ClientInvestmentWithdrawal $withdrawal)
    {
        $this->investment = $investment->fresh();
        $this->withdrawal = $withdrawal;
    }

    public function onWithdrawal()
    {
        if ($this->investment->isSeip()) {
            return $this->processSeipClawback();
        }

        $prepared = $this->getPrepared();

        $type = "withdrawal";

        if (!$this->validate(null, $prepared)) {
            return;
        }

        $handler = new RegisterCommissionClawBackCommandHandler();

        $clawBackAmount = $this->getClawbackAmount();

        $unpaid = $handler->unPaidSchedules($this->investment, $this->withdrawal->date);

        $unpaidAmount = $unpaid->sum('amount');

        $clawbackDifference = $clawBackAmount - $unpaidAmount;

        if ($clawbackDifference == 0) {
            $this->clearUnpaidSchedules($unpaid);
            return;
        }

        if ($clawbackDifference > 0) {
            $this->clearUnpaidSchedules($unpaid);
            $this->clawBack($clawbackDifference, $this->withdrawal, $type);
            return;
        }

        if ($clawbackDifference < 0) {
            $deserved = abs($clawbackDifference);

            if ($prepared && $prepared->principal < 1) {
                $this->clearUnpaidSchedules($unpaid);
                $this->generateSchedule($deserved);
            } else {
                $this->spreadRemainingCommission($deserved, $unpaid);
            }

            return;
        }
    }

    private function processSeipClawback()
    {
        $handler = new RegisterCommissionClawBackCommandHandler();

        $unpaid = $handler->unPaidSchedules($this->investment, $this->withdrawal->date);

        $this->clearUnpaidSchedules($unpaid);

        return;
    }

    private function clearUnpaidSchedules($unpaid)
    {
        $unpaid->each(function ($schedule) {
            $schedule->claw_back = true;
            $schedule->save();
        });
    }

    public function onRollover($principal, Carbon $new, Carbon $original)
    {
        $type = "rollover";

        $tenor = $original->diffInDays($new);

        $amount = $principal * $this->investment->commission->rate * $tenor / (100 * 365);

        if ($amount <= 0) {
            return;
        }

        $handler = new RegisterCommissionClawBackCommandHandler();

        $clawBackAmount = $amount;

        $unpaid = $handler->unPaidSchedules($this->investment, $this->withdrawal->date);
        $unpaidAmount = $unpaid->sum('amount');

        $clawbackDifference = $clawBackAmount - $unpaidAmount;

        if ($clawbackDifference == 0) {
            $this->clearUnpaidSchedules($unpaid);
            return;
        }

        if ($clawbackDifference > 0) {
            $this->clearUnpaidSchedules($unpaid);
            $this->clawBack($clawbackDifference, $this->withdrawal, $type);
            return;
        }

        if ($clawbackDifference < 0) {
            $this->clearUnpaidSchedules($unpaid);
            $this->generateSchedule(abs($clawbackDifference));
            return;
        }

        $this->clawBack($amount, $this->withdrawal, $type);
    }

    private function getPrepared()
    {
        return collect($this->investment->calculate(
            $this->withdrawal->date,
            true,
            null,
            false,
            true
        )->getPrepared()->actions)
            ->filter(
                function ($action) {
                    if (isset($action->action)) {
                        if ($action->action instanceof ClientInvestmentWithdrawal) {
                            return $action->action->id == $this->withdrawal->id;
                        }
                    }

                    return false;
                }
            )->first();
    }

    private function validate(Carbon $date = null, $prepared)
    {
        $maturity = $date ? $date->copy() : $this->investment->maturity_date->copy();

        if ($this->withdrawal->date->toDateString() == $maturity->toDateString()) {
            return;
        }

        if ($this->investment->commission->rate == 0) {
            return;
        }

        $amount = 0;
        if ($prepared) {
            $amount =  $prepared->original_principal - $prepared->principal;
        }

        if ($amount <= 1) {
            return;
        }

        return true;
    }

    private function getClawbackAmount(Carbon $date = null)
    {
        $maturity = $date ? $date->copy() : $this->investment->maturity_date->copy();

        $tenor = $maturity->diffInDays($this->withdrawal->date);

        return $this->withdrawal->amount * $this->investment->commission->rate * $tenor / (100 * 365);
    }

    private function clawBack($amount, $withdrawal, $type)
    {
        return CommissionClawback::create([
            'amount' => $amount,
            'commission_id' => $this->investment->commission->id,
            'date' => $this->withdrawal->date,
            'narration' => $this->narration($withdrawal, $type),
            'type' => 'withdraw',
            'withdrawal_id' => $withdrawal->id
        ]);
    }

    private function generateSchedule($amount)
    {
        CommissionPaymentSchedule::create([
            'commission_id' => $this->investment->commission->id,
            'amount' => $amount,
            'first' => true,
            'full' => true,
            'date' => $this->withdrawal->date,
            'description' => 'Full commission payment'
        ]);
    }

    private function narration($withdrawal, $type)
    {
        $client = $withdrawal->investment->client;

        $withdrawn = AmountPresenter::currency($withdrawal->amount);
        $principal = $withdrawal->investment->amount;
        $date = DatePresenter::formatDate($withdrawal->date);
        $name = $client->present()->fullName;

        $typeDesc = ($type == 'rollover') ? "Premature rollover" : "Premature withdrawal";

        return "$typeDesc Client: $name Code: {$client->client_code} 
        Principal: $principal Date: $date Amount withdrawn: $withdrawn";
    }

    private function spreadRemainingCommission($amount, $unpaid)
    {
        if ($amount <= 0 || $unpaid->count() == 0 || ($unpaid->sum('amount') == 0)) {
            return;
        }

        $ratio = $amount/($unpaid->sum('amount'));

        $unpaid->each(
            function (CommissionPaymentSchedule $schedule) use ($ratio) {
                $amount = $schedule->amount;
                $schedule->update(['amount' => $amount * $ratio]);
            }
        );
    }

    private function remainingCommission($principal)
    {
        $tenor = $this->investment->maturity_date->copy()->diffInDays($this->withdrawal->date);

        return $principal * $this->investment->commission->rate * $tenor / (100 * 365);
    }
}
