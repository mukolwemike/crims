<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 04/06/2018
 * Time: 13:10
 */

namespace Cytonn\Investment\Commission\Projections;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Investment\Commission\ProjectionsCalculator;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Realestate\Commissions\Calculator\ReProjectionsCalculator;

class CommissionProjection
{
    public function getProjections(CommissionRecepient $fa, Carbon $start, Carbon $end)
    {
        $clients = $this->getClients($start, $end, $fa)->get();

        $months = $this->getMonths($start, $end);

        return $clients->map(
            function (Client $client) use ($months, $fa) {
                $out = [
                    'Client code' => $client->client_code,
                    'Name' => ClientPresenter::presentJointFullNames($client->id),
                ];

                $out['total_commission'] = 0;

                foreach ($months as $month) {
                    $month = Carbon::parse($month);
                    $commission = $this->project($fa, $client, $month);
                    $out[$month->format('M, Y')] = $commission;
                    $out['total_commission'] += $commission;
                }

                return $out;
            }
        );
    }

    private function project($fa, $client, $month)
    {
        $currency = Currency::where('code', 'KES')->first();

        $dates = $this->commissionDates($month);

        $inv = (new ProjectionsCalculator($fa, $currency, $dates->start, $dates->end))
            ->setRetained(null)
            ->calculate($client)
            ->sum('amount');
        $re = (new ReProjectionsCalculator($fa, $dates->start, $dates->end))
            ->setRetained(null)
            ->calculate($client)
            ->sum('amount');

        return $re + $inv;
    }

    private function getClients($start, $end, CommissionRecepient $recipient)
    {
        $client = new Client();

        $query =  $client->whereHas(
            'investments',
            function ($investments) use ($start, $end, $recipient) {
                $investments->whereHas(
                    'commission',
                    function ($commission) use ($start, $end, $recipient) {
                        $commission->where('recipient_id', $recipient->id);
                    }
                );
            }
        )->orWhereHas(
            'unitHoldings',
            function ($holding) use ($start, $end, $recipient) {
                $holding->whereHas(
                    'paymentSchedules',
                    function ($paymentSchedule) use ($start, $end, $recipient) {
                    }
                )->whereHas(
                    'commission',
                    function ($commission) use ($recipient) {
                        $commission->where('recipient_id', $recipient->id);
                    }
                );
            }
        );

        return clone $query;
    }

    private function getMonths(Carbon $startDate, Carbon $endDate)
    {
        $months = [];

        for ($start = $startDate->copy(); $start->lte($endDate->copy()); $start = $start->addMonthNoOverflow()) {
            $months[] = $start->copy()->startOfMonth();
        }

        return $months;
    }

    protected function commissionDates(Carbon $date)
    {
        $bulk = $this->getBulk($date);

        if ($bulk) {
            return (object)[
                'start'=>$bulk->start,
                'end'=>$bulk->end
            ];
        }

        return (object)[
            'start'=> $this->getStart($date),
            'end' => $this->getEnd($date)
        ];
    }

    private function getStart(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->subMonthNoOverflow())) {
            return $bulk->end->copy()->addDay();
        }

        return $date->copy()->subMonthNoOverflow()->startOfMonth()->addDays(20);
    }

    private function getEnd(Carbon $date)
    {
        if ($bulk = $this->getBulk($date->copy()->addMonthNoOverflow())) {
            return $bulk->start->copy()->subDay();
        }

        return $date->copy()->startOfMonth()->addDays(19);
    }

    private function getBulk($date)
    {
        return BulkCommissionPayment::where('start', '<=', $date)->where('end', '>=', $date)->first();
    }
}
