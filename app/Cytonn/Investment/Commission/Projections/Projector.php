<?php
/**
 * Date: 11/12/2017
 * Time: 15:36
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Commission\Projections;

abstract class Projector
{
    protected $retained = null;

    public function setRetained(bool $retained = null)
    {
        $this->retained = $retained;

        return $this;
    }

    protected function getRequiredType()
    {
        $name = 'staff';

        if ($this->retained) {
            $name = 'ifa';
        }

        return $name;
    }
}
