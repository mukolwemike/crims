<?php

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Events\Investments\Actions\MaturityDateEdited;
use Carbon\Carbon;
use Cytonn\Investment\Commands\RegisterCommissionClawBackCommand;
use Laracasts\Commander\CommanderTrait;

class InvestmentEdit
{
    use CommanderTrait;

    public static function handle(
        ClientInvestment $oldInvestment,
        ClientInvestment $newInvestment,
        ClientTransactionApproval $approval,
        array $editedColumns
    ) {
        $generator = new Generator();

        //principal
        if ($newInvestment->amount != $oldInvestment->amount) {
            return $generator->recreateSchedules($newInvestment);
        }

        //value date
        if ($newInvestment->invested_date->copy()->gt($oldInvestment->invested_date)) {
            return $generator->recreateSchedules($newInvestment);
        } elseif ($newInvestment->invested_date->copy()->ne($oldInvestment->invested_date)) {
            return $generator->recreateSchedules($newInvestment);
        }

        //maturity date
        if ($newInvestment->maturity_date->copy()->gt($oldInvestment->maturity_date)) {
            //extension
            $newInvestment->maturity_notification_sent_on = null;
            $newInvestment->save();

            $generator->createSchedulesForExtension($newInvestment, $oldInvestment->maturity_date);

            if (Carbon::parse($newInvestment->maturity_date)
                    ->diffInDays(Carbon::parse($oldInvestment->maturity_date)) >= 90) {
                event(new MaturityDateEdited($newInvestment, $oldInvestment));
            }

            return;
        } elseif ($newInvestment->maturity_date->copy()->ne($oldInvestment->maturity_date)) {
            //claw back
            return (new static())->execute(
                RegisterCommissionClawBackCommand::class,
                ['investment' => $oldInvestment, 'newInvestment' =>
                    $newInvestment, 'approval' => $approval, 'edit' => true]
            );
        }

        //commission rate
        if (isset($editedColumns['commission_rate']) || isset($editedColumns['commission_recepient'])) {
            return $generator->recreateSchedules($newInvestment);
        }

        return null;
    }
}
