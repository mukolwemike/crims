<?php
/**
 * Date: 10/04/2017
 * Time: 08:58
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Commission;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;

interface GeneratorInterface
{
    public function generate(ClientInvestment $investment);

    public function extend(ClientInvestment $investment, Carbon $oldMaturityDate);
}
