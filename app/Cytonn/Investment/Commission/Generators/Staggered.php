<?php
/**
 * Date: 10/04/2017
 * Time: 08:57
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Commission\Generators;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Base;
use Cytonn\Investment\Commission\GeneratorInterface;

class Staggered extends Base implements GeneratorInterface
{
    /**
     * @param ClientInvestment $investment
     * @return CommissionPaymentSchedule|\Illuminate\Database\Eloquent\Model
     */
    public function generate(ClientInvestment $investment)
    {
        $investedDate = $investment->invested_date;

        $commissionAmount = $this->totalCommission($investment);

        $commissionStartDate = ($investment->commission && $investment->commission->start_date)
            ? Carbon::parse($investment->commission->start_date) : null;

        $months = getDateRange($investedDate, Carbon::parse($investment->maturity_date));

        $monthsCount = count($months);

        if ($monthsCount == 0) {
            return CommissionPaymentSchedule::create([
                'commission_id' => $investment->commission->id,
                'date' => $this->getScheduleDate($investedDate, $commissionStartDate),
                'amount' => $commissionAmount,
                'description' => 'Upfront commission payment',
                'first' => true,
                'commission_payment_schedule_type_id' => 1
            ]);
        }

        $firstAmount = round($commissionAmount / 2, 2);

        $staggerAmount = $commissionAmount - $firstAmount;

        $monthlyStaggerAmount = round(($staggerAmount) / $monthsCount, 2);

        $paid = 0;

        foreach ($months as $key => $month) {
            if ($key == 0) {
                CommissionPaymentSchedule::create([
                    'commission_id' => $investment->commission->id,
                    'date' => $this->getScheduleDate($month, $commissionStartDate),
                    'amount' => $firstAmount,
                    'description' => 'Upfront commission payment',
                    'first' => true,
                    'commission_payment_schedule_type_id' => 1
                ]);
            }

            $amountToStagger = $monthsCount == $key + 1 ? $staggerAmount - $paid : $monthlyStaggerAmount;

            CommissionPaymentSchedule::create([
                'commission_id' => $investment->commission->id,
                'date' => $this->getScheduleDate($month, $commissionStartDate),
                'amount' => $amountToStagger,
                'description' => 'Commission installment : ' . ($key + 1),
                'first' => false,
                'commission_payment_schedule_type_id' => 1
            ]);

            $paid += $amountToStagger;
        }

        return $investment->commission->schedules;
    }

    /**
     * @param Carbon $scheduledDate
     * @param Carbon|null $commissionStartDate
     * @return Carbon
     */
    private function getScheduleDate(Carbon $scheduledDate, Carbon $commissionStartDate = null)
    {
        return $commissionStartDate && $scheduledDate->lt($commissionStartDate) ? $commissionStartDate : $scheduledDate;
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $oldMaturityDate
     */
    public function extend(ClientInvestment $investment, Carbon $oldMaturityDate)
    {
        if ($investment->commission->schedules()->count() == 0) {
            return $this->generate($investment);
        }

        $bonusDate = $this->getFirstPaymentDate(Carbon::today());

        $commissionAmount = $this->commissionDifference($investment, $investment->maturity_date->copy()
            ->diffInDays($oldMaturityDate));

        $months = getDateRange($oldMaturityDate, Carbon::parse($investment->maturity_date));

        $monthsCount = count($months);

        if ($monthsCount == 0) {
            return CommissionPaymentSchedule::create([
                'commission_id' => $investment->commission->id,
                'date' => $bonusDate,
                'amount' => $commissionAmount,
                'description' => 'Bonus for investment extension',
                'first' => false,
                'commission_payment_schedule_type_id' => 1
            ]);
        }

        $firstAmount = round($commissionAmount / 2, 2);

        $staggerAmount = $commissionAmount - $firstAmount;

        $monthlyStaggerAmount = round(($staggerAmount) / $monthsCount, 2);

        $paid = 0;

        foreach ($months as $key => $month) {
            if ($key == 0) {
                CommissionPaymentSchedule::create([
                    'commission_id' => $investment->commission->id,
                    'date' => $bonusDate,
                    'amount' => $firstAmount,
                    'description' => 'Bonus for investment extension',
                    'first' => true,
                    'commission_payment_schedule_type_id' => 1
                ]);
            }

            $amountToStagger = $monthsCount == $key + 1 ? $staggerAmount - $paid : $monthlyStaggerAmount;

            CommissionPaymentSchedule::create([
                'commission_id' => $investment->commission->id,
                'date' => $month,
                'amount' => $amountToStagger,
                'description' => 'Extension Bonus Commission installment : ' . ($key + 1),
                'first' => false,
                'commission_payment_schedule_type_id' => 1
            ]);

            $paid += $amountToStagger;
        }
    }
}
