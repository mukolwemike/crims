<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\Commission\Generators;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Base;
use Cytonn\Investment\Commission\GeneratorInterface;
use Cytonn\Models\Investment\PeriodicCommissionRate;

class Periodic extends Base implements GeneratorInterface
{
    /**
     * @param ClientInvestment $investment
     * @param int $startTenor
     * @return mixed
     */
    public function generate(ClientInvestment $investment, $startTenor = 0)
    {
        $commission = $investment->commission;

        $investedDate = Carbon::parse($investment->invested_date);

        $maturityDate = Carbon::parse($investment->maturity_date);

        $startDate = is_null($commission->commission_start_date) ? $investedDate->copy()
            : Carbon::parse($commission->commission_start_date);

        $rates = $this->getPeriodicRates($investment);

        $tenor = $investment->repo->getTenorInDays();

        foreach ($rates as $key => $rate) {
            $cutoffDays = $rate->tenor * 365;

            $endTenor = $tenor >= $cutoffDays ? $cutoffDays : $tenor;

            $inPeriodDays = $endTenor - $startTenor;

            if ($inPeriodDays <= 0) {
                continue;
            }

            $inPeriodDays = $inPeriodDays <= 365 ? $inPeriodDays : 365;

            $commissionAmount = $investment->amount * $rate->rate * $inPeriodDays / (100 * 365);

            $startTenor = $endTenor;

            $date = $investedDate->copy()->addYears($rate->tenor - 1);

            $date = $date <= $startDate ? $startDate : $date;

            $date = $date <= $maturityDate ? $date : $maturityDate;

            CommissionPaymentSchedule::create([
                'commission_id' => $commission->id,
                'date' => $date,
                'amount' => $commissionAmount,
                'description' => 'Commission payment for Year ' . $rate->tenor,
                'first' => $key == 0,
                'commission_payment_schedule_type_id' => 1
            ]);
        }

        return $investment->commission->schedules;
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $oldMaturityDate
     * @return mixed
     */
    public function extend(ClientInvestment $investment, Carbon $oldMaturityDate)
    {
        $oldTenor = $oldMaturityDate->diffInDays(Carbon::parse($investment->invested_date));

        return $this->generate($investment, $oldTenor);
    }

    /**
     * @param ClientInvestment $investment
     * @return mixed
     */
    private function getPeriodicRates(ClientInvestment $investment)
    {
        $rates = $investment->product->periodicRatesAsAt($investment->invested_date);

        $tenor = $investment->repo->getTenorInDays();

        return $rates->filter(function (PeriodicCommissionRate $periodicCommissionRate) use ($tenor) {
            return $periodicCommissionRate->tenor <= ceil($tenor / 365);
        });
    }
}
