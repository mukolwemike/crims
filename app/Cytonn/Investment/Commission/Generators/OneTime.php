<?php
/**
 * Date: 10/04/2017
 * Time: 08:57
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Commission\Generators;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CommissionPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Base;
use Cytonn\Investment\Commission\GeneratorInterface;

/**
 * Class OneTime
 *
 * @package Cytonn\Investment\Commission\Generators
 */
class OneTime extends Base implements GeneratorInterface
{
    /**
     * @param ClientInvestment $investment
     * @return mixed
     */
    public function generate(ClientInvestment $investment)
    {
        $commission = $investment->commission;

        $amount = $this->totalCommission($investment);

        $commissionStartDate = ($commission->start_date) ? Carbon::parse($commission->start_date) : null;

        CommissionPaymentSchedule::create([
            'commission_id' => $commission->id,
            'amount' => $amount,
            'first' => true,
            'full' => true,
            'date' => ($commissionStartDate)
                ? $commissionStartDate : $this->getFirstPaymentDate($investment->invested_date),
            'description' => 'Full commission payment'
        ]);

        return $investment->commission->schedules;
    }


    /**
     * @param ClientInvestment $investment
     * @param Carbon $oldMaturityDate
     */
    public function extend(ClientInvestment $investment, Carbon $oldMaturityDate)
    {
        $diffInCommission =
            $this->commissionDifference($investment, $investment->maturity_date->copy()->diffInDays($oldMaturityDate));

        $bonus_date = $this->getFirstPaymentDate(Carbon::today());

        //make first installment
        CommissionPaymentSchedule::create([
            'commission_id' => $investment->commission->id,
            'date' => $bonus_date,
            'amount' => abs($diffInCommission),
            'description' => 'Bonus for investment extension',
            'first' => false
        ]);
    }
}
