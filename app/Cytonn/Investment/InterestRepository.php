<?php
    /**
     * Date: 05/11/2015
     * Time: 11:10 AM
     *
     * @author Mwaruwa Chaka <mchaka@cytonn.com>
     * Project: crm
     * Cytonn Technology
     */

    namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\InterestPaymentSchedule;
use Carbon\Carbon;
use Cytonn\Investment\InterestPayments\ScheduleGenerator;
use Illuminate\Support\Collection;
use Cytonn\Presenters\ClientPresenter;

/**
     * Class InterestRepository
     *
     * @package Cytonn\Investment
     */
class InterestRepository
{

    /**
     * @param ClientInvestment $investment
     * @return bool
     */
    public function generateInterestScheduleForInvestment(ClientInvestment $investment)
    {
        return (new ScheduleGenerator())->generate($investment);
    }

    /**
     * @param ClientInvestment $investment
     * @param ClientInvestment $oldInvestment
     */
    public function reGenerateInterestScheduleForInvestment(
        ClientInvestment $investment,
        ClientInvestment $oldInvestment = null
    ) {
        $generate = function () use ($investment) {
            $investment->interestSchedules()->forceDelete();

            $this->generateInterestScheduleForInvestment($investment);
        };

        //check if interest payment information has changes
        if ($oldInvestment) {
            if ($investment->amount != $oldInvestment->amount
                || $investment->interest_rate != $oldInvestment->interest_rate
                || $investment->interest_payment_date != $oldInvestment->interest_payment_date
                || $investment->interest_payment_interval != $oldInvestment->interest_payment_interval
                || $investment->interest_payment_start_date != $oldInvestment->interest_payment_start_date
                || $investment->maturity_date != $oldInvestment->maturity_date
            ) {
                $generate();
            }
        } else {
            $generate();
        }
    }


    /**
     * @return Collection
     */
    public function getAllInterestSchedules()
    {
        $schedules = InterestPaymentSchedule::all();

        $interestScheduleCollection = new Collection();
        foreach ($schedules as $schedule) {
            $inv = ClientInvestment::find($schedule->investment_id);
            $schedule->investment_amount = $inv->amount;
            $schedule->client_name = ClientPresenter::presentFullNames($inv->client_id);
            $interestScheduleCollection->push($schedule);
        }

        return $interestScheduleCollection;
    }

    /**
     * Get the scheduled payments for this month
     *
     * @return mixed
     */
    public function getScheduledThisMonth($date = null)
    {
        return  InterestPaymentSchedule::where(
            'date',
            '>=',
            (new Carbon($date))->startOfMonth()
        )->where('date', '<=', (new Carbon($date))->endOfMonth())->get();
    }

    /**
     * @param null $date
     * @return static
     */
    public function getClientsWithSchedulesThisMonth($date = null)
    {
        $schedules = $this->getScheduledThisMonth($date);

        $client_coll = new Collection();

        foreach ($schedules as $schedule) {
            $client_coll->push($schedule->investment->client);
        }

        return $client_coll->unique();
    }

    /**
     * @param $client
     * @param $product
     * @param null    $date
     * @return Collection
     */
    public function getClientsSchedulesForProductThisMonth($client, $product, $date = null)
    {
        $investments = ClientInvestment::where('client_id', $client->id)
            ->where('product_id', $product->id)->get();

        $schedule_col = new Collection();

        foreach ($investments as $investment) {
            $schedules = InterestPaymentSchedule::where('investment_id', $investment->id)
                ->where('date', '>=', (new Carbon($date))->startOfMonth())
                ->where('date', '<=', (new Carbon($date))->endOfMonth())->get();

            foreach ($schedules as $schedule) {
                $schedule_col->push($schedule);
            }
        }
        return $schedule_col;
    }

    /**
     * @param $client
     * @param $product
     * @param null    $date
     * @return mixed
     */
    public function getTotalInterestForClientForProductThisMonth($client, $product, $date = null)
    {
        $schedules = $this->getClientsSchedulesForProductThisMonth($client, $product, $date);
        return $schedules->sum('amount');
    }

    /**
     * @param $client
     * @param $startDate
     * @param $endDate
     * @return Collection
     */
    public function getSchedulesForClientForAPeriod($client, $startDate, $endDate)
    {
        $investments = ClientInvestment::where('client_id', $client)->get();

        $scheduleCollection = new Collection();
        foreach ($investments as $investment) {
            $schedulesThisMonth = InterestPaymentSchedule::where('investment_id', $investment->id)
                ->where('date', '>=', (new Carbon($startDate)))
                ->where('date', '<=', (new Carbon($endDate)))->get();
            foreach ($schedulesThisMonth as $schedule) {
                $schedule->investment = $investment->amount;
                $schedule->client_name = ClientPresenter::presentFullNames($client);
                $scheduleCollection->push($schedule);
            }
        }

        return $scheduleCollection;
    }
}
