<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\Rates;

use Laracasts\Presenter\Presenter;

class RatePresenter extends Presenter
{
    /*
     * Get the name for the rate
     */
    public function getTenorName()
    {
        if ($this->tenor == 0) {
            return '1 month';
        }

        if ($this->tenor_type != 2) {
            return $this->tenor == 1 ? $this->tenor . ' ' . 'month' : $this->tenor . ' ' . 'months';
        } else {
            return $this->tenor == 1 ? $this->tenor . ' ' . 'year' : $this->tenor . ' ' . 'years';
        }
    }
}
