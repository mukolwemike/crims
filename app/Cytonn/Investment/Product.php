<?php

namespace App\Cytonn\Investment;

use App\Cytonn\BaseModel;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Type;
use Cytonn\Investment\Products\ProductRepository;

class Product extends BaseModel
{
    public $repo;

    public function __construct()
    {
        $this->repo = new ProductRepository($this);
        parent::__construct();
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function fundManager()
    {
        return $this->belongsTo(FundManager::class);
    }

    public function investments()
    {
        return $this->hasMany(ClientInvestment::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }
    
    public function rateUpdates()
    {
        return $this->hasMany(InterestRateUpdate::class);
    }
}
