<?php

namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;

/**
 * Class RollbackInvestmentCommand
 *
 * @package Cytonn\Investment\Commands
 */
class RollbackInvestmentCommand
{

    /**
     * @var ClientTransactionApproval
     */
    public $approval;

    /**
     * @var ClientInvestment
     */
    public $investment;


    /**
     * @param ClientInvestment          $investment
     * @param ClientTransactionApproval $approval
     */
    public function __construct(ClientInvestment $investment, ClientTransactionApproval $approval)
    {
        $this->approval = $approval;

        $this->investment = $investment;
    }
}
