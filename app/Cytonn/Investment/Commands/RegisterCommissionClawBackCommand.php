<?php

namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;

class RegisterCommissionClawBackCommand
{
    public $investment;

    public $newInvestment;

    public $edit = false;
    /**
     * @var ClientTransactionApproval
     */
    public $approval;

    /**
     * RegisterCommissionClawBackCommand constructor.
     *
     * @param ClientInvestment          $investment
     * @param ClientInvestment          $newInvestment
     * @param ClientTransactionApproval $approval
     * @param null|bool                 $edit
     */
    public function __construct(
        ClientInvestment $investment,
        ClientInvestment $newInvestment = null,
        ClientTransactionApproval $approval,
        $edit = false
    ) {
        $this->investment = $investment;
        $this->newInvestment = $newInvestment;
        $this->edit = $edit;
        $this->approval = $approval;
    }
}
