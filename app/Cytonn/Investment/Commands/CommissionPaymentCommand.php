<?php namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\ClientTransactionApproval;

class CommissionPaymentCommand
{
    public $approval;

    /**
     */
    public function __construct(ClientTransactionApproval $approval)
    {
        $this->approval = $approval;
    }
}
