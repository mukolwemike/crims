<?php

namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\ClientTax;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\InterestPayment;
use Cytonn\Investment\Action\Base;
use Cytonn\Investment\BaseInvestment;
use Cytonn\Investment\Events\InvestmentHasBeenRolledBack;
use Cytonn\Presenters\ClientPresenter;
use DB;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class RollbackInvestmentCommandHandler extends Base implements CommandHandler
{
    use EventGenerator, DispatchableTrait;

    /**
     * Handle the command.
     * Reverse the investment and all its associated records:
     * Custodial transactions, interest payments, commission payments, tax and the investment itself
     *
     * @param  object $command
     * @return void
     * @throws \Throwable
     */
    public function handle($command)
    {
        DB::transaction(
            function () use ($command) {
                $investment = $command->investment;

                //custodial
                $this->rollbackCustodial($investment);

                //interest
                $this->rollbackInterest($investment);
                //commission
                $this->rollbackCommission($investment);
                //tax
                $this->rollbackTax($investment);

                //finally the investment
                $investment->delete();

                $this->raise(new InvestmentHasBeenRolledBack($investment));

                $this->dispatchEventsFor($this);
            }
        );
    }

    /**
     * Rollback commission schedules and payments
     *
     * @param $investment
     */
    private function rollbackCommission($investment)
    {
        $schedules = $investment->commission->schedules;

        foreach ($schedules as $schedule) {
            $schedule->delete();
        }

        $investment->commission->delete();
    }

    /**
     * Rollback interest schedules and payment
     *
     * @param $investment
     */
    private function rollbackInterest($investment)
    {
        $payments = InterestPayment::where('investment_id', $investment->id)->get();

        foreach ($payments as $p) {
            $p->delete();
        }

        $schedules = $investment->interestSchedules;

        foreach ($schedules as $schedule) {
            $schedule->delete();
        }
    }

    /**
     * Rollback any tax recorded for the investment
     *
     * @param $investment
     */
    private function rollbackTax($investment)
    {
        $taxes = ClientTax::where('investment_id', $investment->id)->get();

        foreach ($taxes as $tax) {
            $tax->delete();
        }
    }

    /**
     * Rollback the associated custodial transactions
     * Rollback the amount, withdrawn amount, interest payments & commission payments
     *
     * @param $investment
     */
    private function rollbackCustodial($investment)
    {
        //rollback amount
        $amount = $this->createCustodialTransaction($investment);

        $amount->amount = -abs($investment->amount);
        $amount->date = $investment->invested_date;
        $amount->type = CustodialTransactionType::where('name', 'FO')->first()->id;
        $amount->description =
            'Client Investment was Rolled back :' . ClientPresenter::presentFullNames($investment->client_id);
        $amount->save();

        //rollback payments
        foreach ($investment->payments as $payment) {
            $p = $this->createCustodialTransaction($investment);

            $p->amount = abs($payment->amount);
            $p->type = CustodialTransactionType::where('name', 'FI')->first()->id;
            $p->date = $payment->date_paid;
            $p->description = 'Client Investment interest payment was Rolled back :' .
                ClientPresenter::presentFullNames($investment->client_id);
            $p->save();
        }

        //rollback withdrawal
        if ($investment->withdrawn) {
            $withdrawal = $this->createCustodialTransaction($investment);

            $withdrawal->amount = abs($investment->withdraw_amount);
            $withdrawal->date = $investment->withdrawal_date;
            $withdrawal->type = CustodialTransactionType::where('name', 'FI')->first()->id;
            $withdrawal->description = 'Client Investment Withdrawal was Rolled back :' .
                ClientPresenter::presentFullNames($investment->client_id);
            $withdrawal->save();
        }
    }


    /**
     * Create and return a custodial transaction object with the defaults set
     *
     * @param  $investment
     * @return CustodialTransaction
     */
    private function createCustodialTransaction($investment)
    {
        //update custodial
        $custodial = new CustodialTransaction();
        $custodial->custodial_account_id = $investment->product->custodial_account_id;
        $custodial->transaction_owners_id = (new BaseInvestment())->getClientTransactionOwner($investment)->id;

        return $custodial;
    }
}
