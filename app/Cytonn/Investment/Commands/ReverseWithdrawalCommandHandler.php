<?php

namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTax;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Base;
use Cytonn\Investment\BaseInvestment;
use Cytonn\Presenters\ClientPresenter;
use DB;
use Laracasts\Commander\CommandHandler;

class ReverseWithdrawalCommandHandler extends Base implements CommandHandler
{

    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     * @throws \Throwable
     */
    public function handle($command)
    {
        DB::transaction(
            function () use ($command) {
                $investment = $command->investment;

                //custodial
                $this->rollbackCustodial($investment);

                //tax
                $this->rollbackTax($investment);

                //un-withdraw
                $investment->withdrawn = null;
                $investment->rolled = null;
                $investment->withdraw_approval_id = null;
                $investment->withdrawal_date = null;
                $investment->withdraw_amount = null;
                $investment->save();

                if ($investment->withdrawApproval->transaction_type == 'combined_rollover') {
                    $data = $investment->withdrawApproval->payload;

                    foreach (json_decode($data['investments']) as $id) {
                        $investment = ClientInvestment::find($id);

                        if ($investment->withdrawn) {
                            $investment->withdrawn = null;
                            $investment->rolled = null;
                            $investment->withdraw_approval_id = null;
                            $investment->withdrawal_date = null;
                            $investment->withdraw_amount = null;
                            $investment->save();
                        }
                    }
                }

                $withdraw_approval = $investment->withdrawApproval;

                $investment->repo->reverseDeduction($withdraw_approval);
            }
        );
    }

    /**
     * Create and return a custodial transaction object with the defaults set
     *
     * @param  $investment
     * @return CustodialTransaction
     */
    private function createCustodialTransaction($investment)
    {
        //update custodial
        $custodial = new CustodialTransaction();
        $custodial->custodial_account_id = $investment->product->custodial_account_id;
        $custodial->transaction_owners_id = (new BaseInvestment())->getClientTransactionOwner($investment)->id;

        return $custodial;
    }

    /**
     * Rollback any tax recorded for the investment
     *
     * @param $investment
     */
    private function rollbackTax($investment)
    {
        $tax = ClientTax::where('investment_id', $investment->id)->get();

        foreach ($tax as $t) {
            $t->delete();
        }
    }

    /**
     * Rollback the associated custodial transactions
     * Rollback the amount, withdrawn amount, interest payments & commission payments
     *
     * @param  $investment
     * @throws \Exception
     */
    private function rollbackCustodial($investment)
    {
        if ($investment->withdrawApproval->transaction_type == 'combined_rollover') {
            $data = $investment->withdrawApproval->payload;

            //rollback the topup
            if ($data['reinvest'] == 'topup') {
                $withdrawal = $this->createCustodialTransaction($investment);

                $withdrawal->amount = -abs($data['amount']);
                $withdrawal->type = CustodialTransactionType::where('name', 'FO')->first()->id;
                $withdrawal->date = $investment->withdrawal_date;
                $withdrawal->description = 'Client Investment Topup was reversed :' .
                    ClientPresenter::presentFullNames($investment->client_id);
                $withdrawal->save();
            } else {
                //rollback the withdrawal_amts
                //break if it's a combined rollover
                throw new ClientInvestmentException('Cannot rollback a combined rollover without top up');
            }
        } else {
            $withdrawal = $this->createCustodialTransaction($investment);

            $withdrawal->amount = abs($investment->withdraw_amount);
            $withdrawal->type = CustodialTransactionType::where('name', 'FI')->first()->id;
            $withdrawal->date = $investment->withdrawal_date;
            $withdrawal->description = 'Client Investment Withdrawal was reversed :' .
                ClientPresenter::presentFullNames($investment->client_id);
            $withdrawal->save();
        }
    }
}
