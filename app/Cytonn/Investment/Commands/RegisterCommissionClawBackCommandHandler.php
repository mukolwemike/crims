<?php

namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\CommissionClawback;
use App\Cytonn\Models\CommissionPaymentSchedule;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Calculator;
use Cytonn\Investment\Commission\Generator;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Presenters\ClientPresenter;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Laracasts\Commander\CommandHandler;

/**
 * Class RegisterCommissionClawBackCommandHandler
 *
 * @package Cytonn\Investment\Commands
 */
class RegisterCommissionClawBackCommandHandler extends Calculator implements CommandHandler
{
    /**
     * RegisterCommissionClawBackCommandHandler constructor.
     */
    public function __construct()
    {
        parent::__construct(Currency::first(), Carbon::now(), Carbon::now());
    }


    /**
     * Handle the command.
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        DB::transaction(
            function () use ($command) {
                if ($command->edit) {
                    $this->onEdit($command->investment, $command->newInvestment, $command->approval);
                } else {
                    $this->premature($command->investment);
                    //partials will be compensated by next inv
                }
            }
        );
    }

    /**
     * @param ClientInvestment $oldInvestment
     */
    public function premature(ClientInvestment $oldInvestment)
    {
        $commissionPaid = $this->paidSchedules($oldInvestment, $oldInvestment->withdrawal_date)->sum('amount');

        $commissionForPeriod = $this->commissionAtDate($oldInvestment, $oldInvestment->withdrawal_date);

        $clawBack = $commissionPaid - $commissionForPeriod;

        $msg = ClientPresenter::presentJointFirstNameLastName($oldInvestment->client_id) . ' investment of '
            . AmountPresenter::currency($oldInvestment->amount) . ' was withdrawn partially on '
            . $oldInvestment->withdrawal_date->toDateString()
            . ' approval: ' . $oldInvestment->withdraw_approval_id;

        if ($clawBack > 0) {
            $this->performClawBack($oldInvestment, $clawBack, $oldInvestment->withdrawal_date, $msg, 'withdraw');
        }
    }

    /**
     * Generate the claw back when investment is edited
     *
     * @param ClientInvestment $oldInvestment
     * @param ClientInvestment $newInvestment
     * @param ClientTransactionApproval $approval
     */
    protected function onEdit(
        ClientInvestment $oldInvestment,
        ClientInvestment $newInvestment,
        ClientTransactionApproval $approval
    ) {
        $commissionPaid = $this->paidSchedules($oldInvestment, $newInvestment->maturity_date)->sum('amount');

        if ($commissionPaid == 0) {
            $generator = new Generator();

            $generator->recreateSchedules($newInvestment);

            return;
        } else {
            $unpaidSchedules = $this->unPaidSchedules($newInvestment, $newInvestment->maturity_date);

            $amount = $this->getClawbackAmount($oldInvestment, $newInvestment);

            $msg = 'Maturity date of investment for ' .
                ClientPresenter::presentJointFirstNameLastName($oldInvestment->client_id) .
                ' was edited and changed from ' . $oldInvestment->maturity_date->toDateString() .
                ' to ' . $newInvestment->maturity_date->toFormattedDateString() . ' approval: ' . $approval->id;

            $this->clawBack($amount, $msg, $newInvestment);

            $unpaidSchedules->each(function ($schedule) use ($newInvestment) {
                if (Carbon::parse($schedule->date) > Carbon::parse($newInvestment->maturity_date)) {
                    $schedule->update([
                        'date' => $newInvestment->maturity_date
                    ]);
                }
            });

            return;
        }

        // $refundedClawbacks = $this->refundedClawbacks($oldInvestment,
        // $newInvestment->maturity_date)->sum('amount');
        //
        //        $commissionForPeriod = $this->commissionAtDate($oldInvestment, $newInvestment->maturity_date);
        //
        //        $commission_diff = $commissionPaid - $commissionForPeriod - $refundedClawbacks;
        //
        // $msg = 'Maturity date of investment for '.
        // ClientPresenter::presentJointFirstNameLastName($oldInvestment->client_id).
        //            ' was edited and changed from '.$oldInvestment->maturity_date->toDateString().
        //            ' to '.$newInvestment->maturity_date->toFormattedDateString().' approval: '.$approval->id;
        //
        //        if($commission_diff > 0)  $this->performClawBack($newInvestment,
        // $commission_diff, Carbon::today(), $msg, 'edit');
    }

    /*
     * Get the deserved clawback amount based on the edited dates
     */
    private function getClawbackAmount($oldInvestment, $newInvestment)
    {
        $tenor = $oldInvestment->maturity_date->copy()->diffInDays($newInvestment->maturity_date);

        return $newInvestment->amount * $newInvestment->commission->rate * $tenor / (100 * 365);
    }

    private function clawBack($amount, $msg, $newInvestment)
    {
        return CommissionClawback::create([
            'amount' => $amount,
            'commission_id' => $newInvestment->commission->id,
            'date' => Carbon::today(),
            'narration' => $msg,
            'type' => 'edit',
        ]);
    }


    /**
     * Perform the actual claw back
     *
     * @param ClientInvestment $investment
     * @param $commissionDifference
     * @param Carbon $startDate
     * @param $msg
     * @param $type
     */
    protected function performClawBack(
        ClientInvestment $investment,
        $commissionDifference,
        Carbon $startDate,
        $msg,
        $type
    ) {
        //get unpaid, future schedules and write them off
        $this->unPaidSchedules($investment, $startDate)
            ->each(
                function ($unpaid) {
                    $unpaid->claw_back = true;
                    $unpaid->save();
                }
            );

        $this->unRefundedClawbacks($investment, $startDate)
            ->each(
                function ($unrefunded) {
                    $unrefunded->delete();
                }
            );

        //record claw back
        $cl = new CommissionClawback();
        $cl->amount = $commissionDifference;
        $cl->commission_id = $investment->commission->id;
        $cl->date = $startDate;
        $cl->narration = $msg;
        $cl->type = $type;
        $cl->save();
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @return Collection
     */
    public function unPaidSchedules(ClientInvestment $investment, Carbon $date)
    {
        $d = $date;
        $bcp = BulkCommissionPayment::includes($date)->first();

        if ($bcp) {
            $d = $bcp->end;
        } else {
            $bcp = BulkCommissionPayment::orderBy('end', 'DESC')->first();

            if ($bcp) {
                $d = $bcp->end;
            }
        }

        return CommissionPaymentSchedule::where('commission_id', $investment->commission->id)
            ->where('date', '>=', $d)
            ->clawback(false)
            ->get();
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @return Collection
     */
    protected function unRefundedClawbacks(ClientInvestment $investment, Carbon $date)
    {
        $d = $date;
        $bcp = BulkCommissionPayment::includes($date)->first();

        if ($bcp) {
            $d = $bcp->end;
        } else {
            $bcp = BulkCommissionPayment::orderBy('end', 'DESC')->first();

            if ($bcp) {
                $d = $bcp->end;
            }
        }

        return CommissionClawback::where('commission_id', $investment->commission->id)
            ->where('date', '>=', $d)
            ->get();
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @return Collection
     */
    public function paidSchedules(ClientInvestment $investment, Carbon $date)
    {
        $d = $date;
        $bcp = BulkCommissionPayment::includes($date)->first();

        if ($bcp) {
            $d = $bcp->end;
        } else {
            $bcp = BulkCommissionPayment::orderBy('end', 'DESC')->first();

            if ($bcp) {
                $d = $bcp->end;
            }
        }

        return CommissionPaymentSchedule::where('commission_id', $investment->commission->id)
            ->where('date', '<', $d)
            ->clawback(false)
            ->get();
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $date
     * @return Collection
     */
    protected function refundedClawbacks(ClientInvestment $investment, Carbon $date)
    {
        $d = $date;
        $bcp = BulkCommissionPayment::includes($date)->first();

        if ($bcp) {
            $d = $bcp->end;
        } else {
            $bcp = BulkCommissionPayment::orderBy('end', 'DESC')->first();

            if ($bcp) {
                $d = $bcp->end;
            }
        }

        return CommissionClawback::where('commission_id', $investment->commission->id)
            ->where('date', '<', $d)
            ->get();
    }

    /**
     * @param ClientInvestment $investment
     * @return float
     */
    public function commissionAtDate(ClientInvestment $investment, Carbon $date)
    {
        $tenor = $date->copy()->diffInDays($investment->invested_date->copy());

        return $investment->amount * $investment->commission->rate * $tenor / (100 * 365);
    }
}
