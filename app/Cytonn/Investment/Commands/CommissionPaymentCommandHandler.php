<?php

namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use Cytonn\Investment\Commission\Payment;
use Laracasts\Commander\CommandHandler;
use Carbon\Carbon;
use DB;

class CommissionPaymentCommandHandler implements CommandHandler
{


    /**
     * Pay the commission
     *
     * @param  object $command
     * @return void
     */
    public function handle($command)
    {
        dd('no longer in use');

        DB::transaction(
            function () use ($command) {
                $data = $command->approval->payload;

                $currency = Currency::findOrFail($data['currency_id']);

                $recipient = CommissionRecepient::findOrFail($data['recipient_id']);

                return (new Payment())->make($recipient, $currency, $command->approval);
            }
        );
    }
}
