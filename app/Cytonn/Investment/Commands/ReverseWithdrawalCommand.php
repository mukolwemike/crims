<?php

namespace Cytonn\Investment\Commands;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;

class ReverseWithdrawalCommand
{
    public $investment;

    public $approval;

    /**
     */
    public function __construct(ClientInvestment $investment, ClientTransactionApproval $approval)
    {
        $this->investment = $investment;

        $this->approval = $approval;
    }
}
