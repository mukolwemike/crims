<?php
/**
 * Date: 8/17/15
 * Time: 8:41 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Setting;
use App\Cytonn\Models\TransactionOwner;
use \Carbon\Carbon;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Illuminate\Support\Facades\DB;

/**
 * Class BaseInvestment
 *
 * @package Cytonn\Investment
 */
class BaseInvestment
{
    use CalculatorTrait, ValidatorTrait;


    public $client;
    public $custodial;


    /**
     * Finds or adds the client to transaction owners table. A transaction is owned by either client or bank investment
     *
     * @param  ClientInvestment $investment
     * @return TransactionOwner
     */
    public function getClientTransactionOwner(ClientInvestment $investment)
    {
        //get transaction owner from client id
        $owner = TransactionOwner::where('client_id', $investment->client_id)->first();

        if ($owner==null) { //client not in transaction owners table, add
            $owner = new TransactionOwner();
            $owner->fund_investor_id = null;
            $owner->client_id = $investment->client_id;
            $owner->fund_manager_id = $investment->product->fund_manager_id;
            $owner->save();
        }

        return $owner;
    }

    /**
     * Return the value after adding interest earned over the period
     *
     * @param  float $principal
     * @param  int   $tenorInDays
     * @param  float $ratePercent
     * @return float
     */
    protected function getFinalValue($principal, $tenorInDays, $ratePercent, $taxable)
    {
        $interest = $principal*$tenorInDays*$ratePercent/(365*100);

        if ($taxable) {
            $tax_rate = Setting::where('key', 'withholding_tax_rate')->latest()->first()->value;

            $withHoldingTax = $tax_rate * $interest/100;
        } else {
            $withHoldingTax = 0;
        }

        return $principal + $interest - $withHoldingTax;
    }

    /**
     * Returns the number of days between the maturity and the invested date
     *
     * @param  \Carbon\Carbon $investedDate
     * @param  \Carbon\Carbon $maturityDate
     * @return int
     */
    protected function getTenorInDays($investedDate, $maturityDate)
    {
        return $investedDate->copy()->diffInDays($maturityDate);
    }

    /**
     * Returns the custody fees charged for that investment
     *
     * @param  Int $amount
     * @return float
     */
    protected function getCustodyFees($amount)
    {
        return Setting::where('key', 'custody_fee_rate')->latest()->first()->value*$amount;
    }


    /**
     * Remove unnecessary items from form id
     * Used to avoid inserting characters and zeros to topup_form_id, withdrawal_form_id and rollover_form_id
     *
     * @param  $form_id
     * @return null|string
     */
    protected function cleanFormID($form_id)
    {
        $form = trim($form_id);

        if ($form != 0 && $form != '' && is_numeric($form) && $form != '0') {
            return (int) $form;
        }

        return null;
    }
}
