<?php
/**
 * Date: 9/16/15
 * Time: 10:26 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\Setting;
use Carbon\Carbon;

/**
 * This trait is used to perform calculate Investment related calculations
 * Class CalculatorTrait
 *
 * @package Cytonn\Investment
 */
trait CalculatorTrait
{
    public static $rates = [];

    protected $useRate;

    /**
     * Returns the gross interest
     *
     * @param  $principal
     * @param  $ratePercent
     * @param  $investedDate
     * @param  $maturityDate
     * @return mixed
     */
    public function getGrossInterest($principal, $ratePercent, $investedDate, $maturityDate)
    {
        $investedDate = new Carbon($investedDate);

        $maturityDate = new Carbon($maturityDate);

        if ($maturityDate->copy()->lt($investedDate)) {
            return 0;
        }

        $tenorInDays = $investedDate->copy()->diffInDays($maturityDate->copy());

        return $principal*$tenorInDays*$ratePercent/(365*100);
    }

    public function grossInterestForDay($principal, $ratePercent)
    {
        if (abs($principal) < 1) {
            return 0;
        }
        
        return $principal * ($ratePercent/100) * (1/365);
    }

    /**
     * Used for statements
     * Checks if maturity date is in future and uses current date to calculate interest
     *
     * @param  $principal
     * @param  $ratePercent
     * @param  $investedDate
     * @param  $maturityDate
     * @return mixed
     */
    public function getCurrentGrossInterest($principal, $ratePercent, $investedDate, $maturityDate, $today = null)
    {
        is_null($today) ? $today =  Carbon::today(): $today = new Carbon($today);
        $investedDate = new Carbon($investedDate);
        $maturityDate = new Carbon($maturityDate);

        if ($investedDate->copy()->gte($today->copy())) {//don't pay interest if invested in future
            return 0;
        }


        if ($maturityDate->copy()->gt($today->copy())) {
            return $this->getGrossInterest($principal, $ratePercent, $investedDate, $maturityDate);
        } else {
            return $this->getGrossInterest($principal, $ratePercent, $investedDate, $today);
        }
    }

    public function getCurrentGrossInterestAsAtEndOfDay(
        $principal,
        $ratePercent,
        $investedDate,
        $maturityDate,
        $today = null
    ) {
        $investedDate = new Carbon($investedDate);
        $maturityDate = new Carbon($maturityDate);
        is_null($today) ? $today =  Carbon::today(): $today = new Carbon($today);

        if ($investedDate->copy()->gte($today->copy())) {//don't pay interest if invested in future
            return 0;
        }

        $maturity = new Carbon($maturityDate);

        if ($maturity->copy()->lte($today->copy())) {
            return $this->getGrossInterest($principal, $ratePercent, $investedDate, $maturity);
        } else {
            //show interest as at next day
            return $this->getGrossInterest($principal, $ratePercent, $investedDate, $today->addDay());
        }
    }

    /**
     * Returns the net interest after tax
     *
     * @param  $grossInterest
     * @param  bool | DepositHolding          $taxable
     * @return mixed
     */
    public function getNetInterest($grossInterest, $taxable)
    {
        if (!$taxable) {
            return $grossInterest;
        }

        $tax_rate = $this->withHoldingTaxRate(null, $taxable);

        return $grossInterest - ($tax_rate * $grossInterest / 100);
    }

    public function calculateGrossFromNet($netInterest, $taxable)
    {
        if (!$taxable) {
            return $netInterest;
        }

        $tax_rate = $this->withHoldingTaxRate(null, $taxable);

        return $netInterest / ((100 - $tax_rate)/100);
    }

    /**
     * @param $tax
     * @return float|int
     */
    public function calculateGrossFromWht($tax)
    {
        $tax_rate = $this->withHoldingTaxRate(null, true);

        return $tax * 100 / $tax_rate;
    }

    /**
     * Calculates the withholding tax for an investment
     *
     * @param  $grossInterest
     * @return float
     */
    public function getWithHoldingTax($grossInterest, $deposit = null)
    {
        $tax_rate = $this->withHoldingTaxRate(null, $deposit);

        return $tax_rate * $grossInterest / 100;
    }

    public function limitToZero($amount)
    {
        if ($amount < 0) {
            return 0;
        }

        return $amount;
    }

    public function withHoldingTaxRate(Carbon $date = null, $investment = null)
    {
        if ($investment instanceof DepositHolding) {
            return $investment->withHoldingTaxRate();
        }

        $key = is_null($date) ? 'today' : $date->toDateString();

        if (isset(static::$rates[$key])) {
            return static::$rates[$key];
        }

        $rate = \setting('withholding_tax_rate');

        static::$rates[$key] = $rate;

        return $rate;
    }
}
