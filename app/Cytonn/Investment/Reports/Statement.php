<?php

namespace App\Cytonn\Investment\Reports;

use App\Cytonn\Models\ClientUser as User;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class Statement
{
    protected $protect = false;

    public function generate(
        Product $product,
        Client $client,
        $dateFrom = null,
        $statementDate = null,
        User $user = null
    ) {
        $statementDate = is_null($statementDate)
            ? $statementDate = Carbon::now()
            : $statementDate = Carbon::parse($statementDate);

        $dateFrom = is_null($dateFrom)
            ? $statementDate->copy()->subMonthNoOverflow()
            : Carbon::parse($dateFrom);

        $stmt_data = (new StatementProcessor())
            ->process($client, $product, $dateFrom->copy(), $statementDate->copy());

        $pdf = \PDF::loadView('reports.investments.statement', [
            'client' => $client,
            'fromDate'=>$dateFrom,
            'statementDate' => $statementDate,
            'data' => $stmt_data,
            'product' => $product,
            'logo' => $product->fundManager->logo,
            'user' => $user
        ]);

        if ($this->protect) {
            $pdf->setEncryption($client->repo->documentPassword());
        }

        return $pdf;
    }

    public function html(Product $product, Client $client, $dateFrom = null, $statementDate = null, User $user = null)
    {
        $statementDate = is_null($statementDate)
            ? $statementDate = Carbon::now()
            : $statementDate = Carbon::parse($statementDate);

        $dateFrom = is_null($dateFrom)
            ? $statementDate->copy()->subMonthNoOverflow()
            : Carbon::parse($dateFrom);

        $stmt_data = (new StatementProcessor())->process(
            $client,
            $product,
            $dateFrom->copy(),
            $statementDate->copy()
        );

        return view('reports.investments.partials.statement_table', [
            'client' => $client,
            'statementDate' => $statementDate,
            'fromDate'=>$dateFrom,
            'data' => $stmt_data,
            'product' => $product,
            'user' => $user,
            'logo' => $product->fundManager->logo
        ]);
    }

    /**
     * @param Product $product
     * @param Client $client
     * @param null $dateFrom
     * @param null $statementDate
     * @param User|null $user
     * @return mixed
     */
    public function excel(Product $product, Client $client, $dateFrom = null, $statementDate = null, User $user = null)
    {
        $statementDate = is_null($statementDate)
            ? $statementDate = Carbon::now()
            : $statementDate = Carbon::parse($statementDate);

        $dateFrom = is_null($dateFrom)
            ? $statementDate->copy()->subMonthNoOverflow()
            : Carbon::parse($dateFrom);

        $stmt_data = (new StatementProcessor())->process(
            $client,
            $product,
            $dateFrom->copy(),
            $statementDate->copy()
        );

        $excelSheet =  Excel::create(
            'Statement',
            function ($excel) use ($stmt_data, $client, $statementDate, $dateFrom, $product, $user) {
                $excel->sheet(
                    'Statement',
                    function ($sheet) use ($stmt_data, $client, $statementDate, $dateFrom, $product, $user) {

                        $sheet->loadView('reports.investments.partials.statement_excel', [
                        'client' => $client,
                        'statementDate' => $statementDate,
                        'fromDate'=>$dateFrom,
                        'data' => $stmt_data,
                        'product' => $product,
                        'user' => $user,
                        'logo' => $product->fundManager->logo
                        ]);
                    }
                );
            }
        )->download('xlsx');

        return $excelSheet;
    }

    public function passwordProtect($protect = true)
    {
        $this->protect = $protect;

        return $this;
    }
}
