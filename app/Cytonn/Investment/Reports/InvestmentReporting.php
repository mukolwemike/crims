<?php

namespace App\Cytonn\Investment\Reports;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Setting;
use Carbon\Carbon;
use Cytonn\Investment\CalculatorTrait;

class InvestmentReporting
{
    use CalculatorTrait;

    protected $hasTopup = false;

    protected $statementDate;

    protected $params = [];


    public function processInvestment(ClientInvestment $investment, Carbon $statementDate)
    {
        $this->statementDate = $statementDate;

        if ($investment->payments()->where('date_paid', '<=', $this->statementDate)->count() > 1) {
            $this->params['has_payments'] = true;
            $first = $investment->payments()->where('date_paid', '<=', $this->statementDate)
                ->orderBy('date_paid', 'DESC')->first();
            $second = $investment->payments()->where('date_paid', '<=', $this->statementDate)
                ->where('id', '!=', $first->id)->orderBy('date_paid', 'DESC')->first();
            $this->params['second'] = $second;
        }

        $this->params['tax_rate'] = (new Setting())->where('key', 'withholding_tax_rate')
                ->latest()->first()->value / 100;

        $statement = [];
        $statement['value_date'] = $investment->invested_date;
        $statement['principal'] = $this->principal($investment);
        $statement['invested_date'] = $this->investedDate($investment);
        $statement['maturity_date'] = $this->maturityDate($investment);
        $statement['interest_rate'] = $this->interestRate($investment);
        $statement['gross_interest'] = $this->grossInterest($investment);
        $statement['net_interest'] = $this->netInterest($investment);
        $statement['topup'] = $this->topup($investment);
        $statement['withdrawal'] = $this->withdrawal($investment);
        $statement['withdrawn'] = $this->withdrawn($investment);
        $statement['rollover'] = $this->isRollover($investment);
        $statement['total'] = $this->total($investment);
        $statement['payments'] = $this->payments($investment);
        $statement['deductions'] = $this->deductions($investment);
        $statement['total_net_interest'] = $this->totalNetInterest($investment);
        $statement['total_gross_interest'] = $this->totalGrossInterest($investment);
        $statement['net_interest_at_withdrawal'] = $this->netInterestAtWithdrawal($investment);
        $statement['gross_interest_at_withdrawal'] = $this->grossInterestAtWithdrawal($investment);

        $investment->statement = (object)$statement;

        $this->params = [];

        return $investment;
    }

    public function simpleStatement(ClientInvestment $investment, Carbon $statementDate, Product $product)
    {
        $investment = $this->processInvestment($investment, $statementDate);

        $investment->principal = $investment->statement->principal;
        $investment->net_interest = $investment->statement->total_net_interest;
        $investment->total = $investment->statement->total;
        $investment->product = $product->name;

        return $investment;
    }

    protected function principal(ClientInvestment $investment)
    {
        $this->params['principal'] = round($investment->amount);

        return $this->params['principal'];
    }

    protected function payments(ClientInvestment $investment)
    {
        $payments = $investment->payments()->where('date_paid', '<=', $this->statementDate)
            ->orderBy('date_paid', 'ASC')->get();

        return $payments->each(
            function ($p) use ($investment, $payments) {
                $net_interest = $investment->repo->getNetInterestForInvestment($p->date_paid->copy()->addDay());

                $previous = $investment->payments()->where('date_paid', '<', $p->date_paid)
                    ->latest('date_paid')->first();

                if (!is_null($previous)) {
                    $net_interest -= $investment->repo
                        ->getNetInterestForInvestment($previous->date_paid->copy()->addDay());
                }

                $p->net_interest = $net_interest;

                $p->gross_interest = $this->getGrossFromNetInterest($p->net_interest, $investment->client->taxable);

                $p->balance = $this->principal($investment)
                    + $investment->repo->getNetInterestForInvestment($p->date_paid->copy()->addDay())
                    - $this->getPaymentsOnOrBeforeDate($investment, $p->date_paid->copy()->subDay());
            }
        );
    }

    protected function deductions(ClientInvestment $investment)
    {
        return $investment->deductions()->where('date', '<=', $this->statementDate)
            ->orderBy('date', 'ASC')->get();
    }

    protected function investedDate($investment)
    {
        return $investment->invested_date;
    }

    protected function maturityDate($investment)
    {
        if ($this->withdrawn($investment)) {
            return $investment->withdrawal_date;
        }

        return $investment->maturity_date;
    }

    protected function interestRate($investment)
    {
        return $investment->interest_rate;
    }

    protected function grossInterest(ClientInvestment $investment)
    {
        $gross = $investment->repo->getGrossInterestForInvestment($this->statementDate->copy()->addDay());

        $this->params['gross_interest'] = $gross;

        return $gross;
    }

    protected function netInterest($investment)
    {
        $net = $this->getNetInterest($this->params['gross_interest'], $investment->client->taxable);

        $this->params['net_interest'] = $net;

        return $net;
    }

    protected function totalNetInterest($investment)
    {
        return
            $this->netInterest($investment) -
            $this->deductions($investment)->sum('amount') -
            $this->payments($investment)->sum('amount');
    }

    protected function totalGrossInterest($investment)
    {
        return $this->getGrossFromNetInterest($this->totalNetInterest($investment), $investment->client->taxable);
    }

    protected function topup(ClientInvestment $investment)
    {
        $amount = $investment->repo->topupAmount();

        $this->hasTopup = $amount > 0;
        $this->params['topup'] = $amount;

        return $this->params['topup'];
    }

    protected function withdrawal($investment)
    {
        $payments = $investment->payments()->where('date_paid', '>', $this->investedDate($investment))
            ->where('date_paid', '<=', $this->statementDate)->sum('amount');
        $deductions = $investment->deductions()->where('date', '>', $this->investedDate($investment))
            ->where('date', '<=', $this->statementDate)->sum('amount');

        $withdrawal = $deductions + $payments;

        if ($this->withdrawn($investment)) {
            $withdrawal = $withdrawal + $investment->withdraw_amount;
        }

        $this->params['withdrawal'] = $withdrawal;

        return $withdrawal;
    }

    protected function withdrawn($investment)
    {
        return $investment->withdrawn && $this->statementDate >= $investment->withdrawal_date;
    }

    protected function total($investment)
    {
        if ($investment->rolled || $investment->partial_withdraw) {
            $total = $this->params['principal']
                + $this->params['net_interest']
                + $this->params['topup']
                - $this->payments($investment)->sum('amount')
                - $this->deductions($investment)->sum('amount');
        } elseif ($this->withdrawn($investment) && !$investment->descendant()) {
            $total = 0;
        } else {
            $total =
                $this->params['principal'] + $this->params['net_interest'] +
                $this->params['topup'] - $this->params['withdrawal'];
        }

        $this->params['total'] = $total;

        return $total;
    }

    protected function isRollover($investment)
    {
        if ($investment->type->name == 'rollover' &&
            (new Carbon($investment->invested_date))->gt($this->statementDate->copy()->startOfMonth())) {
            return true;
        }

        return false;
    }

    protected function calculateTotals($investments)
    {
        $total = function ($column) use ($investments) {
            return $investments->sum(
                function ($inv) use ($column) {
                    if (isset($inv->total->$column)) {
                        return $inv->total->$column;
                    }
                    return 0;
                }
            );
        };

        return [
            'total_interest' => $total('net_interest_cumulative'),
            'total_principal' => $total('principal'),
            'total_client_value' => $total('total')
        ];
    }

    protected function getPaymentsOnOrBeforeDate($investment, $date)
    {
        $payments = $investment->payments()->where('date_paid', '<=', $date)->sum('amount');
        $deductions = $investment->deductions()->where('date', '<=', $date)->sum('amount');

        return $payments + $deductions;
    }

    protected function getGrossFromNetInterest($netInterest, $taxable = true)
    {
        if ($taxable) {
            return $netInterest / (1 - $this->params['tax_rate']);
        }

        return $netInterest;
    }

    protected function grossInterestAtWithdrawal($investment)
    {
        return
            $this->getGrossFromNetInterest($this->netInterestAtWithdrawal($investment), $investment->client->taxable);
    }

    protected function netInterestAtWithdrawal(ClientInvestment $investment)
    {
        return $investment->repo->getNetInterestForInvestment($investment->withdrawal_date)
            - $this->deductions($investment)->sum('amount')
            - $this->payments($investment)->sum('amount');
    }
}
