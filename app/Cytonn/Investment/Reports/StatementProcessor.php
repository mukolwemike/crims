<?php

namespace App\Cytonn\Investment\Reports;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use Carbon\Carbon;

class StatementProcessor extends InvestmentReporting
{
    public function process(Client $client, Product $product, Carbon $from = null, Carbon $statementDate)
    {
        $this->statementDate = $statementDate->copy();

        $eligibleInvestments = (new ClientInvestment())->where('client_id', $client->id)
            ->where('product_id', $product->id)
            ->statement($statementDate, $from)
            ->get();

        $inv = $eligibleInvestments->map(
            function (ClientInvestment $investment) use ($statementDate) {
                $prepared = $investment->calculate($statementDate, true)->getPrepared();

                return $prepared;
            }
        );

        $allowed_ids = [];

        $total = $inv->each(
            function ($prepared) use (&$allowed_ids) {
                $investment = $prepared->investment;

                $withdrawals = $investment->withdrawals;

                foreach ($withdrawals as $withdrawal) {
                    array_push($allowed_ids, $withdrawal->reinvested_to);
                }
            }
        )->filter(
            function ($prepared) use ($allowed_ids) {
                $investment = $prepared->investment;

                if (in_array($investment->id, $allowed_ids)) {
                    return true;
                }

                return $investment->invested_date->lte($this->statementDate);
            }
        );

        return [ 'investments'=>$inv, 'totals'=>$this->calculateTotals($total)];
    }
}
