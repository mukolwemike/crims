<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 11/16/16
 * Time: 11:37 AM
 */

namespace Cytonn\Investment\Reports;

use App\Cytonn\Models\User;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Mailers\GeneralMailer;

/**
 * Class Builder
 *
 * @package Cytonn\Investment\Reports
 */
abstract class Builder
{
    /**
     * @var array
     */
    protected $columns;

    /**
     * @var
     */
    protected $user;

    /**
     * @var $end_date
     */
    protected $end_date;

    /**
     * Builder constructor.
     *
     * @param $columns
     * @param Carbon  $end_date
     */
    public function __construct(array $columns = [], Carbon $end_date = null)
    {
        $this->columns = $columns;
        $this->end_date = $end_date;
    }

    /**
     * @param $job
     * @param $data
     */
    public function run($job, $data)
    {
        $this->user = User::find($data['user_id']);

        $this->columns = $data['columns'];

        $this->end_date = $data['end_date'];

        $this->build($data);

        $job->delete();
    }

    /**
     * Generate the
     *
     * @param  $columns
     * @param  Carbon  $end_date
     * @return static
     */
    public static function generate($columns, Carbon $end_date = null)
    {
        is_null($end_date) ? : $end_date = $end_date->toDateString();
        $columns["end_date"] = $end_date;

        \Queue::push(static::class.'@run', ['user_id'=>\Auth::user()->id, 'columns'=>$columns]);
    }

    /**
     * Build the report
     *
     * @return mixed
     */
    abstract protected function build();

    /**
     * Send an email with the export
     *
     * @param $file_path
     * @param $subject
     * @param $message
     */
    protected function mailResult($file_path, $subject, $message)
    {
        $mailer = new GeneralMailer();
        $mailer->to($this->user->email);
        $mailer->from('support@cytonn.com');
        $mailer->subject($subject);
        $mailer->file($file_path);
        $mailer->queue(false);

        $mailer->sendGeneralEmail($message);

        \File::delete($file_path);
    }
}
