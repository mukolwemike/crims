<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 11/16/16
 * Time: 11:35 AM
 */

namespace Cytonn\Investment\Reports\Builders;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\EmptyModel;
use App\Cytonn\Models\Product;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Core\DataStructures\Files\Excel;
use Cytonn\Investment\Reports\Builder;
use Cytonn\Investment\Reports\Entities\Client\Investments;
use Cytonn\Investment\Reports\Entities\CommissionRecipient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class ClientSummary
 *
 * @package Cytonn\Investment\Reports\Builders
 */
class ClientSummary extends Builder
{
    /*
     *
     */
    protected function build($data = [])
    {
        $products = Product::all();

        $fileName = 'Client Summary';

        $model = [];

        $end_date = isset($data['end_date']) ? Carbon::make($data['end_date']) : null;

        foreach ($products as $product) {
            $model[$product->name] = $this->make($product, $end_date);
        }

        Excel::fromModel($fileName, $model)->store('xlsx');

        $path = storage_path() . '/exports/' . $fileName . '.xlsx';

        $subject = 'Client summary export in excel';

        $this->mailResult($path, $subject, 'Please find the attached client summary report');
    }

    /**
     * @param Product $product
     * @param Carbon $end_date
     * @return mixed
     */
    protected function make(Product $product, Carbon $end_date = null)
    {
        $clients = Client::whereHas(
            'investments',
            function ($i) use ($product, $end_date) {
                is_null($end_date)
                    ? $i->where('product_id', $product->id)
                    : $i->where('invested_date', '<=', $end_date)->where('product_id', $product->id);
            }
        )->get();

        return $clients->map(
            function ($client) use ($product) {
                return $this->makeForClient($client, $product);
            }
        );
    }

    /**
     * @param Client $client
     * @param Product $product
     * @return Model
     */
    protected function makeForClient(Client $client, Product $product)
    {
        $clients = $this->clientDetails($client);

        $fa = $this->fa($client);

        $investments = $this->investment($client, $product);

        $data = Collection::make($clients)
            ->merge($fa)
            ->merge($investments)
            ->all();

        return (new EmptyModel())->fill($data);
    }

    /**
     * @param Client $client
     * @return array
     */
    private function clientDetails(Client $client)
    {
        if (!isset($this->columns['clients'])) {
            return [];
        }

        $builder = new \Cytonn\Investment\Reports\Entities\Client\Client();

        $client_details = $builder->build($client);

        return array_only($client_details, $this->columns['clients']);
    }

    /**
     * @param Client $client
     * @param Product $product
     * @return array
     */
    private function investment(Client $client, Product $product)
    {
        if (!isset($this->columns['investment'])) {
            return [];
        }

        $builder = new Investments();

        $inv_details = $builder->build($client, $product);

        return array_only($inv_details, $this->columns['investment']);
    }

    /**
     * @param Client $client
     * @return array
     */
    private function fa(Client $client)
    {
        if (!isset($this->columns['fa'])) {
            return [];
        }

        $builder = new CommissionRecipient();

        $fa = [];

        if ($r = $client->getLatestFA('investments')) {
            $fa = $builder->build($r);
        }

        return array_only($fa, $this->columns['fa']);
    }
}
