<?php
/**
 * Date: 28/11/2016
 * Time: 15:32
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Reports\Entities;

use App\Cytonn\Models\CommissionRecepient;

class CommissionRecipient
{
    public function build(CommissionRecepient $recipient)
    {
        $fa = $recipient->toArray();
        $fa['type'] = $recipient->type->name;

        return $fa;
    }
}
