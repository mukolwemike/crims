<?php
/**
 * Date: 28/11/2016
 * Time: 15:45
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Reports\Entities\Client;

use App\Cytonn\Models\Product;
use Carbon\Carbon;
use App\Cytonn\Models\Client;

class Investments
{
    public function build(Client $client, Product $product)
    {
        return [
            'amount' => $client->repo->getTodayInvestedAmountForProduct($product, Carbon::today()),
            'gross_interest' => $client->repo->getTodayTotalGrossInterestForProduct($product),
            'net_interest' => $client->repo->getTodayTotalInterestForProduct($product),
            'interest_paid' => $client->repo->getTodayTotalPaymentForProduct($product),
            'interest_balance' => $client->net_interest - $client->interest_paid,
            'tenor' => $client->repo->sumInvestmentsTenor(),
            'total' => $client->repo->getTodayTotalInvestmentsValueForProduct($product, Carbon::today()),
            'start_date' => $client->repo->getInvestedDate()->toDateString(),
            'end_date' => $client->repo->getMaturityDate()->toDateString()
        ];
    }
}
