<?php
/**
 * Date: 28/11/2016
 * Time: 15:45
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Reports\Entities\Client;

use Cytonn\Presenters\ClientPresenter;
use App\Cytonn\Models\Client as Cl;

class Client
{
    public function build(Cl $client)
    {
        $data = $client->toArray();
        $data['firstname'] = ClientPresenter::presentJointFirstNames($client->id);
        $data['full_name'] = ClientPresenter::presentJointFullNames($client->id);
        $data['email'] = $client->contact->email;
        $data['country'] = is_null($client->country) ? null : $client->country->name;
        $data['emails'] = implode(", ", $client->getContactEmailsArray());
        return $data;
    }
}
