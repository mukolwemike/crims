<?php
/**
 * Date: 26/10/2015
 * Time: 6:17 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\CustodialTransaction;
use App\Cytonn\Models\CustodialTransactionType;
use App\Cytonn\Models\InterestPayment;
use Cytonn\Investment\Events\ClientPaymentIsDue;
use Cytonn\Presenters\ClientPresenter;

class InvestmentTransaction extends BaseInvestment
{
    public function makeInterestPayment(ClientInvestment $investment, $amount, $date_paid, $approval)
    {
        $payment = new InterestPayment();
        $payment->amount = $amount;
        $payment->date_paid = $date_paid;
        $payment->investment_id = $investment->id;
        $payment->approval_id = $approval->id;
        $payment->save();

        $custodial = new CustodialTransaction();
        $custodial->amount = -$payment->amount;
        $custodial->description =
            'Payment made to client: ' . ClientPresenter::presentFullNames($investment->client_id);
        $custodial->custodial_account_id = $investment->product->custodial_account_id;
        $custodial->transaction_owners_id = $this->getClientTransactionOwner($investment)->id;
        $custodial->type = CustodialTransactionType::where('name', 'FO')->first()->id;
        $custodial->date = $date_paid;
        $custodial->save();

        $investment->raise(new ClientPaymentIsDue(
            $payment->amount,
            'Interest payment',
            $investment,
            'interest_payment',
            $date_paid,
            $approval
        ));
        $investment->dispatchEventsFor($investment);

        return $payment;
    }
}
