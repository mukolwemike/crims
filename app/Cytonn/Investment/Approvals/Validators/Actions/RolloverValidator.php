<?php
/**
 * Date: 25/04/2016
 * Time: 9:33 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Approvals\Validators\Actions;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Investment\Approvals\Validators\ClientInvestmentValidatorInterface;
use Cytonn\Exceptions\ClientInvestmentException as Error;
use Carbon\Carbon;
use Cytonn\Portfolio\Forms\RolloverForm;
use Illuminate\Support\Facades\App;

/**
 * Class RolloverValidator
 *
 * @package Cytonn\Investment\Approvals\Validators\Actions
 */
class RolloverValidator implements ClientInvestmentValidatorInterface
{

    /**
     * Set to true when scheduling the transaction
     *
     * @var bool
     */
    public $scheduling = false;


    /**
     * Validate the rollover
     *
     * @param  ClientTransactionApproval $approval
     * @throws Error
     */
    public function validate(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $oldInvestment = ClientInvestment::findOrFail($data['investment']);

        $validation_data = $data;

        //allow same day
        $validation_data = array_add(
            $validation_data,
            'old_investment_maturity_date',
            (new Carbon($oldInvestment->maturity_date))->subDay()->toDateString()
        );
        $validation_data = array_add(
            $validation_data,
            'old_investment_amount',
            (float)$oldInvestment->repo->getTotalValueOfAnInvestment()
        );
        $validation_data['amount'] = (float)$validation_data['old_investment_amount'];


        if ($data['amount'] > $validation_data['old_investment_amount']) {
            throw new Error('The amount rolled over should be less than total value');
        }

        App::make(RolloverForm::class)->validate($validation_data);

        if ($oldInvestment->withdrawn == 1) {
            throw new Error(
                'This investment has already been withdrawn, you can only rollover active investments'
            );
        }

        if ((new Carbon($oldInvestment->maturity_date))->isFuture() && !$this->scheduling) {
            throw new Error('This investment has not matured, you cannot rollover');
        }
    }
}
