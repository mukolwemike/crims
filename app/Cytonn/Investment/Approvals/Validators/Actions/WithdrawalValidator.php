<?php
/**
 * Date: 21/04/2016
 * Time: 3:34 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Approvals\Validators\Actions;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Investment\Approvals\Validators\ClientInvestmentValidatorInterface;
use Cytonn\Investment\Forms\WithdrawalForm;
use App;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException as Error;

class WithdrawalValidator implements ClientInvestmentValidatorInterface
{
    protected $withdrawalForm;

    /**
     * WithdrawalValidator constructor.
     */
    public function __construct()
    {
        $this->withdrawalForm = App::make(WithdrawalForm::class);
    }

    public function validate(ClientTransactionApproval $approval)
    {
        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        if ($investment->withdrawn == 1) {
            throw new Error('This investment has already been withdrawn, you cannot withdraw it again');
        }

        $validation_data = $data;

        if ($data['premature'] == 'false' || $data['premature'] == false) {
            if ((new Carbon($investment->maturity_date))->isFuture() && !$approval->scheduled) {
                throw new Error('This investment has not matured, you cannot withdraw');
            }
        } elseif ($data['premature'] == true || $data['premature'] == 'true') {
            $this->withdrawalForm->premature();

            $validation_data = array_add(
                $validation_data,
                'invested_date',
                (new Carbon($investment->invested_date))->subDay()->toDateString()
            );
            $validation_data = array_add(
                $validation_data,
                'maturity_date',
                $investment->maturity_date
            );

            if ($data['partial_withdraw'] == 'true' || $data['partial_withdraw'] === true) {
                $validation_data = array_add(
                    $validation_data,
                    'investment_value',
                    $investment->repo->getTotalValueOfAnInvestment()
                );

                $this->withdrawalForm->partial();

                if ($validation_data['investment_value'] < (float) $data['amount']) {
                    throw new Error('You cannot withdraw more than the Investment current Value');
                }
            }
        }

        $this->withdrawalForm->validate($validation_data);
    }
}
