<?php
/**
 * Date: 21/04/2016
 * Time: 3:32 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Approvals\Validators;

use App\Cytonn\Models\ClientTransactionApproval;

interface ClientInvestmentValidatorInterface
{
    public function validate(ClientTransactionApproval $approval);
}
