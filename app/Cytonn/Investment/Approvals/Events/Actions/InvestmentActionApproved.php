<?php

namespace Cytonn\Investment\Approvals\Events\Actions;

use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Approvals\Handlers\InvestmentRolloverApprovedHandler;
use Cytonn\Investment\Approvals\Handlers\InvestmentWithdrawApprovedHandler;

class InvestmentActionApproved
{
    public $approval;

    public $handlerClass;

    public $runningSchedule;

    public function __construct($approval, $runningSchedule = false)
    {
        $this->approval = $approval;

        $this->handlerClass = $this->getHandlerClass($approval->transaction_type);

        $this->runningSchedule = $runningSchedule;
    }

    public function getHandlerClass($action_name)
    {
        switch ($action_name) {
            case 'withdraw':
                return new InvestmentWithdrawApprovedHandler();
            case 'rollover':
                return new InvestmentRolloverApprovedHandler();
            default:
                throw new ClientInvestmentException('transaction type not supported');
        }
    }
}
