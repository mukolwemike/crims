<?php

namespace Cytonn\Investment\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use Auth;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Clients\Approvals\SchedulableApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Withdraw;
use Cytonn\Investment\Approvals\Validators\Actions\WithdrawalValidator;
use Cytonn\Investment\Events\ClientPaymentIsDue;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class InvestmentWithdrawApprovedHandler extends SchedulableApproval implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        (new WithdrawalValidator())->validate($approval);

        $data = $approval->payload;
        $data['approval_id'] = $approval->id;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        if ($investment->withdrawn) {
            throw new ClientInvestmentException('The investment has already been withdrawn');
        }

        $withdrawal = new Withdraw();

        is_null(Auth::user()) ? $user = User::findOrFail($approval->approved_by) : $user = Auth::user();

        //mature
        if ($data['premature'] == 'false' || $data['premature'] == false) {
            $amount = $investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date);

            if ($approval->scheduled && !$this->runningSchedule) {
                $schedule = ClientScheduledTransaction::add(
                    ['investment_id' => $investment->id, 'data' => $data,
                        'action' => 'withdraw', 'time' => $investment->maturity_date]
                );
            } else {
                $withdrawal->mature($investment, $user, $data);
            }
        } elseif (//premature & partial
            ($data['premature'] == true || $data['premature'] == 'true') &&
            (($data['partial_withdraw'] == 'true' || $data['partial_withdraw'] === true))
        ) {
            $amount = $data['amount'];

            if ($approval->scheduled && !$this->runningSchedule) {
                $schedule = ClientScheduledTransaction::add(
                    ['investment_id' => $investment->id, 'data' => $data,
                        'action' => 'withdraw', 'time' => $data['end_date']]
                );
            } else {
                $withdrawal->partial($investment, $user, $data);
            }
        } else { //just premature
            $amount = $investment->repo->getTotalValueOfInvestmentAtDate($data['end_date']);

            if ($approval->scheduled && !$this->runningSchedule) {
                $schedule = ClientScheduledTransaction::add(
                    ['investment_id' => $investment->id, 'data' => $data,
                        'action' => 'withdraw', 'time' => $data['end_date']]
                );
            } else {
                $withdrawal->premature($investment, $user, $data);
            }
        }

        isset($data['end_date']) ? $final_date = $data['end_date'] : $final_date = $investment->maturity_date;

        if (isset($schedule)) {
            $approval->schedule_id = $schedule->id;
            $approval->save();

            $this->raise(new ClientPaymentIsDue(
                $amount,
                'Withdrawal',
                $investment,
                'withdraw',
                $final_date,
                $approval
            ));

            $this->dispatchEventsFor($this);
        }

        if (!$approval->approved) {
            $approval->approve();
        }
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        return [];
    }

    public function runTransaction($approval)
    {
        // TODO: Implement runTransaction() method.
    }
}
