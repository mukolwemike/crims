<?php

namespace Cytonn\Investment\Approvals\Handlers;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\User;
use Cytonn\Clients\Approvals\Handlers\ApprovalHandlerInterface;
use Cytonn\Clients\Approvals\SchedulableApproval;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Action\Rollover;
use Cytonn\Investment\Approvals\Validators\Actions\RolloverValidator;
use Cytonn\Investment\Events\ClientPaymentIsDue;
use Illuminate\Support\Facades\DB;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventGenerator;

class InvestmentRolloverApprovedHandler extends SchedulableApproval implements ApprovalHandlerInterface
{
    use EventGenerator, DispatchableTrait;

    public function handle(ClientTransactionApproval $approval)
    {
        $validator = new RolloverValidator();

        if ($approval->scheduled && !$this->runningSchedule) {//schedule
            $validator->scheduling = true;
        }

        $validator->validate($approval);

        $data = $approval->payload;

        $oldInvestment = ClientInvestment::findOrFail($data['investment']);

        if ($oldInvestment->withdrawn) {
            throw new ClientInvestmentException('The investment has already been withdrawn');
        }

        is_null(\Auth::user()) ? $user = User::findOrFail($approval->approved_by) : $user = \Auth::user();

        DB::beginTransaction();
        $clientRolloverApp = $oldInvestment->application;

        if (!isset($data['on_call'])) {
            $data['on_call'] = false;
        }

        if ($data['reinvest'] == 'withdraw') {
            $amount = $oldInvestment
                    ->repo->getTotalValueOfInvestmentAtDate($oldInvestment->maturity_date) - $data['amount'];
            $amountToWithdraw = $data['amount'];
        } elseif ($data['reinvest'] == 'reinvest') {
            $amount = $data['amount'];
            $amountToWithdraw = $oldInvestment
                    ->repo->getTotalValueOfInvestmentAtDate($oldInvestment->maturity_date) - $data['amount'];
        } else {
            throw new ClientInvestmentException('Only reinvest and withdraw supported');
        }

        $data = array_add($data, 'approval_id', $approval->id);


        if ($approval->scheduled && !$this->runningSchedule) {//schedule
            $schedule = ClientScheduledTransaction::add([
                'investment_id' => $oldInvestment->id,
                'data' => $data, 'action' =>
                    'rollover', 'time' => $oldInvestment->maturity_date
            ]);
        } else { //perform the action
            //fill new data to application
            !is_null($clientRolloverApp) ?: $clientRolloverApp = new ClientInvestmentApplication();

            $clientRolloverApp->amount = $amount;
            $clientRolloverApp->application_type_id = 2;
            $clientRolloverApp->parent_application_id = $oldInvestment->application_id;
            $clientRolloverApp->agreed_rate = $data['interest_rate'];

            $clientRolloverApp->save();

            $rollover = new Rollover();

            $rollover->single($oldInvestment, $clientRolloverApp, $user, $data);
        }

        if (isset($schedule)) {
            $approval->schedule_id = $schedule->id;
            $approval->save();

            $this->raise(new ClientPaymentIsDue(
                $amountToWithdraw,
                'Rollover',
                $oldInvestment,
                'withdraw',
                $oldInvestment->maturity_date,
                $approval
            ));
            $this->dispatchEventsFor($this);
        }

        if (!$approval->approved) {
            $approval->approve();
        }

        $instruction = $this->getInstruction($data);

        if ($instruction) {
            $instructionInvestment = ClientInvestment::withTrashed()->findOrFail($instruction->investment_id);
            $instructionInvestment->withdrawn = 1;
            $instructionInvestment->save();
        }

        DB::commit();
    }

    public function prepareView(ClientTransactionApproval $approval, array $vars = null)
    {
        // TODO: Implement prepareView() method.
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    private function getInstruction(array $data)
    {
        if (!isset($data['client_instruction']) or is_null($data['client_instruction'])) {
            return null;
        }

        return ClientInvestmentInstruction::findOrFail($data['client_instruction']);
    }

    public function runTransaction($approval)
    {
        // TODO: Implement runTransaction() method.
    }
}
