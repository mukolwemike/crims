<?php
/**
 * Date: 06/01/2016
 * Time: 11:11 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidationException;
use Laracasts\Validation\FormValidator;

/**
 * Class CombinedRolloverForm
 *
 * @package Cytonn\Investment\Forms
 */
class CombinedRolloverForm extends FormValidator
{
    /**
     * @var array
     */
    public $rules = [
        'invested_date'=>'required|date|after:old_investment_maturity_date',
        'maturity_date'=>'required|date|after:invested_date',
        'interest_rate'=>'required|numeric',
        'amount'=>'required | numeric'
    ];

    /**
     * Use different validator to display the errors
     *
     * @param  $formData
     * @return array
     */
    public function validates($formData)
    {
        try {
            $this->validate($formData);

            return (object) ['valid'=>true];
        } catch (FormValidationException $e) {
            return (object) ['valid'=>false, 'errors'=>$e->getErrors()];
        }
    }
}
