<?php
/**
 * Date: 26/10/2015
 * Time: 3:57 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class PaymentForm extends FormValidator
{
    protected $rules = [
        'amount'=>'required|numeric',
        'date_paid'=>'required|before:maturity_date|after:invested_date'
    ];
}
