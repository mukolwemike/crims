<?php
/**
 * Date: 9/11/15
 * Time: 10:11 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class WithdrawalForm extends FormValidator
{
    public $rules = [

    ];

    //no fields to be validated for mature withdrawal

    public function premature()
    {
        $this->rules = array_add($this->rules, 'premature', 'required');
        $this->rules = array_add(
            $this->rules,
            'end_date',
            'required|date|after:invested_date|before:maturity_date'
        );
        //end date for premature must be between invested and maturity dates
    }

    public function partial()
    {
        $this->rules = array_add($this->rules, 'amount', 'required|numeric');
        $this->rules = array_add($this->rules, 'new_maturity_date', 'date|after:end_date');
    }
}
