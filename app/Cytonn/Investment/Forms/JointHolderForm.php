<?php
/**
 * Date: 17/11/2015
 * Time: 11:35 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class JointHolderForm extends FormValidator
{
    public $rules = [
        'title_id'=>'required',
        'lastname'=>'required',
        'firstname'=>'required',
        'gender_id'=>'required',
        'id_or_passport'=>'required',
        'email' => 'required|email',
        'country_id'=>'required',
        'telephone_home'=>'required',
        'town'=>'required',
        'method_of_contact_id'=>'required'
    ];
}
