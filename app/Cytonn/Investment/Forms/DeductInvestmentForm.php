<?php
/**
 * Date: 12/03/2016
 * Time: 11:53 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class DeductInvestmentForm extends FormValidator
{
    public $rules = [
    //        'date'=>'required|date|same_or_before:maturity_date',
        'amount'=>'required|numeric|max:interest_accrued',
        'narrative'=>'required'
    ];
}
