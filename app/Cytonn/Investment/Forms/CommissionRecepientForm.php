<?php
/**
 * Date: 05/10/2015
 * Time: 11:25 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class CommissionRecepientForm extends FormValidator
{
    public $rules = [
        'recipient_type_id'=>'required',
        'name'=>'required',
        'email'=>'required|email|unique:commission_recepients',
        'phone'=>'required',
        'phone_country_code'=>'required',
        'reporting_date'=>'required|date'
    ];

    public $messages = [
        'staff_id.unique'=>'An FA with the email already exists',
        'reporting_date.required'=>'Please provide the reporting of the FA'
    ];

    public function editing()
    {
        $this->rules['email'] = 'required|email';
    }
}
