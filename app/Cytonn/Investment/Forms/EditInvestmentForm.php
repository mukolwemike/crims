<?php
/**
 * Date: 23/11/2015
 * Time: 10:02 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class EditInvestmentForm extends FormValidator
{
    public $rules = [
        'investment'=>'required',
        'invested_date'=>'date',
        'maturity_date'=>'date|after:invested_date',
        'interest_rate'=>'numeric',
        'amount'=>'numeric'
    ];
}
