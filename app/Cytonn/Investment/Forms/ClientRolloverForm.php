<?php
/**
 * Date: 9/11/15
 * Time: 2:46 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class ClientRolloverForm extends FormValidator
{
    public $max;

    public $rules = [
        'invested_date'=>'required|date|after:old_investment_maturity_date',
        'maturity_date'=>'required|date|after:invested_date',
        'interest_rate'=>'required|numeric',
        'amount'=>'required | numeric'
    ];
}
