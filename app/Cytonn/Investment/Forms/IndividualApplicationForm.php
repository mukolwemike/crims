<?php
/**
 * Date: 9/7/15
 * Time: 9:56 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

/**
 * Class IndividualApplicationForm
 *
 * @package Cytonn\Investment\Forms
 */
class IndividualApplicationForm extends FormValidator
{
    /**
     * @var array
     */
    public $rules = [];

    /**
     * @var array
     */
    protected $all_rules = [
        'funds_source_id'=>'required',
    //        'investor_account_name'=>'required',
    //        'investor_account_number'=>'required',
    //        'investor_bank_branch'=>'required',
        'branch_id'=>'required_with:investor_account_name|numeric',
        'terms_accepted'=>'required|accepted',
        'town'=>'required',
        'title'=>'required',
        'lastname'=> 'required',
        'firstname'=> 'required',
        'gender_id' => 'required',
        'email' => 'required|email',
        'method_of_contact_id' => 'required',
        'phone' => 'required',
        'postal_code'=>'required_with:postal_address',
        'street'=>'required_without:postal_address',
        'preferredname' => 'nullable|min:3'
    ];

    protected $application_details_rules = [
        'product_id'=>'required',
        'amount'=>'required|numeric',
        'tenor'=>'required|numeric',
        'agreed_rate'=>'required|numeric'
    ];

    /**
     * @var array
     */
    protected $incomplete_rules = [
        'lastname'=> 'required',
        'firstname'=> 'required',
        'product_id'=>'required',
        'amount'=>'required|numeric',
        'tenor'=>'required|numeric',
        'agreed_rate'=>'required|numeric'
    ];


    /**
     * @param ValidatorFactory $validator
     */
    public function __construct(ValidatorFactory $validator)
    {
        $this->rules = $this->all_rules + $this->application_details_rules + $this->incomplete_rules;

        parent::__construct($validator);
    }

    /**
     * toggle incomplete
     */
    public function incomplete()
    {
        $this->rules = $this->incomplete_rules;
    }

    public function applicationDetails()
    {
        $this->rules = $this->application_details_rules;
    }
}
