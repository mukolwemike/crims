<?php
/**
 * Date: 18/10/2015
 * Time: 8:15 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class CommissionRecipientTypeForm extends FormValidator
{
    public $rules = [
        'name'=>'required|unique:commission_recepient_type',
        'rate_id'=>'required',
    ];
}
