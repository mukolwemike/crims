<?php
/**
 * Date: 9/10/15
 * Time: 9:19 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class BusinessConfirmationForm extends FormValidator
{
    public $rules = [
        'product_id'=>'required',
        'invested_date'=>'required|date',
        'maturity_date'=>'required|date|same_or_after:invested_date',
        'interest_rate'=>'required|numeric',
        'commission_recepient'=>'required',
    //        'description'=>'required',
        'commission_rate'=>'required'
    ];

    protected $messages = [
        'product_id.required'=>'You must select a product',
        'invested_date.required'=>'Please enter the date when the Investment starts',
        'maturity_date.required'=>'Please enter the date when the investment matures',
        'description.required'=>'Please enter the description of the transaction',
        'interest_rate.required'=>'Please enter the agreed interest rate of the investment'
    ];
}
