<?php
/**
 * Date: 29/02/2016
 * Time: 8:08 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class RollbackInvestmentForm extends FormValidator
{
    public $rules = [
        'reason'=>'required|min:10'
    ];
}
