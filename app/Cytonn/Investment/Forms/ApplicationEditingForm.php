<?php
    /**
     * Date: 23/10/15
     * Time: 1:56 PM
     *
     * @author Edwin Mukiri <emukiri@cytonn.com>
     * Project: crm
     * Cytonn Technology
     */

    namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class ApplicationEditingForm extends FormValidator
{
    public $rules = [
    'funds_source_id'=>'required',
    'product_id'=> 'required',
    'amount'=> 'required',
    'tenor' => 'required',
    'agreed_rate' => 'required'

    ];
}
