<?php
/**
 * Date: 9/14/15
 * Time: 10:08 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class ClientTopupForm extends FormValidator
{
    public $rules = [
        'invested_date'=>'required|date',
        'maturity_date'=>'required|date|same_or_after:invested_date',
        'amount'=>'required|numeric',
        'interest_rate'=>'required|numeric',
        'description'=>'required',
        'interest_payment_date'=>'required_with:interest_payment_interval'
    ];
}
