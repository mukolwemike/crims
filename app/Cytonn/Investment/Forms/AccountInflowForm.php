<?php

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

class AccountInflowForm extends FormValidator
{
    public $create_rules = [
        'category'          =>  'required',
        'project_id'        =>  'numeric',
        'product_id'        =>  'numeric',
        'entity_id'         =>  'numeric',
        'unit_fund_id'      =>  'numeric',
        'new_client'        =>  'required|boolean',
        'received_from'     =>  'string|nullable',
        'client_id'         =>  'numeric|nullable',
        'recipient_known'   =>  'required|boolean',
        'recipient_id'      =>  'numeric',
        'amount'            =>  'required|numeric',
        'date'              =>  'required|date',
        'entry_date'        =>  'required_if:source,cheque|date',
        'received_date'     =>  'required_if:source,cheque|date',
        'cheque_file'       =>  'required_if:source,cheque|file'
    ];

    public $update_rules = [
        'client_id'         =>  'required|numeric',
    ];

    public $messages = [
        'category.required'=>'Please select a category',
        'new_client.required'=>'Please select if client is new or not',
        'received_from.alpha'=>'The client\'s name should be alphabetic characters',
        'recipient_known.required'=>'Please select if the FA is known',
        'bank_reference_no.required'=>'Please enter the bank reference no.',
        'amount.required'=>'Please enter the amount',
        'date.required'=>'Please enter the date'
    ];

    /**
     * @param ValidatorFactory $validator
     */
    public function __construct(ValidatorFactory $validator)
    {
        $this->rules = $this->create_rules;

        parent::__construct($validator);
    }

    /**
     * Handle incomplete requests
     */
    public function update($input)
    {
        $this->rules = $this->update_rules;
        $this->validate($input);
    }
}
