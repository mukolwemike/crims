<?php
/**
 * Date: 13/01/2016
 * Time: 12:41 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class RemoveApplicationForm extends FormValidator
{
    public $rules = [
        'reason'=>'required|min:30'
    ];
}
