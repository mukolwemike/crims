<?php
/**
 * Date: 11/01/2016
 * Time: 11:08 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class ContactPersonAddForm extends FormValidator
{
    public $rules = [
        'name'=>'required',
        'email'=>'email|nullable',
        'address'=>'required'
    ];
}
