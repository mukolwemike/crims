<?php

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class InterestPaymentScheduleForm extends FormValidator
{
    public $rules = [
        'date'                      =>  'required|date',
        'amount'                    =>  'required|numeric',
        'description'               =>  'required|min:10|max:255',
        'investment_id'             =>  'required|numeric'
    ];

    public $messages = [
        'date.required'             =>  'The date field is required',
        'amount.required'           =>  'The amount field is required',
        'amount.numeric'            =>  'The amount should be numeric',
        'description.required'      =>  'The description field is required',
        'description.min'           =>  'The description should be a minimum of 10 characters',
        'description.max'           =>  'The description should be a maximum of 255 characters',
    ];
}
