<?php
/**
 * Date: 9/7/15
 * Time: 9:56 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

/**
 * Class CorporateApplicationForm
 *
 * @package Cytonn\Investment\Forms
 */
class CorporateApplicationForm extends FormValidator
{


    /**
     * @var array
     */
    public $rules = [];

    /**
     * @var array
     */
    protected $incomplete_rules = [
        'registered_name' => 'required',
        'product_id'=>'required',
        'amount'=>'required|numeric',
        'tenor'=>'required|numeric',
        'agreed_rate'=>'required|numeric'
    ];

    /**
     * @var array
     */
    protected $all_rules = [
        'funds_source_id'=>'required',
    //        'investor_account_name'=>'required',
    //        'investor_account_number'=>'required',
        'branch_id'=>'required_with:investor_account_name|numeric',
    //        'investor_bank_branch'=>'required',
        'terms_accepted'=>'required|accepted',
        'product_id'=>'required',
        'amount'=>'required|numeric',
        'tenor'=>'required|numeric',
        'agreed_rate'=>'required|numeric',

        'corporate_investor_type' => 'required',
        'registered_name' => 'required',
        'registered_address' => 'required',
        'phone' => 'required',
        'email' => 'required|email',
        'method_of_contact_id'=>'required',
        'postal_code'=>'required_with:postal_address',
        'street'=>'required_without:postal_address',
        'contact_person_title'=>'required',
        'contact_person_fname'=>'required',
        'contact_person_lname'=>'required'
        ];


    /**
     * Constructor
     *
     * @param ValidatorFactory $validator
     */
    public function __construct(ValidatorFactory $validator)
    {
        $this->rules = $this->incomplete_rules + $this->all_rules;

        parent::__construct($validator);
    }

    /**
     * Use the applicable rules for incomplete applications
     */
    public function incomplete()
    {
        $this->rules = $this->incomplete_rules;
    }
}
