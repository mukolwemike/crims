<?php

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;
use Laracasts\Validation\FactoryInterface as ValidatorFactory;

class TransferCashToAnotherClientForm extends FormValidator
{
    public $rules = [
        'balance_type_id' => 'required',
        'amount' => 'required|numeric',
        'date' => 'required|date',
        'category' => 'required',
        'project_id' => 'numeric',
        'product_id' => 'numeric',
        'entity_id' => 'numeric',
        'unit_fund_id' => 'numeric',
        'exchange_rate' => 'required|numeric',
        'client_id' => 'required|numeric',
        'recipient_known' => 'required|boolean',
        'recipient_id' => 'numeric',
    ];

    public $messages = [
        'category.required'=>'Please select a category',
        'amount.required'=>'Please enter the amount',
        'date.required'=>'Please enter the transfer date',
        'client_id.required'=>'Please specify the client being transferred to',
        'exchange_rate.alpha'=>'Please enter the exchange rate',
        'recipient_known.required'=>'Please select if the FA is known',
    ];
}
