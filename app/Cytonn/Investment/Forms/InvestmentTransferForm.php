<?php
/**
 * Date: 22/04/2016
 * Time: 8:33 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\Forms;

use Laracasts\Validation\FormValidator;

class InvestmentTransferForm extends FormValidator
{
    public $rules = [
        'client_id'=>'required',
        'date'=>'required|date',
        'narration'=>'required'
    ];

    public $messages = [
        'client_id.required'=>'Please select an existing client'
    ];
}
