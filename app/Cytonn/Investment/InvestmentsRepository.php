<?php
    /**
     * Date: 13/10/2015
     * Time: 9:43 AM
     * @author Mwaruwa Chaka <mchaka@cytonn.com>
     * Project: crm
     * Cytonn Technology
     */

namespace Cytonn\Investment;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBusinessConfirmation;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionRate;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\User;
use App\Exceptions\CrimsException;
use App\Http\CrimsClient\Transformers\InvestmentTransformer;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Calculate\Calculator;
use Cytonn\Mailers\BusinessConfirmationMailer;
use Cytonn\Models\ClientInvestmentWithdrawal;

/**
     * Class InvestmentsRepository
     * @package Cytonn\Investment
     */
class InvestmentsRepository
{
    use CalculatorTrait;

    public static $calculated = [];

    const MAX_STORED = 10;

    protected $totalInvestmentBalance;

    protected $totalInvestmentInterest;

    /**
     * @var
     */
    public $investment;

    /**
     * InvestmentsRepository constructor.
     *
     * @param $investment
     */
    public function __construct(ClientInvestment $investment = null)
    {
        is_null($investment) ? $this->investment = new ClientInvestment() : $this->investment = $investment;
    }

    /**
     * Get the number of days an investment spans
     *
     * @param  null $endDate
     * @return int
     */
    public function getTenorInDays($endDate = null)
    {
        $maturity_date = new Carbon($endDate);

        if (is_null($endDate)) {
            $maturity_date = $this->investment->maturity_date->copy();
        }

        return $this->investment->invested_date->copy()->diffInDays($maturity_date);
    }

    public function getTenorInDaysAsAtDate($date = null)
    {
        $invested_date = new Carbon($this->investment->invested_date);

        $maturity_date = new Carbon($this->investment->maturity_date);

        $date = $date ? $date : Carbon::now();

        $maturity_date = $maturity_date <= $date ? $maturity_date : $date;

        return $invested_date->copy()->diffInDays($maturity_date);
    }

    /**
     * Get the number of months an investment spans
     *
     * @param  null $endDate
     * @return int
     */
    public function getTenorInMonths($endDate = null)
    {
        $maturity_date = new Carbon($endDate);

        if (is_null($endDate)) {
            $maturity_date = new Carbon($this->investment->maturity_date);
        }

        return $this->investment->invested_date->copy()->diffInMonths($maturity_date);
    }

    /**
     * Get the number of years an investment spans
     *
     * @param  null $endDate
     * @return int
     */
    public function getTenorInYears($endDate = null)
    {
        $maturity_date = new Carbon($endDate);

        if (is_null($endDate)) {
            $maturity_date = new Carbon($this->investment->maturity_date);
        }

        return $this->investment->invested_date->copy()->diffInYears($maturity_date);
    }

    /**
     * Get the number of months remaining in the investments tenor
     *
     * @param  null $endDate
     * @return int
     */
    public function getTenorToMaturityInMonths($startDate = null)
    {
        $startDate = ($startDate) ? Carbon::parse($startDate) : Carbon::now();

        $maturity_date = Carbon::parse($this->investment->maturity_date);

        return $startDate->diffInMonths($maturity_date);
    }


    /**
     * @param null $date
     * @param bool $as_at_next_day
     * @return Calculator $calc
     */
    protected function calculated($date = null, $as_at_next_day = false, $interestRate = null)
    {
        $hash = sha1(((string)$date) . '' . (string)$as_at_next_day . 'investment' . $this->investment->id);

        if (isset(static::$calculated[$hash])) {
            return static::$calculated[$hash];
        }

        $date = Carbon::parse($date);
        $calculated = $this->investment->calculate($date, $as_at_next_day);

        $this->setCalculated($calculated, $hash);

        return $calculated;
    }

    private function setCalculated($calculated, $hash)
    {
        static::$calculated[$hash] = $calculated;

        if (count(static::$calculated) > static::MAX_STORED) {
            $first_key = array_first(array_keys(static::$calculated));

            unset(static::$calculated[$first_key]);
        }
    }

    public function getPrepared($date = null, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->getPrepared();
    }


    /**
     * @return int
     */
    public function getDaysElapsed()
    {
        $endDate = $this->getCurrentEffectiveInterestDate($this->investment->maturity_date);

        return (new Carbon($this->investment->invested_date))->diffInDays($endDate);
    }

    public function principal(Carbon $date = null)
    {
        return $this->calculated($date, false)->principal();
    }

    /**
     * @return mixed
     */
    public function getNetInterestForInvestment()
    {
        return $this->calculated($this->investment->maturity_date)->netInterest();
    }

    /**
     * @return mixed
     */
    public function getGrossInterestForInvestment()
    {
        return $this->calculated($this->investment->maturity_date)->grossInterest();
    }

    public function netInterestAfterWithdrawals($date = null, $next_day = false)
    {
        return $this->calculated($date, $next_day)->netInterest();
    }

    public function getNetInterestForInvestmentAtDate($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->netInterest();
    }

    public function getWithdrawals(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        return $this->investment->withdrawals()->before($date)->get();
    }


    public function getTopupsTo(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        return $this->investment->topupsTo()->before($date)->get();
    }

    /**
     * @param $date
     * @param bool $as_at_next_day
     * @return mixed
     */
    public function getGrossInterestForInvestmentAtDate($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->grossInterest();
    }

    public function grossInterestBeforeDeductions($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->grossInterestBeforeDeductions();
    }

    public function netInterestBeforeDeductions($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->netInterestBeforeDeductions();
    }

    public function active(Carbon $date = null)
    {
        return $this->getTotalValueOfInvestmentAtDate($date) >= 1;
    }

    /**
     * @param $date
     * @return float|int
     */
    public function getWithholdingTaxAtDate($date)
    {
        return $this->calculated($date)->withholdingTax();
    }

    /**
     * Calculate gross interest for period
     *
     * @param  $start
     * @param  $end
     * @return mixed
     */
    public function getGrossInterestForInvestmentBetweenDates($start, $end)
    {
        $beginning = $this->calculated($start)->grossInterestBeforeDeductions();
        $ending = $this->calculated($end)->grossInterestBeforeDeductions();

        return $ending - $beginning;
    }

    /**
     * @param $start
     * @param $end
     * @return mixed
     */
    public function getNetInterestForInvestmentBetweenDates($start, $end)
    {
        $beginning = $this->calculated($start)->netInterestBeforeDeductions();
        $ending = $this->calculated($end)->netInterestBeforeDeductions();

        return $ending - $beginning;
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getTotalValueOfInvestmentAtDate($date, $as_at_next_day = false)
    {
        return $this->calculated($date, $as_at_next_day)->value();
    }

    /*
     * Get the total value of an investment in kenya shillings equivalent
     */
    public function getKesConvertedValue($amount)
    {
        $currency = $this->investment->product->currency;

        if ($currency->id == 1) {
            return $amount;
        } else {
            $exchangeRate = $currency->toExchangeRates()->latest()->first();

            if (!$exchangeRate) {
                throw new ClientInvestmentException(
                    "Exchange rate for currency $currency->code was not found"
                );
            }

            return $amount * $exchangeRate->rate;
        }
    }

    /**
     * @param $maturity_date
     * @return Carbon
     */
    public function getCurrentEffectiveInterestDate($maturity_date)
    {
        $maturity_date = new Carbon($maturity_date);

        if ($maturity_date->isPast()) {
            return $maturity_date;
        }

        return Carbon::today();
    }

    /**
     * @return mixed
     */
    public function getTotalValueOfAnInvestment()
    {
        return $this->calculated(null, false)->value();
    }

    /**
     * Calculate interest as at the next day
     */
    public function getTotalValueAsAtEndOfDay()
    {
        return $this->calculated(null, true)->value();
    }

    /**
     * Return the total payments made on an investment
     *
     * @param  null $date
     * @return number
     */
    public function getTotalPayments($date = null)
    {
        return $this->getWithdrawals($date)->sum('amount');
    }

    /**
     * Return the amount deposited to account for the investment
     *
     * @return int|mixed
     */
    public function inflow()
    {
        if ($this->investment->isNew()) {
            $type = $this->investment->investmentType->name;

            if ($type == 'partial_withdraw' || $type == 'rollover') {
                throw new CrimsException("Nope");
            }

            return $this->investment->amount;
        }

        return $this->topupAmount();
    }

    /*
     * Get the value of an investment with respect to the FA productions
     */
    public function inflowNet()
    {
        $withdrawn = $this->getWithdrawals($this->investment->maturity_date)->sum('amount');
        $net = $this->investment->amount - $withdrawn;

        if ($net < 0) {
            return 0;
        }

        return $net;
    }

    /**
     * Check whether investment was withdrawn on some given date
     *
     * @param  $date
     * @return bool
     */
    public function withdrawnOnDate($date)
    {
        return $this->getWithdrawals($date)->sum('amount');
    }

    /**
     * Get the amount topped up to investment
     * This only applies to combined rollovers where a client can combine and top up investments
     * @return int
     */
    public function topupAmount()
    {
        return $this->investment->topupsFrom()->sum('amount');
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getTotalAvailableInterestAtDate($date, $asAtNextDay = false, $interestRate = null)
    {
        return $this->calculated($date, $asAtNextDay, $interestRate)->netInterest();
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getTotalAvailableInterestAtNextDay($date)
    {
        return $this->getTotalAvailableInterestAtDate($date, true);
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getAvailablePrincipal($date, $asAtNextDay = false, $interestRate = null)
    {
        return $this->calculated($date, $asAtNextDay, $interestRate)->principal();
    }

    /**
     * @param null $date
     * @return mixed|number
     */
    public function getWithdrawnAmount($date = null)
    {
        return $this->getWithdrawals($date)->sum('amount');
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getInvestmentCurrencyType($id)
    {
        $investment = ClientInvestment::findOrFail($id);

        return $investment->product->currency->code;
    }

    /**
     * @return int
     */
    public function checkIfConfirmationHasBeenSent()
    {
        return ClientBusinessConfirmation::where('investment_id', $this->investment->id)->count() > 0;
    }

    /**
     * @return mixed
     */
    public function getCommissionRecipientId()
    {
        $commission = Commission::where('investment_id', $this->investment->id)->first();

        if (!is_null($commission)) {
            return $commission->recipient_id;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getCommissionRate()
    {
        $recipientType = $this->investment->commission->recipient->type;

        $product = $this->investment->product;

        //Uncomment out if product specific rates come up
        //$rate = $recipientType->commissionRates()->forCurrency($product->currency)->forProduct($product)->first();
        //
        //        if($rate)
        //        {
        //            return $rate->rate;
        //        }

        $rate = $recipientType->commissionRates()->forCurrency($product->currency)->first();

        if ($rate) {
            return $rate->rate;
        }

        $rate = $recipientType->commissionRates()->first();

        if ($rate) {
            return $rate->rate;
        }

        return null;
    }

    /*
     * Get the commission rates for the product and recipient
     */
    public function getCommissionRatesForProductAndRecipient(
        Product $product = null,
        CommissionRecepient $recipient = null,
        $date = null
    ) {
        $date = ($date) ? Carbon::parse($date) : Carbon::now();

        if (is_null($product) && $this->investment) {
            $product = $this->investment->product;
        }

        if (is_null($recipient) && $this->investment) {
            $recipient = $this->investment->commission->recipient;
        }

        $ratesArray = array();

        $recipientType = $this->getRecipientType($recipient, $date);

        if ($product) {
            $rates = CommissionRate::where('commission_recipient_type_id', $recipientType)
                ->forCurrency($product->currency);

            if ($rates->forProduct($product)->exists()) {
                $rates = $rates->forProduct($product);
            } else {
                $rates = CommissionRate::where('commission_recipient_type_id', $recipientType)
                    ->forCurrency($product->currency);
            }
        } else {
            $rates = CommissionRate::where('commission_recipient_type_id', $recipientType);
        }

        foreach ($rates->get() as $rate) {
            $ratesArray[] = $rate;
        }

        $otherRates = CommissionRate::whereNull('commission_recipient_type_id')
            ->whereNull('product_id');

        if ($product) {
            $otherRates->forCurrency($product->currency);
        }

        foreach ($otherRates->get() as $rate) {
            $ratesArray[] = $rate;
        }

        return $ratesArray;
    }

    public function getRecipientType($recipient, $date)
    {
        $previous = $recipient->commissionRecipientPositions->where('start_date', '<=', $date)
            ->where('end_date', '>=', $date)->last();

        if (count($previous) > 0) {
            return $previous->recipient_type_id;
        }

        return $recipient->recipient_type_id;
    }

    //to be refactored
    public function getCommissionsPaidFromInvestment()
    {
        $totalCommissions = $this->investment->commission->schedules()->sum('amount');

        $clawback = $this->investment->commission->clawBacks()->sum('amount');

        return ($totalCommissions - $clawback);
    }

    //to be refactored
    public function getScheduledCommissions()
    {
        return $this->investment->commission->schedules->sum('amount');
    }

    //to be refactored
    public function getCommissionClawbacks()
    {
        return $this->investment->commission->clawBacks->sum('amount');
    }

    /**
     * @param Carbon|null $date
     * @return mixed
     */
    public function getTotalCommissionAlreadyPaidToFa(Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $bulk = BulkCommissionPayment::where('end', '<', $date)
            ->orderBy('end', 'DESC')->first();

        if ($bulk) {
            $date = Carbon::parse($bulk->end);
        }

        $commissions = $this->investment->commission->schedules()
            ->where('date', '<=', $date)
            ->clawBack(false)
            ->sum('amount');

        $clawbacks = $this->investment->commission->clawBacks()
            ->where('date', '<=', $date)
            ->sum('amount');

        return $commissions - $clawbacks;
    }

    /**
     * Get the total value at maturity or withdrawal date
     * @return mixed
     */
    public function getFinalTotalValueOfInvestment()
    {
        return $this->calculated($this->investment->maturity_date, true)->total();
    }

    public function getFinalValueOnWithdrawal()
    {
        $date = $this->investment->withdrawn ? Carbon::parse($this->investment->withdrawal_date)->subDay() :
            Carbon::parse($this->investment->maturity_date);

        return $this->calculated($date, true)->total();
    }

    /**
     * @return mixed
     */
    public function getFinalGrossInterest()
    {
        return $this->calculated($this->investment->maturity_date, true)->grossInterest();
    }

    /**
     * @return mixed
     */
    public function getFinalNetInterest()
    {
        return $this->calculated($this->investment->maturity_date, true)->netInterest();
    }

    /**
     * Check whether an investment is compliant
     *
     * @return mixed
     */
    public function checkCompliance()
    {
        return $this->investment->client->repo->checkCompliance();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function checkIfTopUpIsDuplicate(array $data)
    {
        $data = array_only(
            $data,
            ['client_id', 'invested_date', 'maturity_date', 'amount', 'interest_rate', 'investment_type_id', 'product_id']
        );
        return (bool)ClientInvestment::where($data)->first();
    }

    /*
     * Get the last withdrawal date
     */
    public function getPreviousWithdrawalDate(Carbon $date)
    {
        $withdrawal = $this->investment->withdrawals()
            ->where('type_id', 1)
            ->where('withdraw_type', 'withdrawal')
            ->where('date', '<', $date)
            ->latest('date')
            ->first();

        return $withdrawal ? Carbon::parse($withdrawal->date) : Carbon::parse($this->investment->invested_date);
    }

    public function withdrawalToBeReinvested(Carbon $date)
    {
        return ClientInvestment::where('invested_date', '>', $date)
            ->whereHas('reinvestedFrom', function ($q) use ($date) {
                $q->where('date', '<=', $date)->where('investment_id', $this->investment->id);
            })->sum('amount');
    }

    public function getInvestmentDetails(Client $client)
    {
        $products = $client->repo->getActiveInvestmentsForClientQuery()
            ->orderBy('maturity_date')
            ->get()
            ->groupBy('product_id');

        return $products->map(function ($investments, $index) {
            $product = Product::find($index);

            $details = $this->investmentDetails($investments);

            $this->totalInvestmentBalance = 0;

            $this->totalInvestmentInterest = 0;

            $this->getTotalAmountInvestedInAProduct($details);

            return [
                'investments' => $details,
                'product' => [
                    'id' => $product->id,
                    'name' => $product->name,
                    'shortname' => $product->shortname,
                    'longname' => $product->longname,
                    'currency' => $product->currency->code,
                    'category' => 'sp'
                    ],

                'totals' => [
                    'no_of_investments' => count($investments),
                    'total_value' => $this->totalInvestmentBalance,
                    'last_investment_date' => $investments->sortByDesc('invested_date')
                        ->pluck('invested_date')->first(),
                    'next_maturity_date' => $investments->where('maturity_date', '>=', Carbon::today())
                        ->sortBy('maturity_date')
                        ->pluck('maturity_date')
                        ->first()
                ]

            ];
        })->values();
    }

    public function investmentDetails($ClientInvestments)
    {
        $investmentTransformer = (new InvestmentTransformer())->append(['activities', 'instructions']);

        return $ClientInvestments->map(function ($investment) use ($investmentTransformer) {
            return [
                'details' => $investmentTransformer->transform($investment),
            ];
        });
    }

    public function getTotalAmountInvestedInAProduct($investments)
    {
        $investments->each(function ($investment) {
            $this->totalInvestmentBalance += $investment['details']['total_value'];

            $this->totalInvestmentInterest += $investment['details']['net_interest'];
        });
    }

    public function clientInvInvestments($client)
    {
        $investments = $client->repo->getActiveInvestmentsForClientQuery()->get();

        $products = $investments->pluck('product_id')->unique();

        return $products->map(function ($product) use ($client) {

            $product = Product::findOrFail($product);

            $nextMaturityDate = $this->nextMaturityDate($client, $product);

            $lastInvestmentDate = $this->lastInvestmentDates($client, $product);

            $productInvestment = $this->productInvestments($client, $product);

            return [
                'name' => $product->name,
                'shortname' => $product->shortname,
                'currency' => $product->currency->code,
                'next_maturity_date' => ($nextMaturityDate) ? $nextMaturityDate->toFormattedDateString() : 'matured',
                'last_investment' => ($lastInvestmentDate) ? $lastInvestmentDate->toFormattedDateString() : '',
                'investments' => $productInvestment->count(),
                'amount' => $productInvestment->sum('amount'),
                'total_value' => $productInvestment->sum('total_value'),
                'product_id' => $product->id
            ];
        });
    }

    public function nextMaturityDate($client, $product)
    {
        return $client->repo->getActiveInvestmentForClientForProduct($product)
            ->where('maturity_date', '>=', Carbon::today())
            ->sortBy('maturity_date')
            ->pluck('maturity_date')
            ->first();
    }

    public function lastInvestmentDates($client, $product)
    {
        return $client->repo->getActiveInvestmentForClientForProduct($product)
            ->sortByDesc('invested_date')
            ->pluck('invested_date')
            ->first();
    }

    public function productInvestments($client, $product)
    {
        $investments = $client->repo->getActiveInvestmentForClientForProduct($product);

        return $investments->map(function ($investment) {

            return (new InvestmentTransformer())->transform($investment);
        });
    }

    public function upcomingMaturities(Client $client)
    {
        $today = Carbon::today();
        $end = Carbon::today()->addMonth(2);

        $investments = $client->repo->getActiveInvestmentsForClientQuery();

        return $investments
            ->whereBetween('maturity_date', [$today->copy(), $end->copy()])
            ->where('withdrawn', '!=', 1)
            ->get();
    }

    public function sendBusinessConfirmation(User $user = null)
    {
        $investment = $this->investment;

        $approval = ClientTransactionApproval::find($investment->approval_id);

        $approvers = $approval->steps()->take(1)->pluck('user_id')->toArray();

        $sender = is_null($approval) ? null : array_merge([$approval->sent_by], $approvers);

        $payload = [
            'principal' => $investment->amount,
            'sender' => $sender,
            'investment_id' => $investment->id,
            'interest_rate' => $investment->interest_rate,
            'on_call' => $investment->on_call,
            'duration' => $investment->repo->getTenorInDays(),
            'invested_date' => $investment->invested_date,
            'maturity_date' => $investment->maturity_date
        ];

        $client = Client::findOrFail($investment->client_id);

        (new BusinessConfirmationMailer())->sendBusinessConfirmationToClient($client, $payload, 'MBC', $user);

        $data = ['investment_id' => $payload['investment_id'], 'payload' => $payload];

        $sender = $sender && count($sender) > 0 ? $sender[0] : $sender;

        ClientBusinessConfirmation::add($data, $sender);

        return;
    }

    public function canWithdraw()
    {
        if ($this->investment->withdrawn) {
            return ['status' => false, 'reason' => 'Investment Withdrawn'];
        }

        $rollover = $this->investment->instructions()->ofType('rollover')
            ->where('inactive', 0)->count();

        if ($rollover) {
            return
                [
                    'status' => false,
                    'reason' => 'Rollover Scheduled'
                ];
        }

        $interest = $this->netInterestAfterWithdrawals(Carbon::today(), true);

        $withdrawal = $this->investment->withdrawals()->latest('created_at')->first();

        $i = $this->investment->instructions()->ofType('withdraw')
            ->where(
                function ($q) use ($interest) {
                    $q->where('amount_select', '!=', 'interest')
                        ->orWhere(
                            function ($q) use ($interest) {
                                $q->whereNotNull('amount')->where('amount', '<=', $interest);
                            }
                        );
                }
            )->where('inactive', 0);

        if ($withdrawal) {
            $i = $i->where('created_at', '>', $withdrawal->created_at);
        }

        if ($i->count() > 0) {
            return [
                'status' => false,
                'reason' => 'Withdrawal scheduled',
            ];
        }

        $maturity_date = Carbon::parse($this->investment->maturity_date);

        $today = Carbon::today();

        $fund = $this->investment->product->fund;

        $fund_name = null;

        if ($fund) {
            $fund_name = $fund->short_name ? $fund->short_name : $fund->name;
        }

        if ($fund && !$fund->allow_premature_withdrawal &&
            $today < $maturity_date &&
            $today->diffInMonths($maturity_date) > 1
        ) {
            return [
                'status' => false,
                'reason' => 'Premature withdrawal is not allowed in ' . $fund_name,
            ];
        }

        return [
            'status' => true,
            'reason' => 'No instruction given',
        ];
    }

    /*
     * SEIP
     */
    /**
     * @return mixed
     */
    public function nextInvestmentPaymentSchedule(Carbon $date = null)
    {
        $date = $date ? $date : $this->investment->invested_date;

        return $this->investment->childInvestmentPaymentSchedules()->paidBefore($date, false)->orderBy('date')
            ->first();
    }

    /**
     * @return InvestmentsRepository|mixed
     */
    public function getParentInvestment()
    {
        $parent = $this->investment->parentInvestment;

        return $parent ? $parent : $this->investment;
    }

    /**
     * @return mixed|null
     */
    public function getInvestmentPaymentScheduleDetail()
    {
        $detail = $this->investment->investmentPaymentScheduleDetail;

        if ($detail) {
            return $detail;
        }

        $parent = $this->investment->parentInvestment;

        if ($parent) {
            return $parent->investmentPaymentScheduleDetail;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function totalValueOfSchedules()
    {
        $parent = $this->getParentInvestment();

        return $parent->childInvestmentPaymentSchedules->sum(function ($schedule) {
            $calculate = $schedule->repo->calculate();

            return $calculate->total;
        });
    }

    /**
     * @return mixed
     */
    public function getSeipFinalTotalValueOfInvestment()
    {
        $parent = $this->getParentInvestment();

        return $this->totalValueOfSchedules() + $parent->repo->getFinalTotalValueOfInvestment();
    }

    /**
     * @return mixed
     */
    public function totalNetInterestOfSchedules()
    {
        $parent = $this->getParentInvestment();

        return $parent->childInvestmentPaymentSchedules->sum(function ($schedule) {

            $calculate = $schedule->repo->calculate();

            return $calculate->net_interest;
        });
    }

    /**
     * @return bool
     */
    public function isSeipParent()
    {
        if (!$this->investment->product->present()->isSeip) {
            return false;
        }

        return !is_null($this->investment->investmentPaymentScheduleDetail);
    }

    /**
     * @return bool
     */
    public function isSeipChild()
    {
        if (!$this->investment->product->present()->isSeip) {
            return false;
        }

        return is_null($this->investment->investmentPaymentScheduleDetail);
    }

    /**
     * @param Carbon|null $date
     * @param bool $as_at_next_date
     * @return int
     */
    public function getSeipChildrenInvestedValueAsAtDate(Carbon $date = null, $as_at_next_date = false)
    {
        if (!$this->investment->product->present()->isSeip) {
            return 0;
        }

        return $this->investment->childInvestments->sum(function (ClientInvestment $investment) use ($date, $as_at_next_date) {
            return $investment->repo->getTotalValueOfInvestmentAtDate($date, $as_at_next_date);
        });
    }
}
