<?php
/**
 * Author: Edwin Mukiri
 * Email: emukiri@cytonn.com / edwinmukiri@gmail.com
 * Date: 11/23/15
 * Time: 9:32 AM
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientScheduledTransaction;
use App\Cytonn\Models\ClientTransactionApproval;
use Carbon\Carbon;
use Cytonn\Authorization\Authorizer;

class ScheduledInvestmentApprovalRepository
{

    /**
     * ScheduledInvestmentApprovalRepository constructor.
     */
    public function __construct($form = null)
    {
        $this->authorizer = new Authorizer();
        $this->form = $form;
    }

    public function postScheduledWithdraw($id)
    {
        //TODO remove dd
        dd('scheduling');
        $this->authorizer->checkAuthority('addinvestment');

        $approval = ClientTransactionApproval::findOrFail($id);

        $data = $approval->payload;

        $investment = ClientInvestment::findOrFail($data['investment_id']);

        if ($investment->withdrawn == 1) {
            \Flash::error('This investment has already been withdrawn, you cannot withdraw it again');

            return \Redirect::back();
        }

        $data = array_add($data, 'approval_id', $approval->id);

        if ($data['premature'] == 'false' || $data['premature'] == false) {
            unset($data['end_date']);
            $this->form->validate($data);

            $time = (new Carbon($investment->maturity_date))->toDateTimeString();
            ClientScheduledTransaction::add([
                    'investment_id' => $investment->id,
                    'data' => $data, 'action' => 'withdraw',
                    'time' => $time]);
        } elseif ($data['premature'] == true || $data['premature'] == 'true') {
            $this->form->premature();

            $validation_data = $data;
            $validation_data = array_add($validation_data, 'invested_date', $investment->invested_date);
            $validation_data = array_add($validation_data, 'maturity_date', $investment->maturity_date);

            $this->form->validate($validation_data);

            $time = (new Carbon($data['end_date']))->toDateTimeString();
            ClientScheduledTransaction::add([
                    'investment_id' => $investment->id,
                    'data' => $data, 'action' => 'withdraw',
                    'time' => $time]);
        }

        $approval->approve();

        \Flash::message('Investment Withdrawal Scheduled');

        return \redirect('/dashboard/investments/');
    }

    public function postScheduledRollover($id)
    {
        dd('to be authorized');
        $this->authorizer->checkAuthority('addinvestment');

        $approval = ClientTransactionApproval::findOrFail($id);

        $data = $approval->payload;

        $oldInvestment = ClientInvestment::findOrFail($data['investment']);

        $validation_data = $data;
        $validation_data = array_add(
            $validation_data,
            'old_investment_maturity_date',
            $oldInvestment->maturity_date
        );
        $validation_data = array_add(
            $validation_data,
            'old_investment_amount',
            (float)$oldInvestment->repo->getTotalValueOfAnInvestment()
        );
        $validation_data['amount'] = (float)$validation_data['old_investment_amount'];


        if ($data['amount'] > $validation_data['old_investment_amount']) {
            \Flash::error('The amount rolled over should be less than total value');

            return \Redirect::back()->withInput();
        }

        $this->form->validate($validation_data);

        $clientRolloverApp = $oldInvestment->application;

        if ($data['reinvest'] == 'withdraw') {
            $amount = $oldInvestment->repo->getTotalValueOfAnInvestment() - $data['amount'];
        }
        if ($data['reinvest'] == 'reinvest') {
            $amount = $data['amount'];
        }

        if ($oldInvestment->withdrawn == 1) {
            \Flash::error('This investment has already been withdrawn, you can only rollover active investments');

            return \Redirect::back();
        }

        //fill new data to application
        !is_null($clientRolloverApp) ?: $clientRolloverApp = new ClientInvestmentApplication();

        $clientRolloverApp->amount = $amount;
        $clientRolloverApp->application_type_id = 2;
        $clientRolloverApp->parent_application_id = $oldInvestment->application_id;
        $clientRolloverApp->agreed_rate = $data['interest_rate'];

        $clientRolloverApp->save();

        $time = (new Carbon($oldInvestment->maturity_date))->toDateTimeString();
        $data = array_add($data, 'approval_id', $approval->id);
        ClientScheduledTransaction::add([
            'investment_id' => $oldInvestment->id,
            'data' => $data,
            'action' => 'rollover',
            'time' => $time
        ]);

        $approval->approve();

        \Flash::message('Investment Rollover has been Scheduled');

        return \redirect('/dashboard/investments/');
    }
}
