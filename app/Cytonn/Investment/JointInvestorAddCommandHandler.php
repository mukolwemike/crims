<?php namespace Cytonn\Investment;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientJointDetail;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\ContactEntity;
use App\Cytonn\Models\JointClientHolder;
use App\Cytonn\Models\ReservationForm;
use App\Cytonn\Models\ShareHolder;
use Laracasts\Commander\CommandHandler;

/**
 * Class JointInvestorAddCommandHandler
 *
 * @package Cytonn\Investment
 */
class JointInvestorAddCommandHandler implements CommandHandler
{

    /**
     * The existing client
     *
     * @var $client
     */
    private $client;


    /**
     * Handle the command - create or update a joint client for cms or real estate
     *
     * @param  object $command
     * @return void
     * @throws \Throwable
     */
    public function handle($command)
    {
        $data = $command->data;

        \DB::transaction(
            function () use ($data) {
                $joint = $this->getJoint($data);

                $this->saveContact($joint, $data);
            }
        );
    }

    /**
     * Create the joint client from reservation form or application form or get an existing one
     *
     * @param  array $data
     * @return ClientJointDetail|JointInvestorAddCommandHandler|\Illuminate\Support\Collection|static
     */
    protected function getJoint(array $data)
    {
        if (isset($data['application_id'])) {
            return $this->saveJointFromApplication($data);
        } elseif (isset($data['reservation_form_id'])) {
            return $this->saveJointFromReservation($data);
        } elseif (isset($data['share_holding_id'])) {
            return $this->saveJointFromShareHolding($data);
        } elseif (isset($data['unit_fund_application_id'])) {
            return $this->saveJointFromUnitFundApplication($data);
        } elseif (isset($data['existing_client_id'])) {
            return $this->saveClientJoint($data);
        } else {
            return $this->getExistingJoint($data);
        }
    }

    /**
     * Get the joint client from cms application form
     *
     * @param  array $data
     * @return mixed
     */
    protected function saveJointFromApplication(array $data)
    {
        $app = ClientInvestmentApplication::findOrFail($data['application_id']);

        is_null($data['joint_id'])
            ? $joint = new ClientJointDetail() : $joint = ClientJointDetail::findOrFail($data['joint_id']);

        unset($data['joint_id']);
        unset($data['_token']);

        $joint->add($data);
        $joint->save();

        $app->joint = true;

        $app->save();

        $client = Client::findOrFail($data['client_id']);
        $client->joint = true;
        $client->save();

        $this->client = $client;

        return $joint;
    }

    public function saveClientJoint($data)
    {
        $client = Client::findOrFail($data['client_id']);

        is_null($data['joint_id'])
            ? $joint = new ClientJointDetail()
            : $joint = ClientJointDetail::findOrFail($data['joint_id']);

        unset($data['existing_client_id']);

        $joint->Add($data);
        $joint->save();

        $client->joint = true;
        $client->save();

        $this->client = $client;

        return $joint;
    }

    /**
     * Save the joint client from the real estate reservation form
     *
     * @param  array $data
     * @return mixed
     */
    protected function saveJointFromReservation(array $data)
    {
        $reservationForm = ReservationForm::findOrFail($data['reservation_form_id']);

        is_null($data['joint_id'])
            ? $joint = new ClientJointDetail() : $joint = ClientJointDetail::findOrFail($data['joint_id']);
        unset($data['joint_id']);

        $joint->add($data);
        $joint->save();
        $reservationForm->joint = true;
        $reservationForm->save();

        $client = $reservationForm->client;
        $client->joint = true;
        $client->save();

        $this->client = $client;

        return $joint;
    }

    protected function saveJointFromShareHolding(array $data)
    {
        $app = (new ShareHolder())->findOrFail($data['share_holding_id']);

        is_null($data['joint_id'])
            ? $joint = new ClientJointDetail() : $joint = (new ClientJointDetail())->findOrFail($data['joint_id']);

        unset($data['joint_id']);
        unset($data['_token']);
        $joint->add($data);
        $joint->save();
        $app->joint = true;
        $app->save();

        $client = (new Client())->findOrFail($data['client_id']);
        $client->joint = true;
        $client->save();

        $this->client = $client;

        return $joint;
    }

    public function saveJointFromUnitFundApplication(array $data)
    {
        $app = \App\Cytonn\Models\Unitization\UnitFundApplication::findOrFail($data['unit_fund_application_id']);

        is_null($data['joint_id'])
            ? $joint = new ClientJointDetail() : $joint = ClientJointDetail::findOrFail($data['joint_id']);

        unset($data['joint_id']);
        unset($data['_token']);
        $joint->add($data);
        $joint->save();

        $app->joint = true;
        $app->save();

        $client = Client::findOrFail($data['client_id']);
        $client->joint = true;
        $client->save();

        $this->client = $client;

        return $joint;
    }

    /**
     * Get an existing joint client from its ID in the data array, also cleans the array
     *
     * @param  array $data
     * @return mixed
     */
    protected function getExistingJoint(array $data)
    {
        $joint = ClientJointDetail::findOrFail($data['joint_id']);

        unset($data['joint_id']);
        unset($data['_token']);

        $this->client = $joint->client;

        return $joint;
    }

    /**
     * Save the contact information for the joint client
     *
     * @param ClientJointDetail $joint
     * @param array $data
     */
    protected function saveContact(ClientJointDetail $joint, array $data)
    {
        unset($data['holder_type']);
        unset($data['duplicate_reason']);

        if (isset($data['share_holding_id'])) {
            unset($data['share_holding_id']);
        }

        if (isset($data['unit_holding_id'])) {
            unset($data['unit_holding_id']);
        }

        if (isset($data['existing_client_id'])) {
            unset($data['existing_client_id']);
        }

        $data = array_add($data, 'entity_type_id', ContactEntity::where('name', 'individual')->first()->id);

        $contact_keys = ['client_id', 'email', 'dob', 'pin_no', 'id_or_passport', 'postal_code',
            'postal_address', 'telephone_office', 'telephone_home', 'telephone_cell', 'town',
            'residential_address', 'method_of_contact_id', 'application_id', 'reservation_form_id',
            'joint_id', 'unit_fund_application_id', 'holder_type', 'duplicate_reason', 'code'
        ];


        $joint->update($data);

        if (!is_null($joint->holder)) {
            $joint_client = $joint->holder;

            $contact = $joint_client->contact;

            $contact->add($this->cleanArray($data, $contact_keys));
        } else {
            $contact = new Contact();
            $contact->add($this->cleanArray($data, $contact_keys));

            $joint_client = new JointClientHolder();
            $joint_client->client_id = $this->client->id;
            $joint_client->contact_id = $contact->id;
            $joint_client->joint_detail_id = $joint->id;
            $joint_client->save();
        }
    }

    /**
     * Remove unwanted array keys to avoid errors when mass assigning
     *
     * @param  $array
     * @param  $keys
     * @return mixed
     */
    protected function cleanArray($array, $keys)
    {
        foreach ($keys as $key) {
            unset($array[$key]);
        }

        return $array;
    }
}
