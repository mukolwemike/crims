<?php
/**
 * Date: 22/08/2016
 * Time: 12:08 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment\InterestPayments;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;

/**
 * Class ScheduleGenerator
 *
 * @package Cytonn\Investment\InterestPayments
 */
class ScheduleGenerator
{
    const CUT_OFF = 25;

    /**
     * @param ClientInvestment $investment
     * @return bool
     */
    public function generate(ClientInvestment $investment)
    {
        if ($this->validate($investment)) {
            return $this->generateSchedules($investment);
        }

        return false;
    }

    /**
     * Check whether a schedule should actually be generated
     *
     * @param  ClientInvestment $investment
     * @return bool
     */
    private function validate(ClientInvestment $investment)
    {
        $invested_date = new Carbon($investment->invested_date);

        $maturity_date = new Carbon($investment->maturity_date);

        $tenorInMonths = $invested_date->copy()->diffInMonths($maturity_date);

        if ($investment->interest_payment_interval <= 0 || $investment->interest_payment_interval > $tenorInMonths) {
            return false;
        }

        return true;
    }

    /**
     * @param ClientInvestment $investment
     * @param Carbon $start_date
     * @param bool $endMonth
     * @return bool
     */
    private function generateSchedules(ClientInvestment $investment)
    {
        if (is_null($investment->interest_payment_date)) {
            $investment->interest_payment_date = 31;
        }

        $endMonth = $investment->interest_payment_date == 31 || $investment->interest_payment_date == 0;
        $amountPaid = 0;

        $date = $endMonth ? null : $investment->interest_payment_date;

        $startDate = ($investment->interest_payment_start_date)
            ? Carbon::parse($investment->interest_payment_start_date) : $investment->invested_date;

        $schedules = $this->schedule(
            $startDate,
            $investment->maturity_date,
            $investment->interest_payment_interval,
            $investment->invested_date,
            $endMonth,
            $date
        );

        foreach ($schedules as $key => $schedule) {
            $schedules[$key]['amount'] =
                $investment->repo->getTotalAvailableInterestAtNextDay($schedule['date']) - $amountPaid;
            $amountPaid += $schedules[$key]['amount'];
        }

        $investment->interestSchedules()->createMany($schedules);

        return true;
    }

    public function schedule(Carbon $start, Carbon $end, $interval, $investedDate, $endMonth = false, $date = null)
    {
        $schedules = [];
        $run_date = $this->firstPaymentDate($start);
        $run_date = $run_date->addMonthsNoOverflow($interval - 1)->copy();
        $counter = 1;

        if ($date) {
            $run_date = $run_date->startOfMonth()->addDays($date - 1);
        }

        if ($endMonth) {
            $run_date = $run_date->endOfMonth();
        }

        while ($end->gt($run_date)) {
            $schedules[] = [
                'date' => $run_date->copy(),
                'amount' => 0,
                'description' => 'Interest payment after ' . $counter . ' months'
            ];

            $run_date = $run_date->addMonthsNoOverflow($interval);
            if ($endMonth) {
                $run_date = $run_date->endOfMonth();
            }

            $counter++;
        }

        return $schedules;
    }

    private function firstPaymentDate($date)
    {
        return $date->day > static::CUT_OFF
            ? $date->copy()->addMonthNoOverflow()
            : $date->copy();
    }
}
