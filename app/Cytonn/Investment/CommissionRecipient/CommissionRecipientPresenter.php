<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\CommissionRecipient;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;

class CommissionRecipientPresenter extends Presenter
{
    /*
     * Get the latest commission recipient position
     */
    public function latestCommissionRecipient()
    {
        return $this->entity->commissionRecipientPositions()->orderBy('start_date', 'DESC')->first();
    }

    /*
     * Get the supervisor name for the commission recipient
     */
    public function getCurrentSupervisorName()
    {
        $recipientPosition = $this->latestCommissionRecipient();

        if ($recipientPosition && $recipientPosition->reportsTo) {
            return $recipientPosition->reportsTo->name;
        }

        return '';
    }

    /*
     * Get the commission recipient position rank
     */
    public function getCurrentRank()
    {
        $recipientPosition = $this->latestCommissionRecipient();

        if ($recipientPosition) {
            return $recipientPosition->rank->name;
        }

        return '';
    }

    /*
     * Get the commission recipient position type
     */
    public function getCurrentType()
    {
        $recipientPosition = $this->latestCommissionRecipient();

        if ($recipientPosition) {
            return $recipientPosition->type->name;
        }

        return '';
    }

    /*
     * Get the branch linked to the commission recipient
     */
    public function getBranch()
    {
        if ($this->departmentUnit) {
            return $this->departmentUnit->present()->getBranch;
        } else {
            if ($this->type) {
                return (in_array($this->type->id, [1, 3])) ? $this->type->name : "Others";
            }

            return 'Unknown';
        }
    }

    /*
     * Get the unit linked to the recipient
     */
    public function getDepartmentUnit()
    {
        if ($this->departmentUnit) {
            return $this->departmentUnit->name;
        } else {
            if ($this->type) {
                return (in_array($this->type->id, [1, 3])) ? $this->type->name : "Others";
            }

            return 'Unknown';
        }
    }

    /*
     * Check if an FA is active or not
     */
    public function getActive()
    {
        return ($this->active == 1) ? "Active" : "Inactive";
    }

    /*
     * Get the commission recipient position based on date
     */
    public function getRecipientPositionByDate(Carbon $date)
    {
        return $this->entity->commissionRecipientPositions()
            ->where('start_date', '<=', $date)
            ->orderBy('start_date', 'DESC')
            ->first();
    }

    public function activeStatus()
    {
        return ($this->active == 1) ? 'Active' : 'In active';
    }
}
