<?php

namespace Cytonn\Investment\Products;

use App\Cytonn\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductGridTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'description'=>$product->description,
            'currency' => @$product->currency->code,
            'fund_manager' => @$product->fundManager->name,
            'status' => $product->present()->getStatus
        ];
    }
}
