<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Investment\Products;

use Laracasts\Presenter\Presenter;

class ProductPresenter extends Presenter
{

    /*
     * Get all the rates linked to a given product
     */
    public function getRates()
    {
        $rateUpdate = $this->entity->interestRateUpdates()->orderBy('date', 'DESC')->first();

        if (is_null($rateUpdate)) {
            return $this->rates;
        }

        return $rateUpdate->rates;
    }


    public function getStatus()
    {
        return ($this->active == 1) ? 'Active' : 'Inactive';
    }

    public function isSeip()
    {
        return $this->type->slug == 'seip';
    }
}
