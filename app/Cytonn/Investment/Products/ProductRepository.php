<?php

namespace Cytonn\Investment\Products;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\InterestRateUpdate;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Rate;
use App\Cytonn\Models\Setting;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\Summary\Analytics;
use Carbon\Carbon;

class ProductRepository
{
    public $product;

    public function __construct(Product $product = null)
    {
        is_null($product) ? $this->product = new Product() : $this->product = $product;
    }

    public function investmentsActiveOnDate($date)
    {
        return ClientInvestment::where('product_id', $this->product->id)->activeOnDate($date)->get();
    }


    public function custodyFeesAtDate($date)
    {
        $date = new Carbon($date);
        $investments = ClientInvestment::where('product_id', $this->product->id)->activeOnDate($date)->get();

        $custody_fees_rate = Setting::where('key', 'custody_fee_rate')->first()->value;

        $fees = 0;

        $startOfMonth = (new Carbon($date))->startOfMonth();

        foreach ($investments as $investment) {
            if ($investment->withdrawn) {
                $withdraw_date = new Carbon($investment->withdrawal_date);

                if ((new Carbon($investment->withdrawal_date))->gt($startOfMonth->copy())) {
                    if ($withdraw_date->copy()->gt($date)) {
                        $tenor = $withdraw_date->copy()->diffInDays($startOfMonth->copy());
                    } else {
                        $tenor = $date->copy()->diffInDays($startOfMonth->copy());
                    }
                } else {
                    $tenor = 0;
                }
            } else {
                $tenor = $date->copy()->diffInDays($startOfMonth->copy());
            }

            $fees += $custody_fees_rate * $investment->amount * $tenor / (365*100);
        }

        return $fees;
    }

    public function totalCostValueAtDate($date)
    {
        return ClientInvestment::where('product_id', $this->product->id)->activeOnDate($date)->sum('amount');
    }

    public function totalMarketValueAtDate($date)
    {
        $investments = ClientInvestment::where('product_id', $this->product->id)->activeOnDate($date)->get();

        return $investments->sum(
            function ($investment) use ($date) {
                return $investment->repo->getTotalValueOfInvestmentAtDate($date);
            }
        );
    }

    public function withholdingTaxAtDate($date)
    {
        $investments = ClientInvestment::where('product_id', $this->product->id)->activeOnDate($date)->get();

        return $investments->sum(
            function ($investment) use ($date) {
                return $investment->repo->getWithholdingTaxAtDate($date);
            }
        );
    }

    public function countActiveClients()
    {
        return Client::whereHas(
            'investments',
            function ($investment) {
                $investment->where('product_id', $this->product->id)->active();
            }
        )->count();
    }

    public function getActiveClients()
    {
        $ids = ClientInvestment::where('withdrawn', '!=', 1)
            ->where('product_id', $this->product->id)->get()->lists('client_id');

        return Client::whereIn('id', $ids)->get();
    }

    public function getAllClients()
    {
        $ids = ClientInvestment::where('product_id', $this->product->id)->get()->lists('client_id');

        return Client::whereIn('id', $ids)->get();
    }

    public function countClients()
    {
        return Client::whereHas(
            'investments',
            function ($investment) {
                $investment->where('product_id', $this->product->id);
            }
        )->count();
    }

    public function weightedRateAtDate($date)
    {
        return (new Analytics(FundManager::all(), $date))
            ->setProduct($this->product)
            ->setBaseCurrency($this->product->currency)
            ->weightedRate();
    }

    public function weightedTenorAtDate($date)
    {
        return (new Analytics(FundManager::all(), $date))
            ->setProduct($this->product)
            ->setBaseCurrency($this->product->currency)
            ->weightedTenor();
    }

    public function persistence($startDate, $endDate)
    {
        return (new Analytics(FundManager::all(), $endDate))
            ->setProduct($this->product)
            ->setBaseCurrency($this->product->currency)
            ->persistence($startDate, $endDate);
    }

    public function getProductById($id)
    {
        return Product::findOrFail($id);
    }

    public function save($input, $id)
    {
        if ($id) {
            $product = $this->getProductById($id);

            $product->update($input);

            return $product;
        }

        return Product::create($input);
    }

    public function interestRate($tenor, $amount = 0, Carbon $date = null)
    {
        $date = Carbon::parse($date);

        $amount = round($amount, 0);

        $rateUpdate = $this->product->interestRateUpdates()
            ->where('date', '<=', $date)
            ->orderBy('date', 'DESC')
            ->first();

        if (is_null($rateUpdate)) {
            throw new CRIMSGeneralException(
                "There are no rates for " . $this->product->name
            );
        }

        $rate = $rateUpdate->rates()
            ->where('tenor', '<=', $tenor)
            ->where('min_amount', '<=', $amount)
            ->orderBy('tenor', 'DESC')
            ->orderBy('min_amount', 'DESC')
            ->first();

        if ($rate) {
            return $rate->rate;
        }

        $rate = $rateUpdate->rates()
            ->where('tenor', '>=', $tenor)
            ->where('min_amount', '<=', $amount)
            ->orderBy('tenor', 'ASC')
            ->orderBy('min_amount', 'ASC')
            ->first();

        if ($rate) {
            return $rate->rate;
        }

        throw new CRIMSGeneralException(
            "There are no rates for " . $this->product->name . " $tenor months"
        );
    }

    public function productRates($amount = 0, $date = null)
    {
        $date = Carbon::parse($date);

        $amount = round($amount, 0);

        $rateUpdate = $this->product->interestRateUpdates()
            ->where('date', '<=', $date)
            ->orderBy('date', 'DESC')
            ->first();

        if (is_null($rateUpdate)) {
            return collect([]);
        }

        return $rateUpdate->rates()
            ->hidden(false)
            ->where('min_amount', '<=', $amount)
            ->orderBy('min_amount', 'DESC')
            ->get()
            ->groupBy('min_amount')
            ->first();
    }

    public function globalRate($tenor = null, $date = null)
    {
        $date = Carbon::parse($date);

        $rateUpdate = $this->product->interestRateUpdates()
            ->where('date', '<=', $date)
            ->orderBy('date', 'DESC')
            ->first();

        if (is_null($rateUpdate)) {
            return collect([]);
        }

        return $rateUpdate->rates()
            ->get()
            ->first();
    }

    public function collectionAccount($type = 'bank')
    {
        $operator = $type == 'mpesa' ? '=' : '!=';

        return $this->product->operatingAccounts()
            ->where('bank_swift', $operator, 'MPESA')
            ->wherePivot('role', 'collection')
            ->orderBy('preferred_account', 'DESC')
            ->first();
    }
}
