<?php
/**
 * Date: 09/05/2016
 * Time: 9:02 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims-client
 * Cytonn Technologies
 */

namespace App\Cytonn\Investments\Products;

use App\Cytonn\BaseModel;

class Rate extends BaseModel
{
    protected $table = 'rates';
}
