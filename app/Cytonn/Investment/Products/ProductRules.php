<?php

namespace Cytonn\Investment\Products;

use Cytonn\Rules\Rules;

trait ProductRules
{
    use Rules;

    public function createProduct($request)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'currency_id' => 'required',
            'custodial_account_id' => 'required',
            'fund_manager_id' => 'required',
            'shortname' => 'required',
            'longname' => 'required',
            'type_id' => 'required',
            'active' => 'required'
        ];

        return $this->verdict($request, $rules);
    }

    public function uploadProductDocument($request)
    {
        $rules = [
            'name' => 'required',
            'file' => 'required|mimes:pdf',
        ];

        return $this->verdict($request, $rules);
    }
}
