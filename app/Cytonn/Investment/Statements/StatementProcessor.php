<?php

namespace Cytonn\Investment\Statements;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use Carbon\Carbon;

class StatementProcessor
{

    /**
     * @var
     */
    private $statementDate;

    public function process(
        Client $client,
        Product $product,
        Carbon $statementDate,
        Carbon $from = null,
        $expanded = false
    ) {
        $this->statementDate = $statementDate->copy();

        $eligibleInvestments = ClientInvestment::where('client_id', $client->id)
            ->where('product_id', $product->id)
            ->statement($statementDate, $from)
            ->get();

        $inv = $eligibleInvestments->map(function (ClientInvestment $investment) use ($statementDate, $expanded) {
            $prepared = $investment->calculate($statementDate, true, null, $expanded)
                    ->getPrepared();

            return $prepared;
        });

        $allowed_ids = [];

        $total = $inv->each(function ($prepared) use (&$allowed_ids) {
            $withdrawals = $prepared->investment
                ->withdrawals()
                ->where('date', '>=', $this->statementDate)
                ->get();

            foreach ($withdrawals as $withdrawal) {
                array_push($allowed_ids, $withdrawal->reinvested_to);
            }
        })->filter(function ($prepared) use ($allowed_ids) {
            $investment = $prepared->investment;

            if (in_array($investment->id, $allowed_ids)) {
                return true;
            }

            return $investment->invested_date->lte($this->statementDate);
        });

        return [ 'investments'=>$inv, 'totals'=>$this->calculateTotals($total)];
    }

    private function calculateTotals($investments)
    {
        $total = function ($column) use ($investments) {
            return $investments->sum(function ($inv) use ($column) {
                return $inv->total->$column;
            });
        };

        return [
            'total_interest'=> $total('net_interest_cumulative'),
            'total_principal'=> $total('principal'),
            'total_client_value'=> $total('total')
        ];
    }
}
