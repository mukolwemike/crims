<?php
/**
 * Date: 25/07/2016
 * Time: 11:10 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\BankInstruction;
use Illuminate\Support\Collection;

/**
 * Class BankInstructionRepository
 *
 * @package Cytonn\Investment
 */
class BankInstructionRepository
{
    /**
     * @var BankInstruction
     */
    protected $instruction;

    /**
     * BankInstructionRepository constructor.
     *
     * @param BankInstruction | null $instruction
     */
    public function __construct(BankInstruction $instruction = null)
    {
        ($instruction === null) ? $this->instruction = new BankInstruction() : $this->instruction = $instruction;
    }

    /**
     * Combine instructions into one, requires a collection of instructions
     *
     * @param  Collection $instructions
     * @return BankInstruction $instruction
     */
    public function combine(Collection $instructions)
    {
        return \DB::transaction(
            function () use ($instructions) {
                $instruction = new BankInstruction();
                $instruction->combined = true;
                $instruction->amount = $instructions->sum('amount');
                $instruction->date = $instructions->first()->date;
                $instruction->investment_id = $instructions->first()->investment_id;
                $instruction->description = 'Combined instruction';
                $instruction->save();

                $instructions->each(
                    function ($i) use ($instruction) {
                        $i->combined_to = $instruction->id;
                        $i->save();
                    }
                );

                return $instruction;
            }
        );
    }
}
