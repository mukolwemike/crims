<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace Cytonn\Investment\CommissionRecipientPositions;

use Laracasts\Presenter\Presenter;

class CommissionRecipientPositionPresenter extends Presenter
{
    /*
     * Get supervisor
     */
    public function getSupervisor()
    {
        return $this->reportsTo ? $this->reportsTo->name : '';
    }
}
