<?php
    /**
     * Date: 03/11/2015
     * Time: 12:05 PM
     *
     * @author Mwaruwa Chaka <mchaka@cytonn.com>
     * Project: crm
     * Cytonn Technology
     */

    namespace Cytonn\Investment;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Commission;
use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Base;
use Cytonn\Investment\Commission\Generator;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Collection;

/**
     * Class CommissionRepository
     *
     * @package Cytonn\Investment
     */
class CommissionRepository
{

    /**
     * @return Generator
     */
    public function generator()
    {
        return new Generator();
    }

    /**
     * Save commission for new investment, topup or rollover
     *
     * @param  $investment
     * @param  $data
     * @return Commission
     */
    public function saveCommission($investment, $data)
    {
        //commission
        $commission = new Commission();
        $commission->rate = $data['commission_rate'];
        $commission->recipient_id = $data['commission_recepient'];
        $commission->investment_id = $investment->id;
        if (array_key_exists('commission_start_date', $data)) {
            $commission->start_date = $data['commission_start_date'];
        }
        $commission->save();

        return $commission;
    }


    /**
     * Get the currencies where an fa has commissions
     *
     * @param  CommissionRecepient $recipient
     * @return mixed
     */
    public function getCurrenciesForRecipient(CommissionRecepient $recipient)
    {
        return Currency::remember(5)->whereHas('products', function ($product) use ($recipient) {
            $product->whereHas('investments', function ($inv) use ($recipient) {
                $inv->whereHas('commission', function ($commission) use ($recipient) {
                    $commission->where('recipient_id', $recipient->id);
                });
            });
        })->get();
    }
}
