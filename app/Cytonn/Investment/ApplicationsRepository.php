<?php
/**
 * Date: 9/1/15
 * Time: 10:02 PM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\Behaviours\LocksTransactions;
use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBankAccount;
use App\Cytonn\Models\ClientComplianceChecklist;
use App\Cytonn\Models\ClientContactPerson;
use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientFilledInvestmentApplicationContactPersons;
use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientInvestmentApplication;
use App\Cytonn\Models\ClientInvestmentApplicationKycCheck;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\ContactEntity;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\Validation\ValidatorTrait;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Laracasts\Commander\CommanderTrait;
use Throwable;

/**
 * Class ApplicationsRepository
 *
 * @package Cytonn\Investment
 */
class ApplicationsRepository
{
    use ValidatorTrait, CommanderTrait, LocksTransactions;

    /**
     * @var ClientInvestmentApplication
     */
    public $application;

    /**
     * ApplicationsRepository constructor.
     *
     * @param $application
     */
    public function __construct(ClientInvestmentApplication $application = null)
    {
        is_null($application)
            ? $this->application = new ClientInvestmentApplication() : $this->application = $application;
    }


    /**
     * Save the Contact details from the application forms
     *
     * @param  $type
     * @param  array $data
     * @return $this|Model|mixed
     */
    public function saveContactFromApplicationForm($type, array $data)
    {
        if (isset($data['title'])) {
            $data['title_id'] = $data['title'];
        }

        if (isset($data['registered_name'])) {
            $data['corporate_registered_name'] = $data['registered_name'];
        }

        if (isset($data['trade_name'])) {
            $data['corporate_trade_name'] = $data['trade_name'];
        }

        if (!isset($data['new_client'])) {
            $data['new_client'] = ClientFilledInvestmentApplication::NEW_CLIENT;
        }

        if (is_null($data['new_client']) || empty($data['new_client'])) {
            $data['new_client'] = ClientFilledInvestmentApplication::NEW_CLIENT;
        }

        $contactData = $this->filter($data, Contact::getTableColumnsAsArray());

        if (!isset($data['phone'])) {
            $contactData['phone'] = trim($data['telephone_home']);
        }

        $contactData['entity_type_id'] = ContactEntity::where('name', $type)->first()->id;

        if (($data['new_client'] === ClientFilledInvestmentApplication::EXISTING_CLIENT)) {
            $form = ClientFilledInvestmentApplication::findOrFail($data['form_id']);
            $contact = $form->client->contact;
            $contact->fill($contactData);
        } else {
            $contact = Contact::create($contactData);
        }

        $contact->save();

        return $contact;
    }


    /**
     * Save the Client details from the application forms
     *
     * @param  $type
     * @param  Contact $contact
     * @param  array $data
     * @return $this|Collection|Model
     * @throws Exception
     */
    public function saveClientFromApplicationForm($type, Contact $contact, array $data)
    {
        $client_data = $this->filter($data, Client::getTableColumnsAsArray());

        if (!isset($data['new_client'])) {
            $data['new_client'] = 1;
        }

        if (is_null($data['new_client']) || empty($data['new_client'])) {
            $data['new_client'] = 1;
        }

        if ($data['new_client'] === 2) {
            $client = ClientFilledInvestmentApplication::findOrFail($data['form_id'])->client;

            $client->fill($client_data);
        } else {
            $client_data['contact_id'] = $contact->id;
            $client_data['client_type_id'] = ClientType::where('name', $type)->first()->id;

            $client = Client::create($client_data);

            $this->saveBankDetails($client, $data);
        }

        if (isset($data['product_id'])) {
            $client->fund_manager_id = Product::findOrFail($data['product_id'])->fundManager->id;
            $client->save();
        }
        if (isset($data['unit_fund_id'])) {
            $client->fund_manager_id = UnitFund::findOrFail($data['unit_fund_id'])->manager->id;
            $client->save();
        }

        if (is_null($client->fresh()->client_code)) {
            $this->setClientCode($client);
        }

        return $client;
    }

    public function saveBankDetails($client, $data)
    {
        if (!isset($data['branch_id'])) {
            $data['branch_id'] = (isset($data['client_bank_branch_id'])) ? $data['client_bank_branch_id'] : null;
        }

        if (isset($data["investor_bank"]) && (int)$data["investor_bank"] > 0) {
            $data['branch_id'] = $data["investor_bank"];
        }
        if (isset($data["investor_bank_branch"]) && (int)$data["investor_bank_branch"] > 0) {
            $data['branch_id'] = $data["investor_bank_branch"];
        }

        // Save bank branch
        if (isset($data['branch_id'])) {
            $bankAccount = new ClientBankAccount([
                'branch_id' => $data['branch_id'],
                'account_name' => $data['investor_account_name'],
                'account_number' => $data['investor_account_number'],
                'default' => 1
            ]);

            $client->bankAccounts()->save($bankAccount);
        }
    }

    /**
     * @param Client $client
     * @param ClientInvestmentApplication $application
     * @return ClientContactPerson
     */
    public function saveContactPersonFromApplication(Client $client, ClientInvestmentApplication $application)
    {
        $contactPerson = new ClientContactPerson();
        $contactPerson->name = $application->form->kin_name;
        $contactPerson->email = $application->form->kin_email;
        $contactPerson->phone = $application->form->kin_phone;
        $contactPerson->address = $application->form->kin_postal;
        $contactPerson->client_id = $client->id;
        $contactPerson->save();

        return $contactPerson;
    }

    /**
     * Check whether an application has been invested or not
     *
     * @return string
     */
    public function status()
    {
        if (ClientInvestment::where('application_id', $this->application->id)->exists()) {
            return 'invested';
        }

        return 'not invested';
    }

    /**
     * Check compliance for an application
     *
     * @return object
     */
    public function compliance()
    {
        $compliance = ClientInvestmentApplicationKycCheck::where('app_id', $this->application->id)
            ->latest()->first();

        if (!is_null($compliance)) {
            $required =
                ClientComplianceChecklist::where('client_type_id', $this->application->client->client_type_id)
                ->where('fund_manager_id', $this->application->client->fund_manager_id)->lists('id');

            $checked = $compliance->checked;

            if (!is_array($checked) || !is_array($required)) {
                return (object)['approved' => false, 'comment' => $compliance->comment];
            }

            if (!array_diff($checked, $required) && !array_diff($required, $checked)) {
                return (object)['approved' => true, 'comment' => $compliance->comment];
            } else {
                return (object)['approved' => false, 'comment' => $compliance->comment];
            }
        } else {
            return (object)['approved' => false, 'comment' => 'Not checked'];
        }
    }

    /**
     * Delete an application
     *
     * @return bool
     * @throws Throwable
     */
    public function removeApplication()
    {
        if ($this->status() == 'invested') {
            return false;
        }

        DB::transaction(
            function () {
                //delete application
                $this->application->delete();

                //delete client
                $client = $this->application->client;

                if (!Client::where('id', $client->id)->has('investments')->exists()) {
                    $client->delete();
                }

                return true;
            }
        );

        return true;
    }

    /**
     * @param $type
     * @param $application
     * @return mixed
     * @throws Exception
     */
    public function saveUnitFundApplication($type, $application)
    {
        $type = ClientType::findOrFail($type)->name;

        $app = $application->toArray();

        $contact = $this->saveContactFromApplicationForm($type, $app);

        $client = $this->saveClientFromApplicationForm($type, $contact, $app);

        $application->client_id = $client->id;

        $application->save();

        $holders = (isset($application->holders)) ? json_decode($application->holders) : null;

        if ($holders) {
            $this->addJointHoldersFromUnitFundApplication($holders);
        }

        return $application;
    }

    /**
     * @param $holders
     */
    public function addJointHoldersFromUnitFundApplication($holders)
    {
        $clientId = $this->application->client_id;

        collect($holders)->each(
            function ($holder) use ($clientId) {
                $holder = array($holder);
                $holder = array_add($holder, 'joint_id', null);
                $holder = array_add($holder, 'client_id', $clientId);

                $holder['unit_fund_application_id'] = $this->application->id;

                return $this->execute(JointInvestorAddCommand::class, ['data' => $holder]);
            }
        );
    }

    public function addJointHolders($holders)
    {
        $clientId = $this->application->client_id;

        collect($holders)->each(
            function ($holder) use ($clientId) {

                $holder = $holder->toArray();
                $holder = array_add($holder, 'joint_id', null);
                $holder = array_add($holder, 'client_id', $clientId);

                $holder['application_id'] = $this->application->id;

                return $this->execute(JointInvestorAddCommand::class, ['data' => $holder]);
            }
        );
    }

    /**
     * @param Client $client
     * @param ClientInvestmentApplication $application
     * @return ClientContactPerson
     */
    public function saveContactPersonFromFilledContactPerson(Client $client, ClientInvestmentApplication $application)
    {
        $app_id = $application->form_id;

        $contactPersons =
            ClientFilledInvestmentApplicationContactPersons::where('application_id', $app_id)->get();

        $contactPerson =  $contactPersons->map(function ($contactPerson) use ($client) {
            $person = new ClientContactPerson();
            $person->name = $contactPerson->name;
            $person->email = $contactPerson->email;
            $person->phone = $contactPerson->phone;
            $person->address = $contactPerson->address;
            $person->client_id = $client->id;
            $person->save();

            return $person;
        });

        return $contactPerson;
    }

    /**
     * @param $client
     * @throws Exception
     */
    public function setClientCode(Client $client)
    {
        $this->executeWithinAtomicLock(function () use ($client) {

            $save_code = function () use ($client) {
                $client->update(['client_code' => (new ClientRepository())->suggestClientCode()]);
            };

            //try twice
            try {
                $save_code();
            } catch (QueryException $e) {
                if (str_contains($e->getMessage(), 'client_code_UNIQUE')) {
                    $save_code();
                    return;
                }

                throw $e;
            }
        }, 'set_client_code', 30);
    }
}
