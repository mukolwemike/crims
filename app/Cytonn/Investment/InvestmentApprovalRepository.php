<?php
/**
 * Date: 25/10/2015
 * Time: 10:26 AM
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crm
 * Cytonn Technology
 */

namespace Cytonn\Investment;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientBusinessConfirmation;
use App\Cytonn\Models\ClientTransactionApproval;
use Cytonn\Authorization\Authorizer;
use Cytonn\Clients\ClientRepository;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Approvals\Events\Actions\InvestmentActionApproved;
use Cytonn\Investment\Commands\ReverseWithdrawalCommand;
use Cytonn\Investment\Commands\RollbackInvestmentCommand;
use Cytonn\Investment\Events\ClientPaymentIsDue;
use Cytonn\Investment\Events\InvestmentHasBeenEdited;
use Cytonn\Investment\Events\InvestmentHasBeenTransferred;
use Cytonn\Mailers\BusinessConfirmationMailer;
use Cytonn\Reporting\BusinessConfirmationGenerator;
use Cytonn\Users\UserClientRepository;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Laracasts\Commander\CommanderTrait;
use Laracasts\Commander\Events\DispatchableTrait;
use Laracasts\Commander\Events\EventDispatcher;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Cytonn\Investment\ContactPersonAddCommand;
use Cytonn\Clients\ClientBankDetailsAddCommand;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Support\Facades\Auth;
use Laracasts\Commander\Events\EventGenerator;

use Cytonn\Investment\Commands\CommissionPaymentCommand;

/**
 * Class InvestmentApprovalRepository
 *
 * @package Cytonn\Investment
 */
class InvestmentApprovalRepository
{
    use DispatchableTrait, CommanderTrait, EventGenerator;

    public $runningScheduled = false;

    /**
     * InvestmentApprovalRepository constructor.
     *
     * @param null $form
     */
    public function __construct($form = null)
    {
        $this->authorizer = new Authorizer();
        $this->form = $form; //inject the form
    }


    /**
     * NOT IN USE: Business confirmations are no longer confirmed, instead they're sent directly
     * Send business confirmation after the have been used
     *
     * @param      $id
     * @return     mixed
     * @throws     \Cytonn\Exceptions\AuthorizationDeniedException
     * @deprecated
     */
    public function postConfirmation($id)
    {
        //TODO: Remove, not in use

        $approval = ClientTransactionApproval::findOrFail($id);

        $client  = Client::findOrFail($approval->client_id);

        (new BusinessConfirmationMailer())->sendBusinessConfirmationToClient($client, $approval->payload, 'MBC');

        $data = ['investment_id'=>$approval->payload['investment_id'], 'payload'=>$approval->payload];

        ClientBusinessConfirmation::add($data);

        \Flash::success('The Business Confirmation has been sent');

        $approval->approve();

        return \redirect('/dashboard/investments');
    }
}
