<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace Cytonn\Oauth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;

trait ApiCredentialManager
{
    /*
     * Get the initial authorizations
     */
    public function seekCrmAuthorization()
    {
        return $this->getAccessToken(
            env('CRM_AUTH_URL'),
            env('CRM_CLIENT_ID'),
            env('CRM_CLIENT_SECRET'),
            'crm_access_token'
        );
    }

    /**
     * @return mixed|null
     */
    public function seekHrAuthorization()
    {
        return $this->getAccessToken(
            env('HR_AUTH_URL'),
            env('HR_CLIENT_ID'),
            env('HR_CLIENT_SECRET'),
            'hr_access_token'
        );
    }

    /*
     * Get the access token from the corresponding system
     */
    public function getAccessToken($url, $clientId, $clientSecret, $accessTokenKeyName)
    {
        if (Cache::has($accessTokenKeyName)) {
            $accessToken = Cache::get($accessTokenKeyName);

            return $accessToken;
        }

        $client = new Client();

        $accessToken = null;

        try {
            $response = $client->request(
                'POST',
                $url,
                [
                    'form_params' => [
                        'grant_type' => 'client_credentials',
                        'client_id' => $clientId,
                        'client_secret' => $clientSecret
                    ]
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
        } catch (\Exception $e) {
            $response = null;
        }

        if ($response && $response->getStatusCode() == 200) {
            $body = $response->getBody();

            $accessTokenObject = (object)\GuzzleHttp\json_decode($body);

            $accessToken = $accessTokenObject->access_token;

            Cache::put($accessTokenKeyName, $accessToken, $accessTokenObject->expires_in / 60);
        }

        return $accessToken;
    }
}
