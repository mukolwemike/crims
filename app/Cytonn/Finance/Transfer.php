<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 1/9/19
 * Time: 11:21 AM
 */

namespace App\Cytonn\Finance;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\CustodialTransfer;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;
use Cytonn\Mailers\ExcelMailer;

class Transfer
{
    use ExcelMailer;

    public function export(Carbon $from, Carbon $to, User $user = null, $type)
    {
        $filename = 'Fund ' . ucfirst($type). ' Transfers Export';

        $currencies = Currency::all();

        \Excel::create($filename, function ($excel) use ($currencies, $from, $to, $type) {

            foreach ($currencies as $currency) {
                $name = $currency->code . ' - '. ucwords($type);

                $excel->sheet($name, function ($sheet) use ($currency, $from, $to, $type) {

                    $type = str_replace(
                        ' ',
                        '',
                        (ucwords(str_replace('-', ' ', $type)))
                    );

                    $action = 'getFor'.$type;

                    $data = $this->$action($from, $to, $currency);

                    $sheet->loadView('reports.finance.transfers', [
                        'data' => $data,
                        'type' => $type,
                        'currency' => $currency
                    ]);
                });
            }
        })->store('xlsx');

        $email = $user ? $user->email : config('system.administrators');

        $this->sendExcel(
            ucwords($filename),
            'Please find attached fund '. $type .' report for SAP',
            $filename,
            [$email]
        );
    }

    private function getForInterClient(Carbon $start, Carbon $end, Currency $currency)
    {
        return ClientPayment::ofType('TI')
            ->whereHas(
                'custodialTransaction',
                function ($transaction) use ($currency) {
                    $transaction->whereHas(
                        'custodialAccount',
                        function ($custodial) use ($currency) {
                            $custodial
                                ->where('currency_id', $currency->id);
                        }
                    );
                }
            )
            ->whereBetween('date', [$start, $end])
            ->whereNotNull('parent_id')
            ->get()
            ->map(function (ClientPayment $payment) {

                $currency = $payment->custodialTransaction->custodialAccount->currency;

                $baseCurrency = getBaseCurrency($payment->date);

                $payment->amount_in_ksh = convert($payment->amount, $currency);

                $payment->conversion_rate = (new Convert($baseCurrency))
                    ->enableCaching(60)->read($currency, $payment->date);

                return $payment;
            });
    }

    private function getForInterCustodial(Carbon $start, Carbon $end, Currency $currency)
    {
        return CustodialTransfer::whereBetween('updated_at', [$start, $end])
            ->whereHas('sender', function ($sender) use ($currency) {
                $sender->whereHas(
                    'custodialAccount',
                    function ($custodial) use ($currency) {
                        $custodial
                            ->where('currency_id', $currency->id);
                    }
                );
            })
            ->get()
            ->map(function ($transfer) {

                $sender_currency = $transfer->sender->custodialAccount->currency;

                $receiver_currency = $transfer->receiver->custodialAccount->currency;

                $date = Carbon::parse($transfer->sender->date);

                $baseCurrency = getBaseCurrency($date);

                $transfer->sender_amount_in_ksh = convert($transfer->sender->amount, $sender_currency);

                $transfer->receiver_amount_in_ksh = convert($transfer->receiver->amount, $receiver_currency);

                $transfer->sender_conversion_rate = (new Convert($baseCurrency))
                    ->enableCaching(60)->read($sender_currency, $date);

                $transfer->receiver_conversion_rate = (new Convert($baseCurrency))
                    ->enableCaching(60)->read($receiver_currency, $date);

                return $transfer;
            });
    }
}
