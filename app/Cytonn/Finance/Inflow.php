<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 1/9/19
 * Time: 11:21 AM
 */

namespace App\Cytonn\Finance;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\User;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;
use Cytonn\Mailers\ExcelMailer;

class Inflow
{
    use ExcelMailer;

    public function export(Carbon $from, Carbon $to, User $user = null, $inflow = true)
    {
        $action  = $inflow ? 'inflows' : 'outflows';

        $filename = 'Fund ' . ucfirst($action). ' Export';

        $currencies = Currency::all();

        \Excel::create($filename, function ($excel) use ($currencies, $from, $to, $inflow, $action) {

            foreach ($currencies as $currency) {
                $name = $currency->code . ' - '. ucwords($action);

                $excel->sheet($name, function ($sheet) use ($currency, $from, $to, $inflow) {

                    $currency->p_transactions = $this->getForCurrency($from, $to, $currency, $inflow);

                    $data = $currency;

                    $view = $inflow ? 'reports.finance.inflows' : 'reports.finance.outflows';

                    $sheet->loadView($view, [
                        'data' => $data,
                        'currency' => $currency
                    ]);
                });
            }
        })->store('xlsx');

        $email = $user ? $user->email : config('system.administrators');

        $this->sendExcel(
            ucwords($filename),
            'Please find attached fund '. $action .' report for SAP',
            $filename,
            [$email]
        );
    }

    private function getForCurrency(Carbon $start, Carbon $end, Currency $currency, $inflow = true)
    {
        $payment_type = $inflow ? 'FI' : 'FO';

        return ClientPayment::ofType($payment_type)
            ->whereHas(
                'custodialTransaction',
                function ($transaction) use ($currency) {
                    $transaction->whereHas(
                        'custodialAccount',
                        function ($custodial) use ($currency) {
                            $custodial
                                ->where('currency_id', $currency->id);
                        }
                    );
                }
            )
            ->whereBetween('date', [$start, $end])
            ->get()
            ->map(function (ClientPayment $payment) {

                $currency = $payment->custodialTransaction->custodialAccount->currency;

                $baseCurrency = getBaseCurrency($payment->date);

                $payment->amount_in_ksh = convert($payment->amount, $currency);

                $payment->conversion_rate = (new Convert($baseCurrency))->enableCaching(60)
                    ->read($currency, $payment->date);

                return $payment;
            });
    }
}
