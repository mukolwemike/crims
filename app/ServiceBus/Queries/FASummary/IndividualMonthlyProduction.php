<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class IndividualMonthlyProduction
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    /*
     * Handle the fa evaluation summary
     */
    public function handle($data)
    {
        $userData = $data[0];

        $email = $data[1];

        return $this->generateReport($userData, $email);
    }

    /*
     * Generate the report
     */
    public function generateReport($data, $email)
    {
        $data = json_decode($data);

        $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($email);

        $evaluationData = array();

        foreach ($data as $user) {
            $startDate = Carbon::parse($user->start_date)->startOfDay();

            $endDate = Carbon::parse($user->end_date)->endOfDay();

            if ($rec_id) {
                $inflows = $this->getCumulativeInflows($startDate, $endDate, $rec_id);

                $outFlows = $this->getWithdrawals($startDate, $endDate, $rec_id);

                $reSales = $this->realEstateSales($startDate, $endDate, $rec_id);

                $retention = $inflows + $reSales - $outFlows;

                $evaluationData[] = [
                    'id' => $user->id,
                    'month' => $user->month,
                    'month_date' => $user->month_date,
                    'inflows' => $inflows,
                    'outflows' => $outFlows,
                    'reSales' => $reSales,
                    'targets' => $user->targets,
                    'percentage' => percentage($retention, $user->targets, 1),
                ];
            } else {
                $evaluationData[] = [
                    'id' => $user->id,
                    'month' => $user->month,
                    'month_date' => $user->month_date,
                    'inflows' => 0,
                    'outflows' => 0,
                    'reSales' => 0,
                    'targets' => $user->targets,
                    'percentage' => 0,
                ];
            }
        }

        return $evaluationData;
    }
}
