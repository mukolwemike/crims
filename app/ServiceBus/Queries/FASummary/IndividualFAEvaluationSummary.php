<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use Carbon\Carbon;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\FaEvaluationDataTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class IndividualFAEvaluationSummary
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait,
        FaEvaluationDataTrait;

    /*
     * Handle the incoming request
     */
    public function handle($data)
    {
        $users = $data[0];

        $end = $data[1];

        return $this->getUserEvaluationData($users, $end);
    }

    /*
     * Get a particular user evaluation data
     */
    public function getUserEvaluationData($data, $end)
    {
        $endDate = Carbon::parse($end)->endOfDay();

        $data = json_decode($data);

        $evaluationData = array();

        foreach ($data->data as $user) {
            $evaluationData[] = $this->userEvaluationData($user, $endDate);
        }

        $data->data = $evaluationData;

        return $data;
    }
}
