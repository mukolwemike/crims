<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Reporting\SummaryReports\Inflows;

class IndividualMonthProduction
{
    /*
     * Get the FA generator trait
     */
    use Inflows,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    /*
     * Handle the fa evaluation summary
     */
    public function handle($data)
    {
        $userData = $data[0];

        return $this->generateReport($userData);
    }

    /*
     * Generate the report
     */
    public function generateReport($data)
    {
        $user = json_decode($data);

        $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

        $evaluationData = array();

        $startDate = Carbon::parse($user->start_date)->startOfDay();

        $endDate = Carbon::parse($user->end_date)->endOfDay();

        if ($rec_id) {
            $inflows = $this->getInflows($startDate, $endDate, $rec_id);

            $outFlows = $this->getWithdrawals($startDate, $endDate, $rec_id);

            $reSales = $this->realEstateSales($startDate, $endDate, $rec_id);

            $reCashFlows = $this->realEstateActualPayments($startDate, $endDate, $rec_id);

            $retention = $inflows + $reSales - $outFlows;

            $evaluationData = [
                'inflows' => $inflows,
                'outflows' => $outFlows,
                'netflows' => $inflows - $outFlows,
                'reSales' => $reSales,
                'reCashFlows' => $reCashFlows,
                'retention' => $retention,
                'targets' => $user->targets,
                'percentage' => percentage($retention, $user->targets, 1),
            ];
        } else {
            $evaluationData = [
                'inflows' => 0,
                'outflows' => 0,
                'netflows' => 0,
                'reSales' => 0,
                'reCashFlows' => 0,
                'retention' => 0,
                'targets' => $user->targets,
                'percentage' => 0,
            ];
        }

        $user->production = $evaluationData;

        return json_encode($user);
    }
}
