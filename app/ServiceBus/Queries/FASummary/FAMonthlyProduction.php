<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 8/1/17
 * Time: 8:43 AM
 */

namespace ServiceBus\Queries\FASummary;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class FAMonthlyProduction
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    /*
     * Handle the incoming request
     */
    public function handle($data)
    {
        $users = $data[0];

        $startYear = $data[2];

        $endYear = $data[1];

        return $this->generateReport($users, $startYear, $endYear);
    }

    /*
     * Generate the monthly production report
     */
    public function generateReport($data, $startDate, $endDate)
    {
        $data = json_decode($data);

        $users = $data->data;

        $monthlyProductionData = array();

        $consolidatedData = array();

        $startDate = Carbon::parse($startDate)->startOfDay();

        $endDate = Carbon::parse($endDate)->endOfDay();

        $months = getDateRange($startDate, $endDate);

        $faData = $this->getFaData($users, $months, $monthlyProductionData, $consolidatedData);
        $monthlyProductionData = $faData['users'];
        $consolidatedData = $faData['consolidated'];

        $teamUsers = $this->getProductionUsers($startDate, $endDate, 1);
        $teamData = $this->getRecipientProductionData($teamUsers, $months, $consolidatedData, 'Team');
        $monthlyProductionData['Team'] = $teamData['users']['Team'];
        $consolidatedData = $teamData['consolidated'];

        $ifaUsers = $this->getProductionUsers($startDate, $endDate, 3);
        $ifaData = $this->getRecipientProductionData($ifaUsers, $months, $consolidatedData, 'IFA');
        $monthlyProductionData['IFA'] = $ifaData['users']['IFA'];
        $consolidatedData = $ifaData['consolidated'];

        $data->data = collect(
            [
            'users' => $monthlyProductionData,
            'consolidated' => $consolidatedData
            ]
        );

        return $data;
    }

    /*
     * Get the data for the fas in the system
     */
    public function getFaData($users, $months, $monthlyProductionData, $consolidatedData)
    {
        foreach ($users as $user) {
            $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

            $dataArray = $this->getMonthlyProductionData($rec_id, $months, $user->unit, $consolidatedData);

            $consolidatedData = $dataArray['consolidated'];

            $monthlyProductionData[$user->unit][] = [
                'id' => $user->id,
                'name' => $user->name,
                'unit' => $user->unit,
                'days' => $user->days,
                'data' => $dataArray['user']
            ];
        }

        foreach ($consolidatedData as $key => $unitData) {
            $monthlyProductionData[$key][] = [
                'id' => null,
                'name' => 'Unit Total',
                'unit' => $key,
                'days' => '',
                'data' => $unitData
            ];
        }

        return [
            'users' => $monthlyProductionData,
            'consolidated' => $consolidatedData
        ];
    }

    /*
     * Get the data for the team commission recipients
     */
    public function getRecipientProductionData($users, $months, $consolidatedData, $unitName)
    {
        $productionData = array();

        foreach ($users as $user) {
            $rec_id = $user->id;

            $dataArray = $this->getMonthlyProductionData($rec_id, $months, $unitName, $consolidatedData);

            $consolidatedData = $dataArray['consolidated'];

            $productionData[$unitName][] = [
                'id' => $user->id,
                'name' => $user->name,
                'days' => null,
                'data' => $dataArray['user']
            ];
        }

        $productionData[$unitName][] = [
            'id' => null,
            'name' => 'Total',
            'days' => null,
            'data' => $consolidatedData[$unitName]
        ];

        return [
            'users' => $productionData,
            'consolidated' => $consolidatedData
        ];
    }

    /**
     * @param $rec_id
     * @param $months
     * @param $productIds
     * @return array
     */
    public function getMonthlyProductionData($rec_id, $months, $userUnit, $consolidatedData)
    {
        $monthlyProduction = array();

        foreach ($months as $month) {
            if (!is_null($rec_id)) {
                $startOfMonth = $month->startOfMonth();

                $endOfMonth = Carbon::parse($month)->endOfMonth();

                $inflows = $this->getCumulativeInflows($startOfMonth, $endOfMonth, $rec_id);

                $withdrawals = $this->getWithdrawals($startOfMonth, $endOfMonth, $rec_id);

                $actualRealEstateSales = $this->realEstateActualPayments($startOfMonth, $endOfMonth, $rec_id);

                $retention = $inflows - $withdrawals;
            } else {
                $inflows = $withdrawals = $retention = $actualRealEstateSales = 0;
            }

            $monthKey = $month->format('F Y');

            $monthlyProduction[$monthKey] = [
                'inflows' => $inflows,
                'withdrawals' => $withdrawals,
                'netflows' => $retention,
                'realEstateSales' => $actualRealEstateSales,
                'total' => $retention + $actualRealEstateSales
            ];

            if (isset($consolidatedData[$userUnit][$monthKey])) {
                $consolidatedData[$userUnit][$monthKey] = [
                    'inflows' => $inflows + $consolidatedData[$userUnit][$monthKey]['inflows'],
                    'withdrawals' => $withdrawals + $consolidatedData[$userUnit][$monthKey]['withdrawals'],
                    'netflows' => $retention + $consolidatedData[$userUnit][$monthKey]['netflows'],
                    'realEstateSales' =>
                        $actualRealEstateSales + $consolidatedData[$userUnit][$monthKey]['realEstateSales'],
                    'total' => $retention + $actualRealEstateSales + $consolidatedData[$userUnit][$monthKey]['total']
                ];
            } else {
                $consolidatedData[$userUnit][$monthKey] = [
                    'inflows' => $inflows,
                    'withdrawals' => $withdrawals,
                    'netflows' => $retention,
                    'realEstateSales' => $actualRealEstateSales,
                    'total' => $retention + $actualRealEstateSales
                ];
            }
        }

        return [
            'user' => $monthlyProduction,
            'consolidated' => $consolidatedData
        ];
    }

    /*
     * Get the commission recipients that fall in the specified recipient type
     */
    public function getProductionUsers(Carbon $start = null, Carbon $end, $type)
    {
        return CommissionRecepient::where('recipient_type_id', $type)->whereHas(
            'commissions',
            function ($q) use ($start, $end) {
                $q->whereHas(
                    'investment',
                    function ($q) use ($start, $end) {
                        $q->where('invested_date', '<=', $end);

                        if ($start) {
                            $q->where('invested_date', '>=', $start);
                        }
                    }
                );
            }
        )->get();
    }
}
