<?php
/**
 * Date: 08/06/2017
 * Time: 14:15
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace ServiceBus\Queries\FASummary;

use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class FAEvaluationSummary
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    /*
     * Handle the fa evaluation summary
     */
    public function handle($data)
    {
        $users = $data[0];

        $endYear = $data[1];

        return $this->generateReport($users, $endYear);
    }

    /*
     * Generate the report
     */
    public function generateReport($data, $end)
    {
        $end = Carbon::parse($end)->endOfDay();

        $data = json_decode($data);

        $evaluationData = array();

        $year = Carbon::parse($end)->year;

        foreach ($data->data as $user) {
            $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

            $ytdStartDate = Carbon::parse($user->ytd_start_date)->startOfDay();

            $cumulativeStartDate = Carbon::parse($user->cumulative_start_date)->startOfDay();

            if ($rec_id) {
                $YTDInflows = $this->getInflows($ytdStartDate, $end, $rec_id);

                $YTDWithdrawals = $this->getWithdrawals($ytdStartDate, $end, $rec_id);

                //Get the YTD net flow which is calculated by finding the difference between the inflows and withdrawals
                $YTDNetFlow = $YTDInflows - $YTDWithdrawals;

                $cumulativeInflows = $this->getInflows($cumulativeStartDate, $end, $rec_id);

                $cumulativeWithdrawals = $this->getWithdrawals($cumulativeStartDate, $end, $rec_id);

                //Get the cumulative netflow which is calculated by finding the difference between
                // the cumulative inflows and cumulative withdrawals
                $cumulativeNetFlow = $cumulativeInflows - $cumulativeWithdrawals;

                $YTDRealEstateSales = $this->realEstateSales($ytdStartDate, $end, $rec_id);

                $YTDRetention = $YTDNetFlow + $YTDRealEstateSales;

                $cumulativeRealEstateSales = $this->realEstateSales($cumulativeStartDate, $end, $rec_id);

                $cumulativeRetention = $cumulativeNetFlow + $cumulativeRealEstateSales;

                $evaluationData[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'days' => $user->days,
                    'title' => $user->title,
                    'year' => $year,
                    'retention' => [
                        'YTDRetention' => $YTDRetention,
                        'YTDTargets' => $user->YTDTargets,
                        'YTDPercentage' => percentage($YTDRetention, $user->YTDTargets, 1),
                        'cumulativePercentage' => percentage($cumulativeRetention, $user->cumulativeTargets, 1)
                    ]
                ];
            } else {
                $evaluationData[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'days' => $user->days,
                    'title' => $user->title,
                    'year' => $year,
                    'retention' => [
                        'YTDRetention' => 0,
                        'YTDTargets' => 0,
                        'YTDPercentage' => 0,
                        'cumulativePercentage' => 0
                    ]
                ];
            }
        }

        $data->data = $evaluationData;

        return $data;
    }
}
