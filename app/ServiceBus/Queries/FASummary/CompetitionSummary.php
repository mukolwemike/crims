<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\RealEstatePayment;
use Carbon\Carbon;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Presenters\ClientPresenter;

class CompetitionSummary
{
    /*
     * Handle the request
     */
    public function handle($data)
    {
        $startDate = Carbon::parse($data[0])->startOfDay();

        $emails = (array)json_decode($data[1]);

        $emailKeys = array_keys($emails);

        $investmentArray = array();

        $q = ClientInvestment::where('invested_date', '>=', $startDate)
            ->where('investment_type_id', '!=', 3)
            ->whereHas(
                'commission',
                function ($q) use ($emails, $emailKeys) {
                    $q->whereHas(
                        'recipient',
                        function ($q) use ($emails, $emailKeys) {
                            $q->whereIn('email', $emailKeys);
                        }
                    );
                }
            );

        $investments = $q->get();

        foreach ($investments as $investment) {
            if ($investment->product->currency_id == 1) {
                $investment->converted_amount = $investment->amount;
            } else {
                $investment->converted_amount = convert(
                    $investment->amount,
                    $investment->product->currency,
                    Carbon::parse($investment->invested_date)
                );
            }

            //            $aboveAmount = $investment->converted_amount >= 1000000;

            if ($investment->repo->getTenorInMonths() >= 12) {
                $recioient = $investment->commission->recipient;

                $investmentArray[] = [
                    'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
                    'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                    'Amount' => $investment->amount,
                    'Converted_Amount' => $investment->converted_amount,
                    'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                    'Tenor' => $investment->repo->getTenorInMonths(),
                    'Investment_Type' => $investment->type->name,
                    'FA' => $recioient->name,
                    'Position' => $emails[$recioient->email]
                ];
            }
        }

        $topups = ClientInvestmentTopup::where('date', '>=', $startDate)
            ->whereHas(
                'toInvestment',
                function ($q) use ($emails, $emailKeys) {
                    $q->whereHas(
                        'commission',
                        function ($q) use ($emails, $emailKeys) {
                            $q->whereHas(
                                'recipient',
                                function ($q) use ($emails, $emailKeys) {
                                    $q->whereIn('email', $emailKeys);
                                }
                            );
                        }
                    );
                }
            )->get();

        foreach ($topups as $topup) {
            $investment = $topup->toInvestment;

            if ($investment->product->currency_id == 1) {
                $investment->converted_amount = $topup->amount;
            } else {
                $investment->converted_amount = convert(
                    $topup->amount,
                    $investment->product->currency,
                    Carbon::parse($investment->invested_date)
                );
            }

            //            $aboveAmount = $investment->converted_amount >= 1000000;

            if ($investment->repo->getTenorInMonths() >= 12) {
                $recipient = $investment->commission->recipient;

                $investmentArray[] = [
                    'Invested Date' => Carbon::parse($investment->invested_date)->toDateString(),
                    'Client Name' => ClientPresenter::presentFullNames($investment->client_id),
                    'Amount' => $topup->amount,
                    'Converted_Amount' => $investment->converted_amount,
                    'Maturity Date' => Carbon::parse($investment->maturity_date)->toDateString(),
                    'Tenor' => $investment->repo->getTenorInMonths(),
                    'Investment_Type' => $investment->type->name,
                    'FA' => $recipient->name,
                    'Position' => $emails[$recipient->email]
                ];
            }
        }

        $reArray = array();

        $reActual = RealEstatePayment::whereHas(
            'holding',
            function ($holding) use ($emails, $emailKeys) {
                $holding->whereHas(
                    'commission',
                    function ($q) use ($emails, $emailKeys) {
                        $q->whereHas(
                            'recipient',
                            function ($q) use ($emails, $emailKeys) {
                                $q->whereIn('email', $emailKeys);
                            }
                        );
                    }
                );
            }
        )->where('date', '>=', $startDate)->get();

        foreach ($reActual as $payment) {
            $holding = $payment->holding;

            $recipient = $holding->commission->recipient;

            $reArray[] = [
                'Date' => Carbon::parse($payment->date)->toDateString(),
                'Name' => ($holding->client) ? $holding->client->name() : '',
                'Amount' => round($payment->amount, 0),
                'Project' => $holding->unit->project->name . ' - ' . $holding->unit->number,
                'FA' => $recipient->name,
                'Position' => $emails[$recipient->email]
            ];
        }

        return json_encode(
            [
                'Investments' => $investmentArray,
                'RE' => $reArray
            ]
        );
    }
}
