<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\WeeklyDistribution;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use ServiceBus\Queries\FASummary\DailyDistribution\UnitSummaryTrait;

/**
 * Class UnitProductionSummary
 * @package ServiceBus\Queries\FASummary\WeeklyDistribution
 */
class UnitProductionSummary
{
    use UnitSummaryTrait;

    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * @var int
     */
    protected $outflowType;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->outflowType = $outflowType;
    }

    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function getData()
    {
        return $this->processUnitProductionSummary();
    }

    /**
     * @return \Illuminate\Support\Collection|static
     */
    private function processUnitProductionSummary()
    {
        $unitInflows = $this->processInflows();

        $unitOutflows = $this->processOutflows();

        $unitReCashflows = $this->processReCashflow();

        $unitReSales = $this->processReSales();

        return $this->getDepartmentUnitsData($unitInflows, $unitOutflows, $unitReCashflows, $unitReSales);
    }

    /**
     * @param $unitInflows
     * @param $unitOutflows
     * @param $unitReCashflows
     * @param $unitReSales
     * @return \Illuminate\Support\Collection|static
     */
    private function getDepartmentUnitsData($unitInflows, $unitOutflows, $unitReCashflows, $unitReSales)
    {
        $units = $this->getDepartmentUnits();

        $unitArray = array();

        foreach ($units as $unit) {
            $branch = $unit->present()->getBranch;

            $head = $unit->present()->getHead;

            $unitName = $unit->name;

            $unitArray[] = $this->updateUnitArray(
                $unitName,
                $branch,
                $head,
                $unitInflows,
                $unitOutflows,
                $unitReCashflows,
                $unitReSales
            );
        }

        $unitArray = $this->getOthersData($unitArray, $unitInflows, $unitOutflows, $unitReCashflows, $unitReSales);

        $unitCol = collect($unitArray);

        $totalNetTotal = $unitCol->sum('total_netflow');

        $unitCol = $unitCol->map(function ($item) use ($totalNetTotal) {
            $item['percentage'] = percentage($item['total_netflow'], $totalNetTotal, 2);

            return $item;
        });

        $unitCol->put('total', [
            'branch' => 'Total',
            'unit' => '',
            'head' => '',
            'inflow' => $unitCol->sum('inflow'),
            'outflow' => $unitCol->sum('outflow'),
            'netflow' => $unitCol->sum('netflow'),
            're_sales' => $unitCol->sum('re_sales'),
            're_cashflow' => $unitCol->sum('re_cashflow'),
            'total_netflow' => $totalNetTotal,
            'percentage' => 100
        ]);

        return $unitCol;
    }

    /**
     * @param $unitName
     * @param $branch
     * @param $head
     * @param $unitInflows
     * @param $unitOutflows
     * @param $unitReCashflows
     * @param $unitReSales
     * @return array
     */
    private function updateUnitArray(
        $unitName,
        $branch,
        $head,
        &$unitInflows,
        &$unitOutflows,
        &$unitReCashflows,
        &$unitReSales
    ) {
        $inflow = array_key_exists($unitName, $unitInflows) ? array_pull($unitInflows, $unitName) : 0;

        $outflow = array_key_exists($unitName, $unitOutflows) ? array_pull($unitOutflows, $unitName) : 0;

        $reCashflow = array_key_exists($unitName, $unitReCashflows) ? array_pull($unitReCashflows, $unitName) : 0;

        $reSales = array_key_exists($unitName, $unitReSales) ? array_pull($unitReSales, $unitName) : 0;

        return [
            'branch' => $branch,
            'unit' => $unitName,
            'head' => $head,
            'inflow' => $inflow,
            'outflow' => $outflow,
            'netflow' => $inflow - $outflow,
            're_sales' => $reSales,
            're_cashflow' => $reCashflow,
            'total_netflow' => $inflow + $reCashflow - $outflow
        ];
    }

    /**
     * @param $unitArray
     * @param $unitInflows
     * @param $unitOutflows
     * @param $unitReCashflows
     * @param $unitReSales
     * @return array
     */
    private function getOthersData($unitArray, $unitInflows, $unitOutflows, $unitReCashflows, $unitReSales)
    {
        foreach ($unitInflows as $key => $inflow) {
            $unitArray[] = $this->updateUnitArray(
                $key,
                $key,
                '',
                $unitInflows,
                $unitOutflows,
                $unitReCashflows,
                $unitReSales
            );
        }

        foreach ($unitOutflows as $key => $outflow) {
            $unitArray[] = $this->updateUnitArray(
                $key,
                $key,
                '',
                $unitInflows,
                $unitOutflows,
                $unitReCashflows,
                $unitReSales
            );
        }

        foreach ($unitReCashflows as $key => $reCashflow) {
            $unitArray[] = $this->updateUnitArray(
                $key,
                $key,
                '',
                $unitInflows,
                $unitOutflows,
                $unitReCashflows,
                $unitReSales
            );
        }

        foreach ($unitReSales as $key => $reSale) {
            $unitArray[] = $this->updateUnitArray(
                $key,
                $key,
                '',
                $unitInflows,
                $unitOutflows,
                $unitReCashflows,
                $unitReSales
            );
        }

        return $unitArray;
    }

    /**
     * @return array
     */
    private function processReCashflow()
    {
        $inflowArray = array();

        $inflows = $this->getReCashflow();

        foreach ($inflows as $payment) {
            $data = $this->getRePaymentData($payment);

            if (array_key_exists($data['unit'], $inflowArray)) {
                $inflowArray[$data['unit']] = $inflowArray[$data['unit']] + $data['amount'];
            } else {
                $inflowArray[$data['unit']] = $data['amount'];
            }
        }

        return $inflowArray;
    }

    /**
     * @param RealEstatePayment $payment
     * @return array
     */
    private function getRePaymentData(RealEstatePayment $payment)
    {
        $recipient = $payment->present()->getCommissionRecipient;

        return [
            'unit' => ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown',
            'amount' => $payment->amount
        ];
    }

    /**
     * @return mixed
     */
    private function getReCashflow()
    {
        return RealEstatePayment::columnBetween('date', $this->startDate, $this->endDate)
            ->get();
    }

    /**
     * @return array
     */
    private function processReSales()
    {
        $inflowArray = array();

        $inflows = $this->getReSales();

        foreach ($inflows as $payment) {
            $data = $this->getHoldingData($payment);

            if (array_key_exists($data['unit'], $inflowArray)) {
                $inflowArray[$data['unit']] = $inflowArray[$data['unit']] + $data['amount'];
            } else {
                $inflowArray[$data['unit']] = $data['amount'];
            }
        }

        return $inflowArray;
    }

    /**
     * @param UnitHolding $holding
     * @return array
     */
    private function getHoldingData(UnitHolding $holding)
    {
        $recipient = $this->getHoldingCommissionRecipient($holding);

        return [
            'unit' => ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown',
            'amount' => $holding->price()
        ];
    }

    /**
     * @return mixed
     */
    private function getReSales()
    {
        return UnitHolding::columnBetween('created_at', $this->startDate, $this->endDate)
            ->where('active', 1)
            ->get();
    }
}
