<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\WeeklyDistribution;

use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;

class REProductionSummary
{
    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getData()
    {
        $reArray = array();

        $reSales = $this->getReSales();

        foreach ($reSales as $holding) {
            $data = $this->presentHolding($holding);

            if (array_key_exists($data['product'], $reArray)) {
                $reArray[$data['product_key']]['product'] = $data['product'];

                $reArray[$data['product_key']]['product_value'] =
                    $data['product'] + $reArray[$data['product_key']]['product_value'];

                $reArray[$data['product_key']]['units_sold'] = $reArray[$data['product_key']]['units_sold'] + 1;
            } else {
                $reArray[$data['product_key']]['product'] = $data['product'];

                $reArray[$data['product_key']]['product_value'] = $data['product'];

                $reArray[$data['product_key']]['units_sold'] = 1;

                $reArray[$data['product_key']]['amount'] = 0;
            }
        }

        $reCash = $this->getReCashflow();

        foreach ($reCash as $payment) {
            $data = $this->presentPayment($payment);

            if (array_key_exists($data['product'], $reArray)) {
                $reArray[$data['product_key']]['product'] = $data['product'];

                $reArray[$data['product_key']]['amount'] = $data['amount'] + $reArray[$data['product_key']]['amount'];
            } else {
                $reArray[$data['product_key']]['product'] = $data['product'];

                $reArray[$data['product_key']]['amount'] = $data['amount'];

                $reArray[$data['product_key']]['units_sold'] = 0;

                $reArray[$data['product_key']]['product_value'] = 0;
            }
        }

        $reCol = collect($reArray);

        $totalAmount = $reCol->sum('amount');

        $totalUnits = $reCol->sum('units_sold');

        $totalValue = $reCol->sum('product_value');

        $reCol->put('total', [
            'product' => 'Total',
            'amount' => $totalAmount,
            'units_sold' => $totalUnits,
            'product_value' => $totalValue
        ]);

        return $reCol;
    }

    private function getReSales()
    {
        return UnitHolding::where('active', 1)
            ->columnBetween('created_at', $this->startDate, $this->endDate)
            ->get();
    }

    private function presentHolding(UnitHolding $holding)
    {
        $data = array();

        $unit = $holding->unit;

        $data['product_key'] = $unit->project_id . '_' . $unit->size_id;

        $data['product'] = $unit->project->name . ' ' . $unit->present()->unitSizeName;

        $data['product_value'] = $holding->price();

        return $data;
    }

    private function getReCashflow()
    {
        return RealEstatePayment::columnBetween('date', $this->startDate, $this->endDate)
            ->get();
    }

    private function presentPayment(RealEstatePayment $payment)
    {
        $data = array();

        $unit = $payment->holding->unit;

        $data['product_key'] = $unit->project_id . '_' . $unit->size_id;

        $data['product'] = $unit->project->name . ' ' . $unit->present()->unitSizeName;

        $data['amount'] = $payment->amount;

        return $data;
    }
}
