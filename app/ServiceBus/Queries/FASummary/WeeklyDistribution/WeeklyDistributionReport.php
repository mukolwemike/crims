<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\WeeklyDistribution;

use Carbon\Carbon;
use ServiceBus\Queries\FASummary\DailyDistribution\DailyDistributionTrait;

class WeeklyDistributionReport
{
    use DailyDistributionTrait;
    /**
     * @param $data
     * @return array
     */
    public function handle($data)
    {
        $startDate = Carbon::parse($data[0])->startOfDay();

        $endDate = Carbon::parse($data[1])->endOfDay();

        $outflowType = $data[2];

        return $this->getReport($startDate, $endDate, $outflowType);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    public function getReport(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $dataArray = array();

        $dataArray['inflows_summary'] = $this->getInflowSummary($startDate->copy(), $endDate->copy());

        $dataArray['outflows_summary'] = $this->getOutflowSummary($startDate->copy(), $endDate->copy(), $outflowType);

        $dataArray['re_cashflow_summary'] = $this->getReCashFlowSummary($startDate->copy(), $endDate->copy());

        $dataArray['re_sales_summary'] = $this->getReSalesSumary($startDate->copy(), $endDate->copy());

        $dataArray['netflow_summary'] = $this->getNetflowSummary($dataArray);

        $dataArray['wtd_summary'] = $this->getWeekToDateSummary($startDate->copy(), $outflowType);

        $dataArray['re_production_summary'] = $this->getREProductionSummary($startDate->copy(), $endDate->copy());

        $dataArray['unit_production_summary'] = $this->getUnitProductionSummary(
            $startDate->copy(),
            $endDate->copy(),
            $outflowType
        );

        return $dataArray;
    }
}
