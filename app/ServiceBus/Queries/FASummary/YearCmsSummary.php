<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 8/2/17
 * Time: 12:52 PM
 */

namespace ServiceBus\Queries\FASummary;

use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Reporting\SummaryReports\Inflows;

class YearCmsSummary
{
    /*
    * Get the FA generator trait
    */
    use Inflows,
        WithdrawalsTrait;

    /*
     * Handle the request
     */
    public function handle($data)
    {
        $startYear = $data[0];

        $endYear = $data[1];

        return $this->generateReport($startYear, $endYear);
    }

    /*
     * Generete the report
     */
    public function generateReport($startDate, $endDate)
    {
        $cmsIds = $this->getCMSProductId();

        $months = getDateRange(Carbon::parse($startDate), Carbon::parse($endDate));

        $yearCmsSummaryData = array();

        foreach ($months as $month) {
            $startOfMonth = $month->startOfMonth();

            $endOfMonth = Carbon::parse($month)->endOfMonth();

            $inflows = $this->getInflows($startOfMonth, $endOfMonth, null, $cmsIds);

            $withdrawals = $this->getWithdrawals($startOfMonth, $endOfMonth, null, $cmsIds);

            $yearCmsSummaryData[$month->format('F Y')] = [
                'inflows' => $inflows,
                'withdrawals' => $withdrawals,
                'netProduction' => $inflows - $withdrawals
            ];
        }

        return collect($yearCmsSummaryData);
    }

    /*
     * Get CMS product id
    */
    private function getCMSProductId()
    {
        return Product::where('shortname', 'CHYS')
            ->pluck('id')
            ->toArray();
    }
}
