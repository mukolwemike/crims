<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class IndividualYearClientsExport
{
    /*
     * Get the traits
     */
      use InFlowsTrait,
          WithdrawalsTrait,
          RealEstateSalesTrait;

    /*
     * Handle the incoming request
     */
    public function handle($data)
    {
        $userData = json_decode($data[0]);

        $type = $data[1];

        $user = $userData->data;

        return $this->generateReport($user, $type);
    }

    /*
     * Generate the report
     */
    public function generateReport($user, $type)
    {
        $rec_id = $this->getCommissionRecipient($user->email);

        $startDate = Carbon::parse($user->start_date)->startOfDay();

        $endDate = Carbon::parse($user->end_date)->endOfDay();

        if ($type == 1) {
            return $this->getInflowClients($startDate, $endDate, $rec_id);
        } elseif ($type == 2) {
            return $this->getOutFlowClients($startDate, $endDate, $rec_id);
        } elseif ($type == 3) {
            return $this->getRealEstateSalesClients($startDate, $endDate, $rec_id);
        } elseif ($type == 4) {
            return $this->getRealEstateActualPayments($startDate, $endDate, $rec_id);
        }

        return [];
    }

    /**
     * Get the commission recipient
     *
     * @param $email
     * @return mixed
     */
    private function getCommissionRecipient($email)
    {
        return CommissionRecepient::where('email', $email)->pluck('id')->first();
    }
}
