<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use App\Cytonn\Models\ClientInvestment;
use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\Datatables\SummaryPaginator;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\Transformers\YtdInflowClientTransformer;

class IndividualYtdInflowSummary
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait;

    /*
     * Get the sort filter paginate trait
     */
    use SummaryPaginator;

    /*
     * Handle the incoming request
     */
    public function handle($data)
    {
        $userData = json_decode($data[0]);

        $tableState = (array)$userData->tableState;

        $user = $userData->data;

        $endYear = $data[1];

        return $this->generateReport($tableState, $user, $endYear);
    }

    /*
     * Generate the report
     */
    public function generateReport($tableState, $user, $endYear)
    {
        $endYear = Carbon::parse($endYear)->endOfDay();

        $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

        $filterFunction = $this->getInflowClientsQuery($user->ytd_start_date, $endYear, $rec_id);

        return $this->processTable(
            new ClientInvestment(),
            $tableState,
            new YtdInflowClientTransformer($endYear),
            $filterFunction,
            []
        );
    }
}
