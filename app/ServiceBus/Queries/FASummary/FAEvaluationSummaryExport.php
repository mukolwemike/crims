<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Support\Mails\Mailer;
use Maatwebsite\Excel\Facades\Excel;

class FAEvaluationSummaryExport
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    /*
     * Handle the incoming request
     */
    public function handle($data)
    {
        try {
            $users = $data[0];

            $endYear = $data[1];

            $depatmentUnit = isset($data[3]) ? $data[3] : 0;

            $excludeOthers = isset($data[4]) ? $data[4] : 0;

            $email = isset($data[5]) ? $data[5] : null;

            return $this->generateReport($users, $endYear, $depatmentUnit, $excludeOthers, $email);
        } catch (\Exception $exception) {
            $this->notifyError($exception, $email);
        }
    }

    /**
     * @param \Exception $exception
     * @param $email
     */
    private function notifyError(\Exception $exception, $email)
    {
        Mailer::compose()
            ->from([
                'address' => 'crmsystem@cytonn.com',
                'name' => 'Cytonn CRM'
            ])
            ->to([$email])
            ->bcc(config('system.administrators'))
            ->subject('CRIMS : FA Evaluation Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
               <p>Trace : " . $exception->getTraceAsString() . "</p>")
            ->send();
    }

    /*
     * Generate the report
     */
    public function generateReport($data, $end, $departmentUnit, $excludeOthers, $email = null)
    {
        if ($email) {
            Mailer::compose()
                ->from([
                    'address' => 'crmsystem@cytonn.com',
                    'name' => 'Cytonn CRM'
                ])
                ->to([$email])
                ->bcc(config('system.administrators'))
                ->subject('FA Evaluation Acknowledge Notification')
                ->text('FA Evaluation processing Start')
                ->send();
        }

        $reportStartTime = Carbon::now();

        $data = json_decode($data);

        $evaluationData = array();

        $consolidatedData = array();

        $endYear = Carbon::parse($end)->endOfDay();
        $startYear = $endYear->copy()->startOfYear();

        if (in_array($departmentUnit, ['team', 'ifa'])) {
            if ($departmentUnit == 'ifa') {
                $unitName = 'IFA';
                $type = 3;
            } else {
                $unitName = 'Team';
                $type = 1;
            }

            $users = $this->getProductionUsers($startYear, $endYear, $type);

            $evaluationData = $this->getRecipientProductionData($users, $evaluationData, $endYear, $unitName);
        } else {
            $evaluationData = $this->getFaData($data->data, $evaluationData, $consolidatedData, $endYear);

            if ($excludeOthers != 0) {
                $staffUsers = $this->getProductionUsers($startYear, $endYear, 1);
                $evaluationData =
                    $this->getRecipientProductionData($staffUsers, $evaluationData, $endYear, 'Team');

                $ifaUsers = $this->getProductionUsers($startYear, $endYear, 3);
                $evaluationData =
                    $this->getRecipientProductionData($ifaUsers, $evaluationData, $endYear, 'IFA');
            }
        }

        if ($email) {
            Mailer::compose()
                ->from([
                    'address' => 'crmsystem@cytonn.com',
                    'name' => 'Cytonn CRM'
                ])
                ->to([$email])
                ->bcc(config('system.administrators'))
                ->subject('FA Evaluation Acknowledge Notification')
                ->text('FA Evaluation processing end')
                ->send();

            $fileName = 'FA_Evaluation_Summary';

            $evaluationData = \GuzzleHttp\json_encode($evaluationData);

            $evaluationData = \GuzzleHttp\json_decode($evaluationData);

            $this->generateFaEvaluationExcel(
                $evaluationData,
                $fileName,
                'exports.fas.fa_evaluation_summary'
            );

            Mailer::compose()
                ->from([
                    'address' => 'crmsystem@cytonn.com',
                    'name' => 'Cytonn CRM'
                ])
                ->to([$email])
                ->bcc(config('system.administrators'))
                ->subject('CRM FA Evaluation Report')
                ->text('Please find attached the fa evaluation report. (Time taken = '
                    . Carbon::now()->diffInMinutes($reportStartTime) . ' minutes)')
                ->excel([$fileName])
                ->send();

            return [];
        } else {
            return $evaluationData;
        }
    }

    /*
     * Generate FA evaluation evaluation excel
     */
    public function generateFaEvaluationExcel($data, $name, $consolidatedView)
    {
        Excel::create($name, function ($excel) use ($data, $consolidatedView) {
            $excel->sheet('Consolidated', function ($sheet) use ($data, $consolidatedView) {
                $sheet->loadView($consolidatedView, ['values' => $data]);
            });
        })->store('xlsx', storage_path('exports'));
    }

    /*
     * Get the recipient production data
     */
    public function getRecipientProductionData($users, $evaluationData, Carbon $end, $unitName)
    {
        $consolidatedData = array();

        foreach ($users as $user) {
            $rec_id = $user->id;

            $ytdStartDate = $end->copy()->startOfYear();

            $cumulativeStartDate = null;

            $employmentDate = null;

            $userData = $this->getUserEvaluationData(
                $user,
                $rec_id,
                $ytdStartDate,
                $cumulativeStartDate,
                $end,
                $employmentDate,
                0,
                0
            );

            $evaluationData[$unitName][$unitName][] = $this->prepareUserData($user, $userData);

            $consolidatedData = $this->updateUnitData($consolidatedData, $unitName, $unitName, $userData);
        }

        $evaluationData[$unitName][$unitName][] = [
            'id' => null,
            'name' => 'UNIT TOTAL (EVALUATION)',
            'unit' => '',
            'branch' => '',
            'days' => '',
            'value_days' => '',
            'title' => '',
            'fa_title' => '',
            'suspended_days' => '',
            'currentFaPositionDays' => '',
            'employmentStatus' => '',
            'position_status' => '',
            'currentPositionDays' => '',
            'ytd_start_date' => '',
            'cumulative_start_date' => '',
            'targets_start_date' => '',
            'investments' => [
                'inflow' => $consolidatedData[$unitName][$unitName]['investments']['inflow'],
                'cumulativeInflows' => $consolidatedData[$unitName][$unitName]['investments']['cumulativeInflows'],
                'outflow' => $consolidatedData[$unitName][$unitName]['investments']['outflow'],
                'cumulativeOutflows' => $consolidatedData[$unitName][$unitName]['investments']['cumulativeOutflows'],
                'netflow' => $consolidatedData[$unitName][$unitName]['investments']['netflow'],
                'cumulativeNetflow' => $consolidatedData[$unitName][$unitName]['investments']['cumulativeNetflow']
            ],
            'real_estate' => [
                'YTDRealEstateSales' => $consolidatedData[$unitName][$unitName]['real_estate']['YTDRealEstateSales'],
                'YTDRealEstateCashPlanSales' =>
                    $consolidatedData[$unitName][$unitName]['real_estate']['YTDRealEstateCashPlanSales'],
                'YTDActualPayments' => $consolidatedData[$unitName][$unitName]['real_estate']['YTDActualPayments'],
                'cumulativeRealEstateSales' =>
                    $consolidatedData[$unitName][$unitName]['real_estate']['cumulativeRealEstateSales'],
                'cumulativeRealEstateCashPlanSales' =>
                    $consolidatedData[$unitName][$unitName]['real_estate']['cumulativeRealEstateCashPlanSales'],
                'cumulativeCashFlows' => $consolidatedData[$unitName][$unitName]['real_estate']['cumulativeCashFlows']
            ],
            'retention' => [
                'YTDRetention' => $consolidatedData[$unitName][$unitName]['retention']['YTDRetention'],
                'cumulativeRetention' => $consolidatedData[$unitName][$unitName]['retention']['cumulativeRetention'],
                'YTDTargets' => $consolidatedData[$unitName][$unitName]['retention']['YTDTargets'],
                'cumulativeTargets' => $consolidatedData[$unitName][$unitName]['retention']['cumulativeTargets'],
                'productionBeforeDemotion' => '',
                'YTDPercentageRetention' => percentage(
                    $consolidatedData[$unitName][$unitName]['investments']['netflow'],
                    $consolidatedData[$unitName][$unitName]['investments']['inflow'],
                    1
                ),
                'cumulativePercentageRetention' => percentage(
                    $consolidatedData[$unitName][$unitName]['investments']['cumulativeNetflow'],
                    $consolidatedData[$unitName][$unitName]['investments']['cumulativeInflows'],
                    1
                ),
                'YTDPercentage' => percentage(
                    $consolidatedData[$unitName][$unitName]['retention']['YTDRetention'],
                    $consolidatedData[$unitName][$unitName]['retention']['YTDTargets'],
                    1
                ),
                'cumulativePercentage' => percentage(
                    $consolidatedData[$unitName][$unitName]['retention']['cumulativeRetention'],
                    $consolidatedData[$unitName][$unitName]['retention']['cumulativeTargets'],
                    1
                )
            ]
        ];

        return $evaluationData;
    }

    /*
     * Generate a report for the fas
     */
    public function getFaData($users, $evaluationData, $consolidatedData, $end)
    {
        foreach ($users as $user) {
            $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

            if (is_null($rec_id)) {
                continue;
            }

            $ytdStartDate = Carbon::parse($user->ytd_start_date);

            $cumulativeStartDate = Carbon::parse($user->cumulative_start_date);

            $employmentDate = Carbon::parse($user->production_employment_date);

            $userData = $this->getUserEvaluationData(
                $user,
                $rec_id,
                $ytdStartDate,
                $cumulativeStartDate,
                $end,
                $employmentDate
            );

            $evaluationData[$user->branch][$user->unit][] = $this->prepareUserData($user, $userData);

            $consolidatedData = $this->updateUnitData($consolidatedData, $user->branch, $user->unit, $userData, $user->unit_manager);

//            $consolidatedData = $this->processDepartmentalData($user, $rec_id, $consolidatedData, $userData);
        }

        foreach ($consolidatedData as $branchKey => $branchData) {
            foreach ($branchData as $unitKey => $unitData) {
                $evaluationData[$branchKey][$unitKey][] = [
                    'id' => null,
                    'name' => 'UNIT TOTAL (EVALUATION)',
                    'unit' => '',
                    'branch' => '',
                    'days' => '',
                    'value_days' => '',
                    'title' => '',
                    'fa_title' => '',
                    'suspended_days' => '',
                    'currentFaPositionDays' => '',
                    'employmentStatus' => '',
                    'position_status' => '',
                    'currentPositionDays' => '',
                    'ytd_start_date' => '',
                    'cumulative_start_date' => '',
                    'targets_start_date' => '',
                    'investments' => [
                        'inflow' => $unitData['unit_data']['investments']['inflow'],
                        'cumulativeInflows' => $unitData['unit_data']['investments']['cumulativeInflows'],
                        'outflow' => $unitData['unit_data']['investments']['outflow'],
                        'cumulativeOutflows' => $unitData['unit_data']['investments']['cumulativeOutflows'],
                        'netflow' => $unitData['unit_data']['investments']['netflow'],
                        'cumulativeNetflow' => $unitData['unit_data']['investments']['cumulativeNetflow']
                    ],
                    'real_estate' => [
                        'YTDRealEstateSales' => $unitData['unit_data']['real_estate']['YTDRealEstateSales'],
                        'YTDRealEstateCashPlanSales' => $unitData['unit_data']['real_estate']['YTDRealEstateCashPlanSales'],
                        'YTDActualPayments' => $unitData['unit_data']['real_estate']['YTDActualPayments'],
                        'cumulativeRealEstateSales' => $unitData['unit_data']['real_estate']['cumulativeRealEstateSales'],
                        'cumulativeRealEstateCashPlanSales' => $unitData['unit_data']['real_estate']['cumulativeRealEstateCashPlanSales'],
                        'cumulativeCashFlows' => $unitData['unit_data']['real_estate']['cumulativeCashFlows']
                    ],
                    'retention' => [
                        'YTDRetention' => $unitData['unit_data']['retention']['YTDRetention'],
                        'cumulativeRetention' => $unitData['unit_data']['retention']['cumulativeRetention'],
                        'productionBeforeDemotion' => '',
                        'YTDTargets' => $unitData['unit_data']['retention']['YTDTargets'],
                        'cumulativeTargets' => $unitData['unit_data']['retention']['cumulativeTargets'],
                        'YTDPercentageRetention' => percentage(
                            $unitData['unit_data']['investments']['netflow'],
                            $unitData['unit_data']['investments']['inflow'],
                            1
                        ),
                        'cumulativePercentageRetention' => percentage(
                            $unitData['unit_data']['investments']['cumulativeNetflow'],
                            $unitData['unit_data']['investments']['cumulativeInflows'],
                            1
                        ),
                        'YTDPercentage' => percentage(
                            $unitData['unit_data']['retention']['YTDRetention'],
                            $unitData['unit_data']['retention']['YTDTargets'],
                            1
                        ),
                        'cumulativePercentage' => percentage(
                            $unitData['unit_data']['retention']['cumulativeRetention'],
                            $unitData['unit_data']['retention']['cumulativeTargets'],
                            1
                        )
                    ]
                ];

                $evaluationData[$branchKey][$unitKey][] = [
                    'id' => null,
                    'name' => 'UNIT TOTAL LESS UM',
                    'unit' => '',
                    'branch' => '',
                    'days' => '',
                    'value_days' => '',
                    'title' => '',
                    'fa_title' => '',
                    'suspended_days' => '',
                    'currentFaPositionDays' => '',
                    'employmentStatus' => '',
                    'position_status' => '',
                    'currentPositionDays' => '',
                    'ytd_start_date' => '',
                    'cumulative_start_date' => '',
                    'targets_start_date' => '',
                    'investments' => [
                        'inflow' => $unitData['no_um_unit_data']['investments']['inflow'],
                        'cumulativeInflows' => $unitData['no_um_unit_data']['investments']['cumulativeInflows'],
                        'outflow' => $unitData['no_um_unit_data']['investments']['outflow'],
                        'cumulativeOutflows' => $unitData['no_um_unit_data']['investments']['cumulativeOutflows'],
                        'netflow' => $unitData['no_um_unit_data']['investments']['netflow'],
                        'cumulativeNetflow' => $unitData['no_um_unit_data']['investments']['cumulativeNetflow']
                    ],
                    'real_estate' => [
                        'YTDRealEstateSales' => $unitData['no_um_unit_data']['real_estate']['YTDRealEstateSales'],
                        'YTDRealEstateCashPlanSales' => $unitData['no_um_unit_data']['real_estate']['YTDRealEstateCashPlanSales'],
                        'YTDActualPayments' => $unitData['no_um_unit_data']['real_estate']['YTDActualPayments'],
                        'cumulativeRealEstateSales' => $unitData['no_um_unit_data']['real_estate']['cumulativeRealEstateSales'],
                        'cumulativeRealEstateCashPlanSales' => $unitData['no_um_unit_data']['real_estate']['cumulativeRealEstateCashPlanSales'],
                        'cumulativeCashFlows' => $unitData['no_um_unit_data']['real_estate']['cumulativeCashFlows']
                    ],
                    'retention' => [
                        'YTDRetention' => $unitData['no_um_unit_data']['retention']['YTDRetention'],
                        'cumulativeRetention' => $unitData['no_um_unit_data']['retention']['cumulativeRetention'],
                        'productionBeforeDemotion' => '',
                        'YTDTargets' => $unitData['no_um_unit_data']['retention']['YTDTargets'],
                        'cumulativeTargets' => $unitData['no_um_unit_data']['retention']['cumulativeTargets'],
                        'YTDPercentageRetention' => percentage(
                            $unitData['no_um_unit_data']['investments']['netflow'],
                            $unitData['no_um_unit_data']['investments']['inflow'],
                            1
                        ),
                        'cumulativePercentageRetention' => percentage(
                            $unitData['no_um_unit_data']['investments']['cumulativeNetflow'],
                            $unitData['no_um_unit_data']['investments']['cumulativeInflows'],
                            1
                        ),
                        'YTDPercentage' => percentage(
                            $unitData['no_um_unit_data']['retention']['YTDRetention'],
                            $unitData['no_um_unit_data']['retention']['YTDTargets'],
                            1
                        ),
                        'cumulativePercentage' => percentage(
                            $unitData['no_um_unit_data']['retention']['cumulativeRetention'],
                            $unitData['no_um_unit_data']['retention']['cumulativeTargets'],
                            1
                        )
                    ]
                ];

                $evaluationData[$branchKey][$unitKey][] = [
                    'id' => null,
                    'name' => 'TEAM PERCENTAGE',
                    'unit' => '',
                    'branch' => '',
                    'days' => '',
                    'value_days' => '',
                    'title' => '',
                    'fa_title' => '',
                    'suspended_days' => '',
                    'currentFaPositionDays' => '',
                    'employmentStatus' => '',
                    'position_status' => '',
                    'currentPositionDays' => '',
                    'ytd_start_date' => '',
                    'cumulative_start_date' => '',
                    'targets_start_date' => '',
                    'investments' => [
                        'inflow' => $this->getUnitPercentage($unitData, 'investments', 'inflow'),
                        'cumulativeInflows' => $this->getUnitPercentage($unitData, 'investments', 'cumulativeInflows'),
                        'outflow' => $this->getUnitPercentage($unitData, 'investments', 'outflow'),
                        'cumulativeOutflows' => $this->getUnitPercentage($unitData, 'investments', 'cumulativeOutflows'),
                        'netflow' => $this->getUnitPercentage($unitData, 'investments', 'netflow'),
                        'cumulativeNetflow' => $this->getUnitPercentage($unitData, 'investments', 'cumulativeNetflow'),
                    ],
                    'real_estate' => [
                        'YTDRealEstateSales' => $this->getUnitPercentage($unitData, 'real_estate', 'YTDRealEstateSales'),
                        'YTDRealEstateCashPlanSales' => $this->getUnitPercentage($unitData, 'real_estate', 'YTDRealEstateCashPlanSales'),
                        'YTDActualPayments' => $this->getUnitPercentage($unitData, 'real_estate', 'YTDActualPayments'),
                        'cumulativeRealEstateSales' => $this->getUnitPercentage($unitData, 'real_estate', 'cumulativeRealEstateSales'),
                        'cumulativeRealEstateCashPlanSales' => $this->getUnitPercentage($unitData, 'real_estate', 'cumulativeRealEstateCashPlanSales'),
                        'cumulativeCashFlows' => $this->getUnitPercentage($unitData, 'real_estate', 'cumulativeCashFlows'),
                    ],
                    'retention' => [
                        'YTDRetention' => $this->getUnitPercentage($unitData, 'retention', 'YTDRetention'),
                        'cumulativeRetention' => $this->getUnitPercentage($unitData, 'retention', 'cumulativeRetention'),
                        'productionBeforeDemotion' => '',
                        'YTDTargets' => $this->getUnitPercentage($unitData, 'retention', 'YTDTargets'),
                        'cumulativeTargets' => $this->getUnitPercentage($unitData, 'retention', 'cumulativeTargets'),
                        'YTDPercentageRetention' => '',
                        'cumulativePercentageRetention' => '',
                        'YTDPercentage' => '',
                        'cumulativePercentage' => ''
                    ]
                ];
            }
        }

        return $evaluationData;
    }

    /**
     * @param $unitData
     * @param $section
     * @param $subSection
     * @return float|int
     */
    private function getUnitPercentage($unitData, $section, $subSection)
    {
        return percentage(
            $unitData['no_um_unit_data'][$section][$subSection],
            $unitData['unit_data'][$section][$subSection],
            1
        );
    }

    /**
     * @param $user
     * @param $rec_id
     * @param $ytdStartDate
     * @param $cumulativeStartDate
     * @param $end
     * @param $employmentDate
     * @param int $unitData
     * @param int $faData
     * @return array
     */
    private function getUserEvaluationData(
        $user,
        $rec_id,
        $ytdStartDate,
        $cumulativeStartDate,
        $end,
        $employmentDate,
        $unitData = 0,
        $faData = 1
    ) {
        if ($ytdStartDate) {
            $YTDInflows = $this->getYTDInflows($ytdStartDate, $end, $rec_id);

            $YTDWithdrawals = $unitData == 1 ? $this->getWithdrawalsWithoutEndDate($ytdStartDate, $end, $rec_id) :
                $this->getWithdrawals($ytdStartDate, $end, $rec_id);

            $YTDNetFlow = $YTDInflows - $YTDWithdrawals;

            $YTDRealEstateSales = $this->realEstateSales($ytdStartDate, $end, $rec_id);

            $YTDRealEstateCashPlanSales = $this->realEstateCashPlanSales($ytdStartDate, $end, $rec_id);

            $YTDActualPayments = $this->realEstateActualPayments($ytdStartDate, $end, $rec_id);

            $YTDRetention = $YTDNetFlow + $YTDRealEstateCashPlanSales;

            $userYtdTargets = $user->YTDTargets;

            $ytdPercentage = percentage($YTDRetention, $userYtdTargets, 1);

            $ytdPercentageRetention = percentage($YTDNetFlow, $YTDInflows, 1);
        } else {
            $YTDInflows = $YTDWithdrawals = $YTDNetFlow = $YTDRealEstateSales = $YTDRealEstateCashPlanSales = 0;

            $YTDActualPayments = $YTDRetention = $userYtdTargets = $ytdPercentage = $ytdPercentageRetention = 0;
        }

        if ($cumulativeStartDate || $faData == 0) {
            $cumulativeInflows = $this->getCumulativeInflows($cumulativeStartDate, $end, $rec_id);

            $cumulativeWithdrawals = $unitData == 1 ?
                $this->getWithdrawalsWithoutEndDate($cumulativeStartDate, $end, $rec_id) :
                $this->getWithdrawals($cumulativeStartDate, $end, $rec_id);

            $cumulativeNetFlow = $cumulativeInflows - $cumulativeWithdrawals;

            $cumulativeRealEstateSales = $this->realEstateSales($cumulativeStartDate, $end, $rec_id);

            $cumulativeRealEstateCashPlanSales = $this->realEstateCashPlanSales($cumulativeStartDate, $end, $rec_id);

            $cumulativeCashFlows = $this->realEstateActualPayments($cumulativeStartDate, $end, $rec_id);

            $cumulativeRetention = $cumulativeNetFlow + $cumulativeRealEstateCashPlanSales;

            $userCumTargets = $user->cumulativeTargets;

            $cumPercentage = percentage($cumulativeRetention, $user->cumulativeTargets, 1);

            $cumulativePercentageRetention = percentage($cumulativeNetFlow, $cumulativeInflows, 1);
        } else {
            $cumulativeInflows = $cumulativeWithdrawals = $cumulativeNetFlow = $cumulativeRealEstateSales = 0;

            $cumulativeCashFlows = $cumulativeRetention = $userCumTargets = $cumPercentage = 0;

            $cumulativePercentageRetention = $cumulativeRealEstateCashPlanSales = 0;
        }

        $cumulativeRetentionBeforeDemotion = 0;

        if ($employmentDate && $cumulativeStartDate && $employmentDate != $cumulativeStartDate) {
            $cumulativeInflowsBeforeDemotion = $this->getCumulativeInflows(
                $employmentDate,
                $cumulativeStartDate,
                $rec_id
            );

            $cumulativeWithdrawalsBeforeDemotion = $this->getWithdrawals(
                $employmentDate,
                $cumulativeStartDate,
                $rec_id
            );

            $cumulativeNetFlowBeforeDemotion =
                $cumulativeInflowsBeforeDemotion - $cumulativeWithdrawalsBeforeDemotion;

            $cumulativeRealEstateSalesBeforeDemotion = $this->realEstateCashPlanSales(
                $employmentDate,
                $cumulativeStartDate,
                $rec_id
            );

            $cumulativeRetentionBeforeDemotion =
                $cumulativeNetFlowBeforeDemotion + $cumulativeRealEstateSalesBeforeDemotion;
        }

        return [
            'investments' => [
                'inflow' => $YTDInflows,
                'cumulativeInflows' => $cumulativeInflows,
                'outflow' => $YTDWithdrawals,
                'cumulativeOutflows' => $cumulativeWithdrawals,
                'netflow' => $YTDNetFlow,
                'cumulativeNetflow' => $cumulativeNetFlow
            ],
            'real_estate' => [
                'YTDRealEstateSales' => $YTDRealEstateSales,
                'YTDRealEstateCashPlanSales' => $YTDRealEstateCashPlanSales,
                'YTDActualPayments' => $YTDActualPayments,
                'cumulativeRealEstateSales' => $cumulativeRealEstateSales,
                'cumulativeRealEstateCashPlanSales' => $cumulativeRealEstateCashPlanSales,
                'cumulativeCashFlows' => $cumulativeCashFlows
            ],
            'retention' => [
                'YTDRetention' => $YTDRetention,
                'cumulativeRetention' => $cumulativeRetention,
                'YTDTargets' => $userYtdTargets,
                'cumulativeTargets' => $userCumTargets,
                'productionBeforeDemotion' => $cumulativeRetentionBeforeDemotion,
                'YTDPercentageRetention' => $ytdPercentageRetention,
                'cumulativePercentageRetention' => $cumulativePercentageRetention,
                'YTDPercentage' => $ytdPercentage,
                'cumulativePercentage' => $cumPercentage
            ]
        ];
    }

    private function prepareUserData($user, $userData)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'unit' => $user->unit,
            'unit_manager' => $user->unit_manager,
            'branch' => $user->branch,
            'days' => $user->days,
            'value_days' => $user->value_days,
            'title' => $user->title,
            'fa_title' => $user->fa_title,
            'employmentStatus' => $user->employmentStatus,
            'suspended_days' => $user->suspended_days,
            'currentFaPositionDays' => $user->currentFaPositionDays,
            'position_status' => $user->position_status,
            'currentPositionDays' => $user->currentPositionDays,
            'ytd_start_date' => $user->ytd_start_date,
            'cumulative_start_date' => $user->cumulative_start_date,
            'targets_start_date' => $user->targets_start_date,
        ] + $userData;
    }

    /**
     * @param $consolidatedData
     * @param $branchName
     * @param $unitName
     * @param $userData
     * @param int $unitManager
     * @return mixed
     */
    private function updateUnitData(
        $consolidatedData,
        $branchName,
        $unitName,
        $userData,
        $unitManager = 0
    ) {
        if (isset($consolidatedData[$branchName][$unitName]['unit_data'])) {
            $consolidatedData[$branchName][$unitName]['unit_data'] =
                $this->cumulateUnitData($consolidatedData, $branchName, $unitName, $userData, 'unit_data');

            if ($unitManager != 1) {
                $consolidatedData[$branchName][$unitName]['no_um_unit_data'] =
                    $this->cumulateUnitData(
                        $consolidatedData,
                        $branchName,
                        $unitName,
                        $userData,
                        'no_um_unit_data'
                    );
            }
        } else {
            $consolidatedData[$branchName][$unitName]['unit_data'] = $this->prepareNewUnitData($userData);

            if ($unitManager != 1) {
                $consolidatedData[$branchName][$unitName]['no_um_unit_data'] = $this->prepareNewUnitData($userData);
            } else {
                $cleanUserData = $this->getCumulativeDataOnly($this->getYtdDataOnly([]));

                $consolidatedData[$branchName][$unitName]['no_um_unit_data'] = $this->prepareNewUnitData($cleanUserData);
            }
        }

        return $consolidatedData;
    }

    /**
     * @param $userData
     * @return mixed
     */
    private function prepareNewUnitData($userData)
    {
        $userData['retention']['YTDPercentageRetention'] = '';

        $userData['retention']['cumulativePercentageRetention'] = '';

        $userData['retention']['YTDPercentage'] = '';

        $userData['retention']['cumulativePercentage'] = '';

        $userData['retention']['productionBeforeDemotion'] = '';

        return $userData;
    }

    /**
     * @param $consolidatedData
     * @param $branchName
     * @param $unitName
     * @param $userData
     * @param $dataType
     * @return array
     */
    private function cumulateUnitData($consolidatedData, $branchName, $unitName, $userData, $dataType)
    {
        return [
            'investments' => [
                'inflow' => $userData['investments']['inflow'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['investments']['inflow'],
                'cumulativeInflows' => $userData['investments']['cumulativeInflows'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['investments']['cumulativeInflows'],
                'outflow' => $userData['investments']['outflow'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['investments']['outflow'],
                'cumulativeOutflows' => $userData['investments']['cumulativeOutflows'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['investments']['cumulativeOutflows'],
                'netflow' => $userData['investments']['netflow'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['investments']['netflow'],
                'cumulativeNetflow' => $userData['investments']['cumulativeNetflow'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['investments']['cumulativeNetflow']
            ],
            'real_estate' => [
                'YTDRealEstateSales' => $userData['real_estate']['YTDRealEstateSales'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['real_estate']['YTDRealEstateSales'],
                'YTDRealEstateCashPlanSales' => $userData['real_estate']['YTDRealEstateCashPlanSales'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['real_estate']['YTDRealEstateCashPlanSales'],
                'YTDActualPayments' => $userData['real_estate']['YTDActualPayments'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['real_estate']['YTDActualPayments'],
                'cumulativeRealEstateSales' => $userData['real_estate']['cumulativeRealEstateSales'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['real_estate']['cumulativeRealEstateSales'],
                'cumulativeRealEstateCashPlanSales' => $userData['real_estate']['cumulativeRealEstateCashPlanSales'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['real_estate']['cumulativeRealEstateCashPlanSales'],
                'cumulativeCashFlows' => $userData['real_estate']['cumulativeCashFlows'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['real_estate']['cumulativeCashFlows']
            ],
            'retention' => [
                'YTDRetention' => $userData['retention']['YTDRetention'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['retention']['YTDRetention'],
                'cumulativeRetention' => $userData['retention']['cumulativeRetention'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['retention']['cumulativeRetention'],
                'productionBeforeDemotion' => '',
                'YTDTargets' => $userData['retention']['YTDTargets'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['retention']['YTDTargets'],
                'cumulativeTargets' => $userData['retention']['cumulativeTargets'] +
                    $consolidatedData[$branchName][$unitName][$dataType]['retention']['cumulativeTargets'],
                'YTDPercentageRetention' => '',
                'cumulativePercentageRetention' => '',
                'YTDPercentage' => '',
                'cumulativePercentage' => ''
            ]
        ];
    }

    //This is not being used but leave it as is just in case
    private function processDepartmentalData($user, $rec_id, $consolidatedData, $userData)
    {
//        $userDataCopy = $userData;

//        $cumulativePositions = $user->department_positions->cumulative;
//
//        $ytdPositions = $user->department_positions->ytd;
//
//        $departmentCumulativeKeys = array_keys((array) $cumulativePositions);
//
//        if (count($departmentCumulativeKeys) == 1) {
//            $consolidatedData = $this->updateUnitData(
//                $consolidatedData,
//                $cumulativePositions->{$departmentCumulativeKeys[0]}->branch,
//                $departmentCumulativeKeys[0],
//                $userDataCopy
//            );
//        } elseif (count($departmentCumulativeKeys) > 1) {
//            $userCopy = (object) (array) $user;
//
//            foreach ($cumulativePositions as $cumulativePosition) {
//                $dates = $cumulativePosition->dates;
//
//                $userCopy->cumulativeTargets = $cumulativePosition->net_target;
//
//                foreach ($dates as $date) {
//                    $userData = $this->getUserEvaluationData(
//                        $userCopy,
//                        $rec_id,
//                        null,
//                        Carbon::parse($date->start_date),
//                        Carbon::parse($date->end_date),
//                        null,
//                        1
//                    );
//
//                    $consolidatedData = $this->updateUnitData(
//                        $consolidatedData,
//                        $cumulativePosition->branch,
//                        $cumulativePosition->unit,
//                        $userData
//                    );
//                }
//            }
//
//            foreach ($ytdPositions as $ytdPosition) {
//                $dates = $ytdPosition->dates;
//
//                $userCopy->YTDTargets = $ytdPosition->net_target;
//
//                foreach ($dates as $date) {
//                    $userData = $this->getUserEvaluationData(
//                        $userCopy,
//                        $rec_id,
//                        Carbon::parse($date->start_date),
//                        null,
//                        Carbon::parse($date->end_date),
//                        null,
//                        1
//                    );
//
//                    $consolidatedData = $this->updateUnitData(
//                        $consolidatedData,
//                        $ytdPosition->branch,
//                        $ytdPosition->unit,
//                        $userData
//                    );
//                }
//            }
//        } else {
//            $consolidatedData = $this->updateUnitData($consolidatedData, $user->branch, $user->unit, $userDataCopy);
//        }

//        return $consolidatedData;
    }

    /**
     * @param $userData
     * @return mixed
     */
    private function getCumulativeDataOnly($userData)
    {
        $userData['investments']['inflow'] = 0;

        $userData['investments']['outflow'] = 0;

        $userData['investments']['netflow'] = 0;

        $userData['real_estate']['YTDRealEstateSales'] = 0;

        $userData['real_estate']['YTDRealEstateCashPlanSales'] = 0;

        $userData['real_estate']['YTDActualPayments'] = 0;

        $userData['retention']['YTDRetention'] = 0;

        $userData['retention']['YTDTargets'] = 0;

        $userData['retention']['YTDPercentageRetention'] = 0;

        $userData['retention']['YTDPercentage'] = 0;

        return $userData;
    }

    /**
     * @param $userData
     * @return mixed
     */
    private function getYtdDataOnly($userData)
    {
        $userData['investments']['cumulativeInflows'] = 0;

        $userData['investments']['cumulativeOutflows'] = 0;

        $userData['investments']['cumulativeNetflow'] = 0;

        $userData['real_estate']['cumulativeRealEstateSales'] = 0;

        $userData['real_estate']['cumulativeRealEstateCashPlanSales'] = 0;

        $userData['real_estate']['cumulativeCashFlows'] = 0;

        $userData['retention']['cumulativeRetention'] = 0;

        $userData['retention']['cumulativeTargets'] = 0;

        $userData['retention']['productionBeforeDemotion'] = 0;

        $userData['retention']['cumulativePercentageRetention'] = 0;

        $userData['retention']['cumulativePercentage'] = 0;

        return $userData;
    }

    /*
    * Get the commission recipients that fall in the specified recipient type
    */
    /**
     * @param Carbon|null $start
     * @param Carbon $end
     * @param $type
     * @return CommissionRecepient[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getProductionUsers(Carbon $start = null, Carbon $end = null, $type = null)
    {
        return CommissionRecepient::where('recipient_type_id', $type)->whereHas(
            'commissions',
            function ($q) use ($start, $end) {
                $q->whereHas(
                    'investment',
                    function ($q) use ($start, $end) {
                        $q->where('invested_date', '<=', $end);

                        if ($start) {
                            $q->where('invested_date', '>=', $start);
                        }
                    }
                );
            }
        )->get();
    }
}
