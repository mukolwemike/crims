<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\FaEvaluationDataTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Support\Mails\Mailer;

class IndividualFAEvaluationSummaryExport
{
    /*
     * Get the FA generator trait
     */
    use RealEstateSalesTrait,
        InFlowsTrait,
        WithdrawalsTrait,
        FaEvaluationDataTrait;

    /*
     * Handle the incoming request
     */
    public function handle($data)
    {
        $user = json_decode($data[0]);

        $evaluationData = $this->userEvaluationData($user, $data[3]);

        $evaluationData['receiver_firstname'] = $data[2];

        $evaluationData['report_start_date'] = $data[4];

        $evaluationData['report_end_date'] = $data[3];

        Mailer::compose()
            ->from([
                'address' => 'crmsystem@cytonn.com',
                'name' => 'Cytonn CRM'
            ])
            ->to([$data[1]])
            ->subject($user->name . '\'s Evaluation Report')
            ->view('emails.reports.individual_fa_evaluation_report', ['data' => $evaluationData])
            ->send();
    }
}
