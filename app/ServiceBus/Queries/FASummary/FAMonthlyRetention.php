<?php
/**
 * Created by PhpStorm.
 * User: mac-intern
 * Date: 8/2/17
 * Time: 4:36 PM
 */

namespace ServiceBus\Queries\FASummary;

use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class FAMonthlyRetention
{
    /*
     * Use the traits
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;


    public function handle($data)
    {
        $users = \GuzzleHttp\json_decode($data[0]);

        return $this->generateReport($users);
    }

    /*
     * Genereate the FA Monthly Retention report
     */
    public function generateReport($users)
    {
        $monthlyRetentionData = array();

        foreach ($users as $user) {
            $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

            $monthlyRetentionData[] = [
                'id' => $user->id,
                'name' => $user->name,
                'unit' => $user->unit,
                'days' => $user->days,
                'data' => $this->getMonthlyRetentionData($rec_id, $user->monthlyTargets)
            ];
        }

        return $monthlyRetentionData;
    }

    /*
     * Get the monthly retention data for the users
     */
    public function getMonthlyRetentionData($rec_id, $monthlyTargets)
    {
        $monthlyRetention = array();

        foreach ($monthlyTargets as $key => $monthTarget) {
            if (! is_null($rec_id)) {
                $startDate = Carbon::parse($monthTarget->start_date)->startOfDay();

                $endDate = Carbon::parse($monthTarget->end_date)->endOfDay();

                $inflows = $this->getCumulativeInflows($startDate, $endDate, $rec_id);

                $realEstateInflows = $this->realEstateActualPayments($startDate, $endDate, $rec_id);

                $withdrawals = $this->getWithdrawals($startDate, $endDate, $rec_id);

                $netFlow = ($inflows + $realEstateInflows) - $withdrawals;

                $target = $monthTarget->targets;

                $monthlyRetention[$monthTarget->month_date] = [
                    'inflows' => $inflows + $realEstateInflows,
                    'withdrawals' => $withdrawals,
                    'netFlow' => $netFlow,
                    'target' => $target,
                    'variance' => $netFlow - $target
                ];
            } else {
                $monthlyRetention[$monthTarget->month_date] = [
                    'inflows' => 0,
                    'withdrawals' => 0,
                    'netFlow' => 0,
                    'target' => 0,
                    'variance' => 0
                ];
            }
        }

        return $monthlyRetention;
    }
}
