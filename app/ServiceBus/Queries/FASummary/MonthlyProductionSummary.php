<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\FASummary;

use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class MonthlyProductionSummary
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    /*
     * Handle the fa evaluation summary
     */
    public function handle($data)
    {
        $users = $data[0];

        $endYear = $data[1];

        return $this->generateReport($users, $endYear);
    }

    /*
     * Generate the fa monthly production report
     */
    public function generateReport($data, $end)
    {
        $data = json_decode($data);

        $end = Carbon::parse($end)->endOfDay();

        $evaluationData = array();

        foreach ($data->data as $user) {
            $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

            $startDate = Carbon::parse($user->start_date);

            if ($rec_id) {
                $inflows = $this->getCumulativeInflows($startDate, $end, $rec_id);

                $withdrawals = $this->getWithdrawals($startDate, $end, $rec_id);

                $netFlow = $inflows - $withdrawals;

                $realEstateSales = $this->realEstateSales($startDate, $end, $rec_id);

                $retention = $netFlow + $realEstateSales;

                $evaluationData[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'title' => $user->title,
                    'netflow' => $netFlow,
                    'realEstateSales' =>$realEstateSales,
                    'target' => $user->target,
                    'percentage' => dividend($retention, $user->target)
                ];
            } else {
                $evaluationData[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'title' => $user->title,
                    'netflow' => 0,
                    'realEstateSales' => 0,
                    'target' => $user->target,
                    'percentage' => 0
                ];
            }
        }

        $data->data = $evaluationData;

        return $data;
    }
}
