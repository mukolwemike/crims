<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;

class RESales
{
    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getData()
    {
        return [
            'total' => [
                'product' => 'RE Sales',
                'amount' => $this->getReSales()
            ]
        ];
    }

    private function getReSales()
    {
        return UnitHolding::where('active', 1)
            ->columnBetween('created_at', $this->startDate, $this->endDate)
            ->get()
            ->sum(function (UnitHolding $holding) {
                return $holding->price();
            });
    }
}
