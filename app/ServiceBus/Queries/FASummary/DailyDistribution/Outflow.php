<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\Unitization\UnitFundPurchaseSale;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Investment\Action\Base;
use Cytonn\Models\ClientInvestmentWithdrawal;

/**
 * Class Outflow
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class Outflow
{
    /**
     * @var Carbon
     */
    protected $startDate;
    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * @var int
     */
    protected $outflowType;

    /**
     * Outflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param int $outflowType
     */
    public function __construct(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->outflowType = $outflowType;
    }

    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function getData()
    {
        $dataArray = $this->getInvestmentsOutflows();

        $dataArray[] = $this->getShareOutflows();

        $dataArray[] = $this->getUnitFundOutflows();

        $data = collect($dataArray);

        $total = $data->sum('amount');

        $data = $data->map(function ($item) use ($total) {
            $item['percentage'] = percentage($item['amount'], $total, 2);

            return $item;
        });

        $data->put('total', [
            'product' => 'Total',
            'amount' => $total,
            'percentage' => 100
        ]);

        return $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getFundManagers()
    {
        return FundManager::whereIn('id', [1, 2, 3])->get();
    }

    /**
     * @return static
     */
    private function getInvestmentsOutflows()
    {
        return $this->getFundManagers()->map(function ($fundManager) {
            return [
                'product' => $fundManager->name,
                'amount' => $this->getInvestmentOutflowTotal($fundManager),
            ];
        });
    }

    /**
     * @param FundManager $fundManager
     * @return mixed
     */
    private function getInvestmentOutflowTotal(FundManager $fundManager)
    {
        return $this->outflowType == 1 ? $this->getWithdrawals($fundManager) :
            $this->getInvestmentOutflowPayments($fundManager);
    }

    /**
     * @param FundManager $fundManager
     * @return mixed
     */
    private function getWithdrawals(FundManager $fundManager)
    {
        return ClientInvestmentWithdrawal::where('withdraw_type', '!=', 'interest')
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->ofType(Base::TYPE_SENT_TO_CLIENT)
            ->fundManager($fundManager)
            ->where('amount', '>', 0)
            ->get()
            ->sum(function (ClientInvestmentWithdrawal $withdrawal) {
                return convert(
                    $withdrawal->principalWithdrawn(),
                    $withdrawal->investment->product->currency,
                    $withdrawal->date
                );
            });
    }

    /**
     * @param FundManager $fundManager
     * @return mixed
     */
    private function getInvestmentOutflowPayments(FundManager $fundManager)
    {
        return ClientPayment::where('type_id', 2)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->whereHas('product', function ($q) use ($fundManager) {
                $q->where('fund_manager_id', $fundManager->id);
            })->get()
            ->sum(function (ClientPayment $clientPayment) {
                return convert(-($clientPayment->amount), $clientPayment->product->currency, $clientPayment->date);
            });
    }

    /**
     * @return array
     */
    public function getShareOutflows()
    {
        return [
            'product' => 'Shares',
            'amount' => $this->getShareOutflowTotal()
        ];
    }

    /**
     * @return mixed
     */
    private function getShareOutflowTotal()
    {
        return $this->outflowType == 1 ? $this->getShareSales() : $this->getShareOutflowPayments();
    }

    /**
     * @return mixed
     */
    private function getShareOutflowPayments()
    {
        return ClientPayment::where('type_id', 2)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->has('shareEntity')
            ->get()
            ->sum(function (ClientPayment $clientPayment) {
                return convert(-($clientPayment->amount), $clientPayment->shareEntity->currency, $clientPayment->date);
            });
    }

    /**
     * @return mixed
     */
    private function getShareSales()
    {
        return ShareSale::columnBetween('date', $this->startDate, $this->endDate)
            ->get()
            ->sum(function ($sale) {
                return ($sale->number * $sale->sale_price);
            });
    }

    /**
     * @return array
     */
    private function getUnitFundOutflows()
    {
        return [
            'product' => 'Unit Funds',
            'amount' => $this->getUnitFundOutflowTotals()
        ];
    }

    /**
     * @return mixed
     */
    private function getUnitFundOutflowTotals()
    {
        return $this->outflowType == 1 ? $this->getUnitFundSales() : $this->getUnitFundOutflowPayments();
    }

    /**
     * @return mixed
     */
    private function getUnitFundSales()
    {
        return UnitFundSale::columnBetween('date', $this->startDate, $this->endDate)
            ->get()
            ->sum(function (UnitFundSale $sale) {
                return $sale->repository()->getPurchaseValue();
            });
    }

    /**
     * @return mixed
     */
    private function getUnitFundOutflowPayments()
    {
        return ClientPayment::where('type_id', 2)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->has('fund')
            ->get()
            ->sum(function (ClientPayment $clientPayment) {
                return convert(-($clientPayment->amount), $clientPayment->fund->currency, $clientPayment->date);
            });
    }
}
