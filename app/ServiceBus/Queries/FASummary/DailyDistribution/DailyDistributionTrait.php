<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use ServiceBus\Queries\FASummary\WeeklyDistribution\REProductionSummary;
use ServiceBus\Queries\FASummary\WeeklyDistribution\UnitProductionSummary;

/**
 * Trait DailyDistributionTrait
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
trait DailyDistributionTrait
{
    /**
     * @param ClientPayment $clientPayment
     * @return string
     */
    public function getPaymentType(ClientPayment $clientPayment)
    {
        if ($clientPayment->product) {
            return $clientPayment->product->fundManager->name;
        } elseif ($clientPayment->shareEntity) {
            return 'Shares';
        } elseif ($clientPayment->fund) {
            return $clientPayment->fund->short_name;
        } else {
            return "Unknown";
        }
    }

    /**
     * @param ClientPayment $clientPayment
     * @return mixed
     */
    public function getPaymentCurrency(ClientPayment $clientPayment)
    {
        if ($clientPayment->shareEntity) {
            return $clientPayment->shareEntity->currency;
        } elseif ($clientPayment->fund) {
            return $clientPayment->fund->currency;
        } else {
            return $clientPayment->product->currency;
        }
    }

    /**
     * @param ClientPayment $payment
     * @return mixed
     */
    public function getPaymentValue(ClientPayment $payment)
    {
        $currency = $this->getPaymentCurrency($payment);

        return convert($payment->amount, $currency, $payment->date);
    }

    /**
     * @param ClientPayment $clientPayment
     * @return mixed|string
     */
    public function getClientName(ClientPayment $clientPayment)
    {
        return $clientPayment->client ? ClientPresenter::presentFullNames($clientPayment->client_id) :
            $clientPayment->description;
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return \Illuminate\Support\Collection|static
     */
    private function getInflowSummary(Carbon $startDate, Carbon $endDate)
    {
        $inflow = new Inflow($startDate, $endDate);

        return $inflow->getData();
    }


    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param int $outflowType
     * @return \Illuminate\Support\Collection|static
     */
    private function getOutflowSummary(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $outflow = new Outflow($startDate, $endDate, $outflowType);

        return $outflow->getData();
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getReCashFlowSummary(Carbon $startDate, Carbon $endDate)
    {
        $re = new ReCashFlow($startDate, $endDate);

        return $re->getData();
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getReSalesSumary(Carbon $startDate, Carbon $endDate)
    {
        $re = new RESales($startDate, $endDate);

        return $re->getData();
    }

    /**
     * @param $dataArray
     * @return array
     */
    private function getNetflowSummary($dataArray)
    {
        $totalInflow = $dataArray['inflows_summary']['total']['amount'];

        $totalOutflow = $dataArray['outflows_summary']['total']['amount'];

        $reCashflow = $dataArray['re_cashflow_summary']['total']['amount'];

        $reSales = $dataArray['re_sales_summary']['total']['amount'];

        return [
            'inflow' => [
                'product' => 'Inflows',
                'amount' => $totalInflow
            ],
            'outflows' => [
                'product' => 'Outflows',
                'amount' => $totalOutflow
            ],
            'netflow' => [
                'product' => 'Netflow',
                'amount' => $totalInflow - $totalOutflow
            ],
            're_cashflow' => [
                'product' => 'RE Cashflow',
                'amount' => $reCashflow
            ],
            're_sales' => [
                'product' => 'RE Sales',
                'amount' => $reSales
            ],
            'total_netflow' => [
                'product' => 'Total Netflow',
                'amount' => $totalInflow + $reCashflow - $totalOutflow
            ]
        ];
    }

    /**
     * @param Carbon $startDate
     * @param int $outflowType
     * @return array
     */
    private function getWeekToDateSummary(Carbon $startDate, $outflowType = 1)
    {
        $startDate = $startDate->copy()->startOfWeek();

        $endDate = $startDate->copy()->endOfWeek();

        $weekArray = array();

        $weekArray['inflows_summary'] = $this->getInflowSummary($startDate->copy(), $endDate->copy());

        $weekArray['outflows_summary'] = $this->getOutflowSummary($startDate->copy(), $endDate->copy(), $outflowType);

        $weekArray['re_cashflow_summary'] = $this->getReCashFlowSummary($startDate->copy(), $endDate->copy());

        $weekArray['re_sales_summary'] = $this->getReSalesSumary($startDate->copy(), $endDate->copy());

        return $this->getNetflowSummary($weekArray);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return \Illuminate\Support\Collection
     */
    private function getREProductionSummary(Carbon $startDate, Carbon $endDate)
    {
        $re = new REProductionSummary($startDate, $endDate);

        return $re->getData();
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param int $outflowType
     * @return \Illuminate\Support\Collection|static
     */
    private function getUnitProductionSummary(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $unit = new UnitProductionSummary($startDate, $endDate, $outflowType);

        return $unit->getData();
    }

    /**
     * @param UnitHolding $holding
     * @return null
     */
    private function getHoldingCommissionRecipient(UnitHolding $holding)
    {
        $recipient = null;

        if ($holding->commission) {
            $recipient = $holding->commission->recipient;
        }

        if (is_null($recipient) && $holding->client) {
            $recipient = $holding->client->getLatestFa();
        }

        return $recipient;
    }
}
