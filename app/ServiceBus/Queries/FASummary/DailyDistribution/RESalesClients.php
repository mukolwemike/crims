<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;

/**
 * Class RESalesClients
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class RESalesClients
{
    use DailyDistributionTrait;

    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->processReClients();
    }

    /**
     * @return array|mixed
     */
    private function processReClients()
    {
        $reArray = array();

        $reClients = $this->getReClients();

        foreach ($reClients as $holding) {
            $data = $this->presentHolding($holding);

            $reArray[$data['branch']][] = $data;
        }

        $reArray = $this->presentBranchTotals($reArray);

        return $reArray;
    }

    /**
     * @param $dataArray
     * @return mixed
     */
    private function presentBranchTotals($dataArray)
    {
        $grandTotal = 0;

        foreach ($dataArray as $key => $branchInflow) {
            $branchCol = collect($branchInflow);

            $total = $branchCol->sum('product_value');

            $dataArray[$key]['total'] = [
                'date' => 'Total',
                'branch' => $key,
                'client' => '',
                'fa' => '',
                'product' => '',
                'payment_plan' => '',
                'unit' => '',
                'product_value' => $total,
                'reservation_date' => ''
            ];

            $grandTotal += $total;
        }

        $dataArray['Total RE Sales']['total'] = [
            'date' => 'Total RE Sales',
            'branch' => 'Total RE Sales',
            'client' => '',
            'fa' => '',
            'product' => '',
            'payment_plan' => '',
            'unit' => '',
            'product_value' => $grandTotal,
            'reservation_date' => ''
        ];

        return $dataArray;
    }

    /**
     * @param UnitHolding $holding
     * @return array
     */
    private function presentHolding(UnitHolding $holding)
    {
        $data = array();

        $recipient = $this->getHoldingCommissionRecipient($holding);

        $data['date'] = Carbon::parse($holding->created_at)->toDateString();

        $data['branch'] = ($recipient) ? $recipient->present()->getBranch : 'Unknown';

        $data['client'] = ClientPresenter::presentFullNames($holding->client_id);

        $data['fa'] = ($recipient) ? $recipient->name : '';

        $unit = $holding->unit;

        $data['product'] = $unit->project->name . ' ' . $unit->present()->unitSizeName;

        $data['payment_plan'] = ($holding->paymentPlan) ? $holding->paymentPlan->name : '';

        $data['unit'] = ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown';

        $data['product_value'] = $holding->price();

        $data['reservation_date'] =
            $holding->reservation_date ? Carbon::parse($holding->reservation_date)->toDateString() : '';

        return $data;
    }

    /**
     * @return mixed
     */
    private function getReClients()
    {
        return UnitHolding::columnBetween('reservation_date', $this->startDate, $this->endDate)
            ->where('active', 1)
            ->get();
    }
}
