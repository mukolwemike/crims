<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use Carbon\Carbon;

/**
 * Class DailyDistributionReport
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class DailyDistributionReport
{
    use DailyDistributionTrait;

    /**
     * @param $data
     * @return array
     */
    public function handle($data)
    {
        $startDate = Carbon::parse($data[0])->startOfDay();

        $endDate = Carbon::parse($data[1])->endOfDay();

        $outflowType = $data[2];

        return $this->getReport($startDate, $endDate, $outflowType);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    public function getReport(Carbon $startDate, Carbon $endDate, $outflowType)
    {
        $dataArray = array();

        $dataArray['inflows_summary'] = $this->getInflowSummary($startDate->copy(), $endDate->copy());

        $dataArray['outflows_summary'] = $this->getOutflowSummary($startDate->copy(), $endDate->copy(), $outflowType);

        $dataArray['re_cashflow_summary'] = $this->getReCashFlowSummary($startDate->copy(), $endDate->copy());

        $dataArray['re_sales_summary'] = $this->getReSalesSumary($startDate->copy(), $endDate->copy());

        $dataArray['netflow_summary'] = $this->getNetflowSummary($dataArray);

        $dataArray['wtd_summary'] = $this->getWeekToDateSummary($startDate->copy(), $outflowType);

        $dataArray['unit_summary'] = $this->getUnitSummary($startDate->copy(), $endDate->copy(), $outflowType);

        $dataArray['inflow_clients'] = $this->getInflowClients($startDate->copy(), $endDate->copy());

        $dataArray['outflow_clients'] = $this->getOutflowClients($startDate->copy(), $endDate->copy(), $outflowType);

        $dataArray['re_clients'] = $this->getReClients($startDate->copy(), $endDate->copy());

        $dataArray['re_sales_clients'] = $this->getReSalesClients($startDate->copy(), $endDate->copy());

        return $dataArray;
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return \Illuminate\Support\Collection|static
     */
    private function getUnitSummary(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $unit = new UnitSummary($startDate, $endDate, $outflowType);

        return $unit->getData();
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getInflowClients(Carbon $startDate, Carbon $endDate)
    {
        $inflow = new InflowClients($startDate, $endDate);

        return $inflow->getData();
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getOutflowClients(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $outflow = new OutflowClients($startDate, $endDate, $outflowType);

        return $outflow->getData();
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getReClients(Carbon $startDate, Carbon $endDate)
    {
        $re = new REClients($startDate, $endDate);

        return $re->getData();
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getReSalesClients(Carbon $startDate, Carbon $endDate)
    {
        $re = new RESalesClients($startDate, $endDate);

        return $re->getData();
    }
}
