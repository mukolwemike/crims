<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Carbon\Carbon;
use Cytonn\Investment\Action\Base;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;

/**
 * Class OutflowClients
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class OutflowClients
{
    use DailyDistributionTrait;

    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * @var int
     */
    protected $outflowType;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->outflowType = $outflowType;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->processOutflows();
    }

    /**
     * @return array|mixed
     */
    private function processOutflows()
    {
        $outflowArray = $this->outflowType == 1 ? $this->processWithdrawalOutflows() : $this->processPaymentOutflows();

        $outflowArray = $this->presentBranchTotals($outflowArray);

        return $outflowArray;
    }

    /**
     * @return array
     */
    private function processWithdrawalOutflows()
    {
        $outflowArray = array();

        $outflows = $this->getWithdrawalOutflows();

        foreach ($outflows as $withdrawal) {
            $data = $this->presentWithdrawal($withdrawal);

            $outflowArray[$data['branch']][] = $data;
        }

        $shareSales = $this->getShareOutflows();

        foreach ($shareSales as $shareSale) {
            $data = $this->presentShareSale($shareSale);

            $outflowArray[$data['branch']][] = $data;
        }

        $unitFundSales = $this->getUnitFundOutflows();

        foreach ($unitFundSales as $unitFundSale) {
            $data = $this->presentUnitFundSale($unitFundSale);

            $outflowArray[$data['branch']][] = $data;
        }

        return $outflowArray;
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @return array
     */
    private function presentWithdrawal(ClientInvestmentWithdrawal $withdrawal)
    {
        $data = array();

        $recipient = $withdrawal->investment->commission->recipient;

        $data['date'] = Carbon::parse($withdrawal->date)->toDateString();

        $data['branch'] = $recipient ? $recipient->present()->getBranch : 'Unknown';

        $data['client'] = ClientPresenter::presentFullNames($withdrawal->investment->client_id);

        $data['fa'] = $recipient ? $recipient->name : '';

        $data['status'] = $recipient ? $recipient->present()->getActive : '';

        $product = $withdrawal->investment->product;

        $data['type'] = $product->fundManager->name;

        $data['amount'] = convert($withdrawal->principalWithdrawn(), $product->currency, $withdrawal->date);

        $data['unit'] = ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown';

        return $data;
    }

    /**
     * @return mixed
     */
    private function getWithdrawalOutflows()
    {
        return ClientInvestmentWithdrawal::where('withdraw_type', '!=', 'interest')
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->ofType(Base::TYPE_SENT_TO_CLIENT)
            ->where('amount', '>', 0)
            ->get();
    }

    /**
     * @return mixed
     */
    private function getShareOutflows()
    {
        return ShareSale::columnBetween('date', $this->startDate, $this->endDate)
            ->has('payment')
            ->get();
    }

    /**
     * @param ShareSale $shareSale
     * @return array
     */
    private function presentShareSale(ShareSale $shareSale)
    {
        $data = array();

        $payment = $shareSale->payment;

        $recipient = $payment->present()->getCommissionRecipient;

        $data['date'] = Carbon::parse($payment->date)->toDateString();

        $data['branch'] = $recipient ? $recipient->present()->getBranch : 'Unknown';

        $data['client'] = $this->getClientName($payment);

        $data['fa'] = $recipient ? $recipient->name : '';

        $data['status'] = $recipient ? $recipient->present()->getActive : '';

        $data['type'] = $this->getPaymentType($payment);

        $data['amount'] = $this->getPaymentValue($payment);

        $data['unit'] = $recipient ? $recipient->present()->getDepartmentUnit : 'Unknown';

        return $data;
    }

    /**
     * @return mixed
     */
    private function getUnitFundOutflows()
    {
        return UnitFundSale::columnBetween('date', $this->startDate, $this->endDate)
            ->has('payment')
            ->get();
    }

    /**
     * @param UnitFundSale $unitFundSale
     * @return array
     */
    private function presentUnitFundSale(UnitFundSale $unitFundSale)
    {
        $data = array();

        $payment = $unitFundSale->payment;

        $recipient = $payment->present()->getCommissionRecipient('units');

        $data['date'] = Carbon::parse($payment->date)->toDateString();

        $data['branch'] = $recipient ? $recipient->present()->getBranch : 'Unknown';

        $data['client'] = $this->getClientName($payment);

        $data['fa'] = $recipient ? $recipient->name : '';

        $data['status'] = $recipient ? $recipient->present()->getActive : '';

        $data['type'] = $this->getPaymentType($payment);

        $data['amount'] = convert(
            $unitFundSale->repository()->getPurchaseValue(),
            $unitFundSale->fund->currency,
            $unitFundSale->date
        );

        $data['unit'] = $recipient ? $recipient->present()->getDepartmentUnit : 'Unknown';

        return $data;
    }

    /**
     * @return array
     */
    private function processPaymentOutflows()
    {
        $outflowArray = array();

        $outflows = $this->getClientPaymentOutflows();

        foreach ($outflows as $payment) {
            $data = $this->presentPaymentOutflows($payment);

            $outflowArray[$data['branch']][] = $data;
        }

        return $outflowArray;
    }

    /**
     * @param ClientPayment $payment
     * @return array
     */
    private function presentPaymentOutflows(ClientPayment $payment)
    {
        $data = array();

        $recipient = $payment->present()->getCommissionRecipient;

        $data['date'] = Carbon::parse($payment->date)->toDateString();

        $data['branch'] = $recipient ? $recipient->present()->getBranch : 'Unknown';

        $data['client'] = $this->getClientName($payment);

        $data['fa'] = $recipient ? $recipient->name : '';

        $data['status'] = $recipient ? $recipient->present()->getActive : '';

        $data['type'] = $this->getPaymentType($payment);

        $data['amount'] = - ($this->getPaymentValue($payment));

        $data['unit'] = $recipient ? $recipient->present()->getDepartmentUnit : 'Unknown';

        return $data;
    }

    /**
     * @return mixed
     */
    private function getClientPaymentOutflows()
    {
        return ClientPayment::where('type_id', 2)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->where(function ($q) {
                $q->whereNotNull('product_id')->orWhereNotNull('share_entity_id')->orWhereNotNull('unit_fund_id');
            })
            ->get();
    }

    /**
     * @param $dataArray
     * @return mixed
     */
    private function presentBranchTotals($dataArray)
    {
        $grandTotal = 0;

        foreach ($dataArray as $key => $branchInflow) {
            $branchCol = collect($branchInflow);

            $total = $branchCol->sum('amount');

            $dataArray[$key]['total'] = [
                'date' => 'Total',
                'branch' => $key,
                'client' => '',
                'fa' => '',
                'status' => '',
                'type' => '',
                'amount' => $total,
                'unit' => ''
            ];

            $grandTotal += $total;
        }

        $dataArray['Total Outflows']['total'] = [
            'date' => 'Total Outflows',
            'branch' => 'Total Outflows',
            'client' => '',
            'fa' => '',
            'status' => '',
            'type' => '',
            'amount' => $grandTotal,
            'unit' => ''
        ];

        return $dataArray;
    }
}
