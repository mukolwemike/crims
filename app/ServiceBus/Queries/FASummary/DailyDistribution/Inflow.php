<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\SharePurchases;
use Carbon\Carbon;

/**
 * Class Inflow
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class Inflow
{
    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function getData()
    {
        $dataArray = $this->getInvestmentsInflows();

        $dataArray[] = $this->getShareInflows();

        $dataArray[] = $this->getUnitInflows();

        $data = collect($dataArray);

        $total = $data->sum('amount');

        $data = $data->map(function ($item) use ($total) {
            $item['percentage'] = percentage($item['amount'], $total, 2);

            return $item;
        });

        $data->put('total', [
            'product' => 'Total',
            'amount' => $total,
            'percentage' => 100,
        ]);

        return $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getFundManagers()
    {
        return FundManager::whereIn('id', [1, 2, 3])->get();
    }

    /**
     * @return static
     */
    public function getInvestmentsInflows()
    {
        return $this->getFundManagers()->map(function ($fundManager) {
            return [
                'product' => $fundManager->name,
                'amount' => $this->getPaymentsInflows($fundManager),
            ];
        });
    }

    /**
     * @param FundManager $fundManager
     * @return mixed
     */
    public function getPaymentsInflows(FundManager $fundManager)
    {
        return ClientPayment::where('type_id', 1)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->whereHas('product', function ($q) use ($fundManager) {
                $q->where('fund_manager_id', $fundManager->id);
            })->get()
            ->sum(function (ClientPayment $clientPayment) {
                return convert($clientPayment->amount, $clientPayment->product->currency, $clientPayment->date);
            });
    }

    /**
     * @return array
     */
    public function getShareInflows()
    {
        return [
            'product' => 'Shares',
            'amount' => $this->getSharePayments(),
        ];
    }

    /**
     * @return mixed
     */
    private function getSharePayments()
    {
        return ClientPayment::where('type_id', 1)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->has('shareEntity')
            ->get()
            ->sum(function (ClientPayment $clientPayment) {
                return convert($clientPayment->amount, $clientPayment->shareEntity->currency, $clientPayment->date);
            });
    }

    /**
     * @return array
     */
    public function getUnitInflows()
    {
        return [
            'product' => 'Unit Funds',
            'amount' => $this->getUnitPayments(),
        ];
    }

    /**
     * @return mixed
     */
    private function getUnitPayments()
    {
        return ClientPayment::where('type_id', 1)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->has('fund')
            ->get()
            ->sum(function (ClientPayment $clientPayment) {
                return convert($clientPayment->amount, $clientPayment->fund->currency, $clientPayment->date);
            });
    }
}
