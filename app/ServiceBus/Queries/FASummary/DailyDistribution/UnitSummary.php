<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use Carbon\Carbon;

/**
 * Class UnitSummary
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class UnitSummary
{
    use UnitSummaryTrait;

    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    protected $outflowType;

    public function __construct(Carbon $startDate, Carbon $endDate, $outflowType = 1)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->outflowType = $outflowType;
    }

    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function getData()
    {
        return $this->processUnitSummary();
    }

    /**
     * @return \Illuminate\Support\Collection|UnitSummary
     */
    private function processUnitSummary()
    {
        $unitInflows = $this->processInflows();

        $unitOutflows = $this->processOutflows();

        return $this->getDepartmentUnitsData($unitInflows, $unitOutflows);
    }

    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function getDepartmentUnitsData($unitInflows, $unitOutflows)
    {
        $units = $this->getDepartmentUnits();

        $unitArray = array();

        foreach ($units as $unit) {
            $inflow = array_key_exists($unit->name, $unitInflows) ? array_pull($unitInflows, $unit->name) : 0;

            $outflow = array_key_exists($unit->name, $unitOutflows) ? array_pull($unitOutflows, $unit->name) : 0;

            $unitArray[] = [
                'branch' => $unit->present()->getBranch,
                'unit' => $unit->name,
                'inflow' => $inflow,
                'outflow' => $outflow
            ];
        }

        $unitArray = $this->getOthersData($unitArray, $unitInflows, $unitOutflows);

        $unitCol = collect($unitArray);

        $inflowTotal = $unitCol->sum('inflow');
        $outflowTotal = $unitCol->sum('outflow');
        $netflowTotal = $inflowTotal - $outflowTotal;

        $unitCol = $unitCol->map(function ($item) use ($inflowTotal, $outflowTotal, $netflowTotal) {
            $netflow = $item['inflow'] - $item['outflow'];
            $item['percentage_inflow'] = percentage($item['inflow'], $inflowTotal, 2);
            $item['percentage_outflow'] = percentage($item['outflow'], $outflowTotal, 2);
            $item['netflow'] = $netflow;
            $item['percentage_netflow'] = percentage($netflow, $netflowTotal);

            return $item;
        });

        $unitCol->put('total', [
            'branch' => 'Total',
            'unit' => '',
            'inflow' => $inflowTotal,
            'outflow' => $outflowTotal,
            'percentage_inflow' => 100,
            'percentage_outflow' => 100,
            'netflow' => $netflowTotal,
            'percentage_netflow' => 100
        ]);

        return $unitCol;
    }

    /**
     * @param $unitArray
     * @param $unitInflows
     * @param $unitOutflows
     * @return array
     */
    private function getOthersData($unitArray, $unitInflows, $unitOutflows)
    {
        foreach ($unitInflows as $key => $inflow) {
            $outflow = array_key_exists($key, $unitOutflows) ? array_pull($unitOutflows, $key) : 0;

            $unitArray[] = [
                'branch' => $key,
                'unit' => $key,
                'inflow' => $inflow,
                'outflow' => $outflow
            ];
        }

        foreach ($unitOutflows as $key => $outflow) {
            $unitArray[] = [
                'branch' => $key,
                'unit' => $key,
                'inflow' => 0,
                'outflow' => array_pull($unitOutflows, $key)
            ];
        }

        return $unitArray;
    }
}
