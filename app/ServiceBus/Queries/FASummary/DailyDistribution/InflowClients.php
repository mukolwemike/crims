<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientPayment;
use Carbon\Carbon;

/**
 * Class InflowClients
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class InflowClients
{
    use DailyDistributionTrait;

    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->processInflows();
    }

    /**
     * @return array
     */
    private function processInflows()
    {
        $inflowArray = array();

        $inflows = $this->getInflowPayments();

        foreach ($inflows as $payment) {
            $data = $this->presentPayments($payment);

            $inflowArray[$data['branch']][] = $data;
        }

        $inflowArray = $this->presentBranchTotals($inflowArray);

        return $inflowArray;
    }

    /**
     * @param $dataArray
     * @return mixed
     */
    private function presentBranchTotals($dataArray)
    {
        $grandTotal = 0;

        foreach ($dataArray as $key => $branchInflow) {
            $branchCol = collect($branchInflow);

            $total = $branchCol->sum('amount');

            $dataArray[$key]['total'] = [
                'date' => 'Total',
                'branch' => $key,
                'client' => '',
                'fa' => '',
                'status' => '',
                'type' => '',
                'amount' => $total,
                'unit' => ''
            ];

            $grandTotal += $total;
        }

        $dataArray['Total Inflows']['total'] = [
            'date' => 'Total Inflows',
            'branch' => 'Total Inflows',
            'client' => '',
            'fa' => '',
            'status' => '',
            'type' => '',
            'amount' => $grandTotal,
            'unit' => ''
        ];

        return $dataArray;
    }

    /**
     * @param ClientPayment $payment
     * @return array
     */
    private function presentPayments(ClientPayment $payment)
    {
        $data = array();

        $recipient = $payment->present()->getCommissionRecipient;

        $data['date'] = Carbon::parse($payment->date)->toDateString();

        $data['branch'] = ($recipient) ? $recipient->present()->getBranch : 'Unknown';

        $data['client'] = $this->getClientName($payment);

        $data['fa'] = ($recipient) ? $recipient->name : '';

        $data['status'] = $recipient ? $recipient->present()->getActive : '';

        $data['type'] = $this->getPaymentType($payment);

        $data['amount'] = $this->getPaymentValue($payment);

        $data['unit'] = ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown';

        return $data;
    }

    /**
     * @return mixed
     */
    private function getInflowPayments()
    {
        return ClientPayment::where('type_id', 1)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->where(function ($q) {
                $q->whereNotNull('product_id')->orWhereNotNull('share_entity_id')->orWhereNotNull('unit_fund_id');
            })
            ->get();
    }
}
