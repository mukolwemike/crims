<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\DepartmentUnit;
use App\Cytonn\Models\ShareSale;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Investment\Action\Base;
use Cytonn\Models\ClientInvestmentWithdrawal;

/**
 * Trait UnitSummaryTrait
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
trait UnitSummaryTrait
{
    use DailyDistributionTrait;

    /**
     * @return array
     */
    private function processInflows()
    {
        $inflowArray = array();

        $inflows = $this->getInflows();

        foreach ($inflows as $payment) {
            $data = $this->getPaymentData($payment);

            if (array_key_exists($data['unit'], $inflowArray)) {
                $inflowArray[$data['unit']] = $inflowArray[$data['unit']] + $data['amount'];
            } else {
                $inflowArray[$data['unit']] = $data['amount'];
            }
        }

        return $inflowArray;
    }

    /**
     * @param ClientPayment $payment
     * @return array
     */
    private function getPaymentData(ClientPayment $payment)
    {
        $recipient = $payment->present()->getCommissionRecipient;

        return [
            'unit' => ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown',
            'amount' => $this->getPaymentValue($payment)
        ];
    }

    /**
     * @return mixed
     */
    private function getInflows()
    {
        return ClientPayment::where('type_id', 1)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->where(function ($q) {
                $q->whereNotNull('product_id')->orWhereNotNull('share_entity_id')->orWhereNotNull('unit_fund_id');
            })
            ->get();
    }

    /**
     * @return array
     */
    private function processOutflows()
    {
        $outflowArray = array();

        $outflows = $this->outflowType == 1 ? $this->getWithdrawnOutflows() : $this->getOutflows();

        foreach ($outflows as $outflow) {
            $data = $this->outflowType == 1 ? $this->getWithdrawalData($outflow) :
                $this->getOutflowPaymentData($outflow);

            if (array_key_exists($data['unit'], $outflowArray)) {
                $outflowArray[$data['unit']] = $outflowArray[$data['unit']] + $data['amount'];
            } else {
                $outflowArray[$data['unit']] = $data['amount'];
            }
        }

        if ($this->outflowType == 1) {
            $shareSales = $this->getShareSaleOutflows();

            foreach ($shareSales as $outflow) {
                $data = $this->getShareSaleData($outflow);

                if (array_key_exists($data['unit'], $outflowArray)) {
                    $outflowArray[$data['unit']] = $outflowArray[$data['unit']] + $data['amount'];
                } else {
                    $outflowArray[$data['unit']] = $data['amount'];
                }
            }

            $unitFundSales = $this->getUnitFundSaleOutflows();

            foreach ($unitFundSales as $outflow) {
                $data = $this->getUnitFundSaleData($outflow);

                if (array_key_exists($data['unit'], $outflowArray)) {
                    $outflowArray[$data['unit']] = $outflowArray[$data['unit']] + $data['amount'];
                } else {
                    $outflowArray[$data['unit']] = $data['amount'];
                }
            }
        }

        return $outflowArray;
    }

    /**
     * @param ClientPayment $payment
     * @return array
     */
    private function getOutflowPaymentData(ClientPayment $payment)
    {
        $recipient = $payment->present()->getCommissionRecipient;

        return [
            'unit' => ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown',
            'amount' => -($this->getPaymentValue($payment))
        ];
    }

    /**
     * @return mixed
     */
    private function getOutflows()
    {
        return ClientPayment::where('type_id', 2)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->where(function ($q) {
                $q->whereNotNull('product_id')->orWhereNotNull('share_entity_id')->orWhereNotNull('unit_fund_id');
            })
            ->get();
    }

    /**
     * @param ClientInvestmentWithdrawal $withdrawal
     * @return array
     */
    private function getWithdrawalData(ClientInvestmentWithdrawal $withdrawal)
    {
        $recipient = $withdrawal->investment->commission->recipient;

        return [
            'unit' => ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown',
            'amount' => convert(
                $withdrawal->principalWithdrawn(),
                $withdrawal->investment->product->currency,
                $withdrawal->date
            )
        ];
    }

    /**
     * @return mixed
     */
    private function getWithdrawnOutflows()
    {
        return ClientInvestmentWithdrawal::where('withdraw_type', '!=', 'interest')
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->ofType(Base::TYPE_SENT_TO_CLIENT)
            ->where('amount', '>', 0)
            ->get();
    }

    /**
     * @param ShareSale $sale
     * @return array
     */
    private function getShareSaleData(ShareSale $sale)
    {
        $recipient = $sale->payment->present()->getCommissionRecipient;

        return [
            'unit' => ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown',
            'amount' => convert(
                $sale->number * $sale->sale_price,
                $sale->payment->shareEntity->currency,
                $sale->date
            )
        ];
    }

    /**
     * @return mixed
     */
    private function getShareSaleOutflows()
    {
        return ShareSale::columnBetween('date', $this->startDate, $this->endDate)->has('payment')->get();
    }

    /**
     * @param UnitFundSale $unitFundSale
     * @return array
     */
    private function getUnitFundSaleData(UnitFundSale $unitFundSale)
    {
        $recipient = $unitFundSale->repository()->getCommissionRecipient();

        return [
            'unit' => ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown',
            'amount' => convert(
                $unitFundSale->repository()->getPurchaseValue(),
                $unitFundSale->fund->currency,
                $unitFundSale->date
            )
        ];
    }

    /**
     * @return mixed
     */
    private function getUnitFundSaleOutflows()
    {
        return UnitFundSale::columnBetween('date', $this->startDate, $this->endDate)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getDepartmentUnits()
    {
        return DepartmentUnit::all();
    }
}
