<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientPayment;
use Carbon\Carbon;

/**
 * Class ReCashFlow
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class ReCashFlow
{
    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'total' => [
                'product' => 'RE CashFlow',
                'amount' => $this->getReCashflowPayments()
            ]
        ];
    }

    /**
     * @return mixed
     */
    private function getReCashflowPayments()
    {
        return ClientPayment::where('type_id', 1)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->has('project')
            ->sum('amount');
    }
}
