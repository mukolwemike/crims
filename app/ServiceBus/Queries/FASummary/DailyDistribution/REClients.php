<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\FASummary\DailyDistribution;

use App\Cytonn\Models\ClientPayment;
use App\Cytonn\Models\RealEstatePayment;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;

/**
 * Class REClients
 * @package ServiceBus\Queries\FASummary\DailyDistribution
 */
class REClients
{
    use DailyDistributionTrait;

    /**
     * @var Carbon
     */
    protected $startDate;

    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * Inflow constructor.
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(Carbon $startDate, Carbon $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->processReClients();
    }

    /**
     * @return array|mixed
     */
    private function processReClients()
    {
        $reArray = array();

        $reClients = $this->getReClientPayments();

        foreach ($reClients as $payment) {
            $data = $this->presentPayment($payment);

            $reArray[$data['branch']][] = $data;
        }

        $reArray = $this->presentBranchTotals($reArray);

        return $reArray;
    }

    /**
     * @param $dataArray
     * @return mixed
     */
    private function presentBranchTotals($dataArray)
    {
        $grandTotal = 0;

        foreach ($dataArray as $key => $branchInflow) {
            $branchCol = collect($branchInflow);

            $total = $branchCol->sum('amount');

            $dataArray[$key]['total'] = [
                'date' => 'Total',
                'branch' => $key,
                'client' => '',
                'fa' => '',
                'type' => '',
                'product' => '',
                'amount' => $total,
                'unit' => '',
            ];

            $grandTotal += $total;
        }

        $dataArray['Total RE Inflow']['total'] = [
            'date' => 'Total RE Inflow',
            'branch' => 'Total RE Inflow',
            'client' => '',
            'fa' => '',
            'type' => '',
            'product' => '',
            'amount' => $grandTotal,
            'unit' => '',
        ];

        return $dataArray;
    }

    /**
     * @param ClientPayment $payment
     * @return array
     */
    private function presentPayment(ClientPayment $payment)
    {
        $data = array();

        $recipient = $payment->present()->getCommissionRecipient;

        $data['date'] = Carbon::parse($payment->date)->toDateString();

        $data['branch'] = ($recipient) ? $recipient->present()->getBranch : 'Unknown';

        $data['client'] = $this->getClientName($payment);

        $data['fa'] = ($recipient) ? $recipient->name : '';

        $data['product'] = $payment->project->name;

        $data['amount'] = $payment->amount;

        $data['unit'] = ($recipient) ? $recipient->present()->getDepartmentUnit : 'Unknown';

        return $data;
    }

    /**
     * @return mixed
     */
    private function getReClientPayments()
    {
        return ClientPayment::where('type_id', 1)
            ->columnBetween('date', $this->startDate, $this->endDate)
            ->has('project')
            ->get();
    }
}
