<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  19/12/2018
 *
 */

namespace ServiceBus\Queries\FASummary;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Illuminate\Support\Collection;

class UpdateClientInteractions
{
    /*
     * Get the required data and send back
     */
    public function handle($data)
    {
        $data = json_decode($data, true);

        return $this->getHoldingIds(collect($data));
    }

    /*
     *  Given an collection of schedule, ids, get the corresponding holding_ids
     */
    private function getHoldingIds(Collection $scheduleIds)
    {
        $finalArray = [];

        foreach ($scheduleIds as $scheduleId) {
            $finalArray[$scheduleId] = $this->holdingId($scheduleId);
        }

        return $finalArray;
    }

    /*
     * Given a schedule id, get the holding id
     */
    private function holdingId($scheduleId)
    {
        $realEstateSchedule = RealEstatePaymentSchedule::findOrFail($scheduleId);

        return $realEstateSchedule ? $realEstateSchedule->unit_holding_id : null;
    }
}
