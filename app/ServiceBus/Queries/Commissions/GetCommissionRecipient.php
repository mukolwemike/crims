<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\Commissions;

use App\Cytonn\Models\CommissionRecepient;

class GetCommissionRecipient
{
    /*
     * Handle the sending of commission recipients to crm
     */
    public function handle()
    {
        $commissionRecipients = CommissionRecepient::all();

        return $commissionRecipients->map(
            function ($commissionRecipient) {
                $recipientPosition = $commissionRecipient->present()->latestCommissionRecipient;

                return [
                'name' => $commissionRecipient->name,
                'email' => $commissionRecipient->email,
                'phone' => $commissionRecipient->phone,
                'phone_country_code' => $commissionRecipient->phone_country_code,
                'rank_id' => $commissionRecipient->rank_id,
                'user_type_id' => $commissionRecipient->recipient_type_id,
                'start_date' => ($recipientPosition) ? $recipientPosition->start_date : null,
                'end_date' => ($recipientPosition) ? $recipientPosition->end_date : null
                ];
            }
        );
    }
}
