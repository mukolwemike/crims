<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\BI\Investments;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Unitization\UnitFund;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Models\Unitization\UnitFundSale;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Models\ClientInvestmentTopup;
use Cytonn\Models\ClientInvestmentWithdrawal;

class InvestmentFlowSummary
{
    /**
     * @param $data
     * @return array
     */
    public function handle($data)
    {
        $startDate = Carbon::parse($data[0])->startOfDay();

        $endDate = Carbon::parse($data[1])->endOfDay();

        return [
            'investments' => $this->getInvestmentFlow($startDate, $endDate),
            'unit_funds' => $this->getUnitFundFlow($startDate, $endDate)
        ];
    }

    /*
     * INVESTMENTS FLOW
     */
    /**
     * @param Carbon $date
     * @return Product[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getInvestmentFlow(Carbon $startDate, Carbon $endDate)
    {
        $products = Product::all();

        return $products->map(function (Product $product) use ($startDate, $endDate) {
            $inflow = $this->getProductInflow($product, $startDate, $endDate);

            $outflow = $this->getProductOutflow($product, $startDate, $endDate);

            return [
                'id' => $product->id,
                'name' => $product->name,
                'inflow' => $inflow,
                'outflow' => $outflow,
                'netflow' => $inflow - $outflow
            ];
        });
    }

    /**
     * @param Product $product
     * @param Carbon $date
     * @return mixed
     */
    private function getProductInflow(Product $product, Carbon $startDate, Carbon $endDate)
    {
        $inflow = ClientInvestment::forProduct($product)
            ->investedBetweenDates($startDate, $endDate)
            ->where('investment_type_id', '!=', 3)
            ->get()
            ->sum(
                function ($clientInvestment) {
                    return convert(
                        $clientInvestment->amount,
                        $clientInvestment->product->currency,
                        Carbon::parse($clientInvestment->invested_date)
                    );
                }
            );

        $rolloverTopups = ClientInvestmentTopup::where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->whereHas('fromInvestment', function ($q) use ($product) {
                $q->forProduct($product);
            })->get()->sum(
                function ($clientTopup) {
                    return convert(
                        $clientTopup->amount,
                        $clientTopup->fromInvestment->product->currency,
                        Carbon::parse($clientTopup->date)
                    );
                }
            );

        return $inflow + $rolloverTopups;
    }

    /**
     * @param Product $product
     * @param Carbon $date
     * @return mixed
     */
    private function getProductOutflow(Product $product, Carbon $startDate, Carbon $endDate)
    {
        return ClientInvestmentWithdrawal::ofType('sent_to_client')
            ->where('withdraw_type', 'withdrawal')
            ->where('amount', '>', 0)
            ->doesntHave('reinvestedTo')
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->whereHas('investment', function ($q) use ($product) {
                $q->forProduct($product);
            })->get()->sum(
                function ($clientWithdrawal) {
                    return convert(
                        $clientWithdrawal->amount,
                        $clientWithdrawal->investment->product->currency,
                        Carbon::parse($clientWithdrawal->date)
                    );
                }
            );
    }

    /*
     * UNIT FUNDS
     */
    /**
     * @param Carbon $date
     * @return UnitFund[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getUnitFundFlow(Carbon $startDate, Carbon $endDate)
    {
        $funds = UnitFund::all();

        return $funds->map(function (UnitFund $fund) use ($startDate, $endDate) {
            $inflow = $this->getUnitFundInflow($fund, $startDate, $endDate);

            $outflow = $this->getUnitFundOutflow($fund, $startDate, $endDate);

            return [
                'id' => $fund->id,
                'name' => $fund->name,
                'inflow' => $inflow,
                'outflow' => $outflow,
                'netflow' => $inflow - $outflow
            ];
        });
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon $date
     * @return mixed
     */
    private function getUnitFundInflow(UnitFund $unitFund, Carbon $startDate, Carbon $endDate)
    {
        return UnitFundPurchase::where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->where('unit_fund_id', $unitFund->id)
            ->get()
            ->sum(function ($purchase) {
                return $purchase->number * $purchase->price;
            });
    }

    /**
     * @param UnitFund $unitFund
     * @param Carbon $date
     * @return mixed
     */
    private function getUnitFundOutflow(UnitFund $unitFund, Carbon $startDate, Carbon $endDate)
    {
        return UnitFundSale::where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->where('unit_fund_id', $unitFund->id)
            ->nonFees()
            ->get()
            ->sum(function ($sale) {
                return $sale->number * $sale->price;
            });
    }
}
