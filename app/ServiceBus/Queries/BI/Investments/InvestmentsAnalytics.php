<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\BI\Investments;

use App\Cytonn\Models\Currency;
use App\Cytonn\Models\FundManager;
use App\Cytonn\Models\Product;
use Carbon\Carbon;
use Cytonn\Investment\Summary\Analytics;
use Cytonn\Portfolio\Summary\Analytics as PortfolioAnalytics;

class InvestmentsAnalytics
{
    /*
     * Define the currency
     */
    private $currency;


    /*
     * Setup the constructor
     */
    public function __construct()
    {
        $this->currency = Currency::find(1);
    }
    /*
     * Get the investment analytics based on the data
     */
    public function handle($data)
    {
        return $this->getAnalytics($data[0], $data[1], $data[2]);
    }

    /*
     * Get the analytics data
     */
    public function getAnalytics($type, $date, $itemId)
    {
        $date = Carbon::parse($date);

        if ($type == 1) {
            return $this->getPortfolioAnalytics($date, $itemId);
        }

        return $this->getClientAnalytics($date, $itemId);
    }

    /*
     * Get the analytics data for the clients investments
     */
    public function getClientAnalytics(Carbon $date, $productId)
    {
        $product = Product::findOrFail($productId);

        $analytics = new Analytics(FundManager::all(), $date);
        $analytics->setBaseCurrency($product->currency);
        $analytics->setProduct($product);

        return [
            'currency' => $product->currency->code,
            'cost_value' => $analytics->costValue(),
            'market_value' => $analytics->marketValue(),
            'weighted_tenor' => $analytics->weightedTenor(),
            'weighted_rate' => $analytics->weightedRate()
        ];
    }

    /*
     * Get the analytics data for the clients investments
     */
    public function getPortfolioAnalytics(Carbon $date, $fundmanagerId)
    {
        $fundmanager = FundManager::findOrFail($fundmanagerId);

        $analytics = (new PortfolioAnalytics($fundmanager, $date))->setCurrency($this->currency);

        return [
            'currency' => $this->currency->code,
            'cost_value' => $analytics->costValue(),
            'market_value' => $analytics->marketValue(),
            'weighted_tenor' => $analytics->weightedTenor(),
            'weighted_rate' => $analytics->weightedRate()
        ];
    }
}
