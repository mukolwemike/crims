<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\BI\Investments;

use App\Cytonn\Models\FundManager;

class FundManagers
{
    /*
     * Get the fund managers
     */
    public function handle()
    {
        return FundManager::get(['id', 'name', 'fullname']);
    }
}
