<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\BI\Investments;

use App\Cytonn\Models\Product;

class Products
{
    /*
     * Return the products in the system
     */
    public function handle()
    {
        return Product::get(['id', 'name', 'description', 'fund_manager_id']);
    }
}
