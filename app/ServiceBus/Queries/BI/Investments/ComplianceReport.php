<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\BI\Investments;

use App\Cytonn\Models\Client;
use Carbon\Carbon;

class ComplianceReport
{
    /*
     * Handle the report
     */
    public function handle($data)
    {
        $date = Carbon::parse($data[0]);

        return $this->getReport($date);
    }

    /*
     * Get the compliance report
     */
    public function getReport(Carbon $date)
    {
        $clients = Client::activeOnDate($date)->where('id', 1)->has('uploadedKyc')->get();

        $count = 0;

        foreach ($clients as $client) {
            if ($client->repo->compliant($date)) {
                $count ++;
            }
        }

        $activeClients = Client::activeOnDate($date)->where('id', 1)->count();

        return [
            'active_clients' => $activeClients,
            'compliant_clients' => $count
        ];
    }
}
