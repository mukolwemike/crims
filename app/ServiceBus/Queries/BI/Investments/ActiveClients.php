<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\BI\Investments;

use App\Cytonn\Models\Client;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\ContactPresenter;
use Cytonn\Presenters\CountryPresenter;
use ServiceBus\Queries\BI\BIDataTrait;

class ActiveClients
{
    /*
     * Get the BI Data Trait
     */
    use BIDataTrait;

    /*
     * Handle the request
     */
    public function handle($data)
    {
        $type = $data[0];
        $count = $data[1];

        $scopeArray = $this->getDataScope($type);

        if ($count) {
            return $this->getClientCount(
                $scopeArray['investment'],
                $scopeArray['realestate'],
                $scopeArray['portfolio'],
                $scopeArray['unit_funds']
            );
        } else {
            return $this->getClients(
                $scopeArray['investment'],
                $scopeArray['realestate'],
                $scopeArray['portfolio'],
                $scopeArray['unit_funds']
            );
        }
    }

    /*
     * Determine the data scope depending on the request type
     */
    public function getDataScope($type)
    {
        $scopeArray = [
            'investment' => false,
            'realestate' => false,
            'portfolio' => false,
            'unit_funds' => false
        ];

        if ($type == 1) {
            $scopeArray = [
                'investment' => true,
                'realestate' => true,
                'portfolio' => true,
                'unit_funds' => true
            ];
        } elseif ($type == 2) {
            $scopeArray['investment'] = true;
        } elseif ($type == 3) {
            $scopeArray['realestate'] = true;
        } elseif ($type == 4) {
            $scopeArray['portfolio'] = true;
        } elseif ($type == 5) {
            $scopeArray['unit_funds'] = true;
        }

        return $scopeArray;
    }

    /*
     * Get the clients based on the options
     */
    public function getClients($investments = true, $realEstate = true, $portfolio = true, $unitFunds = true)
    {
        $clients = Client::active($investments, $realEstate, $portfolio, $unitFunds)
            ->with(['contact', 'investments', 'unitHoldings', 'investments.payments'])->get();

        return $clients->map(
            function ($client) {
                $contact = $client->contact;

                $age = ClientPresenter::presentAge($client->id);

                $investmentData = $this->processInvestmentsAndInterestPayments($client);

                return [
                    'id' => $client->id,
                    'type' => $client->clientType->name,
                    'country' => CountryPresenter::present($contact->country_id),
                    'town' => $client->town,
                    'complete' => $client->complete,
                    'name' => ClientPresenter::presentFullNameNoTitle($client->id),
                    'occupation' => $client->present_occupation,
                    'gender' => ContactPresenter::getGender($contact),
                    'age' => ($age == '') ? 0 : $age,
                    'dob' => isNotEmptyOrNull($client->dob) ? Carbon::parse($client->dob)->toDateString() : null,
                    'joint' => $client->joint,
                    'created_at' => Carbon::parse($client->created_at)->toDateTimeString(),
                    'investments' => $investmentData['investments'],
                    'holdings' => $this->processUnitHoldings($client)
                ];
            }
        );
    }

    /*
     * Get the client count based on the options
     */
    public function getClientCount($investments = true, $realEstate = true, $portfolio = true, $unitFunds = true)
    {
        return Client::active($investments, $realEstate, $portfolio, $unitFunds)->with('contact')->count();
    }
}
