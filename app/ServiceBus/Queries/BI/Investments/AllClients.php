<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\BI\Investments;

use App\Cytonn\Models\Client;
use Carbon\Carbon;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\ContactPresenter;
use ServiceBus\Queries\BI\BIDataTrait;

class AllClients
{
    /*
     * Get the BI Data Trait
     */
    use BIDataTrait;

    /*
     * Handle the request
     */
    public function handle($data)
    {
        $count = $data[0];

        if ($count) {
            return $this->getClientCount();
        } else {
            return $this->getClients();
        }
    }

    /*
     * Get the clients based on the options
     */
    public function getClients()
    {
        $clients = Client::with(['contact', 'investments', 'unitHoldings', 'investments.payments'])->get();

        return $clients->map(
            function ($client) {
                $contact = $client->contact;

                $age = ClientPresenter::presentAge($client->id);

                $investmentData = $this->processInvestmentsAndInterestPayments($client);

                return [
                    'id' => $client->id,
                    'type' => $client->clientType->name,
                    'town' => $client->town,
                    'complete' => $client->complete,
                    'name' => ClientPresenter::presentFullNameNoTitle($client->id),
                    'occupation' => $client->present_occupation,
                    'gender' => ContactPresenter::getGender($contact),
                    'age' => ($age == '') ? 0 : $age,
                    'created_at' => $client->created_at ? Carbon::parse($client->created_at) : $client->created_at,
                    'investments' => $investmentData['investments'],
                    'holdings' => $this->processUnitHoldings($client)
                ];
            }
        );
    }

    /*
     * Get the client count based on the options
     */
    public function getClientCount()
    {
        return Client::with('contact')->count();
    }
}
