<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\BI;

use App\Cytonn\Models\ClientInvestment;
use App\Cytonn\Models\Currency;
use Carbon\Carbon;
use Cytonn\Currencies\Converter\Convert;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Cytonn\Presenters\ClientPresenter;

trait BIDataTrait
{
    /*
     * Process the investments date
     */
    public function processInvestmentsAndInterestPayments($client)
    {
        $investmentArray = array();

        foreach ($client->investments as $investment) {
            $investmentArray[] = [
                'id' => $investment->id,
                'client_id' => $client->id,
                'name' => ClientPresenter::presentFullNameNoTitle($client->id),
                'product' => $investment->product_id,
                'amount' => $this->convert(
                    $investment->amount,
                    $investment->product->currency,
                    Carbon::parse($investment->invested_date)
                ),
                'invested_date' => Carbon::parse($investment->invested_date)->toDateString(),
                'maturity_date' => Carbon::parse($investment->maturity_date)->toDateString(),
                'withdrawn' => $investment->withdrawn,
                'rolled' => $investment->rolled,
                'withdraw_amount' => $this->convert(
                    $investment->withdraw_amount,
                    $investment->product->currency,
                    Carbon::parse($investment->withdrawal_date)
                ),
                'withdrawal_date' => $investment->withdrawal_date ?
                    Carbon::parse($investment->withdrawal_date)->toDateString() : $investment->withdrawal_date,
                'withdrawals' => $this->getInvestmentWithdrawals($investment)
            ];
        }

        return [
            'investments' => $investmentArray,
        ];
    }

    /*
     * Process the unitholdings data
     */
    public function processUnitHoldings($client)
    {
        return $client->unitHoldings->map(
            function ($holding) {
                return [
                    'id' => $holding->id,
                    'unit_id' => $holding->unit_id,
                    'unit_number' => $holding->unit->number,
                    'project' => $holding->unit->project_id,
                    'price' => $holding->price(),
                    'reservation_date' => $holding->reservation_date,
                    'active' => $holding->active,
                    'forfeit_date' => $holding->forfeit_date ?
                        Carbon::parse($holding->forfeit_date)->toDateString() : $holding->forfeit_date,
                    'payment_plan_id' => $holding->payment_plan_id
                ];
            }
        );
    }

    private function getInvestmentWithdrawals(ClientInvestment $investment)
    {
        return $investment->withdrawals->map(function (ClientInvestmentWithdrawal $withdrawal) {
            return [
                'id' => $withdrawal->id,
                'investment_id' => $withdrawal->investment_id,
                'amount' => $withdrawal->amount,
                'date' => Carbon::parse($withdrawal->date)->toDateString(),
                'type_id' => $withdrawal->type_id,
                'withdraw_type' => $withdrawal->withdraw_type
            ];
        });
    }

    /**
     * @param $amount
     * @param Currency $currency
     * @param Carbon $date
     * @return mixed
     */
    public function convert($amount, Currency $currency, Carbon $date)
    {
        $rate = (new Convert($this->getBaseCurrency()))->enableCaching(10)->read($currency, $date);

        return $amount * $rate;
    }

    public function getBaseCurrency()
    {
        return Currency::find(1);
    }
}
