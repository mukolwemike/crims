<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\ServiceBus\Queries\BI\Portfolio;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use Cytonn\Core\DataStructures\Carbon;

class DepositHoldingSummary
{
    /**
     * @param $data
     * @return mixed
     */
    public function handle($data)
    {
        $date = Carbon::parse($data[0]);

        return $this->getDepositHoldings($date);
    }

    /**
     * @param Carbon $date
     * @return mixed
     */
    private function getDepositHoldings(Carbon $date)
    {
        $holdings = DepositHolding::activeOnDate($date)->get();

        return $holdings->map(function (DepositHolding $holding) use ($date) {
            $prepared = $holding->calculate($date)->getPrepared();

            $total = $prepared->total;

            $totalArray = (array)$total;

            $totalArray = array_except($totalArray, ['type', 'date', 'investment', 'description']);

            $holdingArray = [
                'id' => $holding->id,
                'portfolio_security_id' => $holding->portfolio_security_id,
                'invested_date' => Carbon::parse($holding->invested_date)->toDateString(),
                'maturity_date' => Carbon::parse($holding->maturity_date)->toDateString(),
                'interest_rate' => $holding->interest_rate,
                'withdrawn' => $holding->withdrawn,
                'rolled' => $holding->rolled,
                'withdrawal_date' => isNotEmptyOrNull($holding->withdrawal_date) ?
                    Carbon::parse($holding->withdrawal_date)->toDateString() : null,
                'unit_fund_id' => $holding->unit_fund_id,
                'portfolio_repayments' => $this->getPortfolioRepayments($holding, $date)
            ];

            return $holdingArray + $totalArray;
        });
    }

    /**
     * @param DepositHolding $holding
     * @param Carbon $date
     * @return mixed
     */
    private function getPortfolioRepayments(DepositHolding $holding, Carbon $date)
    {
        $repayments = $holding->repayments()->before($date)->get();

        return $repayments->map(function (PortfolioInvestmentRepayment $repayment) {
            return [
                'id' => $repayment->id,
                'investment_id' => $repayment->investment_id,
                'date' => Carbon::parse($repayment->date)->toDateString(),
                'amount' => $repayment->amount,
                'type_id' => $repayment->type_id,
                'portfolio_repayment_action_id' => $repayment->portfolio_repayment_action_id,
            ];
        });
    }
}
