<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace App\ServiceBus\Queries\BI\Distribution;

use App\Cytonn\Models\CommissionRecepient;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;

class BIFaEvaluationSummary
{
    /*
     * Get the FA generator trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    /**
     * @param $data
     * @return CommissionRecepient[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function handle($data)
    {
        $startDate = Carbon::parse($data[0]);

        $endDate = Carbon::parse($data[1]);

        return $this->getEvaluationData($startDate, $endDate);
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return CommissionRecepient[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getEvaluationData(Carbon $startDate, Carbon $endDate)
    {
        $recipients = CommissionRecepient::all();

        return $recipients->map(function (CommissionRecepient $recepient) use ($startDate, $endDate) {
            return $this->getRecipientData($recepient, $startDate, $endDate);
        });
    }

    /**
     * @param CommissionRecepient $recepient
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     */
    private function getRecipientData(CommissionRecepient $recepient, Carbon $startDate, Carbon $endDate)
    {
        return [
            'id' => $recepient->id,
            'crm_id' => $recepient->crm_id,
            'name' => $recepient->name,
            'email' => $recepient->email,
            'phone' => $recepient->phone,
            'recipient_type_id' => $recepient->recipient_type_id,
            'recipient_type' => @$recepient->type->name,
            'rank_id' => $recepient->rank_id,
            'rank' => @$recepient->rank->name,
            'zero_commission' => $recepient->zero_commission,
            'reports_to' => $recepient->reports_to,
            'department_unit_id' => $recepient->department_unit_id,
            'department_unit' => $recepient->present()->getDepartmentUnit,
            'department_branch_id' => $recepient->departmentUnit ?
                $recepient->departmentUnit->department_branch_id : null,
            'department_branch' => $recepient->present()->getBranch,
            'production_data' => $this->getProductionData($recepient, $startDate, $endDate)
        ];
    }

    /**
     * @param CommissionRecepient $recepient
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    private function getProductionData(CommissionRecepient $recepient, Carbon $startDate, Carbon $endDate)
    {
        $inflows = $this->getCumulativeInflows($startDate, $endDate, $recepient->id);

        $outflows = $this->getWithdrawals($startDate, $endDate, $recepient->id);

        $reSales = $this->realEstateSales($startDate, $endDate, $recepient->id);

        $rePayments = $this->realEstateActualPayments($startDate, $endDate, $recepient->id);

        return [
            'inflows' => $inflows,
            'outflows' => $outflows,
            'netflows' => $inflows - $outflows,
            're_sales' => $reSales,
            're_payments' => $rePayments,
            'retention' => $inflows + $reSales - $outflows
        ];
    }
}
