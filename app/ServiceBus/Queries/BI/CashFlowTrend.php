<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\BI;

use Carbon\Carbon;

class CashFlowTrend
{
    /*
     * Handle the request
     */
    public function handle($data)
    {
        $month = $data[0];

        $startOfMonth = Carbon::parse()->startOfMonth();

        $endOfMonth = $startOfMonth->copy()->endOfMonth();

        return $this->getTrend($startOfMonth, $endOfMonth);
    }

    /*
     * Get the trend of the inflows and outflows for the given month
     */
    public function getTrend(Carbon $startOfMonth, Carbon $endOfMonth)
    {
    }
}
