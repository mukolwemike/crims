<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\BI\RealEstate;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\RealEstatePaymentType;
use App\Cytonn\Models\RealEstateRefund;
use App\Cytonn\Models\RealEstateSalesAgreement;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;

class RealEstateFlowSummary
{
    protected $startDate;
    protected $endDate;
    /**
     * @param $data
     * @return Project[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function handle($data)
    {
        $this->startDate = Carbon::parse($data[0])->startOfDay();

        $this->endDate = Carbon::parse($data[1])->endOfDay();

        $projects = Project::all();

        return $projects->map(function (Project $project) {
            return [
                'id' => $project->id,
                'name' => $project->name,
                'sales' => $this->getProjectSales($project),
                'actual_payments' => $this->getProjectActualPayments($project),
                'actual_payments_by_type' => $this->getProjectActualPaymentsByType($project),
                'refunds' => $this->getProjectRefunds($project),
                'forfeitures' => $this->getProjectForfeitures($project),
                'forfeited_units' => $this->getProjectForfeitedUnits($project),
                'forfeited_units_with_signed_loos' => $this->getForfeitedUnitsWithSignedLoos($project),
                'forfeited_units_with_signed_sas' => $this->getForfeitedUnitsWithSignedSas($project),
                'total_units' => $project->repo->countNumberOfUnits(),
                'total_units_sold' => $this->getProjectUnitsSold($project),
                'total_units_sold_and_paid' => $this->getProjectUnitsSoldAndPaid($project),
                'overdue_units' => $this->getProjectOverdueUnitsCount($project),
                'overdue_amount' => $this->getProjectOverdueUnitsValue($project),
                'loo_signed' => $this->getProjectLooSigned($project),
                'sas_signed' => $this->getProjectSasSigned($project),
                'topologies' => $this->getProjectUnitTopologies($project),
                'forfeited_topologies' => $this->getForfeitedProjectUnitTopologies($project),
                'tranches' => $this->getProjectUnitTranches($project),
                'forfeited_tranches' => $this->getForfeitedProjectUnitTranches($project)
            ];
        });
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectSales(Project $project)
    {
        return $this->getAciveHoldingsQuery($project)->get()->sum(
            function ($holding) {
                    return $holding->price();
            }
        );
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectActualPayments(Project $project, RealEstatePaymentType $type = null)
    {
        $payments = RealEstatePayment::whereHas('holding', function ($holding) use ($project) {
            $holding->inProjectIds([$project->id]);
        })->where('date', '>=', $this->startDate)
            ->where('date', '<=', $this->endDate);

        if ($type) {
            $payments = $payments->where('payment_type_id', $type->id);
        }

        return $payments->sum('amount');
    }

    /**
     * @param Project $project
     * @return RealEstatePaymentType[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    private function getProjectActualPaymentsByType(Project $project)
    {
        $types = RealEstatePaymentType::all();

        return $types->map(function ($type) use ($project) {
            return [
                'name' => $type->name,
                'slug' => $type->slug,
                'value' => $this->getProjectActualPayments($project, $type)
            ];
        });
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectRefunds(Project $project)
    {
        return RealEstateRefund::whereHas('holding', function ($holding) use ($project) {
            $holding->inProjectIds([$project->id]);
        })->where('date', '>=', $this->startDate)->where('date', '<=', $this->endDate)
            ->sum('amount');
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectForfeitures(Project $project)
    {
        return UnitHolding::whereNotNull('forfeit_date')->where('forfeit_date', '>=', $this->startDate)
            ->where('forfeit_date', '<=', $this->endDate)
            ->inProjectIds([$project->id])
            ->get()->sum(
                function ($holding) {
                    return $holding->price();
                }
            );
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectForfeitedUnits(Project $project)
    {
        return UnitHolding::whereNotNull('forfeit_date')->where('forfeit_date', '>=', $this->startDate)
            ->where('forfeit_date', '<=', $this->endDate)->inProjectIds([$project->id])->count();
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getForfeitedUnitsWithSignedLoos(Project $project)
    {
        return UnitHolding::whereNotNull('forfeit_date')->where('forfeit_date', '>=', $this->startDate)
            ->where('forfeit_date', '<=', $this->endDate)->inProjectIds([$project->id])
            ->whereHas('loo', function ($q) {
                $q->whereNotNull('date_signed');
            })->count();
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getForfeitedUnitsWithSignedSas(Project $project)
    {
        return UnitHolding::whereNotNull('forfeit_date')->where('forfeit_date', '>=', $this->startDate)
            ->where('forfeit_date', '<=', $this->endDate)->inProjectIds([$project->id])
            ->whereHas('salesAgreement', function ($q) {
                $q->whereNotNull('date_signed');
            })->count();
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getAciveHoldingsQuery(Project $project)
    {
        return UnitHolding::inProjectIds([$project->id])->reservedAfter($this->startDate)
            ->reservedBefore($this->endDate);
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectUnitsSold(Project $project)
    {
        return $this->getAciveHoldingsQuery($project)->count();
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectUnitsSoldAndPaid(Project $project)
    {
        return $this->getAciveHoldingsQuery($project)->has('unitTranche')->count();
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectUnitsSoldValue(Project $project)
    {
        return $this->getAciveHoldingsQuery($project)->get()
            ->sum(function (UnitHolding $holding) {
                return $holding->price();
            });
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectOverdueUnitsCount(Project $project)
    {
        return $this->getAciveHoldingsQuery($project)
            ->get()->sum(function (UnitHolding $holding) {
                return $holding->repo->isOverdue($this->endDate) ? 1 : 0;
            });
    }

    /**
     * @param Project $project
     * @return mixed
     */
    private function getProjectOverdueUnitsValue(Project $project)
    {
        return $this->getAciveHoldingsQuery($project)
            ->get()->sum(function (UnitHolding $holding) {
                return $holding->repo->isOverdue($this->endDate) ? $holding->price() : 0;
            });
    }

    /**
     * @param Project $project
     * @return int
     */
    private function getProjectLooSigned(Project $project)
    {
        return $this->getProjectLoosQuery($project)->whereNotNull('date_signed')
            ->where('date_signed', '>=', $this->startDate)->where('date_signed', '<=', $this->endDate)
            ->count();
    }

    /**
     * @param Project $project
     * @return RealestateLetterOfOffer|\Illuminate\Database\Eloquent\Builder
     */
    private function getProjectLoosQuery(Project $project)
    {
        return RealestateLetterOfOffer::whereHas('holding', function ($q) use ($project) {
                $q->inProjectIds([$project->id]);
        });
    }

    /**
     * @param Project $project
     * @return int
     */
    private function getProjectSasSigned(Project $project)
    {
        return $this->getProjectSasQuery($project)->where('date_signed', '>=', $this->startDate)
            ->where('date_signed', '<=', $this->endDate)->count();
    }

    /**
     * @param Project $project
     * @return RealEstateSalesAgreement|\Illuminate\Database\Eloquent\Builder
     */
    private function getProjectSasQuery(Project $project)
    {
        return RealEstateSalesAgreement::whereHas('holding', function ($q) use ($project) {
            $q->inProjectIds([$project->id]);
        });
    }

    /**
     * @param Project $project
     * @return array
     */
    private function getProjectUnitTopologies(Project $project)
    {
        $holdings = $this->getAciveHoldingsQuery($project)->get();

        return $this->getTopologyArray($holdings);
    }

    /**
     * @param Project $project
     * @return array
     */
    private function getForfeitedProjectUnitTopologies(Project $project)
    {
        $holdings = UnitHolding::whereNotNull('forfeit_date')->where('forfeit_date', '>=', $this->startDate)
            ->where('forfeit_date', '<=', $this->endDate)->inProjectIds([$project->id])->get();

        return $this->getTopologyArray($holdings);
    }

    /**
     * @param $holdings
     * @return array
     */
    private function getTopologyArray($holdings)
    {
        $topologyArray = array();

        foreach ($holdings as $holding) {
            $unitType = $holding->unit->present()->unitTypeName;

            $unitType = $unitType == '' ? 'Unknown Type' : $unitType;

            $unitSize = $holding->unit->present()->unitSizeName;

            $unitSize = $unitSize == '' ? 'Unknown Size' : $unitSize;

            if (array_key_exists($unitType, $topologyArray)
                && array_key_exists($unitSize, $topologyArray[$unitType])) {
                $totalPrice = $topologyArray[$unitType][$unitSize]['value'] + $holding->price();
                $totalNumber = $topologyArray[$unitType][$unitSize]['number'] + 1;
                $topologyArray[$unitType][$unitSize] = [
                    'number' => $totalNumber,
                    'value' => $totalPrice
                ];
            } else {
                $topologyArray[$unitType][$unitSize] = [
                    'number' => 1,
                    'value' => $holding->price()
                ];
            }
        }

        return $topologyArray;
    }

    /**
     * @param Project $project
     * @return array
     */
    private function getProjectUnitTranches(Project $project)
    {
        $holdings = $this->getAciveHoldingsQuery($project)->has('tranche')->get();

        return $this->getTrancheArray($holdings);
    }

    /**
     * @param Project $project
     * @return array
     */
    private function getForfeitedProjectUnitTranches(Project $project)
    {
        $holdings = UnitHolding::whereNotNull('forfeit_date')->where('forfeit_date', '>=', $this->startDate)
            ->where('forfeit_date', '<=', $this->endDate)->inProjectIds([$project->id])->has('tranche')->get();

        return $this->getTrancheArray($holdings);
    }

    /**
     * @param $holdings
     * @return array
     */
    private function getTrancheArray($holdings)
    {
        $trancheArray = array();

        foreach ($holdings as $holding) {
            $trancheName = $holding->tranche->name;

            if (array_key_exists($trancheName, $trancheArray)) {
                $totalPrice = $trancheArray[$trancheName]['value'] + $holding->price();
                $totalNumber = $trancheArray[$trancheName]['number'] + 1;
                $trancheArray[$trancheName] = [
                    'number' => $totalNumber,
                    'value' => $totalPrice
                ];
            } else {
                $trancheArray[$trancheName] = [
                    'number' => 1,
                    'value' => $holding->price()
                ];
            }
        }

        return $trancheArray;
    }
}
