<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\BI\RealEstate;

use App\Cytonn\Models\Project;

class Projects
{
    /*
     * Return the products in the system
     */
    public function handle()
    {
        return Project::get(['id', 'name', 'long_name', 'fund_manager_id']);
    }
}
