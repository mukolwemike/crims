<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\BI\RealEstate;

use App\Cytonn\Models\Project;
use Carbon\Carbon;

class ProjectReport
{
    /*
     * Handle the report
     */
    public function handle($data)
    {
        $date = Carbon::parse($data[0]);

        $projectId = $data[1];

        return $this->getReport($date, $projectId);
    }

    /*
     * Get the report of the sales agreements
     */
    public function getReport(Carbon $date, $projectId)
    {
        $projects = $projectId ? Project::where('id', $projectId)->get() : Project::all();

        return $projects->map(function ($project) use ($date) {
            return [
                'id' => $project->id,
                'name' => $project->name,
                'reserved_units' => $project->repo->countReservedUnitsAsAt($date),
                'total_units' => $project->repo->countNumberOfUnits(),
                'loos_signed' => $project->repo->sumOfLoosSignedAsAt($date),
                'loos_total' => $project->repo->sumOfLoosTotalAsAt($date),
                'loos_sent' => $project->repo->sumOfLoosSentAsAt($date),
                'sas_signed'=>  $project->repo->sumOfSasSignedAsAt($date),
                'sas_uploaded' => $project->repo->sumOfSasUploadedAsAt($date),
                'sas_total' => $project->repo->sumOfSasTotalAsAt($date),
            ];
        });
    }
}
