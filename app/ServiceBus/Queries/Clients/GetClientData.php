<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\Clients;

use App\Cytonn\Models\Client;

class GetClientData
{
    /*
     * Handle the request
     */
    public function handle($data)
    {
        $clientId = isset($data[0]) ? $data[0] : 0;

        $page = isset($data[1]) ? $data[1] : null;

        $perPage = isset($data[2]) ? $data[2] : null;

        $clients = $this->getClients($clientId, $page, $perPage);

        $clientArray = array();

        foreach ($clients as $client) {
            $contact = $client->contact;

            $fa = $client->getLatestFA();

            $faEmail = ($fa) ? $fa->email : null;

            $clientData = [
                'crims_client_id' => $client->id,
                'client_code' => $client->client_code,
                'type' => $contact->entity_type_id,
                'firstname' =>
                    ($contact->entity_type_id == 1) ? $contact->firstname : $client->contact_person_firstname,
                'lastname' => ($contact->entity_type_id == 1) ? $contact->lastname : $client->contact_person_lastname,
                'middlename' => $contact->middlename,
                'title_id' => ($contact->title_id < 8 && !is_null($contact->title_id)) ? $contact->title_id : 8,
                'email' => $contact->email,
                'phone' => $contact->phone,
                'address' => $client->postal_address . '-' . $client->postal_code,
                'organization' => $client->employer_name ? $client->employer_name : $contact->corporate_registered_name,
                'country_id' => $client->country_id,
                'city' => $client->town,
                'gender_id' => $contact->gender_id,
                'fa_email' => $faEmail
            ];

            $clientArray[] = $clientData;
//
//            if($client->emails)
//            {
//                foreach ($client->emails as $otherEmail)
//                {
//                    $clientData['email'] = $otherEmail;
//
//                    $clientArray[] = $clientData;
//                }
//            }
        }

        return $clientArray;
    }

    /**
     * @param $clientId
     * @param $page
     * @param $perPage
     *
     * @return Client[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    private function getClients($clientId, $page, $perPage)
    {
        if ($clientId != 0) {
            return Client::where('id', $clientId)->get();
        }

        $clients = Client::query();

        if (! is_null($page) && $perPage) {
            $clients = $clients->skip($page * $perPage)->take($perPage);
        }

        return $clients->get();
    }
}
