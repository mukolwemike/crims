<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\Clients;

use App\Cytonn\Models\ClientJointDetail;

class GetJointHolderData
{
    /*
     * Handle the request
     */
    public function handle($data)
    {
        $page = isset($data[0]) ? $data[0] : null;

        $perPage = isset($data[1]) ? $data[1] : null;

        $clientJointDetails = $this->getJointHolders($page, $perPage);

        $clientData = array();

        foreach ($clientJointDetails as $clientJointDetail) {
            $holder = $clientJointDetail->holder;

            if ($holder) {
                $client = $holder->client;

                if ($client) {
                    $fa = $client->getLatestFA();

                    $faEmail = ($fa) ? $fa->email : null;

                    $clientData[] = [
                        'crims_client_id' => $client->id,
                        'client_code' => $client->client_code,
                        'type' => 1,
                        'firstname' => $clientJointDetail->firstname,
                        'lastname' => $clientJointDetail->lastname,
                        'middlename' => $clientJointDetail->middlename,
                        'title_id' => ($clientJointDetail->title_id < 8 && !is_null($clientJointDetail->title_id))
                            ? $clientJointDetail->title_id : 8,
                        'email' => $clientJointDetail->email,
                        'phone' => $clientJointDetail->telephone_home,
                        'address' => $clientJointDetail->postal_address . '-' . $clientJointDetail->postal_code,
                        'organization' => '',
                        'country_id' => $clientJointDetail->country_id,
                        'city' => $clientJointDetail->town,
                        'gender_id' => $clientJointDetail->gender_id,
                        'fa_email' => $faEmail
                    ];
                }
            }
        }

        return $clientData;
    }

    /**
     * @param $page
     * @param $perPage
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    private function getJointHolders($page, $perPage)
    {
        $jointHolders = ClientJointDetail::query();

        if (! is_null($page) && $perPage) {
            $jointHolders = $jointHolders->skip($page * $perPage)->take($perPage);
        }

        return $jointHolders->get();
    }
}
