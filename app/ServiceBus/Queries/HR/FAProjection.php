<?php

namespace ServiceBus\Queries\HR;

use App\Cytonn\Models\BulkCommissionPayment;
use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Cytonn\Investment\Commission\Projections\CommissionProjection;

/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 23/05/2018
 * Time: 08:30
 */
class FAProjection
{
    public function handle($data)
    {
        $email = $data[0];

        $date = $data[1];

        if (!is_string($date)) {
            throw new \InvalidArgumentException("Only strings are accepted as dates");
        }

        $date = Carbon::parse($date);

        $fa = $this->getFaDetails($email);

        $dateArray = $this->getCommissionDates($date);

        return [
            'amount' => is_null($fa) ? 0 : $this->getProjection($fa, $dateArray['start'], $dateArray['end']),
            'three_months_average' => is_null($fa) ? 0 : $this->getProjection(
                $fa,
                $dateArray['start'],
                $dateArray['end']->copy()->addMonths(3)
            )
        ];
    }

    public function getProjection(CommissionRecepient $fa, Carbon $start, Carbon $end)
    {
        $projection = new CommissionProjection();

        return $projection->getProjections($fa, $start, $end)->sum('total_commission');
    }

    public function getFaDetails($email)
    {
        return CommissionRecepient::where('email', $email)->first();
    }

    private function getCommissionDates(Carbon $date)
    {
        $bulk = BulkCommissionPayment::includes($date)->first();

        if ($bulk) {
            return [
                'start' => Carbon::parse($bulk->start),
                'end' => Carbon::parse($bulk->end)
            ];
        }

        $latestBulk = BulkCommissionPayment::latest()->first();

        return [
            'start' => Carbon::parse($latestBulk->end)->addDay(),
            'end' => Carbon::parse($latestBulk->end)->addDay()->addMonthNoOverflow()
        ];
    }
}
