<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Queries\ClientCompliance;

use App\Cytonn\Models\Contact;
use Carbon\Carbon;

class ClientComplianceSummary
{
    /*
     * Handle the client compliance summary
     */
    public function handle($data)
    {
        $clients = $data[0];

        return $this->generateReport($clients);
    }

    /*
     * Generate the report
     */
    public function generateReport($clients)
    {
        $clientsArray = array();

        foreach ($clients as $client) {
            $dateAdded = $lastInvestmentProduct = $lastInvestmentDate = $name = $email = $phone = '';

            $contact = $this->getContact($client);

            if ($contact) {
                $name = $contact->present()->fullname;

                $email = $contact->email;

                $phone = $contact->phone;

                $dateAdded = Carbon::parse($contact->created_at)->toDateTimeString();

                if ($contact->client) {
                    $clientInvestment = $contact->client->investments()->latest()->first();

                    if ($clientInvestment) {
                        $lastInvestmentProduct = $clientInvestment->product->name;

                        $lastInvestmentDate = Carbon::parse($clientInvestment->created_at)->toDateTimeString();
                    }
                }
            }

            $client->crims_name = $name;

            $client->crims_email = $email;

            $client->crims_phone = $phone;

            $client->date_added = $dateAdded;

            $client->last_investment_product = $lastInvestmentProduct;

            $client->last_investment_date = $lastInvestmentDate;

            $clientsArray[] = $client;
        }

        return \GuzzleHttp\json_encode($clientsArray);
    }

    /*
     * Get the contact of the client based on email or phone
     */
    public function getContact($client)
    {
        $contact = Contact::where('email', $client->email)->first();

        if ($contact) {
            return $contact;
        }

        $contact = Contact::where('phone', $client->phone)->first();

        if ($contact) {
            return $contact;
        }

        return null;
    }
}
