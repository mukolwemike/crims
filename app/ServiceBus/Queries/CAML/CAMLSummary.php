<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\CAML;

use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Models\Unitization\FundCategory;
use Cytonn\Unitization\Trading\Performance;

class CAMLSummary
{
    public function handle($data)
    {
        $data = json_decode($data, true);

        $startDate = Carbon::parse($data['start_date']);

        $endDate = Carbon::parse($data['end_date']);

        return $this->getFundCategorySummaries($startDate, $endDate);
    }

    private function getFundCategorySummaries(Carbon $startDate, Carbon $endDate)
    {
        $fundCategories = FundCategory::whereIn('id', [4])->get();

        $categoryArray = array();

        foreach ($fundCategories as $fundCategory) {
            $categoryArray[$fundCategory->name] = $this->getCategoryData($fundCategory, $startDate, $endDate);
        }

        return $categoryArray;
    }

    private function getCategoryData(FundCategory $fundCategory, Carbon $startDate, Carbon $endDate)
    {
        $categoryArray = array();

        $categoryArray['name'] = $fundCategory->name;

        $funds = UnitFund::where('fund_category_id', $fundCategory->id)->get();

        foreach ($funds as $fund) {
            $categoryArray['funds'][$fund->short_name] = $this->getFundData(
                $fund,
                $startDate->copy(),
                $endDate->copy()
            );
        }

        return $categoryArray;
    }

    private function getFundData(UnitFund $unitFund, Carbon $startDate, Carbon $endDate)
    {
        $fundArray = array();

        while ($endDate >= $startDate) {
            $performance = new Performance($unitFund, $startDate->copy());

            switch ($unitFund->category->calculation->slug) {
                case 'daily-yield':
                    $value = $performance->netDailyYield();
                    break;
                case 'variable-unit-price':
                    $value = $performance->unitPrice();
                    break;
                default:
                    $value = 0;
                    break;
            }

            $fundArray['data'][] = [
                'date' => $startDate->toDateString(),
                'value' => $value,
            ];

            $startDate = $startDate->addDay();
        }

        $fundArray['name'] = $unitFund->name;

        $fundArray['short_name'] = $unitFund->short_name;

        return $fundArray;
    }
}
