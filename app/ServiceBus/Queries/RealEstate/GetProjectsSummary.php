<?php
/**
 *
 * @author: Mukolwe Michael <molukaka@cytonn.com>
 *
 * Project: crims.
 *
 */

namespace App\ServiceBus\Queries\RealEstate;

use App\Cytonn\Models\AdvocateProject;
use App\Cytonn\Models\Project;
use Cytonn\Api\Transformers\RealEstateUnitTransformer;

class GetProjectsSummary
{
    public function handle()
    {
        $projects = Project::all();

        return $projects->map(function ($project) {
            return [
                'id' => $project->id,
                'code' => $project->code,
                'name' => $project->name,
                'units' => $this->projectUnitSummary($project),
                'advocates' => $this->projectAdvocates($project)
            ];
        });
    }

    private function projectUnitSummary(Project $project)
    {
        return $project->units()
            ->get()
            ->map(function ($unit) {
                return (new RealEstateUnitTransformer())->transform($unit);
            });
    }

    private function projectAdvocates(Project $project)
    {
        return AdvocateProject::where('project_id', $project->id)->get()
            ->map(function ($advocateProject) {
                return [
                    'name' => $advocateProject->advocate->name,
                    'email' => $advocateProject->advocate->email,
                ];
            });
    }
}
