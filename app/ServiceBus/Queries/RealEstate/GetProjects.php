<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\RealEstate;

use App\Cytonn\Models\Project;

/**
 * Class GetProjects
 * @package ServiceBus\Queries\RealEstate
 */
class GetProjects
{
    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function handle()
    {
        $projects = Project::all();

        return $projects->map(function ($project) {
            return [
                'id' => $project->id,
                'name' => $project->name
            ];
        });
    }
}
