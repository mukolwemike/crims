<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\RealEstate;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\RealEstatePaymentSchedule;
use App\Cytonn\Models\UnitHolding;
use Carbon\Carbon;
use Cytonn\Presenters\AmountPresenter;
use Cytonn\Realestate\Payments\Interest;

class GetScheduleDetails
{
    public function handle($data)
    {
        $scheduleId = $data[0];

        $schedule = RealEstatePaymentSchedule::find($scheduleId);

        if (! $schedule) {
            return [];
        }

        return $this->getScheduleDetails($schedule);
    }

    private function getScheduleDetails(RealEstatePaymentSchedule $schedule)
    {
        $holding = $schedule->holding;

        $unit = $holding->unit;

        $client = $holding->client;

        $unitPrice = $holding->price();

        $totalPaid = $holding->repo->unitTotalPaid($holding);

        $nextPayment = $holding->repo->unitNextPayment($holding);

        $nextPaymentDate = $nextPayment ? Carbon::parse($nextPayment->date)->toDateString() : '';

        $overdueDate = Carbon::parse($schedule->date);

        $age = $overdueDate->diffInDays(Carbon::now());

        $fa = $client->getLatestFA('real_estate');

        return [
            'id' => $schedule->id,
            'holding_id' => $holding->id,
            'project' => $unit->project->name,
            'project_id' => $unit->project_id,
            'unit_number' => $unit->number,
            'typology' => $unit->size->name,
            'price' => AmountPresenter::currency($unitPrice),
            'paid' => AmountPresenter::currency($totalPaid),
            'balance' => AmountPresenter::currency($unitPrice - $totalPaid),
            'overdue_amount' => AmountPresenter::currency($holding->repo->amountOverdue()),
            'interest_accrued' =>
                AmountPresenter::currency((new Interest($schedule->holding))->calculate($schedule, Carbon::today())),
            'overdue_date' => $overdueDate->toDateString(),
            'next_payment_date' => $nextPaymentDate,
            'aging' => $age,
            'fa' => $fa->name,
            'units' => $this->getUnits($client),
        ];
    }

    private function getUnits(Client $client)
    {
        $holdings = $client->unitHoldings()->active()->get();

        $holdingsCol =  $holdings->map(function ($holding) {
            $unit = $holding->unit;

            return [
                'id' => $holding->id,
                'unit_name' => $unit->project->name . ' - ' . $unit->number,
                'project_id' => $unit->project_id,
                'number' => $unit->number
            ];
        });

        return $holdingsCol->sortBy('unit_name');
    }
}
