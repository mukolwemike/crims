<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Queries\RealEstate;

use App\Cytonn\Models\RealEstatePaymentSchedule;
use Carbon\Carbon;
use Cytonn\Api\DataTables\AlternateSortFilterPaginateTrait;
use Cytonn\Api\Transformers\RealEstate\CRMRealEstateScheduleTransformer;

class GetOverduePayments
{
    use AlternateSortFilterPaginateTrait;

    public function handle($data)
    {
        $filters = (array)$data[0];

        $stateArray = (array)$data[1];

        $filterFunction = $this->getOverdueFilters($filters);

        return $this->serviceBusSortFilterPaginate(
            $stateArray,
            new RealEstatePaymentSchedule(),
            [],
            function ($model) {
                return app(CRMRealEstateScheduleTransformer::class)->transform($model);
            },
            $filterFunction
        );
    }

    private function getOverdueFilters($filters)
    {
        return function ($schedule) use ($filters) {
            $func = $schedule->where('paid', false)->inPricing()->isLast(false)->whereHas(
                'holding',
                function ($holding) {
                    $holding->where('active', true);
                }
            );

            if (isset($filters['start_date']) && isset($filters['end_date'])) {
                $func = $func->between(Carbon::parse($filters['start_date']), Carbon::parse($filters['end_date']));
            } elseif (isset($filters['start_date'])) {
                $func = $func->where('date', '>=', $filters['start_date']);
            } elseif (isset($filters['end_date'])) {
                $func = $func->where('date', '<=', $filters['end_date']);
            }

            if (isset($filters['project_id'])) {
                $func = $func->forProjects([$filters['project_id']]);
            }

            return $func->oldest('date');
        };
    }
}
