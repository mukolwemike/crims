<?php
/**
 * Date: 09/06/2017
 * Time: 10:02
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

namespace ServiceBus\Events;

use Carbon\Carbon;

class Investments
{
    public function handle()
    {
        echo "event/queue/command handled";

        return Carbon::now()->toDayDateTimeString();
    }
}
