<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\PaymentSystem;

use App\Jobs\Payments\AccountBalanceProcessor;
use App\Jobs\Payments\PaymentsProcessor;

class PaymentProcessor
{

    /**
     * @param $data
     * @throws \Throwable
     */
    public function handle($data)
    {
        dispatch(new PaymentsProcessor($data));
    }

    public function handleBalance($data)
    {
        dispatch(new AccountBalanceProcessor($data));
    }
}
