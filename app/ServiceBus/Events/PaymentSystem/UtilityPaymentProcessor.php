<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\PaymentSystem;

class UtilityPaymentProcessor
{
    /**
     * @param $data
     * @throws \Throwable
     */
    public function handle($data)
    {
        dispatch(new \App\Jobs\Payments\UtilityPaymentProcessor($data));
    }
}
