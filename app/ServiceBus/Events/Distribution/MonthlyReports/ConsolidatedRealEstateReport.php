<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  14/12/2018
 *
 */

namespace ServiceBus\Events\Distribution\MonthlyReports;

use App\Cytonn\Models\CommissionRecepient;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\CRM\CrmMailers;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Illuminate\Support\Collection;

class ConsolidatedRealEstateReport
{
    /*
     * Use RealEstateSales trait
     */
    use RealEstateSalesTrait, ConsolidatedReportsTrait;

    /*
     * Handle the request for the event
     */
    public function handle($data)
    {
        $filters = json_decode($data[0]);

        $startDate = Carbon::parse($filters->start_date);

        $endDate = Carbon::parse($filters->end_date);

        $months = getDateRange($startDate, $endDate);

        $reportStartTime = Carbon::now();

        $excludeIfas = isset($filters->exclude_others) ? $filters->exclude_others : null;

        $commissionRecipients = $this->getCommissionRecipients($filters->department_unit, $excludeIfas);

        $groupedData = [
            'data' => $this->getGroupedCommissionRecipients($commissionRecipients, $months),
            'months' => $this->monthsArray($months)
        ];

        $reportName = 'Consolidated Real Estate Report';

        ExcelWork::generateSingleCustomExcelAndStore(
            $groupedData,
            formatFileName($reportName),
            'exports.fas.consolidated_real_estate_monthly_report'
        );

        CrmMailers::generalReport([
            'firstname' => $data[2],
            'to' => $data[1],
            'subject' => 'CRM ' . $reportName,
            'content' =>
                'Find attached the report indicated in the subject above.  
                (Time taken = '. Carbon::now()->diffInMinutes($reportStartTime) . ' minutes)',
            'path' => storage_path() . '/exports/' . formatFileName($reportName) . '.xlsx'
        ]);
    }

    /*
     * Given a recipient's id, start date and end date, get the recipients array
     */
    private function getRecipientsData($recipient, Carbon $startDate, Carbon $endDate)
    {
        $recipientId = $recipient->id;

        return [
            'email' => $recipient->email,
            'units_sold' => $this->realEstateUnitsSold($startDate, $endDate, $recipientId),
            'real_estate_actual_amount' => $this->realEstateActualPayments($startDate, $endDate, $recipientId),
            'real_estate_value' => $this->realEstateSales($startDate, $endDate, $recipientId),
        ];
    }

    /*
     * Grouped Commission recipients data
     */
    private function getGroupedCommissionRecipients(Collection $recipients, $months)
    {
        $consolidatedArray = [];

        $branchArray = array();

        foreach ($recipients as $recipient) {
            $commissionRecipientsArray = [];

            $recipientUnit = $this->getRecipientDepartmentUnit($recipient);

            $recipientBranch = $this->getRecipientDepartmentBranch($recipient);

            foreach ($months as $month) {
                $startDate = Carbon::parse($month)->startOfMonth();

                $endDate = Carbon::parse($month)->endOfMonth();

                $commissionRecipientsArray[$month->format('F-Y')]
                    = $this->getRecipientsData($recipient, $startDate, $endDate);
            }

            $consolidatedArray[$recipientBranch][$recipientUnit][$recipient->name] = $commissionRecipientsArray;

            $branchArray = $this->updateConsolidatedData(
                $commissionRecipientsArray,
                $branchArray,
                $recipientUnit,
                $recipientBranch
            );
        }

        foreach ($branchArray as $branchKey => $branchData) {
            foreach ($branchData as $unitKey => $unitData) {
                if ($unitKey != 'Branch Total') {
                    $consolidatedArray[$branchKey][$unitKey]['Unit Total'] = $unitData;
                } else {
                    $consolidatedArray[$branchKey][$unitKey]['Branch Total'] = $unitData;
                }
            }
        }

        return $consolidatedArray;
    }

    /*
     * Update the consolidated data
     */
    private function updateConsolidatedData($userData, $branchData, $unitName, $branchName)
    {
        if (isset($branchData[$branchName][$unitName])) {
            foreach ($userData as $key => $monthData) {
                $branchData[$branchName][$unitName][$key]['units_sold'] =
                    $branchData[$branchName][$unitName][$key]['units_sold']
                    + $userData[$key]['units_sold'];
                $branchData[$branchName][$unitName][$key]['real_estate_actual_amount'] =
                    $branchData[$branchName][$unitName][$key]['real_estate_actual_amount']
                    + $userData[$key]['real_estate_actual_amount'];
                $branchData[$branchName][$unitName][$key]['real_estate_value'] =
                    $branchData[$branchName][$unitName][$key]['real_estate_value']
                    + $userData[$key]['real_estate_value'];
            }
        } else {
            $branchData[$branchName][$unitName] = $userData;
        }

        if (isset($branchData[$branchName]['Branch Total'])) {
            foreach ($userData as $key => $monthData) {
                $branchData[$branchName][$unitName][$key]['units_sold'] =
                    $branchData[$branchName][$unitName][$key]['units_sold']
                    + $userData[$key]['units_sold'];
                $branchData[$branchName]['Branch Total'][$key]['real_estate_actual_amount'] =
                    $branchData[$branchName]['Branch Total'][$key]['real_estate_actual_amount']
                    + $userData[$key]['real_estate_actual_amount'];
                $branchData[$branchName]['Branch Total'][$key]['real_estate_value'] =
                    $branchData[$branchName]['Branch Total'][$key]['real_estate_value']
                    + $userData[$key]['real_estate_value'];
            }
        } else {
            $branchData[$branchName]['Branch Total'] = $userData;
        }

        return $branchData;
    }
}
