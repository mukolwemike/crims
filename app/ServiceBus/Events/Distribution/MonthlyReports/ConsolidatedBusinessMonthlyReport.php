<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  07/12/2018
 *
 */

namespace ServiceBus\Events\Distribution\MonthlyReports;

use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\CRM\CrmMailers;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Illuminate\Support\Collection;
use function Aws\filter;

class ConsolidatedBusinessMonthlyReport
{
    /*
     * Use the mandatory  Trait
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait,
        ConsolidatedReportsTrait;

    private $startDate;
    private $endDate;

    /*
     * Handles the incoming request
     */
    public function handle($data)
    {
        $filters = json_decode($data[0]);

        $this->startDate = Carbon::parse($filters->start_date);

        $this->endDate = Carbon::parse($filters->end_date);

        $months = getDateRange($this->startDate, $this->endDate);

        $reportStartTime = Carbon::now();

        $excludeIfas = isset($filters->exclude_others) ? $filters->exclude_others : null;

        $commissionRecipients = $this->getCommissionRecipients($filters->department_unit, $excludeIfas);

        $groupedData = [
            'data' => $this->getGroupedCommissionRecipients($commissionRecipients, $months),
            'months' => $this->monthsArray($months)
        ];

        $reportName = 'Consolidated Monthly Report';

        ExcelWork::generateSingleCustomExcelAndStore(
            $groupedData,
            formatFileName($reportName),
            'exports.fas.consolidated_business_monthly_report'
        );

        CrmMailers::generalReport([
            'firstname' => $data[2],
            'to' => $data[1],
            'bcc' => config('system.administrators'),
            'subject' => 'CRM ' . $reportName,
            'content' =>
                'Find attached the report indicated in the subject above.
                (Time taken = ' . Carbon::now()->diffInMinutes($reportStartTime) . ' minutes)',
            'path' => storage_path() . '/exports/' . formatFileName($reportName) . '.xlsx'
        ]);
    }

    /*
     * Grouped Commission recipients data
     */
    private function getGroupedCommissionRecipients(Collection $recipients, $months)
    {
        $consolidatedArray = [];

        $branchArray = array();

        foreach ($recipients as $recipient) {
            $commissionRecipientsArray = [];

            $recipientUnit = $this->getRecipientDepartmentUnit($recipient);

            $recipientBranch = $this->getRecipientDepartmentBranch($recipient);

            foreach ($months as $month) {
                $startDate = Carbon::parse($month)->startOfMonth();

                $startDate = ($this->startDate && $startDate < $this->startDate) ? $this->startDate : $startDate;

                $endDate = Carbon::parse($month)->endOfMonth();

                $endDate = ($this->endDate && $endDate > $this->endDate) ? $this->endDate : $endDate;

                $commissionRecipientsArray[$month->format('F-Y')]
                    = $this->getRecipientsData($recipient, $startDate, $endDate);
            }

            $consolidatedArray[$recipientBranch][$recipientUnit][$recipient->name .
            ':' . $recipient->id] = $commissionRecipientsArray;

            $branchArray =
                $this->updateConsolidatedData(
                    $commissionRecipientsArray,
                    $branchArray,
                    $recipientUnit,
                    $recipientBranch
                );
        }

        foreach ($branchArray as $branchKey => $branchData) {
            foreach ($branchData as $unitKey => $unitData) {
                if ($unitKey != 'Branch Total') {
                    $consolidatedArray[$branchKey][$unitKey]['Unit Total'] = $unitData;
                } else {
                    $consolidatedArray[$branchKey][$unitKey]['Branch Total'] = $unitData;
                }
            }
        }

        return $consolidatedArray;
    }

    private function updateConsolidatedData($userData, $branchData, $unitName, $branchName)
    {
        if (isset($branchData[$branchName][$unitName])) {
            foreach ($userData as $key => $monthData) {
                $branchData[$branchName][$unitName][$key]['new_investment_amount'] =
                    $branchData[$branchName][$unitName][$key]['new_investment_amount']
                    + $userData[$key]['new_investment_amount'];
                $branchData[$branchName][$unitName][$key]['top_ups_amount'] =
                    $branchData[$branchName][$unitName][$key]['top_ups_amount']
                    + $userData[$key]['top_ups_amount'];
                $branchData[$branchName][$unitName][$key]['topups_on_rollover'] =
                    $branchData[$branchName][$unitName][$key]['topups_on_rollover']
                    + $userData[$key]['topups_on_rollover'];
                $branchData[$branchName][$unitName][$key]['rollover_amounts'] =
                    $branchData[$branchName][$unitName][$key]['rollover_amounts']
                    + $userData[$key]['rollover_amounts'];
                $branchData[$branchName][$unitName][$key]['partial_withdrawals'] =
                    $branchData[$branchName][$unitName][$key]['partial_withdrawals']
                    + $userData[$key]['partial_withdrawals'];
                $branchData[$branchName][$unitName][$key]['reinvested_interest'] =
                    $branchData[$branchName][$unitName][$key]['reinvested_interest']
                    + $userData[$key]['reinvested_interest'];
                $branchData[$branchName][$unitName][$key]['withdrawals'] =
                    $branchData[$branchName][$unitName][$key]['withdrawals']
                    + $userData[$key]['withdrawals'];
                $branchData[$branchName][$unitName][$key]['re_invested_withdrawals'] =
                    $branchData[$branchName][$unitName][$key]['re_invested_withdrawals']
                    + $userData[$key]['re_invested_withdrawals'];
                $branchData[$branchName][$unitName][$key]['withdrawals_sent_to_client'] =
                    $branchData[$branchName][$unitName][$key]['withdrawals_sent_to_client']
                    + $userData[$key]['withdrawals_sent_to_client'];
                $branchData[$branchName][$unitName][$key]['net_flows_amount'] =
                    $branchData[$branchName][$unitName][$key]['net_flows_amount']
                    + $userData[$key]['net_flows_amount'];
                $branchData[$branchName][$unitName][$key]['real_estate_actual_amount'] =
                    $branchData[$branchName][$unitName][$key]['real_estate_actual_amount']
                    + $userData[$key]['real_estate_actual_amount'];
                $branchData[$branchName][$unitName][$key]['real_estate_value'] =
                    $branchData[$branchName][$unitName][$key]['real_estate_value']
                    + $userData[$key]['real_estate_value'];
                $branchData[$branchName][$unitName][$key]['unit_fund_purchases'] =
                    $branchData[$branchName][$unitName][$key]['unit_fund_purchases']
                    + $userData[$key]['unit_fund_purchases'];
                $branchData[$branchName][$unitName][$key]['unit_fund_sales'] =
                    $branchData[$branchName][$unitName][$key]['unit_fund_sales']
                    + $userData[$key]['unit_fund_sales'];
//                $branchData[$branchName][$unitName][$key]['ytd_withdrawals'] =
//                    $branchData[$branchName][$unitName][$key]['ytd_withdrawals']
//                    + $userData[$key]['ytd_withdrawals'];
                $branchData[$branchName][$unitName][$key]['rollover_interests'] =
                    $branchData[$branchName][$unitName][$key]['rollover_interests']
                    + $userData[$key]['rollover_interests'];
                $branchData[$branchName][$unitName][$key]['period_withdrawals'] =
                    $branchData[$branchName][$unitName][$key]['period_withdrawals']
                    + $userData[$key]['period_withdrawals'];
                $branchData[$branchName][$unitName][$key]['period_unit_fund_sales'] =
                    $branchData[$branchName][$unitName][$key]['period_unit_fund_sales']
                    + $userData[$key]['period_unit_fund_sales'];
            }
        } else {
            $branchData[$branchName][$unitName] = $userData;
        }

        if (isset($branchData[$branchName]['Branch Total'])) {
            foreach ($userData as $key => $monthData) {
                $branchData[$branchName]['Branch Total'][$key]['new_investment_amount'] =
                    $branchData[$branchName]['Branch Total'][$key]['new_investment_amount']
                    + $userData[$key]['new_investment_amount'];
                $branchData[$branchName]['Branch Total'][$key]['top_ups_amount'] =
                    $branchData[$branchName]['Branch Total'][$key]['top_ups_amount']
                    + $userData[$key]['top_ups_amount'];
                $branchData[$branchName]['Branch Total'][$key]['topups_on_rollover'] =
                    $branchData[$branchName]['Branch Total'][$key]['topups_on_rollover']
                    + $userData[$key]['topups_on_rollover'];
                $branchData[$branchName]['Branch Total'][$key]['rollover_amounts'] =
                    $branchData[$branchName]['Branch Total'][$key]['rollover_amounts']
                    + $userData[$key]['rollover_amounts'];
                $branchData[$branchName]['Branch Total'][$key]['partial_withdrawals'] =
                    $branchData[$branchName]['Branch Total'][$key]['partial_withdrawals']
                    + $userData[$key]['partial_withdrawals'];
                $branchData[$branchName]['Branch Total'][$key]['reinvested_interest'] =
                    $branchData[$branchName]['Branch Total'][$key]['reinvested_interest']
                    + $userData[$key]['reinvested_interest'];
                $branchData[$branchName]['Branch Total'][$key]['withdrawals'] =
                    $branchData[$branchName]['Branch Total'][$key]['withdrawals']
                    + $userData[$key]['withdrawals'];
                $branchData[$branchName]['Branch Total'][$key]['re_invested_withdrawals'] =
                    $branchData[$branchName]['Branch Total'][$key]['re_invested_withdrawals']
                    + $userData[$key]['re_invested_withdrawals'];
                $branchData[$branchName]['Branch Total'][$key]['withdrawals_sent_to_client'] =
                    $branchData[$branchName]['Branch Total'][$key]['withdrawals_sent_to_client']
                    + $userData[$key]['withdrawals_sent_to_client'];
                $branchData[$branchName]['Branch Total'][$key]['net_flows_amount'] =
                    $branchData[$branchName]['Branch Total'][$key]['net_flows_amount']
                    + $userData[$key]['net_flows_amount'];
                $branchData[$branchName]['Branch Total'][$key]['real_estate_actual_amount'] =
                    $branchData[$branchName]['Branch Total'][$key]['real_estate_actual_amount']
                    + $userData[$key]['real_estate_actual_amount'];
                $branchData[$branchName]['Branch Total'][$key]['real_estate_value'] =
                    $branchData[$branchName]['Branch Total'][$key]['real_estate_value']
                    + $userData[$key]['real_estate_value'];
                $branchData[$branchName]['Branch Total'][$key]['unit_fund_purchases'] =
                    $branchData[$branchName]['Branch Total'][$key]['unit_fund_purchases']
                    + $userData[$key]['unit_fund_purchases'];
                $branchData[$branchName]['Branch Total'][$key]['unit_fund_sales'] =
                    $branchData[$branchName]['Branch Total'][$key]['unit_fund_sales']
                    + $userData[$key]['unit_fund_sales'];
//                $branchData[$branchName]['Branch Total'][$key]['ytd_withdrawals'] =
//                    $branchData[$branchName]['Branch Total'][$key]['ytd_withdrawals']
//                    + $userData[$key]['ytd_withdrawals'];
                $branchData[$branchName]['Branch Total'][$key]['rollover_interests'] =
                    $branchData[$branchName]['Branch Total'][$key]['rollover_interests']
                    + $userData[$key]['rollover_interests'];
                $branchData[$branchName]['Branch Total'][$key]['period_withdrawals'] =
                    $branchData[$branchName]['Branch Total'][$key]['period_withdrawals']
                    + $userData[$key]['period_withdrawals'];
                $branchData[$branchName]['Branch Total'][$key]['period_unit_fund_sales'] =
                    $branchData[$branchName]['Branch Total'][$key]['period_unit_fund_sales']
                    + $userData[$key]['period_unit_fund_sales'];
            }
        } else {
            $branchData[$branchName]['Branch Total'] = $userData;
        }

        return $branchData;
    }

    /*
     * Given a recipient's id, start date and end date, get the recipients array
     */
    private function getRecipientsData($recipient, Carbon $startDate, Carbon $endDate)
    {
        $recipientId = $recipient->id;

        $newInvestments = $this->getInflows($startDate, $endDate, $recipientId, null, 1);

        $topups = $this->getInflows($startDate, $endDate, $recipientId, null, 2);

        $rolloverTopups = $this->getRolloverTopups($startDate, $endDate, $recipientId);

        $withdrawals = $this->getCompetitionWithdrawals(
            $startDate,
            $endDate,
            $recipient->id,
            null,
            [1, 2]
        );

        $withdrawalsSentToClient = $this->getCompetitionWithdrawals(
            $startDate,
            $endDate,
            $recipient->id,
            null,
            [1]
        );

        return [
            'email' => $recipient->email,
            'active' => $recipient->present()->activeStatus,
            'new_investment_amount' => $newInvestments,
            'top_ups_amount' => $topups,
            'topups_on_rollover' => $rolloverTopups,
            'rollover_amounts' => $this->getInflows($startDate, $endDate, $recipientId, null, 3),
            'partial_withdrawals' => $this->getInflows($startDate, $endDate, $recipientId, null, 4),
            'reinvested_interest' =>
                $this->getInflows($startDate, $endDate, $recipientId, null, 5),
            'withdrawals' => $withdrawals,
            're_invested_withdrawals' =>
                $this->getCompetitionWithdrawals($startDate, $endDate, $recipient->id, null, [2]),
            'withdrawals_sent_to_client' => $withdrawalsSentToClient,
            'net_flows_amount' => $newInvestments + $topups + $rolloverTopups - $withdrawalsSentToClient,
            'real_estate_actual_amount' => $this->realEstateActualPayments($startDate, $endDate, $recipientId),
            'real_estate_value' => $this->realEstateSales($startDate, $endDate, $recipientId),
            'unit_fund_purchases' => $this->getUnitFundInflows($startDate, $endDate, $recipientId),
            'unit_fund_sales' => $this->getUnitFundOutflows($startDate, $endDate, $recipientId),
//            'ytd_withdrawals' =>
//                $this->getWithdrawals($endDate->copy()->startOfYear(), $endDate, $recipient->id),
            'rollover_interests' => $this->getRolloverInflows($startDate, $endDate, $recipient->id),
            'period_withdrawals' => $this->getNormalInvestmentWithdrawals($startDate, $endDate, $recipient->id),
            'period_unit_fund_sales' => $this->getNormalUnitFundOutflows($startDate, $endDate, $recipient->id)
        ];
    }
}
