<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  17/12/2018
 *
 */

namespace ServiceBus\Events\Distribution\MonthlyReports;

use App\Cytonn\Models\CommissionRecepient;

trait ConsolidatedReportsTrait
{
    /*
     * Get the commission recipients grouped into department units
     */
    private function getCommissionRecipients($departmentUnitId = 0, $excludeIFas = null)
    {
        $commissionRecipients = CommissionRecepient::query();

        if ($excludeIFas == 0) {
            $commissionRecipients = $commissionRecipients->isFa();
        }

        if ($departmentUnitId != 0) {
            $commissionRecipients = $commissionRecipients->where('department_unit_id', $departmentUnitId);
        }

        return $commissionRecipients->get();
    }

    /*
     * Get the department unit the recipient belong to
     */
    public function getRecipientDepartmentUnit(CommissionRecepient $commissionRecipient)
    {
        if ($commissionRecipient->recipient_type_id == 1) {
            return 'Staff';
        } elseif ($commissionRecipient->recipient_type_id == 3) {
            return 'IFA';
        } else {
            $departmentUnit = $commissionRecipient->departmentUnit;
            return $departmentUnit ? $departmentUnit->name : 'Unknown Unit';
        }
    }

    /*
     * Get the department branch a commission recipient belongs to
     */
    public function getRecipientDepartmentBranch(CommissionRecepient $commissionRecipient)
    {
        if ($commissionRecipient->recipient_type_id == 1) {
            return 'Staff';
        } elseif ($commissionRecipient->recipient_type_id == 3) {
            return 'IFA';
        } else {
            $departmentUnit = $commissionRecipient->departmentUnit;
            $departmentBranch = $departmentUnit ? $departmentUnit->departmentBranch : null;
            return $departmentUnit && is_null($departmentBranch) ? $departmentUnit->name :
                ($departmentBranch ? $departmentBranch->name : 'Unknown Branch');
        }
    }


    /*
     * Get the formatted months for display
     */
    public function monthsArray($months)
    {
        return collect($months)->map(function ($month) {
                return $month->format('F-Y');
        })->toArray();
    }
}
