<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\Distribution\RTD;

use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Mailers\CRM\CrmMailers;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Support\Mails\Mailer;
use Maatwebsite\Excel\Facades\Excel;

class RTDRecruitmentSummary
{
    /*
     * Use the Traits
     */
    use InFlowsTrait,
        WithdrawalsTrait,
        RealEstateSalesTrait;

    public function handle($data)
    {
        $unitsAndBranchesData = $data[0];

        $endDate = Carbon::parse($data[1]);

        $mailerData  = [
            'recipient'=> $data[2],
            'firstname' => $data[3]
        ];

        $this->sendPreparedReport($unitsAndBranchesData, $endDate, $mailerData);
    }

    /**
     * @param $unitAndBranchData
     * @param Carbon $endDate
     * @param array $mailerData
     */
    private function sendPreparedReport($unitAndBranchData, Carbon $endDate, array $mailerData)
    {
        $unitData = $unitAndBranchData[0];

        $branchData = $unitAndBranchData[1];

        $processedData = $this->processData($unitData, $branchData, $endDate);

        $reportName = 'RTD Recruitment Summary';

        $views = [
            'exports.fas.rtd_units_summary',
            'exports.fas.rtd_branches_summary'
        ];

        ExcelWork::generateMultiExcelFromViewAndStore($processedData, formatFileName($reportName), $views);

        CrmMailers::generalReport([
            'firstname' => $mailerData['firstname'],
            'to' => $mailerData['recipient'],
            'bcc' => config('system.administrators'),
            'subject' => 'CRM ' . $reportName,
            'content' =>
                'Please find attached the RTD recruitment summary as at ' . $endDate->toDateString(),
            'path' => storage_path() . '/exports/' . formatFileName($reportName) . '.xlsx'
        ]);
    }

    /**
     * @param $unitArray
     * @param $branchesArray
     * @param Carbon $end
     *
     * @return array
     */
    private function processData($unitArray, $branchesArray, Carbon $end)
    {
        foreach ($unitArray as $key => $unit) {
            $branchName = $unit->branch;

            foreach ($unit->users as $userKey => $user) {
                $cumulativeRetention = $this->getIndividualRecipientData($user, $end);

                if ($cumulativeRetention != 0) {
                    $unitArray->{$key}->with_production += 1;
                    $branchesArray->with_production->{$branchName} += 1;
                    $branchesArray->with_production->Totals += 1;
                } else {
                    $unitArray->{$key}->without_production += 1;
                    $branchesArray->without_production->{$branchName} += 1;
                    $branchesArray->without_production->Totals += 1;
                }

                $branchesArray->fas->{$branchName} += $unit->fas;
                $branchesArray->fas->Totals += $unit->fas;
            }

            $unitArray->{$key}->unit_variance = $unitArray->{$key}->fas - $unitArray->{$key}->target_number_of_fas;

            $branchesArray->distribution_variance->{$branchName}
                = $branchesArray->fas->{$branchName}
                - ($branchesArray->target_no_of_ums_and_aums->{$branchName}  * 10 );
            //target number of producing fas per unit = 10
        }

        $branchNames = array_keys((array) $branchesArray->fas);

        array_pop($branchNames);

        foreach ($branchNames as $branchName) {
            $branchesArray->distribution_variance->Totals += $branchesArray->distribution_variance->{$branchName};
            $branchesArray->current_um_variance->Totals += $branchesArray->current_um_variance->{$branchName};
            $branchesArray->target_no_of_ums_and_aums->Totals
                += $branchesArray->target_no_of_ums_and_aums->{$branchName};
            $branchesArray->number_of_ums_and_aums->Totals += $branchesArray->number_of_ums_and_aums->{$branchName};
        }

        return [
            'Units Data' => $unitArray,
            'Branches Data' => $branchesArray,
        ];
    }

    /**
     * @param $user
     * @param $end
     *
     * @return int|mixed|number
     */
    private function getIndividualRecipientData($user, $end)
    {
        $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

        $cumulativeStartDate = Carbon::parse($user->cumulative_start_date);

        $cumulativeRetention = 0;

        if ($rec_id) {
            //Get all the client investment that belong to an FA since they joined the company
            $cumulativeInflows = $this->getCumulativeInflows($cumulativeStartDate, $end, $rec_id);

            //Get all the client withdrawals that belong to an FA since they joied the company
            $cumulativeWithdrawals = $this->getWithdrawals($cumulativeStartDate, $end, $rec_id);

            //Get the cumulative netflow which is calculated by finding the difference
            // between the cumulative inflows and cumulative withdrawals
            $cumulativeNetFlow = $cumulativeInflows - $cumulativeWithdrawals;

            $cumulativeRealEstateSales = $this->realEstateSales($cumulativeStartDate, $end, $rec_id);

            $cumulativeRetention = $cumulativeNetFlow + $cumulativeRealEstateSales;
        }

        return $cumulativeRetention;
    }
}
