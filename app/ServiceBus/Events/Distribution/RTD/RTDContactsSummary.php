<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\Distribution\RTD;

use Carbon\Carbon;
use Cytonn\Investment\CommissionRecipientRepository;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\InFlowsTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\RealEstateSalesTrait;
use Cytonn\Reporting\SummaryReports\FaEvaluationSummaries\WithdrawalsTrait;
use Cytonn\Support\Mails\Mailer;
use Maatwebsite\Excel\Facades\Excel;

class RTDContactsSummary
{
    /*
     * Get the traits
     */
    use InFlowsTrait,
        RealEstateSalesTrait,
        WithdrawalsTrait;

    public function handle($data)
    {
        $userData = $data[0];

        $startDate = Carbon::parse($data[1]);

        $endDate = Carbon::parse($data[2]);

        $recipient = $data[3];

        return $this->generateReport($userData, $startDate, $endDate, $recipient);
    }

    private function generateReport($userData, Carbon $startDate, Carbon $endDate, $recipient)
    {
        $userData = $this->processData($userData, $endDate);

        $fileName = 'RTD_Contacts_Summary';

        $this->generateExcel($userData, $fileName);

        Mailer::compose()
            ->from([
                'address' => 'crmsystem@cytonn.com',
                'name' => 'Cytonn CRM'
            ])
            ->to([$recipient])
            ->bcc(config('system.administrators'))
            ->subject('CRM RTD Contacts Summary')
            ->text('Please find attached the RTD contacts summary for the period between '
                . $startDate->toDateString() . ' and ' . $endDate->toDateString())
            ->excel([$fileName])
            ->send();

        return [];
    }

    private function generateExcel($userData, $fileName)
    {
        Excel::create($fileName, function ($excel) use ($userData) {
            $excel->sheet('Contacts Summary', function ($sheet) use ($userData) {
                $sheet->loadView('exports.fas.rtd_users_summary', ['values' =>  $userData]);
            });
        })->store('xlsx', storage_path('exports'));
    }

    private function processData($userArray, Carbon $end)
    {
        foreach ($userArray as $key => $user) {
            $userValues = $this->getIndividualRecipientData($user, $end);

            $ytdRetention = $userValues['ytd_retention'];

            $cumulativeRetention = $userValues['cumulative_retention'];

            $userArray[$key]->ytd_retention = $ytdRetention;

            $userArray[$key]->cumulative_retention = $cumulativeRetention;
        }

        return $userArray;
    }

    private function getIndividualRecipientData($user, $end)
    {
        $rec_id = (new CommissionRecipientRepository())->getCommissionRecipientIdByEmail($user->email);

        $ytdStartDate = Carbon::parse($user->ytd_start_date);

        $cumulativeStartDate = Carbon::parse($user->cumulative_start_date);

        $YTDRetention = $cumulativeRetention = 0;

        if ($rec_id) {
            //Get all the client investment that belong to an FA since the start of the year (YTD Inflows)
            $YTDInflows = $this->getYTDInflows($ytdStartDate, $end, $rec_id);

            //Get all the client investment that belong to an FA since they joined the company
            $cumulativeInflows = $this->getCumulativeInflows($cumulativeStartDate, $end, $rec_id);

            //Get all the client withdrawals that belong to an FA since that start of the Year (YTD Withdrawals)
            $YTDWithdrawals = $this->getWithdrawals($ytdStartDate, $end, $rec_id);

            //Get all the client withdrawals that belong to an FA since they joied the company
            $cumulativeWithdrawals = $this->getWithdrawals($cumulativeStartDate, $end, $rec_id);

            //Get the YTD net flow which is calculated by finding the difference between the inflows and withdrawals
            $YTDNetFlow = $YTDInflows - $YTDWithdrawals;

            //Get the cumulative netflow which is calculated by finding the difference
            // between the cumulative inflows and cumulative withdrawals
            $cumulativeNetFlow = $cumulativeInflows - $cumulativeWithdrawals;

            $YTDRealEstateSales = $this->realEstateSales($ytdStartDate, $end, $rec_id);

            $cumulativeRealEstateSales = $this->realEstateSales($cumulativeStartDate, $end, $rec_id);

            $YTDRetention = $YTDNetFlow + $YTDRealEstateSales;

            $cumulativeRetention = $cumulativeNetFlow + $cumulativeRealEstateSales;
        }

        return [
            'ytd_retention' => $YTDRetention,
            'cumulative_retention' => $cumulativeRetention
        ];
    }
}
