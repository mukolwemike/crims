<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  2019-04-05
 *
 */

namespace ServiceBus\Events\Distribution;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Product;
use App\Cytonn\Models\Project;
use App\Cytonn\Models\Unitization\UnitFund;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\CRM\CrmMailers;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Database\Eloquent\Collection;

class ClientInteractionsReport
{

    /**
     * @param null $data
     */
    public function handle($data = null)
    {
        try {
            Mailer::compose()
                ->from([
                    'address' => 'crmsystem@cytonn.com',
                    'name' => 'Cytonn CRM'
                ])
                ->to(['tkimathi@cytonn.com'])
                ->subject('Client Interaction Notification')
                ->text('Client Interaction processing Start')
                ->send();

            $decodedData = json_decode($data, true);

            $reportName = $decodedData['reportName'];

            $processedReportData = $this->putMissingClientsData($decodedData['data']);

            ExcelWork::generateAndStoreMultiSheet($processedReportData, formatFileName($reportName));

            CrmMailers::generalReport([
                'firstname' => $decodedData['firstname'],
                'to' => $decodedData['to'],
                'bcc' => config('system.administrators'),
                'subject' => 'CRM ' . $reportName,
                'content' =>
                    'Find attached the report indicated in the subject above.',
                'path' => storage_path() . '/exports/' . formatFileName($reportName) . '.xlsx'
            ]);
        } catch (\Exception $e) {
            $this->notifyError($e);
        }
    }

    private function notifyError(\Exception $exception)
    {
        Mailer::compose()
            ->to(['tkimathi@cytonn.com'])
            ->subject('CRIMS : Client Interaction Error')
            ->text("<p>Message :  " . $exception->getMessage() . "</p>
               <p>Trace : " . $exception->getTraceAsString() . "</p>")
            ->send();
    }

    /**
     * @param $clientId
     * @param bool $active
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|Collection|\Illuminate\Database\Eloquent\Model|null
     */
    private function getClientById($clientId, $active = false)
    {
        $client = Client::query();

        if ($active) {
            return $client->active()->find($clientId);
        }

        return $client->find($clientId);
    }

    /**
     * @param array $clientsData
     * @return array
     */
    private function putMissingClientsData(array $clientsData) : array
    {
        foreach ($clientsData as $clientCategory => $categorisedClientsArray) {
            foreach ($categorisedClientsArray as $key => $clientArray) {
                $client = $this->getClientById($clientArray['id']);

                if (is_null($client)) {
                    unset($clientsData[$clientCategory][$key]);
                    continue;
                }

                $productsString = $this->getClientProducts($client);

                $clientArray['Client Code'] = $client->client_code;

                $clientArray['Products'] = $productsString;

                $clientArray['Status'] = $this->getClientById($clientArray['id'], true) ? 'Active' : 'Inactive';

                $clientsData[$clientCategory][$key] = $clientArray;
            }
        }

        return $clientsData;
    }

    /**
     * @param Client $client
     * @return string
     */
    private function getClientProducts(Client $client)
    {
        $products = Product::whereHas('investments', function ($q) use ($client) {
            $q->where('client_id', $client->id);
        })->pluck('name')->toArray();

        $projects = Project::whereHas('units', function ($q) use ($client) {
            $q->whereHas('holdings', function ($q) use ($client) {
                $q->where('client_id', $client->id);
            });
        })->pluck('name')->toArray();

        $unitFunds = UnitFund::whereHas('purchases', function ($q) use ($client) {
            $q->where('client_id', $client->id);
        })->pluck('short_name')->toArray();

        $productArray = array_merge($products, $projects);

        $productArray = array_merge($productArray, $unitFunds);

        $productString = '';

        foreach ($productArray as $product) {
            if ($product && str_is('', $productString)) {
                $productString = $product;
            } else {
                $productString .= ', ' . $product;
            }
        }

        return trim($productString);
    }
}
