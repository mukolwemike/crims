<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\SalesAdmin;

use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Presenters\General\AmountPresenter;
use Carbon\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Support\Mails\Mailer;

class WeeklySalesAdminPaymentsDue
{
    /*
     * Handle the sales request
     */
    public function handle($data)
    {
        $holdings = $data[0];

        $recipients = $data[1];

        $report = $this->generateReport($holdings);

        $fileName = 'Weekly Sales Payments Due';

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        Mailer::compose()
            ->from([
                'address' => 'crmsystem@cytonn.com',
                'name' => 'Cytonn CRM'
            ])
            ->to($recipients->to)
            ->bcc($recipients->bcc)
            ->subject($fileName)
            ->text('Please find attached the weekly sales payments due report for this week')
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    /*
     * Generate the holdings report
     */
    private function generateReport($holdings)
    {
        $dataArray = array();

        foreach ($holdings as $holdingId) {
            $holding = UnitHolding::findOrFail($holdingId->holding_id);

            $client = $holding->client;

            $nextPayment = $holding->repo->unitNextPayment($holding);

            $nextPaymentDate = $nextPayment ? Carbon::parse($nextPayment->date)->toDateString() : '';

            $dataArray[] = [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Project' => $holding->unit->project->name,
                'Unit Number' => $holding->unit->number,
                'Amount' => AmountPresenter::currency($holding->repo->amountOverdue()),
                'Date Amount Due' => $nextPaymentDate,
                'Agreed Next Payment Date' => $holdingId->next_payment_date
            ];
        }

        return $dataArray;
    }
}
