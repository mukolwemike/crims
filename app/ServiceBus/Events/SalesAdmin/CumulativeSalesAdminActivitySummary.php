<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\SalesAdmin;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;

class CumulativeSalesAdminActivitySummary
{
    public function handle($data)
    {
        $date = Carbon::parse($data[0]);

        return $this->getReport($date);
    }

    private function getReport(Carbon $date)
    {
        $projectArray = array();

        $projects = Project::all();

        foreach ($projects as $project) {
            $projectArray[$project->id] = [
                'Name' => $project->name,
                'Holdings' => $this->getProjectOverdueHoldings($project, $date)
            ];
        }

        return $projectArray;
    }

    private function getProjectOverdueHoldings(Project $project, Carbon $date)
    {
        $holdings = UnitHolding::activeAtDate($date)->inProjectIds([$project->id])
            ->get();

        $holdingArray = array();

        foreach ($holdings as $holding) {
            $schedules = $holding->paymentSchedules()->inPricing()
                ->before($date)->orderBy('date')->get();

            foreach ($schedules as $schedule) {
                if (!$schedule->paid()) {
                    $balance = $holding->repo->amountOverdue();

                    $paid = $this->getPayments($holding, $schedule->date, $date);

                    $holdingArray[$holding->id] = [
                        'id' => $holding->id,
                        'overdue_date' => Carbon::parse($schedule->date)->toDateString(),
                        'balance' => $balance,
                        'amount_paid' => $paid,
                        'outstanding' => $balance + $paid
                    ];

                    break;
                }
            }
        }

        return $holdingArray;
    }

    private function getPayments(UnitHolding $holding, $scheduleDate, $reportDate)
    {
        return RealEstatePayment::where('holding_id', $holding->id)
            ->between($scheduleDate, $reportDate)
            ->sum('amount');
    }
}
