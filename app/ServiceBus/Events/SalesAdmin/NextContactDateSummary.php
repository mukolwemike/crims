<?php
/**
 * Cytonn Technologies.
 *
 * @author Charles <cokoyoh@cytonn.com>
 *
 * Project CRM
 *
 * @date  21/12/2018
 *
 */

namespace ServiceBus\Events\SalesAdmin;

use App\Cytonn\Models\UnitHolding;
use App\Cytonn\Presenters\General\AmountPresenter;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Mailers\CRM\CrmMailers;
use Cytonn\Presenters\ClientPresenter;

class NextContactDateSummary
{
    public function handle($data)
    {
        $holdingIds = json_decode($data[0]);

        $reportStartTime = Carbon::now();

        $processedData = $this->getNextContactData($holdingIds);

        $reportName = 'Next Contact Date Schedules';

        ExcelWork::generateAndStoreSingleSheet($processedData, $reportName);

        $filePath = storage_path() . '/exports/' . $reportName . '.xlsx';

        CrmMailers::generalReport([
            'firstname' => $data[2],
            'to' => $data[1],
            'subject' => 'CRM ' . 'Sales Admin ' . $reportName,
            'content' =>
                'Find attached the report indicated in the subject above.  
                (Time taken = '. Carbon::now()->diffInMinutes($reportStartTime) . ' minutes)',
            'path' => $filePath,
        ]);

        return \File::delete($filePath);
    }


    private function getNextContactData(array $holdingIds)
    {
        $unitHoldingsArray = [];

        foreach ($holdingIds as $holdingId) {
            $holding =  $this->getUnitHoldingById($holdingId);

            $client = $holding->client;

            $nextPayment = $holding->repo->unitNextPayment($holding);

            $nextPaymentDate = $nextPayment ? Carbon::parse($nextPayment->date)->toDateString() : '';

            $unitHoldingsArray[] = [
                'Client Code' => $client->client_code,
                'Client Name' => ClientPresenter::presentFullNames($client->id),
                'Project' => $holding->unit->project->name,
                'Unit Number' => $holding->unit->number,
                'Amount' => AmountPresenter::currency($holding->repo->amountOverdue()),
                'Date Amount Due' => $nextPaymentDate,
            ];
        }

        return $unitHoldingsArray;
    }


    private function getUnitHoldingById($id)
    {
        return UnitHolding::findOrFail($id);
    }
}
