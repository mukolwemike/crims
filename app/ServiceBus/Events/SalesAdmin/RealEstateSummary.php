<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\SalesAdmin;

use App\Cytonn\Models\Project;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;

class RealEstateSummary
{
    /*
     * Handle the request
     */
    public function handle($data)
    {
        $date = Carbon::parse($data[0]);

        $recipients = $data[1];

        $report = $this->generateReport($date);

        $fileName = 'Real Estate Summary';

        ExcelWork::generateAndStoreSingleSheet($report, $fileName);

        Mailer::compose()
            ->from([
                'address' => 'crmsystem@cytonn.com',
                'name' => 'Cytonn CRM'
            ])
            ->to($recipients->to)
            ->bcc($recipients->bcc)
            ->subject($fileName)
            ->text('Please find attached the real estate summary report for ' . $data[0])
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function generateReport(Carbon $date)
    {
        $projects = Project::all();

        $projectCol = $projects->map(function ($project) use ($date) {
            $repo = $project->repo;

            $sales = $repo->totalSalesAsAt($date);

            $reservation = $repo->totalPaymentsAsAt($date, 1);

            $deposits = $repo->totalPaymentsAsAt($date, 2);

            $installments = $repo->totalPaymentsAsAt($date, 3);

            $collections = $reservation + $deposits + $installments;

            return [
                'Name' => $project->name,
                'Sales' => $sales,
                'Reservation' => $reservation,
                'Deposits' => $deposits,
                'Installments' => $installments,
                'Total Collections' => $collections,
                'Balance' => $sales - $collections,
                'Overdue Amount' => $repo->totalOverduePaymentsAsAt($date),
            ];
        });

        $projectCol = $projectCol->put('total', [
            'Name' => 'Total',
            'Sales' => $projectCol->sum('Sales'),
            'Reservation' => $projectCol->sum('Reservation'),
            'Deposits' => $projectCol->sum('Deposits'),
            'Installments' => $projectCol->sum('Installments'),
            'Total Collections' => $projectCol->sum('Total Collections'),
            'Balance' => $projectCol->sum('Balance'),
            'Overdue Amount' => $projectCol->sum('Overdue Amount'),
        ]);

        return $projectCol;
    }
}
