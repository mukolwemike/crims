<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\SalesAdmin;

use App\Cytonn\Models\Project;
use App\Cytonn\Models\RealEstatePayment;
use App\Cytonn\Models\UnitHolding;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Excels\ExcelWork;
use Cytonn\Support\Mails\Mailer;

class DailySalesAdminActivitySummary
{
    /*
     * Handle the request
     */
    public function handle($data)
    {
        $clients = (array)$data[0];

        $date = Carbon::parse($data[1]);

        $recipients = $data[2];

        $report = $this->generateReport($clients, $date);

        $fileName = 'Daily Sales Admin Activity';

        ExcelWork::generateAndStoreMultiSheet($report, $fileName);

        Mailer::compose()
            ->from([
                'address' => 'crmsystem@cytonn.com',
                'name' => 'Cytonn CRM'
            ])
            ->to($recipients->to)
            ->bcc($recipients->bcc)
            ->subject($fileName)
            ->text('Please find attached the daily sales admin activity summary for ' . $data[1])
            ->excel([$fileName])
            ->send();

        $path = storage_path().'/exports/' . $fileName .'.xlsx';

        return \File::delete($path);
    }

    private function generateReport($clients, Carbon $date)
    {
        $projects = Project::all();

        $projectArray = array();

        foreach ($projects as $project) {
            $overdueHoldings = $project->repo->totalOverdueHoldings($date);

            $contactedArray = $this->getContactedArray($project);

            $nonContactedArray = $this->getNonContactedArray($project);

            foreach ($overdueHoldings as $holding) {
                $paid = $this->getPayments($holding, $date);

                if (array_key_exists($holding->id, $clients)) {
                    $contactedArray['Total'] += 1;

                    $interaction = $clients[$holding->id];

                    if ($interaction->responsive == 1) {
                        $contactedArray['Responsive'] += 1;
                    } else {
                        $contactedArray['Non-responsive'] += 1;
                    }

                    if ($paid != 0) {
                        $contactedArray['Paid'] += 1;
                    }

                    $contactedArray['Amount Paid'] += $paid;
                } else {
                    $nonContactedArray['Total'] += 1;

                    if ($paid != 0) {
                        $nonContactedArray['Paid'] += 1;
                    }

                    $nonContactedArray['Amount Paid'] += $paid;
                }
            }

            $projectArray['Contacted'][$project->id] = $contactedArray;

            $projectArray['Not Contacted'][$project->id] = $nonContactedArray;
        }

        return $projectArray;
    }

    private function getContactedArray(Project $project)
    {
        return [
            'Name' => $project->name,
            'Total' => 0,
            'Responsive' => 0,
            'Non-responsive' => 0,
            'Paid' => 0,
            'Amount Paid' => 0,
        ];
    }

    private function getNonContactedArray(Project $project)
    {
        return [
            'Name' => $project->name,
            'Total' => 0,
            'Paid' => 0,
            'Amount Paid' => 0,
        ];
    }

    private function getPayments(UnitHolding $holding, Carbon $date)
    {
        return RealEstatePayment::where('holding_id', $holding->id)
            ->where('date', $date)
            ->sum('amount');
    }
}
