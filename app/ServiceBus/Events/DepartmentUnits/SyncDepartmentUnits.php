<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\DepartmentUnits;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\DepartmentBranch;
use App\Cytonn\Models\DepartmentUnit;
use Cytonn\Models\Investment\Position;

class SyncDepartmentUnits
{
    /*
     * Handle the request
     */
    public function handle($data)
    {
        $this->syncBranches($data->branches);

        $this->syncDepartmentUnits($data->units);

        $this->syncPositions($data->positions);
    }

    /*
     * Create or update the branches
     */
    public function syncBranches($branches)
    {
        foreach ($branches as $branchData) {
            $branch = DepartmentBranch::where('id', $branchData->id)->first();

            $branch = $branch ? $branch : new DepartmentBranch();

            $branch->id = $branchData->id;

            $branch->name = $branchData->name;

            $branch->save();
        }
    }

    /*
     * Create or update the department units
     */
    public function syncDepartmentUnits($departmentUnits)
    {
        foreach ($departmentUnits as $departmentUnit) {
            $unit = DepartmentUnit::where('id', $departmentUnit->id)->first();

            $head = CommissionRecepient::where('email', $departmentUnit->head_email)->first();

            $unit = $unit ? $unit : new DepartmentUnit();

            $unit->id = $departmentUnit->id;

            $unit->name = $departmentUnit->name;

            $unit->head_id = ($head) ? $head->id : null;

            $unit->department_branch_id = $departmentUnit->department_branch_id;

            $unit->save();
        }
    }

    /**
     * @param $positions
     */
    public function syncPositions($positions)
    {
        foreach ($positions as $positionData) {
            $position = Position::where('id', $positionData->id)->first();

            $position = $position ? $position : new Position();

            $position->id = $positionData->id;

            $position->name = $positionData->name;

            $position->save();
        }
    }
}
