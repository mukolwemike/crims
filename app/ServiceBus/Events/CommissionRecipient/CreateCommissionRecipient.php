<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Events\CommissionRecipient;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecepientPosition;

class CreateCommissionRecipient
{
    /*
     * Get the commission recepient trait
     */
    use CommissionRecepientTrait;

    /*
     * Handle the client compliance summary
     */
    /**
     * @param $data
     * @return bool
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle($data)
    {
        $recipient = json_decode($data[0], true);

        return $this->manageCommissionRecipient($recipient);
    }

    /*
     * Store or update a commission recipient record
     */
    /**
     * @param $recipients
     * @return bool
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function manageCommissionRecipient($recipients)
    {
        foreach ($recipients as $recipient) {
            $supervisor = $this->checkSupervisor($recipient);

            $commissionRecipient = $this->storeCommissionRecipient($recipient, $supervisor);

            if ($this->compareLastRecipientPosition($commissionRecipient, $supervisor, $recipient)) {
                $this->closeLastRecipientPosition($commissionRecipient, $recipient);

                $this->storeCommissionRecipientPosition($commissionRecipient, $supervisor, $recipient);
            }

            if (!$commissionRecipient->repo->hasAccount()) {
                $user = $commissionRecipient->repo->createAccount();

                $commissionRecipient->userAccounts()->save($user);
            }
        }

        return true;
    }

    /*
     * Create a commission recipient record if none exists
     */
    /**
     * @param $recipient
     * @param $supervisor
     * @return CommissionRecepient|\Illuminate\Database\Eloquent\Model|null
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function storeCommissionRecipient($recipient, $supervisor)
    {
        $commissionRecipient = $this->getCommissionRecipientByEmail($recipient['email']);

        if (is_null($commissionRecipient)) {
            $commissionRecipient = new CommissionRecepient();
        }

        $commissionRecipient->crm_id = $recipient['crm_id'];
        $commissionRecipient->email = (array_key_exists('new_email', $recipient))
            ? $recipient['new_email'] : $recipient['email'];
        $commissionRecipient->phone = $recipient['phone'];
        $commissionRecipient->name = $recipient['name'];
        $commissionRecipient->recipient_type_id = $recipient['recipient_type_id'];
        $commissionRecipient->rank_id = $recipient['rank_id'];
        $commissionRecipient->phone_country_code = $recipient['phone_country_code'];
        $commissionRecipient->reports_to = ($supervisor) ? $supervisor->id : null;
        $commissionRecipient->reporting_date = $recipient['reporting_date'];
        $commissionRecipient->kra_pin = $recipient['kra_pin'];
        $commissionRecipient->account_number = $recipient['account_number'];
        $commissionRecipient->bank_code = $recipient['bank_code'];
        $commissionRecipient->branch_code = $recipient['branch_code'];
        $commissionRecipient->department_unit_id = $recipient['department_unit_id'];
        $commissionRecipient->active = $recipient['active'];

        $commissionRecipient->save();

        $commissionRecipient->repo->assignReferralCode();

        return $commissionRecipient;
    }

    /*
     * Store a new commission recipient position
     */
    public function storeCommissionRecipientPosition(CommissionRecepient $commissionRecipient, $supervisor, $recipient)
    {
        return CommissionRecepientPosition::create(
            [
                'commission_recipient_id' => $commissionRecipient->id,
                'recipient_type_id' => $recipient['recipient_type_id'],
                'rank_id' => $recipient['rank_id'],
                'reports_to' => ($supervisor) ? $supervisor->id : null,
                'start_date' => $recipient['start_date'],
                'end_date' => $recipient['end_date']
            ]
        );
    }

    /*
     * Verify if we need to create a new commission recipient position
     */
    public function compareLastRecipientPosition(CommissionRecepient $commissionRecipient, $supervisor, $recipient)
    {
        $lastPosition = $commissionRecipient->commissionRecipientPositions()->latest()->first();

        if (is_null($lastPosition)) {
            return true;
        }

        $lastPositionArray = array_except($lastPosition->toArray(), ['id', 'created_at', 'updated_at', 'deleted_at']);

        $newArray = [
            'commission_recipient_id' => $commissionRecipient->id,
            'recipient_type_id' => $recipient['recipient_type_id'],
            'rank_id' => $recipient['rank_id'],
            'reports_to' => ($supervisor) ? $supervisor->id : null,
            'start_date' => $recipient['start_date'],
            'end_date' => $recipient['end_date']
        ];

        return count(array_diff($lastPositionArray, $newArray)) > 0;
    }

    /*
     * Set an end date on the last commision recipient position
     */
    public function closeLastRecipientPosition($commissionRecipient, $recipient)
    {
        $lastRecipientPosition = $commissionRecipient->repo->lastRecipientPosition();

        if ($lastRecipientPosition) {
            $lastRecipientPosition->end_date = $recipient['start_date'];

            $lastRecipientPosition->save();
        }
    }
}
