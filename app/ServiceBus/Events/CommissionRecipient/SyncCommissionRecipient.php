<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 *
 */

namespace ServiceBus\Events\CommissionRecipient;

use App\Cytonn\Models\CommissionRecepient;

class SyncCommissionRecipient
{
    /*
     * Get the commission recipient trait
     */
    use CommissionRecepientTrait;

    /*
     * Handle the data from crm and sync the commission recipients
     */
    public function handle($data)
    {
        $users = json_decode($data[0], true);

        $this->syncRecipients($users);
    }

    /*
     * Sync the commission recipients
     */
    /**
     * @param $users
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function syncRecipients($users)
    {
        foreach ($users as $user) {
            $recipient = $this->saveRecipient(array_except($user, ['positions', 'user_positions']));

            $this->savePositions($recipient, $user['positions'], $user['user_positions']);

            if (!$recipient->repo->hasAccount()) {
                $recipient->repo->createAccount();
            }
        }
    }

    /*
     * Create or update the comm recipients
     */
    /**
     * @param $input
     * @return \App\Cytonn\Models\CommissionRecepient|\Illuminate\Database\Eloquent\Model|null|CommissionRecepientTrait
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function saveRecipient($input)
    {
        $input['reports_to'] = $this->getSupervisorId($input['reports_to'], $input['reports_to_crm_id']);

        return $this->saveCommissionRecipient($input);
    }

    /*
     * Save the positions for a commissionrecipient
     */
    public function savePositions(CommissionRecepient $recipient, $positions, $userPositions)
    {
        $recipient->commissionRecipientPositions()->delete();

        $recipient->commissionRecipientUserPositions()->delete();

        foreach ($positions as $position) {
            $position['reports_to'] = $this->getSupervisorId($position['reports_to'], $position['reports_to_crm_id']);

            $position['commission_recipient_id'] = $recipient->id;

            unset($position['reports_to_crm_id']);

            $this->storePosition($position);
        }

        foreach ($userPositions as $userPosition) {
            $userPosition['commission_recipient_id'] = $recipient->id;

            $this->storeUserPosition($userPosition);
        }

        $recipient->commissionRecipientPositions()->withTrashed()->whereNotNull('deleted_at')->forceDelete();
    }
}
