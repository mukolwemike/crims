<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Events\CommissionRecipient;

use App\Cytonn\Models\CommissionRecepient;

class GetCommissionRecipient
{
    /*
     * Handle the sending of commission recipients to crm
     */
    public function handle()
    {
        $commissionRecipients = CommissionRecepient::get();

        return $commissionRecipients->map(
            function ($commissionRecipient) {
                $recipientPosition = $commissionRecipient->present()->latestCommissionRecipient;

                return [
                    'name' => $commissionRecipient->name,
                    'email' => $commissionRecipient->email,
                    'phone' => $commissionRecipient->phone,
                    'phone_country_code' => $commissionRecipient->phone_country_code,
                    'rank_id' => $commissionRecipient->rank_id,
                    'user_type_id' => $commissionRecipient->recipient_type_id,
                    'start_date' => ($recipientPosition) ? $recipientPosition->start_date : null
                ];
            }
        );
    }
}
