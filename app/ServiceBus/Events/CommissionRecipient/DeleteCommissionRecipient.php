<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Events\CommissionRecipient;

class DeleteCommissionRecipient
{
    /*
     * Get the commission recepient trait
     */
    use CommissionRecepientTrait;

    /*
     * Handle the client compliance summary
     */
    public function handle($data)
    {
        $data = json_decode($data[0], true);

        $recipient = $data[0];

        $all = $data[1];

        return $this->deleteCommissionRecipient($recipient, $all);
    }

    /*
     * Delete the commission recepient
     */
    public function deleteCommissionRecipient($recipients, $all)
    {
        foreach ($recipients as $recipient) {
            $commissionRecipient = $this->getCommissionRecipientByEmail($recipient['email']);

            if (is_null($commissionRecipient)) {
                return null;
            }

            $supervisor = $this->checkSupervisor($recipient);

            $this->deleteRecepientPosition($recipient, $commissionRecipient, $supervisor, $all);

            return true;
        }
    }

    /*
     * Get the commission recepient position based on the supplied data
     */
    public function deleteRecepientPosition($recipient, $commissionRecipient, $supervisor, $all)
    {
        $position = $commissionRecipient->commissionRecipientPositions();

        if ($all == 1) {
            $position->forceDelete();

            return;
        }

        $position->where('recipient_type_id', $recipient['recipient_type_id'])
            ->where('rank_id', $recipient['rank_id'])
            ->where('start_date', $recipient['start_date'])
            ->where('end_date', $recipient['end_date']);

        if ($supervisor) {
            $position->where('reports_to', $supervisor->id);
        }

        $position->forceDelete();

        return;
    }
}
