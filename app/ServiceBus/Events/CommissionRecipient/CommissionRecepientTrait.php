<?php
/**
 *
 * @author: Timothy Kimathi <timsnky@gmail.com>
 *
 * Project: crims.
 */

namespace ServiceBus\Events\CommissionRecipient;

use App\Cytonn\Models\CommissionRecepient;
use App\Cytonn\Models\CommissionRecepientPosition;
use Cytonn\Models\Investment\CommissionRecepientUserPosition;

/**
 * Trait CommissionRecepientTrait
 * @package ServiceBus\Events\CommissionRecipient
 */
trait CommissionRecepientTrait
{
    /**
     * @param $email
     * @param $crmId
     * @return mixed|null
     */
    private function getSupervisorId($email, $crmId)
    {
        $recipient = $this->getCommissionRecipient($email, $crmId);

        return $recipient ? $recipient->id : null;
    }

    /**
     * @param $email
     * @param $crmId
     * @return \Illuminate\Database\Eloquent\Model|null|CommissionRecepientTrait
     */
    public function getCommissionRecipient($email, $crmId)
    {
        if ($crmId && $crmId != '') {
            $supervisor = $this->getCommissionRecipientByCrmId($crmId);

            if ($supervisor) {
                return $supervisor;
            }
        }

        if ($email && $email != '') {
            $supervisor = $this->getCommissionRecipientByEmail($email);

            if ($supervisor) {
                return $supervisor;
            }
        }

        return null;
    }

    /**
     * @param $id
     * @return CommissionRecepient|\Illuminate\Database\Eloquent\Model|null
     */
    private function getCommissionRecipientByCrmId($id)
    {
        return CommissionRecepient::where('crm_id', $id)->first();
    }

    /**
     * @param $email
     * @return CommissionRecepient|\Illuminate\Database\Eloquent\Model|null
     */
    public function getCommissionRecipientByEmail($email)
    {
        return CommissionRecepient::where('email', $email)->first();
    }

    /**
     * @param $input
     * @return CommissionRecepient|\Illuminate\Database\Eloquent\Model|null|CommissionRecepientTrait
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function saveCommissionRecipient($input)
    {
        $recipient = $this->getCommissionRecipient($input['email'], $input['crm_id']);

        if ($recipient) {
            $recipient->update($input);

            return $recipient->fresh();
        } else {
            $recipient = CommissionRecepient::create($input);

            $recipient->repo->assignReferralCode();

            return $recipient;
        }
    }

    /**
     * @param $input
     * @return CommissionRecepientPosition|\Illuminate\Database\Eloquent\Model
     */
    public function storePosition($input)
    {
        $position = CommissionRecepientPosition::where($input)->withTrashed()->first();

        if ($position) {
            $position->restore();

            return $position->fresh();
        }

        return CommissionRecepientPosition::create($input);
    }

    /**
     * @param $input
     * @return mixed
     */
    public function storeUserPosition($input)
    {
        $position = CommissionRecepientUserPosition::where($input)->withTrashed()->first();

        if ($position) {
            $position->restore();

            return $position->fresh();
        }

        return CommissionRecepientUserPosition::create($input);
    }
}
