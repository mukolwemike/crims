<?php

namespace App\Events\Users;

use App\Cytonn\Models\User;
use Cytonn\Authorization\Permission;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserAssignedPermission
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /*
     * Get the variables for the event
     */
    public $user;

    public $permission;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Permission $permission)
    {
        $this->user = $user;

        $this->permission = $permission;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
