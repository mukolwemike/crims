<?php
/**
 * Created by PhpStorm.
 * User: molukaka
 * Date: 2019-03-07
 * Time: 11:02
 */

namespace App\Events\Realestate;

use App\Cytonn\Models\RealEstate\RealEstateInstruction;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ClientHoldingPaymentFilled extends Event
{
    use SerializesModels;

    public $instruction;

    public function __construct(RealEstateInstruction $instruction)
    {
        $this->instruction = $instruction;
    }

    public function broadcastOn()
    {
        return [];
    }
}
