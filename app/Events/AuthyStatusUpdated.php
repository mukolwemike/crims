<?php

namespace App\Events;

use App\Cytonn\Models\ClientUser as User;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AuthyStatusUpdated extends Event implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'AuthyStatusUpdated';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     */
    public function broadcastOn()
    {
        return new Channel('authy-one-touch-for-' . $this->user->present()->cleanUsername);
    }

    /**
     * @return array
     * @throws \Laracasts\Presenter\Exceptions\PresenterException
     */
    public function broadcastWith()
    {
        return [
            'user' => [
                'username' => $this->user->present()->cleanUsername,
                'authy_uuid' => $this->user->authy_uuid,
                'token' => $this->user->createToken('CRIMS Mobile app')->accessToken
            ],
        ];
    }
}
