<?php

namespace App\Events;

use App\Cytonn\Models\ClientFilledInvestmentApplication;

class InvestmentApplicationSaved extends Event
{
    public $application;

    public function __construct(ClientFilledInvestmentApplication $application)
    {
        $this->application = $application;
    }

    public function broadcastOn()
    {
        return [];
    }
}
