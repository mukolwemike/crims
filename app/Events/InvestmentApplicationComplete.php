<?php

namespace App\Events;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use Illuminate\Queue\SerializesModels;

class InvestmentApplicationComplete extends Event
{
    use  SerializesModels;

    public $application;

    public function __construct(ClientFilledInvestmentApplication $application)
    {
        $this->application = $application;
    }

    public function broadcastOn()
    {
        return [];
    }
}
