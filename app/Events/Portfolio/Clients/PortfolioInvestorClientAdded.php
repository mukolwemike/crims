<?php

namespace App\Events\Portfolio\Clients;

use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestor;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PortfolioInvestorClientAdded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /*
     * Specify the data for the event
     */
    public $data;
    public $portfolioSecurity;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, PortfolioSecurity $portfolioSecurity)
    {
        $this->data = $data;
        $this->portfolioSecurity = $portfolioSecurity;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
