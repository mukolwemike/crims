<?php

namespace App\Events\Portfolio\Actions;

use App\Cytonn\Models\PortfolioInvestmentRepayment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PortfolioRepaymentHasBeenRemoved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $repayment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PortfolioInvestmentRepayment $repayment)
    {
        $this->repayment = $repayment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
