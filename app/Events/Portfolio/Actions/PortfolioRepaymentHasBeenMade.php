<?php

namespace App\Events\Portfolio\Actions;

use App\Cytonn\Models\Portfolio\DepositHolding;
use App\Cytonn\Models\PortfolioInvestmentRepayment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class PortfolioRepaymentHasBeenMade
 * @package App\Events\Portfolio\Actions
 */
class PortfolioRepaymentHasBeenMade
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var DepositHolding
     */
    public $depositHolding;

    /**
     * @var PortfolioInvestmentRepayment
     */
    public $portfolioInvestmentRepayment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        DepositHolding $depositHolding,
        PortfolioInvestmentRepayment $portfolioInvestmentRepayment
    ) {
        $this->depositHolding = $depositHolding;

        $this->portfolioInvestmentRepayment = $portfolioInvestmentRepayment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
