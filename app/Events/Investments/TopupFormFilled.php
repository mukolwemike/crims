<?php

namespace App\Events\Investments;

use App\Cytonn\Models\ClientTopupForm;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class TopupFormFilled extends Event
{
    use SerializesModels;

    public $form;

    public function __construct(ClientTopupForm $form)
    {
        $this->form = $form;
    }

    public function broadcastOn()
    {
        return [];
    }
}
