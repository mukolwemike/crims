<?php

namespace App\Events\Investments;

use App\Cytonn\Models\ClientInvestmentInstruction;
use App\Events\Event;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InvestmentActionPerformed extends Event
{
    use SerializesModels, InteractsWithSockets, Dispatchable;

    public $instruction;

    public function __construct(ClientInvestmentInstruction $instruction)
    {
        $this->instruction = $instruction;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
