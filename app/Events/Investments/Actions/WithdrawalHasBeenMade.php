<?php

namespace App\Events\Investments\Actions;

use App\Cytonn\Models\ClientInvestment;
use Cytonn\Models\ClientInvestmentWithdrawal;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WithdrawalHasBeenMade
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var ClientInvestment
     */
    public $investment;
    /**
     * @var ClientInvestmentWithdrawal
     */
    public $withdrawal;

    /**
     * Create a new event instance.
     *
     * @param ClientInvestment           $investment
     * @param ClientInvestmentWithdrawal $withdrawal
     */
    public function __construct(ClientInvestment $investment, ClientInvestmentWithdrawal $withdrawal)
    {
        $this->investment = $investment;
        $this->withdrawal = $withdrawal;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
