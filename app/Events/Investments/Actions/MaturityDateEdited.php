<?php

namespace App\Events\Investments\Actions;

use App\Cytonn\Models\ClientInvestment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MaturityDateEdited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $investment;
    public $oldInvestment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ClientInvestment $investment, ClientInvestment $oldInvestment)
    {
        $this->investment = $investment;
        $this->oldInvestment = $oldInvestment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
