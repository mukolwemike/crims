<?php

namespace App\Events\Investments\Actions;

use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UnitFundPurchaseHasBeenMade
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $purchase;

    public $instruction;

    /**
     * Create a new event instance.
     *
     * @param UnitFundPurchase $purchase
     * @param UnitFundInvestmentInstruction $instruction
     */
    public function __construct(UnitFundPurchase $purchase, UnitFundInvestmentInstruction $instruction)
    {
        $this->purchase = $purchase;

        $this->instruction = $instruction;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
