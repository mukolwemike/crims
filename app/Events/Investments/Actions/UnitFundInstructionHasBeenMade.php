<?php

namespace App\Events\Investments\Actions;

use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UnitFundInstructionHasBeenMade
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $instruction;

    /**
     * Create a new event instance.
     *
     * @param UnitFundInvestmentInstruction $instruction
     */
    public function __construct(UnitFundInvestmentInstruction $instruction)
    {
        $this->instruction = $instruction;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
