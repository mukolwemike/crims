<?php

namespace App\Events\Utilities;

use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UtilityPaymentHasFailed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $instruction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UtilityBillingInstructions $instruction)
    {
        $this->instruction = $instruction;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
