<?php
/**
 * Created by PhpStorm.
 * User: emwazonga
 * Date: 05/07/2018
 * Time: 16:08
 */

namespace App\Events;

use App\Cytonn\Models\ClientUser as User;
use Cytonn\Presenters\UserPresenter;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AuthyConfirmPasswordReset extends Event implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('authy-one-touch-password-reset-'. $this->user->present()->cleanUsername);
    }

    public function broadcastWith()
    {
        return [
            'user' => [
                'username' => $this->user->present()->cleanUsername,
                'authy_uuid' => $this->user->authy_uuid,
            ],
        ];
    }
}
