<?php

namespace App\Events;

use App\Cytonn\Models\ClientFilledInvestmentApplication;
use App\Cytonn\Models\ClientUploadedKyc;
use Illuminate\Queue\SerializesModels;

class KycDocumentUploaded extends Event
{
    use SerializesModels;

    public $document;

    public $application;

    public function __construct(ClientFilledInvestmentApplication $application, ClientUploadedKyc $document)
    {
        $this->document = $document;
        $this->application = $application;
    }

    public function broadcastOn()
    {
        return [];
    }
}
