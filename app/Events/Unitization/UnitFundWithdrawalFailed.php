<?php

namespace App\Events\Unitization;

use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UnitFundWithdrawalFailed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $instruction;

    public $reason;

    /**
     * Create a new event instance.
     *
     * @param UnitFundInvestmentInstruction $instruction
     * @param string $reason
     */
    public function __construct(UnitFundInvestmentInstruction $instruction, string $reason)
    {
        $this->instruction = $instruction;
        $this->reason = $reason;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
