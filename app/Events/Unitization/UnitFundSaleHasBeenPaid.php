<?php

namespace App\Events\Unitization;

use App\Cytonn\Models\BankInstruction;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UnitFundSaleHasBeenPaid
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $instruction;
    public $bankInstruction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UnitFundInvestmentInstruction $instruction, BankInstruction $bankInstruction)
    {
        $this->instruction = $instruction;
        $this->bankInstruction = $bankInstruction;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
