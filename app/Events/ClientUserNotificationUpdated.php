<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 12/10/18
 * Time: 12:38 PM
 */

namespace App\Events;

use App\Cytonn\Models\ClientUser;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ClientUserNotificationUpdated extends Event implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $clientUser;

    public function __construct(ClientUser $clientUser)
    {
        $this->clientUser = $clientUser;
    }

    public function broadcastAs()
    {
        return 'ClientUserNotificationUpdated';
    }

    public function broadcastOn()
    {
        return new PrivateChannel('notification');
    }

    public function broadcastWith()
    {
        return [
            'user' => [
                'name' => $this->clientUser->username,
                'uuid' => $this->clientUser->uuid,
            ],

            'message' => 'Notification updated'
        ];
    }
}
