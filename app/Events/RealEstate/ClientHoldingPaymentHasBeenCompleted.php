<?php

namespace App\Events\RealEstate;

use App\Cytonn\Models\UnitHolding;
use App\Events\Event;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ClientHoldingPaymentHasBeenCompleted extends Event
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $holding;

    public function __construct(UnitHolding $holding)
    {
        $this->holding = $holding;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
