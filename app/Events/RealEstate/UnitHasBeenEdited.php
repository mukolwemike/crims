<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 4/2/19
 * Time: 12:06 PM
 */

namespace App\Events\RealEstate;

use App\Cytonn\Models\UnitHolding;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UnitHasBeenEdited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $holding;

    public $data;

    public function __construct(UnitHolding $holding, $data = [])
    {
        $this->holding = $holding;

        $this->data = $data;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
