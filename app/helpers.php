<?php
/**
 * Date: 02/05/2017
 * Time: 18:21
 *
 * @author Mwaruwa Chaka <mchaka@cytonn.com>
 * Project: crims
 * Cytonn Technologies
 */

use Amp\Deferred;
use Amp\Loop;
use App\Cytonn\Models\Currency;
use App\Cytonn\Models\RealEstatePaymentPlan;
use App\Cytonn\Models\Unitization\UnitFundTransfer;
use App\Cytonn\Models\User;
use App\Jobs\ProcessQueuedClosure;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Cytonn\Currencies\Converter\Convert;
use Cytonn\Investment\FundManager\FundManagerScope;
use SuperClosure\Serializer;

if (!function_exists('secEnv')) {
    function secEnv($key)
    {
        return getenv($key);
    }
}

if (!function_exists('accessModule')) {
    function accessModule($name)
    {
        return app(FundManagerScope::class)->getSelectedFundManager()->hasModule($name);
    }
}

if (!function_exists('fundManager')) {
    function fundManager()
    {
        return app(FundManagerScope::class)->getSelectedFundManager();
    }
}

if (!function_exists('take_back')) {
    function take_back($e, $flash = true)
    {
        if ($flash && property_exists($e, 'message')) {
            try {
                \Flash::error(@$e->getMessage());
            } catch (\Exception $e) {
            }
        }

        try {
            return redirect()->back()->withInput();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
}

if (!function_exists('fortrabbitSecret')) {
    function fortrabbitSecret($key)
    {
        try {
            //            if (env('APP_ENV') != 'production') return null;

            $result = @json_decode(file_get_contents($_SERVER['APP_SECRETS']), true);

            if (!$result) {
                return null;
            }

            foreach (explode('.', $key) as $section) {
                $result = $result[$section];
            }

            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }
}

if (!function_exists('setting')) {
    function setting($name, $date = null)
    {
        if (is_null($date)) {
            return \App\Cytonn\Models\Setting::where('key', $name)->latest()->first()->value;
        }

        return \App\Cytonn\Models\Setting::where('key', $name)
            ->where('date', '>=', $date)
            ->latest()->first()->value;
    }
}

/*
 * Trim the text in a  string
 */
function trimText($input, $length, $ellipses = true)
{
    if (strlen($input) <= $length) {
        return $input;
    }

    $last_space = strrpos(substr($input, 0, $length), ' ');

    $trimmed_text = substr($input, 0, $last_space);

    if ($ellipses) {
        $trimmed_text .= '...';
    }

    return $trimmed_text;
}

/**
 * Trim phone number
 * @param $number
 * @return mixed
 */
function trimPhoneNumber($number)
{
    return str_replace(' ', '', trim(preg_replace('/\D/', '', trim($number))));
}

/*
 * Format a slug for display
 */
function formatSlug($input)
{
    return ucfirst(str_replace('_', ' ', $input));
}

function systemEmailGroups(array $names)
{
    $groups = new \Illuminate\Support\Collection();

    foreach ($names as $name) {
        $groups = $groups->merge(config('system.emails.' . $name));
    }

    return (new \Cytonn\Users\UserRepository())->removeInactive($groups->unique()->all());
}


function hasPermission($name)
{
    return \Auth::user()->isAbleTo($name);
}

function allowed($permission)
{
    return \Auth::user()->isAbleTo($permission);
}

function serializeClosure(Closure $closure)
{
    $serializer = new \SuperClosure\Serializer();

    return $serializer->serialize($closure);
}

function unserializeClosure($serialized)
{
    $serializer = new \SuperClosure\Serializer();

    return $serializer->unserialize($serialized);
}

function sendToQueue($closure, $key = null, $overwrite = false, $queue = false, $args = [], $delay = null)
{
    $serialized = (new Serializer())->serialize($closure);

    $job = new ProcessQueuedClosure($serialized, $key, $overwrite);

    if ($queue) {
        $job->onQueue($queue);
    }

    if ($delay) {
        $job->delay($delay);
    }

    if (isset($args['tries'])) {
        $job->tries = $args['tries'];
    }

    dispatch($job);
}

function async($closure)
{
    $deferred = new Deferred;

    Loop::run(
        function () use ($closure) {
            Loop::defer($closure);
        }
    );

    return $deferred->promise();
}

/*
 * Function to convert a given investment value to another
 * @return float
 */
/**
 * @param $amount
 * @param Currency $currency
 * @param null $date
 * @param Currency|null $baseCurrency
 * @return float|int
 */
function convert($amount, Currency $currency, $date = null, Currency $baseCurrency = null)
{
    $date = ($date) ? Carbon::parse($date) : Carbon::now();

    if (!$baseCurrency) {
        $baseCurrency = Convert::$system_base_currency;
    }

    if (!$baseCurrency) {
        $code = config('system.base_currency');

        $baseCurrency = Currency::where('code', $code)->first();

        Convert::$system_base_currency = $baseCurrency;
    }

    if ($currency->id == $baseCurrency->id) {
        return $amount;
    }

    $rate = (new Convert($baseCurrency))->enableCaching(60)->read($currency, $date);

    return $amount * $rate;
}


function getBaseCurrency($date = null, Currency $baseCurrency = null)
{

    $date = ($date) ? Carbon::parse($date) : Carbon::now();

    if (!$baseCurrency) {
        $baseCurrency = Convert::$system_base_currency;
    }

    if (!$baseCurrency) {
        $code = config('system.base_currency');

        $baseCurrency = Currency::where('code', $code)->first();

        Convert::$system_base_currency = $baseCurrency;
    }

    return $baseCurrency;
}

/*
 * Get the date range for the months
 */
function getDateRange(Carbon $from, Carbon $to)
{
    if ($from->gt($to)) {
        return null;
    }

    $from = $from->copy()->startOfDay();

    $to = $to->copy()->startOfDay();

    $step = CarbonInterval::month(1);

    $period = new DatePeriod($from, $step, $to);

    $range = [];

    foreach ($period as $day) {
        $range[] = new Carbon($day);
    }

    return !empty($range) ? $range : null;
}

function memoryLimit($val)
{
    ini_set('memory_limit', $val);
}

if (!function_exists('reportException')) {
    function reportException(Exception $exception)
    {
        app(Illuminate\Contracts\Debug\ExceptionHandler::class)->report($exception);
    }
}

if (!function_exists('roundDown')) {
    function roundDown($value, $precision)
    {
        $multiplier = 10 ** $precision;

        return (float)(floor($multiplier * $value) / $multiplier);
    }
}

if (!function_exists('roundOffUnits')) {
    function roundOffUnits($units)
    {
        return roundDown($units, 2);
    }
}

/*
 * Get the result of a value divided by another value then round off to a number of decimal places
 */
function dividend($numerator, $denominator, $precision = 3)
{
    return ($denominator != 0) ? round(($numerator / $denominator), $precision) : 0;
}

/*
 * Get a percentage of a given dividend
 */
function percentage($numerator, $denominator, $precision = 3)
{
    return ($denominator != 0) ? round((($numerator / $denominator) * 100), $precision) : 0;
}

if (!function_exists('get_optional')) {
    function get_optional($object, $property, $default = null)
    {
        if (is_array($object) && isset($object[$property])) {
            return $object[$property];
        }

        if (!is_object($object)) {
            return $default;
        }

        if (property_exists($object, $property)) {
            return $object->{$property};
        }

        return $default;
    }
}

if (!function_exists('carbon')) {
    function carbon($time = null, $tz = null)
    {
        return new \Carbon\Carbon($time, $tz);
    }
}

if (!function_exists('isTruthy')) {
    function isTruthy($val)
    {
        if (is_bool($val)) {
            return $val;
        }

        if ($val === 'true' || $val === '1' || $val === 1) {
            return true;
        }

        if ($val === 'false' || $val === '0' || $val === 0) {
            return false;
        }

        return boolval($val);
    }
}

if (!function_exists('asyncMap')) {
    function asyncMap($items, Closure $closure)
    {
        $async = new \Cytonn\System\Processing\Async();

        return $async->map($items, $closure);
    }
}

if (!function_exists('delaySave')) {
    function delaySave($model, $data)
    {
        $job = new \App\Jobs\System\DelayedSave($model, $data);

        $job->onQueue(config('queue.priority.low'));

        dispatch($job);
    }
}

/*
 * Format a file name
 */
if (!function_exists('formatFileName')) {
    function formatFileName($name, $slice = false)
    {
        $fileName = ucfirst(str_replace(' ', '_', $name));

        return $slice ? substr($fileName, 0, 30) : $fileName;
    }
}

/*
 * Check if empty or null
 */
if (!function_exists('isNotEmptyOrNull')) {
    function isNotEmptyOrNull($record)
    {
        return ((!is_null($record)) && ($record != ''));
    }
}

if (!function_exists('isEmptyOrNull')) {
    function isEmptyOrNull($record)
    {
        return !isNotEmptyOrNull($record);
    }
}

/*
 * Check if empty or null
 */
if (!function_exists('roundToHundred')) {
    function roundToHundred($amount)
    {
        return round($amount / 100, 0) * 100;
    }
}

/*
 * Check if empty or null
 */
if (!function_exists('getSystemUser')) {
    /**
     * @return User|\Illuminate\Database\Eloquent\Model|null
     * @throws Exception
     */
    function getSystemUser()
    {
        if (cache()->has('crims_system_user')) {
            return cache()->get('crims_system_user');
        }

        $user = User::where('username', 'system')->first();

        cache(['crims_system_user' => $user], Carbon::now()->addHours(6));

        return $user;
    }
}

/*
 * Get the real estate cash plan
 */
if (!function_exists('getRealEstateCashPlan')) {

    /**
     * @return RealEstatePaymentPlan|\Illuminate\Database\Eloquent\Model|null
     * @throws Exception
     */
    function getRealEstateCashPlan()
    {
        if (cache()->has('crims_real_estate_cash_plan')) {
            return cache()->get('crims_real_estate_cash_plan');
        }

        $plan = RealEstatePaymentPlan::where('slug', 'cash')->first();

        cache(['crims_real_estate_cash_plan' => $plan], Carbon::now()->addHours(6));

        return $plan;
    }
}

if (!function_exists('glueWithAnd')) {
    function glueWithAnd(array $pieces)
    {
        $count = count($pieces);
        $result = '';

        for ($i = 0; $i < $count; $i++) {
            if ($i == 0) {
                $result = $pieces[$i];
            } elseif ($i == $count - 1) {
                $result = $result . ' and ' . $pieces[$i];
            } else {
                $result = $result . ', ' . $pieces[$i];
            }
        }

        return $result;
    }
}

if (!function_exists('logRollbar')) {
    function logRollbar($log)
    {
        \Rollbar\Rollbar::init(config('services.rollbar'));

        Rollbar\Rollbar::info($log);
    }
}

if (!function_exists('isLocal')) {
    function isLocal()
    {
        return in_array(getenv('APP_ENV'), ['local', 'testing']);
    }
}

function hasInterTransfer($client)
{
    $clientCodes = [2169,14132,13131,11378,14229,13578,10637,1912,9350,14940,11529,196,17319,5886,17197];

    if (in_array($client->client_code, $clientCodes)) {
        return false;
    }

    return UnitFundTransfer::where('created_at', '>', '2020-01-22 00:00:00')
        ->where(function ($q) use ($client) {
            $q->where('transferer_id', $client->id)->orWhere('transferee_id', $client->id);
        })->exists();
}

function splitPhoneNumber($phone)
{
    //multiple phones separated by comma or /
    if (str_contains($phone, ',')) {
        $phone = trim(explode(',', $phone)[0]);
    }

    if (str_contains($phone, '/')) {
        $phone = trim(explode('/', $phone)[0]);
    }

    $code = '+254';
    $phone = trim($phone);
    $phone_s = $phone;

    if (starts_with($phone, '+254') ||
        starts_with($phone, '254') ||
        starts_with($phone, '0')
    ) {
        return splitKenyanNumber($phone);
    }

    if (str_contains($phone, ' ')) {
        $exploded = explode(' ', $phone);
        $code = $exploded[0];

        unset($exploded[0]);

        return [$code, implode("", $exploded)];
    }

    if (starts_with($phone, 0)) {
        $phone_s = ltrim($phone, '0');
    }

    if (starts_with($phone, '+')) {
        $code = substr($phone, 0, 4);
        $phone_s = substr($phone, 4, strlen($phone));
    }


    return [$code, $phone_s];
}

function splitKenyanNumber($phone)
{
    if (starts_with($phone, '0')) {
        return ['+254', ltrim($phone, '0')];
    }

    if (starts_with($phone, '+254') || starts_with($phone, '+')) {
        return [
            substr($phone, 0, 4),
            substr($phone, 4)
        ];
    }

    return [
        '+'.substr($phone, 0, 3),
        substr($phone, 3)
    ];
}

function cleanPhoneNumber($phone)
{
    $new_phone = str_replace(' ', '', $phone);
    $new_phone = ltrim($new_phone, '+');
    $new_phone = ltrim($new_phone, '254');
    $new_phone = ltrim($new_phone, '0');

    return '254' . $new_phone;
}
