<?php

namespace App\Listeners\Approvals;

use App\Cytonn\Models\ClientTransactionApproval;
use App\Cytonn\Models\PortfolioTransactionApproval;
use App\Events\Approvals\ClientTransactionApproved as Event;
use Cytonn\Logger\Logger;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientTransactionApproved
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Create the event listener.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        //
        $this->logger = $logger;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        /**
         * @var ClientTransactionApproval | PortfolioTransactionApproval
         */
        $approval = $event->approval;

        $desc = "Approved {$approval->present()->type} ";

        $desc = $approval->client ? $desc. 'for '.ClientPresenter::presentFullNames($approval->client_id) : $desc;

        $desc = $approval->institution ? $desc. ' for '.$approval->institution->name : $desc;

        $this->logger->addActivityLog(
            $desc,
            $approval->url(),
            \Auth::id(),
            $approval->id,
            get_class($approval),
            $approval->fund_manager_id
        );
    }
}
