<?php

namespace App\Listeners\Unitization;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Events\Unitization\UnitFundWithdrawalFailed as Failure;
use App\Jobs\USSD\SendMessages;
use App\Mail\Mail;
use Cytonn\Presenters\ClientPresenter;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Arr;

class UnitFundWithdrawalFailed implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UnitFundWithdrawalFailed  $event
     * @return void
     */
    public function handle(Failure $event)
    {
        $reason = $event->reason;
        $instruction = $event->instruction;

        \App\Cytonn\System\Locks\Facade\Lock::executeWithinLock(function () use ($event, $reason) {
            $instruction = $event->instruction->fresh();
            if ($instruction->cancelled || $instruction->approval) {
                return;
            }

            $this->cancel($instruction);

            $this->notifyClient($instruction, $reason);
        }, 'cancel_instruction_on_failure_'.$instruction->id, 300);
    }

    private function cancel(UnitFundInvestmentInstruction $instruction)
    {
        $instruction->update(['cancelled' => true]);
    }

    private function notifyClient(UnitFundInvestmentInstruction $instruction, $reason)
    {
        $client = $instruction->client;
        $product = $instruction->unitFund->name;

        Mail::make()
            ->from(['operations@cytonn.com' => ucfirst($product)])
            ->to($emails = $client->getContactEmailsArray())
            ->cc(systemEmailGroups(['operations_group']))
            ->bcc(systemEmailGroups(['administrators']))
            ->subject('Withdrawal request could not be processed')
            ->view(
                'emails.unitization.unit_fund_withdrawal_failed',
                ['reason' => $reason, 'client' => $client, 'product' => $product]
            )
            ->send();

        if (count($emails) === 0) {
            $this->notifyAsSMS($client, $reason);
        }
    }

    private function notifyAsSMS(Client$client, $reason)
    {
        \SMS::send(
            Arr::first($client->getContactPhoneNumbersArray()),
            'Your withdrawal request could not be processed.'.PHP_EOL.$reason
            .PHP_EOL.' Call us on '.SendMessages::CYTONN_PHONE.'.'
        );
    }
}
