<?php

namespace App\Listeners\Unitization;

use App\Cytonn\Presenters\General\AmountPresenter;
use App\Events\Unitization\UnitFundSaleHasBeenPaid;
use Carbon\Carbon;
use Cytonn\Exceptions\CRIMSGeneralException;
use Cytonn\Investment\BankDetails;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\DatePresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Support\Arr;

class WhenUnitFundSaleHasBeenPaid
{
    const OUTFLOW_ACCOUNT_SWIFT = 'SCBLKENXXXX';

    protected $instruction;

    protected $bankInstruction;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UnitFundSaleHasBeenPaid  $event
     * @return void
     */
    public function handle(UnitFundSaleHasBeenPaid $event)
    {
        $this->instruction = $event->instruction;

        $this->bankInstruction = $event->bankInstruction;

        $client = $this->instruction->client;

        $unitFund = $this->instruction->unitFund;

        $toEmails = $client->getContactEmailsArray();

        if (count($toEmails) == 0) {
            $this->sendSmsNotification();

            return;
        }

        $sender = config('system.emails.operations_group');

        $ccEmails = [$sender];

        $fa = $client->getLatestFA('units');

        if ($fa) {
            $ccEmails[] = $fa->email;
        }

        Mailer::compose()
            ->from([$sender => ucfirst($unitFund->name)])
            ->subject("Payment Notification - " . ClientPresenter::presentJointFirstNames($client->id))
            ->to($toEmails)
            ->cc($ccEmails)
            ->bcc(config('system.administrators'))
            ->view('emails.unitization.unit_sale_payment_notification', [
                'client' => $client,
                'unitSale' => $this->instruction->sale,
                'deliveryDetails' => $this->getDeliveryDetails(),
                'unitFund' => $unitFund
            ])
            ->queue();
    }

    /**
     * @return string
     */
    private function getDeliveryDetails()
    {
        $payment = $this->bankInstruction->custodialTransaction->clientPayment;

        $bank = new BankDetails(null, $payment->client, $this->bankInstruction->clientBankAccount);

        if ($bank->paymentType() == 'mpesa') {
            return (object)[
                'type' => 'MPESA',
                'time' => '1 hour'
            ];
        }

        if ($bank->swiftCode() == static::OUTFLOW_ACCOUNT_SWIFT) {
            return (object)[
                'type' => 'Bank Transfer',
                'time' => '12 hours'
            ];
        }

        $today = Carbon::now();

        $time = $today < $today->copy()->startOfDay()->addHours(14) && $today->isWeekday()
            ? "12 hours" : "next business day";

        return (object)[
            'type' => 'RTGS',
            'time' => $time
        ];
    }

    private function sendSmsNotification()
    {
        $client = $this->instruction->client;

        $phone = $client->individualPhoneNumber();

        if (!$phone) {
            $numbers = $client->getContactPhoneNumbersArray();

            if (Arr::first($numbers)) {
                $phone = Arr::first($numbers);
            }
        }

        if (isEmptyOrNull($phone)) {
            return;
        }

        $number = AmountPresenter::currency($this->instruction->sale->number);

        $fundName = $this->instruction->unitFund->name;

        $expectedDeliveryTime = $this->getDeliveryDetails()->time;

        $message = "We have processed your sale of $number units in $fundName. You'll receive " .
                    "the funds within $expectedDeliveryTime. Call us on 0709101200.";

        \SMS::queue($phone, $message);
    }
}
