<?php

namespace App\Listeners\Unitization;

use App\Cytonn\Clients\ClientInstructionApprovalsRepository;
use App\Events\Unitization\UnitFundTransferPerformed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WhenUnitFundTransferPerformed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UnitFundTransferPerformed  $event
     * @return void
     */
    public function handle(UnitFundTransferPerformed $event)
    {
        (new ClientInstructionApprovalsRepository())->transferApprovals($event->instruction);
    }
}
