<?php

namespace App\Listeners\Unitization;

use App\Events\Unitization\UnitFundSaleHasBeenRemoved;
use Cytonn\Investment\Tax\WithholdingTaxRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WhenUnitFundSaleHasBeenRemoved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UnitFundSaleHasBeenRemoved  $event
     * @return void
     */
    public function handle(UnitFundSaleHasBeenRemoved $event)
    {
        (new WithholdingTaxRepository())->reverseForUnitSale($event->sale);
    }
}
