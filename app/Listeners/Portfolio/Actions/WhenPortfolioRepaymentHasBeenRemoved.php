<?php

namespace App\Listeners\Portfolio\Actions;

use App\Cytonn\Models\PortfolioInvestmentRepayment;
use App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenRemoved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WhenPortfolioRepaymentHasBeenRemoved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PortfolioRepaymentHasBeenRemoved  $event
     * @return void
     */
    public function handle(PortfolioRepaymentHasBeenRemoved $event)
    {
        $this->updateDepositHolding($event->repayment);
    }

    private function updateDepositHolding(PortfolioInvestmentRepayment $repayment)
    {
        $holding = $repayment->investment;

        if ($holding->withdrawn) {
            $holding->update([
                'withdraw_amount' => null,
                'withdrawal_date' => null,
                'withdrawn' => null,
                'withdrawn_by' => null,
                'withdraw_transaction_approval_id' => null,
                'partial_withdraw' => null,
                'custodial_withdraw_transaction_id' => null,
                'rolled' => null
            ]);
        }
    }
}
