<?php

namespace App\Listeners\Portfolio\Actions;

use App\Events\Portfolio\Actions\PortfolioRepaymentHasBeenMade;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;

/**
 * Class WhenPortfolioRepaymentHasBeenMade
 * @package App\Listeners\Portfolio\Actions
 */
class WhenPortfolioRepaymentHasBeenMade
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PortfolioRepaymentHasBeenMade $event
     * @return void
     * @throws ClientInvestmentException
     */
    public function handle(PortfolioRepaymentHasBeenMade $event)
    {
        $this->validateRepaymentAmount($event);
        $this->checkFinalRepayment($event);
        $this->validateRepaymentDate($event);
    }

    /**
     * @param PortfolioRepaymentHasBeenMade $e
     * @throws ClientInvestmentException
     */
    private function validateRepaymentAmount(PortfolioRepaymentHasBeenMade $e)
    {
        $depositHolding = $e->depositHolding->fresh();

        $val = $depositHolding->calculate($e->portfolioInvestmentRepayment->date, false)
            ->value();

        $pri = $depositHolding->calculate($e->portfolioInvestmentRepayment->date, false)->value();

        $val1 = $depositHolding->calculate($e->portfolioInvestmentRepayment->date, true)->value();

        $pri1 = $depositHolding->calculate($e->portfolioInvestmentRepayment->date, true)->value();

        if ($val < (-1) || $val1 < -1 || $pri1 < -1 || $pri < -1) {
            throw new ClientInvestmentException(
                "Portfolio repayment failed, repayment amount exceeds available"
            );
        }
    }

    /**
     * @param PortfolioRepaymentHasBeenMade $e
     */
    private function checkFinalRepayment(PortfolioRepaymentHasBeenMade $e)
    {
        $principal = $e->depositHolding->fresh()
            ->calculate($e->portfolioInvestmentRepayment->date, false)
            ->principal();

        if ($principal < 1) {
            $e->depositHolding->update([
                'withdraw_amount' => $e->portfolioInvestmentRepayment->amount,
                'withdrawal_date' => $e->portfolioInvestmentRepayment->date,
                'withdrawn' => true,
                'withdrawn_by' => \Auth::id(),
                'withdraw_transaction_approval_id' => $e->portfolioInvestmentRepayment->approval_id,
                'partial_withdraw' => false
            ]);
        }
    }

    /**
     * @param PortfolioRepaymentHasBeenMade $e
     * @throws ClientInvestmentException
     */
    private function validateRepaymentDate(PortfolioRepaymentHasBeenMade $e)
    {
        if (Carbon::parse($e->portfolioInvestmentRepayment->date) < Carbon::parse($e->depositHolding->invested_date)) {
            throw new ClientInvestmentException(
                "Repayment failed, the repayment date should not be before the investment invested date"
            );
        }
    }
}
