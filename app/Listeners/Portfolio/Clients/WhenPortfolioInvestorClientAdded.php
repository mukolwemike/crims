<?php

namespace App\Listeners\Portfolio\Clients;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ClientType;
use App\Cytonn\Models\Contact;
use App\Cytonn\Models\Portfolio\PortfolioSecurity;
use App\Cytonn\Models\PortfolioInvestor;
use App\Events\Portfolio\Clients\PortfolioInvestorClientAdded;
use Cytonn\Clients\ClientRepository;
use Cytonn\Core\Validation\ValidatorTrait;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\ApplicationsRepository;

class WhenPortfolioInvestorClientAdded
{
    use ValidatorTrait;

    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  PortfolioInvestorClientAdded $event
     * @return void
     */
    public function handle(PortfolioInvestorClientAdded $event)
    {
        \DB::transaction(
            function () use ($event) {
                $this->createPortfolioInvestorClient($event->data, $event->portfolioSecurity);
            }
        );
    }

    /*
     * Create a portfolio investor client record
     */
    public function createPortfolioInvestorClient($data, $portfolioSecurity)
    {
        $client = $this->getClientFromData($data, $portfolioSecurity);

        $portfolioSecurity->client_id = $client->id;

        $portfolioSecurity->save();
    }

    /**
     * Create or get the client
     *
     * @param  array $data
     * @throws ClientInvestmentException
     * @return Client
     */
    private function getClientFromData(array $data, PortfolioSecurity $portfolioSecurity)
    {
        //Handle existing clients
        if ($data['existing_client']) {
            $client = $this->getExistingClient($data);

            if (is_null($client)) {
                throw new ClientInvestmentException("The client does not exist");
            }

            return $client;
        }

        //Check if another client exists with these details
        $repo = new ClientRepository();

        $type = ClientType::findOrFail($data['client_type_id']);

        //Handle new clients and ignore duplicate clients
        if ($data['new_client'] != 2) {
            $duplicate = Contact::where('email', $data['email'])->exists();

            if ($duplicate) {
                throw new ClientInvestmentException('Another client already exists with these details.');
            }
        }

        //Save client and contact record
        unset($data['client_id']);

        $client_data = $this->filter($data, Client::getTableColumnsAsArray());

        $client = new Client();

        $client->fill($client_data);

        $client->fund_manager_id = $portfolioSecurity->fund_manager_id;

        $client->contact()->associate($this->createContactInformation($data));

        (new ApplicationsRepository())->setClientCode($client);

        return $client;
    }

    /**
     * @param array $data
     * @return Client|null
     */
    private function getExistingClient(array $data)
    {
        if (isset($data['client_id'])) {
            return Client::find($data['client_id']);
        }

        if (isset($data['client_code'])) {
            return Client::where('client_code', $data['client_code'])->first();
        }

        return null;
    }

    /**
     * @param $data
     * @return Contact
     */
    private function createContactInformation($data)
    {
        $contact_data = $this->filter($data, Contact::getTableColumnsAsArray());

        $contact_data['entity_type_id'] = $data['client_type_id'];

        unset($contact_data['client_type_id']);

        return Contact::create($contact_data);
    }
}
