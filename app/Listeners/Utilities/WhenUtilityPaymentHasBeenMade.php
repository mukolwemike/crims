<?php

namespace App\Listeners\Utilities;

use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Events\Utilities\UtilityPaymentHasBeenMade;
use App\Jobs\USSD\SendMessages;
use Cytonn\Core\DataStructures\Carbon;
use Cytonn\Presenters\AmountPresenter;

class WhenUtilityPaymentHasBeenMade
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UtilityPaymentHasBeenMade $event
     * @return void
     */
    public function handle(UtilityPaymentHasBeenMade $event)
    {
        $this->sendUtilitiesNotification($event->instruction);
    }

    /**
     * @param UtilityBillingInstructions $instruction
     */
    private function sendUtilitiesNotification(UtilityBillingInstructions $instruction)
    {
        if (isEmptyOrNull($instruction->msisdn)) {
            return;
        }

        $data = [
            'reference' => $instruction->utility_payment_id . '-' . $instruction->id,
            'amount' => AmountPresenter::currency($instruction->amount),
            'bill' => $instruction->utility->name,
            'datePaid' => Carbon::parse($instruction->date)->toDateString(),
            'bill_type' => ''
        ];

        dispatch(new SendMessages($instruction->msisdn, 'utility-payment-successful', $data));
    }
}
