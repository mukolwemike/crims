<?php

namespace App\Listeners\Utilities;

use App\Cytonn\Models\Billing\UtilityBillingInstructions;
use App\Events\Utilities\UtilityPaymentHasFailed;
use Cytonn\Presenters\AmountPresenter;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WhenUtilityPaymentHasFailed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UtilityPaymentHasFailed  $event
     * @return void
     */
    public function handle(UtilityPaymentHasFailed $event)
    {
        $this->sendFailedNotification($event->instruction);
    }

    /**
     * @param UtilityBillingInstructions $instruction
     */
    private function sendFailedNotification(UtilityBillingInstructions $instruction)
    {
        if (isEmptyOrNull($instruction->msisdn)) {
            return;
        }

        $reference = $instruction->utility_payment_id . '-' . $instruction->id;

        $message = "Your payment to {$instruction->utility->name} of KES. " . AmountPresenter::currency($instruction->amount) .
            " failed, ref {$reference}. Call 0709101000";

        \SMS::queue($instruction->msisdn, $message);
    }
}
