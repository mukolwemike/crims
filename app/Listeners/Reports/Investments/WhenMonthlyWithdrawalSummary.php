<?php

namespace App\Listeners\Reports\Investments;

use App\Events\Reports\Investments\MonthlyWithdrawalSummary;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WhenMonthlyWithdrawalSummary
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MonthlyWithdrawalSummary $event
     * @return void
     */
    public function handle(MonthlyWithdrawalSummary $event)
    {
    }
}
