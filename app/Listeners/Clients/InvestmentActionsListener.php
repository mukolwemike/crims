<?php

namespace App\Listeners\Clients;

use App\Cytonn\Clients\ClientInstructionApprovalsRepository;
use App\Cytonn\Mailers\InvestmentActionsMailer;
use App\Cytonn\Notifier\ClientNotificationRepository;
use App\Cytonn\Mailers\RealEstate\ClientMakePaymentNotificationMailer;
use App\Events\Investments\InvestmentActionPerformed;
use App\Events\Investments\TopupFormFilled;
use App\Events\Realestate\ClientHoldingPaymentFilled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvestmentActionsListener
{
    use InteractsWithQueue, SerializesModels;

    public function onTopupFormFilled(TopupFormFilled $event)
    {
        (new InvestmentActionsMailer())->notifyOnTopup($event->form);

        (new InvestmentActionsMailer())->notifyClientOnTopup($event->form);

        (new ClientNotificationRepository())->topUpNotification($event->form);

        (new ClientInstructionApprovalsRepository())->topUpApprovals($event->form);

        (new InvestmentActionsMailer())->notifyClientOnTopUpApprovalCreated($event->form);
    }

    public function onInvestmentActionPerformed(InvestmentActionPerformed $event)
    {
        (new InvestmentActionsMailer())->notifyOnRolloverOrWithdraw($event->instruction);

        (new InvestmentActionsMailer())->notifyClientOnRolloverOrWithdraw($event->instruction);


        (new ClientNotificationRepository())->rolloverNotification($event->instruction);

        (new ClientInstructionApprovalsRepository())->rolloverApprovals($event->instruction);

        (new InvestmentActionsMailer())->notifyClientOnRolloverApprovalCreated($event->instruction);
    }

    public function onClientHoldingPaymentFilled(ClientHoldingPaymentFilled $event)
    {
        (new ClientMakePaymentNotificationMailer())->notifyPayment($event->instruction);

        (new ClientMakePaymentNotificationMailer())->notifyClientPayment($event->instruction);

        (new ClientNotificationRepository())->schedulePaymentNotification($event->instruction);
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Investments\TopupFormFilled',
            'App\Listeners\Clients\InvestmentActionsListener@onTopupFormFilled'
        );

        $events->listen(
            'App\Events\Investments\InvestmentActionPerformed',
            'App\Listeners\Clients\InvestmentActionsListener@onInvestmentActionPerformed'
        );

        $events->listen(
            'App\Events\Realestate\ClientHoldingPaymentFilled',
            'App\Listeners\Clients\InvestmentActionsListener@onClientHoldingPaymentFilled'
        );
    }
}
