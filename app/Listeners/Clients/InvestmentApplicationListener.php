<?php

namespace App\Listeners\Clients;

use App\Cytonn\Mailers\Client\ApplicationSaveMailer;
use App\Cytonn\Mailers\Client\InvestmentApplicationCompleteMailer;
use App\Cytonn\Mailers\Client\KycDocumentUploadedMailer;
use App\Events\InvestmentApplicationComplete;
use App\Events\InvestmentApplicationSaved;
use App\Events\KycDocumentUploaded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvestmentApplicationListener
{
    use InteractsWithQueue;
    use SerializesModels;

    public function onInvestmentApplicationComplete(InvestmentApplicationComplete $event)
    {
        $client = new InvestmentApplicationCompleteMailer();
        $client->sendEmailToClient($event->application);

        $staff = new InvestmentApplicationCompleteMailer();
        $staff->sendEmailToStaff($event->application);
    }

    public function onKycDocumentUploaded(KycDocumentUploaded $event)
    {
        $mailer = new KycDocumentUploadedMailer();

        $mailer->sendOnDocumentUploaded($event->application, $event->document);
    }

    public function onInvestmentApplicationSaved(InvestmentApplicationSaved $event)
    {
        $mailer = new ApplicationSaveMailer();
        $mailer->sendOnApplicationSave($event->application);

        if ($event->application->app_start_email) {
            $client = new ApplicationSaveMailer();
            $client->sendResumeLinkToClient($event->application);
        }

        $event->application->app_start_email = true;
        $event->application->save();
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\KycDocumentUploaded',
            'App\Listeners\Clients\InvestmentApplicationListener@onKycDocumentUploaded'
        );

        $events->listen(
            'App\Events\InvestmentApplicationComplete',
            'App\Listeners\Clients\InvestmentApplicationListener@onInvestmentApplicationComplete'
        );

        $events->listen(
            'App\Events\InvestmentApplicationSaved',
            'App\Listeners\Clients\InvestmentApplicationListener@onInvestmentApplicationSaved'
        );
    }
}
