<?php

namespace App\Listeners\Clients;

use App\Cytonn\Mailers\Unitization\UnitFundInstructionMailer;
use App\Events\Investments\Actions\UnitFundInstructionHasBeenMade;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WhenUnitFundInstructionHasBeenMade implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public function handle(UnitFundInstructionHasBeenMade $event)
    {
        (new UnitFundInstructionMailer())->notify($event->instruction);
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Investments\Actions\UnitFundInstructionHasBeenMade',
            'App\Listeners\Clients\WhenUnitFundInstructionHasBeenMade@handle'
        );
    }
}
