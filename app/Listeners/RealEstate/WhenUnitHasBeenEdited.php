<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 4/2/19
 * Time: 12:07 PM
 */

namespace App\Listeners\RealEstate;

use App\Cytonn\Models\RealEstate\RealEstateContractVariation;
use App\Cytonn\Models\RealestateLetterOfOffer;
use App\Cytonn\Reporting\ContractVariationGenerator;
use App\Events\RealEstate\UnitHasBeenEdited;
use Carbon\Carbon;

class WhenUnitHasBeenEdited
{
    public function handle(UnitHasBeenEdited $event)
    {
        $holding = $event->holding;

        $data = $event->data;

        return $this->generate($holding->loo->id, $data);
    }

    public function generate($loo_id, $data)
    {
        $loo = RealestateLetterOfOffer::findOrFail($loo_id);

        $variation = new RealEstateContractVariation();

        if ($loo->contractVariation->count()) {
            $variation = $loo->contractVariation->first();
        }

        $variation->update($data);

        $variation->generated_on = Carbon::today();
        $variation->generated_by = auth()->user()->id;
        $variation->loo_id = $loo->id;

        $variation->save();

        \Flash::message('Contract variation generated');

        return \Redirect::back();
    }
}
