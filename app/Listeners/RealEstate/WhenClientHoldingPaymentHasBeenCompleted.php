<?php

namespace App\Listeners\RealEstate;

use App\Cytonn\Mailers\RealEstate\HoldingPaymentCompletedMailer;
use App\Events\RealEstate\ClientHoldingPaymentHasBeenCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WhenClientHoldingPaymentHasBeenCompleted
{
    use InteractsWithQueue, SerializesModels;

    public function handle(ClientHoldingPaymentHasBeenCompleted $event)
    {
        (new HoldingPaymentCompletedMailer())->notify($event->holding);
    }
}
