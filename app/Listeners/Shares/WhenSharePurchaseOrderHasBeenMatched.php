<?php

namespace App\Listeners\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Events\Shares\SharePurchaseOrderHasBeenMatched;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Mails\Mailer;

class WhenSharePurchaseOrderHasBeenMatched
{
    public function handle(SharePurchaseOrderHasBeenMatched $event)
    {
        $data = $event->approval->payload;

        $purchaseOrders = SharesPurchaseOrder::whereIn('id', $data['purchase_order_ids'])->get();

        $purchaseOrders->each(function ($purchaseOrder) use ($data) {

            $client = $purchaseOrder->buyer->client;

            if ($this->hasBalance($purchaseOrder, $client)) {
                return;
            }

//            $this->sendNotification($purchaseOrder, $client);
        });
    }

    public function sendNotification(SharesPurchaseOrder $purchaseOrder, Client $client)
    {
        $view = 'emails.shares.matched_purchase_order';

        try {
            $email_sender = UserPresenter::presentLetterClosingNoSignature(auth()->id());
        } catch (\Exception $e) {
            $email_sender = 'Cytonn Investments';
        }

        $data = [
            'purchaseOrder' => $purchaseOrder,
            'client' => $client,
            'email_sender'  => $email_sender,
            'total_amount' => $purchaseOrder->number * $purchaseOrder->price,
            'balance' => $purchaseOrder->buyer->sharePaymentsBalance($purchaseOrder->matched_date)
        ];

        Mailer::compose()
            ->to([ $client->getContactEmailsArray() ])
            ->cc(['operations@cytonn.com'])
            ->bcc(config('system.administrators'))
            ->subject('Share Purchase Order has been matched')
            ->view($view, $data)
            ->send();

        return false;
    }

    public function hasBalance(SharesPurchaseOrder $purchaseOrder, Client $client)
    {
        $amount = abs($purchaseOrder->price * $purchaseOrder->number);

        $balance = $client->clientBalance(
            $purchaseOrder->request_date,
            null,
            null,
            $purchaseOrder->buyer->entity,
            null
        );

        if ($balance < $amount) {
            return false;
        }

        return true;
    }
}
