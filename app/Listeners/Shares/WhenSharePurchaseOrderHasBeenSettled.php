<?php

namespace App\Listeners\Shares;

use App\Cytonn\Models\Client;
use App\Cytonn\Models\ShareHolding;
use App\Cytonn\Models\SharePurchases;
use App\Cytonn\Models\SharesPurchaseOrder;
use App\Events\Shares\SharePurchaseOrderHasBeenSettled;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Shares\ShareHoldingRepository;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WhenSharePurchaseOrderHasBeenSettled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SharePurchaseOrderHasBeenSettled  $event
     * @return void
     */
    public function handle(SharePurchaseOrderHasBeenSettled $event)
    {
        $data = $event->approval->payload;

        $shareHoldings = ShareHolding::whereIn('id', $data['share_purchase_ids'])->get();

        $shareHoldings->each(function (ShareHolding $shareHolding) use ($data) {

//            $repo = new ShareHoldingRepository($shareHolding);
//
//            $repo->createBusinessConfirmation();
        });
    }
}
