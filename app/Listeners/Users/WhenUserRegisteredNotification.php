<?php

namespace App\Listeners\Users;

use App\Events\Users\UserRegisteredNotification;
use App\Mail\Mailers\SimpleMailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WhenUserRegisteredNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisteredNotification $event
     * @return void
     */
    public function handle(UserRegisteredNotification $event)
    {
        $this->sendUserRegisteredNotification($event->user);
    }

    /*
     * Send a notification to system admins when a user is registered
     */
    public function sendUserRegisteredNotification($user)
    {
        $data = [
            'view' => 'emails.users.newuser',
            'subject' => 'CRIMS : New User Added Notification',
            'fullname' => $user->fullname,
            'email' => $user->email,
            'jobtitle' => $user->jobtitle,
            'id' => $user->id,
            'user' => Auth::user()->fullname,
            'to' => Config::get('system.emails.operations_mgt'),
        ];

        Mail::queue(new SimpleMailable($data));
    }
}
