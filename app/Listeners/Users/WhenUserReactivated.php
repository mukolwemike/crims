<?php

namespace App\Listeners\Users;

use App\Events\Users\UserReactivated;
use App\Mail\Mailers\SimpleMailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WhenUserReactivated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserReactivated $event
     * @return void
     */
    public function handle(UserReactivated $event)
    {
        $this->sendUserReactivatedNotification($event->user);
    }

    /*
     * Send a notification when a user is deactivated
     */
    public function sendUserReactivatedNotification($user)
    {
        $data = [
            'view' => 'emails.users.user_activated',
            'subject' => 'CRIMS : User Account Reactivated Notification',
            'fullname' => $user->fullname,
            'email' => $user->email,
            'jobtitle' => $user->jobtitle,
            'id' => $user->id,
            'user' => Auth::user()->fullname,
            'to' => Config::get('system.emails.operations_mgt'),
        ];

        Mail::queue(new SimpleMailable($data));
    }
}
