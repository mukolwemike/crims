<?php

namespace App\Listeners\Users;

use App\Events\Users\UserAssignedPermission;
use App\Mail\Mailers\SimpleMailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WhenUserAssignedPermission
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserAssignedPermission $event
     * @return void
     */
    public function handle(UserAssignedPermission $event)
    {
        $this->sendUserPermissionsAssignedNotification($event->user, $event->permission);
    }

    /*
     * Send a notification that a user has been assigned a permission
     */
    public function sendUserPermissionsAssignedNotification($user, $permission)
    {
        $data = [
            'view' => 'emails.users.new_permission_assigned',
            'subject' => 'CRIMS : Permission Assigned To User',
            'fullname' => $user->fullname,
            'email' => $user->email,
            'permission' => $permission->description,
            'id' => $user->id,
            'user' => Auth::user()->fullname,
            'to' => Config::get('system.emails.operations_mgt'),
        ];

        Mail::queue(new SimpleMailable($data));
    }
}
