<?php

namespace App\Listeners\Users;

use App\Events\Users\UserDeactivated;
use App\Mail\Mailers\SimpleMailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WhenUserDeactivated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserDeactivated $event
     * @return void
     */
    public function handle(UserDeactivated $event)
    {
        $this->sendUserDeactivatedNotification($event->user);
    }

    /*
     * Send a notification when a user is deactivated
     */
    public function sendUserDeactivatedNotification($user)
    {
        $data = [
            'view' => 'emails.users.user_deactivated',
            'subject' => 'CRIMS : User Account Deactivated Notification',
            'fullname' => $user->fullname,
            'email' => $user->email,
            'jobtitle' => $user->jobtitle,
            'id' => $user->id,
            'user' => Auth::user()->fullname,
            'to' => Config::get('system.emails.operations_mgt'),
        ];

        Mail::queue(new SimpleMailable($data));
    }
}
