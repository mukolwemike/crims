<?php

namespace App\Listeners\Roles;

use App\Events\Roles\RolePermissionsUpdated;
use App\Mail\Mailers\SimpleMailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WhenRolePermissionsUpdated
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RolePermissionsUpdated $event
     * @return void
     */
    public function handle(RolePermissionsUpdated $event)
    {
        $this->sendRolePermissionsUpdatedNotification($event->role);
    }

    /*
     * Send a notification when a role details are updated
     */
    public function sendRolePermissionsUpdatedNotification($role)
    {
        $data = [
            'view' => 'emails.roles.permssions_updated',
            'subject' => 'CRIMS : Role Permissions Updated',
            'name' => $role->name,
            'description' => $role->description,
            'id' => $role->id,
            'user' => Auth::user()->fullname,
            'to' => Config::get('system.emails.operations_mgt'),
            'bcc' => config('system.administrators')
        ];

        Mail::queue(new SimpleMailable($data));
    }
}
