<?php

namespace App\Listeners\Roles;

use App\Events\Roles\RoleAdded;
use App\Mail\Mailers\SimpleMailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WhenRoleAdded
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RoleAdded $event
     * @return void
     */
    public function handle(RoleAdded $event)
    {
        $this->sendRoleAddedNotification($event->role);
    }

    /*
     * Send a notification when a role is added
     */
    public function sendRoleAddedNotification($role)
    {
        $data = [
            'view' => 'emails.roles.newrole',
            'subject' => 'CRIMS : New Role Added',
            'name' => $role->name,
            'description' => $role->description,
            'id' => $role->id,
            'user' => Auth::user()->fullname,
            'to' => Config::get('system.emails.operations_mgt'),
        ];

        Mail::queue(new SimpleMailable($data));
    }
}
