<?php

namespace App\Listeners\Investments\Actions;

use App\Events\Investments\Actions\WithdrawalHasBeenRemoved as Evt;
use Cytonn\Investment\Tax\WithholdingTaxRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WithdrawalHasBeenRemoved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WithdrawalHasbeenRemoved $event
     * @return void
     */
    public function handle(Evt $event)
    {
        //Remove clawback with withdrawal_id
        (new WithholdingTaxRepository())->reverseForWithdrawal($event->withdrawal);

        $this->checkFinalWithdrawal($event->investment, $event->withdrawal);
    }

    /**
     * @param $investment
     * @param $withdrawal
     */
    private function checkFinalWithdrawal($investment, $withdrawal)
    {
        $principal = $investment->fresh()
            ->calculate($withdrawal->date, false, null, false, true)
            ->principal();

        if ($principal > 1) {
            $investment->update([
                'withdrawn' => null,
                'rolled' => null,
                'withdraw_approval_id' => null,
                'withdrawn_on' => null,
                'withdrawal_date' => null,
                'withdraw_amount' => null,
                'partial_withdraw' => null,
                'withdrawal_instruction_id' => null
            ]);
        }
    }
}
