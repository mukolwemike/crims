<?php

namespace App\Listeners\Investments\Actions;

use App\Cytonn\Models\ClientInvestment;
use App\Events\Investments\Actions\MaturityDateEdited;
use Cytonn\Presenters\ClientPresenter;
use Cytonn\Presenters\UserPresenter;
use Cytonn\Support\Mails\Mailer;
use Illuminate\Support\Facades\Auth;

class WhenMaturityDateEdited
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaturityDateEdited  $event
     * @return void
     */
    public function handle(MaturityDateEdited $event)
    {
        $this->sendMaturityEditNotification($event->investment, $event->oldInvestment);
    }

    private function sendMaturityEditNotification(ClientInvestment $investment, ClientInvestment $oldInvestment)
    {
        $client = $investment->client;

        $from = ['operations@cytonn.com' => 'Cytonn Investments'];

        $cc = [config('system.emails.operations_group')];

        $fa = $client->getLatestFA();

        if ($fa) {
            $cc[] = $fa->email;
        }

        $to = $client->getContactEmailsArray();

        $subject = 'Investment Duration Extension - ' . ClientPresenter::presentJointFirstNames($client->id);

        $sender = Auth::user() ? UserPresenter::presentLetterClosingNoSignature(Auth::id()) :
            'Cytonn Investments Management Limited.';

        Mailer::compose()
            ->from($from)
            ->to($to)
            ->cc($cc)
            ->bcc(config('system.administrators'))
            ->subject($subject)
            ->view('emails.investment.edit_maturity_date_notification', [
                'oldInvestment' => $oldInvestment,
                'investment' => $investment,
                'email_sender' => $sender
            ])
            ->send();
    }
}
