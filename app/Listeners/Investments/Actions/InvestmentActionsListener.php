<?php

namespace App\Listeners\Investments\Actions;

use App\Cytonn\Mailers\InvestmentActionsMailer;
use App\Events\Investments\InvestmentActionPerformed;
use App\Events\Investments\TopupFormFilled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InvestmentActionsListener
{
    use InteractsWithQueue;
    use SerializesModels;

    public function onTopupFormFilled(TopupFormFilled $event)
    {
        (new InvestmentActionsMailer())->notifyOnTopup($event->form);
        (new InvestmentActionsMailer())->notifyClientOnTopup($event->form);
    }

    public function onInvestmentActionPerformed(InvestmentActionPerformed $event)
    {
        (new InvestmentActionsMailer())->notifyOnRolloverOrWithdraw($event->instruction);
        (new InvestmentActionsMailer())->notifyClientOnRolloverOrWithdraw($event->instruction);
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Investments\TopupFormFilled',
            'App\Listeners\Investments\Actions\InvestmentActionsListener@onTopupFormFilled'
        );

        $events->listen(
            'App\Events\Investments\InvestmentActionPerformed',
            'App\Listeners\Investments\Actions\InvestmentActionsListener@onInvestmentActionPerformed'
        );
    }
}
