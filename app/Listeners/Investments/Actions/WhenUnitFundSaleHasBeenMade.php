<?php

namespace App\Listeners\Investments\Actions;

use App\Cytonn\Mailers\Unitization\UnitFundSaleMailer;
use App\Cytonn\Models\Unitization\UnitFundSale;
use App\Cytonn\SMS\Unitization\UnitFundActionSMSNotify;
use App\Cytonn\Unitization\UnitFundClientRepository;
use App\Events\Investments\Actions\UnitFundSaleHasBeenMade;
use Cytonn\Investment\Tax\WithholdingTaxRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WhenUnitFundSaleHasBeenMade
{
    protected $client;

    protected $fund;

    protected $sale;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UnitFundSaleHasBeenMade  $event
     * @return void
     */
    public function handle(UnitFundSaleHasBeenMade $event)
    {
        $sale = $event->sale;

        $this->sale = $sale->fresh();

        $this->client = $this->sale->client;

        $this->fund = $this->sale->fund;

        if ($this->sale->approval && $this->sale->approval->transaction_type == "create_unit_fund_sale") {
            $this->createBcAndNotify();
        }

        $this->recordWhtWithdrawn();
    }

    /**
     *
     */
    public function createBcAndNotify()
    {
        (new UnitFundClientRepository($this->client, $this->fund, $this->sale->date))->sellBC($this->sale->id);

//        (new UnitFundSaleMailer())->sendNotification($sale);

//        (new UnitFundActionSMSNotify())->notifyOnSale($sale);
    }

    public function recordWhtWithdrawn()
    {
        (new WithholdingTaxRepository())->recordForUnitSale($this->sale);
    }
}
