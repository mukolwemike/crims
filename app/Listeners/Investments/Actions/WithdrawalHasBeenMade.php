<?php

namespace App\Listeners\Investments\Actions;

use App\Events\Investments\Actions\WithdrawalHasBeenMade as Evt;
use Carbon\Carbon;
use Cytonn\Exceptions\ClientInvestmentException;
use Cytonn\Investment\Commission\ClawBack;
use Cytonn\Investment\Tax\WithholdingTaxRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class WithdrawalHasBeenMade
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Evt $event
     * @return void
     * @throws ClientInvestmentException
     */
    public function handle(Evt $event)
    {
        $this->validateWithdrawal($event);
        $this->checkFinalWithdrawal($event);
        $this->validateWithdrawalDate($event);

        if ($event->withdrawal->withdraw_type == 'withdrawal' && $event->withdrawal->type_id == 1) {
            (new ClawBack($event->investment, $event->withdrawal))->onWithdrawal();
        }

        (new WithholdingTaxRepository())->recordForWithdrawal($event->withdrawal);
    }

    /**
     * @param Evt $e
     * @throws ClientInvestmentException
     */
    private function validateWithdrawal(Evt $e)
    {
        $investment = $e->investment->fresh();

        $val = $investment
            ->calculate($e->withdrawal->date, false, null, false, true)
            ->value();
        $pri = $investment->calculate($e->withdrawal->date, false)->value();

        $val1 = $investment->calculate($e->withdrawal->date, true)->value();
        $pri1 = $investment->calculate($e->withdrawal->date, true)->value();

        if ($val < (-1) || $val1 < -1 || $pri1 < -1 || $pri < -1) {
            throw new ClientInvestmentException("Withdrawal failed, amount withdrawn exceeds available");
        }
    }

    private function checkFinalWithdrawal(Evt $e)
    {
        $principal = $e->investment->fresh()
            ->calculate($e->withdrawal->date, false, null, false, true)
            ->principal();

        if ($principal < 1) {
            $e->investment->update([
                'withdrawn' => true,
                'withdrawal_date' => $e->withdrawal->date
            ]);
        }
    }

    /**
     * @param Evt $e
     * @throws ClientInvestmentException
     */
    private function validateWithdrawalDate(Evt $e)
    {
        if (Carbon::parse($e->withdrawal->date) > Carbon::parse($e->investment->maturity_date)) {
            throw new ClientInvestmentException(
                "Withdrawal failed, the withdrawal date should not be after the investment maturity date"
            );
        }

        if (Carbon::parse($e->withdrawal->date) < Carbon::parse($e->investment->invested_date)) {
            throw new ClientInvestmentException(
                "Withdrawal failed, the withdrawal date should not be before the investment invested date"
            );
        }

        $investment = $e->investment->fresh();

        $lastWithdraws = $investment->withdrawals()->latest('date')->first();

        $val1 = $investment->calculate($lastWithdraws->date, true)->value();
        $pri1 = $investment->calculate($lastWithdraws->date, true)->value();

        if ($val1 < (-1) || $pri1 < (-1)) {
            throw new ClientInvestmentException(
                "Withdrawal failed, investment would end up with negative balance"
            );
        }
    }
}
