<?php

namespace App\Listeners\Investments\Actions;

use App\Cytonn\Models\Unitization\UnitFundInvestmentInstruction;
use App\Cytonn\Models\Unitization\UnitFundPurchase;
use App\Cytonn\Unitization\UnitFundClientRepository;
use App\Events\Investments\Actions\UnitFundPurchaseHasBeenMade;
use Cytonn\Mailers\Unitization\BusinessConfirmationMailer;

class WhenUnitFundPurchaseHasBeenMade
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UnitFundPurchaseHasBeenMade $event
     * @return void
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function handle(UnitFundPurchaseHasBeenMade $event)
    {
        $purchase = $event->purchase;

        $instruction = $event->instruction;

        $client = $purchase->client;

        $unitFund = $purchase->unitFund;

        (new UnitFundClientRepository($client, $unitFund, $purchase->date))->buyBC($event->purchase->id);

        $this->notify($purchase, $instruction);
    }

    /**
     * @param UnitFundPurchase $purchase
     * @param UnitFundInvestmentInstruction $instruction
     * @throws \Cytonn\Exceptions\CRIMSGeneralException
     */
    public function notify(UnitFundPurchase $purchase, UnitFundInvestmentInstruction $instruction)
    {
        if ($purchase->unitFund->category->slug == 'pension-fund') {
            return;
        }

        (new BusinessConfirmationMailer())->sendBC($purchase);

//        (new UnitFundPurchaseMailer())->notify($purchase);
//
//        if (!(\App::environment('testing') || \App::environment('local'))) {
//            (new UnitFundActionSMSNotify())->notifyOnPurchase($purchase);
//        }
    }
}
