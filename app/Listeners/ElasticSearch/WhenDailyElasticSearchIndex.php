<?php

namespace App\Listeners\ElasticSearch;

use App\Events\ElasticSearch\DailyElasticSearchIndex;
use Carbon\Carbon;
use Cytonn\Support\Mails\Mailer;

class WhenDailyElasticSearchIndex
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DailyElasticSearchIndex $event
     * @return void
     */
    public function handle(DailyElasticSearchIndex $event)
    {
        $this->sendElasticSearchIndexResults($event->data);
    }

    /*
     * send an email to admins after elastic search index
     */
    public function sendElasticSearchIndexResults($results)
    {
        Mailer::compose()
            ->to(config('system.emails.administrators'))
            ->subject('Daily Elastic Search Re-index Results for - ' . Carbon::now()->toDateString())
            ->view(
                'emails.elasticsearch.elastic_search_report',
                [
                'results' => $results
                ]
            )
            ->send();
    }
}
