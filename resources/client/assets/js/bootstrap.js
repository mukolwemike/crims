/**
 * Created by daniel on 05/01/2017.
 */

import Vue from 'vue';
window.Vue = Vue;
import router from './router';
import store from './store';
import VueResource from 'vue-resource';
import vueFilter from 'vue-filter';


import * as types from './store/mutation-types';
// import Chartkick from 'chartkick';
// import VueChartkick from 'vue-chartkick';
// import Chart from 'chart.js';
import { resolveObject } from './utils';
import VueAnalytics from 'vue-analytics'


/**
 * Setup vue-resource
 */
Vue.use(VueResource);

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
    request.headers.set('X-Requested-With', 'XMLHttpRequest',);
    next();
});

//Add headers
let headers = {
    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
    'X-Requested-With': 'XMLHttpRequest'
};

store.commit(types.SET_DEFAULT_HEADERS, headers);


// Attempts to refresh the token
Vue.http.interceptors.push((request, next) => {
    next((response) => {
        let msg = resolveObject('error', response.data);

        if (response.status === 401 && msg === 'Unauthenticated.') {
            store.commit(types.SET_USER_LOGGED_OUT);
            let curr = router.currentRoute;
            let requireAuth = curr.matched.some(record => record.meta.requireAuth);

            if (requireAuth) {
                router.push({
                    name: 'login',
                    query: { redirect: curr.fullPath }
                });

                location.reload(true);
            }
        }

        if (msg === 'TokenMismatch') {
            //reload page on TokenMismatchException
            location.reload(true)
        }
    })
});

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = headers['X-Requested-With'];
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = headers['X-CSRF-TOKEN'];

/**
 * Setup Vue-filter
 */
Vue.use(vueFilter);

/**
 * ElementUI imports & setup
 */
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en';

Vue.use(ElementUI, { locale });



//  Instantiating the Echo instance
import Echo from "laravel-echo"

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key:  'ce3170bb1508329e0a3c',
    secret: '613811e49fb8bad87839',
    app_id: '671280',
    cluster: 'ap1',
    encrypt: true
});

// window.pusherKey = '8b498feaba333667f339';

Vue.use(VueAnalytics, {
    id: 'UA-134168156-5',
    checkDuplicatedScript: true
});
