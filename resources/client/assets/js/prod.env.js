/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 29/04/2017.
 * Project crims-client
 */
import Vue from 'vue';

Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;