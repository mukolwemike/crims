/**
 * Created by Raphael Karanja on 18/09/2018.
 */
var cy_dropdown = document.getElementsByClassName("cy_dropdown");
var i;

for (i = 0; i < cy_dropdown.length; i++) {
    cy_dropdown[i].addEventListener("click", function() {
        this.classList.toggle("cy_dropdown_active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.maxHeight === "500px") {
            dropdownContent.style.maxHeight = "0";
        } else {
            // dropdownContent.style.maxHeight = "500px";
            dropdownContent.classList.toggle("cy_dropdown_content_active");
        }
    });
}