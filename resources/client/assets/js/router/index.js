/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 12/12/2016.
 * Project crims-client
 */
import Vue from 'vue';
import VueRouter from 'vue-router';

import ForgotPassword from '../crims/auth/password/index';
import NotFound from '../crims/others/404.vue';
import Landing from '../crims/others/landing.vue';
import Login from '../crims/others/login.vue';
import store from '../store';
/**
 * Inv applications
 */
import Application from '../crims/application/apply.vue';
import RiskAssessment from '../crims/application/risk.vue';
import Investment from '../crims/application/investment.vue';
import Subscribe from '../crims/application/subscribe.vue';
import Kyc from '../crims/application/kyc.vue';
import Payment from '../crims/application/payment.vue';
import Resume from '../crims/application/resume.vue';
import Complete from '../crims/application/complete.vue';
import Mandate from '../crims/application/mandate'
import Deposit from '../crims/application/deposit.vue';

/*Dashboard*/
import Dashboard from '../crims/dashboard/dashboard.vue';
import Main from '../crims/dashboard/main.vue'
import SpActivities from '../crims/recentactivities/structuredproducts/recent_activities.vue'
import FundActivities from '../crims/recentactivities/unittrust/recent_activities.vue'
/*Investments*/
import AllInvestments from '../crims/investments/index.vue';
import InvestmentsDetails from '../crims/investments/structuredproducts/details.vue';
import InvestmentsStatements from '../crims/statements/structured_products/index.vue';
import InvestmentProduct from '../crims/investments/structuredproducts/investment.vue';
import TopUPList from '../crims/investments/structuredproducts/partials/topup_details/topup-list.vue';
import TopUpDetails from '../crims/investments/structuredproducts/partials/topup_details/topup-details.vue';
/*Unit Trust*/
import UnitTrustList from '../crims/investments/unittrust/list/list.vue'
import UnitTrustDetails from '../crims/investments/unittrust/details/details.vue';
import UnitTrustInstructions from '../crims/investments/unittrust/details/instructions.vue';
import UnitStatements from '../crims/statements/unittrust/index.vue';
import UnitTrustPurchaseDetails from '../crims/investments/unittrust/details/purchases/purchasedetails/purchasedetails.vue'


/*Real Estate*/
import REList from '../crims/investments/realestate/list'
import RePayments from '../crims/investments/realestate/payments/payments-list'
import REUnitDetail from '../crims/investments/realestate/details'
import REPaymentDetail from '../crims/investments/realestate/payments/payment-details'
import ReStatements from '../crims/statements/realestate/index'
/*Resources*/
import InvestmentCalculator from '../crims/resources/calculator.vue';
import My_Documents from '../crims/resources/documents.vue';
import PrevailingRates from '../crims/resources/prevailing_rates/index.vue'
/*Contacts*/
import ContactUs from '../crims/partials/contactus.vue';
/*Sales*/
import InvCommisions from '../crims/sales/commissions.vue'
import InvCommisionsInvestments from '../crims/sales/commissions/products.vue'
import InvCommisionsClawbacks from '../crims/sales/commissions/clawbacks.vue'
import InvCommisionsOverrides from '../crims/sales/commissions/overrides.vue'
import InvCommisionsClient from '../crims/sales/commissions/client.vue'
import My_FaClients from '../crims/sales/myClient/fa_clients.vue'
import My_FaClients_portfolio from '../crims/sales/myClient/portfolio.vue'
import My_FaList from '../crims/sales/myFa/faList.vue'
import FA_Projections from '../crims/sales/projections/projections_table.vue'
/**
 * 404
 */
import p404 from '../crims/404/404.vue'
/**
 * User Profile
 */
import Profile from '../crims/user/profile.vue';
import ProfileDetails from '../crims/user/details/details.vue';
import ProfileAccountDetails from '../crims/user/bank/add_edit_details.vue';
import ProfileEdit from '../crims/user/edit/edit.vue';
import ProfileEditSent from '../crims/user/edit_sent/edit_sent';
import ProfileEditKyc from '../crims/user/edit_kyc/edit_kyc.vue';

/**
 * Approvals
 */
import PendingApprovals from '../crims/approval/list'

/*
 * Real Estate Projects module
 */
import RealEstateProjects from '../crims/realestate/index';
import RealEstateProjectDetails from '../crims/realestate/details/details';
import RealEstateActivities from '../crims/investments/realestate/partials/activities';

/*
   Loyalty Points
 */
import ClientLoyaltyPointsDetails from '../crims/loyaltypoints/details';
import ClientLoyaltyPointsActivities from '../crims/loyaltypoints/partials/loyalty_recent_activities_comp';
/**
 * AboutPage Section
 */
import AboutPage from '../crims/about/index'


/**
 * Cypesa Billing Section
 */
import BillingIndex from '../crims/billing/index';


/**
 * Notifications
 */
// import Notifications from '../crims/notifications/list/list.vue';
// import Notification from '../crims/notifications/notification/notification.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            name: 'landing',
            path: '/',
            component: Landing
        },
        {
            name: 'password.forgot',
            path: '/login/forgot',
            component: ForgotPassword
        },
        {
            name: 'about',
            path: '/about',
            component: AboutPage
        },
        {
            name: 'real_estate.projectDetails',
            path: '/relestate/projects/:id',
            component: RealEstateProjectDetails
        },
        {
            path: '/home',
            component: Dashboard,
            meta: {requireAuth: true},
            children: [
                {
                    name: 'main',
                    path: '/',
                    component: Main
                },
                {
                    name: 'Activities',
                    path: '/activities',
                    component: SpActivities
                },
                {
                    name: 'FundActivities',
                    path: '/fundactivities',
                    component: FundActivities
                },
                {
                    name: 'all-investments',
                    path: '/investments',
                    component: AllInvestments,
                },
                {
                    name: 'investments.details',
                    path: '/investments/:id',
                    component: InvestmentsDetails
                },
                {
                    name: 'investments.single_product',
                    path: '/investments/product/:id/:client_id?',
                    component: InvestmentProduct
                },
                {
                    name: 'inv-statements',
                    path: '/statements/structured_products',
                    component: InvestmentsStatements
                },
                {
                    name: 'investments.TopUPList',
                    path: '/investments/topup/list',
                    component: TopUPList
                },
                {
                    name: 'investments.TopUpDetails',
                    path: '/investments/topup/details/:id',
                    component: TopUpDetails
                },
                {
                    name: 'unittrust.details',
                    path: '/unittrust/details/:id',
                    component: UnitTrustDetails
                },
                {
                    name: 'unittrust.instructions',
                    path: '/unittrust/instructions/:id',
                    component: UnitTrustInstructions
                },
                {
                    name: 'unittrust.list',
                    path: '/unittrust',
                    component: UnitTrustList
                },
                {
                    name: 'unittrust.purchase',
                    path: '/unittrust/:fundId/purchase/:id',
                    component: UnitTrustPurchaseDetails
                },
                {
                    name: 'unittrust.statements',
                    path: '/statement/unittrust/',
                    component: UnitStatements
                },
                {
                    name: 'unittrust.client.statements',
                    path: '/unittrust/:clientId/statements/:faId?',
                    component: UnitStatements
                },
                {
                    name: 'pendingApprovals',
                    path: '/approvals/:clientId',
                    component: PendingApprovals
                },
                {
                    name: 'calculator',
                    path: '/calculator',
                    component: InvestmentCalculator
                },
                {
                    name: 'prevailing_rates',
                    path: '/prevailing-rates',
                    component: PrevailingRates
                },
                {
                    name: 'sales.invcommissions',
                    path: '/commisions/:faId?',
                    component: InvCommisions

                },
                {
                    name: 'sales.invcommissions.products',
                    path: '/fa/:faId/commissions/:pName',
                    component: InvCommisionsInvestments

                },
                {
                    name: 'sales.invcommissions.clawbacks',
                    path: '/fa/:faId/clawbacks',
                    component: InvCommisionsClawbacks
                },
                {
                    name: 'sales.invcommissions.overrides',
                    path: '/fa/:faId/overrides',
                    component: InvCommisionsOverrides
                },
                {
                    name: 'sales.commissions.client',
                    path: '/sales/clients/:clientId/commission/:faId?',
                    component: InvCommisionsClient
                },
                {
                    name: 'sales.FaClients',
                    path: '/sales/fa/clients',
                    component: My_FaClients
                },
                {
                    name: 'sales.FaClients_portfolio',
                    path: '/sales/fa/clients/:clientId/portfolio/:faId?',
                    component: My_FaClients_portfolio
                },
                {
                    name: 'sales.My_FaList',
                    path: '/sales/fa/fas',
                    component: My_FaList
                },
                {
                    name: 'sales.FA_Projections',
                    path: '/sales/fa/projections',
                    component: FA_Projections
                },
                {
                    name: 'sales.client.statements',
                    path: '/sales/statements/:clientId/commissions/:faId?',
                    component: InvestmentsStatements
                },
                {
                    name: 'My_Documents',
                    path: '/documents',
                    component: My_Documents,
                    meta: {requireAuth: true}
                },
                {
                    name: 'ContactUs',
                    path: '/contactUs',
                    component: ContactUs
                },
                {
                    name: 'p404',
                    path: '/p404',
                    component: p404
                },
                {
                    path: '/profile',
                    component: Profile,
                    children: [
                        {
                            name: 'profile.details',
                            path: '/',
                            component: ProfileDetails
                        },
                        {
                            name: 'profile.edit',
                            path: '/edit',
                            component: ProfileEdit
                        },
                        {
                            name: 'profile.edit_sent',
                            path: '/edit_sent',
                            component: ProfileEditSent
                        },
                        {
                            name: 'profile.edit_kyc',
                            path: '/profile/kyc',
                            component: ProfileEditKyc
                        },
                        {
                            name: 'profile.client_account_details',
                            path: '/profile/account-details/:accId?',
                            component: ProfileAccountDetails
                        }
                    ]
                },

                {
                    name: 'investments.REList',
                    path: '/investments/realestate/list/:id?',
                    component: REList,
                },

                {
                    name: 'investments.RePayments',
                    path: '/investments/realestate/payments/list',
                    component: RePayments
                },

                {
                    name: 'realestate.payment.details',
                    path: '/investments/realestate/payment/:id',
                    component: REPaymentDetail
                },

                {
                    name: 'realestate.details',
                    path: '/investments/realestate/unit/:id',
                    component: REUnitDetail
                },

                {
                    name: 'realestate.statements',
                    path: '/statement/realestate',
                    component: ReStatements
                },
                {
                    name: 'realestate.client.statements',
                    path: '/realestate/:clientId/statements/:faId?',
                    component: ReStatements
                },
                {
                    name: 'real_estate.projects',
                    path: '/realestates/projects',
                    component: RealEstateProjects
                },
                {
                    name: 'real_estate.activities',
                    path: '/relestate/activities',
                    component: RealEstateActivities
                },
                {
                    name: 'billing',
                    path: '/billing',
                    component: BillingIndex
                },

                {
                    name: 'loyalty_points:details',
                    path: '/loyalty-points/details',
                    component: ClientLoyaltyPointsDetails
                },

                {
                    name: 'loyalty_points.activities',
                    path: '/loyalty-points/recent-activities',
                    component: ClientLoyaltyPointsActivities
                }
            ]
        },

        {
            name: 'login',
            path: '/login',
            component: Login
        },
        {
            name: 'apply',
            path: '/apply/:referredFA?',
            component: Application,
            children: [
                {
                    name: 'apply.risk',
                    path: '/apply/risk',
                    component: RiskAssessment
                },
                {
                    name: 'apply.investment',
                    path: '/apply/investment/:referredFA?',
                    component: Investment
                },
                {
                    name: 'apply.subscriber',
                    path: '/apply/subscriber',
                    component: Subscribe
                },
                {
                    name: 'apply.kyc',
                    path: '/apply/kyc',
                    component: Kyc
                },
                {
                    name: 'apply.payment',
                    path: '/apply/payment',
                    component: Payment
                },
                {
                    name: 'apply.deposit',
                    path: '/apply/deposit',
                    component: Deposit
                },
                {
                    name: 'apply.mandate',
                    path: '/apply/mandate',
                    component: Mandate
                },
                {
                    name: 'apply.complete',
                    path: '/apply/complete',
                    component: Complete
                },
                {
                    name: 'apply.resume',
                    path: '/apply/resume/:applicationId',
                    component: Resume
                },
            ]
        },

        {
            name: '404',
            path: '/404',
            component: NotFound
        }
    ]

});

export default router;

router.beforeEach((to, from, next) => {

    let is_auth = store.getters.isAuthenticated;

    if (to.matched.some(record => record.meta.requireAuth)) {
        if (!is_auth) {
            next({
                path: '/login',
                query: {redirect: to.fullPath}
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
});

