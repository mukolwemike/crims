/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 16/12/2016.
 * Project crims-client
 */
import Vue from 'vue';

import Vuetable from 'vuetable-2/src/components/Vuetable';
import VuetablePagination from 'vuetable-2/src/components/VuetablePagination';
import VuetablePaginationDropdown  from 'vuetable-2/src/components/VuetablePaginationDropdown.vue';
import VuetablePaginationInfo from 'vuetable-2/src/components/VuetablePaginationInfo';

/**
 * Default Page Container
 */
Vue.component(
    'page-container',
    {
        template:"<div><slot></slot></div>"
    }
);


/**
 * Global Components
 */


/*Main dashboard page*/
Vue.component('crims-page', require('./crims/layout/crims-page'));
Vue.component('side-menu', require('./crims/layout/sidebar'));
Vue.component('footerSection', require('./crims/layout/footer.vue'));
Vue.component('top-bar', require('./crims/layout/topbar.vue'));
Vue.component('crims-notifications', require('./crims/layout/partials/notifications/notifications.vue'));




/**UI COMPONENTS*/

// Vue.component('topMenu', require('./pages/topmenu.vue'));
// Vue.component('myAccount', require('./pages/dashboard/my_account.vue'));

Vue.component('dropdown', require('./components/ui/dropdown.vue'));
Vue.component('vueDatepicker', require('vuejs-datepicker'));
Vue.component('FormError', require('./components/forms/form-error.vue'));

Vue.component('vuetable', Vuetable);
Vue.component('vuetable-pagination', VuetablePagination);
Vue.component('vuetable-pagination-dropdown', VuetablePaginationDropdown);
Vue.component('vuetable-pagination-info', VuetablePaginationInfo);
Vue.component('vue-table-foot', require('./components/tables/vuetable_foot'));

/*Redesign Components*/
Vue.component('fa-breadcrumb', require('./crims/partials/fa-breadcrumb.vue'));

/**
 * Drawer
 */

Vue.component('action-drawer', require('./crims/partials/drawer.vue'));

/**
 * PlaceholderSimmers
 */
Vue.component('table-holder', require('./crims/utils/_table_loader.vue'));
Vue.component('tab-loader-shimmer', require('./crims/utils/_tab_loader.vue'));
Vue.component('users-loader-shimmer', require('./crims/utils/_user_cards_loaders.vue'));
Vue.component('content-card-shimmer', require('./crims/utils/_single_card_loader.vue'));
Vue.component('table_shimmer', require('./crims/utils/_table_shrimmer.vue'));


/*
*UI COMPONENTS
 */
Vue.component('menu-dropdown', require('./components/ui/menu-dropdown.vue'));
Vue.component('session-message', require('./crims/layout/session_message'));


/**
 * APIS
 */
Vue.component('fetch-sp-apis', require('./crims/dashboard/partials/fetch_sp_apis.vue'));
Vue.component('fetch-common-apis', require('./crims/dashboard/partials/fetch_common_apis.vue'));
Vue.component('fetch-fund-apis', require('./crims/dashboard/partials/fetch_fund_apis.vue'));
Vue.component('fetch-re-apis', require('./crims/dashboard/partials/fetch_re_apis.vue'));
Vue.component('fetch-loyalty-points-apis', require('./crims/dashboard/partials/fetch_loyalty_points_apis.vue'));


/**
 * Details Components
 */
Vue.component('bank-details', require('./crims/partials/bank_details'));
Vue.component('utf-bank-details', require('./crims/partials/utf_bank_details'));

/**
 * INVESTMENT ACTIONS
 */
Vue.component('new-top-up', require('./crims/investments/structuredproducts/partials/actions/topup/top-up.vue'));

/*
 *  Real Estate Actions
 */
Vue.component('real-estate', require('./crims/investments/realestate/index'));
Vue.component('make-payment', require('./crims/investments/realestate/actions/make-payments'));
Vue.component('password-reset', require('./crims/auth/password/reset'));


Vue.component('top-up-terms', require('./components/terms/topup'));
Vue.component('rollover-terms', require('./components/terms/rollover'));

Vue.component('approval-card', require('./crims/approval/card'));

Vue.component('re-bank-details', require('./crims/investments/realestate/partials/bank_details'));

/*
 * Loyalty Points
 */
Vue.component('loyalty-points', require('./crims/loyaltypoints/index'));

export default {}



