import "babel-polyfill";
import Vue from 'vue';
import router from './router';

require('./bootstrap');
require('./directives');
require('./filters');

import store from './store';
import components from './components';

require('cookieconsent');

import VueTelInput from 'vue-tel-input'

Vue.use(VueTelInput);

const app = new Vue(
    {
        mounted() {
            $(document).trigger('vue_is_ready');

            Echo.private('notification')
                .listen('ClientUserNotificationUpdated', (e) => {
                    console.log("Listening succesfully");
                });
        },
        router,
        store,
        components,
    }
).$mount('#cyapp');


store.commit('fetchSelectedClient');

window.addEventListener("load", function () {
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#000"
            },
            "button": {
                "background": "#f1d600"
            }
        },
        "theme": "classic",
        "position": "bottom"
    })
});

