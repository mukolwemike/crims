import accounting from 'accounting';
import moment from 'moment';
import events from '../../events';

export default {
    data: () => ({
        pagination: {
            infoClass: 'pull-left',
            wrapperClass: 'vuetable-pagination pull-right',
            activeClass: 'pagination__active',
            disabledClass: 'disabled',
            pageClass: 'pagination__btn',
            linkClass: 'pagination__btn',
            icons: {
                first: '',
                prev: '',
                next: '',
                last: '',
            },
        },
        vueTableLoading: true,
        noData: ''
    }),
    methods: {
        onPaginationData(paginationData) {
            this.$refs.pagination.setPaginationData(paginationData);
            this.$refs.paginationInfo.setPaginationData(paginationData);
            if (this.$refs.totalComponent) {
                this.$refs.totalComponent.setPaginationData(paginationData)
            };
            events.bus.$emit('pagination_data', paginationData);
        },
        onChangePage(page) {
            this.$refs.vuetable.changePage(page)
        },
        onLoaded() {
            this.vueTableLoading = false;
            this.noData = 'No relevant data';
        },
        formatNumber(value) {
            return accounting.formatNumber(value, 2)
        },
        formatDate(value, fmt = 'D MMM YYYY') {
            return (value == null)
                ? ''
                : moment(value, 'YYYY-MM-DD').format(fmt)
        },
        processedStatusLabel(value) {
            return (value == 'cancelled')
                ? '<span class="status_label status_label__primary">Cancelled</span>'
                : ((value)
                    ? '<span class="status_label status_label__success">Completed</span>'
                    : '<span class="status_label status_label__basic">Pending</span>')
        },
        formatYield(value) {
            return value + '%';
        },
    }
}