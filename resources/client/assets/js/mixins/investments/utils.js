/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 11/04/2017.
 * Project crims-client
 */
import {resolveObject} from '../../utils'
import moment from 'moment'

const mixin = {
    created: function () {

    },
    computed: {
        canWithdraw: function () {
            let can = resolveObject('can_withdraw.status', this.investment);
            if (!can) {
                return false;
            }

            return this.investment.active;
        },

        canRollover: function () {
            if (!this.canWithdraw) {
                return false;
            }

            return this.maturityIsNear();
        },

        cannot_withdraw_or_rollover: function () {
            return (!this.canWithdraw) && (!this.canRollover);
        }
    },

    methods:{
        maturityIsNear: function () {
            let mat = resolveObject('maturity_date', this.investment);
            if (!mat) {
                return false;
            }

            return moment(mat).diff(moment(), 'days') <= 14;
        },
        maturityIsPast: function () {
            let mat = resolveObject('maturity_date', this.investment);
            if (!mat) {
                return false;
            }

            return moment(mat).diff(moment(), 'days') <= 0;
        },
        whyCannotWithdrawOrRollover: function () {
            if (!this.investment.active) {
                return 'Investment Withdrawn';
            }

            return resolveObject('can_withdraw.reason', this.investment);
        },
        
        prematureWithdrawal: function (date) {

            if(moment(date).format('YYYY-MM-DD') < this.investment.maturity_date
            || moment(date).format('YYYY-MM-DD') === this.investment.maturity_date) {
                return true;
            }

            return false;
        }
    }
};

export default mixin;