/**
 * Created by Raphael Karanja on 2019-01-22.
 */
import { mapGetters} from 'vuex';
import events from '../../../events';

export default {
    data: ()=>({
        investments: [],
        loaded: false,
        investmentDetailsClicked: false
    }),
    computed: {
        ...mapGetters({
            products: 'products',
            fetchingProducts: 'fetchingProducts'
        }),
        sp_Steps(){
            if (this.products.length === 0 && !this.fetchingProducts) {
                return [
                    {
                        description: `Here is the list of all the products and the investments under each.`,
                        showNext: true,
                        selector: "#inv_step_1"
                    }
                ]
            }else {
                return [
                    {
                        description: `The table below shows the list of all <br/> your investment in our structured products. <br/> <b>Click next for more guide</b>.`,
                        event: 'click',
                        showNext: true,
                        showSkip: true,
                        selector: "#inv_step_0"
                    },
                    {
                        description: `You can switch between different products you have invested in. <br><br>`,
                        event: 'click',
                        showNext: true,
                        nextButton : {className: "next_inv_list", text: "Next"},
                        selector: "#inv_step_1 .el-tabs__item",
                        onBeforeStart: ()=>{
                            let thisElement = $("#inv_step_1");
                            if (thisElement.length ===0){
                                this.enjoyhint_instance.trigger('skip');
                                this.$confirm('There are no more steps. Make Sure you have invested in at least one structured product. Or ENSURE ALL PRODUCTS HAVE LOADED BEFORE PROCEEDING.', 'Tour Stopped', {
                                    confirmButtonText: 'Restart Tour!',
                                    confirmButtonClass: 'success',
                                    cancelButtonText: 'End Tour!',
                                    type: 'warning'
                                }).then(() => {
                                    this.startTour();
                                }).catch(() => {
                                    this.$message({
                                        type: 'success',
                                        message: 'Tour Cancelled'
                                    });
                                });
                            }
                        }
                    },
                    {
                        description: `To view more details on the this investments; <b>click on the eye icon</b>.</a>`,
                        showNext: false,
                        event: 'click',
                        selector: ".view_investment",
                        onBeforeStart: () => {
                            let nextElement = $('.view_investment');
                            if(nextElement.length === 0) {
                                this.enjoyhint_instance.trigger('skip');
                                this.$confirm( 'There are no more steps. Make Sure you have at least one investment. Or ensure all the investments have fully loaded.', 'Tour Stopped', {
                                    confirmButtonText: 'Restart Tour!',
                                    confirmButtonClass: 'success',
                                    cancelButtonText: 'End Tour!',
                                    type: 'warning'
                                }).then(() => {
                                    this.startTour();
                                }).catch(() => {
                                    this.$message({
                                        type: 'success',
                                        message: 'Tour Cancelled'
                                    });
                                });
                            }
                            nextElement.click((e) => {
                                if(!this.investmentDetailsClicked){
                                    setTimeout(()=>{
                                        let enjoyhint_steps = this.sp_Details_Steps;
                                        this.launchTour(enjoyhint_steps);
                                        this.investmentDetailsClicked = true;
                                    }, 300)
                                }
                            });
                        },
                    }
                ];
            }
        }
    },
    created(){
        events.bus.$on('pagination_data', (data)=>{
            this.investments = data.data;
            this.loaded = true;
        })
    }
}