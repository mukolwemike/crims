/**
 * Created by Raphael Karanja on 2019-01-22.
 */
export default {

    computed: {
        sp_Details_Steps(){
            return [
                // {
                //     description: `This is the selected investment details page.<br><br>`,
                //     showNext: true,
                //     showSkip: false,
                //     shape: "circle",
                //     radius: 20,
                //     selector: "#inv_details_step_0"
                // },
                {
                    description: `This section displays the <br/> details of the selected <a>investment</a>. <br><br><br>`,
                    event: 'click',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#inv_details_step_4"
                },
                {
                    description: `You can perform <a>Top-Up, Rollover</a> or <a>Withdraw</a> actions from any of these buttons. (The actions are only active if the instruction you want to perform is permitted as per the investment policy. Hover on a disabled button to find out why.) <br><br><br><br> <br><br><br><br>`,
                    event: 'click',
                    showNext: true,
                    // showSkip: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#inv_details_step_1"
                },

                {
                    description: `This section displays the details of <a>account activities</a>  for this <a>investment</a>. <br><br>`,
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#inv_details_step_5",
                    onBeforeStart: ()=> {
                        if ($('#inv_details_step_5').length ===0) {
                            this.enjoyhint_instance.trigger('next');
                        }
                    }
                },
                {
                    description: `This section displays the details of <a>past instructions</a> issued for this <a>investment</a>. <br><br><br>`,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#inv_details_step_6",
                    showNext: false,
                    onBeforeStart: ()=> {
                        if ($('#inv_details_step_6').length ===0) {
                            this.enjoyhint_instance.trigger('next');
                        }
                    }
                }
                // {
                //     description: `Top-Up button allows you to <a>view list of top-ups</a> (if pending top-ups exist) or you can simply <a>top-up from this page</a> (if no pending top-ups exists). <br><br><br><br>`,
                //     event: 'click',
                //     showNext: true,
                //     selector: "#inv_details_step_1"
                // },
                // {
                //     description: `Withdraw button allows you to withdraw your investments. Clicking the button <a>will launch a withdraw modal</a> (Only when the option is viable).   <br><br><br><br>`,
                //     event: 'click',
                //     showNext: true,
                //     selector: "#inv_details_step_2"
                // },
                // {
                //     description: `Rollover button allows you to Rollover your matured investments. Clicking the button <a>will launch a Rollover modal</a> (Only when the option is viable).   <br><br><br><br>`,
                //     event: 'click',
                //     showNext: true,
                //     selector: "#inv_details_step_3"
                // }
            ]
        }
    }
}