/**
 * Created by Raphael Karanja on 2019-01-23.
 */
export default {
    computed: {
        ut_details_steps(){
            return [
                {
                    description: `Here are more details of the selected fund, <br/> and below it are the lists of <a>purchases</a>, <a>sales</a> <br> and <a>instructions</a> as available`,
                    showNext: true,
                    showSkip: false,
                    selector: "#ut_details_step_0"
                },

                {
                    description: `You can <a>buy</a>, <a>sell/ redeem</a></a> on the fund using these action buttons. `,
                    showNext: true,
                    selector: "#ut_details_step_2",
                    skipButton: {
                        className: 'skipNow',
                        text: 'Got it!'
                    }
                },
                {
                    description: `This section comprises list of previously made <a>purchases</a> `,
                    showNext: true,
                    selector: "#ut_details_step_3",
                    skipButton: {
                        className: 'skipNow',
                        text: 'Got it!'
                    },
                    onBeforeStart: ()=> {
                        if ($('#ut_details_step_3').length ===0) {
                            this.enjoyhint_instance.trigger('next');
                        }
                    }
                },
                {
                    description: `This section comprises previously made <a>sales</a> `,
                    showNext: true,
                    selector: "#ut_details_step_4",
                    skipButton: {
                        className: 'skipNow',
                        text: 'Got it!'
                    },
                    onBeforeStart: ()=> {
                        if ($('#ut_details_step_4').length < 1) {
                            this.enjoyhint_instance.trigger('next');
                        }
                    },
                },
                {
                    description: `This section comprises previously made <a>Past Instructions</a> `,
                    showNext: false,
                    selector: "#ut_details_step_5",
                    skipButton: {
                        className: 'skipNow',
                        text: 'Got it!'
                    }
                },
                {
                    onBeforeStart: ()=> {
                        if ($('#ut_details_step_5').length < 1) {
                            this.enjoyhint_instance.trigger('next');
                        }
                    },
                    description: `This section comprises previously made <a>Transfers given</a> `,
                    showNext: true,
                    selector: "#ut_details_step_6",
                    skipButton: {
                        className: 'skipNow',
                        text: 'Got it!'
                    }

                },
                {
                    description: `This section comprises previously made <a>Transfers Received</a> `,
                    showNext: true,
                    selector: "#ut_details_step_7",
                    skipButton: {
                        className: 'skipNow',
                        text: 'Got it!'
                    }
                }
            ]
        }
    }
}