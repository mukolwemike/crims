/**
 * Created by Raphael Karanja on 2019-01-23.
 */
export default {
    computed: {
        ut_Steps(){
            return [
                {
                    description: `Here is the list of your unit trust fund investments. <br><b>Click next for more tour</b>.`,
                    showNext: true,
                    showSkip: false,
                    selector: "#ut_step_0"
                },
                {
                    description: 'To view further details,</a> click on this button.',
                    selector: '.view_ut_investment',
                    skipButton: {
                        className: 'skipNow',
                        text: 'Got it!'
                    },
                    onBeforeStart: ()=>{
                        $('.view_ut_investment').click(()=>{
                            if(!this.uTinvestmentDetailsClicked){
                                setTimeout(()=>{
                                    let enjoyhint_steps = this.ut_details_steps;
                                    this.launchTour(enjoyhint_steps);
                                    this.uTinvestmentDetailsClicked = true;
                                }, 300)
                            }
                        })
                    }
                }
            ]
        }
    }
}