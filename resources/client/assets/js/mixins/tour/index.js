/**
 * Created by Raphael Karanja on 2019-01-15.
 */
import { mapGetters } from 'vuex';

import events from '../../events';
import SP_Tour_Mixins from './sp/index';
import SP_InvDetailsTourMixins from './sp/details';
import UT_Tour_Mixins from './utf/index'
import UT_Details_Tour_Mixins from './utf/details';
import Dashboard_Tour_Mixins from './dasboard';
import Statements_Tour_Mixins from './statements';
import Calculator_Tour_Mixins from './calculator'

export default {

    mixins: [
        SP_Tour_Mixins,
        SP_InvDetailsTourMixins,
        UT_Tour_Mixins,
        UT_Details_Tour_Mixins,
        Dashboard_Tour_Mixins,
        Statements_Tour_Mixins,
        Calculator_Tour_Mixins
    ],

    data: () => ({
        investmentMenuOpen: false,
        UTMenuOpen: false,
        investmentMenuClicked: false,
        uTinvestmentDetailsClicked: false,
        enjoyhint_instance: null
    }),

    computed: {
        ...mapGetters({
            currentUser: 'currenctUser'
        }),

        hintSteps() {
            let steps = [
                {
                    description: `Hello ${this.currentUser.lastname}, For More <a>Help</a> on how to move around click <a>next</a> below.`,
                    showNext: true,
                    selector: "#v-step-0"
                },
                {
                    selector: '#v-step-1',
                    showNext: true,
                    description: `This menu provides links to different investments you have with us. <br/> (Structured Products, Unit Trust Funds or even Real Estate)`,
                    onBeforeStart: () => {
                        setTimeout(() => {
                            this.investmentMenuOpen = true
                        }, 100)
                    }
                }
            ];

            if(this.selectedClient.hasSP ) {
                steps.push(
                    {
                        selector: '#v-step-2',
                        showNext: this.selectedClient.hasUT,
                        description: `You can view the list <br> of all your investments in our structured product here, <br><b>click</b> on the menu item.</a> `,
                        onBeforeStart: () => {
                            $('#v-step-2').click((e) => {
                                if (!this.investmentMenuClicked) {
                                    setTimeout(() => {
                                        let enjoyhint_steps = this.sp_Steps;
                                        this.launchTour(enjoyhint_steps);
                                        this.investmentMenuClicked = true;
                                    }, 300)
                                }
                            });
                        },

                    },
                )
            }

            if(this.selectedClient.hasUT ) {
                steps.push(
                    {
                        selector: '#v-step-3',
                        event: 'click',
                        description: `You can view Unit Trust Fund Investments here. <br> Click the menu item to view.</a>`,
                        onBeforeStart: () => {
                            $('#v-step-3').click((e) => {
                                if (!this.investmentMenuClicked) {
                                    setTimeout(() => {
                                        let enjoyhint_steps = this.ut_Steps;
                                        this.launchTour(enjoyhint_steps);
                                        this.UTMenuOpen = true;
                                    }, 300)
                                }
                            });
                        },

                    }
                )
            }

            return steps;
        }
    },


    methods: {
        startTour() {
            this.investmentMenuClicked = false;
            this.investmentDetailsClicked = false;
            this.uTinvestmentDetailsClicked = false;
            this.UTMenuOpen = false;

            let page = this.$route.name;

            switch (page) {
                case 'all-investments' :
                    this.launchTour(this.sp_Steps);
                    break;
                case 'investments.details' :
                    this.launchTour(this.sp_Details_Steps);
                    break;
                case 'unittrust.list' :
                    this.launchTour(this.ut_Steps);
                    break;
                case 'unittrust.details' :
                    this.launchTour(this.ut_details_steps);
                    break;
                case 'main' :
                    this.launchTour(this.dashboard_steps);
                    break;
                case 'inv-statements' :
                    this.launchTour(this.statements_tour_steps);
                    break;
                case 'unittrust.statements' :
                    this.launchTour(this.statements_tour_steps);
                    break;
                case 'calculator' :
                    this.launchTour(this.calculator_tour_steps);
                    break;
                default :
                    this.launchTour(this.hintSteps);
            }

        },

        launchTour(enjoyhint_steps) {
            let enjoyhint_instance = new EnjoyHint({});
            enjoyhint_instance.set(enjoyhint_steps);
            enjoyhint_instance.run();
            this.enjoyhint_instance = enjoyhint_instance;
        }
    },

    created() {
        events.bus.$on('site_tour_start', () => {
            this.launchTour(this.hintSteps);
        })
    }
}