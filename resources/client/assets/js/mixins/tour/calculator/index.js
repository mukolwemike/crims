/**
 * Created by Raphael Karanja on 2019-02-04.
 */
export default {
    computed: {
        calculator_tour_steps(){
            return [
                {
                    description: `Calculator page allows you to make any calculations on any investment estimate. <br /> (<small>Click <strong>Next</strong> below to view more.</small> <br><br><br>`,
                    event: 'click',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: ".calculator_tour_step_1"
                },
                {
                    onBeforeStart: ()=>{
                        $("#tab-cms").click()
                    },
                    description: `Use these tabs to <a>Navigate</a> to your <a>Goal Calculator</a> and also <a>Interest rates</a>  <br /> (<small>Click <strong>Next</strong> below to view more.</small> <br><br><br>`,
                    event: 'click',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#tab-cms"
                },
                {
                    description: `Fill this form to calculate your investment.  <br /> (<small>Click <strong>Next</strong> below to view more.</small> <br><br><br>`,
                    event: 'custom',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector:  '.calculator_tour_step_3_cms'
                },
                {
                    description: `View the calculated results here. <br><br><br>`,
                    event: 'custom',
                    showNext: false,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector:  '.calculator_tour_step_4'
                }
            ]
        }
    }
}