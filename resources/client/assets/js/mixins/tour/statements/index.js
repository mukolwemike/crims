/**
 * Created by Raphael Karanja on 2019-02-04.
 */
export default {
    computed: {
        statements_tour_steps(){
            return [
                {
                    description: `The table generated below displays your <a>Investments Statements</a> <br> (<strong>Click <a>Next</a> below for more guide</strong>). <br><br><br>`,
                    event: 'click',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: ".statements_tour_step_1"
                },
                {
                    description: `You can also view statements for <a>Other accounts</a>. <br> (<small>Click <a>Next</a> below for more guide</small>). <br><br><br>`,
                    event: 'blur',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: ".switch-user"
                },
                {
                    description: `Use these filters to select <a>Product</a> or <a>Period</a> you want displayed. <br> (<small>Click <a>Next</a> below for more guide</small>). <br><br><br>`,
                    event: 'blur',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: ".statements_tour_step_3"
                },
                {
                    description: `Use this button to <a>Download </a> the displayed <a>Statement</a> above. <br><br><br>`,
                    event: 'blur',
                    showNext: false,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: ".statements_tour_step_4"
                }

            ]
        }
    }
}