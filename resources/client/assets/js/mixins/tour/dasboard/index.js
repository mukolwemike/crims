/**
 * Created by Raphael Karanja on 2019-02-04.
 */

export default {
    computed: {
        dashboard_steps(){
            return [
                {
                    description: `You can view <a>Other Investments</a> on these tabs. <br><br><br>`,
                    event: 'click',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#dashboard_tour_step_1 .el-tabs__nav"
                },
                {
                    description: `You can switch to <a>another account</a> you manage on this dropdown. <br><br><br>`,
                    event: 'blur',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#dashboard_tour_step_2"
                },
                {
                    description: `You can view your <a>profile(s) details</a> from this menu here. <br><br><br>`,
                    event: 'blur',
                    showNext: true,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: "#dashboard_tour_step_3"
                },
                {
                    description: `You can view your <a>account activities</a> from these tables. <br><br><br>`,
                    event: 'blur',
                    showNext: false,
                    skipButton: {className: 'skipNow', text: 'Got it!'},
                    selector: ".dashboard_tour_step_4"
                },
            ]
        }
    }
}