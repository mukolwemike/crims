/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 16/12/2016.
 * Project crims-client
 */

import Vue from 'vue';


Vue.directive(
    'foundation',
    function (el) {
        const init = function (el) {
            $(el).foundation();
        };

        init(el);

        const target = $(el).attr('data-toggle');

        if (target) {
            init('#'+target);
        }
    }
);

export default {

};