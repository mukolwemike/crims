/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 12/12/2016.
 * Project crims-client
 */
import * as Api from '../api/InvestmentsApi';

import Vue from 'vue';
import Vuex from 'vuex';
import Auth from './modules/Auth';
import Applications from './modules/Applications';
import Home from './modules/Client/AccountActivity';
import Investments from './modules/Investments';
import ContactUs from './modules/ContactUsModule';
import FA from './modules/FA';
import Document from './modules/DocumentsModule';
import Resource from './modules/Resources';
import Dashboard from './modules/Dashboard';
import Notifications from './modules/Notifications';
import Approvals from './modules/Approvals';
import Terms from './modules/Terms';
import ReProjects from './modules/RealEstate';
import LoyaltyPoints from './modules/Client/LoyaltyPoints';
import User from './modules/User';
import Billing from './modules/Billing';

Vue.use(Vuex);

let usr = null;
let cl = null;
try {
    usr = JSON.parse(sessionStorage.getItem('current_user'));
} catch (err) {
}

try {
    cl = JSON.parse(sessionStorage.getItem('default_client'));
} catch (err) {
}
const state = {
    count: 0,

    //Auth
    selectedClient: {},
    currentUser: usr,
    defaultClient: cl,
    token: localStorage.getItem('id_token'),
    isAuthenticated: false,

    //Set the rootState for investment application
    individualApplication: true,
    corporateApplication: false,

    applicationId: sessionStorage.getItem('application-form'),
    applicationProgress: {
        risk: false,
        investment: false,
        subscriber: false,
        kyc: false,
        payment: false
    },

    selectedFund: '',
    appliedRisk: {},
    appliedInvestment: {},
    appliedSubscriber: {},
    applicationDocuments: {
        id_or_passport: null
    },

    //Set the rootState of investments
    pendingWithdraw: null,
    pendingRollover: null,
    agreedRate: null,
    currencies: []

};
const getters = {
    currencies: state => state.currencies,
};
const mutations = {
    GET_ALL_CURRENCIES(state) {

        Api.getCurrencies()
        .then( ({ data }) => {
            state.currencies = data.data;
        }, () => {

        });
    }
};

const actions = {
};

const store = new Vuex.Store({
    debug: true,
    state,
    getters,
    mutations,
    actions,
    modules: {
        Auth,
        Applications,
        Home,
        Investments,
        ContactUs,
        FA,
        Document,
        ReProjects,
        Resource,
        Notifications,
        Approvals,
        Dashboard,
        Terms,
        Billing,
        User,
        LoyaltyPoints
    },
});

export default store
