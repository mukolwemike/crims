import * as clientApi from '../../../api/ClientsApi';
import store from '../../../store';
import router from '../../../router';

const state = {
    clientNotifications: []
};

const getters = {
    clientNotifications: state => state.clientNotifications
};

const mutations = {
    GET_CLIENT_NOTIFICATIONS(state, uuid) {

        clientApi.getClientNotification(uuid)
            .then( ({ data }) => {
                state.clientNotifications = data;
            });
    },

    UPDATE_CLIENT_NOTIFICATION(state, data) {

        clientApi.updateClientNotification(data.notification.id)
            .then( () => {
                router.push({ path: data.notification.url });

                refreshNotifications(data.client_uuid);
            });
    }
};

const actions = {

};

function refreshNotifications(uuid) {
    store.commit('GET_CLIENT_NOTIFICATIONS', uuid);
}

export default {
    state,
    getters,
    actions,
    mutations,
}