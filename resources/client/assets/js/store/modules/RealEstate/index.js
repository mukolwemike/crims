import * as reProjectsApi from '../../../api/reProjectsApi';

const state = {
    re_projects: [],
    fetchingREProjects: false,
    re_paymentPlans: []
};

const getters = {
    re_projects: state => state.re_projects,
    fetchingREProjects: state => state.fetchingREProjects,
    re_paymentPlans: state => state.re_paymentPlans
};

const mutations = {
    GET_RE_PROJECTS(state) {
        state.fetchingREProjects = true;
        reProjectsApi.fetchProjects()
            .then(({data}) => {
                state.fetchingREProjects = false;
                state.re_projects = data;
            })
            .catch(error => {
                state.fetchingREProjects = false;
            })
    },
    GET_RE_PAYMENT_PLANS(state) {
        reProjectsApi.getPaymentPlansApi()
            .then(({data}) => {
                state.re_paymentPlans = data.data;
            })
            .catch(error => {
            })
    }
};


export default {
    state, getters, mutations
}