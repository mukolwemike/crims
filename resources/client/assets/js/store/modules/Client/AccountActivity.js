/**
 * Created by daniel on 21/01/2017.
 */
import * as types from '../../mutation-types';
import * as Account from '../../../api/AccountActivityApi';
import { Message } from 'element-ui';

const state = {
    activityLogs: [],
    getBriefActivityLog:[],
    getBriefOverviewLog: [],
    getfundActivityLogs: [],
    fundActivityLogs: []
};

const getters = {
    activityLogs: state => state.activityLogs,
    fundActivityLogs: state => state.fundActivityLogs,
    getBriefOverviewLog: state => state.getBriefOverviewLog
};

const mutations = {
    [types.GET_BRIEF_ACTIVITY_LOG] (state, clientId) {
        Account.GET_BRIEF_ACTIVITY_LOG(clientId)
            .then(
                ({ data }) =>
                {
                    state.getBriefActivityLog = data.data;
                    state.clientId = clientId;

                    if (state.getBriefActivityLog.length > 0) {
                        var logs = [];
                        state.getBriefActivityLog.forEach(function (trans) {
                            if (trans.type == 'withdrawal') {
                                trans.narrative = trans.product + ' ' + parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,") + ' was withdrawn';
                            } else if (trans.type == 'investment') {
                                trans.narrative = trans.product + ' ' + parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,") + ' was invested';
                            } else if (trans.type == 'topup') {
                                trans.narrative = 'A top up of '+trans.product +' '+parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,") +' was added';
                            } else if (trans.type == 'rollover') {
                                trans.narrative = trans.product+' '+parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' was rolled over';
                            } else if (trans.type == 'partial_withdraw') {
                                trans.narrative = trans.product+' '+parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' was reinvested';
                            } else if (trans.type == 'reinvested_interest') {
                                trans.narrative = trans.product+' '+parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' interest was reinvested';
                            } else if (trans.type == 'interest_withdrawal') {
                                trans.narrative = trans.product+' '+parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")+' interest was withdrawn';
                            } else {
                                return '';
                            }
                            logs.push(trans);
                        });
                        state.activityLogs = logs;
                    }
                },
                ()=> {
                }
            );
    },

    GET_BRIEF_FUND_ACTIVITY_LOG(state, clientId) {
        Account.GET_BRIEF_FUND_ACTIVITY_LOG(clientId)
            .then(
                ({ data }) =>
                {
                    state.getfundActivityLogs = data;
                    state.clientId = clientId;

                    if (state.getfundActivityLogs.length > 0) {
                        var logs = [];
                        state.getfundActivityLogs.forEach(function (trans) {
                            if (trans.type == 'Purchase') {
                                trans.narrative =   'A ' + trans.unitFund+' purchase of Amount '+ parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,") + ' was made';
                            } else if (trans.type == 'Sale') {
                                trans.narrative = 'A ' + trans.unitFund + ' sale for ' + trans.number+ ' for Amount' + parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,") + ' was made';
                            } else if (trans.type == 'transfer') {
                                trans.narrative = 'A top up of '+trans.product +' '+parseFloat(trans.amount).toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,") +' was added';
                            }  else {
                                return '';
                            }
                            logs.push(trans);
                        });
                        state.fundActivityLogs = logs;
                    }
                },
                ()=> {
                }
            );
    },

    [types.GET_BRIEF_OVERVIEW_LOG] (state, clientId) {

        Account.GET_BRIEF_OVERVIEW_LOG(clientId)
        .then(
            ({ data })=>
            {
                state.getBriefOverviewLog = data;
            },
            ()=>
            {
            }
        );
    }
};

const actions = {

};


export default {
    state,
    getters,
    actions,
    mutations
}
