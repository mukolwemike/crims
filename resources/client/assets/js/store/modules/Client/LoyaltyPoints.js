/**
 * Created by Kuku Mike on 17/09/2019.
 */

import * as loyaltyApi from '../../../api/loyaltyPointsApi';

const state = {
    fetchingPointsSummary: false,
    loyaltyPointsDetails: [],
    vouchersDetails: []
};

const getters = {
    fetchingPointsSummary: state => state.fetchingPointsSummary,
    loyaltyPointsDetails: state => state.loyaltyPointsDetails,
    vouchersDetails: state => state.vouchersDetails
};

const mutations = {
    FETCH_LOYALTY_POINTS_SUMMARY(state, client_id) {
        state.fetchingPointsSummary = true;

        loyaltyApi.fetchSummary(client_id).then(({data}) => {
            state.fetchingPointsSummary = false;
            state.loyaltyPointsDetails = data.details;
        }, () => {
            state.loyaltyPointsDetails = [];
            state.fetchingPointsSummary = false;
        });
    },

    FETCH_VOUCHERS_DETAILS(state) {
        loyaltyApi.fetchVouchers().then(({data}) => {
            state.vouchersDetails = data.details;
        }, () => {
        });
    }
};

export default {
    state,
    getters,
    mutations,
}
