/**
 * Created by Raphael on 09/02/2018.
 */

import Responsive from './responsiveness';

const state = {

};

const getters = {

};

const mutations = {
};

const actions = {
};


export default {
    state,
    getters,
    actions,
    mutations,
    modules: {
        Responsive
    },
    }