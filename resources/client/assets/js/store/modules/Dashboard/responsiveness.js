

/**
 * Created by Raphael on 09/02/2018.
 */

const state = {
    menuVisible: false
};

const getters = {
    menuVisible:state=> state.menuVisible
};

const mutations = {
    TOGGLE_MENU(state, isvisible){
        state.menuVisible = isvisible;
    }
};

const actions = {
};


export default {
    state,
    getters,
    actions,
    mutations,

}