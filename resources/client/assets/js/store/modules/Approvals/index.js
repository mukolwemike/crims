import * as approvalApi from '../../../api/AccountActivityApi';

const state = {
    clientApprovals: {},
    fetchingPendingApprovals: false
};

const getters = {
    clientApprovals:state=> state.clientApprovals,
    fetchingPendingApprovals: state => state.fetchingPendingApprovals
};

const mutations = {

    CLIENT_APPROVALS(state, uuid) {
        state.fetchingPendingApprovals = true;
        approvalApi.getApprovals(uuid)
            .then( ({ data }) => {
                state.clientApprovals = data;
                state.fetchingPendingApprovals = false;
            }, ()=>{
                state.fetchingPendingApprovals = false;
            });
    }
};

export default {
    state,
    getters,
    mutations
}
