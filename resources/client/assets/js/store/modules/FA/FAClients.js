/**
 * Created by daniel on 31/01/2017.
 */
import * as Inv from '../../../api/InvestmentsApi';

const state = {
    myProductsList: {},
    stmtForm: {},
    searchClient: null,
    faClientsList: {},
    fecthingFA_Clients: false,
    fetchingProducts: false,
    products: []
};

const getters = {
    myProductsList: (state, getters) => {
        if (state.myProductsList === undefined || Object.keys(state.myProductsList).length <= 0) {
            Inv.myProducts(getters.selectedClient.id)
                .then(function (response) {
                    let data = response.data.data;
                    state.myProductsList = data !== undefined ? data : {};
                }, function () {
                    state.myProductsList = {};
                });
        }

        return state.myProductsList;
    },

    stmtForm: (state, getters) => {

        state.stmtForm = {
            product: getters.myProductsList[0] ? getters.myProductsList[0] : '',
            statement_date: new Date(),
        };

        return state.stmtForm;
    },

    searchClient: state => state.searchClient,

    faClientsList: state => state.faClientsList,

    fetchingProducts: state => state.fetchingProducts,

    products: state => state.products,

    fecthingFA_Clients: state => state.fecthingFA_Clients

};

const mutations = {
    MY_PRODUCTS_LIST(state, selectedClient) {
        state.fetchingProducts = true;
        Inv.myProducts(selectedClient)
            .then(function (response) {
                state.myProductsList = response.data.data;
                state.fetchingProducts = false;
            }, function (response) {
                state.fetchingProducts = false;
            });
    },

    CLIENT_PRODUCT_LIST(state, selectedClient) {

        state.fetchingProducts = true;
        Inv.myProducts(selectedClient)
            .then(function (response) {
                state.products = response.data.data;
                state.fetchingProducts = false;

            }, function (response) {
                state.fetchingProducts = false;
            });
    },

    FETCH_FA_CLIENTS(state, data) {
        state.fecthingFA_Clients = true;
        Inv.myClients(data).then(function ({data}) {
            state.faClientsList = data;
            state.fecthingFA_Clients = false;
        }, function (response) {
            state.fecthingFA_Clients = false;
        });
    },
    FETCH_FA() {

    }
};

const actions = {
    myProductsList({commit, getters}) {
        commit('MY_PRODUCTS_LIST', getters.selectedClient.id)
    },
    searchClient({state}, filterText) {
        state.searchClient = filterText;
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}

