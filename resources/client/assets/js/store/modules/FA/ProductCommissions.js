/**
 * Created by daniel on 16/02/2017.
 */
import * as types from '../../mutation-types';
import FaApi from '../../../api/FaApi';

const state = {
    faProdCommissions: {},
    selectedCliente: '',
    slectedCurrency: 'KES'
};

const getters = {
    faProdCommissions: state => state.faProdCommissions,
    selectedCliente: state => state.selectedCliente,
    slectedCurrency: state => state.slectedCurrency,
};

const mutations = {

    [types.GET_FA_PROD_COMMISSIONS](state, data){

        FaApi.faProdCommissions(data)
            .then(({data}) =>  {
                state.faProdCommissions = data.data;
            }, function () {
                state.faProdCommissions = {};
            });
    },

    SELECT_CLIENT(state, clientCode) {
        state.selectedCliente = clientCode;
    },

    SET_CURRENCY(state, currency) {
        state.slectedCurrency = currency;
    }
};

const actions = {

};


export default {
    state,
    getters,
    mutations,
    actions
}

