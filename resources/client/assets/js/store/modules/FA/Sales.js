import moment from 'moment';
import FaApi from '../../../api/FaApi'
import * as types from '../../mutation-types';
import { Message } from 'element-ui';

const state = {
    commissionDate: moment().startOf('month').format('YYYY-MM-DD'),
    faSummary : {},
    searchCommissionClient: null,
    currentFaClient : {},
    searchCommissionFa: {},
    currentClawbackFa: {},
    faClawbacks: {},
    faClientCommissions: [],
    monthlyCommissions: {},
    CommissionProjections: {
        data: [],
        meta: {}
    },
    selectedMonths: {},
    newColumns: {},
    tableSearch: false,
    tableAlter: false,
    monthLoad: false,
    monthlyTotal: [],
    monthlyTotals: [],
    fetchingCommission: false,
    fetchingClients: false,
    exportCommission: false,
    commissionFetchTimeout: false,
    exportingCommission: false,
};

const getters = {
    getCommissionDate: state => state.commissionDate,

    getFaSummary: state => state.faSummary,

    searchCommissionClient: state => state.searchCommissionClient,

    getCurrentFaClient: state => state.currentFaClient,

    searchCommissionFa: state => state.searchCommissionFa,

    currentClawbackFa: state => state.currentClawbackFa,

    faClawbacks: state => state.faClawbacks,

    faClientCommissions: state => state.faClientCommissions,

    newColumns: state => state.newColumns,

    tableSearch: state => state.tableSearch,

    tableAlter: state => state.tableAlter,

    monthLoad: state => state.monthLoad,

    monthlyTotals: state => state.monthlyTotals,

    commissionProjections: state => state.CommissionProjections,

    fetchingCommission: state=> state.fetchingCommission,

    fetchingClients: state=> state.fetchingClients,

    exportCommission: state=> state.exportCommission,

    commissionFetchTimeout: state=> state.commissionFetchTimeout,

    exportingCommission: state=> state.exportingCommission
};

const mutations = {
    [types.FETCH_FA_CLIENT_SUMMARY](state, data){

        state.fetchingClients = true;

        FaApi.faClientCommissions(data)
            .then( ({ data }) => {

                state.faClientCommissions = data.data;

                state.fetchingClients = false;

        }, () => {
            state.faClientCommissions = [];
            state.fetchingClients = false;
        });
    },

    [types.FETCH_FA_SUMMARY](state, summary){

        state.faSummary = summary;

        state.fetchingCommission = false;

    },

    [types.SET_COMMISSION_DATE](state, date){
        state.commissionDate = date;
    },

    [types.FA_CURRENT_CLIENT](state, client){
        state.currentFaClient = client;
    },

    [types.GET_FA_CLAWBACKS](state, data){
        FaApi.faClawbacks(data)
            .then(function (response) {
                state.faClawbacks = response.data.data;
            }, function () {
                state.faClawbacks = {};
            });
    },

    [types.FETCH_COMMISSION_PROJECTIONS](state, data){

        FaApi.fetchCommissionProjections(data)
        .then(
            function ({data}) {
                state.CommissionProjections = data;
            },
            function () {
                state.CommissionProjections = {
                    data: [],
                    meta: {}
                };
            }
        );
    },

    EXPORT_COMMISSION(state, form) {

        state.exportingCommission = true;

        FaApi.exportCommissionToMail(form)
            .then( () => {
                state.exportingCommission = false;
                state.exportCommission = false;

                Message.success({ message: 'An email has been sent you with commission details'});

            }, () => {
                state.exportingCommission = false;
                state.exportCommission = false;
            });
    },

    CLOSE_EXPORT_MODAL(state, value) {
        state.exportCommission = value;
    }
};

const actions = {
    fetchSummary : (context, form) => {
        let id = null;

        state.fetchingCommission = true;

        if (form.fa.hasOwnProperty('id')) {
            form.id = form.fa.id;
        }

        FaApi.commissionSummary(form)
            .then( ({data}) => {

                const summary = data;

                context.commit(types.FETCH_FA_SUMMARY, summary);

            }, ({ data }) =>  {

                state.exportCommission = true;
                state.commissionFetchTimeout = true;

                state.fetchingCommission = false;
            });
    },

    searchCommissionClient({state}, filterText) {
        state.searchCommissionClient = filterText;
    },

    searchCommissionFa({state}, filterText) {
        state.searchCommissionFa = filterText;
    },

    fetchClient : (context, {id, date, fa}) => {

        FaApi.clientSummary(id, date, fa.id)
            .then(function ({data}) {
                context.commit(types.FA_CURRENT_CLIENT, data.data);
            }, function (response) {
            });
    },

    setClawbackFa({state, commit}, data) {
        state.currentClawbackFa = data.fa;
        commit('GET_FA_CLAWBACKS', data);
    },

    collectCommission({ state }, pData) {

        var faCommissionSummary = state.faClientCommissions.filter(val => val.total != 0);
        var monthComm = pData.data.filter(val => val.on_retainer != 0);

        faCommissionSummary.forEach(function (commSumm) {

            var monthComs = monthComm.filter(({ id }) => {
                return (id == commSumm.id);
            });

            let newMonthComms = [];
            monthComs.forEach(function (monthCom) {
                newMonthComms.push({'value': monthCom.on_retainer});
                newMonthComms.push({'value': monthCom.off_retainer});
            });

            monthComs = newMonthComms;

            if (!commSumm.commissions) {
                commSumm.commissions = [];
                commSumm.commissions.push(monthComs[0]);
                commSumm.commissions.push(monthComs[1]);
            } else {
                commSumm.commissions.push(monthComs[0]);
                commSumm.commissions.push(monthComs[1]);
            }
        });

        //set the totals
        let commTotal = monthComm.reduce(function (carry, val) {
            return carry + parseFloat(val.on_retainer);
        }, 0);

        let commRetainer = monthComm.reduce(function (carry, val) {
            return carry + parseFloat(val.off_retainer);
        }, 0);

        state.monthlyTotal.push({ 'value': commTotal });
        state.monthlyTotal.push({ 'value': commRetainer });

        state.monthlyCommissions = faCommissionSummary;
    },

    selectedMonths({state}, months) {
        state.selectedMonths = months;

        let addedColumns = [];
        months.forEach(function () {
            addedColumns.push('On Retainer');
            addedColumns.push('Off Retainer');
        });

        state.newColumns = addedColumns;
    },

    setSearch({state}) {
        state.tableSearch = true;
        state.tableAlter = true;
        state.monthLoad = false;
    },

    setMonthLoad({state}) {
        state.monthLoad = true;
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}
