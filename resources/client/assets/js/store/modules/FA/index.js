/**
 * Created by daniel on 31/01/2017.
 */
import FAClients from './FAClients';
import FAClientStatements from './FAClientStatements';
import Sales from './Sales';
import Overrides from './Overrides';
import ProductCommissions from './ProductCommissions';
import Projections from './Projections'

const state = {

};

const getters = {

};

const mutations = {
};

const actions = {
};


export default {
    state,
    getters,
    actions,
    mutations,
    modules: {
        FAClients,
        FAClientStatements,
        Sales,
        Overrides,
        ProductCommissions,
        Projections
    },
    }
