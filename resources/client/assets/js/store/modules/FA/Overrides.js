/**
 * Created by daniel on 14/02/2017.
 */
import * as types from '../../mutation-types';
import FaApi from '../../../api/FaApi';

const state = {
    faOverrides: {},
    overrideMeta: {rank :{}},
    fetchingOverrides: false
};

const getters = {
    faOverrides: state => state.faOverrides,
    faOverrideMeta: state => state.overrideMeta,
    fetchingOverrides: state => state.fetchingOverrides
};

const mutations = {

    [types.GET_FA_OVERRIDES](state, data){
        state.fetchingOverrides = true;
        FaApi.faOverrides(data)
            .then(response =>  {
                state.faOverrides = response.data.data;
                state.overrideMeta = response.data.meta;
                state.fetchingOverrides = false;
            }, function (response) {
                state.faOverrides = {};
                state.fetchingOverrides = false;
            });

    }
};

const actions = {

};


export default {
    state,
    getters,
    mutations,
    actions
}

