/**
 * Created by daniel on 27/01/2017.
 */
import Vue from 'vue';
import {Notification} from 'element-ui';
import FileSaver from 'file-saver';
import * as InvStatement from '../../../api/InvestmentStatementApi';
import moment from 'moment';

const state = {
    downloadStatements: false,
    statementDate: new Date(),
    dateFrom: new Date(),
    portfolioList: {},
    rePortfolio: {},
    downloadingSPStatements: false,
    downloadingExcelSPStatement: false,
};

const getters = {
    downloadStatements: state => state.downloadStatements,

    downloadUrl: (state, getters) => (form) => {

        var data = {
            statement_date: moment(form.statement_date).format('YYYY-MM-DD'),
            date_from: moment(form.from_date).format('YYYY-MM-DD'),
            product: form.product,
            clientId: form.clientId
        };

        state.downloadingSPStatements = true;

        Vue.http.get(
            '/api/investments/statements/' + data.product + '/' + data.clientId + '/preview?date_from=' + data.date_from + '&statement_date=' + data.statement_date,
            {responseType: 'arraybuffer'}
        ).then(
            function (response) {
                state.downloadingSPStatements = false;

                let blob = new Blob([response.data], {type: response.headers.get('content-type')});

                FileSaver.saveAs(blob, "Cytonn Investments " + form.product_name + "  Statement from (" + data.date_from + ") to  (" + data.statement_date + ").pdf");
            }
        );
    },

    downloadExcelUrl: (state, getters) => (form) => {

        var data = {
            statement_date: moment(form.statement_date).format('YYYY-MM-DD'),
            date_from: moment(form.from_date).format('YYYY-MM-DD'),
            product: form.product,
            clientId: form.clientId
        };

        state.downloadingExcelSPStatement = true;

        Vue.http.get(
            '/api/investments/statements/' + data.product + '/' + data.clientId + '/excel-preview?date_from=' + data.date_from + '&statement_date=' + data.statement_date,
            {responseType: 'arraybuffer'}
        ).then(
            function (response) {

                state.downloadingExcelSPStatement = false;

                let blob = new Blob([response.data], {type: response.headers.get('content-type')});

                FileSaver.saveAs(blob, "Cytonn Investments " + form.product_name + "  Statement from (" + data.date_from + ") to  (" + data.statement_date + ").xlsx");
            }
        );
    },

    portfolioList: state => state.portfolioList,

    rePortfolio: state => state.rePortfolio,
    downloadingSPStatements: state => state.downloadingSPStatements,
    downloadingExcelSPStatement: state => state.downloadingExcelSPStatement,
};

const mutations = {
    GET_RE_INVESTMENTS(state, data) {
        InvStatement.reInvestments(data).then(
            function ({data}) {
                state.rePortfolio = data.data;
            },
            function () {
                Notification.warning({message: 'Error while fetching real estate investments'});
            }
        );
    }
};

const actions = {
    getStatementEmbed({getters, state}, form) {

        Vue.component(
            'getStatementEmbed',
            function (resolve, reject) {
                var data = {
                    dateTo: moment(form.statement_date).format('YYYY-MM-DD'),
                    dateFrom: moment(form.from_date).format('YYYY-MM-DD'),
                    product: form.product,
                    clientId: form.clientId
                };

                InvStatement.getStatementEmbed(data)
                    .then(
                        function ({data}) {
                            state.downloadStatements = true;
                            resolve(
                                {
                                    template: data
                                }
                            );
                        },
                        function () {
                            Notification.warning({message: 'You have no access to the client\'s statements'});

                            reject(
                                {
                                    template: '<div> </div>'
                                }
                            )
                        }
                    );
            }
        );
    },

    updateStatement({rootState}, data) {

        rootState.statementDate = data.statement_date;

        rootState.dateFrom = data.from_date;

        rootState.statementProduct = data.product;
    },

    getClientPortfolio({state}, form) {
        var data = {
            date: moment(form.statement_date).format('YYYY-MM-DD'),
            productId: form.product.id,
            clientId: form.clientId
        };

        InvStatement.getClientPortfolio(data)
            .then(
                function ({data}) {
                    state.portfolioList = data;
                },
                function () {
                    Notification.warning({message: 'Failed to fetch the client portfolio details'});
                }
            );
    }
};

export default {
    state,
    mutations,
    getters,
    actions
}

