/**
 * Created by yeric on 7/20/17.
 */
import * as types from '../../mutation-types'
import * as backApi from '../../../api/InvestmentsApi'


const state = {
    clientsList: {},
    productType: '',
    projections: [],
    fetchingProjections: false,
};

const getters = {
    clientsList: state => state.clientsList,

    testCommissions: state => state.testCommissions,

    productType: state => state.productType,

    projections: state => state.projections,

    fetchingProjections: state => state.fetchingProjections,
};

const mutations = {
    [types.GET_CLIENTS] (state, userId) {
        backApi.GET_CLIENTS(userId).then(
            function ({data}) {

                    state.clientsList = data.data;

                    const allClients = {
                        client_code: 'all', fullname: 'All'
                }

                    state.clientsList.unshift(allClients);

            },
            function () {
                    Notification.warning({message:  'No clients available', title: 'warning'});
            }
        );
    },

    GET_PROJECTIONS(state, data) {

        state.fetchingProjections = true;

        backApi.getProjections(data)
            .then(
                ({ data })=>
                {
                    state.fetchingProjections = false;
                    state.projections = data;
                },
                ()=>
                {
                    state.fetchingProjections = false;
                    state.projections = [];
                }
            );
    },

    SELECTED_PRODUCT_TYPE(state, product) {
        state.productType = product;
    }
};

const actions = {

};

export default {
    state, getters, mutations, actions
}
