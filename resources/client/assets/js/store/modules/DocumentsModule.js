/**
 * Created by yeric on 6/20/17.
 */
import * as types from '../mutation-types';
import { Notification } from 'element-ui';
import * as Doc from '../../api/DocumentApi';

const state = {
    documents: {},
    documentTypes: {},
    fetchingDocuments: false,
    fetchingDocumentsTypes: false,
}

const getters = {
    documents: state => state.documents,
    documentTypes: state => state.documentTypes,
    fetchingDocuments: state => state.fetchingDocuments,
    fetchingDocumentsTypes: state => state.fetchingDocumentsTypes,
};

const mutations = {
    [types.GET_CLIENT_DOCUMENTS] (state, data) {
        state.fetchingDocuments = true;
        Doc.GET_CLIENT_DOCUMENTS(data).then(
            function ({ data }) {
                    state.fetchingDocuments = false;
                    state.documents = data.data;

            },
            function ( ) {
                state.fetchingDocuments = false;
                Notification.warning({ title: 'warning', message: 'No document found' });
            }
        );
    },

    [types.GET_DOCUMENT_TYPES] (state, d) {
            state.fetchingDocumentsTypes = true;
        Doc.GET_DOCUMENT_TYPES(d).then(
            function ({ data }) {
                    state.fetchingDocumentsTypes = false;
                    state.documentTypes = data;

                    let allTypes = { id: 0, name: 'All' };

                    state.documentTypes.unshift(allTypes);

            },
            function () {
                state.fetchingDocumentsTypes = false;
                    Notification.warning({title: '', message: 'Cannot retrieve document type'});
            }
        );
    },

    [types.GET_DOCUMENTS] (state, doc_type) {
        Doc.GET_DOCUMENTS(doc_type).then(
            function ({ data }) {
                state.documents = data;
            },
            function ( ) {
                Notification.warning({ title: 'warning', message:  'No documents found'  });
            }
        );
    },

    [types.VIEW_DOCUMENT] ( state, uuid ) {

        Doc.VIEW_DOCUMENT(uuid).then(
            function (response) {
                let out = new Blob([response.data], {type:response.headers.get('content-type')});

                let reader = new FileReader();

                reader.onload = function (e) {
                    window.location.href = reader.result;
                };

                reader.readAsDataURL(out);
            },
            function () {
                Notification.warning({title: '', message: 'Failed to load file'});
            }
        );
    }
};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions
}
