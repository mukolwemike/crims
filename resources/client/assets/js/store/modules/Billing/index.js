import * as billingApi from '../../../api/billingApi';

const state = {
    allBills: [],
    clientBills: [],
    airtimeTypes: [],
    sourceFunds: [],
    fetchingAllBills: false,
    fetchingClientBills: false,
    fetchingAirtimeTypes: false,
    fetchingSourceFunds: false,
};


const getters = {
    allBills: state => state.allBills,
    clientBills: state => state.clientBills,
    sourceFunds: state => state.sourceFunds,
    airtimeTypes: state => state.airtimeTypes,
    fetchingAllBills: state => state.fetchingAllBills,
    fetchingAirtimeTypes: state => state.fetchingAirtimeTypes,
    fetchingSourceFunds: state => state.fetchingSourceFunds
};


const mutations = {
    GET_ALL_UTILITY_BILLS(state) {
        state.fetchingAllBills = true;

        billingApi.fetchAllUtility()
            .then(({data}) => {
                state.fetchingAllBills = false;
                state.allBills = data;
            }).catch(error => {
            state.fetchingAllBills = false;
        });
    },

    GET_CLIENT_UTILITY_BILLS(state, clientId) {
        state.fetchingClientBills = true;

        billingApi.fetchClientUtility(clientId)
            .then(({data}) => {
                state.fetchingClientBills = false;
                state.clientBills = data;
            }).catch(error => {
            state.fetchingClientBills = false;
        });
    },

    GET_ALL_AIRTIME_TYPES(state) {
        state.fetchingAirtimeTypes = true;

        billingApi.fetchAirtimeTypes()
            .then(({data}) => {
                state.fetchingAirtimeTypes = false;
                state.airtimeTypes = data;
            }).catch(error => {
            state.fetchingAirtimeTypes = false;
        });
    },

    GET_SOURCE_FUNDS(state) {
        state.fetchingSourceFunds = true;

        billingApi.getSourceFunds()
            .then(({data}) => {
                state.fetchingSourceFunds = false;
                state.sourceFunds = data;
            }).catch(error => {
            state.fetchingSourceFunds = false;
        });
    },
};


export default {
    state, getters, mutations
}