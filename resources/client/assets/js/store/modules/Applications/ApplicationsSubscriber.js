/**
 * Created by daniel on 19/01/2017.
 */
import router from '../../../router';
import * as types from '../../mutation-types';
import {Notification} from 'element-ui';
import * as Apply from '../../../api/ApplicationsApi';
import * as Contact from '../../../api/ContactApi';


const state = {
    applicationProgress: {},
    submitSubscriber: false,
    subscriberErrors: [],
    subscriberForm: {},
    titlesList: [],
    countriesList: [],
    employmentTypesList: [],
    contactMethodsList: [],
    sourceOfFundsList: [],
    businessNatures: [],
    jointHolderModalForms: false,
    genderList: [],
    jointHolderForm: [],
    jointHolderErrors: [],
    submitJointHolder: false,
    formClientJointHolder: {},
    editJointHolderModal: false,
    selectedJointHolder: {},
    bankAccounts: [],
    branches: [],
    fetchingBranches: false,
    loadingbusinessNatures: false,
    selectedFund: '',
    clientUUID: '',
    appUUID: '',
    clientDetails: {},
};

const getters = {
    clientDetails: state => state.clientDetails,

    clientUUID: state => state.clientUUID,

    appUUID: state => state.appUUID,

    submitSubscriber: state => state.submitSubscriber,

    subscriberErrors: state => state.subscriberErrors,

    rootStateForm: (state, getters, rootState) => {
        return Object.assign({}, rootState.appliedRisk, rootState.appliedInvestment);
    },

    subscriberForm: (state, getters, rootState) => {

        if (rootState.applicationId == 'undefined' || rootState.applicationId === undefined || rootState.applicationId === null || rootState.applicationId == 'null') {
            state.subscriberForm = {
                title: '',
                employment_id: '',
                country_id: '',
                method_of_contact_id: '',
                funds_source_id: '',
                corporate_investor_type: '',
                contact_person_title: '',
                financial_advisor: '',
            };
        } else {
            rootState.appliedSubscriber.dob = new Date(rootState.appliedSubscriber.dob);

            state.subscriberForm = rootState.appliedSubscriber;
        }

        return state.subscriberForm;
    },

    titlesList(state) {
        return state.titlesList;
    },
    countriesList: (state) => {
        return state.countriesList;
    },
    employmentTypesList: (state) => {
        return state.employmentTypesList;
    },
    contactMethodsList: (state) => {
        return state.contactMethodsList;
    },
    sourceOfFundsList: (state) => {
        return state.sourceOfFundsList;
    },

    businessNatures: state => state.businessNatures,

    jointHolderModal: state => state.jointHolderModalForms,

    genderList: state => state.genderList,

    jointHolderForm: state => state.jointHolderForm,

    jointHolderErrors: state => state.jointHolderErrors,

    submitJointHolder: state => state.submitJointHolder,

    formClientJointHolder: state => state.formClientJointHolder,

    editJointHolderModal: state => state.editJointHolderModal,

    selectedJointHolder: state => state.selectedJointHolder,

    bankAccounts: state => state.bankAccounts,

    branches: state => state.branches,

    fetchingBranches: state => state.fetchingBranches,
    loadingbusinessNatures: state => state.loadingbusinessNatures
};

const mutations = {
    RESET_JOINT_COLLAPSE(state) {
        state.jointCollapse = [];
    },

    GET_TITLES_LIST(state) {
        Contact.titles()
            .then(
                function (response) {
                    state.titlesList = response.data;

                    if (state.subscriberForm.title === null ||
                        state.subscriberForm.title === undefined
                    ) {
                        state.subscriberForm.title = state.titlesList[0].id.toString();
                    }
                    if (state.subscriberForm.contact_person_title === null ||
                        state.subscriberForm.contact_person_title === undefined
                    ) {
                        state.subscriberForm.contact_person_title = state.titlesList[0].id.toString();
                    }

                },
                function (response) {

                }
            );
    },

    [types.GET_JOINT_HOLDERS](state, applId) {
        Apply.GET_JOINT_HOLDERS(applId).then(
            function (response) {

                state.jointHolderForm = response.data;

            },
            function () {

                state.jointHolderForm = {};

            }
        );
    },

    GET_COUNTRIES_LIST(state) {

        Contact.countries()
            .then(
                function (response) {
                    state.countriesList = response.data;

                    if (state.subscriberForm.country_id === null ||
                        state.subscriberForm.country_id === undefined
                    ) {
                        state.subscriberForm.country_id = state.countriesList[113].id.toString();
                    }

                },
                function (response) {

                }
            );
    },
    GET_EMPLOYMENT_TYPES_LIST(state) {

        Contact.employmentTypes()
            .then(
                function (response) {
                    state.employmentTypesList = response.data;

                    if (state.subscriberForm.employment_id === null ||
                        state.subscriberForm.employment_id === undefined) {
                        state.subscriberForm.employment_id = state.employmentTypesList[0].id.toString();
                    }
                },
                function (response) {

                }
            );
    },
    GET_CONTACT_METHOD_LIST(state) {

        Contact.contactMethods()
            .then(
                function (response) {
                    state.contactMethodsList = response.data;

                    if (state.subscriberForm.method_of_contact_id === null ||
                        state.subscriberForm.method_of_contact_id === undefined
                    ) {
                        state.subscriberForm.method_of_contact_id = state.contactMethodsList[0].id.toString();
                    }

                },
                function (response) {

                }
            );
    },
    GET_SOURCE_OF_FUNDS_LIST(state) {

        Contact.sourceOfFunds()
            .then(
                function (response) {
                    state.sourceOfFundsList = response.data;
                    if (state.subscriberForm.funds_source_id === null
                        || state.subscriberForm.funds_source_id === undefined
                    ) {
                        state.subscriberForm.funds_source_id = state.sourceOfFundsList[0].id.toString();
                    }

                },
                function (response) {

                }
            );
    },

    GET_BUSINESS_NATURES_LIST(state) {
        state.loadingbusinessNatures = true;

        Contact.businessNatures()
            .then(function (response) {

                    state.businessNatures = response.data;

                    state.loadingbusinessNatures = false;

                    if (state.subscriberForm.corporate_investor_type === null ||
                        state.subscriberForm.corporate_investor_type === undefined
                    ) {
                        state.subscriberForm.corporate_investor_type = state.businessNatures[0].id.toString();
                    }
                },

                function (response) {
                    state.loadingbusinessNatures = false;
                }
            );
    },

    [types.FILL_SUBSCRIBER](state, submit_form) {
        state.submitSubscriber = true;

        Apply.FILL_SUBSCRIBER(state, submit_form)
            .then(
                function (response) {
                    state.submitSubscriber = false;

                    state.subscriberForm = submit_form;

                    Notification.success({
                        title: 'Form Saved',
                        message: 'The form was saved successfully, and client created, we have sent an sms'
                    });

                    state.clientDetails = response.data.client;
                    state.clientUUID = (response.data.client) ? response.data.client.uuid : null;
                    state.appUUID = response.data.application;

                    sessionStorage.setItem('application-form', response.headers.get('Location'));

                    router.push({name: (state.selectedFund === 'CMMF') ? 'apply.deposit' : 'apply.kyc'});

                    state.applicationProgress.investment = true;
                    state.applicationProgress.subscriber = true;
                },
                function (response) {

                    state.submitSubscriber = false;

                    if (response.status === 422) {
                        state.subscriberErrors = response.data;

                        Notification.warning({
                            title: 'Error saving form',
                            message: 'Some required details are missing in the form'
                        });
                    } else {
                        Notification.error({
                            title: 'Error saving form',
                            message: 'Some errors were encountered when saving the form'
                        });
                    }
                }
            );
    },

    GET_GENDER(state) {
        Contact.gender().then(
            function (response) {
                state.genderList = response.data;
            }
        );
    },

    jointHolderModal(state, active) {
        state.formClientJointHolder = {};
        state.jointHolderErrors = '';
        state.jointHolderModalForms = active;
    },

    [types.SUBMIT_CLIENT_JOINT_HOLDER_FORM](state, form) {
        state.submitJointHolder = true;

        Apply.VALIDATE_JOINT_HOLDER(form).then(
            function () {

                state.jointHolderForm.push(form);
                state.jointHolderModalForms = false;

                state.submitJointHolder = false;
                state.jointHolderErrors = '';

                state.formClientJointHolder = {};

                swal("Joint Holder", "You have successfully added a joint holder", "success");

            },
            function (response) {

                state.submitJointHolder = false;
                state.jointHolderModalForms = true;

                if (response.status == 422) {
                    state.jointHolderErrors = response.data;

                    Notification.warning({title: 'Warning', message: 'Please fill all required fields.'});
                } else {
                    Notification.error({
                        title: 'Error',
                        message: 'An error occurred while adding the joint holder details'
                    });
                }
            }
        );
    },

    EDIT_JOINT_HOLDER_MODAL(state, data) {
        state.editJointHolderModal = data.status;
        state.selectedJointHolder = data.holder;
    },

    EDIT_JOINT_HOLDER(state, data) {
        state.editJointHolderModal = data.status;
        state.selectedJointHolder = data.holder;
        Notification.success({title: 'Success', message: 'Joint Holder update successfully'});
    },

    GET_BANK_LIST(state) {
        Apply.getBanks()
            .then(({data}) => {
                state.bankAccounts = data;
            }, () => {
            });
    },

    FETCH_BANK_BRANCHES(state, bankId) {
        state.fetchingBranches = true;
        Apply.getBankBranches(bankId).then(
            ({data}) => {
                state.branches = data;
                state.fetchingBranches = false;
            },
            () => {
                state.fetchingBranches = false;
            }
        );
    },

};

const actions = {
    fill_subscriber({state, rootState, commit}, data) {
        rootState.appliedSubscriber = data;

        state.subscriberForm = data;

        if (Object.keys(rootState.appliedInvestment).length === 0) {
            Notification.warning('Please fill the investment section before submitting');

            router.push({name: 'apply.investment'});
        } else if (Object.keys(rootState.appliedRisk).length === 0 && rootState.selectedFund !== 'CMMF') {
            Notification.warning('Please fill the risk assessment section before submitting');

            router.push({name: 'apply.risk'});
        } else {
            state.applicationProgress = rootState.applicationProgress;

            var submit_form = Object.assign({}, rootState.appliedRisk, rootState.appliedInvestment, data);

            submit_form.individual = rootState.individualApplication;

            var sub = rootState.individualApplication ? 'individual' : 'corporate';

            var id = rootState.applicationId ? '/' + rootState.applicationId : '';

            state.selectedFund = rootState.selectedFund;

            submit_form.subroute = sub + id;

            submit_form.jointHolders = state.jointHolderForm;

            commit('FILL_SUBSCRIBER', submit_form);
        }
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}
