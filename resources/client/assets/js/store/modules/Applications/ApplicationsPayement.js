/**
 * Created by daniel on 20/01/2017.
 */
import router from '../../../router';
import {Message, Notification} from 'element-ui';
import * as types from '../../mutation-types';
import * as Payment from '../../../api/ApplicationsApi';

const state = {
    applicationProgress: {},
    applicationId: null,
    completingApplication: false,
    payment_complete: false,
    complete_application: false,
    dialogBank: false,
    dialogMpesa: false,
    paymentErrors: {},
    sendingSMS: false,
    financialAdvisor: '',
};

const getters = {
    financialAdvisor: state => state.financialAdvisor,
    retrieveApplSummary: (state, getters, rootState) => {

        return {
            amount: rootState.appliedInvestment.amount,
            tenor: rootState.appliedInvestment.tenor,
            agreed_rate: rootState.appliedInvestment.agreed_rate,
            kyc_complete: rootState.applicationDocuments.length > 3,
            financial_advisor:
                (rootState.appliedSubscriber.financial_advisor != null || rootState.appliedSubscriber.financial_advisor !== undefined)
                ? rootState.appliedSubscriber.financial_advisor : null,
            name: function () {
                if (rootState.individualApplication) {
                    return rootState.appliedSubscriber.firstname + ' ' + rootState.appliedSubscriber.lastname;
                } else {
                    return rootState.appliedSubscriber.registered_name;
                }
            }
        }
    },

    completingApplication: state => state.completingApplication,
    payment_complete: state => state.payment_complete,
    complete_application: state => state.complete_application,
    dialogBank: state => state.dialogBank,
    dialogMpesa: state => state.dialogMpesa,
    paymentErrors: state => state.paymentErrors,
    sendingSMS: state => state.sendingSMS
};

const mutations = {
    GET_REFERRED_FA(state, referralCode) {
        Payment.GET_REFERRED_FA(referralCode).then(
            (response)=>{
                state.financialAdvisor = response.data.financialAdvisor;
            }
        )
    },

    [types.APPL_COMPLETE](state, applId) {

        state.completingApplication = true;

        state.applicationProgress.complete_application = true;

        Payment.APPL_COMPLETE(applId)

            .then(
                function (response) {

                    router.push({name: 'apply.complete'});
                    state.completingApplication = false;

                },
                function (response) {
                    if (response.status == 400) {

                        router.push({name: 'apply.kyc'});

                        Notification.error({title: 'Error', message: response.data.message});
                    } else {
                        Notification.error({
                            title: 'Error',
                            message: 'Error. An error occurred while completing your application'
                        });
                    }

                    state.completingApplication = false;
                }
            );

    },

    UPLOAD_PAYMENT_COMPLETE(state, data) {

        Payment.UPLOAD_PAYMENT_COMPLETE(data.applId, data.amount).then(
            function (response) {
                Message.success('The payment transfer is uploaded successfully');

                state.applicationProgress.payment = true;

                state.payment_complete = true;

                state.dialogBank = true;
            },
            function (response) {
                state.applicationProgress.payment = false;

                state.payment_complete = false;

                state.dialogBank = true;
            }
        )
    },

    ACTIVATE_DIALOG_BANK(state, data) {
        state.dialogBank = data.value;
    },

    ACTIVATE_DIALOG_MPESA(state, data) {
        state.dialogMpesa = data.value;
    },


    CONFIRM_MPESA_DEPOSIT(state, data) {
        Payment.CONFIRM_MPESA_DEPOSIT(data.applId, data).then(
            function (response) {
                Message.success('The payment transfer is uploaded successfully');

                state.applicationProgress.payment = true;

                state.payment_complete = true;

                state.dialogMpesa = true;
            },
            function (response) {
                state.paymentErrors = response.body;

                state.applicationProgress.payment = false;

                state.payment_complete = false;

                state.dialogMpesa = true;
            }
        )
    },

    REQUEST_BANK_DETAIL_SMS(state, data) {
        state.sendingSMS = true;

        Payment.REQUEST_BANK_DETAIL_SMS(data).then(
            () => {
                state.sendingSMS = false;
                Message.success('Bank Details SMS was successfully sent.');
            },
            () => {
                state.sendingSMS = false;
                Message.error('Bank Detail request by SMS was unsuccessful. Please confirm your Phone Number');
            }
        )
    }
};

const actions = {
    appl_complete({rootState, state, commit}) {

        let applId = rootState.applicationId;

        state.applicationProgress = rootState.applicationProgress;

        commit('APPL_COMPLETE', applId);
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}