/**
 * Created by daniel on 18/01/2017.
 */
import router from '../../../router';
import * as types from '../../mutation-types';
import {Message} from 'element-ui';
import * as Apply from '../../../api/ApplicationsApi';
import * as Inv from '../../../api/InvestmentsApi';
import * as Contact from "../../../../../../admin/assets/js/api/applicationPartials";
import * as portfolio from "../../../../../../admin/assets/js/api/portfolioApi";

const state = {
    applicationProgress: {},

    investmentErrors: [],
    submitInvestment: false,
    investmentRate: null,
    productsList: [],
    allUnitFunds: [],
    allShareEntities: [],
    cisUnitFunds: [],
    loadingCisFunds: true,
    unitFundDetails: {},
    unitTrustFundDetails: {},
    unitFundBankAccounts: [],
    unitFundMpesaAccounts: [],
    fetchingUnitFund: false,
    fetchingRate: false,
    applicationProduct: null,
    selectedFund: '',
    formType: 'long',
    tenorsList: [
        // {id: "1", name: '1 Month'},
        {id: "3", name: '3 Months'},
        {id: "6", name: '6 Months'},
        {id: "9", name: '9 Months'},
        {id: "12", name: '1 Year'},
        {id: "24", name: '2 Years'},
        {id: "36", name: '3 Years'}
    ],

    chysTenorList: [
        {id: "3", name: '3 Months'},
        {id: "6", name: '6 Months'},
        {id: "9", name: '9 Months'},
        {id: "12", name: '1 Year'},
    ],

    invApplForm: {},
    interestRate: null,
    default_fa: '',
    bankCurrencies:[]
};

const getters = {
    selectedFund: state => state.selectedFund,

    submitInvestment: state => state.submitInvestment,

    investmentErrors: state => state.investmentErrors,

    applicationProduct: state => state.applicationProduct,

    formType: state => state.formType,

    invApplForm: (state, getters, rootState) => {
        if (rootState.applicationId == 'undefined' || rootState.applicationId === undefined || rootState.applicationId === null || rootState.applicationId == 'null') {
            state.invApplForm = {
                amount: null,
                unit_fund_id: null,
                product_id: null,
                tenor: null,
                appType: 'individual',
                item_id: '',
            };
        } else {
            state.invApplForm = rootState.appliedInvestment;
        }

        return state.invApplForm;
    },

    productsList: (state) => {
        return state.productsList;
    },
    tenorsList: (state) => {
        return state.tenorsList;
    },

    chysTenorList: state => {
        return state.chysTenorList;
    },

    allUnitFunds: (state) => {
        return state.allUnitFunds;
    },
    allShareEntities: (state) => {
        return state.allShareEntities;
    },
    cisUnitFunds: (state) => {
        return state.cisUnitFunds;
    },
    loadingCisFunds: state => state.loadingCisFunds,
    investmentRate: state => state.investmentRate,

    interestRate: state => state.interestRate,

    fetchingRate: state => state.fetchingRate,

    fetchingUnitFund: state => state.fetchingUnitFund,

    unitFundDetails: state => state.unitFundDetails,

    unitTrustFundDetails: state => state.unitTrustFundDetails,

    unitFundBankAccounts: state => state.unitFundBankAccounts,

    unitFundMpesaAccounts: state => state.unitFundMpesaAccounts,

    default_fa: state => state.default_fa,
    bankCurrencies: state => state.bankCurrencies,
};

const mutations = {
    FETCH_CURRENCIES(state) {
        Inv.getCurrencies()
            .then(({data}) => {
                state.bankCurrencies = data.data;
            });
    },
    SET_DEFAULT_FA(state, referral_code) {
        Contact.getAllFas()
            .then(({data}) => {
                let fas = data.data;
                let fa = fas.filter(fa => fa.referral_code === referral_code)[0];

                if (fa) {
                    state.default_fa = fa;
                } else {
                    Message.error('Unknown referral code supplied please confirm.');
                }

            })

    },
    GET_PRODUCTS_LIST(state) {

        Inv.getProducts('cms')
            .then(
                function (response) {
                    state.productsList = response.data.data;

                    if (state.invApplForm.product_id === undefined || state.invApplForm.product_id === null) {
                        state.invApplForm.product_id = state.productsList[0].id.toString();
                    }

                },
                function (response) {

                }
            );
    },

    GET_ALL_UNIT_FUNDS(state) {

        Inv.getAllUnitFunds()
            .then(
                function (response) {
                    state.allUnitFunds = response.data.data;

                    if (state.invApplForm.unit_fund_id === undefined || state.invApplForm.unit_fund_id === null) {
                        state.invApplForm.unit_fund_id = state.allUnitFunds[0].id.toString();
                    }

                },
            );
    },

    GET_ALL_SHARE_ENTITY(state) {

        Inv.getAllShareEntity()
            .then(
                function (response) {
                    state.allShareEntities = response.data.data;

                    if (state.invApplForm.entity_id === undefined || state.invApplForm.entity_id === null) {
                        state.invApplForm.entity_id = state.allUnitFunds[0].id.toString();
                    }

                },
            );
    },

    GET_CIS_UNIT_FUNDS(state) {
        state.loadingCisFunds = true;

        Inv.getCISUnitFunds()
            .then(
                function (response) {
                    state.cisUnitFunds = response.data.data;
                    state.loadingCisFunds = false;

                }, () => {
                    state.loadingCisFunds = false;

                }
            );
    },
    GET_AGREED_INTEREST_RATE(state, form) {

        state.fetchingRate = true;
        Inv.getAgreedInterestRate(form)
            .then(
                ({data}) => {
                    state.fetchingRate = false;
                    state.interestRate = data;

                },
                () => {
                    state.fetchingRate = false;
                    Message.warning('Failed to get agreed interest rate');
                }
            );
    },
    GET_UNIT_FUND_DETAILS(state, unitFundId) {
        state.fetchingUnitFund = true;

        Inv.getUnitFundDetails(unitFundId).then(
            ({data}) => {
                state.unitFundDetails = data.data;
                state.fetchingUnitFund = false;
            },
            () => {
                state.unitFundDetails = {};
                state.fetchingUnitFund = false;
            }
        );
    },

    APPLICATION_PRODUCT(state, product) {
        state.applicationProduct = product;
    },

    GET_UNIT_TRUST_FUND_DETAILS(state, unitFundId) {
        state.fetchingUnitFund = true;

        Inv.getUnitTrustFundDetails(unitFundId).then(
            ({data}) => {
                state.unitTrustFundDetails = data;
                state.fetchingUnitFund = false;
            },
            () => {
                state.unitTrustFundDetails = {};
                state.fetchingUnitFund = false;
            }
        );
    },

    [types.FILL_INVESTMENT](state, data) {

        if (data.proceed == true) {
            state.submitInvestment = true;
        }

        let filled = Apply.FILL_INVESTMENT(state, data);

        if (filled.rate) {
            filled.rate.then(
                function ({data}) {
                    state.investmentRate = parseFloat(data) + '%';
                    state.interestRate = parseFloat(data);
                },
                function (response) {
                    Message.warning('Could not obtain interest rates');
                }
            );
        }

        filled.validation.then(
            function (response) {
                state.investmentErrors = '';

                if (data.proceed == true) {

                    state.applicationProgress.investment = true;

                    state.submitInvestment = false;

                    if (data.product_category === 'funds' && data.appType === 'individual') {
                        router.push({name: (state.selectedFund === 'CMMF') ? 'apply.subscriber' : 'apply.risk'});
                    } else {
                        router.push({name: 'apply.risk'});
                    }

                    Message.success('Investment details saved');
                }
            },

            function (response) {
                if (data.proceed == true) {
                    state.submitInvestment = false;
                }

                if (!(router.path = 'apply.investment') && (router.path = 'apply')) {
                    router.push({path: 'apply.investment'}); //return back
                }

                if (response.status == 422) {
                    state.investmentErrors = response.data;

                    if (data.proceed == true) {
                        Message.warning({
                            title: 'Validation Failed',
                            message: 'Some required information may be missing in the form'
                        });
                    }
                } else {
                    if (data.proceed == true) {
                        Message.error({
                            title: 'Error',
                            message: 'Some errors were encountered when submitting the form'
                        });
                    }
                }
            }
        );
    }
};

const actions = {
    fillInvestment({state, rootState, commit}, data) {

        rootState.appliedInvestment = data;

        state.investmentForm = data;

        state.applicationProgress = rootState.applicationProgress;

        if (data.appType === 'individual') {
            rootState.individualApplication = true;
        } else rootState.individualApplication = data.appType === 'joint';

        if (data.product_category === 'funds') {
            state.selectedFund = rootState.selectedFund = state.cisUnitFunds.filter((fund) => {
                return fund.id == data.unit_fund_id;
            })[0].short_name;

            rootState.formType = state.formType = 'long';

            if (state.selectedFund === 'CMMF' && data.appType === 'individual') {
                rootState.formType = state.formType = 'short';
            }
        }

        delete (data['item_id']);

        commit('FILL_INVESTMENT', data);
    },
    getTenorsList({state}, form) {

        state.productsList.forEach(
            function (product) {

                if (product.id.toString() == form.product_id) {
                    if (product.shortname == 'RIS') {
                        state.tenorsList = [
                            {id: "12", name: '1 Year'},
                            {id: "24", name: "2 Years"},
                            {id: "36", name: "3 Years"},
                            {id: "48", name: "4 Years"},
                            {id: "60", name: "5 Years"}
                        ];
                    } else {
                        state.tenorsList = [
                            {id: "1", name: '1 Month'},
                            {id: "3", name: '3 Months'},
                            {id: "6", name: '6 Months'},
                            {id: "9", name: '9 Months'},
                            {id: "12", name: '1 Year'},
                            {id: "24", name: '2 Years'},
                            {id: "36", name: '3 Years'},
                        ];
                    }
                }
            }
        );
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}
