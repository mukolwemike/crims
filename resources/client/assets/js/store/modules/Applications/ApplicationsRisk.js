/**
 * Created by daniel on 18/01/2017.
 */
import router from '../../../router';
import * as types from '../../mutation-types';
import {Message} from 'element-ui';
import * as Apply from '../../../api/ApplicationsApi';

const state = {
    applicationProgress: {},
    submitRisk: false,
    riskErrors: [],
    riskForm: {},
};

const getters = {

    submitRisk: state => state.submitRisk,

    riskErrors: state => state.riskErrors,

    riskForm: (state, getters, rootState) => {

        if (rootState.applicationId == 'undefined' || rootState.applicationId === undefined || rootState.applicationId === null || rootState.applicationId == 'null') {
            state.riskForm = {
                'risk_duration': null,
                'risk_inv_types': null,
                'risk_inv_volatility': null,
                'risk_inv_objective': null,
                'risk_trade_frequency': null,
                'risk_knowledge': null,
                'risk_profile': null
            };
        } else {
            state.riskForm = rootState.appliedRisk;
        }

        return state.riskForm;
    }
};

const mutations = {
    [types.FILL_RISK](state, cont) {

        state.submitRisk = true;

        Apply.FILL_RISK(state, cont)
            .then(
                function () {
                    router.push({name: 'apply.subscriber'});

                    state.applicationProgress.risk = true;

                    Message.success('Risk questionnaire completed successfully');

                    state.riskErrors = '';

                    state.submitRisk = false;

                },
                function (response) {
                    state.submitRisk = false;

                    if (response.status == 422) {
                        state.riskErrors = response.data;

                        Message.warning('Please fill all required fields.');
                    } else {
                        Message.error('An error occurred while submitting Risk form details');
                    }
                }
            );
    }
};

const actions = {
    fill_risk({commit, rootState, state}, data) {
        rootState.appliedRisk = data;

        state.applicationProgress = rootState.applicationProgress;

        commit('FILL_RISK', data);

    }
};


export default {
    state,
    getters,
    actions,
    mutations
}