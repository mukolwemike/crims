/**
 * Created by daniel on 19/01/2017.
 */
import {Message} from 'element-ui';
import * as Appl from '../../../api/ApplicationsApi';

const state = {
    applicationProgress: {},
    uploadDocuments: {},
    kyc_complete: false
};

const getters = {
    uploadDocuments: state => state.uploadDocuments,
    applicationDocuments: (state, getters, rootState) => {
        return rootState.applicationDocuments;
    },
    applId: (state, getters) => {
        return getters.applicationId ? getters.applicationId : sessionStorage.getItem('application-form');
    },
    kyc_complete: state => state.kyc_complete
};

const mutations = {
    UPLOAD_KYC_COMPLETE({state, rootState}, applId) {

        Appl.UPLOAD_KYC_COMPLETE(applId).then(
            function (response) {

                Message.success('The kyc documents are all uploaded successfully');

                state.applicationProgress.kyc = true;

                state.kyc_complete = true;
            },

            function (response) {
                state.applicationProgress.kyc = false;
            }
        )
    },

    GET_DOCUMENTS(state, applId) {

        Appl.GET_DOCUMENTS(applId).then(
            function (response) {
                state.uploadDocuments = response.data.data.documents;

                console.log(state.uploadDocuments, "uploaded documents");
            },
            function (response) {
            }
        );
    },

    FILL_KYC(state) {
        // state.applicationProgress.kyc = true;
        // router.push({name: 'apply.mandate'});
    }
};

const actions = {};


export default {
    state,
    getters,
    actions,
    mutations
}
