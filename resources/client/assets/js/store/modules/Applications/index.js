/**
 * Created by daniel on 17/01/2017.
 */
import router from '../../../router';
import { Notification } from 'element-ui';
import * as types from '../../mutation-types';
import * as Apply from '../../../api/ApplicationsApi';
import Risk from './ApplicationsRisk';
import Invest from './ApplicationsInvestment';
import Subscribe from './ApplicationsSubscriber';
import KYC from './ApplicationsKYC';
import Payment from './ApplicationsPayement';
import Mandate from './ApplicationsMandate';
// import JointHolders from './ApplicationJointHolder'

const state = {
    applicationId: null,
};

const getters = {

    //Overall application
    individualApplication: (state, getters, rootState) => {
        return rootState.individualApplication;
    },

    applicationId: (state, getters, rootState) => {

        if (rootState.applicationId == 'undefined' || rootState.applicationId === undefined ||rootState.applicationId === null || rootState.applicationId == 'null') {
            rootState.applicationId = null;
        } else {
            Apply.FETCH_APPLICATION(rootState.applicationId, state, rootState);
        }

        return rootState.applicationId;
    },

    applicationProgress: (state, getters, rootState) => {

        window.onbeforeunload = function () {
            return "Data will be lost if you leave the page, are you sure?";
        };

        return rootState.applicationProgress;
    },
};

const mutations = {

    [types.LOAD_UUID] (state, applId) {

        Apply.LOAD_UUID(applId)
            .then(
                function (response) {

                    sessionStorage.setItem('application-form', applId);

                    router.push({name: 'apply.kyc'});

                    Notification.success({title: 'Success', message: 'The application has been resumed'});

                },
                function (response) {
                    router.push({name: 'landing'});

                    Notification.error({title: 'Error', message: 'The application was not found, please start a new one'});
                }
            );

    },
    // [types.GET_JOINT_HOLDERS] (state, applId) {
    //     Apply.GET_JOINT_HOLDERS(applId).then( function (response) {
    //
    //         state.jointHolderForm = response.data;
    //
    //     }, function () {
    //
    //         state.jointHolderForm = {};
    //
    //     });
    // }
};

const actions = {
    selectIndividual({rootState, getters}) {

        state.applicationId = getters.applicationId;

        rootState.individualApplication = true;

    },
    selectCorporate({rootState, getters}) {

        state.applicationId = getters.applicationId;

        rootState.individualApplication = false;

    },
    loadUuid({commit, rootState, state}, applId) {

        rootState.applicationId = applId;

        commit('LOAD_UUID', applId);

        Apply.FETCH_APPLICATION(rootState.applicationId, state, rootState);
    },
    loadAppl({rootState, state}) {

        Apply.FETCH_APPLICATION(rootState.applicationId, state, rootState);

    }
};


export default {
    state,
    getters,
    actions,
    mutations,
    modules: {
        Risk,
        Invest,
        Subscribe,
        KYC,
        Payment,
        Mandate
    },
    }
