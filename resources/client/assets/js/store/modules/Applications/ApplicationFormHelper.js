/**
 * Created by daniel on 17/01/2017.
 */
/*
 * Fill investment section from data pulled from the server
 */
const fillInvestment = function (appl) {
    var keys = ['amount', 'product_id', 'unit_fund_id', 'tenor', 'agreed_rate'];

    return filter(appl, keys);
};

/*
 * Fill the risk section
 */
const fillRisk = function (appl) {
    var keys = ['risk_duration', 'risk_inv_types', 'risk_inv_volatility', 'risk_inv_objective', 'risk_trade_frequency', 'risk_knowledge', 'risk_profile'];

    return filter(appl, keys);
};

/*
 * Fill subscriber section from data pulled from the server
 */
const fillSubscriber = function (appl) {
    //keys
    var keys = [
        'title', 'firstname', 'middlename', 'lastname', 'dob', 'id_or_passport', 'pin_no', 'email',

        'telephone_home', 'telephone_office', 'country_id', 'gender_id', 'postal_code', 'postal_address', 'street',

        'town', 'method_of_contact_id', 'employment_id', 'employment_other', 'employer_name', 'employer_address',

        'present_occupation', 'business_sector', 'kin_name', 'kin_phone', 'kin_postal', 'kin_email',

        'corporate_investor_type', 'funds_source_id', 'funds_source_other', 'registered_name', 'trade_name',

        'registered_address', 'registration_number', 'contact_person_title', 'contact_person_fname', 'contact_person_lname',

        'investor_account_name', 'investor_account_number', 'investor_bank', 'investor_bank_branch',

        'investor_clearing_code', 'investor_swift_code'
    ];

    return filter(appl, keys);
};

const filter = function (data, keys) {
    var out = {};

    keys.forEach(
        function (val) {
            if (data.hasOwnProperty(val)) {
                out[val] = data[val];
            }
        }
    );

    return out;
};

export default {
    fillInvestment,
    fillRisk,
    fillSubscriber,
    filter
}