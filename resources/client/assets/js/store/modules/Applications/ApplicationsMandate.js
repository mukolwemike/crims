/**
 * Created by mukolwe on 17/12/2018.
 */
import router from '../../../router';
import { Notification } from 'element-ui';
import * as Appl from '../../../api/ApplicationsApi';

const state = {
    applicationProgress: {},
    uploadDocuments: {},
    defaultHeaders: {},
    uploadMandates:{}
};

const getters = {
    uploadDocuments: state => state.uploadDocuments,
    applicationDocuments: (state, getters, rootState) => {
        return rootState.applicationDocuments;
    },
    applId: (state, getters) => {
        return getters.applicationId ? getters.applicationId: sessionStorage.getItem('application-form');
    },
    defaultHeaders: state => state.defaultHeaders,
    uploadMandates: state => state.uploadMandates,
};

const mutations = {
    SET_DEFAULT_HEADERS(state, headers) {
        state.defaultHeaders = headers;
    },

    UPLOAD_MANDATE_COMPLETE(state, applId) {

        Appl.UPLOAD_MANDATE_COMPLETE(applId).then(
            function (response) {

                Notification.success({title: 'Mandate Uploaded', message: 'The Mandate Signature successfully updated'});
                state.applicationProgress.mandate = true;

                router.push({ name: 'apply.payment' });
            },
            function (response) {
                state.applicationProgress.mandate = false;
            }
        )
    },

    GET_DOCUMENTS(state, applId) {

        Appl.GET_DOCUMENTS(applId).then(
            function (response) {
                state.uploadDocuments = response.data.data.documents;
            },
            function (response) {
            }
        );
    },
};

const actions = {

};


export default {
    state,
    getters,
    actions,
    mutations
}
