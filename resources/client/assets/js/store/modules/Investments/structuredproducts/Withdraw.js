/**
 * Created by daniel on 25/01/2017.
 */

import * as types from '../../../mutation-types';
import { Notification, Message } from 'element-ui';
import * as Inv from '../../../../api/InvestmentsApi';
import event from '../../../../events';
import router from '../../../../router/index';

const state = {
    submitWithdraw: false,
    withdrawForm: {},
    submitWithdrawSuccess: false,
    withdrawErrors: {},
    pendingWithdraw: null,

    /**
     * new withdrawal states
     */
    newAccountModal: false,
    confirmWithdrawal: false,
    withdrawalForm: {},
    bankDetails: {},
    submitWithdrawal: false,
    setBankDetails: false,
    withdrawOption: '',
    withdrawalSuccess: false,
    loadingWithdrawConfirm: false,
    withdrawFormStep: 'form'
};

const getters = {

    submitWithdraw: state => state.submitWithdraw,

    withdrawForm: (state) => {

        state.withdrawForm = {
            amount_select: 'principal_interest',
            withdrawal_stage: 'mature'
        };

        return state.withdrawForm;
    },

    submitWithdrawSuccess: state => state.submitWithdrawSuccess,

    withdrawErrors: state => state.withdrawErrors,

    /**
     * New withdrawals
     * @param state
     */

    newAccountModal: state => state.newAccountModal,

    confirmWithdrawal: state => state.confirmWithdrawal,

    withdrawalForm: state => state.withdrawalForm,

    bankDetails: state => state.bankDetails,

    submitWithdrawal: state => state.submitWithdrawal,

    setBankDetails: state => state.setBankDetails,
    withdrawOption: state=> state.withdrawOption,
    withdrawalSuccess: state=> state.withdrawalSuccess,
    loadingWithdrawConfirm: state => state.loadingWithdrawConfirm,
    withdrawFormStep: state => state.withdrawFormStep

};

const mutations = {
    [types.SUBMIT_WITHDRAW] (state, data) {

        state.submitWithdraw = true;

        Inv.SUBMIT_WITHDRAW(data)
            .then(function () {

                state.submitWithdraw = false;
                state.submitWithdrawSuccess = true;
                state.withdrawFormStep = 'success';
                state.withdrawErrors = {};
                Notification.success({title: "Withdrawal", message: 'Withdrawal instruction was sent successfully'});

                try {
                    $('#withdrawModal').foundation('close');
                    $('#rolloverModal').foundation('close');
                } catch (err) {
                }

                event.bus.$emit('refresh_investment');

                state.pendingWithdraw = null;

            }, function (response) {
                state.submitWithdraw = false;

                state.submitWithdrawSuccess = false;

                if (response.status === 422) {
                    state.withdrawErrors = response.data;

                    Notification.warning({title: 'Withdrawal', message: 'An Error Occurred while submitting Withdraw form. Some data may be missing on the instruction'});
                } else {
                    Notification.warning({title: 'Withdrawal', message: 'An Error Occurred while submitting Withdraw form. There was an error sending the instruction'});
                }
            });
    },

    /**
     * New withdrawal mutations
     * @param state
     * @param status
     */
    [types.SET_BANK_DETAILS](state, account) {
        state.bankDetails = {};
        Inv.getBankDetails(account).then(
            ({ data }) =>
            {
                state.bankDetails = data;
            },
            () =>
            {
                state.bankDetails = {};
            }
        );
    },
    OPEN_WITHDRAWAL_CALLOUT(state, option){
        state.withdrawOption = option;
    },
    CONFIRM_WITHDRAWAL(state, { form, status }) {
        state.loadingWithdrawConfirm = true;
        if (!form.amount) {
            Inv.getAmountSelected(form).then(({ data }) => {
                if (data != null) {
                    form.amount = data.amount;  
                    form.currency = data.currency
                }
                state.confirmWithdrawal = status;
                state.withdrawalForm = form;
                state.loadingWithdrawConfirm = true;
                state.withdrawFormStep = 'confirm'

            }, ()=>
            {
                form.amount = form.amount_select;
                state.confirmWithdrawal = status;
                state.confirmWithdrawal = status;
                state.withdrawalSuccess = false;
                state.withdrawalForm = form;
                state.loadingWithdrawConfirm = true;
                state.withdrawFormStep = 'confirm'
            });
        } else {
            state.confirmWithdrawal = status;
            state.withdrawalForm = form;
            state.loadingWithdrawConfirm = true;
            state.withdrawFormStep = 'confirm'
        }
    },

    SUBMIT_CLIENT_WITHDRAWAL_FORM (state, form)
    {
        state.submitWithdrawal = form;

        Inv.submitWithdrawal(form).then(
            ({data})=> {
                state.submitWithdrawal = false;

                state.confirmWithdrawal = false;

                if(data.status === 406) {
                    Message.warning({ message: 'Premature withdrawal is not allowed'})
                }
                else {
                    Message.success({ message: 'Withdrawal instruction sent succesfully.' });

                    event.bus.$emit('refresh_investment');

                    state.pendingWithdraw = null;

                    state.withdrawalSuccess = true;
                    state.withdrawFormStep = 'success'
                }

            }, ()=> {
                state.submitWithdrawal = false;

                state.confirmWithdrawal = false;

                Message.warning({ message: 'Failed to submit withdrawal instruction. An Error Occured. Please Try again later.' });
            }
        );
    },

    accountModal(state, status) {
        state.bankDetails = {};
        state.newAccountModal = status;
    }
};

const actions = {

    submitWithdraw({commit, rootState}, form) {

        let data = {
            invId : form.invId,
            withdrawal_stage : form.withdrawal_stage,
            withdrawal_date : form.withdrawal_date,
            amount_select : form.amount_select,
            amount : form.amount,
            reason : form.reason
        };

        commit('SUBMIT_WITHDRAW', data);
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}
