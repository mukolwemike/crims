/**
 * Created by daniel on 23/01/2017.
 */
import List from './Details';
import Topup from './Topup';
import Withdraw from './Withdraw';
import Rollover from './Rollover';


const state = {
    statementClient: JSON.parse(localStorage.getItem('statement_client'))
};

const getters = {
    statementClient: (state) => {
        return state.statementClient === null ? {} : state.statementClient;
    }

};

const mutations = {
};

const actions = {
    setStatementClient({state, getters}, data) {
        if (data === undefined) {
            state.statementClient = getters.selectedClient;
            localStorage.setItem('statement_client', JSON.stringify(getters.selectedClient));
        } else {
            state.statementClient = data;

            localStorage.setItem('statement_client', JSON.stringify(data));
        }
    }
};


export default {
    state,
    getters,
    actions,
    mutations,
    modules: {
        List,
        Topup,
        Withdraw,
        Rollover
    },
    }
