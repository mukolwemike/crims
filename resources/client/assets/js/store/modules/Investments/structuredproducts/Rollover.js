/**
 * Created by daniel on 25/01/2017.
 */

import * as types from '../../../mutation-types';
import {Notification, Message} from 'element-ui';
import * as Inv from '../../../../api/InvestmentsApi';
import event from '../../../../events';
import router from '../../../../router';

const state = {
    submitRollover: false,
    rolloverForm: {},
    submitRolloverSuccess: false,
    rolloverErrors: {},
    pendingRollover: null,
    confirmRollover: false,
    agreedRate: '',
    fetchingRolloverRates: false,
    submittingRollover: false,
    rolloverAmount: 0,
    rolloverFormStep: 'form',
    submittingRolloverConfirm: false
};

const getters = {
    submitRollover: state => state.submitRollover,
    rolloverForm: (state) => {
        state.rolloverForm = {
            amount_select: 'principal_interest',
            tenor: '12',
            agreed_rate: null,
            tenor_value: 6
        };
        return state.rolloverForm;
    },
    submitRolloverSuccess: state => state.submitRolloverSuccess,
    rolloverErrors: state => state.rolloverErrors,
    pendingRollover: (state, getters, rootState) => {
        state.pendingRollover = rootState.pendingRollover;
        return rootState.pendingRollover;
    },
    confirmRollover: state => state.confirmRollover,
    agreedRate: state => state.agreedRate,
    fetchingRolloverRates: state => state.fetchingRolloverRates,
    rolloverAmount: state => state.rolloverAmount,
    submittingRollover: state => state.submittingRollover,
    rolloverFormStep: state => state.rolloverFormStep,
    submittingRolloverConfirm: state => state.submittingRolloverConfirm
};

const mutations = {

    [types.SUBMIT_ROLLOVER](state, data) {

        state.submitRollover = true;

        Inv.SUBMIT_ROLLOVER(data)

            .then(function (response) {

                state.submitRollover = false;

                state.submitRolloverSuccess = true;

                state.rolloverErrors = {};

                Notification.success({title: "Rollover Successful", message: 'Rollover instruction sent successfully'});

                state.pendingRollover = null;
                state.rolloverFormStep = 'success';

                try {
                    $('#withdrawModal').foundation('close');
                    $('#rolloverModal').foundation('close');
                } catch (err) {
                }

                event.bus.$emit('refresh_investment');

            }, function (response) {

                state.submitRollover = false;

                state.submitRolloverSuccess = false;

                if (response.status == 422) {
                    state.rolloverErrors = response.data;

                    Notification.warning({title: 'Rollover', message: 'Some data may be missing on the instruction'});
                } else {
                    Notification.warning({title: 'Rollover', message: 'There was an error sending the instruction'});
                }
            });
    },
    [types.GET_ROLLOVER_RATES](state, form) {
        if (form.tenor) {
            state.fetchingRolloverRates = true;
            Inv.GET_ROLLOVER_RATES(form)
                .then(function ({data}) {
                    state.agreedRate = data + '%';
                    state.fetchingRolloverRates = false;
                }, function () {
                    Notification.warning({title: 'Error', message: 'Could not obtain interest rates'});
                    state.fetchingRolloverRates = false;
                });
        }
    },
    CONFIRM_INV_ROLLOVER(state, form) {
        state.submittingRolloverConfirm = true;
        state.rolloverForm = form;
        state.rolloverFormStep = 'confirm';
    },

    [types.SUBMIT_ROLLOVER_FORM](state, form) {
        state.submittingRollover = true;

        Inv.submitRolloverForm(form).then(
            () => {
                state.submittingRollover = false;
                state.confirmRollover = false;
                state.submitRolloverSuccess = true;
                Message.success({message: 'Rollover instruction is succesfully sent'});
                state.rolloverFormStep = 'success';
            },
            () => {
                state.submittingRollover = false;
                Message.warning({message: 'Rollover instruction has failed to send'});
            }
        );
    },
    [types.GET_ROLLOVER_AMOUNT](state, form) {
        if (!form.amount) {
            Inv.getAmountSelected(form).then(({data}) => {
                if (data != null) {
                    state.rolloverAmount = data.amount;
                }
            }, () => {
                Notification.warning({title: 'Error', message: 'Could not obtain the rollover amount'});
            });
        } else {
            state.rolloverAmount = form.amount;
        }
    },
};

const actions = {

    submitRollover({commit}, data) {

        commit('SUBMIT_ROLLOVER', data);
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}
