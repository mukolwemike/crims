
/**
 * Created by daniel on 18/01/2017.
 */
import * as types from '../../../mutation-types';
import { Notification, Message } from 'element-ui';
import * as Inv from '../../../../api/InvestmentsApi';
import router from '../../../../router';

const state = {
    searchInvestment: {},
    invTotals: {},
    investments: {},
    investment: {},
    fetchingInvestmentsDetails: false,
    totalBalance: '',
    clientBankAccounts: [],
    investmentSummary: [],
    fetchingSummary: false,
    fetchingClientInvestments: false,
    clientRecentActivities: [],
    fetchingRecentActivities: false,
    fetchingMaturities: false,
    maturities: [],
    passedInvestment: {},
    reSammary: {},
    reProjects: [],
    investmentHasError: false
};

const getters = {
    searchInvestment: state => state.searchInvestment,

    invTotals: state => state.invTotals,

    investments: state => state.investments,

    investment: state => state.investment,
    fetchingInvestmentsDetails: state => state.fetchingInvestmentsDetails,
    investmentHasError: state=>state.investmentHasError,

    totalBalance: state => state.totalBalance,
    clientBankAccounts: state => state.clientBankAccounts,
    fetchingClientInvestments: state=>state.fetchingClientInvestments,
    investmentSummary: state=>state.investmentSummary,
    fetchingSummary: state=>state.fetchingSummary,
    clientRecentActivities: state=>state.clientRecentActivities,
    fetchingRecentActivities: state=>state.fetchingRecentActivities,
    fetchingMaturities: state=>state.fetchingMaturities,
    maturities: state => state.maturities,
    passedInvestment: state => state.passedInvestment,
    reProjects: state=>state.reProjects,

    reTotals: state => {
        if (state.reSammary) {
            return state.reSammary.realEstateTotals
        }

        return {};
    },

    reDetails: state => {
        if (state.reSammary) {
            return state.reSammary.realEstateDetails
        }
    }
};

const mutations = {
    [types.GET_ACTIVE_INVESTMENTS] (state, data) {
        Inv.getActiveInvestments(data)
            .then(function (response) {
                state.investments = response.data.data;
                state.invTotals = response.data.meta;
            }, function () {
                state.invTotals = {};
                state.investments = {};
            })
    },
    [types.GET_INVESTMENT](state, data){
        state.fetchingInvestmentsDetails = true;

        if (data !== undefined) {
            Inv.GET_INVESTMENT(data)
                .then(function ({data}) {

                    if (data.status == 411) {
                        state.investmentHasError = true;
                        state.fetchingInvestmentsDetails = false;
                    } else if (data.status == 401) {
                        Message.warning({ message: 'An Error Occured. Could not get investment details' });
                        router.push({ name: 'main'});
                    } else {
                        state.investment = data.data;
                        state.investmentHasError = false;
                        state.fetchingInvestmentsDetails = false;
                    }
                }, function () {
                    state.investment = {};
                    state.investmentHasError = true;
                    state.fetchingInvestmentsDetails = false;
                });
        }
    },
    [types.GET_CLIENT_TOTAL_BALANCE](state, client)
    {
    },
    
    [types.CLIENT_BANK_ACCOUNTS](state, client)
    {
        let clientId = client.id;

        Inv.getClientBankAccounts(clientId)
        .then( ({ data }) => {
            state.clientBankAccounts = data.data;
        }, () => {});
    },

    GET_CLIENT_INVESTMENTS_FETCH(state, status){

        state.fetchingClientInvestments = status
    },

    GET_INVESTMENT_SAMMARY(state, client){
        let client_id = client.id;
        state.fetchingSummary = true;
        Inv.getInvestmentsSammary(client_id)
            .then(({data})=> {
                    state.investmentSummary = data.inv;
                    state.reSammary = data.re;
                    state.fetchingSummary = false;
                },
                ()=>{
                    state.investmentSummary = [];
                    state.fetchingSummary = false;
                })
    },


    // GET_UPCOMING_MATURITIES(state, client) {
    //     state.fetchingMaturities = true;
    //     Inv.getUpcomingMaturities(client.id)
    //         .then( ({ data })=>
    //         {
    //             state.maturities = data.data;
    //             state.fetchingMaturities = false;
    //         }, () => {
    //             state.maturities = [];
    //             state.fetchingMaturities = false;
    //         });
    // },
    GET_CLIENT_RECENT_ACTIVITIES(state, client) {
        let client_id = client.id;
        state.fetchingRecentActivities = true;
        Inv.getClientRecentActivities(client_id)
            .then(({data})=> {
                    state.clientRecentActivities = data.data;
                    state.fetchingRecentActivities = false;
                },
                ()=>{
                    state.clientRecentActivities = [];
                    state.fetchingRecentActivities = false;
                })

    },

    PASS_PRODUCT(state, investments){
        state.passedInvestment = investments;
    },

    // FETCH_REAL_ESTATE(state, client){
    //     let client_id = client.id;
    //     Inv.getRealEstate(client_id)
    //         .then((data)=>{
    //             state.reProjects = data;
    //         },
    //         ()=>{
    //         state.reProjects = []
    //         });
    // }


};

const actions = {
    searchInvestment({state}, data) {
        state.searchInvestment = data;
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}
