/**
 * Created by daniel on 23/01/2017.
 */
import router from '../../../../router';
import * as types from '../../../mutation-types';
import { Notification, Message } from 'element-ui';
import * as Inv from '../../../../api/InvestmentsApi';
import store from '../../../../store';

const state = {

    topupErrors: {},
    topupCount: null,
    submitTopup: false,
    submitTopupSuccess: false,
    submitSuccess: false,
    topupSuccessful: false,
    getLoader: false,
    topupForm: {},
    topup:{},
    fetch_topup: false,
    topupId: null,
    loader: false,
    clientProducts: [],
    allProducts: [],
    all_UnitFunds: [],
    actionCalloutActive: false,
    calloutAction: '',
    calloutActionProductId: '',
    calloutTitle: null,
    confirmTopup: false,
    suspendCallout: false,
    calloutName: '',
    fetchingClientProducts: false,
    fetchingAllProducts: false,
    fetchingAllUnitFunds: false,
    fetchCount: false,
    loadingRates: false



};

const getters = {

    topupErrors: state => state.topupErrors,

    topupCount: state => state.topupCount,
    fetchCount: state=> state.fetchCount,

    topupSuccessful: state => state.topupSuccessful,

    getLoader: (state) => { return state.loader; },

    topupForm: (state) => {
        state.topupForm = {
            amount: null,
            product: null,
            agreed_rate: null,
            period: 12,
            entered_tenor: 12,
            products: null,
            allProducts: null,
            all_UnitFunds: null,
            mode_of_payment: null,
            topups_data: [],
            interest_rate: null

        };

        return state.topupForm;
    },
    submitTopup: state => state.submitTopup,

    submitTopupSuccess: state => state.submitTopupSuccess,
    submitSuccess:state=> state.submitSuccess,

    topup: state => state.topup,
    fetch_topup:state => state.fetch_topup,
    topupId: state => state.topup.id,
    loadingRates: state => state.loadingRates,
    clientProducts: state => state.clientProducts,
    allProducts: state => state.allProducts,
    all_UnitFunds: state => state.all_UnitFunds,
    fetchingClientProducts: state => state.fetchingClientProducts,
    fetchingAllProducts: state => state.fetchingAllProducts,
    fetchingAllUnitFunds: state => state.fetchingAllUnitFunds,
    actionCalloutActive: state=>state.actionCalloutActive,
    calloutAction: state=>state.calloutAction,
    calloutActionProductId: state=>state.calloutActionProductId,
    calloutTitle: state=>state.calloutTitle,
    confirmTopup: state=>state.confirmTopup,
    suspendCallout: state=> state.suspendCallout,
    calloutName: state=> state.calloutName,
    topups_data: state=>state.topups_data

};

const mutations = {
    [types.GET_TOPUPS] (state, clientId) {
         state.fetchCount = true;
        Inv.GET_TOPUPS(clientId)
            .then(function (response) {
                state.topupCount = response.data.meta.count;
                state.topups_data = response;
                state.fetchCount = false;
            }, function (response) {
                state.fetchCount = false;
                Notification.warning({message:  'Could not obtain TopUp information. Some data may be missing on the instruction', title: 'warning'});
            });
    },
    [types.GET_TOPUP] (state, topupId) {
        state.fetch_topup = true;
        Inv.GET_TOPUP(topupId)
            .then(function ({ data }) {
                state.fetch_topup = false;
                state.topup = data.data;
            }, function (response) {
                Notification.warning({ message: 'An Error Occured. Could not Obtain TopUp details' });
                state.fetch_topup = false;
            });
    },

    [types.GET_TOPUP_RATES] (state, form) {
        state.loadingRates = true;
        Inv.GET_TOPUP_RATES(form)
            .then(function (response) {
                state.loadingRates = false;

                state.topupForm.agreed_rate = response.data;

            }, function () {
                Message.warning({ message: 'Sorry, An Error Occurred. Could not obtain interest rates' });
                state.loadingRates = false;

            });
    },
    [types.SUBMIT_TOPUP] (state, data) {

        state.submitTopup = true;

        Inv.SUBMIT_TOPUP(data)
            .then(function (response) {


                state.confirmTopup = false;
                state.submitTopup = false;

                state.submitSuccess = true;

                state.topupErrors = {};

                Notification.success({title: '', message:  'Investment Form sent successfully'});

                state.topupSuccessful = true;

                state.loader = false;
                state.calloutTitle =  'Top Up was Successful';

            }, function (response) {

                state.loader = false;

                state.submitTopup = false;

                state.submitSuccess = false;

                state.topupSuccessful = false;

                if (response.status === 422) {
                    state.topupErrors = response.data;

                    Notification.warning({message: 'An Error Occurred while submitting TopUp. Some data may be missing on the instruction'});
                } else {
                    Notification.warning({message: 'An Error Occurred while submitting TopUp. There was an error sending the topup'});
                }
            });
    },

    GET_ALL_PRODUCTS(state) {
        state.fetchingAllProducts =true;
        Inv.getProducts()
            .then(
                ({ data })=>
                {
                    state.allProducts = data.data;
                    state.fetchingAllProducts =false;
                },
                ()=>
                {
                    state.fetchingAllProducts =false;
                    Message.warning({ message: 'There was an error while fetching the client products information' });
                }
            );
    },

    GET_CLIENT_PRODUCTS(state, clientId) {
        state.fetchingClientProducts =true;
        Inv.myProducts(clientId)
            .then(
                ({ data })=>
                {
                    state.clientProducts = data.data;
                    state.fetchingClientProducts =false;
                },
                ()=>
                {
                    state.fetchingClientProducts =false;
                    Message.warning({ message: 'There was an error while fetching the client products information' });
                }
            );
    },

    GET_ALL_UNIT_FUNDS(state) {
        state.fetchingAllUnitFunds =true;
        Inv.getAllUnitFunds()
            .then(
                ({ data })=>
                {
                    state.all_UnitFunds = data.data;
                    state.fetchingAllUnitFunds =false;
                },
                ()=>
                {
                    state.fetchingAllUnitFunds =false;
                    Message.warning({ message: 'There was an error while fetching all unit funds Information.' });
                },
            );
    },

    OPEN_MODAL(state, product){
            state.actionCalloutActive = true;
            state.calloutAction = product.method;
            state.calloutTitle = product.method;
            state.calloutName = product.name;
            state.calloutActionProductId = product.id;
    },


    CLOSE_CALLOUT(state){
            state.actionCalloutActive = false;
            state.calloutAction = '';
            state.calloutActionProductId = '';
            state.confirmTopup = false;
            state.topupForm = {};
            state.topupSuccessful = false;
    },
    SUSPEND_CALLOUT(state, status){
        state.suspendCallout = status;
    },
    CONFIRM_TOPUP(state, name){
        state.calloutTitle = name;
        state.confirmTopup = true;
    },
    CALLOUT_TITLE(state, name){
        state.calloutTitle = name;
    },

    modalState(state, active) {
        state.loader = active;
    },
};

const actions = {
    getTopups({commit, getters}){
        var clientId;
        try {
            clientId = getters.selectedClient.id;
        } catch (err) {
            return [];
        }

        commit('GET_TOPUPS', clientId)

    },

    topupInvestment({commit, getters}, form){
        var clientId;

        try {
            clientId = getters.selectedClient.id;
        } catch (err) {
            return [];
        }

        form.client_id = clientId;

        commit(types.SUBMIT_TOPUP, form);
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}
