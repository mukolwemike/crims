/**
 * Created by Raphael Karanja on 06/09/2018.
 */
import * as fund_api from '../../../../api/unittrustApi';
const state = {
    fundDetails: {},
    fetchingFundDetails: false,
    fundsummary: [],
    fetchingFundSummary: false,
    fundBalance: '',
    fetchingFundBalance: false,
    fundGivenTransfers: [],
    fetchingGivenTransfers: false,

    fundRecievedTransfers: [],
    fetchingRecievedTransfers: false,

    fundInstructions: [],
    fetchingFundInstructions: false,

    clientFunds: [],
    fetchingClientFunds: false,

};
const getters = {
    fundsummary: state => state.fundsummary,
    fetchingFundSummary: state => state.fetchingFundSummary,
    fundDetails: state => state.fundDetails,
    fetchingFundDetails: state=> state.fetchingFundDetails,
    fundBalance: state => state.fundBalance,
    fetchingFundBalance: state => state.fetchingFundBalance,
    fundGivenTransfers: state => state.fundGivenTransfers,
    fetchingGivenTransfers: state => state.fetchingGivenTransfers,
    fundRecievedTransfers: state => state.fundRecievedTransfers,
    fetchingRecievedTransfers: state => state.fetchingRecievedTransfers,
    fundInstructions: state => state.fundInstructions,
    fetchingFundInstructions: state => state.fetchingFundInstructions,
    clientFunds: state => state.clientFunds,
    fetchingClientFunds: state => state.fetchingClientFunds
};
const mutations = {
    FETCH_FUND_SUMMARY(state, client_id){
        state.fetchingFundSummary = true;
        fund_api.getFundSummary(client_id)
            .then(({data})=> {
                state.fundsummary = data.data.summaries;

                state.fetchingFundSummary = false;
            }, ()=> {
                state.fetchingFundSummary = false;
            })
    },

    FETCH_CLIENT_FUNDS(state, client_id){
        state.fetchingClientFunds = true;
        fund_api.getClientFunds(client_id)
            .then(({data})=> {
                state.clientFunds = data.data.funds;
                state.fetchingClientFunds = false;
            }, ()=> {
                state.fetchingClientFunds = false;
            })
    },

    FETCH_FUND_BALANCE(state, details){
        state.fetchingFundBalance = true;
        fund_api.getFundBalance(details)
            .then(({data})=> {
                state.fundBalance = data;
                state.fetchingFundBalance = false;
            }, ()=> {
                state.fetchingFundBalance = false;
            })
    },

    FETCH_FUND_GIVEN_TRANSFERS(state, details){
        state.fetchingGivenTransfers = true;
        fund_api.getFundGivenTransfers(details)
            .then(({data})=> {
                state.fundGivenTransfers = data;
                state.fetchingGivenTransfers = false;
            }, ()=> {
                state.fetchingGivenTransfers = false;
            })
    },

    FETCH_FUND_RECIEVED_TRANSFERS(state, details){
        state.fetchingRecievedTransfers = true;
        fund_api.getFundRecievedTransfers(details)
            .then(({data})=> {
                state.fundRecievedTransfers = data;
                state.fetchingRecievedTransfers = false;
            }, ()=> {
                state.fetchingRecievedTransfers = false;
            })
    },

    FETCH_FUND_INSTRUCTIONS(state, details){
        state.fetchingFundInstructions = true;
        fund_api.getFundInstructions(details)
            .then(({data})=> {
                state.fundInstructions = data.data;
                state.fetchingFundInstructions = false;
            }, ()=> {
                state.fetchingFundInstructions = false;
            })
    },
};

export default {
    state,
    getters,
    mutations
}