/**
 * Created by Raphael Karanja on 20/09/2018.
 */
import * as fund_api from '../../../../api/unittrustApi';
import {Notification} from 'element-ui';
import FileSaver from 'file-saver';
import Vue from 'vue';

const state = {
    loadingFundStatement: false,
    fundStatement: [],
    donwloadingUTFStatement: false,
    donwloadingUTFStatementExcel: false,
};
const getters = {
    loadingFundStatement: state => state.loadingFundStatement,
    fundStatement: state => state.fundStatement,
    donwloadingUTFStatement: state => state.donwloadingUTFStatement,
    donwloadingUTFStatementExcel: state => state.donwloadingUTFStatementExcel,

};
const mutations = {
    GET_FUND_STATEMENT(state, details) {
        state.loadingFundStatement = true;

        Vue.component(
            'getFundStatementEmbed',
            function (resolve, reject) {
                fund_api.getFundStatement(details)
                    .then(
                        function ({data}) {
                            state.loadingFundStatement = false;
                            resolve(
                                {
                                    template: data
                                }
                            );
                        },

                        function () {
                            state.loadingFundStatement = false;

                            Notification.warning({message: 'You have no access to the client\'s statements'});

                            reject(
                                {
                                    template: '<div> </div>'
                                }
                            )
                        }
                    );
            }
        );
    },

    DOWNLOAD_FUND_STATEMENT(state, details) {

        state.donwloadingUTFStatement = true;
        Vue.http.get(
            '/api/unitization/' + details.fund_id + '/unit-fund-holders/' + details.client_id + '/statement-download/' + details.start + '/' + details.date,
            {responseType: 'arraybuffer'}
        ).then(
            function (response) {

                state.donwloadingUTFStatement = false;

                let blob = new Blob([response.data], {type: response.headers.get('content-type')});

                FileSaver.saveAs(blob, "Cytonn Investments " + details.fundname.shortname + "  Statement for date (" + details.date + ").pdf");
            }
        );

    },

    DOWNLOAD_FUND_STATEMENT_EXCEL(state, details) {

        state.donwloadingUTFStatementExcel = true;

        Vue.http.get(
            '/api/unitization/' + details.fund_id + '/unit-fund-holders/' + details.client_id + '/excel-statement-download/' + details.start + '/' + details.date,
            {responseType: 'arraybuffer'}
        ).then(
            function (response) {

                state.donwloadingUTFStatementExcel = false;

                let blob = new Blob([response.data], {type: response.headers.get('content-type')});

                FileSaver.saveAs(blob, "Cytonn Investments " + details.fundname.shortname + "  Statement for date (" + details.date + ").xlsx");
            }
        );

    }
};

export default {
    state,
    getters,
    mutations
}