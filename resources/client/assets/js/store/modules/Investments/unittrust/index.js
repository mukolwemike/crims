/**
 * Created by Raphael Karanja on 06/09/2018.
 */
import Sales from './sales';
import Purchases from './purchases';
import Transfers from './transfers';
import Details from './details';
import Statement from './statement';

const state = {};
const getters = {};
const mutations = {};

export default {
    state,
    getters,
    mutations,
    modules: {
        Sales,
        Purchases,
        Transfers,
        Details,
        Statement
    }
}