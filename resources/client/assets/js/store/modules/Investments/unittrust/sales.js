/**
 * Created by Raphael Karanja on 06/09/2018.
 */
import * as unit_api from '../../../../api/unittrustApi';
import { Notification, Message } from 'element-ui';

const state = {
    fundSales: []
};
const getters = {
    fundSales: state => state.fundSales
};
const mutations = {
    FETCH_FUND_SALES(state, details){
        unit_api.getFundSales(details)
            .then((data)=>{
                state.fundSales = data.data
            }, ()=>{
                this.fundSales = [];
                Message.warning({
                    message: 'Could not load Fund information!! Try again later or try another fund. ',
                    duration: '6000',
                    showClose: true
                });
            })
    }
};

export default {
    state,
    getters,
    mutations
}