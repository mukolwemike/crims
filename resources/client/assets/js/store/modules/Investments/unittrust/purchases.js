/**
 * Created by Raphael Karanja on 06/09/2018.
 */
import * as unit_api from '../../../../api/unittrustApi';
import {Message} from 'element-ui';

const state = {
    unitsToPurchase: '',
    fetchUnitsToPurchase: false,
    fetchingFundPurchases: false,
    unitPurchases: [],

    fetchAmountNeeded: false,
    amountNeeded: [],

    calculatedUnits: [],
    fetchCalculatedUnits: false,

    fundBankAccount:{},
    fundMpesaAccount:{},
    fetchingAccounts: false,
};
const getters = {
    unitsToPurchase: state => state.unitsToPurchase,
    fetchUnitsToPurchase: state => state.fetchUnitsToPurchase,
    unitPurchases: state => state.unitPurchases,
    fetchingFundPurchases: state => state.fetchingFundPurchases,

    amountNeeded: state => state.amountNeeded,
    fetchAmountNeeded: state => state.fetchAmountNeeded,

    calculatedUnits: state => state.calculatedUnits,
    fetchCalculatedUnits: state => state.fetchCalculatedUnits,

    fundBankAccount: state => state.fundBankAccount,
    fundMpesaAccount: state => state.fundMpesaAccount,
    fetchingAccounts: state => state.fetchingAccounts,
};
const mutations = {
    FETCH_FUND_PURCHASES(state, details) {
        state.fetchingFundPurchases = true;
        unit_api.getFundPurchases(details)
            .then(({data}) => {
                state.fetchingFundPurchases = false;
                state.unitPurchases = data;
            }, () => {
                state.fetchingFundPurchases = false;
            })
    },
    UNIT_PURCHASE_CALCULATOR(state, details) {
        state.fetchUnitsToPurchase = true;
        unit_api.calculateFund(details)
            .then(({data}) => {
                state.fetchUnitsToPurchase = false;
                state.unitsToPurchase = data.data;
            }, () => {
                state.fetchUnitsToPurchase = false;
                state.unitsToPurchase = {};
                Message.warning({
                    message: 'Could not load Fund information!! Try again later or try another fund. ',
                    duration: '6000',
                    showClose: true
                });
            })
    },
    UNIT_PURCHASE_CALCULATOR_AMOUNT(state, details) {
        state.fetchAmountNeeded = true;
        unit_api.calculateFund(details)
            .then(({data}) => {
                state.fetchAmountNeeded = false;
                state.amountNeeded = data.data;
            }, () => {
                state.fetchAmountNeeded = false;
                state.unitsToPurchase = {};
                Message.warning({
                    message: 'Could not load Fund information!! Try again later or try another fund. ',
                    duration: '6000',
                    showClose: true
                });
            })
    },
    UNIT_PURCHASE_CALCULATOR_UNITS(state, details) {
        state.fetchCalculatedUnits = true;
        unit_api.calculateFund(details)
            .then(({data}) => {
                state.fetchCalculatedUnits = false;
                state.calculatedUnits = data.data;
            }, () => {
                state.fetchCalculatedUnits = false;
                state.calculatedUnits = {};
                Message.warning({
                    message: 'Could not load Fund information!! Try again later or try another fund. ',
                    duration: '6000',
                    showClose: true
                });
            })
    },
    FETCH_BANK_ACCOUNT_DETAILS(state, fundId) {
        state.fetchingAccounts = true;
        unit_api.fetchAccounts(fundId).then(({data}) => {
            state.fetchingAccounts = false;
            state.fundBankAccount = data.data.bank;
            state.fundMpesaAccount = data.data.mpesa;

        }, ()=>{
            state.fetchingAccounts = false;
        });
    }

};

export default {
    state,
    getters,
    mutations
}