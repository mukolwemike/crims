/**
 * Created by Raphael Karanja on 06/09/2018.
 */
import * as unit_api from '../../../../api/unittrustApi';

const state = {
    fundTransfers: []
};
const getters = {
    fundTransfers: state => state.fundTransfers
};
const mutations = {
    FETCH_FUND_TRANSFERS(state, details){
        unit_api.getFundTransfers(details)
            .then(({data})=>{

            }, ()=>{

            })
    }
};

export default {
    state,
    getters,
    mutations
}