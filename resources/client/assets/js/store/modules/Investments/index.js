/**
 * Created by Raphael Karanja on 06/09/2018.
 */
import StructuredProducts from './structuredproducts/index';
import UnitTrust from './unittrust/index';
import RealEstate from './realestate/index';

const state = {};
const getters = {};
const mutations = {};

export default {
    state,
    getters,
    mutations,
    modules: {
        StructuredProducts,
        UnitTrust,
        RealEstate
    }
}