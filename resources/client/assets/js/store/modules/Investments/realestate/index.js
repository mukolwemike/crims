/**
 * Created by Mukolwe Michael on 13/01/2019.
 */
import Details from './details';

const state = {};
const getters = {};
const mutations = {};

export default {
    state,
    getters,
    mutations,
    modules: {
        Details
    }
}