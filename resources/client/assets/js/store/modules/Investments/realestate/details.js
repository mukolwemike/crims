/**
 * Created by Mukolwe Michael on 13/01/2019.
 */

import * as realEstate_api from '../../../../api/realestateApi';
import {Notification} from 'element-ui';
import Vue from 'vue';
import FileSaver from "file-saver";
import events from "../../../../events";

const state = {
    step: 'form',
    realEstateSummary: [],
    realEstateDetails: [],
    paymentDetails: [],
    realEstateTotals: {},
    fetchingRESummary: false,
    unitDetails: {},
    paymentSchedules: [],
    payments: {},
    paymentTypes: {},
    makePaymentErrors: {},
    validatingPaymentForm: false,
    submitPaymentSuccess: false,
    submittingPayment: false,
    loadingReStatements: false,
    myProjects: {},
    reStatements: {},
    reStatementsTotals: {},
    reStatementsSchedules: {},
    downloadREStatements: false,
    fetchingProjects: false,
    downloadingReStatement: false,
    downloadingExcelReStatement: false,
    fetchingREPayment: false,
    fetchingREPayments: false,
};

const getters = {
    realEstateSummary: state => state.realEstateSummary,
    realEstateTotals: state => state.realEstateTotals,
    realEstateDetails: state => state.realEstateDetails,
    fetchingRESummary: state => state.fetchingRESummary,
    unitDetails: state => state.unitDetails,
    paymentSchedules: state => state.paymentSchedules,
    paymentTypes: state => state.paymentTypes,

    makePaymentErrors: state => state.makePaymentErrors,
    validatingPaymentForm: state => state.validatingPaymentForm,
    submitPaymentSuccess: state => state.submitPaymentSuccess,
    submittingPayment: state => state.submittingPayment,
    step: state => state.step,

    loadingReStatements: state => state.loadingReStatements,
    fetchingProjects: state => state.fetchingProjects,
    myProjects: state => state.myProjects,
    downloadREStatements: state => state.downloadREStatements,

    reStatementsTotals: state => state.reStatementsTotals,
    reStatementsSchedules: state => state.reStatementsSchedules,
    reStatements: state => state.reStatements,
    downloadingReStatement: state => state.downloadingReStatement,
    downloadingExcelReStatement: state => state.downloadingExcelReStatement,
    payments: state => state.payments,
    paymentDetails: state => state.paymentDetails,
    fetchingREPayment: state => state.fetchingREPayment,
    fetchingREPayments: state => state.fetchingREPayments,

    downloadREStatement: (state, getters) => (form) => {

        state.downloadingReStatement = true;

        Vue.http.get(
            '/api/client/realestate/' + form.project + '/' + form.clientId + '/' + form.date_from + '/' + form.date_to + '/download-statement',
            {responseType: 'arraybuffer'}
        ).then(
            function (response) {

                state.downloadingReStatement = false;

                let blob = new Blob([response.data], {type: response.headers.get('content-type')});

                FileSaver.saveAs(blob, "Cytonn Real Estate " + form.product_name + "  Statement from (" + form.date_from + ") to  (" + form.statement_date + ").pdf");
            }, () => {
                state.downloadingReStatement = false;
            }
        );
    },

    downloadExcelREStatement: (state, getters) => form => {

        state.downloadingExcelReStatement = true;

        Vue.http.get(
            '/api/client/realestate/' + form.project + '/' + form.clientId + '/' + form.date_from + '/' + form.date_to + '/download-statement-excel',
            {responseType: 'arraybuffer'}
        ).then(
            function (response) {

                state.downloadingExcelReStatement = false;

                let blob = new Blob([response.data], {type: response.headers.get('content-type')});

                FileSaver.saveAs(blob, "Cytonn Real Estate " + form.product_name + "  Statement from (" + form.date_from + ") to  (" + form.statement_date + ").xlsx");
            }, () => {
                state.downloadingExcelReStatement = false;
            }
        );
    },
};

const mutations = {
    GET_REAL_ESTATE_SUMMARY(state, client_id) {
        state.fetchingRESummary = true;

        realEstate_api.reSummary(client_id).then(({data}) => {

            state.fetchingRESummary = false;

            state.realEstateSummary = data.summaries;
            state.realEstateDetails = data.details;
            state.realEstateTotals = data.totals;

        }, () => {
            state.realEstateSummary = [];
            state.fetchingRESummary = false;
        });
    },

    GET_UNIT_DETAILS(state, holding) {
        realEstate_api.unitDetails(holding)
            .then(({data}) => {
                state.unitDetails = data.data;
            });
    },

    GET_UNIT_PAYMENT_SCHEDULES(state, holding) {
        realEstate_api.paymentSchedules(holding)
            .then(({data}) => {
                state.paymentSchedules = data.data;
            });
    },

    GET_UNIT_PAYMENTS(state, data) {
        state.fetchingREPayments = true;

        realEstate_api.fetchPayments(data)
            .then(({data}) => {
                state.fetchingREPayments = false;

                state.payments = data.payments;

            }, () => {
                state.fetchingREPayments = false;
            });
    },

    GET_UNIT_PAYMENT_DETAILS(state, payment) {
        state.fetchingREPayment = true;

        realEstate_api.fetchPayment(payment)
            .then(({data}) => {
                state.fetchingREPayment = false;

                state.paymentDetails = data.payment;
            }, () => {
                state.fetchingREPayment = false;
            });
    },

    SUBMIT_PAYMENT_DETAILS(state, form) {
        state.submittingPayment = true;

        realEstate_api.submitPaymentDetails(form)
            .then((response) => {

                state.submittingPayment = false;
                state.submitPaymentSuccess = true;
                state.makePaymentErrors = {};

                Notification.success({title: '', message: 'Investment Payment sent successfully'});
                events.bus.$emit('close_drawer');
            }, (response) => {
                state.submittingPayment = false;
                state.submitPaymentSuccess = false;

                Notification.error({title: '', message: 'Investment Payment not sent successfully'});
            });
    },

    GET_RE_STATEMENTS(state, form) {

        state.loadingReStatements = true;

        realEstate_api.fetchingReStatements(form).then((data) => {
            state.loadingReStatements = false;
        });


        Vue.component(
            'getRealEstateStatementEmbed',
            function (resolve, reject) {

                state.loadingReStatements = true;

                realEstate_api.fetchingReStatements(form)
                    .then(
                        function ({data}) {
                            state.loadingReStatements = false;
                            resolve(
                                {
                                    template: data
                                }
                            );
                        },

                        function () {
                            state.loadingReStatements = false;

                            Notification.warning({message: 'You have no access to the client\'s statements'});

                            reject(
                                {
                                    template: '<div> </div>'
                                }
                            )
                        }
                    );
            }
        );
    },


    GET_CLIENTS_PROJECTS(state, client_id) {
        state.fetchingProjects = true;

        realEstate_api.getClientsProjects(client_id)
            .then((response) => {
                state.fetchingProjects = false;

                state.myProjects = response.data.projects;
            }, () => {
                state.fetchingProjects = false;
            });
    }
};

export default {
    state,
    getters,
    mutations,
}