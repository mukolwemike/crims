import * as termsApi from '../../../api/termsApi';

const states = {
    general_terms: {},
};

const mutations = {

    GENERAL_TERMS: function (state, form) {

        state.fetchingTerms = true;

        let item = null;

        if (form.product_category === 'funds') {
            item = form.unit_fund_id;
        } else if (form.product_category === 'products') {
            item = form.product_id;
        }

        termsApi.getGeneralTerms(form.product_category, item)
            .then(({data}) => {
                state.general_terms = data;
                state.fetchingTerms = false;
            }, () => {
                state.fetchingTerms = false;
            })
    }
};

const getters = {
    general_terms: state => state.general_terms,
};

export default {
    states, mutations, getters
}