/**
 * Created by daniel on 31/01/2017.
 */
import {Message} from 'element-ui';
import * as types from '../../mutation-types';
import * as Auth from '../../../api/AuthApi';
import router from '../../../router';
import store from "../../index";

const state = {
    verifyingUser: false,
    submitForgot: false,
    foundUser: false,
    pswdResetSuccess: {
        success: false,
        email_sent: false,
        needs_confirmation: false,
        message: ''
    },
    reset_creds: {},
    reset_user: {},
    requestingSms: false,
    requestMade: false,
    sms_sent: false
};

const getters = {

    submitForgot: state => state.submitForgot,

    verifyingUser: state => state.verifyingUser,

    pswdResetSuccess: state => state.pswdResetSuccess,

    foundUser: state => state.foundUser,

    reset_creds: state => {
        return {
            username: '',
            password: '',
            option: 'sms'
        }
    },

    reset_user: state => state.reset_user,
    requestingSms: state => state.requestingSms,
    requestMade: state => state.requestMade,
    sms_sent: state => state.sms_sent,
};

const mutations = {
    [types.FORGOT_PASSWORD](state, creds) {

        state.verifyingUser = true;

        Auth.VERIFY_USERNAME(creds)

            .then(function ({data}) {

                state.verifyingUser = false;
                let cleanedUsername = creds.username;
                cleanedUsername = cleanedUsername.replace(/[^A-Z0-9]/ig, "");

                if (data.status === 200 && data.has_no_clients) {
                    Message.success({message: data.message});

                    router.push({name: 'login'});

                    return;
                }

                if (data.status === 200) {

                    state.reset_user = data.user;

                    state.foundUser = true;
                    Message.success('Mail found, proceed');

                    window.Echo.channel('authy-one-touch-password-reset-' + cleanedUsername)
                        .listen('AuthyConfirmPasswordReset', (e) => {

                            Message.success('Password Reset approved via One Touch');

                            creds.one_touch_approval_uuid = e.user.authy_uuid;
                            creds.from_authy_one_touch = true;

                            authenticatePasswordReset(creds);
                        });


                } else {
                    Message.warning({message: 'Username not correct'});
                }
            });
    },

    [types.REQUEST_SMS_PASSWORD_RESET](state, creds) {
        state.requestingSms = true;

        Auth.REQUEST_SMS_PASSWORD_RESET(creds)
            .then(() => {
                Message.success(`Verification token sent to you via ${creds.option}`);
                state.requestingSms = false;
                state.requestMade = true;
                state.sms_sent = true;
            }, function (response) {

                state.requestingSms = false;

                let str = response.data;

                if (response.status === 400) {
                    Message.warning('Error' + str);
                } else {
                    Message.error('Error Could not send login token');
                }
            });
    },

    [types.AUTHENTICATE_PASSWORD_RESET](state, creds) {
        state.submitForgot = true;

        Auth.AUTHENTICATE_PASSWORD_RESET(creds)
            .then(({data}) => {
                state.submitForgot = false;

                if (data.status === 200) {
                    localStorage.setItem('id_token', data.token);
                    Message.success(data.message);
                    router.push({name: 'login'});
                } else {
                    Message.warning(data.message);
                }
            });
    },

    CLOSE_PASS_RESET_TOKEN_MODAL(state, value) {
        state.sms_sent = value;
    }
};


function authenticatePasswordReset(creds) {
    store.commit('AUTHENTICATE_PASSWORD_RESET', creds);
}


export default {
    state,
    getters,
    mutations
}

