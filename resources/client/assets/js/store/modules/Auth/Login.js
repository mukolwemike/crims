/**
 * Created by daniel on 10/01/2017.
 */

import router from '../../../router';
import * as Auth from '../../../api/AuthApi';
import * as types from '../../mutation-types';
import {Message, Notification} from 'element-ui';
import {isEmptyObject} from '../../../utils'
import store from '../../index'


try {
    let user = JSON.parse(sessionStorage.getItem('current_user'))
} catch (err) {
    let user = null;
}

const state = {
    loggingIn: true,
    submitLogin: false,
    currentUser: null,
    token: localStorage.getItem('id_token'),
    isAuthenticated: false,
    selectedClient: {},
    isFa: false,
    isClient: false,
    isMultipleClient: false,
    defaultHeaders: {},
    userStatus: {},
    credentials: {},
    userDetails: {},
    verification_sent: false,
    creds: {},
    requestingSMS: false,
    ver_option: null
};

const getters = {
    loggingIn: state => state.loggingIn,

    credentials: state => state.credentials,

    defaultHeaders: state => state.defaultHeaders,

    submitLogin: state => state.submitLogin,

    currentUser: (state, getters, rootState) => {

        rootState.currentUser = state.currentUser;

        return state.currentUser;
    },
    isAuthenticated: (state, getters, rootState) => {

        if ((state.currentUser === null) || (state.currentUser === undefined) || isEmptyObject(state.currentUser)) {
            return rootState.isAuthenticated = state.isAuthenticated = false;
        }

        return rootState.isAuthenticated = state.isAuthenticated = true;
    },
    currentFa: (state, getters, rootState) => {
        return state.isAuthenticated ? rootState.currentUser.fas[0] : {};
    },
    token: state => state.token,

    selectedClient: (state, getters, rootState) => {

        if (!state.isAuthenticated) {
            return {};
        }

        state.selectedClient = rootState.selectedClient;

        if (Object.keys(state.selectedClient).length >= 1) {
            return state.selectedClient;
        }

        if (state.defaultClient) {
            return state.defaultClient;
        } else if (state.currentUser) {
            if (state.currentUser.access.length >= 1) {
                return state.selectedClient = state.currentUser.access[0];
            }

            return state.selectedClient;
        } else {
            return state.selectedClient;
        }
    },
    isClient: (state) => {
        try {
            if (state.currentUser.access.length > 0) {
                return true;
            }
        } catch (err) {
        }

        return state.isClient;
    },
    isMultipleClient: (state) => {
        try {
            if (state.currentUser.access.length > 1) {
                return true;
            }
        } catch (err) {
        }


        return state.isMultipleClient;
    },
    isFa: (state) => {
        try {
            if (state.currentUser.fas.length > 0) {
                return true;
            }
        } catch (err) {
        }

        return state.isFa;
    },

    userStatus: state => state.userStatus,
    userDetails: state => state.userDetails,
    verification_sent: state => state.verification_sent,
    requestingSMS: state => state.requestingSMS,
    creds: state => {
        return {
            username: '',
            password: '',
            option: 'sms'
        }
    },

    ver_option: state => state.ver_option
};

const mutations = {

    SET_USER(state, user) {
        state.userStatus = user;
    },

    fetchSelectedClient(state) {

        Auth.LOGIN_USER(state, router)
            .then(() => {
                let access = state.currentUser.access[0];
                state.selectedClient = access === undefined ? {} : access;
            })
            .catch(() => {
                state.currentUser = {};
                state.selectedClient = {};
            });

        return state.selectedClient;
    },
    [types.VERIFY_USER](state, creds) {

        state.submitLogin = true;

        state.credentials = creds;

        Auth.VERIFY_USER(creds)
            .then(({data}) => {

                state.userDetails = data.user;

                state.submitLogin = false;

                if (data.status === 401 && data.error === 'invalid_credentials') {
                    Message.warning('Login failed! Username/password do not match');
                } else if (data.status === 403 && data.message === 'user_inactive') {
                    Message.warning('Account inactive, This account is inactive');
                } else if (data.status === 403 && data.message === 'too_many_login_attempts') {
                    Message.warning( 'Error! Too many incorrect login attempts, try again in 30 minutes');
                } else {
                    // store.commit(types.AUTHENTICATE_USER, creds);

                    Auth.LOGIN_USER(state, router);

                    Message.success('Login successful Welcome back!');
                }
            })
            .catch(() => {
                state.submitLogin = false;
                Message.error('Error An error occurred when logging in');
            });
    },

    [types.AUTHENTICATE_USER](state, creds) {
        state.submitLogin = true;

        Auth.AUTHENTICATE_USER(creds)
            .then(({data}) => {
                state.submitLogin = false;
                state.loggingIn = false;

                if (data.status === 400) {
                    Message.warning('Token incorrect Could not verify login token');
                } else if (data.status === 401) {
                    Message.error('Token expired, resend token');
                    state.verification_sent = false;
                }
                else {
                    state.verification_sent = false;

                    localStorage.setItem('id_token', data.token);

                    if (data.last_two_factor) {
                        localStorage.setItem('_key', data.last_two_factor);
                    }

                    Auth.LOGIN_USER(state, router);

                    Message.success('Login successful Welcome back!');
                }
            })
            .catch(response => {
                state.submitLogin = false;

                state.verification_sent = false;

                if (response.status === 401) {
                    Message.warning('Login failed! Username/password do not match');
                } else {
                    Message.error( 'Error An error occurred when logging in');
                }
            });
    },

    SET_VERIFICATION_OPTION(state, option) {
        state.ver_option = option;
    },

    [types.REQUEST_SMS](state, creds) {
        state.requestingSMS = true;
        Auth.REQUEST_SMS(creds)
            .then(() => {
                state.requestingSMS = false;

                Message.success(`Your login token has been sent through ${creds.option}`);
                state.verification_sent = true;

            }, (response) => {

                let str = response.data;
                state.requestingSMS = false;

                if (response.status === 400) {
                    Notification.warning({message: 'Error', str});
                } else {
                    Notification.error('Error Could not send login token');
                }
            });
    },

    [types.LOGOUT_USER](state) {
        Auth.LOGOUT(state);
    },

    [types.SET_DEFAULT_HEADERS](state, headers) {
        state.defaultHeaders = headers;
    },
    [types.SET_USER_LOGGED_OUT](state) {
        state.isAuthenticated = false;
    },
    SWITCH_FA(state, data) {
        state.currentFa = data
    },

    CLOSE_TOKEN_MODAL(state) {
        state.verification_sent = false;
    }
};

const actions = {
    verifyUser({commit, rootState, state}, creds) {
        state.currentUser = rootState.currentUser;
        state.isAuthenticated = rootState.isAuthenticated;
        let cleanedUsername = creds.username;
        cleanedUsername = cleanedUsername.replace(/[^A-Z0-9]/ig, "");

        window.Echo.channel('authy-one-touch-for-' + cleanedUsername)
            .listen('AuthyStatusUpdated', (e) => {

                Message.success('Login approved via One Touch');

                let credentials = store.state.Auth.Login.credentials;

                let creds = {
                    'username': credentials.username,
                    'password': credentials.password,
                    'key': credentials.key,
                    'one_touch_approval_uuid': e.user.authy_uuid,
                    'from_authy_one_touch': true
                };

                store.dispatch('authenticateUser', creds);
            });

        commit('VERIFY_USER', creds);
    },
    authenticateUser({commit, rootState, state}, creds) {
        state.currentUser = rootState.currentUser;
        state.isAuthenticated = rootState.isAuthenticated;

        commit('AUTHENTICATE_USER', creds);
    },
    requestSMS({commit, rootState, state}, creds) {
        state.currentUser = rootState.currentUser;
        state.isAuthenticated = rootState.isAuthenticated;

        commit('REQUEST_SMS', creds);
    },
    logoutUser({commit, rootState, state}) {
        commit('LOGOUT_USER');
    },
    switchClient({commit, rootState, state}, client) {
        state.selectedClient = rootState.selectedClient = client;
        Message.info( 'You have switched to ' + client.fullname);
    },
    fetchSelectedClient({commit}) {
        commit('fetchSelectedClient');
    }
};


export default {
    state,
    getters,
    actions,
    mutations
}