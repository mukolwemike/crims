/**
 * Created by daniel on 31/01/2017.
 */
import Login from './Login';
import ForgotPassword from './ForgotPassword';

const state = {

};

const getters = {

};

const mutations = {
};

const actions = {
};


export default {
    state,
    getters,
    actions,
    mutations,
    modules: {
        Login,
        ForgotPassword
    },
    }
