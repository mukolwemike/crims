import * as ClientApi from '../../../api/ClientsApi';
import {Message} from "element-ui";
import router from '../../../router';

const state = {
    savingAccountDetails: false,
    deletingAccountDetails: false,
    accountDetailsForm: {
        country_id: 114
    },
    errors: [],
    accountBank: '',
    accountBranch: '',
};

const getters = {
    savingAccountDetails: state => state.savingAccountDetails,
    deletingAccountDetails: state => state.deletingAccountDetails,
    accountDetailsForm: state => state.accountDetailsForm,
    errors: state => state.errors,
    accountBank: state => state.accountBank,
    accountBranch: state => state.accountBranch,
};

const mutations = {
    SAVE_ACCOUNT_DETAILS(state, form) {
        state.savingAccountDetails = true;

        ClientApi.saveAccountDetails(form)
            .then(({data}) => {
                if (data.status !== 422)
                {
                    state.savingAccountDetails = false;
                    state.accountDetailsForm = {};
                    Message.success('Adding Account Details was successfully saved for approval');
                    router.push({name: 'profile.details'});
                }else {
                    state.savingAccountDetails = false;
                    Message.error(data.errors);
                    router.push({name: 'profile.details'});
                }
            }, ({}) => {
                state.savingAccountDetails = false;
                Message.error('An error occurred during saving details. Try Again later')
            })
    },

    FETCH_ACCOUNT_DETAILS(state, accId)
    {
        ClientApi.getAccountDetails(accId)
            .then(({data}) => {
                state.accountDetailsForm = data.details;
                state.accountBank = state.accountDetailsForm.bank;
                state.accountBranch = state.accountDetailsForm.branch;
            }, ({}) => {
                state.accountDetailsForm = {};
            })
    },

    DELETE_ACCOUNT_DETAILS(state, form)
    {
        state.deletingAccountDetails = true;

        ClientApi.deleteAccountDetails(form)
            .then(({data}) => {
                state.deletingAccountDetails = false;
                Message.success('Deactivating Account Details was successfully saved for approval');
                router.push({name: 'profile.details'});
            }, ({}) => {
                state.deletingAccountDetails = false;
                Message.error('An error occurred during saving for approval. Try Again later')
            })
    }
};

export default {
    state, getters, mutations
}


