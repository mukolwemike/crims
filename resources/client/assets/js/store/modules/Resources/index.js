/**
 * Created by yeric on 9/2/17.
 */
import * as type from '../../../store/mutation-types';
import * as rscApi from '../../../api/resourceApi';

import { Message } from 'element-ui'

const state = {
    Invproducts: {},
    interestReturn: {},
    rateSlider: [],
    duration: '',
    returnsForm: {},
    rate: '',
    fetchPrevailingRates: false,
    fetchingInvproducts: false,
    prevailingRates: {},
    investment_returns: {},
    fetchingReturns: false,
    fetchingGoalReturns: false,
    goal_returns: {}
};

const getters = {
    Invproducts: state => state.Invproducts,
    interestReturn: state => state.interestReturn,
    rateSlider: state => state.rateSlider,
    duration: state => state.duration,
    returnsForm: state => state.returnsForm,
    rate: state => state.rate,
    fetchPrevailingRates: state => state.fetchPrevailingRates,
    prevailingRates: state => state.prevailingRates,
    fetchingInvproducts: state => state.fetchingInvproducts,
    investment_returns: state => state.investment_returns,
    fetchingReturns: state => state.fetchingReturns,
    fetchingGoalReturns: state => state.fetchingGoalReturns,
    goal_returns: state => state.goal_returns
};

const mutations = {
    /**
     * get the list of the products
     * @param state
     */
    [type.GET_PRODUCTS] (state)
    {
        state.fetchingInvproducts = true;
        rscApi.getProducts('cms').then(function ({ data }) {
            state.Invproducts = data.data;
            state.fetchingInvproducts = false;
        }, function () {
                state.Invproducts = {};
                state.fetchingInvproducts = false;
        });
    },

    /**
     * get the interets rates and thus the interest on the amount provied
     * @param state
     * @param form
     */
    [type.GET_INTEREST_RETURN] (state, form)
    {
        if (form.product == null) {
            form.product = 1;
        }
        if (form.tenor == null) {
            form.tenor = 12;
        }

        state.fetchingReturns = true;

        rscApi.getInterestInvestment(form)
            .then( ({ data }) => {

                state.fetchingReturns = false;
                state.investment_returns = data.data;

            }, () => {
                state.fetchingReturns = false;
                Message.warning({ message: 'Failed to get prevailing rates, here' });
            }
        );
    },

    [type.GET_MONTHLY_RATES](state, form)
    {
        if (form.product === undefined) {
            form.product = 1;
        }
        if (form.tenor === undefined) {
            form.tenor = 12;
        }
        rscApi.getInterestRates(form).then(
            ({ data }) =>
            {
                state.rate = data;
            },
            () =>
            {
                Message.warning({ message: 'There are no prevailing rates for the product selected' });
            }
        );
    },

    GLOBAL_RATES_API(state, form) {
        if (form.product === undefined) {
            form.product = 1;
        }
        if (form.tenor === undefined) {
            form.tenor = 12;
        }

        rscApi.getGlobalInterestRate(form)
            .then(({ data }) => {

            }, () => {

            });
    },

    [type.GET_RATES] (state, product)
    {
        if (!product) {
            product = 1;
        }
        rscApi.getRates(product).then(
            ({ data }) =>
            {
                let productRates = [];
                data.forEach(
                    (r) =>
                    {
                        productRates.push(r.rate);
                    }
                );
            state.rateSlider = productRates;
            },
            () =>
            {
                Message.warning({ message: 'Failed to get prevailing rates for the product' });
            }
        );
    },

    /**
     * Get the goal value
     * @param state
     * @param form
     */
    [type.GOAL_VALUE] (state, form) {

        state.fetchingGoalReturns = true;

        rscApi.getGoalReturns(form)
            .then( ({ data }) => {

                    state.fetchingGoalReturns = false;
                    state.goal_returns = data.data;

                }, () => {
                    state.fetchingGoalReturns = false;
                    Message.warning({ message: 'Failed to get goal calculations' });
                }
            );
    }
};

const actions = {

};

export default {
    state, getters, mutations, actions
}