/**
 * Created by daniel on 24/01/2017.
 */
import * as types from '.././mutation-types';
import { Notification } from 'element-ui';
import * as Contact from '../.././api/ContactApi';

const state = {
    submitForm: false,
    contactUsSubmited: false,
    feedbackSubmited: false,
    formErrors: [],
    contactUsForm: {},
    feedbackForm: {},
    submitFeedbackForm: false,
    feedbackDialogForm: false,
    contactDialogForm: false,
};

const getters = {

    submitForm: state => state.submitForm,

    contactUsSubmited: state => state.contactUsSubmited,

    feedbackSubmited: state => state.feedbackSubmited,

    formErrors: state => state.formErrors,

    contactUsForm: state => state.contactUsForm,

    feedbackForm: state => state.feedbackForm,

    feedbackModal: (state) => { return state.feedbackDialogForm; },

    contactModal: (state) => { return state.contactDialogForm; },

    submitFeedbackForm: state => state.submitFeedbackForm,
};

const mutations = {

    //set the feedback modal state
    feedbackModal(state, active) {
        state.feedbackDialogForm = active;
    },

    //set contact us modal state
    contactModal(state, active) {
        state.contactDialogForm = active;
    },

    [types.CONTACT_US] (state, cont) {

        state.submitForm = true;

        Contact.CONTACT_US(cont)
            .then(
                function (response) {
                    state.formErrors = {};

                    state.submitForm = false;
                    state.contactUsSubmited = true;

                    state.contactDialogForm = false;
                    swal("Contact Us", "Your message has been sent succesfully, will get back to you", "success");

                },
                function (response) {

                    state.submitForm = false;
                    state.contactUsSubmited = false;

                    if (response.status === 422) {
                        state.formErrors = response.data;

                        Notification.warning({title: 'Form incomplete', message: 'Please fill in all the fields'});
                    } else {
                        Notification.error({title: 'Error', message: 'There was an error submitting your request, try again later'});
                    }
                }
            );
    },

    CLEAR_CONTACT_US(state){
        state.formErrors = []
    },

    [types.SUBMIT_FEEDBACK] (state, form) {

        state.submitFeedbackForm = true;

        Contact.SUBMIT_FEEDBACK(form) .then(
            function (response) {

                state.formErrors = {};
                state.submitFeedbackForm = false;

                state.feedbackSubmited = true;

                state.formErrors = {};
                Notification.success({title: '', message: 'Feedback sent succesfully.'});
                state.feedbackDialogForm = false;

                swal("Feedback", "Feedback sent succesfully, will get back to you", "success");

            },
            function (response) {

                state.submitForm = false;
                state.feedbackSubmited = true;

                if (response.status === 422) {
                    state.formErrors = response.data;
                    Notification.warning({title: '', message: 'Please fill in all the fields'});
                } else {
                    Notification.error({title: '', message: 'There was an error submitting your feedback, try again later'});
                }
            }
        );
    }
};

const actions = {
};


export default {
    state,
    getters,
    actions,
    mutations
}