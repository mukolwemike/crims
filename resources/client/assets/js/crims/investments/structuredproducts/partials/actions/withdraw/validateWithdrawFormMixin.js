export default {
    data(){
        var ValidateWithDrawalDate = (rule, value, callback) =>{
            if(this.form.withdrawal_stage === 'premature'){
                if (!value || value ==='' ){
                    callback(new Error('Please Specify withdraw date.'));
                } else {
                    callback()
                }
            }else {
                callback()
            }
        };
        var validateAmount = (rule, value, callback) =>{
            if(this.form.amount_select ==='amount'){
                if (!value || value ==='' ){
                    callback(new Error('Please add a valid input form amount.'));
                } else {
                    callback()
                }
            }else {
                callback()
            }
        };

        var validateSelectedAccount = (rule, value, callback) =>{
            if(this.form.amount_select != 'principal_interest'){
                if (!value || value ==='' ){
                    callback(new Error('Please select account before proceeding.'));
                } else {
                    callback()
                }
            }else {
                callback()
            }
        };
        var validateTerms = (rule, value, callback) => {
            if (!value || value === '') {
                callback(new Error('Please accept terms of payment before proceeding.'));
            }else {
                callback();
            }
        };

        return {
            rules: {
                withdrawal_stage: [
                    {
                        required: true, message: 'Please specify withdraw stage', trigger: 'change'
                    }
                ],
                withdrawal_date: [
                    {
                        validator: ValidateWithDrawalDate, trigger: 'change'
                    }
                ],
                amount_select: [
                    {
                        required: true, message: 'Please select amount before proceeding', trigger: 'change'
                    }
                ],
                amount: [
                    {
                        validator: validateAmount, trigger: 'change'
                    }
                ],
                client_account_id: [
                    {
                        validator: validateSelectedAccount, trigger: 'change'
                    }
                ],
                agreed_to_terms: [
                    {
                        type: 'boolean', message: 'Value provided is invalid.', trigger: 'change'
                    },
                    {
                        validator: validateTerms, trigger: 'change'
                    }
                ]

            }
        }
    }
}