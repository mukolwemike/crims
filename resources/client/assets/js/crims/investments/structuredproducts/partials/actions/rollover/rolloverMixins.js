/**
 * Created by Raphael Karanja on 15/11/2018.
 */
import {mapGetters} from 'vuex';
import events from '../../../../../../events';
import {resolveObject} from '../../../../../../utils';
import invUtils from '../../../../../../mixins/investments/utils';
import ValidationMixin from './validateRolloverMixin'
import {Message} from 'element-ui';

export default {
    mixins: [ValidationMixin, invUtils],
    data: () => ({
        step: 'form',
        rolloverForm: {
            agreed_to_terms: false,
            channel: 'web'
        },
        interest_rate: '',
        rolled_amount: 0,
        dialogTableVisible: false,
        options: [
            {value: 'principal', label: 'Principal'},
            {value: 'principal_interest', label: 'Principal + Interest'},
            {value: 'interest', label: 'Interest'},
            {value: 'amount', label: 'Amount'},
        ],
    }),

    computed: {
        ...mapGetters({
            currentUser: 'currentUser',
            investment: 'investment',
            rate: 'agreedRate',
            form: 'rolloverForm',
            fetchingagreedRate: 'fetchingRolloverRates',
            rollover_amount: 'rolloverAmount',
            accounts: 'clientBankAccounts',
            selectedClient: 'selectedClient',
            submitting: 'submittingRollover',
            submitRolloverSuccess: 'submitRolloverSuccess',
            rolloverFormStep: 'rolloverFormStep',
            submittingConfirm: 'submittingConfirm',
        }),
        amount_balance: function () {
            let balance = parseInt(this.investment.amount) - parseInt(this.rolloverForm.amount);
            if (balance > 0) {
                return balance;
            } else {
                return 'Amount specified exceeds the total (Principal + Interest ) in your account and thus gives a Negative balance which is invalid';
            }
        },
        currency_code: function () {
            if (this.investment.currency_code === "KES") {
                return "Ksh. ";
            } else if (this.investment.currency_code === "USD") {
                return "$";
            } else {
                return "";
            }
        },
        amount_select() {
            return this.rolloverForm.amount_select;
        },
        tenor() {
            return this.rolloverForm.tenor;
        },
    },

    watch: {
        rate() {
            this.interest_rate = this.rate;
            this.rolloverForm.agreed_rate = parseFloat(this.rate);
        },
        amount_select() {
            if (this.amount_select) {
                this.getRolloverAmount();
            }
        },
        tenor() {
            if (this.tenor !== NaN) {
                this.form.tenor = this.calculateTenor();
            }
            // this.rates();
        },
        rollover_amount: {
            handler: function () {
                this.rolled_amount = this.rollover_amount;
            }
        },
        rolloverFormStep() {
            this.step = this.rolloverFormStep;
            if (this.rolloverFormStep === 'confirm') {
                let name = 'Confirm Rollover';
                this.step = 'confirm';
                events.bus.$emit('update_title', name);
            }
            if (this.rolloverFormStep === 'success') {
                events.bus.$emit('update_title', 'Rollover was Successful');
                events.bus.$emit('close_drawer');
            }
        },

    },


    methods: {

        bankAccounts: function () {
            if (this.selectedClient && this.accounts.length === 0) {
                this.$store.commit('CLIENT_BANK_ACCOUNTS', this.selectedClient);
            }
        },

        showConfirm: function (formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    this.rolloverForm.investment_id = this.investment.id;
                    delete this.rolloverForm.selected_account;
                    this.rolloverForm.status = true;
                    if (this.rolloverForm.selected_account) {
                        this.rolloverForm.client_account_id = this.rolloverForm.selected_account.id;
                    }
                    this.$store.commit('CONFIRM_INV_ROLLOVER', this.rolloverForm);
                } else {
                    return false;
                    Message.warning({message: 'Please fill all required inputs before submitting'});
                }
            });
        },

        submit() {
            this.setFormData();
            delete (this.rolloverForm.selected_account);

            this.$store.commit('SUBMIT_ROLLOVER_FORM', this.rolloverForm);
        },

        setFormData() {
            if (this.form.amount) {
                this.rolloverForm.amount = this.form.amount;
            }

            switch (this.amount_select) {
                case 'principal_interest':
                    this.rolloverForm.amount = this.investment.total_value;
                    break;
                case 'principal':
                    this.rolloverForm.amount = this.investment.amount;
                    break;
                case 'interest':
                    this.rolloverForm.amount = this.investment.net_interest;
                    break;
            }

        },

        getRolloverAmount() {
            let form = {
                amount_select: this.amount_select,
                amount: this.rolloverForm.amount,
                investment_id: this.investment.id,
            };
            this.$store.commit('GET_ROLLOVER_AMOUNT', form);
        },

        rates: function () {
            const form = {
                product: resolveObject('product.data.id', this.investment),
                tenor: this.calculateTenor(),
                client_id: this.selectedClient.id,
                amount: this.rolled_amount
            };

            if (form.product) {
                this.$store.commit('GET_ROLLOVER_RATES', form);
            } else {
                this.checkRates();
            }
        },

        checkRates: function () {
            var vm = this;
            setTimeout(function () {
                vm.rates();
            }, 1000);
        },

        calculateTenor() {
            return this.tenor === "NaN" ? parseFloat(this.tenor) : parseFloat(this.tenor);
        },

        clearForm() {
            this.rolloverForm = {};
            this.step = 'form';
            if (this.$refs['rolloverForm']) {
                this.$refs['rolloverForm'].resetFields();
            }
        },

        close() {
            this.clearForm();
            events.bus.$emit('close_Rollover');
        }
    },

    created() {
        events.bus.$on('clear_drawer_form', () => {
            this.clearForm();
        });
    }
}
