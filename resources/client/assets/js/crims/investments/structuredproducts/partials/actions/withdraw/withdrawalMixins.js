import invUtils from '../../../../../../mixins/investments/utils';
import validateFormMixin from './validateWithdrawFormMixin';
import moment from 'moment';
import sweetAlert from 'sweetalert';
import {Message} from 'element-ui';
import {mapGetters} from 'vuex';
import events from "../../../../../../events";

// Vue.component('rollover-terms', require('../rollover/rollover_terms'));

export default {
    mixins: [invUtils, validateFormMixin],
    data() {
        return {
            form: {
                withdrawal_stage: 'mature',
                channel: 'web',
            },
            options: [
                {value: 'principal', label: 'Principal'},
                {value: 'principal_interest', label: 'Principal + Interest'},
                {value: 'interest', label: 'Interest'},
                {value: 'amount', label: 'Amount'},
            ],
            step: 'form',
            dialogTableVisible: false
        }
    },

    computed: {
        ...mapGetters({
            currentUser: 'currentUser',
            bankDetails: 'bankDetails',
            selectedClient: 'selectedClient',
            accounts: 'clientBankAccounts',
            investment: 'investment',
            withdrawalForm: 'withdrawalForm',
            loadingConfirm: 'loadingWithdrawConfirm',
            formStep: 'withdrawFormStep',
            submitting: 'submitWithdrawal',
        }),
        selected_account() {
            return this.form.client_account_id;
        },
        amount_select() {
            return this.form.amount_select;
        },
        shortname() {
            if (this.investment.product) {
                return this.investment.product.data.shortname
            } else {
                return ''
            }
        },
        currency_code() {
            if (this.investment.product) {
                return this.investment.product.data.currency.code
            } else {
                return ''
            }
        },
        withdraw_date() {
            return this.form.withdrawal_date;
        }
    },

    watch: {
        selected_account() {
            this.form.client_account_id = this.selected_account;
            this.setBankDetails();
        },
        amount_select() {
            if (this.form.withdrawal_stage === 'mature' && this.form.amount_select !== 'principal_interest' && this.form.amount_select !== undefined) {
                this.rollover();
            }
        },
        formStep() {
            this.step = this.formStep;

            if (this.formStep === 'confirm') {
                let name = 'Confirm Withdraw';
                this.step = 'confirm';
                events.bus.$emit('update_title', name + ' (' + this.shortname + ')');
            }

            if (this.formStep === 'success') {
                events.bus.$emit('update_title', 'Withdraw was Successful');
                events.bus.$emit('close_drawer');
            }
        },

        withdraw_date() {
            this.form.withdrawal_date = moment(this.form.withdrawal_date).format('YYYY-MM-DD');
        },

        canRollover() {
            this.checkStage()
        }
    },

    methods: {
        confirm(formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    if (this.bankDetails) this.form.account = this.bankDetails;
                    this.form.investment_id = this.$route.params.id;
                    this.$store.commit("CONFIRM_WITHDRAWAL", {form: this.form, status: true});
                } else {
                    Message.warning({
                        message: 'Cannot submit Withdraw details! Some form fields are missing'
                    });
                    return false;
                }
            });

        },

        confirmTransaction(){
            this.step = 'verify';
        },

        transValidated(){
            this.submit();
        },

        rollover: function () {
            var vm = this;
            if (this.canRollover) {
                sweetAlert({
                        title: "Roll over",
                        text: "To withdraw an amount less than the full amount (principal + interest) you will need to roll over",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#a3cf5f",
                        confirmButtonText: "Yes, continue to rollover",
                        closeOnConfirm: true
                    },
                    function (isConfirm) {
                        if (!isConfirm) {
                            events.bus.$emit('closeModal');
                            return false;
                        }
                        events.bus.$emit('open_rollover');
                        events.bus.$emit('close_withdraw');
                    });
            }
        },

        submit() {
            this.$store.commit('SUBMIT_CLIENT_WITHDRAWAL_FORM', this.form);
        },

        setBankDetails: function () {
            this.$store.commit('SET_BANK_DETAILS', this.selected_account);
        },
        clearForm() {
            this.form = {};
            this.step = 'form';
            if (this.$refs['withdrawForm']) {
                this.$refs['withdrawForm'].resetFields();
            }
            this.checkStage();
        },
        close() {
            events.bus.$emit('close_withdraw');
            this.clearForm();
        },
        checkStage() {
            this.form.withdrawal_stage = 'mature';
        }
    },

    created() {
        events.bus.$on('clear_drawer_form', () => {
            this.clearForm();
        })
    },
    mounted() {
        this.checkStage();
    }
}
