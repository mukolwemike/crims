/**
 * Created by Raphael Karanja on 15/11/2018.
 */
export default {
    data() {
        var ValidateAmount =  (rule, value, callback) => {
            if(this.rolloverForm.amount_select === 'amount') {
                if (!value || value == '') {
                    callback(new Error('Please add a valid value for amount.'));
                }else {
                    callback();
                }
            }else {
                callback();
            }
        };
        var ValidateTenorValue =  (rule, value, callback) => {
            if(this.rolloverForm.tenor ==='NaN') {
                if (!value || value === '') {
                    callback(new Error('Please add a valid months value.'));
                }else {
                    callback();
                }
            }else {
                callback();
            }
        };
        var ValidateSelectedAccount =  (rule, value, callback) => {
            if(this.rolloverForm.amount_select !== 'principal_interest') {
                if (!value || value === '') {
                    callback(new Error('Please Select the account to send the balance to.'));
                }else {
                    callback();
                }
            }else {
                callback();
            }
        };
        var ValidateTenorValueValidity =  (rule, value, callback) => {
            if(this.rolloverForm.tenor ==='NaN') {
                if (value % 1 !== 0 ||  value===0 ) {
                    callback(new Error('Kindly provide a valid input for period (Value in months only)'));
                }else {
                    callback();
                }
            }else {
                callback();
            }
        };

        var validateTerms = (rule, value, callback) => {
            if (!value || value === '') {
                callback(new Error('Please accept terms of payment before proceeding.'));
            }else {
                callback();
            }
        };
        return {
            rules:{
                amount_select: [
                    {
                        required: true, message: 'Please select amount to rollover before proceeding', trigger: 'change'
                    }
                ],
                amount: [
                    {
                        validator: ValidateAmount, trigger: 'change'
                    }
                ],
                tenor: [
                    {
                        required: true, message: 'Please add a valid value for New investment duration', trigger: 'change'
                    }
                ],
                agreed_rate: [
                    {
                        required: true, message: 'Agreed rate is not processed. Please fill in all the required fields.', trigger: 'change'
                    }
                ],
                tenor_value: [
                    {
                        validator: ValidateTenorValue, trigger: 'change'
                    },
                    {
                        validator: ValidateTenorValueValidity, trigger: 'change'
                    }
                ],
                selected_account: [
                    {
                        validator: ValidateSelectedAccount, trigger: 'change'
                    }
                ],
                agreed_to_terms: [
                    {
                        type: 'boolean', message: 'Value provided is invalid.', trigger: 'change'
                    },
                    {
                        validator: validateTerms, trigger: 'change'
                    }
                ]
            }
        }
    },
    methods: {},
}