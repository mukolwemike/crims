import { mapGetters} from 'vuex';
import events from '../../../../../../events';

export default {

    props: {
        product_id: {
            default: '1'
        }
    },

    data: function () {
        let validateTerms = (rule, value, callback) => {
            if (value === false) {
                callback(new Error('Please accept terms of payment before proceeding.'));
            } else {
                callback();
            }
        };

        return {
            topupForm: {
                agreed_to_terms: false,
                product: 1,
                channel: 'web'
            },
            interest_payment_interval: '',
            tenor_error: false,
            dialogTableVisible: false,
            shortname: 'Cytonn HYS',
            step: 'form',
            product: 1,
            filename: undefined,
            errors: [],
            submitting: false,
            payBank: false,
            payMpesa: false,
            rules: {
                amount: [
                    {
                        required: true, message: 'Please input valid value for amount', trigger: 'change'
                    }
                ],
                entered_tenor: [
                    {
                        required: true, message: 'Please input a valid tenor value', trigger: 'change'
                    }
                ],
                agreed_rate: [
                    {
                        required: true, message: 'Rate is not defined', trigger: 'change'
                    }
                ],
                agreed_to_terms: [
                    {
                        type: 'boolean', message: 'Value provided is invalid.', trigger: 'change'
                    },
                    {
                        validator: validateTerms, trigger: 'change'
                    }
                ]
            }
        }
    },

    watch: {
        "topupForm.mode_of_payment": {
            handler: function () {
                this.payBank = (this.topupForm.mode_of_payment === 'bank');
            }
        },
        tenor: function () {
            this.topupForm.tenor = this.tenor;
        },

        interest_payment_interval: {
            handler: function () {
                this.topupForm.interest_payment_interval = this.interest_payment_interval
            }
        },

        submitSuccess: {
            handler: function () {
                this.step = 'success';
                events.bus.$emit('update_title', 'Top-up was Successful');
                events.bus.$emit('close_drawer');

                setTimeout(() => {
                    if (this.topupCount == 0) {
                        this.$router.push({
                            name: 'investments.TopUPList'
                        });
                    }
                }, 2100);

            }
        },

        formErrors() {
            this.errors = this.formErrors;
        },
        submitTopup() {
            this.submitting = this.submitTopup
        },
        product: {
            handler: function () {

                this.topupForm.product = this.product;

                let selectedProduct = this.allProducts.filter((product) => {
                    return product.id === this.product;
                })[0];

                this.shortname = selectedProduct.shortname;
                events.bus.$emit('update_title', 'Top-up ' + this.shortname);
                this.getRates();
            }
        },

        product_id() {
            this.product = this.product_id
        },

        rate() {
            this.topupForm.agreed_rate = this.rate;
            this.$refs['topupForm'].validate((valid) => {
            });
        },

        canFetchRate: {
            handler: function () {
                this.getRates();
            }, deep: true
        }
    },

    computed: {
        ...mapGetters({
            topup_successful: 'topupSuccessful',
            products: 'clientProducts',
            allProducts: 'allProducts',
            allUnitFunds: 'all_UnitFunds',
            formErrors: 'topupErrors',
            topup_count: 'topupCount',
            form: 'topupForm',
            headers: 'defaultHeaders',
            loadingRates: 'loadingRates',
            submitTopup: 'submitTopup',
            getLoader: 'getLoader',
            selectedClient: 'selectedClient',
            confirmTopup: 'confirmTopup',
            submitSuccess: 'submitSuccess',
            calloutAction: 'calloutAction',
            topupCount: 'topupCount',
            fetchingAllProducts: 'fetchingAllProducts',
            fetchingAllUnitFunds: 'fetchingAllUnitFunds',
            currentUser: 'currentUser'
        }),

        tenor: function () {
            let tenor = this.topupForm.entered_tenor;
            if (tenor === 'NaN') return this.topupForm.period;

            return tenor;
        },
        rate() {
            return this.form.agreed_rate
        },
        canFetchRate() {
            let form = {};
            if ((this.tenor !== undefined) && (this.topupForm.amount !== '') && this.topupForm.product !== '') {
                form.tenor = this.tenor;
                form.amount = this.topupForm.amount;
                form.prroduct = this.topupForm.product;
                return form;
            }
        },

        url() {
            return '/api/investments/' + this.selectedClient.id + '/topup'
        }
    },

    methods: {
        resetForm() {
            this.topupForm = {
                agreed_to_terms: false,
                product: 1
            };
            this.tenor_error = false;
            this.step = 'form';
            if (this.$refs['topupForm']) {
                this.$refs['topupForm'].resetFields();
            }
            if (this.$refs.upload) {
                this.$refs.upload.clearFiles();
            }
            this.filename = undefined
        },

        close() {
            events.bus.$emit('close_drawer');
        },

        getProducts() {
            this.$store.commit('GET_CLIENT_PRODUCTS', this.selectedClient.id);
        },

        getAllProducts() {
            this.$store.commit('GET_ALL_PRODUCTS');
        },

        getAllUnitFunds() {
            this.$store.commit('GET_ALL_UNIT_FUNDS');
        },

        showConfirm(formName) {
            this.topupForm.agreed_rate = this.form.agreed_rate;
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    let name = 'Confirm Top-up';
                    this.step = 'confirm';
                    events.bus.$emit('update_title', name + ' (' + this.shortname + ')');

                } else {
                    this.$message({
                        message: 'Cannot submit Top-up details! Some form fields are missing',
                        type: 'warning'
                    });
                    return false;
                }
            });
        },

        getRates: function () {
            if ((this.tenor !== undefined) && (this.topupForm.amount !== '') && this.topupForm.product !== '') {
                this.topupForm.tenor = this.tenor;
                this.topupForm.client_id = this.selectedClient.id;

                if (this.tenor % 1 !== 0 || this.tenor === 0 || this.tenor < 3) {
                    this.tenor_error = true;
                } else {
                    this.tenor_error = false;
                    this.$store.commit('GET_TOPUP_RATES', this.topupForm);
                }
            }
        },

        submit() {
            if (this.$refs['upload'] && this.$refs['upload'].uploadFiles[0] && this.topupForm !== {}) {
                this.submitting = true;
                this.$refs['upload'].submit();
            } else {
                this.submitTopupForm();
            }

        },

        submitTopupForm() {
            this.$store.dispatch('topupInvestment', this.topupForm);
        },

        showFile(file) {
            this.filename = file;
        },

        showUploadSuccess(data) {
            this.submitting = false;
            if (parseInt(data.status) === 201) {
                this.step = 'success';
                events.bus.$emit('update_title', 'Top-up was Successful');
                events.bus.$emit('close_drawer');

                setTimeout(() => {
                    if (parseInt(this.topupCount) === 0) {
                        this.$router.push({
                            name: 'investments.TopUPList'
                        });
                    }
                }, 2100);
            } else {
                this.errors = data.errors;
                this.$message({
                    message: 'An Error occurred when submitting the form!',
                    type: 'warning'
                });
                this.step = 'form';
                events.bus.$emit('update_title', 'TOP-UP (Form Error!!)');
            }

        },

        errorOccurred(data) {
            this.$message({
                message: 'An Error occurred when submitting the form!',
                type: 'warning'
            });
            },
    },

    created() {
        events.bus.$on('clear_drawer_form', () => {
            this.resetForm();
        })
    },

    mounted() {
        this.getAllProducts();
        this.getAllUnitFunds();
    }
}
