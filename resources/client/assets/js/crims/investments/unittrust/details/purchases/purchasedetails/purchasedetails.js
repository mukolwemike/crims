/**
 * Created by Raphael Karanja on 12/09/2018.
 */
import {mapGetters} from 'vuex';
import {Notification} from 'element-ui';

export default {
    name: "purchasedetails",

    data: () => ({
        fund_id: null,
        fund: {},
        purchase: {},
        bank_details_modal: false
    }),
    computed: {
        ...mapGetters({
            fundInstructions: 'fundInstructions',
            selectedClient: 'selectedClient',
            funds: 'fundsummary',
            defaultHeaders: 'defaultHeaders',
        }),
        url() {
            return '/api/unitization/unit-funds/unit-fund-clients/' + this.purchase.id + '/bank-transfer'
        }
    },
    watch: {
        funds() {
            this.filterfunds();
        },
        fundInstructions() {
            this.filterPurchase();
        },
        selectedClient() {
            this.checkPageData();
        }
    },
    methods: {
        fetchFundInstructions() {
            let details = {
                fund_id: this.$route.params.fundId,
                client_id: this.selectedClient.id
            };

            if (this.fundInstructions.length == 0) {
                this.$store.commit('FETCH_FUND_INSTRUCTIONS', details);//unitization/unit-funds/{fundId}/unit-fund-clients/{clientId}/unit-fund-instructions
            } else {
                this.filterPurchase();
            }
        },
        fetchFundSummary() {
            if (this.funds.length === 0) {
                this.$store.commit('FETCH_FUND_SUMMARY', this.selectedClient.id);
            } else {
                this.filterfunds();
            }

        },
        filterfunds() {
            let this_fund = this.funds.filter(fund => fund.id == this.$route.params.fundId);
            this.fund = this_fund;
        },
        filterPurchase() {
            let fundPurchase = this.fundInstructions;
            let purchase = fundPurchase.filter(pur => pur.id == this.$route.params.id);
            if (purchase.length > 0) {
                this.purchase = purchase[0];
            }
        },
        showUploadSuccess() {
            Notification.success({title: '', message: 'File uploaded successfully'});

            let details = {
                fund_id: this.$route.params.fundId,
                client_id: this.selectedClient.id
            };
            this.$store.commit('FETCH_FUND_INSTRUCTIONS', details);
        },
        checkPageData() {
            if (!this.selectedClient.hasUT) {
                this.$router.push({
                    path: '/home'
                });
            }
        },

        closeBankDetailsModal() {
            this.bank_details_modal = false;
        },
    },
    mounted() {
        this.fetchFundSummary();
        this.fetchFundInstructions();
        this.checkPageData();
        this.fund_id = this.$route.params.fundId;
    }
}