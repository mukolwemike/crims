import {mapGetters} from 'vuex';
import PaginationMixins from '../../../../../mixins/vuetable/paginationMixins'
export default {
    mixins: [PaginationMixins],
    data: ()=>({
        fields: [
            {
                name: 'date',
                title: 'Date'
            },
            {
                name: 'senderName',
                title: 'Sender Name'
            },
            {
                name: 'senderNumber',
                title: 'Sender Number'
            },
            {
                name: 'recipientName',
                title: 'Receipt Name',
            },
            {
                name: 'recipientNumber',
                title: 'Receipt Number'
            },
            {
                name: 'numberOfUnits',
                title: 'Number of Units'
            }

        ]
    }),

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient'
        }),
        url(){
            return '/api/unitization/unit-funds/'+this.$route.params.id+'/unit-fund-clients/'+this.selectedClient.id+'/unit-fund-transfers-given'
        }
    }

}