import {mapGetters} from 'vuex';
import PaginationMixins from '../../../../../mixins/vuetable/paginationMixins'
export default {
    mixins: [PaginationMixins],
    data: ()=>({
        fields: [
            {
                name: 'amount_before_investment',
                title: 'Total Amount',
            },
            {
                name: 'numberOfUnits',
                title: 'No of Units'
            },
            {
                name: 'unitPrice',
                title: 'Unit Price'
            },
            {
                name: 'feesIncurred',
                title: 'Fees Incurred'
            },
            {
                name: 'date',
                sortField: 'date',
                title: 'Date'
            },
            {
                name: 'currency',
                title: 'Currency'
            }

        ]
    }),

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient'
        }),
        url(){
            return '/api/unitization/unit-funds/'+this.$route.params.id+'/unit-fund-clients/'+this.selectedClient.id+'/unit-fund-purchases'
        }
    }

}