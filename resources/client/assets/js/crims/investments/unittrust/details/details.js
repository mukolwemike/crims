/**
 * Created by Raphael Karanja on 06/09/2018.
 */
import {mapGetters} from 'vuex';
import Vue from 'vue';
import events from '../../../../events';

Vue.component('buy-units', require('./actions/buy/buy.vue'));
Vue.component('sell-units', require('./actions/sell/sell.vue'));
Vue.component('transfer-units', require('./actions/transfer/transfer.vue'));
Vue.component('unit-instructions', require('./instructions/instructions.vue'));
Vue.component('unit-purchases', require('./purchases/purchases.vue'));
Vue.component('unit-sales', require('./sales/sales.vue'));
Vue.component('unit-transfers-given', require('./transfersgiven/transfersgiven.vue'));
Vue.component('unit-transfers-received', require('./transfersrecieved/transfersrecieved.vue'));

export default {
    data() {
        return {
            fund: {},
            buy_units: false,
            sell_units: false,
            transfer_units: false,
            fundDetailed: [],
        }
    },

    computed: {
        ...mapGetters({
            fundsummary: 'fundsummary',
            selectedClient: 'selectedClient',
            fetchingFundSummary: 'fetchingFundSummary',
            fundBalance: 'fundBalance',

            fetchingFundPurchases: 'fetchingFundPurchases',
            unitPurchases: 'unitPurchases',

            fundSales: 'fundSales',
            fundTransfers: 'fundTransfers',

            fundGivenTransfers: 'fundGivenTransfers',
            fetchingGivenTransfers: 'fetchingGivenTransfers',

            fundRecievedTransfers: 'fundRecievedTransfers',
            fetchingRecievedTransfers: 'fetchingRecievedTransfers',

            fundInstructions: 'fundInstructions',
            fetchingFundInstructions: 'fetchingFundInstructions',

            cisUnitFunds: 'cisUnitFunds'
        }),

        canSellUnit() {
            return this.fund.ownedUnits > 0;
        },

        fund_purchases() {
            if (this.fundDetailed.purchases) {
                return this.fundDetailed.purchases.total
            }
            return {}
        },

        fund_sales() {
            if (this.fundDetailed.sales) {
                return this.fundDetailed.sales.total
            }
            return {}
        },

        fundGiven_Transfers() {
            if (this.fundDetailed.transfers_given) {
                return this.fundDetailed.transfers_given.total;
            }
            return {}
        },

        fundRecieved_Transfers() {
            if (this.fundDetailed.transfers_received) {
                return this.fundDetailed.transfers_received.total;
            }
            return {}
        },

        fund_Instructions() {
            if (this.fundDetailed.instructions) {
                return this.fundDetailed.instructions.total
            }
            return {};
        },
    },
    watch: {
        fundsummary: {
            handler: function () {
                this.filterfunds();
            }
        },

        buy_units: {
            handler: function () {
                if (this.buy_units) {
                    events.bus.$emit('update_title', 'Buy Units');
                }
            }
        },

        sell_units: {
            handler: function () {
                if (this.sell_units) {
                    events.bus.$emit('update_title', 'Sell Units');
                }
            }
        },
        transfer_units: {
            handler: function () {
                if (this.transfer_units) {
                    events.bus.$emit('update_title', 'Transfer Units');
                }
            }
        },
        selectedClient() {
            this.checkPageData()
        }
    },
    methods: {
        getParamId() {
            return this.$route.params.id;
        },
        fetchFundSummary() {
            if (this.fundsummary.length === 0) {
                this.$store.commit('FETCH_FUND_SUMMARY', this.selectedClient.id);
            } else {
                this.filterfunds();
            }
        },

        checkPageData() {
            if (!this.selectedClient.hasUT) {
                this.$router.push({
                    path: '/home'
                });
            }
        },

        filterfunds() {

            if (this.fundsummary.length > 0) {
                let fundId = this.$route.params.id;
                let funds = this.fundsummary.filter(fund => fund.details.fundId === parseInt(fundId));

                let this_fund = funds[0];

                if (this_fund) {
                    this.fund = this_fund.details;
                    this.fundDetailed = this_fund;

                }

            }
        },

        gotoPurchase(purchase) {
            this.$store.commit('GO_TO_PURCHASE', purchase);
        },

        onPaginationData(paginationData) {
            this.$refs.pagination.setPaginationData(paginationData);
            this.$refs.paginationInfo.setPaginationData(paginationData);
        },

        onChangePage(page) {
            this.$refs.vuetable.changePage(page);
        },

        getCISUnitFunds() {
            if (this.cisUnitFunds.length === 0) {
                this.$store.commit('GET_CIS_UNIT_FUNDS');
            }
        },
    },


    mounted() {
        this.fetchFundSummary();
        this.checkPageData();
        this.getCISUnitFunds();
    }
}