/**
 * Created by Michael Mukolwe on 19/07/2019.
 */

import events from "../../../../../../events";
import {mapGetters} from 'vuex';
import moment from "moment";
import axios from 'axios';

export default {
    props: {
        fund: {
            require: true,
            default: '',
        }
    },

    data: () => ({
        step: 'form',
        transferUnitsForm: {},
        client_code: '',
        errors: [],
        fundDetails: {},
        submittingFundTransfer: false,
        fetchingTransferClient: false,
        transferClient: {},
        transferLimit: false,
        minTransfer: 100,
        maxTransfer: 100000,
        confirmingFundTransfer: false,
    }),

    watch: {
        "transferUnitsForm.number": function () {
            this.transferLimit = this.transferUnitsForm.number < this.minTransfer;
        }
    },

    computed: {
        ...mapGetters({
            currentUser: 'currentUser',
            selectedClient: 'selectedClient',
            fundSummary: 'fundsummary',
        }),

        canTransferUnit() {
            if (this.fundDetails) {
                let units = parseInt(this.fundDetails.ownedUnitsUnedited);
                return !!(this.transferUnitsForm.number >= this.minTransfer
                    && this.transferUnitsForm.number <= units
                    && units > 0
                    && this.transferUnitsForm.number <= this.fundDetails.withdrawable_units
                    && this.transferUnitsForm.number !== ''
                    && this.client_code);
            }
        },

        sameClient(){
            return (this.client_code === this.selectedClient.client_code);
        },
    },

    methods: {
        checkLimit(num) {
            this.checking(num, this);
        },

        checking: _.debounce((num, vm) => {
            this.transferLimit = this.transferUnitsForm.number < this.minTransfer;
        }, 350),

        confirmUnitTransfer: function () {
            this.confirmingFundTransfer = true;
            this.transferUnitsForm.date = moment().format('YYYY-MM-DD');

            if (this.transferUnitsForm.number && this.client_code) {
                this.fetchClient();
                this.step = 'confirm';
                events.bus.$emit('update_title', 'Confirm ' + this.fundDetails.shortname + ' Transfer');
            } else {
                this.$message({
                    message: 'Please fill in all the form fields before proceeding!!',
                    type: 'warning'
                });
            }
        },

        fetchClient() {
            this.fetchingTransferClient = true;

            axios.get('/api/unitization/unit-fund/' + this.client_code).then(
                ({data}) => {
                    this.transferClient = data.data;
                    this.fetchingTransferClient = false;
                },
                () => {
                    this.fetchingTransferClient = false;
                    this.$message({
                        message: 'Unknown Client with the given client code',
                        type: 'warning'
                    });
                    this.step = 'form';
                }
            );
        },

        verifyTransfer() {
            this.step = 'verify';
        },

        transValidated() {
            this.completeTransfer();
        },

        completeTransfer() {
            this.submittingFundTransfer = true;
            this.transferUnitsForm.date = moment().format('YYYY-MM-DD');

            this.setupTransferForm();

            axios.post('/api/unitization/unit-funds/' + this.fundDetails.fundId + '/unit-fund-clients/' + this.selectedClient.id + '/unit-fund-transfers', this.transferUnitsForm).then(
                ({data}) => {
                    this.submittingFundTransfer = false;

                    if (!data.locked && data.status === 422) {
                        this.step = 'form';
                        this.resetForm();
                        this.$message({
                            showClose: true,
                            message: data.message,
                            type: 'warning',
                            duration: 5000
                        });
                    }else if (data.status !== 422) {
                        this.step = 'success';
                        events.bus.$emit('update_title', 'Transfer was Successful');
                        events.bus.$emit('close_drawer');
                        events.bus.$emit('refresh_instructions');
                    } else {
                        this.errors = data.errors;
                        this.$message({
                            message: 'An Error occurred when submitting the form!',
                            type: 'warning'
                        });
                        this.step = 'form';
                        events.bus.$emit('update_title', 'Transfer Units (Form Error!!)');
                    }
                },
                () => {
                    this.submittingFundTransfer = false;
                    this.$message({
                        message: 'An Error occurred when submitting the form!',
                        type: 'warning'
                    });
                    this.step = 'form';
                    events.bus.$emit('update_title', 'Transfer Units (Form Error!!)');
                }
            );
        },

        setupTransferForm() {
            this.transferUnitsForm.transferee_id = this.transferClient.id;
            this.transferUnitsForm.transferer_id = this.selectedClient.id;
            this.transferUnitsForm.user_id = this.currentUser.id;
            this.transferUnitsForm.unit_fund_id = this.fundDetails.fundId;
        },

        cancel() {
            events.bus.$emit('close_drawer');
            this.resetForm();
        },

        resetForm() {
            this.transferUnitsForm = {};
            this.step = 'form';
            this.errors = [];
            events.bus.$emit('update_title', 'Transfer Units');

        },

        setupFundSummary() {
            this.fundDetails = this.fundSummary.length > 0 ? this.fundSummary[0].details : '';
        }
    },

    created() {
        this.setupFundSummary();

        events.bus.$on('clear_drawer_form', () => {
            this.resetForm();
        })
    },
}
