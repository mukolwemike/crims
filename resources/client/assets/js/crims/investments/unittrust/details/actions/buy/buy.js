/**
 * Created by Raphael Karanja on 12/09/2018.
 */
import {mapGetters} from 'vuex';
import events from '../../../../../../events';
import moment from 'moment';

export default {
    props: {
        fund: {
            default: '',
            required: true
        }
    },
    data: () => ({
        buyUnitsForm: {
            fundId: ''
        },
        actions: 'units',
        submittingFundPurchases: false,
        amount: '',
        errors: [],
        step: 'form',
        filename: undefined,
        unit_fund_id: '',
        payBank: false, payMpesa: false,
        confirmingBuyingUnits: false
    }),

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient',
            cisUnitFunds: 'cisUnitFunds',
            loadingClientFunds: 'fetchingClientFunds',
            fundBalance: 'fundBalance',
            fetchingFundBalance: 'fetchingFundBalance',
            unitsToPurchase: 'unitsToPurchase',
            fetchUnitsToPurchase: 'fetchUnitsToPurchase',
            headers: 'defaultHeaders',
            currentUser: 'currentUser',
        }),
        canBuyUnits() {
            return !!(this.buyUnitsForm.amount !== ''
                && !this.minimumTopUpAmount
                && this.buyUnitsForm.units && this.buyUnitsForm.unit_fund_id !== ''
                && this.buyUnitsForm.mode_of_payment && this.buyUnitsForm.mode_of_payment !== '');
        },
        url() {
            return '/api/unitization/unit-funds/' + this.fund.fundId + '/unit-fund-clients/' + this.selectedClient.id + '/unit-fund-purchases';
        },
        balance() {
            if (this.fund.balance) {
                return this.fund.balance.balance;
            } else {
                return {}
            }
        },
        fullform() {
            if (this.buyUnitsForm.unit_fund_id && this.buyUnitsForm.unit_fund_id !== '' && this.buyUnitsForm.amount && this.buyUnitsForm.amount !== '') {
                let form = {};
                form.fund = this.buyUnitsForm.unit_fund_id;
                form.amount = this.buyUnitsForm.amount;
                return form;
            }
        },
        fundName() {
            if (this.cisUnitFunds.length > 0 && this.buyUnitsForm.unit_fund_id && this.buyUnitsForm.unit_fund_id !== '') {
                return this.selectedFund.short_name;
            }
        },
        selectedFund() {
            if (this.cisUnitFunds.length > 0 && this.buyUnitsForm.unit_fund_id && this.buyUnitsForm.unit_fund_id !== '') {
                let unit = this.cisUnitFunds.filter(fund => fund.id === this.buyUnitsForm.unit_fund_id);
                if (unit.length > 0) {
                    return unit[0]
                }
            }
        },
        minimumTopUpAmount() {
            if (this.buyUnitsForm.amount && this.buyUnitsForm.amount !== '' && this.buyUnitsForm.unit_fund_id && this.buyUnitsForm.unit_fund_id !== '') {
                if (this.buyUnitsForm.amount < parseFloat(this.selectedFund.minimumTopupAmount)) {
                    return true;
                }
            }
        },

        funds() {
            return this.cisUnitFunds;
        },
    },

    watch: {
        fullform: {
            handler: function () {
                if (this.fullform) {
                    this.$store.commit('UNIT_PURCHASE_CALCULATOR', this.fullform);
                }
            }
        },

        "buyUnitsForm.mode_of_payment": {
            handler: function () {
                this.payBank = (this.buyUnitsForm.mode_of_payment === 'bank');
            }
        },

        unitsToPurchase: {
            handler: function () {
                if (this.unitsToPurchase) {
                    this.buyUnitsForm.units = this.unitsToPurchase.units;
                }
            }
        },

        fund() {
            this.getFundBalance()
        },

        unit_fund_id() {
            this.buyUnitsForm.unit_fund_id = this.unit_fund_id;
            this.getFundBalance();
        }
    },

    created() {
        events.bus.$on('clear_drawer_form', () => {
            this.resetForm();
        })
    },

    methods: {
        buy_units() {
            this.confirmingBuyingUnits = false;
            if (this.buyUnitsForm.amount !== '' && this.buyUnitsForm.unit_fund_id !== '') {
                this.step = 'confirm';
                this.buyUnitsForm.fund = this.fund.fundId;
                this.buyUnitsForm.units = this.unitsToPurchase.units;
                this.buyUnitsForm.date = moment().format('YYYY-MM-DD');
                events.bus.$emit('update_title', 'Confirm Purchase');
            } else {
                this.$message({
                    message: 'Please fill in all the form fields before proceeding!!',
                    type: 'warning'
                });
            }
        },
        resetForm() {
            this.buyUnitsForm = {};
            this.amount = '';
            this.unit_fund_id = '';
            this.filename = undefined;

            if (this.$refs.upload) {
                this.$refs.upload.clearFiles();
            }
            this.step = 'form';
            events.bus.$emit('update_title', '');
        },
        submitForm(units) {
            this.submittingFundPurchases = true;
            this.buyUnitsForm.user_id = this.currentUser.id;
            this.buyUnitsForm.channel = 'web';

            this.$http.post('/api/unitization/unit-funds/' + this.fund.fundId + '/unit-fund-clients/' + this.selectedClient.id + '/unit-fund-purchases', this.buyUnitsForm)
                .then(({data}) => {
                        this.submittingFundPurchases = false;
                        if (!data.locked && data.status === 422) {
                            this.step = 'form';
                            this.resetForm();
                            this.$message({
                                showClose: true,
                                message: data.message,
                                type: 'warning',
                                duration: 5000
                            });
                        } else {
                            this.step = 'success';
                            this.$message({
                                message: 'Purchase instruction saved successfully',
                                type: 'success',
                            });
                            events.bus.$emit('update_title', 'Purchase was Successful');
                            events.bus.$emit('close_drawer');
                            events.bus.$emit('refresh_instructions');
                        }
                    },
                    () => {
                        this.submittingFundPurchases = false;
                        this.resetForm();
                    });
        },
        submit() {
            event.preventDefault();
            if (this.$refs.upload.uploadFiles[0] && this.buyUnitsForm !== {}) {
                this.buyUnitsForm.user_id = this.currentUser.id;
                this.buyUnitsForm.date = moment().format('YYYY-MM-DD');
                this.submittingFundPurchases = true;
                this.$refs.upload.submit();
            } else {
                this.submitForm();
            }
        },

        getFundBalance() {
            let details = {
                fund_id: this.buyUnitsForm.unit_fund_id,
                client_id: this.selectedClient.id
            };

            if (this.buyUnitsForm.unit_fund_id) {
                this.$store.commit('FETCH_FUND_BALANCE', details);
            }
        },

        cancel() {
            events.bus.$emit('close_drawer');
            this.resetForm();
        },

        showUploadSuccess(data) {
            this.submittingFundPurchases = false;

            if (data.status === 201) {
                this.step = 'success';
                events.bus.$emit('update_title', 'Purchase was Successful');
            } else {
                this.errors = data.errors;
                if (!data.locked && data.status === 422) {
                    this.submittingFundPurchases = false;
                    this.$message({
                        showClose: true,
                        message: data.message,
                        type: 'warning',
                        duration: 5000
                    });
                } else {
                    this.$message({
                        message: 'An Error occurred when submitting the form!',
                        type: 'warning'
                    });
                }
                this.resetForm();
                this.step = 'form';
                events.bus.$emit('update_title', 'Purchase Units (Form Error!!)');
            }

            events.bus.$emit('close_drawer');
            events.bus.$emit('refresh_instructions');
        },
        errorOccurred(data) {
            this.submittingFundPurchases = false;
            this.errors = {};
            this.resetForm();

            if (!data.locked && data.status === 422) {
                this.$message({
                    showClose: true,
                    message: data.message,
                    type: 'warning',
                    duration: 5000
                });
            }else{
                this.$message({
                    message: 'An Error occurred when submitting the form!',
                    type: 'error'
                });
            }

            this.step = 'form';
            events.bus.$emit('update_title', 'Purchase Units (Form Error!!)')
        },
        showFile(file) {
            this.filename = file;
        }

    },
    mounted() {
        this.getFundBalance();
    }
}
