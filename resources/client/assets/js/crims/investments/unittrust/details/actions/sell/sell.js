/**
 * Created by Raphael Karanja on 12/09/2018.
 */
import {mapGetters} from 'vuex';
import moment from 'moment';
import events from '../../../../../../events';

export default {
    props: {
        fund: {
            require: true,
            default: ''
        }
    },
    data: () => ({
        sellUnitsForm: {},
        saleChargeForm: {fee: 0},
        calculatingSaleCharge: false,
        step: 'form',
        submittingFundSales: false,
        showLockInMessage: false,
        errors: [],
        formerrors: [],
        unit_fund_id: '',
        selectedBank: '',
        maxMpesaAmount: true,
        showMaxMessage: false,
        confirmingSellUnits: false,
        selectedBankObject: {},
    }),
    computed: {
        ...mapGetters({
            banks: 'clientBankAccounts',
            selectedClient: 'selectedClient',
            cisUnitFunds: 'cisUnitFunds',
            loadingClientFunds: 'fetchingClientFunds',
            fundSummary: 'fundsummary',
            currentUser: 'currentUser',
        }),
        canSellUnit() {
            let units = parseInt(this.fund.ownedUnitsUnedited);

            return !!(this.sellUnitsForm.number <= units && units > 0 && this.sellUnitsForm.number !== ''
                && this.sellUnitsForm.account_id && this.selectedClient.kycValidated
                && this.sellUnitsForm.number <= this.selectedFund.withdrawable_units - this.saleChargeForm.fee)
                && this.maxMpesaAmount;
        },
        funds() {
            return this.cisUnitFunds;
        },

        selectedFund() {
            if (this.fundSummary.length > 0 && this.sellUnitsForm.unit_fund_id && this.sellUnitsForm.unit_fund_id !== '') {
                let unit = this.fundSummary.filter(fund => fund.details.fundId === this.sellUnitsForm.unit_fund_id);

                if (unit.length > 0) {
                    return unit[0].details
                }
            }

            return {}
        },
    },
    watch: {
        "sellUnitsForm.number": {
            handler: function () {
                this.showLockInMessage = !!(this.sellUnitsForm.number
                    && (parseInt(this.fund.ownedUnitsUnedited) !== this.sellUnitsForm.number)
                    && this.selectedFund.lock_in_days > 0);

                this.showMaxMessage = false;
                this.maxMpesaAmount = true;
            }
        },

        "sellUnitsForm.account_id": {
            handler: function () {
                this.getSelectBankObject();
                if (this.selectedBankObject.swift_code === "MPESA" && this.sellUnitsForm.number > 70000) {
                    this.maxMpesaAmount = false;
                    this.sellUnitsForm.account_id = null;
                }
                this.calculateSaleCharge();
            }
        }
    },
    created() {
        events.bus.$on('clear_drawer_form', () => {
            this.resetForm();
        })
    },
    methods: {
        getSelectedBank() {
            let vm = this;
            vm.selectedBank = '';

            this.banks.forEach(function (bank) {
                if (bank.id === vm.sellUnitsForm.account_id) {
                    vm.selectedBank = bank.name;
                }
            });
        },

        getSelectBankObject() {
            this.selectedBankObject = {id: null, swift_code: ''};

            if (this.sellUnitsForm.account_id) {
                let bank = this.banks.filter(bank => bank.id === this.sellUnitsForm.account_id);

                this.selectedBankObject = bank[0];
            }
        },

        sell_units() {
            this.confirmingSellUnits = true;
            this.sellUnitsForm.date = moment().format('YYYY-MM-DD');
            this.getSelectedBank();

            if (this.sellUnitsForm.number) {
                this.step = 'confirm';
                events.bus.$emit('update_title', 'Confirm ' + this.fund.shortname + ' Sale');
            } else {
                this.$message({
                    message: 'Please fill in all the form fields before proceeding!!',
                    type: 'warning'
                });
            }
        },

        cancel() {
            events.bus.$emit('close_drawer');
            this.resetForm();
        },

        transValidated() {
            this.submit();
        },

        confirmTransaction() {
            this.step = 'verify';
        },

        calculateSaleCharge() {
            this.saleChargeForm.fee = 0;

            if (this.selectedBankObject.swift_code !== "MPESA" || !this.sellUnitsForm.number) {
                return;
            }

            this.saleChargeForm.fee = this.selectedFund.withdrawal_fee;

            //In case of future withdrawal dynamic charges

            // this.saleChargeForm.date = moment().format('YYYY-MM-DD');
            // this.saleChargeForm.units_sold = this.sellUnitsForm.number;
            // this.calculatingSaleCharge = true;
            //
            // this.$http.post('/api/unitization/unit-funds/' + this.fund.fundId + '/unit-fund-clients/' +
            //     this.selectedClient.id + '/unit-fund-sales/calculate-sale-charge', this.saleChargeForm)
            //     .then(({data}) => {
            //             this.calculatingSaleCharge = false;
            //             this.saleChargeForm.fee = data.data.fee;
            //         },
            //         () => {
            //             this.calculatingSaleCharge = false;
            //         });
        },

        submit() {
            this.submittingFundSales = true;
            this.sellUnitsForm.user_id = this.currentUser.id;
            this.sellUnitsForm.date = moment().format('YYYY-MM-DD');
            this.sellUnitsForm.fee = this.saleChargeForm.fee;
            this.sellUnitsForm.channel = 'web';

            this.$http.post('/api/unitization/unit-funds/' + this.fund.fundId + '/unit-fund-clients/' + this.selectedClient.id + '/unit-fund-sales', this.sellUnitsForm)
                .then(({data}) => {
                        this.submittingFundSales = false;
                        if (!data.locked && data.status === 422) {
                            this.step = 'form';
                            this.resetForm();
                            this.$message({
                                showClose: true,
                                message: data.message,
                                type: 'warning',
                                duration: 5000
                            });
                        }else if (data.status === 445) {
                            this.step = 'form';
                            this.$message({
                                message: data.errors,
                                type: 'warning'
                            });
                            events.bus.$emit('update_title', 'Sell Units (Form Error!!)');
                        } else if (data.status !== 422 && data.status !== 423) {
                            this.step = 'success';
                            events.bus.$emit('update_title', 'Sale was Successful');
                            events.bus.$emit('close_drawer');
                            events.bus.$emit('refresh_instructions');
                        } else {
                            this.showMaxMessage = data.status === 423 || data.status === 422;
                            this.errors = data.errors;
                            this.$message({
                                message: 'An Error occurred when submitting the form!',
                                type: 'warning'
                            });
                            this.step = 'form';
                            events.bus.$emit('update_title', 'Sell Units (Form Error!!)');
                        }
                    },
                    () => {
                        this.submittingFundSales = false;
                        this.resetForm();
                    });

        },
        resetForm() {
            this.sellUnitsForm = {};
            this.step = 'form';
            this.errors = [];
            this.selectedBank = '';
            events.bus.$emit('update_title', 'Sell Units');
        },
    }
}
