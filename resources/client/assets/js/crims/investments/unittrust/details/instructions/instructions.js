import {mapGetters} from 'vuex';
import PaginationMixins from '../../../../../mixins/vuetable/paginationMixins'
import events from "../../../../../events";
import accounting from "accounting";

export default {
    mixins: [PaginationMixins],
    data: ()=>({
        fields: [
            {
                name: 'type',
                title: 'Transaction Type'
            },
            {
                name: 'amount',
                title: 'Amount'
            },
            {
                name: 'number',
                title: 'No of Units'
            },
            {
                name: 'date',
                sortField: 'date',
                title: 'Date',
                callback: 'formatDate|DD-MM-YYYY'
            },
            {
                name: 'processesed',
                title: 'Status',
                callback: 'processedStatusLabel'
            },
            {
                name: '__slot:actions',
                title: 'Actions',
                dataClass: 'action',
            },

        ]
    }),

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient'
        }),
        url(){
            return '/api/unitization/unit-funds/'+this.$route.params.id+'/unit-fund-clients/'+this.selectedClient.id+'/unit-fund-instructions'
        }
    },


    created() {
        events.bus.$on('refresh_instructions', ()=>{
            if (this.$refs.vuetable){
                this.$refs.vuetable.reload();
            }
        });
    },

}