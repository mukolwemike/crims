/**
 * Created by Raphael Karanja on 06/09/2018.
 */

import {mapGetters} from 'vuex';

export default {

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient',
            funds: 'fundsummary',
            fetchingFundSummary: 'fetchingFundSummary',

        }),

    },
    watch: {
        selectedClient() {
            this.checkPageData()
        }
    },
    methods: {
        fetchFundSummary() {
            if (this.funds.length === 0) {
                this.$store.commit('FETCH_FUND_SUMMARY', this.selectedClient.id);
            }
        },
        checkPageData() {
            if (!this.selectedClient.hasUT) {
                this.$router.push({
                    path: '/home'
                });
            } else {
                this.$store.commit('FETCH_FUND_SUMMARY', this.selectedClient.id);
            }
        },
    },
    mounted() {
        this.fetchFundSummary();
        this.checkPageData();
    }
}
