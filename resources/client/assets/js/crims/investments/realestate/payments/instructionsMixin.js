import {mapGetters} from 'vuex'
import {Notification} from 'element-ui';

export default {
    data() {
        return {
            options: [{
                value: '',
                label: 'All'
            }, {
                value: 'uploaded',
                label: 'Uploaded'
            }, {
                value: 'not-uploaded',
                label: 'Not-Uploaded'
            }],
            uploaded_status: '',
            file: '',
            extensions: 'image/pdf',
            uploadStatus: ''
        }
    },

    computed: {
        ...mapGetters({
            payments: 'payments',
            selectedClient: 'selectedClient',
            fetchingREPayments: 'fetchingREPayments',
            fetchingREPayment: 'fetchingREPayment',
            paymentDetails: 'paymentDetails',
            defaultHeaders: 'defaultHeaders',
        }),

        url() {
            return '/api/realestate/payment/upload/' + this.$route.params.id;
        }
    },

    watch: {
        uploaded_status: {
            handler: function () {
                this.fetchPayments();
            }
        },

        selectedClient: {
            handler: function () {
                this.fetchPayments();
            }
        }
    },

    methods: {
        fetchPayments: function () {
            let data = {
                'clientId': this.selectedClient.id,
                'uploadStatus': this.uploaded_status
            };

            this.$store.commit('GET_UNIT_PAYMENTS', data);
        },

        viewPayments: function () {
            this.$router.push({name: 'investments.RePayments'});
        },

        showUploadSuccess() {
            Notification.success({title: '', message: 'File uploaded successfully'});
            this.fetchPayments();
            this.fetchPayment();
        },

        errorOccurred(data) {
            this.$message({
                message: 'An Error occurred when submitting the form!',
                type: 'warning'
            });
        },

        checkPageData() {
            if (!this.selectedClient.hasRE) {
                this.$router.push({
                    path: '/home'
                });
            }
        },
    },

    mounted() {
        this.fetchPayments();
        this.checkPageData();
    }
}