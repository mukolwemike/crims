import {mapGetters} from 'vuex';
import events from '../../../../events';
import moment from 'moment';

export default {
    data: function () {
        return {
            makePaymentForm: {
                slug: 'payment',
            },
            extensions: 'image/pdf',
            paymentProofFile: undefined,
            step: 'form',
            errors: [],
            submitting: false,
            visible: false,

            rules: {
                amount: [
                    {
                        required: true, message: 'Please input valid value for amount', trigger: 'change'
                    }
                ],

                description: [
                    {
                        required: true, message: 'Please provide narration for payment', trigger: 'change'
                    }
                ],

                schedule_id: [
                    {
                        required: true, message: 'Please select schedule for payment', trigger: 'change'
                    }
                ],

                date: [
                    {
                        required: true, message: 'Please input a valid date value', trigger: 'change'
                    }
                ],

                mode_of_payment: [
                    {
                        required: true, message: 'Payment mode is not defined', trigger: 'change'
                    }
                ],
            }
        }
    },

    watch: {
        submitPaymentSuccess: {
            handler: function () {
                let vm = this;
                this.step = 'success';
                events.bus.$emit('update_title', 'Make-Payment was Successful');
                events.bus.$emit('close_drawer');
            }
        },

        formErrors() {
            this.errors = this.formErrors;
        },

        "makePaymentForm.schedule_id": {
            handler: function () {
                let schedule = this.schedules.find((schedule) => {
                    return schedule.id === this.makePaymentForm.schedule_id;
                });

                this.makePaymentForm.amount = schedule ? parseFloat(schedule.total_amount) : 0;
            }
        }
    },

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient',
            defaultHeaders: 'defaultHeaders',
            makePaymentErrors: 'makePaymentErrors',
            headers: 'defaultHeaders',
            submitPaymentSuccess: 'submitPaymentSuccess',
            submittingPayment: 'submittingPayment',
            unitDetails: 'unitDetails',
            getLoader: 'getLoader',
            schedules: 'paymentSchedules'
        }),

        url() {
            return `/api/client/make-payments/${this.selectedClient.id}`
        }
    },

    methods: {
        errorOccurred(data) {
            this.$message({
                message: 'An Error occurred when submitting the form!',
                type: 'warning'
            });
            this.submitting = false;
            events.bus.$emit('close_drawer');
        },

        showConfirm(formName) {
            this.makePaymentForm.date = moment(this.makePaymentForm.date).format('YYYY-MM-DD');
            this.makePaymentForm.clientId = this.selectedClient.id;
            this.makePaymentForm.holding_id = this.unitDetails.holding_id;
            this.makePaymentForm.type = 'holding_payment';

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    let name = 'Confirm Unit Payment';
                    this.step = 'confirm';
                    events.bus.$emit('update_title', name);

                } else {
                    this.$message({
                        message: 'Cannot submit Unit Payment details! Some form fields are missing',
                        type: 'warning'
                    });
                    return false;
                }
            });
        },

        resetForm() {
            this.makePaymentForm = {
                slug: 'payment',
            };
            this.step = 'form';
        },

        showFile(file) {
            this.paymentProofFile = file;
        },

        submit() {
            event.preventDefault();

            this.submittingPayment = true;

            if (this.$refs.upload.uploadFiles[0] && this.makePaymentForm !== {}) {

                this.$refs['upload'].submit();

                this.makePaymentForm = {}
            } else {
                this.$store.commit('SUBMIT_PAYMENT_DETAILS', this.makePaymentForm)
            }
        },

        showUploadSuccess(data) {
            this.submittingPayment = false;
            this.submitPaymentSuccess = true;
            this.step = 'success';

            events.bus.$emit('update_title', 'Unit Payment was Successful');
            events.bus.$emit('close_drawer');
        },

        close() {
            events.bus.$emit('close_drawer');
        },

        getScheduleName: function (scheduleId) {
            let s = this.schedules.filter(function (schedule) {
                return schedule.id == scheduleId
            });

            return s[0].description;
        }
    },

    created: function () {
        events.bus.$on('clear_drawer_form', () => {
            this.resetForm();
        });
    },
}