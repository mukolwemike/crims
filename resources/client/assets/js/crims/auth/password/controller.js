import {mapGetters} from 'vuex';
import {Message} from 'element-ui';
import router from '../../../router';

const PasswordController = {
    data: function () {
        return {
            form: null,
            year: new Date().getFullYear(),
            reset_form: {},
            password_confirmation: '',
            password: '',
            characters: false,
            charLength: false,
            passwordChecks: false,
            passConfirm: false,
            url: '',
            reseting: false,
            verification_option: '',
            seePass: false,
        };
    },

    computed: {
        ...mapGetters({
            verifying: 'verifyingUser',
            submitting: 'submitForgot',
            reset_success: 'pswdResetSuccess',
            foundUser: 'foundUser',
            reset_creds: 'reset_creds',
            reset_user: 'reset_user',
            requestingSms: 'requestingSms',
            requestMade: 'requestMade',
            sms_sent: 'sms_sent'
        }),

        passwordCorrect() {
            return !!(this.passwordChecks && this.passConfirm);
        },

        togglePass() {
            return this.seePass ? 'text' : 'password';
        }
    },

    watch: {
        password: 'checkPassRules',
        password_confirmation: 'confirmPass',
        verification_option: 'requestSMSPasswordReset'
    },

    methods: {
        submit() {
            this.$store.commit('FORGOT_PASSWORD', this.reset_creds)
        },

        authenticateResetPassword(event) {
            event.preventDefault();

            this.$store.commit('AUTHENTICATE_PASSWORD_RESET', this.reset_creds);
        },

        requestSMSPasswordReset() {
            this.setOption();

            this.$store.commit('REQUEST_SMS_PASSWORD_RESET', this.reset_creds);
        },

        setOption() {
            if (this.verification_option === 1) {
                this.reset_creds.option = 'email';
            } else if (this.verification_option === 2) {
                this.reset_creds.option = 'sms';
            } else {
                this.reset_creds.option = 'authy';
            }
        },

        resetPassword(event) {
            event.preventDefault();

            this.reseting = true;

            axios.post(this.url, {
                password: this.password,
                password_confirmation: this.password_confirmation
            })
                .then(({data}) => {

                    this.reseting = false;

                    Message.success({message: data.message});

                    router.push({name: 'landing'});

                    location.reload();

                }, () => {
                    this.reseting = false;
                });
        },

        checkPassRules() {

            this.checkLength();
            this.checkCharacters();

            this.passwordChecks = (this.characters && this.charLength);
        },

        confirmPass() {
            this.passConfirm = (this.password_confirmation === this.password);
        },

        checkLength() {
            let str = this.password;

            this.charLength = (str.length === 8 || str.length > 8);
        },

        checkCharacters() {
            let hasLower = this.hasLowerCase(this.password);
            let hasUpper = this.hasUpperCase(this.password);
            let hasSymbol = this.hasSymbolCharacter(this.password);

            this.characters = !!(hasLower && hasUpper && hasSymbol);
        },

        hasLowerCase(str) {
            return (/[a-z]/.test(str));
        },

        hasUpperCase(str) {
            return (/[A-Z]/.test(str));
        },

        hasSymbolCharacter(str) {
            // return (/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(str));
            return true;
        },

        closeVerifyModal() {
            this.$store.commit('CLOSE_PASS_RESET_TOKEN_MODAL', false);
        }
    }
};

export default PasswordController;