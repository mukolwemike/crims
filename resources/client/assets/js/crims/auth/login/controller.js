import {mapGetters} from 'vuex';

const LoginController = {

    data() {
        return {
            form: {},
            verification_option: 2,
            seePass: false
        }
    },

    computed: {
        ...mapGetters({
            loggingIn: 'loggingIn',
            submitting: 'submitLogin',
            userDetails: 'userDetails',
            verification_sent: 'verification_sent',
            requestingSMS: 'requestingSMS',
            creds: 'creds',
            ver_option: 'ver_option'
        }),
        togglePass() {
            if (this.seePass) {
                return 'text';
            }

            return 'password'
        }
    },

    methods: {

        verify(evt) {
            evt.preventDefault();

            this.$store.dispatch('verifyUser', this.creds);
        },

        requestSMS() {
            this.$store.commit('REQUEST_SMS', this.creds);

            this.optionSet();
        },

        optionSet() {
            this.$store.commit('SET_VERIFICATION_OPTION', this.verification_option)
        },

        authenticate(event) {

            event.preventDefault();

            this.$store.dispatch('authenticateUser', this.creds);
        },

        setOption() {
            if (this.verification_option === 1) {
                this.creds.option = 'email';
            } else if (this.verification_option === 2) {
                this.creds.option = 'sms';
            } else {
                this.creds.option = 'authy';
            }
        },

        closeTokenModal() {
            this.$store.commit('CLOSE_TOKEN_MODAL');
        }
    }
};

export default LoginController;