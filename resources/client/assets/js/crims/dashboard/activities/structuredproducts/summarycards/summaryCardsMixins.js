/**
 * Created by Raphael Karanja on 17/12/2018.
 */
import Vue from 'vue'
import {mapGetters} from 'vuex';
import event from '../../../../../events';
Vue.component('dash-card-placeholder', require('../../utils/card-loaders.vue'));
export default {
    data(){
        return {
            itemsPerPage: 2,
            hasRE: false,
            pageNumber: 0,
            topupDrawer: false
        }
    },
    computed: {
        ...mapGetters({
            currentClient: 'getCurrentFaClient',
            fetchingSammary: 'fetchingSummary',
            investments: 'investmentSummary',
            selectedClient: 'selectedClient',
            reSammary: 'reTotals',
            reDetails: 'reDetails',
            suspendCallout: 'suspendCallout',
            topupCount: 'topupCount',
            clientApprovals: 'clientApprovals'
        }),
        re() {
            if(this.investments) {
                return this.reSammary;
            }
        },
        pageCount(){
            let l = this.investments.length,
                s = this.itemsPerPage;
            return Math.floor(l/s);
        },
        investmentPaginated(){
            const start = this.pageNumber * this.itemsPerPage,
                end = start + this.itemsPerPage;
            return this.investments.slice(start, end);
        },
        paginationCount(){
            let page =parseInt(this.pageNumber)+1,
                total =  parseInt(this.pageCount)+1
            return 'Page '+page+' of '+total;
        },
        hasApprovals(){
            if(this.clientApprovals.approvals){
                if(this.clientApprovals.approvals.length > 0){
                    return true
                }
                return false;
            }
            return false
        }

    },
    watch: {
        selectedClient: {
            handler: function () {
                this.getInvestmentSammary();
            }
        },

        reSammary: {
            handler: function () {
                this.hasRE = this.reSammary.investments > 0;
            }
        },

        investments(){
            this.getWindowWidth();
        }
    },
    methods: {
        investmentClass(item){

            let index = this.investments.indexOf(item);
            let result = index / 2;
            let investment_class = '';

            let class_green = index + 2;
            let class_blue = index + 1;
            let class_grey = index;

            if (class_grey % 3 == 0) {
                investment_class = 'investment-grey';
            }else if (class_green % 3 == 0) {
                investment_class = 'investment-green';
            } else if (class_blue % 3 == 0) {
                investment_class = 'investment-blue';
            }

            return investment_class;
        },
        getWindowWidth(event) {
            this.windowWidth = document.documentElement.clientWidth;
            if (this.windowWidth < 1310 && this.windowWidth > 1270) {
                this.itemsPerPage = 3;
                this.hasApprovals ? this.itemsPerPage = 2 : this.itemsPerPage =3;
            }else if(this.windowWidth < 1270 && this.windowWidth > 615){
                this.itemsPerPage =2;
                this.hasApprovals ? this.itemsPerPage = 1 : this.itemsPerPage =2;
            }else if(this.windowWidth < 615){
                this.itemsPerPage = 1;
            }else {
                this.itemsPerPage = 3;
                this.hasApprovals ? this.itemsPerPage = 2 : this.itemsPerPage =3;
            }
            this.pageNumber = 0;
        },
        viewProduct(product){
            this.$store.commit('PASS_PRODUCT', product);
            this.$router.push({
                name: 'investments.single_product',
                params: { id: product.product_id, client_id: this.selectedClient.id  }

            });
        },
        viewRE(){
            this.$router.push({name:'realestate'});
        },
        getInvestmentSammary(){
            let client = this.selectedClient;
            if (client.id && this.investments.length === 0){
                this.$store.commit('GET_INVESTMENT_SAMMARY', client);
            }
        },
        newInvestment() {
            // this.$router.push({name: 'investments.TopUPList'})

            if(this.topupCount > 0){
                this.$router.push({name: 'investments.TopUPList'})
            }else{
                this.topupDrawer = true;
            }

        },
        getTopups(){
            this.$store.commit('GET_TOPUPS', this.selectedClient.id);
        },
        nextPage(){
            this.pageNumber++;
        },
        prevPage(){
            this.pageNumber--;
        },



    },
    mounted(){
        this.$nextTick(function() {
            window.addEventListener('resize', this.getWindowWidth);
            this.getWindowWidth()
        });

        this.getInvestmentSammary();
        this.getTopups()

    },

    created() {

        let vm = this;
        event.bus.$on('client_switched', function () {
            vm.getTopups();
            let client = vm.selectedClient;
            vm.$store.commit('GET_INVESTMENT_SAMMARY', client);
        });
    }
}
