/**
 * Created by Raphael Karanja on 2018-12-18.
 */
import {mapGetters} from 'vuex';
import Vue from 'vue';
import events from "../../../events";

Vue.component('approve-transaction', require('./partials/Approval'));

export default {
    data: () => ({
        approvalData: {},
        approvalUser: {},
        approve_status: 0,
        approve_drawer: false
    }),
    computed: {
        ...mapGetters({
            clientApprovals: 'clientApprovals',
            selectedClient: 'selectedClient',
            fetchingApprovals: 'fetchingPendingApprovals'
        }),
        approvals() {
            if (this.clientApprovals.approvals) {
                return this.clientApprovals.approvals
            }
            return []
        }
    },
    methods: {
        handleApprove(rowData, signator) {
            events.bus.$emit('update_title', 'Confirm Approval');
            this.approvalData = rowData;
            this.approvalUser = signator;
            this.approve_drawer = true;
            this.approve_status = 1;

        },
        handleReject(rowData, signator) {
            this.approvalData = rowData;
            this.approvalUser = signator;
            this.approve_drawer = true;
            this.approve_status = 2;
            events.bus.$emit('update_title', 'Reject Transaction');
        },
        getApprovals() {
            if (this.approvals.length > 0) {
            } else {
                this.$store.commit('CLIENT_APPROVALS', this.selectedClient.id);
            }
        }

    },
    created() {
        this.getApprovals();
        events.bus.$on('update_approvals', ()=>{
            this.$store.commit('CLIENT_APPROVALS', this.selectedClient.id);
        })
    }
}