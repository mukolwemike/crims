/**
 * Created by Raphael Karanja on 2018-12-18.
 */
export default {

    data() {
        var validateReason =  (rule, value, callback) => {
            if(this.approve_status === 2) {
                if (!value || value === '') {
                    callback(new Error('Please add a reason before proceeding'));
                }else {
                    callback();
                }
            }else {
                callback();
            }
        };
        return {
            rules:{
                reason:[
                    {
                        validator: validateReason, trigger: 'change'
                    }
                ]
            }
        }
    }
}