/**
 * Created by Raphael Karanja on 2018-12-18.
 */
import {mapGettersa} from "vuex";
import axios from 'axios';
import ReasonValidatorMixin from './ApprovalFormValidationMixin'
import {Message} from "element-ui";
import events from "../../../../../events";

export default {
    props: ['approver', 'transaction', 'approve_status'],
    mixins: [ReasonValidatorMixin],
    data: () => ({
        submitting: false,
        approveForm: {},
        step: 'confirm'
    }),
    methods: {
        submit() {
            let formName = 'approveForm';
            this.approveForm.approval_id = this.approver.id;
            this.approveForm.value = this.approve_status;

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    this.submitting = true;
                    axios.post('/api/approvals/approve', this.approveForm)
                        .then((response) => {
                            this.submitting = false;

                            if(response.status === 200){
                                this.step = 'success';
                                events.bus.$emit('update_title', 'Approval was Sent');
                                setTimeout(()=>{
                                    events.bus.$emit('close_drawer');
                                    events.bus.$emit('update_approvals');
                                }, 800)
                            }else{
                                Message.warning({message: 'Error Occurred while submitting Aproval details, try again in a moment'});
                            }

                        }, () => {
                            this.submitting = false;
                            Message.warning({message: 'Error Occurred while submitting Aproval details, try again in a moment'});
                        })
                }else {
                    return false;
                    Message.warning({message: 'Some inputs are empty'});
                }
            });


        },
        cancel() {
            events.bus.$emit('close_drawer');
        },

        resetForm() {
            let formName = 'approveForm';
            this.step = 'confirm';
            this.approveForm = {};
            if (this.$refs[formName]) {
                this.$refs[formName].resetFields();
            }
        }
    },

    created(){
        events.bus.$on('close_drawer', ()=>{
            setTimeout(()=>{
                this.resetForm();
            }, 2000)
        })
    }

}