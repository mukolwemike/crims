/**
 * Created by Kuku Mike on 16/09/2019.
 */
import {mapGetters} from 'vuex';
import events from '../../../events';

export default {
    data: () => ({
        itemsPerPage: 3,
        pageNumber: 0,
    }),
    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient',
            pointsSummaries: 'loyaltyPointsDetails',
            fetchingPointsSummary: 'fetchingPointsSummary',
        }),
        pageCount() {
            let l = this.pointsSummaries.length,
                s = this.itemsPerPage;
            return Math.floor(l / s);
        },
        summaryPaginated() {
            let start = this.pageNumber * this.itemsPerPage,
                end = start + this.itemsPerPage;
            return this.pointsSummaries.slice(start, end);
        },
        placeholderCards() {
            let l = this.pointsSummaries.length,
                s = this.itemsPerPage,
                n = 0,
                c = [];

            if (l < s) {
                n = s - l;

                for (var i = 1; i <= n; i++) {
                    c.push({html: `<div class="crims_fund_card_holder"></div>`});
                }

                return c;
            }
        },
        paginationCount() {
            let page = parseInt(this.pageNumber) + 1,
                total = this.pageCount;
            return 'Page ' + page + ' of ' + total;
        },
    },
    watch: {
        selectedClient: {
            handler: function () {
                this.fetchSummaries();
            }
        },
        pointsSummaries() {
            this.getWindowWidth();
        }
    },
    methods: {
        viewPointsDetails() {
            this.$router.push({name: 'loyalty_points:details'});
        },
        fetchSummaries() {
            if (this.pointsSummaries.length === 0) {
                this.$store.commit('FETCH_LOYALTY_POINTS_SUMMARY', this.selectedClient.id)
            }
        },

        getWindowWidth(event) {
            this.windowWidth = document.documentElement.clientWidth;
            if (this.windowWidth < 1310 && this.windowWidth > 1270) {
                this.itemsPerPage = 3;
                this.hasApprovals ? this.itemsPerPage = 2 : this.itemsPerPage = 3;
            } else if (this.windowWidth < 1270 && this.windowWidth > 615) {
                this.itemsPerPage = 2;
                this.hasApprovals ? this.itemsPerPage = 1 : this.itemsPerPage = 2;
            } else if (this.windowWidth < 615) {
                this.itemsPerPage = 1;
            } else {
                this.itemsPerPage = 3;
                this.hasApprovals ? this.itemsPerPage = 2 : this.itemsPerPage = 3;
            }
            this.pageNumber = 0;
        },
        nextPage() {
            this.pageNumber++;
        },
        prevPage() {
            this.pageNumber--;
        }
    },
    created() {
        events.bus.$on('client_switched', () => {
            this.$store.commit('FETCH_LOYALTY_POINTS_SUMMARY', this.selectedClient.id)
        })
    },

    mounted() {
        this.fetchSummaries();
        this.$nextTick(function () {
            window.addEventListener('resize', this.getWindowWidth);
            this.getWindowWidth()
        });
    }
}
