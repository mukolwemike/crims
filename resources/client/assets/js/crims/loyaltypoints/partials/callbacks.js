export default {
    methods: {
        redeemOrigin(value) {
            let out = `<span class="alert alert-info"></span>`;

            if (value === 0) {
                out = `<span class="alert alert-info">Admin</span>`;
            } else {
                out = `<span class="alert alert-info">Client</span>`;
            }
            return out;
        },

        redeemStatus(value) {
            let out = `<span class="alert alert-success"></span>`;

            if (value === 1 || value === true) {
                out = `<span class="alert alert-success">Redeemed</span>`;
            } else {
                out = `<span class="alert alert-warning">Pending</span>`;
            }
            return out;
        },
    }
}