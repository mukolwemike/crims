import {mapGetters} from 'vuex';
import events from "../../../events";
import axios from 'axios';
import {Message} from 'element-ui';

export default {
    props: ['pointsDetail', 'vouchers'],

    data: () => ({
        redeemPointsForm: {},
        step: 'form',
        errors: [],
        formErrors: [],
        submittingForm: false,
    }),

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient',
            currentUser: 'currentUser',
        }),

        canRedeemPoints() {
            if(this.selectedVoucher)
            {
                return this.redeemPointsForm.voucher_id
                    && this.redeemPointsForm.voucher_id !== ''
                    && this.selectedVoucher.value <= parseFloat(this.pointsDetail.availablePoints);
            }else{
                return false;
            }
        },

        selectedVoucher() {
            if (this.redeemPointsForm.voucher_id && this.redeemPointsForm.voucher_id !== '') {
                let voucher = this.vouchers.filter(voucher => voucher.id == this.redeemPointsForm.voucher_id);

                if (voucher.length > 0) {
                    return voucher[0]
                }
            }
        }
    },
    methods: {
        resetForm() {
            this.redeemPointsForm = {};
            this.step = 'form';
            this.errors = [];
            events.bus.$emit('update_title', 'Redeem Loyalty Points');
        },

        confirmRedeemPoints() {
            if (this.redeemPointsForm.voucher_id) {
                this.step = 'confirm';
                events.bus.$emit('update_title', 'Confirm Loyalty Redeem Details');
            } else {
                this.$message({
                    message: 'Please fill in all the form fields before proceeding!!',
                    type: 'warning'
                });
            }
        },

        verifyRedeem() {
            this.redeemPointsForm.client_id = this.selectedClient.id;
            this.redeemPointsForm.user_id = this.currentUser.id;

            this.submittingForm = true;

            axios.post('/api/clients/loyalty/redeem/voucher/', this.redeemPointsForm)
                .then(({data}) => {
                    this.submittingForm = false;

                    if (data.status !== 422) {
                        this.step = 'success';
                        events.bus.$emit('update_title', 'Redeem Points instruction successfully saved for approval');
                        events.bus.$emit('close_drawer');
                        events.bus.$emit('refresh_instructions');

                        // location.reload();
                    } else {
                        this.errors = data.errors;
                        Message.warning(data.errors);
                        this.step = 'form';
                        events.bus.$emit('update_title', 'Redeem Loyalty Points (Form Error!!)');
                    }
                }, ({data}) => {
                    Message.warning(data.errors);
                    this.submittingForm = false;
                })
        },

        cancel() {
            events.bus.$emit('close_drawer');
            this.resetForm();
        }
    },

    created() {
        events.bus.$on('clear_drawer_form', () => {
            this.resetForm();
        })
    },
}