import {mapGetters} from 'vuex';
import DashCardPlaceholder from '../../utils/_single_card_loader';
import Vue from 'vue';

Vue.component('add-bill', require('./actions/add-bill'));
Vue.component('buy-airtime', require('./actions/buy-airtime'));
Vue.component('manage-bill', require('./actions/manage-bill'));

export default {
    components: {DashCardPlaceholder},

    data() {
        return {
            add_bill: false,
            buy_airtime: false,
            edit_bill: false,
            bill: {},
            airtimeType: {},
            bill_action: ''
        }
    },

    computed: {
        ...mapGetters({
            allBills: 'allBills',
            clientBills: 'clientBills',
            airtimeTypes: 'airtimeTypes',
            sourceFunds: 'sourceFunds',
            selectedClient: 'selectedClient',
            fetchingAllBills: 'fetchingAllBills',
            fetchingClientBills: 'fetchingClientBills',
            fetchingAirtimeTypes: 'fetchingAirtimeTypes'
        }),
    },

    watch: {
        selectedClient: {
            handler: function () {
                this.getClientBills();
            }
        }
    },

    methods: {
        updateData() {
            this.getAllBills();
            this.getClientBills();
            this.getAirtimeTypes();
        },
        addBill(bill) {
            this.bill = bill;
            this.add_bill = true;
        },
        buyAirtime(type) {
            this.airtimeType = type;
            this.buy_airtime = true;
        },
        editDeleteBill(bill, action) {
            this.bill = bill;
            this.bill_action = action;
            this.edit_bill = true;
        },
        getAllBills() {
            if (this.allBills.length === 0) {
                this.$store.commit('GET_ALL_UTILITY_BILLS');
            }
        },
        getClientBills() {
            this.$store.commit('GET_CLIENT_UTILITY_BILLS', this.selectedClient.id);
        },
        getAirtimeTypes() {
            if (this.airtimeTypes.length === 0) {
                this.$store.commit('GET_ALL_AIRTIME_TYPES');
            }
        },
        card_logo(logo) {
            if (logo === 'dstv-logo') {
                return 'dstv_back';
            } else if (logo === 'zuku-logo') {
                return 'zuku_back';
            } else if (logo === 'kenya-power') {
                return 'power_back';
            } else if (logo === 'nairobi-water-logo') {
                return 'water_back';
            } else if (logo === 'airtel-logo') {
                return 'airtel_back';
            } else if (logo === 'safaricom-logo') {
                return 'safaricom_back';
            } else if (logo === 'telkom-logo') {
                return 'telkom_back';
            } else if (logo === 'gotv-logo') {
                return 'gotv-back';
            } else if (logo === 'zuku-internet-logo') {
                return 'zuku-internet-back';
            } else if (logo === 'startimes-logo') {
                return 'startimes-back';
            }
        },
        fetchSourceFunds() {
            if (this.sourceFunds.length === 0) {
                this.$store.commit('GET_SOURCE_FUNDS');
            }
        },
        kplcLogo(slug) {
            if (slug) {
                return slug.split("_")[0] === 'kplc';
            }
        },
        zukuLogo(slug) {
            if (slug) {
                return slug.split("_")[0] === 'zuku';
            }
        },
        billName(name) {
            if (name) {
                return name.split(" ")[1];
            }
        }
    },

    mounted() {
        this.fetchSourceFunds();
        this.getAllBills();
        this.getClientBills();
        this.getAirtimeTypes();
    }
}