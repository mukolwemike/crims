import {mapGetters} from 'vuex';
import {Message} from "element-ui";
import axios from 'axios';
import VueTelInput from 'vue-tel-input';

export default {
    data() {
        return {
            bank: '',
            branch: '',
            other_bank: '',
            other_branch: '',
            enter_other_bank: false,
            account_name: '',
            account_number: '',
            currency: '',
            formType: '',
            step: 'form',
            inputToken: false,
            sendingToken: false,
            authenticating: false,
            editUpload: false,
            ver_option: 2,
            form: {},
            accountActiveName: 'bank',
            accId: null,
            mpesaForm: false,
            countries: []
        }
    },

    components: {
        VueTelInput,
    },

    computed: {
        ...mapGetters({
            accountBank: 'accountBank',
            accountBranch: 'accountBranch',
            accountDetailsForm: 'accountDetailsForm',
            selectedClient: 'selectedClient',
            banks: 'bankAccounts',
            branches: 'branches',
            fetchingBranches: 'fetchingBranches',
            bankCurrencies: 'bankCurrencies',
            errors: 'errors',
            savingAccountDetails: 'savingAccountDetails',
            client: 'currentUser',
            deletingAccount: 'deletingAccountDetails',
            headers: 'defaultHeaders',
        }),

        url() {
            return `/api/client/profile/` + this.selectedClient.id + `/account-details`;
        },
    },

    watch: {
        accountBank: {
            handler: function () {
                if (this.accountBank) {
                    this.bank = this.accountBank;
                }
            }
        },
        accountBranch: {
            handler: function () {
                if (this.accountBranch) {
                    this.branch = this.accountBranch;
                }
            }
        },
        bank: {
            handler: function () {
                if (this.bank === 'other') {
                    this.enter_other_bank = true;
                } else {
                    this.accountDetailsForm.bank_id = this.bank;

                    this.getBankBranch();
                    this.enter_other_bank = false;
                }
            }
        },
        other_bank: {
            handler: function () {
                if (this.bank === 'other') {
                    this.accountDetailsForm.bank_id = this.other_bank;
                }
            }
        },
        branch: {
            handler: function () {
                this.accountDetailsForm.branch_id = this.branch;
            }
        },
    },

    methods: {
        getBanks: function () {
            this.$store.commit('GET_BANK_LIST');
        },

        getBankBranch() {
            this.$store.commit('FETCH_BANK_BRANCHES', this.bank);
        },

        getCurrencies() {
            this.$store.commit('FETCH_CURRENCIES');
        },

        confirmRequest() {
            if (this.mpesaForm) {
                this.accountDetailsForm.formType = 'mpesa';
            } else {
                this.accountDetailsForm.formType = 'bank';
            }

            if (!this.validateForm()) {
                Message.error('Please fill all required details before submitting...')
            } else {
                this.step = 'verify';
            }
        },

        viewMpesa() {
            this.mpesaForm = !this.mpesaForm;
        },

        deleteAccount() {
            let form = {
                'accId': this.accId,
                'client_id': this.selectedClient.id
            };

            this.$store.commit('DELETE_ACCOUNT_DETAILS', form);
        },

        saveAccountDetails() {
            this.accountDetailsForm.client_id = this.selectedClient.id;

            if (this.$refs.upload.uploadFiles[0] && this.accountDetailsForm !== {}) {
                this.$refs.upload.submit();
            } else {
                this.$store.commit('SAVE_ACCOUNT_DETAILS', this.accountDetailsForm);
            }
        },

        showFile(file) {
            this.filename = file;
        },

        errorOccurred(data) {
            this.savingAccountDetails = false;
            this.errors = {};
            Message.error('An Error occurred when submitting the form!');
            this.step = 'form';
        },

        validateForm() {
            if (this.accountDetailsForm.formType === 'bank') {
                if (this.accountDetailsForm.currency_id == null) {
                    return false;
                } else if (this.accountDetailsForm.bank_id == null) {
                    return false;
                } else if (this.accountDetailsForm.branch_id == null) {
                    return false;
                } else if (!this.accountDetailsForm.hasOwnProperty('investor_account_name')) {
                    return false;
                } else return this.accountDetailsForm.hasOwnProperty('investor_account_number');
            } else if (this.accountDetailsForm.formType === 'mpesa' && !this.accId) {
                return this.accountDetailsForm.hasOwnProperty('phone_number') && this.accountDetailsForm.country_id != null;
            } else {
                return true;
            }
        },

        requestToken() {
            this.sendingToken = true;
            this.setOption();

            axios.post('/api/verify/transaction/token', this.form).then(
                ({data}) => {
                    if (data.verified) {
                        this.sendingToken = false;
                        this.inputToken = true;
                        Message.success('Token request successfully done. Enter Code to proceed');
                    } else {
                        this.sendingToken = false;
                        Message.error('Token request was unsuccessful. Retry or confirm you contact information')
                    }
                },
            );
        },

        setOption() {
            this.form.userID = this.client.id;

            if (this.ver_option === 1) {
                this.form.option = 'email';
            } else if (this.ver_option === 2) {
                this.form.option = 'sms';
            } else {
                this.form.option = 'authy';
            }
        },

        authenticate(event) {
            event.preventDefault();

            this.authenticating = true;

            axios.post('/api/verify/transaction/validate', this.form).then(
                ({data}) => {
                    if (data.validated) {
                        this.authenticating = false;
                        Message.info('User Transaction validated successfully. Now Saving...');
                        this.saveAccountDetails();
                    } else {
                        this.authenticating = false;
                        Message.error('User Transaction not validated successfully. Try Again')
                    }
                }
            );
        },

        getCountries() {
            axios.get('/api/countries').then(
                ({data}) => {
                    this.countries = data.countries;
                }
            );
        },

        showUploadSuccess(data) {
        },

        fetchAccountDetails() {
            if (this.$route.params.accId) {
                this.accId = this.$route.params.accId;

                this.$store.commit('FETCH_ACCOUNT_DETAILS', this.accId);
            }
        }
    },

    mounted() {
        this.getBanks();
        this.getBanks();
        this.getCurrencies();
        this.fetchAccountDetails();
        this.getCountries();
    }
}