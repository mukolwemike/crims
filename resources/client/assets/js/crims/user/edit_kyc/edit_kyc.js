import {mapGetters} from 'vuex';
import {Message} from 'element-ui';
import axios from 'axios';

export default {
    data() {
        return {
            editClientKyc: {},
            form: {
                docsUploaded: [],
                jointDetails: []
            },
            viewJoint: false,
            uploadedDocs: [],
            pendingKycDocs: [],
            fetchingPendingKyc: false,
            submitting: false,
            url: '/api/client/kyc/documents',
            extensions: 'image/*, application/pdf',
            errors: [],
            kycUploadData: {},

            idJointData: {slug: 'id_or_passport', jointId: ''},
            pinJointData: {slug: "pin_individual", jointId: ''},
        }
    },

    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient',
            defaultHeaders: 'defaultHeaders',
        }),
    },

    watch: {
        selectedClient() {
            if (this.selectedClient.kycValidated) {
                Message.success('Selected User already has provided validated KYC Documents.');

                this.$router.push({
                    name: 'profile.details'
                })
            }

            this.editClientKyc = this.selectedClient;
            this.pendingKYC();
        }
    },

    methods: {
        dataToUpload(slug, joint) {
            let data = {};
            if (joint) {
                switch (slug) {
                    case 'id_or_passport':
                        data.slug = slug;
                        data.jointId = joint.id;
                        data.number = joint.id_or_passport;
                        break;
                    case 'pin_individual':
                        data.slug = slug;
                        data.jointId = joint.id;
                        data.number = joint.pin_no;
                        break;
                    default:
                        break;
                }

            } else {
                switch (slug) {
                    case 'id_or_passport':
                        data.number = this.editClientKyc.id_or_passport;
                        data.slug = slug;
                        break;
                    case 'pin_individual':
                        data.number = this.editClientKyc.tax_pin;
                        data.slug = slug;
                        break;
                    case 'pin_corporate':
                        data.number = this.editClientKyc.tax_pin;
                        data.slug = slug;
                        break;
                    case 'address_individual':
                        data.slug = slug;
                        break;
                    case 'banking_individual':
                        data.slug = slug;
                        break;
                    case 'founding_docs':
                        data.slug = slug;
                        break;
                    case 'board_res':
                        data.slug = slug;
                        break;
                    case 'banking_corporate':
                        data.slug = slug;
                        break;
                    case 'authorized_ids':
                        data.slug = slug;
                        break;
                    default:
                        break;
                }
            }

            return data;
        },

        uploadJointId(joint) {
            this.idJointData.number = joint.id_or_passport;
            this.idJointData.jointId = joint.id;
        },

        pinJointIndividual(joint) {
            this.pinJointData.number = joint.pin_no;
            this.pinJointData.jointId = joint.id;
        },

        errorOnUpload() {
            Message.warning('Upload unsuccessful please try again.');
        },

        showUploadSuccess(response) {
            this.form.docsUploaded.push(response.data);
            this.uploadedDocs.push(
                response.data.jointId
                    ? ((response.data.jointId && response.data.slug === 'pin_individual') ? 'joint_pin_no' : 'joint_id_or_passport')
                    : response.data.slug
            );

            Message.success('Kyc Document Uploaded successfully');
        },

        updateJoint() {
            this.viewJoint = !this.viewJoint;
        },

        saveEdit() {
            event.preventDefault();

            this.submitting = true;

            this.setFormDetails();

            axios.post('/api/client/profile/edit-kyc', this.form).then(
                ({data}) => {
                    this.submitting = false;

                    if (data.status === 404) {
                        Message.error('Please provide KYC Document Uploads to continue.');
                    } else {
                        Message.success('Edit Kyc Details successfully saved for Approval.');

                        this.$router.push({ name: 'profile.details'});
                    }
                },
                () => {
                    this.submitting = false;

                    Message.error('Kyc Uploads unsuccessfully saved for Approval');
                }
            );
        },

        setFormDetails() {
            this.form.client_uuid = this.selectedClient.id;

            this.form.client_type = this.selectedClient.clientType;

            this.form.id_or_passport = this.editClientKyc.id_or_passport ? this.editClientKyc.id_or_passport : null;

            this.form.pin_no = this.editClientKyc.tax_pin ? this.editClientKyc.tax_pin : null;

            this.editClientKyc.joint ? this.form.jointDetails = this.editClientKyc.jointHolders : null
        },

        checkSlug(slug) {
            return this.uploadedDocs.includes(slug);
        },

        firstItem(id) {
            return this.pendingKycDocs.findIndex((item) => item.id === id) === 0;
        },

        pendingKYC() {
            this.fetchingPendingKyc = true;

            axios.get('/api/client/profile/' + this.selectedClient.id + '/pending-kyc').then(
                ({data}) => {
                    this.pendingKycDocs = data.data;

                    this.fetchingPendingKyc = false;
                }
            )
        },

        toggleUploadBlocks() {

            let expander = $('.expander');
            let uploadBlock = $('.upload-block');

            uploadBlock.hide();

            expander.children('.expander-icon').addClass('fa-chevron-down');

            expander.click(function () {

                $(this).next().slideToggle(400);

                $(this).parent().siblings().find(uploadBlock).slideUp();

            });

            uploadBlock.slideUp(400);

            $('.identity-block').children(uploadBlock).slideDown(400);

            $('.founding-block').children(uploadBlock).slideDown(400);
        },

    },

    mounted() {
        if (this.selectedClient.kycValidated) {
            Message.success('Selected User already has provided validated KYC Documents.');

            this.$router.push({ name: 'profile.details'});
        }

        this.editClientKyc = this.selectedClient;
        this.toggleUploadBlocks();
        this.pendingKYC();
    }
}