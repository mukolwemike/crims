import {mapGetters} from 'vuex';

export default {
    computed: {
        ...mapGetters({
            selectedClient: 'selectedClient',
        })
    },

    methods:{
        addAccountDetail(accId=null)
        {
            this.$router.push({name: 'profile.client_account_details', params:{accId: accId}})
        }
    }
}