import {mapGetters} from 'vuex';
export default {
    data: ()=>({
        editClient: {
            residence: ''
        },
        rules: {
            residence: [
                {
                    required: true, message: 'Please input Residential area Details', trigger: 'blur'
                }
            ],
            phone: [
                {
                    required: true, message: 'Please input Phone Number', trigger: 'blur'
                }
            ],
            email: [
                {
                    required: true, message: 'Please input Email Address', trigger: 'blur'
                }
            ],
            employment_id: [
                {
                    required: true, message: 'Please input Employment status', trigger: 'blur'
                }
            ],
            present_occupation: [
                {
                    required: true, message: 'Please input Occupation Details', trigger: 'blur'
                }
            ],

        },
        submitting: false
    }),
    computed: {
        ...mapGetters({
            titles: 'titlesList',
            employmentTypes: 'employmentTypesList',
            selectedClient: 'selectedClient',
        })
    },
    watch: {
        selectedClient(){
            this.editClient = this.selectedClient;
        }
    },
    methods: {
        saveEdit(formName){
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    this.sendEditDetails()
                } else {
                    this.$message({
                        message: 'Cannot send edit details! Some form fields are missing',
                        type: 'warning'
                    });
                    return false;
                }
            });
        },
        sendEditDetails(){
            let details = {
                client_uuid: this.selectedClient.id,
                email: this.editClient.email,
                phone: this.editClient.phone,
                residence: this.editClient.residence,
                employment: this.editClient.employment,
                sector: this.editClient.sector,
                occupation: this.editClient.occupation,
                employer: this.editClient.employer,
                employer_address: this.editClient.employer_address
            };
            this.submitting = true;
            this.$http.post('/api/client/profile/edit', details)
                .then(({data})=>{
                    this.$message({
                        message: 'Edit information sent.',
                        type: 'success'
                    });
                    this.$router.push({
                        name: 'profile.edit_sent'
                    });
                    this.submitting = false

                }, ({error})=>{
                    this.$message({
                        message: 'An Error occurred. ' + error,
                        type: 'warning'
                    });
                    this.submitting = false

                })
        },
        getTitlesList () {
            if(this.titles.length == 0) {
                this.$store.commit('GET_TITLES_LIST');
            }
        },
        getEmploymentTypesList () {
            if(this.employmentTypes.length == 0) {
                this.$store.commit('GET_EMPLOYMENT_TYPES_LIST');
            }
        }
    },
    mounted(){
        this.getTitlesList();
        this.getEmploymentTypesList();
        this.editClient = this.selectedClient;
    }
}