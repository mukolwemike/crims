import { mapGetters } from 'vuex';

export default {
    data: () => ({
        notifications_display: false
    }),

    computed: {
        ...mapGetters({
            notifications: 'clientNotifications',
            selectedClient: 'selectedClient'
        }),

        unreadMessages() {
            return this.notifications.filter(notification => notification.status == 'unread').length;
        },
    },

    watch: {
        selectedClient: {
            handler: function () {
                this.getNotifications();
            }
        }
    },

    methods: {

        getNotifications: function () {
            this.$store.commit('GET_CLIENT_NOTIFICATIONS', this.selectedClient.id);
        },

        getNotified() {
            console.log("fetching notifications gain");
            this.$store.commit('GET_CLIENT_NOTIFICATIONS');
        },

        showNotifications() {
            this.notifications_display = !this.notifications_display
        },

        view: function (notification) {

            this.notifications_display = false;

            if(notification.status === 'unread' && !notification.approvals) {
                this.markAsRead(notification);
                return;
            }
            console.log(notification);
            this.$router.push({ path: notification.url });
        },

        markAsRead: function (notification) {
            this.$store.commit('UPDATE_CLIENT_NOTIFICATION', {
                notification: notification,
                client_uuid: this.selectedClient.id
            });
        }
    },

    mounted() {
        this.getNotifications();
    }
}