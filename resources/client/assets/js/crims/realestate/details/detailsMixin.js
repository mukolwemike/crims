import DashboardBanner from '../partials/banner';
import {mapGetters} from 'vuex';
import DashCardPlaceholder from '../../utils/_single_card_loader';

export default {
    data(){
        return {
            activeName: 'real_estate'
        }
    },

    components: {DashboardBanner, DashCardPlaceholder},

    computed: {
        ...mapGetters({
            fetchingProjects: 'fetchingREProjects',
            reProjects: 're_projects',
            paymentPlans: 're_paymentPlans'
        }),
        project(){
            let project_id = this.$route.params.id;
            let reProjects = this.reProjects.filter(data => data.id === project_id);
            if(reProjects.length > 0){
                return reProjects[0]
            }
            return {}
        }
    },
    methods: {
        getData() {
            if (this.reProjects.length === 0) {
                this.$store.commit('GET_RE_PROJECTS');
            }
        },


        getPaymentPlan() {
            if(this.paymentPlans.length === 0){
                this.$store.commit('GET_RE_PAYMENT_PLANS');
            }
        },

        book(url) {
            window.open(url, "_blank");
        },

        reserve(url) {
            window.open(url, "_blank");
        },

        project_color(name) {
            if (name === 'The Amara Ridge') {
                return 'amara-card';
            } else if (name === 'The Alma') {
                return 'alma-card';
            } else if (name === 'Situ Village') {
                return 'situ-card';
            } else if (name === 'Taraji Heights') {
                return 'taraji-card';
            } else if (name === 'The Ridge') {
                return 'ridge-card';
            } else if (name === 'RiverRun Estates') {
                return 'riverrun-card';
            } else if (name === 'Newtown') {
                return 'newtown-card';
            } else if (name === 'Cytonn Towers') {
                return 'towers-card';
            } else if (name === 'Applewood') {
                return 'applewood-card';
            }
        },

        project_logo(name) {
            if (name === 'The Amara Ridge') {
                return 'amara-back';
            } else if (name === 'The Alma') {
                return 'alma-back';
            } else if (name === 'Situ Village') {
                return 'situ-back';
            } else if (name === 'Taraji Heights') {
                return 'taraji-back';
            } else if (name === 'The Ridge') {
                return 'ridge-back';
            } else if (name === 'RiverRun Estates') {
                return 'riverrun-back';
            } else if (name === 'Newtown') {
                return 'newtown-back';
            } else if (name === 'Cytonn Towers') {
                return 'towers-back';
            } else if (name === 'Applewood') {
                return 'applewood-back';
            }
        },

        viewDetails(id) {
            this.$router.push({name: 'real_estate.projectDetails', params: {id: id}})
        }
    },

    mounted() {
        this.getData();
        this.getPaymentPlan();
    }
}