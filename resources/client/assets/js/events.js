/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 06/04/2017.
 * Project crims-client
 */

import Vue from 'vue'

const bus = new Vue();

export default {
    bus: bus
    }