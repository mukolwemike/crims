/**
 * Created by daniel on 27/01/2017.
 */

import Vue from 'vue';
import moment from 'moment';

const roundFilter = function (value, decimals) {
    value = !value ? 0 : value;

    decimals = !decimals ? 0 : decimals;

    return Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
};

const currencyFilter = function (value, currency, decimals) {
    value = parseFloat(value);
    if (!isFinite(value) || (!value && value !== 0)) {
        return '';
    }

    decimals = decimals != null ? decimals : 2;

    currency = currency != null ? currency : '';

    var digitsRE = /(\d{3})(?=\d)/g;

    var stringified = Math.abs(value).toFixed(decimals);
    var _int = decimals
        ? stringified.slice(0, -1 - decimals)
        : stringified;

    var i = _int.length % 3;

    var head = i > 0
        ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
        : '';

    var _float = decimals
        ? stringified.slice(-1 - decimals)
        : '';

    var sign = value < 0 ? '-' : '';

    return sign + currency + head +
        _int.slice(i).replace(digitsRE, '$1,') +
        _float;
};

const accountingFilter = function (val) {
    let use = Math.abs(val);

    let out = currencyFilter(use);

    if (val >= 0) {
        return out;
    }

    return '(' + out + ')';
};

const amountFilter = function (val) {
    let use = Math.abs(val);

    let out = currencyFilter(use);

    if (val >= 0) {
        return out;
    }

    return out;
};

const dateFilter = function (date) {
    return moment(date).format("MMM DD, YYYY");
};

const dateTimeFilter = function (date) {
    return moment(date).format('MMMM Do YYYY, h:mm:ss a');
};

const trimEmailFilter = function (email) {

    let hiddenEmail = "";

    for (let i = 0; i < email.length; i++) {

        if (i > 0 && (i + 2) < email.indexOf("@")) {
            hiddenEmail += "*";
        } else {
            hiddenEmail += email[i];
        }
    }

    return hiddenEmail;
};

const trimPhoneNumberFilter = function (phone) {

    let hiddenNumber = "";

    for (let i = 0; i < phone.length; i++) {

        if (i > 1 && (i + 3) < phone.length) {
            hiddenNumber += "*";
        } else {
            hiddenNumber += phone[i];
        }
    }

    return hiddenNumber;
};

const booleanIconFilter = function (value) {
    var out = `i class="text-center fa fa-close" style="color: #a94442"></i>`;

    if (value === 1 || value === true) {
        out = `<i class="text-center fa fa-check-square-o" style="color: #3c763d"></i>`;
    } else {
        out = `<i class="text-center fa fa-close" style="color: #a94442"></i>`;
    }
    return out;
};

const uppercaseFilter = function (str) {

    return str ? str.toUpperCase() : str;
};

const capitalizeFilter = function(str) {
    if (typeof str !== 'string') return '';
    return str.charAt(0).toUpperCase() + str.slice(1)
};

Vue.filter('round', roundFilter);

Vue.filter('currency', currencyFilter);

Vue.filter('amount', amountFilter);

Vue.filter('date', dateFilter);

Vue.filter('dateTime', dateTimeFilter);

Vue.filter('accounting', accountingFilter);

Vue.filter('trimEmail', trimEmailFilter);

Vue.filter('trimPhone', trimPhoneNumberFilter);

Vue.filter('booleanIcon', booleanIconFilter);

Vue.filter('uppercase', uppercaseFilter);

Vue.filter('capitalize', capitalizeFilter);

export default {
    roundFilter,
    currencyFilter,
    dateFilter,
    amountFilter,
    booleanIconFilter,
    uppercaseFilter,
    capitalizeFilter
};