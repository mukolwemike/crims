/**
 * Created by yeric on 9/4/17.
 */
import axios from 'axios';

export const getInterestRates = function (form) {
    return axios.post('/api/products/interest/rates', { product_id: form.product, tenor: form.tenor, amount: form.amount, clientId: form.client_id });
};

export const getGlobalInterestRate = function (form) {
    return axios.post('/api/products/interest-rates/global', form);
}

export const getPrevailingRate = function (id) {
    return axios.get('/api/product/prevailing-rates/'+id);
}

export const getProducts = function (type) {
    if (type === undefined) {
        type='';
    }
    return axios.get('/api/investments/products/'+type, { cache: true});
};

export const getRates = function (product) {
    return axios.post('/api/interest/rates', { id: product });
};

export const getInterestInvestment = function (form) {
    return axios.post('/api/products/interest/returns', {
        product_id: form.product,
        tenor: form.tenor,
        amount: form.amount,
        clientId: form.client_id
    });
};

export const getGoalReturns = function (form) {
    return axios.post('/api/products/goals/returns', form);
};