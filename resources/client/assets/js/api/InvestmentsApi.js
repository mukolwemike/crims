/**
 * Created by daniel on 18/01/2017.
 */
import axios from 'axios';
import Vue from 'vue';

export const getProducts = function (type) {
    if (type === undefined) {
        type = '';
    }
    return axios.get('/api/investments/products/' + type, {cache: true});
};

export const getActiveInvestments = function (data) {
    return axios.get('/api/investments/active/' + data.id, {cache: true});
};

export const myProducts = function (clientId) {
    return axios.get('/api/investments/client/' + clientId + '/products', {cache: true});
};

export const GET_TOPUPS = function (clientId) {

    return axios.get('/api/investments/' + clientId + '/pending-topups');
};

export const SUBMIT_TOPUP = function (data) {

    let client_id = data.client_id;

    return axios.post('/api/investments/' + client_id + '/topup', data);
};

export const GET_TOPUP_RATES = function (form) {
    return axios.post('/api/products/interest/rates', {
        product_id: form.product,
        tenor: form.tenor,
        amount: form.amount,
        clientId: form.client_id
    });
};

export const GET_ROLLOVER_RATES = function (form) {
    return axios.post('/api/products/interest/rates', {
        product_id: form.product,
        tenor: form.tenor,
        amount: form.amount,
        clientId: form.client_id
    });
};

export const getInvestmentRate = function (form) {
    return axios.post('/api/investment/interest-rate', form);
};

export const SUBMIT_WITHDRAW = function (data) {

    let invId = data.invId;

    Vue.delete(data, 'invId');

    return axios.post('/api/investments/' + invId + '/withdraw', data);
};

export const SUBMIT_ROLLOVER = function (data) {
    let invId = data.invId;

    Vue.delete(data, 'invId');

    return axios.post('/api/investments/' + invId + '/rollover', data);
};

export const submitRolloverForm = function (data) {
    return axios.post('/api/investments/rollover/' + data.investment_id, data);
};

export const GET_INVESTMENT = function (id) {
    return axios.get('/api/investments/' + id);
};

export const GET_TOPUP = function (topupId) {
    return axios.get('/api/investments/topup/' + topupId);
};

export const GET_CLIENTS = function (userId) {
    return axios.get('/api/fa/clients/' + userId, {cache: true});
};

export const GET_MONTH_COMMISSION = function (pData) {
    return axios.post('/api/month/commissions', pData);
};

export const myClients = function (data) {
    return axios.get('/api/fa/clients/portfolio/' + data.id);
};

export const getTotalBal = function (clientId) {
    return axios.get('/api/investments/active/' + clientId);
};

export const getBankDetails = function (id) {
    return axios.get('/api/client/details/' + id);
};

export const submitWithdrawal = function (data) {
    return axios.post('/api/investments/withdraw/' + data.investment_id, data);
};

export const getAmountSelected = function (form) {
    let date = (form.withdrawal_stage == "mature") ? 'mature' : form.withdrawal_date;
    return axios.post('/api/investment/amount/' + form.investment_id, {amount_select: form.amount_select, date: date});
};

export const getClientBankAccounts = function (clientId) {
    return axios.get('/api/client/' + clientId + '/bank-accounts');
};

export const getProjections = function (data) {
    return axios.post('/api/commission/projections', {
        start: data.range.start,
        end: data.range.end,
        faId: data.faId,
        retained: data.retained
    })
};

export const getClientInvestments = function (client_id) {
    return axios.get('/api/investments/active/' + client_id);
};

export const getInvestmentsSammary = function (client_id) {
    return axios.get('/api/client/investments/'+client_id);
};
export const getClientRecentActivities = function (client_id) {
    return axios.get('/api/client/' + client_id + '/activity-log/brief');
};

export const getUpcomingMaturities = function (client_id) {
    return axios.get('/api/client/' + client_id + '/investment/maturities');
};

export const getRealEstate = function (client_id) {
    return {};
};

export const getCurrencies = function () {
    return axios.get('/api/currencies');
};

export const getAllUnitFunds = function () {
    return axios.get('/api/unitization/unit-funds-all');
};

export const getCISUnitFunds = function () {
    return axios.get('/api/unitization/unit-funds-cis');
};

export const getUnitFundDetails = function (unitFundId) {
    return axios.get('/api/client-unitization/unit-funds/' + unitFundId);
};

export const getUnitTrustFundDetails = function (unitFundId) {
    return axios.get('/api/client-unitization/show-unit-funds/' + unitFundId);
};

export const getAgreedInterestRate = function (item) {
    return axios.post('/api/products/interest/rates', {
        product_id: item.product_id,
        tenor: item.tenor,
        amount: item.amount
    });
};

export const getAllShareEntity = function () {
    return axios.get('/api/unitization/share-entity-all');
};