import Vue from 'vue';
import axios from 'axios';

let clients = {};

clients.get = function (id) {
    if (id) {
        return Vue.http.get('/api/client/'+id);
    }
    return {};
};

export default clients;

export const getClientNotification = function (id) {
    return axios.get('/api/notifications/'+id);
};

export const updateClientNotification = function (id) {
    return axios.get('/api/notifications/'+id+'/update');
};

export const saveAccountDetails = function (form) {
  return axios.post(`/api/client/profile/${form.client_id}/account-details`, form);
};

export const getAccountDetails = function (accId) {
    return axios.get(`/api/client/profile/account-details/${accId}`);
};

export const deleteAccountDetails =function (form) {
    return axios.get(`/api/client/${form.client_id}/profile/account-details/${form.accId}/remove`);
};