
/**
 * Created by Raphael Karanja on 06/09/2018.
 */

import axios from 'axios';
import Vue from 'vue';

export const getFundDetails = function (details) {
    return axios.get('/api/unitization/client-unit-funds/'+details.fund_id+'/details/'+details.client_id);
};

export const getFundPurchases = function (details) {
    return axios.get('/api/unitization/unit-funds/'+details.fund_id+'/unit-fund-clients/'+details.client_id+'/unit-fund-purchases');
};

export const getFundSales = function (details) {
    return axios.get('/api/unitization/unit-funds/'+details.fund_id+'/unit-fund-clients/'+details.client_id+'/unit-fund-sales');
};

export const getFundTransfers = function (details) {
    return axios.get('/api/unitization/unit-funds/'+details.fund_id+'/unit-fund-clients/'+details.client_id+'/unit-fund-transfers');
};

export const getFundSummary = function (client_id) {
    return axios.get('/api/unitization/client-unit-funds/'+client_id+'/summaries');
};

export const getClientFunds = function (client_id) {
    return axios.get('/api/unitization/client-funds/'+client_id);
};

export const getFundBalance = function (details) {
    return axios.get('/api/unitization/unit-funds/'+details.fund_id+'/unit-fund-clients/'+details.client_id+'/balance');
};

export const calculateFund = function (details) {
    return axios.post('/api/unitization/unit-fund-calculate', details);
};

export const fetchAccounts = function (fundId) {
    return axios.get(`/api/unitization/${fundId}/bank-mpesa-accounts`);
};

export const getFundStatement = function (details) {
    return axios.get('/api/unitization/'+details.fund_id+'/unit-fund-holders/'+details.client_id+'/statement-load/'+details.start+'/'+details.date, details);
};

export const getFundRecievedTransfers = function (details) {
    return axios.get('/api/unitization/unit-funds/'+details.fund_id+'/unit-fund-clients/'+details.client_id+'/unit-fund-transfers-received');
};

export const getFundGivenTransfers = function (details) {
    return axios.get('/api/unitization/unit-funds/'+details.fund_id+'/unit-fund-clients/'+details.client_id+'/unit-fund-transfers-given');
};

export const getFundInstructions = function (details) {
    return axios.get('/api/unitization/unit-funds/'+details.fund_id+'/unit-fund-clients/'+details.client_id+'/unit-fund-instructions');
};