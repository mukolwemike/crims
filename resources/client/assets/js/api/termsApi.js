import axios from 'axios';

export const getGeneralTerms = function (type, item) {
    return axios.get('/api/terms/' + type + '/' + item + '/general');
};