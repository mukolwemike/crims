import axios from 'axios';

export const fetchAllUtility = () => {
    return axios.get('/api/billing/all-billing-services');
};

export const fetchClientUtility = (clientId) => {
    return axios.get(`/api/billing/${clientId}/client-billing`);
};

export const fetchAirtimeTypes = () => {
    return axios.get('/api/billing/all-airtime-types');
};

export const getSourceFunds = () =>{
    return axios.get('/api/billing/source-funds');
};
