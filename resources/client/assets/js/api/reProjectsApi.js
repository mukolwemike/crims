import axios from 'axios';

export const fetchProjects = ()=>{
    return axios.get('/api/real-estate/projects');
};
export const getPaymentPlansApi = ()=>{
    return axios.get('/api/real-estate/payment-plans');
};