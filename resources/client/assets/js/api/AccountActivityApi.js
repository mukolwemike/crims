/**
 * Created by daniel on 21/01/2017.
 */
import axios from 'axios';

export const GET_BRIEF_ACTIVITY_LOG = function (clientId) {
    return axios.get('/api/client/'+clientId+'/activity-log/brief');
};

export const GET_BRIEF_OVERVIEW_LOG = function (clientId) {
    return axios.get('/api/client/'+clientId+'/activity-log/brief');
};

export const GET_BRIEF_FUND_ACTIVITY_LOG = function (clientId) {
    return axios.get('/api/unitization/unit-fund/'+clientId+'/recent-activities-summaries' );
};

export const getApprovals = function(uuid) {
    return axios.get('/api/approvals/'+uuid);
};