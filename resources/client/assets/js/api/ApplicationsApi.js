import Vue from 'vue';
import axios from 'axios';
import ApplicationFormHelper from '../store/modules/Applications/ApplicationFormHelper';

export const FILL_RISK = function (state, data) {
    return Vue.http.post('/api/applications/validate/risk', data);
};


export const getRates = function () {
    return axios.post('/api/products/interest/rates', {
        product_id: form.product_id,
        tenor: form.tenor,
        amount: form.amount
    });
};
/**
 * Fill the investment section of the form, using data from form as a model
 */
export const FILL_INVESTMENT = function (state, form) {

    var filledInvestment = {};

    if (form.product_id) {
        filledInvestment.rate = Vue.http.post('/api/products/interest/rates', {
            product_id: form.product_id,
            tenor: form.tenor,
            amount: form.amount
        });
    }

    filledInvestment.validation = Vue.http.post('/api/applications/validate/investment', Object.assign({}, form));

    return filledInvestment;

};


/**
 * Fill the subscriber section of the form, using data from form as a model
 */
export const FILL_SUBSCRIBER = function (state, submit_form) {

    var sub = submit_form.subroute;

    Vue.delete(submit_form, 'subroute');

    Vue.delete(submit_form, 'proceed');

    Vue.delete(submit_form, 'appType');

    return Vue.http.post('/api/applications/save/' + sub, submit_form);
};

/**
 * Check whether an application is complete
 */
export const APPL_COMPLETE = function (applId) {
    return Vue.http.get('/api/applications/' + applId + '/complete');
};

export const GET_REFERRED_FA = function (referralCode) {
    return Vue.http.get('/api/applications/' + referralCode + '/referred-fa');
};

export const LOAD_UUID = function (uuid) {
    return Vue.http.get('/api/applications/' + uuid);
};

export const GET_JOINT_HOLDERS = function (applId) {
    return Vue.http.get('/api/applications/joint/' + applId);
};

/**
 * Fetch an application from the server using its uuid
 */
export const FETCH_APPLICATION = function (uuid, state, rootState) {

    var f = Vue.http.get('/api/applications/' + uuid);

    f.then(
        function (response) {
            var appl = response.data.data;

            rootState.appliedInvestment = Object.assign({}, rootState.appliedInvestment, ApplicationFormHelper.fillInvestment(appl));

            rootState.appliedSubscriber = Object.assign({}, rootState.appliedSubscriber, ApplicationFormHelper.fillSubscriber(appl));

            rootState.appliedRisk = Object.assign({}, rootState.appliedRisk, ApplicationFormHelper.fillRisk(appl));

            var ind = appl.individual;

            rootState.individualApplication = ind === null ? true : ind;

            rootState.applicationProgress = appl.progress;

            rootState.applicationDocuments = appl.documents;

            rootState.jointHolderFormLists = appl.jointHolders;

        },
        function (response) {

        }
    );

    return f;
};

export const GET_DOCUMENTS = function (uuid) {
    return Vue.http.get('/api/applications/' + uuid);
};

export const UPLOAD_KYC_COMPLETE = function (applId) {
    return Vue.http.post('/api/applications/kyc/complete/' + applId);
};

export const UPLOAD_PAYMENT_COMPLETE = function (applId, amount) {
    return Vue.http.post('/api/applications/payment/complete/' + applId, {'amount': amount});
};

export const CONFIRM_MPESA_DEPOSIT = function (applId, data) {
    return Vue.http.post('/api/applications/payment/complete/mpesa/' + applId, data);
};

export const REQUEST_BANK_DETAIL_SMS = function (data) {
    return Vue.http.post('/api/applications/payment/sms-bank-details', data)
};

export const UPLOAD_MANDATE_COMPLETE = function (appId) {
    return Vue.http.post('/api/applications/mandate/complete/' + appId);
};

export const VALIDATE_JOINT_HOLDER = function (form) {
    return Vue.http.post('/api/applications/joint_holder/validate', form);
};

export const getBanks = function () {
    return axios.get('/api/all/banks');
};

export const getBankBranches = function (bank_id) {
    return axios.get('/api/banks/' + bank_id + '/bank-branches');
};

