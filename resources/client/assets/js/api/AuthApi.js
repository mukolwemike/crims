/**
 * Created by daniel on 06/01/2017.
 */
import axios from 'axios';
import { Message } from 'element-ui';
import router from '../router';


export const VERIFY_USER = function (creds) {
    return axios.post('/auth/verify', creds);
};


export const VERIFY_USERNAME = function (creds) {
    return axios.post('/auth/verify-username', creds);
};


export const AUTHENTICATE_USER = function (creds) {
    var req = axios.post('auth', creds);

    sessionStorage.removeItem('default_client');

    return req;
};


export const LOGIN_USER = function (state, router) {

    var req =  axios.get('api/user');

    req.then(function (response) {

        state.currentUser  = response.data.data;

        state.isAuthenticated = true;
        state.loggingIn = true;

        let rdr = router.currentRoute.query.redirect;

        if (rdr) {
            router.push({ path: rdr });
        } else {
            router.push({ name: 'main' });
        }

        sessionStorage.setItem('current_user', JSON.stringify(state.currentUser));
        sessionStorage.removeItem('default_client');

    }, function () {
        state.loggingIn = true;
        state.isAuthenticated = false;
    });

    return req;
};

export const REQUEST_SMS = function (creds) {
    return axios.post('/auth/sms', creds);
};

export const REQUEST_SMS_PASSWORD_RESET = function (creds) {
    return axios.post('/auth/password-reset-sms', creds);
};

export const AUTHENTICATE_PASSWORD_RESET = function (creds) {
    return axios.post('/auth/authenticate-password-reset', creds);
};

export const LOGOUT = function (state) {
    axios.get('/api/logout')
    .then(() => {

        sessionStorage.removeItem('current_user');
        state.isAuthenticated = false;

        router.push({ name: 'login' });
        location.reload(true);

        Message.success({ message: 'Logged out succesfully'});
    },()=>
    {
        Message.warning({ message: 'Error while logging out'});
    })
};

export const FORGOT_PASWORD = function (username) {
    return axios.post('/auth/'+username+'/forgot');
};
