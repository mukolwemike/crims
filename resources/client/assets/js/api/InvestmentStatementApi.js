/**
 * Created by daniel on 30/01/2017.
 */
import axios from 'axios'

export const getStatementEmbed = function (data) {
    return axios.get('/api/investments/statements/' + data.product + '/' + data.clientId + '/embed?date_from=' + data.dateFrom + '&statement_date=' + data.dateTo);
};

export const downloadStatements = function (data) {
    return axios.get('/api/investments/statements/' + data.product.id + '/' + data.client_id + '/preview?date_from=' + data.dateFrom + '&statement_date=' + data.dateTo);
};

export const getClientPortfolio = function (data) {
    return axios.get('/api/client/statement/' + data.productId + '/' + data.clientId + '/invPortfolio?date=' + data.date);
};

export const reInvestments = function (data) {
    return axios.get('/api/client/re/portfolio/' + data.id + '/portfolio?date=' + data.date);
};