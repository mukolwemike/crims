/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 06/02/2017.
 * Project crims-client
 */
import axios from 'axios';

let faClientCommissions = function (data) {
    return axios.get('/api/fa/commissions?date='+data.date+'&id='+data.faId+'&currency='+data.currency);
};

let commissionSummary = function (form) {
    return axios.post('/api/fa/commissions/summary/'+form.id, form);
};

let exportCommissionToMail = function (form) {
    return axios.post('/api/fa/commissions/summary/'+form.id+'/export', form);
};

let clientSummary = function (client_id, date, fa_id) {
    return axios.get('/api/fa/commissions/'+client_id+'?date='+date+'&fa_id='+fa_id);
};

let faClawbacks = function (data) {
    return axios.get('/api/fa/'+data.fa.id+'/clawbacks?date='+data.date);
};

let faOverrides = function (data) {
    return axios.get('/api/fa/'+data.fa.id+'/overrides?date='+data.date);
};

let faProdCommissions = function (data) {
    return axios.get('/api/fa/'+data.fa.id+'/product_commissions?date='+data.date+'&currency='+data.currency);
};

let fetchCommissionProjections = function (data) {
    return axios.post('/api/commission/projections', { start: data.range.start, end: data.range.end, faId: data.faId});
};

let clientPortfolio = function (data) {
    return axios.get('/api/client/'+data.id+'/portfolio/'+data.faId+'?date='+data.date);
};

export default  {
    commissionSummary,
    clientSummary,
    faClawbacks,
    faOverrides,
    faProdCommissions,
    faClientCommissions,
    fetchCommissionProjections,
    clientPortfolio,
    exportCommissionToMail
    // faMonthlyClientCommission
};