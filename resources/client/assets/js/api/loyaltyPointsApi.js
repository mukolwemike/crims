import axios from 'axios';

export const fetchSummary = function (clientID) {
  return axios.get(`/api/${clientID}/loyalty-points-summaries`);
};

export const fetchVouchers = function () {
  return axios.get(`/api/vouchers`);
};