/**
 * Created by daniel on 18/01/2017.
 */
import Vue from 'vue';

export const gender = function () {
    return Vue.http.get('/api/gender/list', { cache: true });
}

export const titles = function () {
    return Vue.http.get('/api/contact/titles', { cache: true});
};

export const contactMethods = function () {
    return Vue.http.get('/api/contact/contact_methods', { cache: true});
};

export const businessNatures = function () {
    return Vue.http.get('/api/contact/business_natures', { cache: true});
};

export const employmentTypes = function () {
    return Vue.http.get('/api/contact/employment_types', { cache: true});
};

export const sourceOfFunds = function () {
    return Vue.http.get('/api/contact/source_of_funds', { cache: true});
};

export const countries = function () {
    return Vue.http.get('/api/contact/countries', { cache: true});
};

export const CONTACT_US =  function (form) {
    return Vue.http.post('/api/contactus', {message: form.message, subject: form.subject });
};

//feedback
export const SUBMIT_FEEDBACK = function (form) {
    return Vue.http.post('/api/feedback', form);
}
