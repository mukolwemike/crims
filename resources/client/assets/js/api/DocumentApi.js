/**
 * Created by yeric on 6/20/17.
 */
import axios from 'axios';

export const GET_CLIENT_DOCUMENTS = function (data) {
    return axios.get('/api/client/' + data.id + '/' + data.type + '/documents');
};

export const GET_DOCUMENT_TYPES = function (data) {
    return axios.get('/api/client/document/' + data.client_id + '/document-types');
};

export const GET_DOCUMENTS = function (doc_type) {
    return axios.get('/api/documents/' + doc_type);
};

export const VIEW_DOCUMENT = function (document_id) {
    return axios.get('/api/document/' + document_id);
};