import axios from 'axios'

export const reSummary = function (clientID) {
    return axios.get(`/api/${clientID}/re-summaries`);
};

export const unitDetails = function (holding) {
    return axios.get(`/api/${holding}/unit-details`);
};

export const submitPaymentDetails = function (form) {
    return axios.post(`/api/client/make-payments/${form.clientId}`, form)
};

export const paymentSchedules = function (holding) {
    return axios.get(`/api/${holding}/payment-schedules`);
};

export const fetchingReStatements = function (form) {
    return axios.get("/api/client/realestate/" + form.project + "/" + form.clientId + "/statements?date_from=" + form.date_from + "&date_to=" + form.date_to);
};

export const getClientsProjects = function (clientID) {
    return axios.get(`/api/client/realestate/projects/${clientID}`);
};

export const fetchPayments = function (data) {
    return axios.get(`/api/client/realestate/${data.clientId}/payments/${data.uploadStatus}`);
};

export const fetchPayment = function (payment) {
    return axios.get(`/api/client/realestate/payments/${payment}`);
};