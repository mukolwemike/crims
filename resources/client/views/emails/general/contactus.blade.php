@extends('emails.default')

@section('content')
    <p>There is a new message by the following user in <a href="https://clients.cytonn.com">CRIMS</a>:<br/>

        Name: {{ $user->firstname.' '.$user->lastname }}<br/>
        Email: {{ $user->email }}<br/>
        Phone: {{ $user->phone }}<br/>
    </p>

    <p>Subject: {{ $subject }}</p>

    <h5>Message</h5>
    <p>
        {{ $msg }}
    </p>
@endsection