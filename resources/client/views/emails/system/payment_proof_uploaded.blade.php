@extends('emails.default')

@section('content')

    <p>Attached is a Proof of Payment uploaded by
        <b>{{$clientName}} @if($clientCode)- ({{$clientCode }})@endif</b> for
        <b>{{strtoupper($type)}}</b> instruction made, kindly process the instruction.</p>

    <p>Regards,</p>

    <p class="bold-underline"><strong>Cytonn CRIMS</strong></p>
@endsection