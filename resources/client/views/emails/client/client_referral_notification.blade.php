@extends('emails.default')

@section('content')
    <p>Dear {{ $data['fullname'] }}, </p>

    <p>
        {{ $data['client_name'] }} has invited you to invest with Cytonn! Dial 809# or visit
        <a href="https://clients.cytonn.com/apply/investment">Client Portal</a> to begin your investment journey.
        Contact us on 0709101200 for any queries.
    </p>

    <p>Regards,</p>
@endsection
