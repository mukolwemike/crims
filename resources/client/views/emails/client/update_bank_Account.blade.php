@extends('emails.default')

@section('content')

    <p>A client bank account change request has been made. </p>

    <ul>
        <li> Client Code:   {{$client->client_code}}</li>
        <li> Client Name:   {{\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id)}}</li>
        <li> Client Email:  {{$client->contact->email}}</li>
        <li>Change Details: <b>Bank Account Details</b></li>
    </ul>

    <p>kindly approve the instruction.</p>

    <p>Regards,</p>

    <p class="bold-underline"><strong>Cytonn CRIMS</strong></p>
@endsection