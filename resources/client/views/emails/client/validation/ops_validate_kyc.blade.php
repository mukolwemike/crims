@extends('emails.default')

@section('content')
    <div>
        <p>
            Kindly validate the KYC details for
            <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id)!!}</b>,
            client code ({{$client->client_code}}) that has been updated. <br/>

            <br>

            Regards,<br/>
            Cytonn CRIMS.
        </p>
    </div>
@endsection