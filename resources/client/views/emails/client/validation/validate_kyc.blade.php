@extends('emails.default')

@section('content')
    <div>
        <p>
            @if($client->joint)
                Dear {!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNames($client->id)!!},<br/>
            @else
                Dear {!! \Cytonn\Presenters\ClientPresenter::presentFirstName($client->id)!!},<br/>
            @endif

            The names you provided did not match those in your provided document. Kindly update the details using
            one of the following options: <br/>
        </p>

        <ol>
            <li>
                Dial *809# and select My Account, then choose the Update ID/Passport/Tax Pin number option.
            </li>
            <li>
                Upload a copy of your {{$doc}} by visiting our client portal by following this <a
                        href="https://clients.cytonn.com/">link</a>.
            </li>
            <li>
                Reply to this email with a scanned copy of a valid {{$doc}} matching the names you
                provided.
            </li>
        </ol>


        <p>
            Regards,<br/>
            Cytonn CRIMS.
        </p>
    </div>
@endsection