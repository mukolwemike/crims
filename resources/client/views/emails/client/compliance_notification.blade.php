@extends('emails.default')

@section('content')
    <p>Dear {{ $client->contact->firstname }}, </p>

    <p>
        Please submit your pending KYC documents for the investment in {{ $fund->name }}.
    </p>

    <p>
        You can submit them by logging into our online portal or mobile app and accessing the profile page.
    </p>

    <p>
        Kindly note that your investment will not earn any interests if they remain non-compliant.
    </p>
@endsection