@extends('emails.default')

@section('content')
    <p>Dear {{ $client->present()->firstname }}, </p>
    <p>
        We have received a {{ $type }} request @if($type == 'withdrawal') of {{ $investment->product->currency->code }} {{ number_format($form->repo->amountWithdrawn()) }} @endif for your investment of {{ $investment->product->currency->code }} {{ number_format($investment->amount) }}
        @if(\Carbon\Carbon::today()->toDateString() > $investment->maturity_date) which matured on @else to mature on @endif
        {{ (new \Carbon\Carbon($investment->maturity_date))->toFormattedDateString() }}.
        <br />
        @if($type == 'withdrawal')
            {{--Please note that this instruction will be processed within @if($form->withdrawal_stage == 'premature') 5 @else 3 @endif  working days.--}}
            We shall carry out a call back confirmation.
        @else
            Please expect a business confirmation from us.
        @endif
    </p>

    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>

    <p>
        Thank you,
    </p>

    <span class="bold-underline">Cytonn Investments Management PLC.</span>

@endsection