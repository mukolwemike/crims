@extends('emails.default')

@section('content')
    <p>A {{ $type }} was made in CRIMS. See the details below:</p>

    <ul style="list-style: none">
        <li>Client Name: {{ $client->name() }}</li>
        <li>Product: {{ $form->investment->product->name }}</li>
        <li>Amount: {{ \Cytonn\Presenters\AmountPresenter::currency($form->amount, false, 2)  }}</li>
        @if($type == 'rollover')
            <li>Tenor: {{ $form->tenor }} months</li>
        @endif
    </ul>

    <p>
        Regards,<br/>

        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection
