@extends('emails.default')

@section('content')
    <p>Dear {{ $client->present()->firstname }}, </p>
    <p>
        Thank you for investing with Cytonn Investments.
        <br>
        An instruction type {{ $type }} has been sent for approval. You can approve in any of the following methods:
    </p>

    <ol>
        <li>Please log in to your Cytonn clients portal to approve or</li>
        <li>Send an approval email to operations@cytonn.com. (If you have email indemnity) or </li>
        <li>Send a similarly filled and dully signed  {{ $type }} form to operations@cytonn.com </li>
    </ol>

    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>

    <p>
        Thank you,
    </p>

    <span class="bold-underline">Cytonn Investments Management Limited</span>
@endsection