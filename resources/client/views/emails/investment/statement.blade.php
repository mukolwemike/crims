@extends('emails.default')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Kindly find attached your {!! strtoupper($productName) !!} Statement as at {!! $stmt_date->format('jS F Y')  !!}
        .

        @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
            {!! $campaign->statement_message !!}
        @endif
        <br/><br/>
        Please note that this statement is password protected for security reasons. The password is your 4 digit partner
        code XXXX.
        <br><br>

        Kind regards,<br/>

        {!! $email_sender !!}
    </p>
@endsection