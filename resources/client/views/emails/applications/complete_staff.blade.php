@extends('emails.default')

@section('content')
    <p>Dear {{ $application->present()->firstname }}, </p>
    <p>
        Thank you for investing with Cytonn Investments.
        Your funds will be Invested as per the terms you have indicated and you will receive a business confirmation on the same within the next 24 hours.
    </p>
    @if(!$compliant)
        <p>
            Kindly note that in accordance with the legal requirements by the Central bank of Kenya, the following KYC documents will need to be uploaded within 30 days of application;
        </p>
        <ol>
            <li>Copy of ID /Passport/Social security</li>
            <li>Copy of PIN (TAX) certificate</li>
            <li>Proof of banking details: <br>
                - Bank Statement (not more than 3 months old) or <br>
                - Original cancelled cheque or <br>
                - Certified letter confirming bank account details <br>
            </li>
            <li>Copy of Utility Bill (not more than 3 months old) or a Proof of Address letter</li>
        </ol>
    @endif
    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>
    <p>
        Thank you,
    </p>

    <span class="bold-underline">Cytonn Investments Management Limited</span>
@endsection