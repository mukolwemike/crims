@extends('emails.default')

@section('content')
    <p>Dear {{ $application->present()->firstName }},</p>

    <p>
        Thank you for taking time to apply for our products.<br/>
        Any time you need to continue with your application, kindly resume your application below
        <br/>

        <span class="text-center">
            <a style="
            font-weight: bold;
            color: #fefefe;
            text-decoration: none;
            display: inline-block;
            padding: 20px 40px 20px 40px;
            border: 0px solid #2199e8;
            background: #2199e8;
            font-size: 1.1em;
            border-radius: 3px;
        "
                    class="large expand" href="https://{{ config('app.url')}}/apply/resume/{{ $uuid }}">Resume Application</a>
        </span>

        <br/>

        You can also copy & paste the following url in your browser to resume your application.

        <br/>
        <a href="https://{{ config('app.url') }}/apply/resume/{{ $uuid }}">https://{{ config('app.url') }}/apply/resume/{{ $uuid }}</a>


        <br/>
        <br/>
        Kind regards,<br/>

        <span class="bold-underline">Cytonn Investments Management Limited</span>
    </p>
@endsection