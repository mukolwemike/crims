@extends('emails.default')

@section('content')
    <p>An investment application has been saved. Here are the details:</p>

    <ul style="list-style: none">
        <li>Client Name: {{ $application->present()->name }}</li>
        <li>Client Email: {{ $application->email }}</li>
        <li>Amount: {{ $application->present()->amount }}</li>
        @if(isset($application->product_id))
            <li>Product: {{ $application->product->name }}</li>
            <li>Tenor: {{ $application->tenor }} months</li>
        @elseif(isset($application->unit_fund_id))
            <li>Unit Fund: {{ $application->unitFund->name }}</li>
        @endif

    </ul>

    <p>
        Regards,<br/>

        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection