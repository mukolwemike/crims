@extends('emails.default')

@section('content')
    <p>Dear {{ $application->present()->firstname }}, </p>
    <p>
        Your application has been saved successfully. Your will receive a business confirmation within 24hrs of depositing the funds.
    </p>
    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>
    <p>
        Thank you,
    </p>

    @include('emails.client_service_signature')
@endsection