<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>
        body{
            color: rgb(51, 51, 51);
            display: block;
            font-family: Helvetica Neue, Helvetica, Arial;
            font-size: 15px;

        }
        body p, body a, body li, body td{
            font-family: Helvetica Neue, Helvetica, Arial;
            font-size: 15px;
        }
        .email_body{
            background-color: #FFF;
            min-height: 200px;
            margin: 5px;
            box-shadow: #ebebeb;
        }
        .bold{
            font-weight: 700;
        }
        table{
            border-collapse: collapse;
            margin-top:10px;
            margin-bottom:10px;
            width:95% !important;
        }
        .table-striped tbody>tr:nth-of-type(even){
            background-color:#e3e3e3 !important;
        }
        table tr td, table tr th{
            border:1px solid black;
            padding-left: 5px;
            padding-right: 5px;
            padding-top:3px;
            padding-bottom: 3px;
        }
        table thead tr, table .header td{
            background-color:#006666 !important;
            color: #FFF !important;
        }
        h1, h2, h3, h4, h5, h6{
            color: #006666;
        }
        .underline{
            text-decoration: underline;
        }
        .left{
            text-align: left;
        }
        .vertical-align-top{
            vertical-align: top;
        }
        td.container, th.container{
            vertical-align: top;
            padding: 0;
            border: none;
        }
        .align-left,
        .align-left td,
        .align-left th{
            text-align: left;
        }
    </style>
</head>
<body>
    <div class="email_body">
        @yield('content')
    </div>
    @include('emails.email_footer')
</body>
</html>