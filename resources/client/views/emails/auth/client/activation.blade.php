@extends('emails.default')

@section('content')

<p>Dear {!! $firstname !!}, <br/>

You have been registered for an account at <strong>Cytonn Client Area</strong>.<br/>

Your username is <b>{!! $username !!}</b>.

<br>
<br/>
To activate your account click on the button below.
<br/>
<br>

        <span style="    display: block; width: 100%; text-align: center;">
                    <a style="
                        font-weight: bold;
                        color: #fefefe;
                        text-decoration: none;
                        display: inline-block;
                        padding: 20px 40px 20px 40px;
                        border: 0 solid #2199e8;
                        background: #2199e8;
                        font-size: 1.1em;
                        border-radius: 3px;
                        text-align: center;
                        margin: 0 auto"
                   class="large expand"
                   href="{!! getenv('CLIENT_DOMAIN').'/account/'.$username.'/activate/'.$activation_key !!}"
                    >
                        Activate your account
                    </a>
        </span>

<br>The link expires in <b>24 hours</b>, once it has expired you can request a new one. Follow the instructions below to complete your activation: <br>
<ol>
    <li>Create your password and click continue</li>
    <li>Click on sign in, then enter your username <b>({{ $username }})</b> and the password you created above then submit</li>
    <li>Enter your verification token then submit. You can obtain the token by:
        <ol style="list-style-type: lower-alpha">
            <li>Requesting an SMS to your phone by clicking "I don't have the app, request SMS" <b>OR</b></li>
            <li>Installing the <a href="https://www.authy.com/app/mobile/">the Authy mobile application</a> on your phone</li>
        </ol>
    </li>
</ol>

You are now logged in and you can view your investment status, top up withdraw or rollover your investments.<br/>

Regards,<br/><br>

<span class="bold-underline">Cytonn Investments</span>
</p>

@endsection
