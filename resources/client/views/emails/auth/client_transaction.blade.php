@extends('emails.default')

@section('content')
    <p>Dear {!! $user->present()->fullName() !!},</p>

    <p>Please use <b>{!! $token !!}</b> to verify your transaction in your Cytonn Client Portal.</p>

    <p>
        Regards,<br/>
        <span class="bold-underline">Cytonn Investments Management PLC.</span>
    </p>

@endsection