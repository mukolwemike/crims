@extends('emails.default')

@section('content')
      <h3>Password Reset</h3>

      <div>
            <p>Click <a href="{!! config('app.url') !!}/account/{!! $username !!}/reset/{!! $token !!}">here</a> to reset your password. </p>


            If you can't click the above link, copy the link below to your browser.<br/>

            <a href="{!! config('app.url') !!}/account/{!! $username !!}/reset/{!! $token !!}">{!! config('app.url') !!}/account/{!! $username !!}/reset/{!! $token !!}</a>

      </div>
@endsection