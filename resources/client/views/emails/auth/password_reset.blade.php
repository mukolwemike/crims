@extends('emails.default')

@section('content')
    <p>The following client requested a password reset</p>

    <ul style="list-style: none">
        <li>Name: {{ $user->firstname.' '.$user->lastname }}</li>
        <li>Email: {{ $user->email }}</li>
        <li>Username: {{ $user->username }}</li>
    </ul>

    <p>The user needs a valid password to access his/her client accounts.</p>

    <p>
        Regards,<br/>

        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection