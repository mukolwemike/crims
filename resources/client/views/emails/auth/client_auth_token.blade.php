@extends('emails.default')

@section('content')
    <p>Dear {!! $user->present()->fullName() !!}</p>

    <p>Please use <b>{!! $token !!}</b> to login in to your crims portal.</p>

    <p>
        Regards,<br/>
        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection