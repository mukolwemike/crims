@extends('emails.email_plain')

@section('content')
    <p>Dear {{ $fa->name }}, </p>

    <p>
        Here is the commission summary for the dates {{ Carbon\Carbon::parse($summary['start'])->toFormattedDateString() }} to {{ Carbon\Carbon::parse($summary['end'])->toFormattedDateString() }}.
    </p>

    <table>
        <thead>
            <tr>
                <th></th>
                <th>Total Commission</th>
                <th>Claw Back</th>
                <th>Override</th>
            </tr>
        </thead>
        <tbody>
        @foreach($summary['products'] as $product)
            <tr>
                <td>{{ $product['name'] }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($product['total']) }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($product['claw_backs']) }}</td>
                <td>{{ \Cytonn\Presenters\AmountPresenter::currency($product['overrides']) }}</td>
            </tr>
        @endforeach
            <tr>
                <th>Totals</th>
                <th>{{ \Cytonn\Presenters\AmountPresenter::currency($summary['total']) }}</th>
                <th>{{ \Cytonn\Presenters\AmountPresenter::currency($summary['claw_backs']) }}</th>
                <th>{{ \Cytonn\Presenters\AmountPresenter::currency($summary['overrides']) }}</th>
            </tr>
        </tbody>
    </table>
@endsection