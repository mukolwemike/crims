@extends('emails.default')

@section('content')
    <p>A Real Estate Unit Payment was made in CRIMS. See the details below:</p>

    <ul style="list-style: none">
        <li><b>Client Name:</b>     {{ $client->name() }}</li>
        <li><b>Project:</b>         {{ $holding->unit->project->name }}</li>
        <li><b>Unit:</b>            {{ $holding->unit->number }}</li>
        <li><b>Payment Date:</b>    {!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</li>
        <li><b>Amount:</b>          {!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</li>
        <li><b>Schedule:</b>        {!! $schedule->description !!}</li>
        <li><b>Narration:</b>       {!! $payment->description !!}</li>
        <li><b>Mode of Payment:</b> {!! $payment->mode_of_payment !!}</li>
        <li>
            <b>Proof Provide:</b>
            @if($instruction->document_id)
                        <span>Provided {{$instruction->document_id}}</span>
            @else
                        <span>Not Provided {{$instruction->document_id}}</span>
            @endif
        </li>
    </ul>

    <p>
        Regards,
        <br>
        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection