@extends('emails.email_plain')

@section('content')
    <p>Dear {{ $client->present()->firstname }}, </p>

    <p>
        Thank you for investing with Cytonn Investments.
    </p>

    <p>This is a notification for a Unit Holding Payment you made
        on {{ \Cytonn\Presenters\DatePresenter::formatDate(\Carbon\Carbon::now()) }} awaiting processing</p>

    <p>
        Here is the summary details of the holding:.
    </p>

    <table>
        <thead>
        <tr>
            <th>Project</th>
            <th>Unit</th>
            <th>Unit Name</th>
            <th>Unit Price (KES)</th>
            <th>Paid (KES)</th>
            <th>Balance (KES)</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{!! $holding->unit->project->name !!}</td>
            <td>{!! $holding->unit->number !!}</td>
            <td>{!! $holding->unit->size->name !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($unit_price) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($total_paid) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($balance) !!}</td>
        </tr>
        </tbody>
    </table>

    <p>
        Here is the summary details of the transaction:.
    </p>

    <table class="table table-responsive table-striped">
        <tbody>
        <tr>
            <th>Payment Date</th>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
        </tr>

        <tr>
            <th>Amount</th>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
        </tr>

        <tr>
            <th>Schedule</th>
            <td>{!! $schedule->description !!}</td>
        </tr>

        <tr>
            <th>Narration</th>
            <td>{!! $payment->description !!}</td>
        </tr>

        <tr>
            <th>Mode of Payment</th>
            <td>{!! $payment->mode_of_payment !!}</td>
        </tr>

        <tr>
            <th>Proof of payment</th>
            <td>
                @if($instruction->document_id)
                    <span>Provided</span>
                @else
                    <span>Not Provided</span>
                @endif
            </td>
        </tr>

        </tbody>
    </table>
@endsection

@extends('emails.email_footer')
