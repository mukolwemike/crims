@extends('emails.default')

@section('content')
    <div>
        <p>
            @if($type === 'sender')
                Hi <b>{!! \Cytonn\Presenters\ClientPresenter::presentFirstName($sender->id)!!}</b>, you just transferred
                <b>{{$units}}</b> units from your CMMF account to
                <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($receiver->id)!!}</b>
                - <b>{{$receiver->client_code}}</b>. Transaction ref <b>{{$trans_ref}}</b>. Thank you for doing business with us.
            @elseif($type === 'receiver')
                Great news, <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($sender->id)!!}</b> has
                transferred <b>{{$units}}</b> units to your CMMF account. Transaction ref <b>{{$trans_ref}}</b>. Thank you for doing
                business with us.
            @elseif($type === 'joint_or_corporate')
                This is to acknowledge the request to transfer <b>{{$units}}</b> units from your CMMF to
                <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($receiver->id)!!}</b> -
                <b>{{$receiver->client_code}}</b>. Transaction ref <b>{{$trans_ref}}</b>. We're currently processing your instruction and we will update you shortly.
                Thank you for doing business with us.
            @elseif($type === 'ops-notify')
                Kindly note that  <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($sender->id)!!}</b> -
                <b>{{$sender->client_code}}</b> has transferred <b>{{$units}}</b> units to
                <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($receiver->id)!!}</b> -
                <b>{{$receiver->client_code}}</b> on CMMF Unit Fund.
            @endif
            <br/>
            <br/>

            Regards,<br/>
            Cytonn CRIMS.
        </p>
    </div>
@endsection