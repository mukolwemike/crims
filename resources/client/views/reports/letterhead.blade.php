<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .table{
            margin-bottom: 20px;
        }
        .justify, .justify p{
            text-justify: inter-word;
        }
        .table, .table tr, .table td, .table th{
            border: 1px solid #000;
            border-collapse: collapse;
        }
        .table tr td, .table tr th{
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 3px;
            padding-right: 3px;
        }
        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }
        .table-responsive{
            width: 100%;
        }
        body, body:before, body::after{
            background-color: #FFF;
        }
        html{
            background-color: #FFF !important;
        }
        .bold-underline{
            font-weight: 600;
            text-decoration: underline;
        }
        .underline{
            text-decoration: underline;
        }
        .bold{
            font-weight: 600;
        }
        .logo {
            width: 30%;
            display: inline;
        }
        .address{
            width: 20%;
            display: inline-block;
            position: absolute;
            left: 80%;
            font-family: 'Open Sans',sans-serif;
            font-size: 11px;
            z-index: 0;
        }
        .container{
            font-family: Arial, sans-serif;
            font-size: 14px;
        }

        .cytonn-rule{
            color: #006b5b;
            border-top: 3px solid #006b5b;
            margin-top: 27px;
        }
        .footer-table {
            background-color: #006b5b;
        }

        .footer-table{
            width: 100%;
            /*background-color: #006b5b;*/
            color:#FFF;
            text-align: center;
            position: absolute;
            bottom: 40px;
        }
        .footer-quote{
            position: absolute;
            bottom: 10px;
            font-style: italic;
        }
        .pull-right{
            float: right !important;
        }
        .right{
            text-align: right;
        }
        .pull-left{
            float: left !important;
        }
        .left {
            text-align: left;
        }
        .bordered{
            border: 1px solid #e3e3e3;
        }
        .bordered h2{
            font-size: 1.6em;
            background: #A3CF5F;
            padding: 2px 5px;
            text-align: center;
            font-weight: normal;
        }
        .bordered h3{
            font-size: 1.4em;
            padding: 2px 5px;
            text-align: center;
            color: #006b5b;
            font-weight: normal;
        }
        .bordered h4{
            background: #006b5b;
            color: #fff;
            font-weight: normal;
            font-size: 1.2em;
            padding: 2px 5px;
        }
        label{
            font-weight: bold;
            line-height: 2.5;
        }
        label:after{
            content:':';
        }
        .tab-left{
            margin-left: 30px;
        }
        .uppercase{
            text-transform: uppercase;
        }
        .page-break-after{
            page-break-after: always;
        }

        .cover_letter_header {
            width: 100%;
        }

        .cover_letter_header img{
            width: 100%;
            height: 50%;
        }
    </style>
</head>
<body>
<div class="container">

    @if(!isset($companyAddress))
        <div id="header">
            <div class="logo">
                @if(!isset($logo))
                    <img src="{{ public_path("assets/img/logo_md.png") }}" alt="logo"/>
                @else
                    <img src="{{ public_path("assets/img/".$logo) }}" alt="logo"/>
                @endif
            </div>

            <div class="address">
                3rd Floor, Liaison House<br/>
                State House Avenue,<br/>
                Nairobi, Kenya<br/>
                P.O. Box 20695 - 00200<br/>
                +254 (0)20 4400420 <br/>
                <a href="mailto:info@cytonn.com">info@cytonn.com</a> <br/>
                <a href="http://www.cytonn.com">www.cytonn.com</a>
            </div>

            <hr class="cytonn-rule"/>
        </div>
    @else

        <div id="header" class="cover_letter_header">
            <img src="{{ public_path("assets/img/asset_manager.png") }}" alt="logo"/>
        </div>

        <br>
    @endif

    @yield('content')

    <div class="footer" id="footer">
        @yield('quote')
        @if(!isset($companyAddress))

            <table class="footer-table">
                <tbody>
                <tr><td>Investments</td><td>Fixed Income</td><td>Real Estate</td><td>Equities</td><td>Private Equity</td><td>Advisory</td></tr>
                </tbody>
            </table>

        @else
            <table class="footer-table">
                <tbody>
                <tr><td>Regulated by the Capital Markets Authority, Kenya</td></tr>
                </tbody>
            </table>
        @endif

        @include('reports.partials.report_footer')

    </div>
</div>
</body>
</html>