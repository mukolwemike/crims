<table>
    <thead>
    <tr>
        <th>Unit</th>
        <th>No. of Units</th>
        <th>Unit Numbers</th>
        <th>Price</th>
        <th>Interest</th>
        <th>Paid</th>
        <th>Pending</th>
        <th>Overdue</th>
        <th>Next Payment Date</th>
        <th>Next Payment Amount</th>
    </tr>
    </thead>
    <tbody>

    @foreach($clusters as $cluster)
        <tr>
            <td>{!! $cluster->size->name !!}</td>
            <td>{!! $cluster->holdings->count() !!}</td>
            <td>{!! $cluster->unitNumbers !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_price) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->interest_accrued) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_payments_received) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_pending) !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_due) !!}</td>
            <td>{!! $cluster->next_payment_date !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->next_payment_amount) !!}</td>
        </tr>
    @endforeach

    <tr>
        <th></th>
        <th>{!! $totals->no_of_units !!}</th>
        <th></th>
        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_price) !!}</th>
        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('interest_accrued')) !!}</th>
        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_payments_received) !!}</th>
        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->payment_pending) !!}</th>
        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('payment_due')) !!}</th>
        <th></th>
        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('next_payment_amount')) !!}</th>
    </tr>
    </tbody>
</table>

@if($totals->interest_accrued > 0)
    <table class="table table-responsive real-estate-statement">
        <tbody>
        <tr>
            <th colspan="7"><p>Below is the breakdown of the interest accrued </p></th>
        </tr>
        <tr class="cytonn-green">
            <th>Principal</th><th>Rate</th><th>Due Date</th><th>Tenor</th><th>Interest</th><th>Paid</th><th>Balance</th>
        </tr>
        </tbody>
        <tbody>
        @foreach($clusters as $cluster)
            @if($cluster->price != 0)
                @foreach($cluster->holdings as $holding)
                    @foreach($holding->overdue_schedules as $schedule)
                        <tr>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->principal) !!}</td>
                            <td>{!! \Cytonn\Realestate\Payments\Interest::INTEREST_RATE !!}%</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->due_date) !!}</td>
                            <td>{!! $schedule->tenor !!} days</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->interest_accrued) !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->paid) !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->balance) !!}</td>
                        </tr>
                    @endforeach
                @endforeach
            @else
                <td></td>
            @endif
        @endforeach
        </tbody>
    </table>
@endif