@extends('reports.letterhead')

@section('content')
    <style>
        .real-estate-statement, .real-estate-statement tr, .real-estate-statement th, .real-estate-statement td{
            border: 1px solid #000;
            font-size: 12px;
        }
        .real-estate-statement tr th {
            font-weight: bold;
            text-align: left;
        }
        .cytonn-green{
            background: #006666;
            color: #FFF;
        }


    </style>
    {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Client Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif

    <p class="bold-underline">RE: STATEMENT OF ACCOUNT</p>

    Cytonn Real Estate takes this opportunity to thank you for investing in our Real Estate product, {!! ucwords($project->name) !!}.
    <br><br>

    Kindly see below your investment statement showing the account status as at {!! $statementDate->format('jS F Y')  !!}. <br>
    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive real-estate-statement">
                <tbody>
                <tr class="cytonn-green">
                    <th rowspan="2">Unit</th>
                    <th rowspan="2">No. of Units</th>
                    <th rowspan="2">Unit Numbers</th>
                    <th rowspan="2">Price</th>
                    <th rowspan="2">Interest</th>
                    <th rowspan="2">Paid</th>
                    <th rowspan="2">Pending</th>
                    <th rowspan="2">Overdue</th>
                    <th colspan="2">Next Payment</th>
                </tr>
                <tr class="cytonn-green">
                    <th>Date</th>
                    <th>Amount</th>
                </tr>
                @foreach($clusters as $cluster)
                    <tr>
                        <td>{!! $cluster->size->name !!}</td>
                        <td>{!! $cluster->holdings->count() !!}</td>
                        <td>{!! $cluster->unitNumbers !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_price, null, 0) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->interest_accrued, null, 0) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_payments_received, null, 0) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_pending, null, 0) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_due, null, 0) !!}</td>
                        <td>{!! $cluster->next_payment_date !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->next_payment_amount, null, 0) !!}</td>
                    </tr>
                @endforeach
                <tr class="cytonn-green">
                    <th colspan="10">Totals</th>
                </tr>
                <tr>
                    <th></th>
                    <th>{!! $totals->no_of_units !!}</th>
                    <th></th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_price, null, 0) !!}</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('interest_accrued'), null, 0) !!}</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_payments_received, null, 0) !!}</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($totals->payment_pending, null, 0) !!}</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('payment_due'), null, 0) !!}</th>
                    <th></th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('next_payment_amount'), null, 0) !!}</th>
                </tr>
                </tbody>
            </table>
            @if($totals->interest_accrued > 0)
                <p>Below is the breakdown of the interest accrued </p>
                <table class="table table-responsive real-estate-statement">
                    <tbody>
                    <tr class="cytonn-green">
                        <th>Principal</th><th>Rate</th><th>Due Date</th><th>Tenor</th><th>Interest</th><th>Paid</th><th>Balance</th>
                    </tr>
                    </tbody>
                    <tbody>
                    @foreach($clusters as $cluster)
                        @if($cluster->price != 0)
                            @foreach($cluster->holdings as $holding)
                                @foreach($holding->overdue_schedules as $schedule)
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->principal, null, 0) !!}</td>
                                        <td>{!! \Cytonn\Realestate\Payments\Interest::INTEREST_RATE !!}%</td>
                                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->due_date) !!}</td>
                                        <td>{!! $schedule->tenor !!} days</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->interest_accrued, null, 0) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->paid, null, 0) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->balance, null, 0) !!}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @else
                            <td></td>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif

    Thank you for investing with us.
    <br>
@stop
