<div class="statements_table">

    <table class="table table-striped table-responsive" style="font-size:12px;">
        <thead>
        <tr>
            <th class="text-center">UNIT</th>
            <th class="text-center">NO OF UNITS</th>
            <th class="text-center">UNIT NUMBERS</th>
            <th class="text-center">PRICE</th>
            <th class="text-center">INTEREST</th>
            <th class="text-center">PAID</th>
            <th class="text-center">PENDING</th>
            <th class="text-center">OVERDUE</th>
            <th class="text-center">NEXT PAYMENT DATE</th>
            <th class="text-center">NEXT PAYMENT AMOUNT</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clusters as $cluster)
            <tr>
                <td style="text-align: center;">{!! $cluster->size->name !!}</td>
                <td style="text-align: center;">{!! $cluster->holdings->count() !!}</td>
                <td style="text-align: center;">{!! $cluster->unitNumbers !!}</td>
                <td style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_price, null, 0) !!}</td>
                <td style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->interest_accrued , null, 0) !!}</td>
                <td style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->total_payments_received, null, 0) !!}</td>
                <td style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_pending, null, 0) !!}</td>
                <td style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->payment_due, null, 0) !!}</td>
                <td style="text-align: center;">{!! $cluster->next_payment_date !!}</td>
                <td style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($cluster->next_payment_amount, null, 0) !!}</td>
            </tr>
        @endforeach
        </tbody>
        <thead>

        <tr>
            <th colspan="" style="font-size: 14px;">TOTALS</th>
            <th style="text-align: center;">{!! $totals->no_of_units !!}</th>
            <th></th>
            <th style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_price, null, 0) !!}</th>
            <th style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('interest_accrued'), null, 0) !!}</th>
            <th style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($totals->total_payments_received, null, 0) !!}</th>
            <th style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($totals->payment_pending, null, 0) !!}</th>
            <th style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('payment_due'), null, 0) !!}</th>
            <th></th>
            <th style="text-align: center;">{!! \Cytonn\Presenters\AmountPresenter::currency($clusters->sum('next_payment_amount'), null, 0) !!}</th>
        </tr>
        </thead>
    </table>

    <br>

    @if($totals->interest_accrued > 0)
        <table class="table table-striped table-responsive">
            <thead>
                <tr>
                    <th colspan="7"><p>Below is the breakdown of the interest accrued </p></th>
                </tr>
                <tr>
                    <th>PRINCIPAL</th>
                    <th>RATE</th>
                    <th>DUE DATE</th>
                    <th>TENOR</th>
                    <th>INTEREST</th>
                    <th>PAID</th>
                    <th>BALANCE</th>
                </tr>
            </thead>
            <tbody>
            @foreach($clusters as $cluster)
                @if($cluster->price != 0)
                    @foreach($cluster->holdings as $holding)
                        @foreach($holding->overdue_schedules as $schedule)
                            <tr>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->principal, null, 0) !!}</td>
                                <td>{!! \Cytonn\Realestate\Payments\Interest::INTEREST_RATE !!}%</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->due_date) !!}</td>
                                <td>{!! $schedule->tenor !!} days</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->interest_accrued, null, 0) !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->paid, null, 0) !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->balance, null, 0) !!}</td>
                            </tr>
                        @endforeach
                    @endforeach
                @else
                    <td></td>
                @endif
            @endforeach
            </tbody>
        </table>
    @endif
</div>

