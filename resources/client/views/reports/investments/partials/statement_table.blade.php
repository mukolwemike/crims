<div class="statements_table">
    <?php use App\Cytonn\Presenters\General\Presenter; ?>
{{--    <p>{{ strtoupper($product->shortname) }} statement as from {!! $fromDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!} to {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}.</p>--}}

        <table class="table table-striped" style="font-size:12px;">
            <thead>
            <tr>
                <th>DESCRIPTION</th>
                <th>AMOUNT</th>
                <th>RATE</th>
                <th>ACTION DATE</th>
                <th>GROSS INTEREST</th>
                <th>NET INTEREST</th>
                <th>ACTION</th>
                <th>TOTAL</th>
                <th>MATURITY DATE</th>
            </tr>
            </thead>
            <?php $count = 1 ?>
            @foreach($data['investments'] as $prepared)
                <tbody>
                <tr><td colspan="9" style="padding-left: 10px"><b>{!! $count++ !!}.</b></td></tr>
                <tr>
                    <td>Principal</td>
                    <td align="left">{!! \App\Cytonn\Presenters\General\Presenter::currency($prepared->investment->amount) !!}</td>
                    <td align="left">{!! $prepared->investment->interest_rate !!}%</td>
                    <td>{!! \App\Cytonn\Presenters\General\Presenter::date($prepared->investment->invested_date) !!}</td>
                    <td align="left"></td>
                    <td align="left"></td>
                    <td align="left"></td>
                    <td align="left">{!! \App\Cytonn\Presenters\General\Presenter::currency($prepared->investment->amount) !!}</td>
                    @if($prepared->investment->on_call)
                        <td>On call</td>
                    @else
                        <td>{!! \App\Cytonn\Presenters\General\Presenter::date($prepared->investment->maturity_date) !!}</td>
                    @endif
                </tr>
                @foreach($prepared->actions as $action)
                    @if($action->amount != 0)
                        <tr>
                            <td>{{ $action->description  }}</td>
                            <td></td><td></td>
                            <td>{{ \App\Cytonn\Presenters\General\Presenter::date($action->date) }}</td>
                            <td align="left">{{ \App\Cytonn\Presenters\General\Presenter::currency($action->gross_interest) }}</td>
                            <td align="left">{{ \App\Cytonn\Presenters\General\Presenter::currency($action->net_interest) }}</td>
                            <td align="left">{{ Cytonn\Presenters\AmountPresenter::accounting($action->amount, false, 0) }}</td>
                            <td align="left">{{ \App\Cytonn\Presenters\General\Presenter::currency($action->total) }}</td>
                            <td></td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
                <thead>
                <tr class="total">
                    <th class="crims_statement_left">TOTAL</th>
                    <td class="crims_statement_left bold">
                        {!! \App\Cytonn\Presenters\General\Presenter::currency($prepared->total->principal) !!}
                    </td>
                    <th class="crims_statement_left">
                        @if($prepared->total->principal > 0)
                            {!! $prepared->investment->interest_rate !!}%
                        @endif
                    </th>
                    <th class="crims_statement_left">{!! \App\Cytonn\Presenters\General\Presenter::date($statementDate) !!}</th>
                    <th class="crims_statement_left">
                        {!! \App\Cytonn\Presenters\General\Presenter::currency($prepared->total->gross_interest_cumulative) !!}
                    </th>
                    <th class="crims_statement_left">
                        {!! \App\Cytonn\Presenters\General\Presenter::currency($prepared->total->net_interest_cumulative) !!}
                    </th>
                    <th></th>
                    <th class="crims_statement_left">
                        {!! \App\Cytonn\Presenters\General\Presenter::currency($prepared->total->total) !!}
                    </th>
                    <th class="crims_statement_left">
                        @if($prepared->investment->on_call && $prepared->total->principal <= 0)
                            On call
                        @elseif($prepared->total->principal <= 0)
                            {!! \App\Cytonn\Presenters\General\Presenter::date($prepared->investment->maturity_date) !!}
                        @endif
                    </th>
                </tr>
                </thead>
            @endforeach
            <tbody>
                <tr><td colspan="9" height="14px"></td></tr>
            </tbody>


            <thead>
            <tr class="crims_statement_left total">
                <th class="crims_statement_left">GRAND TOTAL</th>
                <th>{!! $product->currency->symbol.' '.\App\Cytonn\Presenters\General\Presenter::currency($data['totals']['total_principal']) !!}</th>
                <th  colspan="3"></th>
                <th class="crims_statement_left">{!! $product->currency->symbol.' '.\App\Cytonn\Presenters\General\Presenter::currency($data['totals']['total_interest']) !!}</th>
                <th></th>
                <th class="crims_statement_left">{!! $product->currency->symbol.' '.\App\Cytonn\Presenters\General\Presenter::currency($data['totals']['total_client_value']) !!}</th>
                <th></th>
            </tr>
            </thead>
        </table>
</div>