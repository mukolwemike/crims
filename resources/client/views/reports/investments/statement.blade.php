<?php use App\Cytonn\Presenters\General\Presenter; ?>

@extends('layouts.letterhead')

@section('content')
    <style>
        .statement-table{
            font-size: 12px;
        }
    </style>
    {!! $statementDate->format('j\<\s\u\p\>S \<\/\s\u\p\> F Y') !!}<br/><br/>

    {{ $client->present()->firstNameLastName }}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {{ $client->present()->contactPerson }} <br/>
    @endif
    (Partner Code – {{ $client->client_code }})<br/>

    {!! $client->present()->address !!}

    @if($client->clientType->name == 'corporate')
        Dear  {{  $client->present()->contactPersonShortName }},
    @else
        Dear {{ $client->present()->shortName }},
    @endif

    <p class="bold-underline">RE: {!! $product->name !!} STATEMENT OF ACCOUNT</p>

    @include('reports.investments.partials.statement_table')

    <br/>
    <p>Thank you for investing with us.</p><br/>
    <p>Yours Sincerely,</p>
    <br/>
    <p class="bold-underline">{{ $product->fundManager->fullname }}</p>

    <small style="position: absolute; bottom: 20px; ">Generated
        @if($user)
            by {{ $user->present()->fullName() }}
        @endif
        on {{ \Carbon\Carbon::now()->toDayDateTimeString() }}</small>
    <small style="position: absolute; bottom: 20px; right: 0">CRIMS</small>
@stop
