@extends('layouts.default')

@section('content')
    <div class="align-center grid-block" ng-controller="staticLoginController">
        <div class="md-5 medium-7 grid-block">
            <div class="grid-content">
                <div class="login-panel">
                    <h3>Enter Username and Password To Continue</h3>

                    <p ng-if="login_error">Could not login with credentials</p>

                    {!! Form::open( ['name'=>'login_form', 'ng-submit'=>'authenticate($event)']) !!}

                        <input class="" placeholder="Username" name="username" type="text" ng-model="creds.username">

                        <input class="" placeholder="Password" name="password" type="password" ng-model="creds.password">


                        <button ng-disabled="submitting" class="button login-button" type="submit"><i ng-show="submitting" class="fa fa-spinner fa-spin"></i> Continue</button>

                        <a href="#">Forgot your password?</a>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection