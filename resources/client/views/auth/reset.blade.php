@extends('layouts.default')

@section('content')
    <div class="row align-center">
        <div class="small-8 columns">
            <br>
            <br>
            <br>

            <password-reset></password-reset>

            <div class="login-footer password-reset-blade">
                <ul>
                    <li><a href="https://www.cytonn.com">Cytonn Investments</a>. &copy{{ \Carbon\Carbon::now()->format('Y') }}</li>
                    <li>&#8226; <a href="https://cytonn.com/client-portal-terms">Terms of use</a></li>
                    <li>&#8226; <a href="https://cytonn.com/privacy-policy">Privacy policy</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection