@extends('layouts.default')

@section('content')
        <div class="row align-center">
            <div class="small-12 columns">
                <div class="login-panel">
                    <h3>Create Your Password</h3>

                    @if (count($errors) > 0)
                        <ul class="error">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open() !!}

                    <input class="" placeholder="Create password" name="password" type="password">

                    <input class="" placeholder="Repeat password" name="password_confirmation" type="password">

                    {!! Form::submit('Continue', ['class'=>'button login-button']) !!}

                    {!! Form::close() !!}
                </div>

                <div class="login-footer">
                    <ul>
                        <li><a href="https://www.cytonn.com">Cytonn Investments</a>. &copy{{ \Carbon\Carbon::now()->format('Y') }}</li>
                        <li>&#8226; <a href="https://cytonn.com/client-portal-terms">Terms of use</a></li>
                        <li>&#8226; <a href="https://cytonn.com/privacy-policy">Privacy policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
@endsection