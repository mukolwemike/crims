@extends('unitization.reports.index')

@section('content')

    @include('unitization.reports.statements.'.$calculation.'-view')

@stop