@extends('reports.letterhead')

@section('content')
    @if($view)
    @else
        <!-- Address -->
        {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

        {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

        @if($client->clientType->name == 'corporate')
            C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
        @endif
        (Client code – {!! $client->client_code !!})<br/>
        {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif

        <!-- RE -->
        <p><strong style="border-bottom: 2px solid #006B5A;">CONSOLIDATED {{ strtoupper($fund->category->name) }} STATEMENT AS AT {!! strtoupper(\Cytonn\Presenters\DatePresenter::formatDate($date)) !!}</strong></p>

        <!-- Body -->
        <p>{!! $fund->name !!} takes this opportunity to thank you for investing with us.</p>
        <p>Below is a summary of your investments with the {{ $fund->name }} to date.</p>
    @endif

    @include('unitization.reports.statements.'.$calculation)


    @if($view)
    @else
        <p>Once again, we thank you for choosing us to serve your investment needs and look forward to delivering to our investment promise.</p>

        {{--<p>Yours sincerely,</p>--}}
        <p><strong>{!! $fund->manager->fullname !!}</strong></p>

    @endif
@stop