<table class="table table-responsive" style="font-size: 13px">
    <thead>
    <tr style="color: #FFF; background-color: #006B5A">
        <th>Date</th>
        <th>Description</th>
        <th>Units</th>
        <th>Unit Price</th>
        <th>In</th>
        <th>Out</th>
        <th>Balance</th>
    </tr>
    </thead>
    <tbody>
    <?php $balance = 0; ?>
    @if($opening_balance != 0)
        <tr>
            <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($opening->date) }}</td>
            <td>Opening Balance</td>
            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($opening_balance, true, 0) }}</td>
            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($opening->price) }}</td>
            <td></td>
            <td></td>
            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($opening->price * $opening->balance, true, 0) }}</td>
        </tr>
    @endif
    @foreach($actions as $action)
        <tr>
            <td>{{ $action->date->toFormattedDateString() }}</td>
            <td> {{ $action->description }}</td>
            <td> {{ \Cytonn\Presenters\AmountPresenter::accounting($action->number, true, 0) }}</td>
            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($action->price) }}</td>
            <td>{{ $action->number >= 0 ? \Cytonn\Presenters\AmountPresenter::currency($action->price * $action->number, true, 0) : '' }}</td>
            <td>{{ $action->number < 0 ? \Cytonn\Presenters\AmountPresenter::currency($action->price * $action->number, true, 0) : '' }}</td>
            <td>{{ \Cytonn\Presenters\AmountPresenter::currency($action->price * $action->balance, true, 0) }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="7" height="14px"></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <th></th>
        <th>Total</th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency($opening_balance + $actions->sum->number, true, 0) }}</th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency($price) }}</th>
        <th></th>
        <th></th>
        <th>{{ \Cytonn\Presenters\AmountPresenter::currency($closing_balance * $price, true, 0) }}</th>
    </tr>
    </tfoot>
</table>