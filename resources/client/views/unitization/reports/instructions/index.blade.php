@extends('emails.default')

@section('content')
    <p>Dear {{ $client->present()->firstname }}, </p>

    <p>
        On behalf of {{ $unitFund->manager->fullname }},
        we would like to inform you that we have received a {{ $instruction->type->name }} of
        @if($instruction->type->slug === 'purchase')
            <b>KES {{ \Cytonn\Presenters\AmountPresenter::currency($instruction->amount)  }}</b>
        @else
            <b>{{ \Cytonn\Presenters\AmountPresenter::currency($instruction->number)  }} units</b>
        @endif
        in our Unit Trust product offering, specifically the <b>{{ $unitFund->name }}</b>.
    </p>

    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>

    <p>
        Kind regards,
    </p>

    <span class="bold-underline">Cytonn Asset Manager.</span>

@endsection