{!! Html::style(mix('/css/app.css')) !!}

{{--Import JS--}}
{!! Html::script('/assets/js/jquery.min.js') !!}

{!! Html::script('/assets/js/foundation.min.js') !!}

<style>
    #splash-overlay{
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: black;
        opacity: 0.8;
        z-index:1001;
    }
    #splash-overlay-content{
        position: absolute;
        display: none;
        width: 60vw;
        height: 140px;
        padding: 16px;
        border-bottom: 5px solid #006b5b;
        border-radius: 3px;
        background-color: white;
        z-index:1002;
    }
    .splash-loading{
        color: #a3cf5f;
        font-size: 30px;
    }
</style>

