<script>
    var ready = false;
    $.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (
            ($(window).height() - $(this).outerHeight()) / 2) +
            $(window).scrollTop()) + "px"
        );
        this.css("left", Math.max(0, (
            ($(window).width() - $(this).outerWidth()) / 2) +
            $(window).scrollLeft()) + "px"
        );
        return this;
    };

    displayError = function () {
        $("#unsupported-browser").fadeIn();
        $("#unsupported-browser-content").show().center();
    }


    removeErrorPopup = function () {
        $("#unsupported-browser").fadeOut();
    }

    setTimeout(function () {
        if(!ready) {

            if(notSupported.indexOf(whatBrowser()) !== -1) {
                $("#splash-overlay").fadeOut();
                displayError();
            } else {
                $("#splash-overlay").fadeIn();
                $("#splash-overlay-content").show().center();
            }
        }
    }, 200);

    setInterval(function(){
        if(ready) {
            $("#splash-overlay").hide();
        }
    }, 1500);

    $(document).on('vue_is_ready', function () {
        $("#splash-overlay").fadeOut();
        ready = true;
        removeErrorPopup();
    });

    setTimeout(function () {
        if(!ready) {
            $("#splash-overlay").fadeOut();
            displayError();
        }
    }, 120000);





    //add error popup html
    //add remove error popup function
</script>

<style>
    #unsupported-browser{
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: black;
        opacity: 0.9;
        z-index:1001;
        border-bottom: 2px solid #f44336;
    }
    #unsupported-browser-content{
        position: absolute;
        display: none;
        width: 60vw;
        height: 340px;
        padding: 16px;
        border-bottom: 5px solid #006b5b;
        border-radius: 3px;
        background-color: white;
        z-index:1002;
        text-align: center;
    }

    #unsupported-browser-content h2 {
        text-align: center;
        font-size: 20px;
        font-weight: 600;
    }
    .splash-loading{
        color: #a3cf5f;
        font-size: 30px;
    }
</style>