<div class="top-menu" ng-controller="TopMenuCtrl">
    <div class="row">
        <div class="small-12 medium-3 shrink columns">
            <span class="logo" ng-click="goToLanding()">
                <img height="50px" src="/assets/img/logo.svg">
            </span>
        </div>
        <div class="small-12 medium-9 columns">
            <div class="row" style="min-height: 50px">

                <span class="mobile-auth hide-for-medium" ng-controller="UserMenuCtrl">
                    <a v-if="!isAuthenticated" ui-sref="login"><i class="fa fa-sign-in"></i></a>
                    <a class="" v-if="isAuthenticated" ng-click="logout()"><i class="fa fa-sign-out"></i></a>
                </span>
                <!-- Left Menu -->
                <div class="columns hide-for-small">
                    <ul  v-if="isAuthenticated" class="vertical medium-horizontal menu pull-left">
                        <li ui-sref-active="menu-active"> <router-link :to="{ name: 'home'}"> Dashboard </router-link></li>
                        <li ng-if="isClient"><a dropdown-toggle="#dropdown-my-account">My Accounts</a>
                            <ul ng-controller="UserMenuCtrl"  class="dropdown-list" id="dropdown-my-account">
                                <li ng-repeat="client in user.access" ng-click="switchClient($event, client)"><a><% client.fullname %></a><i ng-show="client.id == selectedClient.id" class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                            </ul>
                        </li>
                        <li ng-if="isClient" ng-class="{ 'menu-active': $state.includes('investments')}"><router-link :to="{ name: 'investments.list'}">Investments</router-link></li>
                        <li ng-if="isClient" ui-sref-active="menu-active"><router-link :to="{ name: 'topup'}"><span><i class="fa fa-money"></i> Topup</span></router-link></li>
                        <li ng-if="isFa" ng-class="{'menu-active': $state.includes('fa')}"><router-link :to="{ name: 'fa.clients'}">My Clients</router-link></li>
                        <li><a v-foundation data-toggle="contactModal">Contact us</a></li>
                    </ul>

                    <ul ng-controller="UserMenuCtrl"  v-show="isAuthenticated" class="dropdown menu pull-right user-profile-button hide-for-small" data-close-on-click-inside="false" v-foundation data-dropdown-menu>
                        <dropdown>
                            <li slot="button"><i class="fa fa-user avatar"></i> <%currentUser.username%> <i class="fa fa-caret-down"></i></li>

                            <li><a href="#" ng-click="logout()">Log out now <i class="fa fa-sign-out"></i></a></li>
                        </dropdown>
                    </ul>

                    <ul v-show="!isAuthenticated"  class="dropdown menu pull-right" data-close-on-click-inside="false" v-foundation data-dropdown-menu>
                        <li ng-controller="ApplicationLandingController">
                            <a class="apply button">Apply</a>
                            <ul class="menu">
                                <li><router-link :to="{ name: 'apply.risk'}"  v-on:click="individual()">Individual</router-link> </li>
                                <li><router-link :to="{ name: 'apply.risk'}" v-on:click="corporate()">Corporate</router-link></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <my-account></my-account>
        </div>
    </div>

</div>

<div v-foundation class="contact-modal modal reveal " data-reveal id="contactModal" >
    <div class="primary title-bar">
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="center title">Talk to us</div>
    </div>
    <contact-modal></contact-modal>
</div>