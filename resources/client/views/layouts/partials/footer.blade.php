<div class="small-12 columns footer">
    <footer>
        <hr/>
        <span class="left">
                <p>&copy {{ \Carbon\Carbon::now()->format('Y') }} <a href="https://www.cytonn.com">Cytonn Investments</a>. All rights reserved </p>
            </span>

        <span class="right">
                <p><a href="https://cytonn.com/privacy-policy">Privacy policy</a></p>
                <p><a href="https://cytonn.com/client-portal-terms">Terms of use</a></p>
            </span>
    </footer>
</div>