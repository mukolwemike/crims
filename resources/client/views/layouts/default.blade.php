<!doctype html>
<html lang="en" ng-app="cytonn">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="/logo.ico?v=2" type="image/x-icon">


        <base href="/"></base>

        <title> Cytonn | Client Area  </title>

        @include('layouts.partials.import')

    </head>
    <body>
        <div  id="cyapp"  is="page-container" class="default" :class="{ 'landing_page_container': $route.path == '/','landing_login_page': $route.path == '/login' }">
            <div class="" >
                <top-bar></top-bar>
            </div>


            <div class="crims_main_content_area" id="cyapp">
                @yield('content')
            </div>


        </div>
        <script>
            $(document).foundation();
        </script>
        @include('layouts.partials.detect_browser')
        @include('layouts.partials.show_overlay')

        {!! Html::script(mix("/js/manifest.js")) !!}
        {!! Html::script(mix("/js/vendor.js")) !!}
        {!! Html::script(mix("/js/app.js")) !!}
    </body>

    @include('layouts.partials.late_import')
</html>