@extends('layouts.default')

@section('content')
    <div id="splash-overlay">
        <div id="splash-overlay-content">
            <div style="text-align: center">
                <img style="height: 50px" height="50px" src="/assets/img/logo.svg">
            </div>
            <br>
            <div>
                <p style="text-align: center"><i class="fa fa-spinner fa-spin splash-loading"></i></p>
            </div>
        </div>
    </div>

    <div id="unsupported-browser">
        <div id="unsupported-browser-content" style="display: none">
            <div style="text-align: center">
                <img style="height: 50px" height="50px" src="/assets/img/logo.svg">
            </div>
            <br>
            <div>
                <h2>Application could not load.</h2>
                <span  id="browser_is_supported">
                    <p><span>We experienced an error loading the application. Please try again.</span></p>
                    <i class="fa fa-spinner fa-spin splash-loading"></i>
                </span>
                <span id="browser_not_supported">
                    <p>CRIMS is currently not supported in your Browser</p>
                    <p>For better experience please try opening your application with the latest version of the following supported browsers:
                        <a href="https://www.google.com/chrome/">Chrome <i class="fa fa-chrome" aria-hidden="true"></i></a>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox  <i class="fa fa-firefox" aria-hidden="true"></i></a>
                        <a href="https://support.apple.com/downloads/safari">Safari <i class="fa fa-safari" aria-hidden="true"></i></a>
                        <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">Microsoft Edge <i class="fa fa-edge" aria-hidden="true"></i></a>
                    <p>
                        <i class="fa fa-exclamation-circle splash-loading" aria-hidden="true"></i>
                    </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            @if(session('message'))
                <session-message message="{{session('message')['message']}}"></session-message>
                {{--<div ng-show="hide" ng-controller="FlashMessageController">--}}
                    {{--<div class="alert-box {{ session('message')['type'] }} radius">--}}
                        {{--{{ session('message')['message'] }}--}}
                        {{--<a href="#" ng-click="close()" class="float-right close">&times;</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            @endif
        </div>

    </div>

    <crims-page>
        <transition name="fade">
            <router-view></router-view>
        </transition>
    </crims-page>

{{--<div class="grid-x main-body">--}}
    {{----}}
    {{--<div class="small-10 dashbody">--}}
        {{--<div class="dashboard-body" >--}}

            {{--<transition name="fade">--}}
                {{--<router-view></router-view>--}}
            {{--</transition>--}}

            {{--<div v-show="$route.path == '/' || $route.path == '/login'" class="small-12 columns footer" :class=" {'landing_login_footer': $route.path == '/'}">--}}
                {{--<footer-section></footer-section>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@endsection