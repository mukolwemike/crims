//grid page selection for custom pagination
app.directive(
    'pageSelect',
    function () {
        return {
            restrict: 'E',
            template: '<input type="text" class="select-page" ng-model="inputPage" ng-change="selectPage(inputPage)">',
            link: function (scope, element, attrs) {
                scope.$watch(
                    'currentPage',
                    function (c) {
                        scope.inputPage = c;
                    }
                );
            }
        };
    }
);
app.controller(
    'ContactsGridCtrl',
    ['$scope', 'Contacts', function ($scope, Contacts) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        var tbl = JSON.parse(localStorage.getItem('contacts'));

        if (tbl) {
            $scope.rowCollection = tbl;
        }


        Contacts.get()
        .success(
            function (data) {
                $scope.rowCollection = data;

                localStorage.setItem('contacts', JSON.stringify(data));
            }
        )
        .then();

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'ClientsGridCtrl',
    ['$scope', 'Clients', function ($scope, Clients) {

        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        var tbl = JSON.parse(localStorage.getItem('clients'));

        if (tbl) {
            $scope.rowCollection = tbl;
        }


        Clients.get()
        .success(
            function (data) {
                $scope.rowCollection = data;

                localStorage.setItem('clients', JSON.stringify(data));
            }
        )
        .then();

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'ApplicationsGridCtrl',
    ['$scope', 'Applications', function ($scope, Applications) {

        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        var tbl = JSON.parse(localStorage.getItem('applications'));

        if (tbl) {
            $scope.rowCollection = tbl;
        }


        Applications.get()
        .success(
            function (data) {
                $scope.rowCollection = data;

                localStorage.setItem('applications', JSON.stringify(data));
            }
        )
        .then(
            function ($data) {

            }
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

/*
 *@deprecated Use InvestmentsController instead.
 */
app.controller(
    'InvestmentsGridCtrl',
    ['$scope', 'Investments_old', function ($scope, Investments) {

        console.warn("InvestmentsGridCtrl is now deprecated");

        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        $scope.loading = true;
        $scope.failed = false;

        var invs;

        try {
            invs = JSON.parse(localStorage.getItem('client_bc_invs'));
        } catch (err) {
        }

        if (invs) {
            $scope.rowCollection = invs;
        }


        Investments.getActive()
        .then(
            function (response) {
                $scope.rowCollection = response.data;

                $scope.loading = false;

                localStorage.setItem('client_bc_invs', JSON.stringify(response.data));

            },
            function (response) {
                $scope.failed = true;
                $scope.loading = false;
            }
        )
        .then();

        Investments.get()
        .then(
            function (response) {
                $scope.rowCollection = response.data;
            },
            function (response){}
        )
        .then();

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'MaturityGridCtrl',
    ['$scope', 'Investments', function ($scope, Investments) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        Investments.get()
        .success(
            function (data) {
                $scope.data = data;

                var coll = [];
                for (c = 0; c< $scope.data.length; c++) {
                    if (data[c].withdrawn !== 1) {
                        data[c].date = data[c].maturity_date;
                        coll.push(data[c]);
                    }
                }
                $scope.rowCollection = coll;
            }
        )
        .then();





        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'UsersGridCtrl',
    ['$scope', 'Users', function ($scope, Users) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];


        Users.get()
        .success(
            function (data) {
                $scope.rowCollection = data;
            }
        )
        .then();

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'PermissionsGridCtrl',
    ['$scope', 'Permissions', function ($scope, Permissions) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];


        Permissions.get()
        .then(
            function (response) {
                $scope.rowCollection = response.data;
            }
        )
        .then();

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'RolesGridCtrl',
    ['$scope', 'Roles', function ($scope, Roles) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];


        Roles.get()
        .then(
            function (response) {
                $scope.rowCollection = response.data;
            }
        )
        .then();

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'InstitutionsGridCtrl',
    ['$scope', 'Institutions', function ($scope, Institutions) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];


        Institutions.get()
        .then(
            function (response) {
                $scope.rowCollection = response.data;
            }
        )
        .then();

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'PortfolioInvestmentsGridCtrl',
    ['$scope', 'PortfolioInvestments', function ($scope, PortfolioInvestments) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];


        $scope.$watch(
            function () {
                return $scope.currency;},
            function (oldValue, newValue) {
                       PortfolioInvestments.get(newValue)
                    .then(
                        function (response) {
                            $scope.rowCollection = response.data;

                            var totalInvestment = 0;
                            var totalInterest = 0;
                            var totalWithholding = 0;
                            var totalValue = 0;
                            for (var i= 0; i<$scope.rowCollection.length; i++) {
                                var value = $scope.rowCollection[i];
                                if (!(value.rolled === 1 || value.whithdrawn === 1)) {
                                    totalInvestment += parseFloat(value.amount);
                                    totalInterest +=parseFloat(value.gross_interest);
                                    totalWithholding+=parseFloat(value.withholding_tax);
                                    totalValue+=parseFloat(value.total_value);
                                }
                            }
                            $scope.total_investment = totalInvestment;
                            $scope.totalInterest = totalInterest;
                            $scope.totalWithholding = totalWithholding;
                            $scope.totalValue = totalValue;

                            $scope.displayedCollection = [].concat($scope.rowCollection);
                        }
                    )
                       .then();
            }
        );

    }]
);

app.controller(
    'CommissionGridCtrl',
    ['$scope', 'Commission', function ($scope, Commission) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        $scope.$watch(
            function () {
                return $scope.product_id;},
            function (oldValue, newValue) {
                if (newValue) {
                    Commission.get($scope.product_id)
                    .then(
                        function (response) {
                            $scope.rowCollection = response.data;
                        }
                    )
                       .then();
                }
            }
        );



        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'complianceGridCtrl',
    ['$scope', 'Applications', function ($scope, Applications) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        var apps = JSON.parse(localStorage.getItem('apps'));

        if (apps) {
            $scope.rowCollection = apps;
        }

        //TODO only first time applications should be fetched and checked for compliance

        Applications.get()
        .success(
            function (data) {
                $scope.rowCollection = data;

                localStorage.setItem('apps', JSON.stringify(data));
            }
        )
        .then(
            function ($data) {

            }
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'taxationGridCtrl',
    ['$scope', 'Taxation', function ($scope, Taxation) {

        $scope.status = {
            opened: false
        };

        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        $scope.getTableData = function (product_id) {
            var data = {
                _token: '',
                date: $scope.date_picked
            };
            Taxation.get(product_id, data).then(
                function (response) {
                    $scope.rowCollection = response.data;
                    $scope.displayedCollection = [].concat($scope.rowCollection);
                },
                function (){}
            );
        };

        $scope.$watchCollection(
            function () {
                return {date: $scope.date_picked, product: $scope.product_id}; },
            function (oldValue, newValue) {
                if ($scope.date_picked && $scope.product_id) {
                    $scope.getTableData($scope.product_id);
                }
            }
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'faPaymentGridCtrl',
    ['$scope', 'CommissionSchedule', function ($scope, CommissionSchedule) {
        $scope.itemsByPage=15;
        $scope.rowCollection = [];

        $scope.$watch(
            function () {
                return $scope.period; },
            function (newValue, oldValue) {
                if (newValue) {
                    CommissionSchedule.scheduleForFa($scope.fa_id, $scope.period, $scope.product_id).then(
                        function (response) {
                            $scope.rowCollection = response.data;
                            $scope.totalAmount = $scope.sum($scope.rowCollection, 'amount');
                        },
                        function (){}
                    );
                }
            }
        );

        $scope.getTotal = function () {
            var amounts = [];
            for (i=0; i<=$scope.displayedCollection.length; i++) {
                amounts.push($scope.displayedCollection[i].amount);
            }

        };

        $scope.sum = function (items, prop) {
            if (items === null) {
                return 0;
            }
            return items.reduce(
                function (a, b) {
                    return b[prop] === null ? a : a + parseFloat(b[prop]);
                },
                0
            );
        };

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);
app.controller(
    'fullScheduleGridCtrl',
    ['$scope', 'CommissionSchedule', function ($scope, CommissionSchedule) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        $scope.$watch(
            function () {
                return $scope.product_id; },
            function (oldValue, newValue) {
                if (newValue) {
                    CommissionSchedule.get($scope.product_id).then(
                        function (response) {
                            $scope.data = response.data;
                            $scope.rowCollection = response.data;

                        },
                        function (){}
                    );
                }
            }
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'recipientTimedScheduleCtrl',
    ['$scope', 'CommissionSchedule', function ($scope, CommissionSchedule) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        $scope.status = {
            opened: false
        };

        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.getTableData = function (product_id, data) {
            CommissionSchedule.getTimedFa(product_id, data).then(
                function (response) {
                    $scope.data = response.data;
                    $scope.rowCollection = response.data;
                    $scope.displayedCollection = [].concat($scope.rowCollection);
                },
                function (){}
            );
        };

        $scope.$watch(
            function () {
                return $scope.product_id; },
            function (oldValue, newValue) {
                if (newValue) {
                    var data = {
                        _token: '',
                        date: $scope.date_picked
                    };

                    $scope.getTableData($scope.product_id, data);
                }
            }
        );

        $scope.$watch(
            function () {
                return $scope.date_picked;},
            function (oldValue, newValue) {
                if (newValue !== oldValue) {
                    var data = {
                        _token: '',
                        date: $scope.date_picked
                    };
                    $scope.getTableData($scope.product_id, data);
                }
            }
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'interestScheduleGridCtrl',
    ['$scope', 'InterestSchedule', function ($scope, InterestSchedule) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        InterestSchedule.get().then(
            function (response) {
                $scope.data = response.data;
                $scope.rowCollection = response.data;

            },
            function (){}
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'clientPaymentGridCtrl',
    ['$scope', 'InterestSchedule', function ($scope, InterestSchedule) {
        $scope.itemsByPage=15;
        $scope.rowCollection = [];

        $scope.$watch(
            function () {
                return $scope.period; },
            function (newValue, oldValue) {
                if (newValue) {
                    InterestSchedule.scheduleForClient($scope.client_id, $scope.period).then(
                        function (response) {
                            $scope.rowCollection = response.data;
                            $scope.totalAmount = $scope.sum($scope.rowCollection, 'amount');
                        },
                        function (){}
                    );
                }
            }
        );


        $scope.getTotal = function () {
            var amounts = [];
            for (i=0; i<=$scope.displayedCollection.length; i++) {
                amounts.push($scope.displayedCollection[i].amount);
            }

        };

        $scope.sum = function (items, prop) {
            if (items === null) {
                return 0;
            }
            return items.reduce(
                function (a, b) {
                    return b[prop] === null ? a : a + parseFloat(b[prop]);
                },
                0
            );
        };

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'clientsTaxationRecordsGridCtrl',
    ['$scope', 'ClientsTaxationRecords', function ($scope, ClientsTaxationRecords) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        $scope.$watch(
            function () {
                return $scope.product_id; },
            function (oldValue,  newValue) {
                if (newValue) {
                    ClientsTaxationRecords.get($scope.product_id)
                    .then(
                        function (response) {
                            $scope.data = response.data;
                            $scope.rowCollection = response.data;

                        },
                        function (){}
                    );
                }
            }
        );

        $scope.$watch(
            function () {
                return $scope.displayedCollection;},
            function (oldValue, newvalue) {
                if (newvalue) {
                    var total = 0;
                    var invested_amount = 0;
                    var gross_interest = 0;
                    for (var i=0; i<$scope.displayedCollection.length; i++) {
                        total += parseFloat($scope.displayedCollection[i].amount);
                        invested_amount += parseFloat($scope.displayedCollection[i].invested_amount);
                        gross_interest += parseFloat($scope.displayedCollection[i].gross_interest);
                    }

                    $scope.total = total;
                    $scope.invested_amount = invested_amount;
                    $scope.gross_interest = gross_interest;
                }
            }
        );


        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);

app.controller(
    'cmsSummaryHistoryGridCtrl',
    ['$scope', 'CMSHistory', function ($scope, CMSHistory) {
        $scope.rowCollection = [];

        $scope.$watch(
            function () {
                return $scope.product_id; },
            function (oldValue, newValue) {
                if (newValue) {
                    CMSHistory.get($scope.product_id).then(
                        function (response) {
                            $scope.data = response.data;
                            $scope.rowCollection = response.data;

                        },
                        function (){}
                    );
                }
            }
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);

        $scope.$watch(
            function () {
                return $scope.displayedCollection; },
            function (oldValue, newValue) {
                if (newValue) {
                    var total_cost_val = 0;
                    var total_market_val = 0;
                    var total_mgt_fees = 0;
                    var total_custody_fees = 0;
                    for (var i=0; i < $scope.displayedCollection.length; i++) {
                        total_cost_val += parseFloat($scope.displayedCollection[i].total_cost_value);
                        total_market_val += parseFloat($scope.displayedCollection[i].total_market_value);
                        if ($scope.displayedCollection[i].total_management_fees !== null) {
                            total_mgt_fees += parseFloat($scope.displayedCollection[i].total_management_fees);
                        }

                        total_custody_fees  += parseFloat($scope.displayedCollection[i].total_custody_fees);
                    }
                    $scope.total_cost_value  = total_cost_val;
                    $scope.total_market_value = total_market_val;
                    $scope.total_mgt_fees = total_mgt_fees;
                    $scope.total_custody_fees = total_custody_fees;
                }
            }
        );
    }]
);

app.controller(
    'taxationInvestmentsDetailsCtrl',
    ['$scope', 'TaxationInvestmentDetails', function ($scope, TaxationInvestmentDetails) {
        $scope.itemsByPage=15;

        $scope.rowCollection = [];

        $scope.status = {
            opened: false
        };

        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.$watch(
            function () {
                return $scope.client_id; },
            function (newValue, oldValue) {
                if (newValue) {
                    TaxationInvestmentDetails.get($scope.client_id).then(
                        function (response) {
                            $scope.data = response.data;
                            $scope.rowCollection = response.data;

                            var invested_amount_total = 0;
                            for (var i=0; i<response.data.length; i++) {
                                invested_amount_total += parseFloat(response.data[i].amount);
                            }

                            $scope.invested_amount_total = invested_amount_total;

                        },
                        function (){}
                    );
                }
            }
        );

        //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
        $scope.displayedCollection = [].concat($scope.rowCollection);
    }]
);


app.controller(
    'investmentActivityGridCtrl',
    ['$scope', 'ActivityLog', function ($scope, ActivityLog) {

        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.statusSecond = {
            opened: false
        };
        $scope.openSecond = function ($event) {
            $scope.statusSecond.opened = true;
        };

        $scope.loading = true;
        $scope.failed = false;

        var acts = JSON.parse(localStorage.getItem('activity_log'));

        if (acts) {
            $scope.invCollection = acts.investments;
            $scope.confCollection = acts.confirmations;
            $scope.appCollection = acts.applications;
            $scope.instrCollection = acts.instructions;
            $scope.payCollection = acts.payment;

            $scope.loading = false;
        }

        $scope.updateData = function () {
            ActivityLog.getInvestments($scope.start, $scope.end)
            .then(
                function (response) {
                    $scope.activities = response.data;
                    var acts = $scope.activities;

                    $scope.invCollection = acts.investments;
                    $scope.confCollection = acts.confirmations;
                    $scope.appCollection = acts.applications;
                    $scope.instrCollection = acts.instructions;
                    $scope.payCollection = acts.payment;

                    localStorage.setItem('activity_log', JSON.stringify(response.data));

                    $scope.loading = false;
                },
                function (response) {
                    $scope.failed = true;
                    $scope.loading = false;
                }
            );
        };

        $scope.$watchCollection(
            function () {
                return { start: $scope.start, end: $scope.end}; },
            function (oldValue, newValue) {

                if ($scope.start && $scope.end) {
                    $scope.updateData();
                }
            }
        );

        $scope.invDisplayedCollection = [].concat($scope.invCollection);
        $scope.confDisplayedCollection = [].concat($scope.confCollection);
        $scope.appDisplayedCollection = [].concat($scope.appCollection);
        $scope.instrDisplayedCollection = [].concat($scope.instrCollection);
        $scope.payDisplayedCollection = [].concat($scope.payCollection);

    }]
);


