app.controller(
    'PopoverCtrl',
    ['$scope', function ($scope) {
        $scope.dynamicPopover = {
            placement : 'left'
        };
    }]
);
app.controller(
    'DatepickerCtrl',
    ['$scope', function ($scope) {
        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.statusSecond = {
            opened: false
        };
        $scope.openSecond = function ($event) {
            $scope.statusSecond.opened = true;
        };
    }]
);

app.controller(
    'pageCtrl',
    ['$scope', 'localStorageService', function ($scope, localStorageService) {
        $scope.minimised = localStorageService.get('sidebarMinimised');

        if (!$scope.minimised) {
            $scope.minimised = false;
        }

        $scope.toggleSidebar = function (event) {
            event.preventDefault();
            $scope.minimised = !$scope.minimised;
        };

        $scope.$watch(
            function () {
                return $scope.minimised;},
            function (oldValue, newValue) {
                if ($scope.minimised === true) {
                    localStorageService.set('sidebarMinimised', true);
                    $scope.minimiseicon = 'minimisedicon';
                    $scope.minimiselabel = 'minimisedlabel';
                    $scope.sidebarminimised = 'sidebarminimised overflow-visible';
                    $scope.contentsidebarminimised = 'contentsidebarminimised';
                    $scope.toggle_state = 'fa fa-angle-double-right';

                    //Add links
                    $scope.investments_menu_target = "/dashboard/investments";
                    $scope.users_menu_target = "/dashboard/portfolio";
                    $scope.portfolio_menu_target = "/dashboard/users";

                    try {
                        //minimise the menus
                        document.body.querySelector("#client_investments").classList.remove("in");
                        document.body.querySelector("#portfolio").classList.remove("in");
                        document.body.querySelector("#users").classList.remove("in");
                    } catch (err) {
                    }
                }
                if ($scope.minimised === false) {
                    localStorageService.set('sidebarMinimised', false);
                    $scope.minimiseicon = null;
                    $scope.minimiselabel = null;
                    $scope.sidebarminimised = "col-md-2 scrollable";
                    $scope.contentsidebarminimised = "col-md-10";

                    $scope.toggle_state = 'fa fa-angle-double-left';
                }
            }
        );
    }]
);

app.controller(
    'CollapseCtrl',
    ['$scope', function ($scope) {
        $scope.isCollapsed = true;
    }]
);