app.controller(
    'clientRollOverCtrl',
    ['$scope', '$rootScope', '$http', '$location', 'ModalService', 'moment', 'CommissionRecipient', 'Investments', 'CommissionRate',  function ($scope, $rootScope, $http, $location, ModalService, moment, CommissionRecipient, Investments, CommissionRate) {

        $scope.automatic_rollover = false;
        $scope.rates = null;
        $scope.interestRates = [];
        $scope.tenordays = null;
        $scope.commission_rate = 0;
        $scope.currentCommissionRate = 0;

        var url_array = $location.path().split('/');
        $scope.investment_id = url_array[url_array.length - 1];

        Investments.getInvestmentById($scope.investment_id)
        .then(
            function (response) {
                $scope.investment = response.data;
            },
            function (response) {
                console.log('error occurred while retrieving the investment');
            }
        );

        $scope.automaticRollover = function () {
            if ($scope.investment !== null && $scope.investment !== undefined) {
                $scope.automatic_rollover = true;
                $scope.investedAmount = 0;

                $scope.description = 'Automatic Rollover';
                $scope.tenor = 1;

                $investment_id = $scope.investment_id;

                Investments.getInterestRatesForProduct($scope.investment_id)
                .then(
                    function ($response) {
                        $scope.interestRates = $response.data.rates;
                        $scope.interest_rate = getInterestRate($scope.interestRates, $scope.tenor);
                    }
                );

                document.getElementById('reinvest_withdraw').checked = true;

                $scope.invested_date = moment($scope.investment.maturity_date).add(1, 'days');

                $scope.maturity_date = Investments.getNearestMonday(Investments.getMaturityDate($scope.tenor, $scope.invested_date));

                $scope.invested_date = $scope.invested_date.format('YYYY-MM-DD');

                $scope.tenordays = Investments.getDateDifference($scope.invested_date, $scope.maturity_date);
            }
        };

        function getInterestRate(rates, tenor)
        {
            var filtered_array = rates.filter(
                function (rate, index) {
                    return rate.tenor <= tenor;
                }
            );

            return parseInt(filtered_array[0].rate);
        }

        $scope.$watchGroup(
            ['tenor', 'interest_rate'],
            function () {

                if ($scope.tenor && $scope.rates) {
                    $scope.interest_rate = getInterestRate($scope.interestRates, $scope.tenor);
                }
            }
        );

        $scope.$watchGroup(
            ['tenor', 'invested_date'],
            function () {
                var invested_date = document.getElementById('invested_date').value;
                if ($scope.tenor && invested_date) {
                    $scope.maturity_date = Investments.getNearestMonday(Investments.getMaturityDate($scope.tenor, $scope.invested_date));

                    $scope.tenordays = Investments.getDateDifference($scope.invested_date, $scope.maturity_date);
                }

                if (! $scope.tenor) {
                    $scope.tenordays = null;
                }
            },
            true
        );

        $scope.$watch(
            'commission_recepient',
            function () {
                $scope.loading = true;

                CommissionRecipient.getIfHasZeroCommissionRate(document.getElementById('commission_recepient').value)
                .then(
                    function (response) {
                        $scope.zero_commission_rate = response.data;
                    },
                    function (response) {
                        console.log('error occurred while retrieving the zero commission rate status');
                    }
                )
                .finally(
                    function () {
                        $scope.loading = false;
                    }
                );

                CommissionRate.getRatesForProductAndRecipient($scope.product_id, $scope.commission_recepient)
                .then(
                    function (response) {
                        $scope.rates = response.data;
                        $scope.getSelectedCommissionRate();
                    },
                    function (response) {
                    }
                );
            },
            true
        );

        $scope.checkCommissionRateValue = function () {
            if ($scope.zero_commission_rate == 1) {
                return 0;
            }

            return $scope.commission_rate.rate;
        };

        $scope.getCommissionRate = function () {
            var commissionRate_id = document.getElementById("commission_rate");

            $scope.commission_rate = $scope.rates[commissionRate_id.selectedIndex];
        };

        $scope.getSelectedCommissionRate = function () {
            $scope.commission_rate = $scope.rates[0];
        };

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            $rootScope.reinvest = $scope.reinvest;
            $rootScope.interest_payment_interval = $scope.interest_payment_interval;
            $rootScope.interest_payment_date = $scope.interest_payment_date;
            $rootScope.investedAmount = $scope.investedAmount;
            $rootScope.description = $scope.description;
            $rootScope.interest_action_id = $scope.interest_action_id;
            $rootScope.zero_commission_rate = $scope.zero_commission_rate;
            $rootScope.automatic_rollover = $scope.automatic_rollover;
            $rootScope.interest_reinvest_tenor = $scope.interest_reinvest_tenor;
            $rootScope.commissionRateId = $scope.commission_rate.rate;
            ModalService.showModal(
                {
                    templateUrl: "rollover.htm", controller: "rolloverConfirmationController"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'rolloverConfirmationController',
    ['$scope', '$window', '$rootScope', function ($scope, $window, $rootScope) {
        $scope.investedDate = document.getElementById('invested_date').value;
        $scope.maturityDate = document.getElementById('maturity_date').value;
        $scope.interestRate = document.getElementById('rate').value;
        $scope.investedAmount = $rootScope.investedAmount;
        $scope.description = $rootScope.description;
        $scope.commissionRecipientId = document.getElementById('commission_recepient').value;
        $scope.interest_action_id = document.getElementById('interest_action_id').value;
        $scope.interest_reinvest_tenor = document.getElementById('interest_reinvest_tenor').value;
        $scope.commissionRateId = $rootScope.commissionRateId;
        $scope.interest_payment_date = $rootScope.interest_payment_date;
        $scope.interest_payment_interval = $rootScope.interest_payment_interval;
        $scope.automatic_rollover = $rootScope.automatic_rollover;
        $scope.on_call = document.getElementById('on_call').checked;

        $scope.value = document.getElementById('value').value;

        var recipient_id = document.getElementById("commission_recepient");
        $scope.commissionRecipient = recipient_id.options[recipient_id.selectedIndex].text;
        var commissionRate_id = document.getElementById("commission_rate");
        $scope.commissionRate = commissionRate_id.options === undefined ? 'Zero Commission Rate' : commissionRate_id.options[commissionRate_id.selectedIndex].text;
        var commissionStart =  document.getElementById('commission_start_date').value;
        $scope.commissionStartDate = commissionStart === '' ? null : commissionStart;
        var interest_payment_id =  document.getElementById('interest_payment_interval');
        $scope.interest_payment = interest_payment_id.options[interest_payment_id.selectedIndex].text;
        var interest_action = document.getElementById("interest_action_id");
        $scope.interestAction = interest_action.options[interest_action.selectedIndex].text;

        var reinvest_radios = document.getElementsByName('reinvest');

        for (var i = 0; i < reinvest_radios.length; i++) {
            if (reinvest_radios[i].checked) {
                $scope.reinvest = reinvest_radios[i].value;
            }
        }

        if ($scope.reinvest == 'reinvest') {
            $scope.withdraw_val = $scope.value - $scope.investedAmount;
            $scope.reinvest_val = $scope.investedAmount;
        } else {
            $scope.reinvest_val = $scope.value - $scope.investedAmount;
            $scope.withdraw_val = $scope.investedAmount;
        }
    }]
);

app.controller(
    'clientCombinedRolloverCtrl',
    ['$scope', 'ModalService', '$rootScope', 'moment', 'CommissionRecipient', 'Investments', 'CommissionRate', function ($scope, ModalService, $rootScope, moment, CommissionRecipient, Investments, CommissionRate) {

        $scope.tenordays = null;
        $scope.commission_rate = 0;
        $scope.currentCommissionRate = 0;

        $scope.$watchGroup(
            ['tenor', 'invested_date'],
            function () {
                var invested_date = document.getElementById('invested_date').value;

                if ($scope.tenor && invested_date) {
                    $scope.maturity_date = Investments.getNearestMonday(Investments.getMaturityDate($scope.tenor, invested_date));

                    $scope.tenordays = Investments.getDateDifference(invested_date, $scope.maturity_date);
                }

                if (! $scope.tenor) {
                    $scope.tenordays = null;
                }
            },
            true
        );

        $scope.$watch(
            'commission_recepient',
            function () {
                $scope.loading = true;
                CommissionRecipient.getIfHasZeroCommissionRate(document.getElementById('commission_recepient').value)
                .then(
                    function (response) {
                        $scope.zero_commission_rate = response.data;
                    },
                    function (response) {
                        console.log('error occurred while retrieving the zero commission rate status');
                    }
                )
                .finally(
                    function () {
                        $scope.loading = false;
                    }
                );

                CommissionRate.getRatesForProductAndRecipient($scope.product_id, $scope.commission_recepient)
                .then(
                    function (response) {
                        $scope.rates = response.data;
                        $scope.getSelectedCommissionRate();
                    },
                    function (response) {

                    }
                );
            },
            true
        );

        $scope.checkCommissionRateValue = function () {
            if ($scope.zero_commission_rate == 1) {
                return 0;
            }

            return $scope.commission_rate.rate;
        };

        $scope.getCommissionRate = function () {
            var commissionRate_id = document.getElementById("commission_rate");

            $scope.commission_rate = $scope.rates[commissionRate_id.selectedIndex];
        };

        $scope.getSelectedCommissionRate = function () {
            $scope.commission_rate = $scope.rates[0];
        };

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            $rootScope.previous_total = $scope.previous_total;

            $rootScope.reinvest = $scope.reinvest;
            $rootScope.interest_payment_interval = $scope.interest_payment_interval;
            $rootScope.interest_payment_date = $scope.interest_payment_date;
            $rootScope.interest_action_id = $scope.interest_action_id;
            $rootScope.interest_reinvest_tenor = $scope.interest_reinvest_tenor;
            $rootScope.description = $scope.description;
            $rootScope.investments = $scope.investments;
            $rootScope.commissionRateId = $scope.commission_rate.rate;

            ModalService.showModal(
                {
                    templateUrl: "rollover.htm", controller: "clientCombinedRolloverConfirmationCtrl"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };

    }]
);

app.controller(
    'clientCombinedRolloverConfirmationCtrl',
    ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.investedDate = document.getElementById('invested_date').value;
        $scope.interest_action_id = document.getElementById('interest_action_id').value;
        $scope.maturityDate = document.getElementById('maturity_date').value;
        $scope.interestRate = document.getElementById('rate').value;
        $scope.amount = parseFloat(document.getElementById('amount').value);
        $scope.commissionRecipientId = document.getElementById('commission_recepient').value;
        $scope.commissionRateId =  $rootScope.commissionRateId;
        $scope.interest_payment_date = $rootScope.interest_payment_date;
        $scope.description = $rootScope.description;
        $scope.interest_payment_interval = $rootScope.interest_payment_interval;
        $scope.investments = $rootScope.investments;
        $scope.on_call = document.getElementById('on_call').checked;
        $scope.interest_reinvest_tenor = document.getElementById('interest_reinvest_tenor').value;


        var recipient_id = document.getElementById("commission_recepient");
        $scope.commissionRecipient = recipient_id.options[recipient_id.selectedIndex].text;
        var commissionRate_id = document.getElementById("commission_rate");
        $scope.commissionRate = commissionRate_id.options === undefined ? 'Zero Commission Rate' : commissionRate_id.options[commissionRate_id.selectedIndex].text;
        var commissionStart =  document.getElementById('commission_start_date').value;
        $scope.commissionStartDate = commissionStart === '' ? null : commissionStart;
        var interest_payment_id =  document.getElementById('interest_payment_interval');
        $scope.interest_payment = interest_payment_id.options[interest_payment_id.selectedIndex].text;
        var interest_action = document.getElementById("interest_action_id");
        $scope.interestAction = interest_action.options[interest_action.selectedIndex].text;

        var reinvest_radios = document.getElementsByName('reinvest');

        for (var i = 0; i < reinvest_radios.length; i++) {
            if (reinvest_radios[i].checked) {
                $scope.reinvest = reinvest_radios[i].value;
            }
        }

        $scope.value = parseFloat($rootScope.previous_total);

        if ($scope.reinvest == 'reinvest') {
            $scope.topup = false;
            $scope.withdraw_val = $scope.value - $scope.amount;
            $scope.reinvest_val = $scope.amount;
        } else if ($scope.reinvest == 'topup') {
            $scope.topup = true;
            $scope.reinvest_val = $scope.value + $scope.amount;
            $scope.withdraw_val = 0;
        } else if ($scope.reinvest == 'withdraw') {//withdraw
            $scope.topup = false;
            $scope.reinvest_val = $scope.value - $scope.amount;
            $scope.withdraw_val = $scope.amount;
        }


    }]
);

app.controller(
    'editInvestmentCommissionCtrl',
    ['$scope', '$rootScope', '$http', '$location', 'ModalService', 'moment', 'CommissionRecipient', 'Investments', 'CommissionRate',  function ($scope, $rootScope, $http, $location, ModalService, moment, CommissionRecipient, Investments, CommissionRate) {

        $scope.commission_rate = 0;
        $scope.commission_rate_name = '';
        $scope.product_id = null;
        $scope.client_id = null;
        $scope.rate = {rate : 0, rate_name : ''};
        $scope.investment_type_id = 2;
        $scope.isParent = 0;

        $scope.$watchGroup(['commission_recepient'],
            function () {
                $scope.loadingRate = true;
                CommissionRate.getRecipientProductCommissionRate($scope.product_id, $scope.commission_recepient, $scope.invested_date, $scope.client_id, $scope.investment_type_id, $scope.isParent)
                    .then(function (response) {
                            $scope.rate = response.data;

                            if ($scope.commission_rate != 0 && $scope.commission_rate != parseFloat($scope.rate.rate)) {
                                $scope.edit_rate = true;
                                $scope.commission_rate_name = "Edited Rate - " + $scope.commission_rate + '%';
                            } else {
                                $scope.edit_rate = false;
                                $scope.commission_rate = parseFloat($scope.rate.rate);
                                $scope.commission_rate_name = $scope.rate.rate_name;
                            }

                            $scope.loadingRate = false;
                        },
                        function (response) {
                        }
                    );
            },
            true
        );

        $scope.$watch('commission_rate',
            function () {
                if ($scope.commission_rate != parseFloat($scope.rate.rate)) {
                    $scope.commission_rate_name = "Edited Rate - " + $scope.commission_rate + '%';
                }
            }
        );

        $scope.$watch('edit_rate',
            function () {
                if ($scope.edit_rate === false) {
                    $scope.commission_rate_name = $scope.rate.rate_name;
                    $scope.commission_rate = $scope.rate.rate;
                }
            }
        );
    }]
);

app.controller(
    'clientTopupCtrl',
    ['$scope', '$rootScope', 'ModalService', 'moment', 'CommissionRate', 'CommissionRecipient', 'Clients', 'Investments', function ($scope, $rootScope, ModalService, moment, CommissionRate, CommissionRecipient, Clients, Investments) {
        $scope.commission_rate = 0;
        $scope.commission_rate_name = '';
        $scope.tenordays = null;
        $scope.product_id = null;
        $scope.client_id = null;
        $scope.edit_rate = false;
        $scope.rate = {rate : 0, rate_name : ''};
        $scope.investment_type_id = 2;
        $scope.isParent = 0;

        $scope.$watchGroup(['tenor', 'invested_date'],
            function () {
                var invested_date = document.getElementById('invested_date').value;
                if ($scope.tenor && invested_date) {
                    $scope.maturity_date = Investments.getNearestMonday(Investments.getMaturityDate($scope.tenor, invested_date));

                    $scope.tenordays = Investments.getDateDifference(invested_date, $scope.maturity_date);
                }

                if (! $scope.tenor) {
                    $scope.tenordays = null;
                }
            },
            true
        );

        $scope.$watch('product_id',
            function () {
                return Clients.getProductPaymentsBalance($scope.client_id, $scope.product_id)
                .then(
                    function (response) {
                        $scope.payments_balance = response.data;
                    },
                    function () {}
                );
            }
        );

        $scope.$watchGroup(['commission_recepient', 'invested_date'],
            function () {
                $scope.loadingRate = true;
                CommissionRate.getRecipientProductCommissionRate($scope.product_id, $scope.commission_recepient,
                    $scope.invested_date, $scope.client_id, $scope.investment_type_id, $scope.isParent)
                    .then( function (response) {
                            $scope.rate = response.data;
                            $scope.commission_rate = $scope.rate.rate;
                            $scope.commission_rate_name = $scope.rate.rate_name;
                            $scope.loadingRate = false;
                        },
                        function (response) {
                        }
                    );
            },
            true
        );

        $scope.$watch('commission_rate',
            function () {
                if($scope.commission_rate != $scope.rate.rate)
                {
                    $scope.commission_rate_name = "Edited Rate - " + $scope.commission_rate + '%';
                }
            }
        );

        $scope.$watch('edit_rate',
            function () {
                if($scope.edit_rate == 0)
                {
                    $scope.commission_rate_name = $scope.rate.rate_name;
                    $scope.commission_rate = $scope.rate.rate;
                }
            }
        );

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            var recipient_id = document.getElementById("commission_recepient");
            $scope.commissionRecipient = recipient_id.options[recipient_id.selectedIndex].text;
            var monthlyContribution = document.getElementById("contribution_payment_interval");
            $scope.monthlyContribution = monthlyContribution.options[monthlyContribution.selectedIndex].text;
        };
    }]
);

app.controller(
    'topupConfirmationCtrl',
    ['$scope', '$window', '$rootScope', function ($scope, $window, $rootScope) {
        $scope.interestRate = $rootScope.rate;
        $scope.description = $rootScope.description;
        $scope.amount = $rootScope.topupAmount;
        $scope.product_id = $rootScope.product_id;
        $scope.interest_payment_date = $rootScope.interest_payment_date;
        $scope.interest_payment_interval = $rootScope.interest_payment_interval;
        $scope.maturityDate = document.getElementById('maturity_date').value;
        $scope.interest_action_id = document.getElementById('interest_action_id').value;
        $scope.interest_reinvest_tenor = document.getElementById('interest_reinvest_tenor').value;
        $scope.investedDate = document.getElementById('invested_date').value;
        $scope.commissionRecipientId = document.getElementById('commission_recepient').value;
        $scope.commissionRateId = $rootScope.commissionRateId;
        var recipient_id = document.getElementById("commission_recepient");
        $scope.commissionRecipient = recipient_id.options[recipient_id.selectedIndex].text;
        var commissionRate_id = document.getElementById("commission_rate");
        $scope.commissionRate = commissionRate_id.options === undefined ? 'Zero Commission Rate' : commissionRate_id.options[commissionRate_id.selectedIndex].text;
        var commissionStart =  document.getElementById('commission_start_date').value;
        $scope.commissionStartDate = commissionStart === '' ? null : commissionStart;
        var interest_payment_id =  document.getElementById('interest_payment_interval');
        $scope.interest_payment = interest_payment_id.options[interest_payment_id.selectedIndex].text;
        var interest_action = document.getElementById("interest_action_id");
        $scope.interestAction = interest_action.options[interest_action.selectedIndex].text;
        $scope.on_call = document.getElementById('on_call').checked;
    }]
);

app.controller(
    'portfolioInvestmentWithdrawalCtrl',
    ['$scope', 'ModalService', function ($scope, ModalService) {

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "portfoliowithdraw.htm", controller: "portfolioWithdrawConfirmationCtrl",
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'portfolioWithdrawConfirmationCtrl',
    ['$scope', '$window', function ($scope, $window) {
        $scope.endDate = document.getElementById('end_date').value;
        $scope.premature = document.getElementById('premature').checked;

        $scope.amount = document.getElementById('amount').value;
        $scope.partial = document.getElementById('partial').checked;

    }]
);

app.controller(
    'portfolioRolloverCtrl',
    ['$scope', '$rootScope', 'ModalService', function ($scope, $rootScope, ModalService) {

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            $rootScope.rate = $scope.interest_rate;
            $rootScope.description = $scope.description;
            $rootScope.investedAmount = $scope.amount;
            $rootScope.taxable = $scope.taxable;

            ModalService.showModal(
                {
                    templateUrl: "portfolioRollover.htm", controller: "portfolioRolloverConfirmationCtrl",
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'portfolioRolloverConfirmationCtrl',
    ['$scope', '$window', '$rootScope', function ($scope, $window, $rootScope) {
        $scope.interestRate = $rootScope.rate;
        $scope.description = $rootScope.description;
        $scope.amount = $rootScope.investedAmount;
        $scope.maturityDate = document.getElementById('maturity_date').value;
        $scope.investedDate = document.getElementById('invested_date').value;
        $scope.taxable = $rootScope.taxable;

        if ($scope.taxable === 0) {
            $scope.taxable_class = 'alert alert-danger';
            $scope.taxable_text = 'Zero rated (0%)';
        } else {
            $scope.taxable_text = 'Normal rate';
        }
    }]
);

app.controller(
    'addPortfolioInvestmentCtrl',
    ['$scope', '$rootScope', 'ModalService', function ($scope, $rootScope, ModalService) {

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            $rootScope.rate = $scope.interest_rate;
            $rootScope.description = $scope.description;
            $rootScope.investedAmount = $scope.amount;
            $rootScope.contactPerson = $scope.contact_person;
            $rootScope.tax_rate_id = $scope.tax_rate_id;
            $rootScope.taxable = $scope.taxable;

            ModalService.showModal(
                {
                    templateUrl: "addPortfolioInvestment.htm", controller: "addPortfolioConfirmationCtrl",
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'addPortfolioConfirmationCtrl',
    ['$scope', '$window', '$rootScope', 'CusodialAccount', function ($scope, $window, $rootScope, CusodialAccount) {
        // $scope.custodialAccount = document.getElementById('custodial_account_id').value;
        // $scope.investmentType = document.getElementById('type_id').value;
        $scope.maturityDate = document.getElementById('maturity_date').value;
        // $scope.investedDate = document.getElementById('invested_date').value;
        $scope.on_call = document.getElementById('on_call').checked;
        $scope.interestRate = $rootScope.rate;
        $scope.tax_rate_id = $rootScope.tax_rate_id;
        $scope.taxable = $rootScope.taxable;
        $scope.amount = $rootScope.investedAmount;
        $scope.contactPerson = $rootScope.contactPerson;
        $scope.description = $rootScope.description;



        // $scope.institution = document.getElementById('portfolio_investor_id').value;
        // $scope.fundType = document.getElementById('fund_type_id').value;



        $scope.overdraft = false;

        if ($scope.taxable === 0) {
            $scope.taxable_class = 'alert alert-danger';
            $scope.taxable_text = 'Zero rated (0%)';
        } else {
            $scope.taxable_text = 'Normal rate';
        }

        CusodialAccount.get($scope.custodialAccount)
        .then(
            function (response) {
                if ($scope.amount > response.data) {
                    $scope.overdraft = true;
                    $scope.is_overdraft = true;
                    $scope.balance = parseFloat($scope.amount) - parseFloat(response.data);
                }
            },
            function (){}
        );

        $scope.allowoverdraft = function () {
            $scope.overdraft = false;
        };

        // var institution_id = document.getElementById("portfolio_investor_id");
        // $scope.institutionText = institution_id.options[institution_id.selectedIndex].text;
        // var fundType_id = document.getElementById("fund_type_id");
        // var investmentType_id = document.getElementById("type_id");
        // $scope.fundTypeText = fundType_id.options[fundType_id.selectedIndex].text;
        // $scope.investmentTypeText = investmentType_id.options[investmentType_id.selectedIndex].text;
        // var custodial_account_id = document.getElementById("custodial_account_id");
        // $scope.custodialAccountText = custodial_account_id.options[custodial_account_id.selectedIndex].text;


    }]
);

app.controller(
    'approveCmsClientTxCtrl',
    ['$scope', function ($scope) {
        $scope.showedit = false;


        $scope.toggleEdit = function () {
            $scope.showedit = !$scope.showedit;
        };
    }]
);

app.controller(
    'investApplicationCtrl',
    ['$scope', '$rootScope', 'ModalService', 'moment', 'CommissionRecipient', 'Investments', 'CommissionRate', function ($scope, $rootScope, ModalService, moment, CommissionRecipient, Investments, CommissionRate) {

        $scope.tenordays = null;
        $scope.commission_recepient = '';
        $scope.commission_rate = 0;

        $scope.$watchGroup(
            ['tenor', 'invested_date'],
            function () {
                var invested_date = document.getElementById('invested_date').value;

                if ($scope.tenor && invested_date) {
                    $scope.maturity_date = Investments.getNearestMonday(Investments.getMaturityDate($scope.tenor, invested_date));

                    $scope.tenordays = Investments.getDateDifference(invested_date, $scope.maturity_date);
                }

                if (! $scope.tenor) {
                    $scope.tenordays = null;
                }
            },
            true
        );

        $scope.$watch(
            function () {
                return $scope.commission_recepient; },
            function (newValue, oldValue) {
                if (newValue) {
                    $scope.loading = true;

                    CommissionRecipient.getIfHasZeroCommissionRate(document.getElementById('commission_recepient').value)
                    .then(
                        function (response) {
                            $scope.zero_commission_rate = response.data;

                        },
                        function (response) {
                            console.log('error occurred while retrieving the zero commission rate status');
                        }
                    )
                        .finally(
                            function () {
                                $scope.loading = false;
                            }
                        );

                    CommissionRate.getRatesForProductAndRecipient($scope.product_id, $scope.commission_recepient)
                        .then(
                            function (response) {
                                $scope.rates = response.data;

                                $scope.other = {
                                    'name': 'Other',
                                };

                                $scope.rates.push($scope.other);
                                $scope.getSelectedCommissionRate();
                            },
                            function (response) {

                            }
                        );
                }
            }
        );

        $scope.$watch(
            function () {
                return $scope.product_id; },
            function (oldValue, newValue) {
                if (newValue) {
                    if ($scope.commission_recepient !== '') {
                        CommissionRate.getRatesForProductAndRecipient($scope.product_id, $scope.commission_recepient)
                        .then(
                            function (response) {
                                $scope.rates = response.data;
                                $scope.getSelectedCommissionRate();
                            },
                            function (response) {

                            }
                        );
                    }
                }
            }
        );

        $scope.getCommissionRate = function () {
            var commissionRate_id = document.getElementById("commission_rate");

            $scope.commission_rate = $scope.rates[commissionRate_id.selectedIndex];
        };

        $scope.checkCommissionRateValue = function () {
            if ($scope.zero_commission_rate == 1) {
                return 0;
            } else if ($scope.commission_rate.name == 'Other') {
                var comm = document.getElementById("commission_rate_input").value;

                return comm;
            }

            return $scope.commission_rate.rate;
        };

        $scope.getSelectedCommissionRate = function () {
            $scope.commission_rate = $scope.rates[0];
        };

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            $rootScope.rate = $scope.interest_rate;
            $rootScope.description = $scope.description;
            $rootScope.client_code = $scope.client_code;
            $rootScope.interest_action_id = $scope.interest_action_id;
            $rootScope.interest_payment_interval = $scope.interest_payment_interval;
            $rootScope.interest_payment_date = $scope.interest_payment_date;
            $rootScope.zero_commission_rate = $scope.zero_commission_rate;
            $rootScope.interest_reinvest_tenor = $scope.interest_reinvest_tenor;
            $rootScope.maturity_date = document.getElementById('maturity_date').value;
            $rootScope.invested_date = document.getElementById('invested_date').value;

            var recipient_id = document.getElementById("commission_recepient");
            $rootScope.recepientName = recipient_id.options[recipient_id.selectedIndex].text;
            var interest_payment_id =  document.getElementById('interest_payment_interval');
            $rootScope.interest_payment = interest_payment_id.options[interest_payment_id.selectedIndex].text;
            var interest_action = document.getElementById("interest_action_id");
            $rootScope.interestAction = interest_action.options[interest_action.selectedIndex].text;

            $rootScope.interest_reinvest_tenor = $scope.interest_reinvest_tenor;
            $rootScope.interest_payment_date = $scope.interest_payment_date;
            $rootScope.on_call = document.getElementById('on_call').checked;

            // ModalService.showModal({
            //     templateUrl: "investApplication.htm", controller: "investApplicationCtrlConfirmationCtrl"
            // }).then(function (modal) {
            //     modal.element.modal();
            // });
        };
    }]
);

app.controller(
    'investApplicationCtrlConfirmationCtrl',
    ['$scope', '$window', '$rootScope', function ($scope, $window, $rootScope) {
        $scope.interest_rate = $rootScope.rate;
        $scope.description = $rootScope.description;
        $scope.client_code = $rootScope.client_code;
        $scope.interest_action_id = document.getElementById('interest_action_id').value;
        $scope.interest_reinvest_tenor = document.getElementById('interest_reinvest_tenor').value;
        $scope.maturity_date = document.getElementById('maturity_date').value;
        $scope.invested_date = document.getElementById('invested_date').value;
        $scope.product_id = document.getElementById('product_id').value;
        $scope.commission_recepient = document.getElementById('commission_recepient').value;
        $scope.commission_rate = document.getElementById('commission_rate').value;
        $scope.interest_payment_date = $rootScope.interest_payment_date;
        $scope.interest_payment_interval = $rootScope.interest_payment_interval;
        var product_id = document.getElementById("product_id");
        $scope.productName = product_id.options[product_id.selectedIndex].text;
        var commission_recepient = document.getElementById("commission_recepient");
        $scope.recepientName = commission_recepient.options[commission_recepient.selectedIndex].text;
        var commission_rate = document.getElementById("commission_rate");
        $scope.commissionRateName = commission_rate.options[commission_rate.selectedIndex].text;
        var interest_payment_id =  document.getElementById('interest_payment_interval');
        $scope.interest_payment = interest_payment_id.options[interest_payment_id.selectedIndex].text;
        var interest_action = document.getElementById("interest_action_id");
        $scope.interestAction = interest_action.options[interest_action.selectedIndex].text;
        $scope.on_call = document.getElementById('on_call').checked;
    }]
);

app.controller(
    'coopInvestController',
    ['$scope', '$rootScope', '$http', '$location', 'ModalService', 'moment', 'CommissionRecipient', 'Investments', 'CommissionRate',  function ($scope, $rootScope, $http, $location, ModalService, moment, CommissionRecipient, Investments, CommissionRate) {

        $scope.commission_rate = 0;
        $scope.zero_commission_rate = 0;

        $scope.$watch(
            'commission_recepient',
            function () {
                $scope.loading = true;
                $scope.zero_commission_rate = 0;

                CommissionRecipient.getIfHasZeroCommissionRate(document.getElementById('commission_recepient').value)
                .then(
                    function (response) {
                        $scope.zero_commission_rate = response.data;
                    },
                    function (response) {
                        console.log('error occurred while retrieving the zero commission rate status');
                    }
                )
                .finally(
                    function () {
                        $scope.loading = false;
                    }
                );

                CommissionRate.getRatesForProductAndRecipient($scope.product_id, $scope.commission_recepient)
                .then(
                    function (response) {
                        $scope.rates = response.data;
                        $scope.getSelectedCommissionRate();
                    },
                    function (response) {
                    }
                );
            },
            true
        );

        $scope.checkCommissionRateValue = function () {
            if ($scope.zero_commission_rate == 1) {
                return 0;
            }

            return $scope.commission_rate.rate;
        };

        $scope.getCommissionRate = function () {
            var commissionRate_id = document.getElementById("commission_rate");

            $scope.commission_rate = $scope.rates[commissionRate_id.selectedIndex];
        };

        $scope.getSelectedCommissionRate = function () {
            $scope.commission_rate = $scope.rates[0];
        };
    }]
);

app.controller(
    'WithdrawalCtrl',
    ['$scope', '$rootScope', 'ModalService', 'InterestPayment', 'Investment', function ($scope, $rootScope, ModalService, InterestPayment, Investment) {
        $scope.penalty = 0;

        $scope.$watch(
            'end_date',
            function () {
                if ($scope.end_date === '' || $scope.end_date === undefined) {
                    return;
                }

                var d = moment($scope.end_date).format('YYYY-MM-DD').toString();

                InterestPayment.getInterestAvailableOnDate($scope.investment_id, d)
                .then(
                    function (response) {
                        $scope.interest = response.data;
                    }
                );
            }
        );

        $scope.withdrawType = 'Withdrawal';

        $scope.$watch(
            'type',
            function () {
                var name = document.getElementById('withdraw_type');
                $scope.withdrawType = name.options[name.selectedIndex].text;
            }
        );

        $scope.$watchGroup(
            ['premature', 'deduct_penalty', 'amount'],
            function () {
                if ($scope.end_date === '' || $scope.end_date === undefined) {
                    return;
                }

                if ($scope.deduct_penalty == 1 && $scope.premature == 1) {
                    var d = moment($scope.end_date).format('YYYY-MM-DD').toString();

                    Investment.penalty($scope.investment_id, d, $scope.penalty_percentage, $scope.amount)
                    .then(
                        function (result) {
                            $scope.penalty = result.data;
                        }
                    );
                } else {
                    $scope.penalty = 0;
                }
            }
        );
    }]
);

app.controller(
    'InterestPaymentConfirmCtrl',
    ['$scope', '$window', '$rootScope', '$filter', function ($scope, $window, $rootScope, $filter) {
        $scope.amount = $rootScope.amount;
        $scope.payment_date = document.getElementById('date').text;
    }]
);

app.service(
    'InterestPayment',
    ['$http', function ($http) {
        var i = {};

        i.getInterestAvailableOnDate = function (id, date) {
            return $http(
                {
                    url:'/api/investments/interest/'+id+'/calculate',
                    method: 'GET',
                    params: {date: date },
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        return i;
    }]
);

app.controller(
    'statementCtrl',
    ['$scope', '$http', '$window', '$rootScope', 'Statement', '$interval', function ($scope, $http, $window, $rootScope, Statement, $interval) {

        $scope.stmtsent = false;

        $scope.$watch(
            function () {
                return Statement.getInfo();},
            function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    $scope.stmtsent = newValue;
                }
            }
        );

        var process = $interval(
            function () {
                if ($scope.stmtsent) {
                    var batchId = Statement.getBatch().id;

                    $http.get('/api/investments/statements/batch/status/' + batchId).then(
                        function (response) {
                            $scope.progress = response.data;
                        },
                        function () {

                        }
                    );
                }
            },
            1000
        );


    }]
);

app.controller(
    'statementModalCtrl',
    ['$scope', '$http', '$window', '$location', '$rootScope', 'Statement', function ($scope, $http, $window, $location, $rootScope, Statement) {
        $scope.stmtsent = false;

        $scope.token = document.getElementsByName('_token')[0].value;

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.stmt_name.length === 0) {
                $window.alert('You need to select a batch to proceed');
                return;
            }

            var name = document.getElementById('stmt_name');
            $scope.name = name.options[name.selectedIndex].text;

            var data = {
                "_token": $scope.token,
                "id": $scope.stmt_name,
                "name": $scope.name
            };

            $http.post('/api/investments/statements/send', data).then(
                function (response) {
                    $scope.stmtsent = true;
                    Statement.sent = $scope.stmtsent;

                    $scope.batch = response.data;
                    Statement.setBatch(response.data);

                    $window.location.href = '/dashboard/investments/reports/statements/'+response.data.id;

                },
                function (response) {
                    $window.alert('Sending failed, check that the batch is approved try again');
                    $window.location.reload();
                }
            );
        };

        $scope.$watch(
            'stmtsent',
            function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    Statement.setInfo(newValue);
                }
            }
        );
    }]
);

app.controller(
    'approvePortfolioInvCtrl',
    ['$scope', function ($scope) {
        $scope.showedit = false;

        $scope.toggleEdit = function () {
            $scope.showedit = !$scope.showedit;
        };
    }]
);

app.controller(
    'statementStatusCtrl',
    ['$scope', '$http', '$interval', function ($scope, $http, $interval) {

        var process = $interval(
            function () {

                var batchId = $scope.batch_id;

                $http.get('/api/investments/statements/batch/status/' + batchId).then(
                    function (response) {
                        $scope.progress = response.data;

                        if ($scope.progress == 100) {
                            $scope.sent = true;
                        }
                    },
                    function () {

                    }
                );
            },
            1000
        );
    }]
);

app.controller(
    'clientSummaryCtrl',
    ['$scope', 'Persistence', 'DepositWeightedTenor', 'ClientWeightedTenor', function ($scope, Persistence, DepositWeightedTenor, ClientWeightedTenor) {
        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.statusTwo = {
            opened: false
        };
        $scope.openTwo = function ($event) {
            $scope.statusTwo.opened = true;
        };

        $scope.statusThree = {
            opened: false
        };
        $scope.openThree = function ($event) {
            $scope.statusThree.opened = true;
        };

        $scope.updatePersistence = function () {
            Persistence.get($scope.product_id, {date: $scope.persistencedate})
            .then(
                function (response) {
                    $scope.persistence = response.data;
                },
                function (){}
            );
        };

        $scope.$watch(
            function () {
                return $scope.persistencedate; },
            function (oldValue, newValue) {
                if (newValue) {
                    $scope.updatePersistence();
                }
            }
        );

        $scope.$watch(
            function () {
                return $scope.deposit_tenor_date;},
            function (newvalue, oldValue) {

                if (newvalue) {
                    DepositWeightedTenor.get($scope.product_id, { date: newvalue})
                    .then(
                        function (response) {
                            $scope.deposit_weighted_tenor = response.data;
                        },
                        function (){}
                    );
                }
            }
        );

        $scope.$watch(
            function () {
                return $scope.client_tenor_date; },
            function (oldValue, newValue) {
                if (newValue) {
                    ClientWeightedTenor.get($scope.product_id, { date: $scope.client_tenor_date})
                    .then(
                        function (response) {
                            $scope.client_weighted_tenor = response.data;
                        },
                        function (){}
                    );
                }
            }
        );
    }]
);

app.controller(
    'bankInstructionCtrl',
    ['$scope', '$rootScope', 'ModalService', function ($scope, $rootScope, ModalService) {

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $rootScope.amount = $scope.amount;
                $rootScope.first_signatory = $scope.first_signatory;
                $rootScope.second_signatory = $scope.second_signatory;
                $rootScope.investment_bal = $scope.investment_bal;
                $rootScope.client_name = $scope.client_name;
                $rootScope.client_code = $scope.client_code;

                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {


            ModalService.showModal(
                {
                    templateUrl: "bankInstruction.htm", controller: "bankInstructionModalCtrl"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'bankInstructionModalCtrl',
    ['$scope', '$window', '$rootScope', '$document', function ($scope, $window, $rootScope, $document) {
        $scope.first_signatory = $rootScope.first_signatory;
        $scope.second_signatory = $rootScope.second_signatory;
        $scope.amount = $rootScope.amount;
        $scope.investment_bal = $rootScope.investment_bal;

        $scope.client_name = $rootScope.client_name;
        $scope.client_code = $rootScope.client_code;

        var sig1 = document.getElementById("first_signatory");
        $scope.signatory_one_name = sig1.options[sig1.selectedIndex].text;

        var sig2 = document.getElementById("second_signatory");
        $scope.signatory_two_name = sig2.options[sig2.selectedIndex].text;
    }]
);

