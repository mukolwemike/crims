
app.controller(
    'UserRolesCtrl',
    [ '$scope', function ($scope) {
        $scope.isCollapsed = false;
    }]
);

app.controller(
    'UserPermissionsCtrl',
    ['$scope', function ($scope) {
        $scope.isCollapsed = true;
    }]
);

app.controller(
    'ClientApplicationDetailCtrl',
    ['$scope', function ($scope) {
        $scope.editcompleteness = false;

        $scope.editcompliance = false;

        $scope.toggleCompleteness = function () {
            $scope.editcompleteness = !$scope.editcompleteness;
        };

        $scope.toggleCompliance = function () {
            $scope.editcompliance = !$scope.editcompliance;
        };
    }]
);

app.controller(
    'contactForm',
    [ function ($scope, $http) {

        $scope.processForm = function () {
            console.log('processing');
        };
    }]
);


