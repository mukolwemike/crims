app.directive(
    'initModel',
    ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope[attrs.initModel] = element[0].value;
                element.attr('ng-model', attrs.initModel);
                element.removeAttr('init-model');

                element.attr('ng-change', attrs.initChange);
                element.removeAttr('init-change');
                $compile(element)(scope);
            }
        };
    }]
);
   
app.directive(
    'dmcDatepicker',
    [function () {
        var dir = {
            'restrict': 'A'
        };

        dir.link = function (scope, element, attrs) {
            element.attr('is-open', 'status.opened');

            if (!(attrs.datepickerPopup)) {
                attrs.$set('uib-datepicker-popup', 'yyyy-MM-dd');
            }
            element.attr('ng-focus', 'open($event)');
            element.attr('datepicker-options', "dateOptions");
        };

        dir.controller = ['$scope', function ($scope) {
            $scope.status = {
                opened: false
            };
            $scope.open = function () {
                $scope.status.opened = true;
            };

            $scope.dateOptions = {
                format: 'yyyy-MM-dd'
            };
        }];
    
        return dir;
    }]
);

app.directive(
    'toHtml',
    [ function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                el.html(scope.$eval(attrs.toHtml));
            }
        };
    }]
);

app.directive(
    'stringToNumber',
    [ function () {
        return {
            require: 'ngModel',

            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(
                    function (value) {
                        return '' + value;
                    }
                );
                ngModel.$formatters.push(
                    function (value) {
                        return parseFloat(value, 10);
                    }
                );
            }
        };
    }]
);
app.directive(
    'stDateRange',
    ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            require: '^stTable',
            scope: {
                before: '=',
                after: '='
            },
            templateUrl: 'stDateRange.htm',

            link: function (scope, element, attr, table) {

                var predicateName = attr.predicate;

                scope.rangeChanged = function () {

                    var query = {};

                    if (scope.before) {
                        query.before = scope.before;
                    }

                    if (scope.after) {
                        query.after = scope.after;
                    }
                    table.search(query, predicateName);
                };

                function open(before)
                {
                    return function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();

                        if (before) {
                            scope.isBeforeOpen = true;
                        } else {
                            scope.isAfterOpen = true;
                        }
                    };
                }

                scope.openBefore = open(true);
                scope.openAfter = open();
            }
        };
    }]
).directive(
    'stNumberRange',
    ['$timeout', function ($timeout) {
            return {
                restrict: 'E',
                require: '^stTable',
                scope: {
                    lower: '=',
                    higher: '='
                },
                templateUrl: 'stNumberRange.html',
                link: function (scope, element, attr, table) {
                    var predicateName = attr.predicate;

                    scope.rangeChanged = function () {
                        var query = {};

                        if (scope.lower) {
                            query.lower = scope.lower;
                        }

                        if (scope.higher) {
                            query.higher = scope.higher;
                        }
                        table.search(query, predicateName);
                    };
                }
        };
    }]
);

app.directive(
    'initialValue',
    function () {
        var removeIndent = function (str) {
            var result = "";
            if (str && typeof(str) === 'string') {
                var arr = str.split("\n");
                arr.forEach(
                    function (it) {
                        result += it.trim();
                        result += '\n';
                    }
                );
            }
            return result;
        };

        return{
            restrict: 'A',
            controller: ['$scope', '$element', '$attrs', '$parse', function ($scope, $element, $attrs, $parse) {

                var getter, setter, val, tag, values;
                tag = $element[0].tagName.toLowerCase();
                val = $attrs.initialValue || removeIndent($element.val());

                if (tag === 'input') {
                    if ($element.attr('type') === 'checkbox') {
                        val = $element[0].checked;
                    } else if ($element.attr('type') === 'radio') {
                        val = ($element[0].checked || $element.attr('selected') !== undefined) ? $element.val() : undefined;
                    } else if ($element.attr('type') === 'number') {
                        val = ($element.val() !== undefined) ? parseFloat($element.val()) : undefined;
                    } else if ($element.attr('type') === 'color' || $element.attr('type') === 'range') {
                        val = $element[0].getAttribute('value');
                    } else if ($element.attr('type') === 'date') {
                        val = new Date(val.trim());
                    }
                } else if (tag === "select") {
                    values = [];
                    for (i=0; i < $element[0].options.length; i++) {
                        option = $element[0].options[i];
                        if (option.hasAttribute('selected') && $element[0].hasAttribute('multiple')) {
                            values.push(option.text);
                        } else {
                            val = option.text;
                        }
                    }
                }

                if ($attrs.ngModel) {
                    getter = $parse($attrs.ngModel);
                    setter = getter.assign;
                    setter($scope, values !== undefined && values.length ? values : val);
                }
            }]
        };
    }
);


app.directive(
    'jqdatepicker',
    function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                $(element).datepicker(
                    {
                        dateFormat: 'yy-M-dd',
                        onSelect: function (date) {
                            scope.date = date;
                            //element.value = date;
                            scope.$apply();
                        }
                    }
                );
            }
        };
    }
);

app.directive(
    'formatter',
    ['$filter', function ($filter) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) {
                    return;
                }

                ctrl.$formatters.unshift(
                    function (a) {
                        return $filter(attrs.formatter)(ctrl.$modelValue);
                    }
                );

                ctrl.$parsers.unshift(
                    function (viewValue) {
                        var plainNumber = viewValue.replace(/[^\d|\-+|\+.]/g, '');
                        elem.val($filter(attrs.formatter)(plainNumber));
                        return plainNumber;
                    }
                );
            }
        };
    }]
);

app.directive(
    'menuTile',
    [function () {
        return {
            restrict: 'E',
            transclude: true,
            scope: true,
            link: function (scope, elem, attrs, ctrl) {
                scope.icon = attrs.icon;
                scope.link = attrs.link;
                scope.text = attrs.text;

                if (attrs.count) {
                    scope.count = attrs.count;
                }
            },
            template: '<div class="col-md-2 panel grid-menu">' +
            '<a href="<% link %>"><i class="fa <% icon %> fa-5x"></i></a> ' +
            '<div class="panel-footer" ng-transclude=""></div> ' +
            '</div>'
        };
    }]
);
  
app.directive(
    'clickedDisable',
    function () {
        return {
            restrict: 'A',
            link: function (scope, ele, attrs) {

                var ln = $(ele);
                var disable_element  = function(element) {
                    $(element).attr('disabled', true);

                    setTimeout(function () {
                            $(element).attr('disabled', false);
                        },
                        5*1000
                    );
                };

                ln.closest('form').on('submit', function (e) {
                    disable_element(ele)
                });

                if(ln.is('a')) {
                    ln.on('click', function (e) {
                        disable_element(ele);
                    });
                }
            }
        };
    }
);

