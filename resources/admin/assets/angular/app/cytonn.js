/*! Copyright (c) 2015 Cytonn Investments Management Ltd.  All Rights Reserved. */

var app = angular.module(
    'cytonn',
    ['autocomplete','ngAnimate', 'angularModalService','smart-table', 'ui.bootstrap', 'LocalStorageModule', 'angularMoment', 'chart.js', 'ui.select2', 'summernote', 'ngSanitize'],
    [ '$interpolateProvider', '$locationProvider', function ($interpolateProvider, $locationProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
        $locationProvider.html5Mode(
            {
                enabled: true,
                requireBase: false
            }
        );
    }]
);


app.config(
    ['$httpProvider', 'uibDatepickerConfig', 'uibDatepickerPopupConfig', '$uibTooltipProvider',  function ($httpProvider, uibDatepickerConfig, uibDatepickerPopupConfig, $uibTooltipProvider) {
        //$httpProvider.useApplyAsync(true);
        uibDatepickerConfig.datepickerPopup = "yyyy-MM-dd";
        uibDatepickerPopupConfig.datepickerPopup = "yyyy-MM-dd";
        $uibTooltipProvider.options({ placement: 'left'});

    }]
);

app.run(
    function () {
        //clean up
        localStorage.removeItem('client_invs');
    }
);

app.directive(
    'initModel',
    ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope[attrs.initModel] = element[0].value;
                element.attr('ng-model', attrs.initModel);
                element.removeAttr('init-model');

                element.attr('ng-change', attrs.initChange);
                element.removeAttr('init-change');
                $compile(element)(scope);
            }
        };
    }]
);

app.directive(
    'toHtml',
    [ function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                el.html(scope.$eval(attrs.toHtml));
            }
        };
    }]
);

app.directive(
    'stringToNumber',
    [ function () {
        return {
            require: 'ngModel',

            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(
                    function (value) {
                        return '' + value;
                    }
                );
                ngModel.$formatters.push(
                    function (value) {
                        return parseFloat(value, 10);
                    }
                );
            }
        };
    }]
);

app.directive(
    'initialValue',
    function () {
        var removeIndent = function (str) {
            var result = "";
            if (str && typeof(str) === 'string') {
                var arr = str.split("\n");
                arr.forEach(
                    function (it) {
                        result += it.trim();
                        result += '\n';
                    }
                );
            }
            return result;
        };

        return{
            restrict: 'A',
            controller: ['$scope', '$element', '$attrs', '$parse', function ($scope, $element, $attrs, $parse) {

                var getter, setter, val, tag, values;
                tag = $element[0].tagName.toLowerCase();
                val = $attrs.initialValue || removeIndent($element.val());

                if (tag === 'input') {
                    if ($element.attr('type') === 'checkbox') {
                        val = $element[0].checked;
                    } else if ($element.attr('type') === 'radio') {
                        val = ($element[0].checked || $element.attr('selected') !== undefined) ? $element.val() : undefined;
                    } else if ($element.attr('type') === 'number') {
                        val = ($element.val() !== undefined) ? parseFloat($element.val()) : undefined;
                    } else if ($element.attr('type') === 'color' || $element.attr('type') === 'range') {
                        val = $element[0].getAttribute('value');
                    } else if ($element.attr('type') === 'date') {
                        val = new Date(val.trim());
                    }
                } else if (tag === "select") {
                    values = [];
                    for (i=0; i < $element[0].options.length; i++) {
                        option = $element[0].options[i];
                        if (option.hasAttribute('selected') && $element[0].hasAttribute('multiple')) {
                            values.push(option.text);
                        } else {
                            val = option.text;
                        }
                    }
                }

                if ($attrs.ngModel) {
                    getter = $parse($attrs.ngModel);
                    setter = getter.assign;
                    setter($scope, values !== undefined && values.length ? values : val);
                }
            }]
        };
    }
);

//Check on whether the current use session is active or not
app.controller(
    'loginCheckCtrl',
    ['$scope', '$interval', '$http', '$window', function ($scope, $interval, $http, $window) {
        var process = $interval(
            function () {
                console.log('Checking');
                $http.get('/api/login/check')
                .then(
                    function (response) {
                        if (response.data != 1) {
                            console.log('User logged out');
                            $window.location.reload();
                        }
                    },
                    function () {
                    }
                );
            },
            11*60*1000
        );
    }]
);

/* commonjs package manager support (eg componentjs) */
if (typeof module !== "undefined" && typeof exports !== "undefined" && module.exports === exports) {
    module.exports = initialValueModule;
}

