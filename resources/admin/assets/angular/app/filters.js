app.filter(
    'capitalizeFirst',
    function () {
        return function (input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        };
    }
);

app.filter(
    'applicationStatus',
    function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input == 'invested') {
                out = '<span class="label label-success">Invested</span>';
            } else {
                out = '<span class="label label-danger">Not Invested</span>';
            }
            return out;
        };
    }
);

app.filter(
    'asLabel',
    function () {
        return function (input, trueWord, falseWord) {
            input = input || false;

            if (input) {
                return '<span class="label label-success">' + trueWord + '</span>';
            }

            return '<span class="label label-danger">' + falseWord + '</span>';
        };
    }
);

app.filter(
    'unitStatus',
    function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input) {
                out = '<span class="label label-success">Taken</span>';
            } else {
                out = '<span class="label label-danger">Available</span>';
            }
            return out;
        };
    }
);

app.filter(
    'clientApplicationStatus',
    function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input === true || input == 'true') {
                out = '<span class="label label-success">Used</span>';
            } else {
                out = '<span class="label label-danger">Not Used</span>';
            }
            return out;
        };
    }
);

app.filter(
    'clientPaymentInstruction',
    function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input === true || input == 'true') {
                out = '<span class="label label-success">Used</span>';
            } else {
                out = '<span class="label label-danger">Not Used</span>';
            }
            return out;
        };
    }
);

app.filter(
    'status',
    function () {
        return function (input, word) {
            input = input || '';

            var out = '';
            if (input === true || input == 'true') {
                out = '<span class="label label-success">' + word + '</span>';
            } else {
                out = '<span class="label label-danger">Not ' + word + '</span>';
            }
            return out;
        };
    }
);


app.filter(
    'investmentStatus',
    function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input.rolled == '1') {
                out = '<span class="label label-warning">Rolled over</span>';
            } else if (input.withdrawn == '1') {
                out = '<span class="label label-danger">Withdrawn</span>';
            } else {
                out = '<span class="label label-success">Active</span>';
            }
            return out;
        };
    }
);

app.filter(
    'complianceStatus',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input == '1') {
                out = '<span class="label label-success">Compliant</span>';
            } else {
                out = '<span class="label label-danger">Non-Compliant</span>';
            }

            return out;
        };
    }]
);

app.filter(
    'validatedStatus',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input) {
                out = '<span class="label label-success">Validated</span>';
            } else {
                out = '<span class="label label-danger">Not-Validated</span>';
            }

            return out;
        };
    }]
);

app.filter(
    'activeStatus',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input == '1') {
                out = '<span class="label label-success">active</span>';
            } else {
                out = '<span class="label label-danger">inactive</span>';
            }

            return out;
        };
    }]
);

app.filter(
    'boolUnitFundInvestedStatus',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input == '1' || input === true) {
                out = '<span class="label label-success">Invested</span>';
            } else if (input == 'cancelled' || input === true) {
                out = '<span class="label label-danger">Cancelled</span>';
            } else {
                out = '<span class="label label-warning">Not Invested</span>';
            }

            return out;
        };
    }]
);

app.filter(
    'instructionPlatformOrigin',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input === 'web') {
                out = '<span class="label label-info">WEB</span>';
            } else if (input === 'mpesa') {
                out = '<span class="label label-success">MPESA</span>';
            } else if (input === 'mobile') {
                out = '<span class="label label-danger">MOBILE</span>';
            } else if (input === 'admin') {
                out = '<span class="label label-primary">ADMIN</span>';
            } else if (input === 'ussd') {
                out = '<span class="label label-warning">USSD</span>';
            } else if (input === 'client') {
                out = '<span class="label label-info">CLIENT</span>';
            } else {
                out = '<span class="label label-info"></span>';
            }

            return out;
        }
    }]
);

app.filter(
    'boolUtilityBillingStatus',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input === 1) {
                out = '<span class="label label-default">Pending</span>';
            } else if (input === 2) {
                out = '<span class="label label-info">Submitted</span>';
            } else if (input === 3) {
                out = '<span class="label label-success">Completed</span>';
            } else if (input === 4) {
                out = '<span class="label label-danger">Cancelled</span>';
            }

            return out;
        };
    }]
);

app.filter(
    'boolClientApprovalStatus',
    [function () {
        return function (input) {

            input = input || '';

            var out = '';

            if (input == '1' || input === true) {
                out = '<span class="label label-success"><i class="fa fa-check-circle"></i></span>';
            } else {
                out = '<span class="label label-danger"><i class="fa fa-close"></i></span>';
            }

            return out;
        };
    }]
);

app.filter(
    'boolInactiveStatus',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input == '1' || input === true) {
                out = '<span class="label label-danger">Cancelled</span>';
            } else {
                out = '<span class="label label-success">Active</span>';
            }

            return out;
        };
    }]
);

app.filter(
    'boolInvestedStatus',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input == '1' || input === true) {
                out = '<span class="label label-success">Invested</span>';
            } else {
                out = '<span class="label label-danger">Not Invested</span>';
            }

            return out;
        };
    }]
);

app.filter(
    'boolClientApprovalStatus',
    [function () {
        return function (input) {

            input = input || '';

            var out = '';

            if (input == '1' || input === true) {
                out = '<span class="label label-success"><i class="fa fa-check-circle"></i></span>';
            } else {
                out = '<span class="label label-danger"><i class="fa fa-close"></i></span>';
            }

            return out;
        };
    }]
);

app.filter(
    'booleanIcon',
    [function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input == '1') {
                out = '<i class="fa fa-check-square-o color-success"></i>';
            } else {
                out = '<i class="fa fa-close color-danger"></i>';
            }

            return out;
        };
    }]
);

app.filter(
    'origin',
    function () {
        return function (input, trueWord, falseWord) {
            input = input || false;

            if (input) {
                return '<span class="label color-client">' + trueWord + '</span>';
            }

            return '<span class="label color-admin">' + falseWord + '</span>';
        };
    }
);

app.filter(
    'yesno',
    [function () {
        return function (input) {
            if (input === true || input == '1' || input == 1) {
                return 'Yes';
            }

            return 'No';
        };
    }]
);

app.filter(
    'str_lim',
    [function () {
        return function (input, length) {
            input = input || '';
            var out = '';

            if (input.length > length) {
                out = input.substr(0, length) + '...';
            } else {
                out = input;
            }

            return out;
        };
    }]
);

app.filter(
    'confirmationStatus',
    [function () {
        return function (input) {
            input = input || '';

            if (input === 1 || input === true) {
                return '<span  class="label label-success">Sent</span>';
            } else {
                return '<span  class="label label-danger">Not Sent</span>';
            }
        };
    }]
);


app.filter(
    'portfolioInvestmentStatus',
    function () {
        return function (input) {
            input = input || '';
            var out = '';
            if (input.rolled == '1') {
                out = '<span class="label label-warning">Rolled over</span>';
            } else if (input.withdrawn == '1') {
                out = '<span class="label label-danger">Redeemed</span>';
            } else {
                out = '<span class="label label-success">Active</span>';
            }
            return out;
        };
    }
);

app.filter(
    'customRangeFilter',
    ['$filter', function ($filter) {
        var filterFilter = $filter('filter');
        var standardComparator = function standardComparator(obj, text) {
            text = ('' + text).toLowerCase();
            return ('' + obj).toLowerCase().indexOf(text) > -1;
        };

        return function customFilter(array, expression) {
            function customComparator(actual, expected) {

                var isBeforeActivated = expected.before;
                var isAfterActivated = expected.after;
                var isLower = expected.lower;
                var isHigher = expected.higher;
                var higherLimit;
                var lowerLimit;
                var itemDate;
                var queryDate;

                if (angular.isObject(expected)) {
                    //date range
                    if (expected.before || expected.after) {
                        try {
                            if (isBeforeActivated) {
                                higherLimit = expected.before;

                                itemDate = new Date(actual);
                                queryDate = new Date(higherLimit);

                                if (itemDate > queryDate) {
                                    return false;
                                }
                            }

                            if (isAfterActivated) {
                                lowerLimit = expected.after;


                                itemDate = new Date(actual);
                                queryDate = new Date(lowerLimit);

                                if (itemDate < queryDate) {
                                    return false;
                                }
                            }

                            return true;
                        } catch (e) {
                            return false;
                        }
                    } else if (isLower || isHigher) {
                        //number range
                        if (isLower) {
                            higherLimit = expected.lower;

                            if (actual > higherLimit) {
                                return false;
                            }
                        }

                        if (isHigher) {
                            lowerLimit = expected.higher;
                            if (actual < lowerLimit) {
                                return false;
                            }
                        }

                        return true;
                    }
                    //etc

                    return true;
                }
                return standardComparator(actual, expected);
            }

            var output = filterFilter(array, expression, customComparator);
            return output;
        };
    }]
);

app.filter(
    'unique',
    function () {
        return function (arr, field) {
            var o = {}, i, l = arr.length, r = [];
            for (i = 0; i < l; i += 1) {
                o[arr[i][field]] = arr[i];
            }
            for (i in o) {
                r.push(o[i]);
            }
            return r;
        };
    }
);

app.filter(
    'abs',
    function () {
        return function (val) {
            return Math.abs(val);
        };
    }
);
