app.controller(
    'CustodialDepositController',
    ['$scope', '$rootScope', 'ModalService', function ($scope, $rootScope, ModalService) {
        $scope.submit = function (event) {
            event.preventDefault();

            $rootScope.amount = $scope.amount;
            $rootScope.narrative = $scope.narrative;
            $rootScope.deposit_type = $scope.deposit_type;

            if ($scope.depositForm.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "deposit.htm", controller: "CustodialDepositConfirmController"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'CustodialDepositConfirmController',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.amount = $rootScope.amount;
        $scope.narrative = $rootScope.narrative;
        $scope.deposit_date = document.getElementById('deposit_date').value;
        var depositInput = document.getElementById("deposit_type");
        $scope.deposit_type_name = depositInput.options[depositInput.selectedIndex].text;
    }]
);