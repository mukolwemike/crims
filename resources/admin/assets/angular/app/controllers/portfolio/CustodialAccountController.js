/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 14/12/2016.
 * Project crims
 */

app.controller(
    "CustodialAccountController",
    ['$scope', 'CustodialAccount', 'TableState', function ($scope, CustodialAccount, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, CustodialAccount.getTransactions(tableState,  $scope.account_id));
        };

        $scope.$watch(
            'displayed',
            function () {
                var idx = $scope.displayed.length - 1;

                if (idx >= 0) {
                    var last = $scope.displayed[idx];

                    if (last) {
                        $scope.opening_balance = parseFloat(last.balance) - parseFloat(last.amount);
                    }
                }

            }
        );
    }]
);