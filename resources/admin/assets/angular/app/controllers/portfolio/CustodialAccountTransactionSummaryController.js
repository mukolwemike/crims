/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 27/03/2017.
 * Project crims
 */
app.controller(
    'CustodialAccountTransactionSummaryController',
    ['$scope', 'CustodialAccount', function ($scope, CustodialAccount) {

        $scope.summary = {};
        $scope.loading = false;

        $scope.fetchSummary = function (event) {
            if (event) {
                event.preventDefault();
            }
            $scope.loading = true;

            CustodialAccount.fetchSummary($scope.account_id, $scope.start, $scope.end)
            .then(
                function (response) {
                    $scope.summary = response.data;
                    $scope.loading = false;
                },
                function () {
                    $scope.loading = false;
                }
            );
        };

        $scope.$watch(
            'account_id',
            function () {
                if ($scope.account_id) {
                    $scope.fetchSummary();
                }
            }
        );

    }]
);