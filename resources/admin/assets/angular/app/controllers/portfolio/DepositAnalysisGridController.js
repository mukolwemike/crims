
app.controller('DepositAnalysisGridController', ['$scope', 'DepositAnalysis', 'CashDepositAnalysis', function ($scope, DepositAnalysis, CashDepositAnalysis) {

    $scope.displayed = [];
    $scope.total_principal = 0;
    $scope.total_interest = 0;
    $scope.total_adjusted_gross_interest = 0;
    $scope.total_amount = 0;
    $scope.total_market_value = 0;

    $scope.callServer = function callServer(tableState)
    {

        $scope.isLoading = true;

        var pagination = tableState.pagination;
        var start = pagination.start || 0;
        var number = pagination.number || 10;

        DepositAnalysis.getPage(tableState, $scope.currency_id).then(function (result) {
            $scope.total_principal = 0;
            $scope.total_interest = 0;
            $scope.total_adjusted_gross_interest = 0;
            $scope.total_amount = 0;
            $scope.total_market_value = 0;

            $scope.displayed = result.data.data;
            $scope.meta = result.data.meta;
            $scope.total = result.data.total;

            $scope.displayed.forEach(function (row, key) {
                $scope.total_principal += row.principal;
                $scope.total_interest += row.interest;
                $scope.total_adjusted_gross_interest += row.gross_interest;
                $scope.total_amount += row.amount;
                $scope.total_market_value += row.market_value;
            });

            // tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;
            //
            // $scope.perPage = tableState.pagination.number;

            $scope.isLoading = false;
        });
    };

}]);