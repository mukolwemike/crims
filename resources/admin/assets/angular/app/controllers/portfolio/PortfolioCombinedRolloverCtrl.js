/**
 * Created by interntwo on 18/04/2016.
 */
app.controller(
    'PortfolioCombinedRolloverCtrl',
    ['$scope', '$rootScope', 'ModalService', function ($scope, $rootScope, ModalService) {

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            $rootScope.total_value = $scope.total_value;
            $rootScope.amount = $scope.amount;
            $rootScope.description = $scope.description;
            $rootScope.reinvest = $scope.reinvest;
            $rootScope.taxable = $scope.taxable;
            $rootScope.interest_rate = $scope.interest_rate;
            $rootScope.investments = $scope.investments;

            ModalService.showModal(
                {
                    templateUrl: "rollover.htm", controller: "PortfolioCombinedRolloverConfirmCtrl"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };

    }]
);

app.controller(
    'PortfolioCombinedRolloverConfirmCtrl',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.invested_date = document.getElementById('invested_date').value;
        $scope.maturity_date = document.getElementById('maturity_date').value;
        $scope.total_value = $rootScope.total_value;
        $scope.amount = $rootScope.amount;
        $scope.description = $rootScope.description;
        $scope.reinvest = $rootScope.reinvest;
        $scope.taxable = $rootScope.taxable;
        $scope.interest_rate = $rootScope.interest_rate;
        $scope.investments = $rootScope.investments;


        if ($scope.reinvest == 'reinvest') {
            $scope.withdraw_val = $scope.total_value - $scope.amount;
            $scope.reinvest_val = $scope.amount;
        }

        if ($scope.reinvest == 'withdraw') {
            $scope.withdraw_val = $scope.amount;
            $scope.reinvest_val = $scope.total_value - $scope.amount;
        }

        if ($scope.reinvest == 'topup') {
            $scope.topup = true;
            $scope.withdraw_val = 0;
            $scope.reinvest_val = $scope.amount + $scope.total_value;
        }



    }]
);