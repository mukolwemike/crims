/**
 * Created by interntwo on 16/03/2016.
 */


app.controller(
    'EditInvestmentFormController',
    ['$scope', 'ModalService', '$rootScope', function ($scope, ModalService, $rootScope) {

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                console.log('valid');
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {

            $rootScope.amount = $scope.amount;
            $rootScope.contactPerson = $scope.contact_person;
            $rootScope.fund_type_id = $scope.fund_type_id;
            $rootScope.custodial_account_id = $scope.custodial_account_id;
            $rootScope.interest_rate = $scope.interest_rate;
            $rootScope.taxable = $scope.taxable;

            console.log('here');
            ModalService.showModal(
                {
                    templateUrl: "editPortfolioInvestment.htm", controller: "EditInvestmentFormConfirmControl"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'EditInvestmentFormConfirmControl',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.taxable = $rootScope.taxable;
        $scope.amount = $rootScope.amount;
        $scope.contactPerson = $rootScope.contactPerson;
        $scope.fund_type_id = $rootScope.fund_type_id;
        $scope.interest_rate = $rootScope.interest_rate;
        $scope.custodial_account_id = $rootScope.custodial_account_id;
        $scope.invested_date = document.getElementById('invested_date').value;
        $scope.maturity_date = document.getElementById('maturity_date').value;
        $scope.on_call = document.getElementById('on_call').checked;

        var investmentType_id = document.getElementById("fund_type_id");
        $scope.investmentTypeText = investmentType_id.options[investmentType_id.selectedIndex].text;
        var custodial_account_id = document.getElementById("custodial_account_id");
        $scope.custodialAccountText = custodial_account_id.options[custodial_account_id.selectedIndex].text;

        var taxable = parseInt($scope.taxable);
        if (taxable === 0 ) {
            $scope.taxable_class = 'alert alert-danger';
            $scope.taxable_text = 'Zero rated (0%)';
        } else {
            $scope.taxable_text = 'Normal rate';
        }

    }]
);