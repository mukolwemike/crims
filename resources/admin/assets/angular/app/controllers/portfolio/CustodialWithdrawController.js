/**
 * Created by interntwo on 17/03/2016.
 */

app.controller(
    'CustodialWithdrawController',
    ['$scope', '$rootScope', 'ModalService', function ($scope, $rootScope, ModalService) {

        $scope.submit = function (event) {

            event.preventDefault();

            $rootScope.amount = $scope.amount;
            $rootScope.narrative = $scope.narrative;
            $rootScope.transaction = $scope.transaction;


            if ($scope.withdrawForm.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "withdraw.htm", controller: "CustodialWithdrawConfirmController"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);


app.controller(
    'CustodialWithdrawConfirmController',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.amount = $rootScope.amount;
        $scope.narrative = $rootScope.narrative;
        $scope.transaction = $rootScope.transaction;
        $scope.date = document.getElementById('date').value;

        var transaction = document.getElementById('transaction');
        $scope.transaction_text = transaction.options[transaction.selectedIndex].text;
    }]
);