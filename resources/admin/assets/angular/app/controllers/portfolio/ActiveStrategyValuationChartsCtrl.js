/**
 * Created by interntwo on 07/04/2016.
 */

app.controller(
    'ActiveStrategyValuationChartsCtrl',
    ['$scope', 'ActiveStrategy', function ($scope, ActiveStrategy) {

        ActiveStrategy.getAssetAllocationData()
        .then(
            function (response) {
                $scope.d = response.data.values;
                $scope.l = response.data.names;
            },
            function (response) {

            }
        );

    }]
);