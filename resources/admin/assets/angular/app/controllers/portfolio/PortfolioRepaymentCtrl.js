/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 22/06/2016.
 * Project crims
 */
app.controller(
    'PortfolioRepaymentCtrl',
    ['$scope', 'ModalService', '$rootScope', function ($scope, ModalService, $rootScope) {

        $scope.payment_date_type = '0';
        $scope.payment_amount_type = '0';
        $scope.maturityDate = '';
        $scope.maturityAmount = 0;
        $scope.payment_type = '1';

        $scope.submit = function (event) {
            event.preventDefault();

            $rootScope.total_amount = $scope.total_amount;
            $rootScope.amount = $scope.amount;
            $scope.date = document.getElementById('date').value;
            $rootScope.date = $scope.date;
            $rootScope.narrative = $scope.narrative;

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.$watch('payment_date_type',
            function () {
                if ($scope.payment_date_type == 1) {
                    $scope.date = $scope.maturityDate;
                }
                else {
                    $scope.date = '';
                }
            }
        );

        $scope.$watch('payment_amount_type',
            function () {
                if ($scope.payment_amount_type == 1) {
                    $scope.amount = $scope.maturityAmount;
                    $scope.payment_type = '3';
                }
                else {
                    $scope.amount = '';
                    $scope.payment_type = '1';
                }
            }
        );

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "portfoliorepay.htm", controller: "PortfolioRepaymentConfirmCtrl"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };

    }]
);

app.controller(
    'PortfolioRepaymentConfirmCtrl',
    ['$scope', '$rootScope', function ($scope, $rootScope) {

        $scope.total_amount = $rootScope.total_amount;
        $scope.amount = $rootScope.amount;
        $scope.narrative = $rootScope.narrative;
        $scope.date = $rootScope.date;

        var payment_types = document.getElementById('payment_type');
        $scope.payment_type = payment_types.options[payment_types.selectedIndex].text;
        $scope.payment_type_id = payment_types.value;

        var payment_date_types = document.getElementById('payment_date_type');
        $scope.payment_date_type = payment_date_types.options[payment_date_types.selectedIndex].text;
        $scope.payment_date_type_id = payment_date_types.value;

        var payment_amount_types = document.getElementById('payment_amount_type');
        $scope.payment_amount_type = payment_amount_types.options[payment_amount_types.selectedIndex].text;
        $scope.payment_amount_type_id = payment_amount_types.value;

    }]
);