/**
 * Created by interntwo on 06/04/2016.
 */

app.controller(
    'PortfolioSummaryController',
    ['$scope', 'PortfolioInvestment', function ($scope, PortfolioInvestment) {
        $scope.displayed = [];
        $scope.fund_type_id = '';
        $scope.tableState = {};
        $scope.filters = {};

        $scope.callServer = function callServer(tableState)
        {
            $scope.isLoading = true;
            $scope.isLoading2 = true;
            $scope.filters = {};

            var pagination = tableState.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            if ($scope.fund_type_id !== '') {
                $scope.filters.fund_type_id = $scope.fund_type_id;
            }

            PortfolioInvestment.getSummaryPage(tableState, $scope.currency_id, $scope.filters)
            .then(
                function (result) {
                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;
                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update
                    //tableState.pagination.number = result.data.meta.cursor.count;
                    $scope.perPage = tableState.pagination.number;
                    $scope.tableState = tableState;
                    $scope.isLoading = false;
                }
            );

            PortfolioInvestment.getSummaryPageTotals(tableState, $scope.currency_id, $scope.filters)
            .then(
                function (result) {
                    $scope.meta = result.data;
                    $scope.isLoading2 = false;
                }
            );
        };
    }],
);

app.controller(
    'PortfolioAllocationSummary',
    ['$scope', 'PortfolioAllocationSummary', function ($scope, PortfolioAllocationSummary) {
        $scope.displayed = [];
        $scope.unit_fund_id = '';
        $scope.tableState = {};
        $scope.filters = {};

        $scope.callServer = function callServer(tableState)
        {
            $scope.isLoading = true;
            $scope.isLoading2 = true;
            $scope.filters = {};

            var pagination = tableState.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            if ($scope.unit_fund_id !== '') {
                $scope.filters.unit_fund_id = $scope.unit_fund_id;
            }

            PortfolioAllocationSummary.getSummaryPage(tableState, $scope.filters)
                .then(
                    function (result) {
                        $scope.displayed = result.data.data;
                        $scope.meta = result.data.meta;
                        tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update
                        //tableState.pagination.number = result.data.meta.cursor.count;
                        $scope.perPage = tableState.pagination.number;
                        $scope.tableState = tableState;
                        $scope.isLoading = false;
                    }
                );

            PortfolioAllocationSummary.getSummaryPageTotals(tableState, $scope.filters)
                .then(
                    function (result) {
                        $scope.meta = result.data;
                        $scope.isLoading2 = false;
                    }
                );
        };
    }],
);

app.controller(
    'SubAssetClassController',
    ['$scope', 'SubAssetClasses', function ($scope, SubAssetClasses) {

        $scope.loading = false;

        $scope.$watch(
            'asset_class_id',
            function () {
                if ($scope.asset_class_id !== null) {
                    $scope.loading = true;

                    if ($scope.asset_class_id) {
                        SubAssetClasses.subAssetClassList($scope.asset_class_id)
                            .then(
                                function (response) {
                                    $scope.subAssetClasses = response.data;
                                }
                            )
                            .finally(
                                function () {
                                    $scope.loading = false;
                                }
                            );
                    }
                }
            },
            true
        );
    }]
);