app.controller(
    'EquitySecurityValuationChartCtrl',
    ['$scope', 'EquitySecurity', function ($scope, EquitySecurity) {

        EquitySecurity.getAssetAllocationData()
        .then(
            function (response) {
                $scope.d = response.data.values;
                $scope.l = response.data.names;
            },
            function (response) {

            }
        );

    }]
);