/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 16/07/2016.
 * Project crims
 */

app.controller(
    'MaturityAnalysisCtrl',
    ['$scope', 'Maturity', function ($scope, Maturity) {

        $scope.weeklyCollapsed = true;

        $scope.monthlyCollapsed = false;

        //weekly graph
        $scope.$watch(
            function () {
                return $scope.currency;},
            function (oldValue, newValue) {
                if (newValue) {
                    var dates = {
                        start: $scope.date,
                        end: $scope.dateSecond
                    };

                    Maturity.getWeeklyProfileData($scope.currency, dates)
                    .then(
                        function (response) {
                            var data = response.data;


                            $scope.weekly_options = {
                                scaleLabel: function (label) {
                                    return  label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                },

                                multiTooltipTemplate : function (label) {
                                    return label.datasetLabel + ': ' +    label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            };

                            $scope.weekly_labels = data[1];
                            $scope.weekly_series = ['Funds', 'Clients'];
                            $scope.weekly_data = [ data[0], data[2]];

                        },
                        function (){}
                    );

                    Maturity.getMonthlyProfileData($scope.currency, dates)
                    .then(
                        function (response) {
                            var data = response.data;

                            $scope.monthly_options = {
                                scaleLabel: function (label) {
                                    return  label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                },

                                multiTooltipTemplate : function (label) {
                                    return label.datasetLabel + ': ' +    label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            };

                            $scope.monthly_labels = data[1];
                            $scope.monthly_series = ['Funds', 'Clients'];
                            $scope.monthly_data = [ data[0], data[2]];

                        },
                        function (){}
                    );
                }
            }
        );

        //monthly graph
    }]
);