app.controller(
    'CustodialReconcileController',
    ['$scope', '$rootScope', 'ModalService', function ($scope, $rootScope, ModalService) {
        $scope.submit = function (event) {
            event.preventDefault();

            $rootScope.amount = $scope.amount;
            $rootScope.narrative = $scope.narrative;
            $rootScope.type = $scope.type;

            if ($scope.reconcileForm.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "reconcile.htm", controller: "CustodialReconcileConfirmController"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);


app.controller(
    'CustodialReconcileConfirmController',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.amount = $rootScope.amount;
        $scope.narrative = $rootScope.narrative;
        $scope.type = $rootScope.type;
        $scope.reconciliation_date = document.getElementById('reconciliation_date').value;

        var type = document.getElementById('type');
        $scope.type_text = type.options[type.selectedIndex].text;
    }]
);