/**
 * Controller for display of the exchange rates for the different currencies
 */
app.controller(
    "custodialExchangeRatesController",
    ['$scope', 'CustodialExchangeRate', 'TableState', function ($scope, CustodialExchangeRate, TableState) {
        $scope.displaed = [];
        $scope.base_id = '0';
        $scope.to_id = '0';
        $scope.tableState = null;
        $scope.isLoading = true;

        $scope.callServer = function callServer(tableState)
        {
            $scope.tableState = tableState;

            $scope.filters = {};

            if ($scope.base_id != '0') {
                $scope.filters.base_id = $scope.base_id;
            }

            if ($scope.to_id != '0') {
                $scope.filters.to_id = $scope.to_id;
            }

            TableState.renderTable($scope, tableState, CustodialExchangeRate.getRates(tableState, $scope.filters));
        };
    }]
);