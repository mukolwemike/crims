/**
 * Created by interntwo on 17/03/2016.
 */


app.controller(
    'AccountTransferController',
    ['$scope', '$rootScope', 'ModalService', 'CustodialAccount', function ($scope, $rootScope, ModalService, CustodialAccount) {

        $scope.reverse = false;
        $scope.exchange = 1;

        $scope.submit = function (event) {
            event.preventDefault();

            $rootScope.account = $scope.account;
            $rootScope.destination_id = $scope.destination_id;
            $rootScope.amount = $scope.amount;
            $rootScope.narrative = $scope.narrative;
            $rootScope.exchange = $scope.exchange;
            $rootScope.reverse = $scope.reverse;
            $rootScope.converted = $scope.converted;
            $rootScope.effective_rate = $scope.effective_rate;

            if ($scope.transferForm.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "transfer.htm", controller: "AccountTransferConfirmController"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };

        $scope.$watch(
            'account_id',
            function () {
                CustodialAccount.getRemainingAccounts($scope.account_id)
                .then(
                    function (response) {
                        $scope.otherAccounts = response.data.data;
                    }
                );
            }
        );

        $scope.$watch(
            'account',
            function () {
                console.log($scope.account.name);
            }
        );

        $scope.$watchGroup(
            ['amount', 'exchange', 'reverse'],
            function () {
                $scope.effective_rate = $scope.reverse ? $scope.exchange : 1/$scope.exchange;
                $scope.converted = $scope.amount * $scope.effective_rate;
            }
        );
    }]
);

app.controller(
    'AccountTransferConfirmController',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.account = $rootScope.account;
        $scope.destination_id = $rootScope.destination_id;
        $scope.amount = $rootScope.amount;
        $scope.narrative = $rootScope.narrative;
        $scope.transfer_date = document.getElementById('transfer_date').value;
        $scope.exchange = $rootScope.exchange;
        $scope.reverse = $rootScope.reverse;
        $scope.converted = $rootScope.converted;
        $scope.effective_rate = $rootScope.effective_rate;
    }]
);