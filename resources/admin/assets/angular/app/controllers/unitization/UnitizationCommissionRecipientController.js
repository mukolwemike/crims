
app.controller('UnitizationCommissionRecipientController', ['$scope', 'UnitizationCommissionRecipients', function ($scope, UnitizationCommissionRecipients) {
        $scope.recipientId = '';
        $scope.start = '';
        $scope.end = '';
        $scope.overridesLoading = false;
        $scope.schedulesLoading = false;
        $scope.selectedSchedule = {};

        $scope.$watch(function () {
            return $scope.recipientId;
        }, function (newValue, oldValue) {
            if(newValue)
            {
                $scope.overridesLoading = true;
                $scope.schedulesLoading = true;
                UnitizationCommissionRecipients.getCommissionTotals($scope.recipientId, $scope.start, $scope.end)
                    .then(function (response) {
                        $scope.totals = response.data;
                    });

                UnitizationCommissionRecipients.getCommissionSchedules($scope.recipientId, $scope.start, $scope.end)
                    .then(function (response) {
                        $scope.schedules = response.data;
                        $scope.selectedSchedule = $scope.schedules[0];
                        $scope.schedulesLoading = false;
                    });

                UnitizationCommissionRecipients.getCommissionOverrides($scope.recipientId, $scope.start, $scope.end)
                    .then(function (response) {
                        $scope.overrides = response.data;
                        $scope.overridesLoading = false;
                    });
            }
        });
    }]
);