
app.controller(
    'RealEstateInstructionsCtrl',
    ['$scope', 'RealEstateInstruction', function ($scope, RealEstateInstruction) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            RealEstateInstruction.getPage(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update
                    //tableState.pagination.number = result.data.meta.cursor.count;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);

app.factory('RealEstateInstruction', ['TableState', function (TableState) {

    var app = {};

    app.getPage = function (tableState) {
        return TableState.getPage(tableState, '/api/realestate/realestate-instructions/application');
    };

    return app;
}]);