/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 01/10/2016.
 * Project crims
 */

app.controller(
    'RealEstatePaymentDetailController',
    ['$scope', '$filter', function ($scope, $filter) {
        $scope.delete = function (id, amount) {
            amount = $filter('currency')(amount, '');

            var response = confirm("Do you want to delete the payment of "+amount+'?');

            if (response) {
                window.location.href = '/dashboard/realestate/payments/delete/'+id;
            }
        };
    }]
);