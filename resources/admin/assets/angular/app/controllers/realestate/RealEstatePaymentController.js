/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 06/06/2016.
 * Project crims
 */
app.controller('RealEstatePaymentController', ['$scope', '$rootScope', 'ModalService', 'RealEstateCommissionRecipient', function ($scope, $rootScope, ModalService, RealEstateCommissionRecipient) {
    $scope.recipientRate = {id : '' , rate_name : '', amount : 0};
    $scope.recipientRateId = '';
    $scope.recipientRateName = '';
    $scope.client_id = '';
    $scope.commission_rate = 0;

    $scope.status = {
        opened: false
    };
    $scope.open = function ($event) {
        $scope.status.opened = true;
    };

    $scope.$watchGroup(
        ['recipient_id', 'entered_date'],
        function () {
            let entered_date = document.getElementById('entered_date').value;
            if(entered_date && $scope.recipient_id)
            {
                RealEstateCommissionRecipient.getRecipientRealEstateCommissionRate($scope.recipient_id, $scope.project_id, entered_date, $scope.client_id)
                    .then(
                        function (response) {
                            $scope.recipientRate = response.data;
                            $scope.commission_rate = parseFloat($scope.recipientRate.amount);
                            $scope.commissionRateName = $scope.recipientRate.rate_name;
                        },
                        function (response) {
                        }
                    );
            }
        },
        true
    );

    $scope.$watch(
        function () {
            return $scope.awarded;
        },
        function (newValue, oldValue) {
            if (newValue)
            {
                if ($scope.awarded == 0) {
                    $scope.commission_rate = 0;
                    $scope.commissionRateName = 'Zero Commission - 0%';
                    $scope.reason = '';
                }
                else {
                    $scope.commission_rate = parseFloat($scope.recipientRate.amount);
                    $scope.commissionRateName = $scope.recipientRate.rate_name;
                }
            }
        }
    );

    $scope.$watch('edit_rate',
        function () {
            if($scope.edit_rate == 0 && $scope.awarded == 1)
            {
                $scope.commission_rate = parseFloat($scope.recipientRate.amount);
                $scope.commissionRateName = $scope.recipientRate.rate_name;
                $scope.reason = '';
            }
        }
    );

    $scope.$watch('commission_rate',
        function () {

            if($scope.commission_rate != parseFloat($scope.recipientRate.amount) && $scope.awarded == 1)
            {
                $scope.commissionRateName = "Edited Rate - " + $scope.commission_rate + '%';
            }
        }
    );

    $scope.submit = function (event) {
        event.preventDefault();

        var tranches = document.getElementById('tranche_id');
        var payment_plans = document.getElementById('payment_plan_id');
        $rootScope.description = $scope.description;
        $rootScope.amount = $scope.amount;
        $rootScope.schedule_id = $scope.schedule_id;
        $rootScope.negotiated_price = $scope.negotiated_price;
        $rootScope.recipient_id = $scope.recipient_id;
        $rootScope.awarded = $scope.awarded;
        $rootScope.commission_rate = $scope.commission_rate;
        $rootScope.reason = $scope.reason;
        $rootScope.commissionRateName = $scope.commissionRateName;
        $rootScope.client_code = $scope.client_code;
        $rootScope.discount = $scope.discount;
        //$rootScope.tranche_id = $scope.tranche_id;
        try {
            $rootScope.tranche_id = tranches.options[tranches.selectedIndex].value;
        } catch (err) {
        }

        //$rootScope.payment_plan_id = $scope.payment_plan_id;
        try {
            $rootScope.payment_plan_id = payment_plans.options[payment_plans.selectedIndex].value;
        } catch (err) {
        }

        if ($scope.form.$valid) {
            $scope.confirmModal();
        }
    };

    $scope.confirmModal = function () {
        ModalService.showModal(
            {
                templateUrl: "re_payment.htm", controller: "RealEstatePaymentConfirmController"
            }
        ).then(
            function (modal) {
                modal.element.modal();
            }
        );
    };
}]);

app.controller(
    'RealEstatePaymentConfirmController',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.entered_date = document.getElementById('entered_date').value;

        $scope.description = $rootScope.description;
        $scope.amount = $rootScope.amount;
        $scope.schedule_id = $rootScope.schedule_id;
        $scope.negotiated_price = $rootScope.negotiated_price;
        var schedules =  document.getElementById('schedules');
        $scope.schedule = schedules.options[schedules.selectedIndex].text;
        $scope.recipient_id = $rootScope.recipient_id;
        $scope.awarded = $rootScope.awarded;
        $scope.commission_rate = $rootScope.commission_rate;
        $scope.c_rate = $rootScope.commissionRateName;
        $scope.reason = $rootScope.reason;
        $scope.client_code = $rootScope.client_code;
        $scope.discount = $rootScope.discount;

        try {
            var tranches = document.getElementById('tranche_id');
            $scope.selected_tranche = tranches.options[tranches.selectedIndex].text;
        } catch (err) {
        }

        try {
            var payment_plans = document.getElementById('payment_plan_id');
            $scope.selected_payment_plan = payment_plans.options[payment_plans.selectedIndex].text;
        } catch (err) {
        }

        try {
            var recipients = document.getElementById('recipients_id');
            $scope.selected_recipient = recipients.options[recipients.selectedIndex].text;
        } catch (err) {
        }

        try {
            var awards = document.getElementById('awarded');
            $scope.selected_awarded = awards.options[awards.selectedIndex].text;
        } catch (err) {
        }
    }]
);

app.controller('EditRealEstatePaymentController', ['$scope', 'RealEstateCommissionRecipient', function ($scope, RealEstateCommissionRecipient) {
    $scope.recipientRate = {};
    $scope.recipientRateId = null;
    $scope.commissionRateName = '';
    $scope.project_id = '';
    $scope.reservationDate = '';
    $scope.client_id = '';
    $scope.loadingRate = false;
    $scope.commission_rate = 0;

    $scope.$watch(
        function () {
            return $scope.recipient_id;
        },
        function (newValue, oldValue) {
            if (newValue)
            {
                $scope.loadingRate = true;
                RealEstateCommissionRecipient.getRecipientRealEstateCommissionRate($scope.recipient_id, $scope.project_id, $scope.reservationDate, $scope.client_id)
                    .then(function (response) {
                            $scope.recipientRate = response.data;
                            if ($scope.commission_rate != 0 && $scope.commission_rate != parseFloat($scope.recipientRate.amount)) {
                                $scope.edit_rate = true;
                                $scope.commissionRateName = "Edited Rate - " + $scope.commission_rate + '%';
                            }
                            else {
                                $scope.commission_rate = parseFloat($scope.recipientRate.amount);
                                $scope.commissionRateName = $scope.recipientRate.rate_name;
                            }
                            $scope.loadingRate = false;
                            },
                        function (response) {
                        }
                    );
            }
        }
    );

    $scope.$watch(
        function () {
            return $scope.awarded;
        },
        function (newValue, oldValue) {
            if (newValue)
            {
                if ($scope.awarded == 0) {
                    $scope.commission_rate = 0;
                    $scope.commissionRateName = 'Zero Commission - 0%';
                }
                else {
                    if ($scope.recipientRate.amount == 'undefined') {
                        $scope.commission_rate = parseFloat($scope.recipientRate.amount);
                        $scope.commissionRateName = $scope.recipientRate.rate_name;
                    }
                }
            }
        }
    );

    $scope.$watch('edit_rate',
        function () {
            if ($scope.edit_rate === false && $scope.awarded) {
                $scope.commission_rate = parseFloat($scope.recipientRate.amount);
                $scope.commissionRateName = $scope.recipientRate.rate_name;
                $scope.reason = '';
            }
        }
    );

    $scope.$watch('commission_rate',
        function () {
            if ($scope.commission_rate != parseFloat($scope.recipientRate.amount) && $scope.awarded == 1) {
                $scope.commissionRateName = "Edited Rate - " + $scope.commission_rate + '%';
            }
        }
    );
}]);