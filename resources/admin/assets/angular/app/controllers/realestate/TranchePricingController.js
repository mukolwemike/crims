app.controller(
    'TranchePricingController',
    ['$scope', '$location', 'TrancheUnitPricing', function ($scope, $location, TrancheUnitPricing) {

        $scope.tranche ={
            payment_plan_id:'',
            id:'',
            holding_id: ''
        };
        var url_array = $location.path().split('/');

        $scope.tranche.holding_id = url_array[url_array.length - 2];

        $scope.tranche_pricing_for_selected_payment_plan = [];
        $scope.all_tranche_unit_pricings = [];

        TrancheUnitPricing.getPaymentPlans().then(
            function (result) {
                $scope.payment_plans = result.data.data;
            }
        );

        TrancheUnitPricing.getAllTrancheUnitPricings($scope.tranche.holding_id).then(
            function (result) {
                $scope.all_tranche_unit_pricings = result.data.data;
            }
        );

        $scope.getTranchePricingsForSelectedPaymentPlan = function () {
            $scope.tranche_pricing_for_selected_payment_plan = TrancheUnitPricing.getTrancheUnitPricingsForSelectedPaymentPlan($scope.all_tranche_unit_pricings, $scope.tranche.payment_plan_id);
        };
    }]
);