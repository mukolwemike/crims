/**
 * Controller for display of the real estate unit sizes
 */
app.controller(
    "realestateUnitSizeController",
    ['$scope', 'RealEstateUnitSizes', 'TableState', function ($scope, RealEstateUnitSizes, TableState) {
        $scope.displaed = [];
        $scope.tableState = null;
        $scope.isLoading = true;

        $scope.callServer = function callServer(tableState)
        {
            $scope.tableState = tableState;

            TableState.renderTable($scope, tableState, RealEstateUnitSizes.getUnitSizes(tableState));
        };
    }]
);