
app.controller(
    'RealEstateScheduledPaymentGridController',
    ['$scope', 'RealEstateScheduledPayment', function ($scope, RealEstateScheduledPayment) {
        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;
            var number = pagination.number || 10;

            RealEstateScheduledPayment.getPage(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

        $scope.download = function () {
            var blob = new Blob(
                [document.getElementById('exportable').innerHTML],
                {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                }
            );

            saveAs(blob, "Report.xls");
        };
    }]
);