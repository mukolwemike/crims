/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 23/01/2017.
 * Project crims
 */

app.controller(
    'RealEstateOverduePaymentsController',
    ['$scope', 'RealEstateScheduledPayment', 'TableState', function ($scope, RealEstateScheduledPayment, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            TableState.renderTable($scope, tableState, RealEstateScheduledPayment.getOverdue(tableState));
        };

    }]
);