
app.controller(
    'RealEstateClientUnitsController',
    ['$scope', 'RealEstateClientUnits', function ($scope, RealEstateClientUnits) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;
            var number = pagination.number || 10;

            RealEstateClientUnits.getProjectPage(tableState, $scope.client_id).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);