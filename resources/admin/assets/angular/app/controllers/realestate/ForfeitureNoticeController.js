
app.controller(
    'ForfeitureNoticeController',
    ['$scope', 'ForfeitureNotice', function ($scope, ForfeitureNotice) {

        $scope.displayed = [];
        $scope.loading = false;

        $scope.tableState = '';

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;

            ForfeitureNotice.getPage(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );

            $scope.tableState = tableState;
        };

    }]
);