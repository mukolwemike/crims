/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 18/05/2016.
 * Project crims
 */

app.controller(
    'RealEstateClientsGridCtrl',
    ['$scope', 'RealEstateClient', function ($scope, RealEstateClient) {
        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;
            var number = pagination.number || 10;

            RealEstateClient.getPage(tableState).then(
                function (result) {

                    console.log(result);
                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;


                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;
                    //tableState.pagination.number = result.data.meta.cursor.count;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };
    }]
);