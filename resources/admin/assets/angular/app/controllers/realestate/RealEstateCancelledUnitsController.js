/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 23/05/2016.
 * Project crims
 */

app.controller(
    'RealEstateCancelledUnitsController',
    ['$scope', 'RealEstateCancelledUnits', 'TableState', function ($scope, RealEstateCancelledUnits, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            TableState.renderTable($scope, tableState, RealEstateCancelledUnits.getProjectPage(tableState, $scope.project_id));
        };

    }]
);