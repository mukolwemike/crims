app.controller(
    'RealEstateCommissionPaymentDatesGridController',
    ['$scope', 'RealEstateCommissionPaymentDate', function ($scope, RealEstateCommissionPaymentDate) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            $scope.tableState = tableState;
            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;
            var number = pagination.number || 10;

            RealEstateCommissionPaymentDate.getPaymentDates(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;
                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);