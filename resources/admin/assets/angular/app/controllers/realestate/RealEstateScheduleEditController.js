app.controller(
    'RealEstateScheduleEditController',
    ['$scope', function ($scope) {
        $scope.penaltyCharge = false;

        $scope.setPenaltyChargeStatus = function (status) {
            $scope.penaltyCharge = (status === 1);
        };

        $scope.changePenaltyChargeStatus = function () {
            return ! ($scope.penaltyCharge);
        };
    }]
);