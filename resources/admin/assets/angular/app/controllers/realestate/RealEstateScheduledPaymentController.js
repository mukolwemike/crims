app.controller(
    'RealEstateScheduledPaymentController',
    ['$scope', '$location', 'PaymentSchedule', function ($scope, $location, PaymentSchedule) {

        $scope.schedules = [{
            id                  :   '1',
            description         :   '',
            payment_type_id     :   '',
            date                :   '',
            amount              :   ''
        }];
        $scope.custom = {
            start_date          :   '',
            end_date            :   '',
            date_of_month       :   '',
            amount              :   ''
        };

        $scope.spacing = 3;

        $scope.generate = function (holding_id) {
            // Reset array of object
            $scope.schedules = [{
                id                  :   '1',
                description         :   '',
                payment_type_id     :   '',
                date                :   '',
                amount              :   ''
            }];

             PaymentSchedule.getPaymentSchedules(holding_id, $scope.spacing)
            .then(
                function (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.schedules[i] = response.data[i];
                        $scope.addNewSchedule();
                    }
                    $scope.removeSchedule();
                },
                function (response) {

                }
            );
        };

        $scope.addNewSchedule = function () {
            var newItemNo = $scope.schedules.length + 1;
            $scope.schedules.push({'id':newItemNo});
        };

        $scope.removeSchedule = function () {
            var lastItem = $scope.schedules.length - 1;
            $scope.schedules.splice(lastItem);
        };

        $scope.getPaymentType = function (payment_type_id) {
            return payment_type_id === '' || payment_type_id === undefined ? null : document.getElementById('payment_type_id').options[payment_type_id - 1].text;
        };

        $scope.generateSchedules = function () {
            // Reset array of object
            $scope.schedules = [{
                id                  :   '1',
                description         :   '',
                payment_type_id     :   '',
                date                :   '',
                amount              :   ''
            }];

            // Check if fields have values
            var output = checkInput($scope.custom);
            if (output.state === false) {
                alert(output.message);
            } else {
                var start_date = moment($scope.custom.start_date);
                var start_date_date_of_month = start_date.date();
                var end_date = moment($scope.custom.end_date);
                var date_of_month = moment($scope.custom.date_of_month);
                var amount = $scope.custom.amount;

                // Calculate months and installments per month
                var months = start_date_date_of_month < date_of_month ? end_date.diff(start_date, 'months') + 1 : end_date.diff(start_date, 'months');
                var installment = amount / months;

                // Populate schedules
                for (var i = 0; i < months; i++) {
                    // Handling dates
                    if (start_date_date_of_month <= date_of_month && date_of_month <= end_date.date() && i === 0) {
                        $scope.schedules[i].date = start_date.add(0, 'months').date(date_of_month).format();
                        $scope.schedules[i].description = "Installment " + (i+1);
                        $scope.schedules[i].payment_type_id = '3';
                        $scope.schedules[i].amount = installment;
                    } else if (date_of_month <= end_date.date()) {
                        $scope.schedules[i].date = start_date.add(1, 'months').date(date_of_month).format();
                        $scope.schedules[i].description = "Installment " + (i+1);
                        $scope.schedules[i].payment_type_id = '3';
                        $scope.schedules[i].amount = installment;
                    }
                    $scope.addNewSchedule();
                }
                $scope.removeSchedule();
            }
        };

        $scope.$watch(
            'items',
            function () {
                var myEl = angular.element(document.querySelector('#delete-items'));
                myEl.empty();
                if ($scope.items !== undefined) {
                    Object.keys($scope.items).forEach(
                        function (key) {
                            if ($scope.items[key] === true) {
                                myEl.append('<input type="hidden" name="items[]" value="'+key+'" />');
                            }
                        }
                    );
                }
            },
            true
        );

        function checkInput(custom)
        {
            var output = {
                state:      false,
                message:    ''
            };

            if (custom.start_date === '' || custom.end_date === '' || custom.date_of_month === '' || $scope.custom.amount === '') {
                output.message = "Some fields are empty";
            } else if (custom.date_of_month <= 0) {
                output.message = "Invalid input for Date";
            } else if (custom.amount <= 0) {
                output.message = "Invalid input for Amount";
            } else {
                output.state = true;
            }

            return output;
        }
    }]
);

