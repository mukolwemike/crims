/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 06/10/2016.
 * Project crims
 */

app.controller(
    'RealEstateClientsController',
    ['$scope', function ($scope) {
        $scope.sendPaymentPlan = function (url) {

            var c = window.confirm('Are you sure you want to send?');

            if (c) {
                window.location.href = url;
            }
        };
    }]
);