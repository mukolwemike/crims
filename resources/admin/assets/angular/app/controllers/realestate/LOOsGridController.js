
app.controller(
    'LOOsGridController',
    ['$scope', 'LOOs', function ($scope, LOOs) {

        $scope.displayed = [];
        $scope.loading = false;

        $scope.tableState = '';

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;
            var start = pagination.start || 0;
            var number = pagination.number || 10;

            LOOs.getPage(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );

            $scope.tableState = tableState;
        };

        $scope.exportLOOs = function () {
            $scope.loading = true;

            LOOs.export($scope.tableState).then(
                function (result) {
                    $scope.loading = false;
                    toastr.success('An email containing the report has been sent to your inbox.');
                }
            );
        };

    }]
);