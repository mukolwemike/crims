/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 17/06/2016.
 * Project crims
 */
app.controller(
    'ProjectTrancheController',
    ['$scope', function ($scope) {

        $scope.tranche = {
            prices : [],
            pricing : [],
        };

        $scope.price = {price: "", number: ""};
        $scope.pricing = { payment_plan_id: '', price: '' };

        $scope.addPrice = function () {
            $scope.tranche.prices.push($scope.price);
        };

        $scope.addPaymentPlanAndPrice = function () {
            console.log(123);
            $scope.tranche.pricing.push($scope.pricing);
        };

        $scope.addPrice();
        $scope.addPaymentPlanAndPrice();


    }]
);


app.controller(
    'EditTrancheController',
    ['$scope', 'RealEstateTranche', 'RealEstateTranchePricing', '$location', function ($scope, RealEstateTranche, RealEstateTranchePricing, $location) {

        $scope.tranche = {
            prices : [],
            pricing : []
        };

        $scope.size_id = [];

        $scope.$watch(
            'tranche_id',
            function () {
                if ($scope.tranche_id) {
                    RealEstateTranche.getPrices($scope.tranche_id)
                    .then(
                        function (response) {
                            $scope.tranche.prices = response.data;
                            console.log(response.data);

                        },
                        function (response) {

                        }
                    );

                    RealEstateTranchePricing.getPricing($scope.tranche_id)
                    .then(
                        function (response) {
                            $scope.tranche.pricing = response.data;
                        },
                        function (response) {

                        }
                    );
                }
            }
        );

        $scope.addPrice = function () {
            $scope.tranche.prices.push({price: "", number: ""});
        };

        $scope.addPrice();

    }]
);