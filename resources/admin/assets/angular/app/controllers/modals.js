/**
 * Selecting client type for indivisual applicants
 */

app.controller(
    'ClientModalController',
    ['$scope', 'ModalService', function ($scope, ModalService) {

        $scope.show = function () {
            ModalService.showModal(
                {
                    templateUrl: 'modal.html',
                    controller: "ClientTypeModalController"
                }
            ).then(
                function (modal) {

                    modal.element.modal();

                    modal.close.then(
                        function (result) {

                        }
                    );
                }
            );
        };

    }]
);


app.controller(
    'ClientTypeModalController',
    ['$window','ModalService', '$scope', 'close', function ($window,ModalService, $scope, close) {
        $scope.close = function (result) {


            if (result==='new') {
                $window.location.href = '/dashboard/investments/apply/individual';
            }
            if (result==='existing') {
                ModalService.showModal(
                    {
                        templateUrl: 'client.html',
                        controller: "ClientSelectModalController"
                    }
                ).then(
                    function (modal) {

                            modal.element.modal();

                            modal.close.then(
                                function (result) {

                                }
                            );
                    }
                );
            }
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }]
);

app.controller(
    'ClientSelectModalController',
    ['$window', '$scope', 'close', function ($window, $scope, close) {
        $scope.back = function () {

        };
        $scope.proceed = function (email) {
            $window.location.href = '/dashboard/investments/apply/individual?c='+email;
        };

    }]
);



app.controller(
    'SuggestClients',
    ['Clients', '$scope','$filter', function (Clients, $scope,$filter) {
        $scope.clients = [];

        Clients.get($filter)
        .success(
            function (data) {
                $scope.cli = data;
            }
        )
        .then(
            function (data) {
                var arr = [];
                angular.forEach(
                    $scope.cli,
                    function (c) {
                        var contact = c.contact;
                        var email = contact.email;

                        $scope.clients.push(email);
                    }
                );
            }
        );
    }]
);

app.controller(
    'CorporateModalController',
    ['$scope', 'ModalService', function ($scope, ModalService) {

        $scope.show = function () {
            ModalService.showModal(
                {
                    templateUrl: 'type.html',
                    controller: "CorporateApplicationModalController"
                }
            ).then(
                function (modal) {

                    modal.element.modal();

                    modal.close.then(
                        function (result) {



                        }
                    );
                }
            );
        };

    }]
);


app.controller(
    'CorporateApplicationModalController',
    ['$window','ModalService', '$scope', 'close', function ($window,ModalService, $scope, close) {



        $scope.close = function (result) {


            if (result==='new') {
                $window.location.href = '/dashboard/investments/apply/corporate';
            }
            if (result==='existing') {
                ModalService.showModal(
                    {
                        templateUrl: 'corporate.html',
                        controller: "CorporateApplicationSelectModalController"
                    }
                ).then(
                    function (modal) {

                            modal.element.modal();

                            modal.close.then(
                                function (result) {

                                }
                            );
                    }
                );
            }
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };
    }]
);

app.controller(
    'CorporateApplicationSelectModalController',
    ['$window', '$scope', 'close', function ($window, $scope, close) {
        $scope.back = function () {
        };
        $scope.proceed = function (email) {
            $window.location.href = '/dashboard/investments/apply/corporate?c='+email;
        };
    }]
);