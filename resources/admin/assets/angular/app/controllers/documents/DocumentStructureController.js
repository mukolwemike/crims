
app.controller(
    'DocumentStructureController',
    ['$scope', function ($scope) {

        $scope.displayedSection = 'projects';
        $scope.displayedProjectItem = null;
        $scope.displayedProductItem = null;
        $scope.displayedInvestorGroupItem = null;
        $scope.displayedPortfolioItem = null;

        $scope.displaySection = function (section) {
            $scope.displayedSection = $scope.displayedSection == section ? 'projects' : section;

            $scope.displayedProjectItem = $scope.displayedProductItem = $scope.displayedInvestorGroupItem = $scope.displayedPortfolioItem = null;

        };

        $scope.displayProjectItem = function (key) {
            $scope.displayedProjectItem = $scope.displayedProjectItem == key ? null : key;

            $scope.displayedProductItem  = $scope.displayedInvestorGroupItem = $scope.displayedPortfolioItem = null;
        };

        $scope.displayProductItem = function (key) {
            $scope.displayedProductItem = $scope.displayedProductItem == key ? null : key;

            $scope.displayedProjectItem  = $scope.displayedInvestorGroupItem = $scope.displayedPortfolioItem = null;
        };

        $scope.displayInvestorGroupItem = function (key) {
            $scope.displayedInvestorGroupItem = $scope.displayedInvestorGroupItem == key ? null : key;

            $scope.displayedProjectItem = $scope.displayedProductItem = $scope.displayedPortfolioItem = null;
        };

        $scope.displayPortfolioItem = function (key) {
            $scope.displayedPortfolioItem = $scope.displayedPortfolioItem == key ? null : key;

            $scope.displayedProjectItem = $scope.displayedProductItem = null;
        };
    }]
);