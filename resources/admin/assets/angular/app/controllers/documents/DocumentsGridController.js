
app.controller(
    'DocumentsGridController',
    ['$scope', '$location', 'Document', function ($scope, $location, Document) {

        $scope.displayed = [];
        $scope.sectionSlug = '';

        // var url_array = $location.path().split('/');
        // $scope.sectionSlug = url_array[url_array.length - 1];
        // $scope.typeSlug = url_array[url_array.length - 2];
        // $scope.module = url_array[url_array.length - 3];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;
            var start = pagination.start || 0;
            var number = pagination.number || 10;

            Document.getPage(tableState, $scope.module, $scope.typeSlug).then(
                function (result) {
                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);