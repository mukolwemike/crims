
app.controller(
    'CreateSharePurchaseOrderCtrl',
    ['$scope', 'ShareCommissionRecipients', function ($scope, ShareCommissionRecipients) {

        $scope.rates = [];
        $scope.rate = {};
        $scope.recipientId = '';
        $scope.loading = false;
        $scope.client_id = '';
        $scope.date = '';

        $scope.$watchGroup(['recipientId', 'date'],
            function () {
                if($scope.recipientId)
                {
                    $scope.loading = true;
                    ShareCommissionRecipients.getRecipientProductCommissionRate($scope.recipientId, $scope.date,
                        $scope.client_id)
                        .then( function (response) {
                                $scope.rate = response.data;
                                $scope.commission_rate = $scope.rate.rate;
                                $scope.commission_rate_name = $scope.rate.rate_name;
                                $scope.loading = false;
                            },
                            function (response) {
                            }
                        );
                }
            },
            true
        );

        $scope.$watch('commission_rate',
            function () {
                if($scope.commission_rate != $scope.rate.rate)
                {
                    $scope.commission_rate_name = "Edited Rate - " + $scope.commission_rate + '%';
                }
            }
        );

        $scope.$watch('edit_rate',
            function () {
                if($scope.edit_rate == 0)
                {
                    $scope.commission_rate_name = $scope.rate.rate_name;
                    $scope.commission_rate = $scope.rate.rate;
                }
            }
        );
    }]
);

