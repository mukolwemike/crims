app.controller(
    'SharesStatementCampaignMissingController',
    ['$scope', 'SharesStatementCampaign', function ($scope, SharesStatementCampaign) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;
            var number = pagination.number || 10;

            SharesStatementCampaign.getMissingClients(tableState, $scope.campaign_id).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;
                    $scope.count = result.data.meta.total_count;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };
    }]
);