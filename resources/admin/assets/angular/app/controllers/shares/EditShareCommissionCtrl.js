app.controller(
    'editShareCommissionCtrl',
    ['$scope',  '$http', 'ShareCommissionRecipients', function ($scope, $http,  ShareCommissionRecipients) {

        $scope.commission_rate = 0;
        $scope.commission_rate_name = '';
        $scope.client_id = null;
        $scope.rate = {rate : 0, rate_name : ''};

        $scope.$watchGroup(['commission_recepient'],
            function () {
                $scope.loadingRate = true;
                ShareCommissionRecipients.getRecipientProductCommissionRate($scope.commission_recepient, $scope.date, $scope.client_id)
                    .then( function (response) {
                            $scope.loadingRate = false;
                            $scope.rate = response.data;

                            if ($scope.commission_rate != parseFloat($scope.rate.rate) &&
                                $scope.previous_recipient === $scope.commission_recepient) {
                                $scope.edit_rate = true;
                                $scope.commission_rate_name = "Edited Rate - " + $scope.commission_rate + '%';
                            }
                            else {
                                $scope.edit_rate = false;
                                $scope.commission_rate = parseFloat($scope.rate.rate);
                                $scope.commission_rate_name = $scope.rate.rate_name;
                            }
                            $scope.previous_recipient = $scope.commission_recepient;
                            $scope.loadingRate = false;
                        },
                        function (response) {
                        }
                    );
            },
            true
        );

        $scope.$watch('commission_rate',
            function () {
                if ($scope.commission_rate != $scope.rate.rate && $scope.edit_rate == true) {
                    $scope.commission_rate_name = "Edited Rate - " + $scope.commission_rate + '%';
                }
            }
        );

        $scope.$watch('edit_rate',
            function () {
                if ($scope.edit_rate === false) {
                    $scope.commission_rate_name = $scope.rate.rate_name;
                    $scope.commission_rate = $scope.rate.rate;
                }
            }
        );
    }]
);