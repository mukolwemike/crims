app.controller(
    'SellSharesController',
    ['$scope', '$sce', function ($scope, $sce) {
        $scope.total_count = 0;
        $scope.submit_disabled = true;
        $scope.checkboxes_disabled = false;
        $scope.hidden_form_data = '';

        $scope.selected = function (id, number) {
            // Count selected
            $scope.total_count = $scope.checkboxes[id] === true ? $scope.total_count + number : $scope.total_count - number;
            $scope.submit_disabled = $scope.total_count > 0 ? false : true;

            // Exceeded
            if ($scope.total_count >= $scope.sales_order) {
                $scope.balance = number - $scope.sales_order;
                $scope.submit_disabled = false;
                $scope.checkboxes_disabled = true;
            }

            if ($scope.checkboxes_disabled === true) {
                $scope.hidden_form_data = '';
                angular.forEach(
                    $scope.checkboxes,
                    function (item, index) {
                        $scope.hidden_form_data += '<input type="hidden" name="purchase_order_id[]" value="'+ index +'" />';
                    }
                );

                $scope.hidden_form_data = $sce.trustAsHtml($scope.hidden_form_data);
            }
        };

        $scope.clearSelected = function () {
            $scope.total_count = 0;
            $scope.checkboxes = false;
            $scope.submit_disabled = true;
            $scope.checkboxes_disabled = false;
            $scope.hidden_form_data = '';
        };
    }]
);