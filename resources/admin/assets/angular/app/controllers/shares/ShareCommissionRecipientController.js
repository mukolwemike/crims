
app.controller('ShareCommissionRecipientController', ['$scope', 'ShareCommissionRecipients', function ($scope, ShareCommissionRecipients) {
        $scope.recipientId = '';
        $scope.start = '';
        $scope.end = '';
        $scope.overridesLoading = false;
        $scope.schedulesLoading = false;
        $scope.selectedSchedule = {};

        $scope.$watch(function () {
            return $scope.recipientId;
        }, function (newValue, oldValue) {
            if(newValue)
            {
                $scope.overridesLoading = true;
                $scope.schedulesLoading = true;
                ShareCommissionRecipients.getCommissionTotals($scope.recipientId, $scope.start, $scope.end)
                    .then(function (response) {
                        $scope.totals = response.data;
                    });

                ShareCommissionRecipients.getCommissionSchedules($scope.recipientId, $scope.start, $scope.end)
                    .then(function (response) {
                        $scope.schedules = response.data;
                        $scope.selectedSchedule = $scope.schedules[0];
                        $scope.schedulesLoading = false;
                    });

                ShareCommissionRecipients.getCommissionOverrides($scope.recipientId, $scope.start, $scope.end)
                    .then(function (response) {
                        $scope.overrides = response.data;
                        $scope.overridesLoading = false;
                    });
            }
        });
    }]
);