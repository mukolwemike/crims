
app.controller(
    'ShareholdersSharesPurchaseGridController',
    ['$scope', 'ShareholdersSharesPurchases', '$location', function ($scope, ShareholdersSharesPurchases, $location) {

        var url = $location.absUrl().split('/');
        $scope.share_holder_id = url[url.length - 1];

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;
            var start = pagination.start || 0;
            var number = pagination.number || 10;

            ShareholdersSharesPurchases.getPage(tableState, $scope.share_holder_id).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);