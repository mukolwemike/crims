/**
 * Created by interntwo on 09/02/2016.
 */

app.controller(
    'SharesBusinessConfirmationController',
    ['$scope','SharesBusinessConfirmationApplications', function ($scope, SharesBusinessConfirmationApplications) {
        $scope.displayed = [];


        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;
            var number = pagination.number || 10;

            SharesBusinessConfirmationApplications.getPage(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };
    }]
);