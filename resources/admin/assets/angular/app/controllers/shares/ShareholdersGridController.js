
app.controller(
    'ShareholdersGridController',
    ['$scope', 'Shareholder', function ($scope, Shareholder) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;
            var start = pagination.start || 0;
            var number = pagination.number || 10;

            Shareholder.getAll(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);

