/**
 * Created by interntwo on 10/03/2016.
 */

app.controller(
    'SharesStatementCampaignController',
    ['$scope', 'SharesStatementCampaign', function ($scope, SharesStatementCampaign) {

        $scope.displayed = [];
        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;
            var number = pagination.number || 10;

            SharesStatementCampaign.getItemsPage(tableState, $scope.campaign_id).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);