app.controller(
    'ShareholdersRedemptionGridController',
    ['$scope', 'ShareholderRedemption', function ($scope, ShareholderRedemption) {
        $scope.displayed = [];
        $scope.holderId = null;
        $scope.isLoading = false;
        $scope.date = null;

        $scope.$watch(function () {
            return $scope.date;
        }, function (oldValue, newValue) {
            if (newValue) {
                $scope.isLoading = true;

                $scope.date = moment($scope.date).format('YYYY-MM-DD');

                ShareholderRedemption.get($scope.holderId, $scope.date)
                    .then(function (response) {
                        $scope.rowCollection = response.data.shares;
                        $scope.total = response.data.total;
                        $scope.total_shares = response.data.total_shares;
                        $scope.displayed = [].concat[$scope.rowCollection];
                        $scope.isLoading = false;
                    });
            }
        });
    }]
);

