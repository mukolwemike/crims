/**
 * Controller for display of the closed periods for the approvals
 */
app.controller(
    "closedPeriodsController",
    ['$scope', 'ClosedPeriods', 'TableState', function ($scope, ClosedPeriods, TableState) {
        $scope.displaed = [];
        $scope.active = '';
        $scope.isLoading = true;

        $scope.callServer = function callServer(tableState)
        {
            $scope.tableState = tableState;

            $scope.filters = {};

            if ($scope.active !== '') {
                $scope.filters.active = $scope.active;
            }

            TableState.renderTable($scope, tableState, ClosedPeriods.getRates(tableState, $scope.filters));
        };
    }]
);