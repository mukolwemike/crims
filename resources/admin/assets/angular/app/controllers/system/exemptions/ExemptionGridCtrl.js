/**
 * Controller for display of the exemptions
 */
app.controller(
    "exemptionsController",
    ['$scope', 'Exemptions', 'TableState', function ($scope, Exemptions, TableState) {
        $scope.displaed = [];
        $scope.type = '';
        $scope.isLoading = true;

        $scope.callServer = function callServer(tableState)
        {
            $scope.tableState = tableState;

            $scope.filters = {};

            if ($scope.type !== '') {
                $scope.filters.type = $scope.type;
            }

            TableState.renderTable($scope, tableState, Exemptions.getExemptions(tableState, $scope.filters));
        };
    }]
);
