/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 30/01/2018.
 * Project crims
 */
app.controller(
    'BatchApprovalController',
    ['$scope', '$http', function ($scope, $http) {
        $scope.selected = [];
        $scope.submitting = false;
        $scope.approvalType = 'client';

        $("#select_all").change(
            function () {
                let el = $('.select input[type="checkbox"]');

                el.prop("checked", this.checked);
                var checked = this.checked;
                el.each(
                    function () {
                        let index = $(this).attr('index');
                        let id = $(this).attr('ng-true-value');

                        $scope.selected[index] = checked ? id : null;
                    }
                );
            }
        );

        $scope.getApprovalUrl = function () {
            if ($scope.approvalType == 'portfolio') {
                return '/dashboard/portfolio/approve/';
            }
            else {
                return '/dashboard/investments/approve/'
            }
        };

        $scope.submit = function (event, stage_id) {
            event.preventDefault();
            event.stopPropagation();
            $scope.submitting = true;

            let selected = $scope.selected.map(
                function (val) {
                    return parseInt(val.replace(/\"/g, ""));
                }
            ).filter(
                function (val) {
                    return val  > 0;
                }
            );

            var total = selected.length;

            let promises = selected.map(function (appr) {

                    let p = $http.get($scope.getApprovalUrl() +appr+'/stage/'+stage_id);

                    p.then(
                        function (response) {
                            $('#status'+appr).html("<i class='fa fa-check'></i>");
                            $('#status'+appr).attr('data-original-title', 'Approval Successful').tooltip('fixTitle')
                        },
                        function (response) {
                            let msg = '';
                            if (response.data.hasOwnProperty('message')) {
                                msg = response.data.message;
                            }

                            $('#status'+appr).html("<i class='fa fa-warning'></i>");
                            $('#status'+appr).attr('data-original-title', 'Failed: '+msg).tooltip('fixTitle');
                            $('#reject_field'+appr).val(msg);
                        }
                    );



                    return p;
                }
            );

            Promise.all(promises)
            .then(
                function () {
                    $scope.submitting = false;
                    window.toastr['info']("Approval completed!");
                }
            );
        };

        $scope.reject = function (event, trans_id, stage_id) {
            event.preventDefault();
            event.stopPropagation();
            $scope.rejecting = true;

            $http.post($scope.getApprovalUrl() +trans_id+'/reject/'+stage_id, {'reason': $('#reject_field'+trans_id).val()})
            .then(
                function (response) {
                    window.toastr['info']("Transaction rejected");
                    $scope.rejecting = false;
                    $('#status'+trans_id).text('Rejected');
                },
                function (response) {
                    let msg = '';
                    if (response.data.hasOwnProperty('message')) {
                        msg = response.data.message;
                    }

                    window.toastr['error']('Rejection failed: '+msg);
                    $scope.rejecting = false;
                }
            );
        }
    }]
);