app.controller(
    'SummerNoteController',
    ['$scope', function ($scope) {
        $scope.options = {
            height: 150,
            focus: true,
            toolbar: [
            ['edit',['undo','redo']],
            ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript']],
            ['fontface', ['fontname']],
            ['textsize', ['fontsize']],
            ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
            ]
            // popover: {
            //     link: [
            //         ['link', ['linkDialogShow', 'unlink']]
            //     ],
            // }
        };

        $scope.onPaste = function (e) {
            //First define the clean content
            var cleanUp = e.originalEvent.clipboardData.getData('text');

            //Have it wait for 0.5 seconds, before using $apply to update summernote model
            setTimeout(
                function () {
                    $scope.$apply(
                        function () {
                            $scope.summernote = cleanUp;
                        }
                    );
                },
                500
            );

        };
    }]
);