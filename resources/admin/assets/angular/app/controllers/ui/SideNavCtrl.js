/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 18/05/2016.
 * Project crims
 */

app.controller(
    'sideMenuCtrl',
    ['$scope', 'localStorageService', function ($scope, localStorageService) {

        $scope.menuexpanded = localStorageService.get('menuexpanded');
        $scope.minimised = localStorageService.get('sidebarMinimised');
        document.body.querySelector("#sidebar-wrapper").classList.remove("hide");


        if (!$scope.menuexpanded) {
            $scope.menuexpanded = {
                investment: false,
                portfolio: false,
                users: false
            };
        }


        $scope.menuexpand = function (menuitem) {
            $scope.menuexpanded[menuitem] = !$scope.menuexpanded[menuitem];
            $scope.toggleMenus(menuitem);
        };

        $scope.toggleMenus = function (except) {
            for (var property in $scope.menuexpanded) {
                if ($scope.menuexpanded.hasOwnProperty(property)) {
                    if (property != except) {
                        $scope.menuexpanded[property] = false;
                    }
                }
            }
        };
        $scope.$watch(
            function () {
                return $scope.minimised; },
            function (oldValue, newValue) {
                if ($scope.minimised) {
                    //minimise the menus
                    document.body.querySelector("#client_investments").classList.remove("in");
                    document.body.querySelector("#portfolio").classList.remove("in");
                    document.body.querySelector("#users").classList.remove("in");
                }
            }
        );

        $scope.$watchCollection(
            function () {
                return $scope.menuexpanded; },
            function (oldValue, newValue) {
                if (newValue) {
                    var inv = document.body.querySelector("#client_investments");
                    var port = document.body.querySelector("#portfolio");
                    var usr = document.body.querySelector("#users");
                    if (newValue.investment === true) {
                        inv.className = inv.className+" in";
                    } else {
                        inv.classList.remove("in");
                    }
                    if (newValue.portfolio === true) {
                        port.className = port.className+" in";
                    } else {
                        port.classList.remove("in");
                    }
                    if (newValue.users === true) {
                        usr.className = usr.className+" in";
                    } else {
                        usr.classList.remove("in");
                    }
                    localStorageService.set('menuexpanded', $scope.menuexpanded);
                }
            },
            true
        );
    }]
);