
app.controller(
    'BankBranchController',
    ['$scope', 'Branches', function ($scope, Branches) {
        $scope.branch = {};
        $scope.branches = {};
        $scope.loading = false;

        $scope.getBankBranches = function () {
            $scope.loading = true;
            if ($scope.bank_id) {
                Branches.getBranches($scope.bank_id)
                .then(
                    function (response) {
                        $scope.branches = response.data;
                    },
                    function (response) {
                        console.log('error occurred while fetching branches');
                    }
                )
                    .finally(
                        function () {
                            $scope.loading = false;
                        }
                    );
            }
        };

        $scope.$watch(
            'bank_id',
            function () {
                if ($scope.bank_id !== null) {
                    $scope.loading = true;
                    if ($scope.bank_id) {
                        Branches.getBranches($scope.bank_id)
                        .then(
                            function (response) {
                                $scope.branches = response.data;
                            },
                            function (response) {
                                console.log('error occurred while fetching branches');
                            }
                        )
                        .finally(
                            function () {
                                $scope.loading = false;
                            }
                        );
                    }
                }
            },
            true
        );
    }]
);