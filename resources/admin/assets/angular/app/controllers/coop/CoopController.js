app.controller(
    'CoopController',
    ['$scope', function ($scope) {

        $scope.nominees = [{id: '1'}];

        $scope.addNewNominee = function () {
            var newItemNo = $scope.nominees.length + 1;
            $scope.nominees.push({'id':newItemNo});
        };

        $scope.removeNominee = function () {
            var lastItem = $scope.nominees.length-1;
            $scope.nominees.splice(lastItem);
        };
    }]
);