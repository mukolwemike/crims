/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 17/09/2016.
 * Project crims
 */
app.controller(
    'CoopMembershipConfirmationsController',
    ['$scope', 'TableState', 'ClientPayments', function ($scope, TableState, ClientPayments) {

        $scope.callServer = function (tableState) {
            TableState.renderTable($scope, tableState, ClientPayments.getPage(tableState, 'M', 'membership_confirmation'));
        };
    }]
);