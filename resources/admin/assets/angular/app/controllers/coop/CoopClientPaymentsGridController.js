
app.controller(
    'CoopClientPaymentsGridController',
    ['$scope', 'CoopClientPayments', '$location', function ($scope, CoopClientPayments, $location) {

        var url = $location.absUrl().split('/');
        $scope.client_id = url[url.length - 1];
        $scope.balance = 0;

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;
            var start = pagination.start || 0;
            var number = pagination.number || 10;

            CoopClientPayments.getPage(tableState, $scope.client_id).then(
                function (result) {

                    angular.forEach(
                        result.data.data,
                        function (value, key) {
                            $scope.balance += value.amount;
                        }
                    );

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);