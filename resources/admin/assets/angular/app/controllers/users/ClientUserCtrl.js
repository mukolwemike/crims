/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 26/09/2016.
 * Project crims
 */

app.controller(
    'ClientUserCtrl',
    ['$scope', '$http', 'Clients', 'CommissionRecipient', function ($scope, $http, Clients, CommissionRecipient) {

        $scope.existing_client = 2;
        $scope.seachedFa = '';
        $scope.selectedClient = '';
        $scope.loading_fas = false;

        $scope.searchClients = function (name) {
            $scope.loading = true;

            return Clients.search(name)
            .then(
                function (response) {
                    $scope.loading = false;

                    return response.data.data;
                },
                function (response) {

                    $scope.loading = false;
                }
            );
        };

        $scope.searchFas = function (name) {
            $scope.seachedFa = name;

            $scope.loading_fas = true;

            return CommissionRecipient.search($scope.seachedFa)
                .then(
                    function (response) {
                        $scope.loading_fas = false;

                        return response.data.data;
                    },
                    function (response) {
                        $scope.loading_fas = false;
                    }
                );
        };

        $scope.revokeClient = function (name, id) {
            let response = window.confirm('Do you want to revoke access to '+ name+'?');

            if (response) {
                window.location.href="/dashboard/users/client/revoke/"+id;
            }
        };

        $scope.removeSignature = function (name, signature_id, user_id) {

            let response = window.confirm('Do you want to remove '+ name +' signature from this user?');

            if (response) {
                window.location.href="/dashboard/users/"+user_id+"/signature/"+signature_id+"/remove";
            }
        };

        $scope.revokeFa = function (name, fa_id, user_id) {
            let response = window.confirm('Do you want to revoke access to '+ name +'?');

            if (response) {
                window.location.href = "/dashboard/users/client/revoke/"+ fa_id +"/fa/"+ user_id;
            }
        };
    }]
);