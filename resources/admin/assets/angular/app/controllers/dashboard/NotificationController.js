/**
 * Created by interntwo on 09/02/2016.
 */

app.controller(
    'NotificationController',
    ['$scope','Notifications', function ($scope, Notifications) {
        $scope.displayed = [];


        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            Notifications.getPage(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;


                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update
                    //tableState.pagination.number = result.data.meta.cursor.count;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };
    }]
);

app.controller(
    'UnreadNotificationsController',
    ['$scope', 'Notifications', '$timeout', '$interval', function ($scope, Notifications, $timeout, $interval) {

        $scope.unread_count = 0;

        $scope.notifications = [];

        $scope.update = function () {
            Notifications.getAllUnread()
            .then(
                function (response) {
                    $scope.notifications = response.data.data;
                    $scope.unread_count = $scope.notifications.length
                },
                function (response) {

                }
            );
        };

        //wait 300ms before sending request, to ensure it goes last
        $timeout(
            function () {
                $scope.update();
            },
            100
        );

        //wait 10s then start timeout (10300 = 10000 +300)
        $timeout(
            function () {
                $interval($scope.update(), 10000);
            },
            10100
        );

    }]
);