/**
 * Created by interntwo on 08/04/2016.
 */

app.controller(
    'InvestmentActivityGridCtrl',
    ['$scope', 'ActivityLog', 'TableState', function ($scope, ActivityLog, TableState) {

        $scope.loading = true;
        $scope.failed = false;

        var acts = JSON.parse(localStorage.getItem('activity_log'));

        if (acts) {
            $scope.invCollection = acts.investments;
            $scope.confCollection = acts.confirmations;
            $scope.appCollection = acts.applications;
            $scope.instrCollection = acts.instructions;
            $scope.payCollection = acts.payment;

            $scope.loading = false;
        }

        $scope.updateData = function () {
            // ActivityLog.getInvestments($scope.start, $scope.end)
            //     .then(function(response){
            //         $scope.activities = response.data;
            //         var acts = $scope.activities;
            //
            //         $scope.invCollection = acts.investments;
            //         $scope.confCollection = acts.confirmations;
            //         $scope.appCollection = acts.applications;
            //         $scope.instrCollection = acts.instructions;
            //         $scope.payCollection = acts.payment;
            //
            //         localStorage.setItem('activity_log', JSON.stringify(response.data));
            //
            //         $scope.loading = false;
            //     }, function(response){
            //         $scope.failed = true;
            //         $scope.loading = false;
            //     });
        };

        $scope.$watchCollection(
            function () {
                return { start: $scope.start, end: $scope.end}; },
            function (oldValue, newValue) {

                if ($scope.start && $scope.end) {
                    $scope.updateData();
                }
            }
        );

        $scope.invDisplayedCollection = [].concat($scope.invCollection);
        $scope.confDisplayedCollection = [].concat($scope.confCollection);
        $scope.appDisplayedCollection = [].concat($scope.appCollection);
        $scope.instrDisplayedCollection = [].concat($scope.instrCollection);
        $scope.payDisplayedCollection = [].concat($scope.payCollection);

        $scope.getClientTransactions = function (tableState) {
            TableState.renderTable($scope, tableState, ActivityLog.getClientTransactionsPage(tableState,  $scope.$parent.start, $scope.$parent.end));
        };

        $scope.getPortfolioTransactions = function (tableState) {
            TableState.renderTable($scope, tableState, ActivityLog.getPortfolioTransactionsPage(tableState,  $scope.$parent.start, $scope.$parent.end));
        };


    }]
);
