/**
 * Created by interntwo on 04/02/2016.
 */

app.controller(
    'DeductionsGridController',
    ['$scope', 'Deductions', 'TableState', function ($scope, Investments, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, Investments.getPage(tableState));
        };
    }]
);
