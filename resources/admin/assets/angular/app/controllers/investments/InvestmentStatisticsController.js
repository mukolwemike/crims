/**
 * Created by interntwo on 07/03/2016.
 */

app.controller(
    'InvestmentStatisticsController',
    ['$scope', 'Clients', 'Persistence', 'DepositWeightedTenor', 'ClientWeightedTenor','Product', function ($scope, Clients, Persistence, DepositWeightedTenor, ClientWeightedTenor, Product) {

        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.statusTwo = {
            opened: false
        };
        $scope.openTwo = function ($event) {
            $scope.statusTwo.opened = true;
        };

        $scope.statusThree = {
            opened: false
        };
        $scope.openThree = function ($event) {
            $scope.statusThree.opened = true;
        };

        $scope.updateStats = function () {

            Product.getTotalInvestments($scope.product_id)
            .then(
                function (response) {
                    $scope.totalInvestments = response.data;
                },
                function (response) {

                }
            );

            Product.getTotalInvestmentsValue($scope.product_id)
            .then(
                function (response) {
                    $scope.totalInvestmentsValue = response.data;
                },
                function (response) {

                }
            );

            Product.getTotalWithholdingTax($scope.product_id)
            .then(
                function (response) {
                    $scope.totalWithholdingTax = response.data;
                },
                function (response) {

                }
            );

            Product.getTotalManagementFees($scope.product_id)
            .then(
                function (response) {
                    $scope.totalManagementFees = response.data;
                },
                function (response) {

                }
            );

            Product.getTotalCustodyFees($scope.product_id)
            .then(
                function (response) {
                    $scope.totalCustodyFees = response.data;
                },
                function (response) {

                }
            );

            Product.getClientWeightedRate($scope.product_id)
            .then(
                function (response) {
                    $scope.clientWeightedRate = response.data;
                },
                function (response) {

                }
            );

            $scope.updatePersistence = function () {
                Persistence.get($scope.product_id, {date: $scope.persistencedate})
                .then(
                    function (response) {
                        $scope.persistence = response.data;
                    },
                    function (){}
                );
            };

            $scope.$watch(
                'persistencedate',
                function (oldValue, newValue) {
                    if (newValue) {
                        $scope.updatePersistence();
                    }
                }
            );

            $scope.$watch(
                'deposit_tenor_date',
                function (newvalue, oldValue) {

                    if (newvalue) {
                        DepositWeightedTenor.get($scope.product_id, { date: newvalue})
                        .then(
                            function (response) {
                                $scope.deposit_weighted_tenor = response.data;
                            },
                            function (){}
                        );
                    }
                }
            );

            $scope.$watch(
                'client_tenor_date',
                function (oldValue, newValue) {
                    if (newValue) {
                        ClientWeightedTenor.get($scope.product_id, { date: $scope.client_tenor_date})
                        .then(
                            function (response) {
                                $scope.client_weighted_tenor = response.data;
                            },
                            function (){}
                        );
                    }
                }
            );
        };

        $scope.$watch(
            'product_id',
            function () {
                if ($scope.product_id) {
                    $scope.updateStats();
                }
                console.log($scope.product_id);
            }
        );

        $scope.$watch(
            'currency_id',
            function (oldValue, newValue) {
                console.log($scope.currency_id);
            }
        );

    }]
);