/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 13/08/2016.
 * Project crims
 */
app.controller(
    'InvestmentAnalyticsController',
    ['$scope', 'InvestmentAnalytics', function ($scope, InvestmentAnalytics) {

        $scope.totalCostValue = function () {
            InvestmentAnalytics.costValue($scope.type, $scope.entity_id, $scope.date)
            .then(
                function (response) {
                    $scope.total_cost_value = response.data;
                    $scope.loading_cost = false;
                }
            );
        };

        $scope.totalMarketValue = function () {
            InvestmentAnalytics.marketValue($scope.type, $scope.entity_id, $scope.date)
            .then(
                function (response) {
                    $scope.total_market_value = response.data;
                    $scope.loading_market = false;
                }
            );
        };

        $scope.totalWithholdingTax = function () {
            InvestmentAnalytics.withholdingTax($scope.type, $scope.entity_id, $scope.date)
            .then(
                function (response) {
                    $scope.total_withholding_tax = response.data;
                    $scope.loading_wht = false;
                }
            );
        };

        $scope.totalCustodyFees = function () {
            InvestmentAnalytics.custodyFees($scope.type, $scope.entity_id, $scope.date)
            .then(
                function (response) {
                    $scope.total_custody_fees = response.data;
                    $scope.loading_custody = false;
                }
            );
        };

        $scope.totalManagementFees = function () {
            InvestmentAnalytics.managementFees($scope.type, $scope.entity_id, $scope.date)
            .then(
                function (response) {
                    $scope.total_management_fees = response.data;
                    $scope.loading_mgt = false;
                }
            );
        };

        $scope.weightedRate = function () {
            InvestmentAnalytics.weightedRate($scope.type, $scope.entity_id, $scope.date)
            .then(
                function (response) {
                    $scope.weighted_rate = response.data;
                    $scope.loading_rate = false;
                }
            );
        };

        $scope.weightedTenor = function () {
            InvestmentAnalytics.weightedTenor($scope.type, $scope.entity_id, $scope.date)
            .then(
                function (response) {
                    $scope.weighted_tenor = response.data;
                    $scope.loading_tenor = false;
                }
            );
        };

        $scope.getPersistence = function () {
            InvestmentAnalytics.persistence($scope.type, $scope.entity_id, $scope.from_date, $scope.date)
            .then(
                function (response) {
                    $scope.persistence = response.data;
                    $scope.loading_persistence = false;
                }
            );
        };

        $scope.update = function (event) {
            $scope.loading_cost = true;
            $scope.totalCostValue();
            $scope.loading_market = true;
            $scope.totalMarketValue();
            $scope.loading_wht = true;
            $scope.totalWithholdingTax();
            $scope.loading_mgt = true;
            $scope.totalManagementFees();
            $scope.loading_custody = true;
            $scope.totalCustodyFees();
            $scope.loading_rate = true;
            $scope.weightedRate();
            $scope.loading_tenor = true;
            $scope.weightedTenor();
            $scope.loading_persistence = true;
            $scope.getPersistence();
        };

        $scope.$watchCollection(
            function () {
                return {
                    a: $scope.product_id,
                    b: $scope.currency_id,
                    c: $scope.type,
                    d: $scope.date,
                    e: $scope.from_date
                };

            },
            function () {
                if ($scope.type == 'products') {
                    $scope.entity_id = $scope.product_id;
                } else {
                    $scope.entity_id = $scope.currency_id;
                }

                if (!$scope.updated) {
                    $scope.update();
                }

                $scope.updated = 1;
            }
        );

    }]
);