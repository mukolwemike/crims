app.controller(
    'ClientTransactionsGridController',
    ['$scope', 'ClientPayments', function ($scope, ClientPayments) {

        $scope.displayed = [];

        $scope.setClientId = function (client_id) {
            $scope.client_id = client_id;
        };

        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;
            var start = pagination.start || 0;
            var number = pagination.number || 10;

            ClientPayments.getClientPayments(tableState, $scope.client_id).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.balance = 0;
                    angular.forEach(
                        $scope.displayed,
                        function (value, key) {
                            $scope.balance += parseInt(value.amount);
                        }
                    );
                    $scope.meta = result.data.meta;

                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };

    }]
);