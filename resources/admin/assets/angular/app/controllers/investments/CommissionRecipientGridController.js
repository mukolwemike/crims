/**
 * Created by interntwo on 23/03/2016.
 */

app.controller(
    'CommissionRecipientGridController',
    ['$scope', 'CommissionRecipient', 'TableState', function ($scope, CommissionRecipient, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            return TableState.renderTable($scope, tableState, CommissionRecipient.getRecipients(tableState));
        };

    }]
);