/**
 * Created by interntwo on 23/03/2016.
 */

app.controller(
    'CommissionPerMonthGridController',
    ['$scope', 'CommissionRecipient', function ($scope, CommissionRecipient) {

        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.statusSecond = {
            opened: false
        };
        $scope.openSecond = function ($event) {
            $scope.statusSecond.opened = true;
        };

        $scope.displayed = [];


        $scope.callServer = function callServer(tableState)
        {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            CommissionRecipient.getCommissionPerMonth(tableState, $scope.product_id).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;


                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update
                    //tableState.pagination.number = result.data.meta.cursor.count;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );
        };
    }]
);