/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 10/08/2016.
 * Project crims
 */

app.controller(
    'ActivityLogPageController',
    ['$scope', function ($scope) {
        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.statusSecond = {
            opened: false
        };
        $scope.openSecond = function ($event) {
            $scope.statusSecond.opened = true;
        };
    }]
);