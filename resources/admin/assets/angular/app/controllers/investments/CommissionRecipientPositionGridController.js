app.controller('CommissionRecipientPositionGridController', ['$scope', 'CommissionRecipientPositions', 'TableState', function ($scope, CommissionRecipientPositions, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState) {
            return TableState.renderTable($scope, tableState, CommissionRecipientPositions.getRecipientPositions(tableState, $scope.commission_recepient_id));
        }
    }]
);