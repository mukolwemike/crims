/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 28/06/2016.
 * Project crims
 */

app.controller(
    'InvestmentApplicationCtrl',
    ['$scope', 'Clients', function ($scope, Clients) {

        $scope.status = {
            opened: false
        };

        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        var form = document.getElementById('application_form');
        form.noValidate = false;

        $scope.$watch(
            'complete',
            function () {
                form.noValidate = $scope.complete != 1;
            }
        );

        $scope.$watchGroup(
            ['selectedClient', 'product_id'],
            function () {
                if ($scope.selectedClient !== undefined) {
                    if ($scope.selectedClient.id !== undefined) {
                        return Clients.getProductPaymentsBalance($scope.selectedClient.id, $scope.product_id)
                        .then(
                            function (response) {
                                $scope.payments_balance = response.data;
                                $scope.client_id_is_set = true;
                            },
                            function () {}
                        );
                    } else {
                        $scope.client_id_is_set = false;
                    }
                } else {
                    $scope.client_id_is_set = false;
                }
            },
            true
        );

        $scope.searchClients = function (name) {
            $scope.loading = true;

            return Clients.search(name)
            .then(
                function (response) {
                    $scope.loading = false;

                    return response.data.data;
                },
                function () {
                    $scope.loading = false;
                }
            );
        };

    }]
);