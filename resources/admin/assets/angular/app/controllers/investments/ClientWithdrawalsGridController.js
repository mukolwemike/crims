app.controller(
    'ClientWithdrawalsGridController',
    ['$scope', 'ClientWithdrawals', 'TableState', function ($scope, ClientWithdrawals, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, ClientWithdrawals.getPage(tableState));
        };
    }]
);
