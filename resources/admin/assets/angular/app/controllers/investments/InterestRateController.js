app.controller(
    'InterestRateController',
    ['$scope', '$location', 'InterestRateUpdates', function ($scope, $location, InterestRateUpdates) {

        $scope.rates = [];

        $scope.rate = { id:null, tenor:null, rate:null};

        $scope.addRate = function () {
            $scope.rates.push($scope.rate);
        };

        $scope.removeRate = function () {
            $scope.rates.pop();
        };
        $scope.addRate();
    }]
);