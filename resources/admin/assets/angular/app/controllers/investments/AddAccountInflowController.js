app.controller(
    'AddAccountInflowController',
    ['$scope', 'Product', 'RealEstateProject', 'ShareEntity', 'UnitFund', 'CustodialAccount', function ($scope, Product, RealEstateProject, ShareEntity, UnitFund, CustodialAccount) {

        $scope.src_currency = "Account Currency";
        $scope.dest_currency = "Product Currency";
        $scope.compatibility = {};
        $scope.reverse = false;
        $scope.exchange_rate = 1;
        $scope.effective_rate = 1;

        $scope.$watchGroup(
            ['category','product_id','entity_id', 'project_id', 'unit_fund_id'],
            function () {

                $scope.custodial_accounts = [];

                if ($scope.category === 'project') {
                    if ($scope.project_id !== undefined) {
                        RealEstateProject.getCustodialAccounts($scope.project_id)
                        .then(
                            function (response) {
                                $scope.custodial_accounts = response.data;
                            }
                        );
                    }
                } else if ($scope.category === 'product') {
                    if ($scope.product_id !== undefined) {
                        Product.getCustodialAccounts($scope.product_id)
                        .then(
                            function (response) {
                                $scope.custodial_accounts = response.data;
                            }
                        );
                    }
                } else if ($scope.category === 'shares') {
                    if ($scope.entity_id !== undefined) {
                        ShareEntity.getCustodialAccounts($scope.entity_id)
                        .then(
                            function (response) {
                                $scope.custodial_accounts = response.data;
                            }
                        );
                    }
                } else if ($scope.category === 'funds') {
                    if ($scope.unit_fund_id !== undefined) {
                        UnitFund.getCustodialAccounts($scope.unit_fund_id)
                        .then(
                            function (response) {
                                $scope.custodial_accounts = response.data;
                            }
                        );
                    }
                }

                console.log($scope.custodial_accounts);
            }
        );

        $scope.$watchGroup(
            ['amount', 'exchange_rate', 'reverse'],
            function () {
                $scope.effective_rate = $scope.reverse ? $scope.exchange_rate : 1/$scope.exchange_rate;
                $scope.converted = $scope.amount * $scope.effective_rate;
            }
        );
    }]
);