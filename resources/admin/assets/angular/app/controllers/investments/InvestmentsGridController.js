/**
 * Created by interntwo on 04/02/2016.
 */

app.controller(
    'InvestmentsGridController',
    ['$scope', 'Investments', 'TableState', function ($scope, Investments, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, Investments.getPage(tableState));
        };
    }]
);
