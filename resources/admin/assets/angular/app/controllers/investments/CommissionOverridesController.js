app.controller(
    'CommissionOverridesCtrl',
    ['$scope',  'CommissionOverrides', function ($scope, CommissionOverrides) {

        $scope.recipientId = null;
        $scope.currencyId = null;
        $scope.isLoading = false;
        $scope.overrides = [];
        $scope.additional_commission = [];
        $scope.startDate = '';
        $scope.endDate = '';

        $scope.$watch(function () {
            return $scope.endDate;
        }, function (oldValue, newValue) {
            if (newValue) {
                $scope.isLoading = true;
                CommissionOverrides.getOverrides($scope.recipientId, $scope.currencyId, $scope.startDate, $scope.endDate)
                    .then(function (response) {
                        $scope.overrides = response.data;
                        $scope.isLoading = false;
                    });

                if ($scope.currencyId == 1) {
                    CommissionOverrides.getAdditionalCommission($scope.recipientId, $scope.currencyId, $scope.startDate, $scope.endDate)
                        .then(function (response) {
                            $scope.additional_commission = response.data;
                            $scope.isLoading = false;
                        });
                }
            }
        });
    }]
);