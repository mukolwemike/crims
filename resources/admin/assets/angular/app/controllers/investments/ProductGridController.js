/**
 * Controller for display of the exemptions
 */
app.controller(
    "ProductGridController",
    ['$scope', 'Product', 'TableState', function ($scope, Product, TableState) {
        $scope.displaed = [];

        $scope.callServer = function callServer(tableState)
        {
            $scope.tableState = tableState;

            return TableState.renderTable($scope, tableState, Product.getProducts(tableState));
        };
    }]
);