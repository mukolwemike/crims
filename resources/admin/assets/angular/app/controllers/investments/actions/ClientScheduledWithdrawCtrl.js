/**
 * Created by interntwo on 21/04/2016.
 */

app.controller(
    'ClientScheduleWithdrawCtrl',
    ['$scope', 'ModalService', function ($scope, ModalService) {
        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "clientwithdraw.htm", controller: "ClientScheduleWithdrawConfirmCtrl"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'ClientScheduleWithdrawConfirmCtrl',
    ['$scope', function ($scope) {
        $scope.endDate = document.getElementById('end_date').value;
        $scope.premature = document.getElementById('premature').checked;
        $scope.amount = document.getElementById('amount').value;
        $scope.partial_withdraw = document.getElementById('partial_withdraw').checked;
        $scope.callback = document.getElementById('callback').checked;
        $scope.new_maturity_date  = document.getElementById('new_maturity_date').value;
    }]
);