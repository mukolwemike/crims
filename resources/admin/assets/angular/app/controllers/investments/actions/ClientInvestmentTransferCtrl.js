/**
 * Created by interntwo on 21/04/2016.
 */

app.controller(
    'ClientInvestmentTransferCtrl',
    ['$scope', 'Clients', function ($scope, Clients) {

        $scope.getClients = function () {
            Clients.getClientsByName($scope.name)
            .then(
                function (response) {
                    $scope.clients = response.data.data;

                },
                function (response) {
                    console.log('error occured fetching results');
                }
            );
        };



    }]
);