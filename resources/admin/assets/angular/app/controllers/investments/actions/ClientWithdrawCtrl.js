/**
 * Created by interntwo on 21/04/2016.
 */

app.controller(
    'clientWithdrawCtrl',
    ['$scope', 'ModalService', function ($scope, ModalService) {
        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            ModalService.showModal(
                {
                    templateUrl: "clientwithdraw.htm", controller: "clientwithdrawConfirmationCtrl"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };

    }]
);

app.controller(
    'clientwithdrawConfirmationCtrl',
    ['$scope', '$window', '$location', 'Investment', function ($scope, $window, $location, Investment) {
        var url_array = $location.path().split('/');
        $scope.investment_id = url_array[url_array.length - 1];

        $scope.endDate = document.getElementById('end_date').value;
        $scope.premature = document.getElementById('premature').value === 1;
        $scope.penalty_percentage = document.getElementById('penalty_percentage').value;
        $scope.penalty = 0;
        if ($scope.premature) {
            Investment.getNetInterestAtDate($scope.investment_id, $scope.endDate)
            .then(
                function (response) {
                    $scope.net_interest_at_date = response.data;
                }
            );
            Investment.penalty($scope.investment_id, $scope.endDate, $scope.penalty_percentage)
                .then(
                    function (result) {
                        $scope.penalty = result.data;
                    }
                );
            Investment.getTotalValueOfInvestmentAtDate($scope.investment_id, $scope.endDate)
                .then(
                    function (result) {
                        $scope.total_value_of_investment_at_date = result.data;
                    }
                );
        }
        $scope.amount = document.getElementById('amount').value;
        $scope.deduct_penalty = document.getElementById('deduct_penalty').value === 1;
        $scope.partial_withdraw = document.getElementById('partial_withdraw').value === 1;
        $scope.callback = document.getElementById('callback').checked;
    }]
);
