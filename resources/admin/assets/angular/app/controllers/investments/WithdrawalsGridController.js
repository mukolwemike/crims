app.controller(
    'WithdrawalsGridController',
    ['$scope', 'Withdrawals', 'TableState', function ($scope, Withdrawals, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, Withdrawals.getPage(tableState));
        };
    }]
);
