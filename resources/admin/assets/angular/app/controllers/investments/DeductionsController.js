/**
 * Created by interntwo on 12/03/2016.
 */

app.controller(
    'DeductionsController',
    ['$scope', 'ModalService', '$rootScope', function ($scope, ModalService, $rootScope) {

        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.submit = function (event) {
            event.preventDefault();

            if ($scope.form.$valid) {
                $scope.confirmModal();
            }
        };

        $scope.confirmModal = function () {
            $rootScope.amount = $scope.amount;
            $rootScope.narrative = $scope.narrative;
            $rootScope.date = $scope.date;

            ModalService.showModal(
                {
                    templateUrl: "deductions.htm", controller: "DeductionsConfirmController"
                }
            ).then(
                function (modal) {
                    modal.element.modal();
                }
            );
        };
    }]
);

app.controller(
    'DeductionsConfirmController',
    ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.date = document.getElementById('date').value;
        $scope.amount = $rootScope.amount;
        $scope.narrative = $rootScope.narrative;
    }]
);