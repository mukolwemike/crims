/**
 * Created by interntwo on 31/03/2016.
 */
app.controller(
    'InterestPaymentGridCtrl',
    ['$scope', 'Investments', 'Download', '$timeout', function ($scope, Investments, Download, $timeout) {


        $scope.print = function () {
            $scope.bk = $scope.itemsByPage;
            $scope.printing = 10001;
            $scope.itemsByPage = 100000000000;
            $scope.callServer($scope.tableState);
            $scope.printing = false;


            $timeout(
                function () {
                    $scope.itemsByPage = $scope.bk;
                },
                100000
            );
        };

        $scope.download = function () {
            Download.downloadInterestPaymentsXlsx($scope.tableState);
        };

        $scope.displayed = [];

        $scope.status = {
            opened: false
        };
        $scope.open = function ($event) {
            $scope.status.opened = true;
        };

        $scope.callServer = function callServer(tableState)
        {

            $scope.tableState = tableState;

            console.log(tableState);

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            Investments.getInterestPayments(tableState).then(
                function (result) {

                    $scope.displayed = result.data.data;
                    $scope.meta = result.data.meta;


                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update
                    //tableState.pagination.number = result.data.meta.cursor.count;

                    $scope.perPage = tableState.pagination.number;

                    $scope.isLoading = false;
                }
            );

            if ($scope.printing == 10001) {
                window.print();
            }
        };
    }]
);