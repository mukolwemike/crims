/**
 *  Facilitate the addition of client guards
 */
app.controller(
    'addClientGuardController',
    ['$scope', function ($scope) {
        $scope.guardType = '';
    }]
);