/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 02/02/2017.
 * Project crims
 */

app.controller(
    'ApprovalActionCtrl',
    ['$scope', function ($scope) {
        $scope.submissions = 0;

        $scope.showModal = function (link) {
            swal(
                {
                    title: 'Are you sure?',
                    text: "Click approve to proceed",
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#5cb85c',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, approve!'
                }
            ).then(
                function () {
                    $scope.approve(link);
                    swal(
                        'Sending for approval',
                        'The transaction is being approved',
                        'success'
                    );
                },
                function () {
                }
            );
        };

        $scope.approve = function (link) {
            if ($scope.submissions === 0) {
                window.location.href = link;
                $scope.submissions +=1;
            }
        };
    }]
);