/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 22/07/2016.
 * Project crims
 */

app.controller(
    'ClientBankInstructionsGridCtrl',
    ['$scope', 'ClientBankInstruction', 'TableState', function ($scope, ClientBankInstruction, TableState) {
        $scope.displayed = [];


        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, ClientBankInstruction.getPage(tableState));
        };

        $scope.getClientPage = function (tableState) {
            TableState.renderTable($scope, tableState, ClientBankInstruction.getClientPage(tableState, $scope.client_id));
        };
    }]
);