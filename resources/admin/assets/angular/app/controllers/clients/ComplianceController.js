/**
 * Created by interntwo on 09/02/2016.
 */


app.controller(
    'ComplianceController',
    ['$scope', 'Clients', 'TableState', function ($scope, Clients, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, Clients.getPage(tableState));
        };
    }]
);