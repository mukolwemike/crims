
app.controller(
    'ClientsGridController',
    ['$scope', 'Clients', 'TableState', function ($scope, Clients, TableState) {

        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, Clients.getPage(tableState));
        };
    }]
);

app.controller(
    'RiskyClientsGridController',
    ['$scope', 'RiskyClients', 'TableState', function ($scope, RiskyClients, TableState) {
        $scope.displayed = [];

        $scope.callServer = function callServer(tableState)
        {
            TableState.renderTable($scope, tableState, RiskyClients.getPage(tableState));
        };
    }]
);