app.factory(
    'ShareCommissionRecipients',
    ['$http', function ($http) {

        var recipient = {};

        recipient.getRates = function (recipientId) {
            return $http.get('/api/shares/getShareCommissionRates/' + recipientId);
        };

        recipient.getRecipientProductCommissionRate = function (recipientId, date, client_id) {
            return $http.post('/api/shares/get_share_commission_recipient_rate', {recipient_id : recipientId, date : date, client_id : client_id})
        };

        recipient.getCommissionTotals = function (recipientId, start, end) {
            return $http.get('/api/shares/commissions/totals/recipient/' + recipientId + '/start/' + start + '/end/' + end);
        };

        recipient.getCommissionSchedules = function (recipientId, start, end) {
            return $http.get('/api/shares/commissions/schedules/recipient/' + recipientId + '/start/' + start + '/end/' + end);
        };

        recipient.getCommissionOverrides = function (recipientId, start, end) {
            return $http.get('/api/shares/commissions/overrides/recipient/' + recipientId + '/start/' + start + '/end/' + end);
        };

        return recipient;
    }]
);
