/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 12/03/2017.
 * Project crims
 */

app.factory(
    'ShareEntity',
    ['$http', function ($http) {
        var e = {};

        e.getCustodialAccounts = function (entity_id) {
            return $http.get('/api/share_entities/'+entity_id+'/custodial-accounts');
        };

        return e;
    }]
);