app.factory(
    'SharesSales',
    ['TableState', '$http', function (TableState, $http) {

        var sales = {};

        sales.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/shares/sales');
        };

        return sales;
    }]
);
