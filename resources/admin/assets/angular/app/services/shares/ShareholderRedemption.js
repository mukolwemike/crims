app.factory(
    'ShareholderRedemption',
    ['$http', function ($http) {

        var holder = {};

        holder.get = function (holder, date) {
            return $http.get('/api/shares/shareholders/redemption/holder/' + holder + '/date/' + date);
        };

        return holder;
    }]
);
