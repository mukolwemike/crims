app.factory(
    'ShareholdersSharesPurchases',
    ['TableState', '$http', function (TableState, $http) {

        var purchases = {};

        purchases.getPage = function (tableState, share_holder_id) {
            return TableState.getPage(tableState, '/api/shares/purchases/shareholders/'+share_holder_id);
        };

        return purchases;
    }]
);
