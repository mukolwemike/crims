app.factory(
    'Shareholding',
    ['TableState', '$http', function (TableState, $http) {

        var holding = {};

        holding.getAll = function (tableState, share_holder_id) {
            return TableState.getPage(tableState, '/api/shares/shareholders/'+share_holder_id+'/shares');
        };

        return holding;
    }]
);
