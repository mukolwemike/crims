app.factory('ShareCommissions', ['$http', 'TableState', function ($http, TableState) {

        var commissions = {};

        commissions.getRecipients = function (tableState) {
                return TableState.getPage(tableState, '/api/shares/commissions');
            };

        return commissions;
    }]
);