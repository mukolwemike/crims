app.factory(
    'SharesStatementCampaign',
    ['TableState', function (TableState) {
        var campaign = {};

        campaign.getItemsPage = function (tableState, campaign_id) {
            return TableState.getPage(tableState, '/api/shares/statements/campaigns/'+campaign_id);
        };

        campaign.getMissingClients = function (tableState, campaign_id) {
            return TableState.getPage(tableState, '/api/shares/statements/campaigns/'+campaign_id+'/missing');
        };

        return campaign;
    }]
);