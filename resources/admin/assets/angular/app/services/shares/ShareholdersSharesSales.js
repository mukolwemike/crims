app.factory(
    'ShareholdersSharesSales',
    ['TableState', '$http', function (TableState, $http) {

        var sales = {};

        sales.getPage = function (tableState, share_holder_id) {
            return TableState.getPage(tableState, '/api/shares/sales/shareholders/'+share_holder_id);
        };

        return sales;
    }]
);
