app.factory(
    'SharesPurchases',
    ['TableState', '$http', function (TableState, $http) {

        var purchases = {};

        purchases.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/shares/purchases');
        };

        return purchases;
    }]
);
