app.factory(
    'SharesPayments',
    ['TableState', '$http', function (TableState, $http) {

        var payments = {};

        payments.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/shares/payments');
        };

        return payments;
    }]
);
