app.factory(
    'Shareholder',
    ['TableState', '$http', function (TableState, $http) {

        var holder = {};

        holder.getAll = function (tableState) {
            return TableState.getPage(tableState, '/api/shares/shareholders');
        };

        return holder;
    }]
);
