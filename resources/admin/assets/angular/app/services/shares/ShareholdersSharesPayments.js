app.factory(
    'ShareholdersSharesPayments',
    ['TableState', '$http', function (TableState, $http) {

        var payments = {};

        payments.getPage = function (tableState, share_holder_id) {
            return TableState.getPage(tableState, '/api/shares/payments/shareholders/'+share_holder_id);
        };

        return payments;
    }]
);
