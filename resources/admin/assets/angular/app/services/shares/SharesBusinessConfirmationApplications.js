/**
 * Created by interntwo on 09/02/2016.
 */

app.factory(
    'SharesBusinessConfirmationApplications',
    ['TableState', function (TableState) {

        var applications = {};

        applications.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/shares/businessconfirmations');
        };

        return applications;
    }]
);