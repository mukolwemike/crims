/**
 * Closed Periods
 */
app.factory(
    'ClosedPeriods',
    ['$http', 'TableState', function ($http, TableState) {
        var rate = {};

        rate.getRates = function (tableState, filters) {
            return TableState.getPage(tableState, '/api/system/closed_periods', filters);
        };

        return rate;
    }]
);