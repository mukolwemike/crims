/**
 * Exemptions
 */
app.factory(
    'Exemptions',
    ['$http', 'TableState', function ($http, TableState) {
        var exemption = {};

        exemption.getExemptions = function (tableState, filters) {
            return TableState.getPage(tableState, '/api/system/exemptions', filters);
        };

        return exemption;
    }]
);