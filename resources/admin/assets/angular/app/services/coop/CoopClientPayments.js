app.factory(
    'CoopClientPayments',
    ['TableState', '$http', function (TableState, $http) {

        var payments = {};

        payments.getPage = function (tableState, id) {
            return TableState.getPage(tableState, '/api/coop/clients/'+id+'/payments');
        };

        return payments;
    }]
);
