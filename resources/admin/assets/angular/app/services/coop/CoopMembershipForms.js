app.factory(
    'CoopMembershipForms',
    ['TableState', '$http', function (TableState, $http) {

        var forms = {};

        forms.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/coop/memberships');
        };

        return forms;
    }]
);
