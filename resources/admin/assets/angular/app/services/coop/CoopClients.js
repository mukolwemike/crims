app.factory(
    'CoopClients',
    ['TableState', '$http', function (TableState, $http) {

        var clients = {};

        clients.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/coop/clients');
        };

        return clients;
    }]
);
