app.factory(
    'CoopPayments',
    ['TableState', '$http', function (TableState, $http) {

        var payments = {};

        payments.getPage = function (tableState, type, includes) {

            if (type) {
                if (!tableState.search.predicateObject) {
                    tableState.search.predicateObject = {};
                }

                tableState.search.predicateObject.type = type;
            }

            if (includes) {
                tableState.includes = includes;
            }

            return TableState.getPage(tableState, '/api/coop/payments');
        };

        return payments;
    }]
);
