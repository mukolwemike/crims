/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 16/07/2016.
 * Project crims
 */

app.factory(
    'ClientUser',
    ['TableState', function (TableState) {
        var user = {};

        user.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/users/clients');
        };

        return user;
    }]
);
