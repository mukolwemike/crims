/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 22/07/2016.
 * Project crims
 */


app.factory(
    'ClientBankInstruction',
    ['TableState', function (TableState) {
        var i = {};

        i.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/clients/bank-instruction');
        };

        i.getClientPage = function (tableState, client_id) {
            return TableState.getPage(tableState, '/api/clients/bank-instruction/'+client_id);
        };

        return i;
    }]
);