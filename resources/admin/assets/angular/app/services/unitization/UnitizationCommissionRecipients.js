app.factory(
    'UnitizationCommissionRecipients',
    ['$http', function ($http) {

        var recipient = {};

        recipient.getCommissionTotals = function (recipientId, start, end) {
            return $http.get('/api/unitization/commissions/totals/recipient/' + recipientId + '/start/' + start + '/end/' + end);
        };

        recipient.getCommissionSchedules = function (recipientId, start, end) {
            return $http.get('/api/unitization/commissions/schedules/recipient/' + recipientId + '/start/' + start + '/end/' + end);
        };

        recipient.getCommissionOverrides = function (recipientId, start, end) {
            return $http.get('/api/unitization/commissions/overrides/recipient/' + recipientId + '/start/' + start + '/end/' + end);
        };

        return recipient;
    }]
);
