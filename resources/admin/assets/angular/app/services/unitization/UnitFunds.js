app.factory(
    'UnitFund',
    ['$http', function ($http) {
        var e = {};

        e.getCustodialAccounts = function (unit_fund_id) {
            return $http.get(`/api/unit-funds/${unit_fund_id}/custodial-accounts`);
        };

        return e;
    }]
);