/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 04/07/2016.
 * Project crims
 */

app.factory(
    'RealEstatePayment',
    ['TableState', function (TableState) {
        var payment = {};

        payment.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/payments');
        };

        return payment;
    }]
);