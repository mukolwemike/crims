/**
 * Created by interntwo on 10/03/2016.
 */

app.factory(
    'RealEstateStatementCampaign',
    ['TableState', function (TableState) {
        var campaign = {};

        campaign.getItemsPage = function (tableState, campaign_id) {
            return TableState.getPage(tableState, '/api/realestate/statements/campaigns/'+campaign_id);
        };

        campaign.getMissingClients = function (tableState, campaign_id) {
            return TableState.getPage(tableState, '/api/realestate/statements/campaigns/'+campaign_id+'/missing');
        };

        return campaign;
    }]
);