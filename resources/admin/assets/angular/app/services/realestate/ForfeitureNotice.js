app.factory(
    'ForfeitureNotice',
    ['TableState', '$http', function (TableState, $http) {

        let forms = {};

        forms.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/forfeiture_notices');
        };

        return forms;
    }]
);
