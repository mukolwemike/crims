/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 18/05/2016.
 * Project crims
 */

app.factory(
    'RealEstateProject',
    ['$http', 'TableState', function ($http, TableState) {
        var project = {};

        project.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/projects');
        };

        project.getCustodialAccounts = function (project_id) {
            return $http.get('/api/projects/'+project_id+'/custodial-accounts');
        };

        return project;
    }]
);