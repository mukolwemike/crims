app.factory(
    'SalesAgreements',
    ['TableState', '$http', function (TableState, $http) {

        var sales_agreements = {};

        sales_agreements.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/sales-agreements');
        };

        return sales_agreements;
    }]
);
