app.factory(
    'Advocates',
    ['TableState', '$http', function (TableState, $http) {

        var advocates = {};

        advocates.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/advocates');
        };

        return advocates;
    }]
);
