/**
 * Get the real estate unit sizes
 */

app.factory(
    'RealEstateUnitSizes',
    ['TableState', function (TableState) {
        var unitSizes = {};

        unitSizes.getUnitSizes = function (tableState) {

            return TableState.getPage(tableState, '/api/realestate/unitsizes');
        };

        return unitSizes;
    }]
);