/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 30/07/2016.
 * Project crims
 */

app.factory(
    'RealEstateTranche',
    ['$http', function ($http) {
        var t = {};

        t.getPrices = function (trancheId) {
            return $http.get('/dashboard/realestate/projects/tranche-prices/' + trancheId);
        };

        return t;
    }]
).factory(
    'RealEstateTranchePricing',
    ['$http', function ($http) {
            var t = {};

            t.getPricing = function (trancheId) {
                return $http.get('/dashboard/realestate/projects/tranche-pricing/' + trancheId);
            };

            return t;
    }]
);