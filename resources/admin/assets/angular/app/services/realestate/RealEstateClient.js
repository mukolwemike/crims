/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 18/05/2016.
 * Project crims
 */

app.factory(
    'RealEstateClient',
    ['TableState', function (TableState) {
        var client = {};

        client.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/clients');
        };

        return client;
    }]
);