/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 23/05/2016.
 * Project crims
 */

app.factory(
    'RealEstateUnits',
    ['TableState', function (TableState) {
        var units = {};

        units.getProjectPage = function (tableState, project_id) {

            return TableState.getPage(tableState, '/api/realestate/projects/'+project_id+'/units');

        };

        return units;
    }]
);