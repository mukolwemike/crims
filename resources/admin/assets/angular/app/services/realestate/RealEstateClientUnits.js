
app.factory(
    'RealEstateClientUnits',
    ['TableState', function (TableState) {
        var units = {};

        units.getProjectPage = function (tableState, client_id) {

            return TableState.getPage(tableState, '/api/realestate/clients/'+client_id+'/units');

        };

        return units;
    }]
);