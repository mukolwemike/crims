app.factory(
    'RealEstateCommissionRecipient',
    ['TableState', '$http', function (TableState, $http) {
        var rec = {};

        rec.getRecipientsWithRealEstateCommissions = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/commissions');
        };
        
        rec.getRecipientRealEstateCommissionRate = function (recipientId, projectId, date = '', client_id) {
            return $http.post('/api/realestate/get_recipient_commission_rate', {recipientId : recipientId, projectId : projectId, date : date, client_id : client_id});
        };

        return rec;
    }]
);