/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 23/05/2016.
 * Project crims
 */

app.factory(
    'RealEstateCancelledUnits',
    ['TableState', function (TableState) {
        var units = {};

        units.getProjectPage = function (tableState, project_id) {

            return TableState.getPage(tableState, '/api/realestate/projects/'+project_id+'/cancelled-units');

        };

        return units;
    }]
);