app.factory(
    'RealEstateCommissionPaymentDate',
    ['TableState', '$http', function (TableState, $http) {
        var dates = {};

        dates.getPaymentDates = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/commissions/payment-dates');
        };

        return dates;
    }]
);