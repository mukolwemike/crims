
app.factory(
    'RealEstateScheduledPayment',
    ['TableState', function (TableState) {
        var paymentSchedule = {};

        paymentSchedule.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/payments-schedules');
        };

        paymentSchedule.getOverdue = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/payment-schedules/overdue');
        };

        return paymentSchedule;
    }]
);