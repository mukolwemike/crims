app.factory(
    'LOOs',
    ['TableState', '$http', function (TableState, $http) {

        var forms = {};

        forms.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/loos');
        };

        forms.export = function (tableState) {
            return TableState.getPage(tableState, '/api/realestate/loos/export');
        };

        return forms;
    }]
);
