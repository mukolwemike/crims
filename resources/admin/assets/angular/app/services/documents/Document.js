app.factory(
    'Document',
    ['TableState', '$http', function (TableState, $http) {

        var documents = {};

        documents.getPage = function (tableState, module, typeSlug) {
            return TableState.getPage(tableState, '/api/documents/' + module + '/types/' + typeSlug);
        };

        return documents;
    }]
);
