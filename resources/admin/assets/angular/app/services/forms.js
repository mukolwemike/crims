app.controller(
    'SuggestCompanies',
    ['$scope', '$filter', 'Companies', function ($scope,$filter, Companies) {
        $scope.companies = [];

        $scope.selectedcompany = '';

        Companies.get($filter)
        .success(
            function (data) {
                $scope.com = data;
            }
        )
        .then(
            function (data) {
                angular.forEach(
                    $scope.com,
                    function (c) {
                        $scope.companies.push(c.name);
                    }
                );
                $scope.populate();
            }
        );

        $scope.populate = function () {
            var lst = $scope.com;
            var id = document.getElementById("company_id").value;
            if (id) { //if id is defined
                var company = $filter("filter")(lst, {id:id});
                $scope.selectedcompany = company[0].name;
            }
        };
    }]
);
