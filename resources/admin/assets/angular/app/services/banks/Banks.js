app.factory(
    'Banks',
    ['TableState', '$http', function (TableState, $http) {

        var banks = {};

        banks.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/banks');
        };

        return banks;
    }]
);

app.factory(
    'Branches',
    ['TableState', '$http', function (TableState, $http) {

        var branches = {};

        branches.getPage = function (tableState, bank_id) {
            return TableState.getPage(tableState, '/api/banks/' + bank_id + '/branches');
        };

        branches.getBranches = function (bank_id) {
            return $http.get('/api/banks/' + bank_id+ '/branches-list');
        };

        return branches;
    }]
);
