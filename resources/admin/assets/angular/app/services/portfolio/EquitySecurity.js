
app.factory('EquitySecurity', ['TableState', '$http', function (TableState, $http) {
        var sa = {};

        sa.getSecuritiesPage = function (tableState) {
            return TableState.getPage(tableState, '/api/portfolio/equities/list');
        };

        sa.getAssetAllocationData = function () {
            return $http.get('/api/portfolio/equities/allocation');
        };

        sa.getDividends = function (tableState, security_id, type) {
            return TableState.getPage(tableState, '/api/portfolio/equities/'+security_id+'/dividends/'+type);
        };

        sa.getTotals = function (tableState) {
            return TableState.getPage(tableState, '/api/portfolio/equities/totals')
        };

        return sa;
    }]
);