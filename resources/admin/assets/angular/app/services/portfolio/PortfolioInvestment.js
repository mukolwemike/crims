app.factory(
    'PortfolioInvestment',
    ['TableState', function (TableState) {
        var p = {};

        p.getSummaryPage = function (tableState, currency_id, filters) {
            return TableState.getPage(tableState, '/api/portfolio/summary/'+currency_id, filters);
        };

        p.getPage = function (tableState, currency_id) {
            return TableState.getPage(tableState, '/api/portfolio/investments/'+currency_id);
        };

        p.getSummaryPageTotals = function (tableState, currency_id, filters) {
            return TableState.getPage(tableState, '/api/portfolio/summary_totals/'+currency_id, filters);
        };

        return p;
    }]
);

app.factory(
    'PortfolioAllocationSummary',
    ['TableState', function (TableState) {

        console.log("fetching portfolio asset allocation summary");

        var summary = {};

        summary.getSummaryPage = function (tableState, filters) {
            return TableState.getPage(tableState, '/api/portfolio/asset/allocation', filters);
        };

        summary.getPage = function (tableState, currency_id) {
            return TableState.getPage(tableState, '/api/portfolio/investments');
        };

        summary.getSummaryPageTotals = function (tableState, filters) {
            return TableState.getPage(tableState, '/api/portfolio/asset/allocation', filters);
        };

        return summary;
    }]
);