app.factory(
    'CashDepositAnalysis',
    ['TableState', '$http', function (TableState, $http) {

        var cash = {};

        cash.getDetails = function (tableState, currency_id) {
            return TableState.getPage(tableState, '/api/portfolio/analysis/cash/'+currency_id);
        };

        return cash;
    }]
);
