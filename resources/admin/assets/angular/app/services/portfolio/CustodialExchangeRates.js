/**
 * Custodial Exchange Rates
 */
app.factory(
    'CustodialExchangeRate',
    ['$http', 'TableState', function ($http, TableState) {
        var rate = {};

        rate.getRates = function (tableState, filters) {
            return TableState.getPage(tableState, '/api/portfolio/custodials/exchange_rates', filters);
        };

        return rate;
    }]
);