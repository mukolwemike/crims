app.factory(
    'ActiveStrategy',
    ['TableState', '$http', function (TableState, $http) {
        var as = {};

        as.getSecuritiesPage = function (tableState) {
            return TableState.getPage(tableState, '/api/portfolio/activestrategy');
        };

        as.getHoldingsPage = function (tableState, security_id) {
            return TableState.getPage(tableState, '/api/portfolio/activestrategy/'+security_id);
        };

        as.getShareSalePage = function (tableState, security_id) {
            return TableState.getPage(tableState, '/api/portfolio/activestrategy/'+security_id+'/sales');
        };

        as.getAssetAllocationData = function () {
            return $http.get('/api/portfolio/activestrategy/allocation');
        };

        as.getDividends = function (tableState, security_id, type) {
            return TableState.getPage(tableState, '/api/portfolio/activestrategy/'+security_id+'/dividends/'+type);
        };

        return as;
    }]
);