app.factory(
    'PortfolioInstructions',
    ['TableState', '$http', function (TableState, $http) {

        var instructions = {};

        instructions.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/portfolio/instructions');
        };

        return instructions;
    }]
);