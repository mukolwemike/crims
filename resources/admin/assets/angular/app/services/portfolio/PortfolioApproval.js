/**
 * Created by interntwo on 14/03/2016.
 */

app.factory(
    'PortfolioApproval',
    ['TableState', function (TableState) {
        var approval = {};

        approval.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/portfolio/approvals');
        };

        return approval;

    }]
);