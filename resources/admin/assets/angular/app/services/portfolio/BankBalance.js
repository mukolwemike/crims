app.factory(
    'BankBalance',
    ['TableState', '$http', function (TableState, $http) {

        var balance = {};

        balance.getTrail = function (tableState, custodial_id) {
            return TableState.getPage(tableState, '/api/portfolio/custodial/account-balance-trail/' + custodial_id);
        };

        return balance;
    }]
);
