/**
 * Created by interntwo on 17/03/2016.
 */

app.factory(
    'CustodialAccount',
    ['$http', 'TableState', function ($http, TableState) {
        var acc = {};

        acc.getRemainingAccounts = function (acc_id) {
            return $http.get('/api/portfolio/custodial/accounts/remaining/'+acc_id);
        };

        acc.getTransactions = function (tableState, id) {
            return TableState.getPage(tableState, '/api/portfolio/custodial/accounts/transactions/'+id);
        };

        acc.checkCompatibility = function (account_id, productColl) {
            return $http(
                {
                    url: '/api/accounts/'+account_id+'/compatibility',
                    method: 'GET',
                    params: productColl,
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        acc.fetchSummary = function (account_id, start, end) {
            return $http.post('/api/accounts/'+account_id+'/summary', {start: start, end:end});
        };

        return acc;
    }]
);