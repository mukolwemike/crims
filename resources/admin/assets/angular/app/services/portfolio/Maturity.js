/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 16/07/2016.
 * Project crims
 */

app.factory(
    'Maturity',
    ['$http', function ($http) {
        return {
            getWeeklyProfileData: function (currency_id, data) {
                return $http.post('/api/portfolio/maturityprofiledata/weekly/'+currency_id, data);
            },
        
            getMonthlyProfileData: function (currency_id, data) {
                return $http.post('/api/portfolio/maturityprofiledata/monthly/'+currency_id, data);
            }
        };
    }]
);