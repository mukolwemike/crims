app.factory(
    'SubAssetClasses',
    ['TableState', '$http', function (TableState, $http) {
        var sa = {};

        sa.subAssetClassList = function (asset_id) {
            return $http.get('/api/portfolio/'+asset_id+'/sub-asset-classes');
        };

        return sa;
    }]
);