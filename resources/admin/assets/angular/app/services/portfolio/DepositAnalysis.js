app.factory(
    'DepositAnalysis',
    ['TableState', '$http', function (TableState, $http) {

        var analysis = {};

        analysis.getPage = function (tableState, currency_id) {
            return TableState.getPage(tableState, '/api/portfolio/analysis/currency/'+currency_id);
        };

        return analysis;
    }]
);
