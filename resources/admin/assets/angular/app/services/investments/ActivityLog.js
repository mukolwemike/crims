/**
 * Created by interntwo on 05/03/2016.
 */

app.factory(
    'ActivityLog',
    ['$http', 'TableState', function ($http, TableState) {

        var log = {};

        log.get = function () {
            return $http.get('/api/activity');
        };

        log.getInvestments = function (start, end) {
            return $http.post('/api/investments/activity', {start: start, end: end});
        };

        log.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/activity_log');
        };

        log.getClientTransactionsPage = function (tableState, start, end) {
            return TableState.getPage(tableState, '/api/activity_log/client_transactions', {start: start, end: end });
        };

        log.getPortfolioTransactionsPage = function (tableState, start, end) {
            return TableState.getPage(tableState, '/api/activity_log/portfolio_transactions', {start: start, end: end });
        };

        return log;
    }]
);
