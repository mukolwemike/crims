/**
 * Created by interntwo on 23/03/2016.
 */

app.factory(
    'CommissionRecipient',
    ['TableState', '$http', function (TableState, $http) {
        var rec = {};

        rec.getRecipientsWithTotals = function (tableState, currency_id) {
            return TableState.getPage(tableState, '/api/investments/commission/' + currency_id);
        };

        rec.getRecipients = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/commission_recipients/getRecipients');
        };

        rec.getCommissionPerMonth = function (tableState, product_id) {
            return TableState.getPage(tableState, '/api/investments/commission/per_month/' + product_id);
        };

        rec.search = function (name) {
            var tableState = {
                search: {
                    predicateObject: {
                        $: name
                    }
                }
            };

            return TableState.getPage(tableState, '/api/investments/commission/recipients');
        };

        rec.getIfHasZeroCommissionRate = function (recipient_id) {
            return $http(
                {
                    url: '/api/recipients/' + recipient_id + '/zero-commission-rate',
                    method: 'GET',
                }
            );
        };

        return rec;
    }]
);


+app.factory('CommissionRecipientPositions', ['TableState', '$http', function (TableState, http) {

    let rec = {};

    rec.getRecipientPositions = function (tableState, recipient_id) {
        return TableState.getPage(tableState, '/api/investments/commission_recipients/getRecipientPositions/' + recipient_id);
    };

    return rec;

}]);