
app.factory(
    'Investments',
    ['$http', 'TableState', function ($http, TableState) {

        var investments = {};

        investments.getPage= function (tableState) {

            return $http(
                {
                    url: '/api/investments',
                    method: 'GET',
                    params: {tableState: tableState},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        investments.getTaxDue = function (tableState, product_id) {
            return TableState.getPage(tableState, '/api/investments/taxation/due/'+product_id);
        };

        investments.getInterestPayments = function (tableState) {

            return TableState.getPage(tableState, '/api/investments/interest');
        };

        investments.getInvestmentById = function (investment_id) {
            return $http.get('/api/investments/'+investment_id);
        };

        //investments.downloadInterestPaymentsXlsx = function (tableState) {
        //    return TableState.get(tableState, '/api/investments/interest/excel');
        //};

        investments.getInterestRatesForProduct = function (investment_id) {
            return $http.get('/api/investments/interest-rates/' + investment_id);
        };

        investments.getMaturityDate = function (tenor, invested_date) {

            var duration = (parseInt(tenor/12) * 365 ) + (30 *(tenor %12));

            return moment(invested_date).add(duration, 'days');
        };

        investments.getNearestMonday = function (maturity_date) {
            var monday = 1;

            if (maturity_date.isoWeekday() <= monday) {
                return maturity_date.isoWeekday(monday).format('YYYY-MM-DD');
            } else {
                return maturity_date.add(1, 'weeks').isoWeekday(monday).format('YYYY-MM-DD');
            }
        };

        investments.getDateDifference = function (invested_date, maturity_date) {
            return (moment(maturity_date).diff(moment(invested_date), 'days'));
        };

        return investments;

    }]
);

app.factory(
    'Investment',
    ['$http', function ($http) {
        var investment = {};
        investment.getNetInterestAtDate = function (investment_id, date) {
            return $http(
                {
                    url: '/api/investments/' + investment_id + '/net-interest-at-date',
                    method: 'GET',
                    params: { date : date}
                }
            );
        };
        investment.getTotalValueOfInvestmentAtDate = function (investment_id, date) {
            return $http(
                {
                    url: '/api/investments/' + investment_id + '/total-value-at-date',
                    method: 'GET',
                    params: { date : date}
                }
            );
        };

        investment.penalty = function (investment_id, date, percentage, amount) {
            return $http(
                {
                    url: '/api/investments/' + investment_id + '/penalty',
                    method: 'GET',
                    params: { date : date, percentage: percentage, withdrawn_amount: amount}
                }
            );
        };

        return investment;
    }]
);


app.factory(
    'Download',
    ["$http", 'TableState', function ($http, TableState) {

        var d = {};

        d.get = function (url, httpParams) {
            return $http.get(
                url,
                {
                    responseType: 'blob',
                    params: httpParams
                }
            );
        };

        d.downloadInterestPaymentsXlsx = function (tableState) {
            var downloadLink = document.createElement("a");
            document.body.appendChild(downloadLink);
            downloadLink.style = "display: none";

            //This service is written Below how does it work, by accepting necessary params
            return d.get('/api/investments/interest/excel', tableState).then(
                function (result, status, headers, config) {
                    var fName = "Interest Payments Export";
                    console.log(fName);
                    var file = new Blob([result.data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                    var fileURL = (window.URL || window.webkitURL).createObjectURL(file);

                    //Blob, client side object created to with holding browser specific download popup,
                    // on the URL created with the help of window obj.
                    downloadLink.href = fileURL;
                    downloadLink.download = fName;
                    downloadLink.click();
                }
            );
        };

        return d;
    }]
);