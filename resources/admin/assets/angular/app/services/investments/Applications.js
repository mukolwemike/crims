
app.factory(
    'Applications',
    ['TableState', function (TableState) {

        var applications = {};

        applications.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/applications');
        };

        return applications;
    }]
);