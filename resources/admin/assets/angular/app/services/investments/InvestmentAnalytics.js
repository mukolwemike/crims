/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 13/08/2016.
 * Project crims
 */

app.factory(
    'InvestmentAnalytics',
    ['$http', function ($http) {
        var a = {
            base_url : '/api/investments/analytics/',
            completeUrl : function (sec, type, id) {
                return a.base_url+sec+'/'+type+'/'+id;
            }
        };

        a.costValue = function (type, id, date) {
            return $http(
                {
                    url: a.completeUrl('cost-value', type, id),
                    method: 'GET',
                    params: {date: date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        a.marketValue = function (type, id, date) {
            return $http(
                {
                    url: a.completeUrl('market-value', type, id),
                    method: 'GET',
                    params: {date: date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        a.withholdingTax = function (type, id, date) {
            return $http(
                {
                    url: a.completeUrl('withholding-tax', type, id),
                    method: 'GET',
                    params: {date: date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        a.custodyFees = function (type, id, date) {
            return $http(
                {
                    url: a.completeUrl('withholding-tax', type, id),
                    method: 'GET',
                    params: {date: date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );

        };

        a.managementFees = function (type, id, date) {
            return $http(
                {
                    url: a.completeUrl('residual-income', type, id),
                    method: 'GET',
                    params: {date: date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        a.weightedRate = function (type, id, date) {
            return $http(
                {
                    url: a.completeUrl('weighted-rate', type, id),
                    method: 'GET',
                    params: {date: date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        a.weightedTenor = function (type, id, date) {
            return $http(
                {
                    url: a.completeUrl('weighted-tenor', type, id),
                    method: 'GET',
                    params: {date: date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        a.persistence = function (type, id, from_date, date) {
            return $http(
                {
                    url: a.completeUrl('persistence', type, id),
                    method: 'GET',
                    params: {date: date, from_date: from_date},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        return a;
    }]
);
