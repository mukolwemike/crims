/**
 * Created by interntwo on 10/03/2016.
 */

app.factory(
    'StatementCampaign',
    ['TableState', function (TableState) {
        var campaign = {};

        campaign.getItemsPage = function (tableState, campaign_id) {

            return TableState.getPage(tableState, '/api/investments/statements/campaign/'+campaign_id);

        };

        campaign.getMissingClients = function (tableState, campaign_id) {
            return TableState.getPage(tableState, '/api/investments/statements/campaign/'+campaign_id+'/missing');
        };

        return campaign;
    }]
);