/**
 * Created by interntwo on 05/02/2016.
 */

app.factory(
    'Clients',
    ['TableState', '$http', function (TableState, $http) {

        let clients = {};

        clients.get = function () {
            return $http.get('/api/clients');
        };

        clients.search = function (term) {
            return $http.get('/api/clients/search?name='+term);
        };

        clients.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/clients/all');
        };

        clients.getSummaryPage = function (tableState, id) {
            return TableState.getPage(tableState, '/api/clients/summary/'+id);
        };

        clients.getOngoingTax = function (tableState, product_id) {
            return TableState.getPage(tableState, '/api/investments/taxation/'+product_id);
        };

        clients.getClientsByName = function (name) {
            return $http.get('/api/clients/search?name='+name);
        };

        clients.getProductPaymentsBalance = function (client_id, product_id) {
            return $http.get('/api/clients/payments-balance/'+client_id+'/products/'+product_id);
        };

        return clients;
    }]
);

/**
 * Created by molukaka on 06/11/2018
 */

app.factory(
    'RiskyClients',
    ['TableState', '$http', function (TableState, $http) {

        let riskyClients = {};

        riskyClients.get = function () {
            return $http.get('/api/clients/risky');
        };

        riskyClients.search = function (term) {
            return $http.get('/api/clients/risky/search?name='+term);
        };

        riskyClients.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/clients/risky/all');
        };

        riskyClients.getClientsByName = function (name) {
            return $http.get('/api/clients/risky/search?name='+name);
        };

        return riskyClients;
    }]
);
