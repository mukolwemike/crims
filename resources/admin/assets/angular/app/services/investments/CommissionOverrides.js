/**
 * Created by interntwo on 23/03/2016.
 */

app.factory(
    'CommissionOverrides',
    [ '$http', function ($http) {
        var rec = {};

        rec.getOverrides = function (recipient_id, currency_id, start, end) {
            return $http.get('/api/investments/commission/overrides/recipient/'+recipient_id+'/currency/'+currency_id+'/start/'+start +'/end/'+end);
        };

        rec.getAdditionalCommission = function (recipient_id, currency_id, start, end) {
            return $http.get('/api/investments/commission/additional_commissions/recipient/'+recipient_id+'/currency/'+currency_id+'/start/'+start +'/end/'+end);
        };

        return rec;
    }]
);