/**
 * Created by interntwo on 05/02/2016.
 */

app.factory(
    'TableState',
    ['$http', function ($http) {

        var state = {};

        state.getPage = function (tableState, url, args) {
            return $http(
                {
                    url: url,
                    method: 'GET',
                    params: {tableState: tableState, args: args},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        state.renderTable = function (scope, tableState, resource) {
            scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            resource.then(
                function (result) {
                    scope.displayed = result.data.data;
                    scope.meta = result.data.meta;


                    tableState.pagination.numberOfPages = result.data.meta.pagination.total_pages;//set the number of pages so the pagination can update
                    //tableState.pagination.number = result.data.meta.cursor.count;

                    scope.perPage = tableState.pagination.number;

                    scope.isLoading = false;
                }
            );
        };

        return state;
    }]
);
