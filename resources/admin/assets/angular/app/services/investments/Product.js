/**
 * Created by interntwo on 05/03/2016.
 */
app.factory(
    'Product',
    ['$http', 'TableState', function ($http, TableState) {

        var product = {};

        product.getTotalInvestments = function (product_id) {
            return $http.get('/api/products/'+product_id+'/total_investments');
        };

        product.getTotalInvestmentsValue = function (product_id) {
            return $http.get('/api/products/'+product_id+'/total_investments_value');
        };

        product.getTotalWithholdingTax = function (product_id) {
            return $http.get('/api/products/'+product_id+'/total_withholding_tax');
        };

        product.getTotalManagementFees = function (product_id) {
            return $http.get('/api/products/'+product_id+'/total_management_fees');
        };

        product.getTotalCustodyFees = function (product_id) {
            return $http.get('/api/products/'+product_id+'/total_custody_fees');
        };

        product.getClientWeightedRate = function (product_id) {
            return $http.get('/api/products/'+product_id+'/client_weighted_rate');
        };

        product.getCustodialAccounts = function (product_id) {
            return $http.get('/api/products/'+product_id+'/custodial-accounts');
        };

        product.getProducts = function (tableState) {
            return TableState.getPage(tableState, '/api/products');
        };

        return product;

    }]
);