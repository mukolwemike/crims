app.factory(
    'Withdrawals',
    ['$http', 'TableState', function ($http, TableState) {
        var withdrawals = {};
        withdrawals.getPage= function (tableState) {
            return $http(
                {
                    url: '/api/withdrawals',
                    method: 'GET',
                    params: {tableState: tableState},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };
        return withdrawals;
    }]
);