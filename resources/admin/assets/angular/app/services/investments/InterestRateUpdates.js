app.factory(
    'InterestRateUpdates',
    ['TableState', '$http', function (TableState, $http) {

        var updates = {};

        updates.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/updates/interest-rates');
        };

        updates.getRates = function (update_id) {
            return $http.get('/api/investments/updates/interest-rates/' + update_id);
        };

        return updates;
    }]
);
