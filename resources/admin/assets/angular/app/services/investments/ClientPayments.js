app.factory(
    'ClientPayments',
    ['TableState', '$http', function (TableState, $http) {

        var payments = {};

        payments.getPage = function (tableState, type_slug, includes) {

            if (includes) {
                tableState.includes = includes;
            }
        

            var query = '';

            if (!(type_slug === undefined || type_slug === null)) {
                query = '?type='+type_slug;
            }


            return TableState.getPage(tableState, '/api/investments/client-payments'+query);
            // return TableState.getPage(tableState, '/api/investments/client-payments?type='+type_slug);
        };

        payments.getClientPayments = function (tableState, client_id) {
            return TableState.getPage(tableState, '/api/investments/client-payments/client/'+client_id);
        };

        return payments;
    }]
);
