app.factory(
    'AccountsCash',
    ['TableState', '$http', function (TableState, $http) {

        var cash = {};

        cash.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/accounts-cash');
        };

        return cash;
    }]
);
