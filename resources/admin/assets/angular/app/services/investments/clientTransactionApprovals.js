

app.factory(
    'ClientTransactionApproval',
    ['$http', function ($http) {

        var transactions = {};

        transactions.getPage = function (tableState) {

            return $http(
                {
                    url: '/api/investments/approvals',
                    method: 'GET',
                    params: {tableState: tableState},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        return transactions;
    }]
);