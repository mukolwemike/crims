app.factory(
    'ClientWithdrawals',
    ['$http', 'TableState', function ($http, TableState) {
        var clientWithdrawals = {};

        clientWithdrawals.getPage= function (tableState) {
            return TableState.getPage(tableState, '/api/client/withdrawals');
        };
        return clientWithdrawals;
    }]
);