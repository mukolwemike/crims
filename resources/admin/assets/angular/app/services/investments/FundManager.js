/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 19/08/2016.
 * Project crims
 */

app.factory(
    'FundManager',
    ['$http', function ($http) {

        var f = {};

        f.switchTo = function (id) {
            $http.get('/api/investments/fund_manager/switch/'+id)
            .then(
                function () {
                    window.location.reload();
                }
            );
        };

        return f;

    }]
);