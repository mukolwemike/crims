/**
 * Created by interntwo on 09/02/2016.
 */

app.factory(
    'BusinessConfirmationApplications',
    ['TableState', function (TableState) {

        var applications = {};

        applications.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/businessconfirmations');
        };

        return applications;
    }]
);