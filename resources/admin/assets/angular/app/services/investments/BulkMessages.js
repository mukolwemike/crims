app.factory(
    'BulkMessages',
    ['TableState', '$http', function (TableState, $http) {

        var messages = {};

        messages.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/bulk/messages');
        };

        return messages;
    }]
);
