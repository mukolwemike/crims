app.factory(
    'CommissionPaymentDate',
    ['TableState', '$http', function (TableState, $http) {
        var dates = {};

        dates.getPaymentDates = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/commission/payment-dates');
        };
        return dates;
    }]
);