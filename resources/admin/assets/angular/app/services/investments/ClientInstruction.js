
app.factory(
    'ClientRolloverInstruction',
    ['$http', function ($http) {

        var instructions = {};

        instructions.getPage = function (tableState) {
            return $http(
                {
                    url: '/api/investments/client-instructions/rollover',
                    method: 'GET',
                    params: {tableState: tableState},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        return instructions;
    }]
);


app.factory(
    'ClientTopupInstruction',
    ['$http', function ($http) {

        var instructions = {};

        instructions.getPage = function (tableState) {
            return $http(
                {
                    url: '/api/investments/client-instructions/topup',
                    method: 'GET',
                    params: {tableState: tableState},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };

        return instructions;
    }]
);

app.factory(
    'ClientFilledInvestmentApplication',
    ['TableState', function (TableState) {

        var app = {};

        app.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/client-instructions/application');
        };

        return app;
    }]
);

app.factory(
    'UnitFundInvestmentInstruction',
    ['TableState', function (TableState) {

        var app = {};

        app.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/client-instructions/unit_fund_investment_instruction');
        };

        return app;
    }]
);

app.factory(
    'UtilityBillingInstructions',
    ['TableState', function (TableState) {

        var app = {};

        app.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/client-instructions/utility_billing_instructions');
        };

        return app;
    }]
);