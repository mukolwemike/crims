app.factory(
    'PaymentInstructions',
    ['TableState', '$http', function (TableState, $http) {

        var instructions = {};

        instructions.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/investments/payment-instructions');
        };

        return instructions;
    }]
);
