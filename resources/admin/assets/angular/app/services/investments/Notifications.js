/**
 * Created by interntwo on 09/02/2016.
 */

app.factory(
    'Notifications',
    ['TableState', '$http', function (TableState, $http) {

        var notifications = {};

        notifications.getPage = function (tableState) {
            return TableState.getPage(tableState, '/api/notifications');
        };

        notifications.getAllUnread = function () {
            return $http.get('/api/notifications/unread');
        };

        notifications.getUnreadCount = function () {
            return $http.get('/api/notifications/unread/count');
        };

        return notifications;
    }]
);