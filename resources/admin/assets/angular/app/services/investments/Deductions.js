
app.factory(
    'Deductions',
    ['$http', 'TableState', function ($http, TableState) {
        var deductions = {};
        deductions.getPage= function (tableState) {
            return $http(
                {
                    url: '/api/deductions',
                    method: 'GET',
                    params: {tableState: tableState},
                    paramSerializer: '$httpParamSerializerJQLike'
                }
            );
        };
        return deductions;
    }]
);