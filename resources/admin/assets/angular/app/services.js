app.factory(
    'Companies',
    ['$http', function ($http) {
        return {
            get : function () {
                return $http.get('/api/companies');
            }
        };

    }]
);

// app.factory('Clients', ['$http', function($http) {
//     return {
//         get : function() {
//             return $http.get('/api/clients');
//         }
//     };
// }]);


app.factory(
    'Applications',
    ['$http', function ($http) {
        return {
            get : function () {
                return $http.get('/api/investments/applications');
            }
        };
    }]
);

app.factory(
    'Contacts',
    ['$http', function ($http) {
        return {
            get : function () {
                return $http.get('/api/contacts');
            }
        };
    }]
);

app.factory(
    'Investments_old',
    ['$http', function ($http) {
        return {
            getActive: function () {
                return $http.get('/api/investments/list/active');
            },
            get: function () {
                return $http.get('/api/investments/list');
            }
        };
    }]
);
app.factory(
    'Users',
    ['$http', function ($http) {
        return {
            get: function () {
                return $http.get('/api/users');
            }
        };
    }]
);

app.factory(
    'Permissions',
    ['$http', function ($http) {
        return {
            get: function () {
                return $http.get('/api/permissions');
            }
        };
    }]
);
app.factory(
    'Roles',
    ['$http', function ($http) {
        return {
            get: function () {
                return $http.get('/api/roles');
            }
        };
    }]
);
app.factory(
    'Institutions',
    ['$http', function ($http) {
        return {
            get: function () {
                return $http.get('/api/portfolio/institutions');
            }
        };
    }]
);
app.factory(
    'PortfolioInvestments',
    ['$http', function ($http) {
        return {
            get: function (newValue) {
                return $http.get('/api/portfolio/investments/'+newValue);
            }
        };
    }]
);

app.factory(
    'Commission',
    ['$http', function ($http) {
        return {
            get: function (id) {
                return $http.get('/api/investments/commission/'+id);
            }
        };
    }]
);

app.factory(
    'Statement',
    [function () {
        var info = {
            sent: ''
        };

        var batch = {};

        return {
            getInfo: function () {
                return info.sent;
            },
            setInfo: function (value) {
                info.sent = value;
            },
            getBatch: function () {
                return batch;
            },
            setBatch: function (value) {
                batch = value;
            }
        };
    }]
);

app.factory(
    'Products',
    ['$http', function ($http) {
        return{
            get: function () {
                return $http.get('/api/products/all');
            }
        };
    }]
);

app.factory(
    'Taxation',
    ['$http', function ($http) {
        return {
            get : function (id, data) {
                return $http.post('/api/investments/taxation/'+id, data);
            }
        };
    }]
);

app.factory(
    'CommissionSchedule',
    ['$http', function ($http) {
        return {
            get: function (id) {
                return $http.get('/api/investments/commission/fullschedule/'+id);
            },
            scheduleForFa: function (id, period, product) {
                return $http.get('/api/investments/commission/schedule/'+id+'/'+period+'/'+product);
            },
            getTimedFa: function (productId, data) {
                return $http.post('/api/investments/commission/timedschedule/'+productId, data);
            }
        };
    }]
);

app.factory(
    'InterestSchedule',
    ['$http', function ($http) {
        return {
            get: function () {
                return $http.get('/api/investments/interestSchedule');
            },
            scheduleForClient: function (id, period) {
                return $http.get('/api/investments/interest/schedule/'+id+'/'+period);
            }
        };
    }]
);

app.factory(
    'ClientsTaxationRecords',
    ['$http', function ($http) {
        return{
            get: function (product_id) {
                return $http.get('/api/investments/taxation/records/'+product_id);
            }
        };
    }]
);

app.factory(
    'CusodialAccount',
    ['$http', function ($http) {
        return {
            get: function (id) {
                return $http.get('/api/portfolio/custodial/balance/'+id);
            }
        };
    }]
);

app.factory(
    'Persistence',
    ['$http', function ($http) {
        return{
            get: function (product_id, data) {
                return $http.post('/api/investments/summary/persistence/'+product_id, data);
            }
        };
    }]
);
app.factory(
    'ClientWeightedTenor',
    ['$http', function ($http) {
        return{
            get: function (product_id, data) {
                return $http.post('/api/investments/summary/client_weighted_tenor/'+product_id, data);
            }
        };
    }]
);
app.factory(
    'DepositWeightedTenor',
    ['$http', function ($http) {
        return{
            get: function (product_id, data) {
                return $http.post('/api/investments/summary/deposit_weighted_tenor/'+product_id, data);
            }
        };
    }]
);

app.factory(
    'CMSHistory',
    ['$http', function ($http) {
        return {
            get: function (product_id) {
                return $http.get('/api/investments/summary/history/' + product_id);
            }
        };
    }]
);

app.factory(
    'TaxationInvestmentDetails',
    ['$http', function ($http) {
        return {
            get: function (id) {
                return $http.get('/api/investments/taxation/investments/details/'+id);
            }
        };
    }]
);

app.factory(
    'Notification',
    ['$http', function ($http) {
        return{
            get: function () {
                return $http.get('/api/notifications');
            },
            count: function () {
                return $http.get('/api/notifications/unread_count');
            }
        };
    }]
);

app.factory(
    'CommissionRate',
    ['$http', function ($http) {
        return {
            getRatesForProduct: function (product_id) {
                return $http.get('/api/investments/commission/rates/'+product_id);
            },

            getRatesForProductAndRecipient: function (product_id, recipient_id, date = '') {
                return $http.get('/api/investments/commission/rates/' + product_id + '/recipient/' + recipient_id + '/' + date);
            },

            getRecipientProductCommissionRate: function (product_id, recipient_id, date, client_id, investment_type_id, isParent) {
                return $http.post('/api/investments/commission/recipient_product_rate', {
                    recipientId : recipient_id,
                    product_id : product_id,
                    invested_date : date,
                    client_id : client_id,
                    investment_type_id : investment_type_id,
                    is_parent : isParent
                });
            },
        };
    }]
);

app.factory(
    "TrancheUnitPricing",
    ['$http', '$filter', function ($http, $filter) {
        var service = {};

        service.getPaymentPlans = function () {
            return $http.get('/api/realestate/payment-plans');
        };

        service.getAllTrancheUnitPricings = function (holding_id) {
            return $http.get('/api/realestate/projects/' + holding_id + '/tranche-pricings');
        };

        service.getTrancheUnitPricingsForSelectedPaymentPlan = function (tranche_unit_pricing_list, payment_plan_id) {
            if (payment_plan_id === undefined) {
                return [];
            }

            return ($filter('filter')(tranche_unit_pricing_list, {payment_plan_id: payment_plan_id}));
        };

        return service;
    }]
);

app.factory(
    'PaymentSchedule',
    ['$http', function ($http) {
        var service = {};

        service.getPaymentSchedules = function (unit_id, spacing) {
            return $http.get('/api/realestate/payment-schedules/' + unit_id + '/spacing/' + spacing);
        };

        return service;
    }]
);