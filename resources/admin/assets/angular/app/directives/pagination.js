/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 16/07/2016.
 * Project crims
 */

app.directive(
    'dmcPagination',
    [function () {
        var dir = {
            'restrict': 'AE'
        };

        dir.template = '<div class="center">' +
        '<div class="inline-block" style="vertical-align: top; padding-top: 25px; margin-right: 40px;">' +
        'Items per page <input style="width: 40px" type = "text" ng-model = "itemsByPage"/>' +
        '</div> ' +
        '<div class="inline-block"><div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div></div> ' +
        '</div>';

        return dir;
    }]
);