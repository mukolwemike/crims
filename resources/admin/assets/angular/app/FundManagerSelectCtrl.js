/**
 * Created by Mwaruwa Chaka <mwaruwac@gmail.com> on 19/08/2016.
 * Project crims
 */


app.controller(
    'FundManagerSelectCtrl',
    ['$scope', 'FundManager', function ($scope, FundManager) {
        $scope.set = false;

        $scope.$watch(
            'selected_fm_id',
            function () {
                if ($scope.set) {
                    FundManager.switchTo($scope.selected_fm_id);
                }

                $scope.set = true;
            }
        );
    }]
);