import * as portfolio from '../../../../api/portfolioApi';
import { Message } from 'element-ui';

const state = {
    taxSelect: false,
    withholdingTaxes: [],
    multipleSelection: [],
    fetchingTaxes: false,
    submittingWHT: false,
    whtFormErrors: [],
    totalTax: 0,
    calculatingTotalTax: false

};

const getters = {
    taxSelect: state => state.taxSelect,
    withholdingTaxes: state => state.withholdingTaxes,
    multipleSelection: state => state.multipleSelection,
    fetchingTaxes: state => state.fetchingTaxes,
    submittingWHT: state => state.submittingWHT,
    whtFormErrors: state => state.whtFormErrors,
    totalTax: state => state.totalTax,
    calculatingTotalTax: state => state.calculatingTotalTax,
};

const mutations = {
    SELECT_TAXES_MODAL(state, value) {
        state.taxSelect = value;
    },

    GET_UNPAID_WITHHOLDING_TAXES(state, form) {

        state.fetchingTaxes = true;

        portfolio.getWithholdingTaxes(form)
            .then( ({ data }) => {
                state.withholdingTaxes = data;
                state.fetchingTaxes = false;
            }, () => {
                state.fetchingTaxes = false;
            });
    },

    SET_MULTIPLE_SELECTION(state, selected) {
        state.multipleSelection = selected;

    },

    SUBMIT_WHT_WITHDRAWAL(state, form) {

        state.submittingWHT = true;

        portfolio.submitWHTWithdrawal(form)
            .then( ({ data }) => {

                state.submittingWHT = false;

                if(data.status === 422) {
                    Message.warning({ message: 'Fill all fields and proceed' });
                    state.whtFormErrors = data.errors;
                } else {
                    Message.success({ message: 'Withdrawal has been submitted for approval' });

                    location.reload();
                }

            }, () => {
                state.submittingWHT = false;
                Message.warning({ message: 'Something went wrong' });
            });
    }
};

export default {
    state, getters, mutations
}