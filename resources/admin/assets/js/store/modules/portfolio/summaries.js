import * as portfolio from '../../../api/portfolioApi';
import { Message } from 'element-ui';
import router from '../../../router';

const state = {
    depositSummary: [],
};

const getters = {
    depositSummary: state => state.depositSummary,
};

const mutations = {
    GET_DEPOSIT_SUMMARY(state) {
        portfolio.getDepositSummary()
            .then(
                ({ data })=>
                {
                    state.depositSummary = data;
                },
                ()=>
                {
                    Message.warning({ message: 'Failed to get the deposit summaries' });
                }
            );
    }
};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions
};
