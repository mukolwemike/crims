import * as security from '../../../api/portfolioApi';
import { Message } from 'element-ui';

const state = {
    sellShareModal: false,
    sellFormErrors: [],
    sellingShares: false,

    buyShareModal: false,
    buyFormErrors: [],
    buyingShares: false,

    accrueDividendModal: false,
    dividendsErrors: [],
    accruingDividends: false,

    marketPriceModal: false,
    marketPriceForm: [],
    addingMarketTrailPrice: false,

    targetPriceModal: false,
    targetPriceForm: [],
    addingTargetTrailPrice: false,

    fetchingFund: false,
    unitFunds: [],
    fundsForFundManager: []
};

const getters = {
    sellShareModal: state => state.sellShareModal,
    sellFormErrors: state => state.sellFormErrors,
    sellingShares: state => state.sellingShares,

    buyShareModal: state => state.buyShareModal,
    buyFormErrors: state => state.buyFormErrors,
    buyingShares: state => state.buyingShares,

    accrueDividendModal: state => state.accrueDividendModal,
    dividendsErrors: state => state.dividendsErrors,
    accruingDividends: state => state.accruingDividends,

    marketPriceModal: state => state.marketPriceModal,
    marketPriceForm: state => state.marketPriceForm,
    addingMarketTrailPrice: state => state.addingMarketTrailPrice,

    targetPriceModal: state => state.targetPriceModal,
    targetPriceForm: state => state.targetPriceForm,
    addingTargetTrailPrice: state => state.addingTargetTrailPrice,

    fetchingFund: state => state.fetchingFund,
    unitFunds: state => state.unitFunds,
    fundsForFundManager: state => state.fundsForFundManager,

};

const mutations = {
    SELL_SHARES(state, form) {

        state.sellingShares = true;

        security.sellEquityShares(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        state.sellFormErrors = data.errors;
                        Message.warning('There are errors in the form.');
                    } else if (data.status == 202) {
                        Message.success('Share sell has been added successfully for approval');
                        state.sellShareModal = false;
                    }
                    state.sellingShares = false;
                },
                ()=>
                {
                    state.sellFormErrors = [];
                    state.sellShareModal = false;
                    state.sellingShares = false;
                }
            );
    },
    BUY_SHARES(state, form) {

        state.buyingShares = true;

        security.buyEquityShares(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        state.buyFormErrors = data.errors;
                        Message.warning('There are errors in the form.');
                    } else if (data.status == 202) {
                        Message.success('Share purchase has been added successfully for approval');
                        state.buyShareModal = false;
                    }
                    state.buyingShares = false;
                },
                ()=>
                {
                    state.buyFormErrors = [];
                    state.buyShareModal = false;
                    state.buyingShares = false;
                }
            );
    },
    SAVE_SHARE_DIVIDENDS(state, form) {

        state.accruingDividends = true;

        security.accrueDividends(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        state.dividendsErrors = data.errors;
                        Message.warning( 'There are errors in the form.');
                    } else if (data.status == 202) {
                        Message.success('Equity share dividend has been added successfully for approval');
                        state.accrueDividendModal = false;
                    }
                    state.accruingDividends = false;
                },
                ()=>
                {
                    state.dividendsErrors = [];
                    state.accrueDividendModal = false;
                    state.accruingDividends = false;
                }
            );
    },
    SAVE_MARKET_PRICE_TRAIL(state, form){
        state.addingMarketTrailPrice = true;

        security.saveMarketPriceTrail(form)
            .then(
                ({ data }) =>
                {
                    if (data.status == 422) {
                        state.marketPriceForm = data.errors;
                        Message.warning('There are errors in the form.');
                    } else if (data.status == 202) {
                        Message.success('Market price trail has been added successfully for approval');
                        state.marketPriceModal = false;

                        location.reload();
                    }
                    state.addingMarketTrailPrice = false;
                },
                ()=>
                {
                    state.marketPriceForm = [];
                    state.marketPriceModal = false;
                    state.addingMarketTrailPrice = false;
                }
            );
    },
    SAVE_TARGET_PRICE_TRAIL(state, form){
        state.addingTargetTrailPrice = true;

        security.saveTargetPriceTrail(form)
        .then(
            ({ data }) =>
            {
                if (data.status == 422) {
                    state.targetPriceForm = data.errors;
                    Message.warning('There are errors in the form.');
                } else if (data.status == 202) {
                    Message.success('Target price trail has been added successfully for approval');
                    state.targetPriceModal = false;
                }
                state.addingTargetTrailPrice = false;
            },
            ()=>
            {
                state.targetPriceForm = [];
                state.targetPriceModal = false;
                state.addingTargetTrailPrice = false;
            }
        );
    },
    SELL_SHARE_MODAL(state, value) {
        state.sellShareModal = value;
    },
    BUY_SHARE_MODAL(state, value) {
        state.buyShareModal = value;
    },
    ACCRUE_DIVIDEND_MODAL(state, value) {
        state.accrueDividendModal = value;
    },
    MARKET_PRICE_TRAIL_MODAL(state, value) {
        state.marketPriceModal = value;
    },
    TARGET_PRICE_TRAIL_MODAL(state, value) {
        state.targetPriceModal = value;
    },

    GET_UNIT_FUNDS(state, id) {

        state.fetchingFund = true;

        security.getUnitFundForManager(id)
            .then(({ data }) => {
                state.fetchingFund = false;
                state.unitFunds = data;
                state.fundsForFundManager = data;

            }, ()=> {
                state.fetchingFund = false;
            });
    }
};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions
}