import * as portfolio from '../../../api/portfolioApi';
import {Message} from 'element-ui';
import router from '../../../router';

const state = {
    createSecurity: false,
    portfolioSecurityDetails: {},
    savingSecurity: false,
    securityFormErrors: [],
    portfolioSecurity: '',
    rawPortfolioSecurity: [],
    editingSecurity: false
};

const getters = {
    createSecurity: state => state.createSecurity,
    portfolioSecurityDetails: state => state.portfolioSecurityDetails,
    savingSecurity: state => state.savingSecurity,
    securityFormErrors: state => state.securityFormErrors,
    portfolioSecurity: state => state.portfolioSecurity,
    rawPortfolioSecurity: state => state.rawPortfolioSecurity,
    editingSecurity: state => state.editingSecurity,
};

const mutations = {

    CREATE_SECURITY_MODAL(state, value) {
        state.createSecurity = value;
    },

    SAVE_SECURITY(state, form) {

        state.savingSecurity = true;
        portfolio.saveSecurity(form)
            .then(({data}) => {
                    if (data.status == 422) {
                        state.savingSecurity = false;
                        state.securityFormErrors = data.errors;

                        Message.warning('There are errors in the form');
                    } else if (data.status == 202) {
                        Message.success('Security successfully saved');
                        state.createSecurity = false;
                        location.reload();
                    }

                    state.savingSecurity = false;
                }, () => {
                    Message.warning('Error Occurred while saving the security');
                    state.savingSecurity = false;

                    state.createSecurity = false;
                }
            );
    },

    GET_PORTFOLIO_SECURITY_DETAILS(state, securityId) {

        portfolio.getPortfolioSecurityDetails(securityId)
            .then(
                ({data}) => {
                    state.portfolioSecurityDetails = data.data;
                }, () => {
                    Message.warning('Failed to get the details for the security');
                    router.push({name: 'portfolio-securities.index'});
                }
            );
    },

    GET_SECURITY_RAW_DETAILS(state, id) {

        state.gettingRawDetails = true;
        portfolio.getRawPortfolioSecurityDetails(id)
            .then(({data}) => {
                state.rawPortfolioSecurity = data;
            });
    },

    SET_PORTFOLIO_SECURITY_TYPE(state, security) {
        state.portfolioSecurity = security;
    },

    EDIT_PORTFOLIO_SECURITY(state, form) {

        state.editingSecurity = true;
        portfolio.editPortfolioSecurity(form)
            .then(() => {
                Message.success('Portfolio edit request has been saved for approval');
                router.push({name: 'portfolio-securities.index'});
                state.editingSecurity = false;
            }, () => {
                state.editingSecurity = false;
                Message.warning('Error occured while sending the request');
            });
    }
};

const actions = {};

export default {
    state,
    getters,
    mutations,
    actions
};
