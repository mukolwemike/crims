import * as summaryApi from '../../../../api/portfolioFundSummaryApi';

const state = {
    calculationType: '',

    fetchingFundAssets: false,
    fetchingFundLiabilities: false,

    fundAssets: [],
    fundLiabilities: [],
    summaryTotals: {},
    marketValue: '',
    fundCurrency: ''
};

const getters = {

    calculationType: state => state.calculationType,

    fundLiabilities: state => state.fundLiabilities,
    fundAssets: state => state.fundAssets,

    fetchingFundAssets: state => state.fetchingFundAssets,
    fetchingFundLiabilities: state => state.fetchingFundLiabilities,
    summaryTotals: state => state.summaryTotals,
    marketValue: state => state.marketValue,
    fundCurrency: state => state.fundCurrency

};

const mutations = {

    FUND_SUMMARY(state, data) {

        state.fetchingFundAssets = true;

        summaryApi.getFundAssets(data)
            .then( ({ data }) => {

                state.fundAssets = data.assets;

                state.fundLiabilities = data.liabilities;

                state.fetchingFundAssets = false;
                
                state.calculationType = data.calculation;

                state.summaryTotals = data.totals;

                state.marketValue = data.total;
                state.fundCurrency = data.currency;

            }, () => {

                state.fetchingFundAssets = false;

            });
    }
};

export default {
    state, getters, mutations
}