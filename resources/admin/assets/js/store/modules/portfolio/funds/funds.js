import * as summaryApi from '../../../../api/portfolioFundSummaryApi';

const state = {
    fundManagerFunds: [],
    fetchingManagerFunds: false,
    activeUnitFund: '',

    searchingForAssetClasses: false,
    searchingForSubAssetClasses: false,
    searchingForSecurities: false,

    assetClassSummaries: [],
    subAssetClassSummaries: [],
    securitySummaries: [],

    selected_date: Date.now()
};

const getters = {

    fundManagerFunds: state => state.fundManagerFunds,

    fetchingManagerFunds: state => state.fetchingManagerFunds,

    activeUnitFund: state => state.activeUnitFund,

    searchingForAssetClasses: state => state.searchingForAssetClasses,
    searchingForSubAssetClasses: state => state.searchingForSubAssetClasses,
    searchingForSecurities: state => state.searchingForSecurities,

    assetClassSummaries: state => state.assetClassSummaries,
    subAssetClassSummaries: state => state.subAssetClassSummaries,
    securitySummaries: state => state.securitySummaries,
    selected_date: state => state.selected_date,
};


const mutations = {

    SUMMARY_FOR_ASSET_CLASSES(state, form) {

        state.searchingForAssetClasses = true;

        summaryApi.getSummaryForAssetClass(form)
            .then( ({ data }) => {

                state.assetClassSummaries = data;
                state.searchingForAssetClasses = false;

            }, () => {
                state.searchingForAssetClasses = false;
            });
    },

    SUMMARY_FOR_SUB_ASSET_CLASSES(state, form) {

        state.searchingForSubAssetClasses = true;

        summaryApi.getSummaryForSubAssetClass(form)
            .then( ({ data }) => {

                state.subAssetClassSummaries = data;
                state.searchingForSubAssetClasses = false;

            }, () => {
                state.searchingForSubAssetClasses = false;
            });
    },

    SUMMARY_FOR_SECURITIES(state, form) {

        state.searchingForSecurities = true;

        summaryApi.getSummaryForSecurity(form)
            .then( ({ data }) => {

                state.securitySummaries = data;
                state.searchingForSecurities = false;

            }, () => {
                state.searchingForSecurities = false;
            });
    },

    GET_MANAGER_UNIT_FUNDS(state, id) {

        state.fetchingManagerFunds = true;

        summaryApi.getFundsForManager(id)
            .then( ({ data }) => {

                state.fundManagerFunds = data;
                state.fetchingManagerFunds = false;
                
            }, () => {
                state.fetchingManagerFunds = false;
            });
    },

    SET_UNIT_FUND(state, id) {
        state.activeUnitFund = id;
    },

    SET_SELECTED_DATE(state, date) {
        state.selected_date = date;
    }
};

export default {
    state, getters, mutations
}