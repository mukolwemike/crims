import FundSummary from './funds';
import FundPricingSummary from './pricing';

export default {
    modules: {
        FundSummary,
        FundPricingSummary
    }
}