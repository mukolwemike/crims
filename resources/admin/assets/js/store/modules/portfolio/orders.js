import * as portfolio from '../../../api/portfolioApi';
import {Message} from 'element-ui';

const state = {
    orderModal: false,
    formErrors: [],
    savingOrder: false,
    orderTypes: [],
    fetchingTypes: false,
    fetchingSecurities: false,
    securities: [],
    securityFilters: [],
    orderTypeFilters: [],
    portfolioOrderDetails: {},
    fundOrders: [],
    removeOrderModal: false,
    removingOrder: false,
    editOrderModal: false,
    fetchingRawOrder: false,
    rowPortfolioOrder: {},
    rawUnitFundOrders: [],
    editingOrder: false,
    allocateOrderModal: false,
    allocatingOrder: false,
    portfolio_fund_orders: [],
    portfolio_fund_order: {},
    exceeded: false,
    form: {},
    fetchingFundOrders: false,
    createOrderModal: false,
    complianceTypes: [],
    lifespanTypes: []
};

const getters = {
    orderModal: state => state.orderModal,
    formErrors: state => state.formErrors,
    savingOrder: state => state.savingOrder,
    orderTypes: state => state.orderTypes,
    fetchingTypes: state => state.fetchingTypes,
    fetchingSecurities: state => state.fetchingSecurities,
    securities: state => state.securities,
    orderTypeFilters: state => state.orderTypeFilters,
    portfolioOrderDetails: state => state.portfolioOrderDetails,
    fundOrders: state => state.fundOrders,
    removeOrderModal: state => state.removeOrderModal,
    removingOrder: state => state.removingOrder,
    editOrderModal: state => state.editOrderModal,
    fetchingRawOrder: state => state.fetchingRawOrder,
    rowPortfolioOrder: state => state.rowPortfolioOrder,
    rawUnitFundOrders: state => state.rawUnitFundOrders,
    editingOrder: state => state.editingOrder,
    allocateOrderModal: state => state.allocateOrderModal,
    allocatingOrder: state => state.allocatingOrder,
    portfolio_fund_orders: state => state.portfolio_fund_orders,
    portfolio_fund_order: state => state.portfolio_fund_order,
    exceeded: state => state.exceeded,
    form: state => {
        return {fund_order: []};
    },
    fetchingFundOrders: state => state.fetchingFundOrders,
    createOrderModal: state => state.createOrderModal,
    complianceTypes: state => state.complianceTypes,
    lifespanTypes: state => state.lifespanTypes
};

const mutations = {

    CREATE_ORDER_MODAL(state, value) {
        state.createOrderModal = value;
    },

    SUBMIT_ORDER(state, form) {

        state.savingOrder = true;

        portfolio.submitPortfoliOrder(form)
            .then(({data}) => {
                Message.success({message: 'Portfolio order saved for approval'});

                state.orderModal = false;

                state.savingOrder = false;

                location.replace('/dashboard/portfolio/orders');

            }, () => {
                Message.warning({message: 'An error occured while trying save the order'});
                state.savingOrder = false;
            });
    },

    GET_ORDER_TYPES(state) {

        state.fetchingTypes = true;

        portfolio.getOrderTypes()
            .then(({data}) => {

                state.orderTypes = data;

                state.fetchingTypes = false;

                state.orderTypeFilters = data;

            }, () => {

            });
    },

    GET_COMPLIANCE_TYPES(state) {
        portfolio.getComplianceTypes()
            .then(({data}) => {
                state.complianceTypes = data;
            }, () => {
            });
    },

    GET_SECURITIES(state) {

        state.fetchingSecurities = true;

        portfolio.getSecurities()
            .then(({data}) => {

                state.securities = data;
                state.fetchingSecurities = false;

            }, () => {
                state.fetchingSecurities = false;
            });
    },

    GET_ORDER_DETAILS(state, id) {

        state.fetchingFundOrders = true;
        portfolio.getOrderDetails(id)
            .then(({data}) => {
                state.portfolioOrderDetails = data;
                state.fetchingFundOrders = false;
            }, () => {
                state.fetchingFundOrders = false;
            });
    },

    GET_FUND_ORDERS(state, id) {

        portfolio.getFundOrders(id)
            .then(({data}) => {
                state.fundOrders = data;
            }, () => {

            });
    },

    EDIT_PORTFOLIO_ORDER(state, form) {

        state.editingOrder = true;

        portfolio.editOrder(form)
            .then(({data}) => {
                state.editingOrder = false;
                Message.success({message: 'Edit portfolio order saved for approval'});

                state.editOrderModal = false;

            }, () => {
                state.editingOrder = false;
                Message.warning({message: 'Error occurred during editing, try again later'});
            });
    },

    REMOVE_ORDER(state, form) {

        state.removingOrder = true;
        portfolio.removeOrder(form)
            .then(({data}) => {
                state.removingOrder = false;
                state.removeOrderModal = false;
                Message.success({message: 'Order removal saved for approval'});
            }, () => {
                state.removingOrder = false;
            });
    },

    GET_RAW_PORTFOLIO_ORDER(state, id) {

        state.fetchingRawOrder = true;

        portfolio.getRawPortfolioOrderDetails(id)
            .then(({data}) => {
                state.rowPortfolioOrder = data.portfolioOrder;
                state.rawUnitFundOrders = data.unitFundOrders;

                state.fetchingRawOrder = false;
            }, () => {
                state.fetchingRawOrder = false;
            });
    },

    GET_ORDER_LIFESPAN_TYPES(state) {

        state.fetchingOrderLifespanTypes = true;

        portfolio.getOrderLifespanTypes()
            .then(({data}) => {
                state.lifespanTypes = data;
            });
    },

    EDIT_ORDER_MODAL(state, value) {
        state.editOrderModal = value;
    },

    ALLOCATE_ORDER_MODAL(state, value) {
        state.allocateOrderModal = value;
    },

    SAVE_ORDER_ALLOCATION(state, form) {

        state.allocatingOrder = true;

        portfolio.saveOrderAllocation(form)
            .then(() => {
                Message.success({message: 'Order allocation saved for approval'});
                state.allocatingOrder = false;
                state.allocateOrderModal = false;
            }, () => {
                state.allocatingOrder = false;
                state.allocateOrderModal = false;
            });
    },

    SET_PORTFOLIO_FUND_ORDERS(state, data) {

        state.portfolio_fund_orders.push(data);

        state.portfolio_fund_order = {};
    },

    REMOVE_ORDER_MODAL(state, value) {
        state.removeOrderModal = value;
    },

    SET_LIMIT(state, value) {
        state.exceeded = value;
    }
};

const actions = {};

export default {
    state,
    getters,
    mutations,
    actions
};
