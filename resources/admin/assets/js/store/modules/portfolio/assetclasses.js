import * as portfolio from '../../../api/portfolioApi';
import { Message } from 'element-ui';
import router from '../../../router';

const state = {
    formErrors: [],
    assetClass: {},
    updateErors: [],
    assetClassId: '',
    createAssetClass: false,
    savingAsset: false,
    editAssetClass: false,
    editing: false,
};

const getters = {
    formErrors: state => state.formErrors,
    assetClass: state => state.assetClass,
    updateErors: state => state.updateErors,
    assetClassId: state => state.assetClassId,
    createAssetClass: state => state.createAssetClass,
    savingAsset: state => state.savingAsset,
    editAssetClass: state => state.editAssetClass,
    editing: state => state.editing
};

const mutations = {
    ADD_NEW_ASSET_CLASS(state, form)
    {
        state.savingAsset = true;
        portfolio.addNewAssetClass(form)
        .then( (response)=>
        {
            if(response.status == 422)
            {
                state.formErrors = response.data;
            }
            else if(response.status == 202)
            {
                Message.success({ message: 'The asset class saved for approval' });
                router.push({ name: 'asset-classes' });
            }
            state.savingAsset = false;
            state.createAssetClass = false;
        }, ()=> {
            Message.success({ message: 'There were errors while saving the asset class' });
            state.savingAsset = false;
            state.createAssetClass = false;
        });
    },

    ASSET_CLASS_DETAILS(state, assetId) {
        portfolio.getAssetClassDetails(assetId)
        .then( ({ data })=> {
            state.assetClass = data.data;
        }, ()=> {
            Message.warning({ message: 'Something went wrong while getting the asset class details' });
        });
    },

    UPDATE_ASSET_CLASS(state, form) {
        state.editing = true;
        portfolio.updateAssetClass(form)
        .then( (response)=>
        {
            if(response.status == 422) {
                state.updateErors = response.data;
                Message.warning({ message: 'Please check the form and submit again' });
            }
            else if(response.status == 202)
            {
                Message.success({ message: 'Asset Class has been saved for approval' });
                if(state.editAssetClass == false) router.push({ name: 'asset-classes' });
            }

            state.editAssetClass = false;
            state.editing = false;
        }, ()=> {
            Message.warning({ message: 'There were errors while saving the asset value' });
            state.editing = false;
        });
    },

    SET_ASSET_CLASS_ID(state, cId){
        state.assetClassId = cId;
    },


    CREATE_ASSET_CLASS(state, value)
    {
        state.createAssetClass = value;
    },

    EDIT_ASSET_CLASS_MODAL(state, value) {
        state.editAssetClass = value;
    },

};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions
}