import * as portfolio from '../../../api/portfolioApi';
import { Message } from 'element-ui';
import router from '../../../router';

import store from '../../../store';

const state = {
    transacting: false,
    settleModal: false,
    rejectModal: false,
    rejecting: false,
    settlingDeposit: false,
    orderAllocation: {},
    settleEquityModal: false,
    settlementFees: [],
    settling: false
};

const getters = {
    settleModal: state => state.settleModal,
    transacting: state => state.transacting,
    rejectModal: state => state.rejectModal,
    rejecting: state => state.rejecting,
    settlingDeposit: state => state.settlingDeposit,
    orderAllocation: state => state.orderAllocation,
    settleEquityModal: state => state.settleEquityModal,
    settlementFees: state => state.settlementFees,
    settling: state => state.settling,
};

const mutations = {
    SUBMIT_ALLOCATION_TRANSACTION(state, form) {

        state.transacting = true;

        if(form.type == 'reject' && !form.reason) {

            reject(state, true);

            return;
        }

        submit(state, form);

        reject(state, false);
    },

    SETTLE_EQUITY_ORDER_ALLOCATION(state, form) {

        state.settling = true;

        portfolio.settleEquityOrderAllocation(form)
            .then( () => {

                fetchOrderDetails(state, form);

                Message.success({ message: 'Settlement has been saved for approval'});

                state.settleEquityModal = false;

                state.orderAllocation = {};

                state.settling = false;

            }, () => {

                state.settleEquityModal = false;

                state.orderAllocation = {};

                state.settling = false;

                Message.warning({ message: 'Access denied: You have no access to: settle order allocation' });

            });
    },

    SETTLE_DEPOSIT_ORDER_ALLOCATION(state, form) {

        state.settlingDeposit = true;

        portfolio.settleDepositOrderAllocation(form)
            .then( ({ data })=> {

                state.settlingDeposit = false;
                state.settleModal = false;

                fetchOrderDetails(state, form);

                Message.success({ message: 'Settlement has been saved for approval'});
            }, () => {

                Message.warning({ message: 'Access denied: You have no access to: settle order allocation' });

                state.settlingDeposit = false;
                state.settleModal = false;

            });

    },

    SET_ALLOCATION(state, allocation) {
        state.orderAllocation = allocation;
    },

    REJECT_MODAL(state, value) {
        state.rejectModal = value;
    },

    DEPOSIT_SETTLEMENT_DIALOG(state, value) {

        state.settleModal = value;

    },

    EQUITY_SETTLEMENT_DIALOG(state, form) {

        state.settleEquityModal = form.value;
        state.orderAllocation = form.allocation;
    },

    GET_SETTLEMENT_FEES(state) {

        portfolio.getSettlementFees()
            .then( ({ data }) => {
                state.settlementFees = data;
            });
    }
};

function reject(state, value) {
    state.rejectModal = value;
}

function submit(state, form)
{
    if(state.rejectModal) state.rejecting = true;

    portfolio.submitAllocationTransaction(form)
        .then( ({data}) => {

            state.transacting = false;
            state.rejecting = false;

            if(state.rejectModal)  state.rejectModal = false;

            fetchOrderDetails(state, form);

            if(data.status === 403) {
                Message.warning({ message: 'Failed to update, there is compliance limit violated'});
                return;
            }

            Message.success({ message: 'Allocation update has been saved succesfully'});

        }, (response) => {

            Message.warning({ message: 'Access denied: You have no access to: confirm order allocation' });

            state.transacting = false;
            state.rejecting = false;
        });
}

export default {
    state,
    getters,
    mutations
};

function fetchOrderDetails(state, form) {
    store.commit('GET_ORDER_DETAILS', form.order_id);
    store.commit('GET_FUND_ORDERS', form.order_id);
}
