import * as portfolio from '../../../api/portfolioApi';
import { Message } from 'element-ui';
import router from '../../../router';

const state = {
    addSubAssestClass: false,
    savingSubAsset: false,
    editSubAssetClass: false,
    subAssetId: '',
    subAssetName: '',
    editingSubAsset: false
};

const getters = {
    addSubAssestClass: state => state.addSubAssestClass,
    savingSubAsset: state => state.savingSubAsset,
    editSubAssetClass: state => state.editSubAssetClass,
    subAssetId: state => state.subAssetId,
    subAssetName: state => state.subAssetName,
    editingSubAsset: state => state.editingSubAsset,
};

const mutations = {

    ADD_NEW_SUB_ASSET_CLASS(state, form) {

        state.savingSubAsset = true;

        portfolio.addSubAssetClasses(form)
            .then(
                (response)=>
                {
                    if (response.status == 202) {
                        Message.success({ message: 'Sub Asset Class(es) add for approval succesfully' });
                    }
                    state.savingSubAsset = false;
                    state.addSubAssestClass = false;

                },
                ()=>
                {
                    Message.warning({ message: 'There was an error while adding the sub asset classes' });
                    state.savingSubAsset = false;
                    state.addSubAssestClass = false;
                }
            );
    },

    UPDATE_SUB_ASSET_CLASS(state, form)
    {
        state.editingSubAsset = true;
        portfolio.editSubAssetClass(form)
            .then(
                (response)=>
                {
                    if (response.status == 422) {
                        Message.warning({ message: 'There are errors in the form'});
                    } else if (response.status == 202) {
                        Message.success({ message: 'The sub asset class edit has been saved for approval'});
                        state.editSubAssetClass = false;
                    }
                    state.editingSubAsset = false;

                },
                ()=>
                {
                    Message.error({ message: 'There were error while editing the sub asset class' });
                    state.editingSubAsset = false;
                }
            );
    },

    ADD_SUB_ASSET_CLASS_MODAL(state, value) {
        state.addSubAssestClass = value;
    },

    EDIT_SUB_ASSET_CLASS_MODAL(state, form) {
        state.editSubAssetClass = form.value;
    }
};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions
}