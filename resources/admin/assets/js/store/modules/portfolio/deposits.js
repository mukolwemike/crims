import * as portfolio from '../../../api/portfolioApi';
import { Message } from 'element-ui';
import router from '../../../router';

const state = {
    depositModal: false,
    savingDeposit: false,
    depositErrors: [],
    holdingDetails: {},
    holdingRepayments: [],
    reverseModal: false,
    rollbackModal: false,
    rollingbackDeposit: false,
    reversingDeposit: false,
    rollbackErrors: [],
    reverseErrors: [],
};

const getters = {
    depositModal: state => state.depositModal,
    savingDeposit: state => state.savingDeposit,
    depositErrors: state => state.depositErrors,
    holdingDetails: state => state.holdingDetails,
    holdingRepayments: state => state.holdingRepayments,
    reverseModal: state => state.reverseModal,
    rollbackModal: state => state.rollbackModal,
    rollingbackDeposit: state => state.rollingbackDeposit,
    reversingDeposit: state => state.reversingDeposit,
    rollbackErrors: state => state.rollbackErrors,
    reverseErrors: state => state.reverseErrors,
};

const mutations = {

    SAVE_DEPOSIT(state, form) {

        state.savingDeposit = true;

        portfolio.saveDepositHoldings(form)
        .then(({ data })=> {
            if (data.status == 422) {
                state.depositErrors = data.errors;
                Message.warning({ message: 'There are errors in the form.'});
            } else if (data.status == 202) {
                Message.success({ message: 'Deposit holdings has been added succesfully for approval'});
                state.depositModal = false;
            }
            state.savingDeposit = false;
        }, ()=> {
            state.depositErrors = [];
            state.depositModal = false;
            state.savingDeposit = false;
        });

    },

    DEPOSIT_HOLDING_DETAILS(state, form)
    {
        state.holdingDetails = [];
        portfolio.getDepositHoldingDetails(form)
            .then(
                ({ data })=>
                {
                    state.holdingDetails = data.data[0];
                },
                ()=>
                {
                    Message.warning({ message: 'There was error getting the deposit holding details'});
                    router.push({ name: 'portfolio-securities.show', params: { id: form.securityId } });
                }
            );
    },

    GET_DEPOSIT_REPAYMENTS(state, form) {

        portfolio.getDepositRepayments(form)
            .then(
                ({ data })=>
                {
                    state.holdingRepayments = data;
                },
                ()=>
                {
                    state.holdingRepayments = [];
                    Message.warning({ message: 'Failed to get repayments' });
                }
            );
    },

    ADD_DEPOSIT_MODAL(state, value) {
        state.depositModal = value;
    },

    REVERSE_DEPOSIT(state, form) {

        state.reversingDeposit = true;
        portfolio.reverseDeposit(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        state.reverseErrors = data.errors;
                        Message.warning({ message: 'Please provide reason for reverse.'});
                    } else if (data.status == 211) {
                        Message.warning({ message: 'This investment is withdrawn, you first need to rollback the withdrawal'});
                        state.reverseModal = false;
                    } else {
                        Message.success({ message: 'Reverse deposit withdrawal has been saved for approval' });

                        state.reverseModal = false;
                    }

                    state.reversingDeposit = false;

                },
                ()=>
                {
                    state.reversingDeposit = false;
                    Message.warning({ message: 'Failed to reverse the deposit withdrawal' });
                }
            );
    },

    ROLLBACK_DEPOSIT(state, form) {

        state.rollingbackDeposit = true;
        portfolio.rollbackDeposit(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        state.rollbackErrors = data.errors;
                        Message.warning({ message: 'Please provide reason for rollback.'});
                    } else if (data.status == 211) {
                        Message.warning({ message: 'The investment was rolled over to an existing investment, you cannot reverse the withdrawal' });
                        state.reverseModal = false;
                    } else {
                        Message.success({ message: 'The investment deposit rollback has been saved for approval' });
                        state.rollbackModal = false;
                    }

                    state.rollingbackDeposit = false;

                },
                ()=>
                {
                    state.rollingbackDeposit = false;
                    Message.warning({ message: 'Failed to rollback the deposit' });
                }
            );
    },

    SHOW_REVERSE_MODAL(state, value) {
        state.reverseModal = value;
    },

    SHOW_ROLLBACK_MODAL(state, value) {
        state.rollbackModal = value;
    }
};


export default {
    state,
    getters,
    mutations,
}
