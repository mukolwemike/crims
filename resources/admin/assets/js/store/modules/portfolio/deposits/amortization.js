import * as security from '../../../../api/portfolioApi';
import { Message } from 'element-ui';

const state = {
    amortizationSchedules: []
};

const getters = {
    amortizationSchedules: state => state.amortizationSchedules
};

const mutations = {

    GET_AMORTIZATION_SCHEDULE(state, id) {

        security.getAmortizationSchedule(id)
            .then( ({ data }) => {

                state.amortizationSchedules = data;

            }, () => {

            });
    }
};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions
}