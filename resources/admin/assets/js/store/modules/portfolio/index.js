import assetClass from './assetclasses';
import subAssetClass from './subassetclass';
import portfolioSecuriy from './securities';
import portfolioBonds from './bonds';
import portfolioEquities from './equities';
import portfolioDeposits from './deposits';
import portfolioSummaries from './summaries';
import portfolioOrders from './orders';
import PortfolioOrderAllocations from './allocations';
import Benchmark from './benchmark';
import FundsSummary from './funds/index';
import Amortization from './deposits/amortization';
import Custodial from './custodial/index';

import { Message } from 'element-ui';

import * as portfolio from '../../../api/portfolioApi';

const state = {
    investors: [],
    securityTypes: [],
    fundManagers: [],
    assetClasses: [],
    subAssetClasses: [],
    subAssetClassFilters: [],
    assetClassFilters: [],
    fetchingSubAssetClasses: false,
    investmentTypes: [],
    custodialAccounts: [],
    funds: [],
    fetching: false,
    depositTypes: [],
    taxRates: [],
    portfolioDepositTypes: [],
    sectors: [],
    states: [],
    currencies: []
};

const getters = {
    investors: state => state.investors,
    securityTypes: state => state.securityTypes,
    fundManagers: state => state.fundManagers,
    assetClasses: state => state.assetClasses,
    fetchingSubAssetClasses: state => state.fetchingSubAssetClasses,
    subAssetClasses: state => state.subAssetClasses,
    investmentTypes: state => state.investmentTypes,
    custodialAccounts: state => state.custodialAccounts,
    subAssetClassFilters: state => state.subAssetClassFilters,
    assetClassFilters: state => state.assetClassFilters,
    funds: state => state.funds,
    fetching: state => state.fetching,
    depositTypes: state => state.depositTypes,
    taxRates: state => state.taxRates,
    portfolioDepositTypes: state => state.portfolioDepositTypes,
    sectors: state => state.sectors,
    states: state => state.states,
    currencies: state => state.currencies ,
};

const mutations = {
    GET_SECURITY_TYPES(state, asset) {

        portfolio.getSecurityTypes(asset)
            .then(
                ({ data })=>
                {
                    state.securityTypes = data;
                },
                ()=>
                {
                    state.securityTypes = [];
                    Message.warning({ message: 'There are no security types found' });
                }
            );
    },
    GET_PORTFOLIO_INVESTORS(state) {

        portfolio.getPortfolioInvestors()
            .then(
                ({ data })=>
                {
                    state.investors = data;
                },
                ()=>
                {
                    state.investors = [];
                    Message.warning({ message: 'Failed to get the portfolio investors list' });
                }
            );
    },
    GET_FUND_MANAGERS(state) {

        portfolio.getFundManagers()
            .then(
                ({ data })=>
                {
                    state.fundManagers = data.data;
                },
                ()=>
                {
                    state.fundManagers = [];
                }
            );
    },
    GET_ASSET_CLASSES(state) {

        portfolio.getAssetClasses()
            .then(({data}) => {

                state.assetClasses = data.data;

                state.assetClassFilters = data.data;

            }, () => {
                state.assetClasses = [];
            });
    },

    GET_SUB_ASSET_CLASSES(state, classId) {

        state.fetchingSubAssetClasses = true;

        portfolio.getSubAssetClasses(classId)
            .then(({ data })=> {

                state.subAssetClasses = data;

                state.fetchingSubAssetClasses = false;

            }, ()=> {
                state.fetchingSubAssetClasses = false;
            });
    },
    GET_INVESTMENT_TYPE(state) {

        portfolio.getInvestmentTypes()
            .then(
                ({ data })=>
                {
                    state.investmentTypes = data;
                },
                ()=>
                {
                    state.investmentTypes = [];
                    Message.warning({ message: 'Failed to get investment types' });
                }
            );
    },
    GET_CUSTODIAL_ACCOUNTS(state, fundManagerId) {

        portfolio.getCustodialAccounts(fundManagerId)
            .then(
                ({ data })=>
                {
                    state.custodialAccounts = data.data;
                },
                ()=>
                {
                    state.custodialAccounts = [];
                    Message.warning({ message: 'Failed to get custodial accounts' });
                }
            );
    },

    GET_FUNDS(state, fund_manager_id) {

        state.fetching = true;
        portfolio.getFunds(fund_manager_id)
            .then(({ data }) => {
                if (data.status == 200) {
                    state.funds = data.funds;
                } else {
                    Message.warning({ message: 'Oops! Failed to find the fund manager' });
                }

                state.fetching = false;
            }, () => {
                state.fetching = false;
            });
    },

    GET_TAX_RATES(state) {

        portfolio.getTaxRates()
            .then(({ data})=> {
                state.taxRates = data.rates;
            }, ()=> {
                state.taxRates = [];
            });
    },

    GET_DEPOSIT_TYPES(state) {

        portfolio.getDepositTypes()
            .then(({ data})=> {
                state.depositTypes = data.types;
            }, ()=> {
                state.depositTypes = [];
            });
    },

    GET_PORTFOLIO_DEPOSIT_TYPES(state) {

        portfolio.getPortfolioDepositTypes()
            .then( ({ data }) => {
                state.portfolioDepositTypes = data;
            });
    },

    GET_SECTORS(state) {

        portfolio.getSectors()
            .then( ({ data }) => {
                state.sectors = data;
            });
    },

    GET_COUNTRIES(state) {

        portfolio.getCountries()
            .then( ({ data }) => {
                state.states = data;
            });
    },

    GET_CURRENCIES(state) {
        portfolio.getCurrencies()
            .then( ({ data }) => {
                state.currencies = data.data;
            });
    }
};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions,
    modules: {
        assetClass,
        subAssetClass,
        portfolioSecuriy,
        portfolioBonds,
        portfolioEquities,
        portfolioDeposits,
        portfolioSummaries,
        portfolioOrders,
        PortfolioOrderAllocations,
        Benchmark,
        FundsSummary,
        Amortization,
        Custodial
    }
}