import * as security from '../../../api/portfolioApi';
import { Message } from 'element-ui';

const state = {
    createBond: false,
    savingBond: false,
    bondErrors: [],
    securityDetails: {}
};

const getters = {
    createBond: state => state.createBond,
    savingBond: state => state.savingBond,
    bondErrors: state => state.bondErrors,
    securityDetails: state => state.securityDetails,
};

const mutations = {
    SAVE_BONDS(state, form) {

        state.savingBond = true;

        security.saveBonds(form)
            .then(
                ({data})=>
                {
                    if (data.status == 422) {
                        state.bondErrors = data.errors;
                        Message.warning({ message: 'Please check the form and submit again' });
                    } else {
                        Message.success({message: 'The bond security has been succesfully added for approval'});
                        state.bondErrors = [];
                        state.createBond = false;
                    }
                    state.savingBond = false;
                },
                ()=>
                {
                    Message.warning({ message: 'Failed to submit the new bond' });
                    state.savingBond = false;

                }
            );
    },

    GET_BOND_SECURITY_DETAILS(state, params) {
        security.getBondSecurityDetails(params)
        .then(
            ({ data })=>
            {
                state.securityDetails = data;
            },
            ()=>
            {
                Message.warning({ message: 'There was an error while fetching the security details' });
            }
        );
    },

    CREATE_BOND_MODAL(state, value) {
        state.createBond = value;
    }
};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions
}