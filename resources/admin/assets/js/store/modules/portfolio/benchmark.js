import * as benchmarksApi from '../../../api/portfolioApi';
import { Message } from 'element-ui';

const state = {
    benchMarkModal: false,
    savingLimit: false,
    fetchingComplaince: false,
    benchmark: {},
    limits: [],
    compliantFunds: [],

    liquidityModal: false,
    nogozoneModal: false,
};

const getters = {
    benchMarkModal: state => state.benchMarkModal,
    savingLimit: state => state.savingLimit,
    fetchingComplaince: state => state.fetchingComplaince,
    compliance: state => state.benchmark,
    limits: state => state.limits,
    compliantFunds: state => state.compliantFunds,

    liquidityModal: state => state.liquidityModal,
    nogozoneModal: state => state.nogozoneModal,
};

const mutations = {
    SAVE_BENCHMARK_LIMITS(state, form) {

        state.savingLimits = true;

        benchmarksApi.submitBenchmarkLimits(form)
            .then( () => {

                Message.success({ message: 'Limits saved for approval' });
                state.savingLimits = true;
                state.benchMarkModal = false;

            }, () => {
                state.savingLimits = true;
                state.benchMarkModal = false;
                Message.warning({ message: 'Error while submitting' });
            });
    },

    CREATE_BENCHMARK_LIMIT(state, value) {
        state.benchMarkModal = value;
    },

    SAVE_LIQUIDITY_LIMIT(state, form) {

        state.savingLimits = true;

        benchmarksApi.submitLiquidityLimits(form)
            .then( () => {

                Message.success({ message: 'Limits saved for approval' });

                state.savingLimits = true;

                state.liquidityModal = false;
            }, () => {
                state.savingLimits = true;
                state.liquidityModal = false;

                Message.warning({ message: 'Error while submitting' });
            });
    },

    CREATE_LIQUIDITY_LIMIT(state, value) {
        state.liquidityModal = value;
    },

    SAVE_NO_GO_ZONE(state, form) {
        state.savingLimits = true;

        benchmarksApi.submitNoGoZone(form)
            .then( () => {

                Message.success({ message: 'Limits saved for approval' });

                state.savingLimits = true;

                state.nogozoneModal = false;

            }, () => {

                state.savingLimits = true;
                state.nogozoneModal = false;

                Message.warning({ message: 'Error while submitting' });
            });
    },

    CREATE_NO_GO_ZONE(state, value) {
        state.nogozoneModal = value;
    },

    GET_FUND_COMPLIANCE(state, id) {

        state.fetchingComplaince = true;

        benchmarksApi.getCompliance(id)
            .then( ({ data }) => {

                state.benchmark = data.benchmark;
                state.limits = data.limits;
                state.compliantFunds = data.unitFunds;

                state.fetchingComplaince = false;

            }, () => {
                state.fetchingComplaince = false;
            });
    },


};

export default {
    state, getters, mutations
}