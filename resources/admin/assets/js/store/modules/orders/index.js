import  * as portfolioApi from '../../../api/portfolioApi';
import { Message } from 'element-ui';

const state = {
    orderForm: {},
    formFundOrder: {},
    formFundOrders: [],
    gettingCompliance: false,
    status: [],
    warningDialog: false,
};

const getters = {
    orderForm: state => state.orderForm,
    formFundOrder: state => state.formFundOrder,
    formFundOrders: state => state.formFundOrders,
    gettingCompliance: state => state.gettingCompliance,
    status: state => state.status,
    warningDialog: state => state.warningDialog,
};

const mutations = {

    SET_FORM_FUND_ORDER(state, form) {

        state.gettingCompliance = true;

        // state.formFundOrders.push(state.formFundOrder);

        portfolioApi.getComplianceLimit(form)
            .then( ({ data }) => {

                state.formFundOrder = data.unitFund;

                state.gettingCompliance = false;

                if(data.status.length > 0) {

                    state.status =  data.status;

                    state.warningDialog = true;
                }

                else {
                    state.formFundOrders.push(state.formFundOrder);
                }

            }, () => {

                state.gettingCompliance = false;

            });

    },

    CONFIRM_FUND_ORDER(state) {
        state.formFundOrders.push(state.formFundOrder);
        state.warningDialog = false;
    },


    REJECT_FUND_ORDER(state) {
        Message.warning({ message: 'The fund will not be added to the list'});
        state.warningDialog = false;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {

    }
}