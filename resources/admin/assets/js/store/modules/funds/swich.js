/**
 * Created by Yakov on 18/05/2018.
 */
import * as fundApi from '../../../api/unitfundApi';
import { Message } from 'element-ui';

const state = {
    fetchingFunds: false,
    switchingFund: false,
    switchModal: false,
    unitFundsList: [],
    switchFormErrors: {}
};

const getters = {
    switchFormErrors: state => state.switchFormErrors,
    fetchingFunds: state => state.fetchingFunds,
    switchingFund: state => state.switchingFund,
    switchModal: state => state.switchModal,
    unitFundsList: state => state.unitFundsList,
};

const mutations = {
    SWITCH_FUND(state, form) {

        state.switchingFund = true;

        fundApi.switchFund(form)
            .then(({ data })=> {
                if (data.status == 422) {
                    Message.warning({ message: 'Failed to save, there are errors in the form'});
                    state.switchFormErrors = data.errors;
                } else if (data.status == 202) {
                    Message.success({ message: 'Fund switch is successfully saved for approval'});
                    state.switchModal = false;
                    state.switchFormErrors = {};
                }
                state.switchingFund = false;
            }, () => {
                Message.warning({ message: 'Oops! an error occured please try again later'});
                state.switchingFund = false;
                state.switchFormErrors = {};
            });
    },

    GET_UNITS_FUNDS(state, fundId) {

        state.fetchingFunds = true;

        fundApi.getFunds(fundId)
            .then(({ data })=> {
                state.fetchingFunds = false;
                state.unitFundsList = data;
            }, () => {
                state.fetchingFunds = false;
                Message.warning({ message: 'Failed to fetch the funds, please try again later'});
            });
    },

    SWITCH_MODAL(state, value) {
        state.switchModal = value;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: { }
    }