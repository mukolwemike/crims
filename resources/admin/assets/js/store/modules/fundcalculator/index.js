/**
 * Created by yeric on 10/23/17.
 */
import * as calc from '../../../api/unitfundApi';
import { Message } from 'element-ui';

const state = {
    unitFundAmountResult: {},
    unitFundUnitResult: {},
    fetchingUnits: false,
    fetchingAmount: false,
    calculatorModal: false
};

const getters = {
    unitFundAmountResult: state => state.unitFundAmountResult,
    unitFundUnitResult: state => state.unitFundUnitResult,
    fetchingUnits: state => state.fetchingUnits,
    fetchingAmount: state => state.fetchingAmount,
    calculatorModal: state => state.calculatorModal,
};

const mutations = {
    CALCULATE_UNITS(state, form)
    {
        state.fetchingUnits = true;

        calc.calculate(form)
        .then(({ data })=> {

                state.fetchingUnits = false;

                state.unitFundUnitResult = data;

            }, ()=> {

                Message.warning({ message: 'The fund current unit price cannot be zero!' });

                state.fetchingUnits = false;
                state.unitFundUnitResult = {}
            });
    },
    CALCULATE_AMOUNT(state, form)
    {
        state.fetchingAmount = true;
        calc.calculate(form)
        .then(
            ({ data })=>
            {
                state.fetchingAmount = false;
                state.unitFundAmountResult = data;
            },
            ()=>
            {
                Message.warning({ message: 'The fund current unit price cannot be zero!' });

                state.fetchingAmount = false;
                state.unitFundAmountResult = {}
            }
        );
    },
    OPEN_CALCULATOR_MODAL(state, value) {
        state.calculatorModal = value;
    }
};

const actions = {

};

export default {
    state, getters, mutations, actions
}