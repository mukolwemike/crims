import * as unitFundApi from '../../../api/unitfundApi';

import { Message } from 'element-ui';
import router from '../../../router';

const state = {
    submittingFeePayment: false,
    fetchingFeePayment: false,
    fetchingRecipients: false,
    fetching: false,
    feePayment: {},
    recipients: [],
    feeErrors: [],
    fees: [],
};

const getters = {
    submittingFeePayment: state => state.submittingFeePayment,
    fetchingRecipients: state => state.fetchingRecipients,
    fetchingFeePayment: state => state.fetchingFeePayment,
    feePayment: state => state.feePayment,
    recipients: state => state.recipients,
    feeErrors: state => state.feeErrors,
    fetching: state => state.fetching,
    fees: state => state.fees,
};

const mutations = {

    SUBMIT_FEE_PAYMENT(state, form) {
        state.submittingFeePayment = true;

        unitFundApi.submitFeePayment(form)
            .then(({ data }) => {
                state.submittingFeePayment = false;

                if (data.status == 422) {
                    state.feeErrors = data.errors;
                    Message.warning({ message: 'Oops! there are form errors, check and proceed' });
                } else if (data.status == 200) {
                    state.feeErrors = [];
                    Message.success({ message: 'Fee payment sent successfully for approval' });
                    router.push({ name: 'unit-fund-fee-payments.index', params: { fid: form.unit_fund_id } });
                }
            }, () => {
                state.feeErrors = [];
                state.submittingFeePayment = false;
                Message.warning({ message: 'There is an error, please try again' });
            });
    },

    GET_FUND_FEES(state, fundId) {

        state.fetching = true;
        unitFundApi.getFundFees(fundId)
            .then(({ data }) => {
                state.fetching = false;
                state.fees = data.data;
            }, () => {
                state.fetching = false;
                Message.warning(" Failed to get fund fees");
            });
    },

    GET_FEE_PAYMENT_RECIPIENTS(state) {
        state.fetchingRecipients = true;

        unitFundApi.getFeePaymentRecipients()
            .then(({ data }) => {
                state.fetchingRecipients = false;
                state.recipients = data.data;
            }, () => {
                state.fetchingRecipients = false;
                Message.warning({ message: 'Failed to get fee payment recipients' })
            });
    },

    GET_FEE_PAYMENT(state, form) {

        state.fetchingFeePayment = true;

        unitFundApi.getUnitFundFeePayment(form)
            .then(({ data })=> {
                state.fetchingFeePayment = false;
                state.feePayment = data;
            }, () =>{
                state.fetchingFeePayment = false;
                Message.warning({ message: 'Error occurred while fetching fee payment details'});
                router.push({ name: 'unit-fund-fee-payments.index', params: { fid: form.unit_fund_id } });
            });
    }
};

export default {
    state, getters, mutations
}