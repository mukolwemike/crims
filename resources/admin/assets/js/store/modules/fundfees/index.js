import feePayments from './payments';

const state = {
    feeTypeModal: false,
};

const getters = {
    feeTypeModal: state => state.feeTypeModal
};

const mutations = {
    FEE_TYPE_CREATE_MODAL(state, value) {
        state.feeTypeModal = value;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {
        feePayments
    }
    }