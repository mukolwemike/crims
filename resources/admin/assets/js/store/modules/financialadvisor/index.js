import axios from 'axios'

const state = {
    
    selectedFA: null,

};

const getters = {

    selectedClient: state => state.selectedFA,

};

const mutations = {

    SET_SELECTED_ADVISOR(state, advisor) {

        state.selectedFA = advisor;
    },

};


export default {
    state,
    getters,
    mutations,
}