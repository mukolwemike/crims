import * as Instruction from '../../../api/applicationInstructionApi'
import Topup from './topup';
import Withdraw from './withdraw';
import Rollover from './rollover';
import Report from './report';

const state = {
    clientBankAccounts: [],
    bankDetails: {},
};

const getters = {
    clientBankAccounts: state => state.clientBankAccounts,
    bankDetails: state => state.bankDetails,
};

const mutations = {
    GET_CLIENT_ACCOUNTS(state, clientId) {

        Instruction.getClientAccounts(clientId)
            .then(
                ({data}) => {
                    (data.length > 0) ? state.clientBankAccounts = data[0] : state.clientBankAccounts = [];
                },
                () => {
                    state.clientBankAccounts = [];
                }
            );
    },
    SET_BANK_DETAILS(state, account) {

        Instruction.getBankDetails(account)
            .then(
                ({ data })=>
                {
                    state.bankDetails = data;
                },
                ()=>
                {
                    state.bankDetails = {};
                }
            );
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {
        Topup,
        Withdraw,
        Rollover,
        Report
    }
}