import * as Instruction from '../../../api/applicationInstructionApi';
import { Message } from 'element-ui';

const state = {
    confirmWithdrawalModal: false,
    withdrawalForm: {},
    submitWithdrawal: false,
    withdrawalErrors: [],
    updatingWithdrawal: false,
    editWithdrawaErrors: [],
    loadingWithdrawConfirm: false,
    withdrawFormStep: 'form'
};

const getters = {
    withdrawalForm: state => state.withdrawalForm,
    confirmWithdrawalModal: state => state.confirmWithdrawalModal,
    submitWithdrawal: state => state.submitWithdrawal,
    withdrawalErrors: state => state.withdrawalErrors,
    updatingWithdrawal: state => state.updatingWithdrawal,
    editWithdrawaErrors: state => state.editWithdrawaErrors,
    loadingWithdrawConfirm: state => state.loadingWithdrawConfirm,
    withdrawFormStep: state => state.withdrawFormStep
};

const mutations = {
    CONFIRM_WITHDRAWAL(state, { form, status }) {
        state.loadingWithdrawConfirm = true;
        if (!form.amount) {
            Instruction.getAmountSelected(form).then(({ data }) => {
                if (data != null) {
                    form.amount = data.amount;
                    form.currency = data.currency
                }
                state.confirmWithdrawalModal = status;
                state.withdrawalForm = form;
                state.loadingWithdrawConfirm = false;
                state.withdrawFormStep = 'confirm'
            }, ()=>
            {
                form.amount = form.amount_select;
                state.confirmWithdrawal = status;
                state.withdrawalForm = form;
                state.loadingWithdrawConfirm = false;
                state.withdrawFormStep = 'confirm'
            });
        } else {
            state.confirmWithdrawalModal = status;
            state.withdrawalForm = form;
            state.loadWithdrawConfirm = false;
            state.withdrawFormStep = 'confirm'
        }
    },

    SUBMIT_WITHDRAWAL_FORM(state, form)
    {
        state.submitWithdrawal = true;

        form.client_account_id = form.selected_account;
        delete form.selected_account;

        Instruction.submitWithdrawal(form).then(
            ({ data })=>
            {
                state.submitWithdrawal = false;
                if (data.status == 202) {
                    Message.success({ message: 'Withdrawal instruction sent succesfully' });
                    state.withdrawalErrors = [];

                    setTimeout(()=> {
                            location.replace('/dashboard/investments/clientinvestments/'+form.investment_id);
                        },
                        2000
                    );
                } else {
                    if (data.message) {
                        var message = data.message;
                    } else {
                        var message = 'Oops! there are errors in the form';
                    }
                    Message.warning({ message: message});
                    state.withdrawalErrors = data.errors;
                }
            },
            () => {
                state.submitWithdrawal = false;
                state.withdrawalErrors = [];
                Message.warning({ message: 'Failed to submit withdrawal instruction' });
            }
        );
    },

    CLOSE_MODAL(state, status) {
        state.confirmWithdrawalModal = status;
    },

    UPDATE_WITHDRAWAL_INSTRUCTION(state, form) {

        state.updatingWithdrawal = true;
        Instruction.updateWithdrawalInstruction(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        state.editWithdrawaErrors = data.errors;
                        Message.info({ message: 'Oop! there are errors in the form'});
                    } else if (data.status == 421) {
                        state.editWithdrawaErrors = [];
                        Message.warning({ message: 'There was no withdrawal instruction with this id'});
                    } else if (data.status == 202) {
                        state.editWithdrawaErrors = [];
                        Message.success({ message: 'Withdrawal instruction successfully updated'});

                        setTimeout(
                            ()=>
                            {
                                location.replace('/dashboard/investments/client-instructions/rollover/'+form.id)
                            },
                            2000
                        );
                    }
                    state.updatingWithdrawal = false;
                },
                ()=>
                {
                    state.updatingWithdrawal = false;
                }
            );
    }
};

export default {
    state,
    getters,
    mutations,
}