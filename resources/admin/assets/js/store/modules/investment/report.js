import * as Instruction from '../../../api/applicationInstructionApi';
import store from '../../../store';
import { Message } from 'element-ui';

const state = {
    reportForm: {},
    selectedColumns: [],
    submittingReport: false,

    reportDesc: true,
    reportCols: false,
    reportFilter: false,
    reportArgument: false,

    newReport: [],
    report_complete: false,

    modelRelations: [],
    modelColumns: [],
    reportFilters: [],
    reportFilterTypes: [],
    savedColumns: [],
    reportArguments: [],

    created_report: {},
    fetchingRelations: false,
    exportingReport: false,
    deletingReport: false,

    reportDetails: {},
    dynamicArguments: [],
    fillArguments: false,
};

const getters = {
    reportForm: state => state.reportForm,
    selectedColumns: state => state.selectedColumns,
    submittingReport: state => state.submittingReport,
    modelRelations: state => state.modelRelations,
    modelColumns: state => state.modelColumns,
    created_report: state => state.created_report,

    newReport: state => {

        let report = {
            reportDesc: state.reportDesc,
            reportCols: state.reportCols,
            reportFilter: state.reportFilter,
            reportArgument: state.reportArgument,
        };

        return report;
    },

    reportFilters: state => state.reportFilters,
    reportFilterTypes: state => state.reportFilterTypes,
    savedColumns: state => state.savedColumns,
    reportArguments: state => state.reportArguments,
    fetchingRelations: state => state.fetchingRelations,
    reportDetails: state => state.reportDetails,
    exportingReport: state => state.exportingReport,
    deletingReport: state => state.deletingReport,
    dynamicArguments: state => state.dynamicArguments,
    fillArguments: state => state.fillArguments
};

const mutations = {
    EXPORT_REPORT(state, form) {

        state.exportingReport = true;

        Instruction.exportReport(form)
            .then( () => {
                state.exportingReport = false;
                Message.success({ message: 'Report will be sent to your email.' });
                state.fillArguments = false;
            }, () => {
                state.exportingReport = false;
                Message.warning({ message: 'Error has occured while exporting'});
            });
    },

    DELETE_REPORT(state, id) {
        state.deletingReport = true;
        Instruction.deleteReport(id)
            .then( () => {
                state.deletingReport = false;
                Message.success({ message: 'Report was deleted succesfully.' });
                location.replace('/dashboard/investments/reports');
            }, () => {
                state.deletingReport = false;
            });
    },

    REPORT_DETAILS(state, id) {

        Instruction.getReportDetails(id)
            .then( ({ data }) => {
                state.reportDetails = data;
                state.dynamicArguments = data.dynamic_arguments;
            });
    },

    SAVE_REPORT(state, form) {

        Instruction.saveCustomReport(form)
            .then( ({ data })=> {
                state.created_report = data;
                addColumns();
                fetchColumns();
            });
    },

    SAVE_REPORT_COLUMNS(state, form) {
        Instruction.saveReportColumns(form)
            .then( ({ data }) => {
                addArguments();
                state.savedColumns = data;
                Message.success({ message: 'Columns added successfully'});
            });
    },

    SAVE_REPORT_ARGUMENT(state, form) {

        Instruction.saveReportArguments(form)
            .then( () => {
                Message.success({ message: 'Arguments saved succesfully'});
                addFilters();
            });
    },

    SAVE_REPORT_FILTERS(state, form) {
        Instruction.saveReportFilters(form)
            .then( () => {
                Message.success({ message: 'Filters added successfully, report now completely added'});
                location.reload();
            });
    },

    ADD_COLUMN(state, form) {
        state.selectedColumns.push(form);
    },

    ADD_FILTER(state, form) {
        state.reportFilters.push(form);

        console.log(form, state.reportFilters);
    },

    FETCH_MODEL_RELATIONS(state, form) {

        state.fetchingRelations = true;

        Instruction.getModelRelations(form)
            .then( ({ data }) => {
                state.fetchingRelations = false;
                state.modelRelations = data;
            }, () => {
                Message.error({ message: 'Model relations not found'});
                state.fetchingRelations = false;
                state.modelRelations = [];
            });
    },

    FETCH_COLUMNS(state) {
        Instruction.getModelColumns(state.reportForm)
            .then( ({ data }) => {
                state.modelColumns = data;
            });
    },

    REMOVE_COLUMN(state, form) {

        let index = state.selectedColumns.indexOf(form);

        if (index !== -1) {
            state.selectedColumns.splice(index, 1);
        }
    },

    ADD_ARGUMENT(state, form) {
        state.reportArguments.push(form);
    },

    REMOVE_FILTER(state, form) {

        let index = state.reportFilters.indexOf(form);

        if (index !== -1) {
            state.reportFilters.splice(index, 1);
        }
    },

    REMOVE_ARGUMENT(state, form) {

        let index = state.reportArguments.indexOf(form);

        if (index !== -1) {
            state.reportArguments.splice(index, 1);
        }
    },

    NEW_REPORT(state, value) {
        state.reportDesc = value;
        state.reportCols = !value;
        state.reportFilter = !value;
        state.reportArgument = !value;

        state.reportForm = {};
        state.selectedColumns = [];
    },

    GO_TO_COLUMNS(state, value) {

        state.reportDesc = !value;
        state.reportFilter = !value;
        state.reportCols = value;
        state.reportArgument = !value;
    },

    GO_TO_ARGUMENTS(state, value) {

        state.reportDesc = !value;
        state.reportFilter = !value;
        state.reportCols = !value;
        state.reportArgument = value;
    },

    GO_TO_FILTERS(state, value) {
        state.reportDesc = !value;
        state.reportFilter = value;
        state.reportCols = !value;
        state.reportArgument = !value;
    },

    GET_FILTER_TYPES() {
        Instruction.getReportFilterTypes()
            .then( ({ data }) => {
                state.reportFilterTypes = data;
            });
    },

    UPDATE_ARGUMENTS(state, value) {
        state.fillArguments = value;
    }
};


function addColumns() {
    store.commit('GO_TO_COLUMNS', true);
};

function addArguments() {
    store.commit('GO_TO_ARGUMENTS', true);
};

function fetchColumns() {
    store.commit('FETCH_COLUMNS');
};

function addFilters() {
    store.commit('GO_TO_FILTERS', true);
}

export default {
    state,
    getters,
    mutations,
}