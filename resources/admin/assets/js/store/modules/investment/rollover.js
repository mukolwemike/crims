import * as Instruction from '../../../api/applicationInstructionApi';
import { Message } from 'element-ui';

const state = {
    combineModal: false,
    combiningSelected: false,
    combineInvestments: [],
    combinedInvestments: [],
    submittingRollover: false,
    rolloverAmount: '',
    rolloverErrors: {},
    combine_date: null,
    fetchingInvestments: false,
    rolloverUpdateErrors: [],
    combiningFetched: false,
    updatingRollover: false,
};

const getters = {
    combineModal: state => state.combineModal,
    combiningSelected: state => state.combiningSelected,
    combineInvestments: state => state.combineInvestments,
    combinedInvestments: state => state.combinedInvestments,
    submittingRollover: state => state.submittingRollover,
    rolloverAmount: state => state.rolloverAmount,
    rolloverErrors: state => state.rolloverErrors,
    combineDate: state => state.combine_date,
    fetchingInvestments: state => state.fetchingInvestments,
    rolloverUpdateErrors: state => state.rolloverUpdateErrors,
    combiningFetched: state => state.combiningFetched,
    updatingRollover: state => state.updatingRollover,
};

const mutations = {
    COMBINE_MODAL(state, value) {
        state.combine_date = null;
        state.combineModal = value;
    },

    GET_COMBINED_INVESTMENTS(state, invId) {

        state.fetchingInvestments = true;
        Instruction.getCombinedInvestments(invId)
            .then(
                ({ data })=>
                {
                    state.combineInvestments = data;
                    state.fetchingInvestments = false;
                }, () => {
                    state.combineInvestments = [];
                    state.fetchingInvestments = false;
                }
            );
    },

    COMBINE_SELECTED_ROLLOVER(state, form) {

        state.combiningSelected = true;
        state.combine_date = form.combine_date;

        Instruction.combineSelectedRollover(form).then(({ data })=> {
                    state.combiningSelected = false;
                if (data.status === 422) {
                    Message.warning({ message: 'No investment should have its maturity date before the combine date' });
                } else if (data.status === 202) {
                    state.combinedInvestments = data.investments;
                    state.combineModal = false;

                    console.log(state.combinedInvestments, "at the store");

                    Message.success({ message: 'Succesfully combined' });
                }
                }, ()=> {
                    state.combiningSelected = false;
                    state.combineModal = false;

                    Message.warning({ message: 'Oops, error occured while combining' });
                });
    },

    SUBMIT_ROLLOVER(state, form) {

        state.submittingRollover = true;
        Instruction.submitRollover(form).then(({ data }) => {
                    if (data.status === 422) {
                        Message.warning({ message: 'There are errors in the form' });
                        state.rolloverErrors = data.errors;
                    } else if (data.status === 202) {


                        state.rolloverErrors = [];
                        Message.success({ message: 'Rollover submitted succesfully' });

                        setTimeout(() => {
                                location.replace('/dashboard/investments/clientinvestments/'+form.investment_id);
                            }, 2000
                        );
                    }
                    state.submittingRollover = false;
                }, ()=> {
                    state.submittingRollover = false;
                    state.rolloverErrors = [];

                    Message.warning({ message: 'Oops! an error occurred while trying to submit rollover form' });
                }
            );
    },

    GET_ROLLOVER_AMOUNT(state, form) {
        if (!form.amount) {
            Instruction.getAmountSelected(form)
                .then(
                    ({ data }) => {
                        if (data !== null) {
                            state.rolloverAmount = data.amount;
                        }
                    }, ()=> {
                        Message.warning({ message: 'Could not obtain the rollover amount' });
                    }
                );
        } else {
            state.rolloverAmount = form.amount;
        }
    },

    UPDATE_ROLLOVER_INSTRUCTION(state, form) {

        state.updatingRollover = true;
        Instruction.updateRolloverInstruction(form).then(({ data }) => {
                    if (data.status === 422) {
                        state.editRolloverErrors = data.errors;
                        Message.info({ message: 'Oop! there are errors in the form'});
                    } else if (data.status === 421) {
                        state.editRolloverErrors = [];
                        Message.warning({ message: 'There was no withdrawal instruction with this id'});
                    } else if (data.status === 202) {
                        state.editRolloverErrors = [];
                        Message.success({ message: 'Withdrawal instruction successfully updated'});

                        setTimeout(() => {
                                location.replace('/dashboard/investments/client-instructions/rollover/'+form.id)
                            },
                            2000
                        );
                    }

                    state.updatingRollover = false;
                }, ()=> {
                    state.updatingRollover = false;
                    state.editRolloverErrors = [];
                    Message.warning({ message: 'Oop! error occurred while updating the instruction'});
                }
            );
    }
};

export default {
    state,
    getters,
    mutations,
}