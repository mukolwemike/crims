import * as Instruction from '../../../api/applicationInstructionApi';
import { Message } from 'element-ui';

const state = {
    topupLoading: false,
    topupErrors: [],
    topupRate: '',
    fetchingRates: false,
    topupForm: {},
    editTopupErrors: [],
    editingTopup: false
};

const getters = {
    fetchingRates: state => state.fetchingRates,
    topupLoading: state => state.topupLoading,
    topupErrors: state => state.topupErrors,
    topupRate: state => state.topupRate,
    editTopupErrors: state => state.editTopupErrors,
    editingTopup: state => state.editingTopup,
    topupForm: (state) => {
        state.topupForm = {
            amount: null,
            agreed_rate: null,
            tenor: 12,
            product_id: null,
            mode_of_payment: null,
        };

        return state.topupForm;
    },
};

const mutations = {
    SAVE_TOP_UP(state, form) {
        state.topupLoading = true;
        Instruction.saveTopup(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        Message.warning({ message: 'Oops! there are errors in the form' });
                        state.topupErrors = data.errors;
                    } else if (data.status == 202) {
                        Message.success({ message: 'Top up succesfully saved' });
                        state.topupErrors = [];

                        location.reload();
                    } else if (data.status == 311) {
                        Message.warning({ message: 'Could not find client' });
                    }
                    state.topupLoading = false;
                },
                ()=>
                {
                    state.topupLoading = false;
                    state.topupErrors = [];
                    Message.warning({ message: 'There was an error while saving, try again' });
                }
            );
    },

    GET_TOPUP_RATES(state, item) {

         state.fetchingRates = true;
         Instruction.getTopupRates(item)
            .then(
                ({ data }) =>
                {
                    state.fetchingRates = false;
                    state.topupRate = data;
                },
                ()=>
                {
                    Message.warning({ message: 'Could not get the rate' });
                }
            );
    },

    UPDATE_TOPUP(state, form) {

        state.editingTopup = true;
        Instruction.updateTopup(form)
            .then(
                ({ data })=>
                {
                    if (data.status == 422) {
                        state.editTopupErrors = data.errors;
                        Message.info({ message: 'Oop! there are errors in the form'});
                    } else if (data.status == 421) {
                        state.editTopupErrors = [];
                        Message.warning({ message: 'There was no top up instruction with this id'});
                    } else if (data.status == 202) {
                        state.editTopupErrors = [];
                        Message.success({ message: 'Top up instruction successfully updated'});

                        setTimeout(
                            ()=>
                            {
                                location.replace('/dashboard/investments/client-instructions/topup/'+form.id)
                            },
                            2000
                        );
                    }
                    state.editingTopup = false;
                },
                ()=>
                {
                    state.editingTopup = false;
                    Message.warning({ message: 'There was an error while updating the top up instruction' });
                }
            );
    }
};

export default {
    state,
    getters,
    mutations,
}