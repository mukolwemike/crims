/**
 * Created by yeric on 10/10/17.
 */
import * as Instruction from '../../../api/applicationInstructionApi';
import { Message } from 'element-ui';

const state = {
    updatingApplication: false,
    editErrors: []
};

const getters = {
    updatingApplication: state => state.updatingApplication,
    editErrors: state => state.editErrors,
};

const mutations = {

    UPDATE_APPLICATION_INSTRUCTION(state, form) {

        state.updatingApplication = true;

        Instruction.updateSubscriberDetails(form)
            .then( ({ data })=> {
                if (data.status == 202) {

                    Message.success({ message: 'Application instruction updated succesfully' });
                    state.editErrors= [];

                    location.replace('/dashboard/investments/client-instructions/application/'+form.id);

                } else if (data.status == 422) {
                    Message.warning({ message: 'There are errors on the form' });
                    state.editErrors = data.errors;
                }

                state.updatingApplication = false;
            },
            ()=>
            {
                Message.warning({ message: 'Oops! there was an error while updating the form' });
                state.updatingApplication = false;
                state.editErrors = [];
            });
    }
};

const actions = {

};

export default {
    state, getters, mutations, actions
}
