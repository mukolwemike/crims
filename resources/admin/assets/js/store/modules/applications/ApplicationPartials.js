import * as Contact from '../../../api/applicationPartials';

const state = {
    titles: [],
    countries: [],
    employmentTypes: [],
    contactMethods: [],
    sourceOfFunds: [],
    businessNatures: [],
    productsList: [],
    individual: false,
    corporate: false,
    genderList: [],
    bankAccounts: [],
    application_type: '',
    completeType: 1,
    unitFunds: [],
    allUnitFunds: [],
    fetchingFund: false,
    unitFundFilters: [],
    allShareEntities: [],
};

const getters = {
    titles: state => state.titles,
    countries: state => state.countries,
    employmentTypes: state => state.employmentTypes,
    contactMethods: state => state.contactMethods,
    sourceOfFunds: state => state.sourceOfFunds,
    businessNatures: state => state.businessNatures,
    productsList: state => state.productsList,
    individual: state => state.individual,
    corporate: state => state.corporate,
    genderList: state => state.genderList,
    bankAccounts: state => state.bankAccounts,
    application_type: state => state.application_type,
    completeType: state => state.completeType,
    unitFunds: state => state.unitFunds,
    allUnitFunds: state => state.allUnitFunds,
    allShareEntities: state => state.allShareEntities,
    fetchingFund: state => state.fetchingFund,
    unitFundFilters: state => state.unitFundFilters,
};

const mutations = {
    GET_TITLES_LIST(state) {

        Contact.titles()
            .then(
                ({data}) => {
                    state.titles = data;
                }, () => {
                }
            );
    },

    GET_COUNTRIES_LIST(state) {

        Contact.countries()
            .then(
                ({data}) => {
                    state.countries = data;
                }, () => {
                }
            );
    },

    GET_EMPLOYMENT_TYPES_LIST(state) {

        Contact.employmentTypes()
            .then(
                ({data}) => {
                    state.employmentTypes = data;
                }, () => {
                }
            );
    },

    GET_CONTACT_METHOD_LIST(state) {

        Contact.contactMethods()
            .then(
                ({data}) => {
                    state.contactMethods = data;
                }, () => {
                }
            );
    },

    GET_SOURCE_OF_FUNDS_LIST(state) {

        Contact.sourceOfFunds()
            .then(
                ({data}) => {
                    state.sourceOfFunds = data;
                }, () => {
                }
            );
    },

    GET_BUSINESS_NATURES_LIST(state) {

        Contact.businessNatures()
            .then(
                ({data}) => {
                    state.businessNatures = data;
                }, () => {
                }
            );
    },

    GET_GENDER(state) {
        Contact.gender()
            .then(
                ({data}) => {
                    state.genderList = data;
                }, () => {
                }
            );
    },

    GET_PRODUCTS_LIST(state) {

        Contact.getProducts('chys')
            .then(({data}) => {
                state.productsList = data[0];
            }, () => {

            });
    },

    SET_CLIENT_TYPE(state, value) {
        if (value == 0) {
            state.individual = true;
            state.corporate = false;
        } else {
            state.individual = false;
            state.corporate = true;
        }
    },

    SET_COMPLETE_OR_NOT(state, value) {
        state.completeType = value
    },

    SET_APPLICATION_TYPE(state, type) {
        state.application_type = type;
    },

    GET_ALL_UNIT_FUNDS(state) {

        state.fetchingFund = true;

        Contact.getAllUnitFunds()
            .then(({data}) => {

                state.allUnitFunds = data.data;

                state.fetchingFund = false;

            });
    },

    GET_ALL_SHARE_ENTITY(state) {

        Contact.getAllShareEntity()
            .then(({data}) => {

                state.allShareEntities = data.data;

                state.fetchingFund = false;

            });
    },
};

export default {state, getters, mutations}
