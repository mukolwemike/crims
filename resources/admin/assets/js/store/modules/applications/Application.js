/**
 * Created by yeric on 10/11/17.
 */
import {Message} from 'element-ui';
import * as submit from '../../../api/applicationApi';

const state = {
    formErrors: [],
    applicationDetails: {},
    submitting: false,
    sendingForApproval: false,
    sendingForInvestment: false,
    updating: false,
    submittingExisting: false,
    cancellingTransaction: false,
};

const getters = {
    formErrors: state => state.formErrors,
    applicationDetails: state => state.applicationDetails,
    submitting: state => state.submitting,
    sendingForApproval: state => state.sendingForApproval,
    sendingForInvestment: state => state.sendingForInvestment,
    updating: state => state.updating,
    submittingExisting: state => state.submittingExisting,
    cancellingTransaction: state => state.cancellingTransaction,
};

const mutations = {
    SUBMIT_APPLICATION_FORM(state, form) {
        state.submitting = true;

        submit.submitApplicationForm(form)
            .then((response) => {

                if (response.data.status == 202) {
                    Message.success('Application sent successfully');
                    window.location.href = "/dashboard/applications";
                } else if (response.data.status == 422) {
                    state.formErrors = response.data.errors;
                    Message.warning('There are errors in the form');
                } else if (response.data.status == 423) {
                    state.formErrors = [];
                    Message.warning('Oop! Application made already exists');
                }
                state.submitting = false;
            }, () => {
                Message.warning('Failed to save the application');
                state.submitting = false;
            });
    },

    INVEST_APPLICATION(state, form) {
        state.sendingForInvestment = true;

        submit.investApplication(form)
            .then(({data}) => {
                state.sendingForInvestment = false;

                if (data.status === 405) {
                    Message.warning('Client code entered is already used for another client');
                } else {
                    Message.success('The application investment has been saved successfully for approval');

                    if (data.userId) {
                        window.location.href = '/dashboard/investments/client-instructions/';
                    } else {
                        window.location.href = '/dashboard/investments/client-instructions/' + data.approvalId + '/approval-document';
                    }
                }
            }, () => {
                state.sendingForInvestment = false;
                Message.warning('Failed to save the application for investment');
            });
    },

    SALE_APPLICATION(state, form) {
        state.sendingForInvestment = true;

        submit.saleApplication(form)
            .then(({data}) => {
                state.sendingForInvestment = false;

                Message.success('The application sale has been saved successfully for approval');

                if (data.userId) {
                    window.location.href = '/dashboard/investments/client-instructions/';
                } else {
                    window.location.href = '/dashboard/investments/client-instructions/' + data.approvalId + '/approval-document';
                }
            }, () => {
                state.sendingForInvestment = false;
                Message.warning('Failed to save the application for investment');
            });
    },

    TRANSFER_APPLICATION(state, form) {
        state.sendingForApproval = true;

        submit.transferApplication(form)
            .then(({data}) => {
                state.sendingForApproval = false;

                Message.success('The application transfer has been saved successfully for approval');

                if (data.userId) {
                    window.location.href = '/dashboard/investments/client-instructions/';
                } else {
                    window.location.href = '/dashboard/investments/client-instructions/' + data.approvalId + '/approval-document';
                }
            }, () => {
                state.sendingForApproval = false;
                Message.warning('Failed to transfer the application for investment');
            });
    },

    CANCEL_TRANSACTION(state, form){
        state.cancellingTransaction = true;

        submit.cancelApplication(form)
            .then(()=>{
                state.cancellingTransaction = false;

                Message.success('The application cancellation has been saved successfully for approval');

                window.location.href = '/dashboard/investments/client-instructions/';
            },()=>{
                state.cancellingTransaction = false;
                Message.warning('Failed to cancel the application for investment');
            });
    },

    UPDATE_APPLICATION(state, form) {

        state.updating = true;

        submit.updateApplication(form)
            .then(({data}) => {

                Message.success('The application has been saved successfully for approval');

                state.updating = false;

            }, () => {
                state.updating = false;

                Message.warning('Failed to update the application');
            });
    },

    SUBMIT_EXISTING_CLIENT(state, form) {

        console.log(form, "at the mutator");
        state.submittingExisting = true;

        submit.submitApplicationForm(form)
            .then(
                (response) => {
                    state.submittingExisting = false;
                },
                () => {
                    state.submittingExisting = false;
                }
            );
    }

};

export default {
    state, getters, mutations
}

