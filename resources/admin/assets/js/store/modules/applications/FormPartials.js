/**
 * Created by yeric on 10/10/17.
 */
import * as partials from '../../../api/applicationApi';

const state = {
    formPartials: [],
    banks: [],
    branches: [],
    fetchingBranches: false,
    applicationType: '',
    clientType: '',
    unitFundDetails: {},
    fetchingUnitFund: false,
    commissionRecipients: [],
    clients: [],
};

const getters = {
    formPartials: state => state.formPartials,
    banks: state => state.banks,
    branches: state => state.branches,
    fetchingBranches: state => state.fetchingBranches,
    applicationType: state => state.applicationType,
    clientType: state => state.clientType,
    unitFundDetails: state => state.unitFundDetails,
    fetchingUnitFund: state => state.fetchingUnitFund,
    commissionRecipients: state => state.commissionRecipients,
    clients: state => state.clients,
};

const mutations = {
    GET_FORM_PARTIALS(state)
    {
        partials.getFormPartials().then(
            ({ data })=>
            {
                state.formPartials = data;
                console.log(data, "the form partials");
            },
            ()=>
            {
                console.log("Failed to get form partials");
            }
        );
    },

    GET_BANK(state)
    {
        partials.getBanks().then(
            ({ data })=>
            {
                state.banks = data;

            },
            ()=>
            {
                console.log("Failed to retrieve banks details");
            }
        );
    },

    GET_BANK_BRANCHES(state, bankId)
    {
        state.fetchingBranches = true;

        partials.getBankBranches(bankId)
            .then( ({ data })=>
            {
                state.branches = data;

                state.fetchingBranches = false;
            },
            ()=>
            {
                state.fetchingBranches = false;

                console.log("failed to fetch the bank branches");
            }
        );
    },

    SET_FORM_CLIENT_TYPE(state, type)
    {
        state.clientType = type;
        console.log(state.clientType, "client type");
    },

    SET_FORM_APPLICATION_TYPE(state, type)
    {
        state.applicationType = type;
    },

    GET_UNIT_FUND_DETAILS(state, unitFundId)
    {
        state.fetchingUnitFund = true;

        partials.getUnitFundDetails(unitFundId).then(
            ({ data }) =>
            {
                state.unitFundDetails = data.data;
                state.fetchingUnitFund = false;
            },
            () =>
            {
                state.unitFundDetails = {};
                state.fetchingUnitFund = false;
            }
        );
    },

    GET_COMMISSION_RECEPIENT(state)
    {
        partials.getCommissionRecepients()
        .then(
            ({ data }) =>
            {
                state.commissionRecipients = data;
            },
            () =>
            {
                state.commissionRecipients = [];
            }
        );
    },

    GET_ALL_CLIENTS(state) {

        partials.getAllClients()
        .then(
            ({ data })=>
            {
                state.clients= data;
            },
            ()=>
            {
                state.clients = [];
            }
        );
    }
};


const actions = {

};

export default {
    state, getters, mutations, actions
}
