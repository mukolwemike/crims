/**
 * Created by yeric on 10/9/17.
 */
import FormPartials from './FormPartials';
import Applications from './Application';
import ClientSearch from './search';
import InvestmentsApply from './investments';
import ApplicationPartials from './ApplicationPartials'
import UpdateApplication from './update';

const state = {

};

const getters = {

};

const mutations = {

};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions,
    modules: {
        FormPartials,
        Applications,
        ClientSearch,
        InvestmentsApply,
        ApplicationPartials,
        UpdateApplication,
    }
}
