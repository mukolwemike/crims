import * as Instruction from '../../../api/applicationInstructionApi';
import { Message } from 'element-ui';

const state = {
    InvestmentApplication: [],

    investmentErrors: [],
    subscriberErrors: [],
    riskErrors: [],

    riskStatus: false,
    investmentStatus: false,
    subscriberStatus: false,
    kycStatus: false,
    paymentStatus: false,

    riskLoading: false,
    investmentLoading: false,
    subscriberLoading: false,
    completeLoading: false,

    application_uuid: '',

    uploadedDocuments: [],
    defaultHeaders: {},

    fetchingRate: false,
    interestRate: '',

    holdersForm: {},
    jointHolderErrors: '',
    jointHolderModalForms: false,
    submitJointHolder: false,
    jointHolderForm: [],

    completingKyc: false,
    completingPayment: false,
    completeModal: false,
    proceedModal: false,
    paymentProceed: false,

    application_name: null
};

const getters = {
    InvestmentApplication: state => state.InvestmentApplication,

    investmentErrors: state => state.investmentErrors,
    riskErrors: state => state.riskErrors,
    subscriberErrors: state => state.subscriberErrors,

    riskStatus: state => state.riskStatus,
    investmentStatus: state => state.investmentStatus,
    subscriberStatus: state => state.subscriberStatus,
    kycStatus: state => state.kycStatus,
    paymentStatus: state => state.paymentStatus,

    riskLoading: state => state.riskLoading,
    investmentLoading: state => state.investmentLoading,
    subscriberLoading: state => state.subscriberLoading,
    completeLoading: state => state.completeLoading,

    application_uuid: state => state.application_uuid,

    uploadedDocuments: state => state.uploadedDocuments,
    defaultHeaders: state => state.defaultHeaders,

    fetchingRate: state => state.fetchingRate,
    interestRate: state => state.interestRate,

    holdersForm: state => state.holdersForm,
    jointHolderErrors: state => state.jointHolderErrors,
    jointHolderModal: state => state.jointHolderModalForms,

    submitJointHolder: state => state.submitJointHolder,
    jointHolderForm: state => state.jointHolderForm,

    completingKyc: state => state.completingKyc,
    completingPayment: state => state.completingPayment,
    completeModal: state => state.completeModal,

    proceedModal: state => state.proceedModal,
    paymentProceed: state => state.paymentProceed,

    application_name: state => state.application_name,

    InvestmentSummary: state => {
        return {
            amount: state.InvestmentApplication.appliedInvestment.amount,
            tenor: state.InvestmentApplication.appliedInvestment.tenor,
            kyc_complete: state.uploadedDocuments.length > 3,
            name: state.application_name
        }
    },
};

const mutations = {
    SAVE_RISK_ASSESMENT(state, form) {

        state.riskLoading = true;
        Instruction.validateRiskAssesment(form)
            .then(
                ({ data }) =>
                {
                    if (data.status == 202) {
                        Message.success({ message: 'The risk assessment validate successfully '});
                        state.InvestmentApplication.appliedRisk = form;
                        state.riskStatus = true;
                        state.riskErrors = [];
                    } else if (data.status == 422) {
                        Message.warning({ message: 'There are errors in the form, please check and try again'});
                        state.riskErrors = data.errors;
                    }

                    state.riskLoading = false;
                },
                () =>
                {
                    Message.warning({ message: 'There are errors while trying to submit the form'});
                    state.riskLoading = false;
                    state.riskErrors = [];
                }
            );
    },

    SAVE_INVESTMENT_SECTION(state, form) {

        state.investmentLoading = true;

        Instruction.validateInvestmentDetails(form)
            .then(
                ({ data }) =>
                {
                    if (data.status == 202) {
                        Message.success({ message: 'The investment details validated succesfully '});

                        state.InvestmentApplication.appliedInvestment = form;

                        state.investmentStatus = true;

                        state.investmentErrors = [];
                    } else if (data.status == 422) {
                        Message.warning({ message: 'There are errors in the form, please check and try again'});
                        state.investmentErrors = data.errors;
                    }

                    state.investmentLoading = false;
                },
                () =>
                {
                    Message.warning({ message: 'There are errors while trying to submit the form'});
                    state.investmentErrors = [];
                    state.investmentLoading = false;
                }
            );
    },

    SAVE_SUBSCRIBER_DETAILS(state, form) {

        state.subscriberLoading = true;

        if (!state.InvestmentApplication.appliedInvestment) {

            Message.warning({ message: 'Please fill the investment section and proceed'});
            state.subscriberLoading = false;

        } else {
            state.InvestmentApplication.appliedSubscriber = form;

            state.InvestmentApplication.appliedSubscriber.jointHolders = state.jointHolderForm;

            let investmentForm = Object.assign( {},

                state.InvestmentApplication.appliedSubscriber,
                state.InvestmentApplication.appliedInvestment
            );


            Instruction.saveSubscriberDetails(investmentForm)
                .then( ({ data })=> {
                        if (data.status == 202) {
                            state.subscriberStatus = true;
                            Message.success({ message: 'The subscriber details saved correctly' });
                            state.subscriberErrors = [];

                            setTimeout( ()=> {
                                location.reload();
                            }, 1000);
                            // state.application_uuid = data.application_uuid;
                            // state.application_name = data.application_name;

                        } else if (data.status == 422) {
                            Message.warning({ message: 'Oops! there are errors in the form' });
                            state.subscriberErrors = data.errors;
                        }

                        state.subscriberLoading = false;
                    }, ()=> {
                        Message.warning({ message: 'Error occured while saving the subscriber, please try again' });
                        state.subscriberLoading = false;
                        state.subscriberErrors = [];
                    }
                );
        }
    },
    KYC_COMPLETE(state, applId) {

        state.completingKyc = true;
        Instruction.kycComplete(applId)
            .then(
                ({ data })=>
                {
                    Message.success({ message: 'The Kyc documents have been uploaded succesfully'});
                    state.InvestmentApplication.appliedKyc = true;
                    state.kycStatus = true;

                    state.completingKyc = false;
                },
                ()=> { state.completingKyc = false; }
            );
    },

    KYC_NOT_COMPLETE(state, applId) {
        state.completingKyc = true;
        Instruction.kycComplete(applId)
            .then(
                ({  })=>
                {
                    state.InvestmentApplication.appliedKyc = true;
                    state.kycStatus = true;

                    state.completingKyc = false;
                    state.proceedModal = false;
                },
                ()=>
                {
                    state.completingKyc = false;
                    state.proceedModal = false;
                }
            );
    },

    OPEN_CONFIRM_MODAL(state, value){
        state.proceedModal = value;
    },

    GET_KYC_DOCUMENTS(state, applId) {

        Instruction.getKycDocuments(applId)
            .then(
                ({ data })=>
                {
                    state.uploadedDocuments = data.documents;
                },
                ()=>
                {
                    state.uploadedDocuments = [];
                }
            );
    },

    SET_DEFAULT_HEADERS(state, headers) {
        state.defaultHeaders = headers;
    },

    UPLOAD_PAYMENT_COMPLETE(state, applId) {

        state.completingPayment = true;
        Instruction.uploadPaymentComplete(applId)
            .then(
                ()=> {
                    state.InvestmentApplication.appliedPayment = true;
                    state.paymentStatus = true;
                    state.completeModal  = true;
                    state.completingPayment = false;
                },
                ()=>
                {
                    state.completingPayment = false;
                    state.paymentProceed = true;
                }
            );
    },

    PAYMENT_NOT_COMPLETE(state, applId) {
        state.completingPayment = true;
        Instruction.uploadPaymentComplete(applId)
            .then(()=> {
                state.InvestmentApplication.appliedPayment = true;
                state.paymentStatus = true;
                state.completeModal  = true;

                state.completingPayment = false;
            }, ()=> {
                state.completingPayment = false;
                state.paymentProceed = true;
            });
    },

    CLOSE_PAYMENT_MODAL(state, value) {
        state.paymentProceed = value;
    },

    COMPLETE_APPLICATION(state, applId) {

        state.completeLoading = true;
        Instruction.completeApplication(applId)
            .then(
                ({ data })=>
                {
                    if (data.status == 202) {
                        Message.success({ message: 'Application completed succesfully '});
                        state.InvestmentApplication.applicationComplete = true;

                        location.reload();
                    } else if (data.status == 422) {
                        Message.warning({ message: 'Failed to complete the application' });
                    }

                    state.completeLoading = false;

                },
                ()=> { state.completeLoading = false; }
            );
    },
    GET_AGREED_INTEREST_RATE(state, form) {

        state.fetchingRate = true;
        Instruction.getAgreedInterestRate(form)
            .then(
                ({ data })=>
                {
                    state.fetchingRate = false;
                    state.interestRate = data;

                },
                ()=>
                {
                    state.fetchingRate = false;
                    Message.warning({ message: 'Failed to get agreed interest rate' });
                }
            );
    },
    jointHolderModal(state, value) {
        state.holdersForm = {};
        state.jointHolderErrors = '';
        state.jointHolderModalForms = value;
    },

    CLOSE_SUCCESS_MODAL(state, value) {
        state.completeModal = value;
    }
};

export default { state, getters, mutations }