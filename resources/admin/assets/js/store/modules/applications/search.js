const state = {
    getOptions: []
};

const getters = {
    getOptions: state => state.getOptions
};

const mutations = {

};

const actions = {

};

export default {
    state,
    getters,
    mutations,
    actions,
    modules: {

    }
    }
