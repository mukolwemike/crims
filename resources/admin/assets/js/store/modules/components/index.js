import * as crims from '../../../api/crimsApi';
import { Message } from 'element-ui';

const state = {
    holdersModal: false,
    jointHolders: [],
    adding: false,
    holderErrors: [],
    jointForm: {},
};

const getters = {
    holdersModal: state => state.holdersModal,
    jointHolders: state => state.jointHolders,
    adding: state => state.adding,
    holderErrors: state => state.holderErrors,
    jointForm: state => state.jointForm,
};

const mutations = {
    HOLDER_MODAL(state, value) {

        state.holdersModal = value;

    },

    ADD_JOINT_HOLDER(state, form) {

        state.adding = true;
        crims.validateJointHolder(form)
            .then(({ data })=> {
                if (data.status == 202) {
                    state.jointHolders.push(form);
                    Message.success({ message: 'Successfully validated'});
                    state.holdersModal = false;
                } else if (data.status == 422) {
                    state.holderErrors = data.errors;
                    Message.warning({ message: 'Oops! there are errors in the form'});
                }

                state.adding = false;
                state.jointForm = {};
            }, ()=>
            {
                Message.warning({ message: 'There was an error while validating' });
                state.holdersModal = false;
                state.formErrors = [];

                state.adding = false;
                state.jointForm = {};
            });

    },

    REMOVE_HOLDER(state, holder) {

        var index = state.jointHolders.indexOf(holder);

        if (index !== -1) {
            state.jointHolders.splice(index, 1);
            Message.info({ message: 'Holder removed' });
        }
    },

    RESET_FORM(state) {
        state.jointForm = {};
    }
};

export default {
    state, getters, mutations
}