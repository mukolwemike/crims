import * as Dashboard from '../../../api/dashboardApi';
import Summary from './summary';
import Chart from 'chart.js';
import Notifications from './noti.js';

const state = {
    clientsStats: {},
    withdrawalsStats: {},
    inflowsStats: {},
    aumStats: {},

    fetchingClientsStats: false,
    fetchingWithdrawalsStats: false,
    fetchingInflowsStats: false,
    fetchingAumStats: false,

    pendingTransactionsStats: null,
    spInvestmentsStats: {},
    utfInvestmentsStats: {},
    investmentsApplications: {},
    clientPendingInstructions: {},

    fetchingPendingTransactionsStats: false,
    fetchingSpInvestmentsStats: false,
    fetchingUtfInvestmentsStats: false,
    fetchingInvestmentsApplications: false,
    fetchingClientPendingInstructions: false

};

const getters = {
    clientsStats: state => state.clientsStats,
    withdrawalsStats: state => state.withdrawalsStats,
    inflowsStats: state => state.inflowsStats,
    aumStats: state => state.aumStats,

    fetchingClientsStats: state => state.fetchingClientsStats,
    fetchingWithdrawalsStats: state => state.fetchingWithdrawalsStats,
    fetchingInflowsStats: state => state.fetchingInflowsStats,
    fetchingAumStats: state => state.fetchingAumStats,

    pendingTransactionsStats: state => state.pendingTransactionsStats,
    spInvestmentsStats: state => state.spInvestmentsStats,
    utfInvestmentsStats: state => state.utfInvestmentsStats,
    investmentsApplications: state => state.investmentsApplications,
    clientPendingInstructions: state => state.clientPendingInstructions,

    fetchingPendingTransactionsStats: state => state.fetchingPendingTransactionsStats,
    fetchingSpInvestmentsStats: state => state.fetchingSpInvestmentsStats,
    fetchingUtfInvestmentsStats: state => state.fetchingUtfInvestmentsStats,
    fetchingInvestmentsApplications: state => state.fetchingInvestmentsApplications,
    fetchingClientPendingInstructions: state => state.fetchingClientPendingInstructions
};

const mutations = {
    GET_CLIENTS_STATS(state) {
        state.fetchingClientsStats = true;

        Dashboard.getClientsStats()
            .then(({data}) => {
                state.fetchingClientsStats = false;
                state.clientsStats = data;
            }, () => {
                state.fetchingClientsStats = false;
            });
    },

    GET_WITHDRAWALS_STATS(state) {
        state.fetchingWithdrawalsStats = true;

        Dashboard.getWithdrawalsStats()
            .then(({data}) => {
                state.fetchingWithdrawalsStats = false;
                state.withdrawalsStats = data;
            }, () => {
                state.fetchingWithdrawalsStats = false;
            });
    },

    GET_INFLOWS_STATS(state) {
        state.fetchingInflowsStats = true;

        Dashboard.getInflowsStats()
            .then(({data}) => {
                state.fetchingInflowsStats = false;
                state.inflowsStats = data;
            }, () => {
                state.fetchingInflowsStats = false;
            });
    },

    GET_AUM_STATS(state) {
        state.fetchingAumStats = true;

        Dashboard.getAumStats()
            .then(({data}) => {
                state.fetchingAumStats = false;
                state.aumStats = data;
            }, () => {
                state.fetchingAumStats = false;
            });
    },

    GET_SP_INVESTMENTS_STATS(state) {
        state.fetchingSpInvestmentsStats = true;

        Dashboard.getSpInvestments()
            .then(({data}) => {
                state.fetchingSpInvestmentsStats = false;
                state.spInvestmentsStats = data;
            }, () => {
                state.fetchingSpInvestmentsStats = false;
            });
    },

    GET_UTF_INVESTMENTS_STATS(state) {
        state.fetchingUtfInvestmentsStats = true;

        Dashboard.getUtfInvestments()
            .then(({data}) => {
                state.fetchingUtfInvestmentsStats = false;
                state.utfInvestmentsStats = data;
            }, () => {
                state.fetchingUtfInvestmentsStats = false;
            });
    },

    GET_APPLICATION_STATS(state) {
        state.fetchingInvestmentsApplications = true;

        Dashboard.getInvestmentsApplications()
            .then(({data}) => {
                state.fetchingInvestmentsApplications = false;
                state.investmentsApplications = data;
            }, () => {
                state.fetchingInvestmentsApplications = false;
            });
    },

    GET_INSTRUCTIONS_STATS(state) {
        state.fetchingClientPendingInstructions = true;

        Dashboard.getClientPendingInstructions()
            .then(({data}) => {
                state.fetchingClientPendingInstructions = false;
                state.clientPendingInstructions = data;
            }, () => {
                state.fetchingClientPendingInstructions = false;
            });
    },

    GET_PENDING_TRANSACTIONS_STATS(state) {
        state.fetchingPendingTransactionsStats = true;

        Dashboard.getPendingTransactions()
            .then(({data}) => {
                state.fetchingPendingTransactionsStats = false;
                state.pendingTransactionsStats = data;
            }, () => {
                state.fetchingPendingTransactionsStats = false;
            });
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {
        Summary,
        Chart,
        Notifications
    }
}
