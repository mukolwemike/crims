import * as dashboardApi from '../../../api/dashboardApi';

const state = {
    matureWithdrawals: {},
    preMatureWithdrawals: {},
    activeInvestments: {},
    pendingInvestmentTransaction: {},
    pendingPortfolioTransaction: {},
    activePortfolioInvestment: {},

    fetchingMatureWithdrawals: false,
    fetchingPrematureWithdrawals: false,
    fetchingActiveInvestments: false,
    fetchingPendingTransactions: false,
    fetchingPendingPortfolioTransactions: false,
    fetchingPortfolioInvestments: false,
};

const getters = {

    matureWithdrawals: state => state.matureWithdrawals,
    preMatureWithdrawals: state =>state. preMatureWithdrawals,
    activeInvestments: state=> state.activeInvestments,
    pendingInvestmentTransaction: state=> state.pendingInvestmentTransaction,
    pendingPortfolioTransaction: state=> state.pendingPortfolioTransaction,
    activePortfolioInvestment: state=> state.activePortfolioInvestment,

    fetchingMatureWithdrawals: state => state.fetchingMatureWithdrawals,
    fetchingPrematureWithdrawals: state => state.fetchingPrematureWithdrawals,
    fetchingActiveInvestments: state => state.fetchingActiveInvestments,
    fetchingPendingTransactions: state => state.fetchingPendingTransactions,
    fetchingPendingPortfolioTransactions: state => state.fetchingPendingPortfolioTransactions,
    fetchingPortfolioInvestments: state => state.fetchingPortfolioInvestments,
};

const mutations = {
    MATURE_WITHDRAWALS(state) {
         state.fetchingMatureWithdrawals = true;

         dashboardApi.getMatureWithdrawals()
             .then( ({ data }) => {
                 state.matureWithdrawals = data;
                 state.fetchingMatureWithdrawals = false;
             }, () => {
                 state.fetchingMatureWithdrawals = false;
             });
    },

    PRE_MATURE_WITHDRAWALS(state) {
        state.fetchingPrematureWithdrawals = true;

        dashboardApi.getPrematureWithdrawals()
            .then( ({ data }) => {
                state.preMatureWithdrawals = data;
                state.fetchingPrematureWithdrawals = false;
            }, () => {
                state.fetchingPrematureWithdrawals = false;
            });
    },

    ACTIVE_INVESTMENTS(state) {
        state.fetchingActiveInvestments = true;

        dashboardApi.getActiveInvestments()
            .then( ({ data }) => {
                state.activeInvestments = data;
                state.fetchingActiveInvestments = false;
            }, () => {
                state.fetchingActiveInvestments = false;
            });
    },

    PENDING_INVESTMENT_TRANSACTIONS(state) {
        state.fetchingPendingTransactions = true;

        dashboardApi.getPendingInvestmentTransaction()
            .then( ({ data }) => {
                state.pendingInvestmentTransaction = data;
                state.fetchingPendingTransactions = false;
            }, () => {
                state.fetchingPendingTransactions = false;
            });
    },

    ACTIVE_PORTFOLIO_INVESTMENTS(state) {
        state.fetchingPortfolioInvestments = true;

        dashboardApi.getActivePortfolioInvestment()
            .then( ({ data }) => {
                state.activePortfolioInvestment = data;
                state.fetchingPortfolioInvestments = false;
            }, () => {
                state.fetchingPortfolioInvestments = false;
            });
    },

    PENDING_PORTFOLIO_TRANSACTIONS(state) {
        state.fetchingPendingPortfolioTransactions = true;

        dashboardApi.getPendingPortfolioTransaction()
            .then( ({ data }) => {
                state.pendingPortfolioTransaction = data;
                state.fetchingPendingPortfolioTransactions = false;
            }, () => {
                state.fetchingPendingPortfolioTransactions = false;
            });
    }
};

export default {
    state,
    getters,
    mutations
}