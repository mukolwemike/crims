import * as dashboardApi from '../../../api/dashboardApi';

const state = {
    notifications : [],
    fetchingNotifications: false,
    fetchingLogs: false,
    logs: []
};

const getters = {
    fetchingNotifications: state => state.fetchingNotifications,
    fetchingLogs: state => state.fetchingLogs,
    notifications: state => state.notifications,
    logs: state => state.logs,
};

const mutations = {
    GET_NOTIFICATIONS() {

        state.fetchingNotifications = true;
        dashboardApi.getNotifications()
            .then( ({ data }) => {
                state.notifications = data.data;

                state.fetchingNotifications = false;
            }, () => {
                state.fetchingNotifications = false;
            });
    },

    GET_LOGS(state) {

        state.fetchingLogs = true;
        dashboardApi.getLogs()
            .then( ({ data }) => {
                state.fetchingLogs = false;
                
                state.logs = data.data;
            }, () => {
                state.fetchingLogs = false;
            });
    }
};

export default {
    state, getters, mutations
}