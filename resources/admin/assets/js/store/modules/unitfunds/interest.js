/**
 * Created by Yakov on 19/05/2018.
 */
import * as fundApi from '../../../api/unitfundApi';
import { Message } from 'element-ui';

const state = {
    interestModal: false,
    bulk_payment: false,
    submittingSchedules: false,
    submittingBulk: false,
    editInterestModal: false
};

const getters = {
    interestModal: state => state.interestModal,
    bulk_payment: state => state.bulk_payment,

    submittingSchedules: state => state.submittingSchedules,
    submittingBulk: state => state.submittingBulk,

    editInterestModal: state => state.editInterestModal,
};

const mutations = {

    INTEREST_PAYMENT_MODAL(state, value) {
        state.interestModal = value;
    },

    BULK_INTEREST_PAYMENT_MODAL(state, value) {
        state.bulk_payment = value;
    },

    SAVE_INTEREST_PAYMENT(state, form) {

        state.submittingSchedules = true;

        fundApi.saveInterestPayment(form)
            .then( ({data}) => {


                state.submittingSchedules = false;
                state.interestModal = false;

                if(data.status === 400) {
                    Message.warning({ message: data.message })
                } else {
                    Message.success({ message: 'Submitted for approval succesfully' });
                }

            }, () => {
                state.submittingSchedules = false;
            });
    },

    EDIT_INTEREST_PAYMENT(state, form) {

        state.submittingSchedules = true;

        fundApi.saveInterestPayment(form)
            .then( ({data}) => {

                state.submittingSchedules = false;
                state.editInterestModal = false;

                Message.success({ message: 'Submitted for approval succesfully' });

            }, () => {
                state.submittingSchedules = false;
            });
    },

    SEND_INTEREST_BULK_PAYMENT(state, form) {

        state.submittingBulk = true;

        fundApi.sendInterestBulkPayment(form)
            .then( ({data}) => {

                state.submittingBulk = false;
                state.bulk_payment = false;

                if(data.status === 400) {
                    Message.warning({ message: data.message })
                } else {
                    Message.success({ message: data.message })
                }

            }, () => {
                state.submittingBulk = false;
            });
    },

    EDIT_INTEREST_MODAL(state, value) {
        state.editInterestModal = value;
    }
};

export default {
    state,
    getters,
    mutations
}