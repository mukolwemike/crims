/**
 * Created by Yakov on 19/05/2018.
 */
import * as fundApi from '../../../api/unitfundApi';
import { Message } from 'element-ui';

const state = {
    linkToCompliance: false,
    complianceList: [],
    submittingCompliance: false
};

const getters = {
    linkToCompliance: state => state.linkToCompliance,
    complianceList: state => state.complianceList,
    submittingCompliance: state => state.submittingCompliance,
};

const mutations = {
    SUBMIT_SELECTED_COMPLIANCE(state, form) {

        state.submittingCompliance = true;

        fundApi.submitSelectedCompliance(form)
            .then( () => {

                state.submittingCompliance = false;

                Message.success({ message: 'Submitted succesfully for approval'});

                state.linkToCompliance = false;

            }, () => {

                state.submittingCompliance = false;

            });
    },

    GET_COMPLIANCE(state) {
        fundApi.getComplianceList()
            .then( ({ data }) => {
                state.complianceList = data;
            });
    },

    LINK_TO_COMPLIANCE(state, value) {
        state.linkToCompliance = value;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {

    }
}