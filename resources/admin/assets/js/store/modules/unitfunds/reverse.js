/**
 * Created by Yakov on 19/05/2018.
 */
import * as fundApi from '../../../api/unitfundApi';

import { Message } from 'element-ui';

const state = {
    selectedPurchase: {},
    selectedSale: {},
    selectedTransfer: {},

    reversalModal: false,
    saleModal: false,
    transferModal: false,

    reversalErrors: {},
    reversalSalesErrors: {},
    reversalTransferErrors: {},

    reversing: false,
};

const getters = {
    selectedPurchase: state => state.selectedPurchase,
    selectedSale: state => state.selectedSale,
    selectedTransfer: state => state.selectedTransfer,

    reversalModal: state => state.reversalModal,
    saleModal: state => state.saleModal,
    transferModal: state => state.transferModal,

    reversalErrors: state => state.reversalErrors,
    reversalSalesErrors: state => state.reversalSalesErrors,
    reversalTransferErrors: state => state.reversalTransferErrors,

    reversing: state => state.reversing,
};

const mutations = {
    REVERSE_PURCHASES(state, form) {

        state.reversing = true;
        fundApi.reversePurchase(form)
            .then(({ data })=> {
                state.reversing = false;
                if (data.status == 422) {
                    Message.warning({ message: 'Oops! there are errors in the form'});
                    state.reversalErrors = data.errors;
                } else {
                    state.reversalErrors = {};
                    state.reversalModal = false;

                    Message.success({ message: 'Purchase reversal saved successfully for approval' });
                }
            }, ()=> {
                state.reversing = false;
                Message.warning({ message: 'Oop! error occured, please try later' });
            });
    },

    REVERSE_SALE(state, form) {

        state.reversing = true;
        fundApi.reverseSale(form)
            .then(({ data })=> {
                state.reversing = false;
                if (data.status == 422) {
                    Message.warning({ message: 'Oops! there are errors in the form'});
                    state.reversalSalesErrors = data.errors;
                } else {
                    state.reversalSalesErrors = {};
                    state.saleModal = false;

                    Message.success({ message: 'Sale reversal saved successfully for approval' });
                }
            }, ()=> {
                state.reversing = false;
                Message.warning({ message: 'Oop! error occured, please try later' });
            });
    },

    REVERSE_TRANSFER(state, form) {

        state.reversing = true;
        fundApi.reverseTransfer(form)
            .then(({ data })=> {
                state.reversing = false;
                if (data.status == 422) {
                    Message.warning({ message: 'Oops! there are errors in the form'});
                    state.reversalTransferErrors = data.errors;
                } else {
                    state.reversalTransferErrors = {};
                    state.transferModal = false;

                    Message.success({ message: 'Transfer reversal saved successfully for approval' });
                }
            }, ()=> {
                state.reversalTransferErrors = {};
                state.reversing = false;
                state.transferModal = false;
                Message.warning({ message: 'Oop! error occured, please try later' });
            });
    },


    REVERSE_PURCHASE_MODAL(state, data) {
        state.selectedPurchase = data.purchase;
        state.reversalModal = data.value;
    },

    REVERSE_SALE_MODAL(state, data) {
        state.selectedSale = data.sale;
        state.saleModal = data.value;
    },

    REVERSE_TRANSFER_MODAL(state, data) {
        state.selectedTransfer = data.transfer;
        state.transferModal = data.value;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {

    }
    }