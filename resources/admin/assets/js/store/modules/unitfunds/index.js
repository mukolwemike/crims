/**
 * Created by Yakov on 19/05/2018.
 */
import Reversals from './reverse';
import Compliance from './compliance';
import Interest from './interest';

const state = {
    newFundModal: false
};

const getters = {
    newFundModal: state => state.newFundModal
};

const mutations = {

    CREATE_FUND_MODAL(state, value) {
        state.newFundModal = value;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {
        Reversals,
        Compliance,
        Interest
    }
}