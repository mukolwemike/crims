import * as clientApi from '../../../api/clientInvestmentApi';
import { Message } from 'element-ui';

const state = {
    fetchingDocumentTypes: false,
    documentTypes: [],
    kycUploaded: [],
    paymentDocument: {},
    paymentUploaded: false,
};

const getters = {
    fetchingDocumentTypes: state => state.fetchingDocumentTypes,
    documentTypes: state => state.documentTypes,
    kycUploaded: state => state.kycUploaded,
    paymentDocument: state => state.paymentDocument,
    paymentUploaded: state => state.paymentUploaded,
};

const mutations = {

    REMOVE_CLIENT_DOCUMENT(state, doc) {

        clientApi.deleteClientDocument(doc.id)
            .then( () => {
                Message.success({ message: 'Successfully removed' });
            }, () => {
                Message.warning({ message: 'Failed to remove the document' });
            });
    },

    REMOVE_KYC_FROM_LIST(state, doc) {
        let index = state.kycUploaded.indexOf(doc);

        if (index !== -1) {
            state.kycUploaded.splice(index, 1);
        }
    },

    REMOVE_PAYMENT_FROM_UPLOADED(state, doc) {
        delete (state.paymentDocument);
    },

    GET_KYC_DOCUMENT_TYPES(state, type) {
        
        state.fetchingDocumentTypes = true;

        clientApi.kycDocumentTypes(type)
            .then( ({ data }) => {

                state.documentTypes = data;
                state.fetchingDocumentTypes = false;

            }, () => {
                state.fetchingDocumentTypes = false;
            });
    },


    SET_UPLOADED_KYC(state, doc) {

        let index = state.kycUploaded.indexOf(doc);

        if (index !== -1) {
            state.kycUploaded.splice(index, 1);
        }

        state.kycUploaded.push(doc);
    },

    SET_SAVED_KYC_DOCUMENT(state, data) {
        state.kycUploaded = data;
    },

    SET_SAVED_PAYMENT_DOCUMENT(state, data) {
        state.paymentDocument = data;
        state.paymentUploaded = true;
    }
};

export default {
    state, getters, mutations
}


