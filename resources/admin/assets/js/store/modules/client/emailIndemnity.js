import * as clientApi from '../../../api/clientInvestmentApi';

const state = {
    emailIndemnities: []
};

const getters = {
    emailIndemnities: state => state.emailIndemnities,
};

const mutations = {
    SET_EMAIL_INDEMNITY(state, form) {
        state.emailIndemnities.push(form);
    },

    SET_SAVED_EMAIL_INDEMNITY(state, data) {
        state.emailIndemnities = data;
    },

    REMOVE_INDEMNITY(state, doc) {
        let index = state.emailIndemnities.indexOf(doc);

        if (index !== -1) {
            state.emailIndemnities.splice(index, 1);
        }
    }
};

export default {
    state, getters, mutations
}

