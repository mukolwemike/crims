import * as standingOrder from '../../../api/clientApi';
import { Message } from 'element-ui';

const state = {
    standingOrderModal: false,
    savingOrder: false,
    fetching: false,
    standingOrderTypes: [],
    projectHoldings: [],
    fetchingHoldings: false,
    orderErrors: [],
    standingOrders: []
};

const getters = {
    standingOrderModal: state => state.standingOrderModal,
    savingOrder: state => state.savingOrder,
    fetching: state => state.fetching,
    standingOrderTypes: state => state.standingOrderTypes,
    projectHoldings: state => state.projectHoldings,
    fetchingHoldings: state => state.fetchingHoldings,
    orderErrors: state => state.orderErrors,
    standingOrders: state => state.standingOrders,
};

const mutations = {

    GET_STANDING_ORDERS(state, id) {

        state.fetching = true;

        standingOrder.getStandingOrders(id)
            .then( ({ data }) => {
                state.fetching = false;

                state.standingOrders = data;

            }, () => {

                state.fetching = false;
            });
    },

    SUBMIT_STANDING_ORDER(state, form) {

        state.savingOrder = true;

        standingOrder.saveStandingOrder(form)
            .then( ({ data }) => {

                if(data.status == 422)
                {
                    state.orderErrors = data.errors;

                    Message.warning({ message: 'There are errors in the form, fill and proceed'});
                } else {

                    state.standingOrderModal = false;

                    Message.success({ message: 'Standing order save for approval'});
                }

                state.savingOrder = false;

            }, () => {
                state.savingOrder = false;

                Message.warning({ message: 'Error has occured, please try again later'});
            });
    },

    GET_PROJECT_HOLDINGS(state, id) {

        state.fetchingHoldings = true;

        standingOrder.getProjectHoldings(id)
            .then( ({ data }) => {

                state.projectHoldings = data;
                state.fetchingHoldings = false;

            }, () => {
                state.fetchingHoldings = false;
            });
    },

    ORDER_MODAL(state, value) {
        state.standingOrderModal = value;
    },

    GET_ORDER_TYPES(state) {
        state.fetching = true;

        standingOrder.getOrderTypes()
            .then( ({ data }) => {
                state.standingOrderTypes = data;
            });
    }
};

export default {
    state, getters, mutations
}