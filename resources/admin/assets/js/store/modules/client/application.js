import * as clientAppApi from '../../../api/clientInvestmentApi';
import * as shareAppApi from '../../../api/sharesApi';
import * as realEstateApi from '../../../api/realestateApi';
import { Message } from 'element-ui';
import store from '../../../store';

const state = {
    clientForm: {},
    subscriber: {},
    investment: {},

    clientErrors: [],
    subscribeErrors: [],
    riskyFormErrors:[],

    validatingClientInvestment: false,
    validatingShareholderAccDetails: false,
    savingSubscriber: false,

    investmentSection: false,
    subscriberSection: false,
    contactSection: false,
    jointSection: false,
    indemnitySection: false,
    kycSection: false,
    paymentSection: false,
    mandateSection: false,
    shareholderAccountSection: false,

    individualType: 1,
    clientAppType: 1,
    appFranchise: false,

    client_investment_id: null,
    register_transaction_id: null,
    reserve_unit_transaction_id: null,

    applicationStage: 'investment',

    settingClientForm: false,

    client_bank_id: null,
    client_bank_branch_id: null,

    existingClient: {},

    appComplete: true,
    riskyForm: {},
    nations:{},
    date_flagged: '',
    risksStatus: {},
    processType: '',
};

const getters = {
    clientForm: state => state.clientForm,

    clientErrors: state => state.clientErrors,
    subscribeErrors: state => state.subscribeErrors,

    validatingClientInvestment: state => state.validatingClientInvestment,
    validatingShareholderAccDetails: state => state.validatingShareholderAccDetails,
    savingSubscriber: state => state.savingSubscriber,

    investmentSection: state => state.investmentSection,
    subscriberSection: state => state.subscriberSection,
    jointSection: state => state.jointSection,
    contactSection: state => state.contactSection,
    indemnitySection: state => state.indemnitySection,
    kycSection: state => state.kycSection,
    paymentSection: state => state.paymentSection,
    mandateSection: state => state.mandateSection,
    shareholderAccountSection: state => state.shareholderAccountSection,

    individualType: state => state.individualType,
    clientAppType: state => state.clientAppType,
    appFranchise: state => state.appFranchise,

    client_investment_id: state => state.client_investment_id,
    applicationStage: state => state.applicationStage,

    settingClientForm: state => state.settingClientForm,

    client_bank_id: state => state.client_bank_id,
    client_bank_branch_id: state => state.client_bank_branch_id,
    existingClient: state => state.existingClient,

    appComplete: state => state.appComplete,
    riskyForm: state => state.riskyForm,
    riskyFormErrors: state => state.riskyFormErrors,
    nations: state => state.nations,
    date_flagged: state => state.date_flagged,
    risksStatus: state => state.risksStatus,

    processType: state => state.processType,
    register_transaction_id: state => state.register_transaction_id,
    reserve_unit_transaction_id: state => state.reserve_unit_transaction_id,
};

const mutations = {
    SET_APPLICATION_TYPE(state, form_type){
        state.processType = form_type;
    },

    SET_INIT_APPLICATION_STAGE(state, stage){
        state.applicationStage = stage;
    },

    SET_CLIENT_APPLICATION_FORM(state, id) {

        state.settingClientForm = true;

        clientAppApi.setClientApplicationForm(id)
            .then( ({ data }) => {

                state.client_investment_id = data.application.id;

                state.clientForm = data.application;

                state.individualType = data.application.individual;

                state.clientAppType = data.application.new_client;

                state.appFranchise = data.application.franchise;

                state.appComplete = data.application.complete;

                state.clientForm.product_category = data.application.unit_fund_id ? 'funds' : 'products';

                state.settingClientForm = false;

                state.client_bank_id = data.application.investor_bank;

                state.client_bank_branch_id = data.application.investor_bank_branch;

                state.existingClient = data.client;

                setOtherSections(data);

            }, () => {

                Message.warning({ message: 'Failed to set up application details for editing' });

                state.settingClientForm = false;
            });
    },

    VALIDATE_INVESTMENT_FORMS(state, form) {

        state.validatingClientInvestment = true;

        clientAppApi.validatingInvestment(form)
            .then( ({ data }) => {
                state.validatingClientInvestment = false;

                if(data.status === 422) {
                    Message.warning({ message: 'The form is incomplete, kindly complete and proceed' });

                    state.clientErrors = data.errors;
                }else{
                    Message.success({ message: 'Investment Details Successfully Added' });

                    if(!state.clientForm.id) {
                        state.investmentSection = true;
                        state.applicationStage = 'subscriber';
                    }
                }
            }, () => {
                state.validatingClientInvestment = false;
            });
    },

    VALIDATE_SHAREHOLDER_ACCOUNT_FORMS(state, form){
        state.validatingShareholderAccDetails = true;

        clientAppApi.validatingShareholder(form)
            .then( ({ data }) => {
                state.validatingShareholderAccDetails = false;

                if(data.status === 422) {
                    Message.warning({ message: 'The form is incomplete, kindly complete and proceed' });

                    state.clientErrors = data.errors;
                }else{
                    Message.success({ message: 'Shareholder account details Successfully Added' });

                    if(!state.clientForm.id) {
                        state.shareholderAccountSection = true;
                        state.applicationStage = 'subscriber';
                    }
                }
            }, () => {
                state.validatingShareholderAccDetails = false;
            });
    },

    SAVE_SUBSCRIBER(state, form) {

        state.savingSubscriber = true;

        clientAppApi.saveSubscriber(form)
            .then( ({ data }) => {
                state.savingSubscriber = false;

                if(data.status === 422) {
                    Message.warning({ message: 'The form is incomplete, Please check missing fields and proceed'});

                    state.subscribeErrors = data.errors;

                }else if(data.status === 'risk'){
                    Message.warning({ message: 'Client Details match a risky client, please fill the reason for application'});

                    state.subscribeErrors = data.warning;

                }else if(data.status === 'duplicate'){
                    Message.warning({ message: 'The details provided belong to another client! Please apply as a Duplicate'});

                    state.subscribeErrors = data.warning;
                }else {
                    Message.success({ message: 'Form is successfully saved'});

                    state.client_investment_id = data.application;

                    if(!state.clientForm.id) {
                        state.applicationStage = 'contact';

                        state.subscriberSection = true;
                    }
                }
            }, () => {
                state.savingSubscriber = false;
            });
    },

    SAVE_SHAREHOLDER(state, form) {

        state.savingSubscriber = true;

        shareAppApi.saveShareHolder(form)
            .then( ({ data }) => {
                state.savingSubscriber = false;

                if(data.status === 422) {
                    Message.warning({ message: 'The form is incomplete, Please check missing fields and proceed'});

                    state.subscribeErrors = data.errors;

                    console.log(state.subscribeErrors)
                }else if(data.status === 'risk'){
                    Message.warning({ message: 'Client Details match a risky client, please fill the reason for application'});

                    state.subscribeErrors = data.warning;

                }else if(data.status === 'duplicate'){
                    Message.warning({ message: 'The details provided belong to another client! Please apply as a Duplicate'});

                    state.subscribeErrors = data.warning;
                }else {
                    Message.success({ message: 'Shareholder details have been saved for approval'});

                    state.register_transaction_id = data.transactionID;

                    if(!state.clientForm.id) {
                        state.applicationStage = 'contact';
                        state.subscriberSection = true;
                    }
                }
            }, () => {
                state.savingSubscriber = false;
            });
    },

    SAVE_RESERVE_UNIT_CLIENT(state, form) {
        state.savingSubscriber = true;

        realEstateApi.saveReserveUnitClient(form)
            .then( ({ data }) => {
                state.savingSubscriber = false;

                if(data.status === 422) {
                    Message.warning({ message: 'The form is incomplete, Please check missing fields and proceed'});

                    state.subscribeErrors = data.errors;

                    console.log(state.subscribeErrors)
                }else if(data.status === 'risk'){
                    Message.warning({ message: 'Client Details match a risky client, please fill the reason for application'});

                    state.subscribeErrors = data.warning;

                }else if(data.status === 'duplicate'){
                    Message.warning({ message: 'The details provided belong to another client! Please apply as a Duplicate'});

                    state.subscribeErrors = data.warning;
                }else {
                    Message.success({ message: 'The unit reservation has been saved for approval'});

                    state.reserve_unit_transaction_id = data.transactionID;

                    if(!state.clientForm.id) {
                        state.applicationStage = 'contact';
                        state.subscriberSection = true;
                    }
                }
            }, () => {
                state.savingSubscriber = false;
            });
    },

    SET_CLIENT_TYPES(state, clientType) {
        state.individualType = clientType;
    },

    SET_APPLICATION_TYPES(state, appType) {
        state.clientAppType = appType;
    },

    GET_ALL_COUNTRIES(state) {
        clientAppApi.getCountries()
            .then(({data})=>{
                state.nations = data;
            });
    },

    GET_ALL_RISK_STATUS(state) {
      clientAppApi.getRiskStatus()
          .then(({data}) => {
              state.risksStatus = data;
          });
    },

    SAVE_RISKY_CLIENT(state, form) {
        state.savingSubscriber = true;

        clientAppApi.saveRiskyClient(form)
            .then(({data})=>{
                state.savingSubscriber = false;

                if(data.status === 422) {
                    Message.warning({ message: 'The form is incomplete, Please check missing fields and submit'});

                    state.riskyFormErrors = data.errors;

                }else {
                    Message.success({ message: 'Risky Client Details are successfully saved'});

                    state.riskyForm = {}

                    window.location.href = '/dashboard/clients/risky'
                }
            }, () => {
                state.savingSubscriber = false;
            });
    },

    SET_RISKY_FORM(state, risk_id) {
      clientAppApi.getRiskyClient(risk_id)
          .then(({data})=>{
              state.riskyForm = data;

              state.date_flagged = data.date_flagged;
          });
    },

    SET_APPLICATION_STAGE(state, form) {

        state.applicationStage = form.stage;

        if(!state.clientForm.id) {

            if (form.section === 'contactSection') {
                state.contactSection = true;
            } else if (form.section === 'jointSection') {
                state.jointSection = true;
            } else if (form.section === 'kycSection') {
                state.kycSection = true;
            } else if (form.section === 'paymentSection') {
                state.paymentSection = true;
            } else if (form.section === 'indemnitySection') {
                state.indemnitySection = true;
            } else if (form.section === 'mandateSection') {
                state.mandateSection = true;
            }
        }
    }
};

const actions = {
    save_redirect({commit}, form){
        if (form.type == 'register_shareholder') {
            commit('SAVE_SHAREHOLDER', form)
        } else if (form.type == 'client_investment') {
            commit('SAVE_SUBSCRIBER', form)
        } else if(form.type == 'reserve_unit') {
            commit('SAVE_RESERVE_UNIT_CLIENT', form)
        }
    }
};


function setOtherSections(data) {
    store.commit('SET_SAVED_CONTACT_PERSONS', data.contactPersons);
    store.commit('SET_SAVED_JOINT_HOLDERS', data.jointHolders);
    store.commit('SET_SAVED_KYC_DOCUMENT', data.kycDocuments);
    store.commit('SET_SAVED_PAYMENT_DOCUMENT', data.paymentDocuments[0]);
    store.commit('SET_SAVED_PAYMENT_DOCUMENT', data.paymentDocuments[0]);
    store.commit('SET_SAVED_EMAIL_INDEMNITY', data.emailIndemnity);
    store.commit('SET_SAVED_SIGNING_MANADATE', data.clientSignature);
}

export default {
    state, getters, mutations, actions
}