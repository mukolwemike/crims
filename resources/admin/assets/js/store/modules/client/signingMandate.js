import * as clientApi from '../../../api/clientInvestmentApi';

const state = {
    mandateDocuments: [],
    mandateModal: false
};

const getters = {
    mandateDocuments: state => state.mandateDocuments,
    mandateModal: state => state.mandateModal,
};

const mutations = {
    SET_SIGNING_MANDATE(state, form) {
        state.mandateDocuments.push(form);
    },

    SET_SAVED_SIGNING_MANADATE(state, data) {
        state.mandateDocuments = data;
    },

    REMOVE_SIGNING_MANDATE(state, doc) {
        let index = state.mandateDocuments.indexOf(doc);

        if (index !== -1) {
            state.mandateDocuments.splice(index, 1);
        }
    },

    SIGNING_MANDATE_MODAL(state, value) {
        state.mandateModal = value;
    }
};

export default {
    state, getters, mutations
}

