import Orders from './orders.js';
import ClientApplication from './application';
import ContactPerson from './contact-person';
import JointHolders from './joint-holders';
import KYC from './kyc';
import EmailIndemnity from './emailIndemnity';
import SigningMandate from './signingMandate';
import TaxExemption from './taxExemption';
import CashFlow from './cash-flow';

const state = {
    selectedClient: null,
};

const getters = {
    selectedClient: state => state.selectedClient
};

const mutations = {
    SET_SELECTED_CLIENT(state, client) {
        state.selectedClient = client;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {
        Orders,
        ClientApplication,
        ContactPerson,
        JointHolders,
        KYC,
        EmailIndemnity,
        SigningMandate,
        TaxExemption,
        CashFlow
    }
}