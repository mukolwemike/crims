import * as clientApi from '../../../api/clientInvestmentApi';

const state = {
    formData: {},
};

const getters = {
    formData: state => state.formData,
};

const mutations = {
    GET_TAX_EXEMPTION(state, id) {
        console.log('here', id);
        clientApi.getTaxExemptions(id)
            .then( ({ data }) => {
                state.formData = data;
            }, () => {

            });
    },
};

export default {
    state, getters, mutations
}

