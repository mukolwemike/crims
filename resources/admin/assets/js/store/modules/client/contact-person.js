import * as submit from '../../../api/clientInvestmentApi';
import { Message } from 'element-ui';
import store from '../../../store';

const state = {
    contactsErrors: [],
    submittingContactPerson: false,
    contactModal: false,
    contactPersons: [],
    contactForm: {},
    savingContactPersons: false,
    fetchingContactPersons: false,
    removing: false,
};

const getters = {
    contactModal: state => state.contactModal,
    contactsErrors: state => state.contactsErrors,
    submittingContactPerson: state => state.submittingContactPerson,
    contactPersons: state => state.contactPersons,
    contactForm: state => state.contactForm,
    savingContactPersons: state => state.savingContactPersons,
    fetchingContactPersons: state => state.fetchingContactPersons,
    removing: state => state.removing,
};

const mutations = {
    SET_SAVED_CONTACT_PERSONS(state, data) {
        state.contactPersons = data;
    },

    ADD_CONTACT_PERSON(state, form) {

        state.submittingContactPerson = true;

        submit.validateContactPerson(form)
            .then(({ data }) => {

                if (data.status === 422) {

                    state.contactsErrors = data.errors;
                    Message.warning({ message: 'There are errors in the form, kindly check and submit again'});

                } else if (data.status === 202) {

                    state.contactPersons.push(form);

                    Message.success({ message: 'Contact Person added to list'});
                    state.contactModal = false;
                    state.contactForm = {};
                }
                state.submittingContactPerson = false;

            }, () =>{

                state.submittingContactPerson = false;
                Message.error({ message: 'Error occured, please try again later'});

                state.contactForm = {};
            });
    },

    SAVE_CONTACT_PERSON(state, form) {

        state.savingContactPersons = true;

        submit.saveContactPerson(form)
            .then( ({ data }) => {
                Message.success({ message: 'Contact Person has been saved to the application'});

                state.savingContactPersons = false;

                setApplicationStage('joint', 'contactSection');

            }, () => {
                state.savingContactPersons = false;
            });

    },

    REMOVE_CONTACT_PERSON(state, person) {

        if(person.id) {
            state.removing = true;

            submit.removeContactPerson(person.id)
                .then( () => {
                    state.removing = true;

                    let index = state.contactPersons.indexOf(person);

                    if (index !== -1) {
                        state.contactPersons.splice(index, 1);
                    }
                }, () => { state.removing = false; });
        } else {

            let index = state.contactPersons.indexOf(person);

            if (index !== -1) {
                state.contactPersons.splice(index, 1);
            }
        }

    },

    CONTACT_PERSON_MODAL(state, value) {
        state.contactModal = value;
        state.contactForm = {};
    }
};

const actions = {

};

function setApplicationStage(stage, contactSection) {
    store.commit('SET_APPLICATION_STAGE', {
        stage: stage,
        section: contactSection
    });
}

export default {
    state,
    getters,
    mutations,
    actions,
    modules: {

    }
}
