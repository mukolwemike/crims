import * as Instruction from "../../../api/clientInvestmentApi";
import {Message} from "element-ui";
import store from '../../../store';

const state = {
    jointHolderFormErrors: [],
    submitJointHolderForm: [],
    clientJointHolderForm: [],
    clientJointHolderModal: false,
    submittingJointHolder: false,
    clientHoldersForm: {},
    savingClientJointHolders: false,
    removingHolder: false,
    holderType: 1,
};

const getters = {
    jointHolderFormErrors: state => state.jointHolderFormErrors,
    submitJointHolderForm: state => state.submitJointHolderForm,
    clientJointHolderForm: state => state.clientJointHolderForm,
    clientJointHolderModal: state => state.clientJointHolderModal,
    submittingJointHolder: state => state.submittingJointHolder,
    clientHoldersForm: state => state.clientHoldersForm,
    savingClientJointHolders: state => state.savingClientJointHolders,
    removingHolder: state => state.removingHolder,
    holderType: state => state.holderType,
};

const mutations = {

    SET_SAVED_JOINT_HOLDERS(state, data) {
        state.clientJointHolderForm = data;
    },

    SET_JOINT_TYPE(state, type) {
        state.holderType = type;
    },


    SAVE_JOINT_HOLDERS(state, form) {

        state.savingClientJointHolders = true;

        Instruction.saveJointHolders(form)
            .then(({data}) => {
                if (data.status === 202) {
                    Message.success({message: 'Joint holder added successfully'});
                }

                state.savingClientJointHolders = false;

                setApplicationStage('kyc', 'jointSection');

            }, () => {
                state.savingClientJointHolders = false;
            });

    },

    SUBMIT_JOINT_HOLDER_FORM(state, form) {

        state.submittingJointHolder = true;

        Instruction.validateJointHolder(form)
            .then(({data}) => {
                Message.success({message: 'Joint Holder has been saved to the application'});

                state.submittingJointHolder = false;

                if (data.status === 422) {

                    state.clientJointHolderModal = true;
                    state.jointHolderFormErrors = data.errors;

                    Message.warning({message: 'Please fill all required fields.'});

                    return;
                }else if(data.status === 423) {
                    state.clientJointHolderModal = true;
                    state.jointHolderFormErrors = data.errors;

                    Message.warning({message: 'Duplicate client details flagged. Apply as duplicate'});

                    return;
                }

                if (state.holderType == 2) {
                    Instruction.getExistingHolder(form.client_id)
                        .then(({data}) => {
                            state.clientJointHolderForm.push(data);
                        });

                } else {
                    state.clientJointHolderForm.push(form);
                    state.clientHoldersForm = {};
                }

                state.clientJointHolderModal = false;

                Message.success({message: 'You have successfully added a joint holder'});

            }, () => {
                state.submittingJointHolder = false;
                state.jointHolderFormErrors = [];
                state.clientHoldersForm = {};
            });
    },

    CLIENT_JOINT_HOLDER_MODAL(state, value) {
        state.clientJointHolderModal = value;
        state.clientHoldersForm = {};

    },

    REMOVE_JOINT_HOLDER(state, holder) {
        if (holder.id) {

            state.removingHolder = true;

            Instruction.removeJointHolder(holder.id)
                .then(() => {
                    state.removingHolder = false;

                    let index = state.clientJointHolderForm.indexOf(holder);

                    if (index !== -1) {
                        state.clientJointHolderForm.splice(index, 1);
                    }
                }, () => {
                    state.removingHolder = false;
                });
        } else {
            let index = state.clientJointHolderForm.indexOf(holder);

            if (index !== -1) {
                state.clientJointHolderForm.splice(index, 1);
            }
        }

        Message.success({message: 'Joint holder removed successfully'});
    },
};

function setApplicationStage(stage, contactSection) {
    store.commit('SET_APPLICATION_STAGE', {
        stage: stage,
        section: contactSection
    });
}

export default {
    state, getters, mutations
}


