import * as clientApi from '../../../api/clientInvestmentApi';

const state = {
    allCustodialAccounts: [],
    fetchingAllAccounts: false,
    fetchingShortAccounts: false,
};

const getters = {
    allCustodialAccounts: state => state.allCustodialAccounts,
    fetchingAllAccounts: state => state.fetchingAllAccounts,
    fetchingShortAccounts: state => state.fetchingShortAccounts,
};

const mutations = {
    GET_ALL_CUSTODIAL_ACCOUNTS(state) {
        state.fetchingAllAccounts = true;

        clientApi.getShortCustodialAccounts()
            .then(({data}) => {
                state.fetchingAllAccounts = false;
                state.allCustodialAccounts = data.accounts
            }, () => {
                state.fetchingAllAccounts = false;
            });
    },
};

export default {
    state, getters, mutations
}

