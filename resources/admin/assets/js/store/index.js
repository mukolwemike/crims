/**
 * Created by yeric on 10/6/17.
 */
import Vue from 'vue';
import Vuex from 'vuex';

import * as type from './mutation-types';


/**
 * Import all other modules store
 */
import Resources from './modules/resources';
import Applications from './modules/applications';
import Calculator from './modules/fundcalculator';
import Portfolio from './modules/portfolio';
import FundFees from './modules/fundfees';
import Investments from './modules/investment';
import Orders from './modules/orders';
import Client from './modules/client';
import Components from './modules/components';
import UnitFund from './modules/unitfunds';
import UnitFunds from './modules/funds';
import Dashboard from './modules/dashboard';
import FinancialAdvisor from './modules/financialadvisor';

Vue.use(Vuex);

const state = {

};

const getters = {

};

const mutations = {

};

const actions = {

};

const store = new Vuex.Store({
        state,
        getters,
        mutations,
        actions,
    modules: {
        Resources,
        Applications,
        Calculator,
        Portfolio,
        FundFees,
        Investments,
        Orders,
        Client,
        Components,
        UnitFund,
        FinancialAdvisor,
        UnitFunds,
        Dashboard
    }
});

export default store;