import { Message } from 'element-ui';
import { mapGetters } from 'vuex';

const controller = {
    data() {
        return  {
            signature: {
                signature_person: 'new'
            },

            url: '/api/client/signature',

            uploading: false
        }
    },

    computed: {
        ...mapGetters({
            defaultHeaders: 'defaultHeaders',
            mandateModal: 'mandateModal'
        }),

        attachment() {
            this.signature.client_id = this.client.id;
            return this.signature;
        }
    },

    methods: {

        submit() {

            if(!this.validateForm()) {
                Message.warning({message: 'Name is required'});
                return;
            }

            if (!this.$refs.upload.uploadFiles[0]) {
                Message.warning({message: 'Upload the form and proceed'});
                return;
            }

            this.$refs.upload.submit();
        },

        validateForm() {

            console.log(this.signature.name);

            if(this.signature.signature_person === 'new' && !this.signature.name) {
                return false;
            }

            return true;
        },

        create() {
            this.$store.commit('SIGNING_MANDATE_MODAL', true);
        },

        closeModal() {
            this.$store.commit('SIGNING_MANDATE_MODAL', false);
        },

        errorOnUpload() {
            Message.warning({ message: 'Failed to upload client signature, please try again'});
            this.uploading = false;
        },

        showUploadSuccess() {
            this.uploading = false;
            this.closeModal();
            Message.success({ message: 'Client signature added for approval'});
            this.uploading = false;
            // location.reload();
        }
    }
};

export default controller;