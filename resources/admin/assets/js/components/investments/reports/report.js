import { mapGetters } from 'vuex';
import { Message } from 'element-ui';

const ReportController =  {
    data() {
        return {
            reportModal: false,

            models: [
                {
                    name: 'ClientInvestment',
                    value: 'App\\Cytonn\\Models\\ClientInvestment'
                },
                {
                    name: 'ClientInvestmentWithdrawal',
                    value: 'Cytonn\\Models\\ClientInvestmentWithdraw'
                }
            ],

            formats: [
                {
                    name: 'Currency',
                    value: 'currency'
                },
                {
                    name: 'Date',
                    value: 'date'
                }
            ],

            filterOperators: [
                {
                    name: '=',
                    value: '='
                },

                {
                    name: '!=',
                    value: '!='
                },

                {
                    name: '>',
                    value: '>'
                },

                {
                    name: '<',
                    value: '<'
                },

                {
                    name: '>=',
                    value: '>='
                },

                {
                    name: '<=',
                    value: '<='
                }
            ],

            filterBoleans: [
                {
                    name: 'AND',
                    value: 'and'
                },

                {
                    name: 'OR',
                    value: 'or'
                }
            ],

            selectedColumn: {},

            filterForm: {
                type_id: 1,
                boolean: 'and',
                value: []
            },

            filterType: 'where',

            filter_value: '',

            values: [],

            argumentForm: {}
        }
    },

    watch: {
        "filterForm.type_id": {
            handler: function () {

                let vm = this;

                let index = this.reportFilterTypes.find( function (type) {
                    return type.id === vm.filterForm.type_id;
                });

                this.filterType = index ? index.slug : '';

                console.log(this.filterType, "this is he filter type");
            }
        },

        "reportForm.model_name": {
            handler: function () {
                this.$store.commit('FETCH_MODEL_RELATIONS', {
                    model_name: this.reportForm.model_name
                });
            }
        }
    },

    computed: {
        ...mapGetters({
            reportForm: 'reportForm',
            selectedColumns: 'selectedColumns',
            created_report: 'created_report',
            reportFilters: 'reportFilters',
            newReport: 'newReport',
            reportFilterTypes: 'reportFilterTypes',
            savedColumns: 'savedColumns',
            reportArguments: 'reportArguments',
        })
    },

    methods: {
        addDesc: function () {
            this.$store.commit('SAVE_REPORT', this.reportForm);
        },

        saveReportColumns: function () {
            this.$store.commit('SAVE_REPORT_COLUMNS', {
                columns: this.selectedColumns,
                report_id: this.created_report.id
            });
        },

        saveReportFilters: function () {
            this.$store.commit('SAVE_REPORT_FILTERS', {
                filters: this.reportFilters,
                report_id: this.created_report.id
            });
        },

        saveReportArguments: function () {
            this.$store.commit('SAVE_REPORT_ARGUMENT', {
                arguments: this.reportArguments,
                report_id: this.created_report.id
            });
        },

        submit: function () {

            this.reportForm.columns = this.selectedColumns;

            this.$store.commit('SAVE_REPORT', this.reportForm);
        },

        create: function () {
            this.reportModal = !this.reportModal;
        },

        addColumn: function () {
            if(this.selectedColumn.name) {
                this.$store.commit('ADD_COLUMN', this.selectedColumn);
                this.selectedColumn = {};

                return;
            }

            Message.warning({ message: 'Select column and proceed'});
        },

        addFilter: function () {

            if(this.filterType === 'where') {
                this.filterForm.value.push(this.filter_value);
            } else {
                this.filterForm.value = this.values;
            }

            this.$store.commit('ADD_FILTER', this.filterForm);

            this.filterForm = {};

            this.filterForm = {
                type_id: 1,
                boolean: '',
                value: []
            };

            this.values = [];
        },

        addArguments: function () {
            this.$store.commit('ADD_ARGUMENT', this.argumentForm);
            this.argumentForm = {};
        },

        next() {
            if(this.newReport.reportDesc) {
                this.$store.commit('GO_TO_COLUMNS', true);
            }

            else if(this.newReport.reportCols) {
                this.$store.commit('GO_TO_ARGUMENTS', true);
            }

            else if(this.newReport.reportArgument) {
                this.$store.commit('GO_TO_FILTERS', true);
            }
        },

        back() {
            if(this.newReport.reportFilter) {
                this.$store.commit('GO_TO_ARGUMENTS', true);
            }

            else if(this.newReport.reportArgument) {
                this.$store.commit('GO_TO_COLUMNS', true);
            }

            else if(this.newReport.reportCols) {
                this.$store.commit('NEW_REPORT', true);
            }
        },

        removeFilter(form) {
            this.$store.commit('REMOVE_FILTER', form);
        },

        addValue() {
            this.values.push(this.filter_value);
            this.filter_value = '';
        }
    }
};

export default ReportController;