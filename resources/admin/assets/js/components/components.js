import Vue from 'vue';

Vue.component('search-bar', require('./vuetable/searchbar.vue'));

Vue.component('toogle-menu', require('./ui/toggle_menu.vue'));
Vue.component('main-body', require('./layout/main_page.vue'));
Vue.component('sidebar', require('../components/ui/sidebar.vue'));
Vue.component('breadCrumb', require('../components/ui/breadcrumb'));

Vue.component('flash', require('../components/Flash.vue'));

Vue.component('investmentAnalytics', require('../components/investments/summary/analytics.vue'));

Vue.component('unit-fund-commissions', require('../crims/Unitization/unitfundcommissions/index.vue'));

Vue.component('unit-fund-summaries', require('../crims/Unitization/unitfundsummaries/index.vue'));

Vue.component('unit-fund-holders', require('../crims/Unitization/unitfundholders/index.vue'));

Vue.component('custodial-transactions', require('../components/portfolio/custodials/custodial-transactions'));
/**
 * Reports
 */
Vue.component('unit-fund-reports', require('../crims/Unitization/unitfundreports/index.vue'));
Vue.component('new-custom-report', require('../components/investments/reports/index'));
Vue.component('custom-report-table', require('../crims/Investments/reports/custom/index'));
Vue.component('custom-report-show', require('../crims/Investments/reports/custom/show'));

Vue.component('asset-classes', require('../crims/Portfolio/assetclasses/index.vue'));

Vue.component('portfolio-securities', require('../crims/Portfolio/securities/securities.vue'));
Vue.component('portfolio-securities-details', require('../crims/Portfolio/securities/show.vue'));

Vue.component('portfolio-bonds', require('../crims/Portfolio/bonds/index.vue'));

Vue.component('investment-topup-instruction', require('../crims/Investments/instructions/topup/create.vue'));
Vue.component('investment-topup-instruction-edit', require('../crims/Investments/instructions/topup/edit.vue'));

Vue.component('create-purchase-units-instruction', require('../crims/Investments/instructions/unitization/create-purchase.vue'));

Vue.component('client-withdrawal', require('../crims/Investments/instructions/withrawal/create.vue'));
Vue.component('client-withdrawal-edit', require('../crims/Investments/instructions/withrawal/edit.vue'));

Vue.component('investment-rollover-instruction', require('../crims/Investments/instructions/rollover/create.vue'));
Vue.component('investment-rollover-instruction-edit', require('../crims/Investments/instructions/rollover/edit.vue'));
Vue.component('investment-orders', require('../crims/Investments/instructions/orders/orders.vue'));


Vue.component('search-client', require('./investments/client/search-client.vue'));
Vue.component('search-financial-advisor', require('./investments/fa/fa-search'));
Vue.component('component-test', require('./investments/client/test.vue'));
Vue.component('add-joint-holder', require('./investments/client/add-joint-holder.vue'));

Vue.component('deposit-holding', require('../crims/Portfolio/deposits/show.vue'));
Vue.component('invest-fund', require('../crims/Unitization/investment/invest.vue'));
Vue.component('sale-fund', require('../crims/Unitization/investment/sale.vue'));
Vue.component('edit-invest-fund', require('../crims/Unitization/investment/edit'));

/**
 * Order management
 */

Vue.component('portfolio-orders', require('../crims/Portfolio/orders/index'));
Vue.component('portfolio-order-show', require('../crims/Portfolio/orders/show'));


/**
 * Fund management
 */
Vue.component('bench-marks-index', require('../crims/Portfolio/compliance/benchmarks/index'));
Vue.component('view-compliance', require('../crims/Portfolio/compliance/benchmarks/show'));

Vue.component('liquidity-index', require('../crims/Portfolio/compliance/liquidity/index'));
Vue.component('no-go-zone-index', require('../crims/Portfolio/compliance/nogozone/index'));

Vue.component('standing-order', require('../crims/Client/standing-order'));

//dashboard
Vue.component('salutation', require('../components/dashboard/partials/salutation.vue'));
Vue.component('dashboard-investment-analytics', require('../components/dashboard/partials/investment_analytics.vue'));
Vue.component('account-summary', require('../components/dashboard/partials/account_summary.vue'));
Vue.component('fund-valuation', require('../components/dashboard/partials/fund-valuation.vue'));
Vue.component('re_chart', require('../components/dashboard/partials/re_chart.vue'));
Vue.component('notifications-modal', require('../components/dashboard/partials/notifications'));
Vue.component('activity-log', require('../components/dashboard/partials/activity-log'));


/**
 * Client application
 */
Vue.component('client-application', require('../crims/Client/application/index'));
Vue.component('signing-mandate', require('../components/investments/client/signingmandate/index'));
Vue.component('signing-mandate-edit', require('../components/investments/client/signingmandate/edit'));


//Portfolio Fund Summaries
Vue.component('portfolio-fund-summary', require('../crims/Portfolio/funds/summary/summary'));
Vue.component('portfolio-fund-pricing-summary', require('../crims/Portfolio/funds/pricing/index'));

Vue.component('loan-amortization', require('../crims/Portfolio/deposits/actions/amortization'));

Vue.component('deposit-holdings-interest-schedules', require('../crims/Portfolio/deposits/interest_schedules'));


Vue.component('crims-paginator', require('./pagination/paginate'));


// select options
Vue.component('commission-select', require('../components/ui/partials/commision-select'));


/**
 * Menus
 */
Vue.component('menu-card', require('../components/menus/menu_card'));
Vue.component('summary-card', require('../components/menus/summary_card'));
Vue.component('menu-card-details', require('../components/menus/menu_detail_card'));


Vue.component('settle-share-purchases', require('../crims/Shares/sales/settle'));

Vue.component('withholding-tax-index', require('../crims/Portfolio/custodials/tax/index'));
Vue.component('withholding-tax-list', require('../components/investments/tax/withholding'));

// risky clients
Vue.component('add-risky-client', require('../crims/Client/risky/risky-client-create'));
Vue.component('taxable-modal', require('../crims/Client/taxable'));


// risky clients
Vue.component('add-risky-client', require('../crims/Client/risky/risky-client-create'));
Vue.component('taxable-modal', require('../crims/Client/taxable'));

// transferring mismatch
Vue.component('transfer-mismatch-cash', require('../crims/Client/transfer-mismatch'));

//Suspense Transactions
Vue.component('suspense-transactions', require('../crims/Portfolio/custodials/suspense_transactions'));
Vue.component('suspense-transaction-bounce-cheque', require('../crims/Portfolio/custodials/suspense_transactions/bounce_cheque'));
Vue.component('suspense-transaction-add-inflow', require('../crims/Portfolio/custodials/suspense_transactions/add_inflow'));
Vue.component('suspense-transaction-match-custodial-transaction', require('../crims/Portfolio/custodials/suspense_transactions/match_custodial_transaction'));
Vue.component('suspense-transaction-match-suspense-transaction', require('../crims/Portfolio/custodials/suspense_transactions/match_suspense_transaction'));
Vue.component('search-custodial-transaction', require('./portfolio/custodials/search-custodial-transaction'));
Vue.component('search-suspense-transaction', require('./portfolio/custodials/search-suspense-transaction'));


Vue.component('approval-uploads', require('../crims/Client/approval-uploads'));

Vue.component('client-details-validation', require('../crims/Client/compliance/client-details-validation'));

//Investment Payment Schedules
Vue.component('investment-payment-schedules', require('../crims/Investments/investment_payment_schedules/index'));
Vue.component('all-investment-payment-schedules', require('../crims/Investments/investment_payment_schedules/listing'));

Vue.component('link-fund-account', require('../components/portfolio/custodials/unitfund/link-fund-account'));

// Utility Billing Module
Vue.component('process-utility', require('../crims/Billing/details.vue'));

/**
 * Clients Loyalty
 **/
Vue.component('clients-loyalty', require('../crims/Client/loyalty/index'));
Vue.component('clients-loyalty-values', require('../crims/Client/loyalty/loyalty-value'));
Vue.component('clients-loyalty-vouchers', require('../crims/Client/loyalty/loyalty-vouchers'));
Vue.component('clients-loyalty-redeem', require('../crims/Client/loyalty/loyalty-redeem'));

/*
* Investments Dashboard
 */
Vue.component('dashboard-analysis-cards-section', require('../crims/Investments/dashboard/analysis_cards_section'));
