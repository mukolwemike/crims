import { mapGetters } from 'vuex';
import { Message, Notification } from 'element-ui';
import axios from 'axios';

export const hasPermission = function (to, from, next) {

    let permission = to.meta.name;
    
    if (to.meta.permission != undefined) {
        axios.get('/api/user/permissions/' + to.meta.permission)
            .then(({data}) => {

                if (to.matched.some(record => record.meta.requirePermission)) {
                    if (!data.status) {
                        next({
                            path: from.fullPath,
                        });

                        Message.warning({message: `You do not have permission to ${permission}`});

                    } else {
                        next()
                    }
                } else {
                    next()
                }

            }, () => {

            });
    }
    else {
        next();
    }
};