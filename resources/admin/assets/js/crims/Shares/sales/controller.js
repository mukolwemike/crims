import moment from 'moment';
import axios from 'axios';
import { Message } from 'element-ui';

const SettleController = {
    data() {
        return {
            selectedPurchaseOrders: [],
            date: null,
            form: {},
            unmatchForm: {},
            submitting: false,
            unmatching: false
        }
    },

    watch: {
        date: {
            handler: function () {
                this.form.settlement_date = moment(this.date).format('YYYY-MM-DD');
            }
        }
    },

    methods: {
        submit: function (order) {

            if(!this.date) {
                Message.warning({ message: 'Select settlement date and proceed'});
                return;
            }

            this.submitting = true;

            this.form.share_purchase_ids = this.getIds();

            this.form.sales_order_id = order;

            let url = '/api/shares/sales/purchase-settlement';

            axios.post(url, this.form).then( ( ) => {

                Message.success({ message: 'Share Purchases has been saved for approval.' });

                this.submitting = false;

                location.reload();

            }, () => {

                this.submitting = false;

                Message.warning({ message: 'Failed to submit the share purchases for approval.' });
            });
        },

        reverse: function (order) {
            this.unmatching = true;

            this.unmatchForm.share_purchase_ids = this.getIds();

            this.unmatchForm.sales_order_id = order;

            axios.post('/api/shares/sales/purchase-unmatch', this.unmatchForm).then( ( ) => {

                Message.success({ message: 'Share Purchases Unmatching has been saved for approval.' });

                this.unmatching = false;

                location.reload();

            }, () => {

                this.unmatching = false;

                Message.warning({ message: 'Failed to submit the share purchases unmatching for approval.' });
            });
        },

        getIds: function () {
            let purchaseIds = [];

            this.selectedPurchaseOrders.forEach( function ($order) {
                purchaseIds.push($order.id);
            });

            return purchaseIds;
        },

        handleSelectionChange(val) {
            this.selectedPurchaseOrders = val;
        },

        sendReminder: function () {

            let ids = this.getIds();

            this.$confirm('Are you sure to send payment reminders?')
                .then(_ => {

                    if(ids.length > 0) {
                        axios.post('/api/shares/purchase-orders/send-payment-reminders', ids)
                            .then( () => {
                                Message.success({ message: 'Payment reminders will be sent shortly'});
                            });
                    } else {
                        Message.error({ message: 'No purchase selected' });
                    }

                })
                .catch(_ => {});
        }
    }
};

export default  SettleController;