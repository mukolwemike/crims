/**
 * Created by yeric on 10/9/17.
 */
import { mapGetters } from 'vuex';

const applicationFormPartials = {
    data() {
        return {
            applicationTypes: [
                { value: '0', name: 'New client' },
                { value: '1', name: 'Existing client' },
                { value: '2', name: 'Duplicate client' }
            ],
            taxable: [
                { name: 'Yes', id: '1' },
                { name: 'No', id: '0' }
            ],
            paymentMethods: [
                { value: 'Cheque', name: 'Cheque' },
                { value: 'Direct cash', name: 'Direct cash' },
                { value: 'Direct transfer', name: 'Direct transfer' },
                { value: 'M-Pesa', name: 'M-Pesa' },
            ]
        }
    },
    computed: {
        ...mapGetters(
            {
                partials: 'formPartials',
                clients: 'clients'
            }
        )
    },
    methods: {
        getFormPartials: function () {
            this.$store.commit("GET_FORM_PARTIALS");
        },
        getAllClients: function () {
            this.$store.commit("GET_ALL_CLIENTS");
        }
    },
    mounted() {
        this.getFormPartials();
        this.getAllClients();
    },
};

export default applicationFormPartials;
