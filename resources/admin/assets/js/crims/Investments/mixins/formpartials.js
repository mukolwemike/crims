import { mapGetters } from 'vuex';

const formPartials = {
    data() {
        return {
            tenorList: [
                { id: 1, name: '1 Month' },
                { id: 3, name: '3 Months' },
                { id: 6, name: '6 Months' },
                { id: 9, name: '9 Months' },
                { id: 12, name: '1 Year' },
                { id: 24, name: '2 Year' },
                { id: 36, name: '3 Year' },
            ],

            applicationTypes: [
                { value: 1, name: 'New client' },
                { value: 2, name: 'Existing client' },
                { value: 3, name: 'Duplicate client' }
            ],
        }
    },
    computed: {
        ...mapGetters(
            {
                titles: 'titles',
                countries: 'countries',
                employmentTypes: 'employmentTypes',
                contactMethods: 'contactMethods',
                sourceOfFunds: 'sourceOfFunds',
                businessNatures: 'businessNatures',
                products: 'products',
                gender: 'genderList',
                banks: 'bankAccounts',
                funds: 'unitFunds',
                unitFunds: 'allUnitFunds'
            }
        )
    },

    methods: {
        getTitlesList() {
            this.$store.commit('GET_TITLES_LIST');
        },

        getCountriesList() {
            this.$store.commit('GET_COUNTRIES_LIST');
        },

        getEmploymentTypesList() {
            this.$store.commit('GET_EMPLOYMENT_TYPES_LIST');
        },

        getContactMethodsList() {
            this.$store.commit('GET_CONTACT_METHOD_LIST');
        },

        getSourceOfFundsList() {
            this.$store.commit('GET_SOURCE_OF_FUNDS_LIST');
        },

        getBusinessNaturesList() {
            this.$store.commit('GET_BUSINESS_NATURES_LIST');
        },

        getGender() {
            this.$store.commit('GET_GENDER');
        },

        getProductsList() {
            this.$store.commit('GET_PRODUCTS_LIST');
        },

        getBanks: function () {
            this.$store.commit('GET_BANKS');
        },

        getUnitFunds: function () {
            this.$store.commit('GET_UNIT_FUNDS');
        },

        getAllUnitFunds: function () {
            this.$store.commit('GET_ALL_UNIT_FUNDS');
        }
    },
    mounted() {
        this.getTitlesList();
        this.getCountriesList();
        this.getEmploymentTypesList();
        this.getContactMethodsList();
        this.getSourceOfFundsList();
        this.getBusinessNaturesList();
        this.getProductsList();
        this.getUnitFunds();
        this.getGender();
        this.getGender();
        this.getBanks();
        this.getAllUnitFunds();
    }
};

export default formPartials;