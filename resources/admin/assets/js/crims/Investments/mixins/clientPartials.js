import { mapGetters } from 'vuex';

const clientPartials = {
    data() {
        return {

        }
    },
    computed: {
        ...mapGetters(
            {
                accounts: 'clientBankAccounts',
            }
        )
    },
    methods: {
        getClientAccounts: function (clientId) {
            this.$store.commit('GET_CLIENT_ACCOUNTS', clientId);
        }
    }
};

export default clientPartials;