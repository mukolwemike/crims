
const formData = {
    data() {
        return {
            tenorList: [
                { id: 1, name: '1 Month' },
                { id: 3, name: '3 Months' },
                { id: 6, name: '6 Months' },
                { id: 9, name: '9 Months' },
                { id: 12, name: '1 Year' },
                { id: 24, name: '2 Years' },
                { id: 36, name: '3 Years' },
                { id: 48, name: '4 Years' },
                { id: 60, name: '5 Years' },
                { id: 72, name: '6 Years' },
                { id: 84, name: '7 Years' },
            ],

            applicationTypes: [
                { value: 1, name: 'New client' },
                { value: 2, name: 'Existing client' },
                { value: 3, name: 'Duplicate client' }
            ],

            clientTypes: [
                { label: 'Individual', value: 1 },
                { label: 'Corporate', value: 2 }
            ],
        }
    },
};

export default formData;