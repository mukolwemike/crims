import { mapGetters } from 'vuex';

const formPartials = {

    computed: {
        ...mapGetters(
            {
                titles: 'titles',
                countries: 'countries',
                employmentTypes: 'employmentTypes',
                contactMethods: 'contactMethods',
                sourceOfFunds: 'sourceOfFunds',
                businessNatures: 'businessNatures',
                products: 'productsList',
                gender: 'genderList',
                banks: 'bankAccounts',
                funds: 'unitFunds',
                unitFunds: 'allUnitFunds'
            }
        )
    },

    methods: {
        getTitlesList() {
            this.$store.commit('GET_TITLES_LIST');
        },

        getCountriesList() {
            this.$store.commit('GET_COUNTRIES_LIST');
        },

        getEmploymentTypesList() {
            this.$store.commit('GET_EMPLOYMENT_TYPES_LIST');
        },

        getContactMethodsList() {
            this.$store.commit('GET_CONTACT_METHOD_LIST');
        },

        getSourceOfFundsList() {
            this.$store.commit('GET_SOURCE_OF_FUNDS_LIST');
        },

        getBusinessNaturesList() {
            this.$store.commit('GET_BUSINESS_NATURES_LIST');
        },

        getGender() {
            this.$store.commit('GET_GENDER');
        },

        getProductsList() {
            this.$store.commit('GET_PRODUCTS_LIST');
        },

        getBanks: function () {
            this.$store.commit('GET_BANK');
        },

        getUnitFunds: function () {
            this.$store.commit('GET_UNIT_FUNDS');
        },

        getAllUnitFunds: function () {
            this.$store.commit('GET_ALL_UNIT_FUNDS');
        },

        getShareEntity: function () {
            this.$store.commit('GET_ALL_SHARE_ENTITY');
        },
    },
    mounted() {
        this.getTitlesList();
        this.getCountriesList();
        this.getEmploymentTypesList();
        this.getContactMethodsList();
        this.getSourceOfFundsList();
        this.getBusinessNaturesList();
        this.getProductsList();
        this.getUnitFunds();
        this.getGender();
        this.getGender();
        this.getBanks();
        this.getAllUnitFunds();
        this.getShareEntity();
    }
};

export default formPartials;