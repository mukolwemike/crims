import { mapGetters } from 'vuex';

const create = {
    data() {
        return {
            order_type_id: '',
            security_id: '',
            order_type: 'buy',
            asset_class: 'Equities',
            amount: '',
            shares: '',
            fundOrder: {
                custodial_account_id : ''
            },
            orderForm: {},
            good_till: '',
            good_till_type: '',
            expiry_date: '',
            fund: '',
        }
    },

    computed: {
        ...mapGetters({
            funds: 'allUnitFunds',
            orderTypes: 'orderTypes',
            securities: 'securities',
            lifespanTypes: 'lifespanTypes',
            gettingCompliance: 'gettingCompliance',
            fetchingTypes: 'fetchingTypes',
            fetchingSecurities: 'fetchingSecurities',
            fetchingFund: 'fetchingFund',
            formFundOrders: 'formFundOrders',
            warningDialog: 'warningDialog',
            savingOrder: 'savingOrder',
            custodialAccounts: 'custodialAccounts',
        }),


        value() {

            if(this.asset_class === 'Equities') {
                return parseFloat(this.fundOrder.shares) * parseFloat(this.orderForm.cut_off_price);
            }

            return this.fundOrder.amount;
        },
    },

    watch: {
        security_id: {
            handler: function () {
                let vm = this;

                let security = this.securities.filter(function (security) {
                    return (security.id == vm.security_id);
                })[0];

                this.orderForm.security = security;

                this.asset_class = security.asset_class;

                this.orderForm.security_id = this.security_id;
            }
        },

        order_type_id: {
            handler: function () {

                let vm = this;

                let orderType = this.orderTypes.filter(function (order_type) {
                    return (order_type.id == vm.order_type_id);
                })[0];

                this.order_type = orderType.slug;

                this.orderForm.order_type_id = this.order_type_id;
            }
        },

        good_till: {
            handler: function () {
                this.orderForm.good_till = this.good_till;

                let vm = this;

                this.good_till_type = this.lifespanTypes.filter( (lifespanType) => {
                    return lifespanType.id === vm.good_till;
                })[0].slug;
            }
        },

        expiry_date: {
            handler: function () {
                this.orderForm.expiry_date = moment(this.expiry_date).format('YYYY-MM-DD');
            }
        },

        shares: {
            handler: function () {
                this.fundOrder.shares = this.shares;
            }
        },

        amount: {
            handler: function () {
                this.fundOrder.amount = this.amount;
            }
        },

    }
};

export default create;