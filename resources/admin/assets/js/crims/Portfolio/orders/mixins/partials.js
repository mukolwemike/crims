import { mapGetters } from 'vuex';

const partials = {

    computed: {
        ...mapGetters( {
            funds: 'allUnitFunds',
            fetchingFund: 'fetchingFund',
            orderTypes: 'orderTypes',
            fetchingTypes: 'fetchingTypes',
            fetchingSecurities: 'fetchingSecurities',
            securities: 'securities'
        })
    },

    methods: {
        getUnitFunds: function () {
            this.$store.commit('GET_ALL_UNIT_FUNDS');
        },

        getOrderTypes: function () {
            this.$store.commit('GET_ORDER_TYPES');
        },

        getPortfolioSecurities: function () {
            this.$store.commit('GET_SECURITIES');
        },

        getAssetClasses: function () {
            this.$store.commit('GET_ASSET_CLASSES');
        },

        getSettlementFees: function () {
            this.$store.commit('GET_SETTLEMENT_FEES');
        }
    },

    mounted() {

        this.getUnitFunds();

        this.getOrderTypes();

        this.getPortfolioSecurities();

        this.getAssetClasses();

        this.getSettlementFees();
    }
};

export default partials;