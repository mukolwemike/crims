import { mapGetters } from 'vuex';
import { Message } from 'element-ui';
import moment from 'moment';

const allocations = {
    data() {
        return {
            form: {},
            date: '',
            allocations: [],
            allocation: {},
            amount: '',
            shares: ''
        }
    },

    watch: {
        date: {
            handler: function () {

                this.form.date = moment(this.date).format('YYYY-MM-DD');

                if(this.allocations.length > 0) {
                    this.allocations = this.allocations.forEach( (allocation) => {
                        allocation.date = moment(this.date).format('YYYY-MM-DD');
                    });
                }
            }
        },
        amount: {
            handler: function () {
                if(this.amount > this.total_amount) {
                    Message.warning({ message: 'Amount added for allocations is more that amount ordered'});
                } else {
                    this.form.amount = this.amount;
                }
            }
        },

        shares: {
            handler: function () {
                this.allocation.shares = Math.round(this.shares/100)*100;
            }
        }
    },

    computed: {
        ...mapGetters({
            allocatingOrder: 'allocatingOrder',
            order: 'portfolioOrderDetails',
        }),

        total_shares() {
            if(this.order.asset_class == 'Equities') {
                let shares_ordered = this.order.unit_fund_orders.reduce( (carry, order) => {
                    return carry + parseFloat(order.shares);
                }, 0);

                let shares_allocated = this.order.order_allocations.reduce( (carry, allocation) => {

                    let alloc = (allocation.status == 2) ? 0 : parseFloat(allocation.shares);

                    return carry + alloc;

                }, 0);

                return (shares_ordered - shares_allocated);
            }

            return 0;
        },

        total_amount() {
            if(this.order.asset_class !== 'Equities') {
                let amount_ordered  = this.order.unit_fund_orders.reduce( (carry, order) => {
                    return carry + parseFloat(order.amount);
                }, 0);

                let amount_allocated  = this.order.order_allocations.reduce( (carry, allocation) => {
                    return carry + parseFloat(allocation.amount);
                }, 0);

                return (amount_ordered - amount_allocated);
            }

            return 0;
        },

        total_shares_added() {
            return this.allocations.reduce( (carry, allocation) => {
                return carry + parseFloat(allocation.shares);
            }, 0);
        },

        total_amount_added() {
            return this.allocations.reduce( (carry, allocation) => {
                return carry + parseFloat(allocation.amount);
            }, 0);
        }
    },

    methods: {
        submit: function () {
            this.form.portfolio_order_id = this.order.id;
            this.form.allocations = this.allocations;

            if(!this.validateForm()) {
                Message.warning({ message: 'Fill the form and proceed' });

                return;
            }

            this.$store.commit('SAVE_ORDER_ALLOCATION', this.form);
        },

        close: function () {
            this.$store.commit('ALLOCATE_ORDER_MODAL', false);
        },

        validateForm: function () {

            if(this.form.allocations.length > 0 || this.form.amount && this.form.date) {
                return true;
            }

            return false;
        },

        addAllocation: function () {

            this.allocation.portfolio_order_id = this.order.id;

            if(this.date) this.allocation.date = moment(this.date).format('YYYY-MM-DD');

            if(!this.validateAllocation()) {
                Message.warning({ message: 'No values entered'});
                return;
            }

            if(!this.validatePriceOrRate()) {
                return;
            }

            this.allocations.push(this.allocation);

            if(!this.validateTotalSharesOrAmount()) {
                (this.order.asset_class === 'Equities')
                    ? Message.warning({ message: 'Total shares for allocation is more than total shares ordered'})
                    : Message.warning({ message: 'Total amount should be less or equal to amount specified in the order'});

                this.removeAllocation(this.allocation);

                return;
            }

            this.allocation = {};
        },

        validateAllocation: function () {
            if(
                this.order.asset_class === 'Equities' &&
                this.shares &&
                this.allocation.price
            )
            {
                return true;
            } else if(this.allocation.amount && this.allocation.rate) {
                return true;
            }

            return false;
        },

        validatePriceOrRate: function () {

            let price = parseFloat(this.allocation.price);
            let cut_off = parseFloat(this.order.cut_off_price);
            let rate = parseFloat(this.allocation.rate);
            let minimum_rate = parseFloat(this.order.minimum_rate);

            //Equities Buy
            if(this.order.asset_class === 'Equities' && this.order.order_type === 'Buy' && price > cut_off)
            {
                Message.warning({ message: 'Price should be equal or less cut off price'});

                return false;
            }
            //Equities Sell
            else if(this.order.asset_class === 'Equities' && this.order.order_type === 'Sell' && price < cut_off)
            {
                Message.warning({ message: 'Price should be equal or more the cut off price'});

                return false;
            }
            //Deposits/Bonds Buy
            else if(this.order.order_type === 'Buy' && rate < minimum_rate)
            {
                Message.warning({ message: 'Rate should be greater than or equal to minimum rate'});
                return false;
            }
            // //Deposits/Bonds Sell
            // else if(this.order.order_type === 'Sell' && rate < minimum_rate)
            // {
            //     Message.warning({ message: 'Rate should be more or equal to minimum rate'});
            //
            //     return false;
            // }

            return true;
        },

        validateTotalSharesOrAmount: function () {

            if(
                this.order.asset_class === 'Equities' &&
                parseFloat(this.total_shares_added) > parseFloat(this.total_shares)
            )
            {
                return false;
            } else if(parseFloat(this.total_amount_added) > parseFloat(this.total_amount)) {
                return false;
            }

            return true;
        },

        removeAllocation: function (item) {

            let index = this.allocations.indexOf(item);

            if (index !== -1) {
                this.allocations.splice(index, 1);
            }
        }
    },

    mounted() {
        this.form = {};
        this.allocations = [];
        this.allocation = {};
    }
};

export default allocations;