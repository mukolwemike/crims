import { mapGetters } from 'vuex';

const Settlement = {

    computed: {
        ...mapGetters({
            order: 'portfolioOrderDetails',
        }),

        value_to_allocate() {

            if(this.order.asset_class === 'Equities') {
                return this.order.unit_fund_orders.reduce( (carry, order) => {
                    return carry + parseFloat(order.shares);
                }, 0);
            }

            return this.order.unit_fund_orders.reduce( (carry, order) => {
                return carry + parseFloat(order.amount);
            }, 0);
        }
    },

    methods: {
        shares: function (fundOrder, allocation) {

            let ratio = (parseFloat(fundOrder.shares)/ this.value_to_allocate);

            let shares = ratio * parseFloat(allocation.shares);

            return Math.round(shares/100)*100;
        },

        amount: function (fundOrder, allocation) {

            let ratio = (parseFloat(fundOrder.amount)/ this.value_to_allocate);

            let amount = ratio * parseFloat(allocation.amount);

            return Math.round(amount);
        }
    }
};

export default Settlement;