import { mapGetters } from 'vuex';

const PortfolioMixin = {
    computed: {
        ...mapGetters(
            {
                investors: 'investors',
                securityTypes: 'securityTypes',
                fundManagers: 'fundManagers',
                assetClasses: 'assetClasses',
                subAssetClasses: 'subAssetClasses',
                investmentTypes: 'investmentTypes',
                security: 'portfolioSecurity',
                custodialAccounts: 'custodialAccounts',
                funds: 'funds',
                taxRates: 'taxRates',
                depositTypes: 'depositTypes',
                securities: 'securities'
            }
        )
    },

    methods: {
        getBondTypes: function (asset) {
            this.$store.commit('GET_SECURITY_TYPES', asset);
        },

        getInvestorTypes: function () {
            this.$store.commit('GET_PORTFOLIO_INVESTORS');
        },

        getAssetClasses: function () {
            this.$store.commit('GET_ASSET_CLASSES');
        },

        getFundManagers: function () {
            this.$store.commit('GET_FUND_MANAGERS');
        },

        getInvestmentTypes: function () {
            this.$store.commit('GET_INVESTMENT_TYPE');
        },

        getCustodialAccounts: function (fundManagerId) {
            this.$store.commit('GET_CUSTODIAL_ACCOUNTS', fundManagerId);
        },

        getFund: function () {
            this.$store.commit('GET_FUNDS', this.security.fund_manager_id);
        },

        getTaxRates: function () {
            this.$store.commit('GET_TAX_RATES');
        },

        getDepositTypes: function () {
            this.$store.commit('GET_DEPOSIT_TYPES');
        },

        getUnitFunds: function () {
            this.$store.commit('GET_ALL_UNIT_FUNDS');
        },

        getPortfolioSecurities: function () {
            this.$store.commit('GET_SECURITIES');
        },

        getComplianceTypes: function () {
            this.$store.commit('GET_COMPLIANCE_TYPES');
        },

        getOrderLifespanTypes: function () {
            this.$store.commit('GET_ORDER_LIFESPAN_TYPES');
        },

        getSectors: function () {
            this.$store.commit('GET_SECTORS');
        },

        getCountries: function () {
            this.$store.commit('GET_COUNTRIES');
        },

        getCurrencies: function () {
            this.$store.commit('GET_CURRENCIES');
        }
    },

    mounted() {
        this.getAssetClasses();
        this.getInvestorTypes();
        this.getFundManagers();
        this.getInvestmentTypes();
        this.getTaxRates();
        this.getDepositTypes();
        this.getUnitFunds();
        this.getPortfolioSecurities();
        this.getComplianceTypes();
        this.getOrderLifespanTypes();
        this.getSectors();
        this.getCountries();
        this.getCurrencies();
        this.getCustodialAccounts(this.fundmanagerId);
    }
};

export default PortfolioMixin;