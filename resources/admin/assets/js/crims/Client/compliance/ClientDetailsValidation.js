import axios from 'axios';
import {Message} from 'element-ui';

export default {
    props: ['client'],

    name :'client-details-validation',

    data() {
        return {
            checkValidation: false,
            kycIDValidated: false,
            kycPINValidated: false,
            confirming: false,
            requestingID: false,
            requestingPIN: false,
            dialogComment: false,
            comment: '',
            actionType: ''
        }
    },

    methods: {
        checkValidated() {
            this.checkValidation = true;

            axios.get('/api/client/kyc-validation/check-validated/' + this.client.uuid).then(
                ({data}) => {
                    this.kycIDValidated = data.validated_id;
                    this.kycPINValidated = data.validated_pin;

                    this.checkValidation = false;
                },
            );
        },

        confirmData() {
            this.confirming = true;

            let data = {
                'type': this.actionType,
                'comment': this.comment
            };

            axios.post('/api/client/kyc-validation/validate/' + this.client.uuid, data).then(
                ({data}) => {
                    this.kycIDValidated = data.validated_id;
                    this.kycPINValidated = data.validated_pin;
                    this.dialogComment = false;
                    this.comment = '';
                    this.confirming = false;

                    Message.success(type === 'pin_no' ? 'PIN Validated Successfully' : 'ID Validated Successfully');

                    location.reload();
                },
                () => {
                    this.confirming = false;

                    Message.error('Error occurred try again');
                }
            );
        },

        requestProof(type) {
            type === 'pin_no' ? this.requestingPIN = true : this.requestingID = true;

            let data = {
                'type': type,
            };

            axios.post('/api/client/kyc-validation/request-proof/' + this.client.uuid, data).then(
                ({data}) => {
                    type === 'pin_no' ? this.requestingPIN = false : this.requestingID = false;

                    Message.success(type === 'pin_no' ? 'PIN PROOF requested successfully' : 'ID PROOF requested successfully');
                },
                () => {
                    Message.error('Error occurred try again');
                }
            );
        },

        openDialogComment(type) {
            this.dialogComment = true;
            this.actionType = type;
        }
    },

    mounted() {
        this.checkValidated();
    }
}