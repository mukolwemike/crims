export default {
    data() {
        return {
            loyaltyVoucherModal: false,
        }
    },

    methods: {
        activateModal() {
            this.loyaltyVoucherModal = !this.loyaltyVoucherModal;
        },
    }
}