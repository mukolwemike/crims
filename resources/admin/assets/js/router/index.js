import Vue from 'vue';
import VueRouter from 'vue-router';
import { Message } from 'element-ui';
import * as AuthUser from '../auth/user';
/**
 * import unit fund holder components
 */
import UnitFundHoldersIndex from '../crims/Unitization/unitfundholders/indexforfund.vue';
import UnitFundHoldersShow from '../crims/Unitization/unitfundholders/show.vue';
/**
 * import unit fund purchases components
 */
import UnitFundPurchasesNew from '../crims/Unitization/unitfundpurchases/create.vue';
import UnitFundPurchasesShow from '../crims/Unitization/unitfundpurchases/show';
/**
 * import unit fund business confirmation
 */
import UnitFundBusinessConfirmationsIndex from '../crims/Unitization/unitfundbusinessconfirmations/index.vue';
import UnitFundBusinessConfirmationsShow from '../crims/Unitization/unitfundbusinessconfirmations/show.vue';
/**
 * import unit fund sales components
 */
import UnitFundSalesNew from '../crims/Unitization/unitfundsales/create.vue';
/**
 * import unit fund transfers components
 */
import UnitFundTransfersNew from '../crims/Unitization/unitfundtransfers/create.vue';
/**
 * Import unit funds component
 */
import UnitFundsIndex from '../crims/Unitization/unitfunds/index.vue';
import UnitFundsNew from '../crims/Unitization/unitfunds/create.vue';
import UnitFundsShow from '../crims/Unitization/unitfunds/show.vue';
import UnitFundsEdit from '../crims/Unitization/unitfunds/edit.vue';
/**
 * Import unit fund fee types component
 */
import UnitFundFeeTypesIndex from '../crims/Unitization/unitfundfeetypes/index.vue';
import UnitFundFeeTypesNew from '../crims/Unitization/unitfundfeetypes/create.vue';
import UnitFundFeeTypesShow from '../crims/Unitization/unitfundfeetypes/show.vue';
import UnitFundFeeTypesEdit from '../crims/Unitization/unitfundfeetypes/edit.vue';
/**
 * Import unit fund fee parameters component
 */
import UnitFundFeeParametersIndex from '../crims/Unitization/unitfundfeeparameters/index.vue';
import UnitFundFeeParametersNew from '../crims/Unitization/unitfundfeeparameters/create.vue';
import UnitFundFeeParametersShow from '../crims/Unitization/unitfundfeeparameters/show.vue';
import UnitFundFeeParametersEdit from '../crims/Unitization/unitfundfeeparameters/edit.vue';
/**
 * Import unit fund fees component
 */
import UnitFundFeesIndex from '../crims/Unitization/unitfundfees/index.vue';
import UnitFundFeesNew from '../crims/Unitization/unitfundfees/create.vue';
import UnitFundFeesShow from '../crims/Unitization/unitfundfees/show.vue';
import UnitFundFeesEdit from '../crims/Unitization/unitfundfees/edit.vue';
/**
 * Import unit fund price trails component
 */
import UnitFundPriceTrailsIndex from '../crims/Unitization/unitfundpricetrails/index.vue';
import UnitFundPriceTrailsNew from '../crims/Unitization/unitfundpricetrails/create.vue';
import UnitFundPriceTrailsShow from '../crims/Unitization/unitfundpricetrails/show.vue';
import UnitFundPriceTrailsEdit from '../crims/Unitization/unitfundpricetrails/edit.vue';
/**
 * Import unit fund fee payments
 */
import UnitFundFeePaymentIndex from '../crims/Unitization/feepayments/index.vue';
import UnitFundFeePaymentNew from '../crims/Unitization/feepayments/create.vue';
import UnitFundFeePaymentShow from '../crims/Unitization/feepayments/show.vue';
/**
 * Import unit fund compliance
 */
import UnitFundCompliance from '../crims/Unitization/compliance/index.vue';
/**
 * Import unit fund switch rates component
 */
import UnitFundSwitchRatesIndex from '../crims/Unitization/unitfundswitchrates/index.vue';
import UnitFundSwitchRatesNew from '../crims/Unitization/unitfundswitchrates/create.vue';
import UnitFundSwitchRatesShow from '../crims/Unitization/unitfundswitchrates/show.vue';
import UnitFundSwitchRatesEdit from '../crims/Unitization/unitfundswitchrates/edit.vue';
/**
 * Import unit fund commission rates component
 */
import UnitFundCommissionRatesIndex from '../crims/Unitization/unitfundcommissionrates/index.vue';
import UnitFundCommissionRatesNew from '../crims/Unitization/unitfundcommissionrates/create.vue';
import UnitFundCommissionRatesShow from '../crims/Unitization/unitfundcommissionrates/show.vue';
/**
 * Import unit fund statement campaigns component
 */
import UnitFundStatementCampaignsIndex from '../crims/Unitization/unitfundstatementcampaigns/index.vue';
import UnitFundStatementCampaignsNew from '../crims/Unitization/unitfundstatementcampaigns/create.vue';
import UnitFundStatementCampaignsShow from '../crims/Unitization/unitfundstatementcampaigns/show.vue';
import UnitFundStatementCampaignsEdit from '../crims/Unitization/unitfundstatementcampaigns/edit.vue';
/**
 * Import unit fund switches component
 */
import UnitFundSwitchesIndex from '../crims/Unitization/unitfundswitches/index.vue';
import UnitFundSwitchesShow from '../crims/Unitization/unitfundswitches/show.vue';
/**
 * Import unit fund holder fees component
 */
import UnitFundHolderFeesIndex from '../crims/Unitization/unitfundholderfees/index.vue';
/**
 * Import unit fund calculator components
 */
import UnitFundCalculator from '../crims/Unitization/unitfundcalculator/index.vue'
/**
 * Import portfolio asset class
 */
import AssetClasses from '../crims/Portfolio/assetclasses/index.vue';
import AssetClassesNew from '../crims/Portfolio/assetclasses/create.vue';
import AssetClassesEdit from '../crims/Portfolio/assetclasses/editclass.vue';
import AssetClassesShow from '../crims/Portfolio/assetclasses/show.vue';

import SubAssetClasses from '../crims/Portfolio/subassetclasses/index.vue';
/**
 * Import asset class summary components
 */
import AssetClassSummariesIndex from '../crims/Portfolio/assetclasssummaries/summaries.vue';
/**
 * Import portfolio securities components
 */
import PorfolioSecurityIndex from '../crims/Portfolio/securities/securities.vue';
import PorfolioSecurityShow from '../crims/Portfolio/securities/show.vue';
import PorfolioSecurityEdit from '../crims/Portfolio/securities/edit.vue';
import PorfolioSecurityShowMarketPrice from '../crims/Portfolio/equities/market-price-trails.vue';
import PorfolioSecurityShowTargetPrice from '../crims/Portfolio/equities/target-price-trails.vue';
/**
 * Import portfolio security holding components
 */
import PorfolioSecurityShowBondHolding from '../crims/Portfolio/bonds/show.vue';
import PorfolioSecurityShowDepositHolding from '../crims/Portfolio/deposits/show.vue';
import PorfolioSecurityDepositRepayments from '../crims/Portfolio/deposits/actions/repayments.vue';
import CreateOrder from '../crims/Portfolio/orders/create/form';

import ViewOrder from '../crims/Portfolio/orders/show.vue';

import ViewCompliance from '../crims/Portfolio/compliance/benchmarks/show';
import store from "../../../../client/assets/js/store";

/**
 * Import with holding tax page
 */
import WithholdingTaxIndex from '../crims/Portfolio/custodials/tax/index';

import CustomReportShow from '../crims/Investments/reports/custom/show';
/*
 * Unit fund commission schedules
 */
import CommissionSchedules from '../crims/Unitization/unitfundholders/partials/commissionschedules'

/**
 *  Import portfolio orders
 */
import InterestPaymentIndex from '../crims/Unitization/unitfundholders/interest/index';
import InterestPaymentShow from '../crims/Unitization/unitfundholders/interest/show';

/**
 * Unit fund redemption confirmations
 */
import UnitFundRedemptionConfirmationsIndex from '../crims/Unitization/unitfundredemptionconfirmations/index'
import UnitFundRedemptionConfirmationsShow from '../crims/Unitization/unitfundredemptionconfirmations/show'


/**
 * USSD Applications
 */
import USSDApplicationsIndex from '../crims/Ussd/index';
import USSDApplicationsShow from '../crims/Ussd/show';

/**
 * Client Loyalties
 */
import ClientLoyaltyIndex from '../crims/Client/loyalty/index';
import ClientLoyaltyValues from '../crims/Client/loyalty/loyalty-value';
import ClientLoyaltyVouchers from '../crims/Client/loyalty/loyalty-vouchers';
import ClientLoyaltyRedeemView from '../crims/Client/loyalty/loyalty-redeem';

Vue.use(VueRouter);

const router = new VueRouter(
    {
        mode: 'history',
        linkActiveClass: 'active',
        routes: [
            //  --  UNIT FUND CLIENTS
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-clients',
                component: UnitFundHoldersIndex,
                name: 'unit-fund-holders.index',
                meta: {
                    permission: 'unittrust:view-unit-trust-client',
                    name: 'view unit trust clients',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-clients/:id',
                component: UnitFundHoldersShow,
                name: 'unit-fund-holders.show',
                meta: {
                    permission: 'unittrust:view-unit-trust-client',
                    name: 'view unit trust clients',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND PURCHASES
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-client/:id/unit-fund-purchases',
                component: UnitFundPurchasesNew,
                name: 'unit-fund-purchases.new',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit trust instruction',
                    requirePermission: true
                }
            },

            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-client/:id/unit-fund-purchases/:fund_purchase_id',
                component: UnitFundPurchasesShow,
                name: 'unit-fund-purchases.show',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit trust instruction',
                    requirePermission: true
                }
            },

            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-client/:id/unitfund-purchases/:fund_purchase_id',
                component: CommissionSchedules,
                name: 'unit-fund-purchases.commission',
                meta: {
                    permission: 'unittrust:view-unit-trust-client',
                    name: 'view unit trust clients',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND BUSINESS CONFIRMATIONS
            {
                path: '/dashboard/unitization/unit-fund-business-confirmations',
                component: UnitFundBusinessConfirmationsIndex,
                name: 'unit-fund-business-confirmations.index'
            },
            {
                path: '/dashboard/unitization/unit-fund-business-confirmations/:id',
                component: UnitFundBusinessConfirmationsShow,
                name: 'unit-fund-business-confirmations.show'
            },

            //  -- UNIT FUND REDEMPTION CONFIRMATION
            {
                path: '/dashboard/unitization/unit-fund-redemption-confirmations',
                name: 'unit-fund-redemption-confirmations.index',
                component: UnitFundRedemptionConfirmationsIndex
            },
            {
                path: '/dashboard/unitization/unit-fund-redemption-confirmations/:id',
                component: UnitFundRedemptionConfirmationsShow,
                name: 'unit-fund-redemption-confirmations.show'
            },

            //  --  UNIT FUND SALES
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-clients/:id/unit-fund-sales',
                component: UnitFundSalesNew,
                name: 'unit-fund-sales.new',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit trust instruction',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND TRANSFERS
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-clients/:id/unit-fund-transfers',
                component: UnitFundTransfersNew,
                name: 'unit-fund-transfers.new',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit trust instruction',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND INTEREST PAYMENTS
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-client/:id/interest-payments',
                component: InterestPaymentIndex,
                name: 'unit-fund-interest-payments.index',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit trust instruction',
                    requirePermission: true
                }
            },

            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-client/:id/interest-payments/:schedule_id',
                component: InterestPaymentShow,
                name: 'unit-fund-interest-payments.show',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit trust instruction',
                    requirePermission: true
                }
            },


            //  --  UNIT FUNDS
            {
                path: '/dashboard/unitization/unit-funds',
                component: UnitFundsIndex,
                name: 'unit-funds.index',
                meta: {
                    permission: 'unittrust:view-funds',
                    name: 'view unit trusts',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/new',
                component: UnitFundsNew,
                name: 'unit-funds.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:id',
                component: UnitFundsShow,
                name: 'unit-funds.show',
                meta: {
                    permission: 'unittrust:view-funds',
                    name: 'view unit trusts',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:id/edit',
                component: UnitFundsEdit,
                name: 'unit-funds.edit',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            // -- USSD APPLICATIONS
            {
                path: '/dashboard/unitization/ussd/ussd-applications',
                component: USSDApplicationsIndex,
                name: 'ussd-applications.index',
                meta: {
                    permission: 'unittrust:view-funds',
                    name: 'view unit trusts',
                    requirePermission: true
                }
            },

            {
                path: '/dashboard/unitization/ussd/ussd-applications/:id',
                component: USSDApplicationsShow,
                name: 'ussd-applications.show',
                meta: {
                    permission: 'unittrust:view-funds',
                    name: 'view unit trusts',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND FEE TYPES
            {
                path: '/dashboard/unitization/unit-fund-fee-types',
                component: UnitFundFeeTypesIndex,
                name: 'unit-fund-fee-types.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-fee-types/new',
                component: UnitFundFeeTypesNew,
                name: 'unit-fund-fee-types.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-fee-types/:id',
                component: UnitFundFeeTypesShow,
                name: 'unit-fund-fee-types.show',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-fee-types/:id/edit',
                component: UnitFundFeeTypesEdit,
                name: 'unit-fund-fee-types.edit',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND FEE PARAMETERS
            {
                path: '/dashboard/unitization/unit-fund-fee-parameters',
                component: UnitFundFeeParametersIndex,
                name: 'unit-fund-fee-parameters.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-fee-parameters/new',
                component: UnitFundFeeParametersNew,
                name: 'unit-fund-fee-parameters.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-fee-parameters/:id',
                component: UnitFundFeeParametersShow,
                name: 'unit-fund-fee-parameters.show',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-fee-parameters/:id/edit',
                component: UnitFundFeeParametersEdit,
                name: 'unit-fund-fee-parameters.edit',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND FEES
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-fees',
                component: UnitFundFeesIndex,
                name: 'unit-fund-fees.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-fees/new',
                component: UnitFundFeesNew,
                name: 'unit-fund-fees.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-fees/:id',
                component: UnitFundFeesShow,
                name: 'unit-fund-fees.show',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-fees/:id/edit',
                component: UnitFundFeesEdit,
                name: 'unit-fund-fees.edit',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND PRICE TRAILS
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-price-trails',
                component: UnitFundPriceTrailsIndex,
                name: 'unit-fund-price-trails.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-price-trails/new',
                component: UnitFundPriceTrailsNew,
                name: 'unit-fund-price-trails.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-price-trails/:id',
                component: UnitFundPriceTrailsShow,
                name: 'unit-fund-price-trails.show',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-price-trails/:id/edit',
                component: UnitFundPriceTrailsEdit,
                name: 'unit-fund-price-trails.edit',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            //  -- UNIT FUND FEE PAYMENTS
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-fee-payments',
                component: UnitFundFeePaymentIndex,
                name: 'unit-fund-fee-payments.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },

            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-fee-payments/new',
                component: UnitFundFeePaymentNew,
                name: 'unit-fund-fee-payments.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-fee-payments/:id',
                component: UnitFundFeePaymentShow,
                name: 'unit-fund-fee-payments.show',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },

            //  --  UNIT FUND SWITCH RATES
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-switch-rates',
                component: UnitFundSwitchRatesIndex,
                name: 'unit-fund-switch-rates.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-switch-rates/new',
                component: UnitFundSwitchRatesNew,
                name: 'unit-fund-switch-rates.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-switch-rates/:id',
                component: UnitFundSwitchRatesShow,
                name: 'unit-fund-switch-rates.show',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-switch-rates/:id/edit',
                component: UnitFundSwitchRatesEdit,
                name: 'unit-fund-switch-rates.edit',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND SWITCHES
            {
                path: '/dashboard/unitization/unit-fund-switches',
                component: UnitFundSwitchesIndex,
                name: 'unit-fund-switches.index',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit fund instruction',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-holders/:hid/unit-fund-switches/:id',
                component: UnitFundSwitchesShow,
                name: 'unit-fund-switches.show',
                meta: {
                    permission: 'unittrust:create-fund-instructions',
                    name: 'create unit fund instruction',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND COMMISSION RATES
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-commission-rates',
                component: UnitFundCommissionRatesIndex,
                name: 'unit-fund-commission-rates.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'create unit fund instruction',
                    requirePermission: false
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-commission-rates/new',
                component: UnitFundCommissionRatesNew,
                name: 'unit-fund-commission-rates.new',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-commission-rates/:id',
                component: UnitFundCommissionRatesShow,
                name: 'unit-fund-commission-rates.show',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: false
                }
            },

            //  --  UNIT FUND STATEMENT CAMPAIGNS
            {
                path: '/dashboard/unitization/unit-fund-statement-campaigns/:id',
                component: UnitFundStatementCampaignsIndex,
                name: 'unit-fund-statement-campaigns.index',
                meta: {
                    permission: 'unittrust:view-client-statements',
                    name: 'view client statement',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-statement-campaigns/:id/new',
                component: UnitFundStatementCampaignsNew,
                name: 'unit-fund-statement-campaigns.new',
                meta: {
                    permission: 'unittrust:view-client-statements',
                    name: 'view client statement',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-statement-campaigns/:id/:cid',
                component: UnitFundStatementCampaignsShow,
                name: 'unit-fund-statement-campaigns.show',
                meta: {
                    permission: 'unittrust:view-client-statements',
                    name: 'view client statement',
                    requirePermission: true
                }
            },
            {
                path: '/dashboard/unitization/unit-fund-statement-campaigns/:id/:cid/edit',
                component: UnitFundStatementCampaignsEdit,
                name: 'unit-fund-statement-campaigns.edit',
                meta: {
                    permission: 'unittrust:view-client-statements',
                    name: 'view client statement',
                    requirePermission: true
                }
            },

            //  --  UNIT FUND HOLDER FEES
            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-holder-fees',
                component: UnitFundHolderFeesIndex,
                name: 'unit-fund-holder-fees.index',
            },

            // -- UNIT FUND CALCULATOR
            {
                path: '/dashboard/unitization/unit-fund-calculator',
                component: UnitFundCalculator,
                name: 'unit-fund-claculator.index',
            },

            // --PORTFOLIO (Asset Class)
            {
                path: '/dashboard/portfolio/asset-classes',
                name: 'asset-classes',
                component: AssetClasses
            },
            {
                path: '/dashboard/portfolio/asset-classes/new',
                name: 'asset-classes.new',
                component: AssetClassesNew,
            },
            {
                path: '/dashboard/portfolio/asset-classes/:id',
                name: 'asset-classes.show',
                component: AssetClassesShow,
            },
            {
                path: '/dashboard/portfolio/asset-classes/:id/edit',
                name: 'asset-classes.edit',
                component: AssetClassesEdit,
            },

            // -PORTFOLIO (Sub asset Classes)
            {
                path: '/dashboard/portfolio/:id/sub-asset-classes',
                name: 'sub-asset-classes.index',
                component: SubAssetClasses,
            },

            // - Asset Class Summaries
            {
                path: '/dashboard/portfolio/asset-class-summaries',
                name: 'asset-class-summaries.index',
                component: AssetClassSummariesIndex
            },

            // - PORTFOLIO SECURITES (EQUITIES)
            {
                name: 'portfolio-securities.index',
                path: '/dashboard/portfolio/securities',
                component: PorfolioSecurityIndex,
            },
            {
                name: 'portfolio-securities.show',
                path: '/dashboard/portfolio/securities/:id',
                component: PorfolioSecurityShow,
            },
            {
                name: 'portfolio-securities.edit',
                path: '/dashboard/portfolio/securities/:id/edit',
                component: PorfolioSecurityEdit,
            },
            {
                name: 'portfolio-securities.showMarketPrice',
                path: '/dashboard/portfolio/securities/:id/market-price',
                component: PorfolioSecurityShowMarketPrice,
            },
            {
                name: 'portfolio-securities.showTargetPrice',
                path: '/dashboard/portfolio/securities/:id/target-price',
                component: PorfolioSecurityShowTargetPrice,
            },
            {
                name: 'portfolio-securities-bonds.holding',
                path: '/dashboard/portfolio/securities/:id/bond-holdings/:holding_id',
                component: PorfolioSecurityShowBondHolding
            },
            {
                name: 'portfolio-securities-deposits.holding',
                path: '/dashboard/portfolio/securities/:id/deposit-holdings/:holding_id',
                component: PorfolioSecurityShowDepositHolding,
            },
            {
                name: 'portfolio-securities-deposit-repayments',
                path: '/dashboard/portfolio/securities/:id/deposit-holdings/:holding_id/repay',
                component: PorfolioSecurityDepositRepayments,
            },
            //Fund Compliance
            {
                name: 'portfolio-order.create',
                path: '/dashboard/portfolio/orders/create',
                component: CreateOrder
            },

            {
                name: 'portfolio-order.show',
                path: '/dashboard/portfolio/orders/:id',
                component: ViewOrder
            },

            {
                path: '/dashboard/unitization/unit-funds/:fid/unit-fund-compliance',
                component: UnitFundCompliance,
                name: 'unit-fund-compliance.index',
                meta: {
                    permission: 'unittrust:create-unit-trust',
                    name: 'set up unit trust',
                    requirePermission: true
                }
            },

            {
                name: 'fund-compliance.show',
                path: '/dashboard/portfolio/compliance/:id',
                component: ViewCompliance,
                meta: {
                    permission: 'unittrust:unit-trust-compliance',
                    name: 'set up unit trust compliance',
                    requirePermission: true
                }
            },

            {
                name: 'custodial.wht.show',
                path: '/dashboard/portfolio/custodial/:id/wht',
                component: WithholdingTaxIndex,
            },

            {
                name: 'custom.report.show',
                path: '/dashboard/investments/reports/:id',
                component: CustomReportShow
            },

            // Client Loyalties
            {
                name: 'client-loyalties.index',
                path: '/dashboard/clients/loyalty-points',
                component: ClientLoyaltyIndex
            },
            {
                name: 'client-loyalties.values',
                path: '/dashboard/clients/loyalty-values',
                component: ClientLoyaltyValues
            },
            {
                name: 'client-loyalties.vouchers',
                path: '/dashboard/clients/loyalty-vouchers',
                component: ClientLoyaltyVouchers
            },
            {
                name: 'client-loyalties.redeem',
                path: '/dashboard/clients/loyalty-redeeming/:clientId',
                component: ClientLoyaltyRedeemView
            }
        ],
    }
);

export default router;

router.beforeEach((to, from, next) => {
    AuthUser.hasPermission(to, from, next);
});
