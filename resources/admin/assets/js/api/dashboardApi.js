import axios from 'axios';

export const getClientsStats = function () {
    return axios.get('/api/dashboard/clients-stats');
};

export const getWithdrawalsStats = function () {
    return axios.get('/api/dashboard/withdrawals-stats');
};

export const getInflowsStats = function () {
    return axios.get('/api/dashboard/inflows-stats');
};

export const getAumStats = function () {
    return axios.get('/api/dashboard/aum-stats');
};

export const getMatureWithdrawals = function () {
    return axios.get('/api/dashboard/mature-withdrawals');
};

export const getPrematureWithdrawals = function () {
    return axios.get('/api/dashboard/pre-mature-withdrawals');
};

export const getActiveInvestments = function () {
    return axios.get('/api/dashboard/active-investments');
};

export const getPendingInvestmentTransaction = function () {
    return axios.get('/api/dashboard/pending-investment-transactions');
};

export const getActivePortfolioInvestment = function () {
    return axios.get('/api/dashboard/active-portfolio-investments');
};

export const getPendingPortfolioTransaction = function () {
    return axios.get('/api/dashboard/pending-portfolio-transactions');
};

export const getNotifications = function () {
    return axios.get('/api/dashboard/unread-notifications');
};

export const getLogs = function () {
    return axios.get('/api/activity_log');
};
export const getSpInvestments = function () {
    return axios.get('/api/dashboard/investments/sp_investments');
};
export const getUtfInvestments = function () {
    return axios.get('/api/dashboard/investments/utf_investments');
};
export const getInvestmentsApplications = function () {
    return axios.get('/api/dashboard/investments/investments_application');
};
export const getClientPendingInstructions = function () {
    return axios.get('/api/dashboard/investments/pending_instructions');
};
export const getPendingTransactions = function () {
    return axios.get('/api/dashboard/investments/pending_approvals');
};
