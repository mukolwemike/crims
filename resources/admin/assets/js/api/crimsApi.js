import axios from 'axios';

export const validateJointHolder = function (form) {
    return axios.post('/api/validate/joint-holder', form);
};
