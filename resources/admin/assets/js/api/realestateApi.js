import axios from "axios";

export const saveReserveUnitClient = function (form) {
    return axios.post('/api/realestate/units/reserve', form);
}