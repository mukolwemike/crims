import axios from 'axios';

export const getOrderTypes = function () {
    return axios.get('/api/clients/standing-order-types');
};

export const getProjectHoldings = function (form) {
    return axios.get('/api/realestate/projects/'+form.cid+'/'+form.pid+'/units');
};

export const saveStandingOrder = function (form) {
    return axios.post('/api/client/standing-orders', form);
};

export const getStandingOrders = function (id) {
    return axios.get('/api/client/standing-orders/'+id);
};