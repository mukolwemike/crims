import axios from 'axios';

export const gender = function () {
    return axios.get('/api/gender/list');
};

export const titles = function () {
    return axios.get('/api/contact/titles');
};

export const contactMethods = function () {
    return axios.get('/api/contact/contact_methods');
};

export const businessNatures = function () {
    return axios.get('/api/contact/business_natures');
};

export const employmentTypes = function () {
    return axios.get('/api/contact/employment_types');
};

export const sourceOfFunds = function () {
    return axios.get('/api/contact/source_of_funds');
};

export const countries = function () {
    return axios.get('/api/contact/countries');
};

export const getProducts = function (type) {
    if (type === undefined) {
        type='';
    }
    return axios.get('/api/investments/products');
};

export const getBanks = function () {
    return axios.get('/api/all/banks');
};

export const getUnitFunds = function () {
    return axios.get('/api/unitization/unit-funds-list');
};

export const getAllUnitFunds = function () {
    return axios.get('/api/unitization/unit-funds-all');
};

export const getCISUnitFunds = function () {
    return axios.get('/api/unitization/unit-funds-cis');
};

export const getAllShareEntity = function () {
    return axios.get('/api/unitization/share-entity-all');
};

export const getAllFas = function () {
  return axios.get('/api/commission-recipients/all');
};


