import axios from 'axios';

export const validateRiskAssesment = function (data) {
    return axios.post('/api/applications/validate/risk', data);
};

export const validateInvestmentDetails = function (data) {
    return axios.post('/api/applications/validate/investment', data);
};

export const saveSubscriberDetails = function (data) {
    return axios.post('/api/applications/validate/subscriber', data);
};

export const updateSubscriberDetails = function (data) {
    return axios.post('/api/applications/update-instruction', data);
};

export const kycComplete = function (applId) {
    return axios.post('/api/applications/kyc/complete/'+applId);
};

export const getKycDocuments = function (applId) {
    return axios.post('/api/applications/'+applId);
};

export const uploadPaymentComplete = function (applId) {
    return axios.post('/api/applications/payment/complete/'+applId);
};

export const completeApplication = function (applId) {
    return axios.get('/api/investment/applications/complete/'+applId);
};

export const saveTopup = function (form) {
    return axios.post('/api/instruction/top-up-investment', form);
};

export const getTopupRates = function (item) {
    return axios.post('/api/product/interest-rates', { product_id: item.product_id, tenor: item.tenor, amount: item.amount, clientId: item.client_id });
};

export const getAgreedInterestRate = function (item) {
    return axios.post('/api/product/interest-rates', { product_id: item.product_id, tenor: item.tenor, amount: item.amount, clientId: item.client_id });
};


export const getClientAccounts = function (clientId) {
    return axios.get('/api/investment/'+clientId+'/client-bank-accounts');
};

export const getBankDetails = function (accountId) {
    return axios.get('/api/client/account/'+accountId+'/details');
};

export const getAmountSelected = function (form) {
    let date = (form.withdrawal_stage == "mature")? 'mature': form.withdrawal_date;
    return axios.post('/api/investment/amount/'+form.investment_id, { amount_select: form.amount_select, date: date });
};

export const submitWithdrawal = function (form) {
    return axios.post('/api/investments/withdraw/'+form.investment_id, form);
};

export const getCombinedInvestments = function (invId) {
    return axios.get('/api/combine-investments/'+invId);
};

export const combineSelectedRollover = function (form) {
    return axios.post('/api/investments/combine/'+form.investment_id, form);
};

export const combineFetchedRollover = function (item) {
    return axios.post('/api/investments/combine/fetched/'+item.investment_id, item);
};

export const submitRollover = function (form) {
    return axios.post('/api/investments/roll-over-inv/'+form.investment_id, form);
};

export const updateTopup = function (form) {
    return axios.post('/api/instruction/topup/'+form.id+'/edit', form);
};

export const updateWithdrawalInstruction = function (form) {
    return axios.post('/api/instruction/withdraw/'+form.id+'/edit', form);
};

export const updateRolloverInstruction = function (form) {
    return axios.post('/api/instruction/rollover/'+form.id+'/edit', form);
};

export const saveCustomReport = function (form) {
    return axios.post('/api/investments/custom-report', form);
};

export const getModelColumns = function (form) {
    return axios.post('/api/investments/custom-report/model/columns', form);
};

export const getModelRelations = function (form) {
    return axios.post('/api/investments/custom-report/model/relations', form);
};

export const saveReportColumns = function (form) {
    return axios.post('/api/investments/custom-report/'+form.report_id+'/columns', form);
};

export const saveReportArguments = function (form) {
    return axios.post('/api/investments/custom-report/'+form.report_id+'/arguments', form);
};

export const saveReportFilters = function (form) {
    return axios.post('/api/investments/custom-report/'+form.report_id+'/filters', form);
};

export const getReportFilterTypes = function () {
    return axios.get('/api/investments/custom-reports/filters');
};

export const getReportDetails = function (id) {
    return axios.get('/api/investments/custom-report/'+id);
};

export const exportReport = function (form) {
    return axios.post('/api/investments/custom-report/'+form.id+'/send', form);
};

export const deleteReport = function (id) {
    return axios.get('/api/investments/custom-report/'+id+'/delete');
};
