import axios from 'axios';

export const getFundsForManager = function (id) {
    return axios.get('/api/portfolio/fund-manager/'+id+'/unit-funds');
};

export const getSummaryForAssetClass = function (form) {
    return axios.get('/api/portfolio/fund/'+form.id+'/summary/asset-class?date='+form.date);
};

export const getSummaryForSubAssetClass = function (form) {
    return axios.get('/api/portfolio/fund/'+form.id+'/summary/sub-asset-class?date='+form.date);
};

export const getSummaryForSecurity = function (form) {
    return axios.get('/api/portfolio/fund/'+form.id+'/summary/security?date='+form.date);
};

export const getFundAssets = function (data) {
    return axios.get('/api/portfolio/fund/'+data.id+'/fund-summary?date='+data.date);
};