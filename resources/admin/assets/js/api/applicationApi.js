/**
 * Created by yeric on 10/9/17.
 */
import axios from 'axios';

let url ='/api/unitization/unit-fund-applications';

export const getFormPartials = function () {
    return axios.get('/api/contact/application-form/partials');
};

export const getBanks = function () {
    return axios.get('/api/all/banks');
};

export const getBankBranches = function (bank_id) {
    return axios.get('/api/banks/'+bank_id+'/bank-branches');
};

export const submitApplicationForm = function (form) {
    console.log(form, "this is the form");
    return axios.post(url, form);
};

export const getUnitFundDetails = function (unitFundId) {
    return axios.get('/api/unitization/unit-funds/'+unitFundId);
};

export const getCommissionRecepients = function () {
    return axios.get('/api/investments/commission_recipients/list');
};

export const investApplication = function (form) {
    return axios.post('/api/unitization/application-investment', form);
};

export const saleApplication = function (form) {
    return axios.post('/api/unitization/application-sale', form);
};

export const transferApplication = function (form) {
    return axios.post('/api/unitization/application-transfer', form);
};

export const cancelApplication = function (form) {
    return axios.post('/api/unitization/application-cancellation', form);
};

export const getAllClients = function () {
    return axios.get('/api/clients/all');
};

export const updateApplication = function (form) {
    return axios.post('/api/unit-fund-investment/'+form.id+'/edit', form);
};


