import axios from 'axios';

export const validatingInvestment = function (form) {
    return axios.post('/api/client/investment/application', form);
};

export const saveSubscriber = function (form) {
    return axios.post('/api/client/subscriber/application', form);
};

export const validateContactPerson = function (form) {
    return axios.post('/api/validate/contact-person', form);
};

export const saveContactPerson = function (form) {
    return axios.post('/api/client/contact-persons', form)
};

export const getContactPersons = function (id) {
    return axios.get('/api/client/contact-persons/' + id)
};

export const removeContactPerson = function (id) {
    return axios.get('/api/client/contact-persons/' + id + '/delete')
};

export const validateJointHolder = function (form) {
    return axios.post('/api/applications/joint_holder/validate', form);
};

export const removeJointHolder = function (id) {
    return axios.post('/api/client/joint_holder/' + id + '/delete');
};

export const saveJointHolders = function (form) {
    return axios.post('/api/client/joint_holder/save', form);
};

export const kycDocumentTypes = function (type) {
    return axios.get('/api/kyc-document/types/' + type);
};

export const deleteClientDocument = function (id) {
    return axios.get('/api/client/document/' + id + '/delete');
};

export const setClientApplicationForm = function (id) {
    return axios.get('/api/client/investment/application/' + id + '/edit');
};

export const validatingShareholder = function (form) {
    return axios.post('/api/client/shareholder/application', form);
};

export const getCountries = function () {
    return axios.get('/api/contact/countries', {cache: true});
};

export const saveRiskyClient = function (form) {
    return axios.post('/api/client/risky/create', form);
};

export const getRiskyClient = function (id) {
    return axios.get('/api/client/risky/' + id + '/edit');
};

export const getRiskStatus = function () {
    return axios.get('/api/clients/risky/status', {cache: true});
};

export const getExistingHolder = function (id) {
    return axios.get('/api/client/joint_holder/' + id + '/details/')
};

export const getTaxExemptions = function (id) {
    return axios.get('/api/client/taxExemption/' + id + '/edit');
};

export const getAllCustodialAccounts = function () {
    return axios.get('/api/investments/all-custodial-accounts');
};

export const getShortCustodialAccounts = function () {
    return axios.get('/api/investments/short-name/all-custodial-accounts');
};