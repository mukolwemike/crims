import axios from "axios";

export const saveShareHolder = function (form) {
    return axios.post('/api/shares/shareholder/registration', form);
};