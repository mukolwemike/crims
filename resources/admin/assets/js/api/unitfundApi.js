/**
 * Created by yeric on 10/23/17.
 */
import axios from 'axios';

export const unitFund = function (unitfund) {
    return axios.get('/api/unitization/unit-funds/'+unitfund);
};

export const calculate = function (form) {
    return axios.post('/api/unitization/unit-fund-calculate', form);
};


/**
 * unit fund fee payments
 */

export const getFundFees = function (fundId) {
    return axios.get('/api/unitization/unit-funds/'+fundId+'/unit-fund-fees');
};

export const getFeePaymentRecipients = function () {
    return axios.get('/api/unitization/unit-fund-fee-payment/recipients');
};

export const submitFeePayment = function (form) {
    return axios.post('/api/unitization/unit-funds/'+ form.unit_fund_id +'/unit-fund-fee-payments', form);
};

export const getUnitFundFeePayment = function (form) {
    return axios.get('/api/unitization/unit-funds/'+form.unit_fund_id+'/unit-fund-fees/'+form.fee_id);
};

export const getFunds = function (fundId) {
    return axios.get('/api/unitization/unit-funds/'+fundId+'/unit-fund-list');
};

export const switchFund = function (form) {

    return axios.post('/api/unitization/unit-funds/'+form.fund_id+'/unit-fund-clients/'+form.client_id+'/unit-fund-switches', form);
};

export const reversePurchase = function (form) {
    return axios.post('/api/unitization/unit-funds/'+form.fundId+'/unit-fund-clients/'+form.clientId+'/unit-fund-purchases/reverse', form);
};

export const reverseSale = function (form) {
    return axios.post('/api/unitization/unit-funds/'+form.fundId+'/unit-fund-clients/'+form.clientId+'/unit-fund-sales/reverse', form);
};

export const reverseTransfer = function (form) {
    console.log("api");
    return axios.post('/api/unitization/unit-funds/'+form.fundId+'/unit-fund-clients/'+form.clientId+'/unit-fund-transfers/reverse', form);
};

export const getComplianceList = function () {
    return axios.get('/api/fund/compliance');
};

export const submitSelectedCompliance = function (form) {
    return axios.post('/api/unitization/unit-funds/'+form.fund_id+'/compliance/link', form);
};

export const saveInterestPayment = function (form) {
    return axios.post('/api/unitization/unit-funds/interest-payment-schedule/save', form);
};

export const sendInterestBulkPayment = function (form) {
    return axios.post('/api/unitization/unit-funds/interest-payment-schedule/bulk', form);
};

