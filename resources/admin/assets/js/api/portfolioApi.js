
import axios from 'axios';

export const addNewAssetClass = function (form) {
    return axios.post('/api/portfolio/asset-classes', form);
};

export const getAssetClassDetails = function (assetId) {
    return axios.get('/api/portfolio/asset-classes/'+assetId);
};

export const updateAssetClass = function (form) {
    return axios.put('/api/portfolio/asset-classes/'+form.id, form.form);
};

export const getSubAssetClasses = function (assetCLassId) {
    return axios.get('/api/portfolio/'+assetCLassId+'/sub-asset-classes');
};

export const addSubAssetClasses = function (form) {
    return axios.post('/api/portfolio/'+form.asset_class_id+'/sub-asset-classes/add', form.assets);
};

export const editSubAssetClass = function (form) {
    return axios.post('/api/portfolio/'+form.id+'/edit', form);
};

export const getAssetClasses = function () {
    return axios.get('/api/portfolio/asset-classes');
};

export const getSecurityTypes = function (asset) {
    return axios.get('/api/portfolio/security/'+asset);
};

export const getCustodialAccounts = function (fundManagerId) {
    return axios.get(`/api/fund-managers/${fundManagerId}/custodial-accounts-list`);
};

export const getPortfolioInvestors = function () {
    return axios.get('/api/portfolio/investors');
};

export const saveBonds = function (form) {
    return axios.post('/api/portfolio/securities/'+form.portfolio_security_id+'/bonds', form);
};

export const getBondSecurityDetails = function (params) {
    return axios.get(`/api/portfolio/securities/${params.securityId}/bonds/${params.holdingId}`);
};

export const getPortfolioSecurityDetails = function (securityId) {
    return axios.get('/api/portfolio/securities/'+securityId);
};

export const getFundManagers = function () {
    return axios.get('/api/fund-managers-list');
};

export const saveSecurity = function (form) {
    return axios.post('/api/portfolio/securities', form);
};

export const saveEquityHolding = function (form) {
    return axios.post('/api/portfolio/securities/equities', form);
};

export const buyEquityShares = function (form) {
    return axios.post('/api/portfolio/securities/'+form.id+'/equities/buy', form.form);
};

export const sellEquityShares = function (form) {
    return axios.post('/api/portfolio/securities/'+form.id+'/equities/sell', form.form);
};

export const accrueDividends = function (form) {
    return axios.post('/api/portfolio/securities/'+form.id+'/equities/dividends', form.form);
};

export const saveMarketPriceTrail = function (form) {
    return axios.post('/api/portfolio/securities/'+form.id+'/equities/market-price/add', form.form);
};

export const saveTargetPriceTrail = function (form) {
    return axios.post('/api/portfolio/securities/'+form.id+'/equities/target-price/add', form.form);
};

export const getInvestmentTypes = function () {
    return axios.get('/api/portfolio/investment-types');
};

export const saveDepositHoldings = function (form) {
    return axios.post('/api/portfolio/securities/'+form.portfolio_security_id+'/deposits/add', form);
};

export const getDepositHoldingDetails = function (form) {
    return axios.get('/api/portfolio/securities/'+form.id+'/deposits/'+form.holderId);
};

export const getDepositRepayments = function form()
{
    return axios.get('/api/portfolio/securities/'+form.id+'/'+form.holder_id+'/repay');
};

export const getDepositSummary = function () {
    return axios.get('/api/portfolio/deposit/summary');
};

export const reverseDeposit = function (form) {
    return axios.post('/api/portfolio/securities/deposit-holding/reverse', form);
};

export const rollbackDeposit = function (form) {
    console.log(form, "at the rollback api");
    return axios.post('/api/portfolio/securities/deposit-holding/rollback', form);
};

export const getFunds = function (fund_manager_id) {
    return axios.get('/api/portfolio/'+fund_manager_id+'/funds');
};

export const getUnitFundForManager = function (id) {
    return axios.get('/api/portfolio/security/'+id+ '/unit-funds-for-fund-manager');
};

export const getTaxRates = function () {
    return axios.get('/api/portfolio/deposits/tax-rates');
};

export const getDepositTypes = function () {
    return axios.get('/api/portfolio/deposits/deposit-types');
};

export const submitPortfoliOrder = function (form) {
    return axios.post('/api/portfolio/orders', form);
};

export const getOrderTypes = function () {
    return axios.get('/api/order-types');
};

export const getSecurities = function () {
    return axios.get('/api/all-securities');
};

export const getOrderDetails = function (id) {
    return axios.get('/api/portfolio/orders/'+id);
};

export const getFundOrders = function (id) {
    return axios.get('/api/portfolio/fund-orders/'+id);
};

export const editOrder = function (form) {
    return axios.post('/api/portfolio/orders/'+form.id, form);
};

export const removeOrder = function (form) {
    return axios.post('/api/portfolio/order/'+form.id+'/remove', form);
};

export const getRawPortfolioOrderDetails = function (id) {
    return axios.get('/api/portfolio/raw-orders/'+id);
};

export const saveOrderAllocation = function (form) {
    return axios.post('/api/portfolio/order/'+form.portfolio_order_id+'/allocate', form);
};

export const submitAllocationTransaction = function (form) {
    return axios.post('/api/portfolio/order/allocations/'+form.allocation_id, form);
};

export const settleEquityOrderAllocation = function (form) {
    return axios.post('/api/portfolio/equity-order/settlement', form);
};


export const settleDepositOrderAllocation = function (form) {
    return axios.post('/api/portfolio/deposit-order/settlement', form);
};

export const getSettlementFees = function () {
    return axios.get('/api/portfolio/settlement-fees');
};

export const submitBenchmarkLimits = function (form) {
    return axios.post('/api/portfolio/compliance/benchmarks', form);
};

export const submitLiquidityLimits = function (form) {
    return axios.post('/api/portfolio/compliance/liquidity', form);
};

export const submitNoGoZone = function (form) {
    return axios.post('/api/portfolio/compliance/no-go-zone', form);
};

export const getCompliance = function (id) {
    return axios.get('/api/portfolio/compliance/benchmarks/'+id);
};

export const getComplianceLimit = function (form) {
    return axios.post('/api/portfolio/limits/', form);
};

export const getPortfolioDepositTypes = function () {
    return axios.get('/api/portfolio/portfolio-deposits/deposit-types');
};

export const getRawPortfolioSecurityDetails = function (id) {
    return axios.get('/api/portfolio/raw-securities/'+id);
};

export const editPortfolioSecurity = function (form) {
    return axios.post('/api/portfolio/security/'+form.id+'/edit', form);
};

export const getComplianceTypes = function () {
    return axios.get('/api/portfolio/compliance-types');
};

export const getOrderLifespanTypes = function () {
    return axios.get('/api/portfolio/order-lifespan-types');
};

export const getSectors = function () {
    return axios.get('/api/portfolio/sectors');
};

export const getCountries = function () {
    return axios.get('/api/contact/countries');
};

export const getCurrencies = function () {
    return axios.get('/api/currencies-list');
};

export const getAmortizationSchedule = function (id) {
    return axios.get('/api/portfolio/deposit/'+id+'/loan/amortization');
};

export const getWithholdingTaxes = function(form) {
    return axios.get('/api/investment/withholding-taxes/'+form.custodial_account_id+'/'+form.start+'/'+form.end);
};

export const submitWHTWithdrawal = function (form) {
    return axios.post('/api/investment/withholding-taxes', form);
};