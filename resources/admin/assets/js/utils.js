export const pluck = function (arr, property) {
    arr.reduce(
        [],
        function (result, current) {
            result.push(current[property]);

            return result;
        }
    );
};

export const resolveObject = function (path, obj, def) {

    return path.split('.').reduce(
        function (prev, curr) {
            return prev ? prev[curr] : undefined
        },
        obj || self
    )
};

export const isEmptyObject = function (obj) {
    try {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    } catch (err) {
        return true;
    }
};




