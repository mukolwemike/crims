import Vue from 'vue';

Vue.filter(
    'reverse',
    function (value) {
        return value.split('').reverse().join('');
    }
);

Vue.filter(
    'abs',
    function (value) {
        return Math.abs(value);
    }
);

Vue.filter(
    'formatDate',
    function (value) {
        if (value === null) {
            return '';
        }
        return moment(value).format('MMMM Do YYYY, h:mm:ss a');
    }
);

Vue.filter(
    'booleanIcon',
    function (value) {
        var out = `i class="text-center fa fa-close" style="color: #a94442"></i>`;

        if (value === 1 || value === true) {
            out = `<i class="text-center fa fa-check-square-o" style="color: #3c763d"></i>`;
        } else {
            out = `<i class="text-center fa fa-close" style="color: #a94442"></i>`;
        }
        return out;
    }
);

Vue.filter(
    'booleanYesNo',
    function (value) {
        var out = 'No';

        if (value === 1 || value === true) {
            out = 'Yes';
        } else {
            out = 'No';
        }
        return out;
    }
);

Vue.filter(
    'currency',
    function (value, currency, decimals) {
        value = parseFloat(value);
        if (!isFinite(value) || (!value && value !== 0)) {
            return '';
        }

        decimals = decimals != null ? decimals : 2;

        currency = currency != null ? currency : '';

        var digitsRE = /(\d{3})(?=\d)/g;

        var stringified = Math.abs(value).toFixed(decimals);
        var _int = decimals
            ? stringified.slice(0, -1 - decimals)
            : stringified;

        var i = _int.length % 3;

        var head = i > 0
            ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
            : '';

        var _float = decimals
            ? stringified.slice(-1 - decimals)
            : '';

        var sign = value < 0 ? '-' : '';

        return sign + currency + head +
            _int.slice(i).replace(digitsRE, '$1,') +
            _float;
    }
);

Vue.filter(
    'numberWords',
    function (num) {
        let a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
        let b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
        if (num) {
            if ((num = num.toString()).length > 9) return 'overflow';
            let n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
            if (!n) return;
            let str = '';

            str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
            str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
            str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
            str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
            str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';

            return str;
        }

    }
);