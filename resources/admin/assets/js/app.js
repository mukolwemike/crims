/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
/* eslint-disable */
window.Vue = Vue;
window.events = new Vue();

import Vue from 'vue';

import VueResource from 'vue-resource';
import filters from './filters';

import VueEvents from 'vue-events';
Vue.use(VueEvents);

import router from './router';
import store from './store';
import moment from 'moment';
import VueTelInput from 'vue-tel-input'


require('./bootstrap');
require('./components/components');
require('./filters/filters');
require('./directives/directives');

Vue.use(VueTelInput);
Vue.use(VueResource);

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
    request.headers.set('X-Requested-With', 'XMLHttpRequest',);
    next()
});

Vue.$filters = filters;


window.flash = function (message, level = 'success') {
    window.events.$emit('flash', { message, level });
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */
Vue.component('unit-fund-section', require('./crims/Unitization/index.vue'));

export const app = new Vue({
    mounted() {
        $(document).trigger('vue_is_loaded');
    },
    router,
    store,
    moment,

}).$mount('#crims');
