import moment from 'moment';
import Vue from 'vue';

const roundFilter = function (value, decimals) {
    value = !value ? 0 : value;

    decimals = !decimals ? 0 : decimals;

    return Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
};

const currencyFilter = function (value) {

    return parseFloat(value).toLocaleString();
};

const dateFilter = function (date) {
    return moment(date).format("MMM DD, YYYY");
};

const abbrNum = function (number) {

    let decPlaces = Math.pow(10, 1);

    let abbrev = [ "K", "M", "B", "T" ];

    for (let i=abbrev.length-1; i>=0; i--) {

        let size = Math.pow(10,(i+1)*3);

        if(size <= number) {
            number = Math.round(number*decPlaces/size)/decPlaces;

            if((number == 1000) && (i < abbrev.length - 1)) {
                number = 1;
                i++;
            }

            number += abbrev[i];

            break;
        }
    }

    return number;
};

const numberWordsFilter = function(num) {
    let a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
    let b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

    if (num) {
        if ((num = num.toString()).length > 9) return 'overflow';
        let n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return;
        let str = '';

        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
        str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';

        return str;
    }
};

const uppercaseFilter = function (str) {
    return str ? str.toUpperCase() : str;
};

const capitalizeFilter = function(str) {
    if (typeof str !== 'string') return '';
    return str.charAt(0).toUpperCase() + str.slice(1)
};


Vue.filter('round', roundFilter);

Vue.filter('currency', currencyFilter);

Vue.filter('date', dateFilter);

Vue.filter('numAbbr', abbrNum);

Vue.filter('numberWords', numberWordsFilter);

Vue.filter('uppercase', uppercaseFilter);

Vue.filter('capitalize', capitalizeFilter);

export default {
    round: roundFilter,
    currency: currencyFilter,
    date: dateFilter,
    numAbbr: abbrNum,
    numberWords: numberWordsFilter,
    uppercase: uppercaseFilter,
    capitalize: capitalizeFilter
};