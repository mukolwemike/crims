<!doctype html>
<html lang="en" ng-app="cytonn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Auth check -->
    <meta name="signed-in" content="{{ Auth::check() }}">

    <!-- Auth check -->
    <meta name="user" content="{{ Auth::user() }}">

    <meta name="permissions" content="{{ json_encode((new \Cytonn\Authorization\Authorizer())->permissions()) }}">

    <link rel="shortcut icon" href="/logo.ico?v=2" type="image/x-icon">
    {{--<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic">--}}
    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}
    <title> CRIMS | {!! isset($title)?$title:'Dashboard' !!} </title>

    @include('layouts.imports')

    <noscript>
        <style>
            body *{display:none;}h3,p,a{display:block;text-align:center;}
        </style>
        <h3>JavaScript is not enabled, please check your browser settings.</h3>
        <p>CRIMS requires JavaScript to work correctly. Please contact support if you have issues.</p>
    </noscript>

</head>
<body ng-controller="pageCtrl">

    <div id="crims" class="crims_wrapper">
            @include('layouts.partials.topnav')

            <div class="admin_body">
                @if(Auth::check())
                    <main-body >
                        <div>
                            <div ng-controller="loginCheckCtrl"></div>
                            <div class="content" v-if="$route.name">
                                <transition name="fade">
                                    <router-view></router-view>
                                </transition>
                                <flash message="{{ session('flash') }}"></flash>
                            </div>
                            <div class="content" v-if="!$route.name">
                                @include('notifications.notifier')
                                @yield('content')
                                <flash message="{{ session('flash') }}"></flash>
                            </div>

                            <footer>
                                <div class="footer">
                                    <hr/>
                                    <div class="clearfix"></div>
                                    <div class="navbar-left padding-left-20">
                                        &copy {!! date('Y') !!} Cytonn Investments
                                    </div>
                                    <div class="navbar-right padding-right-20">
                                        CRIMS
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </footer>

                        </div>

                    </main-body>
                @else
                    <div class="col-md-12">
                        <div class="content">
                            @include('notifications.notifier')
                            @yield('content')
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <footer class="col-md-12 footer group">
                        &copy {!! date('Y') !!} Cytonn Investments
                    </footer>

                @endif
            </div>

        </div>

{{--    @include('layouts.partials.splash')--}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134168156-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-134168156-4');
    </script>

    {!! Html::script(mix("/js/manifest.js")) !!}
    {!! Html::script(mix("/js/vendor.js")) !!}
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    {!! Html::script(mix("/js/app.js")) !!}
</body>
</html>

<script>
    $("#flash-overlay-modal").modal();

    $('#myTabs a:first').tab('show');

    if($('.avatar_img').length) {
        $('.avatar_img').initial();
    }

    $(':submit').attr('clicked-disable', true);
    $('a').attr('clicked-disable', true);


    $(document).on('click', function () {
        setTimeout(function () {
            $(':submit').attr('clicked-disable', true);
            $('a').attr('clicked-disable', true);
        }, 1000)
    });

    $(document).click(function () {
        $('.top-nav-menu').hide();
    });

    $(".dropdown-toggle").click(function () {
        $(this).next().toggle();
    });
    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>

<!-- AngularJS Global Templates  -->
<script type = "text/ng-template" id = "pagination.custom.html">
    <nav ng-if="pages.length >= 2">
        <ul class="pagination dmc-pagination">
            <li><a ng-click="selectPage(1)"><i class="fa fa-fast-backward"></i></a></li>
            <li><a ng-click="selectPage(currentPage - 1)"><i class="fa fa-chevron-left"></i></a></li>
            <li><a><page-select></page-select> of <% numPages %></a></li>
            <li><a ng-click="selectPage(currentPage + 1)"><i class="fa fa-chevron-right"></i></a></li>
            <li><a ng-click="selectPage(numPages)"><i class="fa fa-fast-forward"></i></a></li>
        </ul>
    </nav>
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134168156-3"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-134168156-3');
</script>


