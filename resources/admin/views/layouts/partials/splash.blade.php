<div id="loader-wrapper">

    <div id="loader">
        <div class="loader_info">
            <img src="/assets/img/new_logo.svg" alt="">
            <h3>CYTONN CRIMS ADMIN</h3>
            <p class="admin_pinner"></p>
        </div>
    </div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>

<style>
    #loader-wrapper {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1000;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    #loader {
        display: block;
        position: relative;
        min-width: 150px;
        min-height: 150px;
        width: 51%;
        /*margin: -75px 0 0 -75px;*/


        z-index: 1001;
        transform: translateY(-50);
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 51%;
        height: 100%;
        background: #222222;
        z-index: 1000;
        -webkit-transform: translateX(0);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: translateX(0);  /* IE 9 */
        transform: translateX(0);  /* Firefox 16+, IE 10+, Opera */
    }

    #loader-wrapper .loader-section.section-left {
        left: 0;
    }

    #loader-wrapper .loader-section.section-right {
        right: 0;
    }

    /* Loaded */
    .loaded #loader-wrapper .loader-section.section-left {
        -webkit-transform: translateX(-100%);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: translateX(-100%);  /* IE 9 */
        transform: translateX(-100%);  /* Firefox 16+, IE 10+, Opera */

        -webkit-transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1.000);
        transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1.000);
    }

    .loaded #loader-wrapper .loader-section.section-right {
        -webkit-transform: translateX(100%);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: translateX(100%);  /* IE 9 */
        transform: translateX(100%);  /* Firefox 16+, IE 10+, Opera */

        -webkit-transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1.000);
        transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1.000);
    }

    .loaded #loader {
        opacity: 0;
        -webkit-transition: all 0.3s ease-out;
        transition: all 0.3s ease-out;
    }

    #loader .loader_info{
        min-width: 200px;
        background: #fff;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        padding: 20px;
        width: 100%;
        border-top: 4px solid #006a5b;
        border-radius: 5px;

    }
    #loader .loader_info .admin_pinner{
        color: #006a5b;
        margin: 40px auto;
        font-size: 14px;
        width: 1em;
        height: 1em;
        border-radius: 50%;
        position: relative;
        text-indent: -9999em;
        -webkit-animation: load5 1.2s infinite ease;
        animation: load5 1.2s infinite ease;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
    }
    .loaded #loader-wrapper {
        visibility: hidden;

        -webkit-transform: translateY(-100%);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: translateY(-100%);  /* IE 9 */
        transform: translateY(-100%);  /* Firefox 16+, IE 10+, Opera */

        -webkit-transition: all 0.3s 1s ease-out;
        transition: all 0.3s 1s ease-out;
    }


    @-webkit-keyframes load5 {
        0%,
        100% {
            box-shadow: 0em -2.6em 0em 0em #008000, 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.5), -1.8em -1.8em 0 0em rgba(0,128,0, 0.7);
        }
        12.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.7), 1.8em -1.8em 0 0em #008000, 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.5);
        }
        25% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.5), 1.8em -1.8em 0 0em rgba(0,128,0, 0.7), 2.5em 0em 0 0em #008000, 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        37.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.5), 2.5em 0em 0 0em rgba(0,128,0, 0.7), 1.75em 1.75em 0 0em #008000, 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        50% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.5), 1.75em 1.75em 0 0em rgba(0,128,0, 0.7), 0em 2.5em 0 0em #008000, -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        62.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.5), 0em 2.5em 0 0em rgba(0,128,0, 0.7), -1.8em 1.8em 0 0em #008000, -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        75% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.5), -1.8em 1.8em 0 0em rgba(0,128,0, 0.7), -2.6em 0em 0 0em #008000, -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        87.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.5), -2.6em 0em 0 0em rgba(0,128,0, 0.7), -1.8em -1.8em 0 0em #008000;
        }
    }
    @keyframes load5 {
        0%,
        100% {
            box-shadow: 0em -2.6em 0em 0em #008000, 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.5), -1.8em -1.8em 0 0em rgba(0,128,0, 0.7);
        }
        12.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.7), 1.8em -1.8em 0 0em #008000, 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.5);
        }
        25% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.5), 1.8em -1.8em 0 0em rgba(0,128,0, 0.7), 2.5em 0em 0 0em #008000, 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        37.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.5), 2.5em 0em 0 0em rgba(0,128,0, 0.7), 1.75em 1.75em 0 0em #008000, 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        50% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.5), 1.75em 1.75em 0 0em rgba(0,128,0, 0.7), 0em 2.5em 0 0em #008000, -1.8em 1.8em 0 0em rgba(0,128,0, 0.2), -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        62.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.5), 0em 2.5em 0 0em rgba(0,128,0, 0.7), -1.8em 1.8em 0 0em #008000, -2.6em 0em 0 0em rgba(0,128,0, 0.2), -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        75% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.5), -1.8em 1.8em 0 0em rgba(0,128,0, 0.7), -2.6em 0em 0 0em #008000, -1.8em -1.8em 0 0em rgba(0,128,0, 0.2);
        }
        87.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(0,128,0, 0.2), 1.8em -1.8em 0 0em rgba(0,128,0, 0.2), 2.5em 0em 0 0em rgba(0,128,0, 0.2), 1.75em 1.75em 0 0em rgba(0,128,0, 0.2), 0em 2.5em 0 0em rgba(0,128,0, 0.2), -1.8em 1.8em 0 0em rgba(0,128,0, 0.5), -2.6em 0em 0 0em rgba(0,128,0, 0.7), -1.8em -1.8em 0 0em #008000;
        }
    }</style>

<script>
    var ready = false;

    $(document).on('vue_is_loaded', function () {
        ready = true;
        console.log('loaded');
    });

    if (!ready) {
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }
</script>