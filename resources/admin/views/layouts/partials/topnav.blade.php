 <header class="main_top_bar">
        <div class="logo">
            <toogle-menu></toogle-menu>
            <img src="/assets/img/new_logo.svg" alt="">
        </div>
        <nav class="main_menu_area">

            <div class="menu_left">
                {{--<a href="#">{!! isset($title) ? $title : 'Dashboard'  !!}</a>--}}
            </div>

            <div class="menu_right notifications">
                @if(Auth::check())
                    <div class="menu_right_icon margin-top-20" ng-controller="FundManagerSelectCtrl">
                        <?php $fm = App::make(\Cytonn\Investment\FundManager\FundManagerScope::class); ?>
                        {{ Form::select('fund_manager', $fm->getFundManagersArr(), $fm->getSelectedFundManagerId(), ['class'=>'form-control no-margin', 'init-model'=>'selected_fm_id']) }}
                    </div>

                    <div class="menu_right_icon notifications " ng-controller="UnreadNotificationsController">
                        <notifications-modal></notifications-modal>
                    </div>

                    <div class="menu_right_icon notifications " ng-controller="UnreadNotificationsController">
                        <activity-log></activity-log>
                    </div>
                        <div class="menu_right_icon ">
                            <el-dropdown>

                    <span class="el-dropdown-link">
                      <i class="fa fa-user-circle" aria-hidden="true"></i>
                    </span>

                                <el-dropdown-menu slot="dropdown">

                                    <el-dropdown-item disabled>{{ Auth::user()->username }}
                                        - {{ fundManager() ? fundManager()->name : '' }}</el-dropdown-item>

                                    <el-dropdown-item divided>
                                        <a href="/dashboard/profile"> <i class="fa fa-user"></i> Profile</a>
                                    </el-dropdown-item>

                                    <el-dropdown-item>
                                        <a href="/logout"> <i class="fa fa-sign-out"></i> Logout</a>
                                    </el-dropdown-item>

                                </el-dropdown-menu>
                            </el-dropdown>
                        </div>
                @else

                    <div class="menu_right_icon">
                        <div class="icon-wrapper ">
                            <a href="/sso/authorize" class="login">
                                <i class="material-icons">lock</i>
                                <span>Login</span>
                            </a>
                        </div>
                    </div>

                @endif



            </div>
        </nav>
    </header>

