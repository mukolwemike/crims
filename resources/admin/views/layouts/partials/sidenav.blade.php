

@if(0)
<!-- Collect the nav links, forms, and other content for toggling -->
<?php
  $authorizer = new \Cytonn\Authorization\Authorizer();
    $permission = function ($name) {
        return Auth::user()->isAbleTo($name);
    }
?>
<div class="navgroup" ng-controller="sideMenuCtrl">
    <div class="smallmenu">
        <ul class="nav navbar-nav side-nav clearfix" id="actions">
            <li popover-placement="right" uib-popover="Dashboard" popover-trigger="mouseenter">
                <a href="/dashboard"><i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-dashboard"></i> <span ng-class="minimiselabel"> Dashboard</span> </a>
            </li>

            @if($permission('viewclients'))
            <li popover-placement="right" uib-popover="Clients" popover-trigger="mouseenter">
                <a href="/dashboard/clients"><i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-users"></i><span ng-class="minimiselabel">  Clients</span></a>
            </li>
            @endif

            @if($permission('viewinvestments'))
            <li popover-placement="right" uib-popover="Client Investments" popover-trigger="mouseenter">
                <a ng-click="menuexpand('investment')" class="collapsed client_investments" href="/dashboard/investments" data-toggle="collapse" data-target="#client_investments">
                    <i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-table"></i><span ng-class="minimiselabel">  Client Investments<span class="fa arrow"></span></span>
                </a>
            </li>
            @endif
            @if($permission('viewportfolio'))
            <li  popover-placement="right" uib-popover="Portfolio" popover-trigger="mouseenter">
                <a ng-click="menuexpand('portfolio')" class="collapsed portfolio" href="/dashboard/portfolio" data-toggle="collapse" data-target="#portfolio">
                    <i  ng-class="minimiseicon" class="fa fa-fw fa-institution" toggle-active=""></i> <span ng-class="minimiselabel"> Portfolio  <span class="fa arrow"></span></span>
                </a>
            </li>
            @endif

            <li popover-placement="right" uib-popover="Cytonn Shares" popover-trigger="mouseenter">
                <a href="/dashboard/shares"><i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-buysellads"></i><span ng-class="minimiselabel">  Shares</span></a>
            </li>

            <li popover-placement="right" uib-popover="Real Estate" popover-trigger="mouseenter">
                <a href="/dashboard/realestate"><i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-building-o"></i><span ng-class="minimiselabel">  Real Estate</span></a>
            </li>

            @if($permission('manageclientusers') || $permission('manageusers'))
            <li popover-placement="right" uib-popover="User Management" popover-trigger="mouseenter">
                <a ng-click="menuexpand('users')" class="collapsed users" href="/dashboard/users/clients" data-toggle="collapse" data-target="#users">
                    <i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-users"></i> <span ng-class="minimiselabel"> User management  <span class="fa arrow"></span></span>
                </a>
            </li>
            @endif
            {{--<li><a href="/dashboard/setup"><i class="nav-icon fa fa-gear"></i> Setup</a> </li>--}}
        </ul>
    </div>


    <div class="bigmenu">
        <ul class="nav navbar-nav side-nav clearfix" id="actions">
            <li>
                <a href="/dashboard"><i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-dashboard"></i> <span ng-class="minimiselabel"> Dashboard</span> </a>
            </li>

            @if($permission('viewclients'))
            <li>
                <a href="/dashboard/clients"><i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-users"></i><span ng-class="minimiselabel">  Clients</span></a>
            </li>
            @endif

            @if($permission('viewinvestments'))
            <li>
                <a ng-click="menuexpand('investment')"  href="javascript:;" class="collapsed client_investments" data-toggle="collapse" data-target="#client_investments">
                    <i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-table"></i><span ng-class="minimiselabel">  Client Investments<span class="fa arrow"></span></span>
                </a>

                <ul id="client_investments" class="collapse">
                    <li><a href="/dashboard/investments"><span class="nav-item-label">Menu</span></a></li>
                    {{--<li><a href="/dashboard/investments/applications"> <span class="nav-item-label">Applications</span></a></li>--}}
                    <li><a href="/dashboard/investments/client-instructions"> <span class="nav-item-label">Applications</span></a></li>
                    <li><a href="/dashboard/investments/clientinvestments"> <span class="nav-item-label">Investments</span></a> </li>
                    <li><a href="/dashboard/clients/summary"> <span class="nav-item-label">Summary</span></a> </li>
                </ul>
            </li>
            @endif

            @if($permission('viewportfolio'))
            <li>
                <a ng-click="menuexpand('portfolio')" href="javascript:;" class="collapsed portfolio" data-toggle="collapse" data-target="#portfolio">
                    <i  ng-class="minimiseicon" class="fa fa-fw fa-institution" toggle-active=""></i> <span ng-class="minimiselabel"> Portfolio  <span class="fa arrow"></span></span>
                </a>

                <ul id="portfolio" class="collapse">
                    <li><a href="/dashboard/portfolio">Menu</a></li>
                    <li><a href="/dashboard/portfolio/deposit-holdings">Investments</a> </li>
                    <li><a href="/dashboard/portfolio/custodials">Custodial Accounts</a></li>
                    <li><a href="/dashboard/portfolio/summary">Summary</a> </li>
                </ul>
            </li>
            @endif
            <li>
                <a href="/dashboard/realestate"><i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-building-o"></i><span ng-class="minimiselabel">  Real Estate</span></a>
            </li>

            @if($permission('manageclientusers') || $permission('manageusers'))
            <li>
                <a ng-click="menuexpand('users')"  href="javascript:;" class="collapsed users" data-toggle="collapse" data-target="#users">
                    <i  ng-class="minimiseicon" class="nav-icon fa fa-fw fa-users"></i> <span ng-class="minimiselabel"> User management  <span class="fa arrow"></span></span>
                </a>

                <ul id="users" class="collapse">
                    <li><a href="/dashboard/users">Users</a></li>
                    <li><a href="/dashboard/users/clients">Clients</a></li>

                    <li><a href="/dashboard/users/permissions">Permissions</a></li>
                    <li><a href="/dashboard/users/roles">Roles</a></li>
                </ul>
            </li>
            @endif
            {{--<li><a href="/dashboard/setup"><i class="nav-icon fa fa-gear"></i> Setup</a> </li>--}}
            <li>

            </li>
        </ul>
    </div>
    <div class="sidebar-footer">
        <a href="#" class="sidebar-toggle" id="sidebar-toggle" ng-click="toggleSidebar($event)">
            <i ng-class="toggle_state"></i>
        </a>
    </div>
</div>
@endif
