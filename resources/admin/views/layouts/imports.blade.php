{{--<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-default/index.css">--}}
{!! HTML::script('/assets/js/jquery.min.js') !!}
{!! HTML::script('/assets/js/angular.min.js') !!}
{!! HTML::script('/assets/js/bootstrap.min.js') !!}
{!! HTML::script('/assets/js/dropdowns-enhancement.js') !!}
{!! HTML::script('/assets/lib/allmighty-autocomplete/script/autocomplete.js') !!}
{!! HTML::script('/assets/lib/angular-modal-service/angular-modal-service.min.js') !!}
{!! HTML::script('/assets/lib/angular-smart-table/smart-table.min.js') !!}
{!! HTML::script('/assets/lib/angular-animate/angular-animate.min.js') !!}
{!! HTML::script('/assets/lib/ui-bootstrap/ui-bootstrap-custom-tpls-0.13.4.min.js') !!}
{!! HTML::script('/assets/lib/ui-bootstrap-tpls-0.14.3.min.js') !!}
{!! HTML::script('/assets/js/angular-local-storage.min.js') !!}
{!! HTML::script('/assets/js/Chart.min.js') !!}
{!! HTML::script('/assets/js/moment.js') !!}
{!! HTML::script('/assets/js/angular-moment.min.js') !!}
{!! HTML::script('/assets/js/angular-chart.min.js') !!}
{!! HTML::script('/assets/lib/summernote/summernote.min.js') !!}
{!! HTML::script('/assets/lib/tinymce/tinymce.min.js') !!}
{!! HTML::script('/assets/js/angular-summernote.min.js') !!}
{!! HTML::script('/assets/js/angular-sanitize.min.js') !!}
{!! HTML::script('/assets/js/angular-toastr.js') !!}
{!! HTML::script(mix('/js/dependencies.js')) !!}
{!! HTML::script(mix('/js/cytonn.js')) !!}


{!! HTML::style('/assets/css/dropdowns-enhancement.css') !!}
{!! HTML::style('/assets/lib/allmighty-autocomplete/style/autocomplete.css') !!}
{!! HTML::style('/assets/css/angular-chart.min.css') !!}
{!! HTML::style('/assets/lib/summernote/summernote.css') !!}
{!! HTML::style('//cdn.jsdelivr.net/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css') !!}
{!! HTML::style(mix('/css/app.css')) !!}
{!! HTML::style('/css/select2.css') !!}
{!! HTML::style('/css/select2.new.css') !!}
{!! HTML::style('/assets/css/angular-toastr.css') !!}


