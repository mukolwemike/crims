@extends('layouts.default')
@section('content')
    <div class = "panel-dashboard">
        <div ng-controller = "ContactsGridCtrl">
            <table st-table = "displayedCollection" st-safe-src = "rowCollection"
                   class = "table table-striped table-responsive">
                <thead>
                <tr>
                    <th colspan = "1"><a class = "margin-bottom-20 pull-left btn btn-success" href = "/dashboard/contacts/create"> <i class = "fa fa-plus"></i> Add new</a></th>
                    <th colspan = "2"></th>
                    <th colspan = "2"><input st-search = "" class = "form-control" placeholder = "Search..." type = "text"/></th>
                </tr>
                <tr>
                    <th st-sort = "id">ID</th>
                    <th st-sort = "fullName">Name</th>
                    <th st-sort = "email">Email Address</th>
                    <th st-sort = "phone">Phone Number</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat = "row in displayedCollection">
                    <td><% row.id %></td>
                    <td><% row.fullName %></td>
                    <td><% row.email %></td>
                    <td><% row.phone %></td>
                    <td><a ng-controller = "PopoverCtrl" uib-popover = "Edit contacts details"
                           popover-trigger = "mouseenter" href = "/dashboard/contacts/create/<%row.id%>"><i
                                    class = "fa fa-edit"></i></a></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan = "2" class = "text-center">
                        Items per page
                    </td>
                    <td colspan = "2" class = "text-center">
                        <input type = "text" ng-model = "itemsByPage"/>
                    </td>
                    <td colspan = "4" class = "text-center">
                        <div st-pagination = "" st-items-by-page = "itemsByPage" st-displayed-pages = "10"></div>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
@stop