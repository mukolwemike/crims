
<h3>Add a new contact</h3>
{!! Form::model($contact, ['url'=>'Dashboard'.$contact->id, $contact->id]) !!}

        <div class="detail-group">
            <div class="form-group">
                <div class="alert alert-info">Select contact type you are adding, whether individual or company (corporate)</div>

                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('entity_type_id', 'Select Contact type') !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('entity_type_id', ContactEntity::lists('name', 'id'), null, ['init-model'=>'individual', 'class'=>'form-control']) !!}
                    </div>
                </div>

            </div>
        </div>

        <div class="clearfix"></div>
        <div class="detail-group">
            <div class="col-md-4" ng-hide="individual == 1">
                <div class="form-group">
                    {!! Form::label('corporate_registered_name', 'Registered name') !!}

                    {!! Form::text('corporate_registered_name', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_registered_name') !!}
                </div>
            </div>

            <div class="col-md-4"  ng-hide="individual == 1">
                <div class="form-group">
                    {!! Form::label('corporate_trade_name', 'Trade name') !!}

                    {!! Form::text('corporate_trade_name', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_trade_name') !!}
                </div>
            </div>

            <div class="col-md-4"  ng-hide="individual == 1">
                <div class="form-group">
                    {!! Form::label('registered_address', 'Registered address') !!}

                    {!! Form::text('registered_address', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_address') !!}
                </div>
            </div>

            <div class="col-md-4" ng-show="individual == 1">
                <div class="form-group">
                    {!! Form::label('title_id', 'Title') !!}

                    {!! Form::select('title_id', Title::lists('name', 'id'), null,  ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title_id') !!}
                </div>
            </div>

            <div class="col-md-4" ng-show="individual == 1">
                <div class="form-group">
                    {!! Form::label('firstname', 'First name') !!}

                    {!! Form::text('firstname', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}
                </div>
            </div>

            <div class="col-md-4"  ng-show="individual == 1">
                <div class="form-group">
                    {!! Form::label('lastname', 'Last name') !!}

                    {!! Form::text('lastname', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-4"  ng-show="individual == 1">
                <div class="form-group">
                    {!! Form::label('middlename', 'Middlename') !!}

                    {!! Form::text('middlename', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('phone', 'Phone number') !!}

                    {!! Form::text('phone', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('email', 'Email address') !!}

                    {!! Form::text('email', null, ['class'=>'form-control'])!!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('country_id', 'Country') !!}

                    {!! Form::select('country_id', Country::lists('name', 'id'),null,  ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'country_id') !!}
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="detail-group">
            <div class="col-md-4" ng-show="individual">
                <div class="form-group">
                    {!! Form::label('company_id', 'Company') !!}

                    <div ng-controller="SuggestCompanies">
                        <input type="hidden" name="company" ng-model="selectedcompany" value="<% selectedcompany %>"/>
                        {!! Form::hidden('company_id', null, ['ng-model'=>'initcompany'])!!}

                        <autocomplete attr-placeholder="Company name" attr-class="" attr-input-class="form-control" ng-model="selectedcompany" data="companies" on-type=""></autocomplete>
                    </div>
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'company_id') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('contact_category_id', 'Contact category') !!}

                    {!! Form::select('contact_category_id', ContactCategory::lists('name', 'id'),null,  ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_category_id') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('contact_type_id', 'Contact type') !!}

                    {!! Form::select('contact_type_id', ContactType::lists('name', 'id'),null,  ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_type_id') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('relationship_contact', 'Relationship contact') !!}

                    {!! Form::select('relationship_contact', \Cytonn\Presenters\StaffPresenters::StaffArray(),null,  ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'relationship_contact') !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('contact_originator', 'Contact originator') !!}

                    {!! Form::select('contact_originator', \Cytonn\Presenters\StaffPresenters::StaffArray(),null,  ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_originator') !!}
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="detail-group">
            <div class="form-group">
                <div class="col-md-2">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success btn-block']) !!}
                </div>
            </div>
        </div>

{!! Form::close() !!}
