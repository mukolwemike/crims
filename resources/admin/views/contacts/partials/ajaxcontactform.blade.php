<div ng-controller="contactForm">
    {!! Form::model($contact, ['url'=>'Dashboard'.$contact->id, $contact->id, 'ng-submit'=>'processForm()' ] )!!}

    <div class="col-md-4">{!! Form::label('firstname', 'First name') !!}</div>

    <div class="col-md-8">{!! Form::text('firstname', null, ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('lastname', 'Last name') !!}</div>

    <div class="col-md-8">{!! Form::text('lastname', null, ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('othernames', 'Other names') !!}</div>

    <div class="col-md-8">{!! Form::text('othernames', null, ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('email', 'Email address') !!}</div>

    <div class="col-md-8">{!! Form::text('email', null, ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('phone', 'Phone number') !!}</div>

    <div class="col-md-8">{!! Form::text('phone', null, ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('country_id', 'Country') !!}</div>

    <div class="col-md-8">{!! Form::select('country_id', Country::lists('name', 'id'),null,  ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('company_id', 'Company') !!}</div>

    <div class="col-md-8">
        <div ng-controller="SuggestCompanies">
            <input type="hidden" name="company" ng-model="selectedcompany" value="<% selectedcompany %>"/>
            {!! Form::hidden('company_id', null, ['ng-model'=>'initcompany'])!!}

            <autocomplete attr-placeholder="Company name" attr-class="" attr-input-class="form-control" ng-model="selectedcompany" data="companies" on-type=""></autocomplete>
        </div>
    </div>

    <div class="col-md-4">{!! Form::label('contact_category_id', 'Contact category') !!}</div>

    <div class="col-md-8">{!! Form::select('contact_category_id', ContactCategory::lists('name', 'id'),null,  ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('contact_type_id', 'Contact type') !!}</div>

    <div class="col-md-8">{!! Form::select('contact_type_id', ContactType::lists('name', 'id'),null,  ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('relationship_contact', 'Relationship contact') !!}</div>

    <div class="col-md-8">{!! Form::select('relationship_contact', Staff::lists( 'id'),null,  ['class'=>'form-control']) !!}</div>

    <div class="col-md-4">{!! Form::label('relationship_contact', 'Contact originator') !!}</div>

    <div class="col-md-8">{!! Form::select('relationship_contact', Staff::lists('id'),null,  ['class'=>'form-control']) !!}</div>

    <div class="col-md-4 col-md-offset-4">
        <button type="submit" class="btn btn-success btn-lg btn-block" ng-click="processForm()">Submit</button>
        <!-- {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!} -->
    </div>
    {!! Form::close() !!}

</div>