@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            @include('contacts/partials/addcontactform')
        </div>
    </div>
@stop