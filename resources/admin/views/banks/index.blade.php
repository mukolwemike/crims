@extends('layouts.default')
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <div ng-controller="BanksGridController">
                <a class="btn btn-default pull-right margin-bottom-20 margin-left-20" href="/dashboard/banks/create"><i class="fa fa-plus-square-o"></i> Add Bank</a>
                <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <th colspan="3"></th>
                            <th colspan="2">
                                <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                            </th>
                        </tr>
                        <tr>
                            <th st-sort="id">ID</th>
                            <th>Name</th>
                            <th>Swift Code</th>
                            <th>Clearing Code</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.name %></td>
                        <td><% row.swiftCode %></td>
                        <td><% row.clearingCode %></td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default btn-xs" href="/dashboard/banks/<% row.id %>"><i class="fa fa-list-alt"></i> View</a>
                                <a class="btn btn-default btn-xs" href="/dashboard/banks/<% row.id %>/edit"><i class="fa fa-pencil-square-o"></i> Edit</a>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@stop