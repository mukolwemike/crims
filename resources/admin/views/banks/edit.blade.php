    @extends('layouts.default')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-dashboard">
            <h3>Edit Bank</h3>

            {!! Form::model($bank, ['route'=>['update_bank', $bank->id], 'method'=>'put']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Name') !!}

                {!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}

                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
            </div>

            <div class="form-group">
                {!! Form::label('swift_code', 'Swift Code') !!}

                {!! Form::text('swift_code', null, ['class'=>'form-control', 'required']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'swift_code') !!}
            </div>

            <div class="form-group">
                {!! Form::label('clearing_code', 'Clearing Code') !!}

                {!! Form::text('clearing_code', null, ['class'=>'form-control', 'required']) !!}
                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'clearing_code') !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection