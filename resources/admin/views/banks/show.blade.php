@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-dashboard">
            <h3>Bank</h3>
            <table class = "table table-hover">
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $bank->name !!}</td>
                    </tr>
                    <tr>
                        <td>Postal Code</td>
                        <td>{!! $bank->swift_code !!}</td>
                    </tr>
                    <tr>
                        <td>Postal Address</td>
                        <td>{!! $bank->clearing_code !!}</td>
                    </tr>
                    <tr>
                        <td>Actions</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default" href="/dashboard/banks/{!! $bank->id !!}/edit"><i class="fa fa-pencil-square-o"></i> Edit Bank</a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="panel-dashboard">
            <div ng-controller="BankBranchesGridController">
                <button class="btn btn-primary" data-toggle="modal" data-target="#add-branch"><i class="fa fa-plus-square-o"></i> Add Branch</button>
                <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <th colspan="4"></th>
                            <th>
                                <input st-search="" class="form-control" placeholder="Search..." type="text"/>
                            </th>
                        </tr>
                        <tr>
                            <th st-sort="id">ID</th>
                            <th>Name</th>
                            <th>Swift Code</th>
                            <th>Branch Code</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody  ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% row.id %></td>
                        <td><% row.name %></td>
                        <td><% row.swiftCode %></td>
                        <td><% row.branchCode %></td>
                        <td>
                            <button class="btn btn-default" data-toggle="modal" data-target="#edit-branch-<% row.id %>"><i class="fa fa-plus-square-o"></i> Edit</button>

                            <div class="modal fade" id="edit-branch-<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Branch</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(['route'=>['update_bank_branch', $bank->id], 'method'=>'put']) !!}

                                            <input type="text" style="display: none;" ng-model="row.id" name="branch_id">

                                            <div class="form-group">
                                                {!! Form::label('name', 'Branch Name') !!}
                                                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Branch Name', 'ng-model'=>'row.name', 'ng-required'=>"true"]) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('swift_code', 'Swift Code') !!}
                                                {!! Form::text('swift_code', null, ['class'=>'form-control', 'ng-model'=>'row.swiftCode', 'placeholder'=>'Swift Code']) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('branch_code', 'Branch Code') !!}
                                                {!! Form::text('branch_code', null, ['class'=>'form-control', 'ng-model'=>'row.branchCode', 'placeholder'=>'Branch Code']) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                            </div>

                                            {!! Form::close() !!}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="100%" class="text-center">Loading ... </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="100%"><dmc-pagination></dmc-pagination></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="modal fade" id="add-branch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Branch</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['route'=>['create_bank_branch', $bank->id]]) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Branch Name') !!}
                            {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Branch Name', 'ng-required'=>"true"]) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('swift_code', 'Swift Code') !!}
                            {!! Form::text('swift_code', null, ['class'=>'form-control', 'placeholder'=>'Swift Code']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('branch_code', 'Branch Code') !!}
                            {!! Form::text('branch_code', null, ['class'=>'form-control', 'placeholder'=>'Branch Code']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
