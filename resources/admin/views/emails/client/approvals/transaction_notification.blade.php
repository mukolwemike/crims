@extends('emails.email')

@section('content')
    <p>A transaction for the client {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($approval->client_id) !!} has been approved.</p>

    <p>The following are the details of the transaction:</p>

    <table class="table table-responsive table-striped">
        <tbody>
            <tr>
                <td>Sent By</td><td>{!! \Cytonn\Presenters\UserPresenter::presentFullNamesNoTitle($approval->sent_by) !!}</td>
            </tr>
            <tr>
                <td>Approved By</td><td>{!! \Cytonn\Presenters\UserPresenter::presentFullNamesNoTitle($approval->approved_by) !!}</td>
            </tr>
            <tr>
                <td>Transaction Type</td><td>{!! $approval->present()->type !!}</td>
            </tr>
            <tr>
                <td>Approved On</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDateTime($approval->approved_on) !!}</td>
            </tr>
            @foreach($details as $key=>$val)
                <tr>
                    <td>{!! $key !!}</td><td>{!! $val !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p>Please view the transaction <a href="{!! getenv('DOMAIN') !!}/dashboard/investments/approve/{!! $approval->id !!}">here</a></p>
@endsection