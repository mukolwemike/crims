@extends('emails.default')

@section('content')
    <p>
        Dear {{ \Cytonn\Presenters\ClientPresenter::presentFirstName($client->id) }},
        your request to redeem <b>{{$amount}}</b> points, valued at <b>KES {{$amount}}</b> from your total points of
        <b>{{$totalPoints}}</b> has been processed.
    </p>

    @if(count($voucherDoc))
        @foreach($voucherDoc as $key=>$doc)
            @if($doc)
                <p>
                    <b>{{$key}}</b> voucher has been attached.
                </p>
            @else
                <p>
                    Please visit the nearest Cytonn Investements Offices to collect your <b>{{$key}}</b> voucher or call +254(0)709-101-200 to make
                    necessary arrangements.
                </p>
            @endif
        @endforeach
    @else
        <p>
            Please visit the nearest Cytonn Investements Offices to collect your voucher. For more information,
            call us on +254(0)709-101-200 or email at clientservices@cytonn.com.
        </p>
    @endif

    <p>Regards,</p>

    <p>Thank you for doing business with us.</p>
@endsection