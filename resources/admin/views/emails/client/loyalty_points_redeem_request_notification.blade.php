@extends('emails.default')

@section('content')
    <p>
        Dear {{ \Cytonn\Presenters\ClientPresenter::presentFirstName($client->id) }},
        we have received your instructions to redeem {{$amount}} points worth KES. {{$amount}} for {{$voucherNames}}
        and we shall reach out to you within 48 hours and advise further.
    </p>

    <p>Regards,</p>

    <p>Thank you for doing business with us.</p>
@endsection