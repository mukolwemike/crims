@extends('emails.email')

@section('content')
    <div>
        <p> Below is a list of the client with birthdays within a period of 14 days to come: </p>
        <p> For a downloadable version, please find attached a copy of the same. </p>
        <br>
        <table>
            <thead>
            <tr>
                <th>Client Names</th>
                <th>Birthday</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{!! $client->Name !!}</td>
                    <td>{!! $client->Date !!}</td>
                    <td>{!! $client->Email !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <br>
        <p>Kind regards,</p>
        <p>Cytonn CRIMS </p>
    </div>
@endsection