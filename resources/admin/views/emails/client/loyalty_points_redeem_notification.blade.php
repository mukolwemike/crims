@extends('emails.default')

@section('content')
    <p>
        <b>{{ \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) }} - {{$client->client_code}}</b>
        has redeemed points <b>{{$amount}}</b>, valued at <b>KES {{$amount}}</b> from their total points
        of <b>{{$totalPoints}}</b>. Reference ref <b>{{$refId}}</b>.
    </p>

    <p>Vouchers: <b>{{$voucherNames}}</b></p>

    <p>
        @if(($client->joint || $client->type->name == 'corporate') && is_null($instruction->approval_id))
            An approval instruction has been made, kindly process the instruction.
        @else
            The voucher details have been shared to the client. Please note the vouchers with no uploads require the
            client to physically pick them from Cytonn Offices. Please link up with client.
        @endif
    </p>
    <p>Regards,</p>

    <p class="bold-underline"><strong>Cytonn CRIMS</strong></p>
@endsection