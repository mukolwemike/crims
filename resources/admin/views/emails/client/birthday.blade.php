@extends('emails.client_default')

@section('banner')
    <img style="width: 100%; margin: 0" src="{!! $message->embed(storage_path('resources/assets/bday_shots/'.$image))  !!}" alt="Happy birthday!">
@endsection