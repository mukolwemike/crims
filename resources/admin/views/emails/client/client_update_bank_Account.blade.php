@extends('emails.default')

@section('content')

    <p>Your request to change bank account details has been successfully approved.</p>

    <ul>
        <li> Client Code: {{$client->client_code}}</li>
        <li> Client Name:{{\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id)}}</li>
        <li> Account Name : <b>{{$account->account_name}}</b></li>
        <li> Account Number : <b>{{$account->account_number}}</b></li>
    </ul>

    <p>View details on your profile.</p>

    <p>Regards,</p>

    <p class="bold-underline"><strong>Cytonn CRIMS</strong></p>
@endsection