@extends('emails.email_plain')

@section('content')
    <p>Dear {{ $client_names }},</p>

    <p>To provide security and confidentiality, the documents we send you (business confirmations and statements) will be
        password protected.
    </p>
    <p>
        When prompted for a password, please enter the password given below. <br>
        <b>
            Client Code: {{ $client->client_code }} <br>
            Password:     {{ $client->repo->documentPassword() }} <br>
        </b>
    </p>

    <p style="font-style: italic; font-weight: 600">
        The password will be shared only once. Therefore, to access the subsequent statements and confirmations,
        please make a point of memorizing the password upon receipt and discard it thereafter for security purposes.
    </p>
    <p>
        We thank you for your continued support and for choosing @if(!isset($company)) Cytonn @else Cytonn Asset Managers @endif as your preferred Investment Manager.
    </p>
    <p>
        Kind regards,
    </p>
    <p>
        {{ $sign_off }}
    </p>
@endsection