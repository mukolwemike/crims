@extends('emails.client_default')

@section('content')
    <p>Dear {{ $client->contact->firstname }}, </p>

    <p>
        Please submit your pending KYC documents for the investment in {{ $fund->name }}.
    </p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <th>Document Name</th>
        </tr>
        </thead>
        <tbody>
        @foreach($checklist as $kyc)
            <tr>
                <td>{{ $kyc->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    
    <p>
        You can submit them by logging into our online portal <a href="https://clients.cytonn.com/">here</a> or mobile
        app and accessing the profile page.
    </p>

    <p>
        Kindly note that your investment will not earn any interests if they remain non-compliant.
    </p>

    <p>Kind regards,</p>

    @if(!isset($companyAddress))
        <p>
            {!! $emailSender !!}
        </p>
    @else
        <p>Pauline Ayoki </p>
        <p>Administrator </p>
        <p>Mobile : 0714143588</p>
        <p>Email : <a href="payoki@serianiasset.com">payoki@serianiasset.com</a></p>
    @endif
@endsection

<style>
    table{
        border-collapse: collapse;
        margin-top:10px;
        margin-bottom:10px;
        width:95% !important;
    }
    .table-striped tbody>tr:nth-of-type(even){
        background-color:#e3e3e3 !important;
    }
    table tr td, table tr th{
        border:1px solid black;
        padding-left: 5px;
        padding-right: 5px;
        padding-top:3px;
        padding-bottom: 3px;
    }
    table thead tr, table .header td{
        background-color:#006666 !important;
        color: #FFF !important;
    }
</style>