@extends('emails.email_plain')

@section('content')

    <p>Please find attached an excel sheet of business confirmations summary for {!! \Cytonn\Presenters\DatePresenter::formatDate($today->toDateString()) !!} :</p>

@endsection