<!doctype html>
<html lang="en">
<body style="background: #d1d1d1;">
<section style="width: 400px; background: white; margin: auto;">
    <header style="width: 100%;height: 217px;z-index: 2;background: url(https://cytonn.sheerhr.com/assets/images/birthday.png);background-repeat: no-repeat;background-size: contain;">
        <h1 style="margin-bottom: 23px; width: 50%;float: left;margin-left: 28%;margin-top: 70px;color: #007c67;font-family: cursive;">Happy Birthday</h1>
        <div id="prof" style="width: 130px;height: 130px;margin: auto;border-radius: 100%;z-index: 5;overflow: hidden;">
            <img src="{!! $message->embed(public_path() . '/assets/img/avatar.jpg') !!}" style="width: 100%;">
        </div>
    </header>

    <div id="content" style="margin-bottom: 40px; margin-top: 80px;">
        <h2 style="margin: auto; text-align: center; font-size: 17px;font-family: sans-serif;margin-top: 20px;font-weight: 700;">Happy Birthday, {!! $firstname !!} !!</h2>
        <h5 style="margin: auto;margin-top: 10px;font-size: 12px;text-align: center;color: #909897;font-weight: 100;font-family: sans-serif;"></h5>
        <p style="width: 70%;margin: auto;text-align: center;font-family: sans-serif;font-weight: 100;margin-top: 20px;font-size: 12px;">{!! $line !!}</p>
        <h3 style="margin: auto;margin-top: 30px;font-size: 13px;text-align: center;color: #15705d;font-family: sans-serif;">Cytonn Client Services</h3>
    </div>

    <footer style="background: #f2f2f3;height: 50px;">
        <div id="logo" style="float: left;">
            <img src="{!! $message->embed(public_path() . '/assets/img/logo.png') !!}" style="width: 90px;margin-left: 10px;margin-top: 14px;">
        </div>
        <h4 style="margin: auto;text-align: left;font-family: sans-serif;font-weight: 100;margin-top: 20px;font-size: 10px;margin-right: 15px;float: right;color: #808080;">We wish you and many more to come </h4>
    </footer>
</section>

</body>

</html>