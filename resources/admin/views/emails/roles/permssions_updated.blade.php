@extends('emails.client_default')

@section('content')
    <div>
        <h3>CRIMS Role Permissions Updated</h3>
        <p>Dear Team,</p>
        <p>The permissions for the role under the following details have been updated</p>
        <table>
            <tbody>
            <tr>
                <td><b style="font-weight: bold">Name:</b></td>
                <td>{!! $name !!}</td>
            </tr>
            <tr>
                <td><b style="font-weight: bold">Description:</b></td>
                <td>{!! $description !!}</td>
            </tr>
            <tr>
                <td><b style="font-weight: bold">Updated By:</b></td>
                <td>{!! $user !!}</td>
            </tr>
            </tbody>
        </table>
        <p>Kindly follow this <a href="{{ URL::to('dashboard/users/roles/details/'. $id) }}">link</a> to view the role details and the permissions</p>

        Regards,<br/>
        Cytonn CRIMS.
        </p>
    </div>
@endsection