@extends('emails.client_default')

@section('content')
    <div>
        <h3>CRIMS New Role</h3>
        <p>Dear Team,</p>
        <p>A new role under the following details has been added to the system</p>
        <table>
            <tbody>
            <tr>
                <td><b style="font-weight: bold">Name:</b></td>
                <td>{!! $name !!}</td>
            </tr>
            <tr>
                <td><b style="font-weight: bold">Description:</b></td>
                <td>{!! $description !!}</td>
            </tr>
            <tr>
                <td><b style="font-weight: bold">Added By:</b></td>
                <td>{!! $user !!}</td>
            </tr>
            </tbody>
        </table>
        <p>Kindly follow this <a href="{{ URL::to('dashboard/users/roles/details/'. $id) }}">link</a> to view the role details.</p>

        Regards,<br/>
        Cytonn CRIMS.
        </p>
    </div>
@endsection