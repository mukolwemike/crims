@extends('emails.client_default')

@section('content')
    <p>
        Hello, <br>
        You have been registered as a Financial Advisor on CRIMS.
        We have  assigned you a referral code <b>{{ $recipient->referral_code }}</b>
        that your clients can use when making investment applications on:
    </p>
    <ol>
        <li>Our Clients portal <a href="https://clients.cytonn.com">clients.cytonn.com</a></li>
        <li>Our Mobile Applications (Android/iOS)</li>
        <li>Our USSD Code *809#</li>
    </ol>

    <p>
        Your assigned code is <b>{{ $recipient->referral_code }}</b>.
        Your clients will need to enter this as a referrer, then all commission from this client will be assigned to you.
    </p>
    <p>
        We encourage that all individual money market applications are be done on these platforms, so that they are processed instantly.
    </p>
    <p>
        Contact us in case of any queries
    </p>
    <p>
        Regards,<br/>

        <span class="bold-underline"> CRIMS </span>
    </p>
@endsection