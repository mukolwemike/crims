@extends('emails.email')

@section('content')
    <div>
        <p>
            {!! $content !!}
        </p>
    </div>
@endsection