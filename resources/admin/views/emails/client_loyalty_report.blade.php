@extends('emails.email')

@section('content')

    <p>Here is the summary of Loyalty Points that you requested. Attached is clients loyalty points.</p>


    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <th>Investments</th>
            <th>Unit Funds</th>
            <th>Real Estate</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td style="text-align: center">{{\Cytonn\Presenters\AmountPresenter::currency($invPoints, false, 2)}}</td>
            <td style="text-align: center">{{\Cytonn\Presenters\AmountPresenter::currency($fundPoints, false, 2)}}</td>
            <td style="text-align: center">{{\Cytonn\Presenters\AmountPresenter::currency($rePoints, false, 2)}}</td>
        </tr>
        </tbody>
    </table>

    <p>Regards,</p>

    <p class="bold-underline"><strong>Cytonn CRIMS</strong></p>
@endsection