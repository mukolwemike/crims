@extends('emails.default')

@section('content')
    <p>An investment application has been saved. Here are the details:</p>

    <ul style="list-style: none">
        <li>Client Name: {{ $application->present()->name }}</li>
        <li>Client Email: {{ $application->email }}</li>
        <li>Product: {{ $application->product->name }}</li>
        <li>Amount: {{ $application->present()->amount }}</li>
        <li>Tenor: {{ $application->tenor }} months</li>
    </ul>

    <p>
        Regards,<br/>

        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection