@extends('emails.default')

@section('content')
    <p>A new KYC document has been uploaded. The details are:</p>

    <ul style="list-style: none">
        <li>Client Name: {{ $application->present()->name }}</li>
        <li>Client Email: {{ $application->email }}</li>
        <li>Document Type: {{ $document->type->name }}</li>
        <li>File Name: {{ $document->filename }}</li>
    </ul>

    You can login to CRIMS to view the application and all uploaded documents<br/>

    <p>
        Regards,<br/>

        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection