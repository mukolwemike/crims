@extends('emails.email')

@section('content')
    <p>Dear Team,</p>
    <p>There are {!! count($results) !!} index operations that took place today.</p>
    <table class="table table-responsive table-hover table-striped">
        <thead>
        <tr>
            <th>Table</th>
            <th>Execution Time</th>
        </tr>
        </thead>
        <tbody>
        @foreach($results as $result)
            <tr>
                <td>{!! $result['table'] !!} </td>
                <td>{!! $result['execution_time'] !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection