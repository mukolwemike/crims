@extends('emails.client_default')

@section('content')
    @foreach($blocks as $block)
        @if(isset($block['title'])) <h4>{{ $block['title'] }}</h4> @endif
        @if(isset($block['intro'])) <p>{{ $block['intro'] }}</p> @endif

        @if(isset($block['table']))
            <table>
                <thead>
                    {{--@foreach($block['table'] as $key => $value)--}}
                        {{--@if(is_array($value))--}}
                            {{--<tr>--}}
                                {{--@foreach($value as $val)<th>{{ $val }}</th>@endforeach--}}
                            {{--</tr>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                </thead>
                <tbody>
                    @foreach($block['table'] as $key => $value)
                        @if(!is_array($value))
                            <tr><td><b>{{ $key }}</b></td><td>{{ $value }}</td></tr>
                        @else
                            <tr>
                                @foreach($value as $val)<td>{!! $val !!}</td>@endforeach
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        @endif
    @endforeach
@endsection