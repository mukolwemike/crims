@extends('emails.email')

@section('content')
    <p>Dear {!! $data['receiver_firstname'] !!},</p>

    <p>Find the table with the evaluation details for <strong>{!! $data['name'] !!}</strong> for the period
        @if(! is_null($data['report_start_date']))
            between {!! Carbon\Carbon::parse($data['report_start_date'])->toDateString() !!} and
        @else
            ending
        @endif
        {!! Carbon\Carbon::parse($data['report_end_date'])->toDateString() !!}
    </p>

    <table class = "table table-responsive table-striped">
        <thead>
        <tr>
            <th colspan="2">{!! $data['name'] !!} Evaluation Report</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Name</td>
            <td>{!! $data['name'] !!}</td>
        </tr>
        <tr>
            <td>Unit</td>
            <td>{!! $data['unit'] !!}</td>
        </tr>
        <tr>
            <td>Unit</td>
            <td>{!! $data['unit'] !!}</td>
        </tr>
        <tr>
            <td>Branch</td>
            <td>{!! $data['branch'] !!}</td>
        </tr>

        <tr>
            <td>Days</td>
            <td>{!! $data['days'] !!}</td>
        </tr>

        <tr>
            <td>Value Days</td>
            <td>{!! $data['value_days'] !!}</td>
        </tr>
        <tr>
            <td>Title</td>
            <td>{!! $data['title'] !!}</td>
        </tr>
        <tr>
            <td>FA Title</td>
            <td>{!! $data['fa_title'] !!}</td>
        </tr>
        <tr>
            <td>Suspended Days</td>
            <td>{!! $data['suspended_days'] !!}</td>
        </tr>
        <tr>
            <td>Current FA Position Days</td>
            <td>{!! $data['currentFaPositionDays'] !!}</td>
        </tr>
        <tr>
            <td>Employment Status</td>
            <td>{!! $data['employmentStatus'] !!}</td>
        </tr>
        <tr>
            <td>Position Status</td>
            <td>{!! $data['position_status'] !!}</td>
        </tr>
        <tr>
            <td>Current Position Days</td>
            <td>{!! $data['currentPositionDays'] !!}</td>
        </tr>
        <tr>
            <td>YTD Start Date</td>
            <td>{!! $data['ytd_start_date'] !!}</td>
        </tr>
        <tr>
            <td>Cumulative Start Date</td>
            <td>{!! $data['cumulative_start_date'] !!}</td>
        </tr>
        <tr>
            <td>Target Start Date</td>
            <td>{!! $data['targets_start_date'] !!}</td>
        </tr>
        <tr>
            <td>YTD Inflows</td>
            <td>{!! number_format($data['investments']['inflow'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Inflows</td>
            <td>{!! number_format($data['investments']['cumulativeInflows'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Withdrawals</td>
            <td>{!! number_format($data['investments']['outflow'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Withdrawals</td>
            <td>{!! number_format($data['investments']['cumulativeOutflows'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Netflows</td>
            <td>{!! number_format($data['investments']['netflow'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Netflows</td>
            <td>{!! number_format($data['investments']['cumulativeNetflow'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Real Estate Sales</td>
            <td>{!! number_format($data['real_estate']['YTDRealEstateSales'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Cash Plan Real Estate Sales</td>
            <td>{!! number_format($data['real_estate']['YTDRealEstateCashPlanSales'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Actual Payments</td>
            <td>{!! number_format($data['real_estate']['YTDActualPayments'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Real Estate Sales</td>
            <td>{!! number_format($data['real_estate']['cumulativeRealEstateSales'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Cash Plan Real Estate Sales</td>
            <td>{!! number_format($data['real_estate']['cumulativeRealEstateCashPlanSales'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Cash Flows</td>
            <td>{!! number_format($data['real_estate']['cumulativeCashFlows'], 2) !!}</td>
        </tr>

        <tr>
            <td>YTD Retention</td>
            <td>{!! number_format($data['retention']['YTDRetention'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Retention</td>
            <td>{!! number_format($data['retention']['cumulativeRetention'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Targets</td>
            <td>{!! number_format($data['retention']['YTDTargets'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Targets</td>
            <td>{!! number_format($data['retention']['cumulativeTargets'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Percentage Retention</td>
            <td>{!! number_format($data['retention']['YTDPercentageRetention'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Retention Before Demotion</td>
            <td>{!! number_format($data['retention']['cumulativeRetentionBeforeDemotion'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Percentage Retention</td>
            <td>{!! number_format($data['retention']['cumulativePercentageRetention'], 2) !!}</td>
        </tr>
        <tr>
            <td>YTD Percentage</td>
            <td>{!! number_format($data['retention']['YTDPercentage'], 2) !!}</td>
        </tr>
        <tr>
            <td>Cumulative Percentage</td>
            <td>{!! number_format($data['retention']['cumulativePercentage'], 2) !!}</td>
        </tr>
        </tbody>
    </table>

    <p>Regards,<br/></p>

    Cytonn Investments<br/>
    -----------------------------------------------------------------
@endsection