@extends('emails.client_default')

@section('content')
    <p>Dear {!! $firstname !!},</p>

    <p>
        {!! $content !!}
    </p>

    <p>Regards,<br/></p>

    Cytonn Investments<br/>
    -----------------------------------------------------------------
@endsection