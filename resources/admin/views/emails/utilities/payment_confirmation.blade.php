@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    <p>Your bill payment request with the following details has been processed successfully:</p>
    <p>
        <b>Reference Number : </b> {!! $data->reference !!} <br />
        <b>From Account : </b> {!! $sourceAccount !!} <br />
        <b>Biller : </b> {!! $instruction->utility->name !!} <br />
        <b>Biller Details : </b> {!! $instruction->account_number !!} <br />
        <b>Amount : </b>{!! $currency !!} {!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment->amount)) !!} <br />
    </p>

    <p>In case of any queries please do not hesitate to contact us on 0709 101 200.</p>
    <p>Your sincerely,</p>

    @include('emails.client_service_signature')
@endsection
