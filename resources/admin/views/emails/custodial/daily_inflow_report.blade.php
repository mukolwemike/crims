@extends('emails.email_plain')

@section('content')
    <p>Dear Team,</p>

    <p>
        Real Estate production for today is:
    </p>

    @foreach($real_estate as $project)
        @include('reports.custody.production.real_estate.partials.project')
    @endforeach

    @foreach($structured_products as $fundManager)
        <p>
            The {!! $fundManager->name !!} Activity is as shown:
        </p>

        @foreach($fundManager->p_currencies as $currency)
            @include('reports.custody.production.structured.partials.currency')
        @endforeach
    @endforeach

    <p class="bold underline">CRIMS - {!! \Carbon\Carbon::today()->toFormattedDateString() !!}</p>

@endsection