@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Our auditors, Grant Thornton (GT), are now carrying out the annual audit of {{ $spv }}.
        As part of their audit procedures, we have been requested by the audit team to ask you to confirm directly to them the balance
        owed to you by {{ $product }}.
    </p>
    {{--<p>--}}
        {{--Partner code: <b> {!! $client->client_code !!}</b> <br>--}}
        {{--Nature of Account:<b> {!! $product !!}</b> <br>--}}
        {{--Balance as at {!! $date->toFormattedDateString() !!}: <b> {!! $currency !!} {!! \Cytonn\Presenters\AmountPresenter::currency($amount, true, 0) !!}</b>  <br>--}}
    {{--</p>--}}
    <p>
        We shall be obliged if you can verify the balance shown above with your records as of {!! $date->toFormattedDateString() !!} and
        indicate whether you are in agreement therewith by either:
    <ol type="1">
        <li>A direct reply on email to our auditors: <a href="mailto:william.kagai@ke.gt.com">william.kagai@ke.gt.com</a>, <a href="mailto:nelius.kuria@ke.gt.com">nelius.kuria@ke.gt.com</a> Grant
            Thornton, P.O. 46986-00100, Nairobi, for the attention of Mr. William Kagai/ Ms Nelius Kuria</li>
        <li>Signing and sending a scanned copy of the attached confirmation letter to <a href="mailto:william.kagai@ke.gt.com">william.kagai@ke.gt.com</a>, <a href="mailto:nelius.kuria@ke.gt.com">nelius.kuria@ke.gt.com</a> Grant
            Thornton, P.O. 46986-00100, Nairobi, for the attention of Mr. William Kagai/ Ms Nelius Kuria</li>
    </ol>
    </p>
    <p>Please note that the attached document is password protected for security reasons. The password is the numeric values in your partner code.</p>
    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection