@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Our auditors, Grant Thornton, are now engaged on their annual audit. As part of their normal audit procedures we
        have been requested for you to confirm directly to them the amount paid to us as at {{ \Cytonn\Presenters\DatePresenter::formatDate($date) }} for the purchase of a unit in {{ $product }}.
    </p>
    <p>
        <b>Development : </b> <span class="right">{{ $product }}</span><br>
        <b>House Number : </b> <span class="right">{{ $house_number }}</span><br>
        <b>Amount as at {{ \Cytonn\Presenters\DatePresenter::formatDate($date) }} : </b> <span>__________________</span><br>
    </p>
    <p>
        Please return this letter to our auditors on email to <a href="mailto:jane.wanjira@ke.gt.com">jane.wanjira@ke.gt.com</a>/<a href="mailto:tejal.bharadva@ke.gt.com">tejal.bharadva@ke.gt.com</a>, for the attention of Mrs. Jane Wanjira / Mrs. Tejal Bharadva.
    </p>
    <p>Your co-operation in this matter is greatly appreciated.</p>
    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection