@extends('emails.default')

@section('content')
    <p>A {{ $type }} was made in CRIMS. See the details below:</p>

    <ul style="list-style: none">
        <li>Client Name: {{ $client->name() }}</li>
    </ul>

    <p>
        Regards,<br/>

        <span class="bold-underline">CRIMS.</span>
    </p>

@endsection