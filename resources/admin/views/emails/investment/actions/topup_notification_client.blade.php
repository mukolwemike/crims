@extends('emails.default')

@section('content')
    <p>Dear {{ $client->present()->firstname }}, </p>
    <p>
        Thank you for investing with Cytonn Investments.
        Your funds will be Invested according to the terms you have indicated. Please expect a business confirmation on the same within the next 24 hours.
    </p>

    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>

    <p>
        Thank you,
    </p>

    <span class="bold-underline">Cytonn Investments Management Limited</span>
@endsection