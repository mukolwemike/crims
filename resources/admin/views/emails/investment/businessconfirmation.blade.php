@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>

    <p>
        Cytonn Investments takes this opportunity to thank you for investing in our
        product, {!! $investment->product->longname !!}
    </p>

    <p>
        @if($rollover)
            We have herein attached your rollover business confirmation letter for your records.
        @else
            We have herein attached the business confirmation letter for your records.
        @endif
    </p>

    <p>
        To manage your investments online, kindly request for your login details by emailing us on
        <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>

    <p>
        Kindly note that you can manage your investment from our online portal at clients.cytonn.com or through our apps
        <a href="https://apps.apple.com/us/app/cytonn/id1411423248">App Store</a> and
        <a href="https://play.google.com/store/apps/details?id=com.cytonn">Play Store</a>.
    </p>

    <p>
        <strong>
            To provide security and confidentiality, the documents we send you (business confirmations and statements)
            will be password protected.
            When prompted for a password, please enter your client code.
        </strong>
    </p>

    <p>Thank you for choosing Cytonn as your preferred Investment Manager.</p>

    <p>Your sincerely,</p>

    {!! $email_sender !!}

{{--    @include('emails.client_service_signature')--}}

@endsection
