@extends('emails.email_plain')

@section('content')
    @if($investment->client->clientType->name == 'corporate')
        <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($investment->client_id) !!},</p>
    @else
        <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($investment->client_id) !!},</p>
    @endif

    <p>As per your instructions to extend the duration of your investment, we have executed the instruction and the new maturity date is
        <b>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</b> at <b>{!! $investment->interest_rate !!}% p.a</b></p>

    <p>
        Your funds have been invested as per below;
    </p>

    <table class="table table-striped">
        <thead>
        <tr>
            <td>Product</td>
            <td>Initial Principal</td>
            <td>Current Principal</td>
            <td>Interest rate</td>
            <td>Duration</td>
            <td>Start Date</td>
            <td>Old Maturity Date</td>
            <td>New Maturity Date</td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>{!! $investment->product->name !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount, false, 0) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getAvailablePrincipal(\Carbon\Carbon::now()), false, 0) !!}</td>
                <td>{!! $investment->interest_rate !!}% p.a.</td>
                <td>{!! $investment->repo->getTenorInDays() !!} days</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($oldInvestment->maturity_date) !!}</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
            </tr>
        </tbody>
    </table>

    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection