@extends('emails.email')

@section('content')
    <h3>Maturity Notification</h3>
    @if(count((array)$investments) == 0)
        <p>
            There are no investments that have matured and have not been withdrawn/rolled over.
        </p>
    @else
        <p>
            The following investments have matured and have not been withdrawn/rolled over.
        </p>

        <table class="table table-responsive table-hover table-striped">
            <thead>
            <tr>
                <th>Client Code</th><th>Client Name</th><th>Product</th><th>Invested Amount</th><th>Value Date</th><th>Maturity Date</th><th>FA</th><th>Position</th>
            </tr>
            </thead>
            <tbody>
            @foreach($investments as $investment)
                <tr>
                    <td>@if(!is_null($investment->client)){!! $investment->client->client_code !!}@endif</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                    <td>{!! $investment->product->name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                    <td>@if($investment->client->getLatestFa('investment')){!! $investment->client->getLatestFa('investment')->name !!}@endif</td>
                    <td>@if($investment->client->getLatestFa('investment')){!! $investment->client->getLatestFa('investment')->type->name !!}@endif</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    <p>
        <hr/>
        Cytonn Investments Management Limited.
    </p>
@endsection