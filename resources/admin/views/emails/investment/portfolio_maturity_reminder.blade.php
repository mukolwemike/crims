@extends('emails.email')

@section('content')
    <h3>Portfolio Maturities</h3>
    @if($investments->count())
        <p>
            The following portfolio investments have matured and have not been withdrawn/rolled over.
        </p>

        <table class="table table-responsive table-hover table-striped">
            <thead>
            <tr>
                <th>Institution</th><th>Fund</th><th>Invested Amount</th><th>Value Date</th><th>Maturity Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($investments as $investment)
                <tr>
                    <td>{!! $investment->security->name !!}</td>
                    <td>{!! @$investment->unitFund->name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>
            There are no portfolio investments that have matured and have not been withdrawn/rolled over.
        </p>
    @endif
    <p>
        <hr/>
        Cytonn Investments Management Limited.
    </p>
@endsection