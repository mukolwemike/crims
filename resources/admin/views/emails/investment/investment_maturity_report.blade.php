@extends('emails.email')

@section('content')
    <p>The following investments have been extended for one month since they are on call</p>
    @if($client_investments->count() > 0)
        <h4>Client Investments</h4>
        <table class="table table-responsive table-hover table-striped">
            <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Invested Amount</th>
                <th>Value Date</th>
                <th>Old Maturity Date</th>
                <th>Extended Maturity Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($client_investments as $investment)
                <tr>
                    <td>{!! $investment->client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->old_maturity_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

    @if($portfolio_investments->count() > 0)
        <h4>Portfolio Investments</h4>
        <table class="table table-responsive table-hover table-striped">
            <thead>
            <tr>
                <th>Portfolio Investor</th>
                <th>Invested Amount</th>
                <th>Value Date</th>
                <th>Old Maturity Date</th>
                <th>Extended Maturity Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($portfolio_investments as $investment)
                <tr>
                    <td>{!! $investment->portfolioInvestor->name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->old_maturity_date) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

    <p>
        <hr/>

        Cytonn Investments Management Limited.
    </p>
@endsection