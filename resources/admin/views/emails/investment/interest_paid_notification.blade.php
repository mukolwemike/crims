@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Please note that we have processed {!! ucfirst($date->format('F')) !!}  interest payments and ​have remitted the same to your bank as follows:
    </p>
    <table>
        <thead>
            <tr><td>Bank</td><td>Account Number</td><td align="right">Amount</td></tr>
        </thead>
        <tbody>
            @foreach($schedules as $schedule)
                <tr>
                    <td>{!! $schedule->bank !!}</td>
                    <td>{!! $schedule->account_number !!}</td>
                    <td align="right">{!! $schedule->currency !!} {!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount_paid) !!}</td>
                </tr>
            @endforeach

            @foreach($schedules->groupBy('currency') as $grouped)
                <?php $grouped = \Cytonn\Core\DataStructures\Collection::make($grouped); ?>
                <tr>
                    <td align="left" class="bold" colspan="2">Total</td>
                    <td align="right" class="bold">{!! $grouped->first()->currency !!} {!! \Cytonn\Presenters\AmountPresenter::currency($grouped->sum('amount_paid')) !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p>
        Feel free to contact us at <a href="mailto:operations@cytonn.com">operations@cytonn.com</a> in case of further queries.
    </p>
    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection