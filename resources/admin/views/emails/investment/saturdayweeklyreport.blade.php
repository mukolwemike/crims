@extends('emails.email_plain')

@section('content')
    <style>
        .green {
            background-color: #006666 !important;
            color: #FFFFFF;
        }
    </style>
    <div style="font-size: 12px">

    <p>Please find the CMS summary. We expect the following Maturities in the upcoming two weeks.</p>

    <h3>CLIENT MATURITIES</h3>

        <table>
            <tbody>
                <tr>
                    <td class="container">
                        <table class="table table-responsive table-hover table-striped">
                            <thead></thead>
                            <tbody>
                                <tr class="green">
                                    <th colspan="2">CLIENT MATURITIES - Week starting {!! $nextWeekStart->toFormattedDateString() !!}</th>
                                </tr>
                                @foreach($nextWeekInvestments as $investment)
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFirstNameLastName($investment->client_id) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date)) !!}</td>
                                    </tr>
                                @endforeach
                                <tr  class="align-left">
                                    <th>Total</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($nextWeekInvestments->sum(function ($investment){  return $investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date);  })) !!}</th>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="container">
                        <table class="table table-responsive table-hover table-striped">
                            <thead></thead>
                            <tbody>
                                <tr class="green" style="background-color: #006666 !important">
                                    <th colspan="2">CLIENT MATURITIES - Week starting {!! $otherWeekStart->toFormattedDateString() !!}</th>
                                </tr>
                                @foreach($otherWeekInvestments as $investment)
                                    <tr>
                                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFirstNameLastName($investment->client_id) !!}</td>
                                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date)) !!}</td>
                                    </tr>
                                @endforeach
                                <tr class="align-left">
                                    <th>Total</th>
                                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($otherWeekInvestments->sum(function ($investment){  return $investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date);  })) !!}</th>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>


    <h3>FUND MATURITIES</h3>

    <table>
        <tbody>
            <tr>
                <td class="container">
                    <table class="table table-responsive table-hover table-striped">
                        <thead></thead>
                        <tbody>
                            <tr class="green">
                                <th colspan="2">FUND MATURITIES - Week starting {!! $nextWeekStart->toFormattedDateString() !!}</th>
                            </tr>
                            @foreach($nextWeekPortfolio as $investment)
                                <tr>
                                    <td>{!! $investment->institution->name !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->maturity_date)) !!}</td>
                                </tr>
                            @endforeach
                            <tr class="align-left">
                                <th>Total</th>
                                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($nextWeekPortfolio->sum(function($investment){ return $investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->maturity_date); })) !!}</th>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td class="container">
                    <table class="table table-responsive table-hover table-striped">
                        <thead></thead>
                        <tbody>
                            <tr class="green" style="background-color: #006666 !important">
                                <th colspan="2">FUND MATURITIES - Week starting {!! $otherWeekStart->toFormattedDateString() !!}</th>
                            </tr>
                            @foreach($otherWeekPortfolio as $investment)
                                <tr>
                                    <td>{!! $investment->institution->name !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->maturity_date)) !!}</td>
                                </tr>
                            @endforeach
                            <tr class="align-left">
                                <th>Total</th>
                                <th>{!! \Cytonn\Presenters\AmountPresenter::currency($otherWeekPortfolio->sum(function($investment){ return $investment->repo->getTotalValueOfAnInvestmentAsAtDate($investment->maturity_date); })) !!}</th>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>



    The following are the analytics for CMS, CPN and Coop.

    <table class="table table-responsive table-hover table-striped">
        <thead>
            <tr>
                <td></td>
                <td>Effective Asset Yield</td>
                <td>Effective Liability Yield</td>
                <td>Effective Asset Tenor</td>
                <td>Effective Liability Tenor</td>
                <td>Spread</td>
            </tr>
        </thead>
        <tbody>
            @foreach($fundManagers as $fm)
                <?php $a = $analytics($fm); ?>
                <tr>
                    <td>{!! $fm->name !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($a['asset_yield']) !!}%</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($a['liability_yield'] )!!}%</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($a['asset_tenor']) !!} months</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($a['liability_tenor']) !!} months</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($a['spread']) !!}%</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>
@endsection