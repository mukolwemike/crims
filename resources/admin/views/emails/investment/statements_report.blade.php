@extends('emails.email_plain')

@section('content')
    <p>All statements have been sent out to clients. Below is the breakdown per product with the failures in each section:</p>

    @foreach($campaigns as $campaign)
        <table>
            <thead>
                <tr>
                    <th colspan="4" class="center">{{ $campaign['name'] }}</th>
                </tr>
                <tr>
                    <th>Product</th>
                    <th>Sent</th>
                    <th>Failed</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($campaign['products'] as $product)
                    <tr>
                        <td>{{ $product['name'] }}</td>
                        <td>{{ $product['sent'] }}</td>
                        <td>{{ $product['failed'] }}</td>
                        <td>{{ $product['total'] }}</td>
                    </tr>
                @endforeach
                <tr>
                    <th class="left">Total</th>
                    <th colspan="2"></th>
                    <th class="left">{{ $campaign['total'] }}</th>
                </tr>
                <tr>
                    <td colspan="100%"><small>*Total is count of clients combined for all products</small></td>
                </tr>
            </tbody>
        </table>
        <br>
    @endforeach

    <p class="bold-underline">Cytonn CRIMS</p>
@endsection