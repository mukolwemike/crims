@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Kindly find attached your {!! strtoupper($productName) !!} Statement as at {!! $stmt_date->format('jS F Y')  !!}.

        <br/><br/>
        Please note that this statement is password protected for security reasons. The password is the numeric values
        in your partner code.

        <br/>
        @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
            {!! $campaign->statement_message !!}
        @endif

        <br/>
{{--        <strong>--}}
{{--            We are conducting a survey to gauge your level of satisfaction with our products and services. Please take a--}}
{{--            few minutes to give us feedback and recommend areas of improvement by filling this brief--}}
{{--            <a href="https://forms.gle/1BdWu28TvnJxcQmM8">Client Satisfaction Survey Form</a>.--}}
{{--        </strong>--}}

{{--        <br/><br/>--}}

        Kind regards,<br/>

        @include('emails.client_service_signature')
    </p>
@endsection
