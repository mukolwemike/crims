@extends('emails.email')

@section('content')

    <h3>Maturities For The Month</h3>
    @if(count($maturities_grouped_by_product))
        <p>
            The following investments are set to mature in the next one month, {!! $today->toFormattedDateString() !!} to {!! $next_month->toFormattedDateString() !!}.
        </p>

        <table class="table table-responsive table-hover table-striped">
            <thead>
                <tr>
                    <th>Client Code</th>
                    <th>Client Name</th>
                    <th>Invested Amount</th>
                    <th>Value Date</th>
                    <th>Maturity Date</th>
                    <th>FA</th>
                </tr>
            </thead>
            <tbody>
                @foreach($maturities_grouped_by_product as $product => $investments_arr)
                    <tr>
                        <th colspan="6">{!! $product !!}</th>
                    </tr>
                    @foreach($investments_arr as $investment)
                        <tr>
                            <td>{!! $investment->{'Client Code'} !!}</td>
                            <td>{!! $investment->{'Name'} !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->{'Principal'}) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->{'Value Date'}) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->{'Maturity Date'}) !!}</td>
                            <td>{!! $investment->{'FA'} !!}</td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>
    @else
        <p>
            The are no investments set to mature in the next one month, {!! $today->toFormattedDateString() !!} to {!! $next_month->toFormattedDateString() !!}.
        </p>
    @endif

    <p>
        <hr/>
        Cytonn Investments Management Limited.
    </p>
@endsection