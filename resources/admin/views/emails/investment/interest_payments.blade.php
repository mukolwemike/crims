@extends('emails.email_plain')

@section('content')

    <p>Please find attached an excel sheet of all interest payments between {!! \Cytonn\Presenters\DatePresenter::formatDate($start_date) !!} to {!! \Cytonn\Presenters\DatePresenter::formatDate($end_date) !!} :</p>

@endsection