@extends('emails.email_plain')

@section('content')

    <p>
        Please find attached an excel sheet of
        @if($monthly) monthly @else daily @endif
        withdrawal summary for {!! \Cytonn\Presenters\DatePresenter::formatDate($start->toDateString()) !!}
        @if($monthly) to {!! \Cytonn\Presenters\DatePresenter::formatDate($end->toDateString()) !!} @endif:
    </p>

@endsection