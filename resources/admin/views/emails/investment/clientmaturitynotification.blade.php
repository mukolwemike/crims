@extends('emails.email_plain')

@section('content')
    @if($investment->client->clientType->name == 'corporate')
        <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($investment->client_id) !!},</p>
    @else
        <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($investment->client_id) !!},</p>
    @endif

    @if($error)
        <div style="color: red">
            Note: An error occurred when sending this email to the client
        </div>
    @endif

    <p>I hope this email finds you well.</p>

    <p>This is a polite notification on maturity of your {!! str_plural('investment', count($investmentGroup)) !!} in {!! ucwords($investment->product->longname) !!}, with details as shown below.</p>

    <table class="table table-striped">
        <thead>
        <tr>
            <td>Product</td>
            <td>Principal</td>
            <td>Interest rate</td>
            <td>Duration</td>
            <td>Start Date</td>
            <td>Maturity Date</td>
            <td>Net Interest Accrued</td>
            @if($scheduleDetail)
                <td>Monthly Contribution</td>
            @else
                <td>Withdrawals</td>
            @endif
            <td>Value at Maturity</td>
        </tr>
        </thead>
        <tbody>
        <?php
            $total_value = 0;
            $total_principal = 0;
            $withdrawal = 0;
            $net_interest = 0;
            $investments = 0;
        ?>
        @foreach($investmentGroup as $inv)
            <tr>
                <td>{!! $inv->product->name !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($inv->amount, false, 0) !!}</td>
                <td>{!! $inv->interest_rate !!}% p.a.</td>
                <td>{!! $inv->repo->getTenorInDays() !!} days</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($inv->invested_date) !!}</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($inv->maturity_date) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($inv->repo->netInterestBeforeDeductions($inv->maturity_date), false, 0) !!}</td>
                @if($scheduleDetail)
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($scheduleDetail->amount) !!}</td>
                @else
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($inv->repo->getWithdrawnAmount(), false, 0) !!}</td>
                @endif
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($inv->repo->getTotalValueOfInvestmentAtDate($inv->maturity_date), false, 0) !!}</td>
            </tr>
            <?php
                $total_value += $inv->repo->getTotalValueOfInvestmentAtDate($inv->maturity_date);
                $total_principal += $inv->amount;
                $net_interest += $inv->repo->netInterestBeforeDeductions($inv->maturity_date);
                $withdrawal += $inv->repo->getWithdrawnAmount();
            ?>
        @endforeach
        </tbody>
        @if(collect($investmentGroup)->count() > 1)
            <tfoot>
                <tr>
                    <th>Total</th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total_principal, false, 0) !!}</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($net_interest, false, 0) !!}</th>
                    @if($scheduleDetail)
                        <td></td>
                    @else
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($withdrawal, false, 0) !!}</th>
                    @endif
                    <th>{!! \Cytonn\Presenters\AmountPresenter::currency($total_value, false, 0) !!}</th>
                </tr>
            </tfoot>
        @endif
    </table>

    <p>On behalf of Cytonn Investments, I take this opportunity to thank you for taking up investments in our
        {!! ucwords($investment->product->longname) !!} and choosing us to deliver to your investment promise.</p>

    <p>Kindly note that you can issue rollover instructions by <strong>replying on this email</strong> or online from <a href="https://clients.cytonn.com">clients.cytonn.com</a> for matured investments.
        Please note that for matured investments for which we do not receive communication, we will automatically rollover for 3 months to ensure you don’t lose any interest income.
    </p>

    <p>
        To manage your investments online, kindly request for log in details by emailing us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>
    </p>

    @if($productRates && count($productRates) > 0)
        <p>
            Kindly note that the prevailing {!! $investment->product->name !!} rates are<br/>
            <strong>
                @foreach($productRates as $rate)
                    {!! $rate->rate !!}% p.a. for {!! $rate->present()->getTenorName !!}
                    <br/>
                @endforeach
            </strong>
        </p>
    @endif

    <p>
        If you have any question, comment or need any assistance, we are at your service. Our relationship team will be at hand
        to assist you with all your investment needs. Please do not hesitate to contact us on <span style="color: #0000EE">+254 (709) 101 000</span> / <span style="color: #0000EE">+254 (709) 101 541</span>
        or email us at <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>
    </p>

    <p>Once again, we thank you for choosing us to serve your investment needs and look forward to doing more business with you.</p>

    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection
