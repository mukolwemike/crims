@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        We acknowledge receipt of {!! $investment->product->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency( $investment->amount) !!}
        in {!! $investment->product->longname !!}.
    </p>
    <p>
        Kindly provide us with following KYC to enable us issue a business confirmation:
    </p>
    <p>
    <ol>
        @foreach($pendingKycs as $pendingKyc)
            <li>{{$pendingKyc}}</li>
        @endforeach
    </ol>
    </p>

    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>

@endsection