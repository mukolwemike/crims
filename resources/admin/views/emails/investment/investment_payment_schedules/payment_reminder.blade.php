@extends('emails.email_plain')

@section('content')

    @if($client->clientType->name == 'corporate')
        <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},</p>
    @else
        <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id) !!},</p>
    @endif

    <p>I hope this email finds you well.</p>

    <p>On behalf of Cytonn Investments, I take this opportunity to thank you for taking up our investments products and
        choosing us to deliver to your investment promise.</p>
    <p>This is to remind you of your overdue investment {!! str_plural('payment', count($schedules)) !!} as per the schedule. <br><br>

        Kindly purpose to make the said payment.
    </p>

    <table>
        <thead>
        <tr>
            <td>Product</td>
            <th>Payment Date</th>
            <th>Description</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php $total = 0; ?>
        @foreach($schedules as $schedule)
            <tr>
                <td>{!! $schedule->parentInvestment->product->name !!}</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                <td>{!! $schedule->description !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                <?php $total += $schedule->amount ?>
            </tr>
        @endforeach
        @if(count($schedules) > 1)
            <tr>
                <th colspan="3">Total</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
            </tr>
        @endif
        </tbody>
    </table>

    <p>Once again, we thank you for choosing us to serve your investment needs and look forward to doing more business with you.</p>

    <p>Kind regards,</p>

    <p>{!! $email_sender !!}</p>
@endsection