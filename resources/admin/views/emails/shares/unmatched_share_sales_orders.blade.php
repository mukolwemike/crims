@extends('emails.email')

@section('content')

    <p>Dear {{ $client->name() }},</p>

    <p>I trust this finds you well.</p>

    <p>Thank you once again for placing your order to participate in the Cytonn Investments Management Limited over-the-counter trading platform.</p>

    <p>We wish to inform you that your order was not filled up during the month of {{ \Carbon\Carbon::today()->shortEnglishMonth }}. The order is still in the queue and we shall keep it open until it's filled/ cancelled/ expired.</p>

    <p>
        <table class="table table-responsive table-hover table-striped">
            <thead>
                <tr>
                    <th>Seller Name</th>
                    <th>Seller Number</th>
                    <th>Number</th>
                    <th>Price</th>
                    <th>Sold</th>
                    <th>Remaining</th>
                    <th>Request Date</th>
                    <th>Good Till</th>
                </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{!! $order['seller'] !!}</td>
                    <td>{!! $order['seller_number'] !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order['number'], true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order['price']) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order['sold'], true, 0) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order['number'] - $order['sold'], true, 0) !!}</td>
                    <td>{!! $order['request_date'] !!}</td>
                    <td>
                        @if(($order['good_till_filled_cancelled']))
                            <span>Filled/Cancelled</span>
                        @else
                            <span>Expiry</span>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </p>

    <br>

    For any questions and / or clarifications, please reach out to us on otc@cytonn.com.
@endsection