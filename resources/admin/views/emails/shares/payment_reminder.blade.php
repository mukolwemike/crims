@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    <p>I trust this finds you well.</p>

    <p>This is a polite reminder to kindly pay the outstanding amount and stamp duty fee to enable filing at the Companies Registry. The stamp duty fee is 1% of the value of the shares purchased.</p>

    <p>
        You have purchased number of shares {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number, false, 0) }} worth
        KES. {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number * $purchase->purchase_price) }} of shares purchased the stamp duty payable is therefore
        KES. {{ \Cytonn\Presenters\AmountPresenter::currency(($purchase->number * $purchase->purchase_price)/100) }}.
    </p>

    <p>
        Kindly proceed to make the payment on or before {{ \Carbon\Carbon::parse($purchase->date)->addDay(14)->toFormattedDateString() }} to the bank details below:
        <br>
        Standard Chartered Bank <br>
        Cytonn Investment Management Limited-OTC <br>
        0105040476704 <br>
        Chiromo <br>
    </p>

    <p>Alternatively, you can pay via the MPESA paybill number <b>329035</b>. On account number, indicate name and product. E.g <b>AlexOTC</b></p>

@endsection