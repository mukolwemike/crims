@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Please find attached Cytonn Investment Cooperative Society Shares Statement as at {!! \Cytonn\Presenters\DatePresenter::formatDate($statementDate) !!}. <br />

        @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
            {!! $campaign->statement_message !!}
        @endif

        <br/>

        Thank you for investing with us.

        <br/><br/>

        Kind regards,<br/>

        @include('emails.client_service_signature')
    </p>
@endsection