@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    <p>I trust this finds you well.<br>

        Thank you once again for placing your order to participate in the Cytonn Investments Management Limited over-the-counter trading platform. <br>

        We are pleased to inform you that we were able to fill your order of <strong>{!! $holding->number !!} shares</strong> fully at a price of <strong>KES {!! $holding->purchase_price !!}</strong> per share. Please see attached our Confirmation on the same.</p>
     
    <p>For any questions and / or clarifications, please reach out to us on otc@cytonn.com.</p>

    <p>Kind regards,</p>

    {!! $email_sender !!}

{{--    @include('emails.client_service_signature')--}}
@endsection
