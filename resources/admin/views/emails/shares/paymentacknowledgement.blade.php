@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>

    <p>I trust this finds you well.</p>

    <p>Thank you once again for placing your order to participate in the Cytonn Investments Management Limited over-the-counter trading platform. </p>

    <p>
        We hereby acknowledge receipt of <b>Kshs {{ \Cytonn\Presenters\AmountPresenter::currency($amount) }}</b>
        for the <b>{{ \Cytonn\Presenters\AmountPresenter::currency($purchase->number, true, 0) }}</b> shares purchased for in Cytonn Investments at
        <b>Kshs {{ \Cytonn\Presenters\AmountPresenter::currency($purchase->purchase_price) }}</b> per share as <b>attached</b>.
    </p>

    <p>For any questions and / or clarifications, please reach out to us on <a href="mailto:otc@cytonn.com">otc@cytonn.com</a></p>

    <br/>

    Kind regards,<br/>

    {!! $emailSender !!}
@endsection