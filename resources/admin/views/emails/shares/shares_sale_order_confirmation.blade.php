@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Please find attached your Cytonn Shares Sale Order Confirmation. <br><br>

        Thank you for investing with us.

        <br/><br/>

        Kind regards,<br/>

        {!! $email_sender !!}
    </p>
@endsection