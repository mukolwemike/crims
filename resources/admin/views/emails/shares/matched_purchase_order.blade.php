@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>

    <p>I trust this finds you well.</p>

    <p>Thank you once again for placing your order to participate in the Cytonn Investments Management Limited over-the-counter trading platform. </p>
     
    <p>
        We wish to inform you that your purchase order of <b>{{ \Cytonn\Presenters\AmountPresenter::currency($purchaseOrder->number, true, 0) }}</b> shares
        at <b>{{ ucfirst($purchaseOrder->buyer->entity->currency->code) }}.{{ \Cytonn\Presenters\AmountPresenter::currency($purchaseOrder->price) }}</b> per share has been matched.
        @if($balance < $total_amount)) Kindly make payments to enable us complete your share purchase.@endif
    </p>

    <p>For any questions and / or clarifications, please reach out to us on <a mailto="otc@cytonn.com">otc@cytonn.com</a></p>

    <br/>

    Kind regards,<br/>

    {!! $email_sender !!}
@endsection