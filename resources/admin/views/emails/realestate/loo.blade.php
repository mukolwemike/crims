@extends('emails.email_plain')

@section('content')
    <p>The following client has made a reservation and will require that an LOO is prepared:</p>
    <table>
        <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Client Email</th>
                <th>Client Phone</th>
                <th>Unit Number</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{!! $client_code !!}</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client_id) !!}</td>
                <td>{!! $email !!}</td>
                <td>{!! $phone !!}</td>
                <td>{!! $unit_number !!}</td>
                <td>
                    {!! @\Cytonn\Presenters\AmountPresenter::currency($price) !!}
                </td>
            </tr>
        </tbody>
    </table>
@endsection