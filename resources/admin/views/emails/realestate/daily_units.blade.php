@extends('emails.email_plain')

@section('content')
    <p>Below is a list of units sold today and those that are remaining</p>
    <h3>SOLD</h3>
    <table>
        <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Unit Number</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach($soldUnits as $unit)
                <tr>
                    <td>{!! $unit->client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($unit->client_id) !!}</td>
                    <td>{!! $unit->unit->number !!}</td>
                    <td>
                        @if($unit->price())
                            {!! \Cytonn\Presenters\AmountPresenter::currency($unit->price()) !!}
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h3>REMAINING</h3>
    <table>
        <thead>
        <tr>
            <th>Unit Number</th>
            <th>Project</th>
        </tr>
        </thead>
        <tbody>
        @foreach($remainingUnits as $unit)
            <tr>
                <td>{!! $unit->number !!}</td>
                <td>{!! $unit->project->name !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection