@extends('emails.email_plain')

@section('content')
    <p>The below Sales Agreement has been uploaded:</p>
    <table>
        <thead>
        <tr>
            <th>Client Code</th>
            <th>Client Name</th>
            <th>Unit Number</th>
            <th>Price</th>
            <th>Date Uploaded</th>
            <th>Document</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{!! $agreement->holding->client->client_code !!}</td>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($agreement->holding->client_id) !!}</td>
            <td>{!! $agreement->holding->unit->number !!}</td>
            <td>
                @if($agreement->holding->price())
                    {!! \Cytonn\Presenters\AmountPresenter::currency($agreement->holding->price()) !!}
                @endif
            </td>
            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($agreement->uploaded_on) !!}</td>
            <td>
                <a href="https://cytonncrims.com/dashboard/documents/{!! $agreement->document_id !!}">Preview</a>
            </td>
        </tr>
        </tbody>
    </table>
@endsection