@extends('emails.email_plain')

@section('content')
    <p>The following reservations have been made this week and require the LOOs to be prepared:</p>
    <table>
        <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Unit Number</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach($looInsuanceReminders as $looInsuanceReminder)
                <tr>
                    <td>{!! $looInsuanceReminder->holding->client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($looInsuanceReminder->holding->client_id) !!}</td>
                    <td>{!! $looInsuanceReminder->holding->unit->number !!}</td>
                    <td>
                        @if($looInsuanceReminder->holding->price())
                            {!! \Cytonn\Presenters\AmountPresenter::currency($looInsuanceReminder->holding->price()) !!}
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection