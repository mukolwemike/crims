@extends('emails.default')

@section('content')
    <p>
        This is a notification of completion of Unit Holding Payment to enable registration of sublease.
    </p>

    <p>Holding Details:</p>

    <table class="table table-responsive table-striped">
        <tbody>
        <tr>
            <td><b>Client</b></td>
            <td class="clearfix"></td>
            <td>{{\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id)}}</td>
        </tr>
        <tr>
            <td><b>Client Code</b></td>
            <td class="clearfix"></td>
            <td>{{$client->client_code}}</td>
        </tr>
        <tr>
            <td><b>Client Email</b></td>
            <td class="clearfix"></td>
            <td>{{$client->contact->email ? $client->contact->email : ''}}</td>
        </tr>
        <tr>
            <td><b>Unit Holding</b></td>
            <td class="clearfix"></td>
            <td>{{$holding->unit->project->name}}</td>
        </tr>
        <tr>
            <td><b>Unit Number</b></td>
            <td class="clearfix"></td>
            <td>{{$holding->unit->number}}</td>
        </tr>
        </tbody>
    </table>
    

    <p>Regards,</p>

    <p class="bold-underline"><strong>Cytonn CRIMS</strong></p>
@endsection