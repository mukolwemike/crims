@extends('emails.email_plain')

@section('content')
    <p>Below (also find attached) are the payment reminders with three weeks or less remaining:</p>

    <table>
        <thead>
            <tr>
                <th>Project</th>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Unit Number</th>
                <th>Unit Type</th>
                <th>Date</th>
                <th>Amount</th>
                <th>FA</th>
            </tr>
        </thead>
        <tbody>
            @foreach($reminders as $reminder)
                <tr>
                    <td>{!! $reminder->holding->project->name !!}</td>
                    <td>{!! $reminder->holding->client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($reminder->holding->client_id) !!}</td>
                    <td>{!! $reminder->holding->unit->number !!}</td>
                    <td>{!! $reminder->holding->unit->size->name !!} {!! $reminder->holding->unit->type->name !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($reminder->date) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($reminder->amount) !!}</td>
                    <td>
                        @if($reminder->holding->commission->recipient)
                            {!! $reminder->holding->commission->recipient->name !!}
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection