@extends('emails.email_plain')

@section('content')

    <!-- Today -->
    @if(count($loos_today_grouped_by_project))
        <p>Below are the LOOs that were sent to clients today</p>

        @foreach($loos_today_grouped_by_project as $key => $loos)
            <?php
            $project = App\Cytonn\Models\Project::findOrFail($key);
            ?>
            <h3>{!! $project->name !!}</h3>
            <table>
                <thead>
                <tr>
                    <th>Client Code</th>
                    <th>Client Name</th>
                    <th>Unit Number</th>
                    <th>Unit Type</th>
                    <th>Price</th>
                    <th>Payment Plan</th>
                    <th>FA</th>
                    <th>Approved By</th>
                    <th>Sent By</th>
                    <th>Sent On</th>
                    <th>Advocate</th>
                </tr>
                </thead>
                <tbody>
                @foreach($loos as $loo)
                    <?php
                    $client = App\Cytonn\Models\Client::findOrFail($loo->holding->client_id);
                    $holding = App\Cytonn\Models\UnitHolding::findOrFail($loo->holding_id);
                    ?>
                    <tr>
                        <td>{!! $client->client_code !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                        <td>{!! $holding->unit->number !!}</td>
                        <td>{!! $holding->unit->size->name !!} {!! $holding->unit->type->name !!}</td>
                        <td>
                            @if($holding->price())
                                {!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->paymentPlan)
                                {!! $holding->paymentPlan->name !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->commission->recipient)
                                {!! $holding->commission->recipient->name !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->pm_approval_id)
                                {!! \Cytonn\Presenters\UserPresenter::presentFullNames($holding->loo->pm_approval_id) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->sent_by)
                                {!! \Cytonn\Presenters\UserPresenter::presentFullNames($holding->loo->sent_by) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->sent_on)
                                {!! \Cytonn\Presenters\DatePresenter::formatDate($holding->loo->sent_on) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->advocate_id)
                                {!! App\Cytonn\Models\Advocate::findOrFail($holding->loo->advocate_id)->name !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    @else
        <p>There were no LOOs that were sent today</p>
    @endif

    <br/><br/>
    <!-- This Week -->
    @if(count($loos_this_week_grouped_by_project))
        <p>Below are the LOOs that have been sent to clients this week.</p>

        @foreach($loos_this_week_grouped_by_project as $key => $loos)
            <?php
            $project = App\Cytonn\Models\Project::findOrFail($key);
            ?>
            <h3>{!! $project->name !!}</h3>
            <table>
                <thead>
                <tr>
                    <th>Client Code</th>
                    <th>Client Name</th>
                    <th>Unit Number</th>
                    <th>Unit Type</th>
                    <th>Price</th>
                    <th>Payment Plan</th>
                    <th>FA</th>
                    <th>Approved By</th>
                    <th>Sent By</th>
                    <th>Sent On</th>
                    <th>Advocate</th>

                </tr>
                </thead>
                <tbody>
                @foreach($loos as $loo)
                    <?php
                    $client = App\Cytonn\Models\Client::findOrFail($loo->holding->client_id);
                    $holding = App\Cytonn\Models\UnitHolding::findOrFail($loo->holding_id);
                    ?>
                    <tr>
                        <td>{!! $client->client_code !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                        <td>{!! $holding->unit->number !!}</td>
                        <td>{!! $holding->unit->size->name !!} {!! $holding->unit->type->name !!}</td>
                        <td>
                            @if($holding->price())
                                {!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->paymentPlan)
                                {!! $holding->paymentPlan->name !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->commission->recipient)
                                {!! $holding->commission->recipient->name !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->pm_approval_id)
                                {!! \Cytonn\Presenters\UserPresenter::presentFullNames($holding->loo->pm_approval_id) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->sent_by)
                                {!! \Cytonn\Presenters\UserPresenter::presentFullNames($holding->loo->sent_by) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->sent_on)
                                {!! \Cytonn\Presenters\DatePresenter::formatDate($holding->loo->sent_on) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->advocate_id)
                                {!! App\Cytonn\Models\Advocate::findOrFail($holding->loo->advocate_id)->name !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    @else
        <p>There were no LOOs that have been sent this week.</p>
    @endif

    <br/><br/>
    <!-- This Month -->
    @if(count($loos_this_month_grouped_by_project))
        <p>Below are the LOOs that have been sent to clients this month.</p>

        @foreach($loos_this_month_grouped_by_project as $key => $loos)
            <?php
            $project = App\Cytonn\Models\Project::findOrFail($key);
            ?>
            <h3>{!! $project->name !!}</h3>
            <table>
                <thead>
                <tr>
                    <th>Client Code</th>
                    <th>Client Name</th>
                    <th>Unit Number</th>
                    <th>Unit Type</th>
                    <th>Price</th>
                    <th>Payment Plan</th>
                    <th>FA</th>
                    <th>Approved By</th>
                    <th>Sent By</th>
                    <th>Sent On</th>
                    <th>Advocate</th>
                </tr>
                </thead>
                <tbody>
                @foreach($loos as $loo)
                    <?php
                    $client = App\Cytonn\Models\Client::findOrFail($loo->holding->client_id);
                    $holding = App\Cytonn\Models\UnitHolding::findOrFail($loo->holding_id);
                    ?>
                    <tr>
                        <td>{!! $client->client_code !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                        <td>{!! $holding->unit->number !!}</td>
                        <td>{!! $holding->unit->size->name !!} {!! $holding->unit->type->name !!}</td>
                        <td>
                            @if($holding->price())
                                {!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->paymentPlan)
                                {!! $holding->paymentPlan->name !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->commission->recipient)
                                {!! $holding->commission->recipient->name !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->pm_approval_id)
                                {!! \Cytonn\Presenters\UserPresenter::presentFullNames($holding->loo->pm_approval_id) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->sent_by)
                                {!! \Cytonn\Presenters\UserPresenter::presentFullNames($holding->loo->sent_by) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->sent_on)
                                {!! \Cytonn\Presenters\DatePresenter::formatDate($holding->loo->sent_on) !!}
                            @endif
                        </td>
                        <td>
                            @if($holding->loo->advocate_id)
                                {!! App\Cytonn\Models\Advocate::findOrFail($holding->loo->advocate_id)->name !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    @else
        <p>There were no LOOs that have been sent this month.</p>
    @endif
@endsection