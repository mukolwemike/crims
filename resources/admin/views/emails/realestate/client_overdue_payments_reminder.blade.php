@extends('emails.email_plain')

@section('content')

    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Cytonn Real Estate takes this opportunity to thank you for investing in Real Estate. <br><br>

        This is to remind you of your overdue {!! str_plural('payment', count($schedules_arr)) !!} as per the schedule. <br><br>

        Kindly purpose to make the said payment.
    </p>

    <table>
        <thead>
            <tr>
                <td>Project</td>
                <th>Unit Number</th>
                <th>Unit Type</th>
                <th>Date</th>
                <th>Overdue</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = 0; ?>
            @foreach($schedules_arr as $schedule)
                <tr>
                    <?php $amount = $schedule->repo->overdue();  $total += $amount ?>
                    <td>{!! $schedule->holding->project->name !!}</td>
                    <td>{!! $schedule->holding->unit->number !!}</td>
                    <td>{!! $schedule->holding->unit->size->name !!} {!! $schedule->holding->unit->type->name !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($amount) !!}</td>
                    <td>{!! $schedule->description !!}</td>
                </tr>
            @endforeach
            @if(count($schedules_arr) > 1)
                <tr>
                    <th colspan="4">Total</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th><td></td>
                </tr>
            @endif
        </tbody>
    </table>

    <p>Below are the bank details for the project(s):</p>
    <table>
        <thead>
            <tr>
                <th>Project</th>
                <th>Bank Name</th>
                <th>Bank Branch</th>
                <th>Acc. Name</th>
                <th>Acc. No.</th>
                <th>Swift Code</th>
                <th>Clearing Code</th>
            </tr>
        </thead>
        <tbody>
            @foreach($projects($schedules_arr) as $project)
                <tr>
                    <td>{!! $project->name !!}</td>
                    <td>{!! $project->bank_name !!}</td>
                    <td>{!! $project->bank_branch_name !!}</td>
                    <td>{!! $project->vendor !!}</td>
                    <td>{!! $project->bank_account_number !!}</td>
                    <td>{!! $project->bank_swift_code !!}</td>
                    <td>{!! $project->bank_clearing_code !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p>Once again, thank you for choosing us to deliver to your investment promise. <br></p>
    <p>Kind regards,</p>
    <p>{!! $email_sender !!}</p>
@endsection