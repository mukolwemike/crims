@extends('emails.email_plain')

@section('content')
    <p>The below LOO has been approved:</p>
    <table>
        <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Project</th>
                <th>Unit Number</th>
                <th>Price</th>
                <th>Date Approved</th>
                <th>Approver</th>
                <th>Document</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{!! $loo->holding->client->client_code !!}</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($loo->holding->client_id) !!}</td>
                <td>{!! $loo->holding->unit->project->name !!}</td>
                <td>{!! $loo->holding->unit->number !!}</td>
                <td>
                    @if($loo->holding->price())
                        {!! \Cytonn\Presenters\AmountPresenter::currency($loo->holding->price()) !!}
                    @endif
                </td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($loo->pm_approved_on) !!}</td>
                <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($loo->pm_approval_id) !!}</td>
                <td>
                    <a href="/dashboard/realestate/reservations/show-loo/{!! $loo->id !!}">Preview</a>
                </td>
            </tr>
        </tbody>
    </table>
@endsection