@extends('emails.email_plain')

@section('content')
    <style>
        tr .green, .green, .green td
        {
            background-color: #006666;
            color: #FFFFFF;
        }
        .text-center
        {
            text-align: center;
        }
    </style>
    <p>Below is a summary report of the tranches as at {!! \Cytonn\Presenters\DatePresenter::formatDate(\Carbon\Carbon::today()->toDateString()) !!}. Find attached a more detailed report.</p>

    @foreach($tranches_grouped_by_project as $project => $tranches_arr)
        <table>
            <thead>
                <tr class="green">
                    <th class="bold" colspan="3">{!! strtoupper(App\Cytonn\Models\Project::findOrFail($project)->name) !!}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tranches_arr as $tranche)
                <tr>
                    <th colspan="3">{!! $tranche->name !!}</th>
                </tr>
                <tr>
                    <th>Total</th>
                    <th>Reserved</th>
                    <th>Remaining</th>
                </tr>
                <tr>
                    <td class="text-center">{!! $tranche->sizes->sum('number') !!} units</td>
                    <td class="text-center">{!! $count = $tranche->holdings()->where('active', 1)->count() !!} units</td>
                    <td class="text-center">{!! $tranche->sizes->sum('number') - $count !!} units</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br /><br />
    @endforeach

@endsection