@extends('emails.email_plain')

@section('content')
    <style>
        tr .green, .green, .green td
        {
            background-color: #006666;
            color: #FFFFFF;
        }
        .text-center
        {
            text-align: center;
        }
    </style>

    <p>The attached is the reporting for the Units with sent Letters of Offer but with pending Sale Agreements as at {!! \Cytonn\Presenters\DatePresenter::formatDate($today->toDateString()) !!}</p>
    @if(count($holdings) > 0)
        <table>
            <thead>
            <tr>
                <th>No:</th>
                <th>Project Name</th>
                <th>Number of Units</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 0 ?>
            @foreach($holdings as $key => $reHoldings)
                <?php
                    $project = App\Cytonn\Models\Project::findOrFail($key);
                    $i++;
                ?>
                <tr>
                    <td>{!! $i !!}</td>
                    <td> {!! $project->name !!}</td>
                    <td>{!! $reHoldings->count() !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection