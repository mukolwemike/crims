@extends('emails.email_plain')

@section('content')
    <p>Below is the weekly summary for the real estate units</p>
    @foreach($projects as $project)
        <h3>{!! $project->name !!}</h3>
        <table>
            <thead>
                <tr>
                    <th>Tranche</th>
                    <th>Size</th>
                    <th>Sold</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($project->tranches as $tranche)
                    <tr>
                        <td colspan="100%">{!! $tranche->name !!}</td>
                    </tr>
                    @foreach($tranche->sizes as $tranche_sizing)
                        <tr>
                            <td></td><td>{!! $tranche_sizing->size->name !!}</td> <td>{!! $tranche->holdings->count() !!}</td> <td>{!! $tranche_sizing->size->units->count() !!}</td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>
    @endforeach

    <h3>Units With LOOs</h3>
    <table>
        <thead>
        <tr>
            <th>Unit</th>
            <th>Project</th>
            <th>Client</th>
            <th>Document</th>
        </tr>
        </thead>
        <tbody>
        @foreach($loos as $loo)
            <tr>
                <td>{!! $loo->holding->unit->number !!}</td>
                <td>{!! $loo->holding->unit->project->name !!}</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($loo->holding->client_id) !!}</td>
                <td><a href="/dashboard/realestate/reservations/show-loo/{!! $loo->id !!}">View Document</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h3>Units With Sales Agreement</h3>
    <table>
        <thead>
        <tr>
            <th>Unit</th>
            <th>Project</th>
            <th>Client</th>
        </tr>
        </thead>
        <tbody>
        @foreach($salesAgreements as $salesAgreement)
            <tr>
                <td>{!! $salesAgreement->holding->unit->number !!}</td>
                <td>{!! $salesAgreement->holding->unit->project->name !!}</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($salesAgreement->holding->client_id) !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h3>Units Bookings</h3>
    <table>
        <thead>
        <tr>
            <th>Unit</th>
            <th>Project</th>
            <th>Reservation Date</th>
            <th>Client</th>
        </tr>
        </thead>
        <tbody>
        @foreach($bookings as $booking)
            <tr>
                <td>{!! $booking->holding->unit->number !!}</td>
                <td>{!! $booking->holding->unit->project->name !!}</td>
                <td>{!! $booking->reservation_date !!}</td>
                <td>{!! $booking->firstname !!} {!! $booking->middlename !!} {!! $booking->lastname !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection