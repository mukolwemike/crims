@extends('emails.email_plain')

@section('content')
    <p>Please find attached an Excel document of the all the Letters of Offer that have been sent for each project.</p>
@endsection