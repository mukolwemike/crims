@extends('emails.email_plain')

@section('content')
    <p>Below are the LOOs whose respective sales agreement have not been signed:</p>
    <table>
        <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Unit Number</th>
                <th>Price</th>
                <th>Due Date</th>
                <th>Date Signed</th>
                <th>Date Sent to Lawyer</th>
                <th>Document</th>
            </tr>
        </thead>
        <tbody>
            @foreach($loos as $loo)
                <tr>
                    <td>{!! $loo->holding->client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($loo->holding->client_id) !!}</td>
                    <td>{!! $loo->holding->unit->number !!}</td>
                    <td>
                        @if($loo->holding->price())
                            {!! \Cytonn\Presenters\AmountPresenter::currency($loo->holding->price()) !!}
                        @endif
                    </td>
                    <td>{!! $loo->due_date !!}</td>
                    <td>{!! $loo->date_signed !!}</td>
                    <td>
                        @if(is_null($loo->date_sent_to_lawyer))
                            NOT SENT
                        @else
                            {!! $loo->date_sent_to_lawyer !!}
                        @endif
                    </td>
                    <td><a href="{!! getenv('DOMAIN') !!}/dashboard/realestate/reservations/show-loo/{!! $loo->id !!}">Preview</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection