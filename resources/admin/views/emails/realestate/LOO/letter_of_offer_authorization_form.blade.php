@extends('emails.email_plain')

@section('content')
    <p>
        Please find attached an LOO Authorization form for
        {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($loo->holding->client_id) !!} for
        {!! $loo->holding->unit->size->name !!} {!! $loo->holding->unit->type->name !!}  ({!! $loo->holding->unit->number !!}) in
        {!! $loo->holding->unit->project->name !!}. <br><br>
    </p>

    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection