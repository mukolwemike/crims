@extends('emails.email_plain')

@section('content')
    <style>
        ol li a {
            text-decoration: none;
        }
    </style>
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Cytonn Real Estate takes this opportunity to thank you for investing in our Real Estate product, {!! $loo->holding->project->name !!}. <br><br>

        Please find attached a Letter of Offer for the {!! $loo->holding->unit->size->name !!} {!! $loo->holding->unit->type->name !!} unit - {!! $loo->holding->unit->number !!}. <br><br>

        Once again, thank you for choosing us to deliver to your investment promise. <br>

    </p>
    {{--@if($loo_compliance_documents->count())--}}
        {{--<p>--}}
            {{--@if($loo_compliance_documents->count() > 1)--}}
                {{--Below are the compliance documents:<br/>--}}
            {{--@else--}}
                {{--Below is the compliance document:<br/>--}}
            {{--@endif--}}
            {{--<ol type="1">--}}
                {{--@foreach($loo_compliance_documents as $document)--}}
                    {{--<li><a href="{!! getenv('DOMAIN') . '/' . $document->path() !!}">{!! $document->title !!}</a> </li>--}}
                {{--@endforeach--}}
            {{--</ol>--}}
        {{--</p>--}}
    {{--@endif--}}
    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection