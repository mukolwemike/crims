@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>This is to remind you of the upcoming payment(s)</p>

    <table>
        <thead>
            <tr>
                <th>Unit</th>
                <th>Typology</th>
                <th>Size</th>
                <th>Project</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Description</th>
            </tr>
        </thead>

        <tbody>
            @foreach($holdings as $holding)
                <tr>
                    <td>{!! $holding->unit->number !!}</td>
                    <td>{!! $holding->unit->type->name !!}</td>
                    <td>{!! $holding->unit->type->name !!}</td>
                    <td>{!! $holding->unit->project->name !!}</td>
                    @for($i = 0; $i < 3; $i++)
                        <td></td>
                    @endfor
                </tr>
                @foreach($holding->schedules_on_date as $schedule)
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{!! $schedule->date !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                        <td>{!! $schedule->description !!}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
        <tfoot>
            <tr style="background-color: yellow; color: red;">
                <td colspan="5" style="text-align: center; font-weight:bold">TOTAL</td>
                <td>{!! $holding->schedules_on_date->sum('amount') !!}</td>
                <td></td>
            </tr>
        </tfoot>
    </table>

    <p>Below are the bank details for the {!! str_plural('project', $projects->count()) !!}:</p>
    <table>
        <thead>
        <tr>
            <th>Project</th>
            <th>Bank Name</th>
            <th>Bank Branch</th>
            <th>Acc. Name</th>
            <th>Acc. No.</th>
            <th>Swift Code</th>
            <th>Clearing Code</th>
        </tr>
        </thead>

        <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{!! $project->name !!}</td>
                <td>{!! $project->bank_name !!}</td>
                <td>{!! $project->bank_branch_name !!}</td>
                <td>{!! $project->vendor !!}</td>
                <td>{!! $project->bank_account_number !!}</td>
                <td>{!! $project->bank_swift_code !!}</td>
                <td>{!! $project->bank_clearing_code !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <p>Kind regards,</p>
    <p>{!! $email_sender !!}</p>
@endsection