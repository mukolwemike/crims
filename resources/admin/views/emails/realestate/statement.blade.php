@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Please find attached
        @if($projects->count() < 2)
            {!! ucwords($projects->first()->name) !!}
        @else
            Real Estate
        @endif

        Statement as at {!! \Cytonn\Presenters\DatePresenter::formatDate($statementDate) !!}.

        <br/><br/>

        Please note that this statement is password protected for security reasons. The password is your 4 digit client
        code XXXX.

        <br/>

    <?php $projectArray = $projects->pluck('id')->toArray() ?>

    @if(in_array(2, $projectArray) || in_array(3, $projectArray) || in_array(6, $projectArray))
        <p>Kindly note that bank account details for Real Estate payments have changed. To make a payment on your unit,
            please use the bank details below:</p>
        @if(in_array(2, $projectArray))
            <span><strong>The Alma</strong></span><br/>
            <strong>Bank:</strong> SBM Bank<br/>
            <strong>Account Name:</strong> Cytonn Integrated Project LLP<br/>
            <strong>Account Name:</strong> 0212307282004<br/>
            <strong>Branch:</strong> Riverside Drive<br/>
            <br/>
        @endif
        @if(in_array(6, $projectArray))
            <span><strong>RiverRun Estates</strong></span><br/>
            <strong>Bank:</strong> SBM Bank<br/>
            <strong>Account Name:</strong> Cytonn Investment Partners Five LLP<br/>
            <strong>Account Name:</strong> 0082273792001<br/>
            <strong>Branch:</strong> Riverside Drive<br/>
            <br/>
        @endif
        @if(in_array(3, $projectArray))
            <span><strong>Situ Village</strong></span><br/>
            <strong>Bank:</strong> SBM Bank<br/>
            <strong>Account Name:</strong> Cytonn Investment Partners One LLP<br/>
            <strong>Account Name:</strong> 0082220485001<br/>
            <strong>Branch:</strong> Riverside Drive<br/>
        @endif
    @endif

    @if($client->fund_manager_id != 2 && $campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif

{{--    <strong>--}}
{{--        We are conducting a survey to gauge your level of satisfaction with our products and services. Please take a--}}
{{--        few minutes to give us feedback and recommend areas of improvement by filling this brief--}}
{{--        <a href="https://forms.gle/1BdWu28TvnJxcQmM8">Client Satisfaction Survey Form</a>.--}}
{{--    </strong>--}}

{{--    <br/><br>--}}

    Thank you for investing with us.

    <br/><br/>

    Kind regards,<br/>

    @include('emails.client_service_signature')
    </p>
@endsection


