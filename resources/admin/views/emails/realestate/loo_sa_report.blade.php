@extends('emails.email_plain')

@section('content')
    <style>
        tr .green, .green, .green td
        {
            background-color: #006666;
            color: #FFFFFF;
        }
        .text-center
        {
            text-align: center;
        }
    </style>

    <p>Below is the reporting for Letters of Offer and Sale Agreements on {!! \Cytonn\Presenters\DatePresenter::formatDate($today->toDateString()) !!}</p>

    <table>
        <tbody>
            <tr class="green">
                <th colspan="2">Projects with Sent LOOs</th>
            </tr>
            @if(count($loos_sent_grouped_by_project) > 0)
                @foreach($loos_sent_grouped_by_project as $project_id => $loos_arr)
                    <tr>
                        <td>{!! @App\Cytonn\Models\Project::findOrFail($project_id)->name !!}</td>
                        <td>{!! count($loos_arr) !!}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" class="text-center">There were no LOOs sent</td>
                </tr>
            @endif

            <tr class="green">
                <th colspan="2">Projects with Signed LOOs</th>
            </tr>
            @if(count($loos_signed_grouped_by_project) > 0)
                @foreach($loos_signed_grouped_by_project as $project_id => $loos_arr)
                    <tr>
                        <td>{!! @App\Cytonn\Models\Project::findOrFail($project_id)->name !!}</td>
                        <td>{!! count($loos_arr) !!}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" class="text-center">There were no LOOs signed</td>
                </tr>
            @endif

            <tr class="green">
                <th colspan="2">Projects with Signed SAs</th>
            </tr>
            @if(count($sas_signed_grouped_by_project) > 0)
                @foreach($sas_signed_grouped_by_project as $project_id => $sas_arr)
                    <tr>
                        <td>{!! @App\Cytonn\Models\Project::findOrFail($project_id)->name !!}</td>
                        <td>{!! count($sas_arr) !!}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="2" class="text-center">There were no SAs signed</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection