@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->client_type_id == 2)
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Find attached the above-captioned Notice for your attention and information.
    </p>
    <p>
        Please note that as indicated therein, your Reservation shall stand automatically cancelled if we are not
        in receipt of the Deposit on or before the expiry of the extended 14 day period which period starts running
        from the date of this e-mail.
    </p>

    <p>We look forward to your prompt and positive action.</p>
    <p>
        Kind regards,

        {!! $email_sender !!}
    </p>

@endsection