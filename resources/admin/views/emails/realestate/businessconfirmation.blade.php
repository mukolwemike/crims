@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>

    <p>
        Cytonn Real Estate takes this opportunity to thank you for investing in our Real Estate
        product, {!! $project->name !!}. <br><br>

        Please find attached a Business Confirmation for the {!! $payment->type->name !!} for
        the {!! $unit->size->name !!} unit. <br><br>

        Kindly note that you can manage your investment from our online portal at clients.cytonn.com, or through our
        apps
        <a href="https://apps.apple.com/us/app/cytonn/id1411423248">App Store</a> and
        <a href="https://play.google.com/store/apps/details?id=com.cytonn">Play Store</a>. <br><br>

        You can request for your login details by emailing us at <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
        <br><br>

        <strong>
            To provide security and confidentiality, the documents we send you (business confirmations and statements)
            will be password protected. When prompted for a password, please enter your client code.
        </strong>
    </p>

    <p>
        Thank you for choosing Cytonn as your preferred Investment Manager
    </p>

    <p>Kind regards,</p>

    {!! $email_sender !!}

{{--    @include('emails.client_service_signature')--}}

@endsection
