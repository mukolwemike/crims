@extends('emails.email_plain')

@section('content')
    <style>
        tr .green, .green, .green td
        {
            background-color: #006666;
            color: #FFFFFF;
        }
        .text-center
        {
            text-align: center;
        }
    </style>

    <p>{!! $content !!}</p>

    <h3 class="text-center">Summary of Project Tranches</h3>

    <table>
        <thead>
            <tr>
                <th class="bold" colspan="5">{!! strtoupper($project->name) !!}</th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Total</th>
                <th>Sold</th>
                <th>Remaining</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach($project->tranches as $tranche)
            <tr>
                <td>{!! $tranche->name !!}</td>
                <td>{!! $tranche->sizes->sum('number') !!} units</td>
                <td>{!! $count = $tranche->holdings()->where('active', 1)->count() !!} units</td>
                <td>{!! $tranche->sizes->sum('number') - $count !!} units</td>
                <td></td>
            </tr>
            <tr><td colspan="5">&nbsp;</td></tr>
            <tr class="green">
                <td></td>
                <th>Size</th>
                <th>Total</th>
                <th>Payment Plan</th>
                <th>Sold</th>
            </tr>
            @foreach($tranche->sizes as $sizing)
                <tr>
                    <td></td>
                    <th>{!! $findSize($sizing->size_id)->name !!}</th>
                    <td>
                        @if(isset($sizing->number))
                            {!! $sizing->number !!} units
                        @endif
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                @foreach($sizing->prices as $pricing)
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{!! $pricing->paymentPlan->name !!}</td>
                        <td>{!! $sold($tranche, $pricing->paymentPlan, $sizing->size) !!}</td>
                    </tr>
                @endforeach
            @endforeach
            <tr><td colspan="5">&nbsp;</td></tr>
        @endforeach
        </tbody>
    </table>
@endsection
