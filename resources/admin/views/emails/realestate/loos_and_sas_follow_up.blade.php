@extends('emails.email_plain')

@section('content')

    <style>
        .text-center { text-align: center; }
    </style>

    <p>Below is the cumulative LOOs & SAs Follow Up</p>

    <table>
        <thead>
        <tr>
            <th>Projects</th>
            <th>No. of Units Reserved</th>
            <th>No. of Signed SAs</th>
            <th>No. of Signed LOOs</th>
            <th>LOOs Issued But Not Signed By Clients</th>
            <th>LOOs To Be Prepared</th>
            <th>Total No. of CRE Clients</th>
            <th>CRE Clients With Forfeitures</th>
        </tr>
        </thead>
        <tbody>
        @foreach($loos_sas_follow_up as $project)
            <tr>
                <th>{!! $project->{'Project'} !!}</th>
                <td class="text-center">{!! $project->{'Units Reserved'} !!}</td>
                <td class="text-center">{!! $project->{'Signed SAs'} !!}</td>
                <td class="text-center">{!! $project->{'Signed LOOs'} !!}</td>
                <td class="text-center">{!! $project->{'LOOs Not Signed'} !!}</td>
                <td class="text-center">{!! $project->{'LOOs To Be Prepared'} !!}</td>
                <td class="text-center">{!! $project->{'Total CRE Clients'} !!}</td>
                <td class="text-center">{!! $project->{'Forfeitures'} !!}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot style="color: red; background-color: yellow;">
            <tr>
                <th>TOTAL</th>
                <th>{!! $loos_sas_follow_up->sum('Units Reserved') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Signed SAs') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Signed LOOs') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('LOOs Not Signed') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('LOOs To Be Prepared') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Total CRE Clients') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Forfeitures') !!}</th>
            </tr>
        </tfoot>
    </table>

    <p>Below is the Today's LOOs & SAs Follow Up</p>

    <table>
        <thead>
            <tr>
                <th>Projects</th>
                <th>No. of Units Reserved</th>
                <th>No. of Signed SAs</th>
                <th>No. of Signed LOOs</th>
                <th>LOOs Issued But Not Signed By Clients</th>
                <th>LOOs To Be Prepared</th>
                <th>CRE Clients</th>
                <th>Forfeitures</th>
            </tr>
        </thead>
        <tbody>
        @foreach($loos_sas_follow_up as $project)
            <tr>
                <th>{!! $project->{'Project'} !!}</th>
                <td class="text-center">{!! $project->{'Units Reserved Today'} !!}</td>
                <td class="text-center">{!! $project->{'Signed SAs Today'} !!}</td>
                <td class="text-center">{!! $project->{'Signed LOOs Today'} !!}</td>
                <td class="text-center">{!! $project->{'LOOs Not Signed Today'} !!}</td>
                <td class="text-center">{!! $project->{'LOOs To Be Prepared Today'} !!}</td>
                <td class="text-center">{!! $project->{'Total CRE Clients Today'} !!}</td>
                <td class="text-center">{!! $project->{'Forfeitures Today'} !!}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot style="color: red; background-color: yellow;">
            <tr>
                <th>TOTAL</th>
                <th>{!! $loos_sas_follow_up->sum('Units Reserved Today') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Signed SAs Today') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Signed LOOs Today') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('LOOs Not Signed Today') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('LOOs To Be Prepared Today') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Total CRE Clients Today') !!}</th>
                <th>{!! $loos_sas_follow_up->sum('Forfeitures Today') !!}</th>
            </tr>
        </tfoot>
    </table>
@endsection