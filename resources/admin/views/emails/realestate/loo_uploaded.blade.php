@extends('emails.email_plain')

@section('content')
    <p>The following client has signed the loo and paid the 10% deposit. The client will require that a sales agreement is prepared.</p>
    <table>
        <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Project</th>
                <th>Unit Number</th>
                <th>Price</th>
                <th>Due Date</th>
                <th>Date Signed</th>
                <th>Document</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{!! $loo->holding->client->client_code !!}</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($loo->holding->client_id) !!}</td>
                <td>{!! $loo->holding->unit->project->name !!}</td>
                <td>{!! $loo->holding->unit->number !!}</td>
                <td>
                    {!! \Cytonn\Presenters\AmountPresenter::currency($loo->holding->price()) !!}
                </td>
                <td>{!! $loo->due_date !!}</td>
                <td>{!! $loo->date_signed !!}</td>
                <td>
                    <a href="{!! getenv('DOMAIN') !!}/dashboard/realestate/loos/show/{!! $loo->id !!}">Preview</a>
                </td>
            </tr>
        </tbody>
    </table>
@endsection