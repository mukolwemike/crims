@extends('emails.email_plain')

@section('content')
    <p>The below LOO has been rejected:</p>
    <table>
        <thead>
            <tr>
                <th>Client Code</th>
                <th>Client Name</th>
                <th>Project</th>
                <th>Unit Number</th>
                <th>Price</th>
                <th>Rejected By</th>
                <th>Rejected On</th>
                <th>Reason</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{!! $loo->holding->client->client_code !!}</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($loo->holding->client_id) !!}</td>
                <td>{!! $loo->holding->unit->project->name !!}</td>
                <td>{!! $loo->holding->unit->number !!}</td>
                <td>
                    @if($loo->holding->price())
                        {!! \Cytonn\Presenters\AmountPresenter::currency($loo->holding->price()) !!}
                    @endif
                </td>
                <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($loo_reject->pm_id) !!}</td>
                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate(\Carbon\Carbon::create($loo_reject->created_at)->toDateString()) !!}</td>
                <td>{!! $loo_reject->reason !!}</td>
            </tr>
        </tbody>
    </table>
@endsection