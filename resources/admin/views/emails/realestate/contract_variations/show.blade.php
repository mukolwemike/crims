@extends('emails.email_plain')

@section('content')
    <style>
        ol li a {
            text-decoration: none;
        }
    </style>
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>
    <p>
        Cytonn Real Estate takes this opportunity to thank you for investing in our Real Estate product, {!! $variation->loo->holding->project->name !!}. <br><br>

        Please find attached a Contract Variation for the {!! $variation->loo->holding->unit->size->name !!} {!! $variation->loo->holding->unit->type->name !!} unit - {!! $variation->loo->holding->unit->number !!}. <br><br>

        Once again, thank you for choosing us to deliver to your investment promise. <br>

    </p>

    <p>
        Kind regards,
    </p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection