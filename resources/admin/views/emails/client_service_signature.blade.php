<div>
    <p><b>Client Services &nbsp;|&nbsp; Cytonn Investments Management Plc</b></p>
    <p>
        <img style="max-height: 15px; width: auto;"
             src="{!! 'https://crims-admin.cytonn.net/assets/img/ico-email.png' !!}" alt=""/> : <a
                href="mailto:clientservices@cytonn.com">clientservices@cytonn.com</a> &nbsp;&nbsp;
        <img style="max-height: 15px; width: auto;"
             src="{!! 'https://crims-admin.cytonn.net/assets/img/ico-phone.png' !!}" alt=""/> : +254 709 101 200
    </p>
    <p><img style="max-height: 15px; width: auto;"
            src="{!! 'https://crims-admin.cytonn.net/assets/img/ico-web.png' !!}" alt="" border="0"/> : <a
                href="https://cytonn.com">www.cytonn.com</a></p>

</div>