@extends('emails.client_default')

@section('content')
    <div>
        <h3>CRIMS Permission Assigned To User</h3>
        <p>Dear Team,</p>
        <p>A permission has been assigned to a user under the following details in the sytem</p>
        <table>
            <tbody>
            <tr>
                <td><b style="font-weight: bold">Name:</b></td>
                <td>{!! $fullname !!}</td>
            </tr>
            <tr>
                <td><b style="font-weight: bold">Email:</b></td>
                <td>{!! $email !!}</td>
            </tr>
            <tr>
                <td><b style="font-weight: bold">Permission:</b></td>
                <td>{!! $permission !!}</td>
            </tr>
            <tr>
                <td><b style="font-weight: bold">Added By:</b></td>
                <td>{!! $user !!}</td>
            </tr>
            </tbody>
        </table>
        <p>Kindly follow this <a href="{{ URL::to('dashboard/users/details/'. $id) }}">link</a> to view the user details.</p>

        Regards,<br/>
        Cytonn CRIMS.
        </p>
    </div>
@endsection