@extends('emails.default')

@section('content')
    <div>
        <p>
            @if($type === 'sender')
                Hi <b>{!! \Cytonn\Presenters\ClientPresenter::presentFirstName($sender->id)!!}</b>, this to confirm
                the transfer request of <b>{{$units}}</b> units from your CMMF to
                <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($receiver->id)!!}</b>
                - <b>{{$receiver->client_code}}</b>
                has been processed and approved. Thank you for doing business with us.
            @elseif($type === 'recipient')
                Great news, <b>{!! \Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($receiver->id)!!}</b> has
                transferred <b>{{$units}}</b> units to your CMMF account. Thank you for doing
                business with us.
            @endif
            <br/>
            <br/>

            Regards,<br/>
            Cytonn CRIMS.
        </p>
    </div>
@endsection