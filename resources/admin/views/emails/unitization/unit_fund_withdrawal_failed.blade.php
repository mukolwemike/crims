@extends('emails.client_default')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    <p>
        Kindly note that your request to withdraw from {{ $product }} could not be processed. <br><br>
        <b>Reason:</b> {{ $reason }}. <br><br>

        Please try again later.
    </p>

    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>

    <p>Your sincerely,</p>

    @include('emails.caml_client_service_signature')
@endsection