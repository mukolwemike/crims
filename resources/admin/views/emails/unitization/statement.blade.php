@extends('emails.email_plain')

@section('content')
    <p>
        @if($client->clientType->name == 'corporate')
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
        @else
            Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
        @endif
    </p>

    <p>
        Please find attached Statement for {{ $unitFund->name }} as
        at {!! \Cytonn\Presenters\DatePresenter::formatDate($statementDate) !!}. <br><br>

        Thank you for investing with us.
    </p>

    <p>
        Please note that this document is password protected for security reasons. The password is your assigned account
        number.
    </p>

    @if($campaign && $campaign->statement_message != '')
        {!! $campaign->statement_message !!}
    @endif

{{--    <p>--}}
{{--        <strong>--}}
{{--            We are conducting a survey to gauge your level of satisfaction with our products and services. Please take a--}}
{{--            few minutes to give us feedback and recommend areas of improvement by filling this brief--}}
{{--            <a href="https://forms.gle/1BdWu28TvnJxcQmM8">Client Satisfaction Survey Form</a>.--}}
{{--        </strong>--}}
{{--    </p>--}}

    <p>Kind regards,</p>

    @include('emails.caml_client_service_signature')

@endsection
