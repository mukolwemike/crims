@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    @if($unitFund->category->slug == 'money-market-fund')

        <p>Thank you for choosing to invest in the Cytonn Unit Trust.</p>

        <p>Please find attached your redemption confirmation letter.</p>

        <p>Should you require any further details or clarifications, please do not hesitate to contact us via email or
            the numbers below.</p>

        <p>Please note that this document is password protected for security reasons. The password is your assigned
            account number.</p>

    @else

        <p>
            Kindly find attached your redemption confirmation letter for the investment
            in {{ ucfirst($unitFund->name) }}
        </p>
        <p>
            We thank you for your continued support and for choosing Cytonn as your preferred Investment Manager.
        </p>

    @endif

    <p>Kind regards,</p>

    @if(!isset($companyAddress))
        <p>
            {!! $emailSender !!}
        </p>
    @else
        @include('emails.caml_client_service_signature')
{{--        <p>Pauline Ayoki </p>--}}
{{--        <p>Administrator </p>--}}
{{--        <p>Mobile : 0714143588</p>--}}
{{--        <p>Email : <a href="payoki@serianiasset.com">payoki@serianiasset.com</a></p>--}}
    @endif
@endsection