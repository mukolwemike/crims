@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    <p>
        Cytonn Investments takes this opportunity to thank you for investing in our
        product, {!! ucfirst( $unitFund->name ) !!}
        @if($unitFund->short_name === 'CMMF')
            , a wallet that you stand to earn upto 11% p.a.
        @endif
    </p>

    <p>We have herein attached the business confirmation letter for your records.</p>

    @if($unitFund->deposit_code)
        <p>
            <strong>
                Kindly note that you can also make subsequent top-ups via our Mpesa pay bill No. 775093 and input your
                client code plus {{$unitFund->deposit_code}} initials at the end i.e 1234{{$unitFund->deposit_code}} as your Account Number.
            </strong>
        </p>
    @endif

    <p>
        Kindly note that you can manage your investment from our online portal at clients.cytonn.com,
        @if($unitFund->short_name === 'CMMF')
            via our short code *809#
        @endif
        or through our apps
        <a href="https://apps.apple.com/us/app/cytonn/id1411423248">App Store</a> and
        <a href="https://play.google.com/store/apps/details?id=com.cytonn">Play Store</a>.
    </p>

    <p>
        Should you require any further details or clarifications, please do not hesitate to contact us via
        <a href="mailto:operations@cytonn.com">operations@cytonn.com</a> or +254 709 101 200.
    </p>

    <p>
        <strong>
            To provide security and confidentiality, the documents we send you (business confirmations and statements)
            will be password protected.
            When prompted for a password, please enter your client code.
        </strong>
    </p>

    <p>Thank you for choosing Cytonn as your preferred Investment Manager.</p>

    <p>Your sincerely,</p>

    {!! $emailSender !!}

{{--    @include('emails.caml_client_service_signature')--}}

@endsection
