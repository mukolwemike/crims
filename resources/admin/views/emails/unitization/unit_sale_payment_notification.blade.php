@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    <p>
        On behalf of {!! $unitFund->fundmanager->fullname !!}, we would like to inform you that we have processed a Sale
        of
        {!! \Cytonn\Presenters\AmountPresenter::currency($unitSale->number) !!} units in our Unit Trust product
        offering, specifically the {!! $unitFund->name !!}. You can invest and withdraw instantly, 24/7 by simply
        dialing *809#. CMMF is your wallet that you stand to earn upto 11% p.a.
    </p>

    <p>
        In case of any queries please do not hesitate to contact us on <a href="mailto:operations@cytonn.com">operations@cytonn.com</a>.
    </p>
    <p>Your sincerely,</p>

    @include('emails.caml_client_service_signature')
@endsection
