@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},</p>

    <p>
        Cytonn Investment Co-operative takes this opportunity to thank you for investing with us. <br><br>

        Please find attached a Business Confirmation of your membership with Cytonn Investment Co-operative. <br><br>

        Once again, thank you for choosing us as your trusted investment partner.

    <p>Kind regards,</p>
    <p>
        {!! $email_sender !!}
    </p>
@endsection