@extends('emails.email_plain')

@section('content')
    <p>Dear {!! \Cytonn\Presenters\ClientPresenter::presentShortName($client->id) !!},</p>

    <p>This is to remind you of your Coop payment due in {!! Carbon\Carbon::today()->diffForHumans($due_date->copy(), true) !!}.

        Please see the details below:</p>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Product</th>
                <th>Duration</th>
                <th>Due Date</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($product_plans as $plan)
                <tr>
                    <td>
                        @if($plan->product)
                            {!! $plan->product->name !!}
                        @elseif($plan->shares > 0)
                            {!! $plan->entity->name !!} Shares ({!! $plan->shares !!})
                        @endif
                    </td>
                    <td>
                        @if($plan->duration > 0)
                            {!! $t = $plan->duration !!} {!! str_plural('year', $t) !!}
                        @endif
                    </td>
                    <td>{!! $due_date->toFormattedDateString() !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($plan->amount) !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p>Once again, thank you for choosing us as your trusted investment partner.</p>

    <p>Kind regards,</p>
    <p style="text-decoration: underline; font-weight: 700">
        {!! $email_sender !!}
    </p>
@endsection