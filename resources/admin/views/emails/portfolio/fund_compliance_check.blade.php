@extends('emails.email')

@section('content')

    <h3>Fund Compliance Notification</h3>

    <p>The following fund orders does not meet the compliance set.</p>

    @foreach($order_checks as $order_check)
        <table class="table table-responsive">
            <thead>
                <tr>
                    <td>Order Date</td>
                    <td>Type</td>
                    <td>Security</td>
                    <td>Order amount</td>
                    <td>Unit Fund</td>
                    <td>Compliance status</td>
                </tr>
            </thead>
            <tbody>
            @foreach($order_check['checks'] as $check)
                <tr>
                    <td>{!! Carbon\Carbon::parse($order_check['order']->created_at)->toFormattedDateString() !!}</td>
                    <td>{!! $order_check['order']->type->name !!}</td>
                    <td>{!! $order_check['order']->security->name !!}</td>
                    <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($check['total_amount']) !!}</td>
                    <td>{!! $check['fund']->name !!}</td>
                    <td>{!! $check['status']->reason !!}</td>
                </tr>
            @endforeach()
            </tbody>
        </table>

        <br>
        <br>
    @endforeach

    <hr>

    <p>Cytonn Investments Management Limited.</p>

@endsection