@extends('emails.email_plain')

@section('content')
    <p>
        Find attached the Deposit Placement Instructions for {!! $investor->name !!} that you requested.
    </p>

@endsection