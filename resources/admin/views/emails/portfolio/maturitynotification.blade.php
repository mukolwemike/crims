@extends('emails.email')

@section('content')

    <h3>Maturity Notification</h3>
    @if(count((array) $investmentsGroupedByFundManager) == 0)
        <p>
            The are no investments set to mature {!! $period !!}.
        </p>
    @else
        <p>
            The following funds are set to mature {!! $period !!}.
        </p>

        <table class="table table-responsive table-hover table-striped">
            <thead>
            <tr>
                <th>Institution</th><th>Type</th><th>Invested Amount</th><th>Value Date</th><th>Maturity Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($investmentsGroupedByFundManager as $fundName => $investments)
                <tr>
                    <th class="text-center" colspan="100%">{!! $fundName !!}</th>
                </tr>
                @foreach($investments as $investment)
                    <tr>
                        <td>{!! $investment->security->name !!}</td>
                        <td>{!! $investment->security->subAssetClass->name !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
    @endif

    <p>
        <hr/>

        Cytonn Investments Management Limited.
    </p>
@endsection