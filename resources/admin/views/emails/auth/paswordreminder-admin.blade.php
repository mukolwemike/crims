@extends('emails.email')

@section('content')

<div>
    <h3>Password Reset</h3>

    <div>
        A User has attempted to reset their password on <a href="https://www.cytonncrims.com">https://www.cytonncrims.com</a>

        <br/><br/>

        The details given are:<br/>
        Username: {!! $username !!}
        Email: {!! $email !!}
    </div>
</div>
@endsection

