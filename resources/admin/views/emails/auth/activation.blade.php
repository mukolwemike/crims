@extends('emails.client_default')

@section('content')

<div>
    <p>Hello, <br/>

        You have been registered for an account at <a href="https://www.cytonncrims.com">https://www.cytonncrims.com</a>.

        Your account details are:

    <table>
        <tbody>
        <tr><td>Username: </td><td>{!! $username !!}</td></tr>
        <tr><td>Email: </td><td>{!! $email !!}</td></tr>
        </tbody>
    </table>

    <br>

    You will need the username and a password to login.<br/>

    To finally activate your account and create a password, please click the following link.<br/><br/>

    <a href="{!! getenv('DOMAIN').'/account/'.$username.'/activate/'.$activation_key !!}">{!! getenv('DOMAIN').'/account/'.$username.'/activate/'.$activation_key !!}</a>

    <br/>

    If clicking the link doesn't work you can copy the link into your browser window or type it there directly.<br/>

    Regards,<br/>

    Cytonn Investments<br/>
    -----------------------------------------------------------------
    </p>
</div>

@endsection