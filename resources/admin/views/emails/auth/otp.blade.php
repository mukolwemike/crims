@extends('emails.client_default')

@section('content')
    <div>
        <h3>CRIMS Login Token</h3>
        <p>
            Dear {!!$name!!},<br/>

            Please use {!!$otp!!} to login<br/>

            You can also click <a href="{!! getenv('DOMAIN') !!}/login/confirm/{!! $username !!}?one_time_key={!! $otp !!}">here to login.</a>
            <br>

            Regards,<br/>
            Cytonn CRIMS.
        </p>
    </div>
@endsection