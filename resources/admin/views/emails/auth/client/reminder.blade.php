@extends('emails.client_default')

@section('content')
    <h3>Password Reset</h3>

    <div>
        <p>Click <a href="{!! getenv('CLIENT_DOMAIN')!!}/account/{!! $username !!}/reset/{!! $token !!}">here</a> to reset your password. </p>


        If you can't click the above link, copy the link below to your browser.<br/>

        <a href="{!! getenv('CLIENT_DOMAIN')!!}/account/{!! $username !!}/reset/{!! $token !!}">{!!getenv('CLIENT_DOMAIN')!!}/account/{!! $username !!}/reset/{!! $token !!}</a>

    </div>
@endsection
