@extends('emails.email')

@section('content')
		<h3>Password Reset</h3>

		<div>
			<p>Click <a href="{{getenv('DOMAIN')}}/password/reset/{{ $username }}/{{ $token }}">here</a> to reset your password. </p>.<br/>

			This link will expire in {{ \Illuminate\Support\Facades\Config::get('auth.reminder.expire') }} minutes.<br/>


            If you can't click the above link, copy the link below to your browser.<br/>

            <a href="{{getenv('DOMAIN')}}/password/reset/{{ $username }}/{{ $token }}">{{getenv('DOMAIN')}}/password/reset/{{ $username }}/{{ $token }}</a>


		</div>
@endsection
