@extends('emails.email')

@section('content')
    <h4>Risky Client Cleared</h4>

    <p>At {{ \Carbon\Carbon::today()->toFormattedDateString() }}</p>

    <p>The following Risky Client Details were <strong>CLEARED</strong>.</p>

    <p>The client details was marked <strong>{{ \Cytonn\Presenters\ClientPresenter::presentRiskyStatus($riskyClient->risky_status_id) }}</strong> after due diligence.</p>

    <table class="table table-responsive table-striped">
        <thead>
        <tr>
            <th>Name/ Organization</th>
            <th>Email</th>
            <th>Nationality</th>
            <th>Affiliations</th>
            <th>Source</th>
            <th>Date Flagged</th>
            <th>Reason</th>
            <th>Flagged By</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                @if(is_null($riskyClient->firstname))
                    {{$riskyClient->organization}}
                @else
                    {{ $riskyClient->firstname .' '. $riskyClient->lastname }}
                @endif
            </td>
            <td>{{ $riskyClient->email }}</td>
            <td>{{ \Cytonn\Presenters\CountryPresenter::present($riskyClient->country_id) }}</td>
            <td>{{ $riskyClient->affiliations }}</td>
            <td>{{ $riskyClient->risk_source }}</td>
            <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($riskyClient->date_flagged) }}</td>
            <td>{{ $riskyClient->reason }}</td>
            <td>{{ \Cytonn\Presenters\UserPresenter::presentFullNames($riskyClient->flagged_by) }}</td>
        </tr>
        </tbody>
    </table>

    <p>Regards,</p>

    <p class="bold-underline"><strong>Cytonn CRIMS</strong></p>
@endsection