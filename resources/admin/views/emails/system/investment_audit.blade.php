@extends('emails.email_plain')

@section('content')
    <h4>Investment Check Report</h4>
    <p>As at {{ \Carbon\Carbon::today()->toFormattedDateString() }}</p>

    <p>Please see below a summary of the findings:</p>

    <table class="table table-bordered table-responsive table-striped">
        <thead>
            <tr><th>Item</th><th>Potential Issues</th></tr>
        </thead>
        <tbody>
            @foreach($counts as $count)
                <tr><td>{{ $count->name }}</td><td>{{ $count->count }}</td></tr>
            @endforeach
        </tbody>
    </table>

    <p>Please find attached the details of each item in this report</p>

    <p>Regards,</p>
    <p class="bold-underline">Cytonn CRIMS</p>
@endsection