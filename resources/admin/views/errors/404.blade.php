@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div>
            <div class="col-md-6 col-md-offset-3">
                <div class="panel">
                    <div class="panel-heading">
                        <h1 class="text-center">Page not found</h1>
                    </div>
                    <div class="panel-body">
                        <br/><br/>
                        <p class="text-center">
                            The page could not be found.<br/><br/>

                            Error code: 404

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop