@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <div>
            <div class="col-md-6 col-md-offset-3">
                <div class="panel">
                    <div class="panel-heading">
                        <h1 class="text-center">Error</h1>
                    </div>
                    <div class="panel-body">
                        <br/><br/>
                        <p class="text-center">
                            {{--{!! $message !!}--}}
                            An error has occurred.

                            <br/>
                            <br/>

                            Error code: {!! $code !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop