<table>
    <thead>
        <tr>
            <td colspan="8"><h3 style="text-align: center">Inactive clients as at {!! (new Carbon\Carbon($date))->toFormattedDateString() !!}</h3></td>
        </tr>
        <tr>
            <th>Client Code</th>
            <th> Name</th>
            <th> Email</th>
            <th> Phone</th>
            <th>Country</th>
            <th>Last Inactive</th>
        </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
            <tr>
                @if(isset($client['client_code'])) <td> {!! $client['client_code'] !!} </td> @endif
                @if(isset($client['name'])) <td>  {!! $client['name'] !!} </td> @endif
                @if(isset($client['email'])) <td> {!! $client['email'] !!} </td> @endif
                @if(isset($client['phone'])) <td> {!! $client['phone'] !!} </td> @endif
                @if(isset($client['country'])) <td> {!! $client['country'] !!} </td> @endif
                @if(isset($client['last_inactive'])) <td> {!! $client['last_inactive'] !!} </td> @endif
            </tr>
    </tbody>
    @endforeach
</table>