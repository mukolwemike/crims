<table>
    <thead>
    <tr>
        <th>Client Code</th>
        <th>Name</th>
        <th>Amount</th>
        <th>Type</th>
        <th>Inflow</th>
        <th>Interest rate</th>
        <th>Invested Date</th>
        <th>Maturity Date</th>
        <th>Withdrawal Date</th>
        <th>Withdrawn</th>
        <th>Amount Withdrawn</th>
        <th>Top up</th>
        <th>Gross Interest</th>
        <th>Net Interest</th>
        <th>Market Value @ End Date</th>
        <th>Gross Interest For Period</th>
        <th>Net Interest For Period</th>
        <th>Total Interest Paid</th>
        <th>Interest Paid In Period</th>
        <th>FA</th>
        <th>Tenor</th>
        <th>Rate</th>
    </tr>
    </thead>
    <tbody>
    @foreach($investments as $investment)
        <tr>
            <td>{!! $investment->client->client_code !!}</td>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id) !!}</td>
            <td>{!! $investment->amount !!}</td>
            <td>{!! ucfirst($investment->investmentType->name) !!}</td>
            <td>{!! $investment->repo->inflow() !!}</td>
            <td>{!! $investment->interest_rate !!}</td>
            <td>{!! $investment->invested_date !!}</td>
            <td>{!! $investment->maturity_date !!}</td>
            <td>{!! $investment->withdrawal_date !!}</td>
            <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($investment->repo->active($end_date)) !!}</td>
            <td>{{ $investment->repo->getWithdrawnAmount($end_date) }} </td>
            <td>{!! $investment->repo->topupAmount() !!}</td>
            {{--<td>{!! $investment->repo->getGrossInterestForInvestmentAtDate($end_date_next) !!}</td>--}}
            {{--<td>{!! $investment->repo->getNetInterestForInvestmentAtDate($end_date_next) !!}</td>--}}
            {{--<td>{!! $investment->repo->getTotalValueOfInvestmentAtDate($end_date_next) !!}</td>--}}
            {{--<td>{!! $investment->repo->getGrossInterestForInvestmentBetweenDates($start_date_next, $end_date_next) !!}</td>--}}
            {{--<td>{!! $investment->repo->getNetInterestForInvestmentBetweenDates($start_date_next, $end_date_next) !!}</td>--}}
            {{--<td>{!! $investment->repo->getSumInterestPaymentsBeforeDate($end_date_next) !!}</td>--}}
            {{--<td>{!! $investment->repo->getSumInterestPaymentsBetweenDates($start_date_next, $end_date_next) !!}</td>--}}
            <td>{!! @$investment->commission->recipient->name !!}</td>
            <td>{!! $investment->repo->getTenorInMonths() !!}</td>
            <td>{!! @$investment->commission->rate !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>