<table>
    <thead>
    <tr>
        <th>Client Code</th>
        <th>Name</th>
        <th>Amount</th>
        <th>Gross Interest</th>
        <th>Net Interest</th>
        <th>Market Value @ End Date</th>
        <th>Gross Interest For Period</th>
        <th>Net Interest For Period</th>
        <th>Total Interest Paid</th>
        <th>Interest Paid In Period</th>
    </tr>
    </thead>
    <tbody>
    @foreach($investments_grouped_by_client as $client_id => $investments)
        <?php
            $client = \App\Cytonn\Models\Client::findOrFail($client_id);
        ?>
        <tr>
            <td>{!! $client->client_code !!}</td>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
            <td>{!! $investments->sum('amount') !!}</td>
            <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->getGrossInterestForInvestmentAtDate($end_date_next); }) !!}</td>
            <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->getNetInterestForInvestmentAtDate($end_date_next); }) !!}</td>
            <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->getTotalValueOfInvestmentAtDate($end_date_next); }) !!}</td>
            <td>{!! $investments->reduce(function($carry, $investment) use ($start_date_next, $end_date_next) { return $carry + $investment->repo->getGrossInterestForInvestmentBetweenDates($start_date_next, $end_date_next); }) !!}</td>
            <td>{!! $investments->reduce(function($carry, $investment) use ($start_date_next, $end_date_next) { return $carry + $investment->repo->getNetInterestForInvestmentBetweenDates($start_date_next, $end_date_next); }) !!}</td>
            <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->getSumInterestPaymentsBeforeDate($end_date_next); }) !!}</td>
            <td>{!! $investments->reduce(function($carry, $investment) use ($start_date_next, $end_date_next) { return $carry + $investment->repo->getSumInterestPaymentsBetweenDates($start_date_next, $end_date_next); }) !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>