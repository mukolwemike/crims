<table>


    <thead>
        <tr>
            <td colspan="8"><h3 style="text-align: center">Client Summary as at {!! (new Carbon\Carbon($date))->toFormattedDateString() !!}</h3></td>
        </tr>
        <tr>
            <th>Client Code</th>
            <th> Name</th>
            <th> Email</th>
            <th> Phone</th>
            <th>FA</th>
            <th>Principal</th>
            <th>Gross Interest</th>
            <th>Net Interest</th>
            <th> Interest Paid</th>
            <th> Balance</th>
            <th>Tenor</th>
            <th>Client Since</th>
            <th>Latest Maturity Date</th>
        </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
            <tr>
                @if(isset($client['client_code'])) <td> {!! $client['client_code'] !!} </td> @endif
                @if(isset($client['name'])) <td>  {!! $client['name'] !!} </td> @endif
                @if(isset($client['email'])) <td> {!! $client['email'] !!} </td> @endif
                @if(isset($client['phone'])) <td> {!! $client['phone'] !!} </td> @endif
                @if(isset($client['fa'])) <td> {!! $client['fa'] !!} </td> @endif
                @if(isset($client['principal'])) <td>  {!! $client['principal']  !!} </td> @endif
                @if(isset($client['gross_interest'])) <td> {!! $client['gross_interest'] !!} </td> @endif
                @if(isset($client['net_interest'])) <td> {!! $client['net_interest'] !!} </td> @endif
                @if(isset($client['interest_paid'])) <td> {!! $client['interest_paid'] !!} </td> @endif
                @if(isset($client['interest_balance'])) <td> {!! $client['interest_balance'] !!} </td> @endif
                @if(isset($client['tenor'])) <td> {!! $client['tenor'] !!} Months </td> @endif
                @if(isset($client['start_date'])) <td> {!! $client['start_date'] !!} </td> @endif
                @if(isset($client['end_date'])) <td> {!! $client['end_date'] !!} </td> @endif

            </tr>
    </tbody>
    @endforeach
</table>