<table>
    <thead>
        <tr>
            <th colspan="4">{!! $recipient->name !!}</th><th colspan="4">{!! \Carbon\Carbon::today()->toFormattedDateString() !!}</th>
        </tr>
        <tr>
            <th>Client</th><th>Value Date</th><th>Maturity Date</th><th>Principal</th><th>Total Commission</th><th>Tenor (Days)</th>

            @foreach($months as $month)
                <th>{!! $month->format('M, Y') !!}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($investments as $investment)
            <tr>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($investment->client_id) !!}</td>
                <td>{!! $investment->invested_date->toDateString() !!}</td>
                <td>{!! $investment->maturity_date->toDateString() !!}</td>
                <td>{!! $investment->amount !!}</td>
                <td>{!! $total_commission($investment) !!}</td>
                <td>{!! $investment->repo->getTenorInDays() !!}</td>
                @foreach($months as $month)
                    <td>{!! $scheduled($investment, $month) !!}</td>
                @endforeach
            </tr>
        @endforeach

        <tr>
            <th colspan="6">Total</th>
            @foreach($months as $month)
                <th></th>
            @endforeach
        </tr>
    </tbody>
</table>