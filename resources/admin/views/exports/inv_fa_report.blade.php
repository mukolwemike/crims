<table>


    <thead>
        <tr>
            <td colspan="8"><h3 style="text-align: center">FA Report as at {!! (new Carbon\Carbon($date))->toFormattedDateString() !!}</h3></td>
        </tr>
        <tr>
            <th>Client Code</th>
            <th> Name</th>
            <th> Email</th>
            <th> Phone</th>
            <th>FA</th>
            <th>Investment Type</th>
            <th>Product</th>
            <th>Principal</th>
            <th>Interest Rate</th>
            <th>Invested Date</th>
            <th>Maturity Date</th>
        </tr>
    </thead>
    <tbody>
    @foreach($commissions as $commission)
            <tr>
                @if(isset($commission['client_code'])) <td> {!! $commission['client_code'] !!} </td> @endif
                @if(isset($commission['name'])) <td>  {!! $commission['name'] !!} </td> @endif
                @if(isset($commission['email'])) <td> {!! $commission['email'] !!} </td> @endif
                @if(isset($commission['phone'])) <td> {!! $commission['phone'] !!} </td> @endif
                @if(isset($commission['fa'])) <td> {!! $commission['fa'] !!} </td> @endif
                @if(isset($commission['investment_type'])) <td> {!! $commission['investment_type'] !!} </td> @endif
                @if(isset($commission['product'])) <td> {!! $commission['product'] !!} </td> @endif
                @if(isset($commission['principal'])) <td>  {!! $commission['principal']  !!} </td> @endif
                @if(isset($commission['interest_rate'])) <td> {!! $commission['interest_rate'] !!} </td> @endif
                @if(isset($commission['invested_date'])) <td> {!! $commission['invested_date'] !!} </td> @endif
                @if(isset($commission['maturity_date'])) <td> {!! $commission['maturity_date'] !!} </td> @endif
            </tr>
    </tbody>
    @endforeach
</table>