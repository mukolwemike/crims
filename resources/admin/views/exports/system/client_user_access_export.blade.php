<table>
    <tr>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Name</th>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Username</th>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Email</th>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Phone</th>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Status</th>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Type</th>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Joined</th>
        <th style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Clients Access</th>
    </tr>
    @foreach($values as $value)
        <tr>
            <td colspan="4">{{$value['name']}}</td>
            <td colspan="4">{{$value['username']}}</td>
            <td colspan="4">{{$value['email']}}</td>
            <td colspan="4">{{$value['phone']}}</td>
            <td colspan="4">{{$value['status']}}</td>
            <td colspan="4">{{$value['account_type']}}</td>
            <td colspan="4">{{$value['joined']}}</td>
            <td colspan="4">
                @foreach($value['clients'] as $client)
                    <p>{{$client}}</p><br>
                @endforeach
            </td>
        </tr>
    @endforeach

</table>
