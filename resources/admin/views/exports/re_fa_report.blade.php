<table>


    <thead>
        <tr>
            <td colspan="8"><h3 style="text-align: center">FA Report as at {!! (new Carbon\Carbon($date))->toFormattedDateString() !!}</h3></td>
        </tr>
        <tr>
            <th>Client Code</th>
            <th> Name</th>
            <th> Email</th>
            <th> Phone</th>
            <th>FA</th>
            <th>Project</th>
            <th>Unit</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
    @foreach($realestate_commissions as $realestate_commission)
            <tr>
                @if(isset($realestate_commission['client_code'])) <td> {!! $realestate_commission['client_code'] !!} </td> @endif
                @if(isset($realestate_commission['name'])) <td>  {!! $realestate_commission['name'] !!} </td> @endif
                @if(isset($realestate_commission['email'])) <td> {!! $realestate_commission['email'] !!} </td> @endif
                @if(isset($realestate_commission['phone'])) <td> {!! $realestate_commission['phone'] !!} </td> @endif
                @if(isset($realestate_commission['fa'])) <td> {!! $realestate_commission['fa'] !!} </td> @endif
                @if(isset($realestate_commission['project'])) <td> {!! $realestate_commission['project'] !!} </td> @endif
                @if(isset($realestate_commission['unit'])) <td>  {!! $realestate_commission['unit']  !!} </td> @endif
                @if(isset($realestate_commission['amount'])) <td> {!! $realestate_commission['amount'] !!} </td> @endif
            </tr>
    </tbody>
    @endforeach
</table>