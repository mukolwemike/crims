<h2>Commission Schedules</h2>
<table>
    <thead>
    <tr>
        <th>Client Name</th>
        <th>Client Code</th>
        <th>Product</th>
        <th>Principal</th>
        <th>Value Date</th>
        <th>Maturity Date</th>
        <th>Rate</th>
        <th>Description</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($schedules as $cs)
        <tr>
            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($cs->investment->client_id) !!}</td>
            <td>{!! $cs->investment->client->client_code !!}</td>
            <td>{!! $cs->investment->product->name !!}</td>
            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->investment->amount) !!}</td>
            <td>{!! $cs->investment->invested_date->toDateString() !!}</td>
            <td>{!! $cs->investment->maturity_date->toDateString() !!}</td>
            <td>{!! $cs->commission->rate . '%' !!}</td>
            <td>{!! $cs->description !!}</td>
            <td>{!! $cs->amount !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@if($claw_backs->count() > 0)
    <h2>Claw backs</h2>
    <table>
        <thead>
        <tr>
            <th>Date</th><th>Description</th><th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($claw_backs as $cs)
            <tr>
                <td>{!! $cs->date !!}</td>
                <td>{!! $cs->narration !!}</td>
                <td>{!! $cs->amount !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if($override != 0)
    <h2>Old Overrides</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Old Override</th><th>New Override</th><th>Difference</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reports as $report)
            <tr>
                <td>{!! $report->name !!}</td>
                <td>{!! $report->rank->name !!}</td>
                <td>{!! $report->commission !!}</td>
                <td>{!! $report->rate !!}</td>
                <td>{!! $report->override !!}</td>
                <td>{!! $report->newoverride !!}</td>
                <td>{!! $report->difference !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if(isset($new_reports))
    <h2>New Overrides</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Old Override</th><th>New Override</th><th>Difference</th>
        </tr>
        </thead>
        <tbody>
        @foreach($new_reports as $new_report)
            <tr>
                <td>{!! $new_report->name !!}</td>
                <td>{!! $new_report->rank->name !!}</td>
                <td>{!! $new_report->commission !!}</td>
                <td>{!! $new_report->rate !!}</td>
                <td>{!! $new_report->override !!}</td>
                <td>{!! $new_report->newoverride !!}</td>
                <td>{!! $new_report->difference !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if(isset($filterOld))
    <h2>In Old Overrides Not In New</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Old Override</th><th>New Override</th><th>Difference</th>
        </tr>
        </thead>
        <tbody>
        @foreach($filterOld as $new_report)
            <tr>
                <td>{!! $new_report->name !!}</td>
                <td>{!! $new_report->rank->name !!}</td>
                <td>{!! $new_report->commission !!}</td>
                <td>{!! $new_report->rate !!}</td>
                <td>{!! $new_report->override !!}</td>
                <td>{!! $new_report->newoverride !!}</td>
                <td>{!! $new_report->difference !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if(isset($filterOld1))
    <h2>In Old Overrides And In New</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Old Override</th><th>New Override</th><th>Difference</th>
        </tr>
        </thead>
        <tbody>
        @foreach($filterOld1 as $new_report)
            <tr>
                <td>{!! $new_report->name !!}</td>
                <td>{!! $new_report->rank->name !!}</td>
                <td>{!! $new_report->commission !!}</td>
                <td>{!! $new_report->rate !!}</td>
                <td>{!! $new_report->override !!}</td>
                <td>{!! $new_report->newoverride !!}</td>
                <td>{!! $new_report->difference !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if(isset($filterNew))
    <h2>In New Overrides Not In Old</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Old Override</th><th>New Override</th><th>Difference</th>
        </tr>
        </thead>
        <tbody>
        @foreach($filterNew as $new_report)
            <tr>
                <td>{!! $new_report->name !!}</td>
                <td>{!! $new_report->rank->name !!}</td>
                <td>{!! $new_report->commission !!}</td>
                <td>{!! $new_report->rate !!}</td>
                <td>{!! $new_report->override !!}</td>
                <td>{!! $new_report->newoverride !!}</td>
                <td>{!! $new_report->difference !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif