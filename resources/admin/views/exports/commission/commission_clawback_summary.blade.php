<table>
    <?php $investmentCurrencies = $values[0]['data']['investments']['currencies'] ?>
    <tr>
        <td style="background-color: #006666; color: #ffffff; text-align: center" colspan="5"></td>
        <td style="background-color: #006666; color: #ffffff; text-align: center" colspan="{!! (count($investmentCurrencies) + 1) * 4 !!}">CHYS</td>
        <td style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Real Estate</td>
        <td style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">OTC</td>
        <td style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">CFSF</td>
        <td style="background-color: #006666; color: #ffffff; text-align: center" colspan="4">Grand Totals</td>
    </tr>
    <tr>
        <td style="background-color: #006666; color: #ffffff">Name</td>
        <td style="background-color: #006666; color: #ffffff">Rank</td>
        <td style="background-color: #006666; color: #ffffff">Position</td>
        <td style="background-color: #006666; color: #ffffff">Employee Id</td>
        <td style="background-color: #006666; color: #ffffff">Employee Number</td>
        @foreach($investmentCurrencies as $currency)
            <td style="background-color: #006666; color: #ffffff">{!! $currency->code . ' - CHYS Commission' !!}</td>
            <td style="background-color: #006666; color: #ffffff">{!! $currency->code . ' - CHYS Clawback' !!}</td>
            <td style="background-color: #006666; color: #ffffff">{!! $currency->code . ' - CHYS Override' !!}</td>
            <td style="background-color: #006666; color: #ffffff">{!! $currency->code . ' - CHYS Net Total' !!}</td>
        @endforeach
        <td style="background-color: #006666; color: #ffffff">Total CHYS Commission</td>
        <td style="background-color: #006666; color: #ffffff">Total CHYS Clawback</td>
        <td style="background-color: #006666; color: #ffffff">Total CHYS Override</td>
        <td style="background-color: #006666; color: #ffffff">Total CHYS Net Total</td>
        <td style="background-color: #006666; color: #ffffff">Total RE Commission</td>
        <td style="background-color: #006666; color: #ffffff">Total RE Clawback</td>
        <td style="background-color: #006666; color: #ffffff">Total RE Override</td>
        <td style="background-color: #006666; color: #ffffff">Total RE Net Total</td>
        <td style="background-color: #006666; color: #ffffff">Total OTC Commission</td>
        <td style="background-color: #006666; color: #ffffff">Total OTC Clawback</td>
        <td style="background-color: #006666; color: #ffffff">Total OTC Override</td>
        <td style="background-color: #006666; color: #ffffff">Total OTC Net Total</td>
        <td style="background-color: #006666; color: #ffffff">Total CFSF Commission</td>
        <td style="background-color: #006666; color: #ffffff">Total CFSF Clawback</td>
        <td style="background-color: #006666; color: #ffffff">Total CFSF Override</td>
        <td style="background-color: #006666; color: #ffffff">Total CFSF Net Total</td>
        <td style="background-color: #006666; color: #ffffff">Total Commission</td>
        <td style="background-color: #006666; color: #ffffff">Total Clawback</td>
        <td style="background-color: #006666; color: #ffffff">Total Override</td>
        <td style="background-color: #006666; color: #ffffff">Net Commission</td>
    </tr>
    @foreach($values as $commission)
        <tr>
            <td>{!! $commission['fa']['name'] !!}</td>
            <td>{!! $commission['fa']['rank'] !!}</td>
            <td>{!! $commission['fa']['type'] !!}</td>
            <td>{!! $commission['fa']['employee_id'] !!}</td>
            <td>{!! $commission['fa']['employee_number'] !!}</td>
            @foreach($investmentCurrencies as $currency)
                <td>{!! $commission['data']['investments']['data'][$currency->code]['commission'] !!}</td>
                <td>{!! $commission['data']['investments']['data'][$currency->code]['clawback'] !!}</td>
                <td>{!! $commission['data']['investments']['data'][$currency->code]['override'] !!}</td>
                <td>{!! $commission['data']['investments']['data'][$currency->code]['net_total'] !!}</td>
            @endforeach
            <td>{!! $commission['data']['investments']['data']['total']['commission'] !!}</td>
            <td>{!! $commission['data']['investments']['data']['total']['clawback'] !!}</td>
            <td>{!! $commission['data']['investments']['data']['total']['override'] !!}</td>
            <td>{!! $commission['data']['investments']['data']['total']['net_total'] !!}</td>
            <td>{!! $commission['data']['real_estate']['commission'] !!}</td>
            <td>{!! $commission['data']['real_estate']['clawback'] !!}</td>
            <td>{!! $commission['data']['real_estate']['override'] !!}</td>
            <td>{!! $commission['data']['real_estate']['net_total'] !!}</td>
            <td>{!! $commission['data']['shares']['commission'] !!}</td>
            <td>{!! $commission['data']['shares']['clawback'] !!}</td>
            <td>{!! $commission['data']['shares']['override'] !!}</td>
            <td>{!! $commission['data']['shares']['net_total'] !!}</td>
            <td>{!! $commission['data']['unitization']['commission'] !!}</td>
            <td>{!! $commission['data']['unitization']['clawback'] !!}</td>
            <td>{!! $commission['data']['unitization']['override'] !!}</td>
            <td>{!! $commission['data']['unitization']['net_total'] !!}</td>
            <td>{!! $commission['total']['commission'] !!}</td>
            <td>{!! $commission['total']['clawback'] !!}</td>
            <td>{!! $commission['total']['override'] !!}</td>
            <td>{!! $commission['total']['net_total'] !!}</td>
        </tr>
    @endforeach
</table>