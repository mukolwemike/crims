@if(count($shares->schedules) > 0)
    <h2>Share Commission Schedules</h2>
    <table>
        <thead>
        <tr>
            <th>Client Name</th>
            <th>Client Code</th>
            <th>Number</th>
            <th>Price</th>
            <th>Purchase Cost</th>
            <th>Value Date</th>
            <th>Entity</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($shares->schedules as $cs)
            <tr>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($cs->sharePurchase->shareHolder->client_id) !!}</td>
                <td>{!! $cs->sharePurchase->shareHolder->client->client_code !!}</td>
                <td>{!! number_format($cs->sharePurchase->number) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->sharePurchase->purchase_price) !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->sharePurchase->repo->getPurchasePrice()) !!}</td>
                <td>{!! $cs->sharePurchase->date !!}</td>
                <td>{!! $cs->sharePurchase->shareHolder->entity->name !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->amount) !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if(count($shares->overrides) != 0)
    <h2>Overrides</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th>Rank</th>
            <th>Commission</th>
            <th>Rate</th>
            <th>Override</th>
        </tr>
        </thead>
        <tbody>
        @foreach($shares->overrides as $report)
            <tr>
                <td>{!! $report['name'] !!}</td>
                <td>{!! $report['rank'] !!}</td>
                <td>{!! $report['commission'] !!}</td>
                <td>{!! $report['rate'] !!}</td>
                <td>{!! $report['override'] !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
