@if(count($additional->additional) > 0)
    <h2>Advance Commissions</h2>
    <table>
        <thead>
        <tr>
            <th>Date</th>
            <th>Type</th>
            <th>Repayable</th>
            <th>Narration</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($additional->additional as $advanceCommission)
            <tr>
                <td>{!! $advanceCommission->date !!}</td>
                <td>{!! @$advanceCommission->type->name !!}</td>
                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($advanceCommission->repayable) !!}</td>
                <td>{!! $advanceCommission->description !!}</td>
                <td>{!! $advanceCommission->amount !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if(count($additional->unpaid) > 0)
    <h2>Unrecovered Clawbacks</h2>
    <table>
        <thead>
        <tr>
            <th>Date</th>
            <th>Type</th>
            <th>Repayable</th>
            <th>Narration</th>
            <th>Amount</th>
            <th>Unpaid Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($additional->unpaid as $unpaidAdvanceCommission)
            <tr>
                <td>{!! $unpaidAdvanceCommission->date !!}</td>
                <td>{!! $unpaidAdvanceCommission->type->name !!}</td>
                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($unpaidAdvanceCommission->repayable) !!}</td>
                <td>{!! $unpaidAdvanceCommission->description !!}</td>
                <td>{!! $unpaidAdvanceCommission->amount !!}</td>
                <td>{!! $unpaidAdvanceCommission->unpaid_amount !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif