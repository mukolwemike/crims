@if(count($real_estate->schedules) > 0)
    <h2>Real Estate Commission Schedules</h2>
    <table>
        <thead>
        <tr>
            <th>Client Name</th>
            <th>Client Code</th>
            <th>Project</th>
            <th>Unit</th>
            <th>Description</th>
            <th>Reservation Date</th>
            <th>Loo Date</th>
            <th>Sales Ag. Date</th>
            <th>Commission Date</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($real_estate->schedules as $cs)
            <?php $holding = $cs->commission->holding ?>
            <tr>
                <td>{!!  \Cytonn\Presenters\ClientPresenter::presentJointFullNames($holding->client_id) !!}</td>
                <td>{!! $holding->client->client_code !!}</td>
                <td>{!! $holding->project->name !!}</td>
                <td>{!! $holding->unit->number !!}</td>
                <td>{!! $cs->description !!}</td>
                <td>{!! $holding->reservation_date !!}</td>
                <td>{!! $holding->loo->date_received !!}</td>
                <td>{!! $holding->salesAgreement->date_received !!}</td>
                <td>{!! $cs->date !!}</td>
                <td>{!! $cs->amount !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if(count($real_estate->overrides) != 0)
    <h2>Overrides</h2>

    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Override</th>
        </tr>
        </thead>
        <tbody>
        @foreach($real_estate->overrides as $report)
            <tr>
                <td>{!! $report['name'] !!}</td>
                <td>{!! $report['rank'] !!}</td>
                <td>{!! $report['commission'] !!}</td>
                <td>{!! $report['rate'] !!}</td>
                <td>{!! $report['override'] !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
