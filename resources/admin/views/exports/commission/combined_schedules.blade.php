@foreach($investments as $schedule)
    <h2>Investments: {{ $schedule->currency->code }}</h2>
    @include('exports.commission.partials.investment_schedules')
@endforeach
<h2>Shares</h2>
@include('exports.commission.partials.share_schedules')
<h2>Real Estate</h2>
@include('exports.commission.partials.real_estate_schedules')
<h2>Unit Funds</h2>
@include('exports.commission.partials.unitization_schedules')
<h2>Additional Commission</h2>
@include('exports.commission.partials.additional_commission')