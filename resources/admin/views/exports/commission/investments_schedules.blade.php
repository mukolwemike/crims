@if($schedules->count() > 0)
    <h2>Commission Schedules</h2>
    <table>
        <thead>
        <tr>
            <th>Client Name</th>
            <th>Client Code</th>
            <th>Product</th>
            <th>Principal</th>
            <th>Value Date</th>
            <th>Maturity Date</th>
            <th>Rate</th>
            <th>Description</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($schedules as $cs)
            <tr>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($cs->investment->client_id) !!}</td>
                <td>{!! $cs->investment->client->client_code !!}</td>
                <td>{!! $cs->investment->product->name !!}</td>
                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cs->investment->amount) !!}</td>
                <td>{!! $cs->investment->invested_date->toDateString() !!}</td>
                <td>{!! $cs->investment->maturity_date->toDateString() !!}</td>
                <td>{!! $cs->commission->rate . '%' !!}</td>
                <td>{!! $cs->description !!}</td>
                <td>{!! $cs->amount !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if($claw_backs->count() > 0)
    <h2>Claw backs</h2>
    <table>
        <thead>
        <tr>
            <th>Date</th><th>Description</th><th>Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($claw_backs as $cs)
            <tr>
                <td>{!! $cs->date !!}</td>
                <td>{!! $cs->narration !!}</td>
                <td>{!! $cs->amount !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

@if($reports->count() > 0)
    <h2>Overrides</h2>
    <table>
        <thead>
        <tr>
            <th>Name</th><th>Rank</th><th>Commission</th><th>Rate</th><th>Override</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reports as $report)
            <tr>
                <td>{!! $report->name !!}</td>
                <td>{!! $report->rank->name !!}</td>
                <td>{!! $report->commission !!}</td>
                <td>{!! $report->rate !!}</td>
                <td>{!! $report->override !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif