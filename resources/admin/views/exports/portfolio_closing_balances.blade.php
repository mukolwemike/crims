<table>
    <thead>
    <tr>
        <th>Institution</th>
        <th>Principal</th>
        <th>Principal Repaid</th>
        <td>Principal Balance</td>
        <th>Gross Interest</th>
        <th>Net Interest</th>
        <th>Interest Repaid</th>
        <th>Interest Balance</th>
        <th>Adjusted Market Value</th>
    </tr>
    </thead>
    <tbody>
        @foreach($portfolio_investments_grouped_by_investor as $investor_id => $investments)
            <?php $investor = \App\Cytonn\Models\PortfolioInvestor::findOrFail($investor_id) ?>
            <tr>
                <td>{!! \Cytonn\Presenters\InstitutionPresenter::presentName($investor->id) !!}</td>
                <td>{!! $investments->sum('amount') !!}</td>
                <td>{!! $investments->reduce(function($carry, $investment) use ($end_date) { return $carry + $investment->repo->repayments($end_date, 'principal_repayment')->sum('amount'); }) !!}</td>
                <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->principal($end_date_next); }) !!}</td>
                <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->getGrossInterestForInvestment($end_date_next); }) !!}</td>
                <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->getNetInterestForInvestment($end_date_next); }) !!}</td>
                <td>{!! $investments->reduce(function($carry, $investment) use ($end_date) { return $carry + $investment->repo->repayments($end_date, 'interest_repayment')->sum('amount'); }) !!}</td>
                <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->getNetInterestAfterRepayments($end_date_next); }) !!}</td>
                <td>{!! $investments->reduce(function($carry, $investment) use ($end_date_next) { return $carry + $investment->repo->adjustedMarketValue($end_date_next); }) !!}</td>
            </tr>
        @endforeach
    </tbody>
</table>