<table>
    <thead>
        <tr>
            <td colspan="8"><h3 style="text-align: center">Account balance trails for {!! $custodial->account_name !!} account as at {!! (new Carbon\Carbon($date))->toFormattedDateString() !!}</h3></td>
        </tr>
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
    @foreach($trails as $trail)
            <tr>
                @if(isset($trail['id'])) <td>  {!! $trail['id'] !!} </td> @endif
                @if(isset($trail['date'])) <td> {!! $trail['date'] !!} </td> @endif
                @if(isset($trail['amount'])) <td> {!! $trail['amount'] !!} </td> @endif
            </tr>
    </tbody>
    @endforeach
</table>