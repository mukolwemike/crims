<table>
    <thead>
        <tr>
            <th colspan="6">Distribution Structure as at {!! $today->copy()->toFormattedDateString() !!}</th>
        </tr>
        <tr style="background: #006666; color: #fff">
            <th>Name</th>
            <th>E-Mail</th>
            <th>Telephone</th>
            <th>Type</th>
            <th>Rank</th>
            <th>Reports To</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sfas as $sfa)
            <tr>
                <th colspan="6">Senior Financial Adviser</th>
            </tr>
            <tr>
                <td>{!! $sfa->name !!}</td>
                <td>{!! $sfa->email !!}</td>
                <td>{!! $sfa->phone !!}</td>
                <td>{!! $sfa->type->name !!}</td>
                <td>{!! $sfa->rank->name !!}</td>
                <td>{!! $sfa->um !!}</td>
            </tr>
            <tr>
                <th colspan="6">Financial Advisers</th>
            </tr>
            @foreach($sfa->fas as $fa)
                <tr>
                    <td>{!! $fa->name !!}</td>
                    <td>{!! $fa->email !!}</td>
                    <td>{!! $fa->phone !!}</td>
                    <td>{!! $fa->type->name !!}</td>
                    <td>{!! $fa->rank->name !!}</td>
                    <td>{!! $fa->dm !!}</td>
                </tr>
            @endforeach
            <tr></tr>
        @endforeach
    </tbody>
</table>