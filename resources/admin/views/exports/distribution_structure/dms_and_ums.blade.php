<table>
    <thead>
        <tr>
            <th colspan="6">Distribution Structure as at {!! $today->copy()->toFormattedDateString() !!}</th>
        </tr>
        <tr style="background: #006666; color: #fff">
            <th>Name</th>
            <th>E-Mail</th>
            <th>Telephone</th>
            <th>Type</th>
            <th>Rank</th>
            <th>Distribution Manager</th>
        </tr>
    </thead>
    <tbody>
        @foreach($ums_arr as $um)
            <tr>
                <td>{!! $um->name !!}</td>
                <td>{!! $um->email !!}</td>
                <td>{!! $um->phone !!}</td>
                <td>{!! $um->type->name !!}</td>
                <td>{!! $um->rank->name !!}</td>
                <td>{!! $um->dm !!}</td>
            </tr>
        @endforeach
    </tbody>
</table>