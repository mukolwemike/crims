<table>
    <thead>
    <tr>
        <th style="background: #006666; color: #fff">Project</th>
        <th style="background: #006666; color: #fff">Unit Number</th>
        <th style="background: #006666; color: #fff">Topology</th>
        <th style="background: #006666; color: #fff">Tranche</th>
        <th style="background: #006666; color: #fff">Reservation Date</th>
        @foreach($months as $month)
            <th style="background: #006666; color: #fff">{!! $month->format('M Y') !!}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
        @foreach($client->unitHoldings()->inProjectIds($project_ids)->active()->get() as $holding)
            <tr>
                <td>{!! $holding->project->name !!}</td>
                <td>{!! $holding->unit->number !!}</td>
                <td>{!! $holding->unit->size->name !!}</td>
                <td>@if($holding->tranche) {!! $holding->tranche->name !!} @endif</td>
                <td>{!! $holding->reservation_date !!}</td>
                @foreach($months as $mon)
                    <td>{!! $summaryForMonth($holding, $mon)['paid'] !!}</td>
                @endforeach
            </tr>
        @endforeach
    @endforeach
    <tr></tr>
    </tbody>
</table>