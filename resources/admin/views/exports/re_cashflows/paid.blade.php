<table>
    <thead>
    <tr style="background: #006666; color: #fff">
        <th>Client Code</th>
        <th>Name</th>
        <th>FA</th>
        <th>Project</th>
        <th>Unit Number</th>
        <th>Topology</th>
        <th>Payment Plan</th>
        <th>Tranche</th>
        <th>Price</th>
        <th>Paid</th>
        <th>% Paid</th>
        <th>Overdue</th>
        {{--<th>Overdue Date</th>--}}
        <th>Aging</th>
        <th>Sent LOO</th>
        <th>Signed LOO</th>
        <th>Signed S.A.</th>
        @foreach($months as $month)
            <th>{!! $month->format('M Y') !!}</th>
        @endforeach
        <th>Pending</th>
    </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
        @foreach($client->unitHoldings()->inProjectIds($project_ids)->active()->get() as $holding)
            <tr>
                <td>{!! $client->client_code !!}</td>
                <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                <td>{!! @$holding->commission->recipient->name !!}</td>
                <td>{!! $holding->project->name !!}</td>
                <td>{!! $holding->unit->number !!}</td>
                <td>{!! $holding->unit->size->name !!}</td>
                <td>@if($holding->paymentPlan) {!! $holding->paymentPlan->name !!} @endif</td>
                <td>@if($holding->tranche) {!! $holding->tranche->name !!} @endif</td>
                <?php $price = $holding->price(); $paid = $holding->payments()->sum('amount') ?>
                <td>{!! $price !!}</td>
                <td>{!! $paid !!}</td>
                <td>{!! percentage($paid, $price, 2) !!}</td>
                <td>{!! $overdueFunc($holding) !!}</td>
                {{--<td>{!! $overdueDateFunc($holding) !!}</td>--}}
                <td>{!! percentage($paid, $price, 2) !!}</td>
                <td>{!! @$holding->loo->sent_on !!}</td>
                <td>{!! @$holding->loo->date_signed !!}</td>
                <td>{!! @$holding->salesAgreement->date_signed !!}</td>
                @foreach($months as $mon)
                    <?php $payment = $holding->payments()->inMonth($mon)->sum('amount'); ?>
                    <td>@if($payment != 0) {!! $payment !!}@endif</td>
                @endforeach
                <td>{!! $holding->price() - $holding->payments()->sum('amount') !!}</td>
            </tr>
        @endforeach
    @endforeach
    <tr></tr>
    {{--<tr style="font-weight: 800">--}}
        {{--<td></td><td>Paid</td><td><td></td><td></td><td></td><td></td><td></td><td></td>--}}
        {{--@foreach($months as $mon)--}}
            {{--<td>{!! RealEstatePayment::forProjects($project_ids)->inMonth($mon)->sum('amount') !!}</td>--}}
        {{--@endforeach--}}
        {{--<td></td>--}}
    {{--</tr>--}}
    </tbody>
</table>