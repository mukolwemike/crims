<table>
    <thead>
        <tr>
            <th style="background: #006666; color: #fff">Client Code</th>
            <th style="background: #006666; color: #fff">Name</th>
            <th style="background: #006666; color: #fff">FA</th>
            <th style="background: #006666; color: #fff">Project</th>
            <th style="background: #006666; color: #fff">Unit Number</th>
            <th style="background: #006666; color: #fff">Topology</th>
            <th style="background: #006666; color: #fff">Payment Plan</th>
            <th style="background: #006666; color: #fff">Tranche</th>
            <th style="background: #006666; color: #fff">Price</th>
            <th style="background: #006666; color: #fff">Paid</th>
            <th style="background: #006666; color: #fff">Pending</th>
            <th style="background: #006666; color: #fff">Due On {!! $date->toFormattedDatestring() !!}</th>
            @foreach($months as $month)
                <th colspan="2" style="background: #006666; color: #fff">{!! $month->format('M Y') !!}</th>
            @endforeach
        </tr>
        <tr>
            <th style="background: #006666; color: #fff" colspan="12"></th>
            @foreach($months as $month)
                <th style="text-align: center; background: #006666; color: #fff">Scheduled</th>
                <th style="text-align: center; background: #006666; color: #fff">Paid</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($clients as $client)
            @foreach($client->unitHoldings()->inProjectIds($project_ids)->active()->get() as $holding)
                <tr>
                    <td>{!! $client->client_code !!}</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                    <td>{!! @$holding->commission->recipient->name !!}</td>
                    <td>{!! $holding->project->name !!}</td>
                    <td>{!! $holding->unit->number !!}</td>
                    <td>{!! $holding->unit->size->name !!}</td>
                    <td>@if($holding->paymentPlan) {!! $holding->paymentPlan->name !!} @endif</td>
                    <td>@if($holding->tranche) {!! $holding->tranche->name !!} @endif</td>
                    <td>{!! $holding->price() !!}</td>
                    <td>{!! $holding->payments()->remember($cache_for)->sum('amount') !!}</td>
                    <td>{!! $holding->price() - $holding->payments()->remember($cache_for)->sum('amount') !!}</td>
                    <td>{!! $due_today($holding) !!}</td>
                    @foreach($months as $mon)
                        <td>{!! $summaryForMonth($holding, $mon)['scheduled'] !!}</td>
                        <td>{!! $summaryForMonth($holding, $mon)['paid'] !!}</td>
                    @endforeach
                </tr>
            @endforeach
        @endforeach
        <tr></tr>
    </tbody>
</table>