<?php $total = 0;
$prices = 0; ?>
<table>
    <tbody>
        <tr>
            <td>Client</td><td>{!! $client->client_code !!}</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
        </tr>
        <tr>
            <td colspan="3">Real Estate Payment Schedules</td>
        </tr>
        @foreach($client->unitHoldings()->inProjectIds($project_ids)->active()->get() as $holding)

            <?php $unit = $holding->unit; ?>
            <tr style="font-weight: 700"><td colspan="3">{!! $unit->project->name !!}</td></tr>
            <tr style="font-weight: 700"><td colspan="3">Unit {!! $holding->unit->number !!} - {!! $unit->size->name !!} {!! $unit->type->name !!}</td></tr>
            <tr style="font-weight: 700"><td>Tranche</td><td colspan="2">@if($holding->tranche) {!! $holding->tranche->name !!} @endif</td></tr>
            <tr><th>Description</th><th>Date</th><th>Amount</th></tr>
            @foreach($schedules = $holding->paymentSchedules()->inPricing()->get() as $schedule)
                <tr><td>{!! $schedule->description !!}</td><td>{!! $schedule->date !!}</td><td>{!! $schedule->amount !!}</td></tr>
            @endforeach
            <tr><td>Sub Total</td><td></td><td>{!!  $schedules->sum('amount') !!}</td></tr>
            <?php $total += $schedules->sum('amount');
            $prices += $holding->price(); ?>
            <tr><td>Price</td><td></td><td>{!!  $holding->price() !!}</td></tr>
            <tr style="background: #e3e3e3"><td></td><td></td><td></td></tr>
        @endforeach
        <tr style="font-weight: 800"><td>Total Schedules</td><td></td><td>{!! $total !!}</td></tr>
        <tr style="font-weight: 800"><td>Total Prices</td><td></td><td>{!! $prices !!}</td></tr>
    </tbody>
</table>