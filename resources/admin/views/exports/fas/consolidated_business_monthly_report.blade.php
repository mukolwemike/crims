<table>
    <tbody>
    <tr style="background-color: #006666; color: #ffffff">
        <td></td>
        <td></td>
        <td></td>
        @foreach($values['months'] as $month)
            <td colspan="17">
                <p style="text-align: center">{!! $month !!}</p>
            </td>
        @endforeach
    </tr>
    <tr>
        <td style="background-color: #006666; color: #ffffff"><p>Name</p></td>
        <td style="background-color: #006666; color: #ffffff"><p>Status</p></td>
        <td style="background-color: #006666; color: #ffffff"><p>Email</p></td>
        @foreach($values['months'] as $month)
            <td style="background-color: #006666; color: #ffffff"><p>New Money</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Top Ups</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Top Ups On Rollover</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Rollover</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Partial Withdrawals</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Reinvested Interest</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Rollover Interest</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Withdrawals</p></td>
{{--            <td style="background-color: #006666; color: #ffffff"><p>YTD Withdrawals</p></td>--}}
            <td style="background-color: #006666; color: #ffffff"><p>Reinvested Withdrawals</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Withdrawals to Client</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Specific Withdrawals for Period</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Net Inflows</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>RE Actual</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>RE Value</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Unit Fund Purchases</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Unit Fund Sales</p></td>
            <td style="background-color: #006666; color: #ffffff"><p>Specific Unit Fund Sales For Period</p></td>
        @endforeach
    </tr>
    <tr>
        @foreach($values['data'] as $branchName => $branchData)
            <tr style="background-color: #95B3D7; font-weight: 700">
                <td>{!! $branchName !!}</td>
            </tr>
            @foreach($branchData as $unitName => $commissionRecipients)
                @if($unitName != 'Branch Total')
                    <tr style="background-color: #95B3D7; font-weight: 700">
                        <td>{!! $unitName !!}</td>
                    </tr>
                @endif
                @foreach($commissionRecipients as $userName => $userData)
                    @if($unitName == 'Branch Total' || $userName == 'Unit Total')
                        <tr style="background-color: #FFFF00; color: #0a0a0a; font-weight: 700">
                    @else
                        <tr>
                    @endif
                        <td>{!! explode(':', $userName)[0] !!}</td>
                        @if($unitName == 'Branch Total' || $userName == 'Unit Total')
                            <td></td>
                            <td></td>
                        @else
                            <td>{!! $userData[$values['months'][0]]['active']  !!}</td>
                            <td>{!! $userData[$values['months'][0]]['email'] !!}</td>
                        @endif
                        @foreach($userData as $month =>  $user)
                            <td>{!! $user['new_investment_amount'] !!}</td>
                            <td>{!! $user['top_ups_amount'] !!}</td>
                            <td>{!! $user['topups_on_rollover'] !!}</td>
                            <td>{!! $user['rollover_amounts'] !!}</td>
                            <td>{!! $user['partial_withdrawals'] !!}</td>
                            <td>{!! $user['reinvested_interest'] !!}</td>
                            <td>{!! $user['rollover_interests'] !!}</td>
                            <td>{!! $user['withdrawals'] !!}</td>
{{--                            <td>{!! $user['ytd_withdrawals'] !!}</td>--}}
                            <td>{!! $user['re_invested_withdrawals'] !!}</td>
                            <td>{!! $user['withdrawals_sent_to_client'] !!}</td>
                            <td>{!! $user['period_withdrawals'] !!}</td>
                            <td>{!! $user['net_flows_amount'] !!}</td>
                            <td>{!! $user['real_estate_actual_amount'] !!}</td>
                            <td>{!! $user['real_estate_value'] !!}</td>
                            <td>{!! $user['unit_fund_purchases'] !!}</td>
                            <td>{!! $user['unit_fund_sales'] !!}</td>
                            <td>{!! $user['period_unit_fund_sales'] !!}</td>
                        @endforeach
                    </tr>
                @endforeach
                    <tr></tr>
            @endforeach
            <tr></tr>
            <tr></tr>
        @endforeach
    </tbody>
</table>