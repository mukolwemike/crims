<table>
    <tr>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Branch</th>
        @foreach( array_keys((array) $values->fas) as $branchName)
            <th style = "text-align: center; background-color: #006666; color: #ffffff">{!! $branchName !!}</th>
        @endforeach
    </tr>
    @foreach($values as $key => $branchDetails)
        <tr>
            <td>{!! $key !!}</td>
            @foreach(array_keys((array) $values->fas) as $branchName)
                <td>{!! $values->{$key}->{$branchName} !!}</td>
            @endforeach
        </tr>
    @endforeach
</table>