<table>
    <tbody>
    <tr style = "background-color: #006666; color: #ffffff">
        <td></td>
        <td></td>
        @foreach($values['months'] as $month)
            <td colspan = "3">
                <p style = "text-align: center">{!! $month !!}</p>
            </td>
        @endforeach
    </tr>
    <tr>
        <td style = "background-color: #006666; color: #ffffff"><p>Name</p></td>
        <td style = "background-color: #006666; color: #ffffff"><p>Email</p></td>
        @foreach($values['months'] as $month)
            <td style = "background-color: #006666; color: #ffffff"><p>Units Sold</p></td>
            <td style = "background-color: #006666; color: #ffffff"><p>RE Actual</p></td>
            <td style = "background-color: #006666; color: #ffffff"><p>RE Value</p></td>
        @endforeach
    </tr>
    <tr>
    @foreach($values['data'] as $branchName => $branchData)
        <tr style = "background-color: #95B3D7; font-weight: 700">
            <td>{!! $branchName !!}</td>
        </tr>
        @foreach($branchData as $unitName => $commissionRecipients)
            @if($unitName != 'Branch Total')
                <tr style = "background-color: #95B3D7; font-weight: 700">
                    <td>{!! $unitName !!}</td>
                </tr>
            @endif
            @foreach($commissionRecipients as $userName => $userData)
                @if($unitName == 'Branch Total' || $userName == 'Unit Total')
                    <tr style = "background-color: #FFFF00; color: #0a0a0a; font-weight: 700">
                @else
                    <tr>
                        @endif
                        <td>{!! $userName !!}</td>
                        @if($unitName == 'Branch Total' || $userName == 'Unit Total')
                            <td></td>
                        @else
                            <td>{!! $userData[$values['months'][0]]['email'] !!}</td>
                        @endif
                        @foreach($userData as $month =>  $user)
                            <td>{!! $user['units_sold'] !!}</td>
                            <td>{!! $user['real_estate_actual_amount'] !!}</td>
                            <td>{!! $user['real_estate_value'] !!}</td>
                        @endforeach
                    </tr>
            @endforeach
            <tr></tr>
        @endforeach
        <tr></tr>
        <tr></tr>
    @endforeach
    </tbody>
</table>