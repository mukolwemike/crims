<table>
    <tr>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Unit Name</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Unit Manager</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Unit Shadow</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Branch</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Target No. of FAs</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of FAs</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Unit Variance</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of FA1s</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of Experienced FAs</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of Retained FAs</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of Non-Retained FAs</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of Suspended Terminated FAs</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of FAs With Production</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">No. of FAs Without Production</th>
    </tr>
    @foreach($values as $unit)
        <tr style = "
        @if($unit->fas < 5) background-color: #FF0000; color: #ffffff;
        @elseif($unit->fas > 8) background-color: #006666; color: #ffffff;
        @elseif($unit->fas > 5 && $unit->fas < 8)  background-color: #FFFF00; color: #000000
        @endif " >
            <td>{!! $unit->unit_name !!}</td>
            <td>{!! $unit->unit_manager !!}</td>
            <td>{!! $unit->unit_shadow !!}</td>
            <td>{!! $unit->branch !!}</td>
            <td style="text-align: right">{!! $unit->target_number_of_fas !!}</td>
            <td style="text-align: right">{!! $unit->fas !!}</td>
            <td style="text-align: right">{!! $unit->unit_variance !!}</td>
            <td style="text-align: right">{!! $unit->fa1s !!}</td>
            <td style="text-align: right">{!! $unit->experienced_fas !!}</td>
            <td style="text-align: right">{!! $unit->retained_fas !!}</td>
            <td style="text-align: right">{!! $unit->non_retained_fas !!}</td>
            <td style="text-align: right">{!! $unit->suspend_fas !!}</td>
            <td style="text-align: right">{!! $unit->with_production !!}</td>
            <td style="text-align: right">{!! $unit->without_production !!}</td>
        </tr>
    @endforeach
</table>