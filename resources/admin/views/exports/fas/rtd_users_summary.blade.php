<table>
    <tr>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Name</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Email</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">FA Position</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">FA Unit</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Days Worked</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Total Interactions</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Contacts From Start</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Contacts For Period</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Contact Target</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">% Contacts Achieved</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Ytd Retention</th>
        <th style="text-align: center; background-color: #006666; color: #ffffff">Cumulative Retention</th>
    </tr>
    @foreach($values as $user)
        <tr>
            <td>{!! $user->fa_name !!}</td>
            <td>{!! $user->email !!}</td>
            <td>{!! $user->fa_position !!}</td>
            <td>{!! $user->fa_unit !!}</td>
            <td style="text-align: right">{!! $user->days_worked !!}</td>
            <td style="text-align: right">{!! $user->interactions !!}</td>
            <td style="text-align: right">{!! $user->contacts_from_start !!}</td>
            <td style="text-align: right">{!! $user->contacts_for_period !!}</td>
            <td style="text-align: right">{!! $user->contact_target !!}</td>
            <td style="text-align: right">{!! $user->percentage_contacts_achieved !!}</td>
            <td style="text-align: right">{!! $user->ytd_retention !!}</td>
            <td style="text-align: right">{!! $user->cumulative_retention !!}</td>
        </tr>
    @endforeach
</table>