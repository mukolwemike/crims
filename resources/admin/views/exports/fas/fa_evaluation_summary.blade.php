<table>
    <tr style="background-color: #006666; color: #ffffff">
        <th style="text-align: center">Name</th>
        <th style="text-align: center">Title</th>
        <th style="text-align: center">Days</th>
        <th style="text-align: center">Value Days</th>
        <th style="text-align: center">Suspended Termination Days</th>
        <th style="text-align: center">Days in Current FA Position</th>
        <th style="text-align: center">Position Status</th>
        <th style="text-align: center">Employment Status</th>
        <th style="text-align: center">Days In Current Position</th>
        <th style="text-align: center">YTD Inflows</th>
        <th style="text-align: center">Cumulative Inflows</th>
        <th style="text-align: center">YTD Withdrawals</th>
        <th style="text-align: center">Cumulative Withdrawals</th>
        <th style="text-align: center">YTD Netflow</th>
        <th style="text-align: center">Cumulative Netflow</th>
        <th style="text-align: center">YTD RE Sales</th>
        <th style="text-align: center">YTD Cash Plan RE Sales</th>
        <th style="text-align: center">YTD RE Actual Cash Received</th>
        <th style="text-align: center">Cumulative RE Sales</th>
        <th style="text-align: center">Cumulative Cash Plan RE Sales</th>
        <th style="text-align: center">Cumulative Cash Flows</th>
        <th style="text-align: center">YTD Retention</th>
        <th style="text-align: center">Cumulative Retention</th>
        <th style="text-align: center">Production Before Demotion</th>
        <th style="text-align: center">YTD Targets</th>
        <th style="text-align: center">Cumulative Targets</th>
        <th style="text-align: center">YTD Percentage Retention</th>
        <th style="text-align: center">Cumulative Percentage Retention</th>
        <th style="text-align: center">YTD Percentage</th>
        <th style="text-align: center">Cumulative Percentage</th>
    </tr>
    @foreach($values as $branchKey => $branchData)
        <tr>
            <td style="background-color: #95B3D7; font-weight: 700">
                <p>{!! $branchKey !!}</p>
            </td>
        </tr>
        @foreach($branchData as $unitKey => $unitData)
            <tr>
                <td style="background-color: #95B3D7; font-weight: 700">
                    <p>{!! $unitKey !!}</p>
                </td>
            </tr>
            @foreach($unitData as $key => $data)
                @if($key >= count($unitData) - 3)
                    <tr style="background-color: #FFFF00; font-weight: 700">
                        <td>{!! $data->name !!}</td>
                        <td>{!! $data->fa_title !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->days !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->value_days !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->suspended_days !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->currentFaPositionDays !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->position_status !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->employmentStatus !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->currentPositionDays !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->investments->inflow !!} </td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->investments->cumulativeInflows !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->investments->outflow !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->investments->cumulativeOutflows !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->investments->netflow !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->investments->cumulativeNetflow !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->real_estate->YTDRealEstateSales !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->real_estate->YTDRealEstateCashPlanSales !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->real_estate->YTDActualPayments !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->real_estate->cumulativeRealEstateSales !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->real_estate->cumulativeRealEstateCashPlanSales !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->real_estate->cumulativeCashFlows !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->retention->YTDRetention !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->retention->cumulativeRetention !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->retention->productionBeforeDemotion !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->retention->YTDTargets !!}</td>
                        <td style="text-align: right; background-color: #FFFF00; font-weight: 700">{!! $data->retention->cumulativeTargets !!}</td>
                        @if($data->retention->YTDPercentageRetention == '')
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @elseif($data->retention->YTDPercentageRetention <= 49.9)
                            <td style="text-align: right; background-color: #FF0000; font-weight: 700">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @elseif($data->retention->YTDPercentageRetention > 49.9 && $data->retention->YTDPercentageRetention <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9; font-weight: 700">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @elseif($data->retention->YTDPercentageRetention > 74.9 && $data->retention->YTDPercentageRetention <= 99.9)
                            <td style="text-align: right; background-color: #FFC000; font-weight: 700">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @elseif($data->retention->YTDPercentageRetention > 99.9)
                            <td style="text-align: right; background-color: #92D050; font-weight: 700">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @else
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @endif
                        @if($data->retention->cumulativePercentageRetention == '')
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @elseif($data->retention->cumulativePercentageRetention <= 49.9)
                            <td style="text-align: right; background-color: #FF0000; font-weight: 700">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @elseif($data->retention->cumulativePercentageRetention > 49.9 && $data->retention->cumulativePercentageRetention <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9; font-weight: 700">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @elseif($data->retention->cumulativePercentageRetention > 74.9 && $data->retention->cumulativePercentageRetention <= 99.9)
                            <td style="text-align: right; background-color: #FFC000; font-weight: 700">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @elseif($data->retention->cumulativePercentageRetention > 99.9)
                            <td style="text-align: right; background-color: #92D050; font-weight: 700">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @else
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @endif
                        @if($data->retention->YTDPercentage == '')
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->YTDPercentage !!}</td>
                        @elseif($data->retention->YTDPercentage <= 49.9)
                            <td style="text-align: right; background-color: #FF0000; font-weight: 700">{!! $data->retention->YTDPercentage !!}</td>
                        @elseif($data->retention->YTDPercentage > 49.9 && $data->retention->YTDPercentage <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9; font-weight: 700">{!! $data->retention->YTDPercentage !!}</td>
                        @elseif($data->retention->YTDPercentage > 74.9 && $data->retention->YTDPercentage <= 99.9)
                            <td style="text-align: right; background-color: #FFC000; font-weight: 700">{!! $data->retention->YTDPercentage !!}</td>
                        @elseif($data->retention->YTDPercentage > 99.9)
                            <td style="text-align: right; background-color: #92D050; font-weight: 700">{!! $data->retention->YTDPercentage !!}</td>
                        @else
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->YTDPercentage !!}</td>
                        @endif
                        @if($data->retention->cumulativePercentage == '')
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->cumulativePercentage !!}</td>
                        @elseif($data->retention->cumulativePercentage <= 49.9)
                            <td style="text-align: right; background-color: #FF0000; font-weight: 700">{!! $data->retention->cumulativePercentage !!}</td>
                        @elseif($data->retention->cumulativePercentage > 49.9 && $data->retention->cumulativePercentage <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9; font-weight: 700">{!! $data->retention->cumulativePercentage !!}</td>
                        @elseif($data->retention->cumulativePercentage > 74.9 && $data->retention->cumulativePercentage <= 99.9)
                            <td style="text-align: right; background-color: #FFC000; font-weight: 700">{!! $data->retention->cumulativePercentage !!}</td>
                        @elseif($data->retention->cumulativePercentage > 99.9)
                            <td style="text-align: right; background-color: #92D050; font-weight: 700">{!! $data->retention->cumulativePercentage !!}</td>
                        @else
                            <td style="text-align: right; font-weight: 700">{!! $data->retention->cumulativePercentage !!}</td>
                        @endif
                    </tr>
                @else
                    <tr>
                        <td>{!! $data->name !!}</td>
                        <td>{!! $data->fa_title !!}</td>
                        <td style="text-align: right">{!! $data->days !!}</td>
                        <td style="text-align: right">{!! $data->value_days !!}</td>
                        <td style="text-align: right">{!! $data->suspended_days !!}</td>
                        <td style="text-align: right">{!! $data->currentFaPositionDays !!}</td>
                        <td style="text-align: right">{!! $data->position_status !!}</td>
                        <td style="text-align: right">{!! $data->employmentStatus!!}</td>
                        <td style="text-align: right">{!! $data->currentPositionDays !!}</td>
                        <td style="text-align: right">{!! $data->investments->inflow !!} </td>
                        <td style="text-align: right">{!! $data->investments->cumulativeInflows !!}</td>
                        <td style="text-align: right">{!! $data->investments->outflow !!}</td>
                        <td style="text-align: right">{!! $data->investments->cumulativeOutflows !!}</td>
                        <td style="text-align: right">{!! $data->investments->netflow !!}</td>
                        <td style="text-align: right">{!! $data->investments->cumulativeNetflow !!}</td>
                        <td style="text-align: right">{!! $data->real_estate->YTDRealEstateSales !!}</td>
                        <td style="text-align: right">{!! $data->real_estate->YTDRealEstateCashPlanSales !!}</td>
                        <td style="text-align: right">{!! $data->real_estate->YTDActualPayments !!}</td>
                        <td style="text-align: right">{!! $data->real_estate->cumulativeRealEstateSales !!}</td>
                        <td style="text-align: right">{!! $data->real_estate->cumulativeRealEstateCashPlanSales !!}</td>
                        <td style="text-align: right">{!! $data->real_estate->cumulativeCashFlows !!}</td>
                        <td style="text-align: right">{!! $data->retention->YTDRetention !!}</td>
                        <td style="text-align: right">{!! $data->retention->cumulativeRetention !!}</td>
                        <td style="text-align: right">{!! $data->retention->productionBeforeDemotion !!}</td>
                        <td style="text-align: right">{!! $data->retention->YTDTargets !!}</td>
                        <td style="text-align: right">{!! $data->retention->cumulativeTargets !!}</td>
                        @if($data->retention->YTDPercentageRetention <= 49.9)
                            <td style="text-align: right; background-color: #FF0000">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @elseif($data->retention->YTDPercentageRetention > 49.9 && $data->retention->YTDPercentageRetention <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @elseif($data->retention->YTDPercentageRetention > 74.9 && $data->retention->YTDPercentageRetention <= 99.9)
                            <td style="text-align: right; background-color: #FFC000">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @elseif($data->retention->YTDPercentageRetention > 99.9)
                            <td style="text-align: right; background-color: #92D050">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @else
                            <td style="text-align: right">{!! $data->retention->YTDPercentageRetention !!}</td>
                        @endif
                        @if($data->retention->cumulativePercentageRetention <= 49.9)
                            <td style="text-align: right; background-color: #FF0000">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @elseif($data->retention->cumulativePercentageRetention > 49.9 && $data->retention->cumulativePercentageRetention <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @elseif($data->retention->cumulativePercentageRetention > 74.9 && $data->retention->cumulativePercentageRetention <= 99.9)
                            <td style="text-align: right; background-color: #FFC000">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @elseif($data->retention->cumulativePercentageRetention > 99.9)
                            <td style="text-align: right; background-color: #92D050">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @else
                            <td style="text-align: right">{!! $data->retention->cumulativePercentageRetention !!}</td>
                        @endif
                        @if($data->retention->YTDPercentage <= 49.9)
                            <td style="text-align: right; background-color: #FF0000">{!! $data->retention->YTDPercentage !!}</td>
                        @elseif($data->retention->YTDPercentage > 49.9 && $data->retention->YTDPercentage <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9">{!! $data->retention->YTDPercentage !!}</td>
                        @elseif($data->retention->YTDPercentage > 74.9 && $data->retention->YTDPercentage <= 99.9)
                            <td style="text-align: right; background-color: #FFC000">{!! $data->retention->YTDPercentage !!}</td>
                        @elseif($data->retention->YTDPercentage > 99.9)
                            <td style="text-align: right; background-color: #92D050">{!! $data->retention->YTDPercentage !!}</td>
                        @else
                            <td style="text-align: right">{!! $data->retention->YTDPercentage !!}</td>
                        @endif
                        @if($data->retention->cumulativePercentage <= 49.9)
                            <td style="text-align: right; background-color: #FF0000">{!! $data->retention->cumulativePercentage !!}</td>
                        @elseif($data->retention->cumulativePercentage > 49.9 && $data->retention->cumulativePercentage <= 74.9)
                            <td style="text-align: right; background-color: #D9D9D9">{!! $data->retention->cumulativePercentage !!}</td>
                        @elseif($data->retention->cumulativePercentage > 74.9 && $data->retention->cumulativePercentage <= 99.9)
                            <td style="text-align: right; background-color: #FFC000">{!! $data->retention->cumulativePercentage !!}</td>
                        @elseif($data->retention->cumulativePercentage > 99.9)
                            <td style="text-align: right; background-color: #92D050">{!! $data->retention->cumulativePercentage !!}</td>
                        @else
                            <td style="text-align: right">{!! $data->retention->cumulativePercentage !!}</td>
                        @endif
                    </tr>
                @endif
            @endforeach
            <tr>
                <td></td>
            </tr>
        @endforeach
        <tr>
            <td></td>
        </tr>
    @endforeach
</table>