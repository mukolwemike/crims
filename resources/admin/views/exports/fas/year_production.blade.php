<table>
    <tbody>
    <tr style="background-color: #006666; color: #ffffff">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        @foreach($values[0]['months'] as $key => $value)
            <td colspan="10">
                <p style="text-align: center">{!! $key !!}</p>
            </td>
        @endforeach
    </tr>
    <tr>
        <td style="background-color: #006666; color: #ffffff">
            <p>Name</p>
        </td>
        <td style="background-color: #006666; color: #ffffff">
            <p>Email</p>
        </td>
        <td style="background-color: #006666; color: #ffffff">
            <p>Reports To</p>
        </td>
        <td style="background-color: #006666; color: #ffffff">
            <p>Rank</p>
        </td>
        <td style="background-color: #006666; color: #ffffff">
            <p>Type</p>
        </td>
        @foreach($values[0]['months'] as $key => $value)
            <td style="background-color: #006666; color: #ffffff">
                <p>New Money</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>TopUps</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>Rollovers</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>Partial Withdrawal</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>Reinvested Interest</p>
            </td>
            {{--<td style="background-color: #006666; color: #ffffff">--}}
                {{--<p>Rollover Inflow</p>--}}
            {{--</td>--}}
            {{--<td style="background-color: #006666; color: #ffffff">--}}
                {{--<p>Topup Inflow</p>--}}
            {{--</td>--}}
            <td style="background-color: #006666; color: #ffffff">
                <p>Withdrawals</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>Reinvested Withdrawals</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>Withdrawals Sent To Client</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>Specific Withdrawals For Period</p>
            </td>
            <td style="background-color: #006666; color: #ffffff">
                <p>Real Estate Actuals</p>
            </td>
        @endforeach
    </tr>
    @foreach($values as $value)
        <tr>
            <td>{!! $value['name'] !!}</td>
            <td>{!! $value['email'] !!}</td>
            <td>{!! $value['reports_to'] !!}</td>
            <td>{!! $value['rank'] !!}</td>
            <td>{!! $value['type'] !!}</td>

            @foreach($value['months'] as $key => $data)
                <td>{!! $data['new_money'] !!}</td>
                <td>{!! $data['topups'] !!}</td>
                <td>{!! $data['rollovers'] !!}</td>
                <td>{!! $data['partial_withdrawals'] !!}</td>
                <td>{!! $data['reinvested_interest'] !!}</td>
{{--                <td>{!! $data['rollover_inflow'] !!}</td>--}}
{{--                <td>{!! $data['topup_inflow'] !!}</td>--}}
                <td>{!! $data['withdrawals'] !!}</td>
                <td>{!! $data['reinvested_withdrawals'] !!}</td>
                <td>{!! $data['sent_to_client_withdrawals'] !!}</td>
                <td>{!! $data['period_withdrawals'] !!}</td>
                <td>{!! $data['real_estate_actuals'] !!}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>