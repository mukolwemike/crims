<table>
    <thead>
    <tr>
        <th>Institution</th>
        <td>Type</td>
        <th>Principal</th>
        <th>Principal Repaid</th>
        <td>Principal Balance</td>
        <th>Interest rate</th>
        <th>Invested Date</th>
        <th>Maturity Date</th>
        <th>Withdrawal Date</th>
        <th>Withdrawn</th>
        <th>Gross Interest</th>
        <th>Net Interest</th>
        <th>Interest Repaid</th>
        <th>Interest Balance</th>
        <th>Adjusted Market Value</th>
    </tr>
    </thead>
    <tbody>
        @foreach($investments as $investment)
            <tr>
                <td>{!! \Cytonn\Presenters\InstitutionPresenter::presentName($investment->portfolio_investor_id) !!}</td>
                <td>{!! $investment->fund_type_id !!}</td>
                <td>{!! $investment->amount !!}</td>
                <td>{!! $investment->repo->repayments($date, 'principal_repayment')->sum('amount') !!}</td>
                <td>{!! $investment->repo->principal($end_date_next) !!}</td>
                <td>{!! $investment->interest_rate !!}</td>
                <td>{!! $investment->invested_date !!}</td>
                <td>{!! $investment->maturity_date !!}</td>
                <td>{!! $investment->withdrawal_date !!}</td>
                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($investment->withdrawn && $investment->withdrawal_date <= $date) !!}</td>
                <td>{!! $investment->repo->getGrossInterestForInvestment($end_date_next) !!}</td>
                <td>{!! $investment->repo->getNetInterestForInvestment($end_date_next) !!}</td>
                <td>{!! $investment->repo->repayments($date, 'interest_repayment')->sum('amount') !!}</td>
                <td>{!! $investment->repo->getNetInterestAfterRepayments($end_date_next) !!}</td>
                <td>{!! $investment->repo->adjustedMarketValue($end_date_next) !!}</td>
            </tr>
        @endforeach
    </tbody>
</table>