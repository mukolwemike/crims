<div class="detail-group">
    @if($client !== null)
        @if(isset($title))
            <h4>{!! $title !!}</h4>
        @endif
        <table class="table table-hover">
            <tbody>
            <tr>
                <td>Client Code</td>
                <td> {!! $client->client_code !!} </td>
            </tr>
            @if($client->clientType->name == 'individual')
                <tr>
                    <td>Name</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                </tr>
                <tr>
                    <td>Email Address</td>
                    <td>{!! $client->contact->email !!}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>
                        @if($client->contact->phone)
                            {!!  $client->contact->phone !!}
                        @elseif($client->telephone_home)
                            {!! $client->telephone_home !!}
                        @else
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>{!! Cytonn\Presenters\GenderPresenter::presentAbbr($client->contact->gender_id) !!}</td>
                </tr>
                <tr>
                    <td>Date of Birth</td>
                    <td>{!! Cytonn\Presenters\DatePresenter::formatDate($client->dob) !!}</td>
                </tr>
                <tr>
                    <td>Pin Number</td>
                    <td>{!! $client->pin_no !!}</td>
                </tr>
                <tr>
                    <td>ID/ Passport</td>
                    <td>{!! $client->id_or_passport !!}</td>
                </tr>

                <tr>
                    <td>Franchise</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($client->franchise) !!}</td>
                </tr>
            @else
                <tr>
                    <td>Type of Investor</td>
                    <td>
                        @if($client->corporate_investor_type)
                            {!! App\Cytonn\Models\CorporateInvestorType::find($client->corporate_investor_type)->name !!}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Name</td>
                    {{--<td>{!! $client->contact->registered_name !!}</td>--}}
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                </tr>
                <tr>
                    <td>Trade Name</td>
                    <td>{!! $client->contact->corporate_trade_name !!}</td>
                </tr>
                <tr>
                    <td>Registered Address</td>
                    <td>{!! $client->contact->registered_address !!}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    @if($client->contact->phone)
                        <td>{!!  $client->contact->phone !!}</td>
                    @elseif($client->telephone_office)
                        <td>{!! $client->telephone_office !!}</td>
                    @else
                    @endif
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{!! $client->contact->email !!}</td>
                </tr>
                <tr>
                    <td>Pin Number</td>
                    <td>{!! $client->pin_no !!}</td>
                </tr>

                <tr>
                    <td>Franchise</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($client->franchise) !!}</td>
                </tr>
            @endif

            <?php $bank = $client->bankDetails() ?>

            <tr>
                <td>Bank Name</td>
                <td>{!! $bank->bankName !!}</td>
            </tr>
            <tr>
                <td>Bank Branch</td>
                <td>{!! $bank->branch !!}</td>
            </tr>
            <tr>
                <td>Account Name</td>
                <td>{!! $bank->accountName !!}</td>
            </tr>
            <tr>
                <td>Account Number</td>
                <td>{!! $bank->accountNumber !!}</td>
            </tr>
            <tr>
                <td>Email Indemnity</td>
                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($client->emailIndemnity->count()) !!}</td>
            </tr>
            @if($client->signing_mandate)
                <tr>
                    <td>Signing Mandate</td>
                    <td>{!! $client->present()->getSigningMandate !!}</td>
                </tr>
            @endif
            </tbody>
        </table>
    @endif
</div>