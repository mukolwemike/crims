{{--Bank Accounts --}}

<!-- Add bank account details form -->
<div class="modal fade" id="bank-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['bank_details_add_path', $client->id], 'id'=>'bank_details_form']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Bank Account</h4>
            </div>
            <div class="modal-body">
                @include('investment.partials.bankdetailsform')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="bank-default-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['bank_set_default', $client->id]]) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Set Default Bank Account</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::select('bank_account_id', $clientAccounts, null,  ['class'=>'form-control', 'placeholder'=> 'Select Bank Account']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Set', ['class'=>'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



@foreach($client->bankAccounts as $acc)
    <?php
        $branch = \App\Cytonn\Models\ClientBankBranch::find($acc->branch_id);
    ?>
    <!-- Edit bank account details form -->
    <div class="modal fade" id="bank-account-modal-{!! $acc->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::open(['route'=>['bank_details_add_path', $client->id]]) !!}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Bank Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {!! Form::hidden('id', $acc->id) !!}
                        @include('investment.partials.bankdetailsform', ['account'=>$acc, 'branch'=>$branch])
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <!-- Delete bank account details -->
    <div class="modal fade" id="bank-account-delete-modal-{!! $acc->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Bank Details</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this bank account?
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td>Account Name</td>
                                <td>{{ $acc->account_name }}</td>
                            </tr>
                            <tr>
                                <td>Account Number</td>
                                <td>{{ $acc->account_number }}</td>
                            </tr>
                            <tr>
                                <td>Bank</td>
                                <td>
                                    @if(@$branch->bank)
                                        {{ $branch->bank->name }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Branch</td>
                                <td>{{ @$branch->name }}</td>
                            </tr>
                        </tbody>
                    </table>

                    {!! Form::open(['route'=>['bank_details_delete_path', $client->id, $acc->id]]) !!}
                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endforeach


<!-- Add/Edit Email -->
<div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['add_client_emails_path', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add an email</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('emails', 'Enter emails separated by commas') !!}

                    {!! Form::textarea('emails', implode(', ', $client->emails), ['class'=>'form-control', 'rows'=>3]) !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Add Contact person modal -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['add_client_contact_person_path', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a contact person</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}

                    {!! Form::text('name', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}

                    {!! Form::text('email', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('address', 'Address') !!}

                    {!! Form::text('address', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Phone Number') !!}

                    {!! Form::text('phone', null, ['class'=>'form-control']) !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



@foreach($client->contactPersons as $contact)
<!-- Edit Contact person modal -->
<div class="modal fade" id="contact-modal-{!! $contact->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['add_client_contact_person_path', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a contact person</h4>
            </div>
            <div class="modal-ody">
                {!! Form::hidden('id', $contact->id) !!}
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}

                        {!! Form::text('name', $contact->name, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'name') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}

                        {!! Form::text('email', $contact->email, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('address', 'Address') !!}

                        {!! Form::text('address', $contact->address, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'address') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Phone Number') !!}

                        {!! Form::text('phone', $contact->phone, ['class'=>'form-control']) !!}

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endforeach

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['files'=>true, 'route'=>['save_indemnity_email', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add indemnified email</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('date', 'Date') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'dmc-datepicker'=>'yyyy-MM-dd', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>'date']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}

                    {!! Form::text('email', null, ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('file', 'File') !!}

                    {!! Form::file('file', ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--Signing Mandate--}}
<div class="modal fade" id="signingMandate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::model($client, ['route'=>['clients.update_signing_mandate', $client->id]]) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Signing Mandate</h4>
            </div>
            <div class="modal-body">
                <div class = "row">
                    <div class = "col-md-12">
                        <div class = "detail-group">
                            <div class="form-group">
                                {!! Form::hidden('client_id', $client->id) !!}
                                <div class="col-md-4">{!! Form::label('signing_mandate', 'Select Signing Mandate') !!}</div>
                                <div class="col-md-8">{!! Form::select('signing_mandate', $signingMandates , $client->signing_mandate, ['class'=>'form-control']) !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--Client Signature Modal--}}
<div class="modal fade" id="signatureUploadModal" tabindex="-1" role="dialog" aria-labelledby="signatureUploadModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['files'=>true, 'route'=>['clients.upload_client_signature', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload client signature</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden('client_id', $client->id) !!}
                    {!! Form::label('client', 'Select signature person') !!}
                    @if($client->joint && $client->jointHolders->count() == 0)
                        {!! Form::select('joint_client_holder_id', $signatureHolders , '', ['class'=>'form-control']) !!}
                    @endif
                </div>

                <div class="form-group">
                    {!! Form::label('file', 'File') !!}
                    {!! Form::file('file', ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--Guards--}}
<div class="modal fade" id="addClientGuardModal" tabindex="-1" role="dialog" aria-labelledby="addClientGuardModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" ng-controller="addClientGuardController">
            {!! Form::open(['route'=>['clients.store_guards', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Client Guard</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden('client_id', $client->id) !!}
                    {!! Form::label('guard_type_id', 'Select type of guard') !!}
                    {!! Form::select('guard_type_id', $guardTypes , '', ['class'=>'form-control', 'required', 'ng-model' => 'guardType']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'guard_type_id') !!}
                </div>
                <div class="form-group" ng-if="guardType == 2">
                    {!! Form::label('minimum_balance', 'Minimum client balance') !!}
                    {!! Form::number('minimum_balance', null, ['class'=>'form-control', 'step'=>'0.01']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'minimum_balance') !!}
                </div>
                <div class="form-group" ng-if="guardType == 3">
                    {!! Form::label('portfolio_investor_id', 'Select portfolio investor') !!}
                    {!! Form::select('portfolio_investor_id', $portfolioInvestors , '', ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'portfolio_investor_id') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('reason', 'Reason') !!}
                    {!! Form::text('reason', null, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--Edit Client Guards--}}
@foreach($client->guards as $guard)
    <div class="modal fade" id="editClientGuardModal-{!! $guard->id !!}" tabindex="-1" role="dialog" aria-labelledby="editClientGuardModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" ng-controller="addClientGuardController">
                {!! Form::open(['route'=>['clients.store_guards', $client->id, $guard->id]]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Client Guard</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::hidden('client_id', $client->id) !!}
                        {!! Form::label('guard_type_id', 'Select type of guard') !!}
                        {!! Form::select('guard_type_id', $guardTypes , $guard->guard_type_id, ['class'=>'form-control', 'required', 'ng-model' => 'guardType', 'ng-init' => "guardType='$guard->guard_type_id'"]) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'guard_type_id') !!}
                    </div>
                    <div class="form-group" ng-if="guardType == 2">
                        {!! Form::label('minimum_balance', 'Minimum client balance') !!}
                        {!! Form::number('minimum_balance', $guard->minimum_balance, ['class'=>'form-control', 'step'=>'0.01']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'minimum_balance') !!}
                    </div>
                    <div class="form-group" ng-if="guardType == 3">
                        {!! Form::label('portfolio_investor_id', 'Select portfolio investor') !!}
                        {!! Form::select('portfolio_investor_id', $portfolioInvestors , $guard->portfolio_investor_id, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'portfolio_investor_id') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('reason', 'Reason') !!}
                        {!! Form::text('reason', $guard->reason, ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteClientGuardModal-{!! $guard->id !!}" tabindex="-1" role="dialog" aria-labelledby="deleteClientGuardModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" ng-controller="addClientGuardController">
                {!! Form::open(['route'=>['clients.delete_guards', $client->id, $guard->id]]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Client Guard</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this client gaurd?
                    <div class="form-group">
                        {!! Form::hidden('client_id', $client->id) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Delete', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endforeach
@foreach($client->clientSignatures as $clientSignature)
    <div class="modal fade" id="deactivate-signature-{!! $clientSignature->id !!}" tabindex="-1" role="dialog" aria-labelledby="deactivate-signature">
        <div class="modal-dialog" role="document">
            <div class="modal-content" ng-controller="addClientGuardController">
                {!! Form::open(['route'=>['deactivate_client_signature', $clientSignature->id ]]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Activate/Deactivate Client Signature</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to @if($clientSignature->active) deactivate @else activate @endif this client signature?
                    <div class="form-group">
                        {!! Form::hidden('client_signature_id', $clientSignature->id) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Yes', ['class'=>'btn btn-success']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endforeach

@foreach($client->clientSignatures as $clientSignature)
    <div class="modal fade" id="edit-signature-{!! $clientSignature->id !!}" tabindex="-1" role="dialog" aria-labelledby="edit-signature">
        <div class="modal-dialog" role="document">
            <div class="modal-content" ng-controller="addClientGuardController">
                <signing-mandate-edit :signature="{{ $clientSignature }}"></signing-mandate-edit>
            </div>
        </div>
    </div>
@endforeach

<div class="modal fade" id="activateCombinedInterestReinvestment" tabindex="-1" role="dialog"
     aria-labelledby="activateCombinedInterestReinvestment">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['activate_interest_reinvested_plan', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Activate/Deactivate combined interest re-investment plan</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden('client_id', $client->id) !!}
                </div>
                @if(!$client->combine_interest_reinvestment)
                    <div class="form-group">
                        {!! Form::label('combine_interest_reinvestment', 'Select interest combine type') !!}
                        {!! Form::select('combine_interest_reinvestment', ['1' => 'Combine All Reinvested Interest', '2' => 'Combined Reinvested Interest Monthly'] , '1', ['class'=>'form-control', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'combine_interest_reinvestment') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('combine_interest_reinvestment_tenor', 'Re-Investment Tenor (in months)') !!}
                        {!! Form::number('combine_interest_reinvestment_tenor', null, ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'combine_interest_reinvestment_tenor') !!}
                    </div>
                @else
                    <div class="form-group">
                        {!! Form::hidden('combine_interest_reinvestment', '0') !!}
                        {!! Form::hidden('combine_interest_reinvestment_tenor', '0') !!}
                    </div>
                    <p class="danger">Are you sure you want deactivate the combined interest reinvestment plan?</p>
                @endif
            </div>
            <div class="modal-footer">
                {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="editTenor" tabindex="-1" role="dialog" aria-labelledby="editTenor">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['activate_interest_reinvested_plan', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit combined interest re-investment plan</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::hidden('client_id', $client->id) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('combine_interest_reinvestment', 'Select interest combine type') !!}
                    {!! Form::select('combine_interest_reinvestment', ['1' => 'Combine All Reinvested Interest', '2' => 'Combined Reinvested Interest Monthly'] , $client->combine_interest_reinvestment, ['class'=>'form-control', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'combine_interest_reinvestment') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('combine_interest_reinvestment_tenor', 'Re-Investment Tenor (in months)') !!}
                    {!! Form::number('combine_interest_reinvestment_tenor', $client->combine_interest_reinvestment_tenor, ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'combine_interest_reinvestment_tenor') !!}
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Edit', ['class'=>'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

{{--Delete Joint Holder Modal--}}
@if(($client->jointHolders)->count() > 0)
    @foreach($client->jointHolders as $holder)
        <div class="modal fade" id="delete-jointHolder-{!! $holder->details->id !!}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route'=>['delete_joint_holder',  $holder->details->id]]) !!}
                    <div class="modal-header">
                        <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title text-center" id="myModalLabel">Are you sure you want to delete this joint
                            holder?</h4>
                    </div>
                    <div class="modal-body row">
                        <div class="modal-body">
                            <p><b>Name</b>
                                : {!! ucfirst($holder->details->firstname) !!} {!! ucfirst($holder->details->lastname) !!}
                                &nbsp;</p>
                            <p><b>Email</b>:&nbsp;{!! $holder->details->email !!}</p>
                            <p><b>Phone</b>:&nbsp;{!! $holder->details->telephone_cell !!}</p>
                            <p><b>PIN</b>:&nbsp;{!! $holder->details->pin_no !!}</p>
                            <br>
                            <input type="hidden" name="id" value="{{ $holder->details->id }}">

                            <div class="form-group">
                                {!! Form::label('reason', 'Reason for deleting joint holder or enter (DELETE)') !!}
                                {!! Form::text('reason', null,['class'=>'form-control', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default">Cancel</a>
                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endforeach
@endif
{{--end of Delete Joint Holder Modal--}}

<!-- Consent Save Form Modal -->
<div class="modal fade" id="consentForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['files'=>true, 'route'=>['save_consent_form', $client->id]]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Personal Data Consent Form</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('date', 'Date (required)') !!}

                    {!! Form::text('date', null, ['class'=>'form-control', 'dmc-datepicker'=>'yyyy-MM-dd', 'datepicker-popup'=>'yyyy-MM-dd', 'init-model'=>'date']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                </div>

                <input type="hidden" name="email" value="{{$client->contact->email}}">

                <div class="form-group">
                    {!! Form::label('consent', 'Consented (required)') !!}

                    {!! Form::checkbox('consent', '1', true, ['class'=>'form-control'] ); !!}

                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'consent') !!}
                </div>

                <div class="form-group">
                    {!! Form::label('file', 'File (optional)') !!}

                    {!! Form::file('file', ['class'=>'form-control']) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'file') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Consent Delete Form Modal -->
@if(($client->consentForm)->count() > 0)
    @foreach($client->consentForm as $consentForm)
        <div class="modal fade" id="delConsentForm-{{$consentForm->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route'=>['delete_consent_form', $consentForm->id]]) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remove Personal Data Consent Form</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {!! Form::label('reason', 'Reason (required)') !!}

                            {!! Form::text('reason', null, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                        </div>
                        <input type="hidden" name="client_id" value="{{$client->id}}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Remove', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endforeach
@endif

{{--Delete Tax Exemption Modal--}}
@if(($client->taxExemptions)->count() > 0)
    @foreach($client->taxExemptions as $exemption)
        <div class="modal fade" id="delTaxExemption-{!! $exemption->id !!}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route'=>['delete_tax_exemption',  $exemption->id]]) !!}
                    <div class="modal-header">
                        <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title text-center" id="myModalLabel">Are you sure you want to delete the client
                            Tax
                            Exemption?</h4>
                    </div>
                    <div class="modal-body row">
                        <div class="modal-body">
                            <p><b>Name</b>
                                : {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}
                                &nbsp;</p>
                            <p><b>Start
                                    Date</b>:&nbsp;{!! \Cytonn\Presenters\DatePresenter::formatDate($exemption->start) !!}
                            </p>
                            <p><b>End
                                    Date</b>:&nbsp;{!! \Cytonn\Presenters\DatePresenter::formatDate($exemption->end) !!}
                            </p>
                            <p>
                                <b>Amount</b>:&nbsp;{!! \Cytonn\Presenters\AmountPresenter::currency($exemption->amount) !!}
                            </p>
                            <br>
                            <input type="hidden" name="id" value="{{ $client->id }}">

                            <div class="form-group">
                                {!! Form::label('reason', 'Reason for deleting tax exemption or enter (DELETE)') !!}
                                {!! Form::text('reason', null,['class'=>'form-control', 'required']) !!}
                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" class="btn btn-default">Cancel</a>
                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endforeach
@endif
{{--end of Delete Tax Exemption Modal--}}

{{--Add Tax Exemption Modal--}}
<div class="modal fade" id="taxExemption" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <taxable-modal :client="{{ json_encode($client) }}" :edit="false"></taxable-modal>
        </div>
    </div>
</div>
{{--end Add Tax Exemption Modal--}}

{{--Edit Tax Exemption Modal --}}
@if(($client->taxExemptions)->count() > 0)
    @foreach($client->taxExemptions as $taxExemption)
        <div class="modal fade" id="editTaxExemption-{!! $taxExemption->id !!}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <taxable-modal :client='{{ json_encode($client) }}' :tax='{{ json_encode($taxExemption) }}'
                                   :edit="true"></taxable-modal>
                </div>
            </div>
        </div>
    @endforeach
@endif
{{--end Edit Tax Exemption Modal --}}




