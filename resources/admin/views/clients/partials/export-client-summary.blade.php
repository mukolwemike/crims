<div class="modal fade" id="client-summary-export-{!! $product->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route'=>['export_client_summry_report', $product->id], 'method'=>'POST']) !!}
            <div class="modal-header">
                <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">Client Summary Export</h4>
            </div>
            <div class="modal-body row">
                <div class="form-group">
                    <div class="col-md-12">
                        <p>Choose the date</p>
                    </div>
                    <div class="col-md-12"  ng-controller="DatepickerCtrl">
                        {!! Form::label('end_date', 'End Date') !!}
                        {!! Form::text('end_date', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"end_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Export', ['class'=>'btn btn-success']) !!}
                <a data-dismiss="modal" class="btn btn-default">Close</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>