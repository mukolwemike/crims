<div ng-controller="ClientsGridController">
    <table  st-pipe="callServer" st-table="displayed" class="table table-striped table-hover table-responsive">
        <thead>
        <tr>
            <th colspan="5">
                {{--<a href="/dashboard/clients/bank-instructions" class="btn btn-success margin-bottom-20">Bank Instructions</a>--}}
            </th>
            <th colspan="1">{!! Form::select('commplete', [null=>'All clients', 0=>'Incomplete', 1=>'Complete'], null, ['class'=>'form-control margin-bottom-20', 'st-search'=>'complete']) !!}</th>
            <th colspan="1">
                {!! Form::select('investment_type', [ null => 'All Investments', 0 => 'Investments', 1 => 'RealEstate', 2 => 'Unitization', '3' => 'Shares'], null, ['class'=>'form-control margin-bottom-20', 'st-search'=>'investment_type']) !!}
            </th>
            <th colspan="2"><input st-search="" class="form-control" placeholder="Search..." type="text"/></th>
        </tr>
        <tr>
            <th st-sort="id">ID</th>
            <th st-sort="client_code">Client Code</th>
            <th>Name</th>
            <th>Email Address</th>
            <th>Phone Number</th>
            <th>Client Type</th>
            <th>Investment</th>
            <th>User Account</th>
            <th>Details</th>
        </tr>
        </thead>

        <tbody  ng-show="!isLoading">
            <tr ng-repeat="row in displayed">
                <td><% row.id %></td>
                <td><% row.client_code %></td>
                <td><% row.fullName %></td>
                <td><% row.email %></td>
                <td><% row.phone %></td>
                <td><% row.type %></td>
                <td><span to-html="row.investment_status | activeStatus"></span></td>
                <td><span to-html="row.account_status | activeStatus"></span></td>
                <td>
                    <a ng-controller="PopoverCtrl" uib-popover="Edit client details" popover-trigger="mouseenter" href="/dashboard/clients/create/<%row.id%>"><i class="fa fa-edit"></i></a>
                    <a ng-controller="PopoverCtrl" uib-popover="View clients details" popover-trigger="mouseenter" href="/dashboard/clients/details/<%row.id%>"><i class="fa fa-list-alt"></i></a>
                    <a ng-controller="PopoverCtrl" uib-popover="View clients summary" popover-trigger="mouseenter" href="/dashboard/investments/summary/client/<%row.id%>"><i class="fa fa-bar-chart"></i></a>
                </td>
            </tr>
        </tbody>
        <tbody ng-show="isLoading">
            <tr>
                <td colspan="8" class="text-center">Loading ... </td>
            </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan = "4" class = "text-center">
                Items per page <input type = "text" ng-model = "itemsByPage"/>
            </td>
            <td colspan = "4" class = "text-center">
                <div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>
            </td>
        </tr>
        </tfoot>
    </table>

</div>
