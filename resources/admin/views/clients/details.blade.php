<?php
is_array($client->emails) ?: $client->emails = [];
?>

@extends('layouts.default')

@section('content')
    <div class="col-md-10 col-md-offset-1" ng-controller="ClientApplicationDetailCtrl">
        <div class="col-md-12 panel-dashboard">

            {{--<div class="btn-group">--}}
            {{--<a type="button" class="btn btn-primary" href="/dashboard/coop/clients/payments/{!! $client->id !!}">View Client Coop Transactions</a>--}}
            {{--<a type="button" class="btn btn-default" href="/dashboard/coop/payments">View All Coop Transactions</a>--}}
            {{--</div>--}}
            <div class="panel panel-default">
                <div class="panel-heading"><span>Client Details</span></div>
                <div class="panel-body">

                    <p class="col-md-4">Client Code: {!! $client->client_code !!}</p>
                    <p class="col-md-4">Client Type: {!! $client->clientType->name !!} </p>
                    <p class="col-md-4">
                        Franchise: {!! \Cytonn\Presenters\BooleanPresenter::presentIcon($client->franchise)  !!}</p>
                    <p class="col-md-4"></p>
                    <div class="clearfix"></div>

                    @if($client->clientType->name == 'individual')
                        <p class="col-md-4">
                            Name: {!! $client->contact->firstname.' '.$client->contact->middlename.' '.$client->contact->lastname!!}</p>
                        <p class="col-md-4">Preferred
                            Name: {!! \Cytonn\Presenters\ClientPresenter::presentPreferredName($client->id) !!} </p>
                        <p class="col-md-4">Email: {!! $client->contact->email !!} </p>
                        <p class="col-md-4">
                            Contact Phone: {!! $client->contact->phone !!}
                        </p>
                        <p class="col-md-4">
                            Client Phone: {!! $client->phone !!}
                        </p>
                        <p class="col-md-4">
                            Telephone Office: {!! $client->telephone_office !!}
                        </p>
                        <p class="col-md-4">
                            Telephone Home: {!! $client->telephone_home !!}
                        </p>
                        <p class="col-md-4">
                            Gender: {!!Cytonn\Presenters\GenderPresenter::presentAbbr($client->contact->gender_id) !!}</p>

                        <p class="col-md-4">Date of
                            Birth: {!!Cytonn\Presenters\DatePresenter::formatDate($client->dob) !!}</p>

                        <p class="col-md-4">Pin No.: {!! $client->pin_no !!}</p>
                        <p class="col-md-4">ID/Passport Number: {!! $client->id_or_passport !!}</p>
                        <p class="col-md-4">Postal code: {!! $client->postal_code !!}</p>
                        <p class="col-md-4">Postal address: {!! $client->postal_address !!}</p>
                        <p class="col-md-4">
                            Country: {!! Cytonn\Presenters\CountryPresenter::present($client->country_id) !!}</p>
                        <p class="col-md-4">
                            Nationality: {!! Cytonn\Presenters\CountryPresenter::present($client->nationality_id) !!}</p>
                        <p class="col-md-4">Home Telephone no.: {!! $client->telephone_home!!} </p>
                        <p class="col-md-4">Office Telephone no.: {!! $client->telephone_office !!}</p>
                        <p class="col-md-4">Cell no.: {!! $client->telephone_cell !!}</p>
                        <p class="col-md-4">Residential Address: {!! $client->residential_address !!}</p>
                        <p class="col-md-4">Town {!! $client->town !!}</p>
                        <p class="col-md-4">Account Name or Tag: {!! $client->account_name !!}</p>

                        <br/>

                        <h5 class="col-md-12">Employment Details</h5>

                        <p class="col-md-4">Employee No: {!! $client->employee_number !!} </p>
                        <p class="col-md-4">Employment: {!! $client->employment ? $client->employment->name: '' !!} </p>
                        <p class="col-md-4">Details: {!! $client->employment_other !!}</p>
                        <p class="col-md-4">Sector: {!! $client->business_sector !!}</p>
                        <p class="col-md-4">Present Occupation: {!!$client->present_occupation!!} </p>
                        <p class="col-md-4">Employer Name: {!! $client->employer_name !!}</p>
                        <p class="col-md-4">Employer Address: {!! $client->employer_address !!}</p>
                        <p class="col-md-4">Preferred method of
                            Contact: {!! Cytonn\Presenters\ContactMethodPresenter::present($client->method_of_contact_id) !!}</p>
                    @else

                        <h5 class="col-md-12">Type of Investor</h5>
                        @if($client->corporate_investor_type)
                            <p class="col-md-4">Type of
                                Investor: {!! App\Cytonn\Models\CorporateInvestorType::find($client->corporate_investor_type)->name !!}</p>
                        @endif
                        @if($client->corporate_investor_type_other)
                            <p class="col-md-4">Details: {!! $client->corporate_investor_type_other !!}</p>
                        @endif
                        <br/>
                        <h5 class="col-md-12">Investor Details</h5>

                        <p class="col-md-4">Registered name: {!! $client->contact->corporate_registered_name !!}</p>
                        <p class="col-md-4">Trade name: {!! $client->contact->corporate_trade_name !!}</p>
                        <p class="col-md-4">Registered address: {!! $client->contact->registered_address !!}</p>
                        <p class="col-md-4">Registration number: {!! $client->contact->registration_number !!}</p>
                        <p class="col-md-4">
                            Telephone number:
                        @if($client->contact->phone)
                            {!!  $client->contact->phone !!}
                        @elseif($client->telephone_office)
                            {!! $client->telephone_office !!}
                        @else
                        @endif
                        <p class="col-md-4">Email: {!! $client->contact->email !!}</p>
                        <p class="col-md-4">Company pin: {!! $client->pin_no !!}</p>
                        <p class="col-md-4">Physical Address: </p>
                        <p class="col-md-4">Preferred method of
                            contact: {!! Cytonn\Presenters\ContactMethodPresenter::present($client->method_of_contact_id) !!} </p>
                        <p class="col-md-4"></p>

                    @endif

                    <div colspan="4" class="col-md-12">
                        <div style="display: flex;align-items: center;">
                            <div style="flex: 1;">
                                <a href="/dashboard/clients/create/{!! $client->id !!}" class="btn btn-success"><i
                                            class="fa fa-edit"></i> Edit Client</a>
                            </div>
                            <div class="end">
                                @if(! $client->repo->hasProducts())
                                    {!! Form::open(['route'=> ['delete_unused_client_account', $client->id], 'method'=>'DELETE']) !!}
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i>
                                            Delete Client
                                        </button>
                                    </div>
                                    {!! Form::close() !!}
                                @else
                                    <button type="button" class="btn btn-danger" disabled><i class="fa fa-trash-o"></i>
                                        Delete Client
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($iprs_validations->count() > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>IPRS Validations</span>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>ID Number</th>
                                    <th>Validated</th>
                                    <th>First name</th>
                                    <th>Other Names</th>
                                    <th>Surname</th>
                                    <th>D.O.B</th>
                                    <th>Location</th>
                                    <th>Photo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($iprs_validations as $validation)
                                    <tr>
                                        <td>{{ $validation->id_number }}</td>
                                        <td>Yes</td>
                                        <td>{{ $validation->firstname }}</td>
                                        <td>{{ $validation->othername }}</td>
                                        <td>{{ $validation->surname }}</td>
                                        <td>{{ $validation->dob }}</td>
                                        <td>{{ $validation->location }}</td>
                                        <td><img alt="" src="/dashboard/clients/iprs-file/{{ $validation->photo_filename ? $validation->photo_filename : 'avatar.jpg' }}/view" style="max-height: 120px;"/></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Bank information</span>
                </div>
                <div class="panel-body">
                    @if($client->bankAccounts()->count() > 0)
                        <table class="table table-responsive table-striped">
                            <thead>
                            <tr>
                                <th>Account Name</th>
                                <th>Account Number</th>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Currency</th>
                                <th>Clearing Code</th>
                                <th>Swift Code</th>
                                <th>Status</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($client->bankAccounts as $acc)
                                <?php
                                $branch = \App\Cytonn\Models\ClientBankBranch::find($acc->branch_id);
                                ?>
                                <tr>
                                    <td>
                                        {!! $acc->account_name !!}
                                        @if($acc->default)
                                            <el-tag size="small">Default</el-tag>
                                        @endif
                                    </td>
                                    <td>{!! $acc->account_number !!}</td>
                                    @if($branch)
                                        <td>{!! $branch->bank->name !!}</td>
                                        <td>{!! $branch->name !!}</td>
                                        <td>{!! $acc->currency ? $acc->currency->code : '-' !!}</td>
                                        <td>{!! $branch->bank->clearing_code !!}</td>
                                        <td>{!! $branch->bank->swift_code !!}</td>
                                    @else
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    @endif
                                    <td>
                                        @if($acc->active === 0)
                                            <el-tag type="danger" size="small">in-active</el-tag>
                                        @else
                                            <el-tag type="success" size="small">active</el-tag>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                                    data-target="#bank-account-modal-{!! $acc->id !!}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal"
                                                    data-target="#bank-account-delete-modal-{!! $acc->id !!}">
                                                <i class="fa fa-close"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>No alternative bank information available</p>
                @endif

                <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#bank-account-modal">
                        Add a bank account
                    </button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#bank-default-modal">
                        Set Default Account
                    </button>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Payments Information</span>
                </div>
                <div class="panel-body">
                    <a href="/dashboard/investments/client-payments/client/{!! $client->id !!}" class="btn btn-primary">View
                        Transactions</a>
                </div>
            </div>

            @module('coop')
            <div class="panel panel-default">
                <div class="panel-heading"><span>Actions</span></div>
                <div class="panel-body">
                    <div class="col-md-12">

                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" ng-controller="PopoverCtrl"
                                    popover-trigger="mouseenter" data-toggle="modal" data-target="#investInProduct">
                                Invest in Product
                            </button>
                            <button type="button" class="btn btn-danger" ng-controller="PopoverCtrl"
                                    popover-trigger="mouseenter" data-toggle="modal" data-target="#topUp">Top Up
                                Investment
                            </button>
                            <button type="button" class="btn btn-default" ng-controller="PopoverCtrl"
                                    popover-trigger="mouseenter" data-toggle="modal" data-target="#buyShares">Buy Shares
                            </button>
                        </div>


                        <!-- Invest in Product -->
                        <div class="modal fade" id="investInProduct" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::open(['route'=>'invest_in_coop_product']) !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Invest in Product</h4>
                                    </div>
                                    <div class="modal-body" data-ng-controller="coopInvestController">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            {!! Form::hidden('client_id', $client->id) !!}
                                                            {!! Form::hidden('investment', 'investment') !!}

                                                            {!! Form::label('product_plan_id', 'Select product plan') !!}

                                                            {!! Form::select('product_plan_id', $coop_product_plans->lists('full_name', 'id'), null, ['class'=>'form-control', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('deduct', 'Payment Type') !!}

                                                            {!! Form::select('deduct',['yes'=>'Deduct from balance', 'no'=>'Add new payment'], null, ['class'=>'form-control', 'step'=>'1000', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'deduct') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" ng-controller="DatepickerCtrl">
                                                            {!! Form::label('invested_date', 'Invested Date') !!}

                                                            {!! Form::text('invested_date', NULL, ['id'=>'invested_date','class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" ng-controller="DatepickerCtrl">
                                                            {!! Form::label('maturity_date', 'Maturity Date') !!}

                                                            {!! Form::text('maturity_date', NULL, ['id'=>'maturity_date','class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('amount', 'Amount') !!}

                                                            {!! Form::text('amount', NULL, ['class'=>'form-control', 'step'=>'1000', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}

                                                            {!! Form::number('interest_rate', NULL, ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('narrative', 'Narrative') !!}

                                                            {!! Form::textarea('narrative', NULL, ['class'=>'form-control', 'rows'=>'3', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="">
                                                    <h4>Commission</h4>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                {!! Form::label('commission_recipient', 'Commission paid to') !!}

                                                                {!! Form::select('commission_recipient', $commissionrecepients , null,  ['id'=>'commission_recepient','class'=>'form-control', 'init-model'=>'commission_recepient', 'required']) !!}
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <i class="fa fa-spinner fa-spin"
                                                               ng-show="loading === true"></i>
                                                            <div class="form-group"
                                                                 ng-show="zero_commission_rate === '0'">
                                                                {!! Form::label('commission_rate', 'Commission rate') !!}
                                                                <select id="commission_rate"
                                                                        ng-model="commission_rate"
                                                                        ng-options="commissionRate.name for commissionRate in rates"
                                                                        class="form-control"
                                                                        required
                                                                        ng-change="getCommissionRate()"
                                                                        ng-if="zero_commission_rate === '0'">
                                                                </select>
                                                                {!! Form::hidden('commission_rate',null, ['id' => 'commission_rate_value', 'data-ng-value' => 'checkCommissionRateValue()']) !!}
                                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                                                            </div>

                                                            <div class="form-group"
                                                                 ng-show="zero_commission_rate === '1'">
                                                                <p class="margin-top-20">This FA has Zero Commission
                                                                    Rate</p>
                                                                {!! Form::hidden('commission_rate', 0, ['id'=>'commission_rate','class'=>'form-control', 'required','ng-disabled'=> '!checked', 'ng-if'=>"zero_commission_rate === '1'"]) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        {!! Form::submit('Invest in Product', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <!-- Top Up -->
                        <div class="modal fade" id="topUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::open(['route'=>'top_up_coop_investment']) !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Top Up Investment</h4>
                                    </div>
                                    <div class="modal-body" data-ng-controller="coopInvestController">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            {!! Form::hidden('client_id', $client->id) !!}
                                                            {!! Form::hidden('investment', 'topup') !!}

                                                            {!! Form::label('product_plan_id', 'Select product plan') !!}

                                                            {!! Form::select('product_plan_id', $coop_product_plans->lists('full_name', 'id'), null, ['class'=>'form-control', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_id') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('deduct', 'Payment Type') !!}

                                                            {!! Form::select('deduct',['yes'=>'Deduct from balance', 'no'=>'Add new payment'], null, ['class'=>'form-control', 'step'=>'1000', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'deduct') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" ng-controller="DatepickerCtrl">
                                                            {!! Form::label('invested_date', 'Invested Date') !!}

                                                            {!! Form::text('invested_date', NULL, ['id'=>'invested_date','class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'invested_date') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" ng-controller="DatepickerCtrl">
                                                            {!! Form::label('maturity_date', 'Maturity Date') !!}

                                                            {!! Form::text('maturity_date', NULL, ['id'=>'maturity_date','class'=>'form-control', 'datepicker-popup init-model'=>"maturity_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('amount', 'Amount') !!}

                                                            {!! Form::text('amount', NULL, ['class'=>'form-control', 'step'=>'1000', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('interest_rate', 'Agreed Interest rate (%)') !!}

                                                            {!! Form::number('interest_rate', NULL, ['class'=>'form-control', 'step'=>'0.01', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'interest_rate') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('narrative', 'Narrative') !!}

                                                            {!! Form::textarea('narrative', NULL, ['class'=>'form-control', 'rows'=>'3', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="">
                                                    <h4>Commission</h4>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                {!! Form::label('commission_recipient', 'Commission paid to') !!}

                                                                {!! Form::select('commission_recipient', $commissionrecepients, null,  ['id'=>'commission_recepient','class'=>'form-control', 'init-model'=>'commission_recepient', 'required']) !!}
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <i class="fa fa-spinner fa-spin"
                                                               ng-show="loading === true"></i>
                                                            <div class="form-group"
                                                                 ng-show="zero_commission_rate === '0'">
                                                                {!! Form::label('commission_rate', 'Commission rate') !!}
                                                                <select id="commission_rate"
                                                                        ng-model="commission_rate"
                                                                        ng-options="commissionRate.name for commissionRate in rates"
                                                                        class="form-control"
                                                                        required
                                                                        ng-change="getCommissionRate()"
                                                                        ng-if="zero_commission_rate === '0'">
                                                                </select>
                                                                {!! Form::hidden('commission_rate',null, ['id' => 'commission_rate_value', 'data-ng-value' => 'checkCommissionRateValue()']) !!}
                                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}
                                                            </div>

                                                            <div class="form-group"
                                                                 ng-show="zero_commission_rate === '1'">
                                                                <p class="margin-top-20">This FA has Zero Commission
                                                                    Rate</p>
                                                                {!! Form::hidden('commission_rate', 0, ['id'=>'commission_rate','class'=>'form-control', 'required','ng-disabled'=> '!checked', 'ng-if'=>"zero_commission_rate === '1'"]) !!}
                                                            </div>
                                                        </div>

                                                        {{--<div class = "col-md-6">--}}
                                                        {{--<div class = "form-group">--}}
                                                        {{--{!! Form::label('commission_rate', 'Commission rate') !!}--}}

                                                        {{--{!! Form::number('commission_rate', null, ['class'=>"form-control",'step'=>'0.01', 'required']) !!}--}}
                                                        {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'commission_rate') !!}--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        {!! Form::submit('Top Up Investment', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <!-- Buy Shares -->
                        <div class="modal fade" id="buyShares" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::open(['route'=>'buy_coop_shares']) !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Buy Shares</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    {{--<div class="col-md-12">--}}
                                                    {{--<div class="form-group">--}}
                                                    {{--{!! Form::label('product_plan_id', 'Select product plan') !!}--}}

                                                    {{--{!! Form::select('product_plan_id', $shares_product_plans->lists('full_name', 'id'), null, ['class'=>'form-control', 'required']) !!}--}}
                                                    {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'product_plan_id') !!}--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('deduct', 'Payment Type') !!}

                                                            {!! Form::select('deduct',['yes'=>'Deduct from balance', 'no'=>'Add new payment'], null, ['class'=>'form-control', 'step'=>'1000', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'deduct') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('custodial_account_id', 'Custodial Account') !!}

                                                            {!! Form::select('custodial_account_id', $custodial_accounts, null, ['class'=>'form-control', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'custodial_account_id') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::hidden('client_id', $client->id) !!}

                                                            {!! Form::label('entity_id', 'Share Entity') !!}

                                                            {!! Form::select('entity_id', $share_entities, null, ['class'=>'form-control', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'entity_id') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('category_id', 'Share Category') !!}

                                                            {!! Form::select('category_id',$share_categories, null, ['class'=>'form-control', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'category_id') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group" ng-controller="DatepickerCtrl">
                                                            {!! Form::label('date', 'Date') !!}

                                                            {!! Form::text('date', NULL, ['id'=>'date','class'=>'form-control', 'datepicker-popup init-model'=>"date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'date') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('amount', 'Amount') !!}

                                                            {!! Form::number('amount', NULL, ['class'=>'form-control', 'step'=>'1', 'required']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'amount') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {!! Form::label('narrative', 'Narrative') !!}

                                                            {!! Form::textarea('narrative', NULL, ['class'=>'form-control', 'rows'=>'3', 'ng-required'=>'true']) !!}
                                                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'narrative') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <a href="#" class="btn btn-primary"
                                                               ng-click="show_purchase_price = !show_purchase_price">Set
                                                                Purchase Price</a>

                                                            <div ng-show="show_purchase_price">
                                                                {!! Form::label('purchase_price', 'Purchase Price Per Share') !!}

                                                                {!! Form::number('purchase_price', NULL, ['class'=>'form-control', 'step'=>'0.1']) !!}
                                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'purchase_price') !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        {!! Form::submit('Buy Coop Shares', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <h4 class="pull-left">Product Plans</h4>

                        <!-- Product Plan Modal -->
                        <div class="modal fade" id="addProductPlan" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    {!! Form::open(['route'=>['add_coop_product_plan']]) !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Add Product Plan</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">

                                                    <div class="form-group">
                                                        <div class="col-md-4">{!! Form::label('category', 'Category') !!}</div>
                                                        <div class="col-md-8">{!! Form::select('category', ['shares'=>'Shares', 'product'=>'Product'], 'product', ['class'=>'form-control', 'init-model'=>'category']) !!}</div>
                                                    </div>
                                                    <div class="form-group" ng-hide="category == 'shares'">
                                                        {!! Form::hidden('client_id', $client->id) !!}
                                                        <div class="col-md-4">{!! Form::label('product_id', 'Select Product') !!}</div>

                                                        <div class="col-md-8">{!! Form::select('product_id', $products, null, ['class'=>'form-control']) !!}</div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-4">{!! Form::label('mode_of_payment', 'Mode of payment') !!}</div>

                                                        <div class="col-md-8">{!! Form::select('mode_of_payment', ['Direct Bank Deposit'=>'Direct Bank Deposit', 'M-Pesa Paybill'=>'M-Pesa Paybill', 'Human Resource Remittance'=>'HR Remittance (Cytonn Staff)'], null, ['class'=>'form-control']) !!}</div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-4">{!! Form::label('duration', 'Investment Duration (years)') !!}</div>

                                                        <div class="col-md-8">{!! Form::text('duration', null, ['class'=>'form-control']) !!}</div>
                                                    </div>

                                                    <div class="form-group" ng-hide="category == 'shares'">
                                                        <div class="col-md-4">{!! Form::label('amount', 'Amount Paid (monthly)') !!}</div>

                                                        <div class="col-md-8">{!! Form::text('amount', null, ['class'=>'form-control']) !!}</div>
                                                    </div>

                                                    <div class="form-group" ng-hide="category == 'product'">
                                                        <div class="col-md-4">{!! Form::label('shares', 'No. of Shares (monthly)') !!}</div>

                                                        <div class="col-md-8">{!! Form::text('shares', null, ['class'=>'form-control']) !!}</div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-4">{!! Form::label('start_date', 'Start Date') !!}</div>

                                                        <div class="col-md-8" ng-controller="DatepickerCtrl">
                                                            <div class="input-group">
                                                                {!! Form::text('start_date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"payment_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"
                                                            ng-click="open($event)"><i
                                                                class="glyphicon glyphicon-calendar"></i></button>
                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        {!! Form::submit('Add Product Plan', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Mode of Payment</th>
                                <th>Amount/Shares</th>
                                <th>Duration</th>
                                <th>Start Date</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($client->coopProductPlans as $plan)
                                <tr>
                                    <td>
                                        @if(is_null($plan->product_id))
                                            Shares
                                        @else
                                            {!! $plan->product['name'] !!}
                                        @endif
                                        <?php
                                        $category = is_null($plan->product_id) ? 'shares' : 'product';
                                        ?>
                                    </td>
                                    <td>{!! $plan->mode_of_payment !!}</td>
                                    <td>
                                        @if(is_null($plan->amount))
                                            {!! $plan->shares !!}
                                        @else
                                            {!! \Cytonn\Presenters\AmountPresenter::currency($plan->amount) !!}
                                        @endif
                                    </td>
                                    <td>{!! $plan->duration !!} years</td>
                                    <td>{!! (new \Carbon\Carbon($plan->start_date))->format('l jS \\of F Y') !!}</td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#editPaymentPlan{!! $plan->id !!}"
                                           title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        <!-- Edit Product Plan Modal -->
                                        <div class="modal fade" id="editPaymentPlan{!! $plan->id !!}" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    {!! Form::model($plan,['route'=>['edit_coop_product_plan', $plan->id], 'method'=>'PUT']) !!}
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Edit Product Plan</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="detail-group">

                                                                    <div class="form-group">
                                                                        <div class="col-md-4">{!! Form::label('category', 'Category') !!}</div>
                                                                        <div class="col-md-8">{!! Form::select('category', ['shares'=>'Shares', 'product'=>'Product'], $category, ['class'=>'form-control', 'ng-model'=>'category_edit']) !!}</div>
                                                                    </div>
                                                                    <div class="form-group"
                                                                         ng-hide="category_edit == 'shares'">
                                                                        {!! Form::hidden('client_id', $client->id) !!}
                                                                        <div class="col-md-4">{!! Form::label('product_id', 'Select Product') !!}</div>

                                                                        <div class="col-md-8">{!! Form::select('product_id', $products, null, ['class'=>'form-control']) !!}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-4">{!! Form::label('mode_of_payment', 'Investment Duration (years)') !!}</div>

                                                                        <div class="col-md-8">{!! Form::select('mode_of_payment', ['Direct Bank Deposit'=>'Direct Bank Deposit', 'M-Pesa Paybill'=>'M-Pesa Paybill', 'Human Resource Remittance'=>'HR Remittance (Cytonn Staff)'], null, ['class'=>'form-control']) !!}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-4">{!! Form::label('duration', 'Investment Duration (years)') !!}</div>

                                                                        <div class="col-md-8">{!! Form::text('duration', null, ['class'=>'form-control']) !!}</div>
                                                                    </div>

                                                                    <div class="form-group"
                                                                         ng-hide="category_edit == 'shares'">
                                                                        <div class="col-md-4">{!! Form::label('amount', 'Amount Paid (monthly)') !!}</div>

                                                                        <div class="col-md-8">{!! Form::text('amount', null, ['class'=>'form-control']) !!}</div>
                                                                    </div>

                                                                    <div class="form-group"
                                                                         ng-hide="category_edit == 'product'">
                                                                        <div class="col-md-4">{!! Form::label('shares', 'No. of Shares (monthly)') !!}</div>

                                                                        <div class="col-md-8">{!! Form::text('shares', null, ['class'=>'form-control']) !!}</div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-md-4">{!! Form::label('start_date', 'Start Date') !!}</div>

                                                                        <div class="col-md-8"
                                                                             ng-controller="DatepickerCtrl">
                                                                            <div class="input-group">
                                                                                {!! Form::text('start_date', null, ['class'=>'form-control', 'datepicker-popup init-model'=>"payment_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                                                <span class="input-group-btn">
                                                                                <button type="button"
                                                                                        class="btn btn-default"
                                                                                        ng-click="open($event)"><i
                                                                                            class="glyphicon glyphicon-calendar"></i></button>
                                                                            </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        {!! Form::submit('Edit Product Plan', ['class'=>'btn btn-success']) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <a href="#" data-toggle="modal"
                                           data-target="#terminatePaymentPlan{!! $plan->id !!}" title="Terminate"><i
                                                    class="fa fa-trash-o" style="color: orange;"></i></a>
                                        <!-- Terminate Product Plan Modal -->
                                        <div class="modal fade" id="terminatePaymentPlan{!! $plan->id !!}" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    {!! Form::model($plan,['route'=>['terminate_coop_product_plan', $plan->id], 'method'=>'PUT']) !!}
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Terminate Product
                                                            Plan</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                {!! Form::hidden('client_id', $client->id) !!}
                                                                <p>Are you sure you want to terminate this payment
                                                                    plan?</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        {!! Form::submit('Terminate', ['class'=>'btn  btn-warning']) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#" data-toggle="modal"
                                           data-target="#deletePaymentPlan{!! $plan->id !!}" title="Delete"><i
                                                    class="fa fa-remove" style="color: red;"></i></a>
                                        <!-- Delete Product Plan Modal -->
                                        <div class="modal fade" id="deletePaymentPlan{!! $plan->id !!}" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    {!! Form::model($plan,['route'=>['delete_coop_product_plan', $plan->id], 'method'=>'DELETE']) !!}
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Delete Product
                                                            Plan</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                @if($plan->investments->count() > 0)
                                                                    <p>This payment plan cannot be deleted because there
                                                                        is an investment associated with it.</p>
                                                                @else
                                                                    {!! Form::hidden('client_id', $client->id) !!}
                                                                    <p>Are you sure you want to delete this payment
                                                                        plan?</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        @if($plan->investments->count() > 0)
                                                        @else
                                                            {!! Form::submit('Delete Product Plan', ['class'=>'btn btn-danger']) !!}
                                                        @endif
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <button type="button" class="btn btn-success pull-right" ng-controller="PopoverCtrl"
                                popover-trigger="mouseenter" data-toggle="modal" data-target="#addProductPlan"><i
                                    class="fa fa-plus-circle"></i> Add Product Plan
                        </button>
                    </div>
                </div>
            </div>
            @endModule

            @module('coop')
            <el-collapse>
                <el-collapse-item title="CLIENT SHARES" name="CLIENT SHARES">
                    @if($share_holdings)
                        <div class="detail-group">
                            <div class="col-md-12">
                                <table class="table table-striped table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Number</th>
                                        <th>Purchase Price</th>
                                        <th>Date</th>
                                        <th>Shares Entity</th>
                                        <th>Shares Category</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($share_holdings as $share_holding)
                                        <tr>
                                            <td>{!! $share_holding->number !!}</td>
                                            <td>{!! $share_holding->purchase_price !!}</td>
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($share_holding->telephone_cell) !!}</td>
                                            <td>{!! $share_holding->entity->name !!}</td>
                                            <td>{!! $share_holding->category->name !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <p>Client has no shares yet.</p>
                    @endif
                </el-collapse-item>
            </el-collapse>
            @endModule

            <el-collapse>
                <el-collapse-item title="JOINTHOLDERS" name="JOINTHOLDERS">
                    @if($client->joint && $client->jointHolders)
                        <div class="detail-group">
                            <h4 class="col-md-12">Joint Holders</h4>

                            <div class="col-md-12">
                                <table class="table table-striped table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email Address</th>
                                        <th>Phone Number</th>
                                        <th>Pin Number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($jointHolders as $holder)
                                        <tr>
                                            <td>
                                                <a href="/dashboard/clients/joint/{!! $holder->details->id !!}">{!! ucfirst($holder->details->firstname) !!} {!! ucfirst($holder->details->lastname) !!}</a>
                                            </td>
                                            <td>{!! $holder->details->email !!}</td>
                                            <td>{!! $holder->details->telephone_cell !!}</td>
                                            <td>{!! $holder->details->pin_no !!}</td>
                                            <td>
                                                <a class="btn btn-info"
                                                   href="/dashboard/clients/joint/{!! $holder->details->id !!}/edit"><i
                                                            class="fa fa-edit"></i></a>

                                                <a class="btn btn-danger" data-toggle="modal"
                                                   data-target="#delete-jointHolder-{!! $holder->details->id !!}"
                                                   data-backdrop="static" data-keyboard="false">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <p>This is not a joint account</p>
                    @endif

                    <a class="btn btn-success" href="/dashboard/clients/{!! $client->id !!}/add-joint-holder"><i
                                class="fa fa-plus"></i> Add</a>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="ADDITIONAL EMAIL" name="ADDITIONAL EMAIL">
                    @if(count($client->emails) > 0)
                        <ol>
                            @foreach($client->emails as $email)
                                <li>{!! $email !!}</li>
                            @endforeach
                        </ol>
                    @else
                        <p>No additional email saved</p><br/>
                    @endif

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#email-modal">
                        Add/Edit Email Addresses
                    </button>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="TAX EXEMPTION" name="TAX EXEMPTION">
                    @if(count($client->taxExemptions) > 0)
                        <table class="table table-responsive table-striped">
                            <thead>
                            <th>#</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Maximum Amount</th>
                            <th>Document</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($client->taxExemptions as $key => $taxExemption)
                                <tr>
                                    <td>{!! $key + 1 !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($taxExemption->start) !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($taxExemption->end) !!}</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($taxExemption->amount) !!}</td>
                                    <td>
                                        <a class="" target="_blank"
                                           href="/dashboard/investments/client-instructions/filled-application-documents/{!! $taxExemption->document_id !!} ">
                                            <span class="glyphicon glyphicon-file"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="button-group">
                                            <button class="btn btn-info" data-toggle="modal"
                                                    data-target="#editTaxExemption-{!! $taxExemption->id !!}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </button>
                                            <button class="btn btn-danger" data-toggle="modal"
                                                    data-target="#delTaxExemption-{!! $taxExemption->id !!}">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    @else
                        <div class="alert alert-info">
                            <p>No tax exemptions saved</p>
                        </div>
                    @endif

                    <button class="btn btn-success" data-toggle="modal" data-target="#taxExemption"> ADD TAX
                        EXEMPTION
                    </button>

                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="CONTACT PERSON" name="CONTACT PERSON">
                    @if($client->contactPersons()->count() == 0)
                        <p>No contact persons added</p><br/>
                    @else
                        <table class="table table-responsive table-striped">
                            <thead>
                            <th>Name</th>
                            <th>ID</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Shares</th>
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach($client->contactPersons as $contact)
                                <tr>
                                    <td>{!! $contact->name !!}</td>
                                    <td>{!! $contact->id_number !!}</td>
                                    <td>{!! $contact->phone !!}</td>
                                    <td>{!! $contact->email !!}</td>
                                    <td>{!! $contact->address !!}</td>
                                    <td>{!! $contact->percentage_share !!}%</td>
                                    <td>
                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                                data-target="#contact-modal-{!! $contact->id !!}">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <a class="btn btn-danger"
                                           href="/dashboard/clients/{{ $client->id }}/contact_person/{{ $contact->id }}/delete"><i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#contact-modal">
                        Add a contact person
                    </button>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="EMAIL INDEMNITY" name="EMAIL INDEMNITY">
                    @if($indemnities->count() <= 0)
                        <p>This client has not indemnified any emails</p>
                    @else
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Email</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($indemnities as $indemnity)
                                <tr>
                                    <td>{!! $indemnity->email !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($indemnity->created_at) !!}</td>
                                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($indemnity->status()) !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-success" target="_blank"
                                               href="/dashboard/investments/client-instructions/filled-application-documents/{!! $indemnity->document_id !!} ">
                                                View File
                                            </a>
                                            @if($indemnity->status())
                                                <a class="btn btn-info"
                                                   href="/dashboard/clients/details/{!! $client->id !!}/indemnity/{!! $indemnity->id !!}/deactivate">Deactivate</a>
                                            @else
                                                <a class="btn btn-primary"
                                                   href="/dashboard/clients/details/{!! $client->id !!}/indemnity/{!! $indemnity->id !!}/activate">Activate</a>
                                            @endif
                                            <a class="btn btn-danger"
                                               href="/dashboard/clients/details/{!! $client->id !!}/indemnity/{!! $indemnity->id !!}/delete">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add
                        emails
                    </button>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="CONSENT FORMS" name="CONSENT FORMS">
                    @if(count($consentForms))
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Client Email</th>
                                <th>Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Uploads</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($consentForms as $consentForm)
                                <tr>
                                    <td>{!! $consentForm->email !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($consentForm->date) !!}</td>
                                    <td class="text-center">{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($consentForm->consented) !!}</td>
                                    @if((is_null($consentForm->document_id)))
                                        <td class="text-danger text-center"><span
                                                    class="glyphicon glyphicon-remove-circle"></span></td>
                                    @else
                                        <td class="text-success text-center"><span
                                                    class="glyphicon glyphicon-ok-circle"></span></td>
                                    @endif
                                    <td class="text-center">
                                        <div class="btn-group">
                                            @if(!(is_null($consentForm->document_id)))
                                                <a class="btn btn-info" target="_blank"
                                                   href="/dashboard/investments/client-instructions/filled-application-documents/{!! $consentForm->document_id !!} ">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                            @endif
                                            <a class="btn btn-danger" data-toggle="modal"
                                               data-target="#delConsentForm-{{$consentForm->id}}" data-backdrop="static"
                                               data-keyboard="false">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>This client has not given a filled Personal Data Consent form</p>
                    @endif

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#consentForm">Add
                        consent form
                    </button>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="SIGNING MANDATE" name="SIGNING MANDATE">
                    <p><b>Signing Mandate: </b> {!! $client->present()->getSigningMandate !!}</p>

                    @if($client->joint == 1 || $client->client_type_id == 2)
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#signingMandate">
                            Update signing mandate
                        </button>
                    @endif

                    <hr>

                    @if($client->clientSignatures->count() <= 0)
                        <p>The client has no uploaded signature</p>
                    @else
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date</th>
                                <th>Active</th>
                                <th>Actions</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($client->clientSignatures as $clientSignature)
                                <tr>
                                    <td>{!! $clientSignature->present()->getName !!}</td>
                                    <td>{!! $clientSignature->present()->getEmail !!}</td>
                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clientSignature->updated_at) !!}</td>
                                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($clientSignature->active) !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-success" target="_blank"
                                                href="/dashboard/investments/client-instructions/filled-application-documents/{!! $clientSignature->document_id !!}">
                                                View File
                                            </a>
                                            @if($clientSignature->active)
                                                <a class="btn btn-danger" href="#" data-toggle="modal" data-target="#deactivate-signature-{!! $clientSignature->id !!}">
                                                    Deactivate
                                                </a>
                                            @else
                                                <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#deactivate-signature-{!! $clientSignature->id !!}">
                                                    Activate
                                                </a>
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-default" href="#" data-toggle="modal" data-target="#edit-signature-{!! $clientSignature->id !!}">
                                                Edit
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="modal fade" id="deactivate-signature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    @if(is_null($client->relationshipPerson))
                                        {!! Form::open(['route'=>['add_relationship_person', $client->id]]) !!}
                                    @else
                                        {!! Form::model($client->relationshipPerson, ['route'=>['add_relationship_person', $client->id], 'method'=>'POST']) !!}
                                    @endif
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Add Relationship Person</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="detail-group">
                                                    <div class="form-group">
                                                        {!! Form::hidden('client_id', $client->id) !!}
                                                        <div class="col-md-4">{!! Form::label('relationship_person_id', 'Select Relationship Person') !!}</div>
                                                        <div class="col-md-8">{!! Form::select('relationship_person_id', $commissionrecepients, $client->relationship_person_id, ['class'=>'form-control']) !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        {!! Form::submit('Save', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    @endif

                    <signing-mandate
                            :client="{{ json_encode($client) }}"
                            :holders="{{ json_encode($signatureHolders) }}"
                            :contacts="{{ json_encode($signatureContactPersons) }}"
                            :relations="{{ json_encode($signatureRelationshipPersons) }}">
                    </signing-mandate>


                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="RELATIONSHIP PERSON" name="RELATIONSHIP PERSON">
                    @if(is_null($client->relationshipPerson))
                        <p>This client does not have a relationship person</p>
                        <button type="button" class="btn btn-success" data-toggle="modal"
                                data-target="#relationship-person">Add Relationship Person
                        </button>
                    @else
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{!! $client->relationshipPerson->name !!}</td>
                                <td>{!! $client->relationshipPerson->email !!}</td>
                                <td>{!! $client->relationshipPerson->phone !!}</td>
                                <td><a class="btn btn-success" href="#" data-toggle="modal"
                                       data-target="#relationship-person">Edit</a></td>
                            </tr>
                            </tbody>
                        </table>
                    @endif

                    <div class="modal fade" id="relationship-person" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                @if(is_null($client->relationshipPerson))
                                    {!! Form::open(['route'=>['add_relationship_person', $client->id]]) !!}
                                @else
                                    {!! Form::model($client->relationshipPerson, ['route'=>['add_relationship_person', $client->id], 'method'=>'POST']) !!}
                                @endif
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    @if(is_null($client->relationshipPerson))
                                    <h4 class="modal-title" id="myModalLabel">Add Relationship Person</h4>
                                    @else
                                    <h4 class="modal-title" id="myModalLabel">Edit Relationship Person</h4>
                                    @endif
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="detail-group">
                                                <div class="form-group">
                                                    {!! Form::hidden('client_id', $client->id) !!}
                                                    <div class="col-md-4">{!! Form::label('relationship_person_id', 'Select Relationship Person') !!}</div>
                                                    <div class="col-md-8">{!! Form::select('relationship_person_id', $commissionrecepients, $client->relationship_person_id, ['class'=>'form-control']) !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('Save', ['class'=>'btn btn-danger']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="INCOMING FINANCIAL ADVISOR" name="INCOMING FINANCIAL ADVISOR">
                    <?php $fa = $client->getLatestFa(); ?>
                    @if(is_null($client->incomingFa))
                        <p>This client does not have an incoming Financial Advisor. @if($fa)Current FA is {{ $fa->name }} {{ $fa->email }}@endif</p>
                        <button type="button" class="btn btn-success" data-toggle="modal"
                                data-target="#incoming-fa">Assign Financial Advisor
                        </button>
                    @else
                        <p>@if($fa)Current FA is {{ $fa->name }} {{ $fa->email }} @endif</p>
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{!! $client->incomingFa->name !!}</td>
                                <td>{!! $client->incomingFa->email !!}</td>
                                <td>{!! $client->incomingFa->phone !!}</td>
                                <td><a class="btn btn-success" href="#" data-toggle="modal"
                                       data-target="#incoming-fa">Edit</a></td>
                            </tr>
                            </tbody>
                        </table>
                    @endif

                    <div class="modal fade" id="incoming-fa" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                @if(is_null($client->incomingFa))
                                    {!! Form::open(['route'=>['add_incoming_fa', $client->id]]) !!}
                                @else
                                    {!! Form::model($client->incomingFa, ['route'=>['edit_incoming_fa', $client->id], 'method'=>'POST']) !!}
                                @endif
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    @if(is_null($client->incomingFa))
                                        <h4 class="modal-title" id="myModalLabel">Add Incoming FA</h4>
                                    @else
                                        <h4 class="modal-title" id="myModalLabel">Edit Incoming FA</h4>
                                    @endif
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="detail-group">
                                                <div class="form-group">
                                                    {!! Form::hidden('client_id', $client->id) !!}
                                                    <div class="col-md-4">{!! Form::label('incoming_fa_id', 'Select Financial Advisor') !!}</div>
                                                    <div class="col-md-8">{!! Form::select('incoming_fa_id', $commissionrecepients, $client->incoming_fa_id, ['class'=>'form-control']) !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @if(!is_null($client->incomingFa))
                                        <input type="hidden" name="previous_fa_id" value="{{$client->incomingFa->id}}">
                                    @endif
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('Save', ['class'=>'btn btn-danger']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="GUARD" name="GUARD">
                    @if($client->guards()->count() > 0)
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Type</th>
                                <th>Minimum Balance</th>
                                <th>Portfolio Investor</th>
                                <th>Reason</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($client->guards as $guard)
                                <tr>
                                    <td>{!! $guard->guardType->name !!}</td>
                                    <td>{!! $guard->minimum_balance !!}</td>
                                    <td>{!! ($guard->portfolioInvestor) ? $guard->portfolioInvestor->name : '' !!}</td>
                                    <td>{!! $guard->reason !!}</td>
                                    <td>
                                        <a class="btn btn-success" href="#" data-toggle="modal"
                                           data-target="#editClientGuardModal-{!! $guard->id !!}"><i
                                                    class="fa fa-edit"></i></a>
                                        <a class="btn btn-danger" href="#" data-toggle="modal"
                                           data-target="#deleteClientGuardModal-{!! $guard->id !!}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>This client does not have any guards set</p>
                    @endif
                    <button type="button" class="btn btn-success" data-toggle="modal"
                            data-target="#addClientGuardModal">Add Guard
                    </button>
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="INTEREST REINVESTED PLAN" name="INTEREST REINVESTED PLAN">
                    @if($client->combine_interest_reinvestment)
                        <table class="table table-responsive table-hover table-striped">
                            <thead>
                            <tr>
                                <th width="30%">Combine Interest</th>
                                <th>Combine Type</th>
                                <th>Combine Interest Reinvestment Tenor</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($client->combine_interest_reinvestment_tenor) !!}</td>
                                <td>{!! $client->present()->getReinvestmentInterestType !!}</td>
                                <td>{!! $client->combine_interest_reinvestment_tenor !!} Months</td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editTenor"><i class="fa fa-edit"></i></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="#activateCombinedInterestReinvestment">Deactivate
                        </button>
                    @else
                        <button type="button" class="btn btn-success" data-toggle="modal"
                                data-target="#activateCombinedInterestReinvestment">Activate
                        </button>
                    @endif
                </el-collapse-item>
            </el-collapse>

            <el-collapse>
                <el-collapse-item title="STANDING ORDER" name="STANDING ORDER">
                    <standing-order
                            :client="{{ $client }}"
                            :orders="{{ $orders }}"
                            :investments="{{ $investments }}"
                            :projects="{{ $projects }}">

                    </standing-order>
                </el-collapse-item>
            </el-collapse>
        </div>
    </div>

    @include('clients.partials.details_partial')
@stop
