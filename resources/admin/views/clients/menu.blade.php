@extends('layouts.default')

@section('content')

    <bread-crumb title="">
        <a class="current" slot="sub_1" href="#">Clients</a>
    </bread-crumb>

    <div class="admin_menus dashboard__account_summary ">
        <div class="summaries">
            <menu-card
                    title="Clients"
                    statistics_title="ACTIVE CLIENTS"
                    statistics="{!! $gen->activeClients($manager) !!}"
                    :fetch="false"
                    link="/dashboard/clients"
                    :first="true"
                    description="Manage Clients">
                <i class="fa fa-users"></i>
            </menu-card>

            <menu-card
                    title="Account Cash"
                    link="/dashboard/investments/accounts-cash"
                    :first="false"
                    description="Manage Client Inflows">
                <i class="fa fa-money"></i>
            </menu-card>


            <menu-card
                    title="Client Payments"
                    link="/dashboard/investments/client-payments"
                    :first="false"
                    description="Manage Client Payments">
                <i class="fa fa-dollar"></i>
            </menu-card>

            <menu-card
                    title="Compliance"
                    link="/dashboard/investments/compliance"
                    :first="false"
                    description="Client Compliance Documents">
                <i class="fa fa-gavel"></i>
            </menu-card>

            <menu-card
                    title="Risky Client"
                    link="/dashboard/clients/risky"
                    :first="false"
                    description="Manage Risky Clients">
                <i class="fa fa-warning"></i>
            </menu-card>

{{--            <menu-card--}}
{{--                    title="Loyalty Points"--}}
{{--                    link="/dashboard/clients/loyalty-points"--}}
{{--                    :first="false"--}}
{{--                    description="Manage Clients Loyalty Points">--}}
{{--                <i class="fa fa-hashtag"></i>--}}
{{--            </menu-card>--}}
        </div>
    </div>
@stop
