@extends('layouts.default')
@section('content')
    <div class = "col-md-12">
        <div class = "panel-dashboard">
            @include('clients.partials.index')
            {{--<ul id="myTabs" class="nav nav-tabs" role="tablist">--}}
                {{--<li role="presentation" class=""><a href="#all-clients" id="section-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" class="text-uppercase">All Clients</a></li>--}}
                {{--<li role="presentation" class=""><a href="#unitization-clients" id="section-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false" class="text-uppercase">Unitization Clients</a></li>--}}
            {{--</ul>--}}
            {{--<div id="myTabContent" class="tab-content margin-top-25">--}}
                {{--<div role="tabpanel" class="tab-pane fade" id="all-clients" aria-labelledby="section-tab">--}}
                    {{--@include('clients.partials.index')--}}
                {{--</div>--}}

                {{--<div role="tabpanel" class="tab-pane fade" id="unitization-clients" aria-labelledby="section-tab">--}}
                    {{--<unit-fund-holders></unit-fund-holders>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@stop