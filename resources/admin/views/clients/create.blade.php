@extends('layouts.default')

@section('content')
    <div class="col-md-11">

        <div class="col-md-12 panel panel-dashboard form-grouping">

            {!! Form::model($client, ['route'=>['create_client', $client->id]]) !!}

            @include("layouts.partials.errors")

            <div class="detail-group">
                <h4 class="col-md-12">Application Details</h4>
                <div class="form-group">
                    <div class="col-md-2"><span
                                class="required-field">{!! Form::label('complete', 'Client type') !!}</span></div>

                    <div class="col-md-4">{!! Form::select('complete', [true=>'Complete', false=>'Incomplete'], true, ['class'=>'form-control', 'init-model'=>'complete']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'complete') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">

                        {!! Form::radio('franchise', 1,($client->franchise == 1),['class'=>'']); !!} Franchise
                        {!! Form::radio('franchise', 0,($client->franchise == 0),['class'=>'']); !!} Non Franchise

                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'franchise') !!}
                    </div>
                </div>
            </div>

            <div class="detail-group clearfix">
                <h4 class="col-md-12">Client Details</h4>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('client_code', 'Client Code') !!}</div>

                    <div class="col-md-4">{!! Form::text('client_code', NULL, ['class'=>'form-control', 'init-model'=>"client_code"]) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_code') !!}</div>
                </div>

                @if($client->contact->entityType->name == 'corporate')
                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('corporate_registered_name', 'Registered name') !!}</div>

                        <div class="col-md-4">{!! Form::text('corporate_registered_name', $client->contact->corporate_registered_name, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_registered_name') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('registration_number', 'Registration Number') !!}</div>

                        <div class="col-md-4">{!! Form::text('registration_number', $client->contact->registration_number, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registration_number') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('corporate_trade_name', 'Trade name') !!}</div>

                        <div class="col-md-4">{!! Form::text('corporate_trade_name', $client->contact->corporate_trade_name, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'corporate_trade_name') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('registered_address', 'Registered address') !!}</div>

                        <div class="col-md-4">{!! Form::text('registered_address', $client->contact->registered_address, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'registered_address') !!}</div>
                    </div>
                @else
                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('title_id', 'Title') !!}</div>

                        <div class="col-md-4">{!! Form::select('title_id', $titles, $client->contact->title_id,  ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title_id') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('firstname', 'First name') !!}</div>

                        <div class="col-md-4">{!! Form::text('firstname', $client->contact->firstname, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('middlename', 'Middlename') !!}</div>

                        <div class="col-md-4">{!! Form::text('middlename', $client->contact->middlename, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('lastname', 'Last name') !!}</div>

                        <div class="col-md-4">{!! Form::text('lastname', $client->contact->lastname, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('preferredname', 'Preferred name') !!}</div>

                        <div class="col-md-4">{!! Form::text('preferredname', $client->contact->preferredname, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'preferredname') !!}</div>
                    </div>

                    <div class="form-group clearfix" ng-controller="DatepickerCtrl">
                        <div class="col-md-2">{!! Form::label('dob', 'Date of Birth') !!}</div>

                        <div class="col-md-4">{!! Form::text('dob', NULL, ['class'=>'form-control', 'datepicker-popup init-model'=>"invested_date", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'dob') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('residence', 'Residential Address') !!}</div>

                        <div class="col-md-4">{!! Form::text('residence', NULL, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'residence') !!}</div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('email', 'Email address') !!}</div>

                    <div class="col-md-4">{!! Form::text('email', $client->contact->email, ['class'=>'form-control'])!!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('country_id', 'Country') !!}</div>

                    <div class="col-md-4">{!! Form::select('country_id', $countries, null,  ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'country_id') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('nationality_id', 'Nationality') !!}</div>

                    <div class="col-md-4">{!! Form::select('nationality_id', $countries, null,  ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'nationality_id') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('contact_phone', 'Contact Phone number') !!}</div>
                    <div class="col-md-4">{!! Form::text('contact_phone', $client->contact->phone , ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_phone') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('phone', 'Phone number') !!}</div>
                    <div class="col-md-4">{!! Form::text('phone', null , ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'phone') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('telephone_office', 'Office Tel No') !!}</div>

                    <div class="col-md-4">{!! Form::text('telephone_office', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_office') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('telephone_home', 'Home Telephone') !!}</div>

                    <div class="col-md-4">{!! Form::text('telephone_home', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_home') !!}</div>
                </div>


                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('pin_no', 'Pin Number') !!}</div>

                    <div class="col-md-4">{!! Form::text('pin_no', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}</div>
                </div>

            {{--<div class="form-group">--}}
            {{--<div class="col-md-2"><span class="required-field">{!! Form::label('taxable', 'Taxable') !!}</span>--}}
            {{--</div>--}}

            {{--<div class="col-md-4">--}}
            {{--                        {!! Form::select('taxable', [1=>'Yes', 0=>'No'], null, ['class'=>'form-control']) !!}--}}
            {{--@if(count($client->taxExemptions) > 0)--}}
            {{--Exempted :--}}
            {{--<button class="btn btn-info btn-sm" type="button" data-toggle="modal"--}}
            {{--data-target="#seeTaxExemption"><span class="glyphicon glyphicon-eye-open"></span>--}}
            {{--View--}}
            {{--</button>--}}
            {{--@else--}}
            {{--YES:--}}
            {{--<button class="btn btn-primary btn-sm" type="button" data-toggle="modal"--}}
            {{--data-target="#taxExemption"><span--}}
            {{--class="glyphicon glyphicon-plus"></span>--}}
            {{--Add Exemption--}}
            {{--</button>--}}
            {{--@endif--}}
            {{--{!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'taxable') !!}--}}
            {{--</div>--}}

            {{--</div>--}}

            <!-- Modal -->
                <div class="modal fade" id="taxExemption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <taxable-modal :client="{{ json_encode($client) }}"></taxable-modal>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="seeTaxExemption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content" style="padding: 30px;">
                            @if(($client->taxExemptions)->count() > 0)
                                <table class="table table-responsive table-striped">
                                    <thead>
                                    <th>#</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Amount</th>
                                    <th>Document</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    @foreach($client->taxExemptions as $key => $exemption)
                                        <tr>
                                            <td>{!! $key + 1 !!}</td>
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($exemption->start) !!}</td>
                                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($exemption->end) !!}</td>
                                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($exemption->amount) !!}</td>
                                            <td>
                                                <a class="" target="_blank"
                                                   href="/dashboard/investments/client-instructions/filled-application-documents/{!! $exemption->document_id !!} ">
                                                    <span class="glyphicon glyphicon-file"></span>
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger" data-toggle="modal"
                                                        data-target="#delTaxExemption-{!! $exemption->id !!}"><span
                                                            class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p>No tax exemptions saved</p><br/>
                            @endif
                        </div>
                    </div>
                </div>


                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('id_or_passport', 'ID/Passport Number') !!}</div>

                    <div class="col-md-4">{!! Form::text('id_or_passport', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'id_or_passport') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('method_of_contact_id', 'Method of Contact') !!}</div>

                    <div class="col-md-4">{!! Form::select('method_of_contact_id', $contactmethods, NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'method_of_contact_id') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('business_sector', 'Business Sector') !!}</div>

                    <div class="col-md-4">{!! Form::text('business_sector', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'business_sector') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('postal_code', 'Postal Code') !!}</div>

                    <div class="col-md-4">{!! Form::text('postal_code', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('postal_address', 'Postal Address') !!}</div>

                    <div class="col-md-4">{!! Form::text('postal_address', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('town', 'Town') !!}</div>

                    <div class="col-md-4">{!! Form::text('town', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('street', 'Street') !!}</div>

                    <div class="col-md-4">{!! Form::text('street', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'street') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">{!! Form::label('account_name', 'Account Name or Tag') !!}</div>

                    <div class="col-md-4">{!! Form::text('account_name', NULL, ['class'=>'form-control']) !!}
                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'account_name') !!}</div>
                </div>
            </div>

            @if($client->contact->entityType->name == 'corporate')
                <div class="detail-group">
                    <h4 class="col-md-12">Contact Person</h4>
                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('contact_person_title', 'Contact Person Title') !!}</div>

                        <div class="col-md-4">{!! Form::select('contact_person_title',  $titles , NULL, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_title') !!}</div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('contact_person_fname', 'Contact Person First name') !!}</div>

                        <div class="col-md-4">{!! Form::text('contact_person_fname', NULL, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_fname') !!}</div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="form-group">
                        <div class="col-md-2">{!! Form::label('contact_person_lname', 'Contact Person Last name') !!}</div>

                        <div class="col-md-4">{!! Form::text('contact_person_lname', NULL, ['class'=>'form-control']) !!}
                            {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'contact_person_lname') !!}</div>
                    </div>
                </div>
            @endif

            {{--            <div class="detail-group">--}}
            {{--                <h4 class="col-md-12">Bank Information</h4>--}}

            {{--                <!-- Investor bank informaion -->--}}
            {{--                <div class="form-group">--}}
            {{--                    <div class="col-md-2"><span--}}
            {{--                                class="required-field">{!! Form::label('investor_account_name', 'Account name') !!}</span>--}}
            {{--                    </div>--}}

            {{--                    <div class="col-md-4">{!! Form::text('investor_account_name', null, ['class'=>'form-control']) !!}--}}
            {{--                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_account_name') !!}</div>--}}
            {{--                </div>--}}

            {{--                <div class="form-group">--}}
            {{--                    <div class="col-md-2"><span--}}
            {{--                                class="required-field">{!! Form::label('investor_account_number', 'Account number') !!}</span>--}}
            {{--                    </div>--}}

            {{--                    <div class="col-md-4">{!! Form::text('investor_account_number', null, ['class'=>'form-control']) !!}--}}
            {{--                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_account_number') !!}</div>--}}
            {{--                </div>--}}

            {{--                <div class="form-group">--}}
            {{--                    <div class="col-md-2"><span--}}
            {{--                                class="required-field">{!! Form::label('investor_bank', 'Bank') !!}</span></div>--}}

            {{--                    <div class="col-md-4">{!! Form::text('investor_bank', null, ['class'=>'form-control']) !!}--}}
            {{--                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_bank') !!}</div>--}}
            {{--                </div>--}}

            {{--                <div class="form-group">--}}
            {{--                    <div class="col-md-2"><span--}}
            {{--                                class="required-field">{!! Form::label('investor_bank_branch', 'Branch') !!}</span>--}}
            {{--                    </div>--}}

            {{--                    <div class="col-md-4">{!! Form::text('investor_bank_branch', null, ['class'=>'form-control']) !!}--}}
            {{--                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_bank_branch') !!}</div>--}}
            {{--                </div>--}}

            {{--                <div class="form-group">--}}
            {{--                    <div class="col-md-2">{!! Form::label('investor_clearing_code', 'Clearing code') !!}</div>--}}

            {{--                    <div class="col-md-4">{!! Form::text('investor_clearing_code', null, ['class'=>'form-control']) !!}--}}
            {{--                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_clearing_code') !!}</div>--}}
            {{--                </div>--}}

            {{--                <div class="form-group">--}}
            {{--                    <div class="col-md-2">{!! Form::label('investor_swift_code', 'Swift Code') !!}</div>--}}

            {{--                    <div class="col-md-4">{!! Form::text('investor_swift_code', null, ['class'=>'form-control']) !!}--}}
            {{--                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'investor_swift_code') !!}</div>--}}
            {{--                </div>--}}

            {{--            </div>--}}

            <div class="detail-group">
                <div class="col-md-12 margin-top-20">
                    {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    {{--Delete Tax Exemption Modal--}}
    @if(($client->taxExemptions)->count() > 0)
        @foreach($client->taxExemptions as $exemption)
            <div class="modal fade" id="delTaxExemption-{!! $exemption->id !!}" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {!! Form::open(['route'=>['delete_tax_exemption',  $exemption->id]]) !!}
                        <div class="modal-header">
                            <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></a>
                            <h4 class="modal-title text-center" id="myModalLabel">Are you sure you want to delete the
                                client Tax
                                Exemption?</h4>
                        </div>
                        <div class="modal-body row">
                            <div class="modal-body">
                                <p><b>Name</b>
                                    : {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}
                                    &nbsp;</p>
                                <p><b>Start
                                        Date</b>:&nbsp;{!! \Cytonn\Presenters\DatePresenter::formatDate($exemption->start) !!}
                                </p>
                                <p><b>End
                                        Date</b>:&nbsp;{!! \Cytonn\Presenters\DatePresenter::formatDate($exemption->end) !!}
                                </p>
                                <br>
                                <input type="hidden" name="id" value="{{ $client->id }}">

                                <div class="form-group">
                                    {!! Form::label('reason', 'Reason for deleting tax exmeption or enter (DELETE)') !!}
                                    {!! Form::text('reason', null,['class'=>'form-control', 'required']) !!}
                                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'reason') !!}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a data-dismiss="modal" class="btn btn-default">Cancel</a>
                            {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endforeach
    @endif
    {{--end of Delete Tax Exemption Modal--}}
@stop