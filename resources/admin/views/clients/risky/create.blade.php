@extends('layouts.default')
@section('content')

    <div class = "col-md-12">
        <div class = "panel-dashboard">
            <h3>@if($edit) Edit Risky Client Form @else Add Risky Client Form @endif</h3>
            <div class="float-left pull-right">
                <a href="/dashboard/clients/risky" class="btn btn-default">View All</a>
            </div>

            <add-risky-client :edit="{{json_encode($edit)}}" :risk_id="{{json_encode($risk_id)}}"></add-risky-client>

        </div>
    </div>
@stop