@extends('layouts.default')
@section('content')
    <div class="col-md-12">
        <div class="panel-dashboard">
            <div ng-controller="RiskyClientsGridController">
                <h3>Risky Clients</h3>
                <table st-pipe="callServer" st-table="displayed"
                       class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <th colspan="5">
                            <a href="/dashboard/clients/risky/create" class="btn btn-info margin-bottom-20"><span
                                        class="fa fa-plus-circle"></span> Add Risky Client</a>
                        </th>
                        <th colspan="1">{!! Form::select('status', [null=>'All Status', 1=>'Due Diligence', 2=>'Risky', 3=>'Cleared'], null, ['class'=>'form-control margin-bottom-20', 'st-search'=>'status']) !!}</th>

                        <th colspan="1">
                            <input st-search="" class="form-control" placeholder="Search by Name..." type="text"/>
                        </th>
                        <th colspan="1">
                            <a href="/dashboard/clients/risky/risky-clients-report" type="button"
                               class="btn btn-success"><span class="fa fa-file-archive-o"></span>&nbsp;EXPORT</a>
                        </th>
                    </tr>
                    <tr>
                        <th st-sort="id">ID</th>
                        <th>Name/Organization</th>
                        <th>Email Address</th>
                        <th>Nationality</th>
                        <th>Flagged Date</th>
                        <th>Flagged By</th>
                        <th>Status</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>

                    <tbody ng-show="!isLoading">
                    <tr ng-repeat="row in displayed">
                        <td><% ($index+1) %></td>
                        <td><% row.name_or_organization %></td>
                        <td><% row.email %></td>
                        <td><% row.nationality %></td>
                        <td><% row.flagged_date %></td>
                        <td><% row.flagged_by %></td>
                        <td>
                            <span ng-if="row.risky_status_id == 1" class="label label-info">DUE DILIGENCE</span>
                            <span ng-if="row.risky_status_id == 2" class="label label-warning">RISKY</span>
                            <span ng-if="row.risky_status_id == 3" class="label label-success">CLEARED</span>
                        </td>
                        <td class="text-center">
                            <a href="/dashboard/client/risky/show/<% row.id %>"><i class="fa fa-eye"></i></a>
                            <a href="/dashboard/client/risky/edit/<% row.id %>"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                    </tbody>
                    <tbody ng-show="isLoading">
                    <tr>
                        <td colspan="8" class="text-center">Loading ...</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4" class="text-center">
                            Items per page <input type="text" ng-model="itemsByPage"/>
                        </td>
                        <td colspan="4" class="text-center">
                            <div st-pagination="" st-items-by-page="itemsByPage"
                                 st-template="pagination.custom.html"></div>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@stop