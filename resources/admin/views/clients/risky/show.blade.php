@extends('layouts.default')
@section('content')

    <div class="col-md-6">
        <div class="panel-dashboard">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Client Details</h4></div>
                <div class="panel-body">

                    <table class="table table-responsive table-striped">
                        <tbody>
                        @if($riskClient->type == 1)
                            <tr>
                                <td>Full Name:</td>
                                <td>{!! $riskClient->firstname. ' '. $riskClient->lastname  !!}</td>
                            </tr>
                        @else
                            <tr>
                                <td>Organization Name:</td>
                                <td>{!! $riskClient->organization  !!}</td>
                            </tr>

                        @endif
                        <tr>
                            <td>Email</td>
                            <td>{!! $riskClient->email !!}</td>
                        </tr>
                        <tr>
                            <td>Nationality</td>
                            <td>{!! \Cytonn\Presenters\CountryPresenter::present($riskClient->country_id) !!}</td>
                        </tr>

                        <tr>
                            <td>Date Flagged</td>
                            <td>{!! \Carbon\Carbon::parse($riskClient->date_flagged)->toFormattedDateString() !!}</td>
                        </tr>

                        <tr>
                            <td>Flagged By</td>
                            <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($riskClient->flagged_by) !!}</td>
                        </tr>

                        <tr>
                            <td>Affiliations/ Relationships</td>
                            <td>{!! $riskClient->affiliations !!}</td>
                        </tr>

                        <tr>
                            <td>Source Document</td>
                            <td>{!! $riskClient->risk_source !!}</td>
                        </tr>

                        <tr>
                            <td>Current Status</td>
                            <td>
                                @if($riskClient->risky_status_id == 1)
                                    <span class="label label-info">{{\Cytonn\Presenters\ClientPresenter::presentRiskyStatus($riskClient->risky_status_id)}}</span>
                                @elseif($riskClient->risky_status_id == 2)
                                    <span class="label label-warning">{{\Cytonn\Presenters\ClientPresenter::presentRiskyStatus($riskClient->risky_status_id)}}</span>
                                @elseif($riskClient->risky_status_id == 3)
                                    <span class="label label-success">{{\Cytonn\Presenters\ClientPresenter::presentRiskyStatus($riskClient->risky_status_id)}}</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Risk Reason</td>
                            <td>{!! $riskClient->reason !!}</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-5 pull-right">
        <div class="panel-dashboard">
            <h4>Risk Status Actions</h4>
            <div class="btn-group" role="group" aria-label="...">
                @if($riskClient->risky_status_id == 2)
                    <button type="button" class="disabled btn btn-warning">RISKY</button>
                @else
                    <button type="button" data-toggle="modal" data-target="#rejectModal" data-backdrop="static"
                            data-keyboard="false" class="btn btn-warning">RISKY
                    </button>
                @endif

                @if($riskClient->risky_status_id == 3)
                    <button type="button" class="disabled btn btn-success">CLEARED</button>
                @else
                    <button type="button" data-toggle="modal" data-target="#clearModal" data-backdrop="static"
                            data-keyboard="false" class="btn btn-success">CLEARED
                    </button>
                @endif

                <button type="button" data-toggle="modal" data-target="#removeModal" data-backdrop="static"
                        data-keyboard="false" class="btn btn-danger">REMOVE RISK
                </button>

            </div>

        </div>
    </div>


    {{--reject modal--}}
    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Rejection Form</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('reject_reason')}}">
                        {{@csrf_field()}}
                        <div class="form-group">
                            <label class="required-field">Risky Client Reason</label>
                            <textarea cols="5" class="form-control" name="reject_reason" id=""
                                      placeholder="Reason for labeling client details risky"></textarea>
                        </div>

                        <input type="hidden" name="risk_id" value="{{$riskClient->id}}">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning">MARK RISKY</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--clear modal--}}
    <div class="modal fade" id="clearModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Clearance Form</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('clear_reason')}}">
                        {{@csrf_field()}}
                        <div class="form-group">
                            <label class="required-field">Clearance Reason</label>
                            <textarea cols="5" class="form-control" id="" name="clear_reason"
                                      placeholder="Reason for labeling client details cleared"></textarea>
                        </div>

                        <input type="hidden" name="risk_id" value="{{$riskClient->id}}">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">MARK CLEARED</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--remove modal--}}
    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Deleting Risk Form</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <span class="fa fa-warning fa-5x"></span>
                        <br>
                        <p class="text-danger" style="font-size: 16px;">Are you sure you want to remove this risk from Risky-Clients List???</p>
                        <br>
                        <form method="post" action="{{route('delete_risk')}}">
                            {{@csrf_field()}}
                            <input type="hidden" name="risk_id" value="{{$riskClient->id}}">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">DELETE RISK</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop