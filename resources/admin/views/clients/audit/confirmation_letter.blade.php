<style>
    .left{
        width: 30%;
        display: inline-block;
        position: absolute;
        left: 0;
        z-index: 0;
    }
    .right{
        width: 30%;
        display: inline-block;
        position: absolute;
        left: 70%;
        z-index: 0;
    }
    .small_text {
        font-size: 12px;
    }
</style>

@extends('reports.letterhead')

@section('content')
    <div class="small_text">
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Partner Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif
    <p class="bold-underline">{!! strtoupper($spv.' '.$audit_date->format('Y').' Audit Confirmation') !!}</p>
    <div class="body">
        <p>
            Our auditors, Grant Thornton (GT), are now carrying out the annual audit of {{ $spv }}.
            As part of their audit procedures, we have been requested by the audit team to ask you to confirm directly to them the balance
            owed to you by {{ $product }} the details of which are as follows.
        </p>
        <p>
            Partner code: <b>{!! $client->client_code !!}</b> <br>
            Nature of Account: <b> {!! $product !!}</b> <br>
            Balance as at {!! $audit_date->toFormattedDateString() !!}:<b> {!! $currency !!} {!! \Cytonn\Presenters\AmountPresenter::currency($amount, true, 0) !!}</b>  <br>
        </p>
        <p>
            We shall be obliged if you can verify the balance shown above with your records as of {!! $audit_date->toFormattedDateString() !!} and
            indicate whether you are in agreement therewith by either:
            <ol type="1">
                <li>A direct reply on email to our auditors: <a href="mailto:william.kagai@ke.gt.com">william.kagai@ke.gt.com</a>, <a href="mailto:nelius.kuria@ke.gt.com">nelius.kuria@ke.gt.com</a> Grant
                    Thornton, P.O. 46986-00100, Nairobi, for the attention of Mr. William Kagai/ Ms Nelius Kuria</li>
                <li>Signing and sending a scanned copy of the attached confirmation letter to <a href="mailto:william.kagai@ke.gt.com">william.kagai@ke.gt.com</a>, <a href="mailto:nelius.kuria@ke.gt.com">nelius.kuria@ke.gt.com</a> Grant
                    Thornton, P.O. 46986-00100, Nairobi, for the attention of Mr. William Kagai/ Ms Nelius Kuria</li>
            </ol>
        </p>
    </div>
    <p>We highly appreciate your cooperation.</p>
    <p>Yours Sincerely,</p>
    <p class="bold"> For {{ $spv }}<br/>
        {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender->id) !!}
    </p>

    <p style="text-align: center">
        <strong>.................................................... Please do not detach ....................................................</strong>
    </p>
    <p>
        <span class="right">Date: ____________________</span>
        Grant Thornton <br>
        P.O. BOX 46986 - 00100 <br>
        Nairobi <br>
        Kenya
    </p>

    <p>
        We agree/not agree with the balances owed to use by {{ $spv }}

        I/We agree/not agree with the balances owed to us by {{ $spv }} and that the balance at {!! $audit_date->toFormattedDateString() !!}
        was _____________________ as per our records.
    </p>

    <p>Signed: ____________________________</p>

    <p>
        <strong><span class="left">Name</span></strong>
        <strong><span class="right">Designation</span></strong>
    </p>
    <br><br>
    <p>
        <span class="left">_____________________</span>
        <span class="right">____________________</span>
    </p>
    </div>
@stop