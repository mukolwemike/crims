<style>
    .left{
        width: 30%;
        display: inline-block;
        position: absolute;
        left: 0;
        z-index: 0;
    }
    .center{
        width: 30%;
        display: inline-block;
        position: absolute;
        left: 40%;
        z-index: 0;
    }
    .right{
        width: 30%;
        display: inline-block;
        position: absolute;
        left: 70%;
        z-index: 0;
    }
</style>

@extends('reports.letterhead')

@section('content')
    {!! date('j').'<sup>'.date('S').' </sup>'.date('F, Y') !!}<br/><br/>

    {!! Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->id) !!}<br/>

    @if($client->clientType->name == 'corporate')
        C/O {!! \Cytonn\Presenters\ClientPresenter::presentContactPerson($client->id) !!} <br/>
    @endif
    (Partner Code – {!! $client->client_code !!})<br/>
    {!! \Cytonn\Presenters\ClientPresenter::presentAddress($client->id) !!}

    @if($client->clientType->name == 'corporate')
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentContactPersonShortName($client->id) !!},
    @else
        Dear {!! \Cytonn\Presenters\ClientPresenter::presentSalutation($client->id)!!},
    @endif
    <p class="bold-underline">{!! strtoupper($product.' '.$audit_date->format('Y').' Audit Confirmation') !!}</p>
    <div class="body">
        <p>
            Our auditors, Grant Thornton, are now engaged on their annual audit. As part of their normal audit procedures
            we have been requested for you to confirm directly to them the amount paid to us as at
            {{ \Cytonn\Presenters\DatePresenter::formatDate($audit_date) }} for the purchase of a unit in {{$product}}.
        </p>
        <p>
            <b>Development : </b>  <span>{{ $product }}</span><br>
            <b>House Number : </b>  <span>{{ $house_number }}</span><br>
            <b>Amount as at {{ \Cytonn\Presenters\DatePresenter::formatDate($audit_date) }} : </b> <span>__________________</span><br>
        </p>
        <p>
            Please return this letter to our auditors, Email: <a href="mailto:jane.wanjira@ke.gt.com">jane.wanjira@ke.gt.com</a>/ <a href="mailto:tejal.bharadva@ke.gt.com">tejal.bharadva@ke.gt.com</a>
            for the attention of Mrs. Jane Wanjira/ Mrs. Tejal Bharadva.
        </p>
    </div>
    <p>Your co-operation in this matter is highly appreciated.</p>

    <p>Yours Sincerely,</p>
    <p class="bold">
        {!! \Cytonn\Presenters\UserPresenter::presentLetterClosing($sender->id) !!}
    </p>

    <p>
        <strong><span class="left">Name</span></strong>
        <strong><span class="center">Designation</span></strong>
        <strong><span class="right">Official Stamp</span></strong>
    </p>
    <br><br>
    <p>
        <span class="left">_____________________________</span>
        <span class="center">____________________</span>
        <span class="right">____________________</span>
    </p>
    <br>
    </div>
@stop