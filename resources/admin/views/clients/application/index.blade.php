@extends('layouts.default')

@section('content')
    <div class = "col-md-12">
        @if(isset($errors))
            @if(count($errors))
                <div class="alert alert-danger">
                    <ol>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ol>
                </div>
            @endif
        @endif

        <client-application
                :edit="{{ json_encode($edit) }}"
                :type="{{ json_encode($type) }}"
                :application="{{ json_encode($applicationId) }}"
                :mandates="{{ json_encode($signingMandates) }}">

        </client-application>
    </div>
@stop