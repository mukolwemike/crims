<?php
    $app = $application;
?>

@extends('layouts.default')

@section('content')
    <div class="col-md-12" ng-controller = "ClientApplicationDetailCtrl">
        <div class="panel-dashboard">
            <div class="col-md-6">
                <div class="col-md-12  panel panel-success">
                    <br>
                    <h5>INVESTMENT DETAILS</h5>
                    <hr>
                    @include('clients.application.partials.investment')
                </div>

                <div class="col-md-12  panel panel-success">
                    <br>
                    <h5>APPLICATION DETAILS</h5>
                    <hr>
                    @include('clients.application.partials.application')
                </div>
                <div class="col-md-12  panel panel-success">
                    <br>
                    <h5>SOURCE OF FUNDS</h5>
                    <hr>
                    @include('clients.application.partials.source')
                </div>

                <div class="col-md-12  panel panel-success">
                    <br>
                    <h5>INVESTOR BANK INFORMATION</h5>
                    <hr>
                    @include('clients.application.partials.bank')
                </div>
            </div>

            <div class="col-md-6">
                <div>
                    <div class="pull-right">

                        @if(!$app->filled)
                            <a href="/dashboard/investment/client/application/{{ $app->id }}/edit" class="btn btn-success">
                                <i class="fa fa-edit"></i>
                                Update
                            </a>
                        @endif

                        <a href="/dashboard/investments/client-instructions" class="btn btn-default">
                            <i class="fa fa-list-alt"></i>
                            Back to Instructions
                        </a>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="row">
                    @if($app->kin_phone || $contactPersons)
                    <div class="col-md-12">
                        <div class="">
                            <el-collapse>
                                <el-collapse-item title="CONTACT PERSONS" name="CONTACT PERSONS">
                                    <div class="panel panel-success">
                                        @include('clients.application.partials.contact-persons')
                                    </div>
                                </el-collapse-item>
                            </el-collapse>
                        </div>
                    </div>
                    @endif

                    <div class="col-md-12">
                        <div class="">
                            <el-collapse>
                                <el-collapse-item title="DOCUMENTS" name="DOCUMENTS">
                                    @include('clients.application.partials.documents')
                                </el-collapse-item>
                            </el-collapse>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="">
                            <el-collapse>
                                <el-collapse-item title="JOINT HOLDERS" name="JOINT HOLDERS">
                                    <div class="panel panel-success">
                                        @include('clients.application.partials.joint-holder')
                                    </div>
                                </el-collapse-item>
                            </el-collapse>
                        </div>
                    </div>

                    {{--<div class="col-md-12">--}}
                        {{--<div class="detail-group">--}}
                            {{--<br>--}}
                            {{--@include('clients.application.partials.progress')--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <div class = "col-md-12">
                        <div class="detail-group">
                            <div class="col-md-12">
                                @if($duplicate && $duplicate->clients->count() > 0)
                                    <div class="alert alert-warning" role="alert">
                                        <p class="text-warning">The client matched the following duplicate records</p>
                                    </div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Code</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($duplicate->clients as $client)
                                            <tr>
                                                <td>
                                                    <a href="/dashboard/clients/details/{{ $client->id }}">
                                                        {!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}
                                                    </a>
                                                </td>
                                                <td>{!! $client->client_code !!}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <br>
                                    <div class="alert alert-info">
                                        <p class="text-info"><strong>Reason for Duplication:</strong></p>
                                        <p class="text-capitalize">{{(!is_null($app->duplicate_reason) ? $app->duplicate_reason : 'No reason given')}}</p>
                                    </div>
                                @endif
                                    <hr>
                                @if(!is_null($riskyClient))
                                    <div class="alert alert-warning" role="alert">
                                        <p class="text-warning">The Client matched the following Risk Client
                                            Records</p>
                                    </div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Name/Organization</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <a target="_blank" href="/dashboard/client/risky/show/{{$riskyClient->id}}">
                                                    @if(is_null($riskyClient->firstname)) {!! $riskyClient->organization  !!} @else {!! $riskyClient->firstname. ' '. $riskyClient->lastname !!} @endif
                                                </a>
                                            </td>
                                            <td>{!! $riskyClient->email !!}</td>
                                            <td>
                                                @if($riskyClient->risky_status_id === 1)<span class="label label-warning">INVESTIGATING</span>
                                                @elseif($riskyClient->risky_status_id === 2)<span class="label label-danger">RISKY</span>
                                                @elseif($riskyClient->risky_status_id === 3)<span class="label label-success">CLEARED</span>@endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <div class="alert alert-info">
                                        <p class="text-info"><strong>Reason for Risky Application:</strong></p>
                                        <p class="text-capitalize">{{(!is_null($app->risk_reason) ? $app->risk_reason : 'Reason Not Given')}}</p>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <br/>
                                <a href="/dashboard/investments/client-instructions/application/{!! $app->id !!}/pdf" class="btn btn-primary" target="_blank"><i class="fa fa-file-pdf-o"></i> View PDF</a>
                                @if($app->filled)
                                    <div class="alert alert-warning"><p>This application has already been used</p></div>
                                @else
                                    <a href="/dashboard/investments/client-instructions/application/{!! $application->id !!}/process" class="btn btn-success">Process Application</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @stop
