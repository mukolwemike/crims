<?php
$app = $application;
?>
<style>
    .detail-group{
        border: 1px solid #e3e3e3;
        border-radius: 5px;
        padding: 10px 15px;
        margin-bottom: 10px;
        width: 100%;
    }
    .detail-group:after{
        clear: both;
    }
    .full-width{
        display: block;
        width: 100%;
    }
    .half-width{
        width: 50%;
        border: 1px solid red;
        position: relative;
    }
    table{
        width: 100%;
    }
    td{
        padding: 10px 0;
    }
</style>


<div>

    <div class = "detail-group">
        <div>
            <h4 class = "full-width">Investment Details</h4>
        </div>

        <table>
            <tbody>
                <tr>
                    <td>
                        @if($product) Product: {!! @$product->name !!}
                        @elseif($unitFund) Unit Fund: {!! $unitFund->name !!}
                        @endif
                    </td>
                    <td>Total Commitment: {!! \Cytonn\Presenters\AmountPresenter::currency($app->amount) !!}</td>
                </tr>
                <tr>
                    <td>Partnership A/C Number: {!! $app->partnership_account_number !!}</td>
                    <td>@if($product) Agreed rate of return: {!! $app->agreed_rate.'%'!!} @endif</td>
                </tr>
                <tr>
                    <td>Investment Date: {!! Cytonn\Presenters\DatePresenter::formatDate($app->updated_at) !!}</td>
                    <td>@if($product) Tenor: {!! $app->tenor !!} Months @endif</td>
                </tr>
            </tbody>
        </table>
    </div>


    @if($app->individual == 1)
        <div class="detail-group">
            <h4 class="full-width">Applicant Details</h4>

            <table>
                <tbody>
                    <tr><td>Name:  {!! @$titles->name.' '.$app->firstname.' '.$app->middlename.' '.$app->lastname !!}</td><td></td></tr>
                    <tr><td>Email:  {!!$app->email !!}</td><td>Phone: {!! $app->telephone_home !!}</td></tr>
                    <tr><td>Gender:  {!!Cytonn\Presenters\GenderPresenter::presentAbbr($app->gender_id) !!}</td><td>Date of Birth:  {!!Cytonn\Presenters\DatePresenter::formatDate($app->dob) !!}</td></tr>
                    <tr><td>Pin No.: {!! $app->pin_no !!}</td><td>ID/Passport Number: {!! $app->id_or_passport !!}</td></tr>
                    <tr><td>Postal code: {!! $app->postal_code !!}</td><td>Postal address: {!! $app->postal_address !!}</td></tr>
                    <tr><td>Country: {!! Cytonn\Presenters\CountryPresenter::present($app->country_id) !!}</td><td>Home Telephone no.: {!!$app->telephone_home!!}</td></tr>
                    <tr><td>Office Telephone no.: {!! $app->telephone_office !!}</td><td>Cell no.: {!! $app->telephone_cell !!}</td></tr>
                    <tr><td>Residential Address: {!! $app->residence !!}</td><td>Town {!! $app->town !!}</td></tr>
                    <tr><td>Email Indemnity: {!! Cytonn\Presenters\BooleanPresenter::presentYesNo($app->email_indemnity) !!}</td></tr>
                </tbody>
            </table>

        </div>
        <div class="detail-group">
            <h4 class="full-width">Employment Details</h4>

            <table>
                <tbody>
                    <tr><td>Employment: {!! @$employments->name !!} </td><td>Details: {!! $app->employment_other !!}</td></tr>
                    <tr><td>Sector: {!! $app->business_sector !!}</td><td>Present Occupation: {!!$app->present_occupation!!}</td></tr>
                    <tr><td>Employer Name: {!! $app->employer_name !!}</td><td>Employer Address: {!! $app->employer_address !!}</td></tr>
                    <tr><td>Preferred method of Contact: {!! Cytonn\Presenters\ContactMethodPresenter::present($app->method_of_contact_id) !!}</td></tr>
                </tbody>
            </table>
        </div>
        <div class="detail-group">
            <h4 class="full-width">Contact Person</h4>

            <table>
                <tbody>
                    <tr><td>Name: {!! $app->kin_name !!}</td><td>Email: {!! $app->kin_email !!}</td></tr>
                    <tr><td>Phone: {!! $app->kin_phone !!}</td><td>Address: {!! $app->kin_postal !!}</td></tr>
                </tbody>
            </table>
        </div>
        @if($jointHolders)
            <div class="detail-group">
                <h4 class = "full-width">Joint Subscribers</h4>
                <table class = "table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Phone Number</th>
                        <th>Pin Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($jointHolders as $holder)
                        <tr>
                            <td><a href="/dashboard/clients/joint/{!! $holder->id !!}">{!! ucfirst($holder->firstname) !!} {!! ucfirst($holder->lastname) !!}</a></td>
                            <td>{!! $holder->email !!}</td>
                            <td>{!! $holder->telephone_cell !!}</td>
                            <td>{!! $holder->pin_no !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
        @endif
    @else
        <div class="detail-group">
            <h4 class="full-width">Type of Investor</h4>

            <table>
                <tbody>
                    <tr><td>Type of Investor: {!! @$corporateinvestorType->name !!}</td><td>Details: {!! $app->corporate_investor_type_other !!}</td></tr>
                </tbody>
            </table>
        </div>

        <div class="detail-group">
            <h4 class="full-width">Investor Details</h4>

            <table>
                <tbody>
                    <tr><td>Registered name: {!! $app->registered_name !!}</td><td>Trade name: {!! $app->trade_name !!}</td></tr>
                    <tr><td>Registered address: {!! $app->registered_address !!}</td><td>Registration number: {!!  $app->registration_number !!}</td></tr>
                    <tr><td>Telephone number: {!! $app->telephone_office !!}</td><td>Email: {!! $app->email !!}</td></tr>
                    <tr><td>Company pin: {!! $app->pin_no !!}</td><td>Physical Address: {!! $app->street !!}</td></tr>
                    <tr><td>Preferred method of contact: {!! Cytonn\Presenters\ContactMethodPresenter::present($app->method_of_contact_id) !!}</td></tr>
                </tbody>
            </table>
        </div>

    @endif

    <div class = "detail-group">
        <h4 class = "full-width">Source of Funds</h4>
        <table>
            <tbody>
                <tr><td>Source of Funds: {!! @$fundSource->name !!}</td><td>Details: {!! $app->funds_source_other !!}</td></tr>
            </tbody>
        </table>
    </div>

    <div class = "detail-group">
        <h4 class = "full-width">Investor Bank information</h4>

        <table>
            <tbody>
                <tr><td>Account name: {!! $app->investor_account_name !!}</td><td>Account number: {!! $app->investor_account_number !!}</td></tr>
                <tr><td>Bank: {!! $app->investor_bank !!}</td><td>Branch: {!! $app->investor_bank_branch !!}</td></tr>
                <tr><td>Clearing Code: {!! $app->investor_clearing_code !!}</td><td>Swift code: {!! $app->investor_swift_code !!}</td></tr>
            </tbody>
        </table>
    </div>
</div>
