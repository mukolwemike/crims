<h5>APPLICATION PROGRESS</h5>
<table class="table table-responsive table-striped">
    <tbody>
    <tr>
        <th>Investment Details</th>
        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($app->progress_investment) !!}</td>
    </tr>

    <tr>
        <th>Subscriber Details</th>
        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($app->progress_subscriber) !!}</td>
    </tr>

    <tr>
        <th>KYC Documents</th>
        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($app->progress_kyc) !!}</td>
    </tr>

    <tr>
        <th>Proof of payment</th>
        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($app->progress_payment) !!}</td>
    </tr>

    <tr>
        <th>Signing Mandate</th>
        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($app->progress_mandate) !!}</td>
    </tr>
    </tbody>
</table>