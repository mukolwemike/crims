<table class="table table-responsive table-striped">
    <tbody>
    <tr>
        @if($product)
            <th width="40%">Product</th>
            <td>{!! @$product->name !!}</td>
        @elseif($unitFund)
            <th width="40%">Unit Fund</th>
            <td>{!! @$unitFund->name !!}</td>
        @endif
    </tr>
    <tr>
        <th>Total Commitment</th>
        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($application->amount) }}</td>
    </tr>
    <tr>
        <th>Partnership Account Number</th>
        <td>{!! $app->partnership_account_number !!}</td>
    </tr>

    @if($product)
        <tr>
            <th>Agreed rate of return</th>
            <td>{!! $app->agreed_rate.'%'!!}</td>
        </tr>

        <tr>
            <th>Tenor</th>
            <td>{!! $app->tenor !!} Months</td>
        </tr>
    @endif

    <tr>
        <th>Email Indemnity</th>
        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($emailIndemnity) !!}</td>
    </tr>

    <tr>
        <th>Signing Mandate</th>
        <td>
            <el-tag size="small">
                {!! \Cytonn\Presenters\ClientPresenter::signingMandate($app['signing_mandate']) !!}
            </el-tag>
        </td>
    </tr>

    </tbody>
</table>