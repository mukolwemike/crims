<table class="table table-responsive table-striped">
    <tbody>
        <tr>
            <th>Account Name</th>
            <td>{!! $app->investor_account_name !!}</td>
        </tr>

        <tr>
            <th>Account Number</th>
            <td>{!! $app->investor_account_number !!}</td>
        </tr>

        <tr>
            <th>Bank</th>
            <td>@if($app->bank) {!! $app->bank->name !!} @endif</td>
        </tr>

        <tr>
            <th>Branch</th>
            <td>@if($app->branch) {!! $app->branch->name !!} @endif</td>
        </tr>
    </tbody>
</table>