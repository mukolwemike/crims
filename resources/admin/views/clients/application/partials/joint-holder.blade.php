<table class="table table-striped table-responsive">
    <thead>
    <tr>
        <th>Name</th>
        <th>Email Address</th>
        <th>Phone Number</th>
        <th>Type</th>
        <th>Reason</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($jointHolders as $holder)
        <tr>
            <td>
                <a href="/dashboard/clients/joint/{!! $holder->id !!}">{!! ucfirst($holder->firstname) !!} {!! ucfirst($holder->lastname) !!}</a>
            </td>
            <td>{!! $holder->email !!}</td>
            <td>{!! $holder->telephone_cell !!}</td>
            <td>
                @if($holder->holder_type == 3) <span class="label label-primary">DUPLICATE HOLDER</span>
                @elseif($holder->holder_type == 2) <span class="label label-info">EXISTING CLIENT</span>
                @else <span class="label label-default">NEW HOLDER</span>@endif
            </td>
            <td>
                @if($holder->holder_type == 3){!! strtolower($holder->duplicate_reason)!!}@endif
            </td>
            <td>
                <a href="#" data-toggle="modal" data-target="#edit-joint-holder-{!! $holder->id !!}"
                   title="Edit Joint Holder"><i class="fa fa-pencil-square-o"></i></a>
                <!-- Edit Modal -->
                <div class="modal fade" id="edit-joint-holder-{!! $holder->id !!}" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open(['route'=>['edit_joint_holder', $app->id, $holder->id]]) !!}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Joint Holder</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form">
                                    {!! Form::hidden('holder_id', $holder->id, null) !!}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                                    <span class="">{!! Form::label('title_id', 'Full name') !!}
                                                                        <br/>
                                                                        {!! Form::select('title_id', $clientTitles , $holder->title, ['class'=>'form-control', 'required']) !!}
                                                                        {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'title') !!}
                                                                        <div class="clearfix"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <span class="required-field">{!! Form::label('lastname', 'Last Name') !!}</span><br/>
                                                {!! Form::text('lastname', $holder->lastname, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'lastname') !!}
                                            </div>

                                            <div class="col-md-4 ">
                                                <span class="required-field">{!! Form::label('firstname', 'First Name') !!}</span><br/>
                                                {!! Form::text('firstname', $holder->firstname, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'firstname') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <span class="">{!! Form::label('middlename', 'Middle Name') !!}</span><br/>
                                                {!! Form::text('middlename', $holder->middlename, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'middlename') !!}
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <span class="">{!! Form::label('gender_id', 'Gender') !!}</span><br/>
                                                {!! Form::select('gender_id', $gender , $holder->gender, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'gender') !!}
                                            </div>

                                            <div class="col-md-4" ng-controller="DatepickerCtrl">
                                                <span class="">{!! Form::label('dob', 'Date of Birth') !!}</span><br/>
                                                {!! Form::text('dob', $holder->dob, ['class'=>'form-control', 'datepicker-popup init-model'=>"dob", 'is-open'=>"status.opened", 'ng-focus'=>'open($event)', 'ng-required'=>"true"]) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'dob') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <span class="required-field">{!! Form::label('telephone_cell', 'Phone Number') !!}</span><br/>
                                                {!! Form::text('telephone_cell', $holder->telephone_cell, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'telephone_cell') !!}
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <span class="required-field">{!! Form::label('pin_no', 'Pin Number') !!}</span><br/>
                                                {!! Form::text('pin_no', $holder->pin_no, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'pin_no') !!}
                                            </div>

                                            <div class="col-md-4 ">
                                                <span class="required-field">{!! Form::label('id_or_passport', 'ID/ Passport') !!}</span><br/>
                                                {!! Form::text('id_or_passport', $holder->id_or_passport, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'id_or_passport') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <span class="required-field">{!! Form::label('email', 'Email Address') !!}</span><br/>
                                                {!! Form::text('email', $holder->email, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'email') !!}
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <span class="">{!! Form::label('postal_code', 'Postal Code') !!}</span><br/>
                                                {!! Form::text('postal_code', $holder->postal_code, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_code') !!}
                                            </div>

                                            <div class="col-md-4 ">
                                                <span class="">{!! Form::label('postal_address', 'Postal Address') !!}</span><br/>
                                                {!! Form::text('postal_address', $holder->postal_address, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'postal_address') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <span class="">{!! Form::label('residential_address', 'Residential Address') !!}</span><br/>
                                                {!! Form::text('residential_address', $holder->residential_address, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'residential_address') !!}
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <span class="">{!! Form::label('country_id', 'Country') !!}</span><br/>
                                                {!! Form::select('country_id', $countries , $holder->country_id, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'country_id') !!}
                                            </div>

                                            <div class="col-md-4">
                                                <span class="">{!! Form::label('nationality_id', 'Nationality') !!}</span><br/>
                                                {!! Form::select('nationality_id', $countries , $holder->nationality_id, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'nationality_id') !!}
                                            </div>

                                            <div class="col-md-4 ">
                                                <span class="">{!! Form::label('town', 'Town') !!}</span><br/>
                                                {!! Form::text('town', $holder->town, ['class'=>'form-control', 'required']) !!}
                                                {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'town') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                {!! Form::submit('Update', ['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>