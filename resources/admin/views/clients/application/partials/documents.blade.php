<div class="">
    @if($kycDocuments)
        <hr>
        <h5>KYC Documents</h5>
        <div class="panel panel-success">
            <table class="table table-striped table-responsive">
                <tbody>
                @foreach($kycDocuments as $kyc)
                    <tr>

                        <td>
                            @if(isset($kyc['kyc_type']))
                                {{ ucwords(str_replace(['-', '_'], ' ', $kyc['kyc_type'])) }}
                            @elseif(isset($kyc['slug']))
                                {{ ucwords(str_replace(['-', '_'], ' ', $kyc['slug'])) }}
                            @endif

                        </td>
                        <td>
                            <a href="/dashboard/investments/client-instructions/filled-application-documents/{!! $kyc['document_id'] !!}">
                                <i class="el-icon-view"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif

    @if($paymentDocuments)
        <hr>
        <h5>Payment Documents</h5>
        <div class="panel panel-success">
            <table class="table table-striped table-responsive">
                <tbody>
                @foreach($paymentDocuments as $kyc)
                    <tr>
                        <td>
                            @if(isset($kyc['kyc_type']))
                                {{ ucwords(str_replace(['-', '_'], ' ', $kyc['kyc_type'])) }}
                            @elseif(isset($kyc['slug']))
                                {{ ucwords(str_replace(['-', '_'], ' ', $kyc['slug'])) }}
                            @endif
                        </td>
                        <td>
                            <a href="/dashboard/investments/client-instructions/filled-application-documents/{!! $kyc['document_id'] !!}">
                                <i class="el-icon-view"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif

        @if($emailIndemnity)
            <hr>
            <h5>Email Indemnity</h5>
            <div class="panel panel-success">
                <table class="table table-striped table-responsive">
                    <tbody>
                    @foreach($emailIndemnity as $indemnity)
                        <tr>
                            <td> {{ $indemnity['email'] }} </td>
                            <td> {{ \Cytonn\Core\DataStructures\Carbon::parse($indemnity['date'])->toFormattedDateString() }} </td>
                            <td> {{ $indemnity['kyc_type'] }} </td>
                            <td>
                                <a href="/dashboard/investments/client-instructions/filled-application-documents/{!! $indemnity['document_id'] !!} ">
                                    <i class="el-icon-view"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

    @if($clientSignature)
        <hr>
        <h5>Client Signature</h5>
        <div class="panel panel-success">
            <table class="table table-striped table-responsive">
                <tbody>
                @foreach($clientSignature as $signature)
                    <tr>
                        <td> {{ $signature['name'] }} </td>
                        <td>
                            <a href="/dashboard/investments/client-instructions/filled-application-documents/{!! $signature['document_id'] !!}">
                                <i class="el-icon-view"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
