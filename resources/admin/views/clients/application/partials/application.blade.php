<table class="table table-responsive table-striped">
    <tbody>
    @if($app->individual == 1)

        <tr>
            <th>Name</th>
            @if($client)
                <td>{{ $client->name() }}</td>
            @else
                <td>{!! @$titles->name .' '.$app->firstname.' '.$app->middlename.' '.$app->lastname !!}</td>
            @endif
        </tr>

        <tr>
            <th>Email</th>
            <td>{!!$app->email !!}</td>
        </tr>

        @if($app->telephone_home)
        <tr>
            <th>Phone</th>
            <td>{!! $app->telephone_home !!}</td>
        </tr>
        @endif

        <tr>
            <th>Gender</th>
            <td>{!!Cytonn\Presenters\GenderPresenter::presentAbbr($app->gender_id) !!}</td>
        </tr>

        <tr>
            <th>Date of Birth</th>
            <td>{!!Cytonn\Presenters\DatePresenter::formatDate($app->dob) !!}</td>
        </tr>

        <tr>
            <th>Pin No.</th>
            <td>{!! $app->pin_no !!}</td>
        </tr>

        <tr>
            <th>ID or Passport</th>
            <td>{!! $app->id_or_passport !!}</td>
        </tr>

        <tr>
            <th>Postal code</th>
            <td>{!! $app->postal_code !!}</td>
        </tr>

        <tr>
            <th>Postal address</th>
            <td>{!! $app->postal_address !!}</td>
        </tr>

        <tr>
            <th>Country</th>
            <td>{!! Cytonn\Presenters\CountryPresenter::present($app->country_id) !!}</td>
        </tr>

        <tr>
            <th>Nationality</th>
            <td>{!! Cytonn\Presenters\CountryPresenter::present($app->nationality_id) !!}</td>
        </tr>

        @if($app->telephone_office)
        <tr>
            <th>Office Telephone no.</th>
            <td>{!! $app->telephone_office !!}</td>
        </tr>
        @endif

        {{--<tr>--}}
        {{--<th>Cell no.</th>--}}
        {{--<td>{!! $app->telephone_cell !!}</td>--}}
        {{--</tr>--}}

        {{--<tr>--}}
        {{--<th>Residential Address</th>--}}
        {{--<td>{!! $app->residence !!}</td>--}}
        {{--</tr>--}}

        <tr>
            <th>Town</th>
            <td>{!! $app->town !!}</td>
        </tr>

        <tr>
            <th>Franchise</th>
            <td>{!! Cytonn\Presenters\BooleanPresenter::presentIcon($app->franchise) !!}</td>
        </tr>

    @else

        <tr>
            <th>Type of Investor</th>
            <td>{!! $investortype ? $investortype->name : '' !!}</td>
        </tr>

        @if(!is_null($app->corporate_investor_type_other))
            <tr>
                <th>Other Details</th>
                <td>{!! $app->corporate_investor_type_other !!}</td>
            </tr>
        @endif
        <tr>
            <th>Registered name</th>
            <td>{!! $app->registered_name !!}</td>
        </tr>

        <tr>
            <th>Trade name</th>
            <td>{!! $app->trade_name !!}</td>
        </tr>

        <tr>
            <th>Registered address</th>
            <td>{!! $app->registered_address !!}</td>
        </tr>

        <tr>
            <th>Registration number</th>
            <td>{!! $app->registration_number !!}</td>
        </tr>

        <tr>
            <th>Telephone number</th>
            <td>{!! $app->telephone_office !!}</td>
        </tr>

        <tr>
            <th>Email</th>
            <td>{!! $app->email !!}</td>
        </tr>
        @if(!is_null($app->pin_no))
            <tr>
                <th>Company pin</th>
                <td>{!! $app->pin_no !!}</td>
            </tr>
        @endif
        <tr>
            <th>Physical Address</th>
            <td>{!! $app->street !!}</td>
        </tr>

        <tr>
            <th>Preferred method of contact</th>
            <td>{!! Cytonn\Presenters\ContactMethodPresenter::present($app->method_of_contact_id) !!}</td>
        </tr>

        <tr>
            <th>Franchise</th>
            <td>{!! Cytonn\Presenters\BooleanPresenter::presentIcon($app->franchise) !!}</td>
        </tr>

    @endif
    </tbody>
</table>