<table class="table table-responsive table-striped">
    <tbody>
        <tr>
            <th>Source of Funds</th>
            <td>@if($fundsource){!! $fundsource->name !!} @else @endif</td>
        </tr>
        <tr>
            <th>Other Details</th>
            <td>@if($fundsource){!! $app->funds_source_other !!} @else @endif</td>
        </tr>
    </tbody>
</table>