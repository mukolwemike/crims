<table class = "table table-striped table-responsive">
    <table class = "table table-striped table-responsive">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Phone Number</th>
            </tr>
        </thead>

        <tbody>
        @if(isset($app->form->kin_phone))
            <tr>
                <td>{!! $app->form->kin_name !!}</td>
                <td>{!! $app->form->kin_email !!}</td>
                <td>{!! $app->form->kin_phone !!}</td>
                <td>{!! $app->form->kin_postal !!}</td>
            </tr>
        @endif

        @if($contactPersons)
            @foreach($contactPersons as $persons)
                <tr>
                    <td>{!! $persons->name !!}</td>
                    <td>{!! $persons->email !!}</td>
                    <td>{!! $persons->address !!}</td>
                    <td>{!! $persons->phone !!}</td>
                </tr>
            @endforeach
        @endif
</tbody>
</table>
</table>