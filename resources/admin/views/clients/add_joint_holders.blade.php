@extends('layouts.default')

@section('content')
    <div class = "col-md-8 col-md-offset-2">

        <div class = "col-md-12 panel panel-dashboard form-grouping">
            <div>
                {!! Form::open(['route'=>['add_client_joint_holder', $client->id]]) !!}

                <add-joint-holder name="holders"></add-joint-holder>

                <div class="detail-group">
                    <div class="col-md-12 margin-top-20 margin-bottom-20">
                        {!! Form::submit('Submit', ['class'=>'btn btn-success']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop