@extends('layouts.default')

@section('content')
    <div class="panel-dashboard" ng-controller="approveCmsClientTxCtrl">
        <h3>Approval Request Details</h3>

        <div>
            <table class="table table-hover table-striped table-responsive">
                <thead>

                </thead>
                <tbody>
                @if($approval->client_id)
                    <tr>
                        <td>Client code</td>
                        <td>{!! $approval->client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($approval->client_id) !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Sent by</td>
                    <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($approval->sent_by) !!}</td>
                </tr>
                <tr>
                    <td>Sent on</td>
                    <td>{!! $approval->created_at->toRfc850String() !!}</td>
                </tr>
                <tr>
                    <td>Transaction type</td>
                    <td>{!! ucfirst(str_replace('_', ' ', $approval->transaction_type)) !!}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        @if($approval->approved)
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <td>Stage</td>
                                    <td>Status</td>
                                    <td>Approved By</td>
                                    <td>Approved On</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($approval->steps as $step)
                                    <tr>
                                        <td>{{ $step->stage->name }}</td>
                                        <td><span class="label label-success">Approved</span></td>
                                        @if($step->approver)
                                            <td>{{ \Cytonn\Presenters\UserPresenter::presentFullNames($step->approver->id)}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{ $step->created_at->toRfc850String() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <td>Stage</td>
                                    <td>Status</td>
                                    <td>Approved By</td>
                                    <td>Approved On</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($approval->type()->allStages() as $approvalStage)
                                    <tr>
                                        <?php $step = $approved_step($approval, $approvalStage); ?>
                                        <td>{{ $approvalStage->name }}</td>
                                        <td>@if($step) <span class="label label-success">Approved</span>@else <span
                                                    class="label label-danger">Awaiting Approval</span> @endif</td>
                                        <td>@if($step) {{ \Cytonn\Presenters\UserPresenter::presentFullNames($step->approver->id) }} @else
                                                - @endif</td>
                                        <td>@if($step) {{ $step->created_at->toRfc850String() }} @else - @endif</td>
                                    </tr>
                                @endforeach
                                @foreach($rejections as $rejection)
                                    <tr>
                                        <td>Rejected</td>
                                        <td>
                                            By: {{ \Cytonn\Presenters\UserPresenter::presentFullNames($rejection->reject_by) }}</td>
                                        <td colspan="2">
                                            {{ $rejection->reason }}
                                            <a href="/dashboard/investments/approve/{{ $approval->id }}/resolve/{{ $rejection->id }}"
                                               class="btn btn-info pull-right">Resolve</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </td>
                </tr>
                @if($approval->scheduled == '1')
                    <tr>
                        <td>Scheduled</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        @if($approval->transaction_type == 'rollover')
                            <td>Execution date</td>
                            {{--<td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($findInvestment($approval->payload['investment'])->maturity_date) !!}</td>--}}
                            <td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($findInvestment->maturity_date) !!}</td>
                        @elseif($approval->transaction_type == 'withdraw')
                            @if($approval->payload['premature'] == '0')
                                <td>Execution date</td>
                                {{--                                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($findInvestment($approval->payload['investment_id'])->maturity_date) !!}</td>--}}
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($findInvestment->maturity_date) !!}</td>
                            @elseif($approval->payload['premature'] == '1')
                                <td>Execution date</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($approval->payload['end_date']) !!}</td>
                            @endif
                        @endif
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

        @if(View::exists($view_folder.'.'.$approval->transaction_type))

            @include($view_folder.'.'.$approval->transaction_type)

            @if(count($approval->approvalDocuments))
                @include('clients.approvals.partials.approval_documents')
            @endif

            @include('clients.approvals.partials.actions')
        @else
            <div>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4>Unsupported Transaction</h4>
                    </div>
                    <div class="panel-body">
                        <div class="well-lg">
                            <div class="alert alert-danger"><p>Transaction type is not yet supported</p></div>
                        </div>
                        <div class="panel-footer" ng-hide="showedit">
                            <a disabled href="#" class="btn btn-success disabled">Approve</a>
                            <a href="/dashboard/investments/approvalremove/{!! $approval->id !!}"
                               class="btn btn-danger pull-right">Remove Request</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
