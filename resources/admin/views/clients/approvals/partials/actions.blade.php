<div class="panel panel-default">
    <div class="panel-footer">
        @if($approval->approved)
            <a disabled href="#" class="btn btn-success disabled">Already Approved</a>
            <a href="#" class="btn disabled btn-danger pull-right">Remove Request</a>
        @else
            <div ng-controller="ApprovalActionCtrl">
                @if($rejections->count() > 0)
                    <a class="btn btn-info" href="#">Resolve the issues above first!</a>
                @else
                    <a href="/dashboard/investments/approve/{!! $approval->id !!}/stage/{{ $stage->id }}" class="btn btn-success">Approve</a>
                @endif
                <button class="btn btn-danger pull-right" data-toggle="modal" data-target="#remove-request">Remove Request</button>
            </div>
        @endif
        <div class="clearfix"></div>
    </div>
</div>

<!-- Remove Request Confirm Modal -->
<div class="modal fade" id="remove-request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            {!! Form::open(['url'=>'dashboard/investments/approvalremove/' . $approval->id, 'method'=>'GET']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Remove Request</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Reason</label>
                    {!! Form::textarea('reason', null, ['class'=>'form-control', 'rows'=>3, 'required']) !!}
                </div>
                <div class="form-group">
                    <label>Type <span class="label label-default">delete</span> and submit to remove this request.</label>
                    {!! Form::text('confirm', null, ['class'=>'form-control', 'required']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                {!! Form::submit('Remove', ['class'=>'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>