<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Approval Documents</h4>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Description</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($approval->approvalDocuments as $doc)
                <tr>
                    <td>{{$doc->name}}</td>
                    <td><a target="_blank" href="/document/view-document/{{$doc->document_id}}"><i
                                    class="fa fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>