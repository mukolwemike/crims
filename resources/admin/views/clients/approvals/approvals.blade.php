@extends('layouts.default')

@section('content')
    <div class="panel-dashboard" ng-controller="investmentApprovalsGridCtrl">
        <h3>Client Transactions</h3>
        <a class="btn btn-success pull-right" href="/dashboard/investments/approvals/batches">Batch Approvals <i class="fa fa-angle-double-right"></i></a>
        <table st-pipe="callServer" st-table="displayed" class="table table-responsive table-hover table-striped">
            <thead>
            <tr>
                <th colspan = "2">
                    {{ Form::select('approval_stage', $stages, null, ['class'=> 'form-control', 'st-search'=>'approval_stage']) }}
                </th>
                <th colspan = "1">
                    <select st-search="transaction_type" class="form-control">
                        <option value="">Transaction Type</option>
                        @foreach($transaction_types as $key => $type)
                            <option value="{!! $key !!}">{!! $type !!}</option>
                        @endforeach
                    </select>
                </th>
                <th colspan = "1"><input st-search="client_code" class = "form-control" placeholder = "Search client code..." type = "text"/></th>
                <th colspan = "2"><input st-search class = "form-control" placeholder = "Search..." type = "text"/></th>
            </tr>
                <tr>
                    <td>Client Code</td>
                    <td>Client Name</td>
                    <td st-sort="transaction_type">Transaction type</td>
                    <td st-sort="sent_by">Sent By</td>
                    <td st-sort="created_at">Sent On</td>
                    <td colspan="2">Details</td>
                </tr>
            </thead>
            <tbody ng-show="!isLoading">
                <tr ng-repeat="row in displayed">
                    <td><% row.client_code  %></td>
                    <td><% row.client_name %></td>
                    <td><% row.transaction_type %></td>
                    <td><% row.sent_by %></td>
                    <td><% row.sent_on %></td>
                    <td><a uib-popover="View details" popover-trigger="mouseenter" href="/dashboard/investments/approve/<% row.id %>"><i class="fa fa-list-alt"></i></a></td>
                    <td>
                        <a uib-popover="Remove transaction" popover-trigger="mouseenter" href="#" data-toggle="modal" data-target="#removeTransaction<% row.id %>"><i class="fa fa-trash-o" style="color: red;"></i></a>

                        <!-- Remove Transaction Modal -->
                        <div class="modal fade" id="removeTransaction<% row.id %>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Remove Transaction</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <h4><% row.client_name %> <% row.transaction_type %></h4>
                                            <p>Are you sure you want to remove this transaction?</p>
                                            <div class="form-group">
                                                <label>Reason</label>
                                                <input type="text" class="form-control" name="reason" id="reason" ng-model="reason" />
                                            </div>
                                            <label>Type <span class="label label-default">delete</span> and submit to remove this request.</label>
                                            <input name='confirm' class="form-control" id="confirm" ng-model="confirm" />
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger pull-left" href="/dashboard/investments/approvalremove/<% row.id %>?confirm=<% confirm %>&reason=<% reason %>">Remove</a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
            <tbody ng-show="isLoading">
                <tr>
                    <td colspan="7" class="text-center">Loading ... </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    {{--<div st-pagination = "" st-items-by-page = "itemsByPage" st-template="pagination.custom.html"></div>--}}
                    <td class="text-center" st-pagination="" st-items-by-page="10" colspan="6"></td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection