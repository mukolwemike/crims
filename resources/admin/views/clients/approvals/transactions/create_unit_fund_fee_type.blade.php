<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Unit Fund Fee Type</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>