<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Buy Entity Shares</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Entity</td>
                        <td>{!! $entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Share Price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->sharePrice($data['request_date'])) !!} as at the request date</td>
                    </tr>
                    @if(isset($data['exemption']))
                        <tr style="background-color: yellow; color:red;">
                            <td>Exempted Share Price</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['exemption']) !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Maximum Shares for Entity</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->maximumIssues($data['request_date']), true, 0) !!} as at the request date</td>
                    </tr>
                    <tr>
                        <td>Shares Sold</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->soldShares($data['request_date']), true, 0) !!} as at the request date</td>
                    </tr>
                    <tr>
                        <td>Shares Remaining</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->remainingShares($data['request_date']), true, 0) !!} as at the request date</td>
                    </tr>
                    <tr>
                        <td>Buyer</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($buyer->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['request_date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], true, 0) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Current Funds</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($funds = $buyer->sharePaymentsBalance()) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Total Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($cost = $price * $data['number']) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Balance After Purchase</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($funds - $cost) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>