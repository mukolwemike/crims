<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Deactivate Account Details</h4>
        </div>
        <div class="panel-body">

            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>Account Name</th>
                    <th>AccountNumber</th>
                    <th>Bank</th>
                    <th>Branch</th>
                    <th>Currency</th>
                    <th>Clearing Code</th>
                    <th>Swift Code</th>
                    <th>Active</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! $account->account_name !!}</td>
                    <td>{!! $account->account_number !!}</td>
                    @if($branch)
                        <td>{!! $bank->name !!}</td>
                        <td>{!! $branch->name !!}</td>
                        <td>{!! $currency !!}</td>
                        <td>{!! $bank->clearing_code !!}</td>
                        <td>{!! $bank->swift_code !!}</td>
                        <td>
                            @if($account->active)
                                <span class="text-success"><i class="fa fa-check"></i></span>
                            @else
                                <span class="text-danger"><i class="fa fa-times"></i></span>
                            @endif
                        </td>
                    @else
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    @endif
                </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>