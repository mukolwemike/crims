<div>
      <div class="panel panel-success">
            <div class="panel-heading">
                  <h4>Transaction details:</h4>
            </div>
            <div class="panel-body">
                @if($duplicate && $duplicate->exists)
                    @if($app->new_client == 2)
                        <p>The sender has requested that the client is added as a duplicate.</p>
                    @elseif($app->new_client == 1)
                        <p>This application belongs to an existing client
                            <a href="/dashboard/clients/details/{{ $app->client_id }}">
                                {{ \Cytonn\Presenters\ClientPresenter::presentFullNames($app->client_id) }}
                                - {{ $app->client->client_code }}
                            </a>
                        </p>
                    @else
                        <p class="warning">You will not be able to approve this unless it's added as a duplicate</p>
                    @endif
                    <br>
                    <p>The client matched the following duplicate records</p>
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Code</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($duplicate->clients as $client)
                            <tr>
                                <td>
                                    <a href="/dashboard/clients/details/{{ $client->id }}">
                                        {{ \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) }}
                                    </a>
                                </td>
                                <td>{{ $client->client_code }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
                <br>
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    View Application Details
                </button>
            </div>
      </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                  <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Client Investment Application</h4>
                  </div>
                  <div class="modal-body">
                        @include('unitization.partials.application_partial')
                        <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
            </div>
      </div>
</div>