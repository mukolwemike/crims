<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Reverse Forfeiture</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <label>Project</label> {!! $project->name !!}<br/>
                <label>Unit</label> {!! $holding->unit->number !!} <br>
                <label>Unit Type</label> {!! $holding->unit->size->name.' '.$holding->unit->type->name !!} <br>
                <label>Forfeited Date</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($holding->forfeit_date) !!} <br/>
                <label>Refunded Amount</label>
                @if($refund)
                    {!! \Cytonn\Presenters\AmountPresenter::currency($refund->amount) !!}
                @endif
                <br>
                <label>Forfeited Reason</label> {!! $holding->forfeit_reason !!} <br>
            </div>
        </div>
    </div>
</div>