<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Add Tax Exemption Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Client</td>
                    <td>{{ $client->name() }}</td>
                </tr>
                <tr>
                    <td>Client Code</td>
                    <td>{{ $client->client_code }}</td>
                </tr>
                <tr>
                    <td>Start Date</td>
                    <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($data['start_date']) }}</td>
                </tr>
                <tr>
                    <td>End Date</td>
                    <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($data['end_date']) }}</td>
                </tr>
                <tr>
                    <td>Maximum Amount</td>
                    <td>{{ \Cytonn\Presenters\AmountPresenter::currency($data['amount'])}}</td>
                </tr>
                <tr>
                    <td>File</td>
                    <td>
                        <a class="" target="_blank"
                           href="/dashboard/investments/client-instructions/filled-application-documents/{!! $data['document_id'] !!} ">
                            <span class="glyphicon glyphicon-file"></span>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>