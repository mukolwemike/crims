<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Update Account Cash</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>New Details</th>
                    <th>Current Details</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                        @if($transaction->client_id)
                            <td>{!! $transaction->client->client_code !!}</td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Client</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                        @if($transaction->client_id)
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($transaction->client->id) !!}</td>
                        @else
                            <td>{!! $transaction->received_from !!} (Not on-boarded yet)</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Payment For</td>
                        <td colspan="2">{!! $payment->present()->paymentFor !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Reference No.</td>
                        <td colspan="2">{!! $transaction->bank_reference_no !!}</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td colspan="2">{!! $transaction->transactionType->name !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($transaction->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td colspan="2">{!! $transaction->description !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td>
                    </tr>
                    <tr>
                        <td>FA</td>
                        <td>@if($recipient) {!! $recipient->name !!} @endif</td>
                        @if($transaction->recipient_id)
                            <td>{!! $transaction->recipient->name !!}</td>
                        @else
                            <td>No FA specified</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>