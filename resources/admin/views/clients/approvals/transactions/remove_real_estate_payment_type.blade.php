<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Payment Type Details</h4>
        </div>
        <div class="panel-body">
            <h5>Remove Payment Type Details</h5>

            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Slug</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! $type->name !!}</td>
                    <td>{!! $type->slug !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
