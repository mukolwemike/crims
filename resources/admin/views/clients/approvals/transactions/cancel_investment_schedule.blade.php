
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Cancel Investment Schedule</h4>
    </div>
    <div class="panel-body">
        <table class="table table-responsive table-striped">
            <thead></thead>
            <tbody>
                <tr>
                    <td>Investment ID</td>
                    <td> {!! $schedule->investment_id !!}</td>
                </tr>
                <tr>
                    <td>Schedule Action</td>
                    <td>{!! $schedule->transaction_type !!}</td>
                </tr>
                <tr>
                    <td>Principal</td><td> {{ \Cytonn\Presenters\AmountPresenter::currency($investment->amount) }}</td>
                </tr>
                <tr>
                    <td>Value date</td><td>{{ \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) }}</td>
                </tr>
                <tr>
                    <td>Maturity Date</td><td>{{ \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) }}</td>
                </tr>
                <tr>
                    <td>Action Date</td><td>{{ \Cytonn\Presenters\DatePresenter::formatDate($schedule->action_date) }}</td>
                </tr>
                <tr>
                    <td>Transaction details</td><td><a href="/dashboard/investments/approve/{{ $schedule->id }}">Details</a></td>
                </tr>
                {{--@if($schedule->transaction_type == 'withdrawal')--}}
                    {{--<tr>--}}
                        {{--<td>Execution Date</td>--}}
                        {{--<td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($schedule->action_date) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Premature Withdraw</td>--}}
                        {{--<td> {!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($schedule->payload['premature']) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Invested Amount</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($clientinvestment->amount) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Invested Date</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clientinvestment->invested_date) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Maturity Date</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clientinvestment->maturity_date) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Scheduled Amount</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->payload['amount']) !!}</td>--}}
                    {{--</tr>--}}
                {{--@elseif($schedule->transaction_type == 'rollover')--}}
                    {{--<tr>--}}
                        {{--<td>Investment Date</td>--}}
                        {{--<td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($schedule->payload['invested_date']) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Maturity Date</td>--}}
                        {{--<td>{!!  \Cytonn\Presenters\DatePresenter::formatDate($schedule->payload['maturity_date']) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Interest Rate</td>--}}
                        {{--<td>{!! $schedule->payload['interest_rate'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Amount</td>--}}
                        {{--<td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->payload['amount']) !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Commission Recipient</td>--}}
                        {{--<td>{!! $recipient($schedule->payload['commission_recepient'])->name !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Commission Rate</td>--}}
                        {{--<td>{!! $schedule->payload['commission_rate'] !!}%</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Interest Payment Interval</td>--}}
                        {{--@if($schedule->payload['interest_payment_interval'] ==0)--}}
                            {{--<td>On Maturity</td>--}}
                        {{--@elseif($schedule->payload['interest_payment_interval'] == '1')--}}
                            {{--<td>Monthly</td>--}}
                        {{--@elseif($schedule->payload['interest_payment_interval'] == '4')--}}
                            {{--<td>Quarterly</td>--}}
                        {{--@elseif($schedule->payload['interest_payment_interval'] == '6')--}}
                            {{--<td>Semi Annually</td>--}}
                        {{--@elseif($schedule->payload['interest_payment_interval'] == '12')--}}
                            {{--<td>Annually</td>--}}
                        {{--@endif--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Interest Payment Date</td>--}}
                        {{--<td>{!! $schedule->payload['interest_payment_date'] !!}</td>--}}
                    {{--</tr>--}}
                {{--@endif--}}
            </tbody>
        </table>
    </div>
</div>
