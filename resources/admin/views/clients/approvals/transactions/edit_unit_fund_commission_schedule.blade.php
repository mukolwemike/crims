<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Commission Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <div class="detail-group">
                <h4>Schedule Details</h4>
                <table class="table table-responsive table-striped">
                    <tbody>
                    <tr>
                        <td>Schedule Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td><td>{!! $schedule->description !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="detail-group">
                <h4>Edit Details</h4>
                <table class="table table-responsive table-hover table-striped">
                    <thead>
                    <tr>
                        <th class="bold">Item</th>
                        <th class="bold">Was</th>
                        <th class="bold">Now</th>
                    <tr/>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Schedule Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['old_schedule_date']) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['schedule_date']) !!}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>