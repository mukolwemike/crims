<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Account Cash</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        <td colspan="2">{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client</td>
                        <td colspan="2">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Custodial Account</td>
                        <td colspan="2">{!! $instruction->custodialTransaction->custodialAccount->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Project</td>
                        <td colspan="2">@if($payment->project) {!! $payment->project->name !!} @endif</td>
                    </tr>
                    <tr>
                        <td>Product</td>
                        <td colspan="2">@if($payment->product) {!! $payment->product->name !!} @endif</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($instruction->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($instruction->date) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td colspan="2">{!! $instruction->description !!}</td>
                    </tr>
                    <tr>
                        <th>First Signatory</th>
                        @if($instruction->firstSignatory)
                            <td colspan="2">{!! $instruction->firstSignatory->full_name !!}</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Second Signatory</th>
                        @if($instruction->firstSignatory)
                            <td colspan="2">{!! $instruction->secondSignatory->full_name !!}</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Bank Details</th>
                        <td>Account Name</td>
                        <td>{!! $account->accountName !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Account Number</th>
                        <td>{!! $account->accountNumber !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Bank</th>
                        <td>{!! $account->bankName !!}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <th>Branch</th>
                        <td>{!! $account->branch !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>