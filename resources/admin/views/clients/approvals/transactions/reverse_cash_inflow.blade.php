<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Reverse Cash Inflow</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Client</td>
                        <td>
                            @if($transaction->client_id)
                                {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($transaction->client->id) !!}
                            @else
                                {!! $transaction->received_from !!}
                            @endif
                        </td>
                    </tr>
                    <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}</td></tr>
                    <tr>
                        <td>Amount</td>
                        <td>
                            {!! \Cytonn\Presenters\AmountPresenter::currency($transaction->amount) !!}
                            @if($transaction->deleted_at)
                                <span class="label label-sm label-danger">Reversed</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Source</td>
                        <td>
                            {!! $transaction->source !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Payment For</td>
                        <td>
                            {!! @$payment->present()->paymentFor !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Reason</td>
                        <td>
                            {!! $data['reason'] !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Bank Reference</td>
                        <td>{!! $transaction->bank_reference_no !!}</td>
                    </tr>
                    <tr>
                        <td>Cheque Number</td>
                        <td>{!! $transaction->cheque_number !!}</td>
                    </tr>
                    <tr>
                        <td>Entry Date</td>
                        <td>{!! $transaction->entry_date !!}</td>
                    </tr>
                    <tr>
                        <td>Mpesa Confirmation Code</td>
                        <td>{!! $transaction->mpesa_confirmation_code !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Transaction Id</td>
                        <td>{!! $transaction->bank_transaction_id !!}</td>
                    </tr>
                    <tr>
                        <td>Outgoing Reference</td>
                        <td>{!! $transaction->outgoing_reference !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>