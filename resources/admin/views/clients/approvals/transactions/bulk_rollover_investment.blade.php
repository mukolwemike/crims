<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Bulk Rollover Maturities - {!! \Cytonn\Presenters\DatePresenter::formatDate($maturityDate) !!}</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                {!! Form::open(['route' => ['approval_post_action' , $approval->id]]) !!}

                <table class="table table-hover table-responsive table-striped">
                    <tbody>
                    <tr>
                        <td colspan="100%">
                            <a href="{!! route('approval_action', [$approval->id, 'export']) !!}" class="btn btn-info">
                                <i class="fa fa-file-excel-o"></i> Export Investments</a>
                        </td>
                    </tr>
                    @if($approval->approved)
                        <tr>
                            <td colspan="100%">
                                <a href="{!! route('approval_action', [$approval->id, 'send_bc']) !!}" class="btn btn-success pull-right">
                                    Send Business Confirmations</a>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th>Client Code</th>
                        <th>Client Name</th>
                        <th>Principal</th>
                        <th>Value On Maturity</th>
                        <th>Invested Date</th>
                        <th>Maturity Date</th>
                        <th>Product</th>
                        <th>Eligible</th>
                        <th colspan="3">Action</th>
                    </tr>
                    @foreach($investments as $investment)
                        <tr>
                            <td>{!! $investment->client->client_code !!}</td>
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                            <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($investment->amount) !!}</td>
                            <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($investment->repo->getFinalValueOnWithdrawal()) !!}</td>
                            <td>{!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($investment->invested_date) !!}</td>
                            <td>{!! \App\Cytonn\Presenters\General\DatePresenter::formatDate($investment->maturity_date)  !!}</td>
                            <td>{!! $investment->product->name  !!}</td>
                            <td>
                                @if($checkEligible($investment->id))
                                    <span class="label label-success">Yes</span>
                                @else
                                    <span class="label label-danger">No</span>
                                @endif

                            </td>
                            @if(! in_array($investment->id, $removedInvestments))
                                <td>
                                    <span class="label label-success">Rolling</span>
                                </td>
                                <td>
                                    @if(! $approval->approved)
                                        <a class="btn btn-danger btn-sm"
                                           href="{!! route('approval_action', [$approval->id, 'remove_investment', $investment->id]) !!}">Remove</a>
                                    @endif
                                </td>
                                <td>
                                    {!! Form::checkbox($investment->id, true, null, ['id' => $investment->id]) !!}
                                </td>
                            @else
                                <td>
                                    <span class="label label-danger">Not Rolling</span>
                                </td>
                                <td>
                                    @if(! $approval->approved)
                                        <a class="btn btn-success btn-sm"
                                           href="{!! route('approval_action', [$approval->id, 'add_investment', $investment->id]) !!}">Add</a>
                                    @endif
                                </td>
                                <td>

                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {!! Form::hidden("action_type", 'bulk_remove_investments') !!}

                {!! Form::submit('Bulk Remove', ['class'=>'btn btn-danger pull-right']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>