<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transfer Cash to Another Client</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Sender</th>
                        <th>Recipient</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $sending_client->client_code !!}</td>
                        <td>{!! $receiving_client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($sending_client->id) !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($receiving_client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Project</td>
                        <td>
                            @if($previous_project)
                                {!! $previous_project->name !!}
                            @endif
                        </td>
                        <td>
                            @if($new_project)
                                {!! $new_project->name !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Product</td>
                        <td>
                            @if($previous_product)
                                {!! $previous_product->name !!}
                            @endif
                        </td>
                        <td>
                            @if($new_product)
                                {!! $new_product->name !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Shares</td>
                        <td>
                            @if($previous_entity)
                                {!! $previous_entity->name !!}
                            @endif
                        </td>
                        <td>
                            @if($new_entity)
                                {!! $new_entity->name !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Unit Funds</td>
                        <td>
                            @if($previous_fund)
                                {!! $previous_fund->name !!}
                            @endif
                        </td>
                        <td>
                            @if($new_fund)
                                {!! $new_fund->name !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Bank Account</td>
                        <td>{!! $sending_custodial_account->fullname !!}</td>
                        <td>{!! $receiving_custodial_account->fullname !!}</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>{!! $sending_custodial_account->account_no !!}</td>
                        <td>{!! $receiving_custodial_account->account_no !!}</td>
                    </tr>
                    <tr>
                        <td>Amount Sent</td>
                        <td>{!! $sending_custodial_account->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency(abs($data['amount'])) !!}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Exchange Rate</td>
                        <td>{!! $data['exchange_rate'] !!}</td>
                        <td>{!! $data['exchange_rate'] !!}</td>
                    </tr>
                    <tr>
                        <td>Amount Received</td>
                        <td></td>
                        <td>{!! $receiving_custodial_account->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency(abs($data['amount'] * $data['exchange_rate'])) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td></td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>FA</td>
                        <td></td>
                        <td>@if($recipient) {!! $recipient->name !!} @endif</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>