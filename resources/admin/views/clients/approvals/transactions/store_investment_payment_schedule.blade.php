<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store Investment Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th colspan="100%" style="text-align: center;">Schedule Details</th>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                </tr>
                <tr>
                    <td>Interest Rate</td>
                    <td colspan="2">{!! $schedule->interest_rate !!}</td>
                </tr>
                <tr>
                    <td>Paid</td>
                    <td colspan="2">{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($schedule->paid) !!}</td>
                </tr>
                <tr>
                    <td>Date Paid</td>
                    <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date_paid) !!}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td colspan="2">{!! $schedule->description !!}</td>
                </tr>
                <tr>
                    <th colspan="100%" style="text-align: center;">Edit Details</th>
                </tr>
                @if($schedule)
                    <tr>
                        <th></th>
                        <th>Current Investment Payment Schedule</th>
                        <th>New Investment Payment Schedule</th>
                    </tr>
                    <tr>
                        <td>Payment Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_before'])!!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date'])!!}</td>
                    </tr>
                    <tr>
                        <td>Payment Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount_before'])!!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'])!!}</td>
                    </tr>
                @else
                    <tr>
                        <th colspan="100%" style="text-align: center;">New Investment Payment Schedule Details</th>
                    </tr>
                    <tr>
                        <td>Payment Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Amount</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
