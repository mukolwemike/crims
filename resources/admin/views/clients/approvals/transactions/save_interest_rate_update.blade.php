<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Save Interest Rate Update</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Product</td>
                    <td>{!! $product->name !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>{!! $data['description'] !!}</td>
                </tr>
                </tbody>
            </table>

            <h4>Rates</h4>
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th>Tenor</th>
                    <th>Rate</th>
                    <th>Minimum Amount</th>
                    <th>Hidden Rate</th>
                </tr>
                @foreach($rates as $rate)
                    <tr>
                        <td>{!! $rate['tenor'] !!}</td>
                        <td>{!! $rate['rate'] !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($rate['min_amount']) !!}</td>
                        <td>
                            @if(isset($rate['hidden']))
                                @if($rate['hidden']) Yes @else No @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>