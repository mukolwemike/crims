<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Unit Fund</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Fund Category</td>
                        <td>{!! $fundCategory->name !!}</td>
                    </tr>
                    <tr>
                        <td>Fund Manager</td>
                        <td>{!! $fundManager->fullname !!}</td>
                    </tr>
                    <tr>
                        <td>Currency</td>
                        <td>{!! $currency->code !!}</td>
                    </tr>
                    <tr>
                        <td>Initial Unit Price</td>
                        <td>{!! isset($data['initial_unit_price'])
                            ? \Cytonn\Presenters\AmountPresenter::currency($data['initial_unit_price'])
                            : \Cytonn\Presenters\AmountPresenter::currency(0, true, 2) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Minimum Investment Amount</td>
                        <td>{!! isset($data['minimum_investment_amount'])
                            ? \Cytonn\Presenters\AmountPresenter::currency($data['minimum_investment_amount'])
                            : \Cytonn\Presenters\AmountPresenter::currency(0, true, 2) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Minimum Investment Horizon</td>
                        <td>{!! isset($data['minimum_investment_horizon']) ? $data['minimum_investment_horizon'] : 0 !!}</td>
                    </tr>

                    @if(isset($data['benefits']))
                    <tr>
                        <td>Fund Benefits</td>
                        <td>{!! $data['benefits'] !!}</td>
                    </tr>
                    @endif

                    @if(isset($data['fund_objectives']))
                    <tr>
                        <td>Fund Objectives</td>
                        <td>{!! $data['fund_objectives'] !!}</td>
                    </tr>
                    @endif

                    <tr>
                        <td>Custodial Account</td>
                        <td>{!! $account->full_name !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>