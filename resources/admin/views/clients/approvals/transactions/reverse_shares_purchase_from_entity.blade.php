<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Reverse Shares Purchase From Entity</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Buyer</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($buyer->client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $buyer->number !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! $order->number !!}</td>
                    </tr>
                    <tr>
                        <td>Bought Shares</td>
                        <td>{!! $bought !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! $order->price !!}</td>
                    </tr>
                    <tr>
                        <td>Remaining Shares</td>
                        <td>{!! $order->number - $bought !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Good Till Filled/Cancelled</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($order->good_till_filled_cancelled) !!}</td>
                    </tr>
                    @if(!$order->good_till_filled_cancelled)
                        <tr>
                            <td>Expiry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->expiry_date) !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Matched</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($order->matched) !!}</td>
                    </tr>
                    @if($order->recipient)
                        <tr>
                            <td>Commission Recipient</td>
                            <td>{!! $order->recipient->name !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Commission rate</td>
                        <td>{!! $order->commission_rate !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>