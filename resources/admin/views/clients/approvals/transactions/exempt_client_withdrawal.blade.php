<div>
    <div class="panel panel-success">

        <div class="panel-heading">
            <h4>Exempt Client Withdrawal Instruction</h4>
        </div>

        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>

                    <tr>
                        <td width="30%">Transaction ID</td>
                        <td>{{ $payment->id }}</td>
                    </tr>

                    <tr>
                        <td width="30%">Payment Type</td>
                        <td>{{ $payment->type->name }}</td>
                    </tr>

                    <tr>
                        <td width="30%">Payment For</td>
                        <td>{!! $payment->present()->paymentFor !!}</td>
                    </tr>

                    <tr>
                        <td width="30%">Amount</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency((double)$payment->amount) }}</td>
                    </tr>

                    <tr>
                        <td width="30%">Description</td>
                        <td>{{ $payment->description }}</td>
                    </tr>

                    <tr>
                        <td width="30%">Current Action Date</td>
                        <td>{{ \Cytonn\Core\DataStructures\Carbon::parse($payment->date)->toFormattedDateString() }}</td>
                    </tr>

                    <tr class="danger">
                        <td width="30%">Exempted Date</td>
                        <td>{{ \Cytonn\Core\DataStructures\Carbon::parse($data['date'])->toFormattedDateString() }}</td>
                    </tr>

                    <tr>
                        <td width="30%">FA</td>
                        <td>{{ is_null($payment->commission_recipient_id) ? null : $payment->recipient->name }}</td>
                    </tr>

                    <tr>
                        <td width="30%">Balance</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($payment->paymentBalance()) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>