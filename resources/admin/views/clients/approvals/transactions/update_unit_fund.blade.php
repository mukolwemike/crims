<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Unit Fund</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>New</th>
                        <th>Current</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                        <td>{!! $fund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Fund Manager</td>
                        <td>{!! $fundManager->fullname !!}</td>
                        <td>{!! $fund->manager->fullname !!}</td>
                    </tr>
                    <tr>
                        <td>Currency</td>
                        <td>{!! $currency->code !!}</td>
                        <td>{!! $fund->currency->code !!}</td>
                    </tr>
                    <tr>
                        <td>Initial Unit Price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['initial_unit_price']) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund->initial_unit_price) !!}</td>
                    </tr>
                    <tr>
                        <td>Minimum Investment Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['minimum_investment_amount']) !!}</td>
                        <td>{!! $fund->minimum_investment_amount !!}</td>
                    </tr>
                    <tr>
                        <td>Minimum Investment Horizon</td>
                        <td>{!! $data['minimum_investment_horizon'] !!}</td>
                        <td>{!! $fund->minimum_investment_horizon !!}</td>
                    </tr>
                    <tr>
                        <td>Fund Benefits</td>
                        <td>{!! $data['benefits'] !!}</td>
                        <td>{!! $fund->benefits !!}</td>
                    </tr>
                    <tr>
                        <td>Fund Objectives</td>
                        <td>{!! $data['fund_objectives'] !!}</td>
                        <td>{!! $fund->fund_objectives !!}</td>
                    </tr>
                    <tr>
                        <td>Custodial Account</td>
                        <td>{!! $account->full_name !!}</td>
                        <td>
                            @if($fund->receivingAccounts()->latest()->first())
                                {!! $fund->receivingAccounts()->latest()->first()->full_name !!}</td>
                            @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>