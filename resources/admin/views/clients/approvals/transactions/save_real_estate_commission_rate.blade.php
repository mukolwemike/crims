<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Commission Rate {!! !isset($data['rate_id']) ? '' : ' (Editing)' !!}</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                @if(isset($data['date']))
                    <label>Effective Date</label> {!! $data['date'] !!}<br/>
                @endif
                <label>Project</label> {!! $project->name !!}<br/>
                <label>FA Type</label> {!! $recipientType->name !!} <br>
                <label>Rate Type</label>  {!! $data['type'] !!}<br>
                <label>Value</label> {!! $data['amount'] !!}
            </div>
        </div>
    </div>
</div>