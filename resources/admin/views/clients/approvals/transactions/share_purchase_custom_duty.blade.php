<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Share Purchase Stamp Duty Payment</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <th width="30%">Client Name:</th>
                        <td>{{ $holder->Client->name() }}</td>
                    </tr>

                    <tr>
                        <th width="30%">Holder Number:</th>
                        <td>{{ $holder->number }}</td>
                    </tr>

                    <tr>
                        <th width="30%">Entity Name:</th>
                        <td>{{ $holder->entity->name }}</td>
                    </tr>

                    <tr>
                        <th width="30%">No of Purchased Shares</th>
                        <td> {{ \Cytonn\Presenters\AmountPresenter::currency($holding->number, true, 0) }} </td>
                    </tr>

                    <tr>
                        <th width="30%">Price Purchased</th>
                        <td> {{ \Cytonn\Presenters\AmountPresenter::currency($holding->purchase_price) }} </td>
                    </tr>

                    <tr>
                        <th width="30%">Total Amount</th>
                        <td>KES. {{ \Cytonn\Presenters\AmountPresenter::currency(($holding->number * $holding->purchase_price)) }}</td>
                    </tr>

                    <tr>
                        <th width="30%">Amount (stamp duty)</th>
                        <td>KES. {{ \Cytonn\Presenters\AmountPresenter::currency(($holding->number * $holding->purchase_price)/100) }}</td>
                    </tr>

                    <tr>
                        <th width="30%">Payment Date</th>
                        @if(isset($data['date']))
                            <td>{{ \Carbon\Carbon::parse($data['date'])->toFormattedDateString() }}</td>
                        @else
                            <td>{{ \Carbon\Carbon::parse($purchase->date)->toFormattedDateString() }}</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>