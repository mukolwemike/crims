<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Account Details</h4>
        </div>
        <div class="panel-body">

            @if(isset($data['id']))
                <h5>Edit Bank Details</h5>

                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Account Name</th>
                        <th>AccountNumber</th>
                        <th>Bank</th>
                        <th>Branch</th>
                        <th>Currency</th>
                        <th>Clearing Code</th>
                        <th>Swift Code</th>
                        <th>Account Type</th>
                        <th>Country</th>
                        <th>Upload</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>From</td>
                        <td>{!! $account->account_name !!}</td>
                        <td>{!! $account->account_number !!}</td>
                        @if($oldBranch)
                            <td>{!! $oldBranch->bank->name !!}</td>
                            <td>{!! $oldBranch->name !!}</td>
                            <td>{!! $account->currency ? $account->currency->name : '-' !!}</td>
                            <td>{!! $oldBranch->bank->clearing_code !!}</td>
                            <td>{!! $oldBranch->bank->swift_code !!}</td>
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                        <td></td>
                        <td>{!! $account->country ? $account->country->name : ''; !!}</td>
                        <td>
                            @if($account->document)
                                <a target="_blank"
                                   href="/document/instruction-approval/{!! $account->document_id !!}"><i
                                            class="fa fa-eye"></i></a>
                            @else
                                <span>-</span>
                            @endif
                        </td>
                    </tr>
                    <tr class="success">
                        <td>To</td>
                        <td>{!! $data['investor_account_name'] !!}</td>
                        <td>{!! $data['investor_account_number'] !!}</td>
                        @if($branch)
                            <td>{!! $bank->name !!}</td>
                            <td>{!! $branch->name !!}</td>
                            <td>{!! $currency->name !!}</td>
                            <td>{!! $bank->clearing_code !!}</td>
                            <td>{!! $bank->swift_code !!}</td>
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                        <td>{{strtoupper($data['formType'])}}</td>
                        <td>{!! $country ? $country : '' !!}</td>
                        <td>
                            @if(isset($data['document_id']))
                                <a target="_blank" href="/document/instruction-approval/{!! $data['document_id'] !!}"><i
                                            class="fa fa-eye"></i></a>
                            @else
                                <span>-</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            @else
                <h5>Add Account Details</h5>

                <table class="table table-responsive table-striped">
                    <thead></thead>
                    <tbody>
                    @if($data['formType'] === 'bank')
                        <tr>
                            <td>Account Name</td>
                            <td>{!! $data['investor_account_name'] !!}</td>
                        </tr>
                        <tr>
                            <td>AccountNumber</td>
                            <td>{!! $data['investor_account_number'] !!}</td>
                        </tr>
                        @if($branch)
                            <tr>
                                <td>Bank</td>
                                <td>{!! $bank->name !!}</td>
                            </tr>
                            <tr>
                                <td>Branch</td>
                                <td>{!! $branch->name !!}</td>
                            </tr>
                            <tr>
                                <td>Currency</td>
                                <td>{!! $currency->name !!}</td>
                            </tr>
                            <tr>
                                <td>Clearing Code</td>
                                <td>{!! $bank->clearing_code !!}</td>
                            </tr>
                            <tr>
                                <td>Swift Code</td>
                                <td>{!! $bank->swift_code !!}</td>
                            </tr>
                            <tr>
                                <td>Account Type</td>
                                <td>{{strtoupper($data['formType'])}}</td>
                            </tr>
                        @endif
                        <tr>
                            <td>Country</td>
                            <td>{!! $country ? $country : '' !!}</td>
                        </tr>
                        @if(isset($data['document_id']))
                            <tr>
                                <td>Upload</td>
                                <td><a target="_blank"
                                       href="/document/instruction-approval/{!! $data['document_id'] !!}"><i
                                                class="fa fa-file-pdf-o"></i> View</a></td>
                            </tr>
                        @endif
                    @elseif($data['formType'] === 'mpesa')
                        <tr>
                            <td>Account Name</td>
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                        </tr>
                        <tr>
                            <td>AccountNumber</td>
                            <td>{!! $data['phone_number'] !!}</td>
                        </tr>
                        <tr>
                            <td>Currency</td>
                            <td>{!! $currency->name !!}</td>
                        </tr>
                        <tr>
                            <td>Account Type</td>
                            <td>{{strtoupper($data['formType'])}}</td>
                        </tr>
                        <tr>
                            <td>Country</td>
                            <td>{{ $country ? $country : '' }}</td>
                        </tr>
                        @if(isset($data['document_id']))
                            <tr>
                                <td>Upload</td>
                                <td><a target="_blank"
                                       href="/document/instruction-approval/{!! $data['document_id'] !!}"><i
                                                class="fa fa-file-pdf-o"></i> View</a></td>
                            </tr>
                        @endif
                    @endif
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>