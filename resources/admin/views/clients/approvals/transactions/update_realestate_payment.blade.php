<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Payment Update Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead>
                    <tr><td></td><th>Was</th><th>Now</th></tr>
                </thead>
                <tbody>
                    <tr><td>Project</td><td colspan="100%">{!! $project->name !!}</td></tr>
                    <tr><td>Unit</td><td colspan="100%">{!! $unit->number !!}</td></tr>
                    <tr><td>Size</td><td colspan="100%">{!! $unit->size->name !!} {!! $unit->type->name !!}</td></tr>
                    <tr><td>Price</td><td colspan="100%">{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($old->amount) !!}</td><td>{!! isset($data['amount']) ? \Cytonn\Presenters\AmountPresenter::currency($data['amount']) : \Cytonn\Presenters\AmountPresenter::currency($old->amount) !!}</td></tr>
                    <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($old->date) !!}</td><td>{!! isset($data['amount']) ? \Cytonn\Presenters\DatePresenter::formatDate($data['date']) : \Cytonn\Presenters\DatePresenter::formatDate($old->date) !!}</td></tr>
                    <tr><td>Description</td><td>{!! $old->description !!}</td><td>{!! $data['description'] !!}</td></tr>
                    <tr><td>Payment</td><td>{!! $old->schedule->description !!}</td><td>{!! $schedule->description !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div>
    <!-- Modal -->
    <div class="modal large fade" id="reservationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reservation Form</h4>
                </div>
                <div class="modal-body">
                    @include('realestate.reservations.partials.reservation_form', ['client'=>$holding->client])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>