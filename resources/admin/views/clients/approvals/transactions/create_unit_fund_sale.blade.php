<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Sell Fund Units</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td width="30%">Client Name</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                </tr>
                <tr>
                    <td>Client Code</td>
                    <td>{!! $client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Fund Name</td>
                    <td>{!! $fund->name !!}</td>
                </tr>
                @if(isset($data['instruction_type']))
                    <tr>
                        <td>Instruction Type</td>
                        <td>{!! $data['instruction_type'] !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Number of Units</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($instruction->number, true, 2) !!}</td>
                </tr>
                <tr>
                    <td>Unit Price</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund->price) !!}</td>
                </tr>
                <tr>
                    <td>Value of Sold Units</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fund->price * $instruction->number) !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                @if($fees)
                    @foreach($fees as $fee)
                        <tr>
                            <td>Fee: {{ $fee->type->name }}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fee->fee_units) !!}</td>
                        </tr>
                    @endforEach
                @endif
                <tr>
                    <td>Source</td>
                    <td>
                        @if($instruction->user_id)
                            <span class="label label-primary">Client</span>
                        @else
                            <span class="label label-info">Admin</span>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>