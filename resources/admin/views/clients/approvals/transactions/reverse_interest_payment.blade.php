<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">

            <h4>Investment Details</h4>

            <table class="table table-responsive table-striped">
                <thead>
                </thead>
                <tbody>
                    <tr><td>Product</td><td>{!! $investment->product->name !!}</td></tr>
                    <tr><td>Value</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestment()) !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount)!!}</td></tr>
                    <tr><td>Interest Rate</td><td>{!! $investment->interest_rate !!}%</td></tr>
                    <tr><td>Invested date</td><td>{!! $investment->invested_date !!}</td></tr>
                    <tr><td>Maturity date</td><td>{!! $investment->maturity_date !!}</td></tr>
                </tbody>
            </table>


            <h4>Payments Made</h4>

            <table class="table table-responsive table-striped">
                <thead>
                    <tr><th>Date</th><td colspan="2">Amount</td></tr>
                </thead>
                <tbody>
                    @foreach($investment->payments as $payment)
                        <tr><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date_paid) !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount)!!}</td></tr>
                    @endforeach
                </tbody>
            </table>

            <h4>Reversed Payments</h4>

            <table class="table table-responsive table-striped">
                <thead>
                <tr><th>Date</th><td colspan="2">Amount</td></tr>
                </thead>
                <tbody>
                @foreach($reversedPayments as $payment)
                    <tr><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date_paid) !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount)!!}</td></tr>
                @endforeach
                </tbody>
            </table>


            <h4>Payment to be reversed</h4>
            <table class="table table-responsive table-striped">
                <thead>
                <tr><th>Date</th><td>Amount</td></tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($the_payment->date_paid) !!}</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($the_payment->amount)!!}</td>
                </tr>
                </tbody>
            </table>
            @if($notFound)
                <div class="alert alert-danger">
                    <p>The payment is already deleted, it could have been reversed.</p>
                </div>
            @endif
        </div>
    </div>
</div>