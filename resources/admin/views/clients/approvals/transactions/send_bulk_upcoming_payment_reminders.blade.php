<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Upcoming Payments Bulk Reminders Approval - {!! \Cytonn\Presenters\DatePresenter::formatDate($start_date) !!}
            to {!! \Cytonn\Presenters\DatePresenter::formatDate($end_date) !!}</h4>
        </div>

        @if($project_id)
            <p>Showing payment reminders for {!! App\Cytonn\Models\Project::find($project_id)->name !!}</p>
        @else
            <p>Showing reminders for all projects</p>
        @endif
        <div class="panel-body">
            <div class="form-detail">
                <table class="table table-hover table-responsive table-striped">
                    <thead>
                        <tr>
                            <td>Client</td>
                            <td>Project</td>
                            <td>Unit</td>
                            <td>Date</td>
                            <td>Total Due</td>
                            <td>Description</td>
                            <td>Status</td>
                            <td>Remove</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schedules as $schedule)
                            <tr>
                                <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($schedule->holding->client_id) !!}</td>
                                <td>{!! $schedule->holding->project->name !!}</td>
                                <td>{!! $schedule->holding->unit->number !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date)!!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount_pending) !!}</td>
                                <td>{!! $schedule->description !!}</td>
                                <td>{!! in_array($schedule->id, $approval->payload['removed']) ? '<a class="label label-danger">Not Sending</a>' : '<a class="label label-success">Sending</a>' !!}</td>
                                <td><a class="btn btn-danger btn-sm" href="{!! route('approval_action', [$approval->id, 'remove', $schedule->id]) !!}">Remove</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>