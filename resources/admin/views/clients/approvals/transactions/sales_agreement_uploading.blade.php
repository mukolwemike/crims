<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Sales Agreement Uploaded</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <label>Project </label> {!! $project->name !!}<br/>
                <label>Unit </label> {!! $unit->number !!}<br/>
                @if(isset($data['title']))
                    <label>Title</label> {!! $data['title'] !!} <br>
                @endif
                @if(isset($data['date_sent_to_client']))
                    <label>Date Sent To Client</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_sent_to_client']) !!} <br>
                @endif
                @if(isset($data['date_received_from_client']))
                    <label>Date Received From Client</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_received_from_client']) !!} <br>
                @endif
                @if(isset($data['date_sent_to_lawyer']))
                    <label>Date Sent To lawyer</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_sent_to_lawyer']) !!} <br>
                @endif
                @if(isset($data['date_signed']))
                    <label>Date Signed</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_signed']) !!} <br>
                @endif
                @if(isset($data['document_id']))
                    <label>Sales Agreement</label> <a href="/dashboard/documents/{!! $data['document_id'] !!}">View</a> <br>
                @endif
            </div>
        </div>
    </div>
</div>