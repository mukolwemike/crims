<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <a href="{!! route('approval_action', [$approval->id, 'excel']) !!}" class="btn btn-info margin-bottom-20 pull-right"><i class="fa fa-file-excel-o"></i> Download</a>
            <p>From: {!! \Cytonn\Presenters\DatePresenter::formatDate($bulk->start) !!} To: {!! \Cytonn\Presenters\DatePresenter::formatDate($bulk->end) !!}</p>
            <p>Description: {!! $bulk->description !!}</p>
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>FA</th>
                        @foreach($currencies as $currency)
                            <th>{!! $currency->code !!}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($recipients as $fa)
                        <tr>
                            <td>{!! $fa->name !!}</td>
                            @foreach($currencies as $currency)
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($calculate($fa, $currency)) !!}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>

             {!! $recipients->links() !!}
        </div>
    </div>
</div>