<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Delete Consent Form</h4>
        </div>

        <div class = "panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{{$client->name()}}</td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td>{!! $consentForm['email'] !!}</td>
                </tr>

                <tr>
                    <td>Consented</td>
                    @if($consentForm['consented'] == 1)
                        <td class="text-success"><span class="glyphicon glyphicon-check"></span></td>
                    @else
                        <td class="text-danger"><span class="glyphicon glyphicon-remove"></span></td>
                    @endif
                </tr>

                <tr>
                    <td>File</td>
                    @if(isset($consentForm['document_id']))
                        <td><span class="glyphicon glyphicon-file"></span> <a target="_blank" href="/dashboard/investments/client-instructions/filled-application-documents/{!! $consentForm['document_id'] !!}">View file</a></td>
                    @else
                        <td>No file uploaded</td>
                    @endif
                </tr>

                <tr>
                    <td>Reason for Delete</td>
                    <td>{{$data['reason']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>