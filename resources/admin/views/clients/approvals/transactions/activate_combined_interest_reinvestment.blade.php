<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Activate/Deactivate combined interest re-investment plan</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Client Name</td>
                        <td colspan="2">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Client Code</td>
                        <td colspan="2">{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>New Details</th>
                        <th>Old Details</th>
                    </tr>
                    <tr>
                        <td>Interest Reinvestment Action</td>
                        <td>
                            @if(isset($data['combine_interest_reinvestment']))
                                @if($data['combine_interest_reinvestment'] == 0)
                                    Deactivate Combine Interest Reinvestment
                                @elseif($data['combine_interest_reinvestment'] == 1)
                                    Combine All Reinvested Interest
                                @elseif($data['combine_interest_reinvestment'] == 2)
                                    Combined Reinvested Interest Monthly
                                @else
                                    Deactivate Combine Interest Reinvestment
                                @endif
                            @else
                                Deactivate Combine Interest Reinvestment
                            @endif
                        </td>
                        <td>
                            @if($client->combine_interest_reinvestment == 0 || is_null($client->combine_interest_reinvestment))
                                Deactivate Combine Interest Reinvestment
                            @elseif($client->combine_interest_reinvestment == 1)
                                Combine All Reinvested Interest
                            @elseif($client->combine_interest_reinvestment == 2)
                                Combined Reinvested Interest Monthly
                            @else
                                Deactivate Combine Interest Reinvestment
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Combined Interest Re-investment Tenor</td>
                        <td>
                            @if(isset($data['combine_interest_reinvestment_tenor']))
                                {!! $data['combine_interest_reinvestment_tenor'] !!} Months
                            @endif
                        </td>
                        <td>{!! $client->combine_interest_reinvestment_tenor ? $client->combine_interest_reinvestment_tenor . ' Months' : '' !!}</td>
                    </tr>
                    <tr>
                        <td width="30%">Currently Active</td>
                        <td colspan="2">{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($client->combinedInterest()) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>