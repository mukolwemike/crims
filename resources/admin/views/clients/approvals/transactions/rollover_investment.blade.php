<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>

        @if(!$approved && $approvals->count())
            <div class="alert alert-warning">
                <p><b>This instruction require that it is approved by authorised persons as in the client signing mandate.</b></p>
                <br>
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Signatory</th>
                        <th>Approval Status</th>
                        <th>Reason</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($approvals as $signatory)
                        <tr>
                            <td>{{ $signatory['user'] }}</td>
                            <td>
                                @if(!$signatory['approval_status'])<el-tag>{{ $signatory['status'] }}</el-tag>@endif
                                @if($signatory['approval_status'] == 1)<el-tag type="success">{{ $signatory['status'] }}</el-tag>@endif
                                @if($signatory['approval_status'] == 2)<el-tag type="danger">{{ $signatory['status'] }}</el-tag>@endif
                            </td>
                            <td>{{ isset($signatory['reason']) ? $signatory['reason'] : 'N\A' }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th width="20%">Reason to continue with processing:</th>
                        <td>
                            @if(isset($data['reason']))
                                <span> {{ $data['reason'] }} </span>
                            @else
                                <span>Not provided</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @else
        @endif

        <div class="panel-body">

            <h3>Investments to be Rolled over</h3>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr><td>Product</td><td>{!! $investment->product->name!!}</td></tr>
                <tr>
                    <td>Source</td>
                    <td>
                        @if($instruction->user_id)
                            <span class="label label-primary">Client</span>
                        @else
                            <span class="label label-info">Admin</span>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-responsive table-hover">
                <thead>
                <tr><th>Principal</th><th>Rate</th>
                    <th>Value Date</th><th>Maturity Date</th>
                    <th>Rollover Date</th>
                    <th>Net Interest</th><th>Value</th>
                </tr>
                </thead>
                <tbody>
                @foreach($investments as $investment)
                    <tr>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($investment->amount) }}</td>
                        <td>{{ $investment->interest_rate }}%</td>
                        <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) }}</td>
                        <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) }}</td>
                        <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($instruction->due_date) }}</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($investment->net_interest) }}</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($investment->total_value) }}</td>
                    </tr>
                @endforeach
                @if($instruction->topup_amount > 0)
                    <tr>
                        <td>Topup</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($instruction->topup_amount) }}</td>
                    </tr>
                @endif
                <?php $seipChildrenTotal = 0 ?>
                @if($investment->repo->isSeipParent())
                    <?php $seipChildrenTotal = $investment->repo->getSeipChildrenInvestedValueAsAtDate($instruction->due_date) ?>
                    <tr>
                        <td>Investment Contributions</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($seipChildrenTotal) }}</td>
                    </tr>
                @endif
                <tr>
                    <th>Total</th><td colspan="5"></td><th>{{ \Cytonn\Presenters\AmountPresenter::currency($total = $instruction->topup_amount + $investments->sum('total_value') + $seipChildrenTotal) }}</th>
                </tr>
                <tr>
                    <td>Rollover</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::accounting($r = -1 * $amount) }}</td>
                </tr>
                <tr>
                    <td>Withdraw</td><td colspan="5"></td><td>{{ \Cytonn\Presenters\AmountPresenter::accounting(-1 * ($total + $r)) }}</td>
                </tr>
                </tbody>
            </table>

            <h3>New Investment Details</h3>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                @if($instruction->automatic_rollover)
                    <tr class="warning"><td>Automatic Rollover</td><td>{{ \Cytonn\Presenters\BooleanPresenter::presentYesNo($instruction->automatic_rollover) }}</td></tr>
                @endif
                <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($invested_date) !!}</td></tr>
                <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($maturity_date) !!}</td></tr>
                <tr><td>Total Days</td><td>{!! \Cytonn\Presenters\DatePresenter::getDateDifference($invested_date, $maturity_date) !!}</td></tr>
                <tr><td>FA</td><td>{!! $commissionRecepient->name !!}</td></tr>
                <tr><td>Current FA Position</td><td>{!! $commissionRecepient->type->name !!}</td></tr>
                <tr><td>Commission Rate</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                @if(isset($data['commission_rate_name']))
                    <tr><td>Commission Rate Name</td><td>{!! $data['commission_rate_name'] !!}</td></tr>
                @endif
                @if(isset($data['commission_start_date']) && $data['commission_start_date'])
                    <tr><td>Commission Start Date</td><td>{!! $data['commission_start_date'] !!}</td></tr>
                @endif
                <tr><td>Previous FA Position</td><td>{!! $commissionRecepient->previous ? $commissionRecepient->previous->name : '' !!}</td></tr>
                <tr><td>Interest payment</td><td>{!! $interestPayment[$data['interest_payment_interval']] !!} on date {!! $data['interest_payment_date'] !!}</td></tr>
                @if(isset($data['interest_action_id']))
                    <tr><td>Interest Action</td><td>{!! $interestAction($data['interest_action_id'])->name !!}</td></tr>
                @endif
                @if(isset($data['description']))
                    <tr><td>Transaction Description</td><td>{!! $data['description'] !!}</td></tr>
                @endif

                @if($instruction->account)
                    <tr>
                        <th colspan="2">Selected Account</th>
                    </tr>

                    <tr>
                        <td>Bank Name</td>
                        <td>{{ $instruction->account->bank->name }}</td>
                    </tr>

                    <tr>
                        <td>Branch Name</td>
                        <td>{{ $instruction->account->branch->name }}</td>
                    </tr>

                    <tr>
                        <td>Account Name</td>
                        <td>{{ $instruction->account->account_name }}</td>
                    </tr>

                    <tr>
                        <td>Account Number</td>
                        <td>{{ $instruction->account->account_number }}</td>
                    </tr>
                @endif

                <tr><td><b>Previous Investment Details</b></td><td></td></tr>
                <tr><td>FA</td><td>{!! $investment->present()->commissionRecipientName !!}</td></tr>
                <tr><td>Interest Payment</td><td>{!! $investment->present()->getInterestPaymentDetails !!}</td></tr>
                @if($investment->interestAction)
                    <tr>
                        <td>Interest Action</td>
                        <td>{!! $investment->present()->getInterestActionDetails !!}</td>
                    </tr>
                @endif
                @if(isset($data['interest_reinvest_tenor']))
                    <tr>
                        <td>Interest Reinvest Tenor</td>
                        <td>
                            @if($data['interest_reinvest_tenor'] != 0)
                                {!! $data['interest_reinvest_tenor'] !!} months
                            @else
                                Until maturity of principal
                            @endif
                        </td>
                    </tr>
                @endif
                @if($instruction->investment->product->present()->isSeip)
                    <tr>
                        <td><b>SEIP Contribution Details</b></td>
                        <td></td>
                    </tr>
                @endif
                @if(isset($data['contribution_amount']))
                    <tr><td>SEIP Contribution Amount</td><td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($data['contribution_amount']) !!}</td></tr>
                @endif
                @if(isset($data['contribution_payment_interval']))
                    <tr>
                        <td>SEIP Contribution Interval</td>
                        <td>{!! $interestPayment[$data['contribution_payment_interval']] !!} on
                            date {!! $data['contribution_payment_date'] !!}</td>
                    </tr>
                @endif
                @if(count($periodicRates) > 0)
                    <tr>
                        <td>SEIP Commission Rates</td>
                        <td>
                            @foreach($periodicRates as $periodicRate)
                                Year {!! $periodicRate->tenor !!} at {!! $periodicRate->rate !!}%<br>
                            @endforeach
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Action</h4>
        </div>
        <div class="panel-body">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                View Client Details
            </button>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Client Investment Application</h4>
            </div>
            <div class="modal-body">
                @include('clients.clientdetails')
                {{--@include('unitization.partials.application_partial')--}}
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
