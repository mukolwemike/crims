<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Unit Fund Fee</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <td>Fund Name</td>
                    <td colspan="2">{!! $fund->name !!}</td>
                </tr>
                <tr>
                    <th></th>
                    <th>New</th>
                    <th>Current</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! $type->name !!}</td>
                    <td>{!! $fee->type->name !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    <td>{!! $fee->date->toDayDateTimeString() !!}</td>
                </tr>
                <tr>
                    <td>Applied</td>
                    <td>{!! $chargeType !!}</td>
                    @if($fee->feeChargeType)
                        <td>{!! $fee->feeChargeType->name !!}</td>
                    @else
                    @endif
                </tr>
                <tr>
                    <td>Frequency:</td>
                    <td>{!! $frequency !!}</td>
                    @if($fee->feeChargeFrequency)
                        <td>{!! $fee->feeChargeFrequency->name !!}</td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    <td>Dependent</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['dependent']) !!}</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($fee->dependent) !!}</td>
                </tr>
                @if($data['dependent'])
                    <tr>
                        <td>Fee Parameter</td>
                        <td>{!! \App\Cytonn\Models\Unitization\UnitFundFeeParameter::findOrFail($data['unit_fund_fee_parameter_id'])->name !!}</td>
                        <td>
                            @if($fee->dependent && $fee->percentages->count() > 0)
                                {!! $fee->percentages()->latest()->first()->parameter->name !!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Percentage</td>
                        <td>{!! $data['percentage'] !!}%</td>
                        <td>
                            @if($fee->dependent)
                                {!! $fee->getPercentage() !!}
                            @endif
                        </td>
                    </tr>
                @else
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                        <td>
                            @if(! $fee->dependent)
                                {!! $fee->getAmount() !!}
                            @endif
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>