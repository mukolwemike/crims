<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Settle Share Purchases from Sale Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Seller</td>
                        <td colspan="8">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($seller->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td colspan="8">{!! $seller->number !!}</td>
                    </tr>
                    <tr>
                        <td>Current shares</td>
                        <td colspan="8">{!! $seller->shareHoldings->sum('number') !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td colspan="8">{!! $seller->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td colspan="8">{!! \Cytonn\Presenters\DatePresenter::formatDate($salesOrder->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares for Sale</td>
                        <td colspan="8">{!! \Cytonn\Presenters\AmountPresenter::currency($salesOrder->number, true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares Already Sold</td>
                        <td colspan="8">{!! \Cytonn\Presenters\AmountPresenter::currency($sold, true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Balance available for sale</td>
                        <td colspan="8">{{ \Cytonn\Presenters\AmountPresenter::currency($balance, true, 0) }}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td colspan="8">{!! \Cytonn\Presenters\AmountPresenter::currency($salesOrder->price) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>TOTAL</td>
                        <td colspan="8">{!! \Cytonn\Presenters\AmountPresenter::currency($total = $salesOrder->price * $salesOrder->number) !!}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <th>Buyer</th>
                    <th>Shareholder Number</th>
                    <th>Number</th>
                    <th>Price</th>
                    <th>Date</th>
                    <th>FA</th>
                    <th>Comm. Rate</th>
                </tr>
                </thead>
                <tbody>
                @foreach($purchases as $purchase)
                    <tr>
                        <td>{!! $purchase['fullName'] !!}</td>
                        <td>{!! $purchase['number'] !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase['share_number'], false, 0) !!}</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($purchase['share_price'], false, 0) }}</td>
                        <td>{{ $purchase['date'] }}</td>
                        <td>{!! $purchase['recipient'] !!}</td>
                        <td>{!! $purchase['commission_rate'] !!}%</td>
                    </tr>
                @endforeach
                {{--<tr>--}}
                    {{--<th>Total</th><td colspan="10"></td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($bought) !!}</th>NOTE: use the sale order price--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<th>Balance</th><td colspan="10"></td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($total - $bought) !!}</th>--}}
                {{--</tr>--}}
                </tbody>
            </table>
        </div>
    </div>
</div>