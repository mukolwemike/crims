<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                <tr><td>Project</td><td colspan="3">{!! $holding->project->name !!}</td></tr>
                <tr><td>Unit</td><td colspan="3">{!! $holding->unit->number !!}</td></tr>
                <tr><td>Size</td><td colspan="3">{!! $holding->unit->size->name !!}</td></tr>
                <tr><td>Type</td><td colspan="3">{!! $holding->unit->type->name !!}</td></tr>
                <tr><td>Price</td><td colspan="3">{!! $holding->price() !!}</td></tr>
                <tr>
                    <th>Description</th>
                    <th>Scheduled Amount</th>
                    <th>Payment Type</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <?php
                        $paymentSchedule = App\Cytonn\Models\RealEstatePaymentSchedule::find($item);
                    ?>
                    @if($paymentSchedule)
                        <tr>
                            <td>{!! $paymentSchedule->description !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($paymentSchedule->amount) !!}</td>
                            <td>{!! @$paymentType($paymentSchedule->payment_type_id)->name !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($paymentSchedule->date) !!}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>

        @if($schedules && !$approval->approved)
            <div class="panel-heading">
                <h4>Existing Payment Schedules</h4>
            </div>
            <div class="panel-body">
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Payment Type</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($schedules as $schedule)
                        <tr @if(in_array($schedule->id, $items)) style="background-color: yellow" @endif>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                            <td>{!! $schedule->description !!}</td>
                            <td>{!! $schedule->type->name !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr style="background-color: yellow; color: red;">
                        <th colspan="2">TOTAL</th>
                        <th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
                    </tr>
                    @if($paymentSchedule)
                        <tr>
                            <th colspan="2">TOTAL AFTER APPROVAL</th>
                            <th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($total - $paymentSchedule->amount) !!}</th>
                        </tr>
                    @endif
                    </tfoot>
                </table>
            </div>
        @endif

    </div>
</div>