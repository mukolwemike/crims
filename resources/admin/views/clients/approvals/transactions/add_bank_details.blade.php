<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Bank Details</h4>
        </div>
        <div class="panel-body">
            @if(isset($data['id']))
                <h5>Edit Bank Details</h5>

                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th></th><th>Account Name</th><th>AccountNumber</th><th>Bank</th><th>Branch</th><th>Currency</th><th>Clearing Code</th><th>Swift Code</th><th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>From</td>
                            <td>{!! $account->account_name !!}</td>
                            <td>{!! $account->account_number !!}</td>
                            @if($oldBranch)
                                <td>{!! $oldBranch->bank->name !!}</td>
                                <td>{!! $oldBranch->name !!}</td>
                                <td>{!! $account->currency ? $account->currency->name : '-' !!}</td>
                                <td>{!! $oldBranch->bank->clearing_code !!}</td>
                                <td>{!! $oldBranch->bank->swift_code !!}</td>
                            @else
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                            <td>
                                @if($account->active === 0)
                                    <el-tag type="danger" size="small">in-active</el-tag>
                                @else
                                    <el-tag type="success" size="small">active</el-tag>
                                @endif
                            </td>
                        </tr>
                        <tr class="success">
                            <td>To</td>
                            <td>{!! $data['investor_account_name'] !!}</td>
                            <td>{!! $data['investor_account_number'] !!}</td>
                            @if($branch)
                                <td>{!! $bank->name !!}</td>
                                <td>{!! $branch->name !!}</td>
                                <td>{!! $currency->name !!}</td>
                                <td>{!! $bank->clearing_code !!}</td>
                                <td>{!! $bank->swift_code !!}</td>
                            @else
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                            <td>
                                @if($data['active'] == 0)
                                    <el-tag type="danger" size="small">in-active</el-tag>
                                @else
                                    <el-tag type="success" size="small">active</el-tag>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            @else
                <h5>Add Bank Details</h5>

                <table class="table table-responsive table-striped">
                    <thead>
                        <tr><th>Account Name</th><th>AccountNumber</th><th>Bank</th><th>Branch</th><th>Currency</th><th>Clearing Code</th><th>Swift Code</th><th>Status</th></tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{!! $data['investor_account_name'] !!}</td>
                            <td>{!! $data['investor_account_number'] !!}</td>
                            @if($branch)
                                <td>{!! $bank->name !!}</td>
                                <td>{!! $branch->name !!}</td>
                                <td>{!! $currency->name !!}</td>
                                <td>{!! $bank->clearing_code !!}</td>
                                <td>{!! $bank->swift_code !!}</td>
                            @else
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                            <td>
                                @if(isset($data['active']) && $data['active'] == 0)
                                    <el-tag type="danger" size="small">in-active</el-tag>
                                @else
                                    <el-tag type="success" size="small">active</el-tag>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            @endif
        </div>
    </div>


</div>