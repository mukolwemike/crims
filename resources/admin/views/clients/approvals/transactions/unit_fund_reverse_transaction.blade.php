<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>
                @if($data['type'] == 'unit_fund_purchase')
                    Unit Fund Purchase Reversal
                @elseif($data['type'] == 'unit_fund_sale')
                    Unit Fund Sale Reversal
                @elseif($data['type'] == 'unit_fund_transfer')
                    Unit Fund Transfer Reversal
                @endif
            </h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                @if($data['type'] == 'unit_fund_purchase')
                    <tbody>
                        <tr>
                            <td width="30%">Unit Fund</td>
                            <td>{!! $fund->name !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Amount Invested</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($purchase->payment->amount + $purchase->fee)) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Number of Units</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number = $purchase->number, true, 0) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Unit Price</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($price = $fund->unitPrice(\Carbon\Carbon::parse($purchase->date))) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Value of Units</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number * $price) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Amount of Fee Paid</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency(abs($purchase->fee)) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($purchase->date) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Reason for reverse</td>
                            <td>{!! $data['reason'] !!}</td>
                        </tr>
                    </tbody>
                @elseif($data['type'] == 'unit_fund_sale')
                    <tbody>
                        <tr>
                            <td width="30%">Unit Fund</td>
                            <td>{!! $fund->name !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Number of Units</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number = $sale->number, true, 0) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Unit Price</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($price = $fund->unitPrice(\Carbon\Carbon::parse($sale->date))) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Value of Units</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number * $price) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Fee Charged</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($sale->fee) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Reason for Reversal</td>
                            <td>{!! $data['reason'] !!}</td>
                        </tr>
                    </tbody>
                @elseif($data['type'] == 'unit_fund_transfer')
                    <tbody>
                        <tr>
                            <td width="30%">Unit Fund</td>
                            <td>{!! $fund->name !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Number of Units</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number = $transfer->number, true, 0) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Unit Price</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($price = $fund->unitPrice(\Carbon\Carbon::parse($transfer->date))) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Value of Units</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number * $price) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Fee Charged</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($transfer->fee) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Sender</td>
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($transfer->sender->id) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Recipient</td>
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($transfer->recipient->id) !!}</td>
                        </tr>
                        <tr>
                            <td width="30%">Reason for Reversal</td>
                            <td>{!! $data['reason'] !!}</td>
                        </tr>
                    </tbody>
                @endif
            </table>
        </div>
    </div>
</div>