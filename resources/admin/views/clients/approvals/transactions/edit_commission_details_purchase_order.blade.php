<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4 class="text-capitalize">editing of commission payment details</h4>
        </div>

        <div class = "panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <td></td>
                    <td>Before</td>
                    <td>After</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Commission Recipient</td>
                    <td>{!! \App\Cytonn\Presenters\CommissionPresenter::presentRecipient($data['existing_commission_recipient']) !!}</td>
                    <td>{!! \App\Cytonn\Presenters\CommissionPresenter::presentRecipient($data['commission_recepient']) !!}</td>
                </tr>
                <tr>
                    <td>Commission Rate</td>
                    <td>{!! $data['existing_commission_rate'] !!}%</td>
                    <td>{!! $data['commission_rate'] !!}%</td>
                </tr>
                <tr>
                    <td>Commission Rate Name</td>
                    <td>{!! $data['existing_commission_rate'] !!}%</td>
                    <td>{!! $data['commission_rate_name'] !!}</td>
                </tr>
                <tr>
                    <td>Commission Start Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['exiting_commission_start_date']) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['commission_start_date']) !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>