<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr><td>Product</td><td>{!!$investment->product->name!!}</td></tr>
                    <tr><td>Investment value at Transfer</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($valueAtTransfer)!!}</td></tr>
                    <tr><td>Current Investment value</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($value)!!}</td></tr>
                    <tr><td>Interest Rate</td><td>{!! $investment->interest_rate !!}%</td></tr>
                    <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td></tr>
                    <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                    <tr><td>FA</td><td>{!! $investment->commission->recipient->name !!}</td></tr>
                    <tr><td>Commission Rate</td><td>{!! $investment->commission->rate !!}%</td></tr>
                    <tr><td colspan="100%"><h5>Transfer to</h5></td></tr>
                    <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($dest->id) !!}</td></tr>
                    <tr><td>Client Code</td><td>{!! $dest->client_code !!}</td></tr>
                    <tr><td>Narrative</td><td>{!! $data['narration'] !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>


</div>