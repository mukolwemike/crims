<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr><td>Project</td><td>{!! $project->name !!}</td></tr>
                    <tr><td>Unit</td><td>{!! $unit->number !!}</td></tr>
                    <tr><td>Size</td><td>{!! $unit->size->name !!} {!! $unit->type->name !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td></tr>
                    <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td></tr>
                    <tr><td>Description</td><td>{!! $payment->description !!}</td></tr>
                    <tr><td>Payment</td><td>{!! $payment->schedule->description !!}</td></tr>
                    <tr><td>Price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>