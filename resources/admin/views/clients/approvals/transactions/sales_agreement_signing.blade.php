<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Sales Agreement Signed</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <label>Client Name</label> {!! \Cytonn\Presenters\ClientPresenter::presentJointFirstNameLastName($client->client_id) !!}<br/>
                <label>Project </label> {!! $project->name !!}<br/>
                <label>Unit </label> {!! $unit->number !!}<br/>
                <label>Date</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!} <br>
                <label>Sales Agreement</label> <a href="/dashboard/realestate/reservations/show-sales-agreement/{!! $salesAgreement->id !!}">View</a> <br>
            </div>
        </div>
    </div>
</div>