<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Issue Entity Shares</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Entity</td><td>{!! $entity->name !!}</td></tr>
                    <tr><td>Max Holding</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->maximumIssues($data['issue_date']), true, 0) !!}</td></tr>
                    <tr><td>Share Price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->shareprice($data['issue_date'])) !!}</td></tr>
                    <tr><td>Sold Shares</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->soldShares($data['issue_date']), true, 0) !!}</td></tr>
                    <tr><td>Remaining Shares</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($entity->remainingShares($data['issue_date']), true, 0) !!}</td></tr>
                    <tr><td>Shares to be Issued</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], true, 0) !!}</td></tr>
                    <tr><td>Price per Share</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['price']) !!}</td></tr>
                    <tr><td>Issue Date</td><td>{!! (new \Carbon\Carbon($data['issue_date']))->format('l jS \\of F Y'); !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>