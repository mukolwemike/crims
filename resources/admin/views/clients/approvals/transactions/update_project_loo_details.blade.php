<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Project LOO Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead>
                    <tr>
                        <th width="30%"></th>
                        <th width="30%">Current</th>
                        <th>New</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Long Name</td>
                        <td>{!! $project->long_name !!}</td>
                        <td>{!! $data['long_name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Vendor</td>
                        <td>{!! $project->vendor !!}</td>
                        <td>{!! $data['vendor'] !!}</td>
                    </tr>
                    <tr>
                        <td>Vendor Address</td>
                        <td>{!! $project->vendor_address !!}</td>
                        <td>{!! $data['vendor_address'] !!}</td>
                    </tr>
                    <tr>
                        <td>Completion Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($project->completion_date)  !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['completion_date'])  !!}</td>
                    </tr>
                    <tr>
                        <td>County Government</td>
                        <td>{!! $project->county_government !!}</td>
                        <td>{!! $data['county_government'] !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Name</td>
                        <td>{!! $project->bank_name !!}</td>
                        <td>{!! $data['bank_name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Branch Name</td>
                        <td>{!! $project->bank_branch_name !!}</td>
                        <td>{!! $data['bank_branch_name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Name</td>
                        <td>{!! $project->bank_account_name !!}</td>
                        <td>{!! $data['bank_account_name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Number</td>
                        <td>{!! $project->bank_account_number !!}</td>
                        <td>{!! $data['bank_account_number'] !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Swift Code</td>
                        <td>{!! $project->bank_swift_code !!}</td>
                        <td>{!! $data['bank_swift_code'] !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Clearing Code</td>
                        <td>{!! $project->bank_clearing_code !!}</td>
                        <td>{!! $data['bank_clearing_code'] !!}</td>
                    </tr>
                    <tr>
                        <td>Development Description</td>
                        <td>{!! $project->development_description !!}</td>
                        <td>{!! $data['development_description'] !!}</td>
                    </tr>
            </table>
        </div>

        <div class="panel-heading">
            <h4>Project Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! $project->name !!}</td>
                </tr>
                <tr>
                    <td>Code</td>
                    <td>{!! $project->code !!}</td>
                </tr>
                <tr>
                    <td>Reservation Fees</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($project->reservation_fee) !!}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>{!! $projectType->name  !!}</td>
                </tr>
                <tr>
                    <td>Commission calculations</td>
                    <td>{!! $calculators[$project->commission_calculator] !!}</td>
                </tr>
            </table>
        </div>
    </div>
</div>