<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Unit Fund Statement Campaign</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>New</th>
                        <th>Current</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Campaign Name</td>
                        <td>{!! $data['name'] !!}</td>
                        <td>{!! $campaign->name !!}</td>
                    </tr>
                    <tr>
                        <td>Campaign Type</td>
                        <td>{!! $type->name !!}</td>
                        <td>{!! $campaign->type->name !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['send_date']) !!}%</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($campaign->send_date) !!}%</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>