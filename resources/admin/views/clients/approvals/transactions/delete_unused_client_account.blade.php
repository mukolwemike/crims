<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Delete Unused Client Account</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <tbody>
                    <tr>
                        <td>Active Investments</td><td>{!! $client->investments(function($investment) { return $investment->active(); })->count() !!}</td>
                    </tr>
                    <tr>
                        <td>Active Unit Holdings</td><td>{!! $client->unitHoldings(function($holding) { return $holding->where('active', true); })->count() !!}</td>
                    </tr>
                    <tr>
                        <td>Active Share Holdings</td><td>{!! $client->shareHoldings()->count() !!}</td>
                    </tr>
                    <tr>
                        <td>Complete?</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($client->complete)  !!}</td>
                    </tr>
                    <tr>
                        <td>Client Code</td><td>{!! $client->client_code !!}</td>
                    </tr>
                    @if($client->contact->entityType->name == 'individual')
                        <tr>
                            <td>Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                        </tr>
                        <tr>
                            <td>Preferred Name</td><td> @if(is_null($client->preferredname)) <em> null </em> @endif{!! ($client->preferredname) !!}</td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($client->dob) !!}</td>
                        </tr>
                        <tr>
                            <td>Residence</td><td>{!! $client->residence !!}</td>
                        </tr>
                    @else
                        <tr>
                            <td>Registered Name</td><td>{!! $client->contact->corporate_registered_name !!}</td>
                        </tr>
                        <tr>
                            <td>Trade Name</td><td>{!! $client->contact->corporate_trade_name !!}</td>
                        </tr>
                        <tr>
                            <td>Registered Address</td><td>{!! $client->contact->registered_address !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Email</td><td>{!! $client->contact->email !!}</td>
                    </tr>
                    <tr>
                        <td>Home Telephone</td><td>{!! $client->telephone_home !!}</td>
                    </tr>
                    <tr>
                        <td>Cell phone number</td><td>{!! $client->phone !!}</td>
                    </tr>
                    <tr>
                        <td>Office phone number</td><td>{!! $client->telephone_office !!}</td>
                    </tr>
                    <tr>
                        <td>Pin Number</td><td>{!! $client->pin_no !!}</td>
                    </tr>
                    <tr>
                        <td>Taxable</td><td>{!! $client->taxable !!}</td>
                    </tr>
                    <tr>
                        <td>ID/Passport Number</td><td>{!! $client->id_or_passport !!}</td>
                    </tr>
                    <tr>
                        <td>Method of Contact</td><td>{!! $client->method_of_contact_id !!}</td>
                    </tr>
                    <tr>
                        <td>Business Sector</td><td>{!! $client->business_sector !!}</td>
                    </tr>
                    <tr>
                        <td>Postal Code</td><td>{!! $client->postal_code !!}</td>
                    </tr>
                    <tr>
                        <td>Postal Address</td><td>{!! $client->postal_address !!}</td>
                    </tr>
                    <tr>
                        <td>Country</td><td>{!! $client->country_id !!}</td>
                    </tr>
                    <tr>
                        <td>Town</td><td>{!! $client->town !!}</td>
                    </tr>
                    <tr>
                        <td>Street</td><td>{!! $client->street !!}</td>
                    </tr>
                    <tr>
                        <td>Account Name or Tag</td><td>{!! $client->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Name</td><td>{!! $client->investor_account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account Number</td><td>{!! $client->investor_account_number !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Name</td><td>{!! $client->investor_bank !!}</td>
                    </tr>
                    <tr>
                        <td>Branch</td><td>{!! $client->investor_bank_branch !!}</td>
                    </tr>
                    <tr>
                        <td>Clearing Code</td><td>{!! $client->investor_clearing_code !!}</td>
                    </tr>
                    <tr>
                        <td>Swift Code</td><td>{!! $client->investor_swift_code !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>