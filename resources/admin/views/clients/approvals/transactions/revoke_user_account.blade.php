<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Assign Access to User Account</h4>
        </div>
        <div class="panel-body">
            <div class="well-lg">
                <h5>User details</h5>
                <table class="table table-hover table-responsive">
                    <tbody>
                    <tr>
                        <td>Username</td><td>{!! $user->username !!}</td>
                    </tr>
                    <tr>
                        <td>Full Name</td><td>{!! $user->firstname.' '.$user->lastname !!}</td>
                    </tr>
                    <tr>
                        <td>Email</td><td>{!! $user->email !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>