<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Send Maturity notifications</h4>
        </div>
        <div class="panel-body">
            <div class="well-lg">
                <p>Maturity Notifications for {!! \Cytonn\Presenters\DatePresenter::formatFullDate($data['date']) !!}</p>
            </div>
        </div>
    </div>
</div>