<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Cancel Client {!! $instruction->type->name !!} Instruction</h4>
        </div>
        <div class="panel-body">
            <h5>{!! $instruction->type->name !!} Details</h5>
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Client</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($instruction->investment->client_id) !!}</td></tr>
                    <tr><td>Type</td><td>{!! ucfirst($instruction->type->name) !!}</td></tr>
                    <tr><td>Tenor</td><td>{!! $instruction->tenor !!} Months</td></tr>
                    <tr><td>Amount</td><td>{!! $instruction->repo->showAmountSelect() !!}  ({!! $instruction->investment->product->currency->code !!} {!! \Cytonn\Presenters\AmountPresenter::currency($instruction->repo->calculateAmountAffected()) !!})</td></tr>
                    <tr><td>Agreed Rate</td><td>{!! $instruction->agreed_rate !!}%</td></tr>
                    <tr><td>Tenor</td><td>{!! $instruction->tenor !!} Months</td></tr>
                    @if($instruction->filledBy)
                    <tr><td>Filled By</td><td>{!! $instruction->filledBy->firstname.' '.$instruction->filledBy->lastname !!}</td></tr>
                    @endif
                    <tr><td>Withdrawal Stage</td><td>{!! ucfirst($instruction->withdrawal_stage) !!} on {!! \Cytonn\Presenters\DatePresenter::formatDate( $instruction->due_date)  !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>