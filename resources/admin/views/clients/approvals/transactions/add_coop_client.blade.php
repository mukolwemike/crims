<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>New Coop Client Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Name</td><td>{!! @$titles($form->title_id)->name.' '.$form->firstname.' '.$form->middlename.' '.$form->lastname !!}</td></tr>
                    @if($form->dob)
                        <tr>
                            <td>Date of Birth</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($form->dob) !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
                View Form
            </button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Coop Application Form</h4>
                        </div>
                        <div class="modal-body">
                            @if($form->client_type_id == 1 || $form->client_type_id == NULL)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span>Personal Details</span>
                                    </div>

                                <div class="panel-body form-detail">
                                    <div class="col-md-6">
                                        <label for="">Name</label>  {!! ($form->title_id != null && $form->title_id != '') ? @$titles($form->title_id)->name : ''.' '.$form->firstname.' '.$form->middlename.' '.$form->lastname !!}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Date of Birth</label>  {!! \Cytonn\Presenters\DatePresenter::formatDate($form->dob) !!}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Email</label> {!! $form->email !!}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Phone</label> {!! $form->phone !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Pin Number</label> {!! $form->pin_no !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">ID/Passport Number</label> {!! $form->id_or_passport !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Postal Code</label> {!! $form->postal_code !!}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Postal Address</label> {!! $form->postal_address !!}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Country</label> {!! $country->name !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Employment Status</label> {!! $employment->name !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Town</label> {!! $form->town !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Street</label> {!! $form->street !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Referee</label> {!! @\Cytonn\Presenters\ClientPresenter::presentFullNames($form->referee_id) !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                </div>
                            @else
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span>Corporate Details</span>
                                    </div>

                                    <div class="panel-body form-detail">
                                        <div class="col-md-6">
                                            <label for="">Name</label>  {!! $form->registered_name !!}
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Email</label> {!! $form->email !!}
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Phone</label> {!! $form->phone !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Pin Number</label> {!! $form->pin_no !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Registered Address</label> {!! $form->regitered_address !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Postal Address</label> {!! $form->postal_address !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Postal Code</label> {!! $form->postal_code !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Country</label> {!! $country->name !!}
                                        </div>


                                        <div class="col-md-6">
                                            <label for="">Town</label> {!! $form->town !!}
                                        </div>

                                        <div class="col-md-6">
                                            <label for="">Referee</label> {!! @\Cytonn\Presenters\ClientPresenter::presentFullNames($form->referee_id) !!}
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            @endif

                            <div class="panel panel-default">
                                <div class="panel-heading"><span>Membership Fee & Share Purchase</span></div>

                                <div class="panel-body form-detail">
                                    <div class="col-md-6">
                                        <label for="">Membership Fee</label> {!! \Cytonn\Presenters\AmountPresenter::currency($form->membership_fee) !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Share Purchase</label> {!! \Cytonn\Presenters\AmountPresenter::currency($form->share_purchase) !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Share Entity</label> {!! $shareentity->name !!}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Share Category</label> {!! $shareentity->name !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Mode of Payment</label> {!! $form->mode_of_payment !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Payment Options</label> Every {!! $form->payment_options !!} Months
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Amount Paid</label> {!! $form->amount !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Payment Date</label> {!! $form->date !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Narrative (For Payment)</label> {!! $form->narrative !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading"><span>Product Plans</span></div>

                                <div class="panel-body form-detail">
                                    <div class="col-md-6">
                                        <label for="">Shares per month</label> {!! $form->shares !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label>Product</label> {!! @$product->name !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Investment Duration (Years)</label> {!! $form->duration !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Monthly Payments</label> {!! \Cytonn\Presenters\AmountPresenter::currency($form->amount_paid_for_product) !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Payment Date</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($form->payment_date) !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading"><span>Nominees</span></div>

                                <div class="panel-body form-detail">
                                    <table class="table table-responsive table-striped">
                                        <thead>
                                        <tr>
                                            <th>Name</th><th>ID Number</th><th>Relationship</th><th>% Share</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($contact_persons as $person)
                                            <tr>
                                                <td>{!! $person['client_contact_person_name'] !!}</td>
                                                <td>{!! $person['id_number'] !!}</td>
                                                <td>{!! $clientpersonrelationship($person['relationship_id'])->relationship !!}</td>
                                                <td>{!! $person['percentage_share'] !!}%</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>