<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store Product Document</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">

                @if($productDocument)
                    <thead>
                    <tr><td>Item</td><td>Was</td><td>Now</td></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Product Name</td>
                        <td colspan="2">{!! $product->name !!}</td>
                    </tr>
                    <tr>
                        <td>Document Name</td>
                        <td>{!! $productDocument->name !!}</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Document</td>
                        <td colspan="2"><a target="_blank" href="/dashboard/documents/{!! $data['document_id'] !!}">View file</a></td>
                    </tr>
                    </tbody>
                @else
                    <thead>
                    <tr><td>Item</td><td>Details</td></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Product Name</td>
                        <td>{!! $product->name !!}</td>
                    </tr>
                    <tr>
                        <td>Document Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Document</td>
                        <td><a target="_blank" href="/dashboard/documents/{!! $data['document_id'] !!}">View file</a></td>
                    </tr>
                    </tbody>
                @endif
            </table>
        </div>
    </div>
</div>