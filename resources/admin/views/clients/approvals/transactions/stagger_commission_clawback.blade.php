<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Stagger Commission Clawback</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                @if(count($eligibleClawbacks) > 0)
                    <tr>
                        <td>Current Clawback Number</td>
                        <td>{!! count($eligibleClawbacks) !!}</td>
                    </tr>
                    <tr>
                        <td>Current Start Date</td>
                        <td>{!! $eligibleClawbacks[0]->date !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Staggered Number</td>
                    <td>{!! $data['stagger_duration'] !!}</td>
                </tr>
                <tr>
                    <td>Stagger Start Date</td>
                    <td>{!! $data['start_date'] !!}</td>
                </tr>
                @if($clawBack)
                    <tr>
                        <td>Clawback Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($clawBack->amount) !!}</td>
                    </tr>
                    @if($clawBack->deleted_at)
                        <tr class="danger">
                            <td>Clawback Deleted</td>
                            <td>The clawback has already been deleted</td>
                        </tr>
                    @endif
                @endif
            </table>
            @if(count($eligibleClawbacks) > 0)
                <table class="table table-hover table-responsive table-striped">

                    <tr>
                        <th colspan="4">Clawbacks To Stagger</th>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                        <th colspan="2">Narration</th>
                    </tr>
                    @foreach($eligibleClawbacks as $eligibleClawback)
                        <tr>
                            <td>{!! $eligibleClawback->date !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($eligibleClawback->amount) !!}</td>
                            <td colspan="2">{!! $eligibleClawback->narration !!}</td>
                        </tr>
                    @endforeach
                </table>
            @endif
        </div>
    </div>
</div>