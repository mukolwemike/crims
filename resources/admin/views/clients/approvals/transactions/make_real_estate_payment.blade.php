<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr><td>Project</td><td>{!! $project->name !!}</td></tr>
                    <tr><td>Unit</td><td>{!! $unit->number !!}</td></tr>
                    <tr><td>Size</td><td>{!! $unit->size->name !!} {!! $unit->type->name !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                    <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td></tr>
                    <tr><td>Description</td><td>{!! $data['description'] !!}</td></tr>
                    @if($schedule)
                    <tr ><td>Payment</td><td>{!! $schedule->description !!}</td></tr>
                    @endif
                    @if(count($existing_payments) == 0)
                        @if(isset($data['client_code']))
                            <tr>
                                <td>Client code</td>
                                <td>{!! $data['client_code'] !!}</td>
                            </tr>
                        @endif
                        @if(isset($data['tranche_id']) && strlen(trim($data['tranche_id'])) > 0)

                            <tr>
                                <td>Tranche</td>
                                <td>{!!  $unittranche($data['tranche_id'])->name !!}</td>
                            </tr>
                        @endif
                        @if(isset($data['payment_plan_id']))
                            @if(is_null($paymentplan($data['payment_plan_id'])))

                            @else
                                <tr>
                                    <td>Payment Plan</td>
                                    <td>{!! $paymentplan($data['payment_plan_id'])->name !!}</td>
                                </tr>
                            @endif
                        @endif
                        @if(isset($data['discount']))
                            @if((float)$data['discount'] > 0)
                                <tr>
                                    <td>Discount</td>
                                    <td>{!! $data['discount'] !!}%</td>
                                </tr>
                            @endif
                        @endif
                        @if(isset($data['negotiated_price']))
                            @if((int) $data['negotiated_price'] > 0)
                                <tr>
                                    <td>Negotiated price</td>
                                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['negotiated_price']) !!}</td>
                                </tr>
                            @endif
                        @else
                            <tr>
                                <td>Price (From tranche)</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($price) !!}</td>
                            </tr>
                        @endif
                        @if(isset($data['recipient_id']))
                            <tr>
                                <td>FA</td>
                                <td>{!! $commissionrecepient($data['recipient_id'])->name !!}</td>
                            </tr>
                            <tr>
                                <td>Current FA Position</td>
                                <td>{!! $commissionrecepient($data['recipient_id'])->type->name !!}</td>
                            </tr>
                        @endif
                        @if(isset($data['awarded']))
                            <tr>
                                <td>Award Commission?</td>
                                <td>
                                    @if($data['awarded']==0)
                                        NO
                                    @elseif($data['awarded']==1)
                                        YES
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if(isset($data['commission_rate']))
                            <tr>
                                <td>Commission Rate</td>
                                <td>{!! $commissionrate($data['commission_rate'])->recipient_type_and_amount !!}</td>
                            </tr>
                        @endif
                        @if(isset($data['rate']))
                            <tr>
                                <td>Commission Rate Percentage</td>
                                <td>{!! $data['rate'] !!}%</td>
                            </tr>
                        @endif
                        @if(isset($data['commission_rate_name']))
                            <tr>
                                <td>Commission Rate Name</td>
                                <td>{!! $data['commission_rate_name'] !!}</td>
                            </tr>
                        @endif
                        @if(isset($data['reason']))
                            <tr>
                                <td>Commission Rate Reason</td>
                                <td>{!! $data['reason'] !!}</td>
                            </tr>
                        @endif
                    @else
                        <tr>
                            <td>FA</td>
                            <td>{!! $holding->commission->recipient->name !!}</td>
                        </tr>
                        <tr>
                            <td>Current FA Position</td>
                            <td>{!! $holding->commission->recipient->type->name !!}</td>
                        </tr>
                        <tr>
                            <td>Commission Rate Percentage</td>
                            <td>{!! $holding->commission->commission_rate !!}%</td>
                        </tr>
                    @endif
                    @if(isset($instruction->document_id))
                        <tr>
                            <td>Proof of Payment</td>
                            <td>
                                <a target="_blank"
                                   href="/dashboard/investments/client-instructions/filled-application-documents/{!! $instruction->document_id !!}">
                                    <i class="fa fa-file-pdf-o"></i> View File
                                </a>
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <h4>Existing Payments</h4>
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Narrative</th>
                </tr>
                </thead>
                <tbody>
                @foreach($existing_payments as $payment)
                    <tr>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($payment->amount) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment->date) !!}</td>
                        <td>{!! $payment->description !!}</td>
                    </tr>
                @endforeach
                <tr style="background: yellow; color: red;">
                    <th>Total</th><th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($existing_payments->sum('amount')) !!}</th>
                </tr>
                <tr style="background: yellow; color: red;">
                    <th>Remaining</th><th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price() - $existing_payments->sum('amount')) !!}</th>
                </tr>
                </tbody>
            </table>

            @if($schedule && $schedule->type->slug == 'reservation_fee')
                <a data-toggle="modal" data-target="#reservationModal" class="btn btn-info">Reservation form</a>
            @endif
        </div>
    </div>
    @if($paymentSchedules)
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4>Payment schedules</h4>
            </div>
            <div class="panel-body">
                <table class="table table-responsive table-striped table-hover">
                    <thead>
                    <tr><th>Remove</th><th>Date</th><th>Description</th><th>Amount</th><th>Charge Penalty</th><th></th><th></th></tr>
                    </thead>
                    <tbody>
                    @foreach($paymentSchedules->sortBy('date') as $schedule)
                        <tr>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                            <td>{!! $schedule->type->name !!}</td>
                            <td>{!! $schedule->description !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount)  !!}</td>
                            <td>{!! Cytonn\Presenters\BooleanPresenter::presentYesNo($schedule->penalty) !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
</div>

<div>
    <!-- Modal -->
    <div class="modal large fade" id="reservationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reservation Form</h4>
                </div>
                <div class="modal-body">
                    @include('realestate.reservations.partials.reservation_form', ['client'=>$holding->client])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>