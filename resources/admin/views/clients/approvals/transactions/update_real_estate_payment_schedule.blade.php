<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr><td>Project</td><td colspan="4">{!! $holding->project->name !!}</td></tr>
                    <tr><td>Unit</td><td colspan="4">{!! $holding->unit->number !!}</td></tr>
                    <tr><td>Size</td><td colspan="4">{!! $holding->unit->size->name !!}</td></tr>
                    <tr><td>Type</td><td colspan="4">{!! $holding->unit->type->name !!}</td></tr>
                    <tr><td>Price</td><td colspan="4">{!! $holding->price() !!}</td></tr>
                    <tr><td>Charge Exemption</td><td colspan="4">{!! Cytonn\Presenters\BooleanPresenter::presentYesNo($holding->penalty_excempt)  !!}</td></tr>
                    <tr>
                        <th>Description</th>
                        <th>Scheduled Amount</th>
                        <th>Payment Type</th>
                        <th>Date</th>
                        <th>Penalty Exemption</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! $schedule['description'] !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule['amount']) !!}</td>
                        <td>{!! $paymenttypes($schedule['payment_type_id'])->name !!} </td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule['date']) !!}</td>
                        <td>
                            @if(isset($data['penalty']))
                                {!! Cytonn\Presenters\BooleanPresenter::presentYesNo(!$data['penalty']) !!}
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        @if($schedules && !$approval->approved)
            <div class="panel-heading">
                <h4>Existing Payment Schedules</h4>
            </div>
            <div class="panel-body">
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Payment Type</th>
                        <th>Date</th>
                        <th>Penalty Exemption</th>
                        <th>Interest Start Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($schedules as $schedule)
                        <tr>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                            <td>{!! $schedule->description !!}</td>
                            <td>{!! $schedule->type->name !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                            <td>{!! Cytonn\Presenters\BooleanPresenter::presentYesNo(!$schedule->penalty) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->interest_date) !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr style="background-color: yellow; color: red;">
                        <th colspan="5">TOTAL</th>
                        <th colspan="4">{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
                    </tr>
                    <tr>
                        <th colspan="5">TOTAL AFTER APPROVAL</th>
                        <th colspan="4">{!! \Cytonn\Presenters\AmountPresenter::currency($total + $schedule['amount'] - $paymentSchedule->amount) !!}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        @endif
    </div>
</div>