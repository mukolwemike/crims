<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Unit Fund Fee Percentage</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Unit Fund</td>
                        <td>{{ $fund->name }}</td>
                    </tr>
                    <tr>
                        <td>Unit Fund Fee</td>
                        <td>{{ $fee->type->name }}</td>
                    </tr>
                    <tr>
                        <td>Effective Date</td>
                        <td> {{ \Carbon\Carbon::parse($fee->date)->toFormattedDateString() }}</td>
                    </tr>
                    <tr>
                        <td>Percentage</td>
                        <td>{{ $data['percentage'] }}%</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>