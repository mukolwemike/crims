<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Client Contact Person</h4>
        </div>
        <div class="panel-body">
            <h5>Existing contact persons</h5>

            @if($client->contactPersons()->count() == 0)
                <p>No contact persons added</p><br/>
            @else
                <table class="table table-responsive table-striped">
                    <thead>
                    <th>Name</th><th>Phone</th><th>Email</th><th>Address</th>
                    </thead>
                    <tbody>
                        @foreach($client->contactPersons as $contact)
                            <tr>
                                <td>{!! $contact->name !!}</td>
                                <td>{!! $contact->phone !!}</td>
                                <td>{!! $contact->email !!}</td>
                                <td>{!! $contact->address !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif

            @if(!isset($data['id']))
                <h5>New Contact Details</h5>

                <table class="table table-responsive table-striped">
                    <thead>
                    <th>Name</th><th>Phone</th><th>Email</th><th>Address</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{!! $data['name'] !!}</td>
                            <td>{!! $data['phone'] !!}</td>
                            <td>{!! $data['email'] !!}</td>
                            <td>{!! $data['address'] !!}</td>
                        </tr>
                    </tbody>
                </table>
            @else
                <?php
                  $contact = $contactPerson($data['id']);
                ?>
                <h5>Edit Contact Details</h5>

                <table class="table table-responsive table-striped">
                    <thead>
                        <th></th><th>Name</th><th>Phone</th><th>Email</th><th>Address</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>From</td>
                            <td>{!! $contact->name !!}</td>
                            <td>{!! $contact->phone !!}</td>
                            <td>{!! $contact->email !!}</td>
                            <td>{!! $contact->address !!}</td>
                        </tr>
                        <tr class="success">
                            <td>To</td>
                            <td>{!! $data['name'] !!}</td>
                            <td>{!! $data['phone'] !!}</td>
                            <td>{!! $data['email'] !!}</td>
                            <td>{!! $data['address'] !!}</td>
                        </tr>
                    </tbody>
                </table>
            @endif
        </div>
    </div>


</div>