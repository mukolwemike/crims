<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Activate/Deactivate Client Signature</h4>
        </div>

        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Client Name</td>
                        <td>{{ $signature->client->name() }}</td>
                    </tr>

                    <tr>
                        <td>Signature For</td>
                        <td>{{ $signature->present()->getName }}</td>
                    </tr>

                    <tr>
                        <td>Email</td>
                        <td>{{ $signature->present()->getEmail }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>