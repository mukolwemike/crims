<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Unit Fund Statement Campaign</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Campaign Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Campaign Type</td>
                        <td>{!! $type->name !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['send_date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Unit Fund</td>
                        <td>{!! $unitFund->name !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>