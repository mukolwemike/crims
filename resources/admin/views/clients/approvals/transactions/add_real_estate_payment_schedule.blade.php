<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Payment Schedules</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr><td>Project</td><td colspan="4">{!! $holding->project->name !!}</td></tr>
                    <tr><td>Unit</td><td colspan="4">{!! $holding->unit->number !!}</td></tr>
                    <tr><td>Size</td><td colspan="4">{!! $holding->unit->size->name !!}</td></tr>
                    <tr><td>Type</td><td colspan="4">{!! $holding->unit->type->name !!}</td></tr>
                    <tr><td>Price</td><td colspan="4">{!! $holding->price() !!}</td></tr>
                    <tr><td>Price</td><td colspan="4">{!! $holding->price() !!}</td></tr>
                    <tr><td>Penalty Exemption</td><td colspan="4">{!! Cytonn\Presenters\BooleanPresenter::presentYesNo($holding->penalty_excempt)  !!}</td></tr>
                    <tr>
                        <th>Description</th>
                        <th>Scheduled Amount</th>
                        <th>Payment Type</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($schedules_arr as $schedule)
                        <tr>
                            <td>{!! $schedule['description'] !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule['amount']) !!}</td>
                            <td>{!! @$paymenttype($schedule['payment_type_id'])->name !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule['date']) !!}</td>
                        </tr>
                    @endforeach
                    <tr><th>Total</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($total_schedules = \Cytonn\Core\DataStructures\Collection::make($schedules_arr)->sum('amount')) !!}</th><th colspan="3"></th></tr>
                </tbody>
            </table>
        </div>

        @if($schedules && !$approval->approved)
            <div class="panel-heading">
                <h4>Existing Payment Schedules</h4>
            </div>
            <div class="panel-body">
                <table class="table table-responsive table-striped">
                    <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Payment Type</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schedules as $schedule)
                            <tr>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                                <td>{!! $schedule->description !!}</td>
                                <td>{!! $schedule->type->name !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr style="background-color: yellow; color: red;">
                            <th colspan="2">TOTAL</th>
                            <th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($total) !!}</th>
                        </tr>
                        <tr>
                            <th colspan="2">TOTAL AFTER APPROVAL</th>
                            <th colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($total + $total_schedules) !!}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        @endif
    </div>


</div>