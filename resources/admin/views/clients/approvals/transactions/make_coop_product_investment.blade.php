<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Make Coop Product Investment</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($data['client_id']) !!}</td></tr>
                    <tr><td>Membership No.</td><td>{!! $client->client_code !!}</td></tr>
                    <tr><td>Product Plan</td><td>{!!
                    $productplan->product->name !!} -
                    {!! $productplan->mode_of_payment !!} -
                    Ksh. {!! \Cytonn\Presenters\AmountPresenter::currency($productplan->amount) !!} -
                    {!! $productplan->duration !!} years</td></tr>
                    <tr><td>Invested Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td></tr>
                    <tr><td>Maturity Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td></tr>
                    <tr><td>Amount</td><td>{!! $data['amount'] !!}</td></tr>
                    <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                    <tr><td>Narrative</td><td>{!! $data['narrative'] !!}</td></tr>
                    <tr><td>Commission Recipient</td><td>{!! $commissionrecepient->name !!}</td></tr>
                    <tr><td>Commission Rate</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>