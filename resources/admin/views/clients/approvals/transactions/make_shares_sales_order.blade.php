<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Make Shares Sales Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Seller</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($seller->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $seller->number !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td>{!!$seller->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['request_date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Current Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($seller->currentShares(\Carbon\Carbon::parse($data['request_date'])), true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['number'], true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['price']) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Total Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['price'] * $data['number']) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Current No. of Shares</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($seller->shareHoldings->sum('number'), true, 0) !!}</td>
                    </tr>
                    <tr>
                        <td>Good Till Filled/Cancelled</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['good_till_filled_cancelled'])  !!}</td>
                    </tr>
                    @if(!$data['good_till_filled_cancelled'])
                        <tr>
                            <td>Expiry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['expiry_date']) !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>