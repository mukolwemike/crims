<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Delete Email Indemnity</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped" ng-hide="showedit">
                <tbody>
                <tr>
                    <td>Client Name</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                </tr>
                <tr>
                    <td>Client Code</td>
                    <td>{!! $client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{!! $indemnity->email !!}</td>
                </tr>
                <tr>
                    <td>Created At</td>
                    <td>{!! $indemnity->created_at->toFormattedDateString() !!}</td>
                </tr>
                <tr>
                    <td>Current Status</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($indemnity->status()) !!}</td>
                </tr>
                <tr>
                    <td>Request To</td>
                    <td>{!! ucfirst($data['action']) !!}</td>
                </tr>
                @if($indemnity->deleted_at)
                    <tr>
                        <td>Status</td>
                        <td><span class="label label-success">Deleted</span></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
