<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Setup a project unit group</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Project</td>
                    <td>{!! $project->name !!}</td>
                </tr>
                @if($unitGroup)
                    <tr>
                        <td>Previous Unit Group</td>
                        <td>{!! $unitGroup->name !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>New Unit Group</td>
                    <td>{!! $data['name'] !!}</td>
                </tr>
            </table>
        </div>
    </div>
</div>