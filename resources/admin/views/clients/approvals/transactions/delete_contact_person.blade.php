<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Delete Contact Person</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped" ng-hide="showedit">
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! $contactPerson->name !!}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{!! $contactPerson->phone !!}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{!! $contactPerson->email !!}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>{!! $contactPerson->address !!}</td>
                </tr>
                @if($contactPerson->deleted_at)
                    <tr>
                        <td>Status</td>
                        <td><span class="label label-success"> Deleted </span></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
