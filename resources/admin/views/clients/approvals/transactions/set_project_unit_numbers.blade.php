<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Set number of units available for project</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Project</td>
                    <td>{!! $project->name !!}</td>
                </tr>
                <tr>
                    <td>Size</td>
                    <td>{!! $realestateUnitSize->present()->getName !!}</td>
                </tr>
                <tr>
                    <td>Number</td>
                    <td>{!! $data['number'] !!}</td>
                </tr>
            </table>
        </div>
    </div>
</div>