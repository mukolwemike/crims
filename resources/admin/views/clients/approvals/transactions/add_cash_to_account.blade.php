<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Account Cash</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        @if($client->id)
                            <td>{!! $client->client_code !!}</td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Client</td>
                        @if($client->id)
                            <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                        @else
                            <td>{!! $data['received_from'] !!} (Not on-boarded yet)</td>
                        @endif
                    </tr>
                    @if(isset($data['project_id']))
                    <tr>
                        <td>Project</td>
                        <td> {!! $projects($data['project_id'])->name !!} </td>
                    </tr>
                    @endif
                    @if(isset($data['product_id']))
                    <tr>
                        <td>Product</td>
                        <td> {!! $products($data['product_id'])->name !!} </td>
                    </tr>
                    @endif
                    @if(isset($data['entity_id']))
                    <tr>
                        <td>Share Entity</td>
                        <td> {!! $shareentities($data['entity_id'])->name !!} </td>
                    </tr>
                    @endif
                    @if(isset($data['unit_fund_id']))
                    <tr>
                        <td>Unit Fund</td>
                        <td> {!! $funds($data['unit_fund_id'])->name !!} </td>
                    </tr>
                    @endif
                    <tr>
                        <td>Bank Reference No.</td>
                        <td>{!! $data['bank_reference_no'] !!}</td>
                    </tr>
                    <tr>
                        <td>Bank Account</td>
                        <td>{!! $account->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>{!! $custodialtransactionaccount($data['type'])->name !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    @if($data['exchange_rate'] != 1)
                        <tr class="warning">
                            <td>Exchange Rate</td>
                            <td>{!! $data['exchange_rate'] !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Amount added to client account</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'] * $data['exchange_rate']) !!}</td>
                    </tr>
                    <tr class="warning">
                        <td>Source</td>
                        <td>{!! ucfirst($data['source']) !!}</td>
                    </tr>
                    @if(isset($data['cheque_number']))
                        <tr>
                            <td>Cheque Number</td>
                            <td>{!! $data['cheque_number'] !!} @if(isset($data['cheque_document_id'])){{ link_to_route('show_document', 'View Cheque', [$data['cheque_document_id']]) }}@endif</td>
                        </tr>
                    @endif
                    @if(isset($data['mpesa_confirmation_code']))
                        <tr>
                            <td>Mpesa confirmation code</td>
                            <td>{!! $data['mpesa_confirmation_code'] !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Description</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                    @if(isset($data['entry_date']))
                        <tr>
                            <td>Entry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['entry_date']) !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Dates</td>
                        <td>
                            @if(isset($data['received_date']))
                                Received: {{ \Cytonn\Presenters\DatePresenter::formatDate($data['received_date']) }} |
                            @endif
                            @if(isset($data['entry_date']))
                                Entry: {{ \Cytonn\Presenters\DatePresenter::formatDate($data['entry_date']) }} |
                            @endif
                            Value: {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>FA</td>
                        @if($recipient->id)
                            <td>{!! $recipient->name !!}</td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>