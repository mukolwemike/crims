<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Signing Mandate</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tr><td>Previous Signing Mandate</td><td>{!! $client->present()->getSigningMandate!!}</td></tr>
                <tr><td>New Signing Mandate</td><td>{!! $signingMandate !!}</td></tr>
            </table>
        </div>
    </div>
</div>