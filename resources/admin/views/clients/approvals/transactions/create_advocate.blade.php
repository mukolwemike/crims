<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Advocate</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td>Postal Code</td>--}}
                        {{--<td>{!! $data['postal_code'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Postal Address</td>--}}
                        {{--<td>{!! $data['postal_address'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Street</td>--}}
                        {{--<td>{!! $data['street'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Town</td>--}}
                        {{--<td>{!! $data['town'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Country</td>--}}
                        {{--<td>{!! \App\Cytonn\Models\Country::find($data['country_id'])->name !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Building</td>--}}
                        {{--<td>{!! $data['building'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Telephone Office</td>--}}
                        {{--<td>{!! $data['telephone_office'] !!}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>Telephone Cell</td>--}}
                        {{--<td>{!! $data['telephone_cell'] !!}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td>E-mail</td>
                        <td>{!! $data['email'] !!}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>{!! $data['address'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>