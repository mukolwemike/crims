<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Unit Fund Fee</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td width="30%">Fund Name</td>
                    <td>{!! $fund->name !!}</td>
                </tr>
                <tr>
                    <td>Fee Type</td>
                    <td>{!! $type->name !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                <tr>
                    <td>Applied</td>
                    <td>{!! $chargeType !!}</td>
                </tr>
                @if($chargeType == 'Recurrent')
                    <tr>
                        <td>Frequency:</td>
                        <td>{!! $frequency !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Dependent</td>
                    <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['dependent']) !!}</td>
                </tr>
                @if($data['dependent'])
                    <tr>
                        <td>Fee Parameter</td>
                        <td>{!! \App\Cytonn\Models\Unitization\UnitFundFeeParameter::findOrFail($data['unit_fund_fee_parameter_id'])->name !!}</td>
                    </tr>
                    @if($data['is_amount'])
                        <tr>
                            <td>Amount</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['percentage']) !!}</td>
                        </tr>
                    @else
                        <tr>
                            <td>Percentage</td>
                            <td>{!! $data['percentage'] !!}%</td>
                        </tr>
                    @endif
                @else
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                @endif
                </tbody>
                @if(isset($data['charged_at']))
                    <tr>
                        <td>Charged At</td>
                        <td>{!! $data['charged_at'] !!}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
</div>