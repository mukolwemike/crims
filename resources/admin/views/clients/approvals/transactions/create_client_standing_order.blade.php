<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Client Standing Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>

                    <tr>
                        <td>Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>

                    <tr>
                        <td>Standing Order Type</td>
                        <td>{!! $orderType->name !!}</td>
                    </tr>

                    @if($investment)
                        <tr>
                            <td>Investment</td>
                            <td> Investment of {{ \Cytonn\Presenters\AmountPresenter::currency($investment->amount) }}
                                    on {{ $investment->product->name }}
                                    from {{ \App\Cytonn\Presenters\General\DatePresenter::formatDate($investment->invested_date) }}
                                    to {{ \App\Cytonn\Presenters\General\DatePresenter::formatDate($investment->maturity_date) }}
                            </td>
                        </tr>
                    @endif

                    @if($project)
                        <tr>
                            <td>Project</td>
                            <td>{{ $project->long_name }}</td>
                        </tr>
                    @endif

                    @if($holding)
                        <tr>
                            <td>Unit Holding</td>
                            <td>{{ $holding->unit->number }}</td>
                        </tr>
                    @endif

                    @if(isset($data['tenor']))
                        <tr>
                            <td>Tenor in Months</td>
                            <td>{{ $data['tenor'] }}</td>
                        </tr>
                    @endif

                    <tr>
                        <td>Date</td>
                        <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($data['date']) }}</td>
                    </tr>

                    @if(isset($data['end_date']))
                    <tr>
                        <td>End Date</td>
                        <td>{{ \Cytonn\Presenters\DatePresenter::formatDate($data['date']) }}</td>
                    </tr>
                    @endif

                    <tr>
                        <td>Description</td>
                        <td>{{ $data['description'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>