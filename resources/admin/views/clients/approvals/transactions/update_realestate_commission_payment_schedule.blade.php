<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Update commission payment schedule</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <table class="table table-responsive table-hover">
                    <tbody>
                        <tr><td>Project</td><td>{!! $holding->project->name !!}</td></tr>
                        <tr><td>Unit</td><td>{!! $unit->number !!} ({!! $unit->size->name !!}) {!! $unit->type->name !!}</td></tr>
                        <tr><td>Client</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($holding->client_id) !!} {!! $holding->client->client_code !!}</td></tr>
                        <tr><td>Price</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td></tr>
                        <tr><td colspan="2"> Commission Details</td></tr>
                        <tr><td>Financial Advisor</td><td>{!! $commission->recipient->name !!}</td></tr>
                        <tr><td>Rate</td><td>{!! $unit->type->name !!}</td></tr>
                        <tr><td>Total Commission</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($commission->amount) !!}</td></tr>
                        <tr><td colspan="2">Schedule Details</td></tr>
                    </tbody>
                </table>

                <h4>Schedule Details</h4>
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Type</th><th>Description</th><th>LOO</th><th>SA</th><th>Date</th><th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{!! $schedule->type->name !!}</td>
                            <td>{!! $schedule->description !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($holding->loo->date_received) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($holding->salesAgreement->date_received) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['before']['date']) !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                        </tr>
                    </tbody>
                </table>

                <h4>New Details</h4>
                <table class="table table-responsive table-hover">
                    <tbody>
                        <tr><td>New Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td></tr>
                        <tr><td>Reason</td><td>{!! $data['reason'] !!}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>