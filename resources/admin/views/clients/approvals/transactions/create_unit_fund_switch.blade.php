<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Unit Fund Switch</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Switch Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    @foreach($fees as $fee)
                        <tr>
                            <td>Fee: {{ $fee->type->name }}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fee->fee_units) !!}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td>Fund</td>
                        <td>From : {!! $fundFrom->name !!}  To: {!! $fundTo->name !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Units</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number) !!}</td>
                    </tr>
                    <tr>
                        <td>Units to be transferred</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($net = $number - $fees->sum('fee_units')) }}</td>
                    </tr>
                    <tr>
                        <td>Unit Price</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($price) !!}</td>
                    </tr>
                    <tr>
                        <td>Value of Units</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($number * $price)!!}</td>
                    </tr>
                    <tr>
                        <td>Transferred value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($net * $price)!!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>