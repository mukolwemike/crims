<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Unit Transfer</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr><td>Project</td><td colspan="2">{!! $holding->project->name !!}</td></tr>
                    <tr><td>Unit</td><td colspan="2">{!! $holding->unit->number !!}</td></tr>
                    <tr><td>Size</td><td colspan="2">{!! $holding->unit->size->name !!}</td></tr>
                    <tr><td>Type</td><td colspan="2">{!! $holding->unit->type->name !!}</td></tr>
                    <tr><td>Price</td><td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td></tr>
                    <tr><td></td><th>Current Client</th><th>New Client</th></tr>
                    <tr>
                        <th>Name</th>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($current_client->id) !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($new_client->id) !!}</td>
                    </tr>
                    <tr>
                        <th>Client Code</th>
                        <td>{!! $current_client->client_code !!}</td>
                        <td>{!! $new_client->client_code !!}</td>
                    </tr>
                    <tr>
                        <th>E-Mail</th>
                        <td>{!! $current_client->contact->email !!}</td>
                        <td>{!! $new_client->contact->email !!}</td>
                    </tr>
                    <tr>
                        <th>Phone</th>
                        <td>{!!  $current_client->contact->phone !!}</td>
                        <td>{!!  $new_client->contact->phone !!}</td>
                    </tr>
                    <tr>
                        <th>ID/Passport No.</th>
                        <td>{!!  $current_client->id_or_passport !!}</td>
                        <td>{!!  $new_client->id_or_passport !!}</td>
                    </tr>
                    <tr>
                        <th>Postal Code</th>
                        <td>{!!  $current_client->postal_code !!}</td>
                        <td>{!!  $new_client->postal_code !!}</td>
                    </tr>
                    <tr>
                        <th>Postal Address</th>
                        <td>{!!  $current_client->postal_address !!}</td>
                        <td>{!!  $new_client->postal_address !!}</td>
                    </tr>
                    <tr>
                        <th>Residential Address</th>
                        <td>{!!  $current_client->residential_address !!}</td>
                        <td>{!!  $new_client->residential_address !!}</td>
                    </tr>
                    <tr>
                        <th>Town</th>
                        <td>{!!  $current_client->town !!}</td>
                        <td>{!!  $new_client->town !!}</td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td>{!!  Cytonn\Presenters\CountryPresenter::present($current_client->country_id) !!}</td>
                        <td>{!!  Cytonn\Presenters\CountryPresenter::present($new_client->country_id) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>