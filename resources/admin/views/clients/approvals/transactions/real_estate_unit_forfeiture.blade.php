<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Real Estate Unit Forfeiture</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <label>Project</label> {!! $project->name !!}<br/>
                <label>Unit</label> {!! $unit->number !!} <br>
                <label>Unit Type</label> {!! $unit->size->name.' '.$unit->type->name !!} <br>
                <label>Forfeiture Date</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['forfeiture_date']) !!}
                <br>
                <label>Refund Amount</label> {!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!} <br>
                <label>Narration</label> {!! $data['narration'] !!} <br>
            </div>
        </div>
    </div>
</div>