<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Investments Commission Payment Dates</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Start Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['start']) !!}</td>
                    </tr>
                    <tr>
                        <td>End Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['end']) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>