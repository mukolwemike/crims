<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Add Interest Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr><td>Client Code</td><td colspan="3">{!! $investment->client->client_code !!}</td></tr>
                    <tr><td>Client Name</td><td colspan="3">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td></tr>
                    <tr><td>Product</td><td colspan="3">{!! $investment->product->name !!}</td></tr>
                    <tr><td>Principal</td><td colspan="3">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td></tr>
                    <tr><td>Interest Rate</td><td colspan="3">{!! $investment->interest_rate !!}%</td></tr>
                    <tr><td>Invested Date</td><td colspan="3">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td></tr>
                    <tr><td>Maturity Date</td><td colspan="3">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                    <tr>
                        <th>Description</th>
                        <th>Scheduled Amount</th>
                        <th>Date</th>
                        <th>Fixed Payment</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! $data['description'] !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                        <td>
                            @if($data['fixed']) True
                            @else false
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        @if($schedules && !$approval->approved)
            <div class="panel-heading">
                <h4>Existing Interest Payment Schedules</h4>
            </div>
            <div class="panel-body">
                <table class="table table-responsive table-striped">
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Scheduled Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schedules as $schedule)
                            <tr>
                                <td>{!! $schedule->description !!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->amount) !!}</td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date) !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>


</div>