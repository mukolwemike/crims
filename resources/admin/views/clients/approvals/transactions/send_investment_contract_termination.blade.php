<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Send Investment Contract Termination</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th colspan="100%" style="text-align: center;">Investment Details</th>
                </tr>
                <tr>
                    <td>Product</td>
                    <td colspan="2">{!! $investment->product->name !!}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                </tr>
                <tr>
                    <td>Invested Date</td>
                    <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                </tr>
                <tr>
                    <td>Maturity Date</td>
                    <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
                <tr>
                    <td>Interest Rate</td>
                    <td colspan="2">{!! $investment->interest_rate !!}%</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>