<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Default Bank Account</h4>
        </div>
        <div class="panel-body">
            <p>The client is setting default bank aacount as:</p>
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th>Bank</th>
                        <th>Branch</th>
                        <th>Clearing Code</th>
                        <th>Swift Code</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! $account->account_name !!}</td>
                    <td>{!! $account->account_number !!}</td>
                    @if($branch)
                        <td>{!! $branch->bank->name !!}</td>
                        <td>{!! $branch->name !!}</td>
                        <td>{!! $branch->bank->clearing_code !!}</td>
                        <td>{!! $branch->bank->swift_code !!}</td>
                    @endif

                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>