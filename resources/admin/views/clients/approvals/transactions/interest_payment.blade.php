<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">

            @if(!is_null($investment))
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                    </tr>
                    <tr>
                        <td>Available Interest on {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_paid']) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalAvailableInterestAtNextDay($data['date_paid'])) !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date_paid']) !!}</td>
                    </tr>
                    @if(isset($data['interest_action_id']))
                        <tr><td>Interest Action</td><td>{!! $interestaction->name !!}</td></tr>
                    @endif
            </table>
            @else
                <div class="alert alert-danger">
                    <p>The investment does not exist. You therefore cannot pay interest for it.</p>
                </div>
            @endif
        </div>
    </div>
</div>