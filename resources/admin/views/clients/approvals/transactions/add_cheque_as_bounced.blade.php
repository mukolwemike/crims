<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Account Cash</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Client Code</td>
                    @if($client)
                        <td>{!! $client->client_code !!}</td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    <td>Client</td>
                    @if($client)
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($client->id) !!}</td>
                    @else
                        <td>{!! $transaction->received_from !!} (Not on-boarded yet)</td>
                    @endif
                </tr>
                @if($payment->project_id)
                    <tr>
                        <td>Project</td>
                        <td> {!! $payment->project->name !!} </td>
                    </tr>
                @endif
                @if($payment->product_id)
                    <tr>
                        <td>Product</td>
                        <td> {!! $payment->product->name !!} </td>
                    </tr>
                @endif
                @if($payment->entity_id)
                    <tr>
                        <td>Share Entity</td>
                        <td> {!! $payment->shareEntity->name !!} </td>
                    </tr>
                @endif
                @if($payment->unit_fund_id)
                    <tr>
                        <td>Unit Fund</td>
                        <td> {!! $payment->fund->name !!} </td>
                    </tr>
                @endif
                <tr>
                    <td>Bank Reference No.</td>
                    <td>{!! $transaction->bank_reference_no !!}</td>
                </tr>
                <tr>
                    <td>Bank Account</td>
                    <td>{!! $transaction->custodialAccount->account_name !!}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>{!! $transaction->typeName() !!}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($value) !!}</td>
                </tr>
                @if($transaction->exchange_rate != 1)
                    <tr class="warning">
                        <td>Exchange Rate</td>
                        <td>{!! $transaction->exchange_rate !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Amount added to client account</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($value * $transaction->exchange_rate) !!}</td>
                </tr>
                <tr class="warning">
                    <td>Source</td>
                    <td>{!! ucfirst($transaction->source) !!}</td>
                </tr>
                @if(isset($transaction->cheque_number))
                    <tr>
                        <td>Cheque Number</td>
                        <td>{!! $transaction->cheque_number !!} @if($transaction->cheque_document_id) {{ link_to_route('show_document', 'View Cheque', [ $transaction->cheque_document_id ]) }} @endif</td>
                    </tr>
                @endif
                @if($transaction->mpesa_confirmation_code)
                    <tr>
                        <td>Mpesa confirmation code</td>
                        <td>{!! $transaction->mpesa_confirmation_code !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Description</td>
                    <td>{!! $transaction->description !!}</td>
                </tr>
                @if($transaction->entry_date)
                    <tr>
                        <td>Entry Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->entry_date) !!}</td>
                    </tr>
                @endif
                <tr>
                    <td>Dates</td>
                    <td>
                        @if($transaction->received_date)
                            Received: {{ \Cytonn\Presenters\DatePresenter::formatDate($transaction->received_date) }} |
                        @endif
                        @if($transaction->entry_date)
                            Entry: {{ \Cytonn\Presenters\DatePresenter::formatDate($transaction->entry_date) }} |
                        @endif
                        Value: {!! \Cytonn\Presenters\DatePresenter::formatDate($transaction->date) !!}
                    </td>
                </tr>
                <tr class="warning"><td>Bounce Date</td><td>{{ \Cytonn\Presenters\DatePresenter::formatDate($data['date']) }}</td></tr>
                @if($approval->suspenseTransaction)
                    <tr>
                        <td>Suspense Transaction</td>
                        <td>
                            <a class="btn btn-sm btn-info margin-bottom-10"
                               href="/dashboard/portfolio/suspense-transactions/details/{!! $approval->suspenseTransaction->id !!}">View Suspense Transaction</a>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>


</div>