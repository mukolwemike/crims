<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store Product</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">

                @if($product)
                    <thead>
                    <tr><td>Item</td><td>Was</td><td>Now</td></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $product->name !!}</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $product->description !!}</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                    <tr>
                        <td>Short Name</td>
                        <td>{!! $product->shortname !!}</td>
                        <td>{!! $data['shortname'] !!}</td>
                    </tr>
                    <tr>
                        <td>Long Name</td>
                        <td>{!! $product->longname !!}</td>
                        <td>{!! $data['longname'] !!}</td>
                    </tr>
                    <tr>
                        <td>Currency</td>
                        <td>{!! $product->currency->code !!}</td>
                        <td>{!! $currency->code !!}</td>
                    </tr>
                    <tr>
                        <td>Custodial Account</td>
                        <td>{!! $product->custodialAccount->account_name !!}</td>
                        <td>{!! $custodialAccount->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Fund Manager</td>
                        <td>{!! $product->fundManager->name !!}</td>
                        <td>{!! $fundManager->name !!}</td>
                    </tr>
                    <tr>
                        <td>Product Type</td>
                        <td>{!! $product->type->name !!}</td>
                        <td>{!! $productType->name !!}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>{!! $product->present()->getStatus !!}</td>
                        <td>{!! $status !!}</td>
                    </tr>
                    </tbody>
                @else
                    <thead>
                    <tr><td>Item</td><td>Details</td></tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                    <tr>
                        <td>Short Name</td>
                        <td>{!! $data['shortname'] !!}</td>
                    </tr>
                    <tr>
                        <td>Long Name</td>
                        <td>{!! $data['longname'] !!}</td>
                    </tr>
                    <tr>
                        <td>Currency</td>
                        <td>{!! $currency->code !!}</td>
                    </tr>
                    <tr>
                        <td>Custodial Account</td>
                        <td>{!! $custodialAccount->account_name !!}</td>
                    </tr>
                    <tr>
                        <td>Fund Manager</td>
                        <td>{!! $fundManager->name !!}</td>
                    </tr>
                    <tr>
                        <td>Product Type</td>
                        <td>{!! $productType->name !!}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>{!! $status !!}</td>
                    </tr>
                    </tbody>
                @endif
            </table>
        </div>
    </div>
</div>