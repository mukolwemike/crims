<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <div class="alert alert-info">Paying commission to FA</div>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>FA Name</td>
                        <td>{!! $recipient->name !!}</td>
                    </tr>
                    <tr>
                        <td>Due Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $data['narrative'] !!}</td>
                    </tr>
            </table>

            <div class="detail-group">
                <h4>Commission Calculation</h4>

                <table class="table table-hover table-responsive">
                    <thead>
                        <tr><th>Entry</th><th>Amount</th></tr>
                    </thead>
                    <tbody>
                        <tr><td>Compliant Commission this month</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($summary->getCompliant()->sum('amount')) !!}</td></tr>
                        <tr><td>Commission brought forward after compliance</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($summary->getBroughtForward()->sum('amount')) !!}</td></tr>
                        <tr><td>Commission claw back</td><td>({!! \Cytonn\Presenters\AmountPresenter::currency($summary->getClawBacks()->sum('amount')) !!})</td></tr>
                        </tbody>
                        <tfoot>
                        <tr><th>Total</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($summary->getTotalPayable()) !!}</th></tr>
                        <tr><th>Paid</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($summary->paid($date)) !!}</th></tr>
                        <tr><th>Pay</th><th>{!! \Cytonn\Presenters\AmountPresenter::currency($summary->pay($date)) !!}</th></tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

