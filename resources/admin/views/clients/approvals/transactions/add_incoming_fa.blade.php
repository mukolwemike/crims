<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>
                Add Client Incoming Financial Advisor
            </h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! $incoming_fa->name !!}</td>
                    <td>{!! $incoming_fa->phone !!}</td>
                    <td>{!! $incoming_fa->email !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>