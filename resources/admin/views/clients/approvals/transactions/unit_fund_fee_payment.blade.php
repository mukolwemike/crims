<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Unit Fund Fee Payment</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Unit Fund</td>
                        <td>{!! $fund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Fee Type</td>
                        <td>{!! $fee->name !!}</td>
                    </tr>
                    <tr>
                        <td>Fee Amount</td>
                        <td>{!! Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Recipient</td>
                        <td>{!! $recipient->name !!}</td>
                    </tr>
                    @if($fund->custodialAccount)
                    <tr>
                        <td>Account</td>
                        <td>{!! $fund->custodialAccount->account_name !!}</td>
                    </tr>
                    @endif

                </tbody>
            </table>
        </div>
    </div>
</div>