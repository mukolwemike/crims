<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Email Indemnity</h4>
        </div>
        <div class="panel-body">
            <p>The client filled an email indemnity with the following details</p>

            <ul>
                <li>Email: {!! $data['email'] !!}</li>
                <li>Date: {!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</li>
                <li>File: <a target="_blank" href="/dashboard/investments/client-instructions/filled-application-documents/{!! $data['document_id'] !!}">View file</a></li>
            </ul>
        </div>
    </div>
</div>