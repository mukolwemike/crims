<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Unit Fund</td>
                        <td colspan="2">{!! $fund->name !!}</td>
                    </tr>

                    @if($selectedCompliance)
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Abbr</th>
                        </tr>
                        @foreach($selectedCompliance as $compliance)
                        <tr>
                            <td>{{ $compliance['name'] }}</td>
                            <td>{{ $compliance['type'] }}</td>
                            <td>{{ $compliance['abbr'] }}</td>
                        </tr>
                        @endforeach
                    <tr>

                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>