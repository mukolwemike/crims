<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Unit Fund Dividend</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <td>Fund Name</td>
                        <td colspan="2">{!! $fund->name !!}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>New</th>
                        <th>Current</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                        <td>{!! $trail->date->toFormattedDateString() !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($trail->amount) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>