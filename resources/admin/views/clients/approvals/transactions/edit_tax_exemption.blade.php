<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Tax Exemption Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <td></td>
                    <td>Before</td>
                    <td>Now</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Start Date</td>
                    <td>{{\Cytonn\Presenters\DatePresenter::formatDate($taxExemption->start)}}</td>
                    <td>{{\Cytonn\Presenters\DatePresenter::formatDate($data['start_date'])}}</td>
                </tr>
                <tr>
                    <td>End Date</td>
                    <td>{{\Cytonn\Presenters\DatePresenter::formatDate($taxExemption->end)}}</td>
                    <td>{{\Cytonn\Presenters\DatePresenter::formatDate($data['end_date'])}}</td>
                </tr>
                <tr>
                    <td>Maximum Amount</td>
                    <td>{{ \Cytonn\Presenters\AmountPresenter::currency($taxExemption->amount) }}</td>
                    <td>{{ \Cytonn\Presenters\AmountPresenter::currency($data['amount'])}}</td>
                </tr>
                <tr>
                    <td>File</td>
                    <td>
                        <a class="" target="_blank"
                           href="/dashboard/investments/client-instructions/filled-application-documents/{!! $taxExemption->document_id !!} ">
                            <span class="glyphicon glyphicon-file"></span>
                        </a>
                    </td>
                    <td>
                        @if(!is_null($data['document_id']))
                            <a class="" target="_blank"E
                               href="/dashboard/investments/client-instructions/filled-application-documents/{!! $data['document_id'] !!} ">
                                <span class="glyphicon glyphicon-file"></span>
                            </a>
                        @else
                            <a class="" target="_blank"
                               href="/dashboard/investments/client-instructions/filled-application-documents/{!! $taxExemption->document_id !!} ">
                                <span class="glyphicon glyphicon-file"></span>
                            </a>
                        @endif
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>