<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Buy Coop Shares</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($data['client_id']) !!}</td></tr>
                    <tr><td>Membership No.</td><td>{!! $client->client_code !!}</td></tr>
                    <tr><td>Amount</td><td>{!! $data['amount'] !!}</td></tr>
                    <tr><td>Number of Shares</td><td>{!! $number_of_shares !!}</td></tr>
                    <tr><td>Date</td><td>{!! $data['date'] !!}</td></tr>
                    @if($data['deduct'] == 'no')
                        <tr><td>Payment</td><td>Add a payment</td></tr>
                        <tr><td>Narration</td><td>{!! $data['narrative'] !!}</td></tr>
                    @else
                        <tr><td>Payment</td><td>Deduct from balance</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>