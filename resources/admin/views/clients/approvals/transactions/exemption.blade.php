<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">

            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <tbody>
                <tr>
                    <td>Exempted Transaction Type</td>
                    <td>{!! ucfirst(str_replace('_', ' ', $exemptedApproval->transaction_type)) !!}</td>
                </tr>
                <tr>
                    <td>Reason</td>
                    <td>{!! $data['reason'] !!}</td>
                </tr>
                <tr>
                    <td>Sent by</td>
                    <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($exemptedApproval->sent_by) !!}</td>
                </tr>
                @if(isset($data['date']))
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date'])!!}</td>
                    </tr>
                @endif
                <tr>
                    <td>View Approval</td>
                    <td><a href="/dashboard/investments/approve/{!! $exemptedApproval->id !!}">View Approval</a> </td>
                </tr>
            </table>
        </div>
    </div>
</div>