<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Make Coop Payment</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($data['client_id']) !!}</td></tr>
                    <tr><td>Membership No.</td><td>{!! @Client::findOrFail($data['client_id'])->client_code !!}</td></tr>
                    <tr><td>Amount</td><td>{!! $data['amount'] !!}</td></tr>
                    <tr><td>Date</td><td>{!! $data['date'] !!}</td></tr>
                    <tr><td>Narrative</td><td>{!! $data['narrative'] !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>