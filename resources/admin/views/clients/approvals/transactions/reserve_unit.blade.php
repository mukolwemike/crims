<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Unit Reservation Details</h4>
        </div>
        <div class="panel-body">
            <h4>Unit Details</h4>
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr>
                    <td>Project Name</td>
                    <td>{{$unit->project->name}}</td>
                </tr>
                <tr>
                    <td>Project Code</td>
                    <td>{{$unit->project->code}}</td>
                </tr>
                <tr>
                    <td>Number</td>
                    <td>{{$unit->number}}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>{{$unit->type->name}}</td>
                </tr>
                <tr>
                    <td>Size</td>
                    <td>{{$unit->size->name}}</td>
                </tr>

                @if(isset($data['penalty_exempt']))
                    <tr>
                        <td>Penalty Exemption</td>
                        <td class="text-success">
                            <span class="glyphicon glyphicon-ok-circle"></span>
                        </td>
                    </tr>
                @endif

                <tr>
                    <td>Status</td>
                    <td>
                        @if(is_null($holding))
                            <span class="label label-info">Available</span>
                        @else
                            <span class="label label-success">Reserved</span>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>

            <hr>

            @if(is_null($client))
                @if($duplicate->exists)
                    @if($data['new_client'] == 2)
                        <div class="alert alert-danger">
                            <p>The sender has requested that the client be added as a duplicate of;</p>
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>You will not be able to approve this unless it's added as a duplicate</p>
                        </div>
                    @endif
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($duplicate->clients as $client)
                            <tr>
                                <a href="/dashboard/clients/details/{!! $client->id !!}">
                                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                                    <td>{!! $client->client_code !!}</td>
                                </a>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="alert alert-info">
                        <p><strong>Reason:</strong>
                            &nbsp; {!! isset($data['duplicate_reason']) ? $data['duplicate_reason'] : '' !!}</p>
                    </div>
                @endif

                @if(isset($data['risk_reason']) && !is_null($data['risk_reason']))
                    <div class="alert alert-warning" role="alert">
                        <p class="text-warning">The Client Details matched the following Client who is deemed
                            <strong>RISKY</strong></p>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name/Organization</th>
                            <th>Email</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <a target="_blank" href="/dashboard/client/risky/show/{{$riskyClient->id}}">
                                    @if(is_null($riskyClient->firstname)) {!! $riskyClient->organization  !!} @else {!! $riskyClient->firstname. ' '. $riskyClient->lastname !!} @endif
                                </a>
                            </td>
                            <td>{!! $riskyClient->email !!}</td>
                            <td>
                                @if($riskyClient->risky_status_id === 1)<span
                                        class="label label-warning">INVESTIGATING</span>
                                @elseif($riskyClient->risky_status_id === 2)<span
                                        class="label label-success">CLEARED</span>
                                @elseif($riskyClient->risky_status_id === 3)<span
                                        class="label label-danger">REJECTED</span>@endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="alert alert-info">
                        <p class="text-info"><strong>Reason for Risky Application:</strong></p>
                        <p class="text-capitalize">{{$data['risk_reason']}}</p>
                    </div>
                @endif
        </div>
    </div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Client Details</h4>
        </div>
        <div class="panel-body">
            <div class="">
                <table class="table table-hover table-responsive table-striped">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>
                            @if($data['individual'] == 1)
                                {!! \Cytonn\Presenters\ClientPresenter::presentTitle($title_id) !!}
                                {!!  \Cytonn\Presenters\ClientPresenter::presentFullNameWithPartials($data['firstname'], $data['lastname'], ((isset($data['middlename'])) ? $data['middlename'] : null) )!!}
                            @else
                                {!! $data['registered_name'] !!}
                            @endif
                        </td>
                    </tr>
                    @if($data['individual'] == 2)
                        <tr>
                            <td>Trade Name</td>
                            <td>{!! $data['trade_name'] !!}</td>
                        </tr>
                    @endif
                    @if(isset($data['id_or_passport']))
                        <tr>
                            <td>ID or Passport</td>
                            <td>{!! $data['id_or_passport'] !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Phone</td>
                        <td>{!! ($data['individual'] == 2) ? $data['telephone_office'] : $data['telephone_home'] !!}</td>
                    </tr>
                    @if(isset($data['email']))
                    <tr>
                        <td>Email</td>
                        <td>{!! $data['email'] !!}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>PIN</td>
                        <td>{!! isset($data['pin_no']) ? $data['pin_no']: '' !!}</td>
                    </tr>
                    <tr>
                        <td>Franchise</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($data['franchise']) !!}</td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td>{!! isset($data['country_id']) ? \Cytonn\Presenters\CountryPresenter::present($data['country_id']) : '' !!}</td>
                    </tr>

                    <tr>
                        <td>Nationality</td>
                        <td>{!! isset($data['nationality_id']) ? \Cytonn\Presenters\CountryPresenter::present($data['nationality_id']) : '' !!}</td>
                    </tr>

                    <tr>
                        <td>Town</td>
                        <td>{!! isset($data['town']) ? $data['town'] : '' !!}</td>
                    </tr>
                    @if(\Illuminate\Support\Arr::exists($data, 'street'))
                        <tr>
                            <td>Street</td>
                            <td>{!! isset($data['street']) ? $data['street'] : '' !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Postal Address</td>
                        <td>{!! isset($data['postal_address']) ? $data['postal_address'] : '' !!}</td>
                    </tr>
                    <tr>
                        <td>Postal Code</td>
                        <td>{!! isset($data['postal_code'] ) ? $data['postal_code']  : ''!!}</td>
                    </tr>
                    <tr>
                        <td>Bank</td>
                        <td>
                            @if(\Illuminate\Support\Arr::exists($data, 'investor_bank')) {!! \App\Cytonn\Presenters\BankPresenter::presentBankName($data['investor_bank']) !!} @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Branch</td>
                        <td>
                            @if(\Illuminate\Support\Arr::exists($data, 'investor_bank_branch')) {!! \App\Cytonn\Presenters\BankPresenter::presentBranchName($data['investor_bank_branch']) !!} @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Account Name</td>
                        <td>
                            @if(\Illuminate\Support\Arr::exists($data, 'investor_account_name')) {!! $data['investor_account_name'] !!} @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>
                            @if(\Illuminate\Support\Arr::exists($data, 'investor_account_number')) {!! $data['investor_account_number'] !!} @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @else
                <h4>Client Details</h4>

                <table class="table table-hover table-responsive table-striped">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$client->name()}}</td>
                    </tr>
                    <tr>
                        <td>Code</td>
                        <td>{{$client->client_code}}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{$client->contact->email}}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>{{$client->contact->phone}}</td>
                    </tr>
                    </tbody>
                </table>

                <div class="panel-body">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#clientDetails">
                        View Client Details
                    </button>
                    @if(\Illuminate\Support\Arr::exists($data, 'holders'))
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#jointHoldersModal">
                            @if($data['individual'] == 1)
                                View Joint Holder Details
                            @else
                                View Contact Person Details
                            @endif
                        </button>
                    @endif
                </div>
            @endif

            @if(count($kycDocuments))
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#holder-documents">
                    Clients Documents
                </button>
            @endif

            @if(array_key_exists('contacts', $data))
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#contact-persons">
                    Contact Person Details
                </button>
            @endif

            <div class="modal fade" id="holder-documents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Clients Documents</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body form-detail">
                                @if($kycDocuments)
                                    <h5>KYC Documents</h5>
                                    <div class="panel panel-success">
                                        <table class="table table-striped table-responsive">
                                            <tbody>
                                            @foreach($kycDocuments as $kyc)
                                                <tr>
                                                    <td> {{ $kyc['kyc_type'] }} </td>
                                                    <td>
                                                        <a href="/dashboard/investments/client-instructions/filled-application-documents/{!! $kyc['document_id'] !!}">
                                                            <i class="el-icon-view"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="contact-persons" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Contact Persons Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body form-detail">
                                <div class="panel panel-success">
                                    @if(array_key_exists('contacts', $data))
                                        <table class="table table-striped table-responsive">
                                            <tbody>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Address</th>
                                            </tr>
                                            @foreach($contacts as $contact)
                                                <tr>
                                                    <td> {{ $contact['name'] }} </td>
                                                    <td> {{ $contact['email'] ? $contact['email'] : '' }}</td>
                                                    <td> {{ $contact['phone'] }}</td>
                                                    <td> {{ $contact['address'] }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

{{--client details modal--}}
<div class="modal fade" id="clientDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Client Investment Application</h4>
            </div>
            <div class="modal-body">
                @include('clients.clientdetails')
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@if(\Illuminate\Support\Arr::exists($data, 'holders'))
    <!-- jointHoldersModal Modal -->
    <div class="modal fade" id="jointHoldersModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        @if($data['individual'] == 1)
                            <h4>Joint Holder details</h4>
                        @else
                            <h4>Contact Person details</h4>
                        @endif
                    </h4>
                </div>
                @foreach($data['holders'] as $holder)
                    <div class="modal-body">
                        <div class="panel panel-default">
                            <div class="panel-body form-detail">
                                @if($data['individual'] == 1)
                                    <div class="col-md-6">
                                        <label for="">Name</label> {!! $holder['title_id'] . ' ' . $holder['firstname'] . ' ' !!}
                                        @if(isset($holder['middlename']))
                                            {!! $holder['middlename'] !!}
                                        @endif
                                        {!!  ' ' . $holder['lastname'] !!}
                                    </div>

                                    @if(isset($holder['dob']))
                                        <div class="col-md-6">
                                            <label for="">Date of
                                                Birth</label> {!! \Cytonn\Presenters\DatePresenter::formatDate($holder['dob']) !!}
                                        </div>
                                    @endif

                                    <div class="col-md-6">
                                        <label for="">Email</label> {!! $holder['email'] !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Phone</label> {!! $holder['telephone_cell'] !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Pin Number</label> {!! $holder['pin_no'] !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">ID/Passport Number</label> {!! $holder['id_or_passport'] !!}
                                    </div>

                                    @if(isset($holder['postal_code']))
                                        <div class="col-md-6">
                                            <label for="">Postal Code</label> {!! $holder['postal_code'] !!}
                                        </div>
                                    @endif

                                    @if(isset($holder['postal_address']))
                                        <div class="col-md-6">
                                            <label for="">Postal Address</label> {!! $holder['postal_address'] !!}
                                        </div>
                                    @endif

                                    @if(isset($holder['country']))
                                        <div class="col-md-6">
                                            <label for="">Country</label> {!! \Cytonn\Presenters\CountryPresenter::present($holder['country']) !!}
                                        </div>
                                    @endif

                                    <div class="clearfix"></div>
                                @else
                                    <div class="col-md-6">
                                        <label for="">Name</label> {!! $holder['name'] !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Email</label> {!! $holder['email'] !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Phone</label> {!! $holder['phone'] !!}
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Address</label> {!! $holder['address'] !!}
                                    </div>

                                @endif

                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                @endforeach
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endif

