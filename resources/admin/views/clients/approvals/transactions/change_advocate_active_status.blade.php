<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Update Advocate Active Status</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! $advocate->name !!}</td>
                </tr>
                {{--<tr>--}}
                    {{--<td>Postal Code</td>--}}
                    {{--<td>{!! $advocate->postal_code !!}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>Postal Address</td>--}}
                    {{--<td>{!! $advocate->postal_address !!}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>Street</td>--}}
                    {{--<td>{!! $advocate->street !!}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>Town</td>--}}
                    {{--<td>{!! $advocate->town !!}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>Country</td>--}}
                    {{--<td>--}}
                        {{--@if($advocate->country)--}}
                            {{--{!! $advocate->country->name !!}--}}
                        {{--@endif--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>Building</td>--}}
                    {{--<td>{!! $advocate->building !!}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>Telephone Office</td>--}}
                    {{--<td>{!! $advocate->telephone_office !!}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td>Telephone Cell</td>--}}
                    {{--<td>{!! $advocate->telephone_cell !!}</td>--}}
                {{--</tr>--}}
                <tr>
                    <td>E-mail</td>
                    <td>{!! $advocate->email !!}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>{!! $advocate->address !!}</td>
                </tr>
                </tbody>
            </table>
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Current</th>
                        <th>New</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Active</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon($advocate->active) !!}</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentIcon(! $advocate->active) !!}</td>
                    </tr>
            </table>
        </div>
    </div>


</div>