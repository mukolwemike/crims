<?php
   is_array($client->emails) ?: $client->emails = [];
?>

<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Client Alternative Emails</h4>
        </div>
        <div class="panel-body">
            <h5>Old Emails</h5>
            @if(count($client->emails) == 0)
                <p>No emails saved</p>
            @else
                <ol>
                    @foreach($client->emails as $email)
                        <li>{!! $email !!}</li>
                    @endforeach
                </ol>
            @endif

            <h5>New Emails</h5>
            @if(count($data['emails']) == 0)
                <p>No emails saved</p>
            @else
                <ol>
                    @foreach($data['emails'] as $email)
                        <li>{!! $email !!}</li>
                    @endforeach
                </ol>
            @endif

        </div>
    </div>


</div>