<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>

        <div class="panel-body">
            @if($investment)
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr><td>Product</td><td>{!! $investment->product->name!!}</td></tr>
                <tr><td>Principal</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td></tr>
                <tr><td>Interest Rate</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td></tr>
                <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                <tr><td>Scheduled</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($approval->scheduled) !!}</td></tr>

                @if($data['premature'])
                    <tr><td>Investment Value at withdraw date</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($value_at_end )!!}</td></tr>
                    <tr><td>Net Interest at withdraw date</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getNetInterestForInvestmentAtDate($data['end_date']) )!!}</td></tr>
                    @if(isset($data['deduct_penalty']))
                        @if($data['deduct_penalty'] === true || $data['deduct_penalty'] == 'true')
                            <tr><td>Penalty Percentage</td><td>{!! $data['penalty_percentage'] !!}%</td></tr>
                            <tr><td>Penalty</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['penalty'])!!}</td></tr>
                            <tr><td>Deduct Penalty</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['deduct_penalty']) !!}</td></tr>
                            @if($data['deduct_penalty'] == 'true' and $data['partial_withdraw'] == false)
                                <tr><td>Amount to be withdrawn</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($value_at_end - $data['penalty'] )!!}</td></tr>
                            @endif
                        @endif
                    @endif
                @else
                    <tr><td>Investment Value today</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestment() )!!}</td></tr>
                    <tr><td>Value on Maturity</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date)) !!}</td></tr>
                @endif
                @if($data['partial_withdraw'])
                    <tr class="danger"><td>Partial Withdraw Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                    <tr class="success"><td>Amount Reinvested</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($value_at_end - $data['amount']) !!}</td></tr>
                    <tr class="success"><td>New maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($new_maturity_date) !!}</td></tr>
                @else
                    <tr class="success"><td>Withdrawing full amount</td><td>Yes</td></tr>
                @endif
                @if($data['premature'] == true)
                    <tr class="danger"><td>Premature end date</td><td> {!! \Cytonn\Presenters\DatePresenter::formatDate($data['end_date']) !!} </td></tr>
                @else
                    <tr class="success"><td>Mature Withdrawal</td><td>Yes</td></tr>
                @endif
            </table>
            @else
                <div class="alert alert-danger">
                    <p>The investment could not be found, it could have been rolled back</p>
                </div>
            @endif
        </div>
    </div>


</div>