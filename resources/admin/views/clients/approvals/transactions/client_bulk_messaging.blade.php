<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Clients Bulk message</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    @if($message->id)
                        <tr>
                            <th></th>
                            <th>New Details</th>
                            <th>Current Details</th>
                        </tr>
                    @endif
                </thead>
                <tbody>
                    <tr>
                        <td>Subject</td>
                        <td>{!! $data['subject'] !!}</td>
                        @if($message->id)
                            <td>{!! $message->subject !!}</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{!! $data['body'] !!}</td>
                        @if($message->id)
                            <td>{!! $message->body !!}</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Composer</td>
                        <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($data['composer']) !!}</td>
                        @if($message->id)
                            <td>{!! \Cytonn\Presenters\UserPresenter::presentFullNames($message->composer) !!}</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>