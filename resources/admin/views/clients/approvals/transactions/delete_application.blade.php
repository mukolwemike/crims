<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Reason</td><td>{!! $data['reason'] !!}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
<!--                            --><?php
//                            $app = $application = ClientInvestmentApplication::findOrFail($data['application_id']);
//
//                            $jointHolders = [];
//
//                            if ($application->joint == 1)
//                            {
//                                $jointHolders = ClientJointDetail::where('application_id', $application->id)->get();
//                            }
//
//                            $invested = ClientInvestment::where('application_id', $application->id)->first();
                            ?>
                                    <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                View Application Details
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div ng-show="showedit">

            </div>
        </div>
    </div>


</div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Client Investment Application</h4>
            </div>
            <div class="modal-body">
                @include('investment.partials.application_partial')
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>