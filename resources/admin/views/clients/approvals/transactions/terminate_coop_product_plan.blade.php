<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Terminate Coop Product Plan</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($data['client_id']) !!}</td></tr>
                    <tr><td>Membership No.</td><td>{!! @App\Cytonn\Models\Client::findOrFail($data['client_id'])->client_code !!}</td></tr>
                    @if($product_plan->product_id)
                        <tr><td>Product</td><td>{!! $product_plan->product->name !!}</td></tr>
                    @endif
                    <tr><td>Duration</td><td>{!! $product_plan->duration !!} years</td></tr>
                    @if($product_plan->shares)
                        <tr><td>Shares</td><td>{!! $product_plan->shares !!} shares</td></tr>
                    @elseif($product_plan->product_id)
                        <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($product_plan->amount) !!}</td></tr>
                    @endif
                    <tr><td>Start Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($product_plan->start_date) !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>