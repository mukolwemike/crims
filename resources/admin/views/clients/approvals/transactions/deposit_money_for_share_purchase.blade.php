<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Deposit Money for Share Purchase</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Shareholder's Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($holder->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Shareholder's Number</td>
                        <td>{!! $holder->number !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td>{!! $holder->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Narrative</td>
                        <td>{!! $data['narrative'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>