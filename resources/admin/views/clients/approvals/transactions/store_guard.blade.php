<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store/Update Guard</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <tr>
                    <td>Guard Type</td>
                    <td>{!! $guardType->name !!}</td>
                </tr>
                <tr>
                    <td>Reason</td>
                    <td>{!! $data['reason'] !!}</td>
                </tr>
                @if(array_key_exists('minimum_balance', $data))
                    <tr>
                        <td>Minimum Balance</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['minimum_balance']) !!}</td>
                    </tr>
                @endif
                @if($portfolioInvestor)
                    <tr>
                        <td>Portfolio Investor</td>
                        <td>{!! $portfolioInvestor->name !!}</td>
                    </tr>
                @endif
                @if($guard)
                    <tr>
                        <td><b>Previous Guard Details</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Guard Type</td>
                        <td>{!! $guard->guardType->name !!}</td>
                    </tr>
                    <tr>
                        <td>Reason</td>
                        <td>{!! $guard->reason !!}</td>
                    </tr>
                    @if($guard->minimum_balance != '')
                        <tr>
                            <td>Minimum Balance</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($guard->minimum_balance) !!}</td>
                        </tr>
                    @endif
                    @if($guard->portfolioInvestor)
                        <tr>
                            <td>Portfolio Investor</td>
                            <td>{!! $guard->portfolioInvestor->name !!}</td>
                        </tr>
                    @endif
                @endif
            </table>
        </div>
    </div>
</div>