<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Unit Fund Purchase Commission Recipient</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead>
                    <tr>
                        <th></th>
                        <th>Old</th>
                        <th>New</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="20%">Unit Fund</td>
                        <td></td>
                        <td>{!! $fund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Purchase Date</td>
                        <td></td>
                        <td>{!! $purchase->date !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td></td>
                        <td>{!! $purchase->price * $purchase->number !!}</td>
                    </tr>
                    <tr>
                        <td>Commission Recipient</td>
                        <td>@if($oldRecipient) {{ $oldRecipient->name }} - {{ $oldRecipient->email }} @endif</td>
                        <td>@if($newRecipient) {{ $newRecipient->name }} - {{ $newRecipient->email }} @endif</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>