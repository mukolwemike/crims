<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>Principal</th><td>Interest Rate</td><td>Duration</td><td>Start date</td><td>Maturity date</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['principal']) !!}</td>
                    <td>{!! $data['interest_rate'] !!} %</td>
                    <td>{!! $data['duration'] !!} Days</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td>
                    <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>