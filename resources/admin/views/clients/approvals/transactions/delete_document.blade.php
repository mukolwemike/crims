<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Delete Document</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Title</td><td>{!! $document->title !!}</td></tr>
                    <tr><td>Document Type</td><td>{!! $document->type->name !!}</td></tr>
                    <tr>
                        <td>Category</td>
                        <td>
                            @if($document->project_id) Real Estate @elseif($document->product_id) Investments @endif
                        </td>
                    </tr>
                    @if($document->project_id)
                        <tr>
                            <td>Project</td>
                            <td>{!! $document->project->name !!}</td>
                        </tr>
                    @elseif($document->product_id)
                        <tr>
                            <td>Product</td>
                            <td>{!! $document->product->name !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Compliance Document</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($document->compliance_document) !!}</td>
                    </tr>
                    <tr><td>Document Type</td><td>{!! $document->type->name !!}</td></tr>
                    <tr><td>Uploaded On</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($document->created_at) !!}</td></tr>
                    <tr><td>Preview</td><td><a href="/dashboard/documents/{!! $document->id !!}"><i class="fa fa-file-pdf-o"></i> </a></td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>