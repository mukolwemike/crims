<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Match Purchase Order to Sales Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead>
                    <th></th>
                    <th>Buyer <span class="small">(Current funds : {!! $buyer->sharePaymentsBalance() !!})</span></th>
                    <th>Seller <span class="small">(Current shares : {!! $seller->shareHoldings->sum('number') !!})</span></th>
                </thead>
                <tbody>
                    <tr>
                        <td>Names</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($buyer->id) !!}</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($seller->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $buyer->client_code !!}</td>
                        <td>{!! $seller->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td>{!! $purchaseOrder->entity->name !!}</td>
                        <td>{!! $salesOrder->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($purchaseOrder->request_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($salesOrder->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! $purchaseOrder->number !!}</td>
                        <td>{!! $salesOrder->number !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchaseOrder->price) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($salesOrder->price) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <th>TOTAL</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($purchaseOrder->price * $purchaseOrder->number) !!}</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($salesOrder->price * $salesOrder->number) !!}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>