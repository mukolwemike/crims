<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Source</td>
                    <td>
                        @if($instruction->user_id)
                            <span class="label label-primary">Client</span>
                        @else
                            <span class="label label-info">Admin</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Unit Fund</td>
                    <td>{!! $instruction->unitFund->name !!}</td>
                </tr>
                <tr>
                    <td>Unit Price as at {!! $date !!}</td>
                    <td>{!! $unitPrice !!}</td>
                </tr>
                <tr>
                    <td>Client Code</td>
                    <td>{!! $instruction->client->client_code !!}</td>
                </tr>
                <tr>
                <tr>
                    <td>Amount</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                </tr>
                @foreach($investment->calculateUpfrontFees() as $fee)
                    <tr>
                        <td>{{ $fee->fee->type->name }}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($fee->amount) !!}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>To be invested</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($invested = $data['amount'] - $investment->calculateUpfrontFees()->sum('amount')) !!}</td>
                </tr>
                <tr>
                    <td>unit price</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($unitPrice) !!}</td>
                </tr>
                <tr>
                    <td>Number of units</td>
                    @if($unitPrice != 0)
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($invested/$unitPrice) !!}</td>
                    @else
                        <td>No unit price set</td>
                    @endif
                </tr>
                <tr>
                    <td>Commission Recipient</td>
                    <td>{!! $recipient ? $recipient->name : "No commission awarded" !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! $date!!}</td>
                </tr>

                @if(isset($data['client_code']))
                    <tr>
                        <td>Suggested Client Code</td>
                        <td>{!! $data['client_code']!!}</td>
                    </tr>
                @endif
                @if(isset($data['description']))
                    <tr>
                        <td>Description</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                @endif
                {{--                    <tr>--}}
                {{--                        <td>Interest Payment</td>--}}
                {{--                        <td>{!! $data['interest_payment'] !!}</td>--}}
                {{--                    </tr>--}}

                {{--                    <tr>--}}
                {{--                        <td>Interest Payment Start Date</td>--}}
                {{--                        <td>{!! $interest_start_date !!}</td>--}}
                {{--                    </tr>--}}

                {{--                    <tr>--}}
                {{--                        <td>Interest Payment End Date</td>--}}
                {{--                        <td>{!! $interest_end_date !!}</td>--}}
                {{--                    </tr>--}}
                </tbody>
            </table>
        </div>
    </div>
</div>