<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Reverse Cash Transfer</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Client</td>
                    <td>
                        {!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($payment_out->client_id) !!}
                    </td>
                </tr>
                <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($payment_out->date) !!}</td></tr>
                <tr>
                    <td>Amount</td>
                    <td>
                        {!! \Cytonn\Presenters\AmountPresenter::currency(abs($payment_out->amount)) !!}
                    </td>
                </tr>
                <tr>
                    <td>Source</td>
                    <td>
                        Cash Wallet
                    </td>
                </tr>
                <tr>
                    <td>Previous Transfer Description</td>
                    <td>
                        {!! $payment_out->description !!}
                    </td>
                </tr>
                <tr>
                    <td>Reason</td>
                    <td>
                        {!! $data['reason'] !!}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
