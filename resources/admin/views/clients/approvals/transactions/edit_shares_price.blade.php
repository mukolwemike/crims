<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>{!! $entity->name !!} Edit Share Price</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <td></td>
                    <td>Before</td>
                    <td>Now</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Share Price</td>
                    <td>{!! $entity->sharePrice() !!}</td>
                    <td>{!! $data['price'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>