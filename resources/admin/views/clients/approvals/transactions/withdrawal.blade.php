<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>

        @if(!$approved && $approvals->count())
            <div class="alert alert-warning">
                <p><b>This instruction require that it is approved by authorised persons as in the client signing mandate.</b></p>
                <br>
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Signatory</th>
                        <th>Approval Status</th>
                        <th>Reason</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($approvals as $signatory)
                        <tr>
                            <td>{{ $signatory['user'] }}</td>
                            <td>
                                @if(!$signatory['approval_status'])<el-tag>{{ $signatory['status'] }}</el-tag>@endif
                                @if($signatory['approval_status'] == 1)<el-tag type="success">{{ $signatory['status'] }}</el-tag>@endif
                                @if($signatory['approval_status'] == 2)<el-tag type="danger">{{ $signatory['status'] }}</el-tag>@endif
                            </td>
                            <td>{{ isset($signatory['reason']) ? $signatory['reason'] : 'N\A' }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th width="20%">Reason to continue with processing:</th>
                        <td>
                            @if(isset($data['reason']))
                                <span> {{ $data['reason'] }} </span>
                            @else
                                <span>Not provided</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @else
        @endif

        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr><td>Type</td><td>{{ ucfirst($type) }}</td></tr>
                <tr>
                    <td>Source</td>
                    <td>
                        @if($instruction->user_id)
                            <span class="label label-primary">Client</span>
                        @else
                            <span class="label label-info">Admin</span>
                        @endif
                    </td>
                </tr>
                <tr><td>Product</td><td>{!! $investment->product->name!!}</td></tr>
                <tr><td>Principal</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td></tr>
                <tr><td>Interest Rate</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td></tr>
                <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                @if($scheduled)
                    <tr class="warning"><td>Scheduled to run on</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($action_date->toDateString()) !!}</td></tr>
                @else
                    <tr><td>Action date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($action_date) !!}</td></tr>
                @endif

                @if($data['premature'] || in_array($type, ['deduction', 'interest']))
                    <tr><td>Investment Value at withdraw date</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($value_at_end )!!}</td></tr>
                    <tr><td>Net Interest at withdraw date</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getNetInterestForInvestmentAtDate($action_date, false) )!!}</td></tr>

                    @if(isset($data['deduct_penalty']) && $type == 'withdrawal')
                        @if($data['deduct_penalty'] == true || $data['deduct_penalty'] == 'true')
                            <tr class="warning"><td>Deduct Penalty</td><td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['deduct_penalty']) !!}</td></tr>
                            @if($data['penalty_percentage'])
                                <tr>
                                    <td>Penalty Percentage</td>
                                    <td>{!! $data['penalty_percentage'] !!}%</td>
                                </tr>
                            @endif
                            <tr><td>Penalty</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['penalty'])!!}</td></tr>
                        @endif
                    @endif
                @else
                    <tr><td>Investment Value today</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfAnInvestment() )!!}</td></tr>
                    <tr><td>Value on Maturity</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->repo->getTotalValueOfInvestmentAtDate($investment->maturity_date, true)) !!}</td></tr>
                @endif
                @if($investment->isSeip())
                    <tr>
                        <td>Investment Contributions</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($contributions) !!}</td>
                    </tr>
                    @if($contributionPenalty > 0)
                        <tr>
                            <td>Investment Contributions Penalty</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($contributionPenalty) !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Total Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($contributions + $value_at_end) !!}</td>
                    </tr>
                    <tr class="danger">
                        <td>Total Penalty</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($contributionPenalty + $data['penalty']) !!}</td>
                    </tr>
                @endif
                <tr class="danger"><td>Amount withdrawn</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                @if($data['premature'] == true)
                    <tr class="danger"><td>As at date</td><td> {!! \Cytonn\Presenters\DatePresenter::formatDate($action_date) !!} </td></tr>
                @else
                    <tr class="success"><td>Mature Withdrawal</td><td>Yes</td></tr>
                @endif

                @if($instruction->account)
                    <tr>
                        <th colspan="2">Selected Account</th>
                    </tr>

                    <tr>
                        <td>Bank Name</td>
                        <td>{{ $instruction->account->bank->name }}</td>
                    </tr>

                    <tr>
                        <td>Branch Name</td>
                        <td>{{ $instruction->account->branch->name }}</td>
                    </tr>

                    <tr>
                        <td>Account Name</td>
                        <td>{{ $instruction->account->account_name }}</td>
                    </tr>

                    <tr>
                        <td>Account Number</td>
                        <td>{{ $instruction->account->account_number }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Action</h4>
        </div>
        <div class="panel-body">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                View Client Details
            </button>
        </div>
    </div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Commission Recipient Details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <th>Fa Name:</th>
                        <td>{!! $fa->name !!}</td>
                    </tr>
                    <tr>
                        <th>Fa Position:</th>
                        <td>{!! $fa->type->name !!}</td>
                    </tr>
                    <tr>
                        <th>Commission Rate:</th>
                        <td>{!! $fa->type->rate->rate !!}%</td>
                    </tr>
                    <tr>
                        <th>Commission Rate Name:</th>
                        <td>{!! $fa->type->rate->name !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Client Investment Application</h4>
            </div>
            <div class="modal-body">
                @include('clients.clientdetails')
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
