<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Cancel Unit Fund Application</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Client</td>
                    <td>{{\Cytonn\Presenters\ClientPresenter::presentFullNames($client->id)}}</td>
                </tr>
                <tr>
                    <td>Unit Fund</td>
                    <td>{{$fund->name}}</td>
                </tr>
                @if(isset($data['amount']))
                <tr>
                    <td>Amount</td>
                    <td>{{\Cytonn\Presenters\AmountPresenter::currency($data['amount'], false, 2)}}</td>
                </tr>
                @endif
                @if(isset($data['amount']) || isset($data['number']) || isset($data['units']))
                <tr>
                    <td>Number</td>
                    <td>
                        @if(isset($data['number'])) {{$data['number']}} @elseif(isset($data['units'])) {{$data['units']}} @else {{$data['amount']}} @endif
                    </td>
                </tr>
                @endif
                @if($instruction->type->slug === 'transfer')
                    @if(isset($data['transferer_id']))
                        <tr>
                            <td>From</td>
                            <td>{{\Cytonn\Presenters\ClientPresenter::presentFullNames($data['transferer_id'])}}</td>
                        </tr>
                    @endif
                    @if(isset($data['transferee_id']))
                        <tr>
                            <td>To</td>
                            <td>{{\Cytonn\Presenters\ClientPresenter::presentFullNames($data['transferee_id'])}}</td>
                        </tr>
                    @endif
                @endif
                @if(isset($data['mode_of_payment']))
                <tr>
                    <td>Mode of Payment</td>
                    <td>{{strtoupper($data['mode_of_payment'])}}</td>
                </tr>
                @endif
{{--                @if(isset($data['date']))--}}
{{--                <tr>--}}
{{--                    <td>Date</td>--}}
{{--                    <td>{{\Cytonn\Presenters\DatePresenter::formatDate($data['date'])}}</td>--}}
{{--                </tr>--}}
{{--                @endif--}}
                @if($instruction->document)
                    <tr>
                        <td>Proof Documents</td>
                        <td>
                            <a target="_blank" :href="'/document/instruction-approval/'+{{$instruction->document->id}}">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>Instruction Type</td>
                    <td>
                        <span class="label label-info">{{$instruction->type->name}}</span>
                    </td>
                </tr>
                <tr>
                    <th>Reason</th>
                    <th>{{$reason}}</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>