<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Rollback Investment</h4>
        </div>
        <div class="panel-body">

            @if(!$not_found)
                <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                    <thead></thead>
                    <tbody>
                    <tr><td>Product</td><td>{!! $investment->product->name !!}</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount )!!}</td></tr>
                    <tr><td>Interest Rate</td><td>{!! $investment->interest_rate !!}%</td></tr>
                    <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date)!!}</td></tr>
                    <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td></tr>
                    <tr><td>FA</td><td>{!! $investment->commission->recipient->name !!}</td></tr>
                    <tr><td>Commission Rate</td><td>{!! $investment->commission->rate !!}%</td></tr>
                    <tr><td colspan="2" class="alert alert-info">Deduction</td></tr>
                    <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                    <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td></tr>
                    <tr><td>Narrative</td><td>{!! $data['narrative'] !!}</td></tr>
                    </tbody>
                </table>
            @else
                <div class="alert alert-danger">
                    <p>The investment could not be found, it could have been reversed</p>
                </div>
            @endif
        </div>
    </div>
</div>