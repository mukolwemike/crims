<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>

        @if(!$approved && $approvals->count())
            <div class="alert alert-warning">
                <p><b>This instruction require that it is approved by authorised persons as in the client signing mandate.</b></p>
                <br>
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Signatory</th>
                        <th>Approval Status</th>
                        <th>Reason</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($approvals as $signatory)
                        <tr>
                            <td>{{ $signatory['user'] }}</td>
                            <td>
                                @if(!$signatory['approval_status'])<el-tag>{{ $signatory['status'] }}</el-tag>@endif
                                @if($signatory['approval_status'] == 1)<el-tag type="success">{{ $signatory['status'] }}</el-tag>@endif
                                @if($signatory['approval_status'] == 2)<el-tag type="danger">{{ $signatory['status'] }}</el-tag>@endif
                            </td>
                            <td>{{ isset($signatory['reason']) ? $signatory['reason'] : 'N\A' }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th width="20%">Reason to continue with processing:</th>
                        <td>
                            @if(isset($data['reason']))
                                <span> {{ $data['reason'] }} </span>
                            @else
                                <span>Not provided</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @else
        @endif

        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                @if(isset($data['topup_form_id']) && $data['topup_form_id'] != 0 )
                    <tr><td>Form Link</td><td><a href="/dashboard/investments/client-instructions/topup/{!! $data['topup_form_id'] !!}">See form</a> </td></tr>
                @endif
                <tr><td>Product</td><td>{!! $product !!}</td></tr>
                <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($form->amount)!!}</td></tr>
                <tr><td>Available Cash Balance</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($balance)!!}</td></tr>
                <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! $invested_date !!}</td></tr>
                <tr><td>Maturity date</td><td>{!! $maturity_date !!}</td></tr>
                <tr><td>Total Days</td><td>{!! \Cytonn\Presenters\DatePresenter::getDateDifference($invested_date, $maturity_date) !!}</td></tr>
                <tr><td>FA</td><td>{!! $recipient->name !!}</td></tr>
                <tr><td>Current FA Position</td><td>{!! $recipient->type->name !!}</td></tr>
                <tr><td>Commission Rate</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                @if(isset($data['commission_rate_name']))
                    <tr><td>Commission Rate Name</td><td>{!! $data['commission_rate_name'] !!}</td></tr>
                @endif
                @if(isset($data['commission_start_date']) && $data['commission_start_date'])
                    <tr><td>Commission Start Date</td><td>{!! $data['commission_start_date'] !!}</td></tr>
                @endif
                <tr>
                    <td>Previous Commission Rate</td>
                    <td>{!! $recipient->previous ? $recipient->previous->rate->rate . '%' : '' !!}</td>
                </tr>
                <tr>
                    <td>Previous Commission Position</td>
                    <td>{!! $recipient->previous ? $recipient->previous->name: '' !!}</td>
                </tr>
                <tr><td>Interest payment</td><td>{!! $interestPayment[$data['interest_payment_interval']] !!} on date {!! $data['interest_payment_date'] !!}</td></tr>
                @if(isset($data['interest_action_id']))
                    <tr><td>Interest Action</td><td>{!! $interestaction($data['interest_action_id'])->name !!}</td></tr>
                @endif
                @if(isset($data['interest_reinvest_tenor']))
                    <tr>
                        <td>Interest Reinvest Tenor</td>
                        <td>
                            @if($data['interest_reinvest_tenor'] != 0)
                                {!! $data['interest_reinvest_tenor'] !!} months
                            @else
                                Until maturity of principal
                            @endif
                        </td>
                    </tr>
                @endif
                <tr><td>Transaction Description</td><td>{!! $data['description'] !!}</td></tr>
                @if($form->product->present()->isSeip)
                    <tr>
                        <td><b>SEIP Contribution Details</b></td>
                        <td></td>
                    </tr>
                @endif
                @if(isset($data['contribution_amount']))
                    <tr><td>SEIP Contribution Amount</td><td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($data['contribution_amount']) !!}</td></tr>
                @endif
                @if(isset($data['contribution_payment_interval']))
                    <tr>
                        <td>SEIP Contribution Interval</td>
                        <td>{!! $interestPayment[$data['contribution_payment_interval']] !!} on
                            date {!! $data['contribution_payment_date'] !!}</td>
                    </tr>
                @endif
                @if(count($periodicRates) > 0)
                    <tr>
                        <td>SEIP Commission Rates</td>
                        <td>
                            @foreach($periodicRates as $periodicRate)
                                Year {!! $periodicRate->tenor !!} at {!! $periodicRate->rate !!}%<br>
                            @endforeach
                        </td>
                    </tr>
                @endif
                @if($investment = $form->latestInvestment())
                    <tr><td><b>Previous Investment Details</b></td><td></td></tr>
                    <tr><td>FA</td><td>{!! $investment->present()->commissionRecipientName !!}</td></tr>
                    <tr><td>Interest Payment</td><td>{!! $investment->present()->getInterestPaymentDetails !!}</td></tr>
                    @if($investment->interestAction)
                        <tr>
                            <td>Interest Action</td>
                            <td>{!! $investment->present()->getInterestActionDetails !!}</td>
                        </tr>
                    @endif
                @endif
            </table>
        </div>
    </div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Action</h4>
        </div>
        <div class="panel-body">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                View Client Details
            </button>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Client Investment Application</h4>
            </div>
            <div class="modal-body">
                @include('clients.clientdetails')
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
