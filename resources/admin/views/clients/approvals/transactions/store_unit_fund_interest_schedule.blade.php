<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store Unit Fund Interest Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <td>Client</td>
                    <td colspan="2">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                </tr>
                <tr>
                    <td>Client Code</td>
                    <td colspan="2">{!! $client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Unit Fund</td>
                    <td colspan="2">{!! $unitFund->name !!}</td>
                </tr>
                @if($schedule)
                    <tr>
                        <th></th>
                        <th>Current Interest Schedule</th>
                        <th>New Interest Schedule</th>
                    </tr>
                    <tr>
                        <td>Payment Interval</td>
                        <td>{!! $data['old_interest_payment_interval'] !!}</td>
                        <td>{!! $data['interest_payment_interval'] !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Start Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['old_interest_payment_start_date']) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['interest_payment_start_date']) !!}</td>
                    </tr>
                @else
                    <tr>
                        <th></th>
                        <th colspan="2" style="text-align: center;">New Interest Schedule</th>
                    </tr>
                    <tr>
                        <td>Payment Interval</td>
                        <td colspan="2">{!! $data['interest_payment_interval'] !!}</td>
                    </tr>
                    <tr>
                        <td>Payment Start Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($data['interest_payment_start_date']) !!}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>