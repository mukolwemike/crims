<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Coop Product Plan</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($data['client_id']) !!}</td></tr>
                    <tr><td>Membership No.</td><td>{!! App\Cytonn\Models\Client::findOrFail($data['client_id'])->client_code !!}</td></tr>
                    @if($data['category'] == 'product')
                        <tr><td>Product</td><td>{!! App\Cytonn\Models\Product::findOrFail($data['product_id'])->name !!}</td></tr>
                    @endif
                    <tr><td>Duration</td><td>{!! $data['duration'] !!} years</td></tr>
                    @if($data['category'] == 'shares')
                        <tr><td>Shares</td><td>{!! $data['shares'] !!} shares</td></tr>
                    @elseif($data['category'] == 'product')
                        <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                    @endif
                    <tr><td>Start Date</td><td>{!! (new \Carbon\Carbon($data['start_date']))->format('l jS \\of F Y'); !!}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>