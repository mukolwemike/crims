<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Cancel Shares Purchase Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Buyer</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($order->buyer->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td>{!! $order->buyer->number !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td>{!! $order->buyer->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Number of Shares</td>
                        <td>{!! $order->number !!}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->price) !!}</td>
                    </tr>
                    @if($order->recipient)
                        <tr>
                            <td>Commission Recipient</td>
                            <td>{!! $order->recipient->name !!}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Commission Rate</td>
                        <td>{!! $order->commission_rate !!}%</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Total Value</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->price * $order->number) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>Current Funds</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($order->buyer->sharePaymentsBalance(\Carbon\Carbon::parse($order->request_date))) !!}</td>
                    </tr>
                    <tr>
                        <td>Good Till Filled/Cancelled</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($order->good_till_filled_cancelled)  !!}</td>
                    </tr>
                    @if(!$order->good_till_filled_cancelled)
                        <tr>
                            <td>Expiry Date</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($order->expiry_date) !!}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>