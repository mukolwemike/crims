<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            {{--<a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>--}}
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">

            <h3>Investments to be Rolled over</h3>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    <tr><td>Product</td><td>{!! $investment->product->name!!}</td></tr>
                </tbody>
            </table>
            <table class="table table-responsive table-hover">
                <thead>
                    <tr><td>Value Date</td><td>Amount</td><td>Maturity date</td><td>Value</td></tr>
                </thead>
                <tbody>
                    @foreach($investments  as $investment)
                        <tr>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                            <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                            <td>{!! \Cytonn\Presenters\AmountPresenter::currency($investment->value) !!} @if($investment->deleted_at) <span class="label label-danger">Rolled back</span>@endif</td>
                        </tr>
                    @endforeach
                    <tr>
                        <th>Total</th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('amount')) !!}</th>
                        <th></th>
                        <th>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('value')) !!}</th>
                    </tr>

                    @if($data['reinvest'] == 'topup')
                            <tr class="success"><td>Topup</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                            <tr><td>Total Value of new Investment</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'] + $investments->sum('value')) !!}</td></tr>
                    @elseif($data['reinvest'] == 'reinvest')
                            <tr class="danger"><td>Withdraw</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('value') - $data['amount'] ) !!}</td></tr>
                            <tr class="success"><td>Reinvest</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                    @elseif($data['reinvest'] == 'withdraw')
                            <tr class="danger"><td>Withdraw</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td></tr>
                            <tr class="success"><td>Reinvest</td><td colspan="2"></td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($investments->sum('value') - $data['amount'] ) !!}</td></tr>
                    @endif
                </tbody>
            </table>

            <h3>New Investment Details</h3>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['invested_date']) !!}</td></tr>
                <tr><td>Maturity date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['maturity_date']) !!}</td></tr>
                <tr><td>Total Days</td><td>{!! \Cytonn\Presenters\DatePresenter::getDateDifference($data['invested_date'], $data['maturity_date']) !!}</td></tr>
                <tr><td>FA</td><td>{!! $commissionRecepient->name !!}</td></tr>
                <tr><td>Commission Rate</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                @if(isset($data['commission_start_date']) && $data['commission_start_date'])
                    <tr><td>Commission Start Date</td><td>{!! $data['commission_start_date'] !!}</td></tr>
                @endif
                <tr><td>Interest payment</td><td>{!! $interestPayment[$data['interest_payment_interval']] !!} on date {!! $data['interest_payment_date'] !!}</td></tr>
                @if(isset($data['interest_action_id']))
                    <tr><td>Interest Action</td><td>{!! $interestAction($data['interest_action_id'])->name !!}</td></tr>
                @endif
                @if(isset($data['description']))
                    <tr><td>Transaction Description</td><td>{!! $data['description'] !!}</td></tr>
                @endif
                <tr><td><b>Previous Investment Details</b></td><td></td></tr>
                <tr><td>FA</td><td>{!! $investment->present()->commissionRecipientName !!}</td></tr>
                <tr><td>Interest Payment</td><td>{!! $investment->present()->getInterestPaymentDetails !!}</td></tr>
                @if($investment->interestAction)
                    <tr>
                        <td>Interest Action</td>
                        <td>{!! $investment->present()->getInterestActionDetails !!}</td>
                    </tr>
                @endif
                @if(isset($data['interest_reinvest_tenor']))
                    <tr>
                        <td>Interest Reinvest Tenor</td>
                        <td>
                            @if($data['interest_reinvest_tenor'] != 0)
                                {!! $data['interest_reinvest_tenor'] !!} months
                            @else
                                Until maturity of principal
                            @endif
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>


</div>