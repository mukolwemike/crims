<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <a class="pull-right" ng-click="toggleEdit()" ng-hide="showedit" href=""><i class="fa fa-edit"></i></a>
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                @if(isset($data['client_code']))
                    <tr><td>Client Code</td><td>{!! $data['client_code'] !!}</td></tr>
                @endif
                <tr><td>Product</td><td> {{  $product->name }}</td></tr>
                <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($clientinvestmentapplication->amount )!!}</td></tr>
                <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! $data['invested_date'] !!}</td></tr>
                <tr><td>Maturity date</td><td>{!! $data['maturity_date'] !!}</td></tr>
                <tr><td>Total Days</td><td>{!! \Cytonn\Presenters\DatePresenter::getDateDifference($data['invested_date'], $data['maturity_date']) !!}</td></tr>
                <tr><td>FA</td><td>{!! $commissionrecepient->name !!}</td></tr>
                <tr><td>Commission Rate</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                <tr><td>Interest payment</td><td>{!! $interestPayment[$data['interest_payment_interval']] !!} on date {!! $data['interest_payment_date'] !!}</td></tr>
                <tr><td>Client code</td><td>@if(isset($data['client_code'])) {!! $data['client_code'] !!} @endif</td></tr>
                @if(isset($data['description']))
                    <tr><td>Transaction Description</td><td>{!! $data['description'] !!}</td></tr>
                @endif
                @if(isset($data['interest_action_id']))
                    <tr><td>Interest Action</td><td>{!! $interestActions($data['interest_action_id'])->name !!}</td></tr>
                @endif
                @if(isset($data['on_call']))
                    <tr><td>On Call</td><td>{!! ($data['on_call']) ? 'Yes' : 'No' !!}</td></tr>
                @endif
                @if(isset($data['interest_reinvest_tenor']))
                    <tr>
                        <td>Interest Reinvest Tenor</td>
                        <td>
                            @if($data['interest_reinvest_tenor'] != 0)
                                {!! $data['interest_reinvest_tenor'] !!} months
                            @else
                                Until maturity of principal
                            @endif
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div ng-show="showedit">
                <h4>Edit details</h4>
                {!! Form::model((object) $data, []) !!}

                {!! Form::hidden('application_id', null) !!}


                <div class="form-group">
                    {!! Form::label('client_code', 'Client code: Suggestion - '.(new \Cytonn\Clients\ClientRepository())->suggestClientCode()) !!}

                    {!! Form::text('client_code', null, ['class'=>'form-control'] ) !!}
                    {!! \Cytonn\Presenters\ErrorsPresenter::formErrors($errors, 'client_code') !!}
                </div>


                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>


</div>