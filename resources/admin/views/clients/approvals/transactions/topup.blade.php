<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                @if(isset($data['topup_form_id']) && $data['topup_form_id'] != 0 )
                    <tr><td>Form Link</td><td><a href="/dashboard/investments/client-instructions/topup/{!! $data['topup_form_id'] !!}">See form</a> </td></tr>
                @endif
                <tr><td>Product</td><td>{!! $product->name !!}</td></tr>
                <tr><td>Amount</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount'] )!!}</td></tr>
                <tr><td>Interest Rate</td><td>{!! $data['interest_rate'] !!}%</td></tr>
                <tr><td>Invested date</td><td>{!! $data['invested_date'] !!}</td></tr>
                <tr><td>Maturity date</td><td>{!! $data['maturity_date'] !!}</td></tr>
                <tr><td>Total Days</td><td>{!! \Cytonn\Presenters\DatePresenter::getDateDifference($data['invested_date'], $data['maturity_date']) !!}</td></tr>
                <tr><td>FA</td><td>{!! $recipient->name !!}</td></tr>
                <tr><td>Commission Rate</td><td>{!! $data['commission_rate'] !!}%</td></tr>
                @if(isset($data['commission_start_date']) && $data['commission_start_date'])
                    <tr><td>Commission Start Date</td><td>{!! $data['commission_start_date'] !!}</td></tr>
                @endif
                <tr><td>Interest payment</td><td>{!! $interestPayment[$data['interest_payment_interval']] !!} on date {!! $data['interest_payment_date'] !!}</td></tr>
                @if(isset($data['interest_action_id']))
                    <tr><td>Interest Action</td><td>{!! $interestaction($data['interest_action_id'])->name !!}</td></tr>
                @endif
                @if(isset($data['interest_reinvest_tenor']))
                    <tr>
                        <td>Interest Reinvest Tenor</td>
                        <td>
                            @if($data['interest_reinvest_tenor'] != 0)
                                {!! $data['interest_reinvest_tenor'] !!} months
                            @else
                                Until maturity of principal
                            @endif
                        </td>
                    </tr>
                @endif
                @if(isset($data['description']))
                    <tr><td>Transaction Description</td><td>{!! $data['description'] !!}</td></tr>
                @endif
                <tr><td><b>Previous Investment Details</b></td><td></td></tr>
                <tr><td>FA</td><td>{!! $investment->present()->commissionRecipientName !!}</td></tr>
                <tr><td>Interest Payment</td><td>{!! $investment->present()->getInterestPaymentDetails !!}</td></tr>
                @if($investment->interestAction)
                    <tr>
                        <td>Interest Action</td>
                        <td>{!! $investment->present()->getInterestActionDetails !!}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>


</div>