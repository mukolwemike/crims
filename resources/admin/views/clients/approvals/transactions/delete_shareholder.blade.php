<div>
    <div class = "panel panel-success">
        <div class = "panel-heading">
            <h4>Share Holder details</h4>
        </div>

        <div class = "panel-body">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentJointFullNames($shareHolder->client_id) !!}</td>
                </tr>
                <tr>
                    <td>Added on</td>
                    <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($shareHolder->created_at) !!}</td>
                </tr>
                <tr>
                    <td>Number</td>
                    <td colspan="2">{!! $shareHolder->number !!}</td>
                </tr>
                <tr>
                    <td>E-Mail Address</td>
                    <td colspan="2">{!! $shareHolder->client->contact->email !!}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{!! $shareHolder->client->contact->phone !!}</td>
                </tr>
                <tr>
                    <td>Reason for Deletion</td>
                    <td>{!! $data['reason'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>