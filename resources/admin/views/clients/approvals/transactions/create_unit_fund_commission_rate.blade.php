<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Unit Fund Commission Rate</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td width="30%">Fund Name</td>
                        <td>{!! $fund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Commission Recipient Type</td>
                        <td>{!! $recipientType->name !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Commission Rate (%)</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['rate']) !!}%</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>