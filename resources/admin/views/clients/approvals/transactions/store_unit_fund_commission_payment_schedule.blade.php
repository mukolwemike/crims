<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store Commission Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th colspan="100%" style="text-align: center;">Unit Fund Purchase Details</th>
                </tr>
                <tr>
                    <td>Client</td>
                    <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($purchase->client_id) !!}</td>
                </tr>
                <tr>
                    <td>Client Code</td>
                    <td>{!! $purchase->client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Number of Units</td>
                    <td>{!! $purchase->number !!}</td>
                </tr>
                <tr>
                    <td>Purchase Price</td>
                    <td>{!! $purchase->price !!}</td>
                </tr>
                <tr>
                    <td>Purchase Cost</td>
                    <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase->price * $purchase->number) !!}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{!! $purchase->date !!}</td>
                </tr>
                <tr>
                    <td>FA</td>
                    <td>{!! $recipient->name !!}</td>
                </tr>
                @if($schedule)
                    <tr>
                        <th>Current Commission Payment Schedule</th>
                        <th>New Commission Payment Schedule</th>
                    </tr>
                    <tr>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['initial_date']) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                @else
                    <tr>
                        <th colspan="100%" style="text-align: center;">New Commission Payment Schedule</th>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Commission</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>