<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Upload Compliance Form</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>
                    <tr>
                        <td>Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>
                    <tr>
                        <td>Client E-mail</td>
                        <td>{!! $client->contact->email !!}</td>
                    </tr>
                    <tr>
                        <td>Document Type</td>
                        <td>{!! $document->type->name !!}</td>
                    </tr>
                    <tr>
                        <td>Uploaded On</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($document->created_at) !!}</td>
                    </tr>
                    <tr>
                        <td>Preview</td>
                        <td><a href="/dashboard/documents/{!! $document->id !!}"><i class="fa fa-file-pdf-o"></i> </a></td>
                    </tr>
                    <tr>
                        <th colspan="2" class="text-center">The KYC Documents Included Therein</th>
                    </tr>
                    @foreach($kycs as $kyc)
                        <tr>
                            <td>Name</td>
                            <td>{!! $kyc->name !!}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>