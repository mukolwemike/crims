<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Unit Fund Price Trail</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Fund Name</td>
                        <td>{!! $fund->name !!}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    @if(isset($totals['assets']))
                        <tr><td>Assets</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['assets']) }}</td></tr>
                    @endif
                    @if(isset($totals['cash']))
                        @foreach($totals['cash'] as $account)
                            <td>Acc Balance: {{ $account->name }}</td><td>{{ $account->currency }} {{ \Cytonn\Presenters\AmountPresenter::currency($account->balance) }}</td>
                        @endforeach
                    @endif
                    <tr>
                        <td>AUM</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['aum'])}}</td>
                    </tr>
                    @if($calc == 'daily-yield')
                        <tr>
                            <td>Today's liabilities</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['today_liabilities']) }}</td>
                        </tr>
                        <tr>
                            <td>Today's income</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['net_income_per_unit'], false, 6) }}</td>
                        </tr>
                        <tr>
                            <td>Net daily yield</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['net_daily_yield']) }}%</td>
                        </tr>
                        <tr>
                            <td>Net annual yield</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['net_annual_yield']) }}%</td>
                        </tr>
                    @endif
                    @if($calc == 'variable-unit-price')
                        <tr><td>NAV</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['nav']) }}</td></tr>
                        <tr><td>Liabilities</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['liabilities']) }}</td></tr>
                        <tr><td>No. of units</td><td>{{ \Cytonn\Presenters\AmountPresenter::currency($totals['units']) }}</td></tr>
                        <tr><td>Unit Price</td><td> {{ \Cytonn\Presenters\AmountPresenter::currency($totals['price']) }}</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>