<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Unit Fund Interest Payments - {!! \Cytonn\Presenters\DatePresenter::formatDate($date) !!}</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <table class="table table-hover table-responsive table-striped">
                    <tr>
                        <td colspan="100%">
                            <a href="{!! route('approval_action', [$approval->id, 'export']) !!}" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Export to excel</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Unit Fund</td>
                        <td>{!! $fund->name !!}</td>
                    </tr>
                    @if($approval->approved)
                        <tr>
                            <td colspan="100%">
                                <a href="{!! route('approval_action', [$approval->id, 'instruction']) !!}" class="btn btn-success pull-right">Download instruction</a>
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>