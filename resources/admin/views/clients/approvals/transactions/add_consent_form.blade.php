<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Personal Data Consent Form Details</h4>
        </div>
        <div class="panel-body">
            <table class = "table table-hover table-responsive table-striped" ng-hide = "showedit">
                <thead></thead>
                <tbody>

                <tr>
                    <td>Email</td>
                    <td>{!! $data['email'] !!}</td>
                </tr>

                <tr>
                    <td>Date</td>
                    <td>{!! $data['date'] !!}</td>
                </tr>

                <tr>
                    <td>Consented</td>
                    @if($data['consent'] == 1)
                        <td class="text-success"><span class="glyphicon glyphicon-check"></span></td>
                    @else
                        <td class="text-danger"><span class="glyphicon glyphicon-remove"></span></td>
                    @endif
                </tr>

                <tr>
                    <td>File</td>
                    @if(isset($data['document_id']))
                        <td><span class="glyphicon glyphicon-file"></span> <a target="_blank" href="/dashboard/investments/client-instructions/filled-application-documents/{!! $data['document_id'] !!}">View file</a></td>
                    @else
                        <td>No file uploaded</td>
                    @endif
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>