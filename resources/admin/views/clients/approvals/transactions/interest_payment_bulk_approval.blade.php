<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Interest Payments Bulk Approval - {!! \Cytonn\Presenters\DatePresenter::formatDate($date) !!}</h4>
        </div>
        <div class="panel-body">
            <div class="form-detail">
                <table class="table table-hover table-responsive table-striped">
                    <thead>
                        <tr>
                            <td>Client</td>
                            <td>Product</td>
                            <td>Date</td>
                            <td>Amount</td>
                            <td>Interest Action</td>
                            <td>Description</td>
                            <td>Status</td>
                            <td>Remove</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schedules as $schedule)
                            <tr>
                                <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($schedule->investment->client_id) !!}</td>
                                <td>
                                    {!! $schedule->investment->product->name !!}
                                </td>
                                <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($schedule->date)!!}</td>
                                <td>{!! \Cytonn\Presenters\AmountPresenter::currency($schedule->calculatePaymentAmount($schedule->date)) !!}</td>
                                <td>
                                    @if($schedule->investment->interest_action_id)
                                        {!! $schedule->investment->interestAction->name !!}
                                    @else
                                        Send to Bank
                                    @endif
                                </td>
                                <td>{!! $schedule->description !!}</td>
                                <td>{!! in_array($schedule->id, $approval->payload['removed']) ? '<a class="label label-danger">Not Sending</a>' : '<a class="label label-success">Sending</a>' !!}</td>
                                <td><a class="btn btn-danger btn-sm" href="{!! route('approval_action', [$approval->id, 'remove', $schedule->id]) !!}">Remove</a></td>
                            </tr>

                        @endforeach
                        <tr>
                            <td colspan="100%">
                                <a href="{!! route('approval_action', [$approval->id, 'export']) !!}" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Export to excel</a>
                            </td>
                        </tr>
                        @if($approval->approved)
                            <tr>
                                <td colspan="100%">
                                    <a href="{!! route('approval_action', [$approval->id, 'notify']) !!}" class="btn btn-info">Notify Clients</a>
                                    <a href="{!! route('approval_action', [$approval->id, 'instruction']) !!}" class="btn btn-success pull-right">Download instruction</a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>