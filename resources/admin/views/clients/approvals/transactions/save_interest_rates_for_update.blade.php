<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Save Interest Rates</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr><td>Product</td><td>{!! $update->product->name !!}</td></tr>
                    <tr><td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($update->date) !!}</td></tr>
                    <tr><td>Description</td><td>{!! $update->description !!}</td></tr>
                    <tr>
                        <th colspan="2" class="bold">RATES</th>
                    </tr>
                    <tr>
                        <th>Tenor</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rates as $rate_arr)
                        <tr>
                            <td>{!! $rate_arr['tenor'] !!} months</td>
                            <td>{!! $rate_arr['rate'] !!}%</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>