<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Client Relationship Person</h4>
        </div>
        <div class="panel-body">
            @if(is_null($client->relationshipPerson))
                <h5>Add Relationship Person</h5>
            @else
                <h5>Edit Relationship Person</h5>
            @endif
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! $relationship_person->name !!}</td>
                        <td>{!! $relationship_person->phone !!}</td>
                        <td>{!! $relationship_person->email !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>