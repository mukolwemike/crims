<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Match Sales Order to Purchase Order</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Seller</td>
                        <td colspan="8">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($seller->client_id) !!}</td>
                    </tr>
                    <tr>
                        <td>Number</td>
                        <td colspan="8">{!! $seller->number !!}</td>
                    </tr>
                    <tr>
                        <td>Current shares</td>
                        <td colspan="8">{!! $seller->shareHoldings->sum('number') !!}</td>
                    </tr>
                    <tr>
                        <td>Entity</td>
                        <td colspan="8">{!! $seller->entity->name !!}</td>
                    </tr>
                    <tr>
                        <td>Request Date</td>
                        <td colspan="8">{!! \Cytonn\Presenters\DatePresenter::formatDate($salesOrder->request_date) !!}</td>
                    </tr>
                    <tr>
                        <td>Shares for Sale</td>
                        <td colspan="8">{!! $salesOrder->number !!}</td>
                    </tr>
                    <tr>
                        <td>Shares Already Sold</td>
                        <td colspan="8">{!! $sold !!}</td>
                    </tr>
                    <tr>
                        <td>Balance available for sale</td>
                        <td colspan="8">{{ $balance }}</td>
                    </tr>
                    <tr>
                        <td>Price per Share</td>
                        <td colspan="8">{!! \Cytonn\Presenters\AmountPresenter::currency($agreedPrice) !!}</td>
                    </tr>
                    <tr style="background: yellow; color: red;">
                        <td>TOTAL</td>
                        <td colspan="8">{!! \Cytonn\Presenters\AmountPresenter::currency($total = $agreedPrice * $salesOrder->number) !!}</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <th>Buyer</th>
                    <th>Shareholder Number</th>
                    <th>Requested No.</th>
                    <th>PO Balance</th>
                    <th>Matched No.</th>
                    <th>PO Bal After Match</th>
                    <th>Buyer's Price</th>
                    <th>Seller's Price</th>
                    <th><i style="color: red;font-size: 8px;" class="fa fa-asterisk"></i> Agreed Price</th>
                    <th>Request Date</th>
                    <th>Total</th>
                    <th>FA</th>
                    <th>Comm. Rate</th>
                </tr>
                </thead>
                <tbody>
                @foreach($purchaseOrders as $purchase)
                    <tr>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($purchase->buyer->client_id) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase->buyer->number, false, 0) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase->number, false, 0) !!}</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($purchase->po_balance, false, 0) }}</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($purchase->matched_number, false, 0) }}</td>
                        <td>{{ \Cytonn\Presenters\AmountPresenter::currency($purchase->po_balance - $purchase->matched_number, false, 0) }}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($purchase->price) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($salesOrder->price) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($agreedPrice) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($purchase->request_date) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency( $purchase->matched_number * $agreedPrice) !!}</td>{{--NOTE: use the sale order price--}}
                        <td>{!! $purchase->recipient ? $purchase->recipient->name : null !!}</td>
                        <td>{!! $purchase->commission_rate !!}%</td>
                    </tr>
                @endforeach
                <tr>
                    <th>Total</th><td colspan="11"></td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($bought) !!}</th>{{--NOTE: use the sale order price--}}
                </tr>
                <tr>
                    <th>Balance</th><td colspan="11"></td><th>{!! \Cytonn\Presenters\AmountPresenter::currency($total - $bought) !!}</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>