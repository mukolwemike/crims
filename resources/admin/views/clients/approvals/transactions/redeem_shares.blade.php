<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Redeem Shares</h4>
        </div>
        <div class="panel-body">
            <h4>Shareholder Details</h4>
            <table class="table table-hover table-responsive table-striped">
                <thead></thead>
                <tbody>
                <tr><td>Client Name</td><td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($holder->client_id) !!}</td></tr>
                <tr><td>Membership No.</td><td>{!! $holder->number !!}</td></tr>
                <tr><td>Share Entity</td><td>{!! $entity->name !!}</td></tr>
                <tr><td>Redeem Date</td><td>{!! $data['date'] !!}</td></tr>
                <tr><td>Redeem Membership</td><td>@if(isset($data['membership_fee'])) Yes @else No @endif</td></tr>
                @if(isset($data['membership_fee']))
                    <tr>
                        <td>Membership Redeem Amount</td>
                        <td>{!! $membershipAmount !!}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <h4>Share Details</h4>
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Number</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Rate</th>
                    <th>Gross</th>
                    <th>Tax</th>
                    <th>Net</th>
                    <th>Tota</th>
                </tr>
                </thead>
                <tbody>
                @foreach($shares as $share)
                    <tr>
                        <td>{!! $share['date'] !!}</td>
                        <td>{!! $share['number'] !!}</td>
                        <td>{!! $share['price'] !!}</td>
                        <td>{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($share['amount']) !!}</td>
                        <td>{!! $share['rate'] !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($share['gross_interest']) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($share['withholding']) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($share['net_interest']) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($share['total']) !!}</td>
                    </tr>
                @endforeach
                <tr style="color:red; background:yellow;">
                    <th colspan="7" class="text-center">TOTAL</th>
                    <th colspan="2" class="text-center">{!! \App\Cytonn\Presenters\General\AmountPresenter::currency($shares->sum('total')) !!}</th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>