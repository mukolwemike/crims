<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Update Commission Clawback</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th colspan="100%" style="text-align: center;">Investment Details</th>
                </tr>
                <tr>
                    <td>Client</td><td colspan="2">{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($investment->client_id) !!}</td>
                </tr>
                <tr>
                    <td>Client Code</td><td colspan="2">{!! $investment->client->client_code !!}</td>
                </tr>
                <tr>
                    <td>Principal</td><td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                </tr>
                <tr>
                    <td>Interest Rate</td><td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->interest_rate) !!}%</td>
                </tr>
                <tr>
                    <td>Invested Date</td><td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                </tr>
                <tr>
                    <td>Maturity Date</td><td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
                <tr>
                    <td>Commission Recipient</td><td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->commission->recipient->name) !!}</td>
                </tr>
                <tr>
                    <th colspan="100%" style="text-align: center;">Commission Clawback</th>
                </tr>
                <tr>
                    <th></th><th>Current Details</th><th>Updated Details</th>
                </tr>
                <tr>
                    <td>Date</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($clawback->date) !!}</td><td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                </tr>
                <tr>
                    <td>Commission</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($clawback->amount) !!}</td><td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                </tr>
                <tr>
                    <td>Narration</td><td>{!! $clawback->narration !!}</td><td>{!! $data['narration'] !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>