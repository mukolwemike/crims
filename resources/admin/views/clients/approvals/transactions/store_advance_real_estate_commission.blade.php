<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store Commission Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th colspan="3">Real Estate Unit Details</th>
                </tr>
                <tr>
                    <td>Project</td>
                    <td colspan="2">{!! $holding->project->name !!}</td>
                </tr>
                <tr>
                    <td>Unit</td>
                    <td colspan="2">{!! $holding->unit->number !!} ({!! $holding->unit->size->name !!}) {!! $holding->unit->type->name !!}</td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($holding->price()) !!}</td>
                </tr>
                <tr>
                    <th colspan="3"> Commission Details</th>
                </tr>
                <tr>
                    <td>Financial Advisor</td>
                    <td colspan="2">{!! $holding->commission->recipient->name !!}</td>
                </tr>
                <tr>
                    <td>Commission Rate</td>
                    <td colspan="2">{!! $holding->commission->commission_rate !!}</td>
                </tr>
                <tr>
                    <td>Total Commission</td>
                    <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($holding->commission->amount) !!}</td>
                </tr>
                <tr>
                    <th colspan="3">Additional Commission Details</th>
                </tr>
                @if($advanceCommission)
                    <tr>
                        <th>Current Details</th>
                        <th>New Details</th>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($advanceCommission->date) !!}</td>
                        <td>{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($advanceCommission->amount) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $advanceCommission->description !!}</td>
                        <td>{!! $data['description'] !!}</td>
                    </tr>
                    <tr>
                        <td>Repayable</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($advanceCommission->repayable) !!}</td>
                        <td>{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['repayable']) !!}</td>
                    </tr>
                @else
                    <tr>
                        <th colspan="3">New Details</th>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($data['date']) !!}</td>
                    </tr>
                    <tr>
                        <td>Commission</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td colspan="2">{!! $data['description'] !!}</td>
                    </tr>
                    <tr>
                        <td>Repayable</td>
                        <td colspan="2">{!! \Cytonn\Presenters\BooleanPresenter::presentYesNo($data['repayable']) !!}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>