<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <h4>Details Before</h4>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <tbody>
                <tr>
                    <td>Client Name</td>
                    <td>{{\Cytonn\Presenters\ClientPresenter::presentFullNameNoTitle($client->id)}}</td>
                </tr>
                <tr>
                    <td>Client ID Number</td>
                    <td>{{$client->id_or_passport}}</td>
                </tr>
                <tr>
                    <td>Client Tax/PIN Number</td>
                    <td>{{$client->pin_no}}</td>
                </tr>
                @if($client->jointDetail)
                    @foreach($client->jointDetail as $joint)
                        <tr>
                            <td>Joint Name</td>
                            <td>{{\Cytonn\Presenters\ClientPresenter::presentJointHolderFullName($joint->id)}}</td>
                        </tr>
                        <tr>
                            <td>Joint ID Number</td>
                            <td>{!! $joint->id_or_passport !!}</td>
                        </tr>
                        <tr>
                            <td>Joint Tax/PIN Number</td>
                            <td>{{$joint->pin_no}}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <h4> Uploads Before</h4>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead>
                <tr>
                    <th>Item</th>
                    <th></th>
                    <th>Number</th>
                    <th>Upload</th>
                </tr>
                </thead>
                <tbody>
                @if($client->clientType->name == 'individual')
                    @foreach($kycClient as $item)
                        @if($item->kyc_id == 1 || $item->kyc_id == 2)
                            <tr>
                                <td>{{$item->type->name}}</td>
                                <td><i class="label label-primary">Client</i></td>
                                <td>{{$item->kyc_id == 1 ? $item->client->id_or_passport : $item->client->pin_no}}</td>
                                <td><a target="_blank" href="/dashboard/clients/kyc/document/{{$item->document_id}}"><i
                                                class="fa fa-eye"></i></a></td>
                            </tr>
                        @endif
                    @endforeach

                    @if($client->jointDetail)
                        @foreach($client->jointDetail as $joint)
                            @foreach($joint->uploadedKyc as $item)
                                @if($item->kyc_id == 1 || $item->kyc_id == 2)
                                    <tr>
                                        <td>{{$item->type->name}}</td>
                                        <td><i class="label label-info">Joint</i></td>
                                        <td>{{$item->kyc_id == 1 ? $item->jointClient->id_or_passport : $item->jointClient->pin_no}}</td>
                                        <td><a target="_blank"
                                               href="/dashboard/clients/kyc/document/{{$item->document_id}}"><i
                                                        class="fa fa-eye"></i></a></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                    @endif
                @else
                    @foreach($kycClient as $item)
                        <tr>
                            <td>{{$item->type->name}}</td>
                            <td></td>
                            <td>
                                {{$item->kyc_id == 8 ? $item->client->pin_no : ''}}
                            </td>
                            <td><a target="_blank"
                                   href="/dashboard/clients/kyc/document/{{$item->document_id}}"><i
                                            class="fa fa-eye"></i></a></td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>

            <h4>Details Now</h4>
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <tbody>
                @if($client->clientType->name == 'individual')
                    <tr>
                        <td>Client ID Number</td>
                        <td>{{$data['id_or_passport']}}</td>
                    </tr>
                    <tr>
                        <td>Client PIN/Tax NUmber</td>
                        <td>{{$data['pin_no']}}</td>
                    </tr>

                    @if(isset($data['jointDetails']))
                        @foreach($data['jointDetails'] as $joint)
                            <tr>
                                <td>Joint ID Number</td>
                                <td>{{$joint['id_or_passport']}}</td>
                            </tr>

                            <tr>
                                <td>Joint PIN/Tax Number</td>
                                <td>{{$joint['pin_no']}}</td>
                            </tr>
                        @endforeach
                    @endif
                @else
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                @endif
                </tbody>
            </table>
            @if(count($data['docsUploaded']))
                <h4>Uploads Now</h4>
                <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th></th>
                        <th>Number</th>
                        <th>Upload</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data['docsUploaded'] as $upload)
                        <tr>
                            <td>
                                @if(isset($upload['slug']))
                                    {{ \Cytonn\Presenters\ClientPresenter::presentUploadedKyc($upload['slug']) }}
                                @endif
                            </td>
                            <td>
                                @if(isset($upload['jointId']))
                                    <i class="label label-info">Joint</i>
                                @else
                                    <i class="label label-primary">Client</i>
                                @endif
                            </td>
                            <td>{{isset($upload['number']) ? $upload['number'] : ''}}</td>
                            <td><a target="_blank" href="/dashboard/clients/kyc/document/{{$upload['document_id']}}"><i
                                            class="fa fa-eye"></i></a></td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>