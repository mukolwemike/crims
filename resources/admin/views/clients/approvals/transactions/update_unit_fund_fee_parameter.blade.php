<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Unit Fund Fee Parameter</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>New</th>
                        <th>Current</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                        <td>{!! $parameter->name !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>