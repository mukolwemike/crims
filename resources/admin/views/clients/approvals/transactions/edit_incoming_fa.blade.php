<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>
                Edit Client Incoming Financial Advisor
            </h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>Before</th>
                    <th>Now</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td>{!! $previous_fa->name !!}</td>
                    <td>{!! $incoming_fa->name !!}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{!! $previous_fa->phone !!}</td>
                    <td>{!! $incoming_fa->phone !!}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{!! $previous_fa->email !!}</td>
                    <td>{!! $incoming_fa->email !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>