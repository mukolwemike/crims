<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Joint Holders</h4>
        </div>
        <div class="panel-body">
            @if($holders)
                @foreach($holders as $holder)
                    <div class="detail-group">
                        <p class="col-md-4">Name:  {!! $holder->firstname !!} {!! isset($holder->middlename) ? $holder->middlename : '' !!} {!! $holder->lastname !!}</p>
                        <p class="col-md-4">Email:  {!! $holder->email !!} </p>
                        <p class="col-md-4">Date of Birth:  {!! (isset($holder->dob) ?$holder->dob: null) !!}</p>
                        <p class="col-md-4">Pin No.: {!! $holder->pin_no !!}</p>
                        <p class="col-md-4">ID/Passport Number: {!! $holder->id_or_passport !!}</p>
                        <p class="col-md-4">Postal code: {!! isset($holder->postal_code) ? $holder->postal_code : '' !!}</p>
                        <p class="col-md-4">Postal address: {!! isset($holder->postal_address) ? $holder->postal_address : '' !!}</p>
                        <p class="col-md-4">Phone: {!! $holder->telephone_cell !!}</p>
                        @if(isset($holder->residential_address))
                            <p class="col-md-4">Residential Address: {!! $holder->residential_address !!}</p>
                        @endif
                        <p class="col-md-4">Town: {!! isset($holder->town) ? $holder->town : '' !!}</p>
                    </div>
                @endforeach
            @else
                <p>There are no joint holders</p>
            @endif
        </div>
    </div>
</div>