<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Store Investment Payment Schedule</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                <tr>
                    <th colspan="100%" style="text-align: center;">Investment Details</th>
                </tr>
                <tr>
                    <td>Product</td>
                    <td colspan="2">{!! $investment->product->name !!}</td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($investment->amount) !!}</td>
                </tr>
                <tr>
                    <td>Invested Date</td>
                    <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->invested_date) !!}</td>
                </tr>
                <tr>
                    <td>Maturity Date</td>
                    <td colspan="2">{!! \Cytonn\Presenters\DatePresenter::formatDate($investment->maturity_date) !!}</td>
                </tr>
                <tr>
                    <td>Interest Rate</td>
                    <td colspan="2">{!! $investment->interest_rate !!}%</td>
                </tr>
                <tr>
                    <th colspan="100%" style="text-align: center;">Schedule Details</th>
                </tr>
                @if($scheduleDetail)
                    <tr>
                        <th></th>
                        <th>Current Investment Payment Schedule</th>
                        <th>New Investment Payment Schedule</th>
                    </tr>
                    <tr>
                        <td>Contribution Amount</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount_before']) !!}</td>
                        <td>{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Contribution Date</td>
                        <td>On day {!! $data['payment_date_before'] !!}</td>
                        <td>On day {!! $data['payment_date'] !!}</td>
                    </tr>
                    <tr>
                        <td>Contribution Interval</td>
                        <td>Every {!! $data['payment_interval_before'] !!} month(s)</td>
                        <td>Every {!! $data['payment_interval'] !!} month(s)</td>
                    </tr>
                @else
                    <tr>
                        <th colspan="100%" style="text-align: center;">New Investment Payment Schedule Details</th>
                    </tr>
                    <tr>
                        <td>Contribution Amount</td>
                        <td colspan="2">{!! \Cytonn\Presenters\AmountPresenter::currency($data['amount']) !!}</td>
                    </tr>
                    <tr>
                        <td>Contribution Date</td>
                        <td colspan="2">On day {!! $data['payment_date'] !!}</td>
                    </tr>
                    <tr>
                        <td>Contribution Interval</td>
                        <td colspan="2">Every {!! $data['payment_date'] !!} month(s)</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
