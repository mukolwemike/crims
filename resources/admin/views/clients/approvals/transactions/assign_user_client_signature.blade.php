<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Assign Client Signature to Client User</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Client Name</td>
                        <td>{!! \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) !!}</td>
                    </tr>

                    <tr>
                        <td>Client Code</td>
                        <td>{!! $client->client_code !!}</td>
                    </tr>

                    <tr>
                        <td>User Name</td>
                        <td>{!! $user->present()->fullName !!}</td>
                    </tr>

                    <tr>
                        <td>User Email</td>
                        <td>{!! $user->email !!}</td>
                    </tr>

                    <tr>
                        <td>Signature Name</td>
                        <td>{!! $signature->present()->getName !!}</td>
                    </tr>

                    <tr>
                        <td>Signature File</td>
                        <td><a target="_blank" class="btn btn-success" href="/dashboard/documents/{!! $signature->document->id !!}">View file</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>