<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Create Bank</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Swift Code</td>
                        <td>{!! $data['swift_code'] !!}</td>
                    </tr>
                    <tr>
                        <td>Clearing Code</td>
                        <td>{!! $data['clearing_code'] !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>