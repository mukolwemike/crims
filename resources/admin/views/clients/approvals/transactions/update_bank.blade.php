<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Edit Bank</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>New</th>
                        <th>Current</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $data['name'] !!}</td>
                        <td>{!! $bank->name !!}</td>
                    </tr>
                    <tr>
                        <td>Swift Code</td>
                        <td>{!! $data['swift_code'] !!}</td>
                        <td>{!! $bank->swift_code !!}</td>
                    </tr>
                    <tr>
                        <td>Clearing Code</td>
                        <td>{!! $data['clearing_code'] !!}</td>
                        <td>{!! $bank->clearing_code !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


</div>