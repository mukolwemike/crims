<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Transaction details</h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-responsive table-striped" ng-hide="showedit">
                <thead></thead>
                <tbody>
                    @if($editedApplication)
                        <tr>
                            <td></td>
                            <td><b>New Application Details</b></td>
                            <td><b>Old Application Details</b></td>
                        </tr>
                        <tr>
                            <td>Product</td>
                            <td>{!! $application->product->name !!}</td>
                            <td>{!! $editedProduct->name !!}</td>
                        </tr>
                        <tr>
                            <td>Invested Amount</td>
                            <td>{!! $app->amount !!}</td>
                            <td>{!! $editedApplication['amount'] !!}</td>
                        </tr>
                        <tr>
                            <td>Tenor In Months</td>
                            <td>{!! $app->tenor !!}</td>
                            <td>{!! $editedApplication['tenor'] !!}</td>
                        </tr>
                        <tr>
                            <td>Interest Rate</td>
                            <td>{!! $app->agreed_rate.'%' !!}</td>
                            <td>{!! $editedApplication['agreed_rate'] . '%' !!}</td>
                        </tr>
                        <tr>
                            <td>Funds Source</td>
                            <td>{!! $fundsource->name  !!}</td>
                            <td>{!! $editedFundSource->name !!}</td>
                        </tr>
                        <tr>
                            <td>Funds Source Other</td>
                            <td>{!! $app->funds_source_other  !!}</td>
                            <td>{!! $editedApplication['funds_source_other'] !!}</td>
                        </tr>

                    @endif
                    <tr>
                        <td colspan="3">
                            <!-- Button trigger modal -->
                            @if($duplicate && $duplicate->exists)
                                <p>The client matched the following duplicate records</p>
                                <ol>
                                    @foreach($duplicate->clients as $client)
                                        <li><a href="/dashboard/clients/details/{{ $client->id }}">
                                                {{ \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) }}
                                                - {{ $client->client_code }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ol>
                            @endif

                            @if($riskyClient && $riskyClient->exists)
                                <div class="alert alert-warning" role="alert">
                                    <p class="text-warning">The Client Details matched the following Client who is deemed <strong>RISKY</strong></p>
                                </div>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name/Organization</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <a target="_blank" href="/dashboard/client/risky/show/{{$riskyClient->id}}">
                                                @if(is_null($riskyClient->firstname)) {!! $riskyClient->organization  !!} @else {!! $riskyClient->firstname. ' '. $riskyClient->lastname !!} @endif
                                            </a>
                                        </td>
                                        <td>{!! $riskyClient->email !!}</td>
                                        <td>
                                            @if($riskyClient->risky_status_id === 1)<span class="label label-warning">INVESTIGATING</span>
                                            @elseif($riskyClient->risky_status_id === 2)<span class="label label-danger">RISKY</span>
                                            @elseif($riskyClient->risky_status_id === 3)<span class="label label-success">CLEARED</span>@endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="alert alert-info">
                                    <p class="text-info"><strong>Reason for Risky Application:</strong></p>
                                    <p class="text-capitalize">{{ is_null($application->risk_reason) ? 'Reason not Given' : $application->risk_reason }}</p>
                                </div>
                            @endif

                            @if($application->new_client == 3)
                                <div class="alert alert-danger" role="alert">
                                    <p class="text-capitalize">The sender has requested that the client be added as a duplicate.</p>
                                </div>

                                <div class="alert alert-info" role="alert">
                                    <p>
                                        <strong>Reason:</strong>&nbsp;<span class="text-capitalize">{{$application->duplicate_reason}}</span>
                                    </p>
                                </div>
                            @elseif($application->new_client == 2)
                                <p>This application belongs to an existing client
                                    <a href="/dashboard/clients/details/{{ $client->id }}">
                                        {{ \Cytonn\Presenters\ClientPresenter::presentFullNames($client->id) }}
                                        - {{ $client->client_code }}
                                    </a>
                                </p>
                            @else
                                @if($duplicate && $duplicate->exists)
                                    <p class="warning">You will not be able to approve this unless it's added as a duplicate</p>
                                @endif
                            @endif
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                View Application Details
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Client Investment Application</h4>
            </div>
            <div class="">

                    @include('investment.partials.application_partial')

                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
